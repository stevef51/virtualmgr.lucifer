# Lucifer Helm deployment

### Terminology
1. AKS - Azure Kubernetes Service
1. k8s - Kubernetes
1. ACR - Azure Container Registry

## Creating up the AKS cluster
The lucifer-dev-centralus cluster was created through the Azure portal

## Connecting to the AKS cluster
The Azure Portal for the AKS cluster will have details on how to connect, but the following command connects to the lucifer-dev-centralus cluster

```
az aks get-credentials --resource-group lucifer-dev --name lucifer-dev-centralus
```

## Setting up the AKS cluster
### Helm
In order for Helm to be able to run it needs to have Tiller installed as a k8s pod, this is done by

```
helm init
```

## Azure Container Registry
In order for k8s to pull images from the Azure Container Registry (virtualmgr.azurecr.io) it needs a _secret_ defined to authenticate with

https://docs.microsoft.com/en-us/azure/aks/cluster-container-registry-integration


## Opening the AKS Cluster dashboard
Once connected to the AKS cluster, the following will setup a SSH tunnel and open a browser to the clusters dashboard

```
az aks browse --resource-group lucifer-dev --name lucifer-dev-centralus
```

## Public IP Load Balancer
Normally when creating a k8s Deployment with a LoadBalancer Service, AKS will create a new Static IP address and assign it to the LoadBalancer, this is fine but means if the Deployment is deleted then the LoadBalancer along with the Static IP address is also deleted, re-creating the Deployment will create a new different Static IP - in order for DNS entries to work without having to update them each time the Deployment is re-cycled then the Static IP needs to be created once and the LoadBalancer configured to use this address rather than allocating one dynamically.
The following Azure Docs show how this is done
https://docs.microsoft.com/en-us/azure/aks/static-ip

The current IP called lucifer-dev-centralUS-IP was created with the following command, note the embedded shell call to retrieve the _node_ resource group (see the Azure docs above)

```
az network public-ip create -n lucifer-dev-centralUS-IP -g $(az aks show -g lucifer-dev -n lucifer-dev-centralus --query nodeResourceGroup -o tsv) --allocation-method static --sku Standard --location centralus
```

This IP address is then plugged into the _values.yaml_ file under _traefik.loadBalancerIP_




