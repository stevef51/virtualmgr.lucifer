#!/bin/bash
RELEASE=${1-lucifer-common}

helm upgrade $RELEASE . --install --atomic --timeout 20m0s
