#!/bin/bash
RELEASE=${1-lucifer-common}

helm upgrade $RELEASE . --install --dry-run --debug