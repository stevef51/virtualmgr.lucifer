#!/bin/bash
IMAGETAG=$1
VALUESFILE=$2
FORCEREBOOT=$3

if [ "$FORCEREBOOT" == "--reboot" ]; then
    REBOOTOPT="--set VM_FORCEREBOOT=\"$RANDOM\""
fi

cmd="helm upgrade lucifer-central . -f $VALUESFILE --set imageTag=$IMAGETAG --install --atomic --namespace default $REBOOTOPT"
echo $cmd
($cmd)

