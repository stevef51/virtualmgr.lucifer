#!/bin/bash
RELEASE=$1
IMAGETAG=$1
FORCEREBOOT=$2

function usage {
    echo "./helm-install.sh IMAGETAG [--reboot]"
    echo "Example:  ./helm-install.sh master.20191107.3"
    echo "--reboot:  Will force Kubernetes to restart Services within this Helm Chart"
    exit
}

if [ -z "$RELEASE" ]; then 
    usage
fi

if [ -z "$IMAGETAG" ]; then 
    usage
fi

if [ "$FORCEREBOOT" == "--reboot" ]; then
    REBOOTOPT="--set VM_FORCEREBOOT=\"$RANDOM\""
fi

cmd="helm upgrade $RELEASE . --install --set imageTag=$IMAGETAG --atomic --namespace default $REBOOTOPT"
echo $cmd
($cmd)
