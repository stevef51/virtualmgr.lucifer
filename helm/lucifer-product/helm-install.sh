#!/bin/bash
PRODUCT=$1

function usage {
    echo "./helm-install.sh PRODUCT"
    echo "Example:  ./helm-install.sh foodsafe.tech"
    exit
}

if [ -z "$PRODUCT" ]; then 
    usage
fi

helm install . -n $PRODUCT -f ./${PRODUCT}.yaml --atomic

