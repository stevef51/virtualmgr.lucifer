PRODUCT=$1
RELEASE=$2

function usage {
    echo "./helm-install.sh PRODUCT RELEASE"
    echo "Example:  ./helm-install.sh foodsafe.tech master.20191112.19"
    exit
}

if [ -z "$RELEASE" ]; then 
    usage
fi
if [ -z "$PRODUCT" ]; then 
    usage
fi

helm install -n $PRODUCT -f ./${PRODUCT}.yaml --set lucifer-release=$RELEASE --dry-run --debug .
