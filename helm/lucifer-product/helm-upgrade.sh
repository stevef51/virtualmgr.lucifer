#!/bin/bash
PRODUCT=$1

function usage {
    echo "./helm-upgrade.sh PRODUCT"
    echo "Example:  ./helm-upgrade.sh foodsafe.tech"
    exit
}

if [ -z "$PRODUCT" ]; then 
    usage
fi

helm upgrade $PRODUCT . -f ./${PRODUCT}.yaml --atomic

