const shell = require('shelljs');
const fs = require('fs');
const _ = require('lodash');
const moment = require('moment');

const sites = JSON.parse(fs.readFileSync('./sites.json'));

/* The code below will take JSON output from the 
https://vmgrletsencrypt.azurewebsites.net/api/get-sites-information
and generate a simple JSON array of the sites to check, sites.json should be a simple array

const sites = _.chain(sites)
    .map(a => a.sites.map(
        b => b.slots.map(
            c => c.domains.map(
                d => d.name))))
    .flattenDeep()
    .uniq()
    .sort()
    .value();
console.log(JSON.stringify(sites));
return;
*/

var expiring = [];
const rex = /notBefore=(?<notBefore>.*)\s*notAfter=(?<notAfter>.*)\s*issuer=.* CN = (?<issuer>.*)\s*/;
const now = moment.utc();

let promises = sites.map(site => {
    var url = site.replace(/^\*/, 'tenant-master');

    return new Promise(function (resolve, reject) {
        shell.exec(`echo | openssl s_client -servername ${url} -connect ${url}:443 2>/dev/null | openssl x509 -noout -dates -issuer`, { silent: true }, function (code, stdout, stderr) {
            if (code !== 0) {
                console.error(`Check for ${site} at ${url} returned ${stderr}`);
                resolve();
                return;
            }
            var [, notBeforeText, notAfterText, issuer] = rex.exec(stdout);

            var notAfter = moment(notAfterText, 'MMM DD HH:mm:ss YYYY');

            var expiresIn = Math.round(moment.duration(notAfter.diff(now)).asDays());
            process.stderr.write(`.`);

            expiring.push({
                site, url, expiresIn
            });

            resolve();
        });
    });
});

console.log('Waiting..');
Promise.all(promises).then(() => {
    var ordered = _.sortBy(expiring, e => e.expiresIn);

    console.log(JSON.stringify(ordered));
});