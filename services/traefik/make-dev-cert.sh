#!/bin/bash

openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout ssl/$1.key -out ssl/$1.crt -subj '/CN=*.$1'

