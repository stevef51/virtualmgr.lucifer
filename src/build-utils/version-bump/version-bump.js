const program = require('commander');
const moment = require('moment');
const BlueBird = require('bluebird');
const fs = BlueBird.promisifyAll(require('fs'));

program
    .version('1.0.0.0')
    .option('-f, --file [file]')
    .parse(process.argv);

async function main(options) {
    let file = JSON.parse(await fs.readFileAsync(options.file));

    file = file || {};
    let split = (file.version || "1.0.0").split('.');
    split.length = 3;
    split.push(moment().format('YYYYMMDDhhmmss'));
    file.version = split.join('.');

    await fs.writeFileAsync(options.file, JSON.stringify(file));
}

try {
    main(program).then(() => {
        console.log('Finished');
    })
} catch (err) {
    console.error(err);
}