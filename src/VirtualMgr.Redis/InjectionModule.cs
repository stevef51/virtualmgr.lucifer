using System;
using System.IO;
using Microsoft.Extensions.DependencyInjection;
using ServiceStack.Redis;
using VirtualMgr.Common;
using VirtualMgr.MultiTenant;

namespace VirtualMgr.Redis
{
    public class InjectionModule : ServiceCollection
    {
        public InjectionModule(string redisUri)
        {
            this.AddStackExchangeRedisCache(options => options.Configuration = redisUri);
        }
    }
}