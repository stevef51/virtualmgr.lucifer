﻿using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;

namespace VirtualMgr.AspNetCore.Authorization
{
    public class AuthorizeBearerAttribute : AuthorizeAttribute
    {
        public AuthorizeBearerAttribute()
        {
            this.AuthenticationSchemes = Consts.Bearer;
        }
    }
}
