﻿using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Authorization;

namespace VirtualMgr.AspNetCore.Authorization
{
    public class AuthorizeOpenIdAttribute : AuthorizeAttribute
    {
        public AuthorizeOpenIdAttribute()
        {
            this.AuthenticationSchemes = Consts.OpenIdConnect;
        }
    }
}
