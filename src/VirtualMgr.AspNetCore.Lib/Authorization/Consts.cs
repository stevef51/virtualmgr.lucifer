﻿using System;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;

namespace VirtualMgr.AspNetCore.Authorization
{
    public static class Consts
    {
        public const string OpenIdConnect = OpenIdConnectDefaults.AuthenticationScheme;
        public const string Cookies = CookieAuthenticationDefaults.AuthenticationScheme;
        public const string Bearer = IdentityServerAuthenticationDefaults.AuthenticationScheme;

        public static readonly string[] SignOutSchemes = { OpenIdConnect, Cookies };
    }
}
