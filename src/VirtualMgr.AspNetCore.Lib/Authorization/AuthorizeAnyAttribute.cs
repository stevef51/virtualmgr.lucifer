﻿using Microsoft.AspNetCore.Authorization;

namespace VirtualMgr.AspNetCore.Authorization
{
    public class AuthorizeAnyAttribute : AuthorizeAttribute
    {
        public AuthorizeAnyAttribute()
        {
            this.AuthenticationSchemes = $"{Consts.Bearer},{Consts.OpenIdConnect}";
        }
    }
}
