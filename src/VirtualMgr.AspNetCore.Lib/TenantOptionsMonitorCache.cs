using System;
using System.Collections.Concurrent;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using VirtualMgr.MultiTenant;

namespace VirtualMgr.AspNetCore
{
    public class TenantOptionsMonitorCache<TOptions> : IOptionsMonitorCache<TOptions> where TOptions : class
    {
        private readonly ConcurrentDictionary<string, ConcurrentDictionary<string, TOptions>> _cache = new ConcurrentDictionary<string, ConcurrentDictionary<string, TOptions>>();
        private readonly IHttpContextAccessor _httpContextAccessor;

        public TenantOptionsMonitorCache(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        private TResult performForTenantOrAll<TResult>(Func<ConcurrentDictionary<string, TOptions>, TResult> fnPerform)
        {
            if (_httpContextAccessor.HttpContext != null)
            {
                var appTenant = _httpContextAccessor.HttpContext.RequestServices.GetRequiredService<AppTenant>();
                var tenantCache = _cache.GetOrAdd(appTenant.PrimaryHostname, _ => new ConcurrentDictionary<string, TOptions>());
                return fnPerform(tenantCache);
            }
            else
            {
                // called outside of a HttpContext, likely someone has edited appsettings.json, perform across all tenants Options with said name
                TResult last = default(TResult);
                foreach (var tenantCache in _cache.Values)
                {
                    last = fnPerform(tenantCache);
                }
                return last;
            }
        }

        public void Clear()
        {
            performForTenantOrAll(tenantCache =>
            {
                tenantCache.Clear();
                return true;
            });
        }

        public TOptions GetOrAdd(string name, Func<TOptions> createOptions)
        {
            return performForTenantOrAll(tenantCache =>
            {
                return tenantCache.GetOrAdd(name, _ => createOptions());
            });
        }

        public bool TryAdd(string name, TOptions options)
        {
            return performForTenantOrAll(tenantCache =>
            {
                return tenantCache.TryAdd(name, options);
            });
        }

        public bool TryRemove(string name)
        {
            return performForTenantOrAll(tenantCache =>
            {
                TOptions value;
                return tenantCache.TryRemove(name, out value);
            });
        }
    }
}