using System.Net.Http;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using VirtualMgr.AspNetCore.Authorization;
using VirtualMgr.MultiTenant;

namespace VirtualMgr.AspNetCore
{
    public class TenantConfigureOpenIdConnectOptions : IConfigureNamedOptions<OpenIdConnectOptions>
    {
        private readonly IHttpContextAccessor _contextAccessor;
        private readonly ServiceOptions _serviceOptions;

        public TenantConfigureOpenIdConnectOptions(IHttpContextAccessor contextAccessor, IOptions<ServiceOptions> serviceOptions)
        {
            _contextAccessor = contextAccessor;
            _serviceOptions = serviceOptions.Value;
        }

        public void Configure(string name, OpenIdConnectOptions options)
        {
            var appTenant = _contextAccessor.HttpContext.RequestServices.GetRequiredService<AppTenant>();
            options.SignInScheme = Consts.Cookies;
            options.Backchannel = new HttpClient();
            options.Backchannel.DefaultRequestHeaders.Add(_serviceOptions.TenantDomainHeader, $"{_serviceOptions.Services.Auth}.{appTenant.PrimaryHostname}");

            options.Authority = $"{_serviceOptions.IdentityServer.Scheme}{_serviceOptions.IdentityServer.Hostname(appTenant)}/{_serviceOptions.Services.Auth}";
            options.RequireHttpsMetadata = _serviceOptions.IdentityServer.RequireHttps;

            options.ClientId = $"{appTenant.PrimaryHostname}/{_serviceOptions.ServiceName}";
            options.ClientSecret = "secret";
            options.ResponseType = "code id_token";

            options.SaveTokens = true;
            options.GetClaimsFromUserInfoEndpoint = true;

            options.Scope.Add("VirtualMgr.Api.Lingo");
            options.Scope.Add("offline_access");

            options.ClaimActions.MapJsonKey("website", "website");

            options.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters()
            {
                NameClaimType = "name",
                RoleClaimType = "role"
            };
        }

        public void Configure(OpenIdConnectOptions options) => Configure(Options.DefaultName, options);
    }
}