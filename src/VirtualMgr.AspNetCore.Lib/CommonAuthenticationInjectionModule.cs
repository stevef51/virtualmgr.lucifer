using System.IO;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using VirtualMgr.AspNetCore.Authorization;

namespace VirtualMgr.AspNetCore
{
    public class CommonAuthenticationInjectionModule : ServiceCollection
    {
        public CommonAuthenticationInjectionModule(ServiceOptions serviceOptions)
        {
            this.AddAuthentication(options =>
            {
                options.DefaultChallengeScheme = Consts.OpenIdConnect;
            })
            .AddCookie(Consts.Cookies)
            .AddJwtBearer(Consts.Bearer, _ => { }) // Configured per tenant via DI below
            .AddOpenIdConnect(Consts.OpenIdConnect, _ => { });    // Configured per tenant via DI below

            // The following splits the caching of Options per tenant
            this.AddTransient<IOptionsMonitorCache<OpenIdConnectOptions>, TenantOptionsMonitorCache<OpenIdConnectOptions>>();
            this.AddTransient<IOptionsMonitorCache<CookieAuthenticationOptions>, TenantOptionsMonitorCache<CookieAuthenticationOptions>>();
            this.AddTransient<IOptionsMonitorCache<IdentityServerAuthenticationOptions>, TenantOptionsMonitorCache<IdentityServerAuthenticationOptions>>();
            this.AddTransient<IOptionsMonitorCache<JwtBearerOptions>, TenantOptionsMonitorCache<JwtBearerOptions>>();

            // and this performs the actual Configuration of the options per tenant
            this.AddTransient<IConfigureOptions<OpenIdConnectOptions>, TenantConfigureOpenIdConnectOptions>();
            this.AddTransient<IConfigureOptions<CookieAuthenticationOptions>, TenantConfigureCookieAuthenticationOptions>();
            this.AddTransient<IConfigureOptions<IdentityServerAuthenticationOptions>, TenantConfigureIdentityServerAuthenticationOptions>();
            this.AddTransient<IConfigureOptions<JwtBearerOptions>, TenantConfigureJwtBearerOptions>();
            this.AddDataProtection().PersistKeysToFileSystem(Directory.CreateDirectory(serviceOptions.SharedKeysPath)).SetApplicationName("VirtualMgr");   // Note, Tenants use a sub-provider which is namedspaced to the Tenant
            this.AddTransient<IClaimsTransformation, ApplicationClaimsTransformation>();
        }
    }
}
