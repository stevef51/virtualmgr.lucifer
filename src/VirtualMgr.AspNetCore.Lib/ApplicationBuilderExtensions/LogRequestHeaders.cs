﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Context;

namespace Microsoft.AspNetCore.Builder
{
    public static class IApplicationBuilderExtensions
    {
        /// <summary>
        /// Logs all request headers
        /// </summary>
        /// <param name="app"><see cref="IApplicationBuilder"/></param>
        /// <param name="loggerFactory"<see cref="ILoggerFactory"/></param>
        public static void LogRequestHeaders(this IApplicationBuilder app, ILoggerFactory loggerFactory)
        {
            app.Use(async (context, next) =>
            {
                Log.ForContext("RequestHeaders", context.Request.Headers.ToDictionary(h => h.Key, v => v.Value.ToString()), destructureObjects: true)
                    .Debug("Request Headers for {RequestPath}");

                await next.Invoke();
            });
        }
    }
}