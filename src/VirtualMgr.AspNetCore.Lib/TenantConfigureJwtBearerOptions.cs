using System.Net.Http;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

using VirtualMgr.MultiTenant;

namespace VirtualMgr.AspNetCore
{
    public class TenantConfigureJwtBearerOptions : IConfigureNamedOptions<JwtBearerOptions>
    {
        private readonly IHttpContextAccessor _contextAccessor;
        private readonly ServiceOptions _serviceOptions;


        public TenantConfigureJwtBearerOptions(IHttpContextAccessor contextAccessor, IOptions<ServiceOptions> serviceOptions)
        {
            _contextAccessor = contextAccessor;
            _serviceOptions = serviceOptions.Value;
        }

        public void Configure(string name, JwtBearerOptions options)
        {
            var appTenant = _contextAccessor.HttpContext.RequestServices.GetRequiredService<AppTenant>();
            options.Authority = $"{_serviceOptions.IdentityServer.Scheme}{_serviceOptions.IdentityServer.Hostname(appTenant)}/{_serviceOptions.Services.Auth}";
            options.Audience = "VirtualMgr.Api.Lingo";
            options.RequireHttpsMetadata = _serviceOptions.IdentityServer.RequireHttps;

            options.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters()
            {
                NameClaimType = "name",
                RoleClaimType = "role"
            };
        }

        public void Configure(JwtBearerOptions options) => Configure(Options.DefaultName, options);
    }
}