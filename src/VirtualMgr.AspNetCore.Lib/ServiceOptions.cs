﻿using System;
using VirtualMgr.MultiTenant;

namespace VirtualMgr.AspNetCore
{
    public class ServiceNamesOptions
    {
        public string Auth { get; set; } = "auth";
        public string SPA { get; set; } = "spa";
        public string API { get; set; } = "api";
        public string OData { get; set; } = "odata";
        public string Media { get; set; } = "media";
        public string Tenant { get; set; } = "tenant";
        public string Redis { get; set; } = "redis";
        public string Seq { get; set; } = "seq";
    }

    public class IdentityServerOptions
    {
        public string Hostname(AppTenant appTenant)
        {
            return HostnameOverride ?? $"{appTenant.PrimaryHostname}";
        }
        public string HostnameOverride { get; set; }
        public string Scheme { get; set; } = "https://";
        public bool RequireHttps { get; set; } = true;
        public bool ValidateIssuerName { get; set; } = true;
    }

    public class ServiceOptions
    {
        // The Service must Configure this at startup
        public string ServiceName { get; set; }

        public IdentityServerOptions IdentityServer { get; set; } = new IdentityServerOptions();

        public ServiceNamesOptions Services { get; set; } = new ServiceNamesOptions();
        public string AspNetCookieName { get; set; } = ".AspNet.Cookies";
        public string TenantDomainHeader { get; set; } = "VM-TenantDomain";

        public string JsReportServerUrl { get; set; } = "https://jsreport.vmgr.net";

        public string SharedKeysPath { get; set; } = "/shared_keys";
    }
}