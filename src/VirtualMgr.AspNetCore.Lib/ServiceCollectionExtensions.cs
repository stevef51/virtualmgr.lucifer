﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.DependencyInjection;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class VirtualMgrAspNetCoreServiceCollectionExtensions
    {
        public static IServiceCollection AddModule(this IServiceCollection services, IServiceCollection module)
        {
            foreach (var descriptor in module)
            {
                services.Add(descriptor);
            }
            return services;
        }

        public static IServiceCollection AddModule<M1>(this IServiceCollection services)
            where M1 : IServiceCollection, new()
        {
            services.AddModule(new M1());
            return services;
        }

        public static IServiceCollection AddModule<M1, M2>(this IServiceCollection services)
            where M1 : IServiceCollection, new()
            where M2 : IServiceCollection, new()
        {
            services.AddModule<M1>();
            services.AddModule<M2>();
            return services;
        }

        public static IServiceCollection AddModule<M1, M2, M3>(this IServiceCollection services)
            where M1 : IServiceCollection, new()
            where M2 : IServiceCollection, new()
            where M3 : IServiceCollection, new()
        {
            services.AddModule<M1>();
            services.AddModule<M2>();
            services.AddModule<M3>();
            return services;
        }

        public static IServiceCollection AddModule<M1, M2, M3, M4>(this IServiceCollection services)
            where M1 : IServiceCollection, new()
            where M2 : IServiceCollection, new()
            where M3 : IServiceCollection, new()
            where M4 : IServiceCollection, new()
        {
            services.AddModule<M1>();
            services.AddModule<M2>();
            services.AddModule<M3>();
            services.AddModule<M4>();
            return services;
        }

        public static IServiceCollection AddModules(this IServiceCollection services, IEnumerable<IServiceCollection> modules)
        {
            foreach (var module in modules)
            {
                services.AddModule(module);
            }
            return services;
        }
    }
}
