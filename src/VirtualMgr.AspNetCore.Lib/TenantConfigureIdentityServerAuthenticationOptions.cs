using System.Net.Http;
using System.Threading.Tasks;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using VirtualMgr.Membership.DirectCore.IdentityOverrides;
using VirtualMgr.MultiTenant;

namespace VirtualMgr.AspNetCore
{
    public class TenantConfigureIdentityServerAuthenticationOptions : IConfigureNamedOptions<IdentityServerAuthenticationOptions>
    {
        private readonly IHttpContextAccessor _contextAccessor;
        private readonly ServiceOptions _serviceOptions;

        public TenantConfigureIdentityServerAuthenticationOptions(IHttpContextAccessor contextAccessor, IOptions<ServiceOptions> serviceOptions)
        {
            _contextAccessor = contextAccessor;
            _serviceOptions = serviceOptions.Value;
        }

        public void Configure(string name, IdentityServerAuthenticationOptions options)
        {
            var appTenant = _contextAccessor.HttpContext.RequestServices.GetRequiredService<AppTenant>();
            options.Authority = $"{_serviceOptions.IdentityServer.Scheme}{_serviceOptions.IdentityServer.Hostname(appTenant)}/{_serviceOptions.Services.Auth}";

            options.ApiName = "VirtualMgr.Api.Lingo";
            options.ApiSecret = "secret";
            options.RequireHttpsMetadata = _serviceOptions.IdentityServer.RequireHttps;

            options.NameClaimType = "name";
            options.RoleClaimType = "role";
        }

        public void Configure(IdentityServerAuthenticationOptions options) => Configure(Options.DefaultName, options);
    }
}