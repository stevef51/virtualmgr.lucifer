﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;

namespace VirtualMgr.AspNetCore.Cors
{
    public class CorsMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ICorsService _corsService;
        private readonly ICorsPolicyProvider _corsPolicyProvider;
        private readonly CorsPolicy _policy;
        private readonly string _corsPolicyName;

        public CorsMiddleware(
            RequestDelegate next,
            ICorsService corsService,
            ICorsPolicyProvider policyProvider)
            : this(next, corsService, policyProvider, policyName: null) { }

        public CorsMiddleware(
            RequestDelegate next,
            ICorsService corsService,
            ICorsPolicyProvider policyProvider,
            string policyName)
        {
            _next = next ?? throw new ArgumentNullException(nameof(next));
            _corsService = corsService ?? throw new ArgumentNullException(nameof(corsService));
            _corsPolicyProvider = policyProvider ?? throw new ArgumentNullException(nameof(policyProvider));
            _corsPolicyName = policyName;
        }

        public CorsMiddleware(
           RequestDelegate next,
           ICorsService corsService,
           CorsPolicy policy)
        {
            _next = next ?? throw new ArgumentNullException(nameof(next));
            _corsService = corsService ?? throw new ArgumentNullException(nameof(corsService));
            _policy = policy ?? throw new ArgumentNullException(nameof(policy));
        }

        public async Task Invoke(HttpContext context)
        {
            if (context.Request.Headers.ContainsKey(CorsConstants.Origin))
            {
                CorsPolicyBuilder builder = new CorsPolicyBuilder();
                builder.AllowAnyHeader();
                builder.AllowAnyMethod();
                builder.AllowCredentials();

                builder.WithOrigins(context.Request.Headers[CorsConstants.Origin]);

                var corsPolicy = builder.Build();

                var corsResult = _corsService.EvaluatePolicy(context, corsPolicy);

                _corsService.ApplyResult(corsResult, context.Response);

                var accessControlRequestMethod = context.Request.Headers[CorsConstants.AccessControlRequestMethod];
                if (string.Equals(
                        context.Request.Method,
                        CorsConstants.PreflightHttpMethod,
                        StringComparison.OrdinalIgnoreCase) &&
                        !StringValues.IsNullOrEmpty(accessControlRequestMethod))
                {
                    // Since there is a policy which was identified,
                    // always respond to preflight requests.
                    context.Response.StatusCode = StatusCodes.Status204NoContent;
                    return;
                }
            }

            await _next(context);
        }
    }
}
