using System.Net.Http;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

using VirtualMgr.MultiTenant;

namespace VirtualMgr.AspNetCore
{
    public class TenantConfigureCookieAuthenticationOptions : IConfigureNamedOptions<CookieAuthenticationOptions>
    {
        private readonly IHttpContextAccessor _contextAccessor;
        private readonly IDataProtectionProvider _dataProtectionProvider;
        private readonly ServiceOptions _serviceOptions;


        public TenantConfigureCookieAuthenticationOptions(IHttpContextAccessor contextAccessor, IDataProtectionProvider dataProtectionProvider, IOptions<ServiceOptions> serviceOptions)
        {
            _dataProtectionProvider = dataProtectionProvider;
            _contextAccessor = contextAccessor;
            _serviceOptions = serviceOptions.Value;
        }

        public void Configure(string name, CookieAuthenticationOptions options)
        {
            var appTenant = _contextAccessor.HttpContext.RequestServices.GetRequiredService<AppTenant>();
            // Setting this appears to put us into a Consent page loop
            options.Cookie.Path = "/";
            options.DataProtectionProvider = _dataProtectionProvider.CreateProtector("Tenant: " + appTenant.PrimaryHostname);
        }

        public void Configure(CookieAuthenticationOptions options) => Configure(Options.DefaultName, options);
    }
}