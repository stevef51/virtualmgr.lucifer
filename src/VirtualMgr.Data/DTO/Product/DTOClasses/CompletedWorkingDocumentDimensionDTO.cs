﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'CompletedWorkingDocumentDimension'.
    /// </summary>
    [Serializable]
    public partial class CompletedWorkingDocumentDimensionDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the Id property that maps to the Entity CompletedWorkingDocumentDimension</summary>
        public virtual System.Guid Id { get; set; }

        /// <summary>Get or set the Name property that maps to the Entity CompletedWorkingDocumentDimension</summary>
        public virtual System.String Name { get; set; }
        
        #endregion

        #region Related Field Public Properties



		
		public virtual IList< CompletedWorkingDocumentFactDTO> CompletedWorkingDocumentFacts
		{
			get; set;
		}



		
		public virtual IList< CompletedWorkingDocumentPresentedFactDTO> CompletedWorkingDocumentPresentedFacts
		{
			get; set;
		}









        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public CompletedWorkingDocumentDimensionDTO()
        {
			
			
			this.CompletedWorkingDocumentFacts = new List< CompletedWorkingDocumentFactDTO>();
			
			
			
			this.CompletedWorkingDocumentPresentedFacts = new List< CompletedWorkingDocumentPresentedFactDTO>();
			
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 