﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'TimesheetItemType'.
    /// </summary>
    [Serializable]
    public partial class TimesheetItemTypeDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the Id property that maps to the Entity TimesheetItemType</summary>
        public virtual System.Int32 Id { get; set; }

        /// <summary>Get or set the IsLeave property that maps to the Entity TimesheetItemType</summary>
        public virtual System.Boolean IsLeave { get; set; }

        /// <summary>Get or set the IsSaturdayPay property that maps to the Entity TimesheetItemType</summary>
        public virtual System.Boolean IsSaturdayPay { get; set; }

        /// <summary>Get or set the IsSundayPay property that maps to the Entity TimesheetItemType</summary>
        public virtual System.Boolean IsSundayPay { get; set; }

        /// <summary>Get or set the IsWeekDayPay property that maps to the Entity TimesheetItemType</summary>
        public virtual System.Boolean IsWeekDayPay { get; set; }

        /// <summary>Get or set the Name property that maps to the Entity TimesheetItemType</summary>
        public virtual System.String Name { get; set; }
        
        #endregion

        #region Related Field Public Properties



		
		public virtual IList< TimesheetItemDTO> TimesheetItems
		{
			get; set;
		}



		
		public virtual IList< TimesheetItemDTO> TimesheetItems_
		{
			get; set;
		}



		
		public virtual IList< HierarchyBucketDTO> HierarchyBuckets
		{
			get; set;
		}



		
		public virtual IList< HierarchyBucketDTO> HierarchyBuckets_
		{
			get; set;
		}



		
		public virtual IList< HierarchyBucketDTO> HierarchyBuckets__
		{
			get; set;
		}









        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public TimesheetItemTypeDTO()
        {
			
			
			this.TimesheetItems = new List< TimesheetItemDTO>();
			
			
			
			this.TimesheetItems_ = new List< TimesheetItemDTO>();
			
			
			
			this.HierarchyBuckets = new List< HierarchyBucketDTO>();
			
			
			
			this.HierarchyBuckets_ = new List< HierarchyBucketDTO>();
			
			
			
			this.HierarchyBuckets__ = new List< HierarchyBucketDTO>();
			
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 