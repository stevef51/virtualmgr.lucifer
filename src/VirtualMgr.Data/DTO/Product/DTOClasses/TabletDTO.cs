﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'Tablet'.
    /// </summary>
    [Serializable]
    public partial class TabletDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the Archived property that maps to the Entity Tablet</summary>
        public virtual System.Boolean Archived { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity Tablet</summary>
        public virtual System.Int32 Id { get; set; }

        /// <summary>Get or set the Name property that maps to the Entity Tablet</summary>
        public virtual System.String Name { get; set; }

        /// <summary>Get or set the SiteId property that maps to the Entity Tablet</summary>
        public virtual System.Guid? SiteId { get; set; }

        /// <summary>Get or set the TabletProfileId property that maps to the Entity Tablet</summary>
        public virtual System.Int32? TabletProfileId { get; set; }

        /// <summary>Get or set the Uuid property that maps to the Entity Tablet</summary>
        public virtual System.String Uuid { get; set; }
        
        #endregion

        #region Related Field Public Properties



		
		public virtual IList< TabletUuidDTO> TabletUuids
		{
			get; set;
		}





		public virtual TabletProfileDTO TabletProfile {get; set;}



		public virtual TabletUuidDTO TabletUuid {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public TabletDTO()
        {
			
			
			this.TabletUuids = new List< TabletUuidDTO>();
			
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 