﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'AssetType'.
    /// </summary>
    [Serializable]
    public partial class AssetTypeDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the Archived property that maps to the Entity AssetType</summary>
        public virtual System.Boolean Archived { get; set; }

        /// <summary>Get or set the Backcolor property that maps to the Entity AssetType</summary>
        public virtual System.Int32 Backcolor { get; set; }

        /// <summary>Get or set the CanTrack property that maps to the Entity AssetType</summary>
        public virtual System.Boolean CanTrack { get; set; }

        /// <summary>Get or set the Forecolor property that maps to the Entity AssetType</summary>
        public virtual System.Int32 Forecolor { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity AssetType</summary>
        public virtual System.Int32 Id { get; set; }

        /// <summary>Get or set the Name property that maps to the Entity AssetType</summary>
        public virtual System.String Name { get; set; }
        
        #endregion

        #region Related Field Public Properties



		
		public virtual IList< AssetDTO> Assets
		{
			get; set;
		}



		
		public virtual IList< AssetTypeCalendarDTO> AssetTypeCalendars
		{
			get; set;
		}



		
		public virtual IList< AssetTypeContextPublishedResourceDTO> AssetTypeContextPublishedResources
		{
			get; set;
		}









        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public AssetTypeDTO()
        {
			
			
			this.Assets = new List< AssetDTO>();
			
			
			
			this.AssetTypeCalendars = new List< AssetTypeCalendarDTO>();
			
			
			
			this.AssetTypeContextPublishedResources = new List< AssetTypeContextPublishedResourceDTO>();
			
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 