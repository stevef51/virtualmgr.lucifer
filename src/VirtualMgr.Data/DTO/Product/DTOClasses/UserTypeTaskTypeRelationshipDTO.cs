﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'UserTypeTaskTypeRelationship'.
    /// </summary>
    [Serializable]
    public partial class UserTypeTaskTypeRelationshipDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the RelationshipType property that maps to the Entity UserTypeTaskTypeRelationship</summary>
        public virtual System.Int32 RelationshipType { get; set; }

        /// <summary>Get or set the TaskTypeId property that maps to the Entity UserTypeTaskTypeRelationship</summary>
        public virtual System.Guid TaskTypeId { get; set; }

        /// <summary>Get or set the UserTypeId property that maps to the Entity UserTypeTaskTypeRelationship</summary>
        public virtual System.Int32 UserTypeId { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual ProjectJobTaskTypeDTO ProjectJobTaskType {get; set;}



		public virtual UserTypeDTO UserType {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public UserTypeTaskTypeRelationshipDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 