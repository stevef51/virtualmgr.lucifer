﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'ProjectJobTaskTypeFinishedStatus'.
    /// </summary>
    [Serializable]
    public partial class ProjectJobTaskTypeFinishedStatusDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the Id property that maps to the Entity ProjectJobTaskTypeFinishedStatus</summary>
        public virtual System.Guid Id { get; set; }

        /// <summary>Get or set the ProjectJobFinishedStatusId property that maps to the Entity ProjectJobTaskTypeFinishedStatus</summary>
        public virtual System.Guid ProjectJobFinishedStatusId { get; set; }

        /// <summary>Get or set the TaskTypeId property that maps to the Entity ProjectJobTaskTypeFinishedStatus</summary>
        public virtual System.Guid TaskTypeId { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual ProjectJobFinishedStatusDTO ProjectJobFinishedStatu {get; set;}



		public virtual ProjectJobTaskTypeDTO ProjectJobTaskType {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public ProjectJobTaskTypeFinishedStatusDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 