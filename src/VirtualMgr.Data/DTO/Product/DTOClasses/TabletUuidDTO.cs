﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'TabletUuid'.
    /// </summary>
    [Serializable]
    public partial class TabletUuidDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the Description property that maps to the Entity TabletUuid</summary>
        public virtual System.String Description { get; set; }

        /// <summary>Get or set the LastContactUtc property that maps to the Entity TabletUuid</summary>
        public virtual System.DateTime? LastContactUtc { get; set; }

        /// <summary>Get or set the LastGpslocationId property that maps to the Entity TabletUuid</summary>
        public virtual System.Int64? LastGpslocationId { get; set; }

        /// <summary>Get or set the LastRange property that maps to the Entity TabletUuid</summary>
        public virtual System.Int32? LastRange { get; set; }

        /// <summary>Get or set the LastRangeUtc property that maps to the Entity TabletUuid</summary>
        public virtual System.DateTime? LastRangeUtc { get; set; }

        /// <summary>Get or set the LastUserId property that maps to the Entity TabletUuid</summary>
        public virtual System.Guid? LastUserId { get; set; }

        /// <summary>Get or set the TabletId property that maps to the Entity TabletUuid</summary>
        public virtual System.Int32? TabletId { get; set; }

        /// <summary>Get or set the Uuid property that maps to the Entity TabletUuid</summary>
        public virtual System.String Uuid { get; set; }
        
        #endregion

        #region Related Field Public Properties



		
		public virtual IList< BeaconSightingDTO> BeaconSightings
		{
			get; set;
		}



		
		public virtual IList< TabletDTO> Tablets
		{
			get; set;
		}



		
		public virtual IList< ESSensorReadingDTO> ESSensorReadings
		{
			get; set;
		}





		public virtual TabletDTO Tablet {get; set;}



		public virtual UserDataDTO UserData {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public TabletUuidDTO()
        {
			
			
			this.BeaconSightings = new List< BeaconSightingDTO>();
			
			
			
			this.Tablets = new List< TabletDTO>();
			
			
			
			this.ESSensorReadings = new List< ESSensorReadingDTO>();
			
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 