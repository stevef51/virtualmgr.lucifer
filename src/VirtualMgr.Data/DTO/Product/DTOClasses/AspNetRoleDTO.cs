﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'AspNetRole'.
    /// </summary>
    [Serializable]
    public partial class AspNetRoleDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the ConcurrencyStamp property that maps to the Entity AspNetRole</summary>
        public virtual System.String ConcurrencyStamp { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity AspNetRole</summary>
        public virtual System.String Id { get; set; }

        /// <summary>Get or set the Name property that maps to the Entity AspNetRole</summary>
        public virtual System.String Name { get; set; }

        /// <summary>Get or set the NormalizedName property that maps to the Entity AspNetRole</summary>
        public virtual System.String NormalizedName { get; set; }
        
        #endregion

        #region Related Field Public Properties



		
		public virtual IList< AspNetRoleClaimDTO> AspNetRoleClaims
		{
			get; set;
		}



		
		public virtual IList< AspNetUserRoleDTO> AspNetUserRoles
		{
			get; set;
		}



		
		public virtual IList< MediaFolderRoleDTO> MediaFolderRoles
		{
			get; set;
		}



		
		public virtual IList< MediaRoleDTO> MediaRoles
		{
			get; set;
		}



		
		public virtual IList< ReportRoleDTO> ReportRoles
		{
			get; set;
		}



		
		public virtual IList< UserTypeDefaultRoleDTO> UserTypeDefaultRoles
		{
			get; set;
		}









        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public AspNetRoleDTO()
        {
			
			
			this.AspNetRoleClaims = new List< AspNetRoleClaimDTO>();
			
			
			
			this.AspNetUserRoles = new List< AspNetUserRoleDTO>();
			
			
			
			this.MediaFolderRoles = new List< MediaFolderRoleDTO>();
			
			
			
			this.MediaRoles = new List< MediaRoleDTO>();
			
			
			
			this.ReportRoles = new List< ReportRoleDTO>();
			
			
			
			this.UserTypeDefaultRoles = new List< UserTypeDefaultRoleDTO>();
			
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 