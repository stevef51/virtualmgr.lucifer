﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'MediaPageViewing'.
    /// </summary>
    [Serializable]
    public partial class MediaPageViewingDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the EndDate property that maps to the Entity MediaPageViewing</summary>
        public virtual System.DateTime EndDate { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity MediaPageViewing</summary>
        public virtual System.Guid Id { get; set; }

        /// <summary>Get or set the MediaViewingId property that maps to the Entity MediaPageViewing</summary>
        public virtual System.Guid MediaViewingId { get; set; }

        /// <summary>Get or set the Page property that maps to the Entity MediaPageViewing</summary>
        public virtual System.Int32 Page { get; set; }

        /// <summary>Get or set the StartDate property that maps to the Entity MediaPageViewing</summary>
        public virtual System.DateTime StartDate { get; set; }

        /// <summary>Get or set the TotalSeconds property that maps to the Entity MediaPageViewing</summary>
        public virtual System.Int32 TotalSeconds { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual MediaViewingDTO MediaViewing {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public MediaPageViewingDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 