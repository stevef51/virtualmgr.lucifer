﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'FactTask'.
    /// </summary>
    [Serializable]
    public partial class FactTaskDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the ActualDuration property that maps to the Entity FactTask</summary>
        public virtual System.Decimal? ActualDuration { get; set; }

        /// <summary>Get or set the ActualVsEstimatedPercentage property that maps to the Entity FactTask</summary>
        public virtual System.Decimal? ActualVsEstimatedPercentage { get; set; }

        /// <summary>Get or set the ActualVsEstimatedPercentile property that maps to the Entity FactTask</summary>
        public virtual System.Decimal? ActualVsEstimatedPercentile { get; set; }

        /// <summary>Get or set the AllActivitiesEstimatedDuration property that maps to the Entity FactTask</summary>
        public virtual System.Decimal AllActivitiesEstimatedDuration { get; set; }

        /// <summary>Get or set the CompletedActivitiesEstimatedDuration property that maps to the Entity FactTask</summary>
        public virtual System.Decimal CompletedActivitiesEstimatedDuration { get; set; }

        /// <summary>Get or set the CompletionNotes property that maps to the Entity FactTask</summary>
        public virtual System.String CompletionNotes { get; set; }

        /// <summary>Get or set the Description property that maps to the Entity FactTask</summary>
        public virtual System.String Description { get; set; }

        /// <summary>Get or set the DimLocalDateCreated property that maps to the Entity FactTask</summary>
        public virtual System.DateTime? DimLocalDateCreated { get; set; }

        /// <summary>Get or set the DimLocalDateTimeCreated property that maps to the Entity FactTask</summary>
        public virtual System.DateTime? DimLocalDateTimeCreated { get; set; }

        /// <summary>Get or set the DimUtcDateCreated property that maps to the Entity FactTask</summary>
        public virtual System.DateTime? DimUtcDateCreated { get; set; }

        /// <summary>Get or set the DimUtcDateTimeCreated property that maps to the Entity FactTask</summary>
        public virtual System.DateTime? DimUtcDateTimeCreated { get; set; }

        /// <summary>Get or set the EstimatedDurationSeconds property that maps to the Entity FactTask</summary>
        public virtual System.Decimal? EstimatedDurationSeconds { get; set; }

        /// <summary>Get or set the HasOrders property that maps to the Entity FactTask</summary>
        public virtual System.Boolean HasOrders { get; set; }

        /// <summary>Get or set the HierarchyBucketId property that maps to the Entity FactTask</summary>
        public virtual System.Int32? HierarchyBucketId { get; set; }

        /// <summary>Get or set the HierarchyBucketPkId property that maps to the Entity FactTask</summary>
        public virtual System.String HierarchyBucketPkId { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity FactTask</summary>
        public virtual System.Guid Id { get; set; }

        /// <summary>Get or set the InactiveDuration property that maps to the Entity FactTask</summary>
        public virtual System.Decimal? InactiveDuration { get; set; }

        /// <summary>Get or set the IsScheduled property that maps to the Entity FactTask</summary>
        public virtual System.Boolean? IsScheduled { get; set; }

        /// <summary>Get or set the Level property that maps to the Entity FactTask</summary>
        public virtual System.Int32 Level { get; set; }

        /// <summary>Get or set the LocalDateTimeCompleted property that maps to the Entity FactTask</summary>
        public virtual System.DateTime? LocalDateTimeCompleted { get; set; }

        /// <summary>Get or set the LocalDateTimeCreated property that maps to the Entity FactTask</summary>
        public virtual System.DateTime? LocalDateTimeCreated { get; set; }

        /// <summary>Get or set the LocalDateTimeStarted property that maps to the Entity FactTask</summary>
        public virtual System.DateTime? LocalDateTimeStarted { get; set; }

        /// <summary>Get or set the LocalStatusDate property that maps to the Entity FactTask</summary>
        public virtual System.DateTime? LocalStatusDate { get; set; }

        /// <summary>Get or set the MinimumDuration property that maps to the Entity FactTask</summary>
        public virtual System.Decimal? MinimumDuration { get; set; }

        /// <summary>Get or set the Name property that maps to the Entity FactTask</summary>
        public virtual System.String Name { get; set; }

        /// <summary>Get or set the OriginalOwnerId property that maps to the Entity FactTask</summary>
        public virtual System.Guid OriginalOwnerId { get; set; }

        /// <summary>Get or set the OriginalOwnerPkId property that maps to the Entity FactTask</summary>
        public virtual System.String OriginalOwnerPkId { get; set; }

        /// <summary>Get or set the OriginalTaskTypeId property that maps to the Entity FactTask</summary>
        public virtual System.Guid? OriginalTaskTypeId { get; set; }

        /// <summary>Get or set the OriginalTaskTypePkId property that maps to the Entity FactTask</summary>
        public virtual System.String OriginalTaskTypePkId { get; set; }

        /// <summary>Get or set the OwnerId property that maps to the Entity FactTask</summary>
        public virtual System.Guid OwnerId { get; set; }

        /// <summary>Get or set the OwnerPkId property that maps to the Entity FactTask</summary>
        public virtual System.String OwnerPkId { get; set; }

        /// <summary>Get or set the PhotoDuringSignature property that maps to the Entity FactTask</summary>
        public virtual System.Boolean PhotoDuringSignature { get; set; }

        /// <summary>Get or set the PkId property that maps to the Entity FactTask</summary>
        public virtual System.String PkId { get; set; }

        /// <summary>Get or set the ProjectJobTaskGroupId property that maps to the Entity FactTask</summary>
        public virtual System.Guid? ProjectJobTaskGroupId { get; set; }

        /// <summary>Get or set the ProjectJobTaskGroupPkId property that maps to the Entity FactTask</summary>
        public virtual System.String ProjectJobTaskGroupPkId { get; set; }

        /// <summary>Get or set the ReferenceNumber property that maps to the Entity FactTask</summary>
        public virtual System.String ReferenceNumber { get; set; }

        /// <summary>Get or set the RequireSignature property that maps to the Entity FactTask</summary>
        public virtual System.Boolean RequireSignature { get; set; }

        /// <summary>Get or set the RoleId property that maps to the Entity FactTask</summary>
        public virtual System.Int32? RoleId { get; set; }

        /// <summary>Get or set the RolePkId property that maps to the Entity FactTask</summary>
        public virtual System.String RolePkId { get; set; }

        /// <summary>Get or set the RosterId property that maps to the Entity FactTask</summary>
        public virtual System.Int32 RosterId { get; set; }

        /// <summary>Get or set the RosterPkId property that maps to the Entity FactTask</summary>
        public virtual System.String RosterPkId { get; set; }

        /// <summary>Get or set the ScheduledOwnerId property that maps to the Entity FactTask</summary>
        public virtual System.Guid? ScheduledOwnerId { get; set; }

        /// <summary>Get or set the ScheduledOwnerPkId property that maps to the Entity FactTask</summary>
        public virtual System.String ScheduledOwnerPkId { get; set; }

        /// <summary>Get or set the SiteId property that maps to the Entity FactTask</summary>
        public virtual System.Guid? SiteId { get; set; }

        /// <summary>Get or set the SitePkId property that maps to the Entity FactTask</summary>
        public virtual System.String SitePkId { get; set; }

        /// <summary>Get or set the SortOrder property that maps to the Entity FactTask</summary>
        public virtual System.Int32 SortOrder { get; set; }

        /// <summary>Get or set the Status property that maps to the Entity FactTask</summary>
        public virtual System.Int32 Status { get; set; }

        /// <summary>Get or set the StatusId property that maps to the Entity FactTask</summary>
        public virtual System.Guid? StatusId { get; set; }

        /// <summary>Get or set the StatusPkId property that maps to the Entity FactTask</summary>
        public virtual System.String StatusPkId { get; set; }

        /// <summary>Get or set the TaskTypeId property that maps to the Entity FactTask</summary>
        public virtual System.Guid TaskTypeId { get; set; }

        /// <summary>Get or set the TaskTypePkId property that maps to the Entity FactTask</summary>
        public virtual System.String TaskTypePkId { get; set; }

        /// <summary>Get or set the TenantId property that maps to the Entity FactTask</summary>
        public virtual System.Int32 TenantId { get; set; }

        /// <summary>Get or set the Tracking property that maps to the Entity FactTask</summary>
        public virtual System.Byte[] Tracking { get; set; }

        /// <summary>Get or set the TrackingLong property that maps to the Entity FactTask</summary>
        public virtual System.Int64? TrackingLong { get; set; }

        /// <summary>Get or set the UtcDateTimeCompleted property that maps to the Entity FactTask</summary>
        public virtual System.DateTime? UtcDateTimeCompleted { get; set; }

        /// <summary>Get or set the UtcDateTimeCreated property that maps to the Entity FactTask</summary>
        public virtual System.DateTime? UtcDateTimeCreated { get; set; }

        /// <summary>Get or set the UtcDateTimeStarted property that maps to the Entity FactTask</summary>
        public virtual System.DateTime? UtcDateTimeStarted { get; set; }

        /// <summary>Get or set the UtcStatusDate property that maps to the Entity FactTask</summary>
        public virtual System.DateTime UtcStatusDate { get; set; }

        /// <summary>Get or set the WorkingDocumentCount property that maps to the Entity FactTask</summary>
        public virtual System.Int32 WorkingDocumentCount { get; set; }

        /// <summary>Get or set the WorkingGroupId property that maps to the Entity FactTask</summary>
        public virtual System.Guid? WorkingGroupId { get; set; }

        /// <summary>Get or set the WorkingGroupPkId property that maps to the Entity FactTask</summary>
        public virtual System.String WorkingGroupPkId { get; set; }
        
        #endregion

        #region Related Field Public Properties









        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public FactTaskDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 