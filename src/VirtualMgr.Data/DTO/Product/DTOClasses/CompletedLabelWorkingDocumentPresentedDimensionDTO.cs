﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'CompletedLabelWorkingDocumentPresentedDimension'.
    /// </summary>
    [Serializable]
    public partial class CompletedLabelWorkingDocumentPresentedDimensionDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the CompletedLabelDimensionId property that maps to the Entity CompletedLabelWorkingDocumentPresentedDimension</summary>
        public virtual System.Guid CompletedLabelDimensionId { get; set; }

        /// <summary>Get or set the CompletedWorkingDocumentPresentedFactId property that maps to the Entity CompletedLabelWorkingDocumentPresentedDimension</summary>
        public virtual System.Guid CompletedWorkingDocumentPresentedFactId { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual CompletedLabelDimensionDTO CompletedLabelDimension {get; set;}



		public virtual CompletedWorkingDocumentPresentedFactDTO CompletedWorkingDocumentPresentedFact {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public CompletedLabelWorkingDocumentPresentedDimensionDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 