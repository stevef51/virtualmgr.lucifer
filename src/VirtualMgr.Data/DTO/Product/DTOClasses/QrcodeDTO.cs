﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'Qrcode'.
    /// </summary>
    [Serializable]
    public partial class QrcodeDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the AssetId property that maps to the Entity Qrcode</summary>
        public virtual System.Int32? AssetId { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity Qrcode</summary>
        public virtual System.Int32 Id { get; set; }

        /// <summary>Get or set the Qrcode property that maps to the Entity Qrcode</summary>
        public virtual System.String Qrcode { get; set; }

        /// <summary>Get or set the Selected property that maps to the Entity Qrcode</summary>
        public virtual System.Boolean Selected { get; set; }

        /// <summary>Get or set the TaskTypeId property that maps to the Entity Qrcode</summary>
        public virtual System.Guid? TaskTypeId { get; set; }

        /// <summary>Get or set the UserId property that maps to the Entity Qrcode</summary>
        public virtual System.Guid? UserId { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual AssetDTO Asset {get; set;}



		public virtual ProjectJobTaskTypeDTO ProjectJobTaskType {get; set;}



		public virtual UserDataDTO UserData {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public QrcodeDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 