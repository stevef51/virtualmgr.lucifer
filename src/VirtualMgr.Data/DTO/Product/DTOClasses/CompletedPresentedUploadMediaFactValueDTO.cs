﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'CompletedPresentedUploadMediaFactValue'.
    /// </summary>
    [Serializable]
    public partial class CompletedPresentedUploadMediaFactValueDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the CompletedWorkingDocumentPresentedFactId property that maps to the Entity CompletedPresentedUploadMediaFactValue</summary>
        public virtual System.Guid CompletedWorkingDocumentPresentedFactId { get; set; }

        /// <summary>Get or set the MediaId property that maps to the Entity CompletedPresentedUploadMediaFactValue</summary>
        public virtual System.Guid MediaId { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual CompletedWorkingDocumentPresentedFactDTO CompletedWorkingDocumentPresentedFact {get; set;}



		public virtual MediaDTO Medium {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public CompletedPresentedUploadMediaFactValueDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 