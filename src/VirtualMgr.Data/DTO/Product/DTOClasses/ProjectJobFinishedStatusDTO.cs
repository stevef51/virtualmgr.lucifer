﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'ProjectJobFinishedStatus'.
    /// </summary>
    [Serializable]
    public partial class ProjectJobFinishedStatusDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the Id property that maps to the Entity ProjectJobFinishedStatus</summary>
        public virtual System.Guid Id { get; set; }

        /// <summary>Get or set the RequiresExtraNotes property that maps to the Entity ProjectJobFinishedStatus</summary>
        public virtual System.Boolean RequiresExtraNotes { get; set; }

        /// <summary>Get or set the Stage property that maps to the Entity ProjectJobFinishedStatus</summary>
        public virtual System.Int32 Stage { get; set; }

        /// <summary>Get or set the Text property that maps to the Entity ProjectJobFinishedStatus</summary>
        public virtual System.String Text { get; set; }
        
        #endregion

        #region Related Field Public Properties



		
		public virtual IList< ProjectJobTaskFinishedStatusDTO> ProjectJobTaskFinishedStatuses
		{
			get; set;
		}



		
		public virtual IList< ProjectJobTaskTypeFinishedStatusDTO> ProjectJobTaskTypeFinishedStatuses
		{
			get; set;
		}









        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public ProjectJobFinishedStatusDTO()
        {
			
			
			this.ProjectJobTaskFinishedStatuses = new List< ProjectJobTaskFinishedStatusDTO>();
			
			
			
			this.ProjectJobTaskTypeFinishedStatuses = new List< ProjectJobTaskTypeFinishedStatusDTO>();
			
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 