﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'CompletedPresentedFactValue'.
    /// </summary>
    [Serializable]
    public partial class CompletedPresentedFactValueDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the CompletedWorkingDocumentPresentedFactId property that maps to the Entity CompletedPresentedFactValue</summary>
        public virtual System.Guid CompletedWorkingDocumentPresentedFactId { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity CompletedPresentedFactValue</summary>
        public virtual System.Guid Id { get; set; }

        /// <summary>Get or set the SearchValue property that maps to the Entity CompletedPresentedFactValue</summary>
        public virtual System.String SearchValue { get; set; }

        /// <summary>Get or set the Value property that maps to the Entity CompletedPresentedFactValue</summary>
        public virtual System.String Value { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual CompletedWorkingDocumentPresentedFactDTO CompletedWorkingDocumentPresentedFact {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public CompletedPresentedFactValueDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 