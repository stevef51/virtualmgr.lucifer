﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'ProjectJobTaskFinishedStatus'.
    /// </summary>
    [Serializable]
    public partial class ProjectJobTaskFinishedStatusDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the Notes property that maps to the Entity ProjectJobTaskFinishedStatus</summary>
        public virtual System.String Notes { get; set; }

        /// <summary>Get or set the ProjectJobFinishedStatusId property that maps to the Entity ProjectJobTaskFinishedStatus</summary>
        public virtual System.Guid ProjectJobFinishedStatusId { get; set; }

        /// <summary>Get or set the TaskId property that maps to the Entity ProjectJobTaskFinishedStatus</summary>
        public virtual System.Guid TaskId { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual ProjectJobFinishedStatusDTO ProjectJobFinishedStatu {get; set;}



		public virtual ProjectJobTaskDTO ProjectJobTask {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public ProjectJobTaskFinishedStatusDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 