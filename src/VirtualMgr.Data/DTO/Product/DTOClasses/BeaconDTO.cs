﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'Beacon'.
    /// </summary>
    [Serializable]
    public partial class BeaconDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the AccuracyMetres property that maps to the Entity Beacon</summary>
        public virtual System.Decimal? AccuracyMetres { get; set; }

        /// <summary>Get or set the AssetId property that maps to the Entity Beacon</summary>
        public virtual System.Int32? AssetId { get; set; }

        /// <summary>Get or set the BatteryPercent property that maps to the Entity Beacon</summary>
        public virtual System.Int32? BatteryPercent { get; set; }

        /// <summary>Get or set the BeaconConfigurationId property that maps to the Entity Beacon</summary>
        public virtual System.Int32? BeaconConfigurationId { get; set; }

        /// <summary>Get or set the FacilityStructureId property that maps to the Entity Beacon</summary>
        public virtual System.Int32? FacilityStructureId { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity Beacon</summary>
        public virtual System.String Id { get; set; }

        /// <summary>Get or set the LastButtonPressedUtc property that maps to the Entity Beacon</summary>
        public virtual System.DateTime? LastButtonPressedUtc { get; set; }

        /// <summary>Get or set the LastContactUtc property that maps to the Entity Beacon</summary>
        public virtual System.DateTime? LastContactUtc { get; set; }

        /// <summary>Get or set the LastGpslocationId property that maps to the Entity Beacon</summary>
        public virtual System.Int64? LastGpslocationId { get; set; }

        /// <summary>Get or set the Latitude property that maps to the Entity Beacon</summary>
        public virtual System.Decimal? Latitude { get; set; }

        /// <summary>Get or set the Longitude property that maps to the Entity Beacon</summary>
        public virtual System.Decimal? Longitude { get; set; }

        /// <summary>Get or set the Name property that maps to the Entity Beacon</summary>
        public virtual System.String Name { get; set; }

        /// <summary>Get or set the StaticHeightFromFloor property that maps to the Entity Beacon</summary>
        public virtual System.Decimal? StaticHeightFromFloor { get; set; }

        /// <summary>Get or set the StaticLocationId property that maps to the Entity Beacon</summary>
        public virtual System.Int64? StaticLocationId { get; set; }

        /// <summary>Get or set the TaskTypeId property that maps to the Entity Beacon</summary>
        public virtual System.Guid? TaskTypeId { get; set; }

        /// <summary>Get or set the UserId property that maps to the Entity Beacon</summary>
        public virtual System.Guid? UserId { get; set; }
        
        #endregion

        #region Related Field Public Properties



		
		public virtual IList< BeaconSightingDTO> BeaconSightings
		{
			get; set;
		}





		public virtual FacilityStructureDTO FacilityStructure {get; set;}



		public virtual GpsLocationDTO LastGpsLocation {get; set;}



		public virtual GpsLocationDTO StaticLocation {get; set;}



		public virtual ProjectJobTaskTypeDTO ProjectJobTaskType {get; set;}



		public virtual UserDataDTO UserData {get; set;}







		public virtual AssetDTO Asset {get; set;}



        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public BeaconDTO()
        {
			
			
			this.BeaconSightings = new List< BeaconSightingDTO>();
			
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 