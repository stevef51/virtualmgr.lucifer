﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'AuditTaskConditionAction'.
    /// </summary>
    [Serializable]
    public partial class AuditTaskConditionActionDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the Group property that maps to the Entity AuditTaskConditionAction</summary>
        public virtual System.String Group { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity AuditTaskConditionAction</summary>
        public virtual System.Int32 Id { get; set; }

        /// <summary>Get or set the ProjectJobScheduledTaskId property that maps to the Entity AuditTaskConditionAction</summary>
        public virtual System.Guid ProjectJobScheduledTaskId { get; set; }

        /// <summary>Get or set the SampleCount property that maps to the Entity AuditTaskConditionAction</summary>
        public virtual System.Int32 SampleCount { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual ProjectJobScheduledTaskDTO ProjectJobScheduledTask {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public AuditTaskConditionActionDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 