﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'ReportData'.
    /// </summary>
    [Serializable]
    public partial class ReportDataDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the Data property that maps to the Entity ReportData</summary>
        public virtual System.Byte[] Data { get; set; }

        /// <summary>Get or set the ReportId property that maps to the Entity ReportData</summary>
        public virtual System.Int32 ReportId { get; set; }
        
        #endregion

        #region Related Field Public Properties









		public virtual ReportDTO Report {get; set;}



        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public ReportDataDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 