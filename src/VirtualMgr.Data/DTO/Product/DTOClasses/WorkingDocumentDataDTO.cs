﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'WorkingDocumentData'.
    /// </summary>
    [Serializable]
    public partial class WorkingDocumentDataDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the Data property that maps to the Entity WorkingDocumentData</summary>
        public virtual System.String Data { get; set; }

        /// <summary>Get or set the UtcLastModified property that maps to the Entity WorkingDocumentData</summary>
        public virtual System.DateTime UtcLastModified { get; set; }

        /// <summary>Get or set the WorkingDocumentId property that maps to the Entity WorkingDocumentData</summary>
        public virtual System.Guid WorkingDocumentId { get; set; }
        
        #endregion

        #region Related Field Public Properties









		public virtual WorkingDocumentDTO WorkingDocument {get; set;}



        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public WorkingDocumentDataDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 