﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'PublishingGroupActor'.
    /// </summary>
    [Serializable]
    public partial class PublishingGroupActorDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the PublishingGroupId property that maps to the Entity PublishingGroupActor</summary>
        public virtual System.Int32 PublishingGroupId { get; set; }

        /// <summary>Get or set the PublishingTypeId property that maps to the Entity PublishingGroupActor</summary>
        public virtual System.Int32 PublishingTypeId { get; set; }

        /// <summary>Get or set the UserId property that maps to the Entity PublishingGroupActor</summary>
        public virtual System.Guid UserId { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual PublishingGroupDTO PublishingGroup {get; set;}



		public virtual PublishingGroupActorTypeDTO PublishingGroupActorType {get; set;}



		public virtual UserDataDTO UserData {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public PublishingGroupActorDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 