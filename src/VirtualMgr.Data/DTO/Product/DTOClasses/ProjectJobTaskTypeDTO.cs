﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'ProjectJobTaskType'.
    /// </summary>
    [Serializable]
    public partial class ProjectJobTaskTypeDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the AutoFinishAfterLastChecklist property that maps to the Entity ProjectJobTaskType</summary>
        public virtual System.Boolean AutoFinishAfterLastChecklist { get; set; }

        /// <summary>Get or set the AutoFinishAfterStart property that maps to the Entity ProjectJobTaskType</summary>
        public virtual System.Boolean AutoFinishAfterStart { get; set; }

        /// <summary>Get or set the AutoStart1stChecklist property that maps to the Entity ProjectJobTaskType</summary>
        public virtual System.Boolean AutoStart1stChecklist { get; set; }

        /// <summary>Get or set the DefaultLengthInDays property that maps to the Entity ProjectJobTaskType</summary>
        public virtual System.String DefaultLengthInDays { get; set; }

        /// <summary>Get or set the EstimatedDurationSeconds property that maps to the Entity ProjectJobTaskType</summary>
        public virtual System.Decimal? EstimatedDurationSeconds { get; set; }

        /// <summary>Get or set the ExcludeFromTimesheets property that maps to the Entity ProjectJobTaskType</summary>
        public virtual System.Boolean ExcludeFromTimesheets { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity ProjectJobTaskType</summary>
        public virtual System.Guid Id { get; set; }

        /// <summary>Get or set the MediaId property that maps to the Entity ProjectJobTaskType</summary>
        public virtual System.Guid? MediaId { get; set; }

        /// <summary>Get or set the MinimumDuration property that maps to the Entity ProjectJobTaskType</summary>
        public virtual System.String MinimumDuration { get; set; }

        /// <summary>Get or set the Name property that maps to the Entity ProjectJobTaskType</summary>
        public virtual System.String Name { get; set; }

        /// <summary>Get or set the PhotoSupport property that maps to the Entity ProjectJobTaskType</summary>
        public virtual System.Int32 PhotoSupport { get; set; }

        /// <summary>Get or set the ProductCatalogs property that maps to the Entity ProjectJobTaskType</summary>
        public virtual System.String ProductCatalogs { get; set; }

        /// <summary>Get or set the UserInterfaceType property that maps to the Entity ProjectJobTaskType</summary>
        public virtual System.Int32 UserInterfaceType { get; set; }
        
        #endregion

        #region Related Field Public Properties



		
		public virtual IList< AssetTypeCalendarTaskDTO> AssetTypeCalendarTasks
		{
			get; set;
		}



		
		public virtual IList< BeaconDTO> Beacons
		{
			get; set;
		}



		
		public virtual IList< ProjectJobScheduledTaskDTO> ProjectJobScheduledTasks
		{
			get; set;
		}



		
		public virtual IList< ProjectJobTaskDTO> ProjectJobTasks
		{
			get; set;
		}



		
		public virtual IList< ProjectJobTaskDTO> ProjectJobTasks_
		{
			get; set;
		}



		
		public virtual IList< ProjectJobTaskTypeFacilityOverrideDTO> ProjectJobTaskTypeFacilityOverrides
		{
			get; set;
		}



		
		public virtual IList< ProjectJobTaskTypeFinishedStatusDTO> ProjectJobTaskTypeFinishedStatuses
		{
			get; set;
		}



		
		public virtual IList< ProjectJobTaskTypeMediaFolderDTO> ProjectJobTaskTypeMediaFolders
		{
			get; set;
		}



		
		public virtual IList< ProjectJobTaskTypePublishingGroupResourceDTO> ProjectJobTaskTypePublishingGroupResources
		{
			get; set;
		}



		
		public virtual IList< ProjectJobTaskTypeRelationshipDTO> ProjectJobTaskTypeDestinationRelationships
		{
			get; set;
		}



		
		public virtual IList< ProjectJobTaskTypeRelationshipDTO> ProjectJobTaskTypeSourceRelationships
		{
			get; set;
		}



		
		public virtual IList< ProjectJobTaskTypeStatusDTO> ProjectJobTaskTypeStatuses
		{
			get; set;
		}



		
		public virtual IList< UserTypeTaskTypeRelationshipDTO> UserTypeTaskTypeRelationships
		{
			get; set;
		}



		
		public virtual IList< QrcodeDTO> Qrcodes
		{
			get; set;
		}





		public virtual MediaDTO Medium {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public ProjectJobTaskTypeDTO()
        {
			
			
			this.AssetTypeCalendarTasks = new List< AssetTypeCalendarTaskDTO>();
			
			
			
			this.Beacons = new List< BeaconDTO>();
			
			
			
			this.ProjectJobScheduledTasks = new List< ProjectJobScheduledTaskDTO>();
			
			
			
			this.ProjectJobTasks = new List< ProjectJobTaskDTO>();
			
			
			
			this.ProjectJobTasks_ = new List< ProjectJobTaskDTO>();
			
			
			
			this.ProjectJobTaskTypeFacilityOverrides = new List< ProjectJobTaskTypeFacilityOverrideDTO>();
			
			
			
			this.ProjectJobTaskTypeFinishedStatuses = new List< ProjectJobTaskTypeFinishedStatusDTO>();
			
			
			
			this.ProjectJobTaskTypeMediaFolders = new List< ProjectJobTaskTypeMediaFolderDTO>();
			
			
			
			this.ProjectJobTaskTypePublishingGroupResources = new List< ProjectJobTaskTypePublishingGroupResourceDTO>();
			
			
			
			this.ProjectJobTaskTypeDestinationRelationships = new List< ProjectJobTaskTypeRelationshipDTO>();
			
			
			
			this.ProjectJobTaskTypeSourceRelationships = new List< ProjectJobTaskTypeRelationshipDTO>();
			
			
			
			this.ProjectJobTaskTypeStatuses = new List< ProjectJobTaskTypeStatusDTO>();
			
			
			
			this.UserTypeTaskTypeRelationships = new List< UserTypeTaskTypeRelationshipDTO>();
			
			
			
			this.Qrcodes = new List< QrcodeDTO>();
			
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 