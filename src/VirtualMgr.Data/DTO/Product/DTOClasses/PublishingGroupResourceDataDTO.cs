﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'PublishingGroupResourceData'.
    /// </summary>
    [Serializable]
    public partial class PublishingGroupResourceDataDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the Data property that maps to the Entity PublishingGroupResourceData</summary>
        public virtual System.String Data { get; set; }

        /// <summary>Get or set the PublishingGroupResourceId property that maps to the Entity PublishingGroupResourceData</summary>
        public virtual System.Int32 PublishingGroupResourceId { get; set; }
        
        #endregion

        #region Related Field Public Properties









		public virtual PublishingGroupResourceDTO PublishingGroupResource {get; set;}



        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public PublishingGroupResourceDataDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 