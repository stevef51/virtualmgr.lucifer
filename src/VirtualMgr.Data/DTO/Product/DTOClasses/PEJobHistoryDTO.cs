﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'PEJobHistory'.
    /// </summary>
    [Serializable]
    public partial class PEJobHistoryDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the Id property that maps to the Entity PEJobHistory</summary>
        public virtual System.Int32 Id { get; set; }

        /// <summary>Get or set the JobId property that maps to the Entity PEJobHistory</summary>
        public virtual System.Int32 JobId { get; set; }

        /// <summary>Get or set the Message property that maps to the Entity PEJobHistory</summary>
        public virtual System.String Message { get; set; }

        /// <summary>Get or set the RanAtUtc property that maps to the Entity PEJobHistory</summary>
        public virtual System.DateTime RanAtUtc { get; set; }

        /// <summary>Get or set the Success property that maps to the Entity PEJobHistory</summary>
        public virtual System.Boolean Success { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual PEJobDTO PEJob {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public PEJobHistoryDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 