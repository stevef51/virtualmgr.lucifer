﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'WorkingDocument'.
    /// </summary>
    [Serializable]
    public partial class WorkingDocumentDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the CleanUpIfFinishedAfter property that maps to the Entity WorkingDocument</summary>
        public virtual System.DateTime? CleanUpIfFinishedAfter { get; set; }

        /// <summary>Get or set the CleanUpIfNotFinishedAfter property that maps to the Entity WorkingDocument</summary>
        public virtual System.DateTime? CleanUpIfNotFinishedAfter { get; set; }

        /// <summary>Get or set the ClientIp property that maps to the Entity WorkingDocument</summary>
        public virtual System.String ClientIp { get; set; }

        /// <summary>Get or set the DateCreated property that maps to the Entity WorkingDocument</summary>
        public virtual System.DateTime DateCreated { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity WorkingDocument</summary>
        public virtual System.Guid Id { get; set; }

        /// <summary>Get or set the Lat property that maps to the Entity WorkingDocument</summary>
        public virtual System.Decimal? Lat { get; set; }

        /// <summary>Get or set the Long property that maps to the Entity WorkingDocument</summary>
        public virtual System.Decimal? Long { get; set; }

        /// <summary>Get or set the Name property that maps to the Entity WorkingDocument</summary>
        public virtual System.String Name { get; set; }

        /// <summary>Get or set the ParentId property that maps to the Entity WorkingDocument</summary>
        public virtual System.Guid? ParentId { get; set; }

        /// <summary>Get or set the PublishingGroupResourceId property that maps to the Entity WorkingDocument</summary>
        public virtual System.Int32 PublishingGroupResourceId { get; set; }

        /// <summary>Get or set the RevieweeId property that maps to the Entity WorkingDocument</summary>
        public virtual System.Guid? RevieweeId { get; set; }

        /// <summary>Get or set the ReviewerId property that maps to the Entity WorkingDocument</summary>
        public virtual System.Guid ReviewerId { get; set; }

        /// <summary>Get or set the Status property that maps to the Entity WorkingDocument</summary>
        public virtual System.Int32 Status { get; set; }

        /// <summary>Get or set the UserAgentId property that maps to the Entity WorkingDocument</summary>
        public virtual System.Int32? UserAgentId { get; set; }

        /// <summary>Get or set the UtcLastModified property that maps to the Entity WorkingDocument</summary>
        public virtual System.DateTime UtcLastModified { get; set; }
        
        #endregion

        #region Related Field Public Properties



		
		public virtual IList< ProjectJobTaskWorkingDocumentDTO> ProjectJobTaskWorkingDocuments
		{
			get; set;
		}



		
		public virtual IList< PublicWorkingDocumentDTO> PublicWorkingDocuments
		{
			get; set;
		}



		
		public virtual IList< WorkingDocumentPublicVariableDTO> WorkingDocumentPublicVariables
		{
			get; set;
		}



		
		public virtual IList< WorkingDocumentReferenceDTO> WorkingDocumentReferences
		{
			get; set;
		}





		public virtual PublishingGroupResourceDTO PublishingGroupResource {get; set;}



		public virtual UserAgentDTO UserAgent {get; set;}



		public virtual UserDataDTO Reviewee {get; set;}



		public virtual UserDataDTO Reviewer {get; set;}



		public virtual WorkingDocumentStatusDTO WorkingDocumentStatu {get; set;}







		public virtual WorkingDocumentDataDTO WorkingDocumentData {get; set;}



        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public WorkingDocumentDTO()
        {
			
			
			this.ProjectJobTaskWorkingDocuments = new List< ProjectJobTaskWorkingDocumentDTO>();
			
			
			
			this.PublicWorkingDocuments = new List< PublicWorkingDocumentDTO>();
			
			
			
			this.WorkingDocumentPublicVariables = new List< WorkingDocumentPublicVariableDTO>();
			
			
			
			this.WorkingDocumentReferences = new List< WorkingDocumentReferenceDTO>();
			
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 