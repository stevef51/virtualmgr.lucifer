﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'ProjectJobScheduledTask'.
    /// </summary>
    [Serializable]
    public partial class ProjectJobScheduledTaskDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the Backcolor property that maps to the Entity ProjectJobScheduledTask</summary>
        public virtual System.Int32? Backcolor { get; set; }

        /// <summary>Get or set the CalendarRuleId property that maps to the Entity ProjectJobScheduledTask</summary>
        public virtual System.Int32 CalendarRuleId { get; set; }

        /// <summary>Get or set the Description property that maps to the Entity ProjectJobScheduledTask</summary>
        public virtual System.String Description { get; set; }

        /// <summary>Get or set the EstimatedDurationSeconds property that maps to the Entity ProjectJobScheduledTask</summary>
        public virtual System.Decimal? EstimatedDurationSeconds { get; set; }

        /// <summary>Get or set the Forecolor property that maps to the Entity ProjectJobScheduledTask</summary>
        public virtual System.Int32? Forecolor { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity ProjectJobScheduledTask</summary>
        public virtual System.Guid Id { get; set; }

        /// <summary>Get or set the IsArchived property that maps to the Entity ProjectJobScheduledTask</summary>
        public virtual System.Boolean IsArchived { get; set; }

        /// <summary>Get or set the LengthInDays property that maps to the Entity ProjectJobScheduledTask</summary>
        public virtual System.String LengthInDays { get; set; }

        /// <summary>Get or set the Level property that maps to the Entity ProjectJobScheduledTask</summary>
        public virtual System.Int32 Level { get; set; }

        /// <summary>Get or set the MinimumDuration property that maps to the Entity ProjectJobScheduledTask</summary>
        public virtual System.String MinimumDuration { get; set; }

        /// <summary>Get or set the Name property that maps to the Entity ProjectJobScheduledTask</summary>
        public virtual System.String Name { get; set; }

        /// <summary>Get or set the PhotoDuringSignature property that maps to the Entity ProjectJobScheduledTask</summary>
        public virtual System.Boolean PhotoDuringSignature { get; set; }

        /// <summary>Get or set the RequireSignature property that maps to the Entity ProjectJobScheduledTask</summary>
        public virtual System.Boolean RequireSignature { get; set; }

        /// <summary>Get or set the RoleId property that maps to the Entity ProjectJobScheduledTask</summary>
        public virtual System.Int32? RoleId { get; set; }

        /// <summary>Get or set the RosterId property that maps to the Entity ProjectJobScheduledTask</summary>
        public virtual System.Int32 RosterId { get; set; }

        /// <summary>Get or set the SiteId property that maps to the Entity ProjectJobScheduledTask</summary>
        public virtual System.Guid? SiteId { get; set; }

        /// <summary>Get or set the SortOrder property that maps to the Entity ProjectJobScheduledTask</summary>
        public virtual System.Int32 SortOrder { get; set; }

        /// <summary>Get or set the StartTime property that maps to the Entity ProjectJobScheduledTask</summary>
        public virtual System.TimeSpan? StartTime { get; set; }

        /// <summary>Get or set the StartTimeWindowSeconds property that maps to the Entity ProjectJobScheduledTask</summary>
        public virtual System.Int32? StartTimeWindowSeconds { get; set; }

        /// <summary>Get or set the TaskTypeId property that maps to the Entity ProjectJobScheduledTask</summary>
        public virtual System.Guid TaskTypeId { get; set; }
        
        #endregion

        #region Related Field Public Properties



		
		public virtual IList< AuditTaskConditionActionDTO> AuditTaskConditionActions
		{
			get; set;
		}



		
		public virtual IList< ProjectJobScheduledJobTaskUserLabelDTO> ProjectJobScheduledJobTaskUserLabels
		{
			get; set;
		}



		
		public virtual IList< ProjectJobScheduledTaskAttachmentDTO> ProjectJobScheduledTaskAttachments
		{
			get; set;
		}



		
		public virtual IList< ProjectJobScheduledTaskLabelDTO> ProjectJobScheduledTaskLabels
		{
			get; set;
		}



		
		public virtual IList< ProjectJobScheduledTaskPublishingGroupResourceDTO> ProjectJobScheduledTaskPublishingGroupResources
		{
			get; set;
		}



		
		public virtual IList< ProjectJobScheduledTaskTranslatedDTO> Translated
		{
			get; set;
		}



		
		public virtual IList< ProjectJobTaskDTO> ProjectJobTasks
		{
			get; set;
		}



		
		public virtual IList< ProjectJobScheduledTaskWorkloadingActivityDTO> ProjectJobScheduledTaskWorkloadingActivities
		{
			get; set;
		}





		public virtual CalendarRuleDTO CalendarRule {get; set;}



		public virtual ProjectJobTaskTypeDTO ProjectJobTaskType {get; set;}



		public virtual RosterDTO Roster {get; set;}



		public virtual HierarchyRoleDTO HierarchyRole {get; set;}



		public virtual UserDataDTO UserData_ {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public ProjectJobScheduledTaskDTO()
        {
			
			
			this.AuditTaskConditionActions = new List< AuditTaskConditionActionDTO>();
			
			
			
			this.ProjectJobScheduledJobTaskUserLabels = new List< ProjectJobScheduledJobTaskUserLabelDTO>();
			
			
			
			this.ProjectJobScheduledTaskAttachments = new List< ProjectJobScheduledTaskAttachmentDTO>();
			
			
			
			this.ProjectJobScheduledTaskLabels = new List< ProjectJobScheduledTaskLabelDTO>();
			
			
			
			this.ProjectJobScheduledTaskPublishingGroupResources = new List< ProjectJobScheduledTaskPublishingGroupResourceDTO>();
			
			
			
			this.Translated = new List< ProjectJobScheduledTaskTranslatedDTO>();
			
			
			
			this.ProjectJobTasks = new List< ProjectJobTaskDTO>();
			
			
			
			this.ProjectJobScheduledTaskWorkloadingActivities = new List< ProjectJobScheduledTaskWorkloadingActivityDTO>();
			
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 