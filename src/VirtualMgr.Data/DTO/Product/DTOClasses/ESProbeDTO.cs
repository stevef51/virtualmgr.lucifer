﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'ESProbe'.
    /// </summary>
    [Serializable]
    public partial class ESProbeDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the Archived property that maps to the Entity ESProbe</summary>
        public virtual System.Boolean Archived { get; set; }

        /// <summary>Get or set the AssetId property that maps to the Entity ESProbe</summary>
        public virtual System.Int32? AssetId { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity ESProbe</summary>
        public virtual System.Int32 Id { get; set; }

        /// <summary>Get or set the Key property that maps to the Entity ESProbe</summary>
        public virtual System.String Key { get; set; }

        /// <summary>Get or set the Model property that maps to the Entity ESProbe</summary>
        public virtual System.String Model { get; set; }

        /// <summary>Get or set the Name property that maps to the Entity ESProbe</summary>
        public virtual System.String Name { get; set; }

        /// <summary>Get or set the SerialNumber property that maps to the Entity ESProbe</summary>
        public virtual System.String SerialNumber { get; set; }
        
        #endregion

        #region Related Field Public Properties



		
		public virtual IList< ESSensorDTO> Sensors
		{
			get; set;
		}





		public virtual AssetDTO Asset {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public ESProbeDTO()
        {
			
			
			this.Sensors = new List< ESSensorDTO>();
			
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 