﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'Roster'.
    /// </summary>
    [Serializable]
    public partial class RosterDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the AutomaticEndOfDayTime property that maps to the Entity Roster</summary>
        public virtual System.DateTime? AutomaticEndOfDayTime { get; set; }

        /// <summary>Get or set the AutomaticEndOfDayTimeLastRun property that maps to the Entity Roster</summary>
        public virtual System.DateTime? AutomaticEndOfDayTimeLastRun { get; set; }

        /// <summary>Get or set the AutomaticScheduleApprovalTime property that maps to the Entity Roster</summary>
        public virtual System.DateTime? AutomaticScheduleApprovalTime { get; set; }

        /// <summary>Get or set the AutomaticScheduleApprovalTimeLastRun property that maps to the Entity Roster</summary>
        public virtual System.DateTime? AutomaticScheduleApprovalTimeLastRun { get; set; }

        /// <summary>Get or set the EndOfDayDefaultTaskStatus property that maps to the Entity Roster</summary>
        public virtual System.Int32 EndOfDayDefaultTaskStatus { get; set; }

        /// <summary>Get or set the EndTime property that maps to the Entity Roster</summary>
        public virtual System.DateTime EndTime { get; set; }

        /// <summary>Get or set the HierarchyId property that maps to the Entity Roster</summary>
        public virtual System.Int32 HierarchyId { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity Roster</summary>
        public virtual System.Int32 Id { get; set; }

        /// <summary>Get or set the IncludeLunchInTimesheetDurations property that maps to the Entity Roster</summary>
        public virtual System.Boolean IncludeLunchInTimesheetDurations { get; set; }

        /// <summary>Get or set the Name property that maps to the Entity Roster</summary>
        public virtual System.String Name { get; set; }

        /// <summary>Get or set the ProvidesClaimableDutyList property that maps to the Entity Roster</summary>
        public virtual System.Boolean ProvidesClaimableDutyList { get; set; }

        /// <summary>Get or set the ReceivesClaimableDutyList property that maps to the Entity Roster</summary>
        public virtual System.Boolean ReceivesClaimableDutyList { get; set; }

        /// <summary>Get or set the RoundTimesheetToNearestMethod property that maps to the Entity Roster</summary>
        public virtual System.Int32 RoundTimesheetToNearestMethod { get; set; }

        /// <summary>Get or set the RoundTimesheetToNearestMins property that maps to the Entity Roster</summary>
        public virtual System.Int32 RoundTimesheetToNearestMins { get; set; }

        /// <summary>Get or set the ScheduleApprovalDefaultTaskStatus property that maps to the Entity Roster</summary>
        public virtual System.Int32 ScheduleApprovalDefaultTaskStatus { get; set; }

        /// <summary>Get or set the SortOrder property that maps to the Entity Roster</summary>
        public virtual System.Int32 SortOrder { get; set; }

        /// <summary>Get or set the StartTime property that maps to the Entity Roster</summary>
        public virtual System.DateTime StartTime { get; set; }

        /// <summary>Get or set the TimesheetApprovedSourceMethod property that maps to the Entity Roster</summary>
        public virtual System.Int32 TimesheetApprovedSourceMethod { get; set; }

        /// <summary>Get or set the TimesheetUseScheduledEndWhenFinishedBySystem property that maps to the Entity Roster</summary>
        public virtual System.Boolean TimesheetUseScheduledEndWhenFinishedBySystem { get; set; }

        /// <summary>Get or set the Timezone property that maps to the Entity Roster</summary>
        public virtual System.String Timezone { get; set; }
        
        #endregion

        #region Related Field Public Properties



		
		public virtual IList< ProjectJobScheduleDayDTO> ProjectJobScheduleDays
		{
			get; set;
		}



		
		public virtual IList< ProjectJobScheduledTaskDTO> ProjectJobScheduledTasks
		{
			get; set;
		}



		
		public virtual IList< ProjectJobTaskDTO> ProjectJobTasks
		{
			get; set;
		}



		
		public virtual IList< ProjectJobTaskChangeRequestDTO> FromProjectJobTaskChangeRequests
		{
			get; set;
		}



		
		public virtual IList< ProjectJobTaskChangeRequestDTO> ToProjectJobTaskChangeRequests
		{
			get; set;
		}



		
		public virtual IList< RosterEventConditionDTO> RosterEventConditions
		{
			get; set;
		}



		
		public virtual IList< TimesheetItemDTO> TimesheetItems
		{
			get; set;
		}





		public virtual HierarchyDTO Hierarchy {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public RosterDTO()
        {
			
			
			this.ProjectJobScheduleDays = new List< ProjectJobScheduleDayDTO>();
			
			
			
			this.ProjectJobScheduledTasks = new List< ProjectJobScheduledTaskDTO>();
			
			
			
			this.ProjectJobTasks = new List< ProjectJobTaskDTO>();
			
			
			
			this.FromProjectJobTaskChangeRequests = new List< ProjectJobTaskChangeRequestDTO>();
			
			
			
			this.ToProjectJobTaskChangeRequests = new List< ProjectJobTaskChangeRequestDTO>();
			
			
			
			this.RosterEventConditions = new List< RosterEventConditionDTO>();
			
			
			
			this.TimesheetItems = new List< TimesheetItemDTO>();
			
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 