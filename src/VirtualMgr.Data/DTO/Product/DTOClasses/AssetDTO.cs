﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'Asset'.
    /// </summary>
    [Serializable]
    public partial class AssetDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the Archived property that maps to the Entity Asset</summary>
        public virtual System.Boolean Archived { get; set; }

        /// <summary>Get or set the AssetTypeId property that maps to the Entity Asset</summary>
        public virtual System.Int32 AssetTypeId { get; set; }

        /// <summary>Get or set the CompanyId property that maps to the Entity Asset</summary>
        public virtual System.Guid? CompanyId { get; set; }

        /// <summary>Get or set the DateCreated property that maps to the Entity Asset</summary>
        public virtual System.DateTime DateCreated { get; set; }

        /// <summary>Get or set the DateDecomissioned property that maps to the Entity Asset</summary>
        public virtual System.DateTime? DateDecomissioned { get; set; }

        /// <summary>Get or set the FacilityStructureId property that maps to the Entity Asset</summary>
        public virtual System.Int32? FacilityStructureId { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity Asset</summary>
        public virtual System.Int32 Id { get; set; }

        /// <summary>Get or set the Name property that maps to the Entity Asset</summary>
        public virtual System.String Name { get; set; }
        
        #endregion

        #region Related Field Public Properties



		
		public virtual IList< AssetContextDataDTO> AssetContextDatas
		{
			get; set;
		}



		
		public virtual IList< AssetScheduleDTO> AssetSchedules
		{
			get; set;
		}



		
		public virtual IList< ESProbeDTO> ESProbes
		{
			get; set;
		}



		
		public virtual IList< ProjectJobTaskDTO> ProjectJobTasks
		{
			get; set;
		}



		
		public virtual IList< QrcodeDTO> Qrcodes
		{
			get; set;
		}





		public virtual AssetTypeDTO AssetType {get; set;}







		public virtual BeaconDTO Beacon {get; set;}



        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public AssetDTO()
        {
			
			
			this.AssetContextDatas = new List< AssetContextDataDTO>();
			
			
			
			this.AssetSchedules = new List< AssetScheduleDTO>();
			
			
			
			this.ESProbes = new List< ESProbeDTO>();
			
			
			
			this.ProjectJobTasks = new List< ProjectJobTaskDTO>();
			
			
			
			this.Qrcodes = new List< QrcodeDTO>();
			
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 