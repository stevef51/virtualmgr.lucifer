﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'FloorPlan'.
    /// </summary>
    [Serializable]
    public partial class FloorPlanDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the BottomLeftLatitude property that maps to the Entity FloorPlan</summary>
        public virtual System.Decimal BottomLeftLatitude { get; set; }

        /// <summary>Get or set the BottomLeftLongitude property that maps to the Entity FloorPlan</summary>
        public virtual System.Decimal BottomLeftLongitude { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity FloorPlan</summary>
        public virtual System.Int32 Id { get; set; }

        /// <summary>Get or set the ImageUrl property that maps to the Entity FloorPlan</summary>
        public virtual System.String ImageUrl { get; set; }

        /// <summary>Get or set the IndoorAtlasFloorPlanId property that maps to the Entity FloorPlan</summary>
        public virtual System.String IndoorAtlasFloorPlanId { get; set; }

        /// <summary>Get or set the TopLeftLatitude property that maps to the Entity FloorPlan</summary>
        public virtual System.Decimal TopLeftLatitude { get; set; }

        /// <summary>Get or set the TopLeftLongitude property that maps to the Entity FloorPlan</summary>
        public virtual System.Decimal TopLeftLongitude { get; set; }

        /// <summary>Get or set the TopRightLatitude property that maps to the Entity FloorPlan</summary>
        public virtual System.Decimal TopRightLatitude { get; set; }

        /// <summary>Get or set the TopRightLongitude property that maps to the Entity FloorPlan</summary>
        public virtual System.Decimal TopRightLongitude { get; set; }
        
        #endregion

        #region Related Field Public Properties



		
		public virtual IList< FacilityStructureDTO> FacilityStructures
		{
			get; set;
		}









        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public FloorPlanDTO()
        {
			
			
			this.FacilityStructures = new List< FacilityStructureDTO>();
			
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 