﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'HierarchyRoleUserType'.
    /// </summary>
    [Serializable]
    public partial class HierarchyRoleUserTypeDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the Id property that maps to the Entity HierarchyRoleUserType</summary>
        public virtual System.Int32 Id { get; set; }

        /// <summary>Get or set the RoleId property that maps to the Entity HierarchyRoleUserType</summary>
        public virtual System.Int32 RoleId { get; set; }

        /// <summary>Get or set the UserTypeId property that maps to the Entity HierarchyRoleUserType</summary>
        public virtual System.Int32 UserTypeId { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual HierarchyRoleDTO HierarchyRole {get; set;}



		public virtual UserTypeDTO UserType {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public HierarchyRoleUserTypeDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 