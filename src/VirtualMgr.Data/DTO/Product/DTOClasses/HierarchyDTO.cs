﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'Hierarchy'.
    /// </summary>
    [Serializable]
    public partial class HierarchyDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the Id property that maps to the Entity Hierarchy</summary>
        public virtual System.Int32 Id { get; set; }

        /// <summary>Get or set the Name property that maps to the Entity Hierarchy</summary>
        public virtual System.String Name { get; set; }
        
        #endregion

        #region Related Field Public Properties



		
		public virtual IList< FacilityStructureDTO> FacilityStructures
		{
			get; set;
		}



		
		public virtual IList< RosterDTO> Rosters
		{
			get; set;
		}



		
		public virtual IList< HierarchyBucketDTO> HierarchyBuckets
		{
			get; set;
		}









        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public HierarchyDTO()
        {
			
			
			this.FacilityStructures = new List< FacilityStructureDTO>();
			
			
			
			this.Rosters = new List< RosterDTO>();
			
			
			
			this.HierarchyBuckets = new List< HierarchyBucketDTO>();
			
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 