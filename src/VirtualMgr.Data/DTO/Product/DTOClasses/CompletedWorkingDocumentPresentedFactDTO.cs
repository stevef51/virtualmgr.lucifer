﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'CompletedWorkingDocumentPresentedFact'.
    /// </summary>
    [Serializable]
    public partial class CompletedWorkingDocumentPresentedFactDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the ContextType property that maps to the Entity CompletedWorkingDocumentPresentedFact</summary>
        public virtual System.Int32 ContextType { get; set; }

        /// <summary>Get or set the DateCompleted property that maps to the Entity CompletedWorkingDocumentPresentedFact</summary>
        public virtual System.DateTime DateCompleted { get; set; }

        /// <summary>Get or set the DateStarted property that maps to the Entity CompletedWorkingDocumentPresentedFact</summary>
        public virtual System.DateTime DateStarted { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity CompletedWorkingDocumentPresentedFact</summary>
        public virtual System.Guid Id { get; set; }

        /// <summary>Get or set the InternalId property that maps to the Entity CompletedWorkingDocumentPresentedFact</summary>
        public virtual System.Guid InternalId { get; set; }

        /// <summary>Get or set the IsUserContext property that maps to the Entity CompletedWorkingDocumentPresentedFact</summary>
        public virtual System.Boolean IsUserContext { get; set; }

        /// <summary>Get or set the NotesText property that maps to the Entity CompletedWorkingDocumentPresentedFact</summary>
        public virtual System.String NotesText { get; set; }

        /// <summary>Get or set the ParentWorkingDocumentId property that maps to the Entity CompletedWorkingDocumentPresentedFact</summary>
        public virtual System.Guid? ParentWorkingDocumentId { get; set; }

        /// <summary>Get or set the PassFail property that maps to the Entity CompletedWorkingDocumentPresentedFact</summary>
        public virtual System.Boolean? PassFail { get; set; }

        /// <summary>Get or set the PossibleScore property that maps to the Entity CompletedWorkingDocumentPresentedFact</summary>
        public virtual System.Decimal PossibleScore { get; set; }

        /// <summary>Get or set the PresentedId property that maps to the Entity CompletedWorkingDocumentPresentedFact</summary>
        public virtual System.Guid PresentedId { get; set; }

        /// <summary>Get or set the PresentedOrder property that maps to the Entity CompletedWorkingDocumentPresentedFact</summary>
        public virtual System.Int32 PresentedOrder { get; set; }

        /// <summary>Get or set the Prompt property that maps to the Entity CompletedWorkingDocumentPresentedFact</summary>
        public virtual System.String Prompt { get; set; }

        /// <summary>Get or set the ReportingPresentedTypeId property that maps to the Entity CompletedWorkingDocumentPresentedFact</summary>
        public virtual System.Guid ReportingPresentedTypeId { get; set; }

        /// <summary>Get or set the ReportingUserRevieweeId property that maps to the Entity CompletedWorkingDocumentPresentedFact</summary>
        public virtual System.Guid ReportingUserRevieweeId { get; set; }

        /// <summary>Get or set the ReportingUserReviewerId property that maps to the Entity CompletedWorkingDocumentPresentedFact</summary>
        public virtual System.Guid ReportingUserReviewerId { get; set; }

        /// <summary>Get or set the ReportingWorkingDocumentId property that maps to the Entity CompletedWorkingDocumentPresentedFact</summary>
        public virtual System.Guid ReportingWorkingDocumentId { get; set; }

        /// <summary>Get or set the Score property that maps to the Entity CompletedWorkingDocumentPresentedFact</summary>
        public virtual System.Decimal Score { get; set; }

        /// <summary>Get or set the WorkingDocumentId property that maps to the Entity CompletedWorkingDocumentPresentedFact</summary>
        public virtual System.Guid WorkingDocumentId { get; set; }
        
        #endregion

        #region Related Field Public Properties



		
		public virtual IList< CompletedLabelWorkingDocumentPresentedDimensionDTO> CompletedLabelWorkingDocumentPresentedDimensions
		{
			get; set;
		}



		
		public virtual IList< CompletedPresentedFactExtraValueDTO> CompletedPresentedFactExtraValues
		{
			get; set;
		}



		
		public virtual IList< CompletedPresentedFactValueDTO> CompletedPresentedFactValues
		{
			get; set;
		}



		
		public virtual IList< CompletedPresentedGeoLocationFactValueDTO> CompletedPresentedGeoLocationFactValues
		{
			get; set;
		}



		
		public virtual IList< CompletedPresentedUploadMediaFactValueDTO> CompletedPresentedUploadMediaFactValues
		{
			get; set;
		}





		public virtual CompletedPresentedTypeDimensionDTO CompletedPresentedTypeDimension {get; set;}



		public virtual CompletedUserDimensionDTO CompletedUserDimensionReviewee {get; set;}



		public virtual CompletedUserDimensionDTO CompletedUserDimensionReviewer {get; set;}



		public virtual CompletedWorkingDocumentDimensionDTO CompletedWorkingDocumentDimension {get; set;}



		public virtual CompletedWorkingDocumentFactDTO CompletedWorkingDocumentFact {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public CompletedWorkingDocumentPresentedFactDTO()
        {
			
			
			this.CompletedLabelWorkingDocumentPresentedDimensions = new List< CompletedLabelWorkingDocumentPresentedDimensionDTO>();
			
			
			
			this.CompletedPresentedFactExtraValues = new List< CompletedPresentedFactExtraValueDTO>();
			
			
			
			this.CompletedPresentedFactValues = new List< CompletedPresentedFactValueDTO>();
			
			
			
			this.CompletedPresentedGeoLocationFactValues = new List< CompletedPresentedGeoLocationFactValueDTO>();
			
			
			
			this.CompletedPresentedUploadMediaFactValues = new List< CompletedPresentedUploadMediaFactValueDTO>();
			
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 