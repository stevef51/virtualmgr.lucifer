﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'BeaconSighting'.
    /// </summary>
    [Serializable]
    public partial class BeaconSightingDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the BatteryPercent property that maps to the Entity BeaconSighting</summary>
        public virtual System.Int32? BatteryPercent { get; set; }

        /// <summary>Get or set the BeaconId property that maps to the Entity BeaconSighting</summary>
        public virtual System.String BeaconId { get; set; }

        /// <summary>Get or set the BestRange property that maps to the Entity BeaconSighting</summary>
        public virtual System.Int32 BestRange { get; set; }

        /// <summary>Get or set the BestRangeUtc property that maps to the Entity BeaconSighting</summary>
        public virtual System.DateTime BestRangeUtc { get; set; }

        /// <summary>Get or set the FirstRangeUtc property that maps to the Entity BeaconSighting</summary>
        public virtual System.DateTime FirstRangeUtc { get; set; }

        /// <summary>Get or set the GoneUtc property that maps to the Entity BeaconSighting</summary>
        public virtual System.DateTime? GoneUtc { get; set; }

        /// <summary>Get or set the GpslocationId property that maps to the Entity BeaconSighting</summary>
        public virtual System.Int64? GpslocationId { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity BeaconSighting</summary>
        public virtual System.Int64 Id { get; set; }

        /// <summary>Get or set the LoggedInUserId property that maps to the Entity BeaconSighting</summary>
        public virtual System.Guid? LoggedInUserId { get; set; }

        /// <summary>Get or set the NextStaticBeaconSightingId property that maps to the Entity BeaconSighting</summary>
        public virtual System.Int64? NextStaticBeaconSightingId { get; set; }

        /// <summary>Get or set the PrevStaticBeaconSightingId property that maps to the Entity BeaconSighting</summary>
        public virtual System.Int64? PrevStaticBeaconSightingId { get; set; }

        /// <summary>Get or set the TabletUuid property that maps to the Entity BeaconSighting</summary>
        public virtual System.String TabletUuid { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual BeaconDTO Beacon {get; set;}



		public virtual TabletUuidDTO TabletUuid_ {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public BeaconSightingDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 