﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'UserType'.
    /// </summary>
    [Serializable]
    public partial class UserTypeDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the CanLogin property that maps to the Entity UserType</summary>
        public virtual System.Boolean CanLogin { get; set; }

        /// <summary>Get or set the DefaultDashboard property that maps to the Entity UserType</summary>
        public virtual System.String DefaultDashboard { get; set; }

        /// <summary>Get or set the HasGeoCordinates property that maps to the Entity UserType</summary>
        public virtual System.Boolean HasGeoCordinates { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity UserType</summary>
        public virtual System.Int32 Id { get; set; }

        /// <summary>Get or set the IsAsite property that maps to the Entity UserType</summary>
        public virtual System.Boolean IsAsite { get; set; }

        /// <summary>Get or set the Name property that maps to the Entity UserType</summary>
        public virtual System.String Name { get; set; }

        /// <summary>Get or set the UserBillingTypeId property that maps to the Entity UserType</summary>
        public virtual System.Int32? UserBillingTypeId { get; set; }
        
        #endregion

        #region Related Field Public Properties



		
		public virtual IList< UserTypeTaskTypeRelationshipDTO> UserTypeTaskTypeRelationships
		{
			get; set;
		}



		
		public virtual IList< HierarchyRoleUserTypeDTO> HierarchyRoleUserTypes
		{
			get; set;
		}



		
		public virtual IList< UserDataDTO> UserDatas
		{
			get; set;
		}



		
		public virtual IList< UserTypeContextPublishedResourceDTO> UserTypeContextPublishedResources
		{
			get; set;
		}



		
		public virtual IList< UserTypeDashboardDTO> UserTypeDashboards
		{
			get; set;
		}



		
		public virtual IList< UserTypeDefaultLabelDTO> UserTypeDefaultLabels
		{
			get; set;
		}



		
		public virtual IList< UserTypeDefaultRoleDTO> UserTypeDefaultRoles
		{
			get; set;
		}





		public virtual UserBillingTypeDTO UserBillingType {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public UserTypeDTO()
        {
			
			
			this.UserTypeTaskTypeRelationships = new List< UserTypeTaskTypeRelationshipDTO>();
			
			
			
			this.HierarchyRoleUserTypes = new List< HierarchyRoleUserTypeDTO>();
			
			
			
			this.UserDatas = new List< UserDataDTO>();
			
			
			
			this.UserTypeContextPublishedResources = new List< UserTypeContextPublishedResourceDTO>();
			
			
			
			this.UserTypeDashboards = new List< UserTypeDashboardDTO>();
			
			
			
			this.UserTypeDefaultLabels = new List< UserTypeDefaultLabelDTO>();
			
			
			
			this.UserTypeDefaultRoles = new List< UserTypeDefaultRoleDTO>();
			
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 