﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'TimesheetedTaskWorkLog'.
    /// </summary>
    [Serializable]
    public partial class TimesheetedTaskWorkLogDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the ProjectJobTaskWorkLogId property that maps to the Entity TimesheetedTaskWorkLog</summary>
        public virtual System.Guid ProjectJobTaskWorkLogId { get; set; }

        /// <summary>Get or set the TimesheetItemId property that maps to the Entity TimesheetedTaskWorkLog</summary>
        public virtual System.Int32 TimesheetItemId { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual ProjectJobTaskWorkLogDTO ProjectJobTaskWorkLog {get; set;}



		public virtual TimesheetItemDTO TimesheetItem {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public TimesheetedTaskWorkLogDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 