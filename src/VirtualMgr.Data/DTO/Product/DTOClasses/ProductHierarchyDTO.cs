﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'ProductHierarchy'.
    /// </summary>
    [Serializable]
    public partial class ProductHierarchyDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the ChildProductId property that maps to the Entity ProductHierarchy</summary>
        public virtual System.Int32 ChildProductId { get; set; }

        /// <summary>Get or set the Name property that maps to the Entity ProductHierarchy</summary>
        public virtual System.String Name { get; set; }

        /// <summary>Get or set the ParentProductId property that maps to the Entity ProductHierarchy</summary>
        public virtual System.Int32 ParentProductId { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual ProductDTO ChildProduct {get; set;}



		public virtual ProductDTO ParentProduct {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public ProductHierarchyDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 