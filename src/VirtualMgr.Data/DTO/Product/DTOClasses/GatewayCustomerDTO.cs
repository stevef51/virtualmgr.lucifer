﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'GatewayCustomer'.
    /// </summary>
    [Serializable]
    public partial class GatewayCustomerDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the GatewayName property that maps to the Entity GatewayCustomer</summary>
        public virtual System.String GatewayName { get; set; }

        /// <summary>Get or set the TokenCustomerId property that maps to the Entity GatewayCustomer</summary>
        public virtual System.String TokenCustomerId { get; set; }

        /// <summary>Get or set the UserId property that maps to the Entity GatewayCustomer</summary>
        public virtual System.Guid UserId { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual UserDataDTO UserData {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public GatewayCustomerDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 