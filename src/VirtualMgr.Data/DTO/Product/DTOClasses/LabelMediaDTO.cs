﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'LabelMedia'.
    /// </summary>
    [Serializable]
    public partial class LabelMediaDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the LabelId property that maps to the Entity LabelMedia</summary>
        public virtual System.Guid LabelId { get; set; }

        /// <summary>Get or set the MediaId property that maps to the Entity LabelMedia</summary>
        public virtual System.Guid MediaId { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual LabelDTO Label {get; set;}



		public virtual MediaDTO Medium {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public LabelMediaDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 