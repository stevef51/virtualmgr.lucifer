﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'ExecutionHandlerQueue'.
    /// </summary>
    [Serializable]
    public partial class ExecutionHandlerQueueDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the Data property that maps to the Entity ExecutionHandlerQueue</summary>
        public virtual System.String Data { get; set; }

        /// <summary>Get or set the DateCreated property that maps to the Entity ExecutionHandlerQueue</summary>
        public virtual System.DateTime DateCreated { get; set; }

        /// <summary>Get or set the Handler property that maps to the Entity ExecutionHandlerQueue</summary>
        public virtual System.String Handler { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity ExecutionHandlerQueue</summary>
        public virtual System.Guid Id { get; set; }

        /// <summary>Get or set the LatestException property that maps to the Entity ExecutionHandlerQueue</summary>
        public virtual System.String LatestException { get; set; }

        /// <summary>Get or set the Method property that maps to the Entity ExecutionHandlerQueue</summary>
        public virtual System.String Method { get; set; }

        /// <summary>Get or set the PrimaryEntityId property that maps to the Entity ExecutionHandlerQueue</summary>
        public virtual System.String PrimaryEntityId { get; set; }

        /// <summary>Get or set the Processed property that maps to the Entity ExecutionHandlerQueue</summary>
        public virtual System.Boolean Processed { get; set; }
        
        #endregion

        #region Related Field Public Properties









        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public ExecutionHandlerQueueDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 