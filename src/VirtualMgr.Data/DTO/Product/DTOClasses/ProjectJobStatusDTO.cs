﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'ProjectJobStatus'.
    /// </summary>
    [Serializable]
    public partial class ProjectJobStatusDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the Description property that maps to the Entity ProjectJobStatus</summary>
        public virtual System.String Description { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity ProjectJobStatus</summary>
        public virtual System.Guid Id { get; set; }

        /// <summary>Get or set the Text property that maps to the Entity ProjectJobStatus</summary>
        public virtual System.String Text { get; set; }
        
        #endregion

        #region Related Field Public Properties



		
		public virtual IList< ProjectJobTaskDTO> ProjectJobTasks
		{
			get; set;
		}



		
		public virtual IList< ProjectJobTaskTypeStatusDTO> ProjectJobTaskTypeStatuses
		{
			get; set;
		}









        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public ProjectJobStatusDTO()
        {
			
			
			this.ProjectJobTasks = new List< ProjectJobTaskDTO>();
			
			
			
			this.ProjectJobTaskTypeStatuses = new List< ProjectJobTaskTypeStatusDTO>();
			
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 