﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'ESSensorReading'.
    /// </summary>
    [Serializable]
    public partial class ESSensorReadingDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the Id property that maps to the Entity ESSensorReading</summary>
        public virtual System.Int32 Id { get; set; }

        /// <summary>Get or set the SensorId property that maps to the Entity ESSensorReading</summary>
        public virtual System.Int32 SensorId { get; set; }

        /// <summary>Get or set the SensorTimestampUtc property that maps to the Entity ESSensorReading</summary>
        public virtual System.DateTime SensorTimestampUtc { get; set; }

        /// <summary>Get or set the ServerTimestampUtc property that maps to the Entity ESSensorReading</summary>
        public virtual System.DateTime ServerTimestampUtc { get; set; }

        /// <summary>Get or set the TabletUuid property that maps to the Entity ESSensorReading</summary>
        public virtual System.String TabletUuid { get; set; }

        /// <summary>Get or set the Value property that maps to the Entity ESSensorReading</summary>
        public virtual System.Decimal Value { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual TabletUuidDTO SensorReadingTabletUuid {get; set;}



		public virtual ESSensorDTO Sensor {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public ESSensorReadingDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 