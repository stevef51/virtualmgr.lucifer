﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'CompletedPresentedTypeDimension'.
    /// </summary>
    [Serializable]
    public partial class CompletedPresentedTypeDimensionDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the Id property that maps to the Entity CompletedPresentedTypeDimension</summary>
        public virtual System.Guid Id { get; set; }

        /// <summary>Get or set the Type property that maps to the Entity CompletedPresentedTypeDimension</summary>
        public virtual System.String Type { get; set; }
        
        #endregion

        #region Related Field Public Properties



		
		public virtual IList< CompletedWorkingDocumentPresentedFactDTO> CompletedWorkingDocumentPresentedFacts
		{
			get; set;
		}









        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public CompletedPresentedTypeDimensionDTO()
        {
			
			
			this.CompletedWorkingDocumentPresentedFacts = new List< CompletedWorkingDocumentPresentedFactDTO>();
			
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 