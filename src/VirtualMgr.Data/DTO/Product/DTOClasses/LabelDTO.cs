﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'Label'.
    /// </summary>
    [Serializable]
    public partial class LabelDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the Backcolor property that maps to the Entity Label</summary>
        public virtual System.Int32 Backcolor { get; set; }

        /// <summary>Get or set the Forecolor property that maps to the Entity Label</summary>
        public virtual System.Int32 Forecolor { get; set; }

        /// <summary>Get or set the ForMedia property that maps to the Entity Label</summary>
        public virtual System.Boolean ForMedia { get; set; }

        /// <summary>Get or set the ForQuestions property that maps to the Entity Label</summary>
        public virtual System.Boolean ForQuestions { get; set; }

        /// <summary>Get or set the ForResources property that maps to the Entity Label</summary>
        public virtual System.Boolean ForResources { get; set; }

        /// <summary>Get or set the ForTasks property that maps to the Entity Label</summary>
        public virtual System.Boolean ForTasks { get; set; }

        /// <summary>Get or set the ForUsers property that maps to the Entity Label</summary>
        public virtual System.Boolean ForUsers { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity Label</summary>
        public virtual System.Guid Id { get; set; }

        /// <summary>Get or set the Name property that maps to the Entity Label</summary>
        public virtual System.String Name { get; set; }
        
        #endregion

        #region Related Field Public Properties



		
		public virtual IList< AssetTypeCalendarTaskLabelDTO> AssetTypeCalendarTaskLabels
		{
			get; set;
		}



		
		public virtual IList< ProjectJobScheduledJobTaskUserLabelDTO> ProjectJobScheduledJobTaskUserLabels
		{
			get; set;
		}



		
		public virtual IList< ProjectJobScheduledTaskLabelDTO> ProjectJobScheduledTaskLabels
		{
			get; set;
		}



		
		public virtual IList< ProjectJobTaskLabelDTO> ProjectJobTaskLabels
		{
			get; set;
		}



		
		public virtual IList< HierarchyBucketLabelDTO> HierarchyBucketLabels
		{
			get; set;
		}



		
		public virtual IList< LabelMediaDTO> LabelMedias
		{
			get; set;
		}



		
		public virtual IList< LabelPublishingGroupResourceDTO> LabelPublishingGroupResources
		{
			get; set;
		}



		
		public virtual IList< LabelResourceDTO> LabelResources
		{
			get; set;
		}



		
		public virtual IList< LabelUserDTO> LabelUsers
		{
			get; set;
		}



		
		public virtual IList< PublishingGroupActorLabelDTO> PublishingGroupActorLabels
		{
			get; set;
		}



		
		public virtual IList< UserTypeDefaultLabelDTO> UserTypeDefaultLabels
		{
			get; set;
		}









        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public LabelDTO()
        {
			
			
			this.AssetTypeCalendarTaskLabels = new List< AssetTypeCalendarTaskLabelDTO>();
			
			
			
			this.ProjectJobScheduledJobTaskUserLabels = new List< ProjectJobScheduledJobTaskUserLabelDTO>();
			
			
			
			this.ProjectJobScheduledTaskLabels = new List< ProjectJobScheduledTaskLabelDTO>();
			
			
			
			this.ProjectJobTaskLabels = new List< ProjectJobTaskLabelDTO>();
			
			
			
			this.HierarchyBucketLabels = new List< HierarchyBucketLabelDTO>();
			
			
			
			this.LabelMedias = new List< LabelMediaDTO>();
			
			
			
			this.LabelPublishingGroupResources = new List< LabelPublishingGroupResourceDTO>();
			
			
			
			this.LabelResources = new List< LabelResourceDTO>();
			
			
			
			this.LabelUsers = new List< LabelUserDTO>();
			
			
			
			this.PublishingGroupActorLabels = new List< PublishingGroupActorLabelDTO>();
			
			
			
			this.UserTypeDefaultLabels = new List< UserTypeDefaultLabelDTO>();
			
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 