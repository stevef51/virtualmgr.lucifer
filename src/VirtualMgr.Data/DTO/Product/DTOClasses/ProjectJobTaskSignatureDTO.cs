﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'ProjectJobTaskSignature'.
    /// </summary>
    [Serializable]
    public partial class ProjectJobTaskSignatureDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the Accuracy property that maps to the Entity ProjectJobTaskSignature</summary>
        public virtual System.Decimal? Accuracy { get; set; }

        /// <summary>Get or set the Latitude property that maps to the Entity ProjectJobTaskSignature</summary>
        public virtual System.Decimal? Latitude { get; set; }

        /// <summary>Get or set the LocationStatus property that maps to the Entity ProjectJobTaskSignature</summary>
        public virtual System.Int32 LocationStatus { get; set; }

        /// <summary>Get or set the Longitude property that maps to the Entity ProjectJobTaskSignature</summary>
        public virtual System.Decimal? Longitude { get; set; }

        /// <summary>Get or set the PhotoMediaId property that maps to the Entity ProjectJobTaskSignature</summary>
        public virtual System.Guid? PhotoMediaId { get; set; }

        /// <summary>Get or set the ProjectJobTaskId property that maps to the Entity ProjectJobTaskSignature</summary>
        public virtual System.Guid ProjectJobTaskId { get; set; }

        /// <summary>Get or set the SignatureData property that maps to the Entity ProjectJobTaskSignature</summary>
        public virtual System.String SignatureData { get; set; }

        /// <summary>Get or set the SignatureMediaId property that maps to the Entity ProjectJobTaskSignature</summary>
        public virtual System.Guid? SignatureMediaId { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual MediaDTO PhotoMedia {get; set;}



		public virtual MediaDTO SignatureMedia {get; set;}







		public virtual ProjectJobTaskDTO ProjectJobTask {get; set;}



        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public ProjectJobTaskSignatureDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 