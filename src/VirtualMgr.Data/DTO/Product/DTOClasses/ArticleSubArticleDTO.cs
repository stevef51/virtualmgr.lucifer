﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'ArticleSubArticle'.
    /// </summary>
    [Serializable]
    public partial class ArticleSubArticleDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the ArticleId property that maps to the Entity ArticleSubArticle</summary>
        public virtual System.Guid ArticleId { get; set; }

        /// <summary>Get or set the Index property that maps to the Entity ArticleSubArticle</summary>
        public virtual System.Int32 Index { get; set; }

        /// <summary>Get or set the Name property that maps to the Entity ArticleSubArticle</summary>
        public virtual System.String Name { get; set; }

        /// <summary>Get or set the SubArticleId property that maps to the Entity ArticleSubArticle</summary>
        public virtual System.Guid SubArticleId { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual ArticleDTO Article {get; set;}



		public virtual ArticleDTO SubArticle {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public ArticleSubArticleDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 