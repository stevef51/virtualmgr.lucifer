﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'ProjectJobTaskChangeRequest'.
    /// </summary>
    [Serializable]
    public partial class ProjectJobTaskChangeRequestDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the ChangeType property that maps to the Entity ProjectJobTaskChangeRequest</summary>
        public virtual System.Int32 ChangeType { get; set; }

        /// <summary>Get or set the FromRosterId property that maps to the Entity ProjectJobTaskChangeRequest</summary>
        public virtual System.Int32? FromRosterId { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity ProjectJobTaskChangeRequest</summary>
        public virtual System.Guid Id { get; set; }

        /// <summary>Get or set the ProcessedByUserId property that maps to the Entity ProjectJobTaskChangeRequest</summary>
        public virtual System.Guid? ProcessedByUserId { get; set; }

        /// <summary>Get or set the ProjectJobTaskId property that maps to the Entity ProjectJobTaskChangeRequest</summary>
        public virtual System.Guid ProjectJobTaskId { get; set; }

        /// <summary>Get or set the RequestedByUserId property that maps to the Entity ProjectJobTaskChangeRequest</summary>
        public virtual System.Guid RequestedByUserId { get; set; }

        /// <summary>Get or set the Status property that maps to the Entity ProjectJobTaskChangeRequest</summary>
        public virtual System.Int32 Status { get; set; }

        /// <summary>Get or set the ToRosterId property that maps to the Entity ProjectJobTaskChangeRequest</summary>
        public virtual System.Int32? ToRosterId { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual ProjectJobTaskDTO ProjectJobTask {get; set;}



		public virtual RosterDTO FromRoster {get; set;}



		public virtual RosterDTO ToRoster {get; set;}



		public virtual UserDataDTO ProcessedByUserData {get; set;}



		public virtual UserDataDTO RequestedByUserData {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public ProjectJobTaskChangeRequestDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 