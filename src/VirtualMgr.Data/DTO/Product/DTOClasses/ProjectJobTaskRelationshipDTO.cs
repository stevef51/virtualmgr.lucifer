﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'ProjectJobTaskRelationship'.
    /// </summary>
    [Serializable]
    public partial class ProjectJobTaskRelationshipDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the DestinationProjectJobTaskId property that maps to the Entity ProjectJobTaskRelationship</summary>
        public virtual System.Guid DestinationProjectJobTaskId { get; set; }

        /// <summary>Get or set the ProjectJobTaskRelationshipTypeId property that maps to the Entity ProjectJobTaskRelationship</summary>
        public virtual System.Int32 ProjectJobTaskRelationshipTypeId { get; set; }

        /// <summary>Get or set the SourceProjectJobTaskId property that maps to the Entity ProjectJobTaskRelationship</summary>
        public virtual System.Guid SourceProjectJobTaskId { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual ProjectJobTaskDTO ProjectJobTask {get; set;}



		public virtual ProjectJobTaskDTO ProjectJobTask_ {get; set;}



		public virtual ProjectJobTaskRelationshipTypeDTO ProjectJobTaskRelationshipType {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public ProjectJobTaskRelationshipDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 