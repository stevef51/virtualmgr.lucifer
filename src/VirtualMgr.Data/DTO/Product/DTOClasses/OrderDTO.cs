﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'Order'.
    /// </summary>
    [Serializable]
    public partial class OrderDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the Archived property that maps to the Entity Order</summary>
        public virtual System.Boolean Archived { get; set; }

        /// <summary>Get or set the CancelledDateUtc property that maps to the Entity Order</summary>
        public virtual System.DateTime? CancelledDateUtc { get; set; }

        /// <summary>Get or set the DateCreated property that maps to the Entity Order</summary>
        public virtual System.DateTime DateCreated { get; set; }

        /// <summary>Get or set the ExpectedDeliveryDate property that maps to the Entity Order</summary>
        public virtual System.DateTime? ExpectedDeliveryDate { get; set; }

        /// <summary>Get or set the ExtensionOfOrderId property that maps to the Entity Order</summary>
        public virtual System.Guid? ExtensionOfOrderId { get; set; }

        /// <summary>Get or set the FacilityStructureId property that maps to the Entity Order</summary>
        public virtual System.Int32 FacilityStructureId { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity Order</summary>
        public virtual System.Guid Id { get; set; }

        /// <summary>Get or set the MaxCoupons property that maps to the Entity Order</summary>
        public virtual System.Int32 MaxCoupons { get; set; }

        /// <summary>Get or set the OrderedById property that maps to the Entity Order</summary>
        public virtual System.Guid OrderedById { get; set; }

        /// <summary>Get or set the PendingExpiryDate property that maps to the Entity Order</summary>
        public virtual System.DateTime? PendingExpiryDate { get; set; }

        /// <summary>Get or set the ProductCatalogId property that maps to the Entity Order</summary>
        public virtual System.Int32 ProductCatalogId { get; set; }

        /// <summary>Get or set the ProjectJobTaskId property that maps to the Entity Order</summary>
        public virtual System.Guid? ProjectJobTaskId { get; set; }

        /// <summary>Get or set the TotalPrice property that maps to the Entity Order</summary>
        public virtual System.Decimal? TotalPrice { get; set; }

        /// <summary>Get or set the WorkflowState property that maps to the Entity Order</summary>
        public virtual System.String WorkflowState { get; set; }
        
        #endregion

        #region Related Field Public Properties



		
		public virtual IList< OrderItemDTO> OrderItems
		{
			get; set;
		}



		
		public virtual IList< OrderMediaDTO> OrderMedia
		{
			get; set;
		}





		public virtual FacilityStructureDTO FacilityStructure {get; set;}



		public virtual ProjectJobTaskDTO ProjectJobTask {get; set;}



		public virtual ProductCatalogDTO ProductCatalog {get; set;}



		public virtual UserDataDTO UserData {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public OrderDTO()
        {
			
			
			this.OrderItems = new List< OrderItemDTO>();
			
			
			
			this.OrderMedia = new List< OrderMediaDTO>();
			
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 