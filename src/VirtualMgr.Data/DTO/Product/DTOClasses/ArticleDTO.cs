﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'Article'.
    /// </summary>
    [Serializable]
    public partial class ArticleDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the Id property that maps to the Entity Article</summary>
        public virtual System.Guid Id { get; set; }
        
        #endregion

        #region Related Field Public Properties



		
		public virtual IList< ArticleBigIntDTO> ArticleBigInts
		{
			get; set;
		}



		
		public virtual IList< ArticleBoolDTO> ArticleBools
		{
			get; set;
		}



		
		public virtual IList< ArticleDateTimeDTO> ArticleDateTimes
		{
			get; set;
		}



		
		public virtual IList< ArticleGuidDTO> ArticleGuids
		{
			get; set;
		}



		
		public virtual IList< ArticleNumberDTO> ArticleNumbers
		{
			get; set;
		}



		
		public virtual IList< ArticleStringDTO> ArticleStrings
		{
			get; set;
		}



		
		public virtual IList< ArticleSubArticleDTO> ArticleSubArticles
		{
			get; set;
		}



		
		public virtual IList< ArticleSubArticleDTO> SubArticleArticles
		{
			get; set;
		}









        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public ArticleDTO()
        {
			
			
			this.ArticleBigInts = new List< ArticleBigIntDTO>();
			
			
			
			this.ArticleBools = new List< ArticleBoolDTO>();
			
			
			
			this.ArticleDateTimes = new List< ArticleDateTimeDTO>();
			
			
			
			this.ArticleGuids = new List< ArticleGuidDTO>();
			
			
			
			this.ArticleNumbers = new List< ArticleNumberDTO>();
			
			
			
			this.ArticleStrings = new List< ArticleStringDTO>();
			
			
			
			this.ArticleSubArticles = new List< ArticleSubArticleDTO>();
			
			
			
			this.SubArticleArticles = new List< ArticleSubArticleDTO>();
			
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 