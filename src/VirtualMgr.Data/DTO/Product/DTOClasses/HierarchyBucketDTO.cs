﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'HierarchyBucket'.
    /// </summary>
    [Serializable]
    public partial class HierarchyBucketDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the AssignTasksThroughLabels property that maps to the Entity HierarchyBucket</summary>
        public virtual System.Boolean AssignTasksThroughLabels { get; set; }

        /// <summary>Get or set the DefaultSaturdayTimesheetItemTypeId property that maps to the Entity HierarchyBucket</summary>
        public virtual System.Int32? DefaultSaturdayTimesheetItemTypeId { get; set; }

        /// <summary>Get or set the DefaultSundayTimesheetItemTypeId property that maps to the Entity HierarchyBucket</summary>
        public virtual System.Int32? DefaultSundayTimesheetItemTypeId { get; set; }

        /// <summary>Get or set the DefaultWeekDayTimesheetItemTypeId property that maps to the Entity HierarchyBucket</summary>
        public virtual System.Int32? DefaultWeekDayTimesheetItemTypeId { get; set; }

        /// <summary>Get or set the HierarchyId property that maps to the Entity HierarchyBucket</summary>
        public virtual System.Int32 HierarchyId { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity HierarchyBucket</summary>
        public virtual System.Int32 Id { get; set; }

        /// <summary>Get or set the IsPrimary property that maps to the Entity HierarchyBucket</summary>
        public virtual System.Boolean? IsPrimary { get; set; }

        /// <summary>Get or set the LeftIndex property that maps to the Entity HierarchyBucket</summary>
        public virtual System.Int32 LeftIndex { get; set; }

        /// <summary>Get or set the Name property that maps to the Entity HierarchyBucket</summary>
        public virtual System.String Name { get; set; }

        /// <summary>Get or set the OpenSecurityThroughLabels property that maps to the Entity HierarchyBucket</summary>
        public virtual System.Boolean OpenSecurityThroughLabels { get; set; }

        /// <summary>Get or set the ParentId property that maps to the Entity HierarchyBucket</summary>
        public virtual System.Int32? ParentId { get; set; }

        /// <summary>Get or set the RightIndex property that maps to the Entity HierarchyBucket</summary>
        public virtual System.Int32 RightIndex { get; set; }

        /// <summary>Get or set the RoleId property that maps to the Entity HierarchyBucket</summary>
        public virtual System.Int32 RoleId { get; set; }

        /// <summary>Get or set the SortOrder property that maps to the Entity HierarchyBucket</summary>
        public virtual System.Int32 SortOrder { get; set; }

        /// <summary>Get or set the TasksCanBeClaimed property that maps to the Entity HierarchyBucket</summary>
        public virtual System.Boolean TasksCanBeClaimed { get; set; }

        /// <summary>Get or set the UserId property that maps to the Entity HierarchyBucket</summary>
        public virtual System.Guid? UserId { get; set; }
        
        #endregion

        #region Related Field Public Properties



		
		public virtual IList< ProjectJobTaskDTO> ProjectJobTasks
		{
			get; set;
		}



		
		public virtual IList< HierarchyBucketDTO> Children
		{
			get; set;
		}



		
		public virtual IList< HierarchyBucketLabelDTO> HierarchyBucketLabels
		{
			get; set;
		}



		
		public virtual IList< HierarchyBucketOverrideDTO> HierarchyBucketOverrides
		{
			get; set;
		}





		public virtual TimesheetItemTypeDTO TimesheetItemType {get; set;}



		public virtual TimesheetItemTypeDTO TimesheetItemType_ {get; set;}



		public virtual TimesheetItemTypeDTO TimesheetItemType__ {get; set;}



		public virtual HierarchyDTO Hierarchy {get; set;}



		public virtual HierarchyBucketDTO Parent {get; set;}



		public virtual HierarchyRoleDTO HierarchyRole {get; set;}



		public virtual UserDataDTO UserData {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public HierarchyBucketDTO()
        {
			
			
			this.ProjectJobTasks = new List< ProjectJobTaskDTO>();
			
			
			
			this.Children = new List< HierarchyBucketDTO>();
			
			
			
			this.HierarchyBucketLabels = new List< HierarchyBucketLabelDTO>();
			
			
			
			this.HierarchyBucketOverrides = new List< HierarchyBucketOverrideDTO>();
			
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 