﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'ArticleGuid'.
    /// </summary>
    [Serializable]
    public partial class ArticleGuidDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the ArticleId property that maps to the Entity ArticleGuid</summary>
        public virtual System.Guid ArticleId { get; set; }

        /// <summary>Get or set the Index property that maps to the Entity ArticleGuid</summary>
        public virtual System.Int32 Index { get; set; }

        /// <summary>Get or set the Name property that maps to the Entity ArticleGuid</summary>
        public virtual System.String Name { get; set; }

        /// <summary>Get or set the Value property that maps to the Entity ArticleGuid</summary>
        public virtual System.Guid Value { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual ArticleDTO Article {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public ArticleGuidDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 