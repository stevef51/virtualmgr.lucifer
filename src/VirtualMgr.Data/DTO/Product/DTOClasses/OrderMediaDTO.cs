﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'OrderMedia'.
    /// </summary>
    [Serializable]
    public partial class OrderMediaDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the DateCreated property that maps to the Entity OrderMedia</summary>
        public virtual System.DateTime DateCreated { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity OrderMedia</summary>
        public virtual System.Int32 Id { get; set; }

        /// <summary>Get or set the MediaId property that maps to the Entity OrderMedia</summary>
        public virtual System.Guid MediaId { get; set; }

        /// <summary>Get or set the OrderId property that maps to the Entity OrderMedia</summary>
        public virtual System.Guid OrderId { get; set; }

        /// <summary>Get or set the Tag property that maps to the Entity OrderMedia</summary>
        public virtual System.String Tag { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual MediaDTO Medium {get; set;}



		public virtual OrderDTO Order {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public OrderMediaDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 