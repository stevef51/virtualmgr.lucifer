﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'AspNetUserToken'.
    /// </summary>
    [Serializable]
    public partial class AspNetUserTokenDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the LoginProvider property that maps to the Entity AspNetUserToken</summary>
        public virtual System.String LoginProvider { get; set; }

        /// <summary>Get or set the Name property that maps to the Entity AspNetUserToken</summary>
        public virtual System.String Name { get; set; }

        /// <summary>Get or set the UserId property that maps to the Entity AspNetUserToken</summary>
        public virtual System.String UserId { get; set; }

        /// <summary>Get or set the Value property that maps to the Entity AspNetUserToken</summary>
        public virtual System.String Value { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual AspNetUserDTO AspNetUser {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public AspNetUserTokenDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 