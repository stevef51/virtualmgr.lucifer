﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'NewsFeed'.
    /// </summary>
    [Serializable]
    public partial class NewsFeedDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the Channel property that maps to the Entity NewsFeed</summary>
        public virtual System.String Channel { get; set; }

        /// <summary>Get or set the CreatedBy property that maps to the Entity NewsFeed</summary>
        public virtual System.Guid CreatedBy { get; set; }

        /// <summary>Get or set the DateCreated property that maps to the Entity NewsFeed</summary>
        public virtual System.DateTime DateCreated { get; set; }

        /// <summary>Get or set the Headline property that maps to the Entity NewsFeed</summary>
        public virtual System.String Headline { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity NewsFeed</summary>
        public virtual System.Guid Id { get; set; }

        /// <summary>Get or set the Program property that maps to the Entity NewsFeed</summary>
        public virtual System.String Program { get; set; }

        /// <summary>Get or set the Title property that maps to the Entity NewsFeed</summary>
        public virtual System.String Title { get; set; }

        /// <summary>Get or set the ValidTo property that maps to the Entity NewsFeed</summary>
        public virtual System.DateTime ValidTo { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual UserDataDTO UserData {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public NewsFeedDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 