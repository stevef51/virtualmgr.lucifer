﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'PeriodicExecutionHistory'.
    /// </summary>
    [Serializable]
    public partial class PeriodicExecutionHistoryDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the FinishUtc property that maps to the Entity PeriodicExecutionHistory</summary>
        public virtual System.DateTime? FinishUtc { get; set; }

        /// <summary>Get or set the ForceFinished property that maps to the Entity PeriodicExecutionHistory</summary>
        public virtual System.Boolean ForceFinished { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity PeriodicExecutionHistory</summary>
        public virtual System.Int32 Id { get; set; }

        /// <summary>Get or set the StartUtc property that maps to the Entity PeriodicExecutionHistory</summary>
        public virtual System.DateTime StartUtc { get; set; }
        
        #endregion

        #region Related Field Public Properties









        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public PeriodicExecutionHistoryDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 