﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'MediaFolder'.
    /// </summary>
    [Serializable]
    public partial class MediaFolderDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the AutoCreated property that maps to the Entity MediaFolder</summary>
        public virtual System.Boolean AutoCreated { get; set; }

        /// <summary>Get or set the Hidden property that maps to the Entity MediaFolder</summary>
        public virtual System.Boolean Hidden { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity MediaFolder</summary>
        public virtual System.Guid Id { get; set; }

        /// <summary>Get or set the ParentId property that maps to the Entity MediaFolder</summary>
        public virtual System.Guid? ParentId { get; set; }

        /// <summary>Get or set the ShowViaChecklistId property that maps to the Entity MediaFolder</summary>
        public virtual System.Int32? ShowViaChecklistId { get; set; }

        /// <summary>Get or set the SortOrder property that maps to the Entity MediaFolder</summary>
        public virtual System.Int32 SortOrder { get; set; }

        /// <summary>Get or set the Text property that maps to the Entity MediaFolder</summary>
        public virtual System.String Text { get; set; }

        /// <summary>Get or set the UseVersioning property that maps to the Entity MediaFolder</summary>
        public virtual System.Boolean UseVersioning { get; set; }
        
        #endregion

        #region Related Field Public Properties



		
		public virtual IList< ProjectJobTaskTypeMediaFolderDTO> ProjectJobTaskTypeMediaFolders
		{
			get; set;
		}



		
		public virtual IList< MediaFolderDTO> MediaFolders
		{
			get; set;
		}



		
		public virtual IList< MediaFolderFileDTO> MediaFolderFiles
		{
			get; set;
		}



		
		public virtual IList< MediaFolderRoleDTO> MediaFolderRoles
		{
			get; set;
		}



		
		public virtual IList< MediaFolderTranslatedDTO> Translated
		{
			get; set;
		}





		public virtual MediaFolderDTO MediaFolder {get; set;}







		public virtual MediaFolderPolicyDTO MediaFolderPolicy {get; set;}



        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public MediaFolderDTO()
        {
			
			
			this.ProjectJobTaskTypeMediaFolders = new List< ProjectJobTaskTypeMediaFolderDTO>();
			
			
			
			this.MediaFolders = new List< MediaFolderDTO>();
			
			
			
			this.MediaFolderFiles = new List< MediaFolderFileDTO>();
			
			
			
			this.MediaFolderRoles = new List< MediaFolderRoleDTO>();
			
			
			
			this.Translated = new List< MediaFolderTranslatedDTO>();
			
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 