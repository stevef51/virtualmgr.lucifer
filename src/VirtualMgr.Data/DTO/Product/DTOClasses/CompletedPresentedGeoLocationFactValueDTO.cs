﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'CompletedPresentedGeoLocationFactValue'.
    /// </summary>
    [Serializable]
    public partial class CompletedPresentedGeoLocationFactValueDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the Accuracy property that maps to the Entity CompletedPresentedGeoLocationFactValue</summary>
        public virtual System.Decimal Accuracy { get; set; }

        /// <summary>Get or set the Altitude property that maps to the Entity CompletedPresentedGeoLocationFactValue</summary>
        public virtual System.Decimal? Altitude { get; set; }

        /// <summary>Get or set the AltitudeAccuracy property that maps to the Entity CompletedPresentedGeoLocationFactValue</summary>
        public virtual System.Decimal? AltitudeAccuracy { get; set; }

        /// <summary>Get or set the CompletedWorkingDocumentPresentedFactId property that maps to the Entity CompletedPresentedGeoLocationFactValue</summary>
        public virtual System.Guid CompletedWorkingDocumentPresentedFactId { get; set; }

        /// <summary>Get or set the Heading property that maps to the Entity CompletedPresentedGeoLocationFactValue</summary>
        public virtual System.Decimal? Heading { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity CompletedPresentedGeoLocationFactValue</summary>
        public virtual System.Guid Id { get; set; }

        /// <summary>Get or set the Latitude property that maps to the Entity CompletedPresentedGeoLocationFactValue</summary>
        public virtual System.Decimal Latitude { get; set; }

        /// <summary>Get or set the Longitude property that maps to the Entity CompletedPresentedGeoLocationFactValue</summary>
        public virtual System.Decimal Longitude { get; set; }

        /// <summary>Get or set the Speed property that maps to the Entity CompletedPresentedGeoLocationFactValue</summary>
        public virtual System.Decimal? Speed { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual CompletedWorkingDocumentPresentedFactDTO CompletedWorkingDocumentPresentedFact {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public CompletedPresentedGeoLocationFactValueDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 