﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'UserContextData'.
    /// </summary>
    [Serializable]
    public partial class UserContextDataDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the CompletedWorkingDocumentId property that maps to the Entity UserContextData</summary>
        public virtual System.Guid CompletedWorkingDocumentId { get; set; }

        /// <summary>Get or set the UserId property that maps to the Entity UserContextData</summary>
        public virtual System.Guid UserId { get; set; }

        /// <summary>Get or set the UserTypeContextPublishedResourceId property that maps to the Entity UserContextData</summary>
        public virtual System.Int32 UserTypeContextPublishedResourceId { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual CompletedWorkingDocumentFactDTO CompletedWorkingDocumentFact {get; set;}



		public virtual UserDataDTO UserData {get; set;}



		public virtual UserTypeContextPublishedResourceDTO UserTypeContextPublishedResource {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public UserContextDataDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 