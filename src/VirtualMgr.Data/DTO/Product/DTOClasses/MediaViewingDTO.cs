﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'MediaViewing'.
    /// </summary>
    [Serializable]
    public partial class MediaViewingDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the Accepted property that maps to the Entity MediaViewing</summary>
        public virtual System.Boolean Accepted { get; set; }

        /// <summary>Get or set the AcceptionNotes property that maps to the Entity MediaViewing</summary>
        public virtual System.String AcceptionNotes { get; set; }

        /// <summary>Get or set the DateCompleted property that maps to the Entity MediaViewing</summary>
        public virtual System.DateTime DateCompleted { get; set; }

        /// <summary>Get or set the DateStarted property that maps to the Entity MediaViewing</summary>
        public virtual System.DateTime DateStarted { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity MediaViewing</summary>
        public virtual System.Guid Id { get; set; }

        /// <summary>Get or set the MediaId property that maps to the Entity MediaViewing</summary>
        public virtual System.Guid MediaId { get; set; }

        /// <summary>Get or set the TotalSeconds property that maps to the Entity MediaViewing</summary>
        public virtual System.Int32 TotalSeconds { get; set; }

        /// <summary>Get or set the UserId property that maps to the Entity MediaViewing</summary>
        public virtual System.Guid UserId { get; set; }

        /// <summary>Get or set the Version property that maps to the Entity MediaViewing</summary>
        public virtual System.Int32 Version { get; set; }

        /// <summary>Get or set the WorkingDocumentId property that maps to the Entity MediaViewing</summary>
        public virtual System.Guid WorkingDocumentId { get; set; }
        
        #endregion

        #region Related Field Public Properties



		
		public virtual IList< MediaPageViewingDTO> MediaPageViewings
		{
			get; set;
		}





		public virtual MediaDTO Media {get; set;}



		public virtual UserDataDTO UserData {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public MediaViewingDTO()
        {
			
			
			this.MediaPageViewings = new List< MediaPageViewingDTO>();
			
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 