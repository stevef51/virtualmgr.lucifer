﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'ProjectJobTaskTypeFacilityOverride'.
    /// </summary>
    [Serializable]
    public partial class ProjectJobTaskTypeFacilityOverrideDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the EstimatedDurationSeconds property that maps to the Entity ProjectJobTaskTypeFacilityOverride</summary>
        public virtual System.Int32? EstimatedDurationSeconds { get; set; }

        /// <summary>Get or set the FacilityStructureId property that maps to the Entity ProjectJobTaskTypeFacilityOverride</summary>
        public virtual System.Int32 FacilityStructureId { get; set; }

        /// <summary>Get or set the MinimumDuration property that maps to the Entity ProjectJobTaskTypeFacilityOverride</summary>
        public virtual System.String MinimumDuration { get; set; }

        /// <summary>Get or set the TaskTypeId property that maps to the Entity ProjectJobTaskTypeFacilityOverride</summary>
        public virtual System.Guid TaskTypeId { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual FacilityStructureDTO FacilityStructure {get; set;}



		public virtual ProjectJobTaskTypeDTO ProjectJobTaskType {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public ProjectJobTaskTypeFacilityOverrideDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 