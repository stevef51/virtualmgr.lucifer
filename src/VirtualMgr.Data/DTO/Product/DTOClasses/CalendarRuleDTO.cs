﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'CalendarRule'.
    /// </summary>
    [Serializable]
    public partial class CalendarRuleDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the Cronexpression property that maps to the Entity CalendarRule</summary>
        public virtual System.String Cronexpression { get; set; }

        /// <summary>Get or set the DateCreated property that maps to the Entity CalendarRule</summary>
        public virtual System.DateTime DateCreated { get; set; }

        /// <summary>Get or set the EndDate property that maps to the Entity CalendarRule</summary>
        public virtual System.DateTime? EndDate { get; set; }

        /// <summary>Get or set the ExecuteFriday property that maps to the Entity CalendarRule</summary>
        public virtual System.Boolean ExecuteFriday { get; set; }

        /// <summary>Get or set the ExecuteMonday property that maps to the Entity CalendarRule</summary>
        public virtual System.Boolean ExecuteMonday { get; set; }

        /// <summary>Get or set the ExecuteSaturday property that maps to the Entity CalendarRule</summary>
        public virtual System.Boolean ExecuteSaturday { get; set; }

        /// <summary>Get or set the ExecuteSunday property that maps to the Entity CalendarRule</summary>
        public virtual System.Boolean ExecuteSunday { get; set; }

        /// <summary>Get or set the ExecuteThursday property that maps to the Entity CalendarRule</summary>
        public virtual System.Boolean ExecuteThursday { get; set; }

        /// <summary>Get or set the ExecuteTuesday property that maps to the Entity CalendarRule</summary>
        public virtual System.Boolean ExecuteTuesday { get; set; }

        /// <summary>Get or set the ExecuteWednesday property that maps to the Entity CalendarRule</summary>
        public virtual System.Boolean ExecuteWednesday { get; set; }

        /// <summary>Get or set the Frequency property that maps to the Entity CalendarRule</summary>
        public virtual System.String Frequency { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity CalendarRule</summary>
        public virtual System.Int32 Id { get; set; }

        /// <summary>Get or set the Period property that maps to the Entity CalendarRule</summary>
        public virtual System.Int32 Period { get; set; }

        /// <summary>Get or set the StartDate property that maps to the Entity CalendarRule</summary>
        public virtual System.DateTime StartDate { get; set; }
        
        #endregion

        #region Related Field Public Properties



		
		public virtual IList< CalendarEventDTO> CalendarEvents
		{
			get; set;
		}



		
		public virtual IList< ProjectJobScheduledTaskDTO> ProjectJobScheduledTasks
		{
			get; set;
		}









        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public CalendarRuleDTO()
        {
			
			
			this.CalendarEvents = new List< CalendarEventDTO>();
			
			
			
			this.ProjectJobScheduledTasks = new List< ProjectJobScheduledTaskDTO>();
			
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 