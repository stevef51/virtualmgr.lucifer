﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'Company'.
    /// </summary>
    [Serializable]
    public partial class CompanyDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the CountryStateId property that maps to the Entity Company</summary>
        public virtual System.Int32? CountryStateId { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity Company</summary>
        public virtual System.Guid Id { get; set; }

        /// <summary>Get or set the Latitude property that maps to the Entity Company</summary>
        public virtual System.Decimal? Latitude { get; set; }

        /// <summary>Get or set the Longitude property that maps to the Entity Company</summary>
        public virtual System.Decimal? Longitude { get; set; }

        /// <summary>Get or set the Name property that maps to the Entity Company</summary>
        public virtual System.String Name { get; set; }

        /// <summary>Get or set the Postcode property that maps to the Entity Company</summary>
        public virtual System.String Postcode { get; set; }

        /// <summary>Get or set the Rating property that maps to the Entity Company</summary>
        public virtual System.Int32? Rating { get; set; }

        /// <summary>Get or set the Street property that maps to the Entity Company</summary>
        public virtual System.String Street { get; set; }

        /// <summary>Get or set the Town property that maps to the Entity Company</summary>
        public virtual System.String Town { get; set; }
        
        #endregion

        #region Related Field Public Properties



		
		public virtual IList< UserDataDTO> UserDatas
		{
			get; set;
		}





		public virtual CountryStateDTO CountryState {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public CompanyDTO()
        {
			
			
			this.UserDatas = new List< UserDataDTO>();
			
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 