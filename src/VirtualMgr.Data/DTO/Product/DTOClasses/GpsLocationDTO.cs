﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'GpsLocation'.
    /// </summary>
    [Serializable]
    public partial class GpsLocationDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the AccuracyMetres property that maps to the Entity GpsLocation</summary>
        public virtual System.Decimal AccuracyMetres { get; set; }

        /// <summary>Get or set the Altitude property that maps to the Entity GpsLocation</summary>
        public virtual System.Decimal? Altitude { get; set; }

        /// <summary>Get or set the AltitudeAccuracyMetres property that maps to the Entity GpsLocation</summary>
        public virtual System.Decimal? AltitudeAccuracyMetres { get; set; }

        /// <summary>Get or set the Archived property that maps to the Entity GpsLocation</summary>
        public virtual System.Boolean Archived { get; set; }

        /// <summary>Get or set the FacilityStructureFloorId property that maps to the Entity GpsLocation</summary>
        public virtual System.Int32? FacilityStructureFloorId { get; set; }

        /// <summary>Get or set the Floor property that maps to the Entity GpsLocation</summary>
        public virtual System.Int32? Floor { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity GpsLocation</summary>
        public virtual System.Int64 Id { get; set; }

        /// <summary>Get or set the LastTimestampUtc property that maps to the Entity GpsLocation</summary>
        public virtual System.DateTime? LastTimestampUtc { get; set; }

        /// <summary>Get or set the Latitude property that maps to the Entity GpsLocation</summary>
        public virtual System.Decimal Latitude { get; set; }

        /// <summary>Get or set the LocationType property that maps to the Entity GpsLocation</summary>
        public virtual System.Int32 LocationType { get; set; }

        /// <summary>Get or set the Longitude property that maps to the Entity GpsLocation</summary>
        public virtual System.Decimal Longitude { get; set; }

        /// <summary>Get or set the StaticBeaconId property that maps to the Entity GpsLocation</summary>
        public virtual System.String StaticBeaconId { get; set; }

        /// <summary>Get or set the TabletUuid property that maps to the Entity GpsLocation</summary>
        public virtual System.String TabletUuid { get; set; }

        /// <summary>Get or set the TaskId property that maps to the Entity GpsLocation</summary>
        public virtual System.Guid? TaskId { get; set; }

        /// <summary>Get or set the TimestampCount property that maps to the Entity GpsLocation</summary>
        public virtual System.Int32? TimestampCount { get; set; }

        /// <summary>Get or set the TimestampUtc property that maps to the Entity GpsLocation</summary>
        public virtual System.DateTime? TimestampUtc { get; set; }
        
        #endregion

        #region Related Field Public Properties



		
		public virtual IList< BeaconDTO> LastGpsLocationBeacons
		{
			get; set;
		}



		
		public virtual IList< BeaconDTO> StaticLocationBeacons
		{
			get; set;
		}



		
		public virtual IList< TabletLocationHistoryDTO> TabletLocationHistories
		{
			get; set;
		}









        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public GpsLocationDTO()
        {
			
			
			this.LastGpsLocationBeacons = new List< BeaconDTO>();
			
			
			
			this.StaticLocationBeacons = new List< BeaconDTO>();
			
			
			
			this.TabletLocationHistories = new List< TabletLocationHistoryDTO>();
			
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 