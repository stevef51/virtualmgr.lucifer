﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'UserLog'.
    /// </summary>
    [Serializable]
    public partial class UserLogDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the Id property that maps to the Entity UserLog</summary>
        public virtual System.Guid Id { get; set; }

        /// <summary>Get or set the Latitude property that maps to the Entity UserLog</summary>
        public virtual System.Decimal? Latitude { get; set; }

        /// <summary>Get or set the LoginMediaId property that maps to the Entity UserLog</summary>
        public virtual System.Guid? LoginMediaId { get; set; }

        /// <summary>Get or set the LogInTime property that maps to the Entity UserLog</summary>
        public virtual System.DateTime LogInTime { get; set; }

        /// <summary>Get or set the LogOutTime property that maps to the Entity UserLog</summary>
        public virtual System.DateTime? LogOutTime { get; set; }

        /// <summary>Get or set the Longitude property that maps to the Entity UserLog</summary>
        public virtual System.Decimal? Longitude { get; set; }

        /// <summary>Get or set the NearestLocation property that maps to the Entity UserLog</summary>
        public virtual System.Guid? NearestLocation { get; set; }

        /// <summary>Get or set the TabletUuid property that maps to the Entity UserLog</summary>
        public virtual System.String TabletUuid { get; set; }

        /// <summary>Get or set the UserId property that maps to the Entity UserLog</summary>
        public virtual System.Guid UserId { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual MediaDTO LoginMedia {get; set;}



		public virtual UserDataDTO UserData {get; set;}



		public virtual UserDataDTO UserData_ {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public UserLogDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 