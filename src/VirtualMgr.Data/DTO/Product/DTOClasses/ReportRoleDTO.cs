﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'ReportRole'.
    /// </summary>
    [Serializable]
    public partial class ReportRoleDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the ReportId property that maps to the Entity ReportRole</summary>
        public virtual System.Int32 ReportId { get; set; }

        /// <summary>Get or set the RoleId property that maps to the Entity ReportRole</summary>
        public virtual System.String RoleId { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual AspNetRoleDTO AspNetRole {get; set;}



		public virtual ReportDTO Report {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public ReportRoleDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 