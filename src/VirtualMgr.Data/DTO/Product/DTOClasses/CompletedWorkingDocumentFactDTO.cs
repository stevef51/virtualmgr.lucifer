﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'CompletedWorkingDocumentFact'.
    /// </summary>
    [Serializable]
    public partial class CompletedWorkingDocumentFactDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the ClientIp property that maps to the Entity CompletedWorkingDocumentFact</summary>
        public virtual System.String ClientIp { get; set; }

        /// <summary>Get or set the ContextType property that maps to the Entity CompletedWorkingDocumentFact</summary>
        public virtual System.Int32 ContextType { get; set; }

        /// <summary>Get or set the DateCompleted property that maps to the Entity CompletedWorkingDocumentFact</summary>
        public virtual System.DateTime DateCompleted { get; set; }

        /// <summary>Get or set the DateStarted property that maps to the Entity CompletedWorkingDocumentFact</summary>
        public virtual System.DateTime DateStarted { get; set; }

        /// <summary>Get or set the FailedQuestionCount property that maps to the Entity CompletedWorkingDocumentFact</summary>
        public virtual System.Int32 FailedQuestionCount { get; set; }

        /// <summary>Get or set the IsUserContext property that maps to the Entity CompletedWorkingDocumentFact</summary>
        public virtual System.Boolean IsUserContext { get; set; }

        /// <summary>Get or set the NaquestionCount property that maps to the Entity CompletedWorkingDocumentFact</summary>
        public virtual System.Int32 NaquestionCount { get; set; }

        /// <summary>Get or set the ParentWorkingDocumentId property that maps to the Entity CompletedWorkingDocumentFact</summary>
        public virtual System.Guid? ParentWorkingDocumentId { get; set; }

        /// <summary>Get or set the PassedQuestionCount property that maps to the Entity CompletedWorkingDocumentFact</summary>
        public virtual System.Int32 PassedQuestionCount { get; set; }

        /// <summary>Get or set the ReportingUserRevieweeId property that maps to the Entity CompletedWorkingDocumentFact</summary>
        public virtual System.Guid ReportingUserRevieweeId { get; set; }

        /// <summary>Get or set the ReportingUserReviewerId property that maps to the Entity CompletedWorkingDocumentFact</summary>
        public virtual System.Guid ReportingUserReviewerId { get; set; }

        /// <summary>Get or set the ReportingWorkingDocumentId property that maps to the Entity CompletedWorkingDocumentFact</summary>
        public virtual System.Guid ReportingWorkingDocumentId { get; set; }

        /// <summary>Get or set the TotalPossibleScore property that maps to the Entity CompletedWorkingDocumentFact</summary>
        public virtual System.Decimal TotalPossibleScore { get; set; }

        /// <summary>Get or set the TotalQuestionCount property that maps to the Entity CompletedWorkingDocumentFact</summary>
        public virtual System.Int32 TotalQuestionCount { get; set; }

        /// <summary>Get or set the TotalScore property that maps to the Entity CompletedWorkingDocumentFact</summary>
        public virtual System.Decimal TotalScore { get; set; }

        /// <summary>Get or set the UserAgentId property that maps to the Entity CompletedWorkingDocumentFact</summary>
        public virtual System.Int32? UserAgentId { get; set; }

        /// <summary>Get or set the WorkingDocumentId property that maps to the Entity CompletedWorkingDocumentFact</summary>
        public virtual System.Guid WorkingDocumentId { get; set; }
        
        #endregion

        #region Related Field Public Properties



		
		public virtual IList< AssetContextDataDTO> AssetContextDatas
		{
			get; set;
		}



		
		public virtual IList< CompletedLabelWorkingDocumentDimensionDTO> CompletedLabelWorkingDocumentDimensions
		{
			get; set;
		}



		
		public virtual IList< CompletedWorkingDocumentPresentedFactDTO> CompletedWorkingDocumentPresentedFacts
		{
			get; set;
		}



		
		public virtual IList< UserContextDataDTO> UserContextDatas
		{
			get; set;
		}





		public virtual CompletedUserDimensionDTO RevieweeUser {get; set;}



		public virtual CompletedUserDimensionDTO ReviewerUser {get; set;}



		public virtual CompletedWorkingDocumentDimensionDTO CompletedWorkingDocumentDimension {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public CompletedWorkingDocumentFactDTO()
        {
			
			
			this.AssetContextDatas = new List< AssetContextDataDTO>();
			
			
			
			this.CompletedLabelWorkingDocumentDimensions = new List< CompletedLabelWorkingDocumentDimensionDTO>();
			
			
			
			this.CompletedWorkingDocumentPresentedFacts = new List< CompletedWorkingDocumentPresentedFactDTO>();
			
			
			
			this.UserContextDatas = new List< UserContextDataDTO>();
			
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 