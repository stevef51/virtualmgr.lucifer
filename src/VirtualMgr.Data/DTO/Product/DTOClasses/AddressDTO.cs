﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'Address'.
    /// </summary>
    [Serializable]
    public partial class AddressDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the CountryCode property that maps to the Entity Address</summary>
        public virtual System.String CountryCode { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity Address</summary>
        public virtual System.Int32 Id { get; set; }

        /// <summary>Get or set the Line1 property that maps to the Entity Address</summary>
        public virtual System.String Line1 { get; set; }

        /// <summary>Get or set the Line2 property that maps to the Entity Address</summary>
        public virtual System.String Line2 { get; set; }

        /// <summary>Get or set the Line3 property that maps to the Entity Address</summary>
        public virtual System.String Line3 { get; set; }

        /// <summary>Get or set the Line4 property that maps to the Entity Address</summary>
        public virtual System.String Line4 { get; set; }

        /// <summary>Get or set the Locality property that maps to the Entity Address</summary>
        public virtual System.String Locality { get; set; }

        /// <summary>Get or set the Region property that maps to the Entity Address</summary>
        public virtual System.String Region { get; set; }

        /// <summary>Get or set the ZipCode property that maps to the Entity Address</summary>
        public virtual System.String ZipCode { get; set; }
        
        #endregion

        #region Related Field Public Properties



		
		public virtual IList< FacilityStructureDTO> FacilityStructures
		{
			get; set;
		}









        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public AddressDTO()
        {
			
			
			this.FacilityStructures = new List< FacilityStructureDTO>();
			
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 