﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'Resource'.
    /// </summary>
    [Serializable]
    public partial class ResourceDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the Id property that maps to the Entity Resource</summary>
        public virtual System.Int32 Id { get; set; }

        /// <summary>Get or set the Name property that maps to the Entity Resource</summary>
        public virtual System.String Name { get; set; }

        /// <summary>Get or set the NodeId property that maps to the Entity Resource</summary>
        public virtual System.Guid NodeId { get; set; }

        /// <summary>Get or set the Type property that maps to the Entity Resource</summary>
        public virtual System.String Type { get; set; }
        
        #endregion

        #region Related Field Public Properties



		
		public virtual IList< LabelResourceDTO> LabelResources
		{
			get; set;
		}



		
		public virtual IList< PublishingGroupResourceDTO> PublishingGroupResources
		{
			get; set;
		}









		public virtual ResourceDataDTO ResourceData {get; set;}



        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public ResourceDTO()
        {
			
			
			this.LabelResources = new List< LabelResourceDTO>();
			
			
			
			this.PublishingGroupResources = new List< PublishingGroupResourceDTO>();
			
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 