﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'InvoiceLineItem'.
    /// </summary>
    [Serializable]
    public partial class InvoiceLineItemDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the Description property that maps to the Entity InvoiceLineItem</summary>
        public virtual System.String Description { get; set; }

        /// <summary>Get or set the DetailLevel property that maps to the Entity InvoiceLineItem</summary>
        public virtual System.Int32 DetailLevel { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity InvoiceLineItem</summary>
        public virtual System.Int32 Id { get; set; }

        /// <summary>Get or set the InvoiceId property that maps to the Entity InvoiceLineItem</summary>
        public virtual System.String InvoiceId { get; set; }

        /// <summary>Get or set the Line property that maps to the Entity InvoiceLineItem</summary>
        public virtual System.String Line { get; set; }

        /// <summary>Get or set the ParentLine property that maps to the Entity InvoiceLineItem</summary>
        public virtual System.String ParentLine { get; set; }

        /// <summary>Get or set the Quantity property that maps to the Entity InvoiceLineItem</summary>
        public virtual System.Decimal Quantity { get; set; }

        /// <summary>Get or set the Total property that maps to the Entity InvoiceLineItem</summary>
        public virtual System.Decimal Total { get; set; }

        /// <summary>Get or set the UnitPrice property that maps to the Entity InvoiceLineItem</summary>
        public virtual System.Decimal UnitPrice { get; set; }

        /// <summary>Get or set the UserId property that maps to the Entity InvoiceLineItem</summary>
        public virtual System.Guid? UserId { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual InvoiceDTO Invoice {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public InvoiceLineItemDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 