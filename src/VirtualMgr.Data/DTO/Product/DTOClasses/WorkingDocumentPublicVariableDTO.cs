﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'WorkingDocumentPublicVariable'.
    /// </summary>
    [Serializable]
    public partial class WorkingDocumentPublicVariableDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the Id property that maps to the Entity WorkingDocumentPublicVariable</summary>
        public virtual System.Guid Id { get; set; }

        /// <summary>Get or set the Name property that maps to the Entity WorkingDocumentPublicVariable</summary>
        public virtual System.String Name { get; set; }

        /// <summary>Get or set the PassFail property that maps to the Entity WorkingDocumentPublicVariable</summary>
        public virtual System.Boolean? PassFail { get; set; }

        /// <summary>Get or set the Score property that maps to the Entity WorkingDocumentPublicVariable</summary>
        public virtual System.Decimal? Score { get; set; }

        /// <summary>Get or set the Value property that maps to the Entity WorkingDocumentPublicVariable</summary>
        public virtual System.String Value { get; set; }

        /// <summary>Get or set the WorkingDocumentId property that maps to the Entity WorkingDocumentPublicVariable</summary>
        public virtual System.Guid WorkingDocumentId { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual WorkingDocumentDTO WorkingDocument {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public WorkingDocumentPublicVariableDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 