﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'RosterEventConditionAction'.
    /// </summary>
    [Serializable]
    public partial class RosterEventConditionActionDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the Action property that maps to the Entity RosterEventConditionAction</summary>
        public virtual System.String Action { get; set; }

        /// <summary>Get or set the Data property that maps to the Entity RosterEventConditionAction</summary>
        public virtual System.String Data { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity RosterEventConditionAction</summary>
        public virtual System.Int32 Id { get; set; }

        /// <summary>Get or set the RosterEventConditionId property that maps to the Entity RosterEventConditionAction</summary>
        public virtual System.Int32 RosterEventConditionId { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual RosterEventConditionDTO RosterEventCondition {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public RosterEventConditionActionDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 