﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'ProductCatalogFacilityStructureRule'.
    /// </summary>
    [Serializable]
    public partial class ProductCatalogFacilityStructureRuleDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the Exclude property that maps to the Entity ProductCatalogFacilityStructureRule</summary>
        public virtual System.Boolean Exclude { get; set; }

        /// <summary>Get or set the FacilityStructureId property that maps to the Entity ProductCatalogFacilityStructureRule</summary>
        public virtual System.Int32 FacilityStructureId { get; set; }

        /// <summary>Get or set the ProductCatalogId property that maps to the Entity ProductCatalogFacilityStructureRule</summary>
        public virtual System.Int32 ProductCatalogId { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual FacilityStructureDTO FacilityStructure {get; set;}



		public virtual ProductCatalogDTO ProductCatalog {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public ProductCatalogFacilityStructureRuleDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 