﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'TabletLocationHistory'.
    /// </summary>
    [Serializable]
    public partial class TabletLocationHistoryDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the GpslocationId property that maps to the Entity TabletLocationHistory</summary>
        public virtual System.Int64? GpslocationId { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity TabletLocationHistory</summary>
        public virtual System.Int64 Id { get; set; }

        /// <summary>Get or set the LoggedInUserId property that maps to the Entity TabletLocationHistory</summary>
        public virtual System.Guid? LoggedInUserId { get; set; }

        /// <summary>Get or set the Range property that maps to the Entity TabletLocationHistory</summary>
        public virtual System.Int32? Range { get; set; }

        /// <summary>Get or set the RangeUtc property that maps to the Entity TabletLocationHistory</summary>
        public virtual System.DateTime? RangeUtc { get; set; }

        /// <summary>Get or set the ServerTimestampUtc property that maps to the Entity TabletLocationHistory</summary>
        public virtual System.DateTime ServerTimestampUtc { get; set; }

        /// <summary>Get or set the TabletTimestampUtc property that maps to the Entity TabletLocationHistory</summary>
        public virtual System.DateTime TabletTimestampUtc { get; set; }

        /// <summary>Get or set the TabletUuid property that maps to the Entity TabletLocationHistory</summary>
        public virtual System.String TabletUuid { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual GpsLocationDTO Gpslocation {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public TabletLocationHistoryDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 