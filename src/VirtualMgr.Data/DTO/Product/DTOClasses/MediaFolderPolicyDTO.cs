﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'MediaFolderPolicy'.
    /// </summary>
    [Serializable]
    public partial class MediaFolderPolicyDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the Data property that maps to the Entity MediaFolderPolicy</summary>
        public virtual System.String Data { get; set; }

        /// <summary>Get or set the MediaFolderId property that maps to the Entity MediaFolderPolicy</summary>
        public virtual System.Guid MediaFolderId { get; set; }
        
        #endregion

        #region Related Field Public Properties









		public virtual MediaFolderDTO MediaFolder {get; set;}



        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public MediaFolderPolicyDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 