﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'UserBillingType'.
    /// </summary>
    [Serializable]
    public partial class UserBillingTypeDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the AmountPerMonth property that maps to the Entity UserBillingType</summary>
        public virtual System.Decimal AmountPerMonth { get; set; }

        /// <summary>Get or set the Archived property that maps to the Entity UserBillingType</summary>
        public virtual System.Boolean Archived { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity UserBillingType</summary>
        public virtual System.Int32 Id { get; set; }

        /// <summary>Get or set the Name property that maps to the Entity UserBillingType</summary>
        public virtual System.String Name { get; set; }
        
        #endregion

        #region Related Field Public Properties



		
		public virtual IList< UserTypeDTO> UserTypes
		{
			get; set;
		}









        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public UserBillingTypeDTO()
        {
			
			
			this.UserTypes = new List< UserTypeDTO>();
			
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 