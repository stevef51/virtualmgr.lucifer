﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'Jsfunction'.
    /// </summary>
    [Serializable]
    public partial class JsfunctionDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the Archived property that maps to the Entity Jsfunction</summary>
        public virtual System.Boolean Archived { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity Jsfunction</summary>
        public virtual System.Int32 Id { get; set; }

        /// <summary>Get or set the Name property that maps to the Entity Jsfunction</summary>
        public virtual System.String Name { get; set; }

        /// <summary>Get or set the Script property that maps to the Entity Jsfunction</summary>
        public virtual System.String Script { get; set; }

        /// <summary>Get or set the Type property that maps to the Entity Jsfunction</summary>
        public virtual System.Int32 Type { get; set; }
        
        #endregion

        #region Related Field Public Properties



		
		public virtual IList< ProductDTO> Products
		{
			get; set;
		}



		
		public virtual IList< ProductCatalogItemDTO> ProductCatalogItems
		{
			get; set;
		}









        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public JsfunctionDTO()
        {
			
			
			this.Products = new List< ProductDTO>();
			
			
			
			this.ProductCatalogItems = new List< ProductCatalogItemDTO>();
			
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 