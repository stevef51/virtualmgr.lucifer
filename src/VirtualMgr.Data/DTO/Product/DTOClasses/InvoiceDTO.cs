﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'Invoice'.
    /// </summary>
    [Serializable]
    public partial class InvoiceDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the BillingAddress property that maps to the Entity Invoice</summary>
        public virtual System.String BillingAddress { get; set; }

        /// <summary>Get or set the BillingCompanyName property that maps to the Entity Invoice</summary>
        public virtual System.String BillingCompanyName { get; set; }

        /// <summary>Get or set the Currency property that maps to the Entity Invoice</summary>
        public virtual System.String Currency { get; set; }

        /// <summary>Get or set the Description property that maps to the Entity Invoice</summary>
        public virtual System.String Description { get; set; }

        /// <summary>Get or set the FromDateUtc property that maps to the Entity Invoice</summary>
        public virtual System.DateTimeOffset FromDateUtc { get; set; }

        /// <summary>Get or set the GeneratedByUserId property that maps to the Entity Invoice</summary>
        public virtual System.Guid? GeneratedByUserId { get; set; }

        /// <summary>Get or set the GeneratedDateUtc property that maps to the Entity Invoice</summary>
        public virtual System.DateTimeOffset GeneratedDateUtc { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity Invoice</summary>
        public virtual System.String Id { get; set; }

        /// <summary>Get or set the IssuedDateUtc property that maps to the Entity Invoice</summary>
        public virtual System.DateTimeOffset? IssuedDateUtc { get; set; }

        /// <summary>Get or set the PaymentDateUtc property that maps to the Entity Invoice</summary>
        public virtual System.DateTimeOffset? PaymentDateUtc { get; set; }

        /// <summary>Get or set the PaymentReference property that maps to the Entity Invoice</summary>
        public virtual System.String PaymentReference { get; set; }

        /// <summary>Get or set the ToDateUtc property that maps to the Entity Invoice</summary>
        public virtual System.DateTimeOffset ToDateUtc { get; set; }

        /// <summary>Get or set the TotalAmount property that maps to the Entity Invoice</summary>
        public virtual System.Decimal TotalAmount { get; set; }
        
        #endregion

        #region Related Field Public Properties



		
		public virtual IList< InvoiceLineItemDTO> InvoiceLineItems
		{
			get; set;
		}









        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public InvoiceDTO()
        {
			
			
			this.InvoiceLineItems = new List< InvoiceLineItemDTO>();
			
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 