﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'PluginData'.
    /// </summary>
    [Serializable]
    public partial class PluginDataDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the Data property that maps to the Entity PluginData</summary>
        public virtual System.Byte[] Data { get; set; }

        /// <summary>Get or set the PluginId property that maps to the Entity PluginData</summary>
        public virtual System.Guid PluginId { get; set; }
        
        #endregion

        #region Related Field Public Properties









		public virtual PluginDTO Plugin {get; set;}



        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public PluginDataDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 