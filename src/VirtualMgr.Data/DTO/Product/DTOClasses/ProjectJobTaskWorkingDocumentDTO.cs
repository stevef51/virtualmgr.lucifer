﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'ProjectJobTaskWorkingDocument'.
    /// </summary>
    [Serializable]
    public partial class ProjectJobTaskWorkingDocumentDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the ProjectJobTaskId property that maps to the Entity ProjectJobTaskWorkingDocument</summary>
        public virtual System.Guid ProjectJobTaskId { get; set; }

        /// <summary>Get or set the PublishingGroupResourceId property that maps to the Entity ProjectJobTaskWorkingDocument</summary>
        public virtual System.Int32 PublishingGroupResourceId { get; set; }

        /// <summary>Get or set the WorkingDocumentId property that maps to the Entity ProjectJobTaskWorkingDocument</summary>
        public virtual System.Guid WorkingDocumentId { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual ProjectJobTaskDTO ProjectJobTask {get; set;}



		public virtual PublishingGroupResourceDTO PublishingGroupResource {get; set;}



		public virtual WorkingDocumentDTO WorkingDocument {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public ProjectJobTaskWorkingDocumentDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 