﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'ProjectJobTaskTypeStatus'.
    /// </summary>
    [Serializable]
    public partial class ProjectJobTaskTypeStatusDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the DefaultToWhenCreated property that maps to the Entity ProjectJobTaskTypeStatus</summary>
        public virtual System.Boolean DefaultToWhenCreated { get; set; }

        /// <summary>Get or set the ReferenceNumberMode property that maps to the Entity ProjectJobTaskTypeStatus</summary>
        public virtual System.Int32 ReferenceNumberMode { get; set; }

        /// <summary>Get or set the SetToWhenFinished property that maps to the Entity ProjectJobTaskTypeStatus</summary>
        public virtual System.Boolean SetToWhenFinished { get; set; }

        /// <summary>Get or set the SortOrder property that maps to the Entity ProjectJobTaskTypeStatus</summary>
        public virtual System.Int32 SortOrder { get; set; }

        /// <summary>Get or set the StatusId property that maps to the Entity ProjectJobTaskTypeStatus</summary>
        public virtual System.Guid StatusId { get; set; }

        /// <summary>Get or set the TaskTypeId property that maps to the Entity ProjectJobTaskTypeStatus</summary>
        public virtual System.Guid TaskTypeId { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual ProjectJobStatusDTO ProjectJobStatu {get; set;}



		public virtual ProjectJobTaskTypeDTO ProjectJobTaskType {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public ProjectJobTaskTypeStatusDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 