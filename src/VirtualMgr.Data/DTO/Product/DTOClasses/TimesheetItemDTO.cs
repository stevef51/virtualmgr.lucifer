﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'TimesheetItem'.
    /// </summary>
    [Serializable]
    public partial class TimesheetItemDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the ActualDuration property that maps to the Entity TimesheetItem</summary>
        public virtual System.Decimal? ActualDuration { get; set; }

        /// <summary>Get or set the ActualEndDate property that maps to the Entity TimesheetItem</summary>
        public virtual System.DateTime ActualEndDate { get; set; }

        /// <summary>Get or set the ActualStartDate property that maps to the Entity TimesheetItem</summary>
        public virtual System.DateTime ActualStartDate { get; set; }

        /// <summary>Get or set the ApprovedByUserId property that maps to the Entity TimesheetItem</summary>
        public virtual System.Guid ApprovedByUserId { get; set; }

        /// <summary>Get or set the ApprovedDuration property that maps to the Entity TimesheetItem</summary>
        public virtual System.Decimal ApprovedDuration { get; set; }

        /// <summary>Get or set the ApprovedEndDate property that maps to the Entity TimesheetItem</summary>
        public virtual System.DateTime ApprovedEndDate { get; set; }

        /// <summary>Get or set the ApprovedStartDate property that maps to the Entity TimesheetItem</summary>
        public virtual System.DateTime ApprovedStartDate { get; set; }

        /// <summary>Get or set the Date property that maps to the Entity TimesheetItem</summary>
        public virtual System.DateTime Date { get; set; }

        /// <summary>Get or set the DateCreated property that maps to the Entity TimesheetItem</summary>
        public virtual System.DateTime DateCreated { get; set; }

        /// <summary>Get or set the ExportedDate property that maps to the Entity TimesheetItem</summary>
        public virtual System.DateTime? ExportedDate { get; set; }

        /// <summary>Get or set the HierarchyBucketOverrideTypeId property that maps to the Entity TimesheetItem</summary>
        public virtual System.Int32? HierarchyBucketOverrideTypeId { get; set; }

        /// <summary>Get or set the HierarchyRoleId property that maps to the Entity TimesheetItem</summary>
        public virtual System.Int32? HierarchyRoleId { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity TimesheetItem</summary>
        public virtual System.Int32 Id { get; set; }

        /// <summary>Get or set the IsLunchPaid property that maps to the Entity TimesheetItem</summary>
        public virtual System.Boolean IsLunchPaid { get; set; }

        /// <summary>Get or set the LunchDuration property that maps to the Entity TimesheetItem</summary>
        public virtual System.Decimal? LunchDuration { get; set; }

        /// <summary>Get or set the PayAsTimesheetItemTypeId property that maps to the Entity TimesheetItem</summary>
        public virtual System.Int32? PayAsTimesheetItemTypeId { get; set; }

        /// <summary>Get or set the RosterId property that maps to the Entity TimesheetItem</summary>
        public virtual System.Int32 RosterId { get; set; }

        /// <summary>Get or set the ScheduledDuration property that maps to the Entity TimesheetItem</summary>
        public virtual System.Decimal? ScheduledDuration { get; set; }

        /// <summary>Get or set the ScheduledEndDate property that maps to the Entity TimesheetItem</summary>
        public virtual System.DateTime? ScheduledEndDate { get; set; }

        /// <summary>Get or set the ScheduledLunchEndDate property that maps to the Entity TimesheetItem</summary>
        public virtual System.DateTime? ScheduledLunchEndDate { get; set; }

        /// <summary>Get or set the ScheduledLunchStartDate property that maps to the Entity TimesheetItem</summary>
        public virtual System.DateTime? ScheduledLunchStartDate { get; set; }

        /// <summary>Get or set the ScheduledStartDate property that maps to the Entity TimesheetItem</summary>
        public virtual System.DateTime? ScheduledStartDate { get; set; }

        /// <summary>Get or set the TimesheetItemTypeId property that maps to the Entity TimesheetItem</summary>
        public virtual System.Int32 TimesheetItemTypeId { get; set; }

        /// <summary>Get or set the UserId property that maps to the Entity TimesheetItem</summary>
        public virtual System.Guid UserId { get; set; }
        
        #endregion

        #region Related Field Public Properties



		
		public virtual IList< TimesheetedTaskWorkLogDTO> TimesheetedTaskWorkLogs
		{
			get; set;
		}





		public virtual RosterDTO Roster {get; set;}



		public virtual TimesheetItemTypeDTO TimesheetItemType {get; set;}



		public virtual TimesheetItemTypeDTO TimesheetItemType_ {get; set;}



		public virtual HierarchyBucketOverrideTypeDTO HierarchyBucketOverrideType {get; set;}



		public virtual HierarchyRoleDTO HierarchyRole {get; set;}



		public virtual UserDataDTO UserData {get; set;}



		public virtual UserDataDTO UserData_ {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public TimesheetItemDTO()
        {
			
			
			this.TimesheetedTaskWorkLogs = new List< TimesheetedTaskWorkLogDTO>();
			
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 