﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'ErrorLogItem'.
    /// </summary>
    [Serializable]
    public partial class ErrorLogItemDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the Data property that maps to the Entity ErrorLogItem</summary>
        public virtual System.String Data { get; set; }

        /// <summary>Get or set the DateCreated property that maps to the Entity ErrorLogItem</summary>
        public virtual System.DateTime DateCreated { get; set; }

        /// <summary>Get or set the ErrorLogTypeId property that maps to the Entity ErrorLogItem</summary>
        public virtual System.Int32 ErrorLogTypeId { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity ErrorLogItem</summary>
        public virtual System.Int32 Id { get; set; }

        /// <summary>Get or set the UserAgentId property that maps to the Entity ErrorLogItem</summary>
        public virtual System.Int32? UserAgentId { get; set; }

        /// <summary>Get or set the UserId property that maps to the Entity ErrorLogItem</summary>
        public virtual System.Guid? UserId { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual ErrorLogTypeDTO ErrorLogType {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public ErrorLogItemDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 