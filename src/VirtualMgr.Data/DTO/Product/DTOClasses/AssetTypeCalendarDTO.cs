﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'AssetTypeCalendar'.
    /// </summary>
    [Serializable]
    public partial class AssetTypeCalendarDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the Archived property that maps to the Entity AssetTypeCalendar</summary>
        public virtual System.Boolean Archived { get; set; }

        /// <summary>Get or set the AssetTypeId property that maps to the Entity AssetTypeCalendar</summary>
        public virtual System.Int32 AssetTypeId { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity AssetTypeCalendar</summary>
        public virtual System.Int32 Id { get; set; }

        /// <summary>Get or set the Name property that maps to the Entity AssetTypeCalendar</summary>
        public virtual System.String Name { get; set; }

        /// <summary>Get or set the ScheduleExpression property that maps to the Entity AssetTypeCalendar</summary>
        public virtual System.String ScheduleExpression { get; set; }

        /// <summary>Get or set the TimeZone property that maps to the Entity AssetTypeCalendar</summary>
        public virtual System.String TimeZone { get; set; }
        
        #endregion

        #region Related Field Public Properties



		
		public virtual IList< AssetScheduleDTO> AssetSchedules
		{
			get; set;
		}



		
		public virtual IList< AssetTypeCalendarTaskDTO> AssetTypeCalendarTasks
		{
			get; set;
		}





		public virtual AssetTypeDTO AssetType {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public AssetTypeCalendarDTO()
        {
			
			
			this.AssetSchedules = new List< AssetScheduleDTO>();
			
			
			
			this.AssetTypeCalendarTasks = new List< AssetTypeCalendarTaskDTO>();
			
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 