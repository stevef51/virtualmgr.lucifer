﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'ProjectJobTaskTypePublishingGroupResource'.
    /// </summary>
    [Serializable]
    public partial class ProjectJobTaskTypePublishingGroupResourceDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the AllowMultiple property that maps to the Entity ProjectJobTaskTypePublishingGroupResource</summary>
        public virtual System.Boolean AllowMultiple { get; set; }

        /// <summary>Get or set the Mandatory property that maps to the Entity ProjectJobTaskTypePublishingGroupResource</summary>
        public virtual System.Boolean Mandatory { get; set; }

        /// <summary>Get or set the PreventFinishing property that maps to the Entity ProjectJobTaskTypePublishingGroupResource</summary>
        public virtual System.Boolean PreventFinishing { get; set; }

        /// <summary>Get or set the PublishingGroupResourceId property that maps to the Entity ProjectJobTaskTypePublishingGroupResource</summary>
        public virtual System.Int32 PublishingGroupResourceId { get; set; }

        /// <summary>Get or set the TaskTypeId property that maps to the Entity ProjectJobTaskTypePublishingGroupResource</summary>
        public virtual System.Guid TaskTypeId { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual ProjectJobTaskTypeDTO ProjectJobTaskType {get; set;}



		public virtual PublishingGroupResourceDTO PublishingGroupResource {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public ProjectJobTaskTypePublishingGroupResourceDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 