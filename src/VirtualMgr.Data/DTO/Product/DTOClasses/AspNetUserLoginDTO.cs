﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'AspNetUserLogin'.
    /// </summary>
    [Serializable]
    public partial class AspNetUserLoginDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the LoginProvider property that maps to the Entity AspNetUserLogin</summary>
        public virtual System.String LoginProvider { get; set; }

        /// <summary>Get or set the ProviderDisplayName property that maps to the Entity AspNetUserLogin</summary>
        public virtual System.String ProviderDisplayName { get; set; }

        /// <summary>Get or set the ProviderKey property that maps to the Entity AspNetUserLogin</summary>
        public virtual System.String ProviderKey { get; set; }

        /// <summary>Get or set the UserId property that maps to the Entity AspNetUserLogin</summary>
        public virtual System.String UserId { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual AspNetUserDTO AspNetUser {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public AspNetUserLoginDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 