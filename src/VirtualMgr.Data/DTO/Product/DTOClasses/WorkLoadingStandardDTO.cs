﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'WorkLoadingStandard'.
    /// </summary>
    [Serializable]
    public partial class WorkLoadingStandardDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the ActivityId property that maps to the Entity WorkLoadingStandard</summary>
        public virtual System.Guid ActivityId { get; set; }

        /// <summary>Get or set the Archived property that maps to the Entity WorkLoadingStandard</summary>
        public virtual System.Boolean Archived { get; set; }

        /// <summary>Get or set the BookId property that maps to the Entity WorkLoadingStandard</summary>
        public virtual System.Guid BookId { get; set; }

        /// <summary>Get or set the FeatureId property that maps to the Entity WorkLoadingStandard</summary>
        public virtual System.Guid FeatureId { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity WorkLoadingStandard</summary>
        public virtual System.Guid Id { get; set; }

        /// <summary>Get or set the Measure property that maps to the Entity WorkLoadingStandard</summary>
        public virtual System.Decimal Measure { get; set; }

        /// <summary>Get or set the Seconds property that maps to the Entity WorkLoadingStandard</summary>
        public virtual System.Decimal Seconds { get; set; }

        /// <summary>Get or set the UnitId property that maps to the Entity WorkLoadingStandard</summary>
        public virtual System.Int32 UnitId { get; set; }
        
        #endregion

        #region Related Field Public Properties



		
		public virtual IList< ProjectJobScheduledTaskWorkloadingActivityDTO> ProjectJobScheduledTaskWorkloadingActivities
		{
			get; set;
		}



		
		public virtual IList< ProjectJobTaskWorkloadingActivityDTO> ProjectJobTaskWorkloadingActivities
		{
			get; set;
		}





		public virtual MeasurementUnitDTO MeasurementUnit {get; set;}



		public virtual WorkLoadingActivityDTO WorkLoadingActivity {get; set;}



		public virtual WorkLoadingBookDTO WorkLoadingBook {get; set;}



		public virtual WorkLoadingFeatureDTO WorkLoadingFeature {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public WorkLoadingStandardDTO()
        {
			
			
			this.ProjectJobScheduledTaskWorkloadingActivities = new List< ProjectJobScheduledTaskWorkloadingActivityDTO>();
			
			
			
			this.ProjectJobTaskWorkloadingActivities = new List< ProjectJobTaskWorkloadingActivityDTO>();
			
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 