﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'ProductCatalogItem'.
    /// </summary>
    [Serializable]
    public partial class ProductCatalogItemDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the MinStockLevel property that maps to the Entity ProductCatalogItem</summary>
        public virtual System.Decimal? MinStockLevel { get; set; }

        /// <summary>Get or set the Price property that maps to the Entity ProductCatalogItem</summary>
        public virtual System.Decimal? Price { get; set; }

        /// <summary>Get or set the PriceJsFunctionId property that maps to the Entity ProductCatalogItem</summary>
        public virtual System.Int32? PriceJsFunctionId { get; set; }

        /// <summary>Get or set the ProductCatalogId property that maps to the Entity ProductCatalogItem</summary>
        public virtual System.Int32 ProductCatalogId { get; set; }

        /// <summary>Get or set the ProductId property that maps to the Entity ProductCatalogItem</summary>
        public virtual System.Int32 ProductId { get; set; }

        /// <summary>Get or set the SortOrder property that maps to the Entity ProductCatalogItem</summary>
        public virtual System.Int32 SortOrder { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual JsfunctionDTO Jsfunction {get; set;}



		public virtual ProductDTO Product {get; set;}



		public virtual ProductCatalogDTO ProductCatalog {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public ProductCatalogItemDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 