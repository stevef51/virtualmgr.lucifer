﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'OrderItem'.
    /// </summary>
    [Serializable]
    public partial class OrderItemDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the EndDate property that maps to the Entity OrderItem</summary>
        public virtual System.DateTime? EndDate { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity OrderItem</summary>
        public virtual System.Guid Id { get; set; }

        /// <summary>Get or set the MinStockLevel property that maps to the Entity OrderItem</summary>
        public virtual System.Decimal? MinStockLevel { get; set; }

        /// <summary>Get or set the Notes property that maps to the Entity OrderItem</summary>
        public virtual System.String Notes { get; set; }

        /// <summary>Get or set the OrderId property that maps to the Entity OrderItem</summary>
        public virtual System.Guid OrderId { get; set; }

        /// <summary>Get or set the Price property that maps to the Entity OrderItem</summary>
        public virtual System.Decimal? Price { get; set; }

        /// <summary>Get or set the ProductId property that maps to the Entity OrderItem</summary>
        public virtual System.Int32 ProductId { get; set; }

        /// <summary>Get or set the Quantity property that maps to the Entity OrderItem</summary>
        public virtual System.Decimal Quantity { get; set; }

        /// <summary>Get or set the StartDate property that maps to the Entity OrderItem</summary>
        public virtual System.DateTime? StartDate { get; set; }

        /// <summary>Get or set the Stock property that maps to the Entity OrderItem</summary>
        public virtual System.Decimal? Stock { get; set; }

        /// <summary>Get or set the TotalPrice property that maps to the Entity OrderItem</summary>
        public virtual System.Decimal? TotalPrice { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual OrderDTO Order {get; set;}



		public virtual ProductDTO Product {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public OrderItemDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 