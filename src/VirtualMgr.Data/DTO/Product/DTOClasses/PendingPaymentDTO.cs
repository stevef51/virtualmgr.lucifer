﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'PendingPayment'.
    /// </summary>
    [Serializable]
    public partial class PendingPaymentDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the ActivatedDateUtc property that maps to the Entity PendingPayment</summary>
        public virtual System.DateTime? ActivatedDateUtc { get; set; }

        /// <summary>Get or set the Archived property that maps to the Entity PendingPayment</summary>
        public virtual System.Boolean Archived { get; set; }

        /// <summary>Get or set the CancelledDateUtc property that maps to the Entity PendingPayment</summary>
        public virtual System.DateTime? CancelledDateUtc { get; set; }

        /// <summary>Get or set the CreatedDateUtc property that maps to the Entity PendingPayment</summary>
        public virtual System.DateTime CreatedDateUtc { get; set; }

        /// <summary>Get or set the Currency property that maps to the Entity PendingPayment</summary>
        public virtual System.String Currency { get; set; }

        /// <summary>Get or set the Description property that maps to the Entity PendingPayment</summary>
        public virtual System.String Description { get; set; }

        /// <summary>Get or set the DiscountAmount property that maps to the Entity PendingPayment</summary>
        public virtual System.Decimal DiscountAmount { get; set; }

        /// <summary>Get or set the DueAmount property that maps to the Entity PendingPayment</summary>
        public virtual System.Decimal DueAmount { get; set; }

        /// <summary>Get or set the DueDateUtc property that maps to the Entity PendingPayment</summary>
        public virtual System.DateTime? DueDateUtc { get; set; }

        /// <summary>Get or set the FromUtc property that maps to the Entity PendingPayment</summary>
        public virtual System.DateTime? FromUtc { get; set; }

        /// <summary>Get or set the FullAmount property that maps to the Entity PendingPayment</summary>
        public virtual System.Decimal FullAmount { get; set; }

        /// <summary>Get or set the GatewayContext property that maps to the Entity PendingPayment</summary>
        public virtual System.String GatewayContext { get; set; }

        /// <summary>Get or set the GatewayName property that maps to the Entity PendingPayment</summary>
        public virtual System.String GatewayName { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity PendingPayment</summary>
        public virtual System.Guid Id { get; set; }

        /// <summary>Get or set the InvoiceNumber property that maps to the Entity PendingPayment</summary>
        public virtual System.Int32? InvoiceNumber { get; set; }

        /// <summary>Get or set the LastFailedDateUtc property that maps to the Entity PendingPayment</summary>
        public virtual System.DateTime? LastFailedDateUtc { get; set; }

        /// <summary>Get or set the OrderBy property that maps to the Entity PendingPayment</summary>
        public virtual System.Int32 OrderBy { get; set; }

        /// <summary>Get or set the OrderId property that maps to the Entity PendingPayment</summary>
        public virtual System.Guid OrderId { get; set; }

        /// <summary>Get or set the PaidInFullDateUtc property that maps to the Entity PendingPayment</summary>
        public virtual System.DateTime? PaidInFullDateUtc { get; set; }

        /// <summary>Get or set the Quantity property that maps to the Entity PendingPayment</summary>
        public virtual System.Decimal? Quantity { get; set; }

        /// <summary>Get or set the Sandbox property that maps to the Entity PendingPayment</summary>
        public virtual System.Boolean Sandbox { get; set; }

        /// <summary>Get or set the Status property that maps to the Entity PendingPayment</summary>
        public virtual System.Int32 Status { get; set; }

        /// <summary>Get or set the StatusDateUtc property that maps to the Entity PendingPayment</summary>
        public virtual System.DateTime StatusDateUtc { get; set; }

        /// <summary>Get or set the ToUtc property that maps to the Entity PendingPayment</summary>
        public virtual System.DateTime? ToUtc { get; set; }

        /// <summary>Get or set the UserId property that maps to the Entity PendingPayment</summary>
        public virtual System.Guid UserId { get; set; }
        
        #endregion

        #region Related Field Public Properties



		
		public virtual IList< PaymentResponseDTO> PaymentResponses
		{
			get; set;
		}





		public virtual UserDataDTO UserData {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public PendingPaymentDTO()
        {
			
			
			this.PaymentResponses = new List< PaymentResponseDTO>();
			
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 