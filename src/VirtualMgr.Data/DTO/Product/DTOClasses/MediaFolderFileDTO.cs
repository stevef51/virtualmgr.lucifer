﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'MediaFolderFile'.
    /// </summary>
    [Serializable]
    public partial class MediaFolderFileDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the AutoCreated property that maps to the Entity MediaFolderFile</summary>
        public virtual System.Boolean AutoCreated { get; set; }

        /// <summary>Get or set the Hidden property that maps to the Entity MediaFolderFile</summary>
        public virtual System.Boolean Hidden { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity MediaFolderFile</summary>
        public virtual System.Guid Id { get; set; }

        /// <summary>Get or set the MediaFolderId property that maps to the Entity MediaFolderFile</summary>
        public virtual System.Guid MediaFolderId { get; set; }

        /// <summary>Get or set the MediaId property that maps to the Entity MediaFolderFile</summary>
        public virtual System.Guid MediaId { get; set; }

        /// <summary>Get or set the ShowViaChecklistId property that maps to the Entity MediaFolderFile</summary>
        public virtual System.Int32? ShowViaChecklistId { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual MediaDTO Medium {get; set;}



		public virtual MediaFolderDTO MediaFolder {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public MediaFolderFileDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 