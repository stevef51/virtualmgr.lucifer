﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'AspNetUserRole'.
    /// </summary>
    [Serializable]
    public partial class AspNetUserRoleDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the RoleId property that maps to the Entity AspNetUserRole</summary>
        public virtual System.String RoleId { get; set; }

        /// <summary>Get or set the UserId property that maps to the Entity AspNetUserRole</summary>
        public virtual System.String UserId { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual AspNetRoleDTO AspNetRole {get; set;}



		public virtual AspNetUserDTO AspNetUser {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public AspNetUserRoleDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 