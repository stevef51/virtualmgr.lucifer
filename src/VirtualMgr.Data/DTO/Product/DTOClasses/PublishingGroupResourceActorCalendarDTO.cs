﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'PublishingGroupResourceActorCalendar'.
    /// </summary>
    [Serializable]
    public partial class PublishingGroupResourceActorCalendarDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the NextRunDate property that maps to the Entity PublishingGroupResourceActorCalendar</summary>
        public virtual System.DateTime NextRunDate { get; set; }

        /// <summary>Get or set the PublishingGroupResourceCalendarId property that maps to the Entity PublishingGroupResourceActorCalendar</summary>
        public virtual System.Guid PublishingGroupResourceCalendarId { get; set; }
        
        #endregion

        #region Related Field Public Properties









		public virtual PublishingGroupResourceCalendarDTO PublishingGroupResourceCalendar {get; set;}



        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public PublishingGroupResourceActorCalendarDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 