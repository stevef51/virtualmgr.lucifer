﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'MediaPreview'.
    /// </summary>
    [Serializable]
    public partial class MediaPreviewDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the Data property that maps to the Entity MediaPreview</summary>
        public virtual System.Byte[] Data { get; set; }

        /// <summary>Get or set the MediaId property that maps to the Entity MediaPreview</summary>
        public virtual System.Guid MediaId { get; set; }

        /// <summary>Get or set the Page property that maps to the Entity MediaPreview</summary>
        public virtual System.Int32 Page { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual MediaDTO Medium {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public MediaPreviewDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 