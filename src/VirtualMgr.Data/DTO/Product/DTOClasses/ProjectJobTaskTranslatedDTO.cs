﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'ProjectJobTaskTranslated'.
    /// </summary>
    [Serializable]
    public partial class ProjectJobTaskTranslatedDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the CultureName property that maps to the Entity ProjectJobTaskTranslated</summary>
        public virtual System.String CultureName { get; set; }

        /// <summary>Get or set the Description property that maps to the Entity ProjectJobTaskTranslated</summary>
        public virtual System.String Description { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity ProjectJobTaskTranslated</summary>
        public virtual System.Guid Id { get; set; }

        /// <summary>Get or set the Name property that maps to the Entity ProjectJobTaskTranslated</summary>
        public virtual System.String Name { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual ProjectJobTaskDTO ProjectJobTask {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public ProjectJobTaskTranslatedDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 