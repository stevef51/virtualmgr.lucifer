﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'TabletProfile'.
    /// </summary>
    [Serializable]
    public partial class TabletProfileDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the AllowNumberPadLogin property that maps to the Entity TabletProfile</summary>
        public virtual System.Boolean AllowNumberPadLogin { get; set; }

        /// <summary>Get or set the Archived property that maps to the Entity TabletProfile</summary>
        public virtual System.Boolean Archived { get; set; }

        /// <summary>Get or set the AutoLogoutLongSecs property that maps to the Entity TabletProfile</summary>
        public virtual System.Int32? AutoLogoutLongSecs { get; set; }

        /// <summary>Get or set the AutoLogoutShortSecs property that maps to the Entity TabletProfile</summary>
        public virtual System.Int32? AutoLogoutShortSecs { get; set; }

        /// <summary>Get or set the ConfirmLogout property that maps to the Entity TabletProfile</summary>
        public virtual System.Int32 ConfirmLogout { get; set; }

        /// <summary>Get or set the EnableEnvironmentalSensing property that maps to the Entity TabletProfile</summary>
        public virtual System.Boolean EnableEnvironmentalSensing { get; set; }

        /// <summary>Get or set the EnableFma property that maps to the Entity TabletProfile</summary>
        public virtual System.Boolean EnableFma { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity TabletProfile</summary>
        public virtual System.Int32 Id { get; set; }

        /// <summary>Get or set the LoginPhotoEvery property that maps to the Entity TabletProfile</summary>
        public virtual System.Int32 LoginPhotoEvery { get; set; }

        /// <summary>Get or set the LogoutPassword property that maps to the Entity TabletProfile</summary>
        public virtual System.String LogoutPassword { get; set; }

        /// <summary>Get or set the Name property that maps to the Entity TabletProfile</summary>
        public virtual System.String Name { get; set; }

        /// <summary>Get or set the SettingsPassword property that maps to the Entity TabletProfile</summary>
        public virtual System.String SettingsPassword { get; set; }
        
        #endregion

        #region Related Field Public Properties



		
		public virtual IList< TabletDTO> Tablets
		{
			get; set;
		}



		
		public virtual IList< UserDataDTO> UserDatas
		{
			get; set;
		}









        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public TabletProfileDTO()
        {
			
			
			this.Tablets = new List< TabletDTO>();
			
			
			
			this.UserDatas = new List< UserDataDTO>();
			
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 