﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'AssetTypeCalendarTask'.
    /// </summary>
    [Serializable]
    public partial class AssetTypeCalendarTaskDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the Archived property that maps to the Entity AssetTypeCalendarTask</summary>
        public virtual System.Boolean Archived { get; set; }

        /// <summary>Get or set the AssetTypeCalendarId property that maps to the Entity AssetTypeCalendarTask</summary>
        public virtual System.Int32 AssetTypeCalendarId { get; set; }

        /// <summary>Get or set the HierarchyRoleId property that maps to the Entity AssetTypeCalendarTask</summary>
        public virtual System.Int32 HierarchyRoleId { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity AssetTypeCalendarTask</summary>
        public virtual System.Int32 Id { get; set; }

        /// <summary>Get or set the StartTimeLocal property that maps to the Entity AssetTypeCalendarTask</summary>
        public virtual System.TimeSpan? StartTimeLocal { get; set; }

        /// <summary>Get or set the TaskDescription property that maps to the Entity AssetTypeCalendarTask</summary>
        public virtual System.String TaskDescription { get; set; }

        /// <summary>Get or set the TaskLengthInDays property that maps to the Entity AssetTypeCalendarTask</summary>
        public virtual System.Int32? TaskLengthInDays { get; set; }

        /// <summary>Get or set the TaskLevel property that maps to the Entity AssetTypeCalendarTask</summary>
        public virtual System.Int32? TaskLevel { get; set; }

        /// <summary>Get or set the TaskName property that maps to the Entity AssetTypeCalendarTask</summary>
        public virtual System.String TaskName { get; set; }

        /// <summary>Get or set the TaskSiteId property that maps to the Entity AssetTypeCalendarTask</summary>
        public virtual System.Guid TaskSiteId { get; set; }

        /// <summary>Get or set the TaskTypeId property that maps to the Entity AssetTypeCalendarTask</summary>
        public virtual System.Guid TaskTypeId { get; set; }
        
        #endregion

        #region Related Field Public Properties



		
		public virtual IList< AssetTypeCalendarTaskLabelDTO> AssetTypeCalendarTaskLabels
		{
			get; set;
		}





		public virtual AssetTypeCalendarDTO AssetTypeCalendar {get; set;}



		public virtual ProjectJobTaskTypeDTO ProjectJobTaskType {get; set;}



		public virtual HierarchyRoleDTO HierarchyRole {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public AssetTypeCalendarTaskDTO()
        {
			
			
			this.AssetTypeCalendarTaskLabels = new List< AssetTypeCalendarTaskLabelDTO>();
			
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 