﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'Message'.
    /// </summary>
    [Serializable]
    public partial class MessageDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the Id property that maps to the Entity Message</summary>
        public virtual System.Guid Id { get; set; }

        /// <summary>Get or set the InReplyToId property that maps to the Entity Message</summary>
        public virtual System.Guid? InReplyToId { get; set; }

        /// <summary>Get or set the RequiresAck property that maps to the Entity Message</summary>
        public virtual System.Boolean RequiresAck { get; set; }

        /// <summary>Get or set the SenderId property that maps to the Entity Message</summary>
        public virtual System.Guid SenderId { get; set; }

        /// <summary>Get or set the SentAt property that maps to the Entity Message</summary>
        public virtual System.DateTime SentAt { get; set; }

        /// <summary>Get or set the Text property that maps to the Entity Message</summary>
        public virtual System.String Text { get; set; }
        
        #endregion

        #region Related Field Public Properties



		
		public virtual IList< MailboxDTO> Mailboxes
		{
			get; set;
		}



		
		public virtual IList< MessageDTO> Replies
		{
			get; set;
		}





		public virtual MessageDTO InReplyTo {get; set;}



		public virtual UserDataDTO Sender {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public MessageDTO()
        {
			
			
			this.Mailboxes = new List< MailboxDTO>();
			
			
			
			this.Replies = new List< MessageDTO>();
			
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 