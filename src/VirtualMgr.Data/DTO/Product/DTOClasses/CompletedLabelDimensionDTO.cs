﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'CompletedLabelDimension'.
    /// </summary>
    [Serializable]
    public partial class CompletedLabelDimensionDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the Backcolor property that maps to the Entity CompletedLabelDimension</summary>
        public virtual System.Int32 Backcolor { get; set; }

        /// <summary>Get or set the Forecolor property that maps to the Entity CompletedLabelDimension</summary>
        public virtual System.Int32 Forecolor { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity CompletedLabelDimension</summary>
        public virtual System.Guid Id { get; set; }

        /// <summary>Get or set the Name property that maps to the Entity CompletedLabelDimension</summary>
        public virtual System.String Name { get; set; }
        
        #endregion

        #region Related Field Public Properties



		
		public virtual IList< CompletedLabelWorkingDocumentDimensionDTO> CompletedLabelWorkingDocumentDimensions
		{
			get; set;
		}



		
		public virtual IList< CompletedLabelWorkingDocumentPresentedDimensionDTO> CompletedLabelWorkingDocumentPresentedDimensions
		{
			get; set;
		}









        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public CompletedLabelDimensionDTO()
        {
			
			
			this.CompletedLabelWorkingDocumentDimensions = new List< CompletedLabelWorkingDocumentDimensionDTO>();
			
			
			
			this.CompletedLabelWorkingDocumentPresentedDimensions = new List< CompletedLabelWorkingDocumentPresentedDimensionDTO>();
			
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 