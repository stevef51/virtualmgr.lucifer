﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'PublishingGroupResource'.
    /// </summary>
    [Serializable]
    public partial class PublishingGroupResourceDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the Description property that maps to the Entity PublishingGroupResource</summary>
        public virtual System.String Description { get; set; }

        /// <summary>Get or set the EstimatedPageCount property that maps to the Entity PublishingGroupResource</summary>
        public virtual System.Int32 EstimatedPageCount { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity PublishingGroupResource</summary>
        public virtual System.Int32 Id { get; set; }

        /// <summary>Get or set the IfFinishedCleanUpAfter property that maps to the Entity PublishingGroupResource</summary>
        public virtual System.Int32? IfFinishedCleanUpAfter { get; set; }

        /// <summary>Get or set the IfNotFinishedCleanUpAfter property that maps to the Entity PublishingGroupResource</summary>
        public virtual System.Int32? IfNotFinishedCleanUpAfter { get; set; }

        /// <summary>Get or set the Name property that maps to the Entity PublishingGroupResource</summary>
        public virtual System.String Name { get; set; }

        /// <summary>Get or set the PictureId property that maps to the Entity PublishingGroupResource</summary>
        public virtual System.Guid? PictureId { get; set; }

        /// <summary>Get or set the PopulateReportingData property that maps to the Entity PublishingGroupResource</summary>
        public virtual System.Boolean PopulateReportingData { get; set; }

        /// <summary>Get or set the PublishingGroupId property that maps to the Entity PublishingGroupResource</summary>
        public virtual System.Int32 PublishingGroupId { get; set; }

        /// <summary>Get or set the ResourceId property that maps to the Entity PublishingGroupResource</summary>
        public virtual System.Int32 ResourceId { get; set; }

        /// <summary>Get or set the UtcLastModified property that maps to the Entity PublishingGroupResource</summary>
        public virtual System.DateTime UtcLastModified { get; set; }
        
        #endregion

        #region Related Field Public Properties



		
		public virtual IList< AssetTypeContextPublishedResourceDTO> AssetTypeContextPublishedResources
		{
			get; set;
		}



		
		public virtual IList< ProjectJobScheduledTaskPublishingGroupResourceDTO> ProjectJobScheduledTaskPublishingGroupResources
		{
			get; set;
		}



		
		public virtual IList< ProjectJobTaskTypePublishingGroupResourceDTO> ProjectJobTaskTypePublishingGroupResources
		{
			get; set;
		}



		
		public virtual IList< ProjectJobTaskWorkingDocumentDTO> ProjectJobTaskWorkingDocuments
		{
			get; set;
		}



		
		public virtual IList< LabelPublishingGroupResourceDTO> LabelPublishingGroupResources
		{
			get; set;
		}



		
		public virtual IList< PublicPublishingGroupResourceDTO> PublicPublishingGroupResources
		{
			get; set;
		}



		
		public virtual IList< PublishingGroupResourceCalendarDTO> PublishingGroupResourceCalendars
		{
			get; set;
		}



		
		public virtual IList< PublishingGroupResourcePublicVariableDTO> PublishingGroupResourcePublicVariables
		{
			get; set;
		}



		
		public virtual IList< UserTypeContextPublishedResourceDTO> UserTypeContextPublishedResources
		{
			get; set;
		}



		
		public virtual IList< UserTypeDashboardDTO> UserTypeDashboards
		{
			get; set;
		}



		
		public virtual IList< WorkingDocumentDTO> WorkingDocuments
		{
			get; set;
		}





		public virtual MediaDTO Picture {get; set;}



		public virtual PublishingGroupDTO PublishingGroup {get; set;}



		public virtual ResourceDTO Resource {get; set;}







		public virtual PublishingGroupResourceDataDTO PublishingGroupResourceData {get; set;}



        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public PublishingGroupResourceDTO()
        {
			
			
			this.AssetTypeContextPublishedResources = new List< AssetTypeContextPublishedResourceDTO>();
			
			
			
			this.ProjectJobScheduledTaskPublishingGroupResources = new List< ProjectJobScheduledTaskPublishingGroupResourceDTO>();
			
			
			
			this.ProjectJobTaskTypePublishingGroupResources = new List< ProjectJobTaskTypePublishingGroupResourceDTO>();
			
			
			
			this.ProjectJobTaskWorkingDocuments = new List< ProjectJobTaskWorkingDocumentDTO>();
			
			
			
			this.LabelPublishingGroupResources = new List< LabelPublishingGroupResourceDTO>();
			
			
			
			this.PublicPublishingGroupResources = new List< PublicPublishingGroupResourceDTO>();
			
			
			
			this.PublishingGroupResourceCalendars = new List< PublishingGroupResourceCalendarDTO>();
			
			
			
			this.PublishingGroupResourcePublicVariables = new List< PublishingGroupResourcePublicVariableDTO>();
			
			
			
			this.UserTypeContextPublishedResources = new List< UserTypeContextPublishedResourceDTO>();
			
			
			
			this.UserTypeDashboards = new List< UserTypeDashboardDTO>();
			
			
			
			this.WorkingDocuments = new List< WorkingDocumentDTO>();
			
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 