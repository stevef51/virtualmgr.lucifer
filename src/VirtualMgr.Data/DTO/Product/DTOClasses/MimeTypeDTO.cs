﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'MimeType'.
    /// </summary>
    [Serializable]
    public partial class MimeTypeDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the Extension property that maps to the Entity MimeType</summary>
        public virtual System.String Extension { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity MimeType</summary>
        public virtual System.Int32 Id { get; set; }

        /// <summary>Get or set the MediaPreviewGenerator property that maps to the Entity MimeType</summary>
        public virtual System.String MediaPreviewGenerator { get; set; }

        /// <summary>Get or set the MimeType property that maps to the Entity MimeType</summary>
        public virtual System.String MimeType { get; set; }
        
        #endregion

        #region Related Field Public Properties









        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public MimeTypeDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 