﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'HierarchyBucketLabel'.
    /// </summary>
    [Serializable]
    public partial class HierarchyBucketLabelDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the HierarchyBucketId property that maps to the Entity HierarchyBucketLabel</summary>
        public virtual System.Int32 HierarchyBucketId { get; set; }

        /// <summary>Get or set the LabelId property that maps to the Entity HierarchyBucketLabel</summary>
        public virtual System.Guid LabelId { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual HierarchyBucketDTO HierarchyBucket {get; set;}



		public virtual LabelDTO Label {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public HierarchyBucketLabelDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 