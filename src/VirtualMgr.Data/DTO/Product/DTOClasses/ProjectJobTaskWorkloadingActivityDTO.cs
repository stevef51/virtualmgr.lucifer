﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'ProjectJobTaskWorkloadingActivity'.
    /// </summary>
    [Serializable]
    public partial class ProjectJobTaskWorkloadingActivityDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the Completed property that maps to the Entity ProjectJobTaskWorkloadingActivity</summary>
        public virtual System.Boolean? Completed { get; set; }

        /// <summary>Get or set the EstimatedDuration property that maps to the Entity ProjectJobTaskWorkloadingActivity</summary>
        public virtual System.Decimal EstimatedDuration { get; set; }

        /// <summary>Get or set the ProjectJobTaskId property that maps to the Entity ProjectJobTaskWorkloadingActivity</summary>
        public virtual System.Guid ProjectJobTaskId { get; set; }

        /// <summary>Get or set the SiteFeatureId property that maps to the Entity ProjectJobTaskWorkloadingActivity</summary>
        public virtual System.Guid SiteFeatureId { get; set; }

        /// <summary>Get or set the Text property that maps to the Entity ProjectJobTaskWorkloadingActivity</summary>
        public virtual System.String Text { get; set; }

        /// <summary>Get or set the WorkLoadingStandardId property that maps to the Entity ProjectJobTaskWorkloadingActivity</summary>
        public virtual System.Guid WorkLoadingStandardId { get; set; }

        /// <summary>Get or set the WorkloadingStandardMeasure property that maps to the Entity ProjectJobTaskWorkloadingActivity</summary>
        public virtual System.Decimal WorkloadingStandardMeasure { get; set; }

        /// <summary>Get or set the WorkloadingStandardSeconds property that maps to the Entity ProjectJobTaskWorkloadingActivity</summary>
        public virtual System.Decimal WorkloadingStandardSeconds { get; set; }

        /// <summary>Get or set the WorkloadingStandardUnitId property that maps to the Entity ProjectJobTaskWorkloadingActivity</summary>
        public virtual System.Int32 WorkloadingStandardUnitId { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual ProjectJobTaskDTO ProjectJobTask {get; set;}



		public virtual SiteFeatureDTO SiteFeature {get; set;}



		public virtual WorkLoadingStandardDTO WorkLoadingStandard {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public ProjectJobTaskWorkloadingActivityDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 