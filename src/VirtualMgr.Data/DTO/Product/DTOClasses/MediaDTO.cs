﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'Media'.
    /// </summary>
    [Serializable]
    public partial class MediaDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the CreatedBy property that maps to the Entity Media</summary>
        public virtual System.Guid? CreatedBy { get; set; }

        /// <summary>Get or set the DateCreated property that maps to the Entity Media</summary>
        public virtual System.DateTime DateCreated { get; set; }

        /// <summary>Get or set the DateVersionCreated property that maps to the Entity Media</summary>
        public virtual System.DateTime DateVersionCreated { get; set; }

        /// <summary>Get or set the Description property that maps to the Entity Media</summary>
        public virtual System.String Description { get; set; }

        /// <summary>Get or set the Filename property that maps to the Entity Media</summary>
        public virtual System.String Filename { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity Media</summary>
        public virtual System.Guid Id { get; set; }

        /// <summary>Get or set the Name property that maps to the Entity Media</summary>
        public virtual System.String Name { get; set; }

        /// <summary>Get or set the PreviewCount property that maps to the Entity Media</summary>
        public virtual System.Int32? PreviewCount { get; set; }

        /// <summary>Get or set the SortOrder property that maps to the Entity Media</summary>
        public virtual System.Int32 SortOrder { get; set; }

        /// <summary>Get or set the Type property that maps to the Entity Media</summary>
        public virtual System.String Type { get; set; }

        /// <summary>Get or set the UseVersioning property that maps to the Entity Media</summary>
        public virtual System.Boolean UseVersioning { get; set; }

        /// <summary>Get or set the Version property that maps to the Entity Media</summary>
        public virtual System.Int32 Version { get; set; }

        /// <summary>Get or set the WorkingDocumentId property that maps to the Entity Media</summary>
        public virtual System.Guid? WorkingDocumentId { get; set; }
        
        #endregion

        #region Related Field Public Properties



		
		public virtual IList< FacilityStructureDTO> FacilityStructures
		{
			get; set;
		}



		
		public virtual IList< CompletedPresentedUploadMediaFactValueDTO> CompletedPresentedUploadMediaFactValues
		{
			get; set;
		}



		
		public virtual IList< ProjectJobScheduledTaskAttachmentDTO> ProjectJobScheduledTaskAttachments
		{
			get; set;
		}



		
		public virtual IList< ProjectJobTaskAttachmentDTO> ProjectJobTaskAttachments
		{
			get; set;
		}



		
		public virtual IList< ProjectJobTaskMediaDTO> ProjectJobTaskMedias
		{
			get; set;
		}



		
		public virtual IList< ProjectJobTaskSignatureDTO> TaskSignatures
		{
			get; set;
		}



		
		public virtual IList< ProjectJobTaskSignatureDTO> TaskSignatures_
		{
			get; set;
		}



		
		public virtual IList< ProjectJobTaskTypeDTO> ProjectJobTaskTypes
		{
			get; set;
		}



		
		public virtual IList< LabelMediaDTO> LabelMedias
		{
			get; set;
		}



		
		public virtual IList< MediaFolderFileDTO> MediaFolderFiles
		{
			get; set;
		}



		
		public virtual IList< MediaPreviewDTO> MediaPreview
		{
			get; set;
		}



		
		public virtual IList< MediaRoleDTO> MediaRoles
		{
			get; set;
		}



		
		public virtual IList< MediaTranslatedDTO> Translated
		{
			get; set;
		}



		
		public virtual IList< MediaVersionDTO> MediaVersions
		{
			get; set;
		}



		
		public virtual IList< MediaViewingDTO> MediaViewings
		{
			get; set;
		}



		
		public virtual IList< PublishingGroupResourceDTO> PublishingGroupResources
		{
			get; set;
		}



		
		public virtual IList< OrderMediaDTO> OrderMedia
		{
			get; set;
		}



		
		public virtual IList< ProductDTO> Products
		{
			get; set;
		}



		
		public virtual IList< UserDataDTO> UserDatas
		{
			get; set;
		}



		
		public virtual IList< UserLogDTO> UserLogs
		{
			get; set;
		}



		
		public virtual IList< UserMediaDTO> UserMedias
		{
			get; set;
		}





		public virtual UserDataDTO UserData {get; set;}







		public virtual MediaDataDTO MediaData {get; set;}



		public virtual MediaPolicyDTO MediaPolicy {get; set;}



        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public MediaDTO()
        {
			
			
			this.FacilityStructures = new List< FacilityStructureDTO>();
			
			
			
			this.CompletedPresentedUploadMediaFactValues = new List< CompletedPresentedUploadMediaFactValueDTO>();
			
			
			
			this.ProjectJobScheduledTaskAttachments = new List< ProjectJobScheduledTaskAttachmentDTO>();
			
			
			
			this.ProjectJobTaskAttachments = new List< ProjectJobTaskAttachmentDTO>();
			
			
			
			this.ProjectJobTaskMedias = new List< ProjectJobTaskMediaDTO>();
			
			
			
			this.TaskSignatures = new List< ProjectJobTaskSignatureDTO>();
			
			
			
			this.TaskSignatures_ = new List< ProjectJobTaskSignatureDTO>();
			
			
			
			this.ProjectJobTaskTypes = new List< ProjectJobTaskTypeDTO>();
			
			
			
			this.LabelMedias = new List< LabelMediaDTO>();
			
			
			
			this.MediaFolderFiles = new List< MediaFolderFileDTO>();
			
			
			
			this.MediaPreview = new List< MediaPreviewDTO>();
			
			
			
			this.MediaRoles = new List< MediaRoleDTO>();
			
			
			
			this.Translated = new List< MediaTranslatedDTO>();
			
			
			
			this.MediaVersions = new List< MediaVersionDTO>();
			
			
			
			this.MediaViewings = new List< MediaViewingDTO>();
			
			
			
			this.PublishingGroupResources = new List< PublishingGroupResourceDTO>();
			
			
			
			this.OrderMedia = new List< OrderMediaDTO>();
			
			
			
			this.Products = new List< ProductDTO>();
			
			
			
			this.UserDatas = new List< UserDataDTO>();
			
			
			
			this.UserLogs = new List< UserLogDTO>();
			
			
			
			this.UserMedias = new List< UserMediaDTO>();
			
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 