﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'HierarchyBucketOverride'.
    /// </summary>
    [Serializable]
    public partial class HierarchyBucketOverrideDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the DateCreated property that maps to the Entity HierarchyBucketOverride</summary>
        public virtual System.DateTime DateCreated { get; set; }

        /// <summary>Get or set the EndDate property that maps to the Entity HierarchyBucketOverride</summary>
        public virtual System.DateTime EndDate { get; set; }

        /// <summary>Get or set the HierarchyBucketId property that maps to the Entity HierarchyBucketOverride</summary>
        public virtual System.Int32 HierarchyBucketId { get; set; }

        /// <summary>Get or set the HierarchyBucketOverrideTypeId property that maps to the Entity HierarchyBucketOverride</summary>
        public virtual System.Int32? HierarchyBucketOverrideTypeId { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity HierarchyBucketOverride</summary>
        public virtual System.Int32 Id { get; set; }

        /// <summary>Get or set the Notes property that maps to the Entity HierarchyBucketOverride</summary>
        public virtual System.String Notes { get; set; }

        /// <summary>Get or set the OriginalUserId property that maps to the Entity HierarchyBucketOverride</summary>
        public virtual System.Guid? OriginalUserId { get; set; }

        /// <summary>Get or set the RoleId property that maps to the Entity HierarchyBucketOverride</summary>
        public virtual System.Int32 RoleId { get; set; }

        /// <summary>Get or set the StartDate property that maps to the Entity HierarchyBucketOverride</summary>
        public virtual System.DateTime StartDate { get; set; }

        /// <summary>Get or set the UserId property that maps to the Entity HierarchyBucketOverride</summary>
        public virtual System.Guid? UserId { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual HierarchyBucketDTO HierarchyBucket {get; set;}



		public virtual HierarchyBucketOverrideTypeDTO HierarchyBucketOverrideType {get; set;}



		public virtual HierarchyRoleDTO HierarchyRole {get; set;}



		public virtual UserDataDTO OriginalUser {get; set;}



		public virtual UserDataDTO UserData {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public HierarchyBucketOverrideDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 