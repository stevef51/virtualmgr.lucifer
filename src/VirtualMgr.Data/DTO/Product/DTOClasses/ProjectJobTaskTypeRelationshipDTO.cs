﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'ProjectJobTaskTypeRelationship'.
    /// </summary>
    [Serializable]
    public partial class ProjectJobTaskTypeRelationshipDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the ProjectJobTaskTypeDestinationId property that maps to the Entity ProjectJobTaskTypeRelationship</summary>
        public virtual System.Guid ProjectJobTaskTypeDestinationId { get; set; }

        /// <summary>Get or set the ProjectJobTaskTypeSourceId property that maps to the Entity ProjectJobTaskTypeRelationship</summary>
        public virtual System.Guid ProjectJobTaskTypeSourceId { get; set; }

        /// <summary>Get or set the RelationshipType property that maps to the Entity ProjectJobTaskTypeRelationship</summary>
        public virtual System.Int32 RelationshipType { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual ProjectJobTaskTypeDTO DestinationProjectJobTaskType {get; set;}



		public virtual ProjectJobTaskTypeDTO SourceProjectJobTaskType {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public ProjectJobTaskTypeRelationshipDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 