﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'PasswordResetTicket'.
    /// </summary>
    [Serializable]
    public partial class PasswordResetTicketDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the DateCreated property that maps to the Entity PasswordResetTicket</summary>
        public virtual System.DateTime DateCreated { get; set; }

        /// <summary>Get or set the ExpiryDate property that maps to the Entity PasswordResetTicket</summary>
        public virtual System.DateTime ExpiryDate { get; set; }

        /// <summary>Get or set the PublicTicketId property that maps to the Entity PasswordResetTicket</summary>
        public virtual System.Guid PublicTicketId { get; set; }

        /// <summary>Get or set the UserId property that maps to the Entity PasswordResetTicket</summary>
        public virtual System.Guid UserId { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual UserDataDTO UserData {get; set;}







		public virtual PublicTicketDTO PublicTicket {get; set;}



        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public PasswordResetTicketDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 