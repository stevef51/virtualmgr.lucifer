﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'ProjectJobTaskWorkLog'.
    /// </summary>
    [Serializable]
    public partial class ProjectJobTaskWorkLogDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the ActualDuration property that maps to the Entity ProjectJobTaskWorkLog</summary>
        public virtual System.Decimal? ActualDuration { get; set; }

        /// <summary>Get or set the ApprovedDuration property that maps to the Entity ProjectJobTaskWorkLog</summary>
        public virtual System.Decimal? ApprovedDuration { get; set; }

        /// <summary>Get or set the ClockinAccuracy property that maps to the Entity ProjectJobTaskWorkLog</summary>
        public virtual System.Decimal? ClockinAccuracy { get; set; }

        /// <summary>Get or set the ClockinLatitude property that maps to the Entity ProjectJobTaskWorkLog</summary>
        public virtual System.Decimal? ClockinLatitude { get; set; }

        /// <summary>Get or set the ClockinLocationStatus property that maps to the Entity ProjectJobTaskWorkLog</summary>
        public virtual System.Int32? ClockinLocationStatus { get; set; }

        /// <summary>Get or set the ClockinLongitude property that maps to the Entity ProjectJobTaskWorkLog</summary>
        public virtual System.Decimal? ClockinLongitude { get; set; }

        /// <summary>Get or set the ClockinMetresFromSite property that maps to the Entity ProjectJobTaskWorkLog</summary>
        public virtual System.Decimal? ClockinMetresFromSite { get; set; }

        /// <summary>Get or set the ClockoutAccuracy property that maps to the Entity ProjectJobTaskWorkLog</summary>
        public virtual System.Decimal? ClockoutAccuracy { get; set; }

        /// <summary>Get or set the ClockoutLatitude property that maps to the Entity ProjectJobTaskWorkLog</summary>
        public virtual System.Decimal? ClockoutLatitude { get; set; }

        /// <summary>Get or set the ClockoutLocationStatus property that maps to the Entity ProjectJobTaskWorkLog</summary>
        public virtual System.Int32? ClockoutLocationStatus { get; set; }

        /// <summary>Get or set the ClockoutLongitude property that maps to the Entity ProjectJobTaskWorkLog</summary>
        public virtual System.Decimal? ClockoutLongitude { get; set; }

        /// <summary>Get or set the ClockoutMetresFromSite property that maps to the Entity ProjectJobTaskWorkLog</summary>
        public virtual System.Decimal? ClockoutMetresFromSite { get; set; }

        /// <summary>Get or set the DateCompleted property that maps to the Entity ProjectJobTaskWorkLog</summary>
        public virtual System.DateTime? DateCompleted { get; set; }

        /// <summary>Get or set the DateCreated property that maps to the Entity ProjectJobTaskWorkLog</summary>
        public virtual System.DateTime DateCreated { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity ProjectJobTaskWorkLog</summary>
        public virtual System.Guid Id { get; set; }

        /// <summary>Get or set the InactiveDuration property that maps to the Entity ProjectJobTaskWorkLog</summary>
        public virtual System.Decimal InactiveDuration { get; set; }

        /// <summary>Get or set the Notes property that maps to the Entity ProjectJobTaskWorkLog</summary>
        public virtual System.String Notes { get; set; }

        /// <summary>Get or set the ProjectJobTaskId property that maps to the Entity ProjectJobTaskWorkLog</summary>
        public virtual System.Guid ProjectJobTaskId { get; set; }

        /// <summary>Get or set the UserId property that maps to the Entity ProjectJobTaskWorkLog</summary>
        public virtual System.Guid UserId { get; set; }
        
        #endregion

        #region Related Field Public Properties



		
		public virtual IList< TimesheetedTaskWorkLogDTO> TimesheetedTaskWorkLogs
		{
			get; set;
		}





		public virtual ProjectJobTaskDTO ProjectJobTask {get; set;}



		public virtual UserDataDTO UserData {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public ProjectJobTaskWorkLogDTO()
        {
			
			
			this.TimesheetedTaskWorkLogs = new List< TimesheetedTaskWorkLogDTO>();
			
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 