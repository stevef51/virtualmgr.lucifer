﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'MergedTeamHierarchyWithLabel'.
    /// </summary>
    [Serializable]
    public partial class MergedTeamHierarchyWithLabelDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the AssignTasksThroughLabels property that maps to the Entity MergedTeamHierarchyWithLabel</summary>
        public virtual System.Boolean AssignTasksThroughLabels { get; set; }

        /// <summary>Get or set the BucketName property that maps to the Entity MergedTeamHierarchyWithLabel</summary>
        public virtual System.String BucketName { get; set; }

        /// <summary>Get or set the CompanyId property that maps to the Entity MergedTeamHierarchyWithLabel</summary>
        public virtual System.Guid? CompanyId { get; set; }

        /// <summary>Get or set the ExpiryDate property that maps to the Entity MergedTeamHierarchyWithLabel</summary>
        public virtual System.DateTime? ExpiryDate { get; set; }

        /// <summary>Get or set the HierarchyId property that maps to the Entity MergedTeamHierarchyWithLabel</summary>
        public virtual System.Int32 HierarchyId { get; set; }

        /// <summary>Get or set the HierarchyName property that maps to the Entity MergedTeamHierarchyWithLabel</summary>
        public virtual System.String HierarchyName { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity MergedTeamHierarchyWithLabel</summary>
        public virtual System.Int32 Id { get; set; }

        /// <summary>Get or set the IsPrimary property that maps to the Entity MergedTeamHierarchyWithLabel</summary>
        public virtual System.Boolean? IsPrimary { get; set; }

        /// <summary>Get or set the LeftIndex property that maps to the Entity MergedTeamHierarchyWithLabel</summary>
        public virtual System.Int32 LeftIndex { get; set; }

        /// <summary>Get or set the Name property that maps to the Entity MergedTeamHierarchyWithLabel</summary>
        public virtual System.String Name { get; set; }

        /// <summary>Get or set the OpenSecurityThroughLabels property that maps to the Entity MergedTeamHierarchyWithLabel</summary>
        public virtual System.Boolean OpenSecurityThroughLabels { get; set; }

        /// <summary>Get or set the ParentId property that maps to the Entity MergedTeamHierarchyWithLabel</summary>
        public virtual System.Int32? ParentId { get; set; }

        /// <summary>Get or set the PrimaryRecord property that maps to the Entity MergedTeamHierarchyWithLabel</summary>
        public virtual System.Int32 PrimaryRecord { get; set; }

        /// <summary>Get or set the RightIndex property that maps to the Entity MergedTeamHierarchyWithLabel</summary>
        public virtual System.Int32 RightIndex { get; set; }

        /// <summary>Get or set the RoleId property that maps to the Entity MergedTeamHierarchyWithLabel</summary>
        public virtual System.Int32 RoleId { get; set; }

        /// <summary>Get or set the RoleName property that maps to the Entity MergedTeamHierarchyWithLabel</summary>
        public virtual System.String RoleName { get; set; }

        /// <summary>Get or set the RosterId property that maps to the Entity MergedTeamHierarchyWithLabel</summary>
        public virtual System.Int32 RosterId { get; set; }

        /// <summary>Get or set the RosterName property that maps to the Entity MergedTeamHierarchyWithLabel</summary>
        public virtual System.String RosterName { get; set; }

        /// <summary>Get or set the TasksCanBeClaimed property that maps to the Entity MergedTeamHierarchyWithLabel</summary>
        public virtual System.Boolean TasksCanBeClaimed { get; set; }

        /// <summary>Get or set the TimeZone property that maps to the Entity MergedTeamHierarchyWithLabel</summary>
        public virtual System.String TimeZone { get; set; }

        /// <summary>Get or set the UserId property that maps to the Entity MergedTeamHierarchyWithLabel</summary>
        public virtual System.Guid? UserId { get; set; }
        
        #endregion

        #region Related Field Public Properties









        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public MergedTeamHierarchyWithLabelDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 