﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'ProjectJobTask'.
    /// </summary>
    [Serializable]
    public partial class ProjectJobTaskDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the ActualDuration property that maps to the Entity ProjectJobTask</summary>
        public virtual System.Decimal? ActualDuration { get; set; }

        /// <summary>Get or set the AllActivitiesEstimatedDuration property that maps to the Entity ProjectJobTask</summary>
        public virtual System.Decimal AllActivitiesEstimatedDuration { get; set; }

        /// <summary>Get or set the AssetId property that maps to the Entity ProjectJobTask</summary>
        public virtual System.Int32? AssetId { get; set; }

        /// <summary>Get or set the Backcolor property that maps to the Entity ProjectJobTask</summary>
        public virtual System.Int32? Backcolor { get; set; }

        /// <summary>Get or set the CompletedActivitiesEstimatedDuration property that maps to the Entity ProjectJobTask</summary>
        public virtual System.Decimal CompletedActivitiesEstimatedDuration { get; set; }

        /// <summary>Get or set the CompletionNotes property that maps to the Entity ProjectJobTask</summary>
        public virtual System.String CompletionNotes { get; set; }

        /// <summary>Get or set the Context property that maps to the Entity ProjectJobTask</summary>
        public virtual System.String Context { get; set; }

        /// <summary>Get or set the DateCompleted property that maps to the Entity ProjectJobTask</summary>
        public virtual System.DateTime? DateCompleted { get; set; }

        /// <summary>Get or set the DateCreated property that maps to the Entity ProjectJobTask</summary>
        public virtual System.DateTime DateCreated { get; set; }

        /// <summary>Get or set the Description property that maps to the Entity ProjectJobTask</summary>
        public virtual System.String Description { get; set; }

        /// <summary>Get or set the EndsOn property that maps to the Entity ProjectJobTask</summary>
        public virtual System.DateTime EndsOn { get; set; }

        /// <summary>Get or set the EstimatedDurationSeconds property that maps to the Entity ProjectJobTask</summary>
        public virtual System.Decimal? EstimatedDurationSeconds { get; set; }

        /// <summary>Get or set the Forecolor property that maps to the Entity ProjectJobTask</summary>
        public virtual System.Int32? Forecolor { get; set; }

        /// <summary>Get or set the HasOrders property that maps to the Entity ProjectJobTask</summary>
        public virtual System.Boolean HasOrders { get; set; }

        /// <summary>Get or set the HierarchyBucketId property that maps to the Entity ProjectJobTask</summary>
        public virtual System.Int32? HierarchyBucketId { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity ProjectJobTask</summary>
        public virtual System.Guid Id { get; set; }

        /// <summary>Get or set the InactiveDuration property that maps to the Entity ProjectJobTask</summary>
        public virtual System.Decimal? InactiveDuration { get; set; }

        /// <summary>Get or set the LateAfterUtc property that maps to the Entity ProjectJobTask</summary>
        public virtual System.DateTimeOffset? LateAfterUtc { get; set; }

        /// <summary>Get or set the Level property that maps to the Entity ProjectJobTask</summary>
        public virtual System.Int32 Level { get; set; }

        /// <summary>Get or set the MinimumDuration property that maps to the Entity ProjectJobTask</summary>
        public virtual System.Decimal? MinimumDuration { get; set; }

        /// <summary>Get or set the Name property that maps to the Entity ProjectJobTask</summary>
        public virtual System.String Name { get; set; }

        /// <summary>Get or set the NotifyUser property that maps to the Entity ProjectJobTask</summary>
        public virtual System.Boolean NotifyUser { get; set; }

        /// <summary>Get or set the OriginalOwnerId property that maps to the Entity ProjectJobTask</summary>
        public virtual System.Guid OriginalOwnerId { get; set; }

        /// <summary>Get or set the OriginalTaskTypeId property that maps to the Entity ProjectJobTask</summary>
        public virtual System.Guid? OriginalTaskTypeId { get; set; }

        /// <summary>Get or set the OwnerId property that maps to the Entity ProjectJobTask</summary>
        public virtual System.Guid OwnerId { get; set; }

        /// <summary>Get or set the PhotoDuringSignature property that maps to the Entity ProjectJobTask</summary>
        public virtual System.Boolean PhotoDuringSignature { get; set; }

        /// <summary>Get or set the ProjectJobScheduledTaskId property that maps to the Entity ProjectJobTask</summary>
        public virtual System.Guid? ProjectJobScheduledTaskId { get; set; }

        /// <summary>Get or set the ProjectJobTaskGroupId property that maps to the Entity ProjectJobTask</summary>
        public virtual System.Guid? ProjectJobTaskGroupId { get; set; }

        /// <summary>Get or set the ReferenceNumber property that maps to the Entity ProjectJobTask</summary>
        public virtual System.String ReferenceNumber { get; set; }

        /// <summary>Get or set the RequireSignature property that maps to the Entity ProjectJobTask</summary>
        public virtual System.Boolean RequireSignature { get; set; }

        /// <summary>Get or set the RoleId property that maps to the Entity ProjectJobTask</summary>
        public virtual System.Int32? RoleId { get; set; }

        /// <summary>Get or set the RosterId property that maps to the Entity ProjectJobTask</summary>
        public virtual System.Int32 RosterId { get; set; }

        /// <summary>Get or set the ScheduledOwnerId property that maps to the Entity ProjectJobTask</summary>
        public virtual System.Guid? ScheduledOwnerId { get; set; }

        /// <summary>Get or set the SiteId property that maps to the Entity ProjectJobTask</summary>
        public virtual System.Guid? SiteId { get; set; }

        /// <summary>Get or set the SiteLatitude property that maps to the Entity ProjectJobTask</summary>
        public virtual System.Decimal? SiteLatitude { get; set; }

        /// <summary>Get or set the SiteLongitude property that maps to the Entity ProjectJobTask</summary>
        public virtual System.Decimal? SiteLongitude { get; set; }

        /// <summary>Get or set the SortOrder property that maps to the Entity ProjectJobTask</summary>
        public virtual System.Int32 SortOrder { get; set; }

        /// <summary>Get or set the StartTimeUtc property that maps to the Entity ProjectJobTask</summary>
        public virtual System.DateTimeOffset? StartTimeUtc { get; set; }

        /// <summary>Get or set the Status property that maps to the Entity ProjectJobTask</summary>
        public virtual System.Int32 Status { get; set; }

        /// <summary>Get or set the StatusDate property that maps to the Entity ProjectJobTask</summary>
        public virtual System.DateTime StatusDate { get; set; }

        /// <summary>Get or set the StatusId property that maps to the Entity ProjectJobTask</summary>
        public virtual System.Guid? StatusId { get; set; }

        /// <summary>Get or set the TaskTypeId property that maps to the Entity ProjectJobTask</summary>
        public virtual System.Guid TaskTypeId { get; set; }

        /// <summary>Get or set the WorkingDocumentCount property that maps to the Entity ProjectJobTask</summary>
        public virtual System.Int32 WorkingDocumentCount { get; set; }

        /// <summary>Get or set the WorkingGroupId property that maps to the Entity ProjectJobTask</summary>
        public virtual System.Guid? WorkingGroupId { get; set; }
        
        #endregion

        #region Related Field Public Properties



		
		public virtual IList< ProjectJobTaskAttachmentDTO> ProjectJobTaskAttachments
		{
			get; set;
		}



		
		public virtual IList< ProjectJobTaskChangeRequestDTO> ProjectJobTaskChangeRequests
		{
			get; set;
		}



		
		public virtual IList< ProjectJobTaskFinishedStatusDTO> ProjectJobTaskFinishedStatuses
		{
			get; set;
		}



		
		public virtual IList< ProjectJobTaskLabelDTO> ProjectJobTaskLabels
		{
			get; set;
		}



		
		public virtual IList< ProjectJobTaskMediaDTO> ProjectJobTaskMedias
		{
			get; set;
		}



		
		public virtual IList< ProjectJobTaskRelationshipDTO> ProjectJobTaskRelationships
		{
			get; set;
		}



		
		public virtual IList< ProjectJobTaskRelationshipDTO> ProjectJobTaskRelationships_
		{
			get; set;
		}



		
		public virtual IList< ProjectJobTaskTranslatedDTO> Translated
		{
			get; set;
		}



		
		public virtual IList< ProjectJobTaskWorkingDocumentDTO> ProjectJobTaskWorkingDocuments
		{
			get; set;
		}



		
		public virtual IList< ProjectJobTaskWorkLogDTO> ProjectJobTaskWorkLogs
		{
			get; set;
		}



		
		public virtual IList< OrderDTO> Orders
		{
			get; set;
		}



		
		public virtual IList< ProjectJobTaskWorkloadingActivityDTO> ProjectJobTaskWorkloadingActivities
		{
			get; set;
		}





		public virtual AssetDTO Asset {get; set;}



		public virtual ProjectJobScheduledTaskDTO ProjectJobScheduledTask {get; set;}



		public virtual ProjectJobStatusDTO ProjectJobStatu {get; set;}



		public virtual ProjectJobTaskTypeDTO ProjectJobTaskType {get; set;}



		public virtual ProjectJobTaskTypeDTO ProjectJobTaskType_ {get; set;}



		public virtual RosterDTO Roster {get; set;}



		public virtual HierarchyBucketDTO HierarchyBucket {get; set;}



		public virtual HierarchyRoleDTO HierarchyRole {get; set;}



		public virtual UserDataDTO Owner {get; set;}



		public virtual UserDataDTO Site {get; set;}







		public virtual ProjectJobTaskSignatureDTO TaskSignature {get; set;}



        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public ProjectJobTaskDTO()
        {
			
			
			this.ProjectJobTaskAttachments = new List< ProjectJobTaskAttachmentDTO>();
			
			
			
			this.ProjectJobTaskChangeRequests = new List< ProjectJobTaskChangeRequestDTO>();
			
			
			
			this.ProjectJobTaskFinishedStatuses = new List< ProjectJobTaskFinishedStatusDTO>();
			
			
			
			this.ProjectJobTaskLabels = new List< ProjectJobTaskLabelDTO>();
			
			
			
			this.ProjectJobTaskMedias = new List< ProjectJobTaskMediaDTO>();
			
			
			
			this.ProjectJobTaskRelationships = new List< ProjectJobTaskRelationshipDTO>();
			
			
			
			this.ProjectJobTaskRelationships_ = new List< ProjectJobTaskRelationshipDTO>();
			
			
			
			this.Translated = new List< ProjectJobTaskTranslatedDTO>();
			
			
			
			this.ProjectJobTaskWorkingDocuments = new List< ProjectJobTaskWorkingDocumentDTO>();
			
			
			
			this.ProjectJobTaskWorkLogs = new List< ProjectJobTaskWorkLogDTO>();
			
			
			
			this.Orders = new List< OrderDTO>();
			
			
			
			this.ProjectJobTaskWorkloadingActivities = new List< ProjectJobTaskWorkloadingActivityDTO>();
			
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 