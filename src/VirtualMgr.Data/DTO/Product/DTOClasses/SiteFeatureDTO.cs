﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'SiteFeature'.
    /// </summary>
    [Serializable]
    public partial class SiteFeatureDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the Id property that maps to the Entity SiteFeature</summary>
        public virtual System.Guid Id { get; set; }

        /// <summary>Get or set the Measure property that maps to the Entity SiteFeature</summary>
        public virtual System.Decimal Measure { get; set; }

        /// <summary>Get or set the Name property that maps to the Entity SiteFeature</summary>
        public virtual System.String Name { get; set; }

        /// <summary>Get or set the SiteAreaId property that maps to the Entity SiteFeature</summary>
        public virtual System.Guid SiteAreaId { get; set; }

        /// <summary>Get or set the SiteId property that maps to the Entity SiteFeature</summary>
        public virtual System.Guid SiteId { get; set; }

        /// <summary>Get or set the UnitId property that maps to the Entity SiteFeature</summary>
        public virtual System.Int32 UnitId { get; set; }

        /// <summary>Get or set the WorkLoadingFeatureId property that maps to the Entity SiteFeature</summary>
        public virtual System.Guid WorkLoadingFeatureId { get; set; }
        
        #endregion

        #region Related Field Public Properties



		
		public virtual IList< ProjectJobScheduledTaskWorkloadingActivityDTO> ProjectJobScheduledTaskWorkloadingActivities
		{
			get; set;
		}



		
		public virtual IList< ProjectJobTaskWorkloadingActivityDTO> ProjectJobTaskWorkloadingActivities
		{
			get; set;
		}





		public virtual UserDataDTO UserData {get; set;}



		public virtual MeasurementUnitDTO MeasurementUnit {get; set;}



		public virtual SiteAreaDTO SiteArea {get; set;}



		public virtual WorkLoadingFeatureDTO WorkLoadingFeature {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public SiteFeatureDTO()
        {
			
			
			this.ProjectJobScheduledTaskWorkloadingActivities = new List< ProjectJobScheduledTaskWorkloadingActivityDTO>();
			
			
			
			this.ProjectJobTaskWorkloadingActivities = new List< ProjectJobTaskWorkloadingActivityDTO>();
			
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 