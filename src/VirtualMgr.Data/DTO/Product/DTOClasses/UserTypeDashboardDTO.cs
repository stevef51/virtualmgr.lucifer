﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'UserTypeDashboard'.
    /// </summary>
    [Serializable]
    public partial class UserTypeDashboardDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the Config property that maps to the Entity UserTypeDashboard</summary>
        public virtual System.String Config { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity UserTypeDashboard</summary>
        public virtual System.Int32 Id { get; set; }

        /// <summary>Get or set the PlayOffline property that maps to the Entity UserTypeDashboard</summary>
        public virtual System.Boolean PlayOffline { get; set; }

        /// <summary>Get or set the PublishingGroupResourceId property that maps to the Entity UserTypeDashboard</summary>
        public virtual System.Int32 PublishingGroupResourceId { get; set; }

        /// <summary>Get or set the SortOrder property that maps to the Entity UserTypeDashboard</summary>
        public virtual System.Int32 SortOrder { get; set; }

        /// <summary>Get or set the Title property that maps to the Entity UserTypeDashboard</summary>
        public virtual System.String Title { get; set; }

        /// <summary>Get or set the Tooltip property that maps to the Entity UserTypeDashboard</summary>
        public virtual System.String Tooltip { get; set; }

        /// <summary>Get or set the UserTypeId property that maps to the Entity UserTypeDashboard</summary>
        public virtual System.Int32 UserTypeId { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual PublishingGroupResourceDTO PublishingGroupResource {get; set;}



		public virtual UserTypeDTO UserType {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public UserTypeDashboardDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 