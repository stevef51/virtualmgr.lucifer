﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'Language'.
    /// </summary>
    [Serializable]
    public partial class LanguageDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the CultureName property that maps to the Entity Language</summary>
        public virtual System.String CultureName { get; set; }

        /// <summary>Get or set the LanguageName property that maps to the Entity Language</summary>
        public virtual System.String LanguageName { get; set; }

        /// <summary>Get or set the NativeName property that maps to the Entity Language</summary>
        public virtual System.String NativeName { get; set; }
        
        #endregion

        #region Related Field Public Properties









        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public LanguageDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 