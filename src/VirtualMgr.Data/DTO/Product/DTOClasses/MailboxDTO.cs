﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'Mailbox'.
    /// </summary>
    [Serializable]
    public partial class MailboxDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the Id property that maps to the Entity Mailbox</summary>
        public virtual System.Guid Id { get; set; }

        /// <summary>Get or set the IsRead property that maps to the Entity Mailbox</summary>
        public virtual System.Boolean? IsRead { get; set; }

        /// <summary>Get or set the MessageId property that maps to the Entity Mailbox</summary>
        public virtual System.Guid MessageId { get; set; }

        /// <summary>Get or set the UserId property that maps to the Entity Mailbox</summary>
        public virtual System.Guid UserId { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual MessageDTO Message {get; set;}



		public virtual UserDataDTO User {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public MailboxDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 