﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'ProductType'.
    /// </summary>
    [Serializable]
    public partial class ProductTypeDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the Id property that maps to the Entity ProductType</summary>
        public virtual System.Int32 Id { get; set; }

        /// <summary>Get or set the Name property that maps to the Entity ProductType</summary>
        public virtual System.String Name { get; set; }

        /// <summary>Get or set the Units property that maps to the Entity ProductType</summary>
        public virtual System.Int32 Units { get; set; }
        
        #endregion

        #region Related Field Public Properties



		
		public virtual IList< ProductDTO> Products
		{
			get; set;
		}









        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public ProductTypeDTO()
        {
			
			
			this.Products = new List< ProductDTO>();
			
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 