﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'PaymentResponse'.
    /// </summary>
    [Serializable]
    public partial class PaymentResponseDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the DateUtc property that maps to the Entity PaymentResponse</summary>
        public virtual System.DateTime DateUtc { get; set; }

        /// <summary>Get or set the Errors property that maps to the Entity PaymentResponse</summary>
        public virtual System.String Errors { get; set; }

        /// <summary>Get or set the GatewayAuthorisationCode property that maps to the Entity PaymentResponse</summary>
        public virtual System.String GatewayAuthorisationCode { get; set; }

        /// <summary>Get or set the GatewayTransactionId property that maps to the Entity PaymentResponse</summary>
        public virtual System.String GatewayTransactionId { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity PaymentResponse</summary>
        public virtual System.Guid Id { get; set; }

        /// <summary>Get or set the PendingPaymentId property that maps to the Entity PaymentResponse</summary>
        public virtual System.Guid PendingPaymentId { get; set; }

        /// <summary>Get or set the RequestedAmount property that maps to the Entity PaymentResponse</summary>
        public virtual System.Decimal RequestedAmount { get; set; }

        /// <summary>Get or set the Response property that maps to the Entity PaymentResponse</summary>
        public virtual System.String Response { get; set; }

        /// <summary>Get or set the ResponseCode property that maps to the Entity PaymentResponse</summary>
        public virtual System.String ResponseCode { get; set; }

        /// <summary>Get or set the ResponseMessage property that maps to the Entity PaymentResponse</summary>
        public virtual System.String ResponseMessage { get; set; }

        /// <summary>Get or set the TransactedAmount property that maps to the Entity PaymentResponse</summary>
        public virtual System.Decimal? TransactedAmount { get; set; }

        /// <summary>Get or set the TransactionStatus property that maps to the Entity PaymentResponse</summary>
        public virtual System.Boolean TransactionStatus { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual PendingPaymentDTO PendingPayment {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public PaymentResponseDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 