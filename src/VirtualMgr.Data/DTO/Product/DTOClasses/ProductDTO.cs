﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'Product'.
    /// </summary>
    [Serializable]
    public partial class ProductDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the Code property that maps to the Entity Product</summary>
        public virtual System.String Code { get; set; }

        /// <summary>Get or set the Description property that maps to the Entity Product</summary>
        public virtual System.String Description { get; set; }

        /// <summary>Get or set the Extra property that maps to the Entity Product</summary>
        public virtual System.String Extra { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity Product</summary>
        public virtual System.Int32 Id { get; set; }

        /// <summary>Get or set the IsCoupon property that maps to the Entity Product</summary>
        public virtual System.Boolean IsCoupon { get; set; }

        /// <summary>Get or set the MediaId property that maps to the Entity Product</summary>
        public virtual System.Guid? MediaId { get; set; }

        /// <summary>Get or set the Name property that maps to the Entity Product</summary>
        public virtual System.String Name { get; set; }

        /// <summary>Get or set the Price property that maps to the Entity Product</summary>
        public virtual System.Decimal? Price { get; set; }

        /// <summary>Get or set the PriceJsFunctionId property that maps to the Entity Product</summary>
        public virtual System.Int32? PriceJsFunctionId { get; set; }

        /// <summary>Get or set the ProductTypeId property that maps to the Entity Product</summary>
        public virtual System.Int32 ProductTypeId { get; set; }

        /// <summary>Get or set the ValidFrom property that maps to the Entity Product</summary>
        public virtual System.DateTime? ValidFrom { get; set; }

        /// <summary>Get or set the ValidTo property that maps to the Entity Product</summary>
        public virtual System.DateTime? ValidTo { get; set; }
        
        #endregion

        #region Related Field Public Properties



		
		public virtual IList< OrderItemDTO> OrderItems
		{
			get; set;
		}



		
		public virtual IList< ProductCatalogItemDTO> ProductCatalogItems
		{
			get; set;
		}



		
		public virtual IList< ProductHierarchyDTO> ChildProductHierarchies
		{
			get; set;
		}



		
		public virtual IList< ProductHierarchyDTO> ParentProductHierarchies
		{
			get; set;
		}





		public virtual MediaDTO Medium {get; set;}



		public virtual JsfunctionDTO Jsfunction {get; set;}



		public virtual ProductTypeDTO ProductType {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public ProductDTO()
        {
			
			
			this.OrderItems = new List< OrderItemDTO>();
			
			
			
			this.ProductCatalogItems = new List< ProductCatalogItemDTO>();
			
			
			
			this.ChildProductHierarchies = new List< ProductHierarchyDTO>();
			
			
			
			this.ParentProductHierarchies = new List< ProductHierarchyDTO>();
			
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 