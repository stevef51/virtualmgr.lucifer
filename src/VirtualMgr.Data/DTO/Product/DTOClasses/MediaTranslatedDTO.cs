﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'MediaTranslated'.
    /// </summary>
    [Serializable]
    public partial class MediaTranslatedDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the CultureName property that maps to the Entity MediaTranslated</summary>
        public virtual System.String CultureName { get; set; }

        /// <summary>Get or set the Description property that maps to the Entity MediaTranslated</summary>
        public virtual System.String Description { get; set; }

        /// <summary>Get or set the Filename property that maps to the Entity MediaTranslated</summary>
        public virtual System.String Filename { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity MediaTranslated</summary>
        public virtual System.Guid Id { get; set; }

        /// <summary>Get or set the Name property that maps to the Entity MediaTranslated</summary>
        public virtual System.String Name { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual MediaDTO Medium {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public MediaTranslatedDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 