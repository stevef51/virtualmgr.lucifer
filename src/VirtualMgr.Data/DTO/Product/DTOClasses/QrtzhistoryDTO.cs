﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'Qrtzhistory'.
    /// </summary>
    [Serializable]
    public partial class QrtzhistoryDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the Completed property that maps to the Entity Qrtzhistory</summary>
        public virtual System.Boolean Completed { get; set; }

        /// <summary>Get or set the Group property that maps to the Entity Qrtzhistory</summary>
        public virtual System.String Group { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity Qrtzhistory</summary>
        public virtual System.Guid Id { get; set; }

        /// <summary>Get or set the Job property that maps to the Entity Qrtzhistory</summary>
        public virtual System.String Job { get; set; }

        /// <summary>Get or set the Notes property that maps to the Entity Qrtzhistory</summary>
        public virtual System.String Notes { get; set; }

        /// <summary>Get or set the RunTime property that maps to the Entity Qrtzhistory</summary>
        public virtual System.DateTime RunTime { get; set; }

        /// <summary>Get or set the Trigger property that maps to the Entity Qrtzhistory</summary>
        public virtual System.String Trigger { get; set; }
        
        #endregion

        #region Related Field Public Properties









        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public QrtzhistoryDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 