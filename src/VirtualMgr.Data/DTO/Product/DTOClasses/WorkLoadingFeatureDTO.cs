﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'WorkLoadingFeature'.
    /// </summary>
    [Serializable]
    public partial class WorkLoadingFeatureDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the Archived property that maps to the Entity WorkLoadingFeature</summary>
        public virtual System.Boolean Archived { get; set; }

        /// <summary>Get or set the Dimensions property that maps to the Entity WorkLoadingFeature</summary>
        public virtual System.Int32 Dimensions { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity WorkLoadingFeature</summary>
        public virtual System.Guid Id { get; set; }

        /// <summary>Get or set the Name property that maps to the Entity WorkLoadingFeature</summary>
        public virtual System.String Name { get; set; }
        
        #endregion

        #region Related Field Public Properties



		
		public virtual IList< SiteFeatureDTO> SiteFeatures
		{
			get; set;
		}



		
		public virtual IList< WorkLoadingStandardDTO> WorkLoadingStandards
		{
			get; set;
		}









        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public WorkLoadingFeatureDTO()
        {
			
			
			this.SiteFeatures = new List< SiteFeatureDTO>();
			
			
			
			this.WorkLoadingStandards = new List< WorkLoadingStandardDTO>();
			
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 