﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'Report'.
    /// </summary>
    [Serializable]
    public partial class ReportDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the Configuration property that maps to the Entity Report</summary>
        public virtual System.String Configuration { get; set; }

        /// <summary>Get or set the Configuration_ property that maps to the Entity Report</summary>
        public virtual System.String Configuration_ { get; set; }

        /// <summary>Get or set the Description property that maps to the Entity Report</summary>
        public virtual System.String Description { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity Report</summary>
        public virtual System.Int32 Id { get; set; }

        /// <summary>Get or set the Name property that maps to the Entity Report</summary>
        public virtual System.String Name { get; set; }

        /// <summary>Get or set the Type property that maps to the Entity Report</summary>
        public virtual System.Int32 Type { get; set; }
        
        #endregion

        #region Related Field Public Properties



		
		public virtual IList< ReportRoleDTO> ReportRoles
		{
			get; set;
		}



		
		public virtual IList< ReportTicketDTO> ReportTickets
		{
			get; set;
		}









		public virtual ReportDataDTO ReportData {get; set;}



        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public ReportDTO()
        {
			
			
			this.ReportRoles = new List< ReportRoleDTO>();
			
			
			
			this.ReportTickets = new List< ReportTicketDTO>();
			
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 