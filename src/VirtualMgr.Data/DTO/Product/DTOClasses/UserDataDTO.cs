﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'UserData'.
    /// </summary>
    [Serializable]
    public partial class UserDataDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the AspNetUserId property that maps to the Entity UserData</summary>
        public virtual System.String AspNetUserId { get; set; }

        /// <summary>Get or set the AuthorizationProviderKey property that maps to the Entity UserData</summary>
        public virtual System.String AuthorizationProviderKey { get; set; }

        /// <summary>Get or set the CommencementDate property that maps to the Entity UserData</summary>
        public virtual System.DateTime? CommencementDate { get; set; }

        /// <summary>Get or set the CompanyId property that maps to the Entity UserData</summary>
        public virtual System.Guid? CompanyId { get; set; }

        /// <summary>Get or set the DefaultLanguageCode property that maps to the Entity UserData</summary>
        public virtual System.String DefaultLanguageCode { get; set; }

        /// <summary>Get or set the DefaultTabletProfileId property that maps to the Entity UserData</summary>
        public virtual System.Int32? DefaultTabletProfileId { get; set; }

        /// <summary>Get or set the ExpiryDate property that maps to the Entity UserData</summary>
        public virtual System.DateTime? ExpiryDate { get; set; }

        /// <summary>Get or set the Latitude property that maps to the Entity UserData</summary>
        public virtual System.Decimal? Latitude { get; set; }

        /// <summary>Get or set the Longitude property that maps to the Entity UserData</summary>
        public virtual System.Decimal? Longitude { get; set; }

        /// <summary>Get or set the MediaId property that maps to the Entity UserData</summary>
        public virtual System.Guid? MediaId { get; set; }

        /// <summary>Get or set the Mobile property that maps to the Entity UserData</summary>
        public virtual System.String Mobile { get; set; }

        /// <summary>Get or set the Name property that maps to the Entity UserData</summary>
        public virtual System.String Name { get; set; }

        /// <summary>Get or set the TimeZone property that maps to the Entity UserData</summary>
        public virtual System.String TimeZone { get; set; }

        /// <summary>Get or set the UserId property that maps to the Entity UserData</summary>
        public virtual System.Guid UserId { get; set; }

        /// <summary>Get or set the UserTypeId property that maps to the Entity UserData</summary>
        public virtual System.Int32 UserTypeId { get; set; }
        
        #endregion

        #region Related Field Public Properties



		
		public virtual IList< BeaconDTO> Beacons
		{
			get; set;
		}



		
		public virtual IList< FacilityStructureDTO> FacilityStructures
		{
			get; set;
		}



		
		public virtual IList< TabletUuidDTO> TabletUuids
		{
			get; set;
		}



		
		public virtual IList< GatewayCustomerDTO> GatewayCustomers
		{
			get; set;
		}



		
		public virtual IList< PendingPaymentDTO> PendingPayments
		{
			get; set;
		}



		
		public virtual IList< ProjectJobScheduledTaskDTO> ProjectJobScheduledTasks_
		{
			get; set;
		}



		
		public virtual IList< ProjectJobTaskDTO> ProjectJobTasks
		{
			get; set;
		}



		
		public virtual IList< ProjectJobTaskDTO> ProjectJobTasks_
		{
			get; set;
		}



		
		public virtual IList< ProjectJobTaskChangeRequestDTO> ProjectJobTaskChangeRequestsProcessedBy
		{
			get; set;
		}



		
		public virtual IList< ProjectJobTaskChangeRequestDTO> ProjectJobTaskChangeRequestsRequestedBy
		{
			get; set;
		}



		
		public virtual IList< ProjectJobTaskWorkLogDTO> ProjectJobTaskWorkLogs
		{
			get; set;
		}



		
		public virtual IList< TimesheetItemDTO> TimesheetItems
		{
			get; set;
		}



		
		public virtual IList< TimesheetItemDTO> TimesheetItems_
		{
			get; set;
		}



		
		public virtual IList< HierarchyBucketDTO> HierarchyBuckets
		{
			get; set;
		}



		
		public virtual IList< HierarchyBucketOverrideDTO> OriginalUserHierarchyBucketOverrides
		{
			get; set;
		}



		
		public virtual IList< HierarchyBucketOverrideDTO> UserDataHierarchyBucketOverrides
		{
			get; set;
		}



		
		public virtual IList< LabelUserDTO> LabelUsers
		{
			get; set;
		}



		
		public virtual IList< MailboxDTO> MailboxesReceived
		{
			get; set;
		}



		
		public virtual IList< MediaDTO> Medias
		{
			get; set;
		}



		
		public virtual IList< MediaVersionDTO> MediaVersionsCreated
		{
			get; set;
		}



		
		public virtual IList< MediaVersionDTO> MediaVersionsReplaced
		{
			get; set;
		}



		
		public virtual IList< MediaViewingDTO> MediaViewings
		{
			get; set;
		}



		
		public virtual IList< MessageDTO> MessagesSent
		{
			get; set;
		}



		
		public virtual IList< NewsFeedDTO> NewsFeeds
		{
			get; set;
		}



		
		public virtual IList< PasswordResetTicketDTO> PasswordResetTickets
		{
			get; set;
		}



		
		public virtual IList< PublishingGroupActorDTO> PublishingGroupActors
		{
			get; set;
		}



		
		public virtual IList< QrcodeDTO> Qrcodes
		{
			get; set;
		}



		
		public virtual IList< OrderDTO> Orders
		{
			get; set;
		}



		
		public virtual IList< UserContextDataDTO> UserContextDatas
		{
			get; set;
		}



		
		public virtual IList< UserLogDTO> UserLogs
		{
			get; set;
		}



		
		public virtual IList< UserLogDTO> UserLogs_
		{
			get; set;
		}



		
		public virtual IList< UserMediaDTO> UserMedias
		{
			get; set;
		}



		
		public virtual IList< SiteAreaDTO> SiteAreas
		{
			get; set;
		}



		
		public virtual IList< SiteFeatureDTO> SiteFeatures
		{
			get; set;
		}



		
		public virtual IList< WorkingDocumentDTO> WorkingDocumentsReviewee
		{
			get; set;
		}



		
		public virtual IList< WorkingDocumentDTO> WorkingDocumentsReviewer
		{
			get; set;
		}





		public virtual AspNetUserDTO AspNetUser {get; set;}



		public virtual TabletProfileDTO TabletProfile {get; set;}



		public virtual CompanyDTO Company {get; set;}



		public virtual MediaDTO Medium {get; set;}



		public virtual UserTypeDTO UserType {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public UserDataDTO()
        {
			
			
			this.Beacons = new List< BeaconDTO>();
			
			
			
			this.FacilityStructures = new List< FacilityStructureDTO>();
			
			
			
			this.TabletUuids = new List< TabletUuidDTO>();
			
			
			
			this.GatewayCustomers = new List< GatewayCustomerDTO>();
			
			
			
			this.PendingPayments = new List< PendingPaymentDTO>();
			
			
			
			this.ProjectJobScheduledTasks_ = new List< ProjectJobScheduledTaskDTO>();
			
			
			
			this.ProjectJobTasks = new List< ProjectJobTaskDTO>();
			
			
			
			this.ProjectJobTasks_ = new List< ProjectJobTaskDTO>();
			
			
			
			this.ProjectJobTaskChangeRequestsProcessedBy = new List< ProjectJobTaskChangeRequestDTO>();
			
			
			
			this.ProjectJobTaskChangeRequestsRequestedBy = new List< ProjectJobTaskChangeRequestDTO>();
			
			
			
			this.ProjectJobTaskWorkLogs = new List< ProjectJobTaskWorkLogDTO>();
			
			
			
			this.TimesheetItems = new List< TimesheetItemDTO>();
			
			
			
			this.TimesheetItems_ = new List< TimesheetItemDTO>();
			
			
			
			this.HierarchyBuckets = new List< HierarchyBucketDTO>();
			
			
			
			this.OriginalUserHierarchyBucketOverrides = new List< HierarchyBucketOverrideDTO>();
			
			
			
			this.UserDataHierarchyBucketOverrides = new List< HierarchyBucketOverrideDTO>();
			
			
			
			this.LabelUsers = new List< LabelUserDTO>();
			
			
			
			this.MailboxesReceived = new List< MailboxDTO>();
			
			
			
			this.Medias = new List< MediaDTO>();
			
			
			
			this.MediaVersionsCreated = new List< MediaVersionDTO>();
			
			
			
			this.MediaVersionsReplaced = new List< MediaVersionDTO>();
			
			
			
			this.MediaViewings = new List< MediaViewingDTO>();
			
			
			
			this.MessagesSent = new List< MessageDTO>();
			
			
			
			this.NewsFeeds = new List< NewsFeedDTO>();
			
			
			
			this.PasswordResetTickets = new List< PasswordResetTicketDTO>();
			
			
			
			this.PublishingGroupActors = new List< PublishingGroupActorDTO>();
			
			
			
			this.Qrcodes = new List< QrcodeDTO>();
			
			
			
			this.Orders = new List< OrderDTO>();
			
			
			
			this.UserContextDatas = new List< UserContextDataDTO>();
			
			
			
			this.UserLogs = new List< UserLogDTO>();
			
			
			
			this.UserLogs_ = new List< UserLogDTO>();
			
			
			
			this.UserMedias = new List< UserMediaDTO>();
			
			
			
			this.SiteAreas = new List< SiteAreaDTO>();
			
			
			
			this.SiteFeatures = new List< SiteFeatureDTO>();
			
			
			
			this.WorkingDocumentsReviewee = new List< WorkingDocumentDTO>();
			
			
			
			this.WorkingDocumentsReviewer = new List< WorkingDocumentDTO>();
			
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 