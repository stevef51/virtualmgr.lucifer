﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'ProjectJobScheduledTaskPublishingGroupResource'.
    /// </summary>
    [Serializable]
    public partial class ProjectJobScheduledTaskPublishingGroupResourceDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the AllowMultipleInstances property that maps to the Entity ProjectJobScheduledTaskPublishingGroupResource</summary>
        public virtual System.Boolean AllowMultipleInstances { get; set; }

        /// <summary>Get or set the IsMandatory property that maps to the Entity ProjectJobScheduledTaskPublishingGroupResource</summary>
        public virtual System.Boolean IsMandatory { get; set; }

        /// <summary>Get or set the ProjectJobScheduledTaskId property that maps to the Entity ProjectJobScheduledTaskPublishingGroupResource</summary>
        public virtual System.Guid ProjectJobScheduledTaskId { get; set; }

        /// <summary>Get or set the PublishingGroupResourceId property that maps to the Entity ProjectJobScheduledTaskPublishingGroupResource</summary>
        public virtual System.Int32 PublishingGroupResourceId { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual ProjectJobScheduledTaskDTO ProjectJobScheduledTask {get; set;}



		public virtual PublishingGroupResourceDTO PublishingGroupResource {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public ProjectJobScheduledTaskPublishingGroupResourceDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 