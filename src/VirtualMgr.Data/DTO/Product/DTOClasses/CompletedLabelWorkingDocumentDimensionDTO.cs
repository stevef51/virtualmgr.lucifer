﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'CompletedLabelWorkingDocumentDimension'.
    /// </summary>
    [Serializable]
    public partial class CompletedLabelWorkingDocumentDimensionDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the CompletedLabelDimensionId property that maps to the Entity CompletedLabelWorkingDocumentDimension</summary>
        public virtual System.Guid CompletedLabelDimensionId { get; set; }

        /// <summary>Get or set the CompletedWorkingDocumentFactId property that maps to the Entity CompletedLabelWorkingDocumentDimension</summary>
        public virtual System.Guid CompletedWorkingDocumentFactId { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual CompletedLabelDimensionDTO CompletedLabelDimension {get; set;}



		public virtual CompletedWorkingDocumentFactDTO CompletedWorkingDocumentFact {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public CompletedLabelWorkingDocumentDimensionDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 