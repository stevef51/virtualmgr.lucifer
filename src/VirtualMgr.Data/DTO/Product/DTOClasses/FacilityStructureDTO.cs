﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'FacilityStructure'.
    /// </summary>
    [Serializable]
    public partial class FacilityStructureDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the AddressId property that maps to the Entity FacilityStructure</summary>
        public virtual System.Int32? AddressId { get; set; }

        /// <summary>Get or set the Archived property that maps to the Entity FacilityStructure</summary>
        public virtual System.Boolean Archived { get; set; }

        /// <summary>Get or set the Description property that maps to the Entity FacilityStructure</summary>
        public virtual System.String Description { get; set; }

        /// <summary>Get or set the FloorHeight property that maps to the Entity FacilityStructure</summary>
        public virtual System.Decimal? FloorHeight { get; set; }

        /// <summary>Get or set the FloorPlanId property that maps to the Entity FacilityStructure</summary>
        public virtual System.Int32? FloorPlanId { get; set; }

        /// <summary>Get or set the HierarchyId property that maps to the Entity FacilityStructure</summary>
        public virtual System.Int32? HierarchyId { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity FacilityStructure</summary>
        public virtual System.Int32 Id { get; set; }

        /// <summary>Get or set the Level property that maps to the Entity FacilityStructure</summary>
        public virtual System.Int32? Level { get; set; }

        /// <summary>Get or set the MediaId property that maps to the Entity FacilityStructure</summary>
        public virtual System.Guid? MediaId { get; set; }

        /// <summary>Get or set the Name property that maps to the Entity FacilityStructure</summary>
        public virtual System.String Name { get; set; }

        /// <summary>Get or set the ParentId property that maps to the Entity FacilityStructure</summary>
        public virtual System.Int32? ParentId { get; set; }

        /// <summary>Get or set the SiteId property that maps to the Entity FacilityStructure</summary>
        public virtual System.Guid? SiteId { get; set; }

        /// <summary>Get or set the SortOrder property that maps to the Entity FacilityStructure</summary>
        public virtual System.Int32 SortOrder { get; set; }

        /// <summary>Get or set the StructureType property that maps to the Entity FacilityStructure</summary>
        public virtual System.Int32 StructureType { get; set; }
        
        #endregion

        #region Related Field Public Properties



		
		public virtual IList< BeaconDTO> Beacons
		{
			get; set;
		}



		
		public virtual IList< FacilityStructureDTO> Children
		{
			get; set;
		}



		
		public virtual IList< ProjectJobTaskTypeFacilityOverrideDTO> ProjectJobTaskTypeFacilityOverrides
		{
			get; set;
		}



		
		public virtual IList< OrderDTO> Orders
		{
			get; set;
		}



		
		public virtual IList< ProductCatalogDTO> ProductCatalogs
		{
			get; set;
		}



		
		public virtual IList< ProductCatalogFacilityStructureRuleDTO> ProductCatalogFacilityStructureRules
		{
			get; set;
		}





		public virtual AddressDTO Address {get; set;}



		public virtual FacilityStructureDTO Parent {get; set;}



		public virtual FloorPlanDTO FloorPlan {get; set;}



		public virtual HierarchyDTO Hierarchy {get; set;}



		public virtual MediaDTO Media {get; set;}



		public virtual UserDataDTO UserData {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public FacilityStructureDTO()
        {
			
			
			this.Beacons = new List< BeaconDTO>();
			
			
			
			this.Children = new List< FacilityStructureDTO>();
			
			
			
			this.ProjectJobTaskTypeFacilityOverrides = new List< ProjectJobTaskTypeFacilityOverrideDTO>();
			
			
			
			this.Orders = new List< OrderDTO>();
			
			
			
			this.ProductCatalogs = new List< ProductCatalogDTO>();
			
			
			
			this.ProductCatalogFacilityStructureRules = new List< ProductCatalogFacilityStructureRuleDTO>();
			
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 