﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'ProjectJobScheduleDay'.
    /// </summary>
    [Serializable]
    public partial class ProjectJobScheduleDayDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the Day property that maps to the Entity ProjectJobScheduleDay</summary>
        public virtual System.Int32 Day { get; set; }

        /// <summary>Get or set the EndTime property that maps to the Entity ProjectJobScheduleDay</summary>
        public virtual System.DateTime EndTime { get; set; }

        /// <summary>Get or set the LunchEnd property that maps to the Entity ProjectJobScheduleDay</summary>
        public virtual System.DateTime LunchEnd { get; set; }

        /// <summary>Get or set the LunchStart property that maps to the Entity ProjectJobScheduleDay</summary>
        public virtual System.DateTime LunchStart { get; set; }

        /// <summary>Get or set the OverrideRoleId property that maps to the Entity ProjectJobScheduleDay</summary>
        public virtual System.Int32? OverrideRoleId { get; set; }

        /// <summary>Get or set the RoleId property that maps to the Entity ProjectJobScheduleDay</summary>
        public virtual System.Int32 RoleId { get; set; }

        /// <summary>Get or set the RosterId property that maps to the Entity ProjectJobScheduleDay</summary>
        public virtual System.Int32 RosterId { get; set; }

        /// <summary>Get or set the StartTime property that maps to the Entity ProjectJobScheduleDay</summary>
        public virtual System.DateTime StartTime { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual RosterDTO Roster {get; set;}



		public virtual HierarchyRoleDTO HierarchyRole {get; set;}



		public virtual HierarchyRoleDTO OverrideHierarchyRole {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public ProjectJobScheduleDayDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 