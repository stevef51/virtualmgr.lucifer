﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'ProjectJobTaskLabel'.
    /// </summary>
    [Serializable]
    public partial class ProjectJobTaskLabelDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the LabelId property that maps to the Entity ProjectJobTaskLabel</summary>
        public virtual System.Guid LabelId { get; set; }

        /// <summary>Get or set the ProjectJobTaskId property that maps to the Entity ProjectJobTaskLabel</summary>
        public virtual System.Guid ProjectJobTaskId { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual ProjectJobTaskDTO ProjectJobTask {get; set;}



		public virtual LabelDTO Label {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public ProjectJobTaskLabelDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 