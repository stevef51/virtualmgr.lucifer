﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'MediaVersion'.
    /// </summary>
    [Serializable]
    public partial class MediaVersionDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the DateVersionClosed property that maps to the Entity MediaVersion</summary>
        public virtual System.DateTime DateVersionClosed { get; set; }

        /// <summary>Get or set the DateVersionCreated property that maps to the Entity MediaVersion</summary>
        public virtual System.DateTime DateVersionCreated { get; set; }

        /// <summary>Get or set the Description property that maps to the Entity MediaVersion</summary>
        public virtual System.String Description { get; set; }

        /// <summary>Get or set the Filename property that maps to the Entity MediaVersion</summary>
        public virtual System.String Filename { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity MediaVersion</summary>
        public virtual System.Guid Id { get; set; }

        /// <summary>Get or set the MediaId property that maps to the Entity MediaVersion</summary>
        public virtual System.Guid MediaId { get; set; }

        /// <summary>Get or set the Name property that maps to the Entity MediaVersion</summary>
        public virtual System.String Name { get; set; }

        /// <summary>Get or set the Version property that maps to the Entity MediaVersion</summary>
        public virtual System.Int32 Version { get; set; }

        /// <summary>Get or set the VersionCreatedBy property that maps to the Entity MediaVersion</summary>
        public virtual System.Guid VersionCreatedBy { get; set; }

        /// <summary>Get or set the VersionReplacedBy property that maps to the Entity MediaVersion</summary>
        public virtual System.Guid VersionReplacedBy { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual MediaDTO Media {get; set;}



		public virtual UserDataDTO VersionCreatedByUser {get; set;}



		public virtual UserDataDTO VersionReplacedByUser {get; set;}







		public virtual MediaVersionDataDTO MediaVersionData {get; set;}



        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public MediaVersionDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 