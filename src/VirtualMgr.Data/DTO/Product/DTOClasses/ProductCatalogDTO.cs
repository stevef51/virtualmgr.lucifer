﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'ProductCatalog'.
    /// </summary>
    [Serializable]
    public partial class ProductCatalogDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the Archived property that maps to the Entity ProductCatalog</summary>
        public virtual System.Boolean Archived { get; set; }

        /// <summary>Get or set the Description property that maps to the Entity ProductCatalog</summary>
        public virtual System.String Description { get; set; }

        /// <summary>Get or set the FacilityStructureId property that maps to the Entity ProductCatalog</summary>
        public virtual System.Int32 FacilityStructureId { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity ProductCatalog</summary>
        public virtual System.Int32 Id { get; set; }

        /// <summary>Get or set the Name property that maps to the Entity ProductCatalog</summary>
        public virtual System.String Name { get; set; }

        /// <summary>Get or set the ShowLineItemNotesWhenOrdering property that maps to the Entity ProductCatalog</summary>
        public virtual System.Boolean ShowLineItemNotesWhenOrdering { get; set; }

        /// <summary>Get or set the ShowPricingWhenOrdering property that maps to the Entity ProductCatalog</summary>
        public virtual System.Boolean ShowPricingWhenOrdering { get; set; }

        /// <summary>Get or set the StockCountWhenOrdering property that maps to the Entity ProductCatalog</summary>
        public virtual System.Boolean StockCountWhenOrdering { get; set; }
        
        #endregion

        #region Related Field Public Properties



		
		public virtual IList< OrderDTO> Orders
		{
			get; set;
		}



		
		public virtual IList< ProductCatalogFacilityStructureRuleDTO> ProductCatalogFacilityStructureRules
		{
			get; set;
		}



		
		public virtual IList< ProductCatalogItemDTO> ProductCatalogItems
		{
			get; set;
		}





		public virtual FacilityStructureDTO FacilityStructure {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public ProductCatalogDTO()
        {
			
			
			this.Orders = new List< OrderDTO>();
			
			
			
			this.ProductCatalogFacilityStructureRules = new List< ProductCatalogFacilityStructureRuleDTO>();
			
			
			
			this.ProductCatalogItems = new List< ProductCatalogItemDTO>();
			
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 