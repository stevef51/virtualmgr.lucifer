﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'PublicWorkingDocument'.
    /// </summary>
    [Serializable]
    public partial class PublicWorkingDocumentDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the PublicTicketId property that maps to the Entity PublicWorkingDocument</summary>
        public virtual System.Guid PublicTicketId { get; set; }

        /// <summary>Get or set the WorkingDocumentId property that maps to the Entity PublicWorkingDocument</summary>
        public virtual System.Guid WorkingDocumentId { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual PublicTicketDTO PublicTicket {get; set;}



		public virtual WorkingDocumentDTO WorkingDocument {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public PublicWorkingDocumentDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 