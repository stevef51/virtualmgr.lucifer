﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'ReportTicket'.
    /// </summary>
    [Serializable]
    public partial class ReportTicketDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the DateCreatedUtc property that maps to the Entity ReportTicket</summary>
        public virtual System.DateTime DateCreatedUtc { get; set; }

        /// <summary>Get or set the DefaultFormat property that maps to the Entity ReportTicket</summary>
        public virtual System.String DefaultFormat { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity ReportTicket</summary>
        public virtual System.Guid Id { get; set; }

        /// <summary>Get or set the JsonParameters property that maps to the Entity ReportTicket</summary>
        public virtual System.String JsonParameters { get; set; }

        /// <summary>Get or set the LastRunOnUtc property that maps to the Entity ReportTicket</summary>
        public virtual System.DateTime? LastRunOnUtc { get; set; }

        /// <summary>Get or set the ReportId property that maps to the Entity ReportTicket</summary>
        public virtual System.Int32 ReportId { get; set; }

        /// <summary>Get or set the RunAsUserId property that maps to the Entity ReportTicket</summary>
        public virtual System.Guid RunAsUserId { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual ReportDTO Report {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public ReportTicketDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 