﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'CompletedUserDimension'.
    /// </summary>
    [Serializable]
    public partial class CompletedUserDimensionDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the Email property that maps to the Entity CompletedUserDimension</summary>
        public virtual System.String Email { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity CompletedUserDimension</summary>
        public virtual System.Guid Id { get; set; }

        /// <summary>Get or set the Name property that maps to the Entity CompletedUserDimension</summary>
        public virtual System.String Name { get; set; }

        /// <summary>Get or set the TimeZone property that maps to the Entity CompletedUserDimension</summary>
        public virtual System.String TimeZone { get; set; }

        /// <summary>Get or set the Username property that maps to the Entity CompletedUserDimension</summary>
        public virtual System.String Username { get; set; }
        
        #endregion

        #region Related Field Public Properties



		
		public virtual IList< CompletedWorkingDocumentFactDTO> CompletedWorkingDocumentFactsAsReviewee
		{
			get; set;
		}



		
		public virtual IList< CompletedWorkingDocumentFactDTO> CompletedWorkingDocumentFactsAsReviewer
		{
			get; set;
		}



		
		public virtual IList< CompletedWorkingDocumentPresentedFactDTO> CompletedWorkingDocumentPresentedFactsReviewee
		{
			get; set;
		}



		
		public virtual IList< CompletedWorkingDocumentPresentedFactDTO> CompletedWorkingDocumentPresentedFactsReviewer
		{
			get; set;
		}









        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public CompletedUserDimensionDTO()
        {
			
			
			this.CompletedWorkingDocumentFactsAsReviewee = new List< CompletedWorkingDocumentFactDTO>();
			
			
			
			this.CompletedWorkingDocumentFactsAsReviewer = new List< CompletedWorkingDocumentFactDTO>();
			
			
			
			this.CompletedWorkingDocumentPresentedFactsReviewee = new List< CompletedWorkingDocumentPresentedFactDTO>();
			
			
			
			this.CompletedWorkingDocumentPresentedFactsReviewer = new List< CompletedWorkingDocumentPresentedFactDTO>();
			
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 