﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'AssetContextData'.
    /// </summary>
    [Serializable]
    public partial class AssetContextDataDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the AssetId property that maps to the Entity AssetContextData</summary>
        public virtual System.Int32 AssetId { get; set; }

        /// <summary>Get or set the AssetTypeContextPublishedResourceId property that maps to the Entity AssetContextData</summary>
        public virtual System.Int32 AssetTypeContextPublishedResourceId { get; set; }

        /// <summary>Get or set the CompletedWorkingDocumentId property that maps to the Entity AssetContextData</summary>
        public virtual System.Guid CompletedWorkingDocumentId { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual AssetDTO Asset {get; set;}



		public virtual AssetTypeContextPublishedResourceDTO AssetTypeContextPublishedResource {get; set;}



		public virtual CompletedWorkingDocumentFactDTO CompletedWorkingDocumentFact {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public AssetContextDataDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 