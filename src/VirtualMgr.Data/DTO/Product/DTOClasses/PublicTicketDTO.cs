﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'PublicTicket'.
    /// </summary>
    [Serializable]
    public partial class PublicTicketDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the Id property that maps to the Entity PublicTicket</summary>
        public virtual System.Guid Id { get; set; }

        /// <summary>Get or set the Status property that maps to the Entity PublicTicket</summary>
        public virtual System.Int32 Status { get; set; }
        
        #endregion

        #region Related Field Public Properties



		
		public virtual IList< PublicPublishingGroupResourceDTO> PublicPublishingGroupResources
		{
			get; set;
		}



		
		public virtual IList< PublicWorkingDocumentDTO> PublicWorkingDocuments
		{
			get; set;
		}









		public virtual PasswordResetTicketDTO PasswordResetTicket {get; set;}



        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public PublicTicketDTO()
        {
			
			
			this.PublicPublishingGroupResources = new List< PublicPublishingGroupResourceDTO>();
			
			
			
			this.PublicWorkingDocuments = new List< PublicWorkingDocumentDTO>();
			
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 