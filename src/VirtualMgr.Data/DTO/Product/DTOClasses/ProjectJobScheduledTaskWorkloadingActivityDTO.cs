﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'ProjectJobScheduledTaskWorkloadingActivity'.
    /// </summary>
    [Serializable]
    public partial class ProjectJobScheduledTaskWorkloadingActivityDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the EstimatedDuration property that maps to the Entity ProjectJobScheduledTaskWorkloadingActivity</summary>
        public virtual System.Decimal? EstimatedDuration { get; set; }

        /// <summary>Get or set the ExecuteFriday property that maps to the Entity ProjectJobScheduledTaskWorkloadingActivity</summary>
        public virtual System.Boolean? ExecuteFriday { get; set; }

        /// <summary>Get or set the ExecuteMonday property that maps to the Entity ProjectJobScheduledTaskWorkloadingActivity</summary>
        public virtual System.Boolean? ExecuteMonday { get; set; }

        /// <summary>Get or set the ExecuteSaturday property that maps to the Entity ProjectJobScheduledTaskWorkloadingActivity</summary>
        public virtual System.Boolean? ExecuteSaturday { get; set; }

        /// <summary>Get or set the ExecuteSunday property that maps to the Entity ProjectJobScheduledTaskWorkloadingActivity</summary>
        public virtual System.Boolean? ExecuteSunday { get; set; }

        /// <summary>Get or set the ExecuteThursday property that maps to the Entity ProjectJobScheduledTaskWorkloadingActivity</summary>
        public virtual System.Boolean? ExecuteThursday { get; set; }

        /// <summary>Get or set the ExecuteTuesday property that maps to the Entity ProjectJobScheduledTaskWorkloadingActivity</summary>
        public virtual System.Boolean? ExecuteTuesday { get; set; }

        /// <summary>Get or set the ExecuteWednesday property that maps to the Entity ProjectJobScheduledTaskWorkloadingActivity</summary>
        public virtual System.Boolean? ExecuteWednesday { get; set; }

        /// <summary>Get or set the Frequency property that maps to the Entity ProjectJobScheduledTaskWorkloadingActivity</summary>
        public virtual System.Int32 Frequency { get; set; }

        /// <summary>Get or set the ProjectJobScheduledTaskId property that maps to the Entity ProjectJobScheduledTaskWorkloadingActivity</summary>
        public virtual System.Guid ProjectJobScheduledTaskId { get; set; }

        /// <summary>Get or set the SiteFeatureId property that maps to the Entity ProjectJobScheduledTaskWorkloadingActivity</summary>
        public virtual System.Guid SiteFeatureId { get; set; }

        /// <summary>Get or set the Text property that maps to the Entity ProjectJobScheduledTaskWorkloadingActivity</summary>
        public virtual System.String Text { get; set; }

        /// <summary>Get or set the WorkloadingStandardId property that maps to the Entity ProjectJobScheduledTaskWorkloadingActivity</summary>
        public virtual System.Guid WorkloadingStandardId { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual ProjectJobScheduledTaskDTO ProjectJobScheduledTask {get; set;}



		public virtual SiteFeatureDTO SiteFeature {get; set;}



		public virtual WorkLoadingStandardDTO WorkLoadingStandard {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public ProjectJobScheduledTaskWorkloadingActivityDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 