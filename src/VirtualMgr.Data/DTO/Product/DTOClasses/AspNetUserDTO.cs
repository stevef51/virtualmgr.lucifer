﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'AspNetUser'.
    /// </summary>
    [Serializable]
    public partial class AspNetUserDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the AccessFailedCount property that maps to the Entity AspNetUser</summary>
        public virtual System.Int32 AccessFailedCount { get; set; }

        /// <summary>Get or set the ConcurrencyStamp property that maps to the Entity AspNetUser</summary>
        public virtual System.String ConcurrencyStamp { get; set; }

        /// <summary>Get or set the Email property that maps to the Entity AspNetUser</summary>
        public virtual System.String Email { get; set; }

        /// <summary>Get or set the EmailConfirmed property that maps to the Entity AspNetUser</summary>
        public virtual System.Boolean EmailConfirmed { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity AspNetUser</summary>
        public virtual System.String Id { get; set; }

        /// <summary>Get or set the LockoutEnabled property that maps to the Entity AspNetUser</summary>
        public virtual System.Boolean LockoutEnabled { get; set; }

        /// <summary>Get or set the LockoutEnd property that maps to the Entity AspNetUser</summary>
        public virtual System.DateTimeOffset? LockoutEnd { get; set; }

        /// <summary>Get or set the NormalizedEmail property that maps to the Entity AspNetUser</summary>
        public virtual System.String NormalizedEmail { get; set; }

        /// <summary>Get or set the NormalizedUserName property that maps to the Entity AspNetUser</summary>
        public virtual System.String NormalizedUserName { get; set; }

        /// <summary>Get or set the PasswordHash property that maps to the Entity AspNetUser</summary>
        public virtual System.String PasswordHash { get; set; }

        /// <summary>Get or set the PhoneNumber property that maps to the Entity AspNetUser</summary>
        public virtual System.String PhoneNumber { get; set; }

        /// <summary>Get or set the PhoneNumberConfirmed property that maps to the Entity AspNetUser</summary>
        public virtual System.Boolean PhoneNumberConfirmed { get; set; }

        /// <summary>Get or set the SecurityStamp property that maps to the Entity AspNetUser</summary>
        public virtual System.String SecurityStamp { get; set; }

        /// <summary>Get or set the TwoFactorEnabled property that maps to the Entity AspNetUser</summary>
        public virtual System.Boolean TwoFactorEnabled { get; set; }

        /// <summary>Get or set the UserName property that maps to the Entity AspNetUser</summary>
        public virtual System.String UserName { get; set; }
        
        #endregion

        #region Related Field Public Properties



		
		public virtual IList< AspNetUserClaimDTO> AspNetUserClaims
		{
			get; set;
		}



		
		public virtual IList< AspNetUserLoginDTO> AspNetUserLogins
		{
			get; set;
		}



		
		public virtual IList< AspNetUserRoleDTO> AspNetUserRoles
		{
			get; set;
		}



		
		public virtual IList< AspNetUserTokenDTO> AspNetUserTokens
		{
			get; set;
		}



		
		public virtual IList< UserDataDTO> UserDatas
		{
			get; set;
		}









        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public AspNetUserDTO()
        {
			
			
			this.AspNetUserClaims = new List< AspNetUserClaimDTO>();
			
			
			
			this.AspNetUserLogins = new List< AspNetUserLoginDTO>();
			
			
			
			this.AspNetUserRoles = new List< AspNetUserRoleDTO>();
			
			
			
			this.AspNetUserTokens = new List< AspNetUserTokenDTO>();
			
			
			
			this.UserDatas = new List< UserDataDTO>();
			
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 