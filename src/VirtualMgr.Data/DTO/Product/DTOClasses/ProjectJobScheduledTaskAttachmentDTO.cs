﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'ProjectJobScheduledTaskAttachment'.
    /// </summary>
    [Serializable]
    public partial class ProjectJobScheduledTaskAttachmentDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the MediaId property that maps to the Entity ProjectJobScheduledTaskAttachment</summary>
        public virtual System.Guid MediaId { get; set; }

        /// <summary>Get or set the ProjectJobScheduledTaskId property that maps to the Entity ProjectJobScheduledTaskAttachment</summary>
        public virtual System.Guid ProjectJobScheduledTaskId { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual ProjectJobScheduledTaskDTO ProjectJobScheduledTask {get; set;}



		public virtual MediaDTO Medium {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public ProjectJobScheduledTaskAttachmentDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 