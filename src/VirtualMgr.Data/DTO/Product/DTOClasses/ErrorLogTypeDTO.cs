﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'ErrorLogType'.
    /// </summary>
    [Serializable]
    public partial class ErrorLogTypeDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the Id property that maps to the Entity ErrorLogType</summary>
        public virtual System.Int32 Id { get; set; }

        /// <summary>Get or set the Message property that maps to the Entity ErrorLogType</summary>
        public virtual System.String Message { get; set; }

        /// <summary>Get or set the Source property that maps to the Entity ErrorLogType</summary>
        public virtual System.String Source { get; set; }

        /// <summary>Get or set the StackTrace property that maps to the Entity ErrorLogType</summary>
        public virtual System.String StackTrace { get; set; }

        /// <summary>Get or set the StackTraceHash property that maps to the Entity ErrorLogType</summary>
        public virtual System.Int32 StackTraceHash { get; set; }

        /// <summary>Get or set the Type property that maps to the Entity ErrorLogType</summary>
        public virtual System.String Type { get; set; }
        
        #endregion

        #region Related Field Public Properties



		
		public virtual IList< ErrorLogItemDTO> ErrorLogItems
		{
			get; set;
		}









        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public ErrorLogTypeDTO()
        {
			
			
			this.ErrorLogItems = new List< ErrorLogItemDTO>();
			
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 