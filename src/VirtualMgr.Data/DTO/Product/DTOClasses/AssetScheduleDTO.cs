﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'AssetSchedule'.
    /// </summary>
    [Serializable]
    public partial class AssetScheduleDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the AssetId property that maps to the Entity AssetSchedule</summary>
        public virtual System.Int32 AssetId { get; set; }

        /// <summary>Get or set the AssetTypeCalendarId property that maps to the Entity AssetSchedule</summary>
        public virtual System.Int32 AssetTypeCalendarId { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity AssetSchedule</summary>
        public virtual System.Int32 Id { get; set; }

        /// <summary>Get or set the LastRunUtc property that maps to the Entity AssetSchedule</summary>
        public virtual System.DateTime? LastRunUtc { get; set; }

        /// <summary>Get or set the NextRunUtc property that maps to the Entity AssetSchedule</summary>
        public virtual System.DateTime? NextRunUtc { get; set; }

        /// <summary>Get or set the OverrideStartTimeLocal property that maps to the Entity AssetSchedule</summary>
        public virtual System.TimeSpan? OverrideStartTimeLocal { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual AssetDTO Asset {get; set;}



		public virtual AssetTypeCalendarDTO AssetTypeCalendar {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public AssetScheduleDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 