﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConceptCave.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'RosterEventCondition'.
    /// </summary>
    [Serializable]
    public partial class RosterEventConditionDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the Condition property that maps to the Entity RosterEventCondition</summary>
        public virtual System.String Condition { get; set; }

        /// <summary>Get or set the Data property that maps to the Entity RosterEventCondition</summary>
        public virtual System.String Data { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity RosterEventCondition</summary>
        public virtual System.Int32 Id { get; set; }

        /// <summary>Get or set the NextRunTime property that maps to the Entity RosterEventCondition</summary>
        public virtual System.DateTime? NextRunTime { get; set; }

        /// <summary>Get or set the RosterId property that maps to the Entity RosterEventCondition</summary>
        public virtual System.Int32 RosterId { get; set; }

        /// <summary>Get or set the RosterProjectJobTaskEvents property that maps to the Entity RosterEventCondition</summary>
        public virtual System.Int32 RosterProjectJobTaskEvents { get; set; }
        
        #endregion

        #region Related Field Public Properties



		
		public virtual IList< RosterEventConditionActionDTO> RosterEventConditionActions
		{
			get; set;
		}





		public virtual RosterDTO Roster {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public RosterEventConditionDTO()
        {
			
			
			this.RosterEventConditionActions = new List< RosterEventConditionActionDTO>();
			
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 