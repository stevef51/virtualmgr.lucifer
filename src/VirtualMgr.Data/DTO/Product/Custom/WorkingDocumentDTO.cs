﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConceptCave.DTO.DTOClasses
{
    public partial class WorkingDocumentDTO
    {
        /// <summary>
        /// Set by the WorkingDocumentRepository when the entity is created based on the publishing group resource InMemoryOnly value.
        /// Players should use the Session to persist state if this is true as the WorkingDocumentRepository will not save the WorkingDocument to the database
        /// if this value is true.
        /// </summary>
        public bool InMemoryOnly { get; set; }
    }
}
