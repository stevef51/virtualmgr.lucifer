﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.DTO.DTOClasses
{
    public partial class ProjectJobTaskDTO
    {
        public string MinimumDurationExpression { get; set; }
    }
}
