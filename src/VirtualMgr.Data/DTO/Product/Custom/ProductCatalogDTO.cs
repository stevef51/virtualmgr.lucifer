﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConceptCave.DTO.DTOClasses
{
    public partial class ProductCatalogDTO
    {
        public bool Excluded { get; set; }
        public string InheritedRule { get; set; }
    }
}
