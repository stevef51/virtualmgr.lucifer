﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace VirtualMgr.Central.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'TenantHost'.
    /// </summary>
    [Serializable]
    public partial class TenantHostDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the BackupWebAppName property that maps to the Entity TenantHost</summary>
        public virtual System.String BackupWebAppName { get; set; }

        /// <summary>Get or set the ConnectionStringTemplate property that maps to the Entity TenantHost</summary>
        public virtual System.String ConnectionStringTemplate { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity TenantHost</summary>
        public virtual System.Int32 Id { get; set; }

        /// <summary>Get or set the Location property that maps to the Entity TenantHost</summary>
        public virtual System.String Location { get; set; }

        /// <summary>Get or set the Name property that maps to the Entity TenantHost</summary>
        public virtual System.String Name { get; set; }

        /// <summary>Get or set the SqlServerName property that maps to the Entity TenantHost</summary>
        public virtual System.String SqlServerName { get; set; }

        /// <summary>Get or set the TrafficManagerName property that maps to the Entity TenantHost</summary>
        public virtual System.String TrafficManagerName { get; set; }

        /// <summary>Get or set the WebAppName property that maps to the Entity TenantHost</summary>
        public virtual System.String WebAppName { get; set; }

        /// <summary>Get or set the WebAppResourceGroup property that maps to the Entity TenantHost</summary>
        public virtual System.String WebAppResourceGroup { get; set; }
        
        #endregion

        #region Related Field Public Properties



		
		public virtual IList< TenantDTO> Tenants
		{
			get; set;
		}









        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public TenantHostDTO()
        {
			
			
			this.Tenants = new List< TenantDTO>();
			
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 