﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace VirtualMgr.Central.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'TenantProfile'.
    /// </summary>
    [Serializable]
    public partial class TenantProfileDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the CanClone property that maps to the Entity TenantProfile</summary>
        public virtual System.Boolean CanClone { get; set; }

        /// <summary>Get or set the CanDelete property that maps to the Entity TenantProfile</summary>
        public virtual System.Boolean CanDelete { get; set; }

        /// <summary>Get or set the CanDisable property that maps to the Entity TenantProfile</summary>
        public virtual System.Boolean CanDisable { get; set; }

        /// <summary>Get or set the CanUpdateSchema property that maps to the Entity TenantProfile</summary>
        public virtual System.Boolean CanUpdateSchema { get; set; }

        /// <summary>Get or set the CreateAlertMessage property that maps to the Entity TenantProfile</summary>
        public virtual System.String CreateAlertMessage { get; set; }

        /// <summary>Get or set the DatabaseAppend property that maps to the Entity TenantProfile</summary>
        public virtual System.String DatabaseAppend { get; set; }

        /// <summary>Get or set the Description property that maps to the Entity TenantProfile</summary>
        public virtual System.String Description { get; set; }

        /// <summary>Get or set the DomainAppend property that maps to the Entity TenantProfile</summary>
        public virtual System.String DomainAppend { get; set; }

        /// <summary>Get or set the EmailEnabled property that maps to the Entity TenantProfile</summary>
        public virtual System.Boolean EmailEnabled { get; set; }

        /// <summary>Get or set the EmailWhitelist property that maps to the Entity TenantProfile</summary>
        public virtual System.String EmailWhitelist { get; set; }

        /// <summary>Get or set the Hidden property that maps to the Entity TenantProfile</summary>
        public virtual System.Boolean Hidden { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity TenantProfile</summary>
        public virtual System.Int32 Id { get; set; }

        /// <summary>Get or set the Name property that maps to the Entity TenantProfile</summary>
        public virtual System.String Name { get; set; }
        
        #endregion

        #region Related Field Public Properties



		
		public virtual IList< TenantDTO> TargetTenants
		{
			get; set;
		}



		
		public virtual IList< TenantDTO> Tenants
		{
			get; set;
		}









        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public TenantProfileDTO()
        {
			
			
			this.TargetTenants = new List< TenantDTO>();
			
			
			
			this.Tenants = new List< TenantDTO>();
			
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 