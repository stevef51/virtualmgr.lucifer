﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace VirtualMgr.Central.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'UpdateScript'.
    /// </summary>
    [Serializable]
    public partial class UpdateScriptDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the AppliedDate property that maps to the Entity UpdateScript</summary>
        public virtual System.DateTime AppliedDate { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity UpdateScript</summary>
        public virtual System.Int32 Id { get; set; }

        /// <summary>Get or set the ResultText property that maps to the Entity UpdateScript</summary>
        public virtual System.String ResultText { get; set; }

        /// <summary>Get or set the ScriptName property that maps to the Entity UpdateScript</summary>
        public virtual System.String ScriptName { get; set; }

        /// <summary>Get or set the ScriptTextHashcode property that maps to the Entity UpdateScript</summary>
        public virtual System.Int32 ScriptTextHashcode { get; set; }

        /// <summary>Get or set the Success property that maps to the Entity UpdateScript</summary>
        public virtual System.Boolean Success { get; set; }
        
        #endregion

        #region Related Field Public Properties









        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public UpdateScriptDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 