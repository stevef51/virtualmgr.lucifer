﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace VirtualMgr.Central.DTO.DTOClasses
{
    
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END 
    /// <summary>
    /// DTO class for the entity 'Tenant'.
    /// </summary>
    [Serializable]
    public partial class TenantDTO
    {
        #region Entity Field Public Properties

        /// <summary>Get or set the BillingAddress property that maps to the Entity Tenant</summary>
        public virtual System.String BillingAddress { get; set; }

        /// <summary>Get or set the BillingCompanyName property that maps to the Entity Tenant</summary>
        public virtual System.String BillingCompanyName { get; set; }

        /// <summary>Get or set the ConnectionString property that maps to the Entity Tenant</summary>
        public virtual System.String ConnectionString { get; set; }

        /// <summary>Get or set the Currency property that maps to the Entity Tenant</summary>
        public virtual System.String Currency { get; set; }

        /// <summary>Get or set the EmailFromAddress property that maps to the Entity Tenant</summary>
        public virtual System.String EmailFromAddress { get; set; }

        /// <summary>Get or set the Enabled property that maps to the Entity Tenant</summary>
        public virtual System.Boolean Enabled { get; set; }

        /// <summary>Get or set the EnableDatawareHousePush property that maps to the Entity Tenant</summary>
        public virtual System.Boolean EnableDatawareHousePush { get; set; }

        /// <summary>Get or set the HostName property that maps to the Entity Tenant</summary>
        public virtual System.String HostName { get; set; }

        /// <summary>Get or set the Id property that maps to the Entity Tenant</summary>
        public virtual System.Int32 Id { get; set; }

        /// <summary>Get or set the InvoiceIdPrefix property that maps to the Entity Tenant</summary>
        public virtual System.String InvoiceIdPrefix { get; set; }

        /// <summary>Get or set the LuciferRelease property that maps to the Entity Tenant</summary>
        public virtual System.String LuciferRelease { get; set; }

        /// <summary>Get or set the Name property that maps to the Entity Tenant</summary>
        public virtual System.String Name { get; set; }

        /// <summary>Get or set the TargetProfileId property that maps to the Entity Tenant</summary>
        public virtual System.Int32? TargetProfileId { get; set; }

        /// <summary>Get or set the TenantHostId property that maps to the Entity Tenant</summary>
        public virtual System.Int32? TenantHostId { get; set; }

        /// <summary>Get or set the TenantProfileId property that maps to the Entity Tenant</summary>
        public virtual System.Int32? TenantProfileId { get; set; }

        /// <summary>Get or set the TimeZone property that maps to the Entity Tenant</summary>
        public virtual System.String TimeZone { get; set; }
        
        #endregion

        #region Related Field Public Properties





		public virtual TenantHostDTO TenantHost {get; set;}



		public virtual TenantProfileDTO TargetProfile {get; set;}



		public virtual TenantProfileDTO TenantProfile {get; set;}







        #endregion

        #region Custom Fields
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomFieldCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
		
		public bool __IsNew {get; set;}

        #region Ctors

        /// <summary>
        /// CTor
        /// </summary>
        public TenantDTO()
        {
			
			
			
        }

        #endregion

        #region Custom DTO code
        
        // __LLBLGENPRO_USER_CODE_REGION_START CustomDTOCode
        // __LLBLGENPRO_USER_CODE_REGION_END
        #endregion
    }
} 