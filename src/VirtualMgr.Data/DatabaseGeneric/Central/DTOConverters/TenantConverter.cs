﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using VirtualMgr.Central.Data;
using VirtualMgr.Central.Data.EntityClasses;
using VirtualMgr.Central.DTO.DTOClasses;

namespace VirtualMgr.Central.Data.DTOConverters
{
	public static class TenantConverter
	{
	
		public static TenantEntity ToEntity(this TenantDTO dto)
		{
			return dto.ToEntity(new TenantEntity(), new Dictionary<object, object>());
		}

		public static TenantEntity ToEntity(this TenantDTO dto, TenantEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static TenantEntity ToEntity(this TenantDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new TenantEntity(), cache);
		}
	
		public static TenantDTO ToDTO(this TenantEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static TenantEntity ToEntity(this TenantDTO dto, TenantEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (TenantEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			if (dto.BillingAddress == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)TenantFieldIndex.BillingAddress, "");
				
			}
			
			newEnt.BillingAddress = dto.BillingAddress;
			
			

			
			
			if (dto.BillingCompanyName == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)TenantFieldIndex.BillingCompanyName, "");
				
			}
			
			newEnt.BillingCompanyName = dto.BillingCompanyName;
			
			

			
			
			if (dto.ConnectionString == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)TenantFieldIndex.ConnectionString, "");
				
			}
			
			newEnt.ConnectionString = dto.ConnectionString;
			
			

			
			
			newEnt.Currency = dto.Currency;
			
			

			
			
			newEnt.EmailFromAddress = dto.EmailFromAddress;
			
			

			
			
			newEnt.Enabled = dto.Enabled;
			
			

			
			
			newEnt.EnableDatawareHousePush = dto.EnableDatawareHousePush;
			
			

			
			
			if (dto.HostName == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)TenantFieldIndex.HostName, "");
				
			}
			
			newEnt.HostName = dto.HostName;
			
			

			
			
			newEnt.Id = dto.Id;
			
			
			

			
			
			if (dto.InvoiceIdPrefix == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)TenantFieldIndex.InvoiceIdPrefix, "");
				
			}
			
			newEnt.InvoiceIdPrefix = dto.InvoiceIdPrefix;
			
			

			
			
			newEnt.LuciferRelease = dto.LuciferRelease;
			
			

			
			
			newEnt.Name = dto.Name;
			
			

			
			
			if (dto.TargetProfileId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)TenantFieldIndex.TargetProfileId, default(System.Int32));
				
			}
			
			newEnt.TargetProfileId = dto.TargetProfileId;
			
			

			
			
			if (dto.TenantHostId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)TenantFieldIndex.TenantHostId, default(System.Int32));
				
			}
			
			newEnt.TenantHostId = dto.TenantHostId;
			
			

			
			
			if (dto.TenantProfileId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)TenantFieldIndex.TenantProfileId, default(System.Int32));
				
			}
			
			newEnt.TenantProfileId = dto.TenantProfileId;
			
			

			
			
			newEnt.TimeZone = dto.TimeZone;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.TenantHost = dto.TenantHost.ToEntity(cache);
			
			newEnt.TargetProfile = dto.TargetProfile.ToEntity(cache);
			
			newEnt.TenantProfile = dto.TenantProfile.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static TenantDTO ToDTO(this TenantEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (TenantDTO)cache[ent];
			
			var newDTO = new TenantDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.BillingAddress = ent.BillingAddress;
			

			newDTO.BillingCompanyName = ent.BillingCompanyName;
			

			newDTO.ConnectionString = ent.ConnectionString;
			

			newDTO.Currency = ent.Currency;
			

			newDTO.EmailFromAddress = ent.EmailFromAddress;
			

			newDTO.Enabled = ent.Enabled;
			

			newDTO.EnableDatawareHousePush = ent.EnableDatawareHousePush;
			

			newDTO.HostName = ent.HostName;
			

			newDTO.Id = ent.Id;
			

			newDTO.InvoiceIdPrefix = ent.InvoiceIdPrefix;
			

			newDTO.LuciferRelease = ent.LuciferRelease;
			

			newDTO.Name = ent.Name;
			

			newDTO.TargetProfileId = ent.TargetProfileId;
			

			newDTO.TenantHostId = ent.TenantHostId;
			

			newDTO.TenantProfileId = ent.TenantProfileId;
			

			newDTO.TimeZone = ent.TimeZone;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.TenantHost = ent.TenantHost.ToDTO(cache);
			
			newDTO.TargetProfile = ent.TargetProfile.ToDTO(cache);
			
			newDTO.TenantProfile = ent.TenantProfile.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}