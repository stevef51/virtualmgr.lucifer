﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using VirtualMgr.Central.Data;
using VirtualMgr.Central.Data.EntityClasses;
using VirtualMgr.Central.DTO.DTOClasses;

namespace VirtualMgr.Central.Data.DTOConverters
{
	public static class TenantProfileConverter
	{
	
		public static TenantProfileEntity ToEntity(this TenantProfileDTO dto)
		{
			return dto.ToEntity(new TenantProfileEntity(), new Dictionary<object, object>());
		}

		public static TenantProfileEntity ToEntity(this TenantProfileDTO dto, TenantProfileEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static TenantProfileEntity ToEntity(this TenantProfileDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new TenantProfileEntity(), cache);
		}
	
		public static TenantProfileDTO ToDTO(this TenantProfileEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static TenantProfileEntity ToEntity(this TenantProfileDTO dto, TenantProfileEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (TenantProfileEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.CanClone = dto.CanClone;
			
			

			
			
			newEnt.CanDelete = dto.CanDelete;
			
			

			
			
			newEnt.CanDisable = dto.CanDisable;
			
			

			
			
			newEnt.CanUpdateSchema = dto.CanUpdateSchema;
			
			

			
			
			if (dto.CreateAlertMessage == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)TenantProfileFieldIndex.CreateAlertMessage, "");
				
			}
			
			newEnt.CreateAlertMessage = dto.CreateAlertMessage;
			
			

			
			
			if (dto.DatabaseAppend == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)TenantProfileFieldIndex.DatabaseAppend, "");
				
			}
			
			newEnt.DatabaseAppend = dto.DatabaseAppend;
			
			

			
			
			newEnt.Description = dto.Description;
			
			

			
			
			if (dto.DomainAppend == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)TenantProfileFieldIndex.DomainAppend, "");
				
			}
			
			newEnt.DomainAppend = dto.DomainAppend;
			
			

			
			
			newEnt.EmailEnabled = dto.EmailEnabled;
			
			

			
			
			newEnt.EmailWhitelist = dto.EmailWhitelist;
			
			

			
			
			newEnt.Hidden = dto.Hidden;
			
			

			
			
			newEnt.Id = dto.Id;
			
			
			

			
			
			newEnt.Name = dto.Name;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			
			
			foreach (var related in dto.TargetTenants)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.TargetTenants.Contains(relatedEntity))
				{
					newEnt.TargetTenants.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.Tenants)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.Tenants.Contains(relatedEntity))
				{
					newEnt.Tenants.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static TenantProfileDTO ToDTO(this TenantProfileEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (TenantProfileDTO)cache[ent];
			
			var newDTO = new TenantProfileDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.CanClone = ent.CanClone;
			

			newDTO.CanDelete = ent.CanDelete;
			

			newDTO.CanDisable = ent.CanDisable;
			

			newDTO.CanUpdateSchema = ent.CanUpdateSchema;
			

			newDTO.CreateAlertMessage = ent.CreateAlertMessage;
			

			newDTO.DatabaseAppend = ent.DatabaseAppend;
			

			newDTO.Description = ent.Description;
			

			newDTO.DomainAppend = ent.DomainAppend;
			

			newDTO.EmailEnabled = ent.EmailEnabled;
			

			newDTO.EmailWhitelist = ent.EmailWhitelist;
			

			newDTO.Hidden = ent.Hidden;
			

			newDTO.Id = ent.Id;
			

			newDTO.Name = ent.Name;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			
			
			foreach (var related in ent.TargetTenants)
			{
				newDTO.TargetTenants.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.Tenants)
			{
				newDTO.Tenants.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}