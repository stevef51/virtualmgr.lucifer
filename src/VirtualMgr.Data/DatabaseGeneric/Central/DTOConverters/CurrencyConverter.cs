﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using VirtualMgr.Central.Data;
using VirtualMgr.Central.Data.EntityClasses;
using VirtualMgr.Central.DTO.DTOClasses;

namespace VirtualMgr.Central.Data.DTOConverters
{
	public static class CurrencyConverter
	{
	
		public static CurrencyEntity ToEntity(this CurrencyDTO dto)
		{
			return dto.ToEntity(new CurrencyEntity(), new Dictionary<object, object>());
		}

		public static CurrencyEntity ToEntity(this CurrencyDTO dto, CurrencyEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static CurrencyEntity ToEntity(this CurrencyDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new CurrencyEntity(), cache);
		}
	
		public static CurrencyDTO ToDTO(this CurrencyEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static CurrencyEntity ToEntity(this CurrencyDTO dto, CurrencyEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (CurrencyEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Code = dto.Code;
			
			

			
			
			newEnt.Description = dto.Description;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static CurrencyDTO ToDTO(this CurrencyEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (CurrencyDTO)cache[ent];
			
			var newDTO = new CurrencyDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Code = ent.Code;
			

			newDTO.Description = ent.Description;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}