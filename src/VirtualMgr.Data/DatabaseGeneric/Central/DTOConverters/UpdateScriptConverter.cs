﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using VirtualMgr.Central.Data;
using VirtualMgr.Central.Data.EntityClasses;
using VirtualMgr.Central.DTO.DTOClasses;

namespace VirtualMgr.Central.Data.DTOConverters
{
	public static class UpdateScriptConverter
	{
	
		public static UpdateScriptEntity ToEntity(this UpdateScriptDTO dto)
		{
			return dto.ToEntity(new UpdateScriptEntity(), new Dictionary<object, object>());
		}

		public static UpdateScriptEntity ToEntity(this UpdateScriptDTO dto, UpdateScriptEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static UpdateScriptEntity ToEntity(this UpdateScriptDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new UpdateScriptEntity(), cache);
		}
	
		public static UpdateScriptDTO ToDTO(this UpdateScriptEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static UpdateScriptEntity ToEntity(this UpdateScriptDTO dto, UpdateScriptEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (UpdateScriptEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.AppliedDate = dto.AppliedDate;
			
			

			
			
			newEnt.Id = dto.Id;
			
			
			

			
			
			newEnt.ResultText = dto.ResultText;
			
			

			
			
			newEnt.ScriptName = dto.ScriptName;
			
			

			
			
			newEnt.ScriptTextHashcode = dto.ScriptTextHashcode;
			
			

			
			
			newEnt.Success = dto.Success;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static UpdateScriptDTO ToDTO(this UpdateScriptEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (UpdateScriptDTO)cache[ent];
			
			var newDTO = new UpdateScriptDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.AppliedDate = ent.AppliedDate;
			

			newDTO.Id = ent.Id;
			

			newDTO.ResultText = ent.ResultText;
			

			newDTO.ScriptName = ent.ScriptName;
			

			newDTO.ScriptTextHashcode = ent.ScriptTextHashcode;
			

			newDTO.Success = ent.Success;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}