﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using VirtualMgr.Central.Data;
using VirtualMgr.Central.Data.EntityClasses;
using VirtualMgr.Central.DTO.DTOClasses;

namespace VirtualMgr.Central.Data.DTOConverters
{
	public static class TenantHostConverter
	{
	
		public static TenantHostEntity ToEntity(this TenantHostDTO dto)
		{
			return dto.ToEntity(new TenantHostEntity(), new Dictionary<object, object>());
		}

		public static TenantHostEntity ToEntity(this TenantHostDTO dto, TenantHostEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static TenantHostEntity ToEntity(this TenantHostDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new TenantHostEntity(), cache);
		}
	
		public static TenantHostDTO ToDTO(this TenantHostEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static TenantHostEntity ToEntity(this TenantHostDTO dto, TenantHostEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (TenantHostEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			if (dto.BackupWebAppName == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)TenantHostFieldIndex.BackupWebAppName, "");
				
			}
			
			newEnt.BackupWebAppName = dto.BackupWebAppName;
			
			

			
			
			newEnt.ConnectionStringTemplate = dto.ConnectionStringTemplate;
			
			

			
			
			newEnt.Id = dto.Id;
			
			
			

			
			
			newEnt.Location = dto.Location;
			
			

			
			
			newEnt.Name = dto.Name;
			
			

			
			
			newEnt.SqlServerName = dto.SqlServerName;
			
			

			
			
			if (dto.TrafficManagerName == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)TenantHostFieldIndex.TrafficManagerName, "");
				
			}
			
			newEnt.TrafficManagerName = dto.TrafficManagerName;
			
			

			
			
			newEnt.WebAppName = dto.WebAppName;
			
			

			
			
			newEnt.WebAppResourceGroup = dto.WebAppResourceGroup;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			
			
			foreach (var related in dto.Tenants)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.Tenants.Contains(relatedEntity))
				{
					newEnt.Tenants.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static TenantHostDTO ToDTO(this TenantHostEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (TenantHostDTO)cache[ent];
			
			var newDTO = new TenantHostDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.BackupWebAppName = ent.BackupWebAppName;
			

			newDTO.ConnectionStringTemplate = ent.ConnectionStringTemplate;
			

			newDTO.Id = ent.Id;
			

			newDTO.Location = ent.Location;
			

			newDTO.Name = ent.Name;
			

			newDTO.SqlServerName = ent.SqlServerName;
			

			newDTO.TrafficManagerName = ent.TrafficManagerName;
			

			newDTO.WebAppName = ent.WebAppName;
			

			newDTO.WebAppResourceGroup = ent.WebAppResourceGroup;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			
			
			foreach (var related in ent.Tenants)
			{
				newDTO.Tenants.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}