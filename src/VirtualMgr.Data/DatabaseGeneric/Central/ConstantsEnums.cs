﻿//////////////////////////////////////////////////////////////
// <auto-generated>This code was generated by LLBLGen Pro 5.5.</auto-generated>
//////////////////////////////////////////////////////////////
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
//////////////////////////////////////////////////////////////
using System;

namespace VirtualMgr.Central.Data
{


	/// <summary>Enum definition for all the entity types defined in this namespace. Used by the entityfields factory.</summary>
	public enum EntityType
	{
		///<summary>Currency</summary>
		CurrencyEntity,
		///<summary>Tenant</summary>
		TenantEntity,
		///<summary>TenantHost</summary>
		TenantHostEntity,
		///<summary>TenantProfile</summary>
		TenantProfileEntity,
		///<summary>UpdateScript</summary>
		UpdateScriptEntity
	}



	// __LLBLGENPRO_USER_CODE_REGION_START CustomUserConstants
	// __LLBLGENPRO_USER_CODE_REGION_END

}

