﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class AssetContextDataConverter
	{
	
		public static AssetContextDataEntity ToEntity(this AssetContextDataDTO dto)
		{
			return dto.ToEntity(new AssetContextDataEntity(), new Dictionary<object, object>());
		}

		public static AssetContextDataEntity ToEntity(this AssetContextDataDTO dto, AssetContextDataEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static AssetContextDataEntity ToEntity(this AssetContextDataDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new AssetContextDataEntity(), cache);
		}
	
		public static AssetContextDataDTO ToDTO(this AssetContextDataEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static AssetContextDataEntity ToEntity(this AssetContextDataDTO dto, AssetContextDataEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (AssetContextDataEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.AssetId = dto.AssetId;
			
			

			
			
			newEnt.AssetTypeContextPublishedResourceId = dto.AssetTypeContextPublishedResourceId;
			
			

			
			
			newEnt.CompletedWorkingDocumentId = dto.CompletedWorkingDocumentId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.Asset = dto.Asset.ToEntity(cache);
			
			newEnt.AssetTypeContextPublishedResource = dto.AssetTypeContextPublishedResource.ToEntity(cache);
			
			newEnt.CompletedWorkingDocumentFact = dto.CompletedWorkingDocumentFact.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static AssetContextDataDTO ToDTO(this AssetContextDataEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (AssetContextDataDTO)cache[ent];
			
			var newDTO = new AssetContextDataDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.AssetId = ent.AssetId;
			

			newDTO.AssetTypeContextPublishedResourceId = ent.AssetTypeContextPublishedResourceId;
			

			newDTO.CompletedWorkingDocumentId = ent.CompletedWorkingDocumentId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.Asset = ent.Asset.ToDTO(cache);
			
			newDTO.AssetTypeContextPublishedResource = ent.AssetTypeContextPublishedResource.ToDTO(cache);
			
			newDTO.CompletedWorkingDocumentFact = ent.CompletedWorkingDocumentFact.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}