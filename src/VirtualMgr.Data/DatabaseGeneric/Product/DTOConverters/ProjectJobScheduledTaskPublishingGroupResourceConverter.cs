﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class ProjectJobScheduledTaskPublishingGroupResourceConverter
	{
	
		public static ProjectJobScheduledTaskPublishingGroupResourceEntity ToEntity(this ProjectJobScheduledTaskPublishingGroupResourceDTO dto)
		{
			return dto.ToEntity(new ProjectJobScheduledTaskPublishingGroupResourceEntity(), new Dictionary<object, object>());
		}

		public static ProjectJobScheduledTaskPublishingGroupResourceEntity ToEntity(this ProjectJobScheduledTaskPublishingGroupResourceDTO dto, ProjectJobScheduledTaskPublishingGroupResourceEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static ProjectJobScheduledTaskPublishingGroupResourceEntity ToEntity(this ProjectJobScheduledTaskPublishingGroupResourceDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new ProjectJobScheduledTaskPublishingGroupResourceEntity(), cache);
		}
	
		public static ProjectJobScheduledTaskPublishingGroupResourceDTO ToDTO(this ProjectJobScheduledTaskPublishingGroupResourceEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static ProjectJobScheduledTaskPublishingGroupResourceEntity ToEntity(this ProjectJobScheduledTaskPublishingGroupResourceDTO dto, ProjectJobScheduledTaskPublishingGroupResourceEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (ProjectJobScheduledTaskPublishingGroupResourceEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.AllowMultipleInstances = dto.AllowMultipleInstances;
			
			

			
			
			newEnt.IsMandatory = dto.IsMandatory;
			
			

			
			
			newEnt.ProjectJobScheduledTaskId = dto.ProjectJobScheduledTaskId;
			
			

			
			
			newEnt.PublishingGroupResourceId = dto.PublishingGroupResourceId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.ProjectJobScheduledTask = dto.ProjectJobScheduledTask.ToEntity(cache);
			
			newEnt.PublishingGroupResource = dto.PublishingGroupResource.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static ProjectJobScheduledTaskPublishingGroupResourceDTO ToDTO(this ProjectJobScheduledTaskPublishingGroupResourceEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (ProjectJobScheduledTaskPublishingGroupResourceDTO)cache[ent];
			
			var newDTO = new ProjectJobScheduledTaskPublishingGroupResourceDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.AllowMultipleInstances = ent.AllowMultipleInstances;
			

			newDTO.IsMandatory = ent.IsMandatory;
			

			newDTO.ProjectJobScheduledTaskId = ent.ProjectJobScheduledTaskId;
			

			newDTO.PublishingGroupResourceId = ent.PublishingGroupResourceId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.ProjectJobScheduledTask = ent.ProjectJobScheduledTask.ToDTO(cache);
			
			newDTO.PublishingGroupResource = ent.PublishingGroupResource.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}