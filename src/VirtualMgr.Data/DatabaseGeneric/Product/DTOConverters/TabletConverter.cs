﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class TabletConverter
	{
	
		public static TabletEntity ToEntity(this TabletDTO dto)
		{
			return dto.ToEntity(new TabletEntity(), new Dictionary<object, object>());
		}

		public static TabletEntity ToEntity(this TabletDTO dto, TabletEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static TabletEntity ToEntity(this TabletDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new TabletEntity(), cache);
		}
	
		public static TabletDTO ToDTO(this TabletEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static TabletEntity ToEntity(this TabletDTO dto, TabletEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (TabletEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Archived = dto.Archived;
			
			

			
			
			newEnt.Id = dto.Id;
			
			
			

			
			
			if (dto.Name == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)TabletFieldIndex.Name, "");
				
			}
			
			newEnt.Name = dto.Name;
			
			

			
			
			if (dto.SiteId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)TabletFieldIndex.SiteId, default(System.Guid));
				
			}
			
			newEnt.SiteId = dto.SiteId;
			
			

			
			
			if (dto.TabletProfileId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)TabletFieldIndex.TabletProfileId, default(System.Int32));
				
			}
			
			newEnt.TabletProfileId = dto.TabletProfileId;
			
			

			
			
			if (dto.Uuid == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)TabletFieldIndex.Uuid, "");
				
			}
			
			newEnt.Uuid = dto.Uuid;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.TabletProfile = dto.TabletProfile.ToEntity(cache);
			
			newEnt.TabletUuid = dto.TabletUuid.ToEntity(cache);
			
			
			
			foreach (var related in dto.TabletUuids)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.TabletUuids.Contains(relatedEntity))
				{
					newEnt.TabletUuids.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static TabletDTO ToDTO(this TabletEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (TabletDTO)cache[ent];
			
			var newDTO = new TabletDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Archived = ent.Archived;
			

			newDTO.Id = ent.Id;
			

			newDTO.Name = ent.Name;
			

			newDTO.SiteId = ent.SiteId;
			

			newDTO.TabletProfileId = ent.TabletProfileId;
			

			newDTO.Uuid = ent.Uuid;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.TabletProfile = ent.TabletProfile.ToDTO(cache);
			
			newDTO.TabletUuid = ent.TabletUuid.ToDTO(cache);
			
			
			
			foreach (var related in ent.TabletUuids)
			{
				newDTO.TabletUuids.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}