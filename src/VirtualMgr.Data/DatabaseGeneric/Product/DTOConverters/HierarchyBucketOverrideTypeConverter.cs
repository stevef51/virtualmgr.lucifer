﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class HierarchyBucketOverrideTypeConverter
	{
	
		public static HierarchyBucketOverrideTypeEntity ToEntity(this HierarchyBucketOverrideTypeDTO dto)
		{
			return dto.ToEntity(new HierarchyBucketOverrideTypeEntity(), new Dictionary<object, object>());
		}

		public static HierarchyBucketOverrideTypeEntity ToEntity(this HierarchyBucketOverrideTypeDTO dto, HierarchyBucketOverrideTypeEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static HierarchyBucketOverrideTypeEntity ToEntity(this HierarchyBucketOverrideTypeDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new HierarchyBucketOverrideTypeEntity(), cache);
		}
	
		public static HierarchyBucketOverrideTypeDTO ToDTO(this HierarchyBucketOverrideTypeEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static HierarchyBucketOverrideTypeEntity ToEntity(this HierarchyBucketOverrideTypeDTO dto, HierarchyBucketOverrideTypeEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (HierarchyBucketOverrideTypeEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Id = dto.Id;
			
			

			
			
			newEnt.Name = dto.Name;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			
			
			foreach (var related in dto.TimesheetItems)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.TimesheetItems.Contains(relatedEntity))
				{
					newEnt.TimesheetItems.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.HierarchyBucketOverrides)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.HierarchyBucketOverrides.Contains(relatedEntity))
				{
					newEnt.HierarchyBucketOverrides.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static HierarchyBucketOverrideTypeDTO ToDTO(this HierarchyBucketOverrideTypeEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (HierarchyBucketOverrideTypeDTO)cache[ent];
			
			var newDTO = new HierarchyBucketOverrideTypeDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Id = ent.Id;
			

			newDTO.Name = ent.Name;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			
			
			foreach (var related in ent.TimesheetItems)
			{
				newDTO.TimesheetItems.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.HierarchyBucketOverrides)
			{
				newDTO.HierarchyBucketOverrides.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}