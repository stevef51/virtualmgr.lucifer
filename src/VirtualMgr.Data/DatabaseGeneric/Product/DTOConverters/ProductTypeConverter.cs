﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class ProductTypeConverter
	{
	
		public static ProductTypeEntity ToEntity(this ProductTypeDTO dto)
		{
			return dto.ToEntity(new ProductTypeEntity(), new Dictionary<object, object>());
		}

		public static ProductTypeEntity ToEntity(this ProductTypeDTO dto, ProductTypeEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static ProductTypeEntity ToEntity(this ProductTypeDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new ProductTypeEntity(), cache);
		}
	
		public static ProductTypeDTO ToDTO(this ProductTypeEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static ProductTypeEntity ToEntity(this ProductTypeDTO dto, ProductTypeEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (ProductTypeEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Id = dto.Id;
			
			
			

			
			
			newEnt.Name = dto.Name;
			
			

			
			
			newEnt.Units = dto.Units;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			
			
			foreach (var related in dto.Products)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.Products.Contains(relatedEntity))
				{
					newEnt.Products.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static ProductTypeDTO ToDTO(this ProductTypeEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (ProductTypeDTO)cache[ent];
			
			var newDTO = new ProductTypeDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Id = ent.Id;
			

			newDTO.Name = ent.Name;
			

			newDTO.Units = ent.Units;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			
			
			foreach (var related in ent.Products)
			{
				newDTO.Products.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}