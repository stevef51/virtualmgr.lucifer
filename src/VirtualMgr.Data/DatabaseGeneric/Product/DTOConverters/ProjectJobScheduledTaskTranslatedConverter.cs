﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class ProjectJobScheduledTaskTranslatedConverter
	{
	
		public static ProjectJobScheduledTaskTranslatedEntity ToEntity(this ProjectJobScheduledTaskTranslatedDTO dto)
		{
			return dto.ToEntity(new ProjectJobScheduledTaskTranslatedEntity(), new Dictionary<object, object>());
		}

		public static ProjectJobScheduledTaskTranslatedEntity ToEntity(this ProjectJobScheduledTaskTranslatedDTO dto, ProjectJobScheduledTaskTranslatedEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static ProjectJobScheduledTaskTranslatedEntity ToEntity(this ProjectJobScheduledTaskTranslatedDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new ProjectJobScheduledTaskTranslatedEntity(), cache);
		}
	
		public static ProjectJobScheduledTaskTranslatedDTO ToDTO(this ProjectJobScheduledTaskTranslatedEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static ProjectJobScheduledTaskTranslatedEntity ToEntity(this ProjectJobScheduledTaskTranslatedDTO dto, ProjectJobScheduledTaskTranslatedEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (ProjectJobScheduledTaskTranslatedEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.CultureName = dto.CultureName;
			
			

			
			
			if (dto.Description == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobScheduledTaskTranslatedFieldIndex.Description, "");
				
			}
			
			newEnt.Description = dto.Description;
			
			

			
			
			newEnt.Id = dto.Id;
			
			

			
			
			newEnt.Name = dto.Name;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.ProjectJobScheduledTask = dto.ProjectJobScheduledTask.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static ProjectJobScheduledTaskTranslatedDTO ToDTO(this ProjectJobScheduledTaskTranslatedEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (ProjectJobScheduledTaskTranslatedDTO)cache[ent];
			
			var newDTO = new ProjectJobScheduledTaskTranslatedDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.CultureName = ent.CultureName;
			

			newDTO.Description = ent.Description;
			

			newDTO.Id = ent.Id;
			

			newDTO.Name = ent.Name;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.ProjectJobScheduledTask = ent.ProjectJobScheduledTask.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}