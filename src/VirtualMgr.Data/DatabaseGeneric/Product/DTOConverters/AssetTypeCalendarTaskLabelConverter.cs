﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class AssetTypeCalendarTaskLabelConverter
	{
	
		public static AssetTypeCalendarTaskLabelEntity ToEntity(this AssetTypeCalendarTaskLabelDTO dto)
		{
			return dto.ToEntity(new AssetTypeCalendarTaskLabelEntity(), new Dictionary<object, object>());
		}

		public static AssetTypeCalendarTaskLabelEntity ToEntity(this AssetTypeCalendarTaskLabelDTO dto, AssetTypeCalendarTaskLabelEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static AssetTypeCalendarTaskLabelEntity ToEntity(this AssetTypeCalendarTaskLabelDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new AssetTypeCalendarTaskLabelEntity(), cache);
		}
	
		public static AssetTypeCalendarTaskLabelDTO ToDTO(this AssetTypeCalendarTaskLabelEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static AssetTypeCalendarTaskLabelEntity ToEntity(this AssetTypeCalendarTaskLabelDTO dto, AssetTypeCalendarTaskLabelEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (AssetTypeCalendarTaskLabelEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.AssetTypeCalendarTaskId = dto.AssetTypeCalendarTaskId;
			
			

			
			
			newEnt.LabelId = dto.LabelId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.AssetTypeCalendarTask = dto.AssetTypeCalendarTask.ToEntity(cache);
			
			newEnt.Label = dto.Label.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static AssetTypeCalendarTaskLabelDTO ToDTO(this AssetTypeCalendarTaskLabelEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (AssetTypeCalendarTaskLabelDTO)cache[ent];
			
			var newDTO = new AssetTypeCalendarTaskLabelDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.AssetTypeCalendarTaskId = ent.AssetTypeCalendarTaskId;
			

			newDTO.LabelId = ent.LabelId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.AssetTypeCalendarTask = ent.AssetTypeCalendarTask.ToDTO(cache);
			
			newDTO.Label = ent.Label.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}