﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class VersionConverter
	{
	
		public static VersionEntity ToEntity(this VersionDTO dto)
		{
			return dto.ToEntity(new VersionEntity(), new Dictionary<object, object>());
		}

		public static VersionEntity ToEntity(this VersionDTO dto, VersionEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static VersionEntity ToEntity(this VersionDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new VersionEntity(), cache);
		}
	
		public static VersionDTO ToDTO(this VersionEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static VersionEntity ToEntity(this VersionDTO dto, VersionEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (VersionEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Id = dto.Id;
			
			

			
			
			newEnt.Version = dto.Version;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static VersionDTO ToDTO(this VersionEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (VersionDTO)cache[ent];
			
			var newDTO = new VersionDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Id = ent.Id;
			

			newDTO.Version = ent.Version;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}