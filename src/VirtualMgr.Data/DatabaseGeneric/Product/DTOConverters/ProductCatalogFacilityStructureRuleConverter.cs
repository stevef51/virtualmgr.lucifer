﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class ProductCatalogFacilityStructureRuleConverter
	{
	
		public static ProductCatalogFacilityStructureRuleEntity ToEntity(this ProductCatalogFacilityStructureRuleDTO dto)
		{
			return dto.ToEntity(new ProductCatalogFacilityStructureRuleEntity(), new Dictionary<object, object>());
		}

		public static ProductCatalogFacilityStructureRuleEntity ToEntity(this ProductCatalogFacilityStructureRuleDTO dto, ProductCatalogFacilityStructureRuleEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static ProductCatalogFacilityStructureRuleEntity ToEntity(this ProductCatalogFacilityStructureRuleDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new ProductCatalogFacilityStructureRuleEntity(), cache);
		}
	
		public static ProductCatalogFacilityStructureRuleDTO ToDTO(this ProductCatalogFacilityStructureRuleEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static ProductCatalogFacilityStructureRuleEntity ToEntity(this ProductCatalogFacilityStructureRuleDTO dto, ProductCatalogFacilityStructureRuleEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (ProductCatalogFacilityStructureRuleEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Exclude = dto.Exclude;
			
			

			
			
			newEnt.FacilityStructureId = dto.FacilityStructureId;
			
			

			
			
			newEnt.ProductCatalogId = dto.ProductCatalogId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.FacilityStructure = dto.FacilityStructure.ToEntity(cache);
			
			newEnt.ProductCatalog = dto.ProductCatalog.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static ProductCatalogFacilityStructureRuleDTO ToDTO(this ProductCatalogFacilityStructureRuleEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (ProductCatalogFacilityStructureRuleDTO)cache[ent];
			
			var newDTO = new ProductCatalogFacilityStructureRuleDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Exclude = ent.Exclude;
			

			newDTO.FacilityStructureId = ent.FacilityStructureId;
			

			newDTO.ProductCatalogId = ent.ProductCatalogId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.FacilityStructure = ent.FacilityStructure.ToDTO(cache);
			
			newDTO.ProductCatalog = ent.ProductCatalog.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}