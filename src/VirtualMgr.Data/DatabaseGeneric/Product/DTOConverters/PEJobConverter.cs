﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class PEJobConverter
	{
	
		public static PEJobEntity ToEntity(this PEJobDTO dto)
		{
			return dto.ToEntity(new PEJobEntity(), new Dictionary<object, object>());
		}

		public static PEJobEntity ToEntity(this PEJobDTO dto, PEJobEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static PEJobEntity ToEntity(this PEJobDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new PEJobEntity(), cache);
		}
	
		public static PEJobDTO ToDTO(this PEJobEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static PEJobEntity ToEntity(this PEJobDTO dto, PEJobEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (PEJobEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Archived = dto.Archived;
			
			

			
			
			newEnt.Cronexpression = dto.Cronexpression;
			
			

			
			
			newEnt.Enabled = dto.Enabled;
			
			

			
			
			newEnt.Id = dto.Id;
			
			
			

			
			
			if (dto.JobData == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)PEJobFieldIndex.JobData, "");
				
			}
			
			newEnt.JobData = dto.JobData;
			
			

			
			
			newEnt.JobType = dto.JobType;
			
			

			
			
			if (dto.LastRunUtc == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)PEJobFieldIndex.LastRunUtc, default(System.DateTime));
				
			}
			
			newEnt.LastRunUtc = dto.LastRunUtc;
			
			

			
			
			newEnt.Name = dto.Name;
			
			

			
			
			if (dto.NextRunUtc == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)PEJobFieldIndex.NextRunUtc, default(System.DateTime));
				
			}
			
			newEnt.NextRunUtc = dto.NextRunUtc;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			
			
			foreach (var related in dto.JobHistories)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.JobHistories.Contains(relatedEntity))
				{
					newEnt.JobHistories.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static PEJobDTO ToDTO(this PEJobEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (PEJobDTO)cache[ent];
			
			var newDTO = new PEJobDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Archived = ent.Archived;
			

			newDTO.Cronexpression = ent.Cronexpression;
			

			newDTO.Enabled = ent.Enabled;
			

			newDTO.Id = ent.Id;
			

			newDTO.JobData = ent.JobData;
			

			newDTO.JobType = ent.JobType;
			

			newDTO.LastRunUtc = ent.LastRunUtc;
			

			newDTO.Name = ent.Name;
			

			newDTO.NextRunUtc = ent.NextRunUtc;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			
			
			foreach (var related in ent.JobHistories)
			{
				newDTO.JobHistories.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}