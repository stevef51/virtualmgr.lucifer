﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class CompletedPresentedUploadMediaFactValueConverter
	{
	
		public static CompletedPresentedUploadMediaFactValueEntity ToEntity(this CompletedPresentedUploadMediaFactValueDTO dto)
		{
			return dto.ToEntity(new CompletedPresentedUploadMediaFactValueEntity(), new Dictionary<object, object>());
		}

		public static CompletedPresentedUploadMediaFactValueEntity ToEntity(this CompletedPresentedUploadMediaFactValueDTO dto, CompletedPresentedUploadMediaFactValueEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static CompletedPresentedUploadMediaFactValueEntity ToEntity(this CompletedPresentedUploadMediaFactValueDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new CompletedPresentedUploadMediaFactValueEntity(), cache);
		}
	
		public static CompletedPresentedUploadMediaFactValueDTO ToDTO(this CompletedPresentedUploadMediaFactValueEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static CompletedPresentedUploadMediaFactValueEntity ToEntity(this CompletedPresentedUploadMediaFactValueDTO dto, CompletedPresentedUploadMediaFactValueEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (CompletedPresentedUploadMediaFactValueEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.CompletedWorkingDocumentPresentedFactId = dto.CompletedWorkingDocumentPresentedFactId;
			
			

			
			
			newEnt.MediaId = dto.MediaId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.CompletedWorkingDocumentPresentedFact = dto.CompletedWorkingDocumentPresentedFact.ToEntity(cache);
			
			newEnt.Medium = dto.Medium.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static CompletedPresentedUploadMediaFactValueDTO ToDTO(this CompletedPresentedUploadMediaFactValueEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (CompletedPresentedUploadMediaFactValueDTO)cache[ent];
			
			var newDTO = new CompletedPresentedUploadMediaFactValueDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.CompletedWorkingDocumentPresentedFactId = ent.CompletedWorkingDocumentPresentedFactId;
			

			newDTO.MediaId = ent.MediaId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.CompletedWorkingDocumentPresentedFact = ent.CompletedWorkingDocumentPresentedFact.ToDTO(cache);
			
			newDTO.Medium = ent.Medium.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}