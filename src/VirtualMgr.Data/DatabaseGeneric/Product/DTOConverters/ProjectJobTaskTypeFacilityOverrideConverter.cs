﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class ProjectJobTaskTypeFacilityOverrideConverter
	{
	
		public static ProjectJobTaskTypeFacilityOverrideEntity ToEntity(this ProjectJobTaskTypeFacilityOverrideDTO dto)
		{
			return dto.ToEntity(new ProjectJobTaskTypeFacilityOverrideEntity(), new Dictionary<object, object>());
		}

		public static ProjectJobTaskTypeFacilityOverrideEntity ToEntity(this ProjectJobTaskTypeFacilityOverrideDTO dto, ProjectJobTaskTypeFacilityOverrideEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static ProjectJobTaskTypeFacilityOverrideEntity ToEntity(this ProjectJobTaskTypeFacilityOverrideDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new ProjectJobTaskTypeFacilityOverrideEntity(), cache);
		}
	
		public static ProjectJobTaskTypeFacilityOverrideDTO ToDTO(this ProjectJobTaskTypeFacilityOverrideEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static ProjectJobTaskTypeFacilityOverrideEntity ToEntity(this ProjectJobTaskTypeFacilityOverrideDTO dto, ProjectJobTaskTypeFacilityOverrideEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (ProjectJobTaskTypeFacilityOverrideEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			if (dto.EstimatedDurationSeconds == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobTaskTypeFacilityOverrideFieldIndex.EstimatedDurationSeconds, default(System.Int32));
				
			}
			
			newEnt.EstimatedDurationSeconds = dto.EstimatedDurationSeconds;
			
			

			
			
			newEnt.FacilityStructureId = dto.FacilityStructureId;
			
			

			
			
			if (dto.MinimumDuration == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobTaskTypeFacilityOverrideFieldIndex.MinimumDuration, "");
				
			}
			
			newEnt.MinimumDuration = dto.MinimumDuration;
			
			

			
			
			newEnt.TaskTypeId = dto.TaskTypeId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.FacilityStructure = dto.FacilityStructure.ToEntity(cache);
			
			newEnt.ProjectJobTaskType = dto.ProjectJobTaskType.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static ProjectJobTaskTypeFacilityOverrideDTO ToDTO(this ProjectJobTaskTypeFacilityOverrideEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (ProjectJobTaskTypeFacilityOverrideDTO)cache[ent];
			
			var newDTO = new ProjectJobTaskTypeFacilityOverrideDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.EstimatedDurationSeconds = ent.EstimatedDurationSeconds;
			

			newDTO.FacilityStructureId = ent.FacilityStructureId;
			

			newDTO.MinimumDuration = ent.MinimumDuration;
			

			newDTO.TaskTypeId = ent.TaskTypeId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.FacilityStructure = ent.FacilityStructure.ToDTO(cache);
			
			newDTO.ProjectJobTaskType = ent.ProjectJobTaskType.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}