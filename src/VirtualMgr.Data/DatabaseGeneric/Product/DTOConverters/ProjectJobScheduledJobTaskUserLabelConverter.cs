﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class ProjectJobScheduledJobTaskUserLabelConverter
	{
	
		public static ProjectJobScheduledJobTaskUserLabelEntity ToEntity(this ProjectJobScheduledJobTaskUserLabelDTO dto)
		{
			return dto.ToEntity(new ProjectJobScheduledJobTaskUserLabelEntity(), new Dictionary<object, object>());
		}

		public static ProjectJobScheduledJobTaskUserLabelEntity ToEntity(this ProjectJobScheduledJobTaskUserLabelDTO dto, ProjectJobScheduledJobTaskUserLabelEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static ProjectJobScheduledJobTaskUserLabelEntity ToEntity(this ProjectJobScheduledJobTaskUserLabelDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new ProjectJobScheduledJobTaskUserLabelEntity(), cache);
		}
	
		public static ProjectJobScheduledJobTaskUserLabelDTO ToDTO(this ProjectJobScheduledJobTaskUserLabelEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static ProjectJobScheduledJobTaskUserLabelEntity ToEntity(this ProjectJobScheduledJobTaskUserLabelDTO dto, ProjectJobScheduledJobTaskUserLabelEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (ProjectJobScheduledJobTaskUserLabelEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.LabelId = dto.LabelId;
			
			

			
			
			newEnt.ProjectJobScheduledTaskId = dto.ProjectJobScheduledTaskId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.ProjectJobScheduledTask = dto.ProjectJobScheduledTask.ToEntity(cache);
			
			newEnt.Label = dto.Label.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static ProjectJobScheduledJobTaskUserLabelDTO ToDTO(this ProjectJobScheduledJobTaskUserLabelEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (ProjectJobScheduledJobTaskUserLabelDTO)cache[ent];
			
			var newDTO = new ProjectJobScheduledJobTaskUserLabelDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.LabelId = ent.LabelId;
			

			newDTO.ProjectJobScheduledTaskId = ent.ProjectJobScheduledTaskId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.ProjectJobScheduledTask = ent.ProjectJobScheduledTask.ToDTO(cache);
			
			newDTO.Label = ent.Label.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}