﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class MediaConverter
	{
	
		public static MediaEntity ToEntity(this MediaDTO dto)
		{
			return dto.ToEntity(new MediaEntity(), new Dictionary<object, object>());
		}

		public static MediaEntity ToEntity(this MediaDTO dto, MediaEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static MediaEntity ToEntity(this MediaDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new MediaEntity(), cache);
		}
	
		public static MediaDTO ToDTO(this MediaEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static MediaEntity ToEntity(this MediaDTO dto, MediaEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (MediaEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			if (dto.CreatedBy == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)MediaFieldIndex.CreatedBy, default(System.Guid));
				
			}
			
			newEnt.CreatedBy = dto.CreatedBy;
			
			

			
			
			newEnt.DateCreated = dto.DateCreated;
			
			

			
			
			newEnt.DateVersionCreated = dto.DateVersionCreated;
			
			

			
			
			if (dto.Description == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)MediaFieldIndex.Description, "");
				
			}
			
			newEnt.Description = dto.Description;
			
			

			
			
			newEnt.Filename = dto.Filename;
			
			

			
			
			newEnt.Id = dto.Id;
			
			

			
			
			newEnt.Name = dto.Name;
			
			

			
			
			if (dto.PreviewCount == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)MediaFieldIndex.PreviewCount, default(System.Int32));
				
			}
			
			newEnt.PreviewCount = dto.PreviewCount;
			
			

			
			
			newEnt.SortOrder = dto.SortOrder;
			
			

			
			
			newEnt.Type = dto.Type;
			
			

			
			
			newEnt.UseVersioning = dto.UseVersioning;
			
			

			
			
			newEnt.Version = dto.Version;
			
			

			
			
			if (dto.WorkingDocumentId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)MediaFieldIndex.WorkingDocumentId, default(System.Guid));
				
			}
			
			newEnt.WorkingDocumentId = dto.WorkingDocumentId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			newEnt.MediaData = dto.MediaData.ToEntity(cache);
			
			newEnt.MediaPolicy = dto.MediaPolicy.ToEntity(cache);
			
			
			
			newEnt.UserData = dto.UserData.ToEntity(cache);
			
			
			
			foreach (var related in dto.FacilityStructures)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.FacilityStructures.Contains(relatedEntity))
				{
					newEnt.FacilityStructures.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.CompletedPresentedUploadMediaFactValues)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.CompletedPresentedUploadMediaFactValues.Contains(relatedEntity))
				{
					newEnt.CompletedPresentedUploadMediaFactValues.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ProjectJobScheduledTaskAttachments)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProjectJobScheduledTaskAttachments.Contains(relatedEntity))
				{
					newEnt.ProjectJobScheduledTaskAttachments.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ProjectJobTaskAttachments)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProjectJobTaskAttachments.Contains(relatedEntity))
				{
					newEnt.ProjectJobTaskAttachments.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ProjectJobTaskMedias)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProjectJobTaskMedias.Contains(relatedEntity))
				{
					newEnt.ProjectJobTaskMedias.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.TaskSignatures)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.TaskSignatures.Contains(relatedEntity))
				{
					newEnt.TaskSignatures.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.TaskSignatures_)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.TaskSignatures_.Contains(relatedEntity))
				{
					newEnt.TaskSignatures_.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ProjectJobTaskTypes)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProjectJobTaskTypes.Contains(relatedEntity))
				{
					newEnt.ProjectJobTaskTypes.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.LabelMedias)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.LabelMedias.Contains(relatedEntity))
				{
					newEnt.LabelMedias.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.MediaFolderFiles)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.MediaFolderFiles.Contains(relatedEntity))
				{
					newEnt.MediaFolderFiles.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.MediaPreview)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.MediaPreview.Contains(relatedEntity))
				{
					newEnt.MediaPreview.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.MediaRoles)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.MediaRoles.Contains(relatedEntity))
				{
					newEnt.MediaRoles.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.Translated)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.Translated.Contains(relatedEntity))
				{
					newEnt.Translated.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.MediaVersions)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.MediaVersions.Contains(relatedEntity))
				{
					newEnt.MediaVersions.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.MediaViewings)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.MediaViewings.Contains(relatedEntity))
				{
					newEnt.MediaViewings.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.PublishingGroupResources)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.PublishingGroupResources.Contains(relatedEntity))
				{
					newEnt.PublishingGroupResources.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.OrderMedia)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.OrderMedia.Contains(relatedEntity))
				{
					newEnt.OrderMedia.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.Products)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.Products.Contains(relatedEntity))
				{
					newEnt.Products.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.UserDatas)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.UserDatas.Contains(relatedEntity))
				{
					newEnt.UserDatas.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.UserLogs)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.UserLogs.Contains(relatedEntity))
				{
					newEnt.UserLogs.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.UserMedias)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.UserMedias.Contains(relatedEntity))
				{
					newEnt.UserMedias.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static MediaDTO ToDTO(this MediaEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (MediaDTO)cache[ent];
			
			var newDTO = new MediaDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.CreatedBy = ent.CreatedBy;
			

			newDTO.DateCreated = ent.DateCreated;
			

			newDTO.DateVersionCreated = ent.DateVersionCreated;
			

			newDTO.Description = ent.Description;
			

			newDTO.Filename = ent.Filename;
			

			newDTO.Id = ent.Id;
			

			newDTO.Name = ent.Name;
			

			newDTO.PreviewCount = ent.PreviewCount;
			

			newDTO.SortOrder = ent.SortOrder;
			

			newDTO.Type = ent.Type;
			

			newDTO.UseVersioning = ent.UseVersioning;
			

			newDTO.Version = ent.Version;
			

			newDTO.WorkingDocumentId = ent.WorkingDocumentId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			newDTO.MediaData = ent.MediaData.ToDTO(cache);
			
			newDTO.MediaPolicy = ent.MediaPolicy.ToDTO(cache);
			
			
			
			newDTO.UserData = ent.UserData.ToDTO(cache);
			
			
			
			foreach (var related in ent.FacilityStructures)
			{
				newDTO.FacilityStructures.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.CompletedPresentedUploadMediaFactValues)
			{
				newDTO.CompletedPresentedUploadMediaFactValues.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ProjectJobScheduledTaskAttachments)
			{
				newDTO.ProjectJobScheduledTaskAttachments.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ProjectJobTaskAttachments)
			{
				newDTO.ProjectJobTaskAttachments.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ProjectJobTaskMedias)
			{
				newDTO.ProjectJobTaskMedias.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.TaskSignatures)
			{
				newDTO.TaskSignatures.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.TaskSignatures_)
			{
				newDTO.TaskSignatures_.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ProjectJobTaskTypes)
			{
				newDTO.ProjectJobTaskTypes.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.LabelMedias)
			{
				newDTO.LabelMedias.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.MediaFolderFiles)
			{
				newDTO.MediaFolderFiles.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.MediaPreview)
			{
				newDTO.MediaPreview.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.MediaRoles)
			{
				newDTO.MediaRoles.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.Translated)
			{
				newDTO.Translated.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.MediaVersions)
			{
				newDTO.MediaVersions.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.MediaViewings)
			{
				newDTO.MediaViewings.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.PublishingGroupResources)
			{
				newDTO.PublishingGroupResources.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.OrderMedia)
			{
				newDTO.OrderMedia.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.Products)
			{
				newDTO.Products.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.UserDatas)
			{
				newDTO.UserDatas.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.UserLogs)
			{
				newDTO.UserLogs.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.UserMedias)
			{
				newDTO.UserMedias.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}