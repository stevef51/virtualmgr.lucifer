﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class FactTaskConverter
	{
	
		public static FactTaskEntity ToEntity(this FactTaskDTO dto)
		{
			return dto.ToEntity(new FactTaskEntity(), new Dictionary<object, object>());
		}

		public static FactTaskEntity ToEntity(this FactTaskDTO dto, FactTaskEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static FactTaskEntity ToEntity(this FactTaskDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new FactTaskEntity(), cache);
		}
	
		public static FactTaskDTO ToDTO(this FactTaskEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static FactTaskEntity ToEntity(this FactTaskDTO dto, FactTaskEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (FactTaskEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			if (dto.ActualDuration == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)FactTaskFieldIndex.ActualDuration, default(System.Decimal));
				
			}
			
			newEnt.ActualDuration = dto.ActualDuration;
			
			

			
			
			
			

			
			
			
			

			
			
			newEnt.AllActivitiesEstimatedDuration = dto.AllActivitiesEstimatedDuration;
			
			

			
			
			newEnt.CompletedActivitiesEstimatedDuration = dto.CompletedActivitiesEstimatedDuration;
			
			

			
			
			if (dto.CompletionNotes == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)FactTaskFieldIndex.CompletionNotes, "");
				
			}
			
			newEnt.CompletionNotes = dto.CompletionNotes;
			
			

			
			
			if (dto.Description == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)FactTaskFieldIndex.Description, "");
				
			}
			
			newEnt.Description = dto.Description;
			
			

			
			
			if (dto.DimLocalDateCreated == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)FactTaskFieldIndex.DimLocalDateCreated, default(System.DateTime));
				
			}
			
			newEnt.DimLocalDateCreated = dto.DimLocalDateCreated;
			
			

			
			
			if (dto.DimLocalDateTimeCreated == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)FactTaskFieldIndex.DimLocalDateTimeCreated, default(System.DateTime));
				
			}
			
			newEnt.DimLocalDateTimeCreated = dto.DimLocalDateTimeCreated;
			
			

			
			
			if (dto.DimUtcDateCreated == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)FactTaskFieldIndex.DimUtcDateCreated, default(System.DateTime));
				
			}
			
			newEnt.DimUtcDateCreated = dto.DimUtcDateCreated;
			
			

			
			
			if (dto.DimUtcDateTimeCreated == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)FactTaskFieldIndex.DimUtcDateTimeCreated, default(System.DateTime));
				
			}
			
			newEnt.DimUtcDateTimeCreated = dto.DimUtcDateTimeCreated;
			
			

			
			
			if (dto.EstimatedDurationSeconds == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)FactTaskFieldIndex.EstimatedDurationSeconds, default(System.Decimal));
				
			}
			
			newEnt.EstimatedDurationSeconds = dto.EstimatedDurationSeconds;
			
			

			
			
			newEnt.HasOrders = dto.HasOrders;
			
			

			
			
			if (dto.HierarchyBucketId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)FactTaskFieldIndex.HierarchyBucketId, default(System.Int32));
				
			}
			
			newEnt.HierarchyBucketId = dto.HierarchyBucketId;
			
			

			
			
			if (dto.HierarchyBucketPkId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)FactTaskFieldIndex.HierarchyBucketPkId, "");
				
			}
			
			newEnt.HierarchyBucketPkId = dto.HierarchyBucketPkId;
			
			

			
			
			newEnt.Id = dto.Id;
			
			

			
			
			if (dto.InactiveDuration == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)FactTaskFieldIndex.InactiveDuration, default(System.Decimal));
				
			}
			
			newEnt.InactiveDuration = dto.InactiveDuration;
			
			

			
			
			if (dto.IsScheduled == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)FactTaskFieldIndex.IsScheduled, default(System.Boolean));
				
			}
			
			newEnt.IsScheduled = dto.IsScheduled;
			
			

			
			
			newEnt.Level = dto.Level;
			
			

			
			
			if (dto.LocalDateTimeCompleted == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)FactTaskFieldIndex.LocalDateTimeCompleted, default(System.DateTime));
				
			}
			
			newEnt.LocalDateTimeCompleted = dto.LocalDateTimeCompleted;
			
			

			
			
			if (dto.LocalDateTimeCreated == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)FactTaskFieldIndex.LocalDateTimeCreated, default(System.DateTime));
				
			}
			
			newEnt.LocalDateTimeCreated = dto.LocalDateTimeCreated;
			
			

			
			
			if (dto.LocalDateTimeStarted == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)FactTaskFieldIndex.LocalDateTimeStarted, default(System.DateTime));
				
			}
			
			newEnt.LocalDateTimeStarted = dto.LocalDateTimeStarted;
			
			

			
			
			if (dto.LocalStatusDate == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)FactTaskFieldIndex.LocalStatusDate, default(System.DateTime));
				
			}
			
			newEnt.LocalStatusDate = dto.LocalStatusDate;
			
			

			
			
			if (dto.MinimumDuration == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)FactTaskFieldIndex.MinimumDuration, default(System.Decimal));
				
			}
			
			newEnt.MinimumDuration = dto.MinimumDuration;
			
			

			
			
			newEnt.Name = dto.Name;
			
			

			
			
			newEnt.OriginalOwnerId = dto.OriginalOwnerId;
			
			

			
			
			newEnt.OriginalOwnerPkId = dto.OriginalOwnerPkId;
			
			

			
			
			if (dto.OriginalTaskTypeId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)FactTaskFieldIndex.OriginalTaskTypeId, default(System.Guid));
				
			}
			
			newEnt.OriginalTaskTypeId = dto.OriginalTaskTypeId;
			
			

			
			
			if (dto.OriginalTaskTypePkId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)FactTaskFieldIndex.OriginalTaskTypePkId, "");
				
			}
			
			newEnt.OriginalTaskTypePkId = dto.OriginalTaskTypePkId;
			
			

			
			
			newEnt.OwnerId = dto.OwnerId;
			
			

			
			
			newEnt.OwnerPkId = dto.OwnerPkId;
			
			

			
			
			newEnt.PhotoDuringSignature = dto.PhotoDuringSignature;
			
			

			
			
			newEnt.PkId = dto.PkId;
			
			

			
			
			if (dto.ProjectJobTaskGroupId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)FactTaskFieldIndex.ProjectJobTaskGroupId, default(System.Guid));
				
			}
			
			newEnt.ProjectJobTaskGroupId = dto.ProjectJobTaskGroupId;
			
			

			
			
			if (dto.ProjectJobTaskGroupPkId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)FactTaskFieldIndex.ProjectJobTaskGroupPkId, "");
				
			}
			
			newEnt.ProjectJobTaskGroupPkId = dto.ProjectJobTaskGroupPkId;
			
			

			
			
			if (dto.ReferenceNumber == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)FactTaskFieldIndex.ReferenceNumber, "");
				
			}
			
			newEnt.ReferenceNumber = dto.ReferenceNumber;
			
			

			
			
			newEnt.RequireSignature = dto.RequireSignature;
			
			

			
			
			if (dto.RoleId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)FactTaskFieldIndex.RoleId, default(System.Int32));
				
			}
			
			newEnt.RoleId = dto.RoleId;
			
			

			
			
			if (dto.RolePkId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)FactTaskFieldIndex.RolePkId, "");
				
			}
			
			newEnt.RolePkId = dto.RolePkId;
			
			

			
			
			newEnt.RosterId = dto.RosterId;
			
			

			
			
			newEnt.RosterPkId = dto.RosterPkId;
			
			

			
			
			if (dto.ScheduledOwnerId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)FactTaskFieldIndex.ScheduledOwnerId, default(System.Guid));
				
			}
			
			newEnt.ScheduledOwnerId = dto.ScheduledOwnerId;
			
			

			
			
			if (dto.ScheduledOwnerPkId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)FactTaskFieldIndex.ScheduledOwnerPkId, "");
				
			}
			
			newEnt.ScheduledOwnerPkId = dto.ScheduledOwnerPkId;
			
			

			
			
			if (dto.SiteId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)FactTaskFieldIndex.SiteId, default(System.Guid));
				
			}
			
			newEnt.SiteId = dto.SiteId;
			
			

			
			
			if (dto.SitePkId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)FactTaskFieldIndex.SitePkId, "");
				
			}
			
			newEnt.SitePkId = dto.SitePkId;
			
			

			
			
			newEnt.SortOrder = dto.SortOrder;
			
			

			
			
			newEnt.Status = dto.Status;
			
			

			
			
			if (dto.StatusId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)FactTaskFieldIndex.StatusId, default(System.Guid));
				
			}
			
			newEnt.StatusId = dto.StatusId;
			
			

			
			
			if (dto.StatusPkId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)FactTaskFieldIndex.StatusPkId, "");
				
			}
			
			newEnt.StatusPkId = dto.StatusPkId;
			
			

			
			
			newEnt.TaskTypeId = dto.TaskTypeId;
			
			

			
			
			newEnt.TaskTypePkId = dto.TaskTypePkId;
			
			

			
			
			newEnt.TenantId = dto.TenantId;
			
			

			
			
			
			

			
			
			
			

			
			
			if (dto.UtcDateTimeCompleted == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)FactTaskFieldIndex.UtcDateTimeCompleted, default(System.DateTime));
				
			}
			
			newEnt.UtcDateTimeCompleted = dto.UtcDateTimeCompleted;
			
			

			
			
			if (dto.UtcDateTimeCreated == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)FactTaskFieldIndex.UtcDateTimeCreated, default(System.DateTime));
				
			}
			
			newEnt.UtcDateTimeCreated = dto.UtcDateTimeCreated;
			
			

			
			
			if (dto.UtcDateTimeStarted == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)FactTaskFieldIndex.UtcDateTimeStarted, default(System.DateTime));
				
			}
			
			newEnt.UtcDateTimeStarted = dto.UtcDateTimeStarted;
			
			

			
			
			newEnt.UtcStatusDate = dto.UtcStatusDate;
			
			

			
			
			newEnt.WorkingDocumentCount = dto.WorkingDocumentCount;
			
			

			
			
			if (dto.WorkingGroupId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)FactTaskFieldIndex.WorkingGroupId, default(System.Guid));
				
			}
			
			newEnt.WorkingGroupId = dto.WorkingGroupId;
			
			

			
			
			if (dto.WorkingGroupPkId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)FactTaskFieldIndex.WorkingGroupPkId, "");
				
			}
			
			newEnt.WorkingGroupPkId = dto.WorkingGroupPkId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static FactTaskDTO ToDTO(this FactTaskEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (FactTaskDTO)cache[ent];
			
			var newDTO = new FactTaskDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.ActualDuration = ent.ActualDuration;
			

			newDTO.ActualVsEstimatedPercentage = ent.ActualVsEstimatedPercentage;
			

			newDTO.ActualVsEstimatedPercentile = ent.ActualVsEstimatedPercentile;
			

			newDTO.AllActivitiesEstimatedDuration = ent.AllActivitiesEstimatedDuration;
			

			newDTO.CompletedActivitiesEstimatedDuration = ent.CompletedActivitiesEstimatedDuration;
			

			newDTO.CompletionNotes = ent.CompletionNotes;
			

			newDTO.Description = ent.Description;
			

			newDTO.DimLocalDateCreated = ent.DimLocalDateCreated;
			

			newDTO.DimLocalDateTimeCreated = ent.DimLocalDateTimeCreated;
			

			newDTO.DimUtcDateCreated = ent.DimUtcDateCreated;
			

			newDTO.DimUtcDateTimeCreated = ent.DimUtcDateTimeCreated;
			

			newDTO.EstimatedDurationSeconds = ent.EstimatedDurationSeconds;
			

			newDTO.HasOrders = ent.HasOrders;
			

			newDTO.HierarchyBucketId = ent.HierarchyBucketId;
			

			newDTO.HierarchyBucketPkId = ent.HierarchyBucketPkId;
			

			newDTO.Id = ent.Id;
			

			newDTO.InactiveDuration = ent.InactiveDuration;
			

			newDTO.IsScheduled = ent.IsScheduled;
			

			newDTO.Level = ent.Level;
			

			newDTO.LocalDateTimeCompleted = ent.LocalDateTimeCompleted;
			

			newDTO.LocalDateTimeCreated = ent.LocalDateTimeCreated;
			

			newDTO.LocalDateTimeStarted = ent.LocalDateTimeStarted;
			

			newDTO.LocalStatusDate = ent.LocalStatusDate;
			

			newDTO.MinimumDuration = ent.MinimumDuration;
			

			newDTO.Name = ent.Name;
			

			newDTO.OriginalOwnerId = ent.OriginalOwnerId;
			

			newDTO.OriginalOwnerPkId = ent.OriginalOwnerPkId;
			

			newDTO.OriginalTaskTypeId = ent.OriginalTaskTypeId;
			

			newDTO.OriginalTaskTypePkId = ent.OriginalTaskTypePkId;
			

			newDTO.OwnerId = ent.OwnerId;
			

			newDTO.OwnerPkId = ent.OwnerPkId;
			

			newDTO.PhotoDuringSignature = ent.PhotoDuringSignature;
			

			newDTO.PkId = ent.PkId;
			

			newDTO.ProjectJobTaskGroupId = ent.ProjectJobTaskGroupId;
			

			newDTO.ProjectJobTaskGroupPkId = ent.ProjectJobTaskGroupPkId;
			

			newDTO.ReferenceNumber = ent.ReferenceNumber;
			

			newDTO.RequireSignature = ent.RequireSignature;
			

			newDTO.RoleId = ent.RoleId;
			

			newDTO.RolePkId = ent.RolePkId;
			

			newDTO.RosterId = ent.RosterId;
			

			newDTO.RosterPkId = ent.RosterPkId;
			

			newDTO.ScheduledOwnerId = ent.ScheduledOwnerId;
			

			newDTO.ScheduledOwnerPkId = ent.ScheduledOwnerPkId;
			

			newDTO.SiteId = ent.SiteId;
			

			newDTO.SitePkId = ent.SitePkId;
			

			newDTO.SortOrder = ent.SortOrder;
			

			newDTO.Status = ent.Status;
			

			newDTO.StatusId = ent.StatusId;
			

			newDTO.StatusPkId = ent.StatusPkId;
			

			newDTO.TaskTypeId = ent.TaskTypeId;
			

			newDTO.TaskTypePkId = ent.TaskTypePkId;
			

			newDTO.TenantId = ent.TenantId;
			

			newDTO.Tracking = ent.Tracking;
			

			newDTO.TrackingLong = ent.TrackingLong;
			

			newDTO.UtcDateTimeCompleted = ent.UtcDateTimeCompleted;
			

			newDTO.UtcDateTimeCreated = ent.UtcDateTimeCreated;
			

			newDTO.UtcDateTimeStarted = ent.UtcDateTimeStarted;
			

			newDTO.UtcStatusDate = ent.UtcStatusDate;
			

			newDTO.WorkingDocumentCount = ent.WorkingDocumentCount;
			

			newDTO.WorkingGroupId = ent.WorkingGroupId;
			

			newDTO.WorkingGroupPkId = ent.WorkingGroupPkId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}