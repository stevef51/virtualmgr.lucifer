﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class PublicPublishingGroupResourceConverter
	{
	
		public static PublicPublishingGroupResourceEntity ToEntity(this PublicPublishingGroupResourceDTO dto)
		{
			return dto.ToEntity(new PublicPublishingGroupResourceEntity(), new Dictionary<object, object>());
		}

		public static PublicPublishingGroupResourceEntity ToEntity(this PublicPublishingGroupResourceDTO dto, PublicPublishingGroupResourceEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static PublicPublishingGroupResourceEntity ToEntity(this PublicPublishingGroupResourceDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new PublicPublishingGroupResourceEntity(), cache);
		}
	
		public static PublicPublishingGroupResourceDTO ToDTO(this PublicPublishingGroupResourceEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static PublicPublishingGroupResourceEntity ToEntity(this PublicPublishingGroupResourceDTO dto, PublicPublishingGroupResourceEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (PublicPublishingGroupResourceEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.PublicTicketId = dto.PublicTicketId;
			
			

			
			
			newEnt.PublishingGroupResourceId = dto.PublishingGroupResourceId;
			
			

			
			
			newEnt.UserId = dto.UserId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.PublicTicket = dto.PublicTicket.ToEntity(cache);
			
			newEnt.PublishingGroupResource = dto.PublishingGroupResource.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static PublicPublishingGroupResourceDTO ToDTO(this PublicPublishingGroupResourceEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (PublicPublishingGroupResourceDTO)cache[ent];
			
			var newDTO = new PublicPublishingGroupResourceDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.PublicTicketId = ent.PublicTicketId;
			

			newDTO.PublishingGroupResourceId = ent.PublishingGroupResourceId;
			

			newDTO.UserId = ent.UserId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.PublicTicket = ent.PublicTicket.ToDTO(cache);
			
			newDTO.PublishingGroupResource = ent.PublishingGroupResource.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}