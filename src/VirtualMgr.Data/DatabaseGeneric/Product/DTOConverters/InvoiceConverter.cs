﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class InvoiceConverter
	{
	
		public static InvoiceEntity ToEntity(this InvoiceDTO dto)
		{
			return dto.ToEntity(new InvoiceEntity(), new Dictionary<object, object>());
		}

		public static InvoiceEntity ToEntity(this InvoiceDTO dto, InvoiceEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static InvoiceEntity ToEntity(this InvoiceDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new InvoiceEntity(), cache);
		}
	
		public static InvoiceDTO ToDTO(this InvoiceEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static InvoiceEntity ToEntity(this InvoiceDTO dto, InvoiceEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (InvoiceEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			if (dto.BillingAddress == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)InvoiceFieldIndex.BillingAddress, "");
				
			}
			
			newEnt.BillingAddress = dto.BillingAddress;
			
			

			
			
			if (dto.BillingCompanyName == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)InvoiceFieldIndex.BillingCompanyName, "");
				
			}
			
			newEnt.BillingCompanyName = dto.BillingCompanyName;
			
			

			
			
			newEnt.Currency = dto.Currency;
			
			

			
			
			if (dto.Description == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)InvoiceFieldIndex.Description, "");
				
			}
			
			newEnt.Description = dto.Description;
			
			

			
			
			newEnt.FromDateUtc = dto.FromDateUtc;
			
			

			
			
			if (dto.GeneratedByUserId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)InvoiceFieldIndex.GeneratedByUserId, default(System.Guid));
				
			}
			
			newEnt.GeneratedByUserId = dto.GeneratedByUserId;
			
			

			
			
			newEnt.GeneratedDateUtc = dto.GeneratedDateUtc;
			
			

			
			
			newEnt.Id = dto.Id;
			
			

			
			
			if (dto.IssuedDateUtc == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)InvoiceFieldIndex.IssuedDateUtc, default(System.DateTimeOffset));
				
			}
			
			newEnt.IssuedDateUtc = dto.IssuedDateUtc;
			
			

			
			
			if (dto.PaymentDateUtc == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)InvoiceFieldIndex.PaymentDateUtc, default(System.DateTimeOffset));
				
			}
			
			newEnt.PaymentDateUtc = dto.PaymentDateUtc;
			
			

			
			
			if (dto.PaymentReference == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)InvoiceFieldIndex.PaymentReference, "");
				
			}
			
			newEnt.PaymentReference = dto.PaymentReference;
			
			

			
			
			newEnt.ToDateUtc = dto.ToDateUtc;
			
			

			
			
			newEnt.TotalAmount = dto.TotalAmount;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			
			
			foreach (var related in dto.InvoiceLineItems)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.InvoiceLineItems.Contains(relatedEntity))
				{
					newEnt.InvoiceLineItems.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static InvoiceDTO ToDTO(this InvoiceEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (InvoiceDTO)cache[ent];
			
			var newDTO = new InvoiceDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.BillingAddress = ent.BillingAddress;
			

			newDTO.BillingCompanyName = ent.BillingCompanyName;
			

			newDTO.Currency = ent.Currency;
			

			newDTO.Description = ent.Description;
			

			newDTO.FromDateUtc = ent.FromDateUtc;
			

			newDTO.GeneratedByUserId = ent.GeneratedByUserId;
			

			newDTO.GeneratedDateUtc = ent.GeneratedDateUtc;
			

			newDTO.Id = ent.Id;
			

			newDTO.IssuedDateUtc = ent.IssuedDateUtc;
			

			newDTO.PaymentDateUtc = ent.PaymentDateUtc;
			

			newDTO.PaymentReference = ent.PaymentReference;
			

			newDTO.ToDateUtc = ent.ToDateUtc;
			

			newDTO.TotalAmount = ent.TotalAmount;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			
			
			foreach (var related in ent.InvoiceLineItems)
			{
				newDTO.InvoiceLineItems.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}