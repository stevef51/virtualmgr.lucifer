﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class MediaFolderRoleConverter
	{
	
		public static MediaFolderRoleEntity ToEntity(this MediaFolderRoleDTO dto)
		{
			return dto.ToEntity(new MediaFolderRoleEntity(), new Dictionary<object, object>());
		}

		public static MediaFolderRoleEntity ToEntity(this MediaFolderRoleDTO dto, MediaFolderRoleEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static MediaFolderRoleEntity ToEntity(this MediaFolderRoleDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new MediaFolderRoleEntity(), cache);
		}
	
		public static MediaFolderRoleDTO ToDTO(this MediaFolderRoleEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static MediaFolderRoleEntity ToEntity(this MediaFolderRoleDTO dto, MediaFolderRoleEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (MediaFolderRoleEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.MediaFolderId = dto.MediaFolderId;
			
			

			
			
			newEnt.RoleId = dto.RoleId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.AspNetRole = dto.AspNetRole.ToEntity(cache);
			
			newEnt.MediaFolder = dto.MediaFolder.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static MediaFolderRoleDTO ToDTO(this MediaFolderRoleEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (MediaFolderRoleDTO)cache[ent];
			
			var newDTO = new MediaFolderRoleDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.MediaFolderId = ent.MediaFolderId;
			

			newDTO.RoleId = ent.RoleId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.AspNetRole = ent.AspNetRole.ToDTO(cache);
			
			newDTO.MediaFolder = ent.MediaFolder.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}