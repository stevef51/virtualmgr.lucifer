﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class LabelPublishingGroupResourceConverter
	{
	
		public static LabelPublishingGroupResourceEntity ToEntity(this LabelPublishingGroupResourceDTO dto)
		{
			return dto.ToEntity(new LabelPublishingGroupResourceEntity(), new Dictionary<object, object>());
		}

		public static LabelPublishingGroupResourceEntity ToEntity(this LabelPublishingGroupResourceDTO dto, LabelPublishingGroupResourceEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static LabelPublishingGroupResourceEntity ToEntity(this LabelPublishingGroupResourceDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new LabelPublishingGroupResourceEntity(), cache);
		}
	
		public static LabelPublishingGroupResourceDTO ToDTO(this LabelPublishingGroupResourceEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static LabelPublishingGroupResourceEntity ToEntity(this LabelPublishingGroupResourceDTO dto, LabelPublishingGroupResourceEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (LabelPublishingGroupResourceEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.LabelId = dto.LabelId;
			
			

			
			
			newEnt.PublishingGroupResourceId = dto.PublishingGroupResourceId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.Label = dto.Label.ToEntity(cache);
			
			newEnt.PublishingGroupResource = dto.PublishingGroupResource.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static LabelPublishingGroupResourceDTO ToDTO(this LabelPublishingGroupResourceEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (LabelPublishingGroupResourceDTO)cache[ent];
			
			var newDTO = new LabelPublishingGroupResourceDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.LabelId = ent.LabelId;
			

			newDTO.PublishingGroupResourceId = ent.PublishingGroupResourceId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.Label = ent.Label.ToDTO(cache);
			
			newDTO.PublishingGroupResource = ent.PublishingGroupResource.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}