﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class ProductConverter
	{
	
		public static ProductEntity ToEntity(this ProductDTO dto)
		{
			return dto.ToEntity(new ProductEntity(), new Dictionary<object, object>());
		}

		public static ProductEntity ToEntity(this ProductDTO dto, ProductEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static ProductEntity ToEntity(this ProductDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new ProductEntity(), cache);
		}
	
		public static ProductDTO ToDTO(this ProductEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static ProductEntity ToEntity(this ProductDTO dto, ProductEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (ProductEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			if (dto.Code == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProductFieldIndex.Code, "");
				
			}
			
			newEnt.Code = dto.Code;
			
			

			
			
			if (dto.Description == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProductFieldIndex.Description, "");
				
			}
			
			newEnt.Description = dto.Description;
			
			

			
			
			if (dto.Extra == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProductFieldIndex.Extra, "");
				
			}
			
			newEnt.Extra = dto.Extra;
			
			

			
			
			newEnt.Id = dto.Id;
			
			
			

			
			
			newEnt.IsCoupon = dto.IsCoupon;
			
			

			
			
			if (dto.MediaId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProductFieldIndex.MediaId, default(System.Guid));
				
			}
			
			newEnt.MediaId = dto.MediaId;
			
			

			
			
			newEnt.Name = dto.Name;
			
			

			
			
			if (dto.Price == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProductFieldIndex.Price, default(System.Decimal));
				
			}
			
			newEnt.Price = dto.Price;
			
			

			
			
			if (dto.PriceJsFunctionId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProductFieldIndex.PriceJsFunctionId, default(System.Int32));
				
			}
			
			newEnt.PriceJsFunctionId = dto.PriceJsFunctionId;
			
			

			
			
			newEnt.ProductTypeId = dto.ProductTypeId;
			
			

			
			
			if (dto.ValidFrom == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProductFieldIndex.ValidFrom, default(System.DateTime));
				
			}
			
			newEnt.ValidFrom = dto.ValidFrom;
			
			

			
			
			if (dto.ValidTo == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProductFieldIndex.ValidTo, default(System.DateTime));
				
			}
			
			newEnt.ValidTo = dto.ValidTo;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.Medium = dto.Medium.ToEntity(cache);
			
			newEnt.Jsfunction = dto.Jsfunction.ToEntity(cache);
			
			newEnt.ProductType = dto.ProductType.ToEntity(cache);
			
			
			
			foreach (var related in dto.OrderItems)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.OrderItems.Contains(relatedEntity))
				{
					newEnt.OrderItems.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ProductCatalogItems)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProductCatalogItems.Contains(relatedEntity))
				{
					newEnt.ProductCatalogItems.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ChildProductHierarchies)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ChildProductHierarchies.Contains(relatedEntity))
				{
					newEnt.ChildProductHierarchies.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ParentProductHierarchies)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ParentProductHierarchies.Contains(relatedEntity))
				{
					newEnt.ParentProductHierarchies.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static ProductDTO ToDTO(this ProductEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (ProductDTO)cache[ent];
			
			var newDTO = new ProductDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Code = ent.Code;
			

			newDTO.Description = ent.Description;
			

			newDTO.Extra = ent.Extra;
			

			newDTO.Id = ent.Id;
			

			newDTO.IsCoupon = ent.IsCoupon;
			

			newDTO.MediaId = ent.MediaId;
			

			newDTO.Name = ent.Name;
			

			newDTO.Price = ent.Price;
			

			newDTO.PriceJsFunctionId = ent.PriceJsFunctionId;
			

			newDTO.ProductTypeId = ent.ProductTypeId;
			

			newDTO.ValidFrom = ent.ValidFrom;
			

			newDTO.ValidTo = ent.ValidTo;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.Medium = ent.Medium.ToDTO(cache);
			
			newDTO.Jsfunction = ent.Jsfunction.ToDTO(cache);
			
			newDTO.ProductType = ent.ProductType.ToDTO(cache);
			
			
			
			foreach (var related in ent.OrderItems)
			{
				newDTO.OrderItems.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ProductCatalogItems)
			{
				newDTO.ProductCatalogItems.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ChildProductHierarchies)
			{
				newDTO.ChildProductHierarchies.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ParentProductHierarchies)
			{
				newDTO.ParentProductHierarchies.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}