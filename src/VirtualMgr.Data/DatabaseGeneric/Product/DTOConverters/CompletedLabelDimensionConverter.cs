﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class CompletedLabelDimensionConverter
	{
	
		public static CompletedLabelDimensionEntity ToEntity(this CompletedLabelDimensionDTO dto)
		{
			return dto.ToEntity(new CompletedLabelDimensionEntity(), new Dictionary<object, object>());
		}

		public static CompletedLabelDimensionEntity ToEntity(this CompletedLabelDimensionDTO dto, CompletedLabelDimensionEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static CompletedLabelDimensionEntity ToEntity(this CompletedLabelDimensionDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new CompletedLabelDimensionEntity(), cache);
		}
	
		public static CompletedLabelDimensionDTO ToDTO(this CompletedLabelDimensionEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static CompletedLabelDimensionEntity ToEntity(this CompletedLabelDimensionDTO dto, CompletedLabelDimensionEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (CompletedLabelDimensionEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Backcolor = dto.Backcolor;
			
			

			
			
			newEnt.Forecolor = dto.Forecolor;
			
			

			
			
			newEnt.Id = dto.Id;
			
			

			
			
			newEnt.Name = dto.Name;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			
			
			foreach (var related in dto.CompletedLabelWorkingDocumentDimensions)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.CompletedLabelWorkingDocumentDimensions.Contains(relatedEntity))
				{
					newEnt.CompletedLabelWorkingDocumentDimensions.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.CompletedLabelWorkingDocumentPresentedDimensions)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.CompletedLabelWorkingDocumentPresentedDimensions.Contains(relatedEntity))
				{
					newEnt.CompletedLabelWorkingDocumentPresentedDimensions.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static CompletedLabelDimensionDTO ToDTO(this CompletedLabelDimensionEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (CompletedLabelDimensionDTO)cache[ent];
			
			var newDTO = new CompletedLabelDimensionDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Backcolor = ent.Backcolor;
			

			newDTO.Forecolor = ent.Forecolor;
			

			newDTO.Id = ent.Id;
			

			newDTO.Name = ent.Name;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			
			
			foreach (var related in ent.CompletedLabelWorkingDocumentDimensions)
			{
				newDTO.CompletedLabelWorkingDocumentDimensions.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.CompletedLabelWorkingDocumentPresentedDimensions)
			{
				newDTO.CompletedLabelWorkingDocumentPresentedDimensions.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}