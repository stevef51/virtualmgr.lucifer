﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class MediaViewingConverter
	{
	
		public static MediaViewingEntity ToEntity(this MediaViewingDTO dto)
		{
			return dto.ToEntity(new MediaViewingEntity(), new Dictionary<object, object>());
		}

		public static MediaViewingEntity ToEntity(this MediaViewingDTO dto, MediaViewingEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static MediaViewingEntity ToEntity(this MediaViewingDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new MediaViewingEntity(), cache);
		}
	
		public static MediaViewingDTO ToDTO(this MediaViewingEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static MediaViewingEntity ToEntity(this MediaViewingDTO dto, MediaViewingEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (MediaViewingEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Accepted = dto.Accepted;
			
			

			
			
			if (dto.AcceptionNotes == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)MediaViewingFieldIndex.AcceptionNotes, "");
				
			}
			
			newEnt.AcceptionNotes = dto.AcceptionNotes;
			
			

			
			
			newEnt.DateCompleted = dto.DateCompleted;
			
			

			
			
			newEnt.DateStarted = dto.DateStarted;
			
			

			
			
			newEnt.Id = dto.Id;
			
			

			
			
			newEnt.MediaId = dto.MediaId;
			
			

			
			
			newEnt.TotalSeconds = dto.TotalSeconds;
			
			

			
			
			newEnt.UserId = dto.UserId;
			
			

			
			
			newEnt.Version = dto.Version;
			
			

			
			
			newEnt.WorkingDocumentId = dto.WorkingDocumentId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.Media = dto.Media.ToEntity(cache);
			
			newEnt.UserData = dto.UserData.ToEntity(cache);
			
			
			
			foreach (var related in dto.MediaPageViewings)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.MediaPageViewings.Contains(relatedEntity))
				{
					newEnt.MediaPageViewings.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static MediaViewingDTO ToDTO(this MediaViewingEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (MediaViewingDTO)cache[ent];
			
			var newDTO = new MediaViewingDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Accepted = ent.Accepted;
			

			newDTO.AcceptionNotes = ent.AcceptionNotes;
			

			newDTO.DateCompleted = ent.DateCompleted;
			

			newDTO.DateStarted = ent.DateStarted;
			

			newDTO.Id = ent.Id;
			

			newDTO.MediaId = ent.MediaId;
			

			newDTO.TotalSeconds = ent.TotalSeconds;
			

			newDTO.UserId = ent.UserId;
			

			newDTO.Version = ent.Version;
			

			newDTO.WorkingDocumentId = ent.WorkingDocumentId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.Media = ent.Media.ToDTO(cache);
			
			newDTO.UserData = ent.UserData.ToDTO(cache);
			
			
			
			foreach (var related in ent.MediaPageViewings)
			{
				newDTO.MediaPageViewings.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}