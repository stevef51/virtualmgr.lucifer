﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class AspNetUserClaimConverter
	{
	
		public static AspNetUserClaimEntity ToEntity(this AspNetUserClaimDTO dto)
		{
			return dto.ToEntity(new AspNetUserClaimEntity(), new Dictionary<object, object>());
		}

		public static AspNetUserClaimEntity ToEntity(this AspNetUserClaimDTO dto, AspNetUserClaimEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static AspNetUserClaimEntity ToEntity(this AspNetUserClaimDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new AspNetUserClaimEntity(), cache);
		}
	
		public static AspNetUserClaimDTO ToDTO(this AspNetUserClaimEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static AspNetUserClaimEntity ToEntity(this AspNetUserClaimDTO dto, AspNetUserClaimEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (AspNetUserClaimEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			if (dto.ClaimType == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)AspNetUserClaimFieldIndex.ClaimType, "");
				
			}
			
			newEnt.ClaimType = dto.ClaimType;
			
			

			
			
			if (dto.ClaimValue == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)AspNetUserClaimFieldIndex.ClaimValue, "");
				
			}
			
			newEnt.ClaimValue = dto.ClaimValue;
			
			

			
			
			newEnt.Id = dto.Id;
			
			
			

			
			
			newEnt.UserId = dto.UserId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.AspNetUser = dto.AspNetUser.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static AspNetUserClaimDTO ToDTO(this AspNetUserClaimEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (AspNetUserClaimDTO)cache[ent];
			
			var newDTO = new AspNetUserClaimDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.ClaimType = ent.ClaimType;
			

			newDTO.ClaimValue = ent.ClaimValue;
			

			newDTO.Id = ent.Id;
			

			newDTO.UserId = ent.UserId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.AspNetUser = ent.AspNetUser.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}