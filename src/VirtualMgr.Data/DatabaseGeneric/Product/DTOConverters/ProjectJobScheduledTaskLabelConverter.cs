﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class ProjectJobScheduledTaskLabelConverter
	{
	
		public static ProjectJobScheduledTaskLabelEntity ToEntity(this ProjectJobScheduledTaskLabelDTO dto)
		{
			return dto.ToEntity(new ProjectJobScheduledTaskLabelEntity(), new Dictionary<object, object>());
		}

		public static ProjectJobScheduledTaskLabelEntity ToEntity(this ProjectJobScheduledTaskLabelDTO dto, ProjectJobScheduledTaskLabelEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static ProjectJobScheduledTaskLabelEntity ToEntity(this ProjectJobScheduledTaskLabelDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new ProjectJobScheduledTaskLabelEntity(), cache);
		}
	
		public static ProjectJobScheduledTaskLabelDTO ToDTO(this ProjectJobScheduledTaskLabelEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static ProjectJobScheduledTaskLabelEntity ToEntity(this ProjectJobScheduledTaskLabelDTO dto, ProjectJobScheduledTaskLabelEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (ProjectJobScheduledTaskLabelEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.LabelId = dto.LabelId;
			
			

			
			
			newEnt.ProjectJobScheduledTaskId = dto.ProjectJobScheduledTaskId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.ProjectJobScheduledTask = dto.ProjectJobScheduledTask.ToEntity(cache);
			
			newEnt.Label = dto.Label.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static ProjectJobScheduledTaskLabelDTO ToDTO(this ProjectJobScheduledTaskLabelEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (ProjectJobScheduledTaskLabelDTO)cache[ent];
			
			var newDTO = new ProjectJobScheduledTaskLabelDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.LabelId = ent.LabelId;
			

			newDTO.ProjectJobScheduledTaskId = ent.ProjectJobScheduledTaskId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.ProjectJobScheduledTask = ent.ProjectJobScheduledTask.ToDTO(cache);
			
			newDTO.Label = ent.Label.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}