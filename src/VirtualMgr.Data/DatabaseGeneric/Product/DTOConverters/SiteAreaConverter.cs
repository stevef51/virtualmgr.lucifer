﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class SiteAreaConverter
	{
	
		public static SiteAreaEntity ToEntity(this SiteAreaDTO dto)
		{
			return dto.ToEntity(new SiteAreaEntity(), new Dictionary<object, object>());
		}

		public static SiteAreaEntity ToEntity(this SiteAreaDTO dto, SiteAreaEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static SiteAreaEntity ToEntity(this SiteAreaDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new SiteAreaEntity(), cache);
		}
	
		public static SiteAreaDTO ToDTO(this SiteAreaEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static SiteAreaEntity ToEntity(this SiteAreaDTO dto, SiteAreaEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (SiteAreaEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Id = dto.Id;
			
			

			
			
			newEnt.Name = dto.Name;
			
			

			
			
			newEnt.SiteId = dto.SiteId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.UserData = dto.UserData.ToEntity(cache);
			
			
			
			foreach (var related in dto.SiteFeatures)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.SiteFeatures.Contains(relatedEntity))
				{
					newEnt.SiteFeatures.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static SiteAreaDTO ToDTO(this SiteAreaEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (SiteAreaDTO)cache[ent];
			
			var newDTO = new SiteAreaDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Id = ent.Id;
			

			newDTO.Name = ent.Name;
			

			newDTO.SiteId = ent.SiteId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.UserData = ent.UserData.ToDTO(cache);
			
			
			
			foreach (var related in ent.SiteFeatures)
			{
				newDTO.SiteFeatures.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}