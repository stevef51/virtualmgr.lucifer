﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class PasswordResetTicketConverter
	{
	
		public static PasswordResetTicketEntity ToEntity(this PasswordResetTicketDTO dto)
		{
			return dto.ToEntity(new PasswordResetTicketEntity(), new Dictionary<object, object>());
		}

		public static PasswordResetTicketEntity ToEntity(this PasswordResetTicketDTO dto, PasswordResetTicketEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static PasswordResetTicketEntity ToEntity(this PasswordResetTicketDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new PasswordResetTicketEntity(), cache);
		}
	
		public static PasswordResetTicketDTO ToDTO(this PasswordResetTicketEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static PasswordResetTicketEntity ToEntity(this PasswordResetTicketDTO dto, PasswordResetTicketEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (PasswordResetTicketEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.DateCreated = dto.DateCreated;
			
			

			
			
			newEnt.ExpiryDate = dto.ExpiryDate;
			
			

			
			
			newEnt.PublicTicketId = dto.PublicTicketId;
			
			

			
			
			newEnt.UserId = dto.UserId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			newEnt.PublicTicket = dto.PublicTicket.ToEntity(cache);
			
			
			
			newEnt.UserData = dto.UserData.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static PasswordResetTicketDTO ToDTO(this PasswordResetTicketEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (PasswordResetTicketDTO)cache[ent];
			
			var newDTO = new PasswordResetTicketDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.DateCreated = ent.DateCreated;
			

			newDTO.ExpiryDate = ent.ExpiryDate;
			

			newDTO.PublicTicketId = ent.PublicTicketId;
			

			newDTO.UserId = ent.UserId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			newDTO.PublicTicket = ent.PublicTicket.ToDTO(cache);
			
			
			
			newDTO.UserData = ent.UserData.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}