﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class UserAgentConverter
	{
	
		public static UserAgentEntity ToEntity(this UserAgentDTO dto)
		{
			return dto.ToEntity(new UserAgentEntity(), new Dictionary<object, object>());
		}

		public static UserAgentEntity ToEntity(this UserAgentDTO dto, UserAgentEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static UserAgentEntity ToEntity(this UserAgentDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new UserAgentEntity(), cache);
		}
	
		public static UserAgentDTO ToDTO(this UserAgentEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static UserAgentEntity ToEntity(this UserAgentDTO dto, UserAgentEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (UserAgentEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Id = dto.Id;
			
			
			

			
			
			newEnt.UserAgent = dto.UserAgent;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			
			
			foreach (var related in dto.WorkingDocuments)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.WorkingDocuments.Contains(relatedEntity))
				{
					newEnt.WorkingDocuments.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static UserAgentDTO ToDTO(this UserAgentEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (UserAgentDTO)cache[ent];
			
			var newDTO = new UserAgentDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Id = ent.Id;
			

			newDTO.UserAgent = ent.UserAgent;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			
			
			foreach (var related in ent.WorkingDocuments)
			{
				newDTO.WorkingDocuments.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}