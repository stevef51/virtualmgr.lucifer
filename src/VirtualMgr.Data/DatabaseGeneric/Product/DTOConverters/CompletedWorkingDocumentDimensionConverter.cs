﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class CompletedWorkingDocumentDimensionConverter
	{
	
		public static CompletedWorkingDocumentDimensionEntity ToEntity(this CompletedWorkingDocumentDimensionDTO dto)
		{
			return dto.ToEntity(new CompletedWorkingDocumentDimensionEntity(), new Dictionary<object, object>());
		}

		public static CompletedWorkingDocumentDimensionEntity ToEntity(this CompletedWorkingDocumentDimensionDTO dto, CompletedWorkingDocumentDimensionEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static CompletedWorkingDocumentDimensionEntity ToEntity(this CompletedWorkingDocumentDimensionDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new CompletedWorkingDocumentDimensionEntity(), cache);
		}
	
		public static CompletedWorkingDocumentDimensionDTO ToDTO(this CompletedWorkingDocumentDimensionEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static CompletedWorkingDocumentDimensionEntity ToEntity(this CompletedWorkingDocumentDimensionDTO dto, CompletedWorkingDocumentDimensionEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (CompletedWorkingDocumentDimensionEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Id = dto.Id;
			
			

			
			
			newEnt.Name = dto.Name;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			
			
			foreach (var related in dto.CompletedWorkingDocumentFacts)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.CompletedWorkingDocumentFacts.Contains(relatedEntity))
				{
					newEnt.CompletedWorkingDocumentFacts.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.CompletedWorkingDocumentPresentedFacts)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.CompletedWorkingDocumentPresentedFacts.Contains(relatedEntity))
				{
					newEnt.CompletedWorkingDocumentPresentedFacts.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static CompletedWorkingDocumentDimensionDTO ToDTO(this CompletedWorkingDocumentDimensionEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (CompletedWorkingDocumentDimensionDTO)cache[ent];
			
			var newDTO = new CompletedWorkingDocumentDimensionDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Id = ent.Id;
			

			newDTO.Name = ent.Name;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			
			
			foreach (var related in ent.CompletedWorkingDocumentFacts)
			{
				newDTO.CompletedWorkingDocumentFacts.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.CompletedWorkingDocumentPresentedFacts)
			{
				newDTO.CompletedWorkingDocumentPresentedFacts.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}