﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class ProjectJobTaskChangeRequestConverter
	{
	
		public static ProjectJobTaskChangeRequestEntity ToEntity(this ProjectJobTaskChangeRequestDTO dto)
		{
			return dto.ToEntity(new ProjectJobTaskChangeRequestEntity(), new Dictionary<object, object>());
		}

		public static ProjectJobTaskChangeRequestEntity ToEntity(this ProjectJobTaskChangeRequestDTO dto, ProjectJobTaskChangeRequestEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static ProjectJobTaskChangeRequestEntity ToEntity(this ProjectJobTaskChangeRequestDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new ProjectJobTaskChangeRequestEntity(), cache);
		}
	
		public static ProjectJobTaskChangeRequestDTO ToDTO(this ProjectJobTaskChangeRequestEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static ProjectJobTaskChangeRequestEntity ToEntity(this ProjectJobTaskChangeRequestDTO dto, ProjectJobTaskChangeRequestEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (ProjectJobTaskChangeRequestEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.ChangeType = dto.ChangeType;
			
			

			
			
			if (dto.FromRosterId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobTaskChangeRequestFieldIndex.FromRosterId, default(System.Int32));
				
			}
			
			newEnt.FromRosterId = dto.FromRosterId;
			
			

			
			
			newEnt.Id = dto.Id;
			
			

			
			
			if (dto.ProcessedByUserId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobTaskChangeRequestFieldIndex.ProcessedByUserId, default(System.Guid));
				
			}
			
			newEnt.ProcessedByUserId = dto.ProcessedByUserId;
			
			

			
			
			newEnt.ProjectJobTaskId = dto.ProjectJobTaskId;
			
			

			
			
			newEnt.RequestedByUserId = dto.RequestedByUserId;
			
			

			
			
			newEnt.Status = dto.Status;
			
			

			
			
			if (dto.ToRosterId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobTaskChangeRequestFieldIndex.ToRosterId, default(System.Int32));
				
			}
			
			newEnt.ToRosterId = dto.ToRosterId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.ProjectJobTask = dto.ProjectJobTask.ToEntity(cache);
			
			newEnt.FromRoster = dto.FromRoster.ToEntity(cache);
			
			newEnt.ToRoster = dto.ToRoster.ToEntity(cache);
			
			newEnt.ProcessedByUserData = dto.ProcessedByUserData.ToEntity(cache);
			
			newEnt.RequestedByUserData = dto.RequestedByUserData.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static ProjectJobTaskChangeRequestDTO ToDTO(this ProjectJobTaskChangeRequestEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (ProjectJobTaskChangeRequestDTO)cache[ent];
			
			var newDTO = new ProjectJobTaskChangeRequestDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.ChangeType = ent.ChangeType;
			

			newDTO.FromRosterId = ent.FromRosterId;
			

			newDTO.Id = ent.Id;
			

			newDTO.ProcessedByUserId = ent.ProcessedByUserId;
			

			newDTO.ProjectJobTaskId = ent.ProjectJobTaskId;
			

			newDTO.RequestedByUserId = ent.RequestedByUserId;
			

			newDTO.Status = ent.Status;
			

			newDTO.ToRosterId = ent.ToRosterId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.ProjectJobTask = ent.ProjectJobTask.ToDTO(cache);
			
			newDTO.FromRoster = ent.FromRoster.ToDTO(cache);
			
			newDTO.ToRoster = ent.ToRoster.ToDTO(cache);
			
			newDTO.ProcessedByUserData = ent.ProcessedByUserData.ToDTO(cache);
			
			newDTO.RequestedByUserData = ent.RequestedByUserData.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}