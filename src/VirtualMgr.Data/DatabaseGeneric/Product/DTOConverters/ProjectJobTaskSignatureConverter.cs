﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class ProjectJobTaskSignatureConverter
	{
	
		public static ProjectJobTaskSignatureEntity ToEntity(this ProjectJobTaskSignatureDTO dto)
		{
			return dto.ToEntity(new ProjectJobTaskSignatureEntity(), new Dictionary<object, object>());
		}

		public static ProjectJobTaskSignatureEntity ToEntity(this ProjectJobTaskSignatureDTO dto, ProjectJobTaskSignatureEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static ProjectJobTaskSignatureEntity ToEntity(this ProjectJobTaskSignatureDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new ProjectJobTaskSignatureEntity(), cache);
		}
	
		public static ProjectJobTaskSignatureDTO ToDTO(this ProjectJobTaskSignatureEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static ProjectJobTaskSignatureEntity ToEntity(this ProjectJobTaskSignatureDTO dto, ProjectJobTaskSignatureEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (ProjectJobTaskSignatureEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			if (dto.Accuracy == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobTaskSignatureFieldIndex.Accuracy, default(System.Decimal));
				
			}
			
			newEnt.Accuracy = dto.Accuracy;
			
			

			
			
			if (dto.Latitude == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobTaskSignatureFieldIndex.Latitude, default(System.Decimal));
				
			}
			
			newEnt.Latitude = dto.Latitude;
			
			

			
			
			newEnt.LocationStatus = dto.LocationStatus;
			
			

			
			
			if (dto.Longitude == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobTaskSignatureFieldIndex.Longitude, default(System.Decimal));
				
			}
			
			newEnt.Longitude = dto.Longitude;
			
			

			
			
			if (dto.PhotoMediaId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobTaskSignatureFieldIndex.PhotoMediaId, default(System.Guid));
				
			}
			
			newEnt.PhotoMediaId = dto.PhotoMediaId;
			
			

			
			
			newEnt.ProjectJobTaskId = dto.ProjectJobTaskId;
			
			

			
			
			newEnt.SignatureData = dto.SignatureData;
			
			

			
			
			if (dto.SignatureMediaId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobTaskSignatureFieldIndex.SignatureMediaId, default(System.Guid));
				
			}
			
			newEnt.SignatureMediaId = dto.SignatureMediaId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			newEnt.ProjectJobTask = dto.ProjectJobTask.ToEntity(cache);
			
			
			
			newEnt.PhotoMedia = dto.PhotoMedia.ToEntity(cache);
			
			newEnt.SignatureMedia = dto.SignatureMedia.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static ProjectJobTaskSignatureDTO ToDTO(this ProjectJobTaskSignatureEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (ProjectJobTaskSignatureDTO)cache[ent];
			
			var newDTO = new ProjectJobTaskSignatureDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Accuracy = ent.Accuracy;
			

			newDTO.Latitude = ent.Latitude;
			

			newDTO.LocationStatus = ent.LocationStatus;
			

			newDTO.Longitude = ent.Longitude;
			

			newDTO.PhotoMediaId = ent.PhotoMediaId;
			

			newDTO.ProjectJobTaskId = ent.ProjectJobTaskId;
			

			newDTO.SignatureData = ent.SignatureData;
			

			newDTO.SignatureMediaId = ent.SignatureMediaId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			newDTO.ProjectJobTask = ent.ProjectJobTask.ToDTO(cache);
			
			
			
			newDTO.PhotoMedia = ent.PhotoMedia.ToDTO(cache);
			
			newDTO.SignatureMedia = ent.SignatureMedia.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}