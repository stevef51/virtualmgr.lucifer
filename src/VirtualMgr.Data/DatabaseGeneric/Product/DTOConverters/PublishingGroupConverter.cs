﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class PublishingGroupConverter
	{
	
		public static PublishingGroupEntity ToEntity(this PublishingGroupDTO dto)
		{
			return dto.ToEntity(new PublishingGroupEntity(), new Dictionary<object, object>());
		}

		public static PublishingGroupEntity ToEntity(this PublishingGroupDTO dto, PublishingGroupEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static PublishingGroupEntity ToEntity(this PublishingGroupDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new PublishingGroupEntity(), cache);
		}
	
		public static PublishingGroupDTO ToDTO(this PublishingGroupEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static PublishingGroupEntity ToEntity(this PublishingGroupDTO dto, PublishingGroupEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (PublishingGroupEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.AllowUseAsAssetContext = dto.AllowUseAsAssetContext;
			
			

			
			
			newEnt.AllowUseAsDashboard = dto.AllowUseAsDashboard;
			
			

			
			
			newEnt.AllowUseAsTaskWorkflow = dto.AllowUseAsTaskWorkflow;
			
			

			
			
			newEnt.AllowUseAsUserContext = dto.AllowUseAsUserContext;
			
			

			
			
			if (dto.Description == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)PublishingGroupFieldIndex.Description, "");
				
			}
			
			newEnt.Description = dto.Description;
			
			

			
			
			newEnt.Id = dto.Id;
			
			
			

			
			
			newEnt.Name = dto.Name;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			
			
			foreach (var related in dto.PublishingGroupActors)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.PublishingGroupActors.Contains(relatedEntity))
				{
					newEnt.PublishingGroupActors.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.PublishingGroupActorLabels)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.PublishingGroupActorLabels.Contains(relatedEntity))
				{
					newEnt.PublishingGroupActorLabels.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.PublishingGroupResources)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.PublishingGroupResources.Contains(relatedEntity))
				{
					newEnt.PublishingGroupResources.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static PublishingGroupDTO ToDTO(this PublishingGroupEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (PublishingGroupDTO)cache[ent];
			
			var newDTO = new PublishingGroupDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.AllowUseAsAssetContext = ent.AllowUseAsAssetContext;
			

			newDTO.AllowUseAsDashboard = ent.AllowUseAsDashboard;
			

			newDTO.AllowUseAsTaskWorkflow = ent.AllowUseAsTaskWorkflow;
			

			newDTO.AllowUseAsUserContext = ent.AllowUseAsUserContext;
			

			newDTO.Description = ent.Description;
			

			newDTO.Id = ent.Id;
			

			newDTO.Name = ent.Name;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			
			
			foreach (var related in ent.PublishingGroupActors)
			{
				newDTO.PublishingGroupActors.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.PublishingGroupActorLabels)
			{
				newDTO.PublishingGroupActorLabels.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.PublishingGroupResources)
			{
				newDTO.PublishingGroupResources.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}