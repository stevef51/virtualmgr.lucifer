﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class UserTypeDefaultLabelConverter
	{
	
		public static UserTypeDefaultLabelEntity ToEntity(this UserTypeDefaultLabelDTO dto)
		{
			return dto.ToEntity(new UserTypeDefaultLabelEntity(), new Dictionary<object, object>());
		}

		public static UserTypeDefaultLabelEntity ToEntity(this UserTypeDefaultLabelDTO dto, UserTypeDefaultLabelEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static UserTypeDefaultLabelEntity ToEntity(this UserTypeDefaultLabelDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new UserTypeDefaultLabelEntity(), cache);
		}
	
		public static UserTypeDefaultLabelDTO ToDTO(this UserTypeDefaultLabelEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static UserTypeDefaultLabelEntity ToEntity(this UserTypeDefaultLabelDTO dto, UserTypeDefaultLabelEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (UserTypeDefaultLabelEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.LabelId = dto.LabelId;
			
			

			
			
			newEnt.UserTypeId = dto.UserTypeId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.Label = dto.Label.ToEntity(cache);
			
			newEnt.UserType = dto.UserType.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static UserTypeDefaultLabelDTO ToDTO(this UserTypeDefaultLabelEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (UserTypeDefaultLabelDTO)cache[ent];
			
			var newDTO = new UserTypeDefaultLabelDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.LabelId = ent.LabelId;
			

			newDTO.UserTypeId = ent.UserTypeId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.Label = ent.Label.ToDTO(cache);
			
			newDTO.UserType = ent.UserType.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}