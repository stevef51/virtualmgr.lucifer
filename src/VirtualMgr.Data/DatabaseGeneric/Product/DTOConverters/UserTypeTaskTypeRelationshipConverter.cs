﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class UserTypeTaskTypeRelationshipConverter
	{
	
		public static UserTypeTaskTypeRelationshipEntity ToEntity(this UserTypeTaskTypeRelationshipDTO dto)
		{
			return dto.ToEntity(new UserTypeTaskTypeRelationshipEntity(), new Dictionary<object, object>());
		}

		public static UserTypeTaskTypeRelationshipEntity ToEntity(this UserTypeTaskTypeRelationshipDTO dto, UserTypeTaskTypeRelationshipEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static UserTypeTaskTypeRelationshipEntity ToEntity(this UserTypeTaskTypeRelationshipDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new UserTypeTaskTypeRelationshipEntity(), cache);
		}
	
		public static UserTypeTaskTypeRelationshipDTO ToDTO(this UserTypeTaskTypeRelationshipEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static UserTypeTaskTypeRelationshipEntity ToEntity(this UserTypeTaskTypeRelationshipDTO dto, UserTypeTaskTypeRelationshipEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (UserTypeTaskTypeRelationshipEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.RelationshipType = dto.RelationshipType;
			
			

			
			
			newEnt.TaskTypeId = dto.TaskTypeId;
			
			

			
			
			newEnt.UserTypeId = dto.UserTypeId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.ProjectJobTaskType = dto.ProjectJobTaskType.ToEntity(cache);
			
			newEnt.UserType = dto.UserType.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static UserTypeTaskTypeRelationshipDTO ToDTO(this UserTypeTaskTypeRelationshipEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (UserTypeTaskTypeRelationshipDTO)cache[ent];
			
			var newDTO = new UserTypeTaskTypeRelationshipDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.RelationshipType = ent.RelationshipType;
			

			newDTO.TaskTypeId = ent.TaskTypeId;
			

			newDTO.UserTypeId = ent.UserTypeId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.ProjectJobTaskType = ent.ProjectJobTaskType.ToDTO(cache);
			
			newDTO.UserType = ent.UserType.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}