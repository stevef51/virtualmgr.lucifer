﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class ArticleConverter
	{
	
		public static ArticleEntity ToEntity(this ArticleDTO dto)
		{
			return dto.ToEntity(new ArticleEntity(), new Dictionary<object, object>());
		}

		public static ArticleEntity ToEntity(this ArticleDTO dto, ArticleEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static ArticleEntity ToEntity(this ArticleDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new ArticleEntity(), cache);
		}
	
		public static ArticleDTO ToDTO(this ArticleEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static ArticleEntity ToEntity(this ArticleDTO dto, ArticleEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (ArticleEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Id = dto.Id;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			
			
			foreach (var related in dto.ArticleBigInts)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ArticleBigInts.Contains(relatedEntity))
				{
					newEnt.ArticleBigInts.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ArticleBools)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ArticleBools.Contains(relatedEntity))
				{
					newEnt.ArticleBools.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ArticleDateTimes)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ArticleDateTimes.Contains(relatedEntity))
				{
					newEnt.ArticleDateTimes.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ArticleGuids)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ArticleGuids.Contains(relatedEntity))
				{
					newEnt.ArticleGuids.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ArticleNumbers)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ArticleNumbers.Contains(relatedEntity))
				{
					newEnt.ArticleNumbers.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ArticleStrings)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ArticleStrings.Contains(relatedEntity))
				{
					newEnt.ArticleStrings.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ArticleSubArticles)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ArticleSubArticles.Contains(relatedEntity))
				{
					newEnt.ArticleSubArticles.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.SubArticleArticles)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.SubArticleArticles.Contains(relatedEntity))
				{
					newEnt.SubArticleArticles.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static ArticleDTO ToDTO(this ArticleEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (ArticleDTO)cache[ent];
			
			var newDTO = new ArticleDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Id = ent.Id;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			
			
			foreach (var related in ent.ArticleBigInts)
			{
				newDTO.ArticleBigInts.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ArticleBools)
			{
				newDTO.ArticleBools.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ArticleDateTimes)
			{
				newDTO.ArticleDateTimes.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ArticleGuids)
			{
				newDTO.ArticleGuids.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ArticleNumbers)
			{
				newDTO.ArticleNumbers.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ArticleStrings)
			{
				newDTO.ArticleStrings.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ArticleSubArticles)
			{
				newDTO.ArticleSubArticles.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.SubArticleArticles)
			{
				newDTO.SubArticleArticles.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}