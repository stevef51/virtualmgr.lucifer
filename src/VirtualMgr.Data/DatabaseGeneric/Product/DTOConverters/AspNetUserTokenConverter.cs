﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class AspNetUserTokenConverter
	{
	
		public static AspNetUserTokenEntity ToEntity(this AspNetUserTokenDTO dto)
		{
			return dto.ToEntity(new AspNetUserTokenEntity(), new Dictionary<object, object>());
		}

		public static AspNetUserTokenEntity ToEntity(this AspNetUserTokenDTO dto, AspNetUserTokenEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static AspNetUserTokenEntity ToEntity(this AspNetUserTokenDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new AspNetUserTokenEntity(), cache);
		}
	
		public static AspNetUserTokenDTO ToDTO(this AspNetUserTokenEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static AspNetUserTokenEntity ToEntity(this AspNetUserTokenDTO dto, AspNetUserTokenEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (AspNetUserTokenEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.LoginProvider = dto.LoginProvider;
			
			

			
			
			newEnt.Name = dto.Name;
			
			

			
			
			newEnt.UserId = dto.UserId;
			
			

			
			
			if (dto.Value == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)AspNetUserTokenFieldIndex.Value, "");
				
			}
			
			newEnt.Value = dto.Value;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.AspNetUser = dto.AspNetUser.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static AspNetUserTokenDTO ToDTO(this AspNetUserTokenEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (AspNetUserTokenDTO)cache[ent];
			
			var newDTO = new AspNetUserTokenDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.LoginProvider = ent.LoginProvider;
			

			newDTO.Name = ent.Name;
			

			newDTO.UserId = ent.UserId;
			

			newDTO.Value = ent.Value;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.AspNetUser = ent.AspNetUser.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}