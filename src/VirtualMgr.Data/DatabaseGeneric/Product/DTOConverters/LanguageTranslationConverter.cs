﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class LanguageTranslationConverter
	{
	
		public static LanguageTranslationEntity ToEntity(this LanguageTranslationDTO dto)
		{
			return dto.ToEntity(new LanguageTranslationEntity(), new Dictionary<object, object>());
		}

		public static LanguageTranslationEntity ToEntity(this LanguageTranslationDTO dto, LanguageTranslationEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static LanguageTranslationEntity ToEntity(this LanguageTranslationDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new LanguageTranslationEntity(), cache);
		}
	
		public static LanguageTranslationDTO ToDTO(this LanguageTranslationEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static LanguageTranslationEntity ToEntity(this LanguageTranslationDTO dto, LanguageTranslationEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (LanguageTranslationEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.CultureName = dto.CultureName;
			
			

			
			
			newEnt.NativeText = dto.NativeText;
			
			

			
			
			newEnt.TranslationId = dto.TranslationId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static LanguageTranslationDTO ToDTO(this LanguageTranslationEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (LanguageTranslationDTO)cache[ent];
			
			var newDTO = new LanguageTranslationDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.CultureName = ent.CultureName;
			

			newDTO.NativeText = ent.NativeText;
			

			newDTO.TranslationId = ent.TranslationId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}