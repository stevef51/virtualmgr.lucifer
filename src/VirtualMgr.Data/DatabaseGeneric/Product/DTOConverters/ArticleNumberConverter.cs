﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class ArticleNumberConverter
	{
	
		public static ArticleNumberEntity ToEntity(this ArticleNumberDTO dto)
		{
			return dto.ToEntity(new ArticleNumberEntity(), new Dictionary<object, object>());
		}

		public static ArticleNumberEntity ToEntity(this ArticleNumberDTO dto, ArticleNumberEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static ArticleNumberEntity ToEntity(this ArticleNumberDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new ArticleNumberEntity(), cache);
		}
	
		public static ArticleNumberDTO ToDTO(this ArticleNumberEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static ArticleNumberEntity ToEntity(this ArticleNumberDTO dto, ArticleNumberEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (ArticleNumberEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.ArticleId = dto.ArticleId;
			
			

			
			
			newEnt.Index = dto.Index;
			
			

			
			
			newEnt.Name = dto.Name;
			
			

			
			
			newEnt.Value = dto.Value;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.Article = dto.Article.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static ArticleNumberDTO ToDTO(this ArticleNumberEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (ArticleNumberDTO)cache[ent];
			
			var newDTO = new ArticleNumberDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.ArticleId = ent.ArticleId;
			

			newDTO.Index = ent.Index;
			

			newDTO.Name = ent.Name;
			

			newDTO.Value = ent.Value;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.Article = ent.Article.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}