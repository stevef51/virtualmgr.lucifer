﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class PublicWorkingDocumentConverter
	{
	
		public static PublicWorkingDocumentEntity ToEntity(this PublicWorkingDocumentDTO dto)
		{
			return dto.ToEntity(new PublicWorkingDocumentEntity(), new Dictionary<object, object>());
		}

		public static PublicWorkingDocumentEntity ToEntity(this PublicWorkingDocumentDTO dto, PublicWorkingDocumentEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static PublicWorkingDocumentEntity ToEntity(this PublicWorkingDocumentDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new PublicWorkingDocumentEntity(), cache);
		}
	
		public static PublicWorkingDocumentDTO ToDTO(this PublicWorkingDocumentEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static PublicWorkingDocumentEntity ToEntity(this PublicWorkingDocumentDTO dto, PublicWorkingDocumentEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (PublicWorkingDocumentEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.PublicTicketId = dto.PublicTicketId;
			
			

			
			
			newEnt.WorkingDocumentId = dto.WorkingDocumentId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.PublicTicket = dto.PublicTicket.ToEntity(cache);
			
			newEnt.WorkingDocument = dto.WorkingDocument.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static PublicWorkingDocumentDTO ToDTO(this PublicWorkingDocumentEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (PublicWorkingDocumentDTO)cache[ent];
			
			var newDTO = new PublicWorkingDocumentDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.PublicTicketId = ent.PublicTicketId;
			

			newDTO.WorkingDocumentId = ent.WorkingDocumentId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.PublicTicket = ent.PublicTicket.ToDTO(cache);
			
			newDTO.WorkingDocument = ent.WorkingDocument.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}