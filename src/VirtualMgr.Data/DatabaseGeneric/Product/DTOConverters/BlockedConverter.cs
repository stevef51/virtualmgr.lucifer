﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class BlockedConverter
	{
	
		public static BlockedEntity ToEntity(this BlockedDTO dto)
		{
			return dto.ToEntity(new BlockedEntity(), new Dictionary<object, object>());
		}

		public static BlockedEntity ToEntity(this BlockedDTO dto, BlockedEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static BlockedEntity ToEntity(this BlockedDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new BlockedEntity(), cache);
		}
	
		public static BlockedDTO ToDTO(this BlockedEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static BlockedEntity ToEntity(this BlockedDTO dto, BlockedEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (BlockedEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.BlockedById = dto.BlockedById;
			
			

			
			
			newEnt.BlockedId = dto.BlockedId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static BlockedDTO ToDTO(this BlockedEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (BlockedDTO)cache[ent];
			
			var newDTO = new BlockedDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.BlockedById = ent.BlockedById;
			

			newDTO.BlockedId = ent.BlockedId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}