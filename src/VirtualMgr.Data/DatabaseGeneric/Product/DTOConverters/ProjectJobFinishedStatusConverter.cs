﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class ProjectJobFinishedStatusConverter
	{
	
		public static ProjectJobFinishedStatusEntity ToEntity(this ProjectJobFinishedStatusDTO dto)
		{
			return dto.ToEntity(new ProjectJobFinishedStatusEntity(), new Dictionary<object, object>());
		}

		public static ProjectJobFinishedStatusEntity ToEntity(this ProjectJobFinishedStatusDTO dto, ProjectJobFinishedStatusEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static ProjectJobFinishedStatusEntity ToEntity(this ProjectJobFinishedStatusDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new ProjectJobFinishedStatusEntity(), cache);
		}
	
		public static ProjectJobFinishedStatusDTO ToDTO(this ProjectJobFinishedStatusEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static ProjectJobFinishedStatusEntity ToEntity(this ProjectJobFinishedStatusDTO dto, ProjectJobFinishedStatusEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (ProjectJobFinishedStatusEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Id = dto.Id;
			
			

			
			
			newEnt.RequiresExtraNotes = dto.RequiresExtraNotes;
			
			

			
			
			newEnt.Stage = dto.Stage;
			
			

			
			
			newEnt.Text = dto.Text;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			
			
			foreach (var related in dto.ProjectJobTaskFinishedStatuses)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProjectJobTaskFinishedStatuses.Contains(relatedEntity))
				{
					newEnt.ProjectJobTaskFinishedStatuses.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ProjectJobTaskTypeFinishedStatuses)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProjectJobTaskTypeFinishedStatuses.Contains(relatedEntity))
				{
					newEnt.ProjectJobTaskTypeFinishedStatuses.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static ProjectJobFinishedStatusDTO ToDTO(this ProjectJobFinishedStatusEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (ProjectJobFinishedStatusDTO)cache[ent];
			
			var newDTO = new ProjectJobFinishedStatusDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Id = ent.Id;
			

			newDTO.RequiresExtraNotes = ent.RequiresExtraNotes;
			

			newDTO.Stage = ent.Stage;
			

			newDTO.Text = ent.Text;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			
			
			foreach (var related in ent.ProjectJobTaskFinishedStatuses)
			{
				newDTO.ProjectJobTaskFinishedStatuses.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ProjectJobTaskTypeFinishedStatuses)
			{
				newDTO.ProjectJobTaskTypeFinishedStatuses.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}