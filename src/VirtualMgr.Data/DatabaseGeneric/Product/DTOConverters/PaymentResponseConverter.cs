﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class PaymentResponseConverter
	{
	
		public static PaymentResponseEntity ToEntity(this PaymentResponseDTO dto)
		{
			return dto.ToEntity(new PaymentResponseEntity(), new Dictionary<object, object>());
		}

		public static PaymentResponseEntity ToEntity(this PaymentResponseDTO dto, PaymentResponseEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static PaymentResponseEntity ToEntity(this PaymentResponseDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new PaymentResponseEntity(), cache);
		}
	
		public static PaymentResponseDTO ToDTO(this PaymentResponseEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static PaymentResponseEntity ToEntity(this PaymentResponseDTO dto, PaymentResponseEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (PaymentResponseEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.DateUtc = dto.DateUtc;
			
			

			
			
			if (dto.Errors == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)PaymentResponseFieldIndex.Errors, "");
				
			}
			
			newEnt.Errors = dto.Errors;
			
			

			
			
			if (dto.GatewayAuthorisationCode == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)PaymentResponseFieldIndex.GatewayAuthorisationCode, "");
				
			}
			
			newEnt.GatewayAuthorisationCode = dto.GatewayAuthorisationCode;
			
			

			
			
			if (dto.GatewayTransactionId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)PaymentResponseFieldIndex.GatewayTransactionId, "");
				
			}
			
			newEnt.GatewayTransactionId = dto.GatewayTransactionId;
			
			

			
			
			newEnt.Id = dto.Id;
			
			

			
			
			newEnt.PendingPaymentId = dto.PendingPaymentId;
			
			

			
			
			newEnt.RequestedAmount = dto.RequestedAmount;
			
			

			
			
			newEnt.Response = dto.Response;
			
			

			
			
			newEnt.ResponseCode = dto.ResponseCode;
			
			

			
			
			if (dto.ResponseMessage == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)PaymentResponseFieldIndex.ResponseMessage, "");
				
			}
			
			newEnt.ResponseMessage = dto.ResponseMessage;
			
			

			
			
			if (dto.TransactedAmount == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)PaymentResponseFieldIndex.TransactedAmount, default(System.Decimal));
				
			}
			
			newEnt.TransactedAmount = dto.TransactedAmount;
			
			

			
			
			newEnt.TransactionStatus = dto.TransactionStatus;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.PendingPayment = dto.PendingPayment.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static PaymentResponseDTO ToDTO(this PaymentResponseEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (PaymentResponseDTO)cache[ent];
			
			var newDTO = new PaymentResponseDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.DateUtc = ent.DateUtc;
			

			newDTO.Errors = ent.Errors;
			

			newDTO.GatewayAuthorisationCode = ent.GatewayAuthorisationCode;
			

			newDTO.GatewayTransactionId = ent.GatewayTransactionId;
			

			newDTO.Id = ent.Id;
			

			newDTO.PendingPaymentId = ent.PendingPaymentId;
			

			newDTO.RequestedAmount = ent.RequestedAmount;
			

			newDTO.Response = ent.Response;
			

			newDTO.ResponseCode = ent.ResponseCode;
			

			newDTO.ResponseMessage = ent.ResponseMessage;
			

			newDTO.TransactedAmount = ent.TransactedAmount;
			

			newDTO.TransactionStatus = ent.TransactionStatus;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.PendingPayment = ent.PendingPayment.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}