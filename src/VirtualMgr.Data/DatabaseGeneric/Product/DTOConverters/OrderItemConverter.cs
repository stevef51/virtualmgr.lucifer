﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class OrderItemConverter
	{
	
		public static OrderItemEntity ToEntity(this OrderItemDTO dto)
		{
			return dto.ToEntity(new OrderItemEntity(), new Dictionary<object, object>());
		}

		public static OrderItemEntity ToEntity(this OrderItemDTO dto, OrderItemEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static OrderItemEntity ToEntity(this OrderItemDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new OrderItemEntity(), cache);
		}
	
		public static OrderItemDTO ToDTO(this OrderItemEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static OrderItemEntity ToEntity(this OrderItemDTO dto, OrderItemEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (OrderItemEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			if (dto.EndDate == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)OrderItemFieldIndex.EndDate, default(System.DateTime));
				
			}
			
			newEnt.EndDate = dto.EndDate;
			
			

			
			
			newEnt.Id = dto.Id;
			
			

			
			
			if (dto.MinStockLevel == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)OrderItemFieldIndex.MinStockLevel, default(System.Decimal));
				
			}
			
			newEnt.MinStockLevel = dto.MinStockLevel;
			
			

			
			
			if (dto.Notes == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)OrderItemFieldIndex.Notes, "");
				
			}
			
			newEnt.Notes = dto.Notes;
			
			

			
			
			newEnt.OrderId = dto.OrderId;
			
			

			
			
			if (dto.Price == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)OrderItemFieldIndex.Price, default(System.Decimal));
				
			}
			
			newEnt.Price = dto.Price;
			
			

			
			
			newEnt.ProductId = dto.ProductId;
			
			

			
			
			newEnt.Quantity = dto.Quantity;
			
			

			
			
			if (dto.StartDate == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)OrderItemFieldIndex.StartDate, default(System.DateTime));
				
			}
			
			newEnt.StartDate = dto.StartDate;
			
			

			
			
			if (dto.Stock == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)OrderItemFieldIndex.Stock, default(System.Decimal));
				
			}
			
			newEnt.Stock = dto.Stock;
			
			

			
			
			if (dto.TotalPrice == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)OrderItemFieldIndex.TotalPrice, default(System.Decimal));
				
			}
			
			newEnt.TotalPrice = dto.TotalPrice;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.Order = dto.Order.ToEntity(cache);
			
			newEnt.Product = dto.Product.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static OrderItemDTO ToDTO(this OrderItemEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (OrderItemDTO)cache[ent];
			
			var newDTO = new OrderItemDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.EndDate = ent.EndDate;
			

			newDTO.Id = ent.Id;
			

			newDTO.MinStockLevel = ent.MinStockLevel;
			

			newDTO.Notes = ent.Notes;
			

			newDTO.OrderId = ent.OrderId;
			

			newDTO.Price = ent.Price;
			

			newDTO.ProductId = ent.ProductId;
			

			newDTO.Quantity = ent.Quantity;
			

			newDTO.StartDate = ent.StartDate;
			

			newDTO.Stock = ent.Stock;
			

			newDTO.TotalPrice = ent.TotalPrice;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.Order = ent.Order.ToDTO(cache);
			
			newDTO.Product = ent.Product.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}