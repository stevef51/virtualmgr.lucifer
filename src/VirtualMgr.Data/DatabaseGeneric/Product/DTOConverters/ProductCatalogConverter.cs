﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class ProductCatalogConverter
	{
	
		public static ProductCatalogEntity ToEntity(this ProductCatalogDTO dto)
		{
			return dto.ToEntity(new ProductCatalogEntity(), new Dictionary<object, object>());
		}

		public static ProductCatalogEntity ToEntity(this ProductCatalogDTO dto, ProductCatalogEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static ProductCatalogEntity ToEntity(this ProductCatalogDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new ProductCatalogEntity(), cache);
		}
	
		public static ProductCatalogDTO ToDTO(this ProductCatalogEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static ProductCatalogEntity ToEntity(this ProductCatalogDTO dto, ProductCatalogEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (ProductCatalogEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Archived = dto.Archived;
			
			

			
			
			if (dto.Description == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProductCatalogFieldIndex.Description, "");
				
			}
			
			newEnt.Description = dto.Description;
			
			

			
			
			newEnt.FacilityStructureId = dto.FacilityStructureId;
			
			

			
			
			newEnt.Id = dto.Id;
			
			
			

			
			
			newEnt.Name = dto.Name;
			
			

			
			
			newEnt.ShowLineItemNotesWhenOrdering = dto.ShowLineItemNotesWhenOrdering;
			
			

			
			
			newEnt.ShowPricingWhenOrdering = dto.ShowPricingWhenOrdering;
			
			

			
			
			newEnt.StockCountWhenOrdering = dto.StockCountWhenOrdering;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.FacilityStructure = dto.FacilityStructure.ToEntity(cache);
			
			
			
			foreach (var related in dto.Orders)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.Orders.Contains(relatedEntity))
				{
					newEnt.Orders.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ProductCatalogFacilityStructureRules)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProductCatalogFacilityStructureRules.Contains(relatedEntity))
				{
					newEnt.ProductCatalogFacilityStructureRules.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ProductCatalogItems)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProductCatalogItems.Contains(relatedEntity))
				{
					newEnt.ProductCatalogItems.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static ProductCatalogDTO ToDTO(this ProductCatalogEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (ProductCatalogDTO)cache[ent];
			
			var newDTO = new ProductCatalogDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Archived = ent.Archived;
			

			newDTO.Description = ent.Description;
			

			newDTO.FacilityStructureId = ent.FacilityStructureId;
			

			newDTO.Id = ent.Id;
			

			newDTO.Name = ent.Name;
			

			newDTO.ShowLineItemNotesWhenOrdering = ent.ShowLineItemNotesWhenOrdering;
			

			newDTO.ShowPricingWhenOrdering = ent.ShowPricingWhenOrdering;
			

			newDTO.StockCountWhenOrdering = ent.StockCountWhenOrdering;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.FacilityStructure = ent.FacilityStructure.ToDTO(cache);
			
			
			
			foreach (var related in ent.Orders)
			{
				newDTO.Orders.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ProductCatalogFacilityStructureRules)
			{
				newDTO.ProductCatalogFacilityStructureRules.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ProductCatalogItems)
			{
				newDTO.ProductCatalogItems.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}