﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class MediaFolderPolicyConverter
	{
	
		public static MediaFolderPolicyEntity ToEntity(this MediaFolderPolicyDTO dto)
		{
			return dto.ToEntity(new MediaFolderPolicyEntity(), new Dictionary<object, object>());
		}

		public static MediaFolderPolicyEntity ToEntity(this MediaFolderPolicyDTO dto, MediaFolderPolicyEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static MediaFolderPolicyEntity ToEntity(this MediaFolderPolicyDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new MediaFolderPolicyEntity(), cache);
		}
	
		public static MediaFolderPolicyDTO ToDTO(this MediaFolderPolicyEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static MediaFolderPolicyEntity ToEntity(this MediaFolderPolicyDTO dto, MediaFolderPolicyEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (MediaFolderPolicyEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Data = dto.Data;
			
			

			
			
			newEnt.MediaFolderId = dto.MediaFolderId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			newEnt.MediaFolder = dto.MediaFolder.ToEntity(cache);
			
			
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static MediaFolderPolicyDTO ToDTO(this MediaFolderPolicyEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (MediaFolderPolicyDTO)cache[ent];
			
			var newDTO = new MediaFolderPolicyDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Data = ent.Data;
			

			newDTO.MediaFolderId = ent.MediaFolderId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			newDTO.MediaFolder = ent.MediaFolder.ToDTO(cache);
			
			
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}