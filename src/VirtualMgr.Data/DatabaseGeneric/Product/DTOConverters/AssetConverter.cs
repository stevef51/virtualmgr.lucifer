﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class AssetConverter
	{
	
		public static AssetEntity ToEntity(this AssetDTO dto)
		{
			return dto.ToEntity(new AssetEntity(), new Dictionary<object, object>());
		}

		public static AssetEntity ToEntity(this AssetDTO dto, AssetEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static AssetEntity ToEntity(this AssetDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new AssetEntity(), cache);
		}
	
		public static AssetDTO ToDTO(this AssetEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static AssetEntity ToEntity(this AssetDTO dto, AssetEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (AssetEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Archived = dto.Archived;
			
			

			
			
			newEnt.AssetTypeId = dto.AssetTypeId;
			
			

			
			
			if (dto.CompanyId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)AssetFieldIndex.CompanyId, default(System.Guid));
				
			}
			
			newEnt.CompanyId = dto.CompanyId;
			
			

			
			
			newEnt.DateCreated = dto.DateCreated;
			
			

			
			
			if (dto.DateDecomissioned == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)AssetFieldIndex.DateDecomissioned, default(System.DateTime));
				
			}
			
			newEnt.DateDecomissioned = dto.DateDecomissioned;
			
			

			
			
			if (dto.FacilityStructureId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)AssetFieldIndex.FacilityStructureId, default(System.Int32));
				
			}
			
			newEnt.FacilityStructureId = dto.FacilityStructureId;
			
			

			
			
			newEnt.Id = dto.Id;
			
			
			

			
			
			newEnt.Name = dto.Name;
			
			
			
			cache.Add(dto, newEnt);
			
			
			newEnt.Beacon = dto.Beacon.ToEntity(cache);
			
			
			
			newEnt.AssetType = dto.AssetType.ToEntity(cache);
			
			
			
			foreach (var related in dto.AssetContextDatas)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.AssetContextDatas.Contains(relatedEntity))
				{
					newEnt.AssetContextDatas.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.AssetSchedules)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.AssetSchedules.Contains(relatedEntity))
				{
					newEnt.AssetSchedules.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ESProbes)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ESProbes.Contains(relatedEntity))
				{
					newEnt.ESProbes.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ProjectJobTasks)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProjectJobTasks.Contains(relatedEntity))
				{
					newEnt.ProjectJobTasks.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.Qrcodes)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.Qrcodes.Contains(relatedEntity))
				{
					newEnt.Qrcodes.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static AssetDTO ToDTO(this AssetEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (AssetDTO)cache[ent];
			
			var newDTO = new AssetDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Archived = ent.Archived;
			

			newDTO.AssetTypeId = ent.AssetTypeId;
			

			newDTO.CompanyId = ent.CompanyId;
			

			newDTO.DateCreated = ent.DateCreated;
			

			newDTO.DateDecomissioned = ent.DateDecomissioned;
			

			newDTO.FacilityStructureId = ent.FacilityStructureId;
			

			newDTO.Id = ent.Id;
			

			newDTO.Name = ent.Name;
			
			
			
			cache.Add(ent, newDTO);
			
			
			newDTO.Beacon = ent.Beacon.ToDTO(cache);
			
			
			
			newDTO.AssetType = ent.AssetType.ToDTO(cache);
			
			
			
			foreach (var related in ent.AssetContextDatas)
			{
				newDTO.AssetContextDatas.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.AssetSchedules)
			{
				newDTO.AssetSchedules.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ESProbes)
			{
				newDTO.ESProbes.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ProjectJobTasks)
			{
				newDTO.ProjectJobTasks.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.Qrcodes)
			{
				newDTO.Qrcodes.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}