﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class ESSensorReadingConverter
	{
	
		public static ESSensorReadingEntity ToEntity(this ESSensorReadingDTO dto)
		{
			return dto.ToEntity(new ESSensorReadingEntity(), new Dictionary<object, object>());
		}

		public static ESSensorReadingEntity ToEntity(this ESSensorReadingDTO dto, ESSensorReadingEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static ESSensorReadingEntity ToEntity(this ESSensorReadingDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new ESSensorReadingEntity(), cache);
		}
	
		public static ESSensorReadingDTO ToDTO(this ESSensorReadingEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static ESSensorReadingEntity ToEntity(this ESSensorReadingDTO dto, ESSensorReadingEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (ESSensorReadingEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Id = dto.Id;
			
			

			
			
			newEnt.SensorId = dto.SensorId;
			
			

			
			
			newEnt.SensorTimestampUtc = dto.SensorTimestampUtc;
			
			

			
			
			newEnt.ServerTimestampUtc = dto.ServerTimestampUtc;
			
			

			
			
			if (dto.TabletUuid == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ESSensorReadingFieldIndex.TabletUuid, "");
				
			}
			
			newEnt.TabletUuid = dto.TabletUuid;
			
			

			
			
			newEnt.Value = dto.Value;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.SensorReadingTabletUuid = dto.SensorReadingTabletUuid.ToEntity(cache);
			
			newEnt.Sensor = dto.Sensor.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static ESSensorReadingDTO ToDTO(this ESSensorReadingEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (ESSensorReadingDTO)cache[ent];
			
			var newDTO = new ESSensorReadingDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Id = ent.Id;
			

			newDTO.SensorId = ent.SensorId;
			

			newDTO.SensorTimestampUtc = ent.SensorTimestampUtc;
			

			newDTO.ServerTimestampUtc = ent.ServerTimestampUtc;
			

			newDTO.TabletUuid = ent.TabletUuid;
			

			newDTO.Value = ent.Value;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.SensorReadingTabletUuid = ent.SensorReadingTabletUuid.ToDTO(cache);
			
			newDTO.Sensor = ent.Sensor.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}