﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class RosterEventConditionActionConverter
	{
	
		public static RosterEventConditionActionEntity ToEntity(this RosterEventConditionActionDTO dto)
		{
			return dto.ToEntity(new RosterEventConditionActionEntity(), new Dictionary<object, object>());
		}

		public static RosterEventConditionActionEntity ToEntity(this RosterEventConditionActionDTO dto, RosterEventConditionActionEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static RosterEventConditionActionEntity ToEntity(this RosterEventConditionActionDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new RosterEventConditionActionEntity(), cache);
		}
	
		public static RosterEventConditionActionDTO ToDTO(this RosterEventConditionActionEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static RosterEventConditionActionEntity ToEntity(this RosterEventConditionActionDTO dto, RosterEventConditionActionEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (RosterEventConditionActionEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Action = dto.Action;
			
			

			
			
			if (dto.Data == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)RosterEventConditionActionFieldIndex.Data, "");
				
			}
			
			newEnt.Data = dto.Data;
			
			

			
			
			newEnt.Id = dto.Id;
			
			

			
			
			newEnt.RosterEventConditionId = dto.RosterEventConditionId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.RosterEventCondition = dto.RosterEventCondition.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static RosterEventConditionActionDTO ToDTO(this RosterEventConditionActionEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (RosterEventConditionActionDTO)cache[ent];
			
			var newDTO = new RosterEventConditionActionDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Action = ent.Action;
			

			newDTO.Data = ent.Data;
			

			newDTO.Id = ent.Id;
			

			newDTO.RosterEventConditionId = ent.RosterEventConditionId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.RosterEventCondition = ent.RosterEventCondition.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}