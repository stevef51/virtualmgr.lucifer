﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class ResourceDataConverter
	{
	
		public static ResourceDataEntity ToEntity(this ResourceDataDTO dto)
		{
			return dto.ToEntity(new ResourceDataEntity(), new Dictionary<object, object>());
		}

		public static ResourceDataEntity ToEntity(this ResourceDataDTO dto, ResourceDataEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static ResourceDataEntity ToEntity(this ResourceDataDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new ResourceDataEntity(), cache);
		}
	
		public static ResourceDataDTO ToDTO(this ResourceDataEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static ResourceDataEntity ToEntity(this ResourceDataDTO dto, ResourceDataEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (ResourceDataEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Data = dto.Data;
			
			

			
			
			newEnt.ResourceId = dto.ResourceId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			newEnt.Resource = dto.Resource.ToEntity(cache);
			
			
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static ResourceDataDTO ToDTO(this ResourceDataEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (ResourceDataDTO)cache[ent];
			
			var newDTO = new ResourceDataDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Data = ent.Data;
			

			newDTO.ResourceId = ent.ResourceId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			newDTO.Resource = ent.Resource.ToDTO(cache);
			
			
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}