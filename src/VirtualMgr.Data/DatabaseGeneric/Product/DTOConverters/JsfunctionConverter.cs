﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class JsfunctionConverter
	{
	
		public static JsfunctionEntity ToEntity(this JsfunctionDTO dto)
		{
			return dto.ToEntity(new JsfunctionEntity(), new Dictionary<object, object>());
		}

		public static JsfunctionEntity ToEntity(this JsfunctionDTO dto, JsfunctionEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static JsfunctionEntity ToEntity(this JsfunctionDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new JsfunctionEntity(), cache);
		}
	
		public static JsfunctionDTO ToDTO(this JsfunctionEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static JsfunctionEntity ToEntity(this JsfunctionDTO dto, JsfunctionEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (JsfunctionEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Archived = dto.Archived;
			
			

			
			
			newEnt.Id = dto.Id;
			
			

			
			
			newEnt.Name = dto.Name;
			
			

			
			
			newEnt.Script = dto.Script;
			
			

			
			
			newEnt.Type = dto.Type;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			
			
			foreach (var related in dto.Products)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.Products.Contains(relatedEntity))
				{
					newEnt.Products.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ProductCatalogItems)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProductCatalogItems.Contains(relatedEntity))
				{
					newEnt.ProductCatalogItems.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static JsfunctionDTO ToDTO(this JsfunctionEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (JsfunctionDTO)cache[ent];
			
			var newDTO = new JsfunctionDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Archived = ent.Archived;
			

			newDTO.Id = ent.Id;
			

			newDTO.Name = ent.Name;
			

			newDTO.Script = ent.Script;
			

			newDTO.Type = ent.Type;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			
			
			foreach (var related in ent.Products)
			{
				newDTO.Products.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ProductCatalogItems)
			{
				newDTO.ProductCatalogItems.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}