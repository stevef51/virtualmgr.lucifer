﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class UserLogConverter
	{
	
		public static UserLogEntity ToEntity(this UserLogDTO dto)
		{
			return dto.ToEntity(new UserLogEntity(), new Dictionary<object, object>());
		}

		public static UserLogEntity ToEntity(this UserLogDTO dto, UserLogEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static UserLogEntity ToEntity(this UserLogDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new UserLogEntity(), cache);
		}
	
		public static UserLogDTO ToDTO(this UserLogEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static UserLogEntity ToEntity(this UserLogDTO dto, UserLogEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (UserLogEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Id = dto.Id;
			
			

			
			
			if (dto.Latitude == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)UserLogFieldIndex.Latitude, default(System.Decimal));
				
			}
			
			newEnt.Latitude = dto.Latitude;
			
			

			
			
			if (dto.LoginMediaId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)UserLogFieldIndex.LoginMediaId, default(System.Guid));
				
			}
			
			newEnt.LoginMediaId = dto.LoginMediaId;
			
			

			
			
			newEnt.LogInTime = dto.LogInTime;
			
			

			
			
			if (dto.LogOutTime == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)UserLogFieldIndex.LogOutTime, default(System.DateTime));
				
			}
			
			newEnt.LogOutTime = dto.LogOutTime;
			
			

			
			
			if (dto.Longitude == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)UserLogFieldIndex.Longitude, default(System.Decimal));
				
			}
			
			newEnt.Longitude = dto.Longitude;
			
			

			
			
			if (dto.NearestLocation == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)UserLogFieldIndex.NearestLocation, default(System.Guid));
				
			}
			
			newEnt.NearestLocation = dto.NearestLocation;
			
			

			
			
			if (dto.TabletUuid == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)UserLogFieldIndex.TabletUuid, "");
				
			}
			
			newEnt.TabletUuid = dto.TabletUuid;
			
			

			
			
			newEnt.UserId = dto.UserId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.LoginMedia = dto.LoginMedia.ToEntity(cache);
			
			newEnt.UserData = dto.UserData.ToEntity(cache);
			
			newEnt.UserData_ = dto.UserData_.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static UserLogDTO ToDTO(this UserLogEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (UserLogDTO)cache[ent];
			
			var newDTO = new UserLogDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Id = ent.Id;
			

			newDTO.Latitude = ent.Latitude;
			

			newDTO.LoginMediaId = ent.LoginMediaId;
			

			newDTO.LogInTime = ent.LogInTime;
			

			newDTO.LogOutTime = ent.LogOutTime;
			

			newDTO.Longitude = ent.Longitude;
			

			newDTO.NearestLocation = ent.NearestLocation;
			

			newDTO.TabletUuid = ent.TabletUuid;
			

			newDTO.UserId = ent.UserId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.LoginMedia = ent.LoginMedia.ToDTO(cache);
			
			newDTO.UserData = ent.UserData.ToDTO(cache);
			
			newDTO.UserData_ = ent.UserData_.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}