﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class ProjectJobTaskTypeRelationshipConverter
	{
	
		public static ProjectJobTaskTypeRelationshipEntity ToEntity(this ProjectJobTaskTypeRelationshipDTO dto)
		{
			return dto.ToEntity(new ProjectJobTaskTypeRelationshipEntity(), new Dictionary<object, object>());
		}

		public static ProjectJobTaskTypeRelationshipEntity ToEntity(this ProjectJobTaskTypeRelationshipDTO dto, ProjectJobTaskTypeRelationshipEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static ProjectJobTaskTypeRelationshipEntity ToEntity(this ProjectJobTaskTypeRelationshipDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new ProjectJobTaskTypeRelationshipEntity(), cache);
		}
	
		public static ProjectJobTaskTypeRelationshipDTO ToDTO(this ProjectJobTaskTypeRelationshipEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static ProjectJobTaskTypeRelationshipEntity ToEntity(this ProjectJobTaskTypeRelationshipDTO dto, ProjectJobTaskTypeRelationshipEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (ProjectJobTaskTypeRelationshipEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.ProjectJobTaskTypeDestinationId = dto.ProjectJobTaskTypeDestinationId;
			
			

			
			
			newEnt.ProjectJobTaskTypeSourceId = dto.ProjectJobTaskTypeSourceId;
			
			

			
			
			newEnt.RelationshipType = dto.RelationshipType;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.DestinationProjectJobTaskType = dto.DestinationProjectJobTaskType.ToEntity(cache);
			
			newEnt.SourceProjectJobTaskType = dto.SourceProjectJobTaskType.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static ProjectJobTaskTypeRelationshipDTO ToDTO(this ProjectJobTaskTypeRelationshipEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (ProjectJobTaskTypeRelationshipDTO)cache[ent];
			
			var newDTO = new ProjectJobTaskTypeRelationshipDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.ProjectJobTaskTypeDestinationId = ent.ProjectJobTaskTypeDestinationId;
			

			newDTO.ProjectJobTaskTypeSourceId = ent.ProjectJobTaskTypeSourceId;
			

			newDTO.RelationshipType = ent.RelationshipType;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.DestinationProjectJobTaskType = ent.DestinationProjectJobTaskType.ToDTO(cache);
			
			newDTO.SourceProjectJobTaskType = ent.SourceProjectJobTaskType.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}