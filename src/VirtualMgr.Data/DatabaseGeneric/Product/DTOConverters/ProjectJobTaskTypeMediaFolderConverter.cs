﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class ProjectJobTaskTypeMediaFolderConverter
	{
	
		public static ProjectJobTaskTypeMediaFolderEntity ToEntity(this ProjectJobTaskTypeMediaFolderDTO dto)
		{
			return dto.ToEntity(new ProjectJobTaskTypeMediaFolderEntity(), new Dictionary<object, object>());
		}

		public static ProjectJobTaskTypeMediaFolderEntity ToEntity(this ProjectJobTaskTypeMediaFolderDTO dto, ProjectJobTaskTypeMediaFolderEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static ProjectJobTaskTypeMediaFolderEntity ToEntity(this ProjectJobTaskTypeMediaFolderDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new ProjectJobTaskTypeMediaFolderEntity(), cache);
		}
	
		public static ProjectJobTaskTypeMediaFolderDTO ToDTO(this ProjectJobTaskTypeMediaFolderEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static ProjectJobTaskTypeMediaFolderEntity ToEntity(this ProjectJobTaskTypeMediaFolderDTO dto, ProjectJobTaskTypeMediaFolderEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (ProjectJobTaskTypeMediaFolderEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.MediaFolderId = dto.MediaFolderId;
			
			

			
			
			newEnt.TaskTypeId = dto.TaskTypeId;
			
			

			
			
			if (dto.Text == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobTaskTypeMediaFolderFieldIndex.Text, "");
				
			}
			
			newEnt.Text = dto.Text;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.ProjectJobTaskType = dto.ProjectJobTaskType.ToEntity(cache);
			
			newEnt.MediaFolder = dto.MediaFolder.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static ProjectJobTaskTypeMediaFolderDTO ToDTO(this ProjectJobTaskTypeMediaFolderEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (ProjectJobTaskTypeMediaFolderDTO)cache[ent];
			
			var newDTO = new ProjectJobTaskTypeMediaFolderDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.MediaFolderId = ent.MediaFolderId;
			

			newDTO.TaskTypeId = ent.TaskTypeId;
			

			newDTO.Text = ent.Text;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.ProjectJobTaskType = ent.ProjectJobTaskType.ToDTO(cache);
			
			newDTO.MediaFolder = ent.MediaFolder.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}