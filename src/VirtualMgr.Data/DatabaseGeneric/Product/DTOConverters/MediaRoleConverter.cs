﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class MediaRoleConverter
	{
	
		public static MediaRoleEntity ToEntity(this MediaRoleDTO dto)
		{
			return dto.ToEntity(new MediaRoleEntity(), new Dictionary<object, object>());
		}

		public static MediaRoleEntity ToEntity(this MediaRoleDTO dto, MediaRoleEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static MediaRoleEntity ToEntity(this MediaRoleDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new MediaRoleEntity(), cache);
		}
	
		public static MediaRoleDTO ToDTO(this MediaRoleEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static MediaRoleEntity ToEntity(this MediaRoleDTO dto, MediaRoleEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (MediaRoleEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.MediaId = dto.MediaId;
			
			

			
			
			newEnt.RoleId = dto.RoleId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.AspNetRole = dto.AspNetRole.ToEntity(cache);
			
			newEnt.Medium = dto.Medium.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static MediaRoleDTO ToDTO(this MediaRoleEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (MediaRoleDTO)cache[ent];
			
			var newDTO = new MediaRoleDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.MediaId = ent.MediaId;
			

			newDTO.RoleId = ent.RoleId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.AspNetRole = ent.AspNetRole.ToDTO(cache);
			
			newDTO.Medium = ent.Medium.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}