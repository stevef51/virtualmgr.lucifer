﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class ErrorLogTypeConverter
	{
	
		public static ErrorLogTypeEntity ToEntity(this ErrorLogTypeDTO dto)
		{
			return dto.ToEntity(new ErrorLogTypeEntity(), new Dictionary<object, object>());
		}

		public static ErrorLogTypeEntity ToEntity(this ErrorLogTypeDTO dto, ErrorLogTypeEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static ErrorLogTypeEntity ToEntity(this ErrorLogTypeDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new ErrorLogTypeEntity(), cache);
		}
	
		public static ErrorLogTypeDTO ToDTO(this ErrorLogTypeEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static ErrorLogTypeEntity ToEntity(this ErrorLogTypeDTO dto, ErrorLogTypeEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (ErrorLogTypeEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Id = dto.Id;
			
			
			

			
			
			newEnt.Message = dto.Message;
			
			

			
			
			newEnt.Source = dto.Source;
			
			

			
			
			newEnt.StackTrace = dto.StackTrace;
			
			

			
			
			newEnt.StackTraceHash = dto.StackTraceHash;
			
			

			
			
			newEnt.Type = dto.Type;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			
			
			foreach (var related in dto.ErrorLogItems)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ErrorLogItems.Contains(relatedEntity))
				{
					newEnt.ErrorLogItems.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static ErrorLogTypeDTO ToDTO(this ErrorLogTypeEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (ErrorLogTypeDTO)cache[ent];
			
			var newDTO = new ErrorLogTypeDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Id = ent.Id;
			

			newDTO.Message = ent.Message;
			

			newDTO.Source = ent.Source;
			

			newDTO.StackTrace = ent.StackTrace;
			

			newDTO.StackTraceHash = ent.StackTraceHash;
			

			newDTO.Type = ent.Type;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			
			
			foreach (var related in ent.ErrorLogItems)
			{
				newDTO.ErrorLogItems.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}