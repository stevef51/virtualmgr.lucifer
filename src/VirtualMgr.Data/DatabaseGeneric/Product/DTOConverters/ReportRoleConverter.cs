﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class ReportRoleConverter
	{
	
		public static ReportRoleEntity ToEntity(this ReportRoleDTO dto)
		{
			return dto.ToEntity(new ReportRoleEntity(), new Dictionary<object, object>());
		}

		public static ReportRoleEntity ToEntity(this ReportRoleDTO dto, ReportRoleEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static ReportRoleEntity ToEntity(this ReportRoleDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new ReportRoleEntity(), cache);
		}
	
		public static ReportRoleDTO ToDTO(this ReportRoleEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static ReportRoleEntity ToEntity(this ReportRoleDTO dto, ReportRoleEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (ReportRoleEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.ReportId = dto.ReportId;
			
			

			
			
			newEnt.RoleId = dto.RoleId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.AspNetRole = dto.AspNetRole.ToEntity(cache);
			
			newEnt.Report = dto.Report.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static ReportRoleDTO ToDTO(this ReportRoleEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (ReportRoleDTO)cache[ent];
			
			var newDTO = new ReportRoleDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.ReportId = ent.ReportId;
			

			newDTO.RoleId = ent.RoleId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.AspNetRole = ent.AspNetRole.ToDTO(cache);
			
			newDTO.Report = ent.Report.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}