﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class BeaconSightingConverter
	{
	
		public static BeaconSightingEntity ToEntity(this BeaconSightingDTO dto)
		{
			return dto.ToEntity(new BeaconSightingEntity(), new Dictionary<object, object>());
		}

		public static BeaconSightingEntity ToEntity(this BeaconSightingDTO dto, BeaconSightingEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static BeaconSightingEntity ToEntity(this BeaconSightingDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new BeaconSightingEntity(), cache);
		}
	
		public static BeaconSightingDTO ToDTO(this BeaconSightingEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static BeaconSightingEntity ToEntity(this BeaconSightingDTO dto, BeaconSightingEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (BeaconSightingEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			if (dto.BatteryPercent == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)BeaconSightingFieldIndex.BatteryPercent, default(System.Int32));
				
			}
			
			newEnt.BatteryPercent = dto.BatteryPercent;
			
			

			
			
			newEnt.BeaconId = dto.BeaconId;
			
			

			
			
			newEnt.BestRange = dto.BestRange;
			
			

			
			
			newEnt.BestRangeUtc = dto.BestRangeUtc;
			
			

			
			
			newEnt.FirstRangeUtc = dto.FirstRangeUtc;
			
			

			
			
			if (dto.GoneUtc == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)BeaconSightingFieldIndex.GoneUtc, default(System.DateTime));
				
			}
			
			newEnt.GoneUtc = dto.GoneUtc;
			
			

			
			
			if (dto.GpslocationId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)BeaconSightingFieldIndex.GpslocationId, default(System.Int64));
				
			}
			
			newEnt.GpslocationId = dto.GpslocationId;
			
			

			
			
			newEnt.Id = dto.Id;
			
			
			

			
			
			if (dto.LoggedInUserId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)BeaconSightingFieldIndex.LoggedInUserId, default(System.Guid));
				
			}
			
			newEnt.LoggedInUserId = dto.LoggedInUserId;
			
			

			
			
			if (dto.NextStaticBeaconSightingId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)BeaconSightingFieldIndex.NextStaticBeaconSightingId, default(System.Int64));
				
			}
			
			newEnt.NextStaticBeaconSightingId = dto.NextStaticBeaconSightingId;
			
			

			
			
			if (dto.PrevStaticBeaconSightingId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)BeaconSightingFieldIndex.PrevStaticBeaconSightingId, default(System.Int64));
				
			}
			
			newEnt.PrevStaticBeaconSightingId = dto.PrevStaticBeaconSightingId;
			
			

			
			
			newEnt.TabletUuid = dto.TabletUuid;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.Beacon = dto.Beacon.ToEntity(cache);
			
			newEnt.TabletUuid_ = dto.TabletUuid_.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static BeaconSightingDTO ToDTO(this BeaconSightingEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (BeaconSightingDTO)cache[ent];
			
			var newDTO = new BeaconSightingDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.BatteryPercent = ent.BatteryPercent;
			

			newDTO.BeaconId = ent.BeaconId;
			

			newDTO.BestRange = ent.BestRange;
			

			newDTO.BestRangeUtc = ent.BestRangeUtc;
			

			newDTO.FirstRangeUtc = ent.FirstRangeUtc;
			

			newDTO.GoneUtc = ent.GoneUtc;
			

			newDTO.GpslocationId = ent.GpslocationId;
			

			newDTO.Id = ent.Id;
			

			newDTO.LoggedInUserId = ent.LoggedInUserId;
			

			newDTO.NextStaticBeaconSightingId = ent.NextStaticBeaconSightingId;
			

			newDTO.PrevStaticBeaconSightingId = ent.PrevStaticBeaconSightingId;
			

			newDTO.TabletUuid = ent.TabletUuid;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.Beacon = ent.Beacon.ToDTO(cache);
			
			newDTO.TabletUuid_ = ent.TabletUuid_.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}