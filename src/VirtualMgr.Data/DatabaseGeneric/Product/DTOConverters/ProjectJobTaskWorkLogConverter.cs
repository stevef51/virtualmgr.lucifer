﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class ProjectJobTaskWorkLogConverter
	{
	
		public static ProjectJobTaskWorkLogEntity ToEntity(this ProjectJobTaskWorkLogDTO dto)
		{
			return dto.ToEntity(new ProjectJobTaskWorkLogEntity(), new Dictionary<object, object>());
		}

		public static ProjectJobTaskWorkLogEntity ToEntity(this ProjectJobTaskWorkLogDTO dto, ProjectJobTaskWorkLogEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static ProjectJobTaskWorkLogEntity ToEntity(this ProjectJobTaskWorkLogDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new ProjectJobTaskWorkLogEntity(), cache);
		}
	
		public static ProjectJobTaskWorkLogDTO ToDTO(this ProjectJobTaskWorkLogEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static ProjectJobTaskWorkLogEntity ToEntity(this ProjectJobTaskWorkLogDTO dto, ProjectJobTaskWorkLogEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (ProjectJobTaskWorkLogEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			if (dto.ActualDuration == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobTaskWorkLogFieldIndex.ActualDuration, default(System.Decimal));
				
			}
			
			newEnt.ActualDuration = dto.ActualDuration;
			
			

			
			
			if (dto.ApprovedDuration == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobTaskWorkLogFieldIndex.ApprovedDuration, default(System.Decimal));
				
			}
			
			newEnt.ApprovedDuration = dto.ApprovedDuration;
			
			

			
			
			if (dto.ClockinAccuracy == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobTaskWorkLogFieldIndex.ClockinAccuracy, default(System.Decimal));
				
			}
			
			newEnt.ClockinAccuracy = dto.ClockinAccuracy;
			
			

			
			
			if (dto.ClockinLatitude == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobTaskWorkLogFieldIndex.ClockinLatitude, default(System.Decimal));
				
			}
			
			newEnt.ClockinLatitude = dto.ClockinLatitude;
			
			

			
			
			if (dto.ClockinLocationStatus == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobTaskWorkLogFieldIndex.ClockinLocationStatus, default(System.Int32));
				
			}
			
			newEnt.ClockinLocationStatus = dto.ClockinLocationStatus;
			
			

			
			
			if (dto.ClockinLongitude == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobTaskWorkLogFieldIndex.ClockinLongitude, default(System.Decimal));
				
			}
			
			newEnt.ClockinLongitude = dto.ClockinLongitude;
			
			

			
			
			if (dto.ClockinMetresFromSite == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobTaskWorkLogFieldIndex.ClockinMetresFromSite, default(System.Decimal));
				
			}
			
			newEnt.ClockinMetresFromSite = dto.ClockinMetresFromSite;
			
			

			
			
			if (dto.ClockoutAccuracy == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobTaskWorkLogFieldIndex.ClockoutAccuracy, default(System.Decimal));
				
			}
			
			newEnt.ClockoutAccuracy = dto.ClockoutAccuracy;
			
			

			
			
			if (dto.ClockoutLatitude == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobTaskWorkLogFieldIndex.ClockoutLatitude, default(System.Decimal));
				
			}
			
			newEnt.ClockoutLatitude = dto.ClockoutLatitude;
			
			

			
			
			if (dto.ClockoutLocationStatus == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobTaskWorkLogFieldIndex.ClockoutLocationStatus, default(System.Int32));
				
			}
			
			newEnt.ClockoutLocationStatus = dto.ClockoutLocationStatus;
			
			

			
			
			if (dto.ClockoutLongitude == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobTaskWorkLogFieldIndex.ClockoutLongitude, default(System.Decimal));
				
			}
			
			newEnt.ClockoutLongitude = dto.ClockoutLongitude;
			
			

			
			
			if (dto.ClockoutMetresFromSite == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobTaskWorkLogFieldIndex.ClockoutMetresFromSite, default(System.Decimal));
				
			}
			
			newEnt.ClockoutMetresFromSite = dto.ClockoutMetresFromSite;
			
			

			
			
			if (dto.DateCompleted == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobTaskWorkLogFieldIndex.DateCompleted, default(System.DateTime));
				
			}
			
			newEnt.DateCompleted = dto.DateCompleted;
			
			

			
			
			newEnt.DateCreated = dto.DateCreated;
			
			

			
			
			newEnt.Id = dto.Id;
			
			

			
			
			newEnt.InactiveDuration = dto.InactiveDuration;
			
			

			
			
			if (dto.Notes == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobTaskWorkLogFieldIndex.Notes, "");
				
			}
			
			newEnt.Notes = dto.Notes;
			
			

			
			
			newEnt.ProjectJobTaskId = dto.ProjectJobTaskId;
			
			

			
			
			newEnt.UserId = dto.UserId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.ProjectJobTask = dto.ProjectJobTask.ToEntity(cache);
			
			newEnt.UserData = dto.UserData.ToEntity(cache);
			
			
			
			foreach (var related in dto.TimesheetedTaskWorkLogs)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.TimesheetedTaskWorkLogs.Contains(relatedEntity))
				{
					newEnt.TimesheetedTaskWorkLogs.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static ProjectJobTaskWorkLogDTO ToDTO(this ProjectJobTaskWorkLogEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (ProjectJobTaskWorkLogDTO)cache[ent];
			
			var newDTO = new ProjectJobTaskWorkLogDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.ActualDuration = ent.ActualDuration;
			

			newDTO.ApprovedDuration = ent.ApprovedDuration;
			

			newDTO.ClockinAccuracy = ent.ClockinAccuracy;
			

			newDTO.ClockinLatitude = ent.ClockinLatitude;
			

			newDTO.ClockinLocationStatus = ent.ClockinLocationStatus;
			

			newDTO.ClockinLongitude = ent.ClockinLongitude;
			

			newDTO.ClockinMetresFromSite = ent.ClockinMetresFromSite;
			

			newDTO.ClockoutAccuracy = ent.ClockoutAccuracy;
			

			newDTO.ClockoutLatitude = ent.ClockoutLatitude;
			

			newDTO.ClockoutLocationStatus = ent.ClockoutLocationStatus;
			

			newDTO.ClockoutLongitude = ent.ClockoutLongitude;
			

			newDTO.ClockoutMetresFromSite = ent.ClockoutMetresFromSite;
			

			newDTO.DateCompleted = ent.DateCompleted;
			

			newDTO.DateCreated = ent.DateCreated;
			

			newDTO.Id = ent.Id;
			

			newDTO.InactiveDuration = ent.InactiveDuration;
			

			newDTO.Notes = ent.Notes;
			

			newDTO.ProjectJobTaskId = ent.ProjectJobTaskId;
			

			newDTO.UserId = ent.UserId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.ProjectJobTask = ent.ProjectJobTask.ToDTO(cache);
			
			newDTO.UserData = ent.UserData.ToDTO(cache);
			
			
			
			foreach (var related in ent.TimesheetedTaskWorkLogs)
			{
				newDTO.TimesheetedTaskWorkLogs.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}