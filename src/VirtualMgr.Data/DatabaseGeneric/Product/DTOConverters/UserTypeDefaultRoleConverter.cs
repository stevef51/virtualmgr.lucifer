﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class UserTypeDefaultRoleConverter
	{
	
		public static UserTypeDefaultRoleEntity ToEntity(this UserTypeDefaultRoleDTO dto)
		{
			return dto.ToEntity(new UserTypeDefaultRoleEntity(), new Dictionary<object, object>());
		}

		public static UserTypeDefaultRoleEntity ToEntity(this UserTypeDefaultRoleDTO dto, UserTypeDefaultRoleEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static UserTypeDefaultRoleEntity ToEntity(this UserTypeDefaultRoleDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new UserTypeDefaultRoleEntity(), cache);
		}
	
		public static UserTypeDefaultRoleDTO ToDTO(this UserTypeDefaultRoleEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static UserTypeDefaultRoleEntity ToEntity(this UserTypeDefaultRoleDTO dto, UserTypeDefaultRoleEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (UserTypeDefaultRoleEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.RoleId = dto.RoleId;
			
			

			
			
			newEnt.UserTypeId = dto.UserTypeId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.AspNetRole = dto.AspNetRole.ToEntity(cache);
			
			newEnt.UserType = dto.UserType.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static UserTypeDefaultRoleDTO ToDTO(this UserTypeDefaultRoleEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (UserTypeDefaultRoleDTO)cache[ent];
			
			var newDTO = new UserTypeDefaultRoleDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.RoleId = ent.RoleId;
			

			newDTO.UserTypeId = ent.UserTypeId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.AspNetRole = ent.AspNetRole.ToDTO(cache);
			
			newDTO.UserType = ent.UserType.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}