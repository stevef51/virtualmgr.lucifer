﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class GatewayCustomerConverter
	{
	
		public static GatewayCustomerEntity ToEntity(this GatewayCustomerDTO dto)
		{
			return dto.ToEntity(new GatewayCustomerEntity(), new Dictionary<object, object>());
		}

		public static GatewayCustomerEntity ToEntity(this GatewayCustomerDTO dto, GatewayCustomerEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static GatewayCustomerEntity ToEntity(this GatewayCustomerDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new GatewayCustomerEntity(), cache);
		}
	
		public static GatewayCustomerDTO ToDTO(this GatewayCustomerEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static GatewayCustomerEntity ToEntity(this GatewayCustomerDTO dto, GatewayCustomerEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (GatewayCustomerEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.GatewayName = dto.GatewayName;
			
			

			
			
			newEnt.TokenCustomerId = dto.TokenCustomerId;
			
			

			
			
			newEnt.UserId = dto.UserId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.UserData = dto.UserData.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static GatewayCustomerDTO ToDTO(this GatewayCustomerEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (GatewayCustomerDTO)cache[ent];
			
			var newDTO = new GatewayCustomerDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.GatewayName = ent.GatewayName;
			

			newDTO.TokenCustomerId = ent.TokenCustomerId;
			

			newDTO.UserId = ent.UserId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.UserData = ent.UserData.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}