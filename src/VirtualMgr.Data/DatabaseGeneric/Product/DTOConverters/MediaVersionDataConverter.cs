﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class MediaVersionDataConverter
	{
	
		public static MediaVersionDataEntity ToEntity(this MediaVersionDataDTO dto)
		{
			return dto.ToEntity(new MediaVersionDataEntity(), new Dictionary<object, object>());
		}

		public static MediaVersionDataEntity ToEntity(this MediaVersionDataDTO dto, MediaVersionDataEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static MediaVersionDataEntity ToEntity(this MediaVersionDataDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new MediaVersionDataEntity(), cache);
		}
	
		public static MediaVersionDataDTO ToDTO(this MediaVersionDataEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static MediaVersionDataEntity ToEntity(this MediaVersionDataDTO dto, MediaVersionDataEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (MediaVersionDataEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Data = dto.Data;
			
			

			
			
			newEnt.MediaVersionId = dto.MediaVersionId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			newEnt.MediaVersion = dto.MediaVersion.ToEntity(cache);
			
			
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static MediaVersionDataDTO ToDTO(this MediaVersionDataEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (MediaVersionDataDTO)cache[ent];
			
			var newDTO = new MediaVersionDataDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Data = ent.Data;
			

			newDTO.MediaVersionId = ent.MediaVersionId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			newDTO.MediaVersion = ent.MediaVersion.ToDTO(cache);
			
			
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}