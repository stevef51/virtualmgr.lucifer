﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class PEJobHistoryConverter
	{
	
		public static PEJobHistoryEntity ToEntity(this PEJobHistoryDTO dto)
		{
			return dto.ToEntity(new PEJobHistoryEntity(), new Dictionary<object, object>());
		}

		public static PEJobHistoryEntity ToEntity(this PEJobHistoryDTO dto, PEJobHistoryEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static PEJobHistoryEntity ToEntity(this PEJobHistoryDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new PEJobHistoryEntity(), cache);
		}
	
		public static PEJobHistoryDTO ToDTO(this PEJobHistoryEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static PEJobHistoryEntity ToEntity(this PEJobHistoryDTO dto, PEJobHistoryEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (PEJobHistoryEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Id = dto.Id;
			
			
			

			
			
			newEnt.JobId = dto.JobId;
			
			

			
			
			if (dto.Message == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)PEJobHistoryFieldIndex.Message, "");
				
			}
			
			newEnt.Message = dto.Message;
			
			

			
			
			newEnt.RanAtUtc = dto.RanAtUtc;
			
			

			
			
			newEnt.Success = dto.Success;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.PEJob = dto.PEJob.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static PEJobHistoryDTO ToDTO(this PEJobHistoryEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (PEJobHistoryDTO)cache[ent];
			
			var newDTO = new PEJobHistoryDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Id = ent.Id;
			

			newDTO.JobId = ent.JobId;
			

			newDTO.Message = ent.Message;
			

			newDTO.RanAtUtc = ent.RanAtUtc;
			

			newDTO.Success = ent.Success;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.PEJob = ent.PEJob.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}