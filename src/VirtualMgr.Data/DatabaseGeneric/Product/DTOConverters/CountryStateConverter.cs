﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class CountryStateConverter
	{
	
		public static CountryStateEntity ToEntity(this CountryStateDTO dto)
		{
			return dto.ToEntity(new CountryStateEntity(), new Dictionary<object, object>());
		}

		public static CountryStateEntity ToEntity(this CountryStateDTO dto, CountryStateEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static CountryStateEntity ToEntity(this CountryStateDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new CountryStateEntity(), cache);
		}
	
		public static CountryStateDTO ToDTO(this CountryStateEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static CountryStateEntity ToEntity(this CountryStateDTO dto, CountryStateEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (CountryStateEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Abbreviation = dto.Abbreviation;
			
			

			
			
			newEnt.Id = dto.Id;
			
			

			
			
			newEnt.Name = dto.Name;
			
			

			
			
			newEnt.TimeZone = dto.TimeZone;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			
			
			foreach (var related in dto.Companies)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.Companies.Contains(relatedEntity))
				{
					newEnt.Companies.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static CountryStateDTO ToDTO(this CountryStateEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (CountryStateDTO)cache[ent];
			
			var newDTO = new CountryStateDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Abbreviation = ent.Abbreviation;
			

			newDTO.Id = ent.Id;
			

			newDTO.Name = ent.Name;
			

			newDTO.TimeZone = ent.TimeZone;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			
			
			foreach (var related in ent.Companies)
			{
				newDTO.Companies.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}