﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class LabelMediaConverter
	{
	
		public static LabelMediaEntity ToEntity(this LabelMediaDTO dto)
		{
			return dto.ToEntity(new LabelMediaEntity(), new Dictionary<object, object>());
		}

		public static LabelMediaEntity ToEntity(this LabelMediaDTO dto, LabelMediaEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static LabelMediaEntity ToEntity(this LabelMediaDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new LabelMediaEntity(), cache);
		}
	
		public static LabelMediaDTO ToDTO(this LabelMediaEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static LabelMediaEntity ToEntity(this LabelMediaDTO dto, LabelMediaEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (LabelMediaEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.LabelId = dto.LabelId;
			
			

			
			
			newEnt.MediaId = dto.MediaId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.Label = dto.Label.ToEntity(cache);
			
			newEnt.Medium = dto.Medium.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static LabelMediaDTO ToDTO(this LabelMediaEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (LabelMediaDTO)cache[ent];
			
			var newDTO = new LabelMediaDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.LabelId = ent.LabelId;
			

			newDTO.MediaId = ent.MediaId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.Label = ent.Label.ToDTO(cache);
			
			newDTO.Medium = ent.Medium.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}