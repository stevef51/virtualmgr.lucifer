﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class MediaFolderConverter
	{
	
		public static MediaFolderEntity ToEntity(this MediaFolderDTO dto)
		{
			return dto.ToEntity(new MediaFolderEntity(), new Dictionary<object, object>());
		}

		public static MediaFolderEntity ToEntity(this MediaFolderDTO dto, MediaFolderEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static MediaFolderEntity ToEntity(this MediaFolderDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new MediaFolderEntity(), cache);
		}
	
		public static MediaFolderDTO ToDTO(this MediaFolderEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static MediaFolderEntity ToEntity(this MediaFolderDTO dto, MediaFolderEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (MediaFolderEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.AutoCreated = dto.AutoCreated;
			
			

			
			
			newEnt.Hidden = dto.Hidden;
			
			

			
			
			newEnt.Id = dto.Id;
			
			

			
			
			if (dto.ParentId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)MediaFolderFieldIndex.ParentId, default(System.Guid));
				
			}
			
			newEnt.ParentId = dto.ParentId;
			
			

			
			
			if (dto.ShowViaChecklistId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)MediaFolderFieldIndex.ShowViaChecklistId, default(System.Int32));
				
			}
			
			newEnt.ShowViaChecklistId = dto.ShowViaChecklistId;
			
			

			
			
			newEnt.SortOrder = dto.SortOrder;
			
			

			
			
			newEnt.Text = dto.Text;
			
			

			
			
			newEnt.UseVersioning = dto.UseVersioning;
			
			
			
			cache.Add(dto, newEnt);
			
			
			newEnt.MediaFolderPolicy = dto.MediaFolderPolicy.ToEntity(cache);
			
			
			
			newEnt.MediaFolder = dto.MediaFolder.ToEntity(cache);
			
			
			
			foreach (var related in dto.ProjectJobTaskTypeMediaFolders)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProjectJobTaskTypeMediaFolders.Contains(relatedEntity))
				{
					newEnt.ProjectJobTaskTypeMediaFolders.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.MediaFolders)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.MediaFolders.Contains(relatedEntity))
				{
					newEnt.MediaFolders.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.MediaFolderFiles)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.MediaFolderFiles.Contains(relatedEntity))
				{
					newEnt.MediaFolderFiles.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.MediaFolderRoles)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.MediaFolderRoles.Contains(relatedEntity))
				{
					newEnt.MediaFolderRoles.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.Translated)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.Translated.Contains(relatedEntity))
				{
					newEnt.Translated.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static MediaFolderDTO ToDTO(this MediaFolderEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (MediaFolderDTO)cache[ent];
			
			var newDTO = new MediaFolderDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.AutoCreated = ent.AutoCreated;
			

			newDTO.Hidden = ent.Hidden;
			

			newDTO.Id = ent.Id;
			

			newDTO.ParentId = ent.ParentId;
			

			newDTO.ShowViaChecklistId = ent.ShowViaChecklistId;
			

			newDTO.SortOrder = ent.SortOrder;
			

			newDTO.Text = ent.Text;
			

			newDTO.UseVersioning = ent.UseVersioning;
			
			
			
			cache.Add(ent, newDTO);
			
			
			newDTO.MediaFolderPolicy = ent.MediaFolderPolicy.ToDTO(cache);
			
			
			
			newDTO.MediaFolder = ent.MediaFolder.ToDTO(cache);
			
			
			
			foreach (var related in ent.ProjectJobTaskTypeMediaFolders)
			{
				newDTO.ProjectJobTaskTypeMediaFolders.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.MediaFolders)
			{
				newDTO.MediaFolders.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.MediaFolderFiles)
			{
				newDTO.MediaFolderFiles.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.MediaFolderRoles)
			{
				newDTO.MediaFolderRoles.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.Translated)
			{
				newDTO.Translated.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}