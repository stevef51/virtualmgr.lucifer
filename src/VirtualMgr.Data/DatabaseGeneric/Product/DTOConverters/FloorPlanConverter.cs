﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class FloorPlanConverter
	{
	
		public static FloorPlanEntity ToEntity(this FloorPlanDTO dto)
		{
			return dto.ToEntity(new FloorPlanEntity(), new Dictionary<object, object>());
		}

		public static FloorPlanEntity ToEntity(this FloorPlanDTO dto, FloorPlanEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static FloorPlanEntity ToEntity(this FloorPlanDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new FloorPlanEntity(), cache);
		}
	
		public static FloorPlanDTO ToDTO(this FloorPlanEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static FloorPlanEntity ToEntity(this FloorPlanDTO dto, FloorPlanEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (FloorPlanEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.BottomLeftLatitude = dto.BottomLeftLatitude;
			
			

			
			
			newEnt.BottomLeftLongitude = dto.BottomLeftLongitude;
			
			

			
			
			newEnt.Id = dto.Id;
			
			
			

			
			
			newEnt.ImageUrl = dto.ImageUrl;
			
			

			
			
			if (dto.IndoorAtlasFloorPlanId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)FloorPlanFieldIndex.IndoorAtlasFloorPlanId, "");
				
			}
			
			newEnt.IndoorAtlasFloorPlanId = dto.IndoorAtlasFloorPlanId;
			
			

			
			
			newEnt.TopLeftLatitude = dto.TopLeftLatitude;
			
			

			
			
			newEnt.TopLeftLongitude = dto.TopLeftLongitude;
			
			

			
			
			newEnt.TopRightLatitude = dto.TopRightLatitude;
			
			

			
			
			newEnt.TopRightLongitude = dto.TopRightLongitude;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			
			
			foreach (var related in dto.FacilityStructures)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.FacilityStructures.Contains(relatedEntity))
				{
					newEnt.FacilityStructures.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static FloorPlanDTO ToDTO(this FloorPlanEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (FloorPlanDTO)cache[ent];
			
			var newDTO = new FloorPlanDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.BottomLeftLatitude = ent.BottomLeftLatitude;
			

			newDTO.BottomLeftLongitude = ent.BottomLeftLongitude;
			

			newDTO.Id = ent.Id;
			

			newDTO.ImageUrl = ent.ImageUrl;
			

			newDTO.IndoorAtlasFloorPlanId = ent.IndoorAtlasFloorPlanId;
			

			newDTO.TopLeftLatitude = ent.TopLeftLatitude;
			

			newDTO.TopLeftLongitude = ent.TopLeftLongitude;
			

			newDTO.TopRightLatitude = ent.TopRightLatitude;
			

			newDTO.TopRightLongitude = ent.TopRightLongitude;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			
			
			foreach (var related in ent.FacilityStructures)
			{
				newDTO.FacilityStructures.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}