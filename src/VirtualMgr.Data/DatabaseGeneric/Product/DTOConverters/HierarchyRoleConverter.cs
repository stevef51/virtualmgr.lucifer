﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class HierarchyRoleConverter
	{
	
		public static HierarchyRoleEntity ToEntity(this HierarchyRoleDTO dto)
		{
			return dto.ToEntity(new HierarchyRoleEntity(), new Dictionary<object, object>());
		}

		public static HierarchyRoleEntity ToEntity(this HierarchyRoleDTO dto, HierarchyRoleEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static HierarchyRoleEntity ToEntity(this HierarchyRoleDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new HierarchyRoleEntity(), cache);
		}
	
		public static HierarchyRoleDTO ToDTO(this HierarchyRoleEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static HierarchyRoleEntity ToEntity(this HierarchyRoleDTO dto, HierarchyRoleEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (HierarchyRoleEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Id = dto.Id;
			
			
			

			
			
			newEnt.IsPerson = dto.IsPerson;
			
			

			
			
			newEnt.Name = dto.Name;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			
			
			foreach (var related in dto.AssetTypeCalendarTasks)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.AssetTypeCalendarTasks.Contains(relatedEntity))
				{
					newEnt.AssetTypeCalendarTasks.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ProjectJobScheduleDays)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProjectJobScheduleDays.Contains(relatedEntity))
				{
					newEnt.ProjectJobScheduleDays.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.OverridesScheduleDays)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.OverridesScheduleDays.Contains(relatedEntity))
				{
					newEnt.OverridesScheduleDays.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ProjectJobScheduledTasks)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProjectJobScheduledTasks.Contains(relatedEntity))
				{
					newEnt.ProjectJobScheduledTasks.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ProjectJobTasks)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProjectJobTasks.Contains(relatedEntity))
				{
					newEnt.ProjectJobTasks.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.TimesheetItems)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.TimesheetItems.Contains(relatedEntity))
				{
					newEnt.TimesheetItems.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.HierarchyBuckets)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.HierarchyBuckets.Contains(relatedEntity))
				{
					newEnt.HierarchyBuckets.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.HierarchyBucketOverrides)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.HierarchyBucketOverrides.Contains(relatedEntity))
				{
					newEnt.HierarchyBucketOverrides.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.HierarchyRoleUserTypes)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.HierarchyRoleUserTypes.Contains(relatedEntity))
				{
					newEnt.HierarchyRoleUserTypes.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static HierarchyRoleDTO ToDTO(this HierarchyRoleEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (HierarchyRoleDTO)cache[ent];
			
			var newDTO = new HierarchyRoleDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Id = ent.Id;
			

			newDTO.IsPerson = ent.IsPerson;
			

			newDTO.Name = ent.Name;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			
			
			foreach (var related in ent.AssetTypeCalendarTasks)
			{
				newDTO.AssetTypeCalendarTasks.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ProjectJobScheduleDays)
			{
				newDTO.ProjectJobScheduleDays.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.OverridesScheduleDays)
			{
				newDTO.OverridesScheduleDays.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ProjectJobScheduledTasks)
			{
				newDTO.ProjectJobScheduledTasks.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ProjectJobTasks)
			{
				newDTO.ProjectJobTasks.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.TimesheetItems)
			{
				newDTO.TimesheetItems.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.HierarchyBuckets)
			{
				newDTO.HierarchyBuckets.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.HierarchyBucketOverrides)
			{
				newDTO.HierarchyBucketOverrides.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.HierarchyRoleUserTypes)
			{
				newDTO.HierarchyRoleUserTypes.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}