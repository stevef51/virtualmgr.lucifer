﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class MediaPageViewingConverter
	{
	
		public static MediaPageViewingEntity ToEntity(this MediaPageViewingDTO dto)
		{
			return dto.ToEntity(new MediaPageViewingEntity(), new Dictionary<object, object>());
		}

		public static MediaPageViewingEntity ToEntity(this MediaPageViewingDTO dto, MediaPageViewingEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static MediaPageViewingEntity ToEntity(this MediaPageViewingDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new MediaPageViewingEntity(), cache);
		}
	
		public static MediaPageViewingDTO ToDTO(this MediaPageViewingEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static MediaPageViewingEntity ToEntity(this MediaPageViewingDTO dto, MediaPageViewingEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (MediaPageViewingEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.EndDate = dto.EndDate;
			
			

			
			
			newEnt.Id = dto.Id;
			
			

			
			
			newEnt.MediaViewingId = dto.MediaViewingId;
			
			

			
			
			newEnt.Page = dto.Page;
			
			

			
			
			newEnt.StartDate = dto.StartDate;
			
			

			
			
			newEnt.TotalSeconds = dto.TotalSeconds;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.MediaViewing = dto.MediaViewing.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static MediaPageViewingDTO ToDTO(this MediaPageViewingEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (MediaPageViewingDTO)cache[ent];
			
			var newDTO = new MediaPageViewingDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.EndDate = ent.EndDate;
			

			newDTO.Id = ent.Id;
			

			newDTO.MediaViewingId = ent.MediaViewingId;
			

			newDTO.Page = ent.Page;
			

			newDTO.StartDate = ent.StartDate;
			

			newDTO.TotalSeconds = ent.TotalSeconds;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.MediaViewing = ent.MediaViewing.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}