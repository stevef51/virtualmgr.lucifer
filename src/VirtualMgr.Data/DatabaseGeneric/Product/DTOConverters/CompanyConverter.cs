﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class CompanyConverter
	{
	
		public static CompanyEntity ToEntity(this CompanyDTO dto)
		{
			return dto.ToEntity(new CompanyEntity(), new Dictionary<object, object>());
		}

		public static CompanyEntity ToEntity(this CompanyDTO dto, CompanyEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static CompanyEntity ToEntity(this CompanyDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new CompanyEntity(), cache);
		}
	
		public static CompanyDTO ToDTO(this CompanyEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static CompanyEntity ToEntity(this CompanyDTO dto, CompanyEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (CompanyEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			if (dto.CountryStateId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)CompanyFieldIndex.CountryStateId, default(System.Int32));
				
			}
			
			newEnt.CountryStateId = dto.CountryStateId;
			
			

			
			
			newEnt.Id = dto.Id;
			
			

			
			
			if (dto.Latitude == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)CompanyFieldIndex.Latitude, default(System.Decimal));
				
			}
			
			newEnt.Latitude = dto.Latitude;
			
			

			
			
			if (dto.Longitude == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)CompanyFieldIndex.Longitude, default(System.Decimal));
				
			}
			
			newEnt.Longitude = dto.Longitude;
			
			

			
			
			if (dto.Name == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)CompanyFieldIndex.Name, "");
				
			}
			
			newEnt.Name = dto.Name;
			
			

			
			
			if (dto.Postcode == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)CompanyFieldIndex.Postcode, "");
				
			}
			
			newEnt.Postcode = dto.Postcode;
			
			

			
			
			if (dto.Rating == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)CompanyFieldIndex.Rating, default(System.Int32));
				
			}
			
			newEnt.Rating = dto.Rating;
			
			

			
			
			if (dto.Street == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)CompanyFieldIndex.Street, "");
				
			}
			
			newEnt.Street = dto.Street;
			
			

			
			
			if (dto.Town == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)CompanyFieldIndex.Town, "");
				
			}
			
			newEnt.Town = dto.Town;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.CountryState = dto.CountryState.ToEntity(cache);
			
			
			
			foreach (var related in dto.UserDatas)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.UserDatas.Contains(relatedEntity))
				{
					newEnt.UserDatas.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static CompanyDTO ToDTO(this CompanyEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (CompanyDTO)cache[ent];
			
			var newDTO = new CompanyDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.CountryStateId = ent.CountryStateId;
			

			newDTO.Id = ent.Id;
			

			newDTO.Latitude = ent.Latitude;
			

			newDTO.Longitude = ent.Longitude;
			

			newDTO.Name = ent.Name;
			

			newDTO.Postcode = ent.Postcode;
			

			newDTO.Rating = ent.Rating;
			

			newDTO.Street = ent.Street;
			

			newDTO.Town = ent.Town;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.CountryState = ent.CountryState.ToDTO(cache);
			
			
			
			foreach (var related in ent.UserDatas)
			{
				newDTO.UserDatas.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}