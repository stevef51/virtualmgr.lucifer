﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class CompletedPresentedFactValueConverter
	{
	
		public static CompletedPresentedFactValueEntity ToEntity(this CompletedPresentedFactValueDTO dto)
		{
			return dto.ToEntity(new CompletedPresentedFactValueEntity(), new Dictionary<object, object>());
		}

		public static CompletedPresentedFactValueEntity ToEntity(this CompletedPresentedFactValueDTO dto, CompletedPresentedFactValueEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static CompletedPresentedFactValueEntity ToEntity(this CompletedPresentedFactValueDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new CompletedPresentedFactValueEntity(), cache);
		}
	
		public static CompletedPresentedFactValueDTO ToDTO(this CompletedPresentedFactValueEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static CompletedPresentedFactValueEntity ToEntity(this CompletedPresentedFactValueDTO dto, CompletedPresentedFactValueEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (CompletedPresentedFactValueEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.CompletedWorkingDocumentPresentedFactId = dto.CompletedWorkingDocumentPresentedFactId;
			
			

			
			
			newEnt.Id = dto.Id;
			
			

			
			
			if (dto.SearchValue == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)CompletedPresentedFactValueFieldIndex.SearchValue, "");
				
			}
			
			newEnt.SearchValue = dto.SearchValue;
			
			

			
			
			if (dto.Value == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)CompletedPresentedFactValueFieldIndex.Value, "");
				
			}
			
			newEnt.Value = dto.Value;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.CompletedWorkingDocumentPresentedFact = dto.CompletedWorkingDocumentPresentedFact.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static CompletedPresentedFactValueDTO ToDTO(this CompletedPresentedFactValueEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (CompletedPresentedFactValueDTO)cache[ent];
			
			var newDTO = new CompletedPresentedFactValueDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.CompletedWorkingDocumentPresentedFactId = ent.CompletedWorkingDocumentPresentedFactId;
			

			newDTO.Id = ent.Id;
			

			newDTO.SearchValue = ent.SearchValue;
			

			newDTO.Value = ent.Value;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.CompletedWorkingDocumentPresentedFact = ent.CompletedWorkingDocumentPresentedFact.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}