﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class LanguageConverter
	{
	
		public static LanguageEntity ToEntity(this LanguageDTO dto)
		{
			return dto.ToEntity(new LanguageEntity(), new Dictionary<object, object>());
		}

		public static LanguageEntity ToEntity(this LanguageDTO dto, LanguageEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static LanguageEntity ToEntity(this LanguageDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new LanguageEntity(), cache);
		}
	
		public static LanguageDTO ToDTO(this LanguageEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static LanguageEntity ToEntity(this LanguageDTO dto, LanguageEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (LanguageEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.CultureName = dto.CultureName;
			
			

			
			
			newEnt.LanguageName = dto.LanguageName;
			
			

			
			
			newEnt.NativeName = dto.NativeName;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static LanguageDTO ToDTO(this LanguageEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (LanguageDTO)cache[ent];
			
			var newDTO = new LanguageDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.CultureName = ent.CultureName;
			

			newDTO.LanguageName = ent.LanguageName;
			

			newDTO.NativeName = ent.NativeName;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}