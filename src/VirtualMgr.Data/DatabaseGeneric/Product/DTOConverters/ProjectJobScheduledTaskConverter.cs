﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class ProjectJobScheduledTaskConverter
	{
	
		public static ProjectJobScheduledTaskEntity ToEntity(this ProjectJobScheduledTaskDTO dto)
		{
			return dto.ToEntity(new ProjectJobScheduledTaskEntity(), new Dictionary<object, object>());
		}

		public static ProjectJobScheduledTaskEntity ToEntity(this ProjectJobScheduledTaskDTO dto, ProjectJobScheduledTaskEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static ProjectJobScheduledTaskEntity ToEntity(this ProjectJobScheduledTaskDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new ProjectJobScheduledTaskEntity(), cache);
		}
	
		public static ProjectJobScheduledTaskDTO ToDTO(this ProjectJobScheduledTaskEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static ProjectJobScheduledTaskEntity ToEntity(this ProjectJobScheduledTaskDTO dto, ProjectJobScheduledTaskEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (ProjectJobScheduledTaskEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			if (dto.Backcolor == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobScheduledTaskFieldIndex.Backcolor, default(System.Int32));
				
			}
			
			newEnt.Backcolor = dto.Backcolor;
			
			

			
			
			newEnt.CalendarRuleId = dto.CalendarRuleId;
			
			

			
			
			if (dto.Description == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobScheduledTaskFieldIndex.Description, "");
				
			}
			
			newEnt.Description = dto.Description;
			
			

			
			
			if (dto.EstimatedDurationSeconds == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobScheduledTaskFieldIndex.EstimatedDurationSeconds, default(System.Decimal));
				
			}
			
			newEnt.EstimatedDurationSeconds = dto.EstimatedDurationSeconds;
			
			

			
			
			if (dto.Forecolor == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobScheduledTaskFieldIndex.Forecolor, default(System.Int32));
				
			}
			
			newEnt.Forecolor = dto.Forecolor;
			
			

			
			
			newEnt.Id = dto.Id;
			
			

			
			
			newEnt.IsArchived = dto.IsArchived;
			
			

			
			
			newEnt.LengthInDays = dto.LengthInDays;
			
			

			
			
			newEnt.Level = dto.Level;
			
			

			
			
			if (dto.MinimumDuration == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobScheduledTaskFieldIndex.MinimumDuration, "");
				
			}
			
			newEnt.MinimumDuration = dto.MinimumDuration;
			
			

			
			
			newEnt.Name = dto.Name;
			
			

			
			
			newEnt.PhotoDuringSignature = dto.PhotoDuringSignature;
			
			

			
			
			newEnt.RequireSignature = dto.RequireSignature;
			
			

			
			
			if (dto.RoleId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobScheduledTaskFieldIndex.RoleId, default(System.Int32));
				
			}
			
			newEnt.RoleId = dto.RoleId;
			
			

			
			
			newEnt.RosterId = dto.RosterId;
			
			

			
			
			if (dto.SiteId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobScheduledTaskFieldIndex.SiteId, default(System.Guid));
				
			}
			
			newEnt.SiteId = dto.SiteId;
			
			

			
			
			newEnt.SortOrder = dto.SortOrder;
			
			

			
			
			if (dto.StartTime == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobScheduledTaskFieldIndex.StartTime, default(System.TimeSpan));
				
			}
			
			newEnt.StartTime = dto.StartTime;
			
			

			
			
			if (dto.StartTimeWindowSeconds == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobScheduledTaskFieldIndex.StartTimeWindowSeconds, default(System.Int32));
				
			}
			
			newEnt.StartTimeWindowSeconds = dto.StartTimeWindowSeconds;
			
			

			
			
			newEnt.TaskTypeId = dto.TaskTypeId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.CalendarRule = dto.CalendarRule.ToEntity(cache);
			
			newEnt.ProjectJobTaskType = dto.ProjectJobTaskType.ToEntity(cache);
			
			newEnt.Roster = dto.Roster.ToEntity(cache);
			
			newEnt.HierarchyRole = dto.HierarchyRole.ToEntity(cache);
			
			newEnt.UserData_ = dto.UserData_.ToEntity(cache);
			
			
			
			foreach (var related in dto.AuditTaskConditionActions)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.AuditTaskConditionActions.Contains(relatedEntity))
				{
					newEnt.AuditTaskConditionActions.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ProjectJobScheduledJobTaskUserLabels)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProjectJobScheduledJobTaskUserLabels.Contains(relatedEntity))
				{
					newEnt.ProjectJobScheduledJobTaskUserLabels.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ProjectJobScheduledTaskAttachments)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProjectJobScheduledTaskAttachments.Contains(relatedEntity))
				{
					newEnt.ProjectJobScheduledTaskAttachments.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ProjectJobScheduledTaskLabels)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProjectJobScheduledTaskLabels.Contains(relatedEntity))
				{
					newEnt.ProjectJobScheduledTaskLabels.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ProjectJobScheduledTaskPublishingGroupResources)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProjectJobScheduledTaskPublishingGroupResources.Contains(relatedEntity))
				{
					newEnt.ProjectJobScheduledTaskPublishingGroupResources.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.Translated)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.Translated.Contains(relatedEntity))
				{
					newEnt.Translated.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ProjectJobTasks)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProjectJobTasks.Contains(relatedEntity))
				{
					newEnt.ProjectJobTasks.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ProjectJobScheduledTaskWorkloadingActivities)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProjectJobScheduledTaskWorkloadingActivities.Contains(relatedEntity))
				{
					newEnt.ProjectJobScheduledTaskWorkloadingActivities.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static ProjectJobScheduledTaskDTO ToDTO(this ProjectJobScheduledTaskEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (ProjectJobScheduledTaskDTO)cache[ent];
			
			var newDTO = new ProjectJobScheduledTaskDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Backcolor = ent.Backcolor;
			

			newDTO.CalendarRuleId = ent.CalendarRuleId;
			

			newDTO.Description = ent.Description;
			

			newDTO.EstimatedDurationSeconds = ent.EstimatedDurationSeconds;
			

			newDTO.Forecolor = ent.Forecolor;
			

			newDTO.Id = ent.Id;
			

			newDTO.IsArchived = ent.IsArchived;
			

			newDTO.LengthInDays = ent.LengthInDays;
			

			newDTO.Level = ent.Level;
			

			newDTO.MinimumDuration = ent.MinimumDuration;
			

			newDTO.Name = ent.Name;
			

			newDTO.PhotoDuringSignature = ent.PhotoDuringSignature;
			

			newDTO.RequireSignature = ent.RequireSignature;
			

			newDTO.RoleId = ent.RoleId;
			

			newDTO.RosterId = ent.RosterId;
			

			newDTO.SiteId = ent.SiteId;
			

			newDTO.SortOrder = ent.SortOrder;
			

			newDTO.StartTime = ent.StartTime;
			

			newDTO.StartTimeWindowSeconds = ent.StartTimeWindowSeconds;
			

			newDTO.TaskTypeId = ent.TaskTypeId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.CalendarRule = ent.CalendarRule.ToDTO(cache);
			
			newDTO.ProjectJobTaskType = ent.ProjectJobTaskType.ToDTO(cache);
			
			newDTO.Roster = ent.Roster.ToDTO(cache);
			
			newDTO.HierarchyRole = ent.HierarchyRole.ToDTO(cache);
			
			newDTO.UserData_ = ent.UserData_.ToDTO(cache);
			
			
			
			foreach (var related in ent.AuditTaskConditionActions)
			{
				newDTO.AuditTaskConditionActions.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ProjectJobScheduledJobTaskUserLabels)
			{
				newDTO.ProjectJobScheduledJobTaskUserLabels.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ProjectJobScheduledTaskAttachments)
			{
				newDTO.ProjectJobScheduledTaskAttachments.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ProjectJobScheduledTaskLabels)
			{
				newDTO.ProjectJobScheduledTaskLabels.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ProjectJobScheduledTaskPublishingGroupResources)
			{
				newDTO.ProjectJobScheduledTaskPublishingGroupResources.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.Translated)
			{
				newDTO.Translated.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ProjectJobTasks)
			{
				newDTO.ProjectJobTasks.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ProjectJobScheduledTaskWorkloadingActivities)
			{
				newDTO.ProjectJobScheduledTaskWorkloadingActivities.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}