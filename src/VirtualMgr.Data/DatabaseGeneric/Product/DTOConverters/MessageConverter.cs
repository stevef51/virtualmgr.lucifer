﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class MessageConverter
	{
	
		public static MessageEntity ToEntity(this MessageDTO dto)
		{
			return dto.ToEntity(new MessageEntity(), new Dictionary<object, object>());
		}

		public static MessageEntity ToEntity(this MessageDTO dto, MessageEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static MessageEntity ToEntity(this MessageDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new MessageEntity(), cache);
		}
	
		public static MessageDTO ToDTO(this MessageEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static MessageEntity ToEntity(this MessageDTO dto, MessageEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (MessageEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Id = dto.Id;
			
			

			
			
			if (dto.InReplyToId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)MessageFieldIndex.InReplyToId, default(System.Guid));
				
			}
			
			newEnt.InReplyToId = dto.InReplyToId;
			
			

			
			
			newEnt.RequiresAck = dto.RequiresAck;
			
			

			
			
			newEnt.SenderId = dto.SenderId;
			
			

			
			
			newEnt.SentAt = dto.SentAt;
			
			

			
			
			newEnt.Text = dto.Text;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.InReplyTo = dto.InReplyTo.ToEntity(cache);
			
			newEnt.Sender = dto.Sender.ToEntity(cache);
			
			
			
			foreach (var related in dto.Mailboxes)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.Mailboxes.Contains(relatedEntity))
				{
					newEnt.Mailboxes.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.Replies)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.Replies.Contains(relatedEntity))
				{
					newEnt.Replies.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static MessageDTO ToDTO(this MessageEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (MessageDTO)cache[ent];
			
			var newDTO = new MessageDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Id = ent.Id;
			

			newDTO.InReplyToId = ent.InReplyToId;
			

			newDTO.RequiresAck = ent.RequiresAck;
			

			newDTO.SenderId = ent.SenderId;
			

			newDTO.SentAt = ent.SentAt;
			

			newDTO.Text = ent.Text;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.InReplyTo = ent.InReplyTo.ToDTO(cache);
			
			newDTO.Sender = ent.Sender.ToDTO(cache);
			
			
			
			foreach (var related in ent.Mailboxes)
			{
				newDTO.Mailboxes.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.Replies)
			{
				newDTO.Replies.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}