﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class AssetTypeConverter
	{
	
		public static AssetTypeEntity ToEntity(this AssetTypeDTO dto)
		{
			return dto.ToEntity(new AssetTypeEntity(), new Dictionary<object, object>());
		}

		public static AssetTypeEntity ToEntity(this AssetTypeDTO dto, AssetTypeEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static AssetTypeEntity ToEntity(this AssetTypeDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new AssetTypeEntity(), cache);
		}
	
		public static AssetTypeDTO ToDTO(this AssetTypeEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static AssetTypeEntity ToEntity(this AssetTypeDTO dto, AssetTypeEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (AssetTypeEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Archived = dto.Archived;
			
			

			
			
			newEnt.Backcolor = dto.Backcolor;
			
			

			
			
			newEnt.CanTrack = dto.CanTrack;
			
			

			
			
			newEnt.Forecolor = dto.Forecolor;
			
			

			
			
			newEnt.Id = dto.Id;
			
			
			

			
			
			newEnt.Name = dto.Name;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			
			
			foreach (var related in dto.Assets)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.Assets.Contains(relatedEntity))
				{
					newEnt.Assets.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.AssetTypeCalendars)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.AssetTypeCalendars.Contains(relatedEntity))
				{
					newEnt.AssetTypeCalendars.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.AssetTypeContextPublishedResources)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.AssetTypeContextPublishedResources.Contains(relatedEntity))
				{
					newEnt.AssetTypeContextPublishedResources.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static AssetTypeDTO ToDTO(this AssetTypeEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (AssetTypeDTO)cache[ent];
			
			var newDTO = new AssetTypeDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Archived = ent.Archived;
			

			newDTO.Backcolor = ent.Backcolor;
			

			newDTO.CanTrack = ent.CanTrack;
			

			newDTO.Forecolor = ent.Forecolor;
			

			newDTO.Id = ent.Id;
			

			newDTO.Name = ent.Name;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			
			
			foreach (var related in ent.Assets)
			{
				newDTO.Assets.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.AssetTypeCalendars)
			{
				newDTO.AssetTypeCalendars.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.AssetTypeContextPublishedResources)
			{
				newDTO.AssetTypeContextPublishedResources.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}