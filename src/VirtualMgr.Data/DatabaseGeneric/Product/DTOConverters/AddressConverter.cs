﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class AddressConverter
	{
	
		public static AddressEntity ToEntity(this AddressDTO dto)
		{
			return dto.ToEntity(new AddressEntity(), new Dictionary<object, object>());
		}

		public static AddressEntity ToEntity(this AddressDTO dto, AddressEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static AddressEntity ToEntity(this AddressDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new AddressEntity(), cache);
		}
	
		public static AddressDTO ToDTO(this AddressEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static AddressEntity ToEntity(this AddressDTO dto, AddressEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (AddressEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.CountryCode = dto.CountryCode;
			
			

			
			
			newEnt.Id = dto.Id;
			
			

			
			
			newEnt.Line1 = dto.Line1;
			
			

			
			
			if (dto.Line2 == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)AddressFieldIndex.Line2, "");
				
			}
			
			newEnt.Line2 = dto.Line2;
			
			

			
			
			if (dto.Line3 == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)AddressFieldIndex.Line3, "");
				
			}
			
			newEnt.Line3 = dto.Line3;
			
			

			
			
			if (dto.Line4 == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)AddressFieldIndex.Line4, "");
				
			}
			
			newEnt.Line4 = dto.Line4;
			
			

			
			
			newEnt.Locality = dto.Locality;
			
			

			
			
			newEnt.Region = dto.Region;
			
			

			
			
			newEnt.ZipCode = dto.ZipCode;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			
			
			foreach (var related in dto.FacilityStructures)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.FacilityStructures.Contains(relatedEntity))
				{
					newEnt.FacilityStructures.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static AddressDTO ToDTO(this AddressEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (AddressDTO)cache[ent];
			
			var newDTO = new AddressDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.CountryCode = ent.CountryCode;
			

			newDTO.Id = ent.Id;
			

			newDTO.Line1 = ent.Line1;
			

			newDTO.Line2 = ent.Line2;
			

			newDTO.Line3 = ent.Line3;
			

			newDTO.Line4 = ent.Line4;
			

			newDTO.Locality = ent.Locality;
			

			newDTO.Region = ent.Region;
			

			newDTO.ZipCode = ent.ZipCode;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			
			
			foreach (var related in ent.FacilityStructures)
			{
				newDTO.FacilityStructures.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}