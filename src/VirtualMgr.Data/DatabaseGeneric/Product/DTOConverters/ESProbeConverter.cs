﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class ESProbeConverter
	{
	
		public static ESProbeEntity ToEntity(this ESProbeDTO dto)
		{
			return dto.ToEntity(new ESProbeEntity(), new Dictionary<object, object>());
		}

		public static ESProbeEntity ToEntity(this ESProbeDTO dto, ESProbeEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static ESProbeEntity ToEntity(this ESProbeDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new ESProbeEntity(), cache);
		}
	
		public static ESProbeDTO ToDTO(this ESProbeEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static ESProbeEntity ToEntity(this ESProbeDTO dto, ESProbeEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (ESProbeEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Archived = dto.Archived;
			
			

			
			
			if (dto.AssetId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ESProbeFieldIndex.AssetId, default(System.Int32));
				
			}
			
			newEnt.AssetId = dto.AssetId;
			
			

			
			
			newEnt.Id = dto.Id;
			
			

			
			
			newEnt.Key = dto.Key;
			
			

			
			
			newEnt.Model = dto.Model;
			
			

			
			
			newEnt.Name = dto.Name;
			
			

			
			
			newEnt.SerialNumber = dto.SerialNumber;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.Asset = dto.Asset.ToEntity(cache);
			
			
			
			foreach (var related in dto.Sensors)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.Sensors.Contains(relatedEntity))
				{
					newEnt.Sensors.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static ESProbeDTO ToDTO(this ESProbeEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (ESProbeDTO)cache[ent];
			
			var newDTO = new ESProbeDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Archived = ent.Archived;
			

			newDTO.AssetId = ent.AssetId;
			

			newDTO.Id = ent.Id;
			

			newDTO.Key = ent.Key;
			

			newDTO.Model = ent.Model;
			

			newDTO.Name = ent.Name;
			

			newDTO.SerialNumber = ent.SerialNumber;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.Asset = ent.Asset.ToDTO(cache);
			
			
			
			foreach (var related in ent.Sensors)
			{
				newDTO.Sensors.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}