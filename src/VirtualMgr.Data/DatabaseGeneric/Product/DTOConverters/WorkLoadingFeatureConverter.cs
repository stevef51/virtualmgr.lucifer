﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class WorkLoadingFeatureConverter
	{
	
		public static WorkLoadingFeatureEntity ToEntity(this WorkLoadingFeatureDTO dto)
		{
			return dto.ToEntity(new WorkLoadingFeatureEntity(), new Dictionary<object, object>());
		}

		public static WorkLoadingFeatureEntity ToEntity(this WorkLoadingFeatureDTO dto, WorkLoadingFeatureEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static WorkLoadingFeatureEntity ToEntity(this WorkLoadingFeatureDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new WorkLoadingFeatureEntity(), cache);
		}
	
		public static WorkLoadingFeatureDTO ToDTO(this WorkLoadingFeatureEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static WorkLoadingFeatureEntity ToEntity(this WorkLoadingFeatureDTO dto, WorkLoadingFeatureEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (WorkLoadingFeatureEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Archived = dto.Archived;
			
			

			
			
			newEnt.Dimensions = dto.Dimensions;
			
			

			
			
			newEnt.Id = dto.Id;
			
			

			
			
			newEnt.Name = dto.Name;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			
			
			foreach (var related in dto.SiteFeatures)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.SiteFeatures.Contains(relatedEntity))
				{
					newEnt.SiteFeatures.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.WorkLoadingStandards)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.WorkLoadingStandards.Contains(relatedEntity))
				{
					newEnt.WorkLoadingStandards.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static WorkLoadingFeatureDTO ToDTO(this WorkLoadingFeatureEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (WorkLoadingFeatureDTO)cache[ent];
			
			var newDTO = new WorkLoadingFeatureDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Archived = ent.Archived;
			

			newDTO.Dimensions = ent.Dimensions;
			

			newDTO.Id = ent.Id;
			

			newDTO.Name = ent.Name;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			
			
			foreach (var related in ent.SiteFeatures)
			{
				newDTO.SiteFeatures.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.WorkLoadingStandards)
			{
				newDTO.WorkLoadingStandards.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}