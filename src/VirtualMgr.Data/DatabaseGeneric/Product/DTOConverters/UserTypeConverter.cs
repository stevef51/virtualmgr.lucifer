﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class UserTypeConverter
	{
	
		public static UserTypeEntity ToEntity(this UserTypeDTO dto)
		{
			return dto.ToEntity(new UserTypeEntity(), new Dictionary<object, object>());
		}

		public static UserTypeEntity ToEntity(this UserTypeDTO dto, UserTypeEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static UserTypeEntity ToEntity(this UserTypeDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new UserTypeEntity(), cache);
		}
	
		public static UserTypeDTO ToDTO(this UserTypeEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static UserTypeEntity ToEntity(this UserTypeDTO dto, UserTypeEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (UserTypeEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.CanLogin = dto.CanLogin;
			
			

			
			
			if (dto.DefaultDashboard == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)UserTypeFieldIndex.DefaultDashboard, "");
				
			}
			
			newEnt.DefaultDashboard = dto.DefaultDashboard;
			
			

			
			
			newEnt.HasGeoCordinates = dto.HasGeoCordinates;
			
			

			
			
			newEnt.Id = dto.Id;
			
			

			
			
			newEnt.IsAsite = dto.IsAsite;
			
			

			
			
			newEnt.Name = dto.Name;
			
			

			
			
			if (dto.UserBillingTypeId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)UserTypeFieldIndex.UserBillingTypeId, default(System.Int32));
				
			}
			
			newEnt.UserBillingTypeId = dto.UserBillingTypeId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.UserBillingType = dto.UserBillingType.ToEntity(cache);
			
			
			
			foreach (var related in dto.UserTypeTaskTypeRelationships)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.UserTypeTaskTypeRelationships.Contains(relatedEntity))
				{
					newEnt.UserTypeTaskTypeRelationships.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.HierarchyRoleUserTypes)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.HierarchyRoleUserTypes.Contains(relatedEntity))
				{
					newEnt.HierarchyRoleUserTypes.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.UserDatas)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.UserDatas.Contains(relatedEntity))
				{
					newEnt.UserDatas.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.UserTypeContextPublishedResources)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.UserTypeContextPublishedResources.Contains(relatedEntity))
				{
					newEnt.UserTypeContextPublishedResources.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.UserTypeDashboards)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.UserTypeDashboards.Contains(relatedEntity))
				{
					newEnt.UserTypeDashboards.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.UserTypeDefaultLabels)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.UserTypeDefaultLabels.Contains(relatedEntity))
				{
					newEnt.UserTypeDefaultLabels.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.UserTypeDefaultRoles)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.UserTypeDefaultRoles.Contains(relatedEntity))
				{
					newEnt.UserTypeDefaultRoles.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static UserTypeDTO ToDTO(this UserTypeEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (UserTypeDTO)cache[ent];
			
			var newDTO = new UserTypeDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.CanLogin = ent.CanLogin;
			

			newDTO.DefaultDashboard = ent.DefaultDashboard;
			

			newDTO.HasGeoCordinates = ent.HasGeoCordinates;
			

			newDTO.Id = ent.Id;
			

			newDTO.IsAsite = ent.IsAsite;
			

			newDTO.Name = ent.Name;
			

			newDTO.UserBillingTypeId = ent.UserBillingTypeId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.UserBillingType = ent.UserBillingType.ToDTO(cache);
			
			
			
			foreach (var related in ent.UserTypeTaskTypeRelationships)
			{
				newDTO.UserTypeTaskTypeRelationships.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.HierarchyRoleUserTypes)
			{
				newDTO.HierarchyRoleUserTypes.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.UserDatas)
			{
				newDTO.UserDatas.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.UserTypeContextPublishedResources)
			{
				newDTO.UserTypeContextPublishedResources.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.UserTypeDashboards)
			{
				newDTO.UserTypeDashboards.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.UserTypeDefaultLabels)
			{
				newDTO.UserTypeDefaultLabels.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.UserTypeDefaultRoles)
			{
				newDTO.UserTypeDefaultRoles.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}