﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class ProjectJobTaskRelationshipConverter
	{
	
		public static ProjectJobTaskRelationshipEntity ToEntity(this ProjectJobTaskRelationshipDTO dto)
		{
			return dto.ToEntity(new ProjectJobTaskRelationshipEntity(), new Dictionary<object, object>());
		}

		public static ProjectJobTaskRelationshipEntity ToEntity(this ProjectJobTaskRelationshipDTO dto, ProjectJobTaskRelationshipEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static ProjectJobTaskRelationshipEntity ToEntity(this ProjectJobTaskRelationshipDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new ProjectJobTaskRelationshipEntity(), cache);
		}
	
		public static ProjectJobTaskRelationshipDTO ToDTO(this ProjectJobTaskRelationshipEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static ProjectJobTaskRelationshipEntity ToEntity(this ProjectJobTaskRelationshipDTO dto, ProjectJobTaskRelationshipEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (ProjectJobTaskRelationshipEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.DestinationProjectJobTaskId = dto.DestinationProjectJobTaskId;
			
			

			
			
			newEnt.ProjectJobTaskRelationshipTypeId = dto.ProjectJobTaskRelationshipTypeId;
			
			

			
			
			newEnt.SourceProjectJobTaskId = dto.SourceProjectJobTaskId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.ProjectJobTask = dto.ProjectJobTask.ToEntity(cache);
			
			newEnt.ProjectJobTask_ = dto.ProjectJobTask_.ToEntity(cache);
			
			newEnt.ProjectJobTaskRelationshipType = dto.ProjectJobTaskRelationshipType.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static ProjectJobTaskRelationshipDTO ToDTO(this ProjectJobTaskRelationshipEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (ProjectJobTaskRelationshipDTO)cache[ent];
			
			var newDTO = new ProjectJobTaskRelationshipDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.DestinationProjectJobTaskId = ent.DestinationProjectJobTaskId;
			

			newDTO.ProjectJobTaskRelationshipTypeId = ent.ProjectJobTaskRelationshipTypeId;
			

			newDTO.SourceProjectJobTaskId = ent.SourceProjectJobTaskId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.ProjectJobTask = ent.ProjectJobTask.ToDTO(cache);
			
			newDTO.ProjectJobTask_ = ent.ProjectJobTask_.ToDTO(cache);
			
			newDTO.ProjectJobTaskRelationshipType = ent.ProjectJobTaskRelationshipType.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}