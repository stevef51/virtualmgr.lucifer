﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class ReportDataConverter
	{
	
		public static ReportDataEntity ToEntity(this ReportDataDTO dto)
		{
			return dto.ToEntity(new ReportDataEntity(), new Dictionary<object, object>());
		}

		public static ReportDataEntity ToEntity(this ReportDataDTO dto, ReportDataEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static ReportDataEntity ToEntity(this ReportDataDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new ReportDataEntity(), cache);
		}
	
		public static ReportDataDTO ToDTO(this ReportDataEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static ReportDataEntity ToEntity(this ReportDataDTO dto, ReportDataEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (ReportDataEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Data = dto.Data;
			
			

			
			
			newEnt.ReportId = dto.ReportId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			newEnt.Report = dto.Report.ToEntity(cache);
			
			
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static ReportDataDTO ToDTO(this ReportDataEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (ReportDataDTO)cache[ent];
			
			var newDTO = new ReportDataDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Data = ent.Data;
			

			newDTO.ReportId = ent.ReportId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			newDTO.Report = ent.Report.ToDTO(cache);
			
			
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}