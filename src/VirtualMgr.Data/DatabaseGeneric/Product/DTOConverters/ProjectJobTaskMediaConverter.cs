﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class ProjectJobTaskMediaConverter
	{
	
		public static ProjectJobTaskMediaEntity ToEntity(this ProjectJobTaskMediaDTO dto)
		{
			return dto.ToEntity(new ProjectJobTaskMediaEntity(), new Dictionary<object, object>());
		}

		public static ProjectJobTaskMediaEntity ToEntity(this ProjectJobTaskMediaDTO dto, ProjectJobTaskMediaEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static ProjectJobTaskMediaEntity ToEntity(this ProjectJobTaskMediaDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new ProjectJobTaskMediaEntity(), cache);
		}
	
		public static ProjectJobTaskMediaDTO ToDTO(this ProjectJobTaskMediaEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static ProjectJobTaskMediaEntity ToEntity(this ProjectJobTaskMediaDTO dto, ProjectJobTaskMediaEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (ProjectJobTaskMediaEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.MediaId = dto.MediaId;
			
			

			
			
			if (dto.Notes == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobTaskMediaFieldIndex.Notes, "");
				
			}
			
			newEnt.Notes = dto.Notes;
			
			

			
			
			newEnt.TaskId = dto.TaskId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.ProjectJobTask = dto.ProjectJobTask.ToEntity(cache);
			
			newEnt.Medium = dto.Medium.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static ProjectJobTaskMediaDTO ToDTO(this ProjectJobTaskMediaEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (ProjectJobTaskMediaDTO)cache[ent];
			
			var newDTO = new ProjectJobTaskMediaDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.MediaId = ent.MediaId;
			

			newDTO.Notes = ent.Notes;
			

			newDTO.TaskId = ent.TaskId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.ProjectJobTask = ent.ProjectJobTask.ToDTO(cache);
			
			newDTO.Medium = ent.Medium.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}