﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class UserTypeContextPublishedResourceConverter
	{
	
		public static UserTypeContextPublishedResourceEntity ToEntity(this UserTypeContextPublishedResourceDTO dto)
		{
			return dto.ToEntity(new UserTypeContextPublishedResourceEntity(), new Dictionary<object, object>());
		}

		public static UserTypeContextPublishedResourceEntity ToEntity(this UserTypeContextPublishedResourceDTO dto, UserTypeContextPublishedResourceEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static UserTypeContextPublishedResourceEntity ToEntity(this UserTypeContextPublishedResourceDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new UserTypeContextPublishedResourceEntity(), cache);
		}
	
		public static UserTypeContextPublishedResourceDTO ToDTO(this UserTypeContextPublishedResourceEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static UserTypeContextPublishedResourceEntity ToEntity(this UserTypeContextPublishedResourceDTO dto, UserTypeContextPublishedResourceEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (UserTypeContextPublishedResourceEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Id = dto.Id;
			
			
			

			
			
			newEnt.PublishingGroupResourceId = dto.PublishingGroupResourceId;
			
			

			
			
			newEnt.UserTypeId = dto.UserTypeId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.PublishingGroupResource = dto.PublishingGroupResource.ToEntity(cache);
			
			newEnt.UserType = dto.UserType.ToEntity(cache);
			
			
			
			foreach (var related in dto.UserContextDatas)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.UserContextDatas.Contains(relatedEntity))
				{
					newEnt.UserContextDatas.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static UserTypeContextPublishedResourceDTO ToDTO(this UserTypeContextPublishedResourceEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (UserTypeContextPublishedResourceDTO)cache[ent];
			
			var newDTO = new UserTypeContextPublishedResourceDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Id = ent.Id;
			

			newDTO.PublishingGroupResourceId = ent.PublishingGroupResourceId;
			

			newDTO.UserTypeId = ent.UserTypeId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.PublishingGroupResource = ent.PublishingGroupResource.ToDTO(cache);
			
			newDTO.UserType = ent.UserType.ToDTO(cache);
			
			
			
			foreach (var related in ent.UserContextDatas)
			{
				newDTO.UserContextDatas.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}