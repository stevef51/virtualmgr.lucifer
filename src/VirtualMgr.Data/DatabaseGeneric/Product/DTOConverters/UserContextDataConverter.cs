﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class UserContextDataConverter
	{
	
		public static UserContextDataEntity ToEntity(this UserContextDataDTO dto)
		{
			return dto.ToEntity(new UserContextDataEntity(), new Dictionary<object, object>());
		}

		public static UserContextDataEntity ToEntity(this UserContextDataDTO dto, UserContextDataEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static UserContextDataEntity ToEntity(this UserContextDataDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new UserContextDataEntity(), cache);
		}
	
		public static UserContextDataDTO ToDTO(this UserContextDataEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static UserContextDataEntity ToEntity(this UserContextDataDTO dto, UserContextDataEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (UserContextDataEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.CompletedWorkingDocumentId = dto.CompletedWorkingDocumentId;
			
			

			
			
			newEnt.UserId = dto.UserId;
			
			

			
			
			newEnt.UserTypeContextPublishedResourceId = dto.UserTypeContextPublishedResourceId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.CompletedWorkingDocumentFact = dto.CompletedWorkingDocumentFact.ToEntity(cache);
			
			newEnt.UserData = dto.UserData.ToEntity(cache);
			
			newEnt.UserTypeContextPublishedResource = dto.UserTypeContextPublishedResource.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static UserContextDataDTO ToDTO(this UserContextDataEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (UserContextDataDTO)cache[ent];
			
			var newDTO = new UserContextDataDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.CompletedWorkingDocumentId = ent.CompletedWorkingDocumentId;
			

			newDTO.UserId = ent.UserId;
			

			newDTO.UserTypeContextPublishedResourceId = ent.UserTypeContextPublishedResourceId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.CompletedWorkingDocumentFact = ent.CompletedWorkingDocumentFact.ToDTO(cache);
			
			newDTO.UserData = ent.UserData.ToDTO(cache);
			
			newDTO.UserTypeContextPublishedResource = ent.UserTypeContextPublishedResource.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}