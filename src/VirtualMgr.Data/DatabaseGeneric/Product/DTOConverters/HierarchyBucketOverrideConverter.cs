﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class HierarchyBucketOverrideConverter
	{
	
		public static HierarchyBucketOverrideEntity ToEntity(this HierarchyBucketOverrideDTO dto)
		{
			return dto.ToEntity(new HierarchyBucketOverrideEntity(), new Dictionary<object, object>());
		}

		public static HierarchyBucketOverrideEntity ToEntity(this HierarchyBucketOverrideDTO dto, HierarchyBucketOverrideEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static HierarchyBucketOverrideEntity ToEntity(this HierarchyBucketOverrideDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new HierarchyBucketOverrideEntity(), cache);
		}
	
		public static HierarchyBucketOverrideDTO ToDTO(this HierarchyBucketOverrideEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static HierarchyBucketOverrideEntity ToEntity(this HierarchyBucketOverrideDTO dto, HierarchyBucketOverrideEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (HierarchyBucketOverrideEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.DateCreated = dto.DateCreated;
			
			

			
			
			newEnt.EndDate = dto.EndDate;
			
			

			
			
			newEnt.HierarchyBucketId = dto.HierarchyBucketId;
			
			

			
			
			if (dto.HierarchyBucketOverrideTypeId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)HierarchyBucketOverrideFieldIndex.HierarchyBucketOverrideTypeId, default(System.Int32));
				
			}
			
			newEnt.HierarchyBucketOverrideTypeId = dto.HierarchyBucketOverrideTypeId;
			
			

			
			
			newEnt.Id = dto.Id;
			
			

			
			
			if (dto.Notes == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)HierarchyBucketOverrideFieldIndex.Notes, "");
				
			}
			
			newEnt.Notes = dto.Notes;
			
			

			
			
			if (dto.OriginalUserId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)HierarchyBucketOverrideFieldIndex.OriginalUserId, default(System.Guid));
				
			}
			
			newEnt.OriginalUserId = dto.OriginalUserId;
			
			

			
			
			newEnt.RoleId = dto.RoleId;
			
			

			
			
			newEnt.StartDate = dto.StartDate;
			
			

			
			
			if (dto.UserId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)HierarchyBucketOverrideFieldIndex.UserId, default(System.Guid));
				
			}
			
			newEnt.UserId = dto.UserId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.HierarchyBucket = dto.HierarchyBucket.ToEntity(cache);
			
			newEnt.HierarchyBucketOverrideType = dto.HierarchyBucketOverrideType.ToEntity(cache);
			
			newEnt.HierarchyRole = dto.HierarchyRole.ToEntity(cache);
			
			newEnt.OriginalUser = dto.OriginalUser.ToEntity(cache);
			
			newEnt.UserData = dto.UserData.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static HierarchyBucketOverrideDTO ToDTO(this HierarchyBucketOverrideEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (HierarchyBucketOverrideDTO)cache[ent];
			
			var newDTO = new HierarchyBucketOverrideDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.DateCreated = ent.DateCreated;
			

			newDTO.EndDate = ent.EndDate;
			

			newDTO.HierarchyBucketId = ent.HierarchyBucketId;
			

			newDTO.HierarchyBucketOverrideTypeId = ent.HierarchyBucketOverrideTypeId;
			

			newDTO.Id = ent.Id;
			

			newDTO.Notes = ent.Notes;
			

			newDTO.OriginalUserId = ent.OriginalUserId;
			

			newDTO.RoleId = ent.RoleId;
			

			newDTO.StartDate = ent.StartDate;
			

			newDTO.UserId = ent.UserId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.HierarchyBucket = ent.HierarchyBucket.ToDTO(cache);
			
			newDTO.HierarchyBucketOverrideType = ent.HierarchyBucketOverrideType.ToDTO(cache);
			
			newDTO.HierarchyRole = ent.HierarchyRole.ToDTO(cache);
			
			newDTO.OriginalUser = ent.OriginalUser.ToDTO(cache);
			
			newDTO.UserData = ent.UserData.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}