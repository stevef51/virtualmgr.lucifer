﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class ErrorLogItemConverter
	{
	
		public static ErrorLogItemEntity ToEntity(this ErrorLogItemDTO dto)
		{
			return dto.ToEntity(new ErrorLogItemEntity(), new Dictionary<object, object>());
		}

		public static ErrorLogItemEntity ToEntity(this ErrorLogItemDTO dto, ErrorLogItemEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static ErrorLogItemEntity ToEntity(this ErrorLogItemDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new ErrorLogItemEntity(), cache);
		}
	
		public static ErrorLogItemDTO ToDTO(this ErrorLogItemEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static ErrorLogItemEntity ToEntity(this ErrorLogItemDTO dto, ErrorLogItemEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (ErrorLogItemEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			if (dto.Data == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ErrorLogItemFieldIndex.Data, "");
				
			}
			
			newEnt.Data = dto.Data;
			
			

			
			
			newEnt.DateCreated = dto.DateCreated;
			
			

			
			
			newEnt.ErrorLogTypeId = dto.ErrorLogTypeId;
			
			

			
			
			newEnt.Id = dto.Id;
			
			
			

			
			
			if (dto.UserAgentId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ErrorLogItemFieldIndex.UserAgentId, default(System.Int32));
				
			}
			
			newEnt.UserAgentId = dto.UserAgentId;
			
			

			
			
			if (dto.UserId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ErrorLogItemFieldIndex.UserId, default(System.Guid));
				
			}
			
			newEnt.UserId = dto.UserId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.ErrorLogType = dto.ErrorLogType.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static ErrorLogItemDTO ToDTO(this ErrorLogItemEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (ErrorLogItemDTO)cache[ent];
			
			var newDTO = new ErrorLogItemDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Data = ent.Data;
			

			newDTO.DateCreated = ent.DateCreated;
			

			newDTO.ErrorLogTypeId = ent.ErrorLogTypeId;
			

			newDTO.Id = ent.Id;
			

			newDTO.UserAgentId = ent.UserAgentId;
			

			newDTO.UserId = ent.UserId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.ErrorLogType = ent.ErrorLogType.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}