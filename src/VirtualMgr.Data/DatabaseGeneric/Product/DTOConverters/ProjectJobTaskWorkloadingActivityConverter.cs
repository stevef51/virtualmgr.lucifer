﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class ProjectJobTaskWorkloadingActivityConverter
	{
	
		public static ProjectJobTaskWorkloadingActivityEntity ToEntity(this ProjectJobTaskWorkloadingActivityDTO dto)
		{
			return dto.ToEntity(new ProjectJobTaskWorkloadingActivityEntity(), new Dictionary<object, object>());
		}

		public static ProjectJobTaskWorkloadingActivityEntity ToEntity(this ProjectJobTaskWorkloadingActivityDTO dto, ProjectJobTaskWorkloadingActivityEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static ProjectJobTaskWorkloadingActivityEntity ToEntity(this ProjectJobTaskWorkloadingActivityDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new ProjectJobTaskWorkloadingActivityEntity(), cache);
		}
	
		public static ProjectJobTaskWorkloadingActivityDTO ToDTO(this ProjectJobTaskWorkloadingActivityEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static ProjectJobTaskWorkloadingActivityEntity ToEntity(this ProjectJobTaskWorkloadingActivityDTO dto, ProjectJobTaskWorkloadingActivityEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (ProjectJobTaskWorkloadingActivityEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			if (dto.Completed == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobTaskWorkloadingActivityFieldIndex.Completed, default(System.Boolean));
				
			}
			
			newEnt.Completed = dto.Completed;
			
			

			
			
			newEnt.EstimatedDuration = dto.EstimatedDuration;
			
			

			
			
			newEnt.ProjectJobTaskId = dto.ProjectJobTaskId;
			
			

			
			
			newEnt.SiteFeatureId = dto.SiteFeatureId;
			
			

			
			
			newEnt.Text = dto.Text;
			
			

			
			
			newEnt.WorkLoadingStandardId = dto.WorkLoadingStandardId;
			
			

			
			
			newEnt.WorkloadingStandardMeasure = dto.WorkloadingStandardMeasure;
			
			

			
			
			newEnt.WorkloadingStandardSeconds = dto.WorkloadingStandardSeconds;
			
			

			
			
			newEnt.WorkloadingStandardUnitId = dto.WorkloadingStandardUnitId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.ProjectJobTask = dto.ProjectJobTask.ToEntity(cache);
			
			newEnt.SiteFeature = dto.SiteFeature.ToEntity(cache);
			
			newEnt.WorkLoadingStandard = dto.WorkLoadingStandard.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static ProjectJobTaskWorkloadingActivityDTO ToDTO(this ProjectJobTaskWorkloadingActivityEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (ProjectJobTaskWorkloadingActivityDTO)cache[ent];
			
			var newDTO = new ProjectJobTaskWorkloadingActivityDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Completed = ent.Completed;
			

			newDTO.EstimatedDuration = ent.EstimatedDuration;
			

			newDTO.ProjectJobTaskId = ent.ProjectJobTaskId;
			

			newDTO.SiteFeatureId = ent.SiteFeatureId;
			

			newDTO.Text = ent.Text;
			

			newDTO.WorkLoadingStandardId = ent.WorkLoadingStandardId;
			

			newDTO.WorkloadingStandardMeasure = ent.WorkloadingStandardMeasure;
			

			newDTO.WorkloadingStandardSeconds = ent.WorkloadingStandardSeconds;
			

			newDTO.WorkloadingStandardUnitId = ent.WorkloadingStandardUnitId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.ProjectJobTask = ent.ProjectJobTask.ToDTO(cache);
			
			newDTO.SiteFeature = ent.SiteFeature.ToDTO(cache);
			
			newDTO.WorkLoadingStandard = ent.WorkLoadingStandard.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}