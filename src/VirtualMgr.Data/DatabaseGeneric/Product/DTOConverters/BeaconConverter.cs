﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class BeaconConverter
	{
	
		public static BeaconEntity ToEntity(this BeaconDTO dto)
		{
			return dto.ToEntity(new BeaconEntity(), new Dictionary<object, object>());
		}

		public static BeaconEntity ToEntity(this BeaconDTO dto, BeaconEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static BeaconEntity ToEntity(this BeaconDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new BeaconEntity(), cache);
		}
	
		public static BeaconDTO ToDTO(this BeaconEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static BeaconEntity ToEntity(this BeaconDTO dto, BeaconEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (BeaconEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			if (dto.AccuracyMetres == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)BeaconFieldIndex.AccuracyMetres, default(System.Decimal));
				
			}
			
			newEnt.AccuracyMetres = dto.AccuracyMetres;
			
			

			
			
			if (dto.AssetId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)BeaconFieldIndex.AssetId, default(System.Int32));
				
			}
			
			newEnt.AssetId = dto.AssetId;
			
			

			
			
			if (dto.BatteryPercent == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)BeaconFieldIndex.BatteryPercent, default(System.Int32));
				
			}
			
			newEnt.BatteryPercent = dto.BatteryPercent;
			
			

			
			
			if (dto.BeaconConfigurationId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)BeaconFieldIndex.BeaconConfigurationId, default(System.Int32));
				
			}
			
			newEnt.BeaconConfigurationId = dto.BeaconConfigurationId;
			
			

			
			
			if (dto.FacilityStructureId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)BeaconFieldIndex.FacilityStructureId, default(System.Int32));
				
			}
			
			newEnt.FacilityStructureId = dto.FacilityStructureId;
			
			

			
			
			newEnt.Id = dto.Id;
			
			

			
			
			if (dto.LastButtonPressedUtc == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)BeaconFieldIndex.LastButtonPressedUtc, default(System.DateTime));
				
			}
			
			newEnt.LastButtonPressedUtc = dto.LastButtonPressedUtc;
			
			

			
			
			if (dto.LastContactUtc == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)BeaconFieldIndex.LastContactUtc, default(System.DateTime));
				
			}
			
			newEnt.LastContactUtc = dto.LastContactUtc;
			
			

			
			
			if (dto.LastGpslocationId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)BeaconFieldIndex.LastGpslocationId, default(System.Int64));
				
			}
			
			newEnt.LastGpslocationId = dto.LastGpslocationId;
			
			

			
			
			if (dto.Latitude == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)BeaconFieldIndex.Latitude, default(System.Decimal));
				
			}
			
			newEnt.Latitude = dto.Latitude;
			
			

			
			
			if (dto.Longitude == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)BeaconFieldIndex.Longitude, default(System.Decimal));
				
			}
			
			newEnt.Longitude = dto.Longitude;
			
			

			
			
			if (dto.Name == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)BeaconFieldIndex.Name, "");
				
			}
			
			newEnt.Name = dto.Name;
			
			

			
			
			if (dto.StaticHeightFromFloor == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)BeaconFieldIndex.StaticHeightFromFloor, default(System.Decimal));
				
			}
			
			newEnt.StaticHeightFromFloor = dto.StaticHeightFromFloor;
			
			

			
			
			if (dto.StaticLocationId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)BeaconFieldIndex.StaticLocationId, default(System.Int64));
				
			}
			
			newEnt.StaticLocationId = dto.StaticLocationId;
			
			

			
			
			if (dto.TaskTypeId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)BeaconFieldIndex.TaskTypeId, default(System.Guid));
				
			}
			
			newEnt.TaskTypeId = dto.TaskTypeId;
			
			

			
			
			if (dto.UserId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)BeaconFieldIndex.UserId, default(System.Guid));
				
			}
			
			newEnt.UserId = dto.UserId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			newEnt.Asset = dto.Asset.ToEntity(cache);
			
			
			
			newEnt.FacilityStructure = dto.FacilityStructure.ToEntity(cache);
			
			newEnt.LastGpsLocation = dto.LastGpsLocation.ToEntity(cache);
			
			newEnt.StaticLocation = dto.StaticLocation.ToEntity(cache);
			
			newEnt.ProjectJobTaskType = dto.ProjectJobTaskType.ToEntity(cache);
			
			newEnt.UserData = dto.UserData.ToEntity(cache);
			
			
			
			foreach (var related in dto.BeaconSightings)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.BeaconSightings.Contains(relatedEntity))
				{
					newEnt.BeaconSightings.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static BeaconDTO ToDTO(this BeaconEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (BeaconDTO)cache[ent];
			
			var newDTO = new BeaconDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.AccuracyMetres = ent.AccuracyMetres;
			

			newDTO.AssetId = ent.AssetId;
			

			newDTO.BatteryPercent = ent.BatteryPercent;
			

			newDTO.BeaconConfigurationId = ent.BeaconConfigurationId;
			

			newDTO.FacilityStructureId = ent.FacilityStructureId;
			

			newDTO.Id = ent.Id;
			

			newDTO.LastButtonPressedUtc = ent.LastButtonPressedUtc;
			

			newDTO.LastContactUtc = ent.LastContactUtc;
			

			newDTO.LastGpslocationId = ent.LastGpslocationId;
			

			newDTO.Latitude = ent.Latitude;
			

			newDTO.Longitude = ent.Longitude;
			

			newDTO.Name = ent.Name;
			

			newDTO.StaticHeightFromFloor = ent.StaticHeightFromFloor;
			

			newDTO.StaticLocationId = ent.StaticLocationId;
			

			newDTO.TaskTypeId = ent.TaskTypeId;
			

			newDTO.UserId = ent.UserId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			newDTO.Asset = ent.Asset.ToDTO(cache);
			
			
			
			newDTO.FacilityStructure = ent.FacilityStructure.ToDTO(cache);
			
			newDTO.LastGpsLocation = ent.LastGpsLocation.ToDTO(cache);
			
			newDTO.StaticLocation = ent.StaticLocation.ToDTO(cache);
			
			newDTO.ProjectJobTaskType = ent.ProjectJobTaskType.ToDTO(cache);
			
			newDTO.UserData = ent.UserData.ToDTO(cache);
			
			
			
			foreach (var related in ent.BeaconSightings)
			{
				newDTO.BeaconSightings.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}