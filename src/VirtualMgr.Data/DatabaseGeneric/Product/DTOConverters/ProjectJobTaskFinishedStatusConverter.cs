﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class ProjectJobTaskFinishedStatusConverter
	{
	
		public static ProjectJobTaskFinishedStatusEntity ToEntity(this ProjectJobTaskFinishedStatusDTO dto)
		{
			return dto.ToEntity(new ProjectJobTaskFinishedStatusEntity(), new Dictionary<object, object>());
		}

		public static ProjectJobTaskFinishedStatusEntity ToEntity(this ProjectJobTaskFinishedStatusDTO dto, ProjectJobTaskFinishedStatusEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static ProjectJobTaskFinishedStatusEntity ToEntity(this ProjectJobTaskFinishedStatusDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new ProjectJobTaskFinishedStatusEntity(), cache);
		}
	
		public static ProjectJobTaskFinishedStatusDTO ToDTO(this ProjectJobTaskFinishedStatusEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static ProjectJobTaskFinishedStatusEntity ToEntity(this ProjectJobTaskFinishedStatusDTO dto, ProjectJobTaskFinishedStatusEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (ProjectJobTaskFinishedStatusEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			if (dto.Notes == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobTaskFinishedStatusFieldIndex.Notes, "");
				
			}
			
			newEnt.Notes = dto.Notes;
			
			

			
			
			newEnt.ProjectJobFinishedStatusId = dto.ProjectJobFinishedStatusId;
			
			

			
			
			newEnt.TaskId = dto.TaskId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.ProjectJobFinishedStatu = dto.ProjectJobFinishedStatu.ToEntity(cache);
			
			newEnt.ProjectJobTask = dto.ProjectJobTask.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static ProjectJobTaskFinishedStatusDTO ToDTO(this ProjectJobTaskFinishedStatusEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (ProjectJobTaskFinishedStatusDTO)cache[ent];
			
			var newDTO = new ProjectJobTaskFinishedStatusDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Notes = ent.Notes;
			

			newDTO.ProjectJobFinishedStatusId = ent.ProjectJobFinishedStatusId;
			

			newDTO.TaskId = ent.TaskId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.ProjectJobFinishedStatu = ent.ProjectJobFinishedStatu.ToDTO(cache);
			
			newDTO.ProjectJobTask = ent.ProjectJobTask.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}