﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class WorkingDocumentPublicVariableConverter
	{
	
		public static WorkingDocumentPublicVariableEntity ToEntity(this WorkingDocumentPublicVariableDTO dto)
		{
			return dto.ToEntity(new WorkingDocumentPublicVariableEntity(), new Dictionary<object, object>());
		}

		public static WorkingDocumentPublicVariableEntity ToEntity(this WorkingDocumentPublicVariableDTO dto, WorkingDocumentPublicVariableEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static WorkingDocumentPublicVariableEntity ToEntity(this WorkingDocumentPublicVariableDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new WorkingDocumentPublicVariableEntity(), cache);
		}
	
		public static WorkingDocumentPublicVariableDTO ToDTO(this WorkingDocumentPublicVariableEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static WorkingDocumentPublicVariableEntity ToEntity(this WorkingDocumentPublicVariableDTO dto, WorkingDocumentPublicVariableEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (WorkingDocumentPublicVariableEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Id = dto.Id;
			
			

			
			
			newEnt.Name = dto.Name;
			
			

			
			
			if (dto.PassFail == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)WorkingDocumentPublicVariableFieldIndex.PassFail, default(System.Boolean));
				
			}
			
			newEnt.PassFail = dto.PassFail;
			
			

			
			
			if (dto.Score == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)WorkingDocumentPublicVariableFieldIndex.Score, default(System.Decimal));
				
			}
			
			newEnt.Score = dto.Score;
			
			

			
			
			if (dto.Value == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)WorkingDocumentPublicVariableFieldIndex.Value, "");
				
			}
			
			newEnt.Value = dto.Value;
			
			

			
			
			newEnt.WorkingDocumentId = dto.WorkingDocumentId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.WorkingDocument = dto.WorkingDocument.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static WorkingDocumentPublicVariableDTO ToDTO(this WorkingDocumentPublicVariableEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (WorkingDocumentPublicVariableDTO)cache[ent];
			
			var newDTO = new WorkingDocumentPublicVariableDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Id = ent.Id;
			

			newDTO.Name = ent.Name;
			

			newDTO.PassFail = ent.PassFail;
			

			newDTO.Score = ent.Score;
			

			newDTO.Value = ent.Value;
			

			newDTO.WorkingDocumentId = ent.WorkingDocumentId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.WorkingDocument = ent.WorkingDocument.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}