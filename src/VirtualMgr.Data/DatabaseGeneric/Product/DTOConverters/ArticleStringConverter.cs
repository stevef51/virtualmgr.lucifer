﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class ArticleStringConverter
	{
	
		public static ArticleStringEntity ToEntity(this ArticleStringDTO dto)
		{
			return dto.ToEntity(new ArticleStringEntity(), new Dictionary<object, object>());
		}

		public static ArticleStringEntity ToEntity(this ArticleStringDTO dto, ArticleStringEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static ArticleStringEntity ToEntity(this ArticleStringDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new ArticleStringEntity(), cache);
		}
	
		public static ArticleStringDTO ToDTO(this ArticleStringEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static ArticleStringEntity ToEntity(this ArticleStringDTO dto, ArticleStringEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (ArticleStringEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.ArticleId = dto.ArticleId;
			
			

			
			
			newEnt.Index = dto.Index;
			
			

			
			
			newEnt.Name = dto.Name;
			
			

			
			
			if (dto.Value == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ArticleStringFieldIndex.Value, "");
				
			}
			
			newEnt.Value = dto.Value;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.Article = dto.Article.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static ArticleStringDTO ToDTO(this ArticleStringEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (ArticleStringDTO)cache[ent];
			
			var newDTO = new ArticleStringDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.ArticleId = ent.ArticleId;
			

			newDTO.Index = ent.Index;
			

			newDTO.Name = ent.Name;
			

			newDTO.Value = ent.Value;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.Article = ent.Article.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}