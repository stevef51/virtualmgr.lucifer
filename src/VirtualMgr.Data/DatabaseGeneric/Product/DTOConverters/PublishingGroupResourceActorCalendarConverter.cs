﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class PublishingGroupResourceActorCalendarConverter
	{
	
		public static PublishingGroupResourceActorCalendarEntity ToEntity(this PublishingGroupResourceActorCalendarDTO dto)
		{
			return dto.ToEntity(new PublishingGroupResourceActorCalendarEntity(), new Dictionary<object, object>());
		}

		public static PublishingGroupResourceActorCalendarEntity ToEntity(this PublishingGroupResourceActorCalendarDTO dto, PublishingGroupResourceActorCalendarEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static PublishingGroupResourceActorCalendarEntity ToEntity(this PublishingGroupResourceActorCalendarDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new PublishingGroupResourceActorCalendarEntity(), cache);
		}
	
		public static PublishingGroupResourceActorCalendarDTO ToDTO(this PublishingGroupResourceActorCalendarEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static PublishingGroupResourceActorCalendarEntity ToEntity(this PublishingGroupResourceActorCalendarDTO dto, PublishingGroupResourceActorCalendarEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (PublishingGroupResourceActorCalendarEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.NextRunDate = dto.NextRunDate;
			
			

			
			
			newEnt.PublishingGroupResourceCalendarId = dto.PublishingGroupResourceCalendarId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			newEnt.PublishingGroupResourceCalendar = dto.PublishingGroupResourceCalendar.ToEntity(cache);
			
			
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static PublishingGroupResourceActorCalendarDTO ToDTO(this PublishingGroupResourceActorCalendarEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (PublishingGroupResourceActorCalendarDTO)cache[ent];
			
			var newDTO = new PublishingGroupResourceActorCalendarDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.NextRunDate = ent.NextRunDate;
			

			newDTO.PublishingGroupResourceCalendarId = ent.PublishingGroupResourceCalendarId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			newDTO.PublishingGroupResourceCalendar = ent.PublishingGroupResourceCalendar.ToDTO(cache);
			
			
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}