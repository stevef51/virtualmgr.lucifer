﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class WorkLoadingStandardConverter
	{
	
		public static WorkLoadingStandardEntity ToEntity(this WorkLoadingStandardDTO dto)
		{
			return dto.ToEntity(new WorkLoadingStandardEntity(), new Dictionary<object, object>());
		}

		public static WorkLoadingStandardEntity ToEntity(this WorkLoadingStandardDTO dto, WorkLoadingStandardEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static WorkLoadingStandardEntity ToEntity(this WorkLoadingStandardDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new WorkLoadingStandardEntity(), cache);
		}
	
		public static WorkLoadingStandardDTO ToDTO(this WorkLoadingStandardEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static WorkLoadingStandardEntity ToEntity(this WorkLoadingStandardDTO dto, WorkLoadingStandardEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (WorkLoadingStandardEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.ActivityId = dto.ActivityId;
			
			

			
			
			newEnt.Archived = dto.Archived;
			
			

			
			
			newEnt.BookId = dto.BookId;
			
			

			
			
			newEnt.FeatureId = dto.FeatureId;
			
			

			
			
			newEnt.Id = dto.Id;
			
			

			
			
			newEnt.Measure = dto.Measure;
			
			

			
			
			newEnt.Seconds = dto.Seconds;
			
			

			
			
			newEnt.UnitId = dto.UnitId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.MeasurementUnit = dto.MeasurementUnit.ToEntity(cache);
			
			newEnt.WorkLoadingActivity = dto.WorkLoadingActivity.ToEntity(cache);
			
			newEnt.WorkLoadingBook = dto.WorkLoadingBook.ToEntity(cache);
			
			newEnt.WorkLoadingFeature = dto.WorkLoadingFeature.ToEntity(cache);
			
			
			
			foreach (var related in dto.ProjectJobScheduledTaskWorkloadingActivities)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProjectJobScheduledTaskWorkloadingActivities.Contains(relatedEntity))
				{
					newEnt.ProjectJobScheduledTaskWorkloadingActivities.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ProjectJobTaskWorkloadingActivities)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProjectJobTaskWorkloadingActivities.Contains(relatedEntity))
				{
					newEnt.ProjectJobTaskWorkloadingActivities.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static WorkLoadingStandardDTO ToDTO(this WorkLoadingStandardEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (WorkLoadingStandardDTO)cache[ent];
			
			var newDTO = new WorkLoadingStandardDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.ActivityId = ent.ActivityId;
			

			newDTO.Archived = ent.Archived;
			

			newDTO.BookId = ent.BookId;
			

			newDTO.FeatureId = ent.FeatureId;
			

			newDTO.Id = ent.Id;
			

			newDTO.Measure = ent.Measure;
			

			newDTO.Seconds = ent.Seconds;
			

			newDTO.UnitId = ent.UnitId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.MeasurementUnit = ent.MeasurementUnit.ToDTO(cache);
			
			newDTO.WorkLoadingActivity = ent.WorkLoadingActivity.ToDTO(cache);
			
			newDTO.WorkLoadingBook = ent.WorkLoadingBook.ToDTO(cache);
			
			newDTO.WorkLoadingFeature = ent.WorkLoadingFeature.ToDTO(cache);
			
			
			
			foreach (var related in ent.ProjectJobScheduledTaskWorkloadingActivities)
			{
				newDTO.ProjectJobScheduledTaskWorkloadingActivities.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ProjectJobTaskWorkloadingActivities)
			{
				newDTO.ProjectJobTaskWorkloadingActivities.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}