﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class QrcodeConverter
	{
	
		public static QrcodeEntity ToEntity(this QrcodeDTO dto)
		{
			return dto.ToEntity(new QrcodeEntity(), new Dictionary<object, object>());
		}

		public static QrcodeEntity ToEntity(this QrcodeDTO dto, QrcodeEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static QrcodeEntity ToEntity(this QrcodeDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new QrcodeEntity(), cache);
		}
	
		public static QrcodeDTO ToDTO(this QrcodeEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static QrcodeEntity ToEntity(this QrcodeDTO dto, QrcodeEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (QrcodeEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			if (dto.AssetId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)QrcodeFieldIndex.AssetId, default(System.Int32));
				
			}
			
			newEnt.AssetId = dto.AssetId;
			
			

			
			
			newEnt.Id = dto.Id;
			
			

			
			
			newEnt.Qrcode = dto.Qrcode;
			
			

			
			
			newEnt.Selected = dto.Selected;
			
			

			
			
			if (dto.TaskTypeId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)QrcodeFieldIndex.TaskTypeId, default(System.Guid));
				
			}
			
			newEnt.TaskTypeId = dto.TaskTypeId;
			
			

			
			
			if (dto.UserId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)QrcodeFieldIndex.UserId, default(System.Guid));
				
			}
			
			newEnt.UserId = dto.UserId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.Asset = dto.Asset.ToEntity(cache);
			
			newEnt.ProjectJobTaskType = dto.ProjectJobTaskType.ToEntity(cache);
			
			newEnt.UserData = dto.UserData.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static QrcodeDTO ToDTO(this QrcodeEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (QrcodeDTO)cache[ent];
			
			var newDTO = new QrcodeDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.AssetId = ent.AssetId;
			

			newDTO.Id = ent.Id;
			

			newDTO.Qrcode = ent.Qrcode;
			

			newDTO.Selected = ent.Selected;
			

			newDTO.TaskTypeId = ent.TaskTypeId;
			

			newDTO.UserId = ent.UserId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.Asset = ent.Asset.ToDTO(cache);
			
			newDTO.ProjectJobTaskType = ent.ProjectJobTaskType.ToDTO(cache);
			
			newDTO.UserData = ent.UserData.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}