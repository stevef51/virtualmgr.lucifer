﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class ESSensorConverter
	{
	
		public static ESSensorEntity ToEntity(this ESSensorDTO dto)
		{
			return dto.ToEntity(new ESSensorEntity(), new Dictionary<object, object>());
		}

		public static ESSensorEntity ToEntity(this ESSensorDTO dto, ESSensorEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static ESSensorEntity ToEntity(this ESSensorDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new ESSensorEntity(), cache);
		}
	
		public static ESSensorDTO ToDTO(this ESSensorEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static ESSensorEntity ToEntity(this ESSensorDTO dto, ESSensorEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (ESSensorEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			if (dto.Compliance == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ESSensorFieldIndex.Compliance, "");
				
			}
			
			newEnt.Compliance = dto.Compliance;
			
			

			
			
			newEnt.Id = dto.Id;
			
			

			
			
			newEnt.ProbeId = dto.ProbeId;
			
			

			
			
			newEnt.SensorIndex = dto.SensorIndex;
			
			

			
			
			newEnt.SensorType = dto.SensorType;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.Probe = dto.Probe.ToEntity(cache);
			
			
			
			foreach (var related in dto.SensorReadings)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.SensorReadings.Contains(relatedEntity))
				{
					newEnt.SensorReadings.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static ESSensorDTO ToDTO(this ESSensorEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (ESSensorDTO)cache[ent];
			
			var newDTO = new ESSensorDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Compliance = ent.Compliance;
			

			newDTO.Id = ent.Id;
			

			newDTO.ProbeId = ent.ProbeId;
			

			newDTO.SensorIndex = ent.SensorIndex;
			

			newDTO.SensorType = ent.SensorType;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.Probe = ent.Probe.ToDTO(cache);
			
			
			
			foreach (var related in ent.SensorReadings)
			{
				newDTO.SensorReadings.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}