﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class TabletLocationHistoryConverter
	{
	
		public static TabletLocationHistoryEntity ToEntity(this TabletLocationHistoryDTO dto)
		{
			return dto.ToEntity(new TabletLocationHistoryEntity(), new Dictionary<object, object>());
		}

		public static TabletLocationHistoryEntity ToEntity(this TabletLocationHistoryDTO dto, TabletLocationHistoryEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static TabletLocationHistoryEntity ToEntity(this TabletLocationHistoryDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new TabletLocationHistoryEntity(), cache);
		}
	
		public static TabletLocationHistoryDTO ToDTO(this TabletLocationHistoryEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static TabletLocationHistoryEntity ToEntity(this TabletLocationHistoryDTO dto, TabletLocationHistoryEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (TabletLocationHistoryEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			if (dto.GpslocationId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)TabletLocationHistoryFieldIndex.GpslocationId, default(System.Int64));
				
			}
			
			newEnt.GpslocationId = dto.GpslocationId;
			
			

			
			
			newEnt.Id = dto.Id;
			
			
			

			
			
			if (dto.LoggedInUserId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)TabletLocationHistoryFieldIndex.LoggedInUserId, default(System.Guid));
				
			}
			
			newEnt.LoggedInUserId = dto.LoggedInUserId;
			
			

			
			
			if (dto.Range == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)TabletLocationHistoryFieldIndex.Range, default(System.Int32));
				
			}
			
			newEnt.Range = dto.Range;
			
			

			
			
			if (dto.RangeUtc == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)TabletLocationHistoryFieldIndex.RangeUtc, default(System.DateTime));
				
			}
			
			newEnt.RangeUtc = dto.RangeUtc;
			
			

			
			
			newEnt.ServerTimestampUtc = dto.ServerTimestampUtc;
			
			

			
			
			newEnt.TabletTimestampUtc = dto.TabletTimestampUtc;
			
			

			
			
			newEnt.TabletUuid = dto.TabletUuid;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.Gpslocation = dto.Gpslocation.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static TabletLocationHistoryDTO ToDTO(this TabletLocationHistoryEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (TabletLocationHistoryDTO)cache[ent];
			
			var newDTO = new TabletLocationHistoryDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.GpslocationId = ent.GpslocationId;
			

			newDTO.Id = ent.Id;
			

			newDTO.LoggedInUserId = ent.LoggedInUserId;
			

			newDTO.Range = ent.Range;
			

			newDTO.RangeUtc = ent.RangeUtc;
			

			newDTO.ServerTimestampUtc = ent.ServerTimestampUtc;
			

			newDTO.TabletTimestampUtc = ent.TabletTimestampUtc;
			

			newDTO.TabletUuid = ent.TabletUuid;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.Gpslocation = ent.Gpslocation.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}