﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class RosterEventConditionConverter
	{
	
		public static RosterEventConditionEntity ToEntity(this RosterEventConditionDTO dto)
		{
			return dto.ToEntity(new RosterEventConditionEntity(), new Dictionary<object, object>());
		}

		public static RosterEventConditionEntity ToEntity(this RosterEventConditionDTO dto, RosterEventConditionEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static RosterEventConditionEntity ToEntity(this RosterEventConditionDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new RosterEventConditionEntity(), cache);
		}
	
		public static RosterEventConditionDTO ToDTO(this RosterEventConditionEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static RosterEventConditionEntity ToEntity(this RosterEventConditionDTO dto, RosterEventConditionEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (RosterEventConditionEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Condition = dto.Condition;
			
			

			
			
			if (dto.Data == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)RosterEventConditionFieldIndex.Data, "");
				
			}
			
			newEnt.Data = dto.Data;
			
			

			
			
			newEnt.Id = dto.Id;
			
			

			
			
			if (dto.NextRunTime == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)RosterEventConditionFieldIndex.NextRunTime, default(System.DateTime));
				
			}
			
			newEnt.NextRunTime = dto.NextRunTime;
			
			

			
			
			newEnt.RosterId = dto.RosterId;
			
			

			
			
			newEnt.RosterProjectJobTaskEvents = dto.RosterProjectJobTaskEvents;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.Roster = dto.Roster.ToEntity(cache);
			
			
			
			foreach (var related in dto.RosterEventConditionActions)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.RosterEventConditionActions.Contains(relatedEntity))
				{
					newEnt.RosterEventConditionActions.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static RosterEventConditionDTO ToDTO(this RosterEventConditionEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (RosterEventConditionDTO)cache[ent];
			
			var newDTO = new RosterEventConditionDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Condition = ent.Condition;
			

			newDTO.Data = ent.Data;
			

			newDTO.Id = ent.Id;
			

			newDTO.NextRunTime = ent.NextRunTime;
			

			newDTO.RosterId = ent.RosterId;
			

			newDTO.RosterProjectJobTaskEvents = ent.RosterProjectJobTaskEvents;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.Roster = ent.Roster.ToDTO(cache);
			
			
			
			foreach (var related in ent.RosterEventConditionActions)
			{
				newDTO.RosterEventConditionActions.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}