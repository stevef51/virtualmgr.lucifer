﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class OrderMediaConverter
	{
	
		public static OrderMediaEntity ToEntity(this OrderMediaDTO dto)
		{
			return dto.ToEntity(new OrderMediaEntity(), new Dictionary<object, object>());
		}

		public static OrderMediaEntity ToEntity(this OrderMediaDTO dto, OrderMediaEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static OrderMediaEntity ToEntity(this OrderMediaDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new OrderMediaEntity(), cache);
		}
	
		public static OrderMediaDTO ToDTO(this OrderMediaEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static OrderMediaEntity ToEntity(this OrderMediaDTO dto, OrderMediaEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (OrderMediaEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.DateCreated = dto.DateCreated;
			
			

			
			
			newEnt.Id = dto.Id;
			
			
			

			
			
			newEnt.MediaId = dto.MediaId;
			
			

			
			
			newEnt.OrderId = dto.OrderId;
			
			

			
			
			newEnt.Tag = dto.Tag;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.Medium = dto.Medium.ToEntity(cache);
			
			newEnt.Order = dto.Order.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static OrderMediaDTO ToDTO(this OrderMediaEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (OrderMediaDTO)cache[ent];
			
			var newDTO = new OrderMediaDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.DateCreated = ent.DateCreated;
			

			newDTO.Id = ent.Id;
			

			newDTO.MediaId = ent.MediaId;
			

			newDTO.OrderId = ent.OrderId;
			

			newDTO.Tag = ent.Tag;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.Medium = ent.Medium.ToDTO(cache);
			
			newDTO.Order = ent.Order.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}