﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class ProjectJobScheduledTaskWorkloadingActivityConverter
	{
	
		public static ProjectJobScheduledTaskWorkloadingActivityEntity ToEntity(this ProjectJobScheduledTaskWorkloadingActivityDTO dto)
		{
			return dto.ToEntity(new ProjectJobScheduledTaskWorkloadingActivityEntity(), new Dictionary<object, object>());
		}

		public static ProjectJobScheduledTaskWorkloadingActivityEntity ToEntity(this ProjectJobScheduledTaskWorkloadingActivityDTO dto, ProjectJobScheduledTaskWorkloadingActivityEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static ProjectJobScheduledTaskWorkloadingActivityEntity ToEntity(this ProjectJobScheduledTaskWorkloadingActivityDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new ProjectJobScheduledTaskWorkloadingActivityEntity(), cache);
		}
	
		public static ProjectJobScheduledTaskWorkloadingActivityDTO ToDTO(this ProjectJobScheduledTaskWorkloadingActivityEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static ProjectJobScheduledTaskWorkloadingActivityEntity ToEntity(this ProjectJobScheduledTaskWorkloadingActivityDTO dto, ProjectJobScheduledTaskWorkloadingActivityEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (ProjectJobScheduledTaskWorkloadingActivityEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			if (dto.EstimatedDuration == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobScheduledTaskWorkloadingActivityFieldIndex.EstimatedDuration, default(System.Decimal));
				
			}
			
			newEnt.EstimatedDuration = dto.EstimatedDuration;
			
			

			
			
			if (dto.ExecuteFriday == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobScheduledTaskWorkloadingActivityFieldIndex.ExecuteFriday, default(System.Boolean));
				
			}
			
			newEnt.ExecuteFriday = dto.ExecuteFriday;
			
			

			
			
			if (dto.ExecuteMonday == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobScheduledTaskWorkloadingActivityFieldIndex.ExecuteMonday, default(System.Boolean));
				
			}
			
			newEnt.ExecuteMonday = dto.ExecuteMonday;
			
			

			
			
			if (dto.ExecuteSaturday == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobScheduledTaskWorkloadingActivityFieldIndex.ExecuteSaturday, default(System.Boolean));
				
			}
			
			newEnt.ExecuteSaturday = dto.ExecuteSaturday;
			
			

			
			
			if (dto.ExecuteSunday == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobScheduledTaskWorkloadingActivityFieldIndex.ExecuteSunday, default(System.Boolean));
				
			}
			
			newEnt.ExecuteSunday = dto.ExecuteSunday;
			
			

			
			
			if (dto.ExecuteThursday == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobScheduledTaskWorkloadingActivityFieldIndex.ExecuteThursday, default(System.Boolean));
				
			}
			
			newEnt.ExecuteThursday = dto.ExecuteThursday;
			
			

			
			
			if (dto.ExecuteTuesday == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobScheduledTaskWorkloadingActivityFieldIndex.ExecuteTuesday, default(System.Boolean));
				
			}
			
			newEnt.ExecuteTuesday = dto.ExecuteTuesday;
			
			

			
			
			if (dto.ExecuteWednesday == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobScheduledTaskWorkloadingActivityFieldIndex.ExecuteWednesday, default(System.Boolean));
				
			}
			
			newEnt.ExecuteWednesday = dto.ExecuteWednesday;
			
			

			
			
			newEnt.Frequency = dto.Frequency;
			
			

			
			
			newEnt.ProjectJobScheduledTaskId = dto.ProjectJobScheduledTaskId;
			
			

			
			
			newEnt.SiteFeatureId = dto.SiteFeatureId;
			
			

			
			
			newEnt.Text = dto.Text;
			
			

			
			
			newEnt.WorkloadingStandardId = dto.WorkloadingStandardId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.ProjectJobScheduledTask = dto.ProjectJobScheduledTask.ToEntity(cache);
			
			newEnt.SiteFeature = dto.SiteFeature.ToEntity(cache);
			
			newEnt.WorkLoadingStandard = dto.WorkLoadingStandard.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static ProjectJobScheduledTaskWorkloadingActivityDTO ToDTO(this ProjectJobScheduledTaskWorkloadingActivityEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (ProjectJobScheduledTaskWorkloadingActivityDTO)cache[ent];
			
			var newDTO = new ProjectJobScheduledTaskWorkloadingActivityDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.EstimatedDuration = ent.EstimatedDuration;
			

			newDTO.ExecuteFriday = ent.ExecuteFriday;
			

			newDTO.ExecuteMonday = ent.ExecuteMonday;
			

			newDTO.ExecuteSaturday = ent.ExecuteSaturday;
			

			newDTO.ExecuteSunday = ent.ExecuteSunday;
			

			newDTO.ExecuteThursday = ent.ExecuteThursday;
			

			newDTO.ExecuteTuesday = ent.ExecuteTuesday;
			

			newDTO.ExecuteWednesday = ent.ExecuteWednesday;
			

			newDTO.Frequency = ent.Frequency;
			

			newDTO.ProjectJobScheduledTaskId = ent.ProjectJobScheduledTaskId;
			

			newDTO.SiteFeatureId = ent.SiteFeatureId;
			

			newDTO.Text = ent.Text;
			

			newDTO.WorkloadingStandardId = ent.WorkloadingStandardId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.ProjectJobScheduledTask = ent.ProjectJobScheduledTask.ToDTO(cache);
			
			newDTO.SiteFeature = ent.SiteFeature.ToDTO(cache);
			
			newDTO.WorkLoadingStandard = ent.WorkLoadingStandard.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}