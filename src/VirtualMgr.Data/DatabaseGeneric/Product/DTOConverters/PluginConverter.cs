﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class PluginConverter
	{
	
		public static PluginEntity ToEntity(this PluginDTO dto)
		{
			return dto.ToEntity(new PluginEntity(), new Dictionary<object, object>());
		}

		public static PluginEntity ToEntity(this PluginDTO dto, PluginEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static PluginEntity ToEntity(this PluginDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new PluginEntity(), cache);
		}
	
		public static PluginDTO ToDTO(this PluginEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static PluginEntity ToEntity(this PluginDTO dto, PluginEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (PluginEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.DateCreated = dto.DateCreated;
			
			

			
			
			if (dto.Description == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)PluginFieldIndex.Description, "");
				
			}
			
			newEnt.Description = dto.Description;
			
			

			
			
			newEnt.Id = dto.Id;
			
			

			
			
			newEnt.Name = dto.Name;
			
			
			
			cache.Add(dto, newEnt);
			
			
			newEnt.PluginData = dto.PluginData.ToEntity(cache);
			
			
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static PluginDTO ToDTO(this PluginEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (PluginDTO)cache[ent];
			
			var newDTO = new PluginDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.DateCreated = ent.DateCreated;
			

			newDTO.Description = ent.Description;
			

			newDTO.Id = ent.Id;
			

			newDTO.Name = ent.Name;
			
			
			
			cache.Add(ent, newDTO);
			
			
			newDTO.PluginData = ent.PluginData.ToDTO(cache);
			
			
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}