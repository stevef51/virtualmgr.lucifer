﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class WorkingDocumentConverter
	{
	
		public static WorkingDocumentEntity ToEntity(this WorkingDocumentDTO dto)
		{
			return dto.ToEntity(new WorkingDocumentEntity(), new Dictionary<object, object>());
		}

		public static WorkingDocumentEntity ToEntity(this WorkingDocumentDTO dto, WorkingDocumentEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static WorkingDocumentEntity ToEntity(this WorkingDocumentDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new WorkingDocumentEntity(), cache);
		}
	
		public static WorkingDocumentDTO ToDTO(this WorkingDocumentEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static WorkingDocumentEntity ToEntity(this WorkingDocumentDTO dto, WorkingDocumentEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (WorkingDocumentEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			if (dto.CleanUpIfFinishedAfter == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)WorkingDocumentFieldIndex.CleanUpIfFinishedAfter, default(System.DateTime));
				
			}
			
			newEnt.CleanUpIfFinishedAfter = dto.CleanUpIfFinishedAfter;
			
			

			
			
			if (dto.CleanUpIfNotFinishedAfter == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)WorkingDocumentFieldIndex.CleanUpIfNotFinishedAfter, default(System.DateTime));
				
			}
			
			newEnt.CleanUpIfNotFinishedAfter = dto.CleanUpIfNotFinishedAfter;
			
			

			
			
			if (dto.ClientIp == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)WorkingDocumentFieldIndex.ClientIp, "");
				
			}
			
			newEnt.ClientIp = dto.ClientIp;
			
			

			
			
			newEnt.DateCreated = dto.DateCreated;
			
			

			
			
			newEnt.Id = dto.Id;
			
			

			
			
			if (dto.Lat == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)WorkingDocumentFieldIndex.Lat, default(System.Decimal));
				
			}
			
			newEnt.Lat = dto.Lat;
			
			

			
			
			if (dto.Long == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)WorkingDocumentFieldIndex.Long, default(System.Decimal));
				
			}
			
			newEnt.Long = dto.Long;
			
			

			
			
			newEnt.Name = dto.Name;
			
			

			
			
			if (dto.ParentId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)WorkingDocumentFieldIndex.ParentId, default(System.Guid));
				
			}
			
			newEnt.ParentId = dto.ParentId;
			
			

			
			
			newEnt.PublishingGroupResourceId = dto.PublishingGroupResourceId;
			
			

			
			
			if (dto.RevieweeId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)WorkingDocumentFieldIndex.RevieweeId, default(System.Guid));
				
			}
			
			newEnt.RevieweeId = dto.RevieweeId;
			
			

			
			
			newEnt.ReviewerId = dto.ReviewerId;
			
			

			
			
			newEnt.Status = dto.Status;
			
			

			
			
			if (dto.UserAgentId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)WorkingDocumentFieldIndex.UserAgentId, default(System.Int32));
				
			}
			
			newEnt.UserAgentId = dto.UserAgentId;
			
			

			
			
			newEnt.UtcLastModified = dto.UtcLastModified;
			
			
			
			cache.Add(dto, newEnt);
			
			
			newEnt.WorkingDocumentData = dto.WorkingDocumentData.ToEntity(cache);
			
			
			
			newEnt.PublishingGroupResource = dto.PublishingGroupResource.ToEntity(cache);
			
			newEnt.UserAgent = dto.UserAgent.ToEntity(cache);
			
			newEnt.Reviewee = dto.Reviewee.ToEntity(cache);
			
			newEnt.Reviewer = dto.Reviewer.ToEntity(cache);
			
			newEnt.WorkingDocumentStatu = dto.WorkingDocumentStatu.ToEntity(cache);
			
			
			
			foreach (var related in dto.ProjectJobTaskWorkingDocuments)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProjectJobTaskWorkingDocuments.Contains(relatedEntity))
				{
					newEnt.ProjectJobTaskWorkingDocuments.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.PublicWorkingDocuments)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.PublicWorkingDocuments.Contains(relatedEntity))
				{
					newEnt.PublicWorkingDocuments.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.WorkingDocumentPublicVariables)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.WorkingDocumentPublicVariables.Contains(relatedEntity))
				{
					newEnt.WorkingDocumentPublicVariables.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.WorkingDocumentReferences)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.WorkingDocumentReferences.Contains(relatedEntity))
				{
					newEnt.WorkingDocumentReferences.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static WorkingDocumentDTO ToDTO(this WorkingDocumentEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (WorkingDocumentDTO)cache[ent];
			
			var newDTO = new WorkingDocumentDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.CleanUpIfFinishedAfter = ent.CleanUpIfFinishedAfter;
			

			newDTO.CleanUpIfNotFinishedAfter = ent.CleanUpIfNotFinishedAfter;
			

			newDTO.ClientIp = ent.ClientIp;
			

			newDTO.DateCreated = ent.DateCreated;
			

			newDTO.Id = ent.Id;
			

			newDTO.Lat = ent.Lat;
			

			newDTO.Long = ent.Long;
			

			newDTO.Name = ent.Name;
			

			newDTO.ParentId = ent.ParentId;
			

			newDTO.PublishingGroupResourceId = ent.PublishingGroupResourceId;
			

			newDTO.RevieweeId = ent.RevieweeId;
			

			newDTO.ReviewerId = ent.ReviewerId;
			

			newDTO.Status = ent.Status;
			

			newDTO.UserAgentId = ent.UserAgentId;
			

			newDTO.UtcLastModified = ent.UtcLastModified;
			
			
			
			cache.Add(ent, newDTO);
			
			
			newDTO.WorkingDocumentData = ent.WorkingDocumentData.ToDTO(cache);
			
			
			
			newDTO.PublishingGroupResource = ent.PublishingGroupResource.ToDTO(cache);
			
			newDTO.UserAgent = ent.UserAgent.ToDTO(cache);
			
			newDTO.Reviewee = ent.Reviewee.ToDTO(cache);
			
			newDTO.Reviewer = ent.Reviewer.ToDTO(cache);
			
			newDTO.WorkingDocumentStatu = ent.WorkingDocumentStatu.ToDTO(cache);
			
			
			
			foreach (var related in ent.ProjectJobTaskWorkingDocuments)
			{
				newDTO.ProjectJobTaskWorkingDocuments.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.PublicWorkingDocuments)
			{
				newDTO.PublicWorkingDocuments.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.WorkingDocumentPublicVariables)
			{
				newDTO.WorkingDocumentPublicVariables.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.WorkingDocumentReferences)
			{
				newDTO.WorkingDocumentReferences.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}