﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class GpsLocationConverter
	{
	
		public static GpsLocationEntity ToEntity(this GpsLocationDTO dto)
		{
			return dto.ToEntity(new GpsLocationEntity(), new Dictionary<object, object>());
		}

		public static GpsLocationEntity ToEntity(this GpsLocationDTO dto, GpsLocationEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static GpsLocationEntity ToEntity(this GpsLocationDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new GpsLocationEntity(), cache);
		}
	
		public static GpsLocationDTO ToDTO(this GpsLocationEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static GpsLocationEntity ToEntity(this GpsLocationDTO dto, GpsLocationEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (GpsLocationEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.AccuracyMetres = dto.AccuracyMetres;
			
			

			
			
			if (dto.Altitude == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)GpsLocationFieldIndex.Altitude, default(System.Decimal));
				
			}
			
			newEnt.Altitude = dto.Altitude;
			
			

			
			
			if (dto.AltitudeAccuracyMetres == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)GpsLocationFieldIndex.AltitudeAccuracyMetres, default(System.Decimal));
				
			}
			
			newEnt.AltitudeAccuracyMetres = dto.AltitudeAccuracyMetres;
			
			

			
			
			newEnt.Archived = dto.Archived;
			
			

			
			
			if (dto.FacilityStructureFloorId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)GpsLocationFieldIndex.FacilityStructureFloorId, default(System.Int32));
				
			}
			
			newEnt.FacilityStructureFloorId = dto.FacilityStructureFloorId;
			
			

			
			
			if (dto.Floor == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)GpsLocationFieldIndex.Floor, default(System.Int32));
				
			}
			
			newEnt.Floor = dto.Floor;
			
			

			
			
			newEnt.Id = dto.Id;
			
			
			

			
			
			if (dto.LastTimestampUtc == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)GpsLocationFieldIndex.LastTimestampUtc, default(System.DateTime));
				
			}
			
			newEnt.LastTimestampUtc = dto.LastTimestampUtc;
			
			

			
			
			newEnt.Latitude = dto.Latitude;
			
			

			
			
			newEnt.LocationType = dto.LocationType;
			
			

			
			
			newEnt.Longitude = dto.Longitude;
			
			

			
			
			if (dto.StaticBeaconId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)GpsLocationFieldIndex.StaticBeaconId, "");
				
			}
			
			newEnt.StaticBeaconId = dto.StaticBeaconId;
			
			

			
			
			if (dto.TabletUuid == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)GpsLocationFieldIndex.TabletUuid, "");
				
			}
			
			newEnt.TabletUuid = dto.TabletUuid;
			
			

			
			
			if (dto.TaskId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)GpsLocationFieldIndex.TaskId, default(System.Guid));
				
			}
			
			newEnt.TaskId = dto.TaskId;
			
			

			
			
			if (dto.TimestampCount == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)GpsLocationFieldIndex.TimestampCount, default(System.Int32));
				
			}
			
			newEnt.TimestampCount = dto.TimestampCount;
			
			

			
			
			if (dto.TimestampUtc == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)GpsLocationFieldIndex.TimestampUtc, default(System.DateTime));
				
			}
			
			newEnt.TimestampUtc = dto.TimestampUtc;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			
			
			foreach (var related in dto.LastGpsLocationBeacons)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.LastGpsLocationBeacons.Contains(relatedEntity))
				{
					newEnt.LastGpsLocationBeacons.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.StaticLocationBeacons)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.StaticLocationBeacons.Contains(relatedEntity))
				{
					newEnt.StaticLocationBeacons.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.TabletLocationHistories)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.TabletLocationHistories.Contains(relatedEntity))
				{
					newEnt.TabletLocationHistories.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static GpsLocationDTO ToDTO(this GpsLocationEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (GpsLocationDTO)cache[ent];
			
			var newDTO = new GpsLocationDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.AccuracyMetres = ent.AccuracyMetres;
			

			newDTO.Altitude = ent.Altitude;
			

			newDTO.AltitudeAccuracyMetres = ent.AltitudeAccuracyMetres;
			

			newDTO.Archived = ent.Archived;
			

			newDTO.FacilityStructureFloorId = ent.FacilityStructureFloorId;
			

			newDTO.Floor = ent.Floor;
			

			newDTO.Id = ent.Id;
			

			newDTO.LastTimestampUtc = ent.LastTimestampUtc;
			

			newDTO.Latitude = ent.Latitude;
			

			newDTO.LocationType = ent.LocationType;
			

			newDTO.Longitude = ent.Longitude;
			

			newDTO.StaticBeaconId = ent.StaticBeaconId;
			

			newDTO.TabletUuid = ent.TabletUuid;
			

			newDTO.TaskId = ent.TaskId;
			

			newDTO.TimestampCount = ent.TimestampCount;
			

			newDTO.TimestampUtc = ent.TimestampUtc;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			
			
			foreach (var related in ent.LastGpsLocationBeacons)
			{
				newDTO.LastGpsLocationBeacons.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.StaticLocationBeacons)
			{
				newDTO.StaticLocationBeacons.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.TabletLocationHistories)
			{
				newDTO.TabletLocationHistories.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}