﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class ResourceConverter
	{
	
		public static ResourceEntity ToEntity(this ResourceDTO dto)
		{
			return dto.ToEntity(new ResourceEntity(), new Dictionary<object, object>());
		}

		public static ResourceEntity ToEntity(this ResourceDTO dto, ResourceEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static ResourceEntity ToEntity(this ResourceDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new ResourceEntity(), cache);
		}
	
		public static ResourceDTO ToDTO(this ResourceEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static ResourceEntity ToEntity(this ResourceDTO dto, ResourceEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (ResourceEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Id = dto.Id;
			
			

			
			
			newEnt.Name = dto.Name;
			
			

			
			
			newEnt.NodeId = dto.NodeId;
			
			

			
			
			newEnt.Type = dto.Type;
			
			
			
			cache.Add(dto, newEnt);
			
			
			newEnt.ResourceData = dto.ResourceData.ToEntity(cache);
			
			
			
			
			
			foreach (var related in dto.LabelResources)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.LabelResources.Contains(relatedEntity))
				{
					newEnt.LabelResources.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.PublishingGroupResources)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.PublishingGroupResources.Contains(relatedEntity))
				{
					newEnt.PublishingGroupResources.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static ResourceDTO ToDTO(this ResourceEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (ResourceDTO)cache[ent];
			
			var newDTO = new ResourceDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Id = ent.Id;
			

			newDTO.Name = ent.Name;
			

			newDTO.NodeId = ent.NodeId;
			

			newDTO.Type = ent.Type;
			
			
			
			cache.Add(ent, newDTO);
			
			
			newDTO.ResourceData = ent.ResourceData.ToDTO(cache);
			
			
			
			
			
			foreach (var related in ent.LabelResources)
			{
				newDTO.LabelResources.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.PublishingGroupResources)
			{
				newDTO.PublishingGroupResources.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}