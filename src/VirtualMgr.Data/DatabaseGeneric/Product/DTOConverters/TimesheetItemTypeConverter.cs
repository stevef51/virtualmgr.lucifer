﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class TimesheetItemTypeConverter
	{
	
		public static TimesheetItemTypeEntity ToEntity(this TimesheetItemTypeDTO dto)
		{
			return dto.ToEntity(new TimesheetItemTypeEntity(), new Dictionary<object, object>());
		}

		public static TimesheetItemTypeEntity ToEntity(this TimesheetItemTypeDTO dto, TimesheetItemTypeEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static TimesheetItemTypeEntity ToEntity(this TimesheetItemTypeDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new TimesheetItemTypeEntity(), cache);
		}
	
		public static TimesheetItemTypeDTO ToDTO(this TimesheetItemTypeEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static TimesheetItemTypeEntity ToEntity(this TimesheetItemTypeDTO dto, TimesheetItemTypeEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (TimesheetItemTypeEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Id = dto.Id;
			
			
			

			
			
			newEnt.IsLeave = dto.IsLeave;
			
			

			
			
			newEnt.IsSaturdayPay = dto.IsSaturdayPay;
			
			

			
			
			newEnt.IsSundayPay = dto.IsSundayPay;
			
			

			
			
			newEnt.IsWeekDayPay = dto.IsWeekDayPay;
			
			

			
			
			newEnt.Name = dto.Name;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			
			
			foreach (var related in dto.TimesheetItems)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.TimesheetItems.Contains(relatedEntity))
				{
					newEnt.TimesheetItems.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.TimesheetItems_)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.TimesheetItems_.Contains(relatedEntity))
				{
					newEnt.TimesheetItems_.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.HierarchyBuckets)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.HierarchyBuckets.Contains(relatedEntity))
				{
					newEnt.HierarchyBuckets.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.HierarchyBuckets_)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.HierarchyBuckets_.Contains(relatedEntity))
				{
					newEnt.HierarchyBuckets_.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.HierarchyBuckets__)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.HierarchyBuckets__.Contains(relatedEntity))
				{
					newEnt.HierarchyBuckets__.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static TimesheetItemTypeDTO ToDTO(this TimesheetItemTypeEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (TimesheetItemTypeDTO)cache[ent];
			
			var newDTO = new TimesheetItemTypeDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Id = ent.Id;
			

			newDTO.IsLeave = ent.IsLeave;
			

			newDTO.IsSaturdayPay = ent.IsSaturdayPay;
			

			newDTO.IsSundayPay = ent.IsSundayPay;
			

			newDTO.IsWeekDayPay = ent.IsWeekDayPay;
			

			newDTO.Name = ent.Name;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			
			
			foreach (var related in ent.TimesheetItems)
			{
				newDTO.TimesheetItems.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.TimesheetItems_)
			{
				newDTO.TimesheetItems_.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.HierarchyBuckets)
			{
				newDTO.HierarchyBuckets.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.HierarchyBuckets_)
			{
				newDTO.HierarchyBuckets_.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.HierarchyBuckets__)
			{
				newDTO.HierarchyBuckets__.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}