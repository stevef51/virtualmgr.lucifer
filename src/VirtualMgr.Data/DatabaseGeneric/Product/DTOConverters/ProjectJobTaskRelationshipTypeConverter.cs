﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class ProjectJobTaskRelationshipTypeConverter
	{
	
		public static ProjectJobTaskRelationshipTypeEntity ToEntity(this ProjectJobTaskRelationshipTypeDTO dto)
		{
			return dto.ToEntity(new ProjectJobTaskRelationshipTypeEntity(), new Dictionary<object, object>());
		}

		public static ProjectJobTaskRelationshipTypeEntity ToEntity(this ProjectJobTaskRelationshipTypeDTO dto, ProjectJobTaskRelationshipTypeEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static ProjectJobTaskRelationshipTypeEntity ToEntity(this ProjectJobTaskRelationshipTypeDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new ProjectJobTaskRelationshipTypeEntity(), cache);
		}
	
		public static ProjectJobTaskRelationshipTypeDTO ToDTO(this ProjectJobTaskRelationshipTypeEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static ProjectJobTaskRelationshipTypeEntity ToEntity(this ProjectJobTaskRelationshipTypeDTO dto, ProjectJobTaskRelationshipTypeEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (ProjectJobTaskRelationshipTypeEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Id = dto.Id;
			
			
			

			
			
			newEnt.Name = dto.Name;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			
			
			foreach (var related in dto.ProjectJobTaskRelationships)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProjectJobTaskRelationships.Contains(relatedEntity))
				{
					newEnt.ProjectJobTaskRelationships.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static ProjectJobTaskRelationshipTypeDTO ToDTO(this ProjectJobTaskRelationshipTypeEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (ProjectJobTaskRelationshipTypeDTO)cache[ent];
			
			var newDTO = new ProjectJobTaskRelationshipTypeDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Id = ent.Id;
			

			newDTO.Name = ent.Name;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			
			
			foreach (var related in ent.ProjectJobTaskRelationships)
			{
				newDTO.ProjectJobTaskRelationships.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}