﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class AssetTypeCalendarTaskConverter
	{
	
		public static AssetTypeCalendarTaskEntity ToEntity(this AssetTypeCalendarTaskDTO dto)
		{
			return dto.ToEntity(new AssetTypeCalendarTaskEntity(), new Dictionary<object, object>());
		}

		public static AssetTypeCalendarTaskEntity ToEntity(this AssetTypeCalendarTaskDTO dto, AssetTypeCalendarTaskEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static AssetTypeCalendarTaskEntity ToEntity(this AssetTypeCalendarTaskDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new AssetTypeCalendarTaskEntity(), cache);
		}
	
		public static AssetTypeCalendarTaskDTO ToDTO(this AssetTypeCalendarTaskEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static AssetTypeCalendarTaskEntity ToEntity(this AssetTypeCalendarTaskDTO dto, AssetTypeCalendarTaskEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (AssetTypeCalendarTaskEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Archived = dto.Archived;
			
			

			
			
			newEnt.AssetTypeCalendarId = dto.AssetTypeCalendarId;
			
			

			
			
			newEnt.HierarchyRoleId = dto.HierarchyRoleId;
			
			

			
			
			newEnt.Id = dto.Id;
			
			

			
			
			if (dto.StartTimeLocal == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)AssetTypeCalendarTaskFieldIndex.StartTimeLocal, default(System.TimeSpan));
				
			}
			
			newEnt.StartTimeLocal = dto.StartTimeLocal;
			
			

			
			
			if (dto.TaskDescription == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)AssetTypeCalendarTaskFieldIndex.TaskDescription, "");
				
			}
			
			newEnt.TaskDescription = dto.TaskDescription;
			
			

			
			
			if (dto.TaskLengthInDays == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)AssetTypeCalendarTaskFieldIndex.TaskLengthInDays, default(System.Int32));
				
			}
			
			newEnt.TaskLengthInDays = dto.TaskLengthInDays;
			
			

			
			
			if (dto.TaskLevel == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)AssetTypeCalendarTaskFieldIndex.TaskLevel, default(System.Int32));
				
			}
			
			newEnt.TaskLevel = dto.TaskLevel;
			
			

			
			
			if (dto.TaskName == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)AssetTypeCalendarTaskFieldIndex.TaskName, "");
				
			}
			
			newEnt.TaskName = dto.TaskName;
			
			

			
			
			newEnt.TaskSiteId = dto.TaskSiteId;
			
			

			
			
			newEnt.TaskTypeId = dto.TaskTypeId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.AssetTypeCalendar = dto.AssetTypeCalendar.ToEntity(cache);
			
			newEnt.ProjectJobTaskType = dto.ProjectJobTaskType.ToEntity(cache);
			
			newEnt.HierarchyRole = dto.HierarchyRole.ToEntity(cache);
			
			
			
			foreach (var related in dto.AssetTypeCalendarTaskLabels)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.AssetTypeCalendarTaskLabels.Contains(relatedEntity))
				{
					newEnt.AssetTypeCalendarTaskLabels.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static AssetTypeCalendarTaskDTO ToDTO(this AssetTypeCalendarTaskEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (AssetTypeCalendarTaskDTO)cache[ent];
			
			var newDTO = new AssetTypeCalendarTaskDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Archived = ent.Archived;
			

			newDTO.AssetTypeCalendarId = ent.AssetTypeCalendarId;
			

			newDTO.HierarchyRoleId = ent.HierarchyRoleId;
			

			newDTO.Id = ent.Id;
			

			newDTO.StartTimeLocal = ent.StartTimeLocal;
			

			newDTO.TaskDescription = ent.TaskDescription;
			

			newDTO.TaskLengthInDays = ent.TaskLengthInDays;
			

			newDTO.TaskLevel = ent.TaskLevel;
			

			newDTO.TaskName = ent.TaskName;
			

			newDTO.TaskSiteId = ent.TaskSiteId;
			

			newDTO.TaskTypeId = ent.TaskTypeId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.AssetTypeCalendar = ent.AssetTypeCalendar.ToDTO(cache);
			
			newDTO.ProjectJobTaskType = ent.ProjectJobTaskType.ToDTO(cache);
			
			newDTO.HierarchyRole = ent.HierarchyRole.ToDTO(cache);
			
			
			
			foreach (var related in ent.AssetTypeCalendarTaskLabels)
			{
				newDTO.AssetTypeCalendarTaskLabels.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}