﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class CompletedLabelWorkingDocumentPresentedDimensionConverter
	{
	
		public static CompletedLabelWorkingDocumentPresentedDimensionEntity ToEntity(this CompletedLabelWorkingDocumentPresentedDimensionDTO dto)
		{
			return dto.ToEntity(new CompletedLabelWorkingDocumentPresentedDimensionEntity(), new Dictionary<object, object>());
		}

		public static CompletedLabelWorkingDocumentPresentedDimensionEntity ToEntity(this CompletedLabelWorkingDocumentPresentedDimensionDTO dto, CompletedLabelWorkingDocumentPresentedDimensionEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static CompletedLabelWorkingDocumentPresentedDimensionEntity ToEntity(this CompletedLabelWorkingDocumentPresentedDimensionDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new CompletedLabelWorkingDocumentPresentedDimensionEntity(), cache);
		}
	
		public static CompletedLabelWorkingDocumentPresentedDimensionDTO ToDTO(this CompletedLabelWorkingDocumentPresentedDimensionEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static CompletedLabelWorkingDocumentPresentedDimensionEntity ToEntity(this CompletedLabelWorkingDocumentPresentedDimensionDTO dto, CompletedLabelWorkingDocumentPresentedDimensionEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (CompletedLabelWorkingDocumentPresentedDimensionEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.CompletedLabelDimensionId = dto.CompletedLabelDimensionId;
			
			

			
			
			newEnt.CompletedWorkingDocumentPresentedFactId = dto.CompletedWorkingDocumentPresentedFactId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.CompletedLabelDimension = dto.CompletedLabelDimension.ToEntity(cache);
			
			newEnt.CompletedWorkingDocumentPresentedFact = dto.CompletedWorkingDocumentPresentedFact.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static CompletedLabelWorkingDocumentPresentedDimensionDTO ToDTO(this CompletedLabelWorkingDocumentPresentedDimensionEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (CompletedLabelWorkingDocumentPresentedDimensionDTO)cache[ent];
			
			var newDTO = new CompletedLabelWorkingDocumentPresentedDimensionDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.CompletedLabelDimensionId = ent.CompletedLabelDimensionId;
			

			newDTO.CompletedWorkingDocumentPresentedFactId = ent.CompletedWorkingDocumentPresentedFactId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.CompletedLabelDimension = ent.CompletedLabelDimension.ToDTO(cache);
			
			newDTO.CompletedWorkingDocumentPresentedFact = ent.CompletedWorkingDocumentPresentedFact.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}