﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class AssetTypeContextPublishedResourceConverter
	{
	
		public static AssetTypeContextPublishedResourceEntity ToEntity(this AssetTypeContextPublishedResourceDTO dto)
		{
			return dto.ToEntity(new AssetTypeContextPublishedResourceEntity(), new Dictionary<object, object>());
		}

		public static AssetTypeContextPublishedResourceEntity ToEntity(this AssetTypeContextPublishedResourceDTO dto, AssetTypeContextPublishedResourceEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static AssetTypeContextPublishedResourceEntity ToEntity(this AssetTypeContextPublishedResourceDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new AssetTypeContextPublishedResourceEntity(), cache);
		}
	
		public static AssetTypeContextPublishedResourceDTO ToDTO(this AssetTypeContextPublishedResourceEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static AssetTypeContextPublishedResourceEntity ToEntity(this AssetTypeContextPublishedResourceDTO dto, AssetTypeContextPublishedResourceEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (AssetTypeContextPublishedResourceEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.AssetTypeId = dto.AssetTypeId;
			
			

			
			
			newEnt.Id = dto.Id;
			
			

			
			
			newEnt.PublishingGroupResourceId = dto.PublishingGroupResourceId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.AssetType = dto.AssetType.ToEntity(cache);
			
			newEnt.PublishingGroupResource = dto.PublishingGroupResource.ToEntity(cache);
			
			
			
			foreach (var related in dto.AssetContextDatas)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.AssetContextDatas.Contains(relatedEntity))
				{
					newEnt.AssetContextDatas.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static AssetTypeContextPublishedResourceDTO ToDTO(this AssetTypeContextPublishedResourceEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (AssetTypeContextPublishedResourceDTO)cache[ent];
			
			var newDTO = new AssetTypeContextPublishedResourceDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.AssetTypeId = ent.AssetTypeId;
			

			newDTO.Id = ent.Id;
			

			newDTO.PublishingGroupResourceId = ent.PublishingGroupResourceId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.AssetType = ent.AssetType.ToDTO(cache);
			
			newDTO.PublishingGroupResource = ent.PublishingGroupResource.ToDTO(cache);
			
			
			
			foreach (var related in ent.AssetContextDatas)
			{
				newDTO.AssetContextDatas.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}