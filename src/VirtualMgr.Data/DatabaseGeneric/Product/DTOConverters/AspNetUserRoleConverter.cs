﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class AspNetUserRoleConverter
	{
	
		public static AspNetUserRoleEntity ToEntity(this AspNetUserRoleDTO dto)
		{
			return dto.ToEntity(new AspNetUserRoleEntity(), new Dictionary<object, object>());
		}

		public static AspNetUserRoleEntity ToEntity(this AspNetUserRoleDTO dto, AspNetUserRoleEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static AspNetUserRoleEntity ToEntity(this AspNetUserRoleDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new AspNetUserRoleEntity(), cache);
		}
	
		public static AspNetUserRoleDTO ToDTO(this AspNetUserRoleEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static AspNetUserRoleEntity ToEntity(this AspNetUserRoleDTO dto, AspNetUserRoleEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (AspNetUserRoleEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.RoleId = dto.RoleId;
			
			

			
			
			newEnt.UserId = dto.UserId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.AspNetRole = dto.AspNetRole.ToEntity(cache);
			
			newEnt.AspNetUser = dto.AspNetUser.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static AspNetUserRoleDTO ToDTO(this AspNetUserRoleEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (AspNetUserRoleDTO)cache[ent];
			
			var newDTO = new AspNetUserRoleDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.RoleId = ent.RoleId;
			

			newDTO.UserId = ent.UserId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.AspNetRole = ent.AspNetRole.ToDTO(cache);
			
			newDTO.AspNetUser = ent.AspNetUser.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}