﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class LabelResourceConverter
	{
	
		public static LabelResourceEntity ToEntity(this LabelResourceDTO dto)
		{
			return dto.ToEntity(new LabelResourceEntity(), new Dictionary<object, object>());
		}

		public static LabelResourceEntity ToEntity(this LabelResourceDTO dto, LabelResourceEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static LabelResourceEntity ToEntity(this LabelResourceDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new LabelResourceEntity(), cache);
		}
	
		public static LabelResourceDTO ToDTO(this LabelResourceEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static LabelResourceEntity ToEntity(this LabelResourceDTO dto, LabelResourceEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (LabelResourceEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.LabelId = dto.LabelId;
			
			

			
			
			newEnt.ResourceId = dto.ResourceId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.Label = dto.Label.ToEntity(cache);
			
			newEnt.Resource = dto.Resource.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static LabelResourceDTO ToDTO(this LabelResourceEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (LabelResourceDTO)cache[ent];
			
			var newDTO = new LabelResourceDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.LabelId = ent.LabelId;
			

			newDTO.ResourceId = ent.ResourceId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.Label = ent.Label.ToDTO(cache);
			
			newDTO.Resource = ent.Resource.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}