﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class CalendarEventConverter
	{
	
		public static CalendarEventEntity ToEntity(this CalendarEventDTO dto)
		{
			return dto.ToEntity(new CalendarEventEntity(), new Dictionary<object, object>());
		}

		public static CalendarEventEntity ToEntity(this CalendarEventDTO dto, CalendarEventEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static CalendarEventEntity ToEntity(this CalendarEventDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new CalendarEventEntity(), cache);
		}
	
		public static CalendarEventDTO ToDTO(this CalendarEventEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static CalendarEventEntity ToEntity(this CalendarEventDTO dto, CalendarEventEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (CalendarEventEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.CalendarRuleId = dto.CalendarRuleId;
			
			

			
			
			if (dto.Description == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)CalendarEventFieldIndex.Description, "");
				
			}
			
			newEnt.Description = dto.Description;
			
			

			
			
			newEnt.Id = dto.Id;
			
			
			

			
			
			newEnt.Title = dto.Title;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.CalendarRule = dto.CalendarRule.ToEntity(cache);
			
			
			
			foreach (var related in dto.PublishingGroupResourceCalendars)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.PublishingGroupResourceCalendars.Contains(relatedEntity))
				{
					newEnt.PublishingGroupResourceCalendars.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static CalendarEventDTO ToDTO(this CalendarEventEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (CalendarEventDTO)cache[ent];
			
			var newDTO = new CalendarEventDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.CalendarRuleId = ent.CalendarRuleId;
			

			newDTO.Description = ent.Description;
			

			newDTO.Id = ent.Id;
			

			newDTO.Title = ent.Title;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.CalendarRule = ent.CalendarRule.ToDTO(cache);
			
			
			
			foreach (var related in ent.PublishingGroupResourceCalendars)
			{
				newDTO.PublishingGroupResourceCalendars.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}