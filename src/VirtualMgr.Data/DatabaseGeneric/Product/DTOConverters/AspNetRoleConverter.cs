﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class AspNetRoleConverter
	{
	
		public static AspNetRoleEntity ToEntity(this AspNetRoleDTO dto)
		{
			return dto.ToEntity(new AspNetRoleEntity(), new Dictionary<object, object>());
		}

		public static AspNetRoleEntity ToEntity(this AspNetRoleDTO dto, AspNetRoleEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static AspNetRoleEntity ToEntity(this AspNetRoleDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new AspNetRoleEntity(), cache);
		}
	
		public static AspNetRoleDTO ToDTO(this AspNetRoleEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static AspNetRoleEntity ToEntity(this AspNetRoleDTO dto, AspNetRoleEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (AspNetRoleEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			if (dto.ConcurrencyStamp == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)AspNetRoleFieldIndex.ConcurrencyStamp, "");
				
			}
			
			newEnt.ConcurrencyStamp = dto.ConcurrencyStamp;
			
			

			
			
			newEnt.Id = dto.Id;
			
			

			
			
			if (dto.Name == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)AspNetRoleFieldIndex.Name, "");
				
			}
			
			newEnt.Name = dto.Name;
			
			

			
			
			if (dto.NormalizedName == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)AspNetRoleFieldIndex.NormalizedName, "");
				
			}
			
			newEnt.NormalizedName = dto.NormalizedName;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			
			
			foreach (var related in dto.AspNetRoleClaims)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.AspNetRoleClaims.Contains(relatedEntity))
				{
					newEnt.AspNetRoleClaims.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.AspNetUserRoles)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.AspNetUserRoles.Contains(relatedEntity))
				{
					newEnt.AspNetUserRoles.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.MediaFolderRoles)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.MediaFolderRoles.Contains(relatedEntity))
				{
					newEnt.MediaFolderRoles.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.MediaRoles)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.MediaRoles.Contains(relatedEntity))
				{
					newEnt.MediaRoles.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ReportRoles)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ReportRoles.Contains(relatedEntity))
				{
					newEnt.ReportRoles.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.UserTypeDefaultRoles)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.UserTypeDefaultRoles.Contains(relatedEntity))
				{
					newEnt.UserTypeDefaultRoles.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static AspNetRoleDTO ToDTO(this AspNetRoleEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (AspNetRoleDTO)cache[ent];
			
			var newDTO = new AspNetRoleDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.ConcurrencyStamp = ent.ConcurrencyStamp;
			

			newDTO.Id = ent.Id;
			

			newDTO.Name = ent.Name;
			

			newDTO.NormalizedName = ent.NormalizedName;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			
			
			foreach (var related in ent.AspNetRoleClaims)
			{
				newDTO.AspNetRoleClaims.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.AspNetUserRoles)
			{
				newDTO.AspNetUserRoles.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.MediaFolderRoles)
			{
				newDTO.MediaFolderRoles.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.MediaRoles)
			{
				newDTO.MediaRoles.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ReportRoles)
			{
				newDTO.ReportRoles.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.UserTypeDefaultRoles)
			{
				newDTO.UserTypeDefaultRoles.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}