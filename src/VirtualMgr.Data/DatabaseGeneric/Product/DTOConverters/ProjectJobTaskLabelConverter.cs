﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class ProjectJobTaskLabelConverter
	{
	
		public static ProjectJobTaskLabelEntity ToEntity(this ProjectJobTaskLabelDTO dto)
		{
			return dto.ToEntity(new ProjectJobTaskLabelEntity(), new Dictionary<object, object>());
		}

		public static ProjectJobTaskLabelEntity ToEntity(this ProjectJobTaskLabelDTO dto, ProjectJobTaskLabelEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static ProjectJobTaskLabelEntity ToEntity(this ProjectJobTaskLabelDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new ProjectJobTaskLabelEntity(), cache);
		}
	
		public static ProjectJobTaskLabelDTO ToDTO(this ProjectJobTaskLabelEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static ProjectJobTaskLabelEntity ToEntity(this ProjectJobTaskLabelDTO dto, ProjectJobTaskLabelEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (ProjectJobTaskLabelEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.LabelId = dto.LabelId;
			
			

			
			
			newEnt.ProjectJobTaskId = dto.ProjectJobTaskId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.ProjectJobTask = dto.ProjectJobTask.ToEntity(cache);
			
			newEnt.Label = dto.Label.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static ProjectJobTaskLabelDTO ToDTO(this ProjectJobTaskLabelEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (ProjectJobTaskLabelDTO)cache[ent];
			
			var newDTO = new ProjectJobTaskLabelDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.LabelId = ent.LabelId;
			

			newDTO.ProjectJobTaskId = ent.ProjectJobTaskId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.ProjectJobTask = ent.ProjectJobTask.ToDTO(cache);
			
			newDTO.Label = ent.Label.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}