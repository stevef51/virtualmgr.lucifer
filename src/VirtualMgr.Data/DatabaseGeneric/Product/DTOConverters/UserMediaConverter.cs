﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class UserMediaConverter
	{
	
		public static UserMediaEntity ToEntity(this UserMediaDTO dto)
		{
			return dto.ToEntity(new UserMediaEntity(), new Dictionary<object, object>());
		}

		public static UserMediaEntity ToEntity(this UserMediaDTO dto, UserMediaEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static UserMediaEntity ToEntity(this UserMediaDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new UserMediaEntity(), cache);
		}
	
		public static UserMediaDTO ToDTO(this UserMediaEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static UserMediaEntity ToEntity(this UserMediaDTO dto, UserMediaEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (UserMediaEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.DateCreated = dto.DateCreated;
			
			

			
			
			newEnt.MediaId = dto.MediaId;
			
			

			
			
			newEnt.UserId = dto.UserId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.Medium = dto.Medium.ToEntity(cache);
			
			newEnt.UserData = dto.UserData.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static UserMediaDTO ToDTO(this UserMediaEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (UserMediaDTO)cache[ent];
			
			var newDTO = new UserMediaDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.DateCreated = ent.DateCreated;
			

			newDTO.MediaId = ent.MediaId;
			

			newDTO.UserId = ent.UserId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.Medium = ent.Medium.ToDTO(cache);
			
			newDTO.UserData = ent.UserData.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}