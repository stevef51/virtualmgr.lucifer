﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class ProjectJobScheduledTaskAttachmentConverter
	{
	
		public static ProjectJobScheduledTaskAttachmentEntity ToEntity(this ProjectJobScheduledTaskAttachmentDTO dto)
		{
			return dto.ToEntity(new ProjectJobScheduledTaskAttachmentEntity(), new Dictionary<object, object>());
		}

		public static ProjectJobScheduledTaskAttachmentEntity ToEntity(this ProjectJobScheduledTaskAttachmentDTO dto, ProjectJobScheduledTaskAttachmentEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static ProjectJobScheduledTaskAttachmentEntity ToEntity(this ProjectJobScheduledTaskAttachmentDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new ProjectJobScheduledTaskAttachmentEntity(), cache);
		}
	
		public static ProjectJobScheduledTaskAttachmentDTO ToDTO(this ProjectJobScheduledTaskAttachmentEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static ProjectJobScheduledTaskAttachmentEntity ToEntity(this ProjectJobScheduledTaskAttachmentDTO dto, ProjectJobScheduledTaskAttachmentEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (ProjectJobScheduledTaskAttachmentEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.MediaId = dto.MediaId;
			
			

			
			
			newEnt.ProjectJobScheduledTaskId = dto.ProjectJobScheduledTaskId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.ProjectJobScheduledTask = dto.ProjectJobScheduledTask.ToEntity(cache);
			
			newEnt.Medium = dto.Medium.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static ProjectJobScheduledTaskAttachmentDTO ToDTO(this ProjectJobScheduledTaskAttachmentEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (ProjectJobScheduledTaskAttachmentDTO)cache[ent];
			
			var newDTO = new ProjectJobScheduledTaskAttachmentDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.MediaId = ent.MediaId;
			

			newDTO.ProjectJobScheduledTaskId = ent.ProjectJobScheduledTaskId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.ProjectJobScheduledTask = ent.ProjectJobScheduledTask.ToDTO(cache);
			
			newDTO.Medium = ent.Medium.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}