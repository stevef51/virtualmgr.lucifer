﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class GlobalSettingConverter
	{
	
		public static GlobalSettingEntity ToEntity(this GlobalSettingDTO dto)
		{
			return dto.ToEntity(new GlobalSettingEntity(), new Dictionary<object, object>());
		}

		public static GlobalSettingEntity ToEntity(this GlobalSettingDTO dto, GlobalSettingEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static GlobalSettingEntity ToEntity(this GlobalSettingDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new GlobalSettingEntity(), cache);
		}
	
		public static GlobalSettingDTO ToDTO(this GlobalSettingEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static GlobalSettingEntity ToEntity(this GlobalSettingDTO dto, GlobalSettingEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (GlobalSettingEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Hidden = dto.Hidden;
			
			

			
			
			newEnt.Id = dto.Id;
			
			
			

			
			
			newEnt.Name = dto.Name;
			
			

			
			
			if (dto.Value == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)GlobalSettingFieldIndex.Value, "");
				
			}
			
			newEnt.Value = dto.Value;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static GlobalSettingDTO ToDTO(this GlobalSettingEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (GlobalSettingDTO)cache[ent];
			
			var newDTO = new GlobalSettingDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Hidden = ent.Hidden;
			

			newDTO.Id = ent.Id;
			

			newDTO.Name = ent.Name;
			

			newDTO.Value = ent.Value;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}