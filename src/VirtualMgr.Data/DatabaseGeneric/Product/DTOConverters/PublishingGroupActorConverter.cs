﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class PublishingGroupActorConverter
	{
	
		public static PublishingGroupActorEntity ToEntity(this PublishingGroupActorDTO dto)
		{
			return dto.ToEntity(new PublishingGroupActorEntity(), new Dictionary<object, object>());
		}

		public static PublishingGroupActorEntity ToEntity(this PublishingGroupActorDTO dto, PublishingGroupActorEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static PublishingGroupActorEntity ToEntity(this PublishingGroupActorDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new PublishingGroupActorEntity(), cache);
		}
	
		public static PublishingGroupActorDTO ToDTO(this PublishingGroupActorEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static PublishingGroupActorEntity ToEntity(this PublishingGroupActorDTO dto, PublishingGroupActorEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (PublishingGroupActorEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.PublishingGroupId = dto.PublishingGroupId;
			
			

			
			
			newEnt.PublishingTypeId = dto.PublishingTypeId;
			
			

			
			
			newEnt.UserId = dto.UserId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.PublishingGroup = dto.PublishingGroup.ToEntity(cache);
			
			newEnt.PublishingGroupActorType = dto.PublishingGroupActorType.ToEntity(cache);
			
			newEnt.UserData = dto.UserData.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static PublishingGroupActorDTO ToDTO(this PublishingGroupActorEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (PublishingGroupActorDTO)cache[ent];
			
			var newDTO = new PublishingGroupActorDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.PublishingGroupId = ent.PublishingGroupId;
			

			newDTO.PublishingTypeId = ent.PublishingTypeId;
			

			newDTO.UserId = ent.UserId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.PublishingGroup = ent.PublishingGroup.ToDTO(cache);
			
			newDTO.PublishingGroupActorType = ent.PublishingGroupActorType.ToDTO(cache);
			
			newDTO.UserData = ent.UserData.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}