﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class CompletedWorkingDocumentFactConverter
	{
	
		public static CompletedWorkingDocumentFactEntity ToEntity(this CompletedWorkingDocumentFactDTO dto)
		{
			return dto.ToEntity(new CompletedWorkingDocumentFactEntity(), new Dictionary<object, object>());
		}

		public static CompletedWorkingDocumentFactEntity ToEntity(this CompletedWorkingDocumentFactDTO dto, CompletedWorkingDocumentFactEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static CompletedWorkingDocumentFactEntity ToEntity(this CompletedWorkingDocumentFactDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new CompletedWorkingDocumentFactEntity(), cache);
		}
	
		public static CompletedWorkingDocumentFactDTO ToDTO(this CompletedWorkingDocumentFactEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static CompletedWorkingDocumentFactEntity ToEntity(this CompletedWorkingDocumentFactDTO dto, CompletedWorkingDocumentFactEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (CompletedWorkingDocumentFactEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			if (dto.ClientIp == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)CompletedWorkingDocumentFactFieldIndex.ClientIp, "");
				
			}
			
			newEnt.ClientIp = dto.ClientIp;
			
			

			
			
			newEnt.ContextType = dto.ContextType;
			
			

			
			
			newEnt.DateCompleted = dto.DateCompleted;
			
			

			
			
			newEnt.DateStarted = dto.DateStarted;
			
			

			
			
			newEnt.FailedQuestionCount = dto.FailedQuestionCount;
			
			

			
			
			newEnt.IsUserContext = dto.IsUserContext;
			
			

			
			
			newEnt.NaquestionCount = dto.NaquestionCount;
			
			

			
			
			if (dto.ParentWorkingDocumentId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)CompletedWorkingDocumentFactFieldIndex.ParentWorkingDocumentId, default(System.Guid));
				
			}
			
			newEnt.ParentWorkingDocumentId = dto.ParentWorkingDocumentId;
			
			

			
			
			newEnt.PassedQuestionCount = dto.PassedQuestionCount;
			
			

			
			
			newEnt.ReportingUserRevieweeId = dto.ReportingUserRevieweeId;
			
			

			
			
			newEnt.ReportingUserReviewerId = dto.ReportingUserReviewerId;
			
			

			
			
			newEnt.ReportingWorkingDocumentId = dto.ReportingWorkingDocumentId;
			
			

			
			
			newEnt.TotalPossibleScore = dto.TotalPossibleScore;
			
			

			
			
			newEnt.TotalQuestionCount = dto.TotalQuestionCount;
			
			

			
			
			newEnt.TotalScore = dto.TotalScore;
			
			

			
			
			if (dto.UserAgentId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)CompletedWorkingDocumentFactFieldIndex.UserAgentId, default(System.Int32));
				
			}
			
			newEnt.UserAgentId = dto.UserAgentId;
			
			

			
			
			newEnt.WorkingDocumentId = dto.WorkingDocumentId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.RevieweeUser = dto.RevieweeUser.ToEntity(cache);
			
			newEnt.ReviewerUser = dto.ReviewerUser.ToEntity(cache);
			
			newEnt.CompletedWorkingDocumentDimension = dto.CompletedWorkingDocumentDimension.ToEntity(cache);
			
			
			
			foreach (var related in dto.AssetContextDatas)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.AssetContextDatas.Contains(relatedEntity))
				{
					newEnt.AssetContextDatas.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.CompletedLabelWorkingDocumentDimensions)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.CompletedLabelWorkingDocumentDimensions.Contains(relatedEntity))
				{
					newEnt.CompletedLabelWorkingDocumentDimensions.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.CompletedWorkingDocumentPresentedFacts)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.CompletedWorkingDocumentPresentedFacts.Contains(relatedEntity))
				{
					newEnt.CompletedWorkingDocumentPresentedFacts.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.UserContextDatas)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.UserContextDatas.Contains(relatedEntity))
				{
					newEnt.UserContextDatas.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static CompletedWorkingDocumentFactDTO ToDTO(this CompletedWorkingDocumentFactEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (CompletedWorkingDocumentFactDTO)cache[ent];
			
			var newDTO = new CompletedWorkingDocumentFactDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.ClientIp = ent.ClientIp;
			

			newDTO.ContextType = ent.ContextType;
			

			newDTO.DateCompleted = ent.DateCompleted;
			

			newDTO.DateStarted = ent.DateStarted;
			

			newDTO.FailedQuestionCount = ent.FailedQuestionCount;
			

			newDTO.IsUserContext = ent.IsUserContext;
			

			newDTO.NaquestionCount = ent.NaquestionCount;
			

			newDTO.ParentWorkingDocumentId = ent.ParentWorkingDocumentId;
			

			newDTO.PassedQuestionCount = ent.PassedQuestionCount;
			

			newDTO.ReportingUserRevieweeId = ent.ReportingUserRevieweeId;
			

			newDTO.ReportingUserReviewerId = ent.ReportingUserReviewerId;
			

			newDTO.ReportingWorkingDocumentId = ent.ReportingWorkingDocumentId;
			

			newDTO.TotalPossibleScore = ent.TotalPossibleScore;
			

			newDTO.TotalQuestionCount = ent.TotalQuestionCount;
			

			newDTO.TotalScore = ent.TotalScore;
			

			newDTO.UserAgentId = ent.UserAgentId;
			

			newDTO.WorkingDocumentId = ent.WorkingDocumentId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.RevieweeUser = ent.RevieweeUser.ToDTO(cache);
			
			newDTO.ReviewerUser = ent.ReviewerUser.ToDTO(cache);
			
			newDTO.CompletedWorkingDocumentDimension = ent.CompletedWorkingDocumentDimension.ToDTO(cache);
			
			
			
			foreach (var related in ent.AssetContextDatas)
			{
				newDTO.AssetContextDatas.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.CompletedLabelWorkingDocumentDimensions)
			{
				newDTO.CompletedLabelWorkingDocumentDimensions.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.CompletedWorkingDocumentPresentedFacts)
			{
				newDTO.CompletedWorkingDocumentPresentedFacts.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.UserContextDatas)
			{
				newDTO.UserContextDatas.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}