﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class WorkingDocumentDataConverter
	{
	
		public static WorkingDocumentDataEntity ToEntity(this WorkingDocumentDataDTO dto)
		{
			return dto.ToEntity(new WorkingDocumentDataEntity(), new Dictionary<object, object>());
		}

		public static WorkingDocumentDataEntity ToEntity(this WorkingDocumentDataDTO dto, WorkingDocumentDataEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static WorkingDocumentDataEntity ToEntity(this WorkingDocumentDataDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new WorkingDocumentDataEntity(), cache);
		}
	
		public static WorkingDocumentDataDTO ToDTO(this WorkingDocumentDataEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static WorkingDocumentDataEntity ToEntity(this WorkingDocumentDataDTO dto, WorkingDocumentDataEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (WorkingDocumentDataEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Data = dto.Data;
			
			

			
			
			newEnt.UtcLastModified = dto.UtcLastModified;
			
			

			
			
			newEnt.WorkingDocumentId = dto.WorkingDocumentId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			newEnt.WorkingDocument = dto.WorkingDocument.ToEntity(cache);
			
			
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static WorkingDocumentDataDTO ToDTO(this WorkingDocumentDataEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (WorkingDocumentDataDTO)cache[ent];
			
			var newDTO = new WorkingDocumentDataDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Data = ent.Data;
			

			newDTO.UtcLastModified = ent.UtcLastModified;
			

			newDTO.WorkingDocumentId = ent.WorkingDocumentId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			newDTO.WorkingDocument = ent.WorkingDocument.ToDTO(cache);
			
			
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}