﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class ReportConverter
	{
	
		public static ReportEntity ToEntity(this ReportDTO dto)
		{
			return dto.ToEntity(new ReportEntity(), new Dictionary<object, object>());
		}

		public static ReportEntity ToEntity(this ReportDTO dto, ReportEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static ReportEntity ToEntity(this ReportDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new ReportEntity(), cache);
		}
	
		public static ReportDTO ToDTO(this ReportEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static ReportEntity ToEntity(this ReportDTO dto, ReportEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (ReportEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			if (dto.Configuration == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ReportFieldIndex.Configuration, "");
				
			}
			
			newEnt.Configuration = dto.Configuration;
			
			

			
			
			if (dto.Configuration_ == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ReportFieldIndex.Configuration_, "");
				
			}
			
			newEnt.Configuration_ = dto.Configuration_;
			
			

			
			
			if (dto.Description == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ReportFieldIndex.Description, "");
				
			}
			
			newEnt.Description = dto.Description;
			
			

			
			
			newEnt.Id = dto.Id;
			
			

			
			
			newEnt.Name = dto.Name;
			
			

			
			
			newEnt.Type = dto.Type;
			
			
			
			cache.Add(dto, newEnt);
			
			
			newEnt.ReportData = dto.ReportData.ToEntity(cache);
			
			
			
			
			
			foreach (var related in dto.ReportRoles)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ReportRoles.Contains(relatedEntity))
				{
					newEnt.ReportRoles.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ReportTickets)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ReportTickets.Contains(relatedEntity))
				{
					newEnt.ReportTickets.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static ReportDTO ToDTO(this ReportEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (ReportDTO)cache[ent];
			
			var newDTO = new ReportDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Configuration = ent.Configuration;
			

			newDTO.Configuration_ = ent.Configuration_;
			

			newDTO.Description = ent.Description;
			

			newDTO.Id = ent.Id;
			

			newDTO.Name = ent.Name;
			

			newDTO.Type = ent.Type;
			
			
			
			cache.Add(ent, newDTO);
			
			
			newDTO.ReportData = ent.ReportData.ToDTO(cache);
			
			
			
			
			
			foreach (var related in ent.ReportRoles)
			{
				newDTO.ReportRoles.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ReportTickets)
			{
				newDTO.ReportTickets.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}