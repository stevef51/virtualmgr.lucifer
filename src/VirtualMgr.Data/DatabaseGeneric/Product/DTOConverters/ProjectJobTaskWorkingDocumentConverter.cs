﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class ProjectJobTaskWorkingDocumentConverter
	{
	
		public static ProjectJobTaskWorkingDocumentEntity ToEntity(this ProjectJobTaskWorkingDocumentDTO dto)
		{
			return dto.ToEntity(new ProjectJobTaskWorkingDocumentEntity(), new Dictionary<object, object>());
		}

		public static ProjectJobTaskWorkingDocumentEntity ToEntity(this ProjectJobTaskWorkingDocumentDTO dto, ProjectJobTaskWorkingDocumentEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static ProjectJobTaskWorkingDocumentEntity ToEntity(this ProjectJobTaskWorkingDocumentDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new ProjectJobTaskWorkingDocumentEntity(), cache);
		}
	
		public static ProjectJobTaskWorkingDocumentDTO ToDTO(this ProjectJobTaskWorkingDocumentEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static ProjectJobTaskWorkingDocumentEntity ToEntity(this ProjectJobTaskWorkingDocumentDTO dto, ProjectJobTaskWorkingDocumentEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (ProjectJobTaskWorkingDocumentEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.ProjectJobTaskId = dto.ProjectJobTaskId;
			
			

			
			
			newEnt.PublishingGroupResourceId = dto.PublishingGroupResourceId;
			
			

			
			
			newEnt.WorkingDocumentId = dto.WorkingDocumentId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.ProjectJobTask = dto.ProjectJobTask.ToEntity(cache);
			
			newEnt.PublishingGroupResource = dto.PublishingGroupResource.ToEntity(cache);
			
			newEnt.WorkingDocument = dto.WorkingDocument.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static ProjectJobTaskWorkingDocumentDTO ToDTO(this ProjectJobTaskWorkingDocumentEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (ProjectJobTaskWorkingDocumentDTO)cache[ent];
			
			var newDTO = new ProjectJobTaskWorkingDocumentDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.ProjectJobTaskId = ent.ProjectJobTaskId;
			

			newDTO.PublishingGroupResourceId = ent.PublishingGroupResourceId;
			

			newDTO.WorkingDocumentId = ent.WorkingDocumentId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.ProjectJobTask = ent.ProjectJobTask.ToDTO(cache);
			
			newDTO.PublishingGroupResource = ent.PublishingGroupResource.ToDTO(cache);
			
			newDTO.WorkingDocument = ent.WorkingDocument.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}