﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class CompletedLabelWorkingDocumentDimensionConverter
	{
	
		public static CompletedLabelWorkingDocumentDimensionEntity ToEntity(this CompletedLabelWorkingDocumentDimensionDTO dto)
		{
			return dto.ToEntity(new CompletedLabelWorkingDocumentDimensionEntity(), new Dictionary<object, object>());
		}

		public static CompletedLabelWorkingDocumentDimensionEntity ToEntity(this CompletedLabelWorkingDocumentDimensionDTO dto, CompletedLabelWorkingDocumentDimensionEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static CompletedLabelWorkingDocumentDimensionEntity ToEntity(this CompletedLabelWorkingDocumentDimensionDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new CompletedLabelWorkingDocumentDimensionEntity(), cache);
		}
	
		public static CompletedLabelWorkingDocumentDimensionDTO ToDTO(this CompletedLabelWorkingDocumentDimensionEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static CompletedLabelWorkingDocumentDimensionEntity ToEntity(this CompletedLabelWorkingDocumentDimensionDTO dto, CompletedLabelWorkingDocumentDimensionEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (CompletedLabelWorkingDocumentDimensionEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.CompletedLabelDimensionId = dto.CompletedLabelDimensionId;
			
			

			
			
			newEnt.CompletedWorkingDocumentFactId = dto.CompletedWorkingDocumentFactId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.CompletedLabelDimension = dto.CompletedLabelDimension.ToEntity(cache);
			
			newEnt.CompletedWorkingDocumentFact = dto.CompletedWorkingDocumentFact.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static CompletedLabelWorkingDocumentDimensionDTO ToDTO(this CompletedLabelWorkingDocumentDimensionEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (CompletedLabelWorkingDocumentDimensionDTO)cache[ent];
			
			var newDTO = new CompletedLabelWorkingDocumentDimensionDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.CompletedLabelDimensionId = ent.CompletedLabelDimensionId;
			

			newDTO.CompletedWorkingDocumentFactId = ent.CompletedWorkingDocumentFactId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.CompletedLabelDimension = ent.CompletedLabelDimension.ToDTO(cache);
			
			newDTO.CompletedWorkingDocumentFact = ent.CompletedWorkingDocumentFact.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}