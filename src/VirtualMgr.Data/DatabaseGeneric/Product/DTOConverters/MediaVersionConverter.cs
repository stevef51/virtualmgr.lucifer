﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class MediaVersionConverter
	{
	
		public static MediaVersionEntity ToEntity(this MediaVersionDTO dto)
		{
			return dto.ToEntity(new MediaVersionEntity(), new Dictionary<object, object>());
		}

		public static MediaVersionEntity ToEntity(this MediaVersionDTO dto, MediaVersionEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static MediaVersionEntity ToEntity(this MediaVersionDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new MediaVersionEntity(), cache);
		}
	
		public static MediaVersionDTO ToDTO(this MediaVersionEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static MediaVersionEntity ToEntity(this MediaVersionDTO dto, MediaVersionEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (MediaVersionEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.DateVersionClosed = dto.DateVersionClosed;
			
			

			
			
			newEnt.DateVersionCreated = dto.DateVersionCreated;
			
			

			
			
			if (dto.Description == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)MediaVersionFieldIndex.Description, "");
				
			}
			
			newEnt.Description = dto.Description;
			
			

			
			
			newEnt.Filename = dto.Filename;
			
			

			
			
			newEnt.Id = dto.Id;
			
			

			
			
			newEnt.MediaId = dto.MediaId;
			
			

			
			
			newEnt.Name = dto.Name;
			
			

			
			
			newEnt.Version = dto.Version;
			
			

			
			
			newEnt.VersionCreatedBy = dto.VersionCreatedBy;
			
			

			
			
			newEnt.VersionReplacedBy = dto.VersionReplacedBy;
			
			
			
			cache.Add(dto, newEnt);
			
			
			newEnt.MediaVersionData = dto.MediaVersionData.ToEntity(cache);
			
			
			
			newEnt.Media = dto.Media.ToEntity(cache);
			
			newEnt.VersionCreatedByUser = dto.VersionCreatedByUser.ToEntity(cache);
			
			newEnt.VersionReplacedByUser = dto.VersionReplacedByUser.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static MediaVersionDTO ToDTO(this MediaVersionEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (MediaVersionDTO)cache[ent];
			
			var newDTO = new MediaVersionDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.DateVersionClosed = ent.DateVersionClosed;
			

			newDTO.DateVersionCreated = ent.DateVersionCreated;
			

			newDTO.Description = ent.Description;
			

			newDTO.Filename = ent.Filename;
			

			newDTO.Id = ent.Id;
			

			newDTO.MediaId = ent.MediaId;
			

			newDTO.Name = ent.Name;
			

			newDTO.Version = ent.Version;
			

			newDTO.VersionCreatedBy = ent.VersionCreatedBy;
			

			newDTO.VersionReplacedBy = ent.VersionReplacedBy;
			
			
			
			cache.Add(ent, newDTO);
			
			
			newDTO.MediaVersionData = ent.MediaVersionData.ToDTO(cache);
			
			
			
			newDTO.Media = ent.Media.ToDTO(cache);
			
			newDTO.VersionCreatedByUser = ent.VersionCreatedByUser.ToDTO(cache);
			
			newDTO.VersionReplacedByUser = ent.VersionReplacedByUser.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}