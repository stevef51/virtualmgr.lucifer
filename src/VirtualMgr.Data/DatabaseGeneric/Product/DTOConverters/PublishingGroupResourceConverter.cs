﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class PublishingGroupResourceConverter
	{
	
		public static PublishingGroupResourceEntity ToEntity(this PublishingGroupResourceDTO dto)
		{
			return dto.ToEntity(new PublishingGroupResourceEntity(), new Dictionary<object, object>());
		}

		public static PublishingGroupResourceEntity ToEntity(this PublishingGroupResourceDTO dto, PublishingGroupResourceEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static PublishingGroupResourceEntity ToEntity(this PublishingGroupResourceDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new PublishingGroupResourceEntity(), cache);
		}
	
		public static PublishingGroupResourceDTO ToDTO(this PublishingGroupResourceEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static PublishingGroupResourceEntity ToEntity(this PublishingGroupResourceDTO dto, PublishingGroupResourceEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (PublishingGroupResourceEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Description = dto.Description;
			
			

			
			
			newEnt.EstimatedPageCount = dto.EstimatedPageCount;
			
			

			
			
			newEnt.Id = dto.Id;
			
			
			

			
			
			if (dto.IfFinishedCleanUpAfter == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)PublishingGroupResourceFieldIndex.IfFinishedCleanUpAfter, default(System.Int32));
				
			}
			
			newEnt.IfFinishedCleanUpAfter = dto.IfFinishedCleanUpAfter;
			
			

			
			
			if (dto.IfNotFinishedCleanUpAfter == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)PublishingGroupResourceFieldIndex.IfNotFinishedCleanUpAfter, default(System.Int32));
				
			}
			
			newEnt.IfNotFinishedCleanUpAfter = dto.IfNotFinishedCleanUpAfter;
			
			

			
			
			newEnt.Name = dto.Name;
			
			

			
			
			if (dto.PictureId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)PublishingGroupResourceFieldIndex.PictureId, default(System.Guid));
				
			}
			
			newEnt.PictureId = dto.PictureId;
			
			

			
			
			newEnt.PopulateReportingData = dto.PopulateReportingData;
			
			

			
			
			newEnt.PublishingGroupId = dto.PublishingGroupId;
			
			

			
			
			newEnt.ResourceId = dto.ResourceId;
			
			

			
			
			newEnt.UtcLastModified = dto.UtcLastModified;
			
			
			
			cache.Add(dto, newEnt);
			
			
			newEnt.PublishingGroupResourceData = dto.PublishingGroupResourceData.ToEntity(cache);
			
			
			
			newEnt.Picture = dto.Picture.ToEntity(cache);
			
			newEnt.PublishingGroup = dto.PublishingGroup.ToEntity(cache);
			
			newEnt.Resource = dto.Resource.ToEntity(cache);
			
			
			
			foreach (var related in dto.AssetTypeContextPublishedResources)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.AssetTypeContextPublishedResources.Contains(relatedEntity))
				{
					newEnt.AssetTypeContextPublishedResources.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ProjectJobScheduledTaskPublishingGroupResources)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProjectJobScheduledTaskPublishingGroupResources.Contains(relatedEntity))
				{
					newEnt.ProjectJobScheduledTaskPublishingGroupResources.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ProjectJobTaskTypePublishingGroupResources)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProjectJobTaskTypePublishingGroupResources.Contains(relatedEntity))
				{
					newEnt.ProjectJobTaskTypePublishingGroupResources.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ProjectJobTaskWorkingDocuments)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProjectJobTaskWorkingDocuments.Contains(relatedEntity))
				{
					newEnt.ProjectJobTaskWorkingDocuments.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.LabelPublishingGroupResources)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.LabelPublishingGroupResources.Contains(relatedEntity))
				{
					newEnt.LabelPublishingGroupResources.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.PublicPublishingGroupResources)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.PublicPublishingGroupResources.Contains(relatedEntity))
				{
					newEnt.PublicPublishingGroupResources.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.PublishingGroupResourceCalendars)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.PublishingGroupResourceCalendars.Contains(relatedEntity))
				{
					newEnt.PublishingGroupResourceCalendars.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.PublishingGroupResourcePublicVariables)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.PublishingGroupResourcePublicVariables.Contains(relatedEntity))
				{
					newEnt.PublishingGroupResourcePublicVariables.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.UserTypeContextPublishedResources)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.UserTypeContextPublishedResources.Contains(relatedEntity))
				{
					newEnt.UserTypeContextPublishedResources.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.UserTypeDashboards)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.UserTypeDashboards.Contains(relatedEntity))
				{
					newEnt.UserTypeDashboards.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.WorkingDocuments)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.WorkingDocuments.Contains(relatedEntity))
				{
					newEnt.WorkingDocuments.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static PublishingGroupResourceDTO ToDTO(this PublishingGroupResourceEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (PublishingGroupResourceDTO)cache[ent];
			
			var newDTO = new PublishingGroupResourceDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Description = ent.Description;
			

			newDTO.EstimatedPageCount = ent.EstimatedPageCount;
			

			newDTO.Id = ent.Id;
			

			newDTO.IfFinishedCleanUpAfter = ent.IfFinishedCleanUpAfter;
			

			newDTO.IfNotFinishedCleanUpAfter = ent.IfNotFinishedCleanUpAfter;
			

			newDTO.Name = ent.Name;
			

			newDTO.PictureId = ent.PictureId;
			

			newDTO.PopulateReportingData = ent.PopulateReportingData;
			

			newDTO.PublishingGroupId = ent.PublishingGroupId;
			

			newDTO.ResourceId = ent.ResourceId;
			

			newDTO.UtcLastModified = ent.UtcLastModified;
			
			
			
			cache.Add(ent, newDTO);
			
			
			newDTO.PublishingGroupResourceData = ent.PublishingGroupResourceData.ToDTO(cache);
			
			
			
			newDTO.Picture = ent.Picture.ToDTO(cache);
			
			newDTO.PublishingGroup = ent.PublishingGroup.ToDTO(cache);
			
			newDTO.Resource = ent.Resource.ToDTO(cache);
			
			
			
			foreach (var related in ent.AssetTypeContextPublishedResources)
			{
				newDTO.AssetTypeContextPublishedResources.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ProjectJobScheduledTaskPublishingGroupResources)
			{
				newDTO.ProjectJobScheduledTaskPublishingGroupResources.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ProjectJobTaskTypePublishingGroupResources)
			{
				newDTO.ProjectJobTaskTypePublishingGroupResources.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ProjectJobTaskWorkingDocuments)
			{
				newDTO.ProjectJobTaskWorkingDocuments.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.LabelPublishingGroupResources)
			{
				newDTO.LabelPublishingGroupResources.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.PublicPublishingGroupResources)
			{
				newDTO.PublicPublishingGroupResources.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.PublishingGroupResourceCalendars)
			{
				newDTO.PublishingGroupResourceCalendars.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.PublishingGroupResourcePublicVariables)
			{
				newDTO.PublishingGroupResourcePublicVariables.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.UserTypeContextPublishedResources)
			{
				newDTO.UserTypeContextPublishedResources.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.UserTypeDashboards)
			{
				newDTO.UserTypeDashboards.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.WorkingDocuments)
			{
				newDTO.WorkingDocuments.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}