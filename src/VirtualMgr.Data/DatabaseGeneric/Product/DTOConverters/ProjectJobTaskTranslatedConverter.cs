﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class ProjectJobTaskTranslatedConverter
	{
	
		public static ProjectJobTaskTranslatedEntity ToEntity(this ProjectJobTaskTranslatedDTO dto)
		{
			return dto.ToEntity(new ProjectJobTaskTranslatedEntity(), new Dictionary<object, object>());
		}

		public static ProjectJobTaskTranslatedEntity ToEntity(this ProjectJobTaskTranslatedDTO dto, ProjectJobTaskTranslatedEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static ProjectJobTaskTranslatedEntity ToEntity(this ProjectJobTaskTranslatedDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new ProjectJobTaskTranslatedEntity(), cache);
		}
	
		public static ProjectJobTaskTranslatedDTO ToDTO(this ProjectJobTaskTranslatedEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static ProjectJobTaskTranslatedEntity ToEntity(this ProjectJobTaskTranslatedDTO dto, ProjectJobTaskTranslatedEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (ProjectJobTaskTranslatedEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.CultureName = dto.CultureName;
			
			

			
			
			if (dto.Description == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobTaskTranslatedFieldIndex.Description, "");
				
			}
			
			newEnt.Description = dto.Description;
			
			

			
			
			newEnt.Id = dto.Id;
			
			

			
			
			newEnt.Name = dto.Name;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.ProjectJobTask = dto.ProjectJobTask.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static ProjectJobTaskTranslatedDTO ToDTO(this ProjectJobTaskTranslatedEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (ProjectJobTaskTranslatedDTO)cache[ent];
			
			var newDTO = new ProjectJobTaskTranslatedDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.CultureName = ent.CultureName;
			

			newDTO.Description = ent.Description;
			

			newDTO.Id = ent.Id;
			

			newDTO.Name = ent.Name;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.ProjectJobTask = ent.ProjectJobTask.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}