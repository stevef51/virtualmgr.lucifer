﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class ProjectJobTaskTypeFinishedStatusConverter
	{
	
		public static ProjectJobTaskTypeFinishedStatusEntity ToEntity(this ProjectJobTaskTypeFinishedStatusDTO dto)
		{
			return dto.ToEntity(new ProjectJobTaskTypeFinishedStatusEntity(), new Dictionary<object, object>());
		}

		public static ProjectJobTaskTypeFinishedStatusEntity ToEntity(this ProjectJobTaskTypeFinishedStatusDTO dto, ProjectJobTaskTypeFinishedStatusEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static ProjectJobTaskTypeFinishedStatusEntity ToEntity(this ProjectJobTaskTypeFinishedStatusDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new ProjectJobTaskTypeFinishedStatusEntity(), cache);
		}
	
		public static ProjectJobTaskTypeFinishedStatusDTO ToDTO(this ProjectJobTaskTypeFinishedStatusEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static ProjectJobTaskTypeFinishedStatusEntity ToEntity(this ProjectJobTaskTypeFinishedStatusDTO dto, ProjectJobTaskTypeFinishedStatusEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (ProjectJobTaskTypeFinishedStatusEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Id = dto.Id;
			
			

			
			
			newEnt.ProjectJobFinishedStatusId = dto.ProjectJobFinishedStatusId;
			
			

			
			
			newEnt.TaskTypeId = dto.TaskTypeId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.ProjectJobFinishedStatu = dto.ProjectJobFinishedStatu.ToEntity(cache);
			
			newEnt.ProjectJobTaskType = dto.ProjectJobTaskType.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static ProjectJobTaskTypeFinishedStatusDTO ToDTO(this ProjectJobTaskTypeFinishedStatusEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (ProjectJobTaskTypeFinishedStatusDTO)cache[ent];
			
			var newDTO = new ProjectJobTaskTypeFinishedStatusDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Id = ent.Id;
			

			newDTO.ProjectJobFinishedStatusId = ent.ProjectJobFinishedStatusId;
			

			newDTO.TaskTypeId = ent.TaskTypeId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.ProjectJobFinishedStatu = ent.ProjectJobFinishedStatu.ToDTO(cache);
			
			newDTO.ProjectJobTaskType = ent.ProjectJobTaskType.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}