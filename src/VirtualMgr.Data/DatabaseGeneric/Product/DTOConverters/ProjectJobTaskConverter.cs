﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class ProjectJobTaskConverter
	{
	
		public static ProjectJobTaskEntity ToEntity(this ProjectJobTaskDTO dto)
		{
			return dto.ToEntity(new ProjectJobTaskEntity(), new Dictionary<object, object>());
		}

		public static ProjectJobTaskEntity ToEntity(this ProjectJobTaskDTO dto, ProjectJobTaskEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static ProjectJobTaskEntity ToEntity(this ProjectJobTaskDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new ProjectJobTaskEntity(), cache);
		}
	
		public static ProjectJobTaskDTO ToDTO(this ProjectJobTaskEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static ProjectJobTaskEntity ToEntity(this ProjectJobTaskDTO dto, ProjectJobTaskEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (ProjectJobTaskEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			if (dto.ActualDuration == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobTaskFieldIndex.ActualDuration, default(System.Decimal));
				
			}
			
			newEnt.ActualDuration = dto.ActualDuration;
			
			

			
			
			newEnt.AllActivitiesEstimatedDuration = dto.AllActivitiesEstimatedDuration;
			
			

			
			
			if (dto.AssetId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobTaskFieldIndex.AssetId, default(System.Int32));
				
			}
			
			newEnt.AssetId = dto.AssetId;
			
			

			
			
			if (dto.Backcolor == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobTaskFieldIndex.Backcolor, default(System.Int32));
				
			}
			
			newEnt.Backcolor = dto.Backcolor;
			
			

			
			
			newEnt.CompletedActivitiesEstimatedDuration = dto.CompletedActivitiesEstimatedDuration;
			
			

			
			
			if (dto.CompletionNotes == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobTaskFieldIndex.CompletionNotes, "");
				
			}
			
			newEnt.CompletionNotes = dto.CompletionNotes;
			
			

			
			
			if (dto.Context == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobTaskFieldIndex.Context, "");
				
			}
			
			newEnt.Context = dto.Context;
			
			

			
			
			if (dto.DateCompleted == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobTaskFieldIndex.DateCompleted, default(System.DateTime));
				
			}
			
			newEnt.DateCompleted = dto.DateCompleted;
			
			

			
			
			newEnt.DateCreated = dto.DateCreated;
			
			

			
			
			if (dto.Description == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobTaskFieldIndex.Description, "");
				
			}
			
			newEnt.Description = dto.Description;
			
			

			
			
			newEnt.EndsOn = dto.EndsOn;
			
			

			
			
			if (dto.EstimatedDurationSeconds == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobTaskFieldIndex.EstimatedDurationSeconds, default(System.Decimal));
				
			}
			
			newEnt.EstimatedDurationSeconds = dto.EstimatedDurationSeconds;
			
			

			
			
			if (dto.Forecolor == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobTaskFieldIndex.Forecolor, default(System.Int32));
				
			}
			
			newEnt.Forecolor = dto.Forecolor;
			
			

			
			
			newEnt.HasOrders = dto.HasOrders;
			
			

			
			
			if (dto.HierarchyBucketId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobTaskFieldIndex.HierarchyBucketId, default(System.Int32));
				
			}
			
			newEnt.HierarchyBucketId = dto.HierarchyBucketId;
			
			

			
			
			newEnt.Id = dto.Id;
			
			

			
			
			if (dto.InactiveDuration == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobTaskFieldIndex.InactiveDuration, default(System.Decimal));
				
			}
			
			newEnt.InactiveDuration = dto.InactiveDuration;
			
			

			
			
			if (dto.LateAfterUtc == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobTaskFieldIndex.LateAfterUtc, default(System.DateTimeOffset));
				
			}
			
			newEnt.LateAfterUtc = dto.LateAfterUtc;
			
			

			
			
			newEnt.Level = dto.Level;
			
			

			
			
			if (dto.MinimumDuration == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobTaskFieldIndex.MinimumDuration, default(System.Decimal));
				
			}
			
			newEnt.MinimumDuration = dto.MinimumDuration;
			
			

			
			
			newEnt.Name = dto.Name;
			
			

			
			
			newEnt.NotifyUser = dto.NotifyUser;
			
			

			
			
			newEnt.OriginalOwnerId = dto.OriginalOwnerId;
			
			

			
			
			if (dto.OriginalTaskTypeId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobTaskFieldIndex.OriginalTaskTypeId, default(System.Guid));
				
			}
			
			newEnt.OriginalTaskTypeId = dto.OriginalTaskTypeId;
			
			

			
			
			newEnt.OwnerId = dto.OwnerId;
			
			

			
			
			newEnt.PhotoDuringSignature = dto.PhotoDuringSignature;
			
			

			
			
			if (dto.ProjectJobScheduledTaskId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobTaskFieldIndex.ProjectJobScheduledTaskId, default(System.Guid));
				
			}
			
			newEnt.ProjectJobScheduledTaskId = dto.ProjectJobScheduledTaskId;
			
			

			
			
			if (dto.ProjectJobTaskGroupId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobTaskFieldIndex.ProjectJobTaskGroupId, default(System.Guid));
				
			}
			
			newEnt.ProjectJobTaskGroupId = dto.ProjectJobTaskGroupId;
			
			

			
			
			if (dto.ReferenceNumber == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobTaskFieldIndex.ReferenceNumber, "");
				
			}
			
			newEnt.ReferenceNumber = dto.ReferenceNumber;
			
			

			
			
			newEnt.RequireSignature = dto.RequireSignature;
			
			

			
			
			if (dto.RoleId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobTaskFieldIndex.RoleId, default(System.Int32));
				
			}
			
			newEnt.RoleId = dto.RoleId;
			
			

			
			
			newEnt.RosterId = dto.RosterId;
			
			

			
			
			if (dto.ScheduledOwnerId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobTaskFieldIndex.ScheduledOwnerId, default(System.Guid));
				
			}
			
			newEnt.ScheduledOwnerId = dto.ScheduledOwnerId;
			
			

			
			
			if (dto.SiteId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobTaskFieldIndex.SiteId, default(System.Guid));
				
			}
			
			newEnt.SiteId = dto.SiteId;
			
			

			
			
			if (dto.SiteLatitude == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobTaskFieldIndex.SiteLatitude, default(System.Decimal));
				
			}
			
			newEnt.SiteLatitude = dto.SiteLatitude;
			
			

			
			
			if (dto.SiteLongitude == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobTaskFieldIndex.SiteLongitude, default(System.Decimal));
				
			}
			
			newEnt.SiteLongitude = dto.SiteLongitude;
			
			

			
			
			newEnt.SortOrder = dto.SortOrder;
			
			

			
			
			if (dto.StartTimeUtc == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobTaskFieldIndex.StartTimeUtc, default(System.DateTimeOffset));
				
			}
			
			newEnt.StartTimeUtc = dto.StartTimeUtc;
			
			

			
			
			newEnt.Status = dto.Status;
			
			

			
			
			newEnt.StatusDate = dto.StatusDate;
			
			

			
			
			if (dto.StatusId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobTaskFieldIndex.StatusId, default(System.Guid));
				
			}
			
			newEnt.StatusId = dto.StatusId;
			
			

			
			
			newEnt.TaskTypeId = dto.TaskTypeId;
			
			

			
			
			newEnt.WorkingDocumentCount = dto.WorkingDocumentCount;
			
			

			
			
			if (dto.WorkingGroupId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobTaskFieldIndex.WorkingGroupId, default(System.Guid));
				
			}
			
			newEnt.WorkingGroupId = dto.WorkingGroupId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			newEnt.TaskSignature = dto.TaskSignature.ToEntity(cache);
			
			
			
			newEnt.Asset = dto.Asset.ToEntity(cache);
			
			newEnt.ProjectJobScheduledTask = dto.ProjectJobScheduledTask.ToEntity(cache);
			
			newEnt.ProjectJobStatu = dto.ProjectJobStatu.ToEntity(cache);
			
			newEnt.ProjectJobTaskType = dto.ProjectJobTaskType.ToEntity(cache);
			
			newEnt.ProjectJobTaskType_ = dto.ProjectJobTaskType_.ToEntity(cache);
			
			newEnt.Roster = dto.Roster.ToEntity(cache);
			
			newEnt.HierarchyBucket = dto.HierarchyBucket.ToEntity(cache);
			
			newEnt.HierarchyRole = dto.HierarchyRole.ToEntity(cache);
			
			newEnt.Owner = dto.Owner.ToEntity(cache);
			
			newEnt.Site = dto.Site.ToEntity(cache);
			
			
			
			foreach (var related in dto.ProjectJobTaskAttachments)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProjectJobTaskAttachments.Contains(relatedEntity))
				{
					newEnt.ProjectJobTaskAttachments.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ProjectJobTaskChangeRequests)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProjectJobTaskChangeRequests.Contains(relatedEntity))
				{
					newEnt.ProjectJobTaskChangeRequests.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ProjectJobTaskFinishedStatuses)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProjectJobTaskFinishedStatuses.Contains(relatedEntity))
				{
					newEnt.ProjectJobTaskFinishedStatuses.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ProjectJobTaskLabels)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProjectJobTaskLabels.Contains(relatedEntity))
				{
					newEnt.ProjectJobTaskLabels.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ProjectJobTaskMedias)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProjectJobTaskMedias.Contains(relatedEntity))
				{
					newEnt.ProjectJobTaskMedias.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ProjectJobTaskRelationships)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProjectJobTaskRelationships.Contains(relatedEntity))
				{
					newEnt.ProjectJobTaskRelationships.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ProjectJobTaskRelationships_)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProjectJobTaskRelationships_.Contains(relatedEntity))
				{
					newEnt.ProjectJobTaskRelationships_.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.Translated)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.Translated.Contains(relatedEntity))
				{
					newEnt.Translated.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ProjectJobTaskWorkingDocuments)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProjectJobTaskWorkingDocuments.Contains(relatedEntity))
				{
					newEnt.ProjectJobTaskWorkingDocuments.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ProjectJobTaskWorkLogs)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProjectJobTaskWorkLogs.Contains(relatedEntity))
				{
					newEnt.ProjectJobTaskWorkLogs.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.Orders)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.Orders.Contains(relatedEntity))
				{
					newEnt.Orders.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ProjectJobTaskWorkloadingActivities)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProjectJobTaskWorkloadingActivities.Contains(relatedEntity))
				{
					newEnt.ProjectJobTaskWorkloadingActivities.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static ProjectJobTaskDTO ToDTO(this ProjectJobTaskEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (ProjectJobTaskDTO)cache[ent];
			
			var newDTO = new ProjectJobTaskDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.ActualDuration = ent.ActualDuration;
			

			newDTO.AllActivitiesEstimatedDuration = ent.AllActivitiesEstimatedDuration;
			

			newDTO.AssetId = ent.AssetId;
			

			newDTO.Backcolor = ent.Backcolor;
			

			newDTO.CompletedActivitiesEstimatedDuration = ent.CompletedActivitiesEstimatedDuration;
			

			newDTO.CompletionNotes = ent.CompletionNotes;
			

			newDTO.Context = ent.Context;
			

			newDTO.DateCompleted = ent.DateCompleted;
			

			newDTO.DateCreated = ent.DateCreated;
			

			newDTO.Description = ent.Description;
			

			newDTO.EndsOn = ent.EndsOn;
			

			newDTO.EstimatedDurationSeconds = ent.EstimatedDurationSeconds;
			

			newDTO.Forecolor = ent.Forecolor;
			

			newDTO.HasOrders = ent.HasOrders;
			

			newDTO.HierarchyBucketId = ent.HierarchyBucketId;
			

			newDTO.Id = ent.Id;
			

			newDTO.InactiveDuration = ent.InactiveDuration;
			

			newDTO.LateAfterUtc = ent.LateAfterUtc;
			

			newDTO.Level = ent.Level;
			

			newDTO.MinimumDuration = ent.MinimumDuration;
			

			newDTO.Name = ent.Name;
			

			newDTO.NotifyUser = ent.NotifyUser;
			

			newDTO.OriginalOwnerId = ent.OriginalOwnerId;
			

			newDTO.OriginalTaskTypeId = ent.OriginalTaskTypeId;
			

			newDTO.OwnerId = ent.OwnerId;
			

			newDTO.PhotoDuringSignature = ent.PhotoDuringSignature;
			

			newDTO.ProjectJobScheduledTaskId = ent.ProjectJobScheduledTaskId;
			

			newDTO.ProjectJobTaskGroupId = ent.ProjectJobTaskGroupId;
			

			newDTO.ReferenceNumber = ent.ReferenceNumber;
			

			newDTO.RequireSignature = ent.RequireSignature;
			

			newDTO.RoleId = ent.RoleId;
			

			newDTO.RosterId = ent.RosterId;
			

			newDTO.ScheduledOwnerId = ent.ScheduledOwnerId;
			

			newDTO.SiteId = ent.SiteId;
			

			newDTO.SiteLatitude = ent.SiteLatitude;
			

			newDTO.SiteLongitude = ent.SiteLongitude;
			

			newDTO.SortOrder = ent.SortOrder;
			

			newDTO.StartTimeUtc = ent.StartTimeUtc;
			

			newDTO.Status = ent.Status;
			

			newDTO.StatusDate = ent.StatusDate;
			

			newDTO.StatusId = ent.StatusId;
			

			newDTO.TaskTypeId = ent.TaskTypeId;
			

			newDTO.WorkingDocumentCount = ent.WorkingDocumentCount;
			

			newDTO.WorkingGroupId = ent.WorkingGroupId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			newDTO.TaskSignature = ent.TaskSignature.ToDTO(cache);
			
			
			
			newDTO.Asset = ent.Asset.ToDTO(cache);
			
			newDTO.ProjectJobScheduledTask = ent.ProjectJobScheduledTask.ToDTO(cache);
			
			newDTO.ProjectJobStatu = ent.ProjectJobStatu.ToDTO(cache);
			
			newDTO.ProjectJobTaskType = ent.ProjectJobTaskType.ToDTO(cache);
			
			newDTO.ProjectJobTaskType_ = ent.ProjectJobTaskType_.ToDTO(cache);
			
			newDTO.Roster = ent.Roster.ToDTO(cache);
			
			newDTO.HierarchyBucket = ent.HierarchyBucket.ToDTO(cache);
			
			newDTO.HierarchyRole = ent.HierarchyRole.ToDTO(cache);
			
			newDTO.Owner = ent.Owner.ToDTO(cache);
			
			newDTO.Site = ent.Site.ToDTO(cache);
			
			
			
			foreach (var related in ent.ProjectJobTaskAttachments)
			{
				newDTO.ProjectJobTaskAttachments.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ProjectJobTaskChangeRequests)
			{
				newDTO.ProjectJobTaskChangeRequests.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ProjectJobTaskFinishedStatuses)
			{
				newDTO.ProjectJobTaskFinishedStatuses.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ProjectJobTaskLabels)
			{
				newDTO.ProjectJobTaskLabels.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ProjectJobTaskMedias)
			{
				newDTO.ProjectJobTaskMedias.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ProjectJobTaskRelationships)
			{
				newDTO.ProjectJobTaskRelationships.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ProjectJobTaskRelationships_)
			{
				newDTO.ProjectJobTaskRelationships_.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.Translated)
			{
				newDTO.Translated.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ProjectJobTaskWorkingDocuments)
			{
				newDTO.ProjectJobTaskWorkingDocuments.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ProjectJobTaskWorkLogs)
			{
				newDTO.ProjectJobTaskWorkLogs.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.Orders)
			{
				newDTO.Orders.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ProjectJobTaskWorkloadingActivities)
			{
				newDTO.ProjectJobTaskWorkloadingActivities.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}