﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class ProjectJobScheduleDayConverter
	{
	
		public static ProjectJobScheduleDayEntity ToEntity(this ProjectJobScheduleDayDTO dto)
		{
			return dto.ToEntity(new ProjectJobScheduleDayEntity(), new Dictionary<object, object>());
		}

		public static ProjectJobScheduleDayEntity ToEntity(this ProjectJobScheduleDayDTO dto, ProjectJobScheduleDayEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static ProjectJobScheduleDayEntity ToEntity(this ProjectJobScheduleDayDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new ProjectJobScheduleDayEntity(), cache);
		}
	
		public static ProjectJobScheduleDayDTO ToDTO(this ProjectJobScheduleDayEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static ProjectJobScheduleDayEntity ToEntity(this ProjectJobScheduleDayDTO dto, ProjectJobScheduleDayEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (ProjectJobScheduleDayEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Day = dto.Day;
			
			

			
			
			newEnt.EndTime = dto.EndTime;
			
			

			
			
			newEnt.LunchEnd = dto.LunchEnd;
			
			

			
			
			newEnt.LunchStart = dto.LunchStart;
			
			

			
			
			if (dto.OverrideRoleId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobScheduleDayFieldIndex.OverrideRoleId, default(System.Int32));
				
			}
			
			newEnt.OverrideRoleId = dto.OverrideRoleId;
			
			

			
			
			newEnt.RoleId = dto.RoleId;
			
			

			
			
			newEnt.RosterId = dto.RosterId;
			
			

			
			
			newEnt.StartTime = dto.StartTime;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.Roster = dto.Roster.ToEntity(cache);
			
			newEnt.HierarchyRole = dto.HierarchyRole.ToEntity(cache);
			
			newEnt.OverrideHierarchyRole = dto.OverrideHierarchyRole.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static ProjectJobScheduleDayDTO ToDTO(this ProjectJobScheduleDayEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (ProjectJobScheduleDayDTO)cache[ent];
			
			var newDTO = new ProjectJobScheduleDayDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Day = ent.Day;
			

			newDTO.EndTime = ent.EndTime;
			

			newDTO.LunchEnd = ent.LunchEnd;
			

			newDTO.LunchStart = ent.LunchStart;
			

			newDTO.OverrideRoleId = ent.OverrideRoleId;
			

			newDTO.RoleId = ent.RoleId;
			

			newDTO.RosterId = ent.RosterId;
			

			newDTO.StartTime = ent.StartTime;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.Roster = ent.Roster.ToDTO(cache);
			
			newDTO.HierarchyRole = ent.HierarchyRole.ToDTO(cache);
			
			newDTO.OverrideHierarchyRole = ent.OverrideHierarchyRole.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}