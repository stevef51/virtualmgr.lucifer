﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class ProductCatalogItemConverter
	{
	
		public static ProductCatalogItemEntity ToEntity(this ProductCatalogItemDTO dto)
		{
			return dto.ToEntity(new ProductCatalogItemEntity(), new Dictionary<object, object>());
		}

		public static ProductCatalogItemEntity ToEntity(this ProductCatalogItemDTO dto, ProductCatalogItemEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static ProductCatalogItemEntity ToEntity(this ProductCatalogItemDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new ProductCatalogItemEntity(), cache);
		}
	
		public static ProductCatalogItemDTO ToDTO(this ProductCatalogItemEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static ProductCatalogItemEntity ToEntity(this ProductCatalogItemDTO dto, ProductCatalogItemEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (ProductCatalogItemEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			if (dto.MinStockLevel == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProductCatalogItemFieldIndex.MinStockLevel, default(System.Decimal));
				
			}
			
			newEnt.MinStockLevel = dto.MinStockLevel;
			
			

			
			
			if (dto.Price == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProductCatalogItemFieldIndex.Price, default(System.Decimal));
				
			}
			
			newEnt.Price = dto.Price;
			
			

			
			
			if (dto.PriceJsFunctionId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProductCatalogItemFieldIndex.PriceJsFunctionId, default(System.Int32));
				
			}
			
			newEnt.PriceJsFunctionId = dto.PriceJsFunctionId;
			
			

			
			
			newEnt.ProductCatalogId = dto.ProductCatalogId;
			
			

			
			
			newEnt.ProductId = dto.ProductId;
			
			

			
			
			newEnt.SortOrder = dto.SortOrder;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.Jsfunction = dto.Jsfunction.ToEntity(cache);
			
			newEnt.Product = dto.Product.ToEntity(cache);
			
			newEnt.ProductCatalog = dto.ProductCatalog.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static ProductCatalogItemDTO ToDTO(this ProductCatalogItemEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (ProductCatalogItemDTO)cache[ent];
			
			var newDTO = new ProductCatalogItemDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.MinStockLevel = ent.MinStockLevel;
			

			newDTO.Price = ent.Price;
			

			newDTO.PriceJsFunctionId = ent.PriceJsFunctionId;
			

			newDTO.ProductCatalogId = ent.ProductCatalogId;
			

			newDTO.ProductId = ent.ProductId;
			

			newDTO.SortOrder = ent.SortOrder;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.Jsfunction = ent.Jsfunction.ToDTO(cache);
			
			newDTO.Product = ent.Product.ToDTO(cache);
			
			newDTO.ProductCatalog = ent.ProductCatalog.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}