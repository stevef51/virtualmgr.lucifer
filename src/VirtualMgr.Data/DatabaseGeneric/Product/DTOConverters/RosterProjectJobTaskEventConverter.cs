﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class RosterProjectJobTaskEventConverter
	{
	
		public static RosterProjectJobTaskEventEntity ToEntity(this RosterProjectJobTaskEventDTO dto)
		{
			return dto.ToEntity(new RosterProjectJobTaskEventEntity(), new Dictionary<object, object>());
		}

		public static RosterProjectJobTaskEventEntity ToEntity(this RosterProjectJobTaskEventDTO dto, RosterProjectJobTaskEventEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static RosterProjectJobTaskEventEntity ToEntity(this RosterProjectJobTaskEventDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new RosterProjectJobTaskEventEntity(), cache);
		}
	
		public static RosterProjectJobTaskEventDTO ToDTO(this RosterProjectJobTaskEventEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static RosterProjectJobTaskEventEntity ToEntity(this RosterProjectJobTaskEventDTO dto, RosterProjectJobTaskEventEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (RosterProjectJobTaskEventEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Id = dto.Id;
			
			

			
			
			newEnt.Name = dto.Name;
			
			

			
			
			newEnt.Timed = dto.Timed;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static RosterProjectJobTaskEventDTO ToDTO(this RosterProjectJobTaskEventEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (RosterProjectJobTaskEventDTO)cache[ent];
			
			var newDTO = new RosterProjectJobTaskEventDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Id = ent.Id;
			

			newDTO.Name = ent.Name;
			

			newDTO.Timed = ent.Timed;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}