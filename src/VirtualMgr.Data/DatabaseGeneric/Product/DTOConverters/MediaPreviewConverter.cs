﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class MediaPreviewConverter
	{
	
		public static MediaPreviewEntity ToEntity(this MediaPreviewDTO dto)
		{
			return dto.ToEntity(new MediaPreviewEntity(), new Dictionary<object, object>());
		}

		public static MediaPreviewEntity ToEntity(this MediaPreviewDTO dto, MediaPreviewEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static MediaPreviewEntity ToEntity(this MediaPreviewDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new MediaPreviewEntity(), cache);
		}
	
		public static MediaPreviewDTO ToDTO(this MediaPreviewEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static MediaPreviewEntity ToEntity(this MediaPreviewDTO dto, MediaPreviewEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (MediaPreviewEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Data = dto.Data;
			
			

			
			
			newEnt.MediaId = dto.MediaId;
			
			

			
			
			newEnt.Page = dto.Page;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.Medium = dto.Medium.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static MediaPreviewDTO ToDTO(this MediaPreviewEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (MediaPreviewDTO)cache[ent];
			
			var newDTO = new MediaPreviewDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Data = ent.Data;
			

			newDTO.MediaId = ent.MediaId;
			

			newDTO.Page = ent.Page;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.Medium = ent.Medium.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}