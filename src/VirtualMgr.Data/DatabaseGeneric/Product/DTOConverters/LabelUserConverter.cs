﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class LabelUserConverter
	{
	
		public static LabelUserEntity ToEntity(this LabelUserDTO dto)
		{
			return dto.ToEntity(new LabelUserEntity(), new Dictionary<object, object>());
		}

		public static LabelUserEntity ToEntity(this LabelUserDTO dto, LabelUserEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static LabelUserEntity ToEntity(this LabelUserDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new LabelUserEntity(), cache);
		}
	
		public static LabelUserDTO ToDTO(this LabelUserEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static LabelUserEntity ToEntity(this LabelUserDTO dto, LabelUserEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (LabelUserEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.LabelId = dto.LabelId;
			
			

			
			
			newEnt.UserId = dto.UserId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.Label = dto.Label.ToEntity(cache);
			
			newEnt.UserData = dto.UserData.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static LabelUserDTO ToDTO(this LabelUserEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (LabelUserDTO)cache[ent];
			
			var newDTO = new LabelUserDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.LabelId = ent.LabelId;
			

			newDTO.UserId = ent.UserId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.Label = ent.Label.ToDTO(cache);
			
			newDTO.UserData = ent.UserData.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}