﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class MediaPolicyConverter
	{
	
		public static MediaPolicyEntity ToEntity(this MediaPolicyDTO dto)
		{
			return dto.ToEntity(new MediaPolicyEntity(), new Dictionary<object, object>());
		}

		public static MediaPolicyEntity ToEntity(this MediaPolicyDTO dto, MediaPolicyEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static MediaPolicyEntity ToEntity(this MediaPolicyDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new MediaPolicyEntity(), cache);
		}
	
		public static MediaPolicyDTO ToDTO(this MediaPolicyEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static MediaPolicyEntity ToEntity(this MediaPolicyDTO dto, MediaPolicyEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (MediaPolicyEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Data = dto.Data;
			
			

			
			
			newEnt.MediaId = dto.MediaId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			newEnt.Medium = dto.Medium.ToEntity(cache);
			
			
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static MediaPolicyDTO ToDTO(this MediaPolicyEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (MediaPolicyDTO)cache[ent];
			
			var newDTO = new MediaPolicyDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Data = ent.Data;
			

			newDTO.MediaId = ent.MediaId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			newDTO.Medium = ent.Medium.ToDTO(cache);
			
			
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}