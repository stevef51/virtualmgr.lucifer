﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class WorkingDocumentStatusConverter
	{
	
		public static WorkingDocumentStatusEntity ToEntity(this WorkingDocumentStatusDTO dto)
		{
			return dto.ToEntity(new WorkingDocumentStatusEntity(), new Dictionary<object, object>());
		}

		public static WorkingDocumentStatusEntity ToEntity(this WorkingDocumentStatusDTO dto, WorkingDocumentStatusEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static WorkingDocumentStatusEntity ToEntity(this WorkingDocumentStatusDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new WorkingDocumentStatusEntity(), cache);
		}
	
		public static WorkingDocumentStatusDTO ToDTO(this WorkingDocumentStatusEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static WorkingDocumentStatusEntity ToEntity(this WorkingDocumentStatusDTO dto, WorkingDocumentStatusEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (WorkingDocumentStatusEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Id = dto.Id;
			
			

			
			
			newEnt.Text = dto.Text;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			
			
			foreach (var related in dto.WorkingDocuments)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.WorkingDocuments.Contains(relatedEntity))
				{
					newEnt.WorkingDocuments.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static WorkingDocumentStatusDTO ToDTO(this WorkingDocumentStatusEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (WorkingDocumentStatusDTO)cache[ent];
			
			var newDTO = new WorkingDocumentStatusDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Id = ent.Id;
			

			newDTO.Text = ent.Text;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			
			
			foreach (var related in ent.WorkingDocuments)
			{
				newDTO.WorkingDocuments.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}