﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class PluginDataConverter
	{
	
		public static PluginDataEntity ToEntity(this PluginDataDTO dto)
		{
			return dto.ToEntity(new PluginDataEntity(), new Dictionary<object, object>());
		}

		public static PluginDataEntity ToEntity(this PluginDataDTO dto, PluginDataEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static PluginDataEntity ToEntity(this PluginDataDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new PluginDataEntity(), cache);
		}
	
		public static PluginDataDTO ToDTO(this PluginDataEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static PluginDataEntity ToEntity(this PluginDataDTO dto, PluginDataEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (PluginDataEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Data = dto.Data;
			
			

			
			
			newEnt.PluginId = dto.PluginId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			newEnt.Plugin = dto.Plugin.ToEntity(cache);
			
			
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static PluginDataDTO ToDTO(this PluginDataEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (PluginDataDTO)cache[ent];
			
			var newDTO = new PluginDataDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Data = ent.Data;
			

			newDTO.PluginId = ent.PluginId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			newDTO.Plugin = ent.Plugin.ToDTO(cache);
			
			
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}