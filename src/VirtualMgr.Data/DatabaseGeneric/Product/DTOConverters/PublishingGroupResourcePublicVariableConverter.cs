﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class PublishingGroupResourcePublicVariableConverter
	{
	
		public static PublishingGroupResourcePublicVariableEntity ToEntity(this PublishingGroupResourcePublicVariableDTO dto)
		{
			return dto.ToEntity(new PublishingGroupResourcePublicVariableEntity(), new Dictionary<object, object>());
		}

		public static PublishingGroupResourcePublicVariableEntity ToEntity(this PublishingGroupResourcePublicVariableDTO dto, PublishingGroupResourcePublicVariableEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static PublishingGroupResourcePublicVariableEntity ToEntity(this PublishingGroupResourcePublicVariableDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new PublishingGroupResourcePublicVariableEntity(), cache);
		}
	
		public static PublishingGroupResourcePublicVariableDTO ToDTO(this PublishingGroupResourcePublicVariableEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static PublishingGroupResourcePublicVariableEntity ToEntity(this PublishingGroupResourcePublicVariableDTO dto, PublishingGroupResourcePublicVariableEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (PublishingGroupResourcePublicVariableEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Id = dto.Id;
			
			
			

			
			
			newEnt.Name = dto.Name;
			
			

			
			
			newEnt.PublishingGroupResourceId = dto.PublishingGroupResourceId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.PublishingGroupResource = dto.PublishingGroupResource.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static PublishingGroupResourcePublicVariableDTO ToDTO(this PublishingGroupResourcePublicVariableEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (PublishingGroupResourcePublicVariableDTO)cache[ent];
			
			var newDTO = new PublishingGroupResourcePublicVariableDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Id = ent.Id;
			

			newDTO.Name = ent.Name;
			

			newDTO.PublishingGroupResourceId = ent.PublishingGroupResourceId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.PublishingGroupResource = ent.PublishingGroupResource.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}