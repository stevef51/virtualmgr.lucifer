﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class AssetTypeCalendarConverter
	{
	
		public static AssetTypeCalendarEntity ToEntity(this AssetTypeCalendarDTO dto)
		{
			return dto.ToEntity(new AssetTypeCalendarEntity(), new Dictionary<object, object>());
		}

		public static AssetTypeCalendarEntity ToEntity(this AssetTypeCalendarDTO dto, AssetTypeCalendarEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static AssetTypeCalendarEntity ToEntity(this AssetTypeCalendarDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new AssetTypeCalendarEntity(), cache);
		}
	
		public static AssetTypeCalendarDTO ToDTO(this AssetTypeCalendarEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static AssetTypeCalendarEntity ToEntity(this AssetTypeCalendarDTO dto, AssetTypeCalendarEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (AssetTypeCalendarEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Archived = dto.Archived;
			
			

			
			
			newEnt.AssetTypeId = dto.AssetTypeId;
			
			

			
			
			newEnt.Id = dto.Id;
			
			
			

			
			
			newEnt.Name = dto.Name;
			
			

			
			
			newEnt.ScheduleExpression = dto.ScheduleExpression;
			
			

			
			
			if (dto.TimeZone == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)AssetTypeCalendarFieldIndex.TimeZone, "");
				
			}
			
			newEnt.TimeZone = dto.TimeZone;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.AssetType = dto.AssetType.ToEntity(cache);
			
			
			
			foreach (var related in dto.AssetSchedules)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.AssetSchedules.Contains(relatedEntity))
				{
					newEnt.AssetSchedules.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.AssetTypeCalendarTasks)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.AssetTypeCalendarTasks.Contains(relatedEntity))
				{
					newEnt.AssetTypeCalendarTasks.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static AssetTypeCalendarDTO ToDTO(this AssetTypeCalendarEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (AssetTypeCalendarDTO)cache[ent];
			
			var newDTO = new AssetTypeCalendarDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Archived = ent.Archived;
			

			newDTO.AssetTypeId = ent.AssetTypeId;
			

			newDTO.Id = ent.Id;
			

			newDTO.Name = ent.Name;
			

			newDTO.ScheduleExpression = ent.ScheduleExpression;
			

			newDTO.TimeZone = ent.TimeZone;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.AssetType = ent.AssetType.ToDTO(cache);
			
			
			
			foreach (var related in ent.AssetSchedules)
			{
				newDTO.AssetSchedules.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.AssetTypeCalendarTasks)
			{
				newDTO.AssetTypeCalendarTasks.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}