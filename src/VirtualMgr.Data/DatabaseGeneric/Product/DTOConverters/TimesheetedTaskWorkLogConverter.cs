﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class TimesheetedTaskWorkLogConverter
	{
	
		public static TimesheetedTaskWorkLogEntity ToEntity(this TimesheetedTaskWorkLogDTO dto)
		{
			return dto.ToEntity(new TimesheetedTaskWorkLogEntity(), new Dictionary<object, object>());
		}

		public static TimesheetedTaskWorkLogEntity ToEntity(this TimesheetedTaskWorkLogDTO dto, TimesheetedTaskWorkLogEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static TimesheetedTaskWorkLogEntity ToEntity(this TimesheetedTaskWorkLogDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new TimesheetedTaskWorkLogEntity(), cache);
		}
	
		public static TimesheetedTaskWorkLogDTO ToDTO(this TimesheetedTaskWorkLogEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static TimesheetedTaskWorkLogEntity ToEntity(this TimesheetedTaskWorkLogDTO dto, TimesheetedTaskWorkLogEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (TimesheetedTaskWorkLogEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.ProjectJobTaskWorkLogId = dto.ProjectJobTaskWorkLogId;
			
			

			
			
			newEnt.TimesheetItemId = dto.TimesheetItemId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.ProjectJobTaskWorkLog = dto.ProjectJobTaskWorkLog.ToEntity(cache);
			
			newEnt.TimesheetItem = dto.TimesheetItem.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static TimesheetedTaskWorkLogDTO ToDTO(this TimesheetedTaskWorkLogEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (TimesheetedTaskWorkLogDTO)cache[ent];
			
			var newDTO = new TimesheetedTaskWorkLogDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.ProjectJobTaskWorkLogId = ent.ProjectJobTaskWorkLogId;
			

			newDTO.TimesheetItemId = ent.TimesheetItemId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.ProjectJobTaskWorkLog = ent.ProjectJobTaskWorkLog.ToDTO(cache);
			
			newDTO.TimesheetItem = ent.TimesheetItem.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}