﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class WorkLoadingActivityConverter
	{
	
		public static WorkLoadingActivityEntity ToEntity(this WorkLoadingActivityDTO dto)
		{
			return dto.ToEntity(new WorkLoadingActivityEntity(), new Dictionary<object, object>());
		}

		public static WorkLoadingActivityEntity ToEntity(this WorkLoadingActivityDTO dto, WorkLoadingActivityEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static WorkLoadingActivityEntity ToEntity(this WorkLoadingActivityDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new WorkLoadingActivityEntity(), cache);
		}
	
		public static WorkLoadingActivityDTO ToDTO(this WorkLoadingActivityEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static WorkLoadingActivityEntity ToEntity(this WorkLoadingActivityDTO dto, WorkLoadingActivityEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (WorkLoadingActivityEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Archived = dto.Archived;
			
			

			
			
			newEnt.Id = dto.Id;
			
			

			
			
			newEnt.Name = dto.Name;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			
			
			foreach (var related in dto.WorkLoadingStandards)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.WorkLoadingStandards.Contains(relatedEntity))
				{
					newEnt.WorkLoadingStandards.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static WorkLoadingActivityDTO ToDTO(this WorkLoadingActivityEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (WorkLoadingActivityDTO)cache[ent];
			
			var newDTO = new WorkLoadingActivityDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Archived = ent.Archived;
			

			newDTO.Id = ent.Id;
			

			newDTO.Name = ent.Name;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			
			
			foreach (var related in ent.WorkLoadingStandards)
			{
				newDTO.WorkLoadingStandards.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}