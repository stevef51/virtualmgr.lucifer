﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class PublishingGroupResourceCalendarConverter
	{
	
		public static PublishingGroupResourceCalendarEntity ToEntity(this PublishingGroupResourceCalendarDTO dto)
		{
			return dto.ToEntity(new PublishingGroupResourceCalendarEntity(), new Dictionary<object, object>());
		}

		public static PublishingGroupResourceCalendarEntity ToEntity(this PublishingGroupResourceCalendarDTO dto, PublishingGroupResourceCalendarEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static PublishingGroupResourceCalendarEntity ToEntity(this PublishingGroupResourceCalendarDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new PublishingGroupResourceCalendarEntity(), cache);
		}
	
		public static PublishingGroupResourceCalendarDTO ToDTO(this PublishingGroupResourceCalendarEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static PublishingGroupResourceCalendarEntity ToEntity(this PublishingGroupResourceCalendarDTO dto, PublishingGroupResourceCalendarEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (PublishingGroupResourceCalendarEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.CalendarEventId = dto.CalendarEventId;
			
			

			
			
			newEnt.Id = dto.Id;
			
			

			
			
			newEnt.PublishingGroupResourceId = dto.PublishingGroupResourceId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			newEnt.PublishingGroupResourceActorCalendar = dto.PublishingGroupResourceActorCalendar.ToEntity(cache);
			
			
			
			newEnt.CalendarEvent = dto.CalendarEvent.ToEntity(cache);
			
			newEnt.PublishingGroupResource = dto.PublishingGroupResource.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static PublishingGroupResourceCalendarDTO ToDTO(this PublishingGroupResourceCalendarEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (PublishingGroupResourceCalendarDTO)cache[ent];
			
			var newDTO = new PublishingGroupResourceCalendarDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.CalendarEventId = ent.CalendarEventId;
			

			newDTO.Id = ent.Id;
			

			newDTO.PublishingGroupResourceId = ent.PublishingGroupResourceId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			newDTO.PublishingGroupResourceActorCalendar = ent.PublishingGroupResourceActorCalendar.ToDTO(cache);
			
			
			
			newDTO.CalendarEvent = ent.CalendarEvent.ToDTO(cache);
			
			newDTO.PublishingGroupResource = ent.PublishingGroupResource.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}