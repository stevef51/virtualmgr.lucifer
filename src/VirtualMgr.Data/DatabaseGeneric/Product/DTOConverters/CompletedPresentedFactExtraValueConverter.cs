﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class CompletedPresentedFactExtraValueConverter
	{
	
		public static CompletedPresentedFactExtraValueEntity ToEntity(this CompletedPresentedFactExtraValueDTO dto)
		{
			return dto.ToEntity(new CompletedPresentedFactExtraValueEntity(), new Dictionary<object, object>());
		}

		public static CompletedPresentedFactExtraValueEntity ToEntity(this CompletedPresentedFactExtraValueDTO dto, CompletedPresentedFactExtraValueEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static CompletedPresentedFactExtraValueEntity ToEntity(this CompletedPresentedFactExtraValueDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new CompletedPresentedFactExtraValueEntity(), cache);
		}
	
		public static CompletedPresentedFactExtraValueDTO ToDTO(this CompletedPresentedFactExtraValueEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static CompletedPresentedFactExtraValueEntity ToEntity(this CompletedPresentedFactExtraValueDTO dto, CompletedPresentedFactExtraValueEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (CompletedPresentedFactExtraValueEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.CompletedWorkingDocumentPresentedFactId = dto.CompletedWorkingDocumentPresentedFactId;
			
			

			
			
			newEnt.Id = dto.Id;
			
			

			
			
			newEnt.Key = dto.Key;
			
			

			
			
			if (dto.Value == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)CompletedPresentedFactExtraValueFieldIndex.Value, "");
				
			}
			
			newEnt.Value = dto.Value;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.CompletedWorkingDocumentPresentedFact = dto.CompletedWorkingDocumentPresentedFact.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static CompletedPresentedFactExtraValueDTO ToDTO(this CompletedPresentedFactExtraValueEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (CompletedPresentedFactExtraValueDTO)cache[ent];
			
			var newDTO = new CompletedPresentedFactExtraValueDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.CompletedWorkingDocumentPresentedFactId = ent.CompletedWorkingDocumentPresentedFactId;
			

			newDTO.Id = ent.Id;
			

			newDTO.Key = ent.Key;
			

			newDTO.Value = ent.Value;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.CompletedWorkingDocumentPresentedFact = ent.CompletedWorkingDocumentPresentedFact.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}