﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class CompletedWorkingDocumentPresentedFactConverter
	{
	
		public static CompletedWorkingDocumentPresentedFactEntity ToEntity(this CompletedWorkingDocumentPresentedFactDTO dto)
		{
			return dto.ToEntity(new CompletedWorkingDocumentPresentedFactEntity(), new Dictionary<object, object>());
		}

		public static CompletedWorkingDocumentPresentedFactEntity ToEntity(this CompletedWorkingDocumentPresentedFactDTO dto, CompletedWorkingDocumentPresentedFactEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static CompletedWorkingDocumentPresentedFactEntity ToEntity(this CompletedWorkingDocumentPresentedFactDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new CompletedWorkingDocumentPresentedFactEntity(), cache);
		}
	
		public static CompletedWorkingDocumentPresentedFactDTO ToDTO(this CompletedWorkingDocumentPresentedFactEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static CompletedWorkingDocumentPresentedFactEntity ToEntity(this CompletedWorkingDocumentPresentedFactDTO dto, CompletedWorkingDocumentPresentedFactEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (CompletedWorkingDocumentPresentedFactEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.ContextType = dto.ContextType;
			
			

			
			
			newEnt.DateCompleted = dto.DateCompleted;
			
			

			
			
			newEnt.DateStarted = dto.DateStarted;
			
			

			
			
			newEnt.Id = dto.Id;
			
			

			
			
			newEnt.InternalId = dto.InternalId;
			
			

			
			
			newEnt.IsUserContext = dto.IsUserContext;
			
			

			
			
			if (dto.NotesText == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)CompletedWorkingDocumentPresentedFactFieldIndex.NotesText, "");
				
			}
			
			newEnt.NotesText = dto.NotesText;
			
			

			
			
			if (dto.ParentWorkingDocumentId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)CompletedWorkingDocumentPresentedFactFieldIndex.ParentWorkingDocumentId, default(System.Guid));
				
			}
			
			newEnt.ParentWorkingDocumentId = dto.ParentWorkingDocumentId;
			
			

			
			
			if (dto.PassFail == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)CompletedWorkingDocumentPresentedFactFieldIndex.PassFail, default(System.Boolean));
				
			}
			
			newEnt.PassFail = dto.PassFail;
			
			

			
			
			newEnt.PossibleScore = dto.PossibleScore;
			
			

			
			
			newEnt.PresentedId = dto.PresentedId;
			
			

			
			
			newEnt.PresentedOrder = dto.PresentedOrder;
			
			

			
			
			newEnt.Prompt = dto.Prompt;
			
			

			
			
			newEnt.ReportingPresentedTypeId = dto.ReportingPresentedTypeId;
			
			

			
			
			newEnt.ReportingUserRevieweeId = dto.ReportingUserRevieweeId;
			
			

			
			
			newEnt.ReportingUserReviewerId = dto.ReportingUserReviewerId;
			
			

			
			
			newEnt.ReportingWorkingDocumentId = dto.ReportingWorkingDocumentId;
			
			

			
			
			newEnt.Score = dto.Score;
			
			

			
			
			newEnt.WorkingDocumentId = dto.WorkingDocumentId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.CompletedPresentedTypeDimension = dto.CompletedPresentedTypeDimension.ToEntity(cache);
			
			newEnt.CompletedUserDimensionReviewee = dto.CompletedUserDimensionReviewee.ToEntity(cache);
			
			newEnt.CompletedUserDimensionReviewer = dto.CompletedUserDimensionReviewer.ToEntity(cache);
			
			newEnt.CompletedWorkingDocumentDimension = dto.CompletedWorkingDocumentDimension.ToEntity(cache);
			
			newEnt.CompletedWorkingDocumentFact = dto.CompletedWorkingDocumentFact.ToEntity(cache);
			
			
			
			foreach (var related in dto.CompletedLabelWorkingDocumentPresentedDimensions)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.CompletedLabelWorkingDocumentPresentedDimensions.Contains(relatedEntity))
				{
					newEnt.CompletedLabelWorkingDocumentPresentedDimensions.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.CompletedPresentedFactExtraValues)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.CompletedPresentedFactExtraValues.Contains(relatedEntity))
				{
					newEnt.CompletedPresentedFactExtraValues.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.CompletedPresentedFactValues)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.CompletedPresentedFactValues.Contains(relatedEntity))
				{
					newEnt.CompletedPresentedFactValues.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.CompletedPresentedGeoLocationFactValues)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.CompletedPresentedGeoLocationFactValues.Contains(relatedEntity))
				{
					newEnt.CompletedPresentedGeoLocationFactValues.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.CompletedPresentedUploadMediaFactValues)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.CompletedPresentedUploadMediaFactValues.Contains(relatedEntity))
				{
					newEnt.CompletedPresentedUploadMediaFactValues.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static CompletedWorkingDocumentPresentedFactDTO ToDTO(this CompletedWorkingDocumentPresentedFactEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (CompletedWorkingDocumentPresentedFactDTO)cache[ent];
			
			var newDTO = new CompletedWorkingDocumentPresentedFactDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.ContextType = ent.ContextType;
			

			newDTO.DateCompleted = ent.DateCompleted;
			

			newDTO.DateStarted = ent.DateStarted;
			

			newDTO.Id = ent.Id;
			

			newDTO.InternalId = ent.InternalId;
			

			newDTO.IsUserContext = ent.IsUserContext;
			

			newDTO.NotesText = ent.NotesText;
			

			newDTO.ParentWorkingDocumentId = ent.ParentWorkingDocumentId;
			

			newDTO.PassFail = ent.PassFail;
			

			newDTO.PossibleScore = ent.PossibleScore;
			

			newDTO.PresentedId = ent.PresentedId;
			

			newDTO.PresentedOrder = ent.PresentedOrder;
			

			newDTO.Prompt = ent.Prompt;
			

			newDTO.ReportingPresentedTypeId = ent.ReportingPresentedTypeId;
			

			newDTO.ReportingUserRevieweeId = ent.ReportingUserRevieweeId;
			

			newDTO.ReportingUserReviewerId = ent.ReportingUserReviewerId;
			

			newDTO.ReportingWorkingDocumentId = ent.ReportingWorkingDocumentId;
			

			newDTO.Score = ent.Score;
			

			newDTO.WorkingDocumentId = ent.WorkingDocumentId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.CompletedPresentedTypeDimension = ent.CompletedPresentedTypeDimension.ToDTO(cache);
			
			newDTO.CompletedUserDimensionReviewee = ent.CompletedUserDimensionReviewee.ToDTO(cache);
			
			newDTO.CompletedUserDimensionReviewer = ent.CompletedUserDimensionReviewer.ToDTO(cache);
			
			newDTO.CompletedWorkingDocumentDimension = ent.CompletedWorkingDocumentDimension.ToDTO(cache);
			
			newDTO.CompletedWorkingDocumentFact = ent.CompletedWorkingDocumentFact.ToDTO(cache);
			
			
			
			foreach (var related in ent.CompletedLabelWorkingDocumentPresentedDimensions)
			{
				newDTO.CompletedLabelWorkingDocumentPresentedDimensions.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.CompletedPresentedFactExtraValues)
			{
				newDTO.CompletedPresentedFactExtraValues.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.CompletedPresentedFactValues)
			{
				newDTO.CompletedPresentedFactValues.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.CompletedPresentedGeoLocationFactValues)
			{
				newDTO.CompletedPresentedGeoLocationFactValues.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.CompletedPresentedUploadMediaFactValues)
			{
				newDTO.CompletedPresentedUploadMediaFactValues.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}