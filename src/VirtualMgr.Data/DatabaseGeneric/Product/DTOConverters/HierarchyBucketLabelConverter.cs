﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class HierarchyBucketLabelConverter
	{
	
		public static HierarchyBucketLabelEntity ToEntity(this HierarchyBucketLabelDTO dto)
		{
			return dto.ToEntity(new HierarchyBucketLabelEntity(), new Dictionary<object, object>());
		}

		public static HierarchyBucketLabelEntity ToEntity(this HierarchyBucketLabelDTO dto, HierarchyBucketLabelEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static HierarchyBucketLabelEntity ToEntity(this HierarchyBucketLabelDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new HierarchyBucketLabelEntity(), cache);
		}
	
		public static HierarchyBucketLabelDTO ToDTO(this HierarchyBucketLabelEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static HierarchyBucketLabelEntity ToEntity(this HierarchyBucketLabelDTO dto, HierarchyBucketLabelEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (HierarchyBucketLabelEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.HierarchyBucketId = dto.HierarchyBucketId;
			
			

			
			
			newEnt.LabelId = dto.LabelId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.HierarchyBucket = dto.HierarchyBucket.ToEntity(cache);
			
			newEnt.Label = dto.Label.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static HierarchyBucketLabelDTO ToDTO(this HierarchyBucketLabelEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (HierarchyBucketLabelDTO)cache[ent];
			
			var newDTO = new HierarchyBucketLabelDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.HierarchyBucketId = ent.HierarchyBucketId;
			

			newDTO.LabelId = ent.LabelId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.HierarchyBucket = ent.HierarchyBucket.ToDTO(cache);
			
			newDTO.Label = ent.Label.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}