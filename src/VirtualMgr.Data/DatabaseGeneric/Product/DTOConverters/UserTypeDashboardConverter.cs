﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class UserTypeDashboardConverter
	{
	
		public static UserTypeDashboardEntity ToEntity(this UserTypeDashboardDTO dto)
		{
			return dto.ToEntity(new UserTypeDashboardEntity(), new Dictionary<object, object>());
		}

		public static UserTypeDashboardEntity ToEntity(this UserTypeDashboardDTO dto, UserTypeDashboardEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static UserTypeDashboardEntity ToEntity(this UserTypeDashboardDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new UserTypeDashboardEntity(), cache);
		}
	
		public static UserTypeDashboardDTO ToDTO(this UserTypeDashboardEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static UserTypeDashboardEntity ToEntity(this UserTypeDashboardDTO dto, UserTypeDashboardEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (UserTypeDashboardEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			if (dto.Config == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)UserTypeDashboardFieldIndex.Config, "");
				
			}
			
			newEnt.Config = dto.Config;
			
			

			
			
			newEnt.Id = dto.Id;
			
			
			

			
			
			newEnt.PlayOffline = dto.PlayOffline;
			
			

			
			
			newEnt.PublishingGroupResourceId = dto.PublishingGroupResourceId;
			
			

			
			
			newEnt.SortOrder = dto.SortOrder;
			
			

			
			
			if (dto.Title == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)UserTypeDashboardFieldIndex.Title, "");
				
			}
			
			newEnt.Title = dto.Title;
			
			

			
			
			if (dto.Tooltip == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)UserTypeDashboardFieldIndex.Tooltip, "");
				
			}
			
			newEnt.Tooltip = dto.Tooltip;
			
			

			
			
			newEnt.UserTypeId = dto.UserTypeId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.PublishingGroupResource = dto.PublishingGroupResource.ToEntity(cache);
			
			newEnt.UserType = dto.UserType.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static UserTypeDashboardDTO ToDTO(this UserTypeDashboardEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (UserTypeDashboardDTO)cache[ent];
			
			var newDTO = new UserTypeDashboardDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Config = ent.Config;
			

			newDTO.Id = ent.Id;
			

			newDTO.PlayOffline = ent.PlayOffline;
			

			newDTO.PublishingGroupResourceId = ent.PublishingGroupResourceId;
			

			newDTO.SortOrder = ent.SortOrder;
			

			newDTO.Title = ent.Title;
			

			newDTO.Tooltip = ent.Tooltip;
			

			newDTO.UserTypeId = ent.UserTypeId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.PublishingGroupResource = ent.PublishingGroupResource.ToDTO(cache);
			
			newDTO.UserType = ent.UserType.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}