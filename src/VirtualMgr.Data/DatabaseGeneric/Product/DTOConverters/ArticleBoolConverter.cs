﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class ArticleBoolConverter
	{
	
		public static ArticleBoolEntity ToEntity(this ArticleBoolDTO dto)
		{
			return dto.ToEntity(new ArticleBoolEntity(), new Dictionary<object, object>());
		}

		public static ArticleBoolEntity ToEntity(this ArticleBoolDTO dto, ArticleBoolEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static ArticleBoolEntity ToEntity(this ArticleBoolDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new ArticleBoolEntity(), cache);
		}
	
		public static ArticleBoolDTO ToDTO(this ArticleBoolEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static ArticleBoolEntity ToEntity(this ArticleBoolDTO dto, ArticleBoolEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (ArticleBoolEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.ArticleId = dto.ArticleId;
			
			

			
			
			newEnt.Index = dto.Index;
			
			

			
			
			newEnt.Name = dto.Name;
			
			

			
			
			newEnt.Value = dto.Value;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.Article = dto.Article.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static ArticleBoolDTO ToDTO(this ArticleBoolEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (ArticleBoolDTO)cache[ent];
			
			var newDTO = new ArticleBoolDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.ArticleId = ent.ArticleId;
			

			newDTO.Index = ent.Index;
			

			newDTO.Name = ent.Name;
			

			newDTO.Value = ent.Value;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.Article = ent.Article.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}