﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class PublishingGroupActorTypeConverter
	{
	
		public static PublishingGroupActorTypeEntity ToEntity(this PublishingGroupActorTypeDTO dto)
		{
			return dto.ToEntity(new PublishingGroupActorTypeEntity(), new Dictionary<object, object>());
		}

		public static PublishingGroupActorTypeEntity ToEntity(this PublishingGroupActorTypeDTO dto, PublishingGroupActorTypeEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static PublishingGroupActorTypeEntity ToEntity(this PublishingGroupActorTypeDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new PublishingGroupActorTypeEntity(), cache);
		}
	
		public static PublishingGroupActorTypeDTO ToDTO(this PublishingGroupActorTypeEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static PublishingGroupActorTypeEntity ToEntity(this PublishingGroupActorTypeDTO dto, PublishingGroupActorTypeEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (PublishingGroupActorTypeEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Id = dto.Id;
			
			
			

			
			
			newEnt.Name = dto.Name;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			
			
			foreach (var related in dto.PublishingGroupActors)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.PublishingGroupActors.Contains(relatedEntity))
				{
					newEnt.PublishingGroupActors.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.PublishingGroupActorLabels)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.PublishingGroupActorLabels.Contains(relatedEntity))
				{
					newEnt.PublishingGroupActorLabels.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static PublishingGroupActorTypeDTO ToDTO(this PublishingGroupActorTypeEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (PublishingGroupActorTypeDTO)cache[ent];
			
			var newDTO = new PublishingGroupActorTypeDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Id = ent.Id;
			

			newDTO.Name = ent.Name;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			
			
			foreach (var related in ent.PublishingGroupActors)
			{
				newDTO.PublishingGroupActors.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.PublishingGroupActorLabels)
			{
				newDTO.PublishingGroupActorLabels.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}