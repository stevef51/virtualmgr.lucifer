﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class PublishingGroupActorLabelConverter
	{
	
		public static PublishingGroupActorLabelEntity ToEntity(this PublishingGroupActorLabelDTO dto)
		{
			return dto.ToEntity(new PublishingGroupActorLabelEntity(), new Dictionary<object, object>());
		}

		public static PublishingGroupActorLabelEntity ToEntity(this PublishingGroupActorLabelDTO dto, PublishingGroupActorLabelEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static PublishingGroupActorLabelEntity ToEntity(this PublishingGroupActorLabelDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new PublishingGroupActorLabelEntity(), cache);
		}
	
		public static PublishingGroupActorLabelDTO ToDTO(this PublishingGroupActorLabelEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static PublishingGroupActorLabelEntity ToEntity(this PublishingGroupActorLabelDTO dto, PublishingGroupActorLabelEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (PublishingGroupActorLabelEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.LabelId = dto.LabelId;
			
			

			
			
			newEnt.PublishingGroupId = dto.PublishingGroupId;
			
			

			
			
			newEnt.PublishingTypeId = dto.PublishingTypeId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.Label = dto.Label.ToEntity(cache);
			
			newEnt.PublishingGroup = dto.PublishingGroup.ToEntity(cache);
			
			newEnt.PublishingGroupActorType = dto.PublishingGroupActorType.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static PublishingGroupActorLabelDTO ToDTO(this PublishingGroupActorLabelEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (PublishingGroupActorLabelDTO)cache[ent];
			
			var newDTO = new PublishingGroupActorLabelDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.LabelId = ent.LabelId;
			

			newDTO.PublishingGroupId = ent.PublishingGroupId;
			

			newDTO.PublishingTypeId = ent.PublishingTypeId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.Label = ent.Label.ToDTO(cache);
			
			newDTO.PublishingGroup = ent.PublishingGroup.ToDTO(cache);
			
			newDTO.PublishingGroupActorType = ent.PublishingGroupActorType.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}