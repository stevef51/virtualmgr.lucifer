﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class MediaFolderTranslatedConverter
	{
	
		public static MediaFolderTranslatedEntity ToEntity(this MediaFolderTranslatedDTO dto)
		{
			return dto.ToEntity(new MediaFolderTranslatedEntity(), new Dictionary<object, object>());
		}

		public static MediaFolderTranslatedEntity ToEntity(this MediaFolderTranslatedDTO dto, MediaFolderTranslatedEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static MediaFolderTranslatedEntity ToEntity(this MediaFolderTranslatedDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new MediaFolderTranslatedEntity(), cache);
		}
	
		public static MediaFolderTranslatedDTO ToDTO(this MediaFolderTranslatedEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static MediaFolderTranslatedEntity ToEntity(this MediaFolderTranslatedDTO dto, MediaFolderTranslatedEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (MediaFolderTranslatedEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.CultureName = dto.CultureName;
			
			

			
			
			newEnt.Id = dto.Id;
			
			

			
			
			newEnt.Text = dto.Text;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.MediaFolder = dto.MediaFolder.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static MediaFolderTranslatedDTO ToDTO(this MediaFolderTranslatedEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (MediaFolderTranslatedDTO)cache[ent];
			
			var newDTO = new MediaFolderTranslatedDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.CultureName = ent.CultureName;
			

			newDTO.Id = ent.Id;
			

			newDTO.Text = ent.Text;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.MediaFolder = ent.MediaFolder.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}