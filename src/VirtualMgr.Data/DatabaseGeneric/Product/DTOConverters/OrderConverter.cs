﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class OrderConverter
	{
	
		public static OrderEntity ToEntity(this OrderDTO dto)
		{
			return dto.ToEntity(new OrderEntity(), new Dictionary<object, object>());
		}

		public static OrderEntity ToEntity(this OrderDTO dto, OrderEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static OrderEntity ToEntity(this OrderDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new OrderEntity(), cache);
		}
	
		public static OrderDTO ToDTO(this OrderEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static OrderEntity ToEntity(this OrderDTO dto, OrderEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (OrderEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Archived = dto.Archived;
			
			

			
			
			if (dto.CancelledDateUtc == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)OrderFieldIndex.CancelledDateUtc, default(System.DateTime));
				
			}
			
			newEnt.CancelledDateUtc = dto.CancelledDateUtc;
			
			

			
			
			newEnt.DateCreated = dto.DateCreated;
			
			

			
			
			if (dto.ExpectedDeliveryDate == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)OrderFieldIndex.ExpectedDeliveryDate, default(System.DateTime));
				
			}
			
			newEnt.ExpectedDeliveryDate = dto.ExpectedDeliveryDate;
			
			

			
			
			if (dto.ExtensionOfOrderId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)OrderFieldIndex.ExtensionOfOrderId, default(System.Guid));
				
			}
			
			newEnt.ExtensionOfOrderId = dto.ExtensionOfOrderId;
			
			

			
			
			newEnt.FacilityStructureId = dto.FacilityStructureId;
			
			

			
			
			newEnt.Id = dto.Id;
			
			

			
			
			newEnt.MaxCoupons = dto.MaxCoupons;
			
			

			
			
			newEnt.OrderedById = dto.OrderedById;
			
			

			
			
			if (dto.PendingExpiryDate == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)OrderFieldIndex.PendingExpiryDate, default(System.DateTime));
				
			}
			
			newEnt.PendingExpiryDate = dto.PendingExpiryDate;
			
			

			
			
			newEnt.ProductCatalogId = dto.ProductCatalogId;
			
			

			
			
			if (dto.ProjectJobTaskId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)OrderFieldIndex.ProjectJobTaskId, default(System.Guid));
				
			}
			
			newEnt.ProjectJobTaskId = dto.ProjectJobTaskId;
			
			

			
			
			if (dto.TotalPrice == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)OrderFieldIndex.TotalPrice, default(System.Decimal));
				
			}
			
			newEnt.TotalPrice = dto.TotalPrice;
			
			

			
			
			if (dto.WorkflowState == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)OrderFieldIndex.WorkflowState, "");
				
			}
			
			newEnt.WorkflowState = dto.WorkflowState;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.FacilityStructure = dto.FacilityStructure.ToEntity(cache);
			
			newEnt.ProjectJobTask = dto.ProjectJobTask.ToEntity(cache);
			
			newEnt.ProductCatalog = dto.ProductCatalog.ToEntity(cache);
			
			newEnt.UserData = dto.UserData.ToEntity(cache);
			
			
			
			foreach (var related in dto.OrderItems)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.OrderItems.Contains(relatedEntity))
				{
					newEnt.OrderItems.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.OrderMedia)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.OrderMedia.Contains(relatedEntity))
				{
					newEnt.OrderMedia.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static OrderDTO ToDTO(this OrderEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (OrderDTO)cache[ent];
			
			var newDTO = new OrderDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Archived = ent.Archived;
			

			newDTO.CancelledDateUtc = ent.CancelledDateUtc;
			

			newDTO.DateCreated = ent.DateCreated;
			

			newDTO.ExpectedDeliveryDate = ent.ExpectedDeliveryDate;
			

			newDTO.ExtensionOfOrderId = ent.ExtensionOfOrderId;
			

			newDTO.FacilityStructureId = ent.FacilityStructureId;
			

			newDTO.Id = ent.Id;
			

			newDTO.MaxCoupons = ent.MaxCoupons;
			

			newDTO.OrderedById = ent.OrderedById;
			

			newDTO.PendingExpiryDate = ent.PendingExpiryDate;
			

			newDTO.ProductCatalogId = ent.ProductCatalogId;
			

			newDTO.ProjectJobTaskId = ent.ProjectJobTaskId;
			

			newDTO.TotalPrice = ent.TotalPrice;
			

			newDTO.WorkflowState = ent.WorkflowState;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.FacilityStructure = ent.FacilityStructure.ToDTO(cache);
			
			newDTO.ProjectJobTask = ent.ProjectJobTask.ToDTO(cache);
			
			newDTO.ProductCatalog = ent.ProductCatalog.ToDTO(cache);
			
			newDTO.UserData = ent.UserData.ToDTO(cache);
			
			
			
			foreach (var related in ent.OrderItems)
			{
				newDTO.OrderItems.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.OrderMedia)
			{
				newDTO.OrderMedia.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}