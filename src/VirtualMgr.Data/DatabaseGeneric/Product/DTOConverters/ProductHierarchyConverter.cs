﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class ProductHierarchyConverter
	{
	
		public static ProductHierarchyEntity ToEntity(this ProductHierarchyDTO dto)
		{
			return dto.ToEntity(new ProductHierarchyEntity(), new Dictionary<object, object>());
		}

		public static ProductHierarchyEntity ToEntity(this ProductHierarchyDTO dto, ProductHierarchyEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static ProductHierarchyEntity ToEntity(this ProductHierarchyDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new ProductHierarchyEntity(), cache);
		}
	
		public static ProductHierarchyDTO ToDTO(this ProductHierarchyEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static ProductHierarchyEntity ToEntity(this ProductHierarchyDTO dto, ProductHierarchyEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (ProductHierarchyEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.ChildProductId = dto.ChildProductId;
			
			

			
			
			if (dto.Name == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProductHierarchyFieldIndex.Name, "");
				
			}
			
			newEnt.Name = dto.Name;
			
			

			
			
			newEnt.ParentProductId = dto.ParentProductId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.ChildProduct = dto.ChildProduct.ToEntity(cache);
			
			newEnt.ParentProduct = dto.ParentProduct.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static ProductHierarchyDTO ToDTO(this ProductHierarchyEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (ProductHierarchyDTO)cache[ent];
			
			var newDTO = new ProductHierarchyDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.ChildProductId = ent.ChildProductId;
			

			newDTO.Name = ent.Name;
			

			newDTO.ParentProductId = ent.ParentProductId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.ChildProduct = ent.ChildProduct.ToDTO(cache);
			
			newDTO.ParentProduct = ent.ParentProduct.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}