﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class HierarchyBucketConverter
	{
	
		public static HierarchyBucketEntity ToEntity(this HierarchyBucketDTO dto)
		{
			return dto.ToEntity(new HierarchyBucketEntity(), new Dictionary<object, object>());
		}

		public static HierarchyBucketEntity ToEntity(this HierarchyBucketDTO dto, HierarchyBucketEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static HierarchyBucketEntity ToEntity(this HierarchyBucketDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new HierarchyBucketEntity(), cache);
		}
	
		public static HierarchyBucketDTO ToDTO(this HierarchyBucketEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static HierarchyBucketEntity ToEntity(this HierarchyBucketDTO dto, HierarchyBucketEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (HierarchyBucketEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.AssignTasksThroughLabels = dto.AssignTasksThroughLabels;
			
			

			
			
			if (dto.DefaultSaturdayTimesheetItemTypeId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)HierarchyBucketFieldIndex.DefaultSaturdayTimesheetItemTypeId, default(System.Int32));
				
			}
			
			newEnt.DefaultSaturdayTimesheetItemTypeId = dto.DefaultSaturdayTimesheetItemTypeId;
			
			

			
			
			if (dto.DefaultSundayTimesheetItemTypeId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)HierarchyBucketFieldIndex.DefaultSundayTimesheetItemTypeId, default(System.Int32));
				
			}
			
			newEnt.DefaultSundayTimesheetItemTypeId = dto.DefaultSundayTimesheetItemTypeId;
			
			

			
			
			if (dto.DefaultWeekDayTimesheetItemTypeId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)HierarchyBucketFieldIndex.DefaultWeekDayTimesheetItemTypeId, default(System.Int32));
				
			}
			
			newEnt.DefaultWeekDayTimesheetItemTypeId = dto.DefaultWeekDayTimesheetItemTypeId;
			
			

			
			
			newEnt.HierarchyId = dto.HierarchyId;
			
			

			
			
			newEnt.Id = dto.Id;
			
			
			

			
			
			if (dto.IsPrimary == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)HierarchyBucketFieldIndex.IsPrimary, default(System.Boolean));
				
			}
			
			newEnt.IsPrimary = dto.IsPrimary;
			
			

			
			
			newEnt.LeftIndex = dto.LeftIndex;
			
			

			
			
			newEnt.Name = dto.Name;
			
			

			
			
			newEnt.OpenSecurityThroughLabels = dto.OpenSecurityThroughLabels;
			
			

			
			
			if (dto.ParentId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)HierarchyBucketFieldIndex.ParentId, default(System.Int32));
				
			}
			
			newEnt.ParentId = dto.ParentId;
			
			

			
			
			newEnt.RightIndex = dto.RightIndex;
			
			

			
			
			newEnt.RoleId = dto.RoleId;
			
			

			
			
			newEnt.SortOrder = dto.SortOrder;
			
			

			
			
			newEnt.TasksCanBeClaimed = dto.TasksCanBeClaimed;
			
			

			
			
			if (dto.UserId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)HierarchyBucketFieldIndex.UserId, default(System.Guid));
				
			}
			
			newEnt.UserId = dto.UserId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.TimesheetItemType = dto.TimesheetItemType.ToEntity(cache);
			
			newEnt.TimesheetItemType_ = dto.TimesheetItemType_.ToEntity(cache);
			
			newEnt.TimesheetItemType__ = dto.TimesheetItemType__.ToEntity(cache);
			
			newEnt.Hierarchy = dto.Hierarchy.ToEntity(cache);
			
			newEnt.Parent = dto.Parent.ToEntity(cache);
			
			newEnt.HierarchyRole = dto.HierarchyRole.ToEntity(cache);
			
			newEnt.UserData = dto.UserData.ToEntity(cache);
			
			
			
			foreach (var related in dto.ProjectJobTasks)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProjectJobTasks.Contains(relatedEntity))
				{
					newEnt.ProjectJobTasks.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.Children)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.Children.Contains(relatedEntity))
				{
					newEnt.Children.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.HierarchyBucketLabels)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.HierarchyBucketLabels.Contains(relatedEntity))
				{
					newEnt.HierarchyBucketLabels.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.HierarchyBucketOverrides)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.HierarchyBucketOverrides.Contains(relatedEntity))
				{
					newEnt.HierarchyBucketOverrides.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static HierarchyBucketDTO ToDTO(this HierarchyBucketEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (HierarchyBucketDTO)cache[ent];
			
			var newDTO = new HierarchyBucketDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.AssignTasksThroughLabels = ent.AssignTasksThroughLabels;
			

			newDTO.DefaultSaturdayTimesheetItemTypeId = ent.DefaultSaturdayTimesheetItemTypeId;
			

			newDTO.DefaultSundayTimesheetItemTypeId = ent.DefaultSundayTimesheetItemTypeId;
			

			newDTO.DefaultWeekDayTimesheetItemTypeId = ent.DefaultWeekDayTimesheetItemTypeId;
			

			newDTO.HierarchyId = ent.HierarchyId;
			

			newDTO.Id = ent.Id;
			

			newDTO.IsPrimary = ent.IsPrimary;
			

			newDTO.LeftIndex = ent.LeftIndex;
			

			newDTO.Name = ent.Name;
			

			newDTO.OpenSecurityThroughLabels = ent.OpenSecurityThroughLabels;
			

			newDTO.ParentId = ent.ParentId;
			

			newDTO.RightIndex = ent.RightIndex;
			

			newDTO.RoleId = ent.RoleId;
			

			newDTO.SortOrder = ent.SortOrder;
			

			newDTO.TasksCanBeClaimed = ent.TasksCanBeClaimed;
			

			newDTO.UserId = ent.UserId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.TimesheetItemType = ent.TimesheetItemType.ToDTO(cache);
			
			newDTO.TimesheetItemType_ = ent.TimesheetItemType_.ToDTO(cache);
			
			newDTO.TimesheetItemType__ = ent.TimesheetItemType__.ToDTO(cache);
			
			newDTO.Hierarchy = ent.Hierarchy.ToDTO(cache);
			
			newDTO.Parent = ent.Parent.ToDTO(cache);
			
			newDTO.HierarchyRole = ent.HierarchyRole.ToDTO(cache);
			
			newDTO.UserData = ent.UserData.ToDTO(cache);
			
			
			
			foreach (var related in ent.ProjectJobTasks)
			{
				newDTO.ProjectJobTasks.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.Children)
			{
				newDTO.Children.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.HierarchyBucketLabels)
			{
				newDTO.HierarchyBucketLabels.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.HierarchyBucketOverrides)
			{
				newDTO.HierarchyBucketOverrides.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}