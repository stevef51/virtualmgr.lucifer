﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class ArticleSubArticleConverter
	{
	
		public static ArticleSubArticleEntity ToEntity(this ArticleSubArticleDTO dto)
		{
			return dto.ToEntity(new ArticleSubArticleEntity(), new Dictionary<object, object>());
		}

		public static ArticleSubArticleEntity ToEntity(this ArticleSubArticleDTO dto, ArticleSubArticleEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static ArticleSubArticleEntity ToEntity(this ArticleSubArticleDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new ArticleSubArticleEntity(), cache);
		}
	
		public static ArticleSubArticleDTO ToDTO(this ArticleSubArticleEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static ArticleSubArticleEntity ToEntity(this ArticleSubArticleDTO dto, ArticleSubArticleEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (ArticleSubArticleEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.ArticleId = dto.ArticleId;
			
			

			
			
			newEnt.Index = dto.Index;
			
			

			
			
			newEnt.Name = dto.Name;
			
			

			
			
			newEnt.SubArticleId = dto.SubArticleId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.Article = dto.Article.ToEntity(cache);
			
			newEnt.SubArticle = dto.SubArticle.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static ArticleSubArticleDTO ToDTO(this ArticleSubArticleEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (ArticleSubArticleDTO)cache[ent];
			
			var newDTO = new ArticleSubArticleDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.ArticleId = ent.ArticleId;
			

			newDTO.Index = ent.Index;
			

			newDTO.Name = ent.Name;
			

			newDTO.SubArticleId = ent.SubArticleId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.Article = ent.Article.ToDTO(cache);
			
			newDTO.SubArticle = ent.SubArticle.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}