﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class QrtzhistoryConverter
	{
	
		public static QrtzhistoryEntity ToEntity(this QrtzhistoryDTO dto)
		{
			return dto.ToEntity(new QrtzhistoryEntity(), new Dictionary<object, object>());
		}

		public static QrtzhistoryEntity ToEntity(this QrtzhistoryDTO dto, QrtzhistoryEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static QrtzhistoryEntity ToEntity(this QrtzhistoryDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new QrtzhistoryEntity(), cache);
		}
	
		public static QrtzhistoryDTO ToDTO(this QrtzhistoryEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static QrtzhistoryEntity ToEntity(this QrtzhistoryDTO dto, QrtzhistoryEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (QrtzhistoryEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Completed = dto.Completed;
			
			

			
			
			newEnt.Group = dto.Group;
			
			

			
			
			newEnt.Id = dto.Id;
			
			

			
			
			newEnt.Job = dto.Job;
			
			

			
			
			newEnt.Notes = dto.Notes;
			
			

			
			
			newEnt.RunTime = dto.RunTime;
			
			

			
			
			newEnt.Trigger = dto.Trigger;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static QrtzhistoryDTO ToDTO(this QrtzhistoryEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (QrtzhistoryDTO)cache[ent];
			
			var newDTO = new QrtzhistoryDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Completed = ent.Completed;
			

			newDTO.Group = ent.Group;
			

			newDTO.Id = ent.Id;
			

			newDTO.Job = ent.Job;
			

			newDTO.Notes = ent.Notes;
			

			newDTO.RunTime = ent.RunTime;
			

			newDTO.Trigger = ent.Trigger;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}