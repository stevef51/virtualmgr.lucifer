﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class MeasurementUnitConverter
	{
	
		public static MeasurementUnitEntity ToEntity(this MeasurementUnitDTO dto)
		{
			return dto.ToEntity(new MeasurementUnitEntity(), new Dictionary<object, object>());
		}

		public static MeasurementUnitEntity ToEntity(this MeasurementUnitDTO dto, MeasurementUnitEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static MeasurementUnitEntity ToEntity(this MeasurementUnitDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new MeasurementUnitEntity(), cache);
		}
	
		public static MeasurementUnitDTO ToDTO(this MeasurementUnitEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static MeasurementUnitEntity ToEntity(this MeasurementUnitDTO dto, MeasurementUnitEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (MeasurementUnitEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Dimensions = dto.Dimensions;
			
			

			
			
			newEnt.Id = dto.Id;
			
			

			
			
			newEnt.LongName = dto.LongName;
			
			

			
			
			newEnt.PerMetric = dto.PerMetric;
			
			

			
			
			newEnt.ShortName = dto.ShortName;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			
			
			foreach (var related in dto.SiteFeatures)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.SiteFeatures.Contains(relatedEntity))
				{
					newEnt.SiteFeatures.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.WorkLoadingStandards)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.WorkLoadingStandards.Contains(relatedEntity))
				{
					newEnt.WorkLoadingStandards.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static MeasurementUnitDTO ToDTO(this MeasurementUnitEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (MeasurementUnitDTO)cache[ent];
			
			var newDTO = new MeasurementUnitDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Dimensions = ent.Dimensions;
			

			newDTO.Id = ent.Id;
			

			newDTO.LongName = ent.LongName;
			

			newDTO.PerMetric = ent.PerMetric;
			

			newDTO.ShortName = ent.ShortName;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			
			
			foreach (var related in ent.SiteFeatures)
			{
				newDTO.SiteFeatures.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.WorkLoadingStandards)
			{
				newDTO.WorkLoadingStandards.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}