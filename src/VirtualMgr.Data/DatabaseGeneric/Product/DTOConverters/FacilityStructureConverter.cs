﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class FacilityStructureConverter
	{
	
		public static FacilityStructureEntity ToEntity(this FacilityStructureDTO dto)
		{
			return dto.ToEntity(new FacilityStructureEntity(), new Dictionary<object, object>());
		}

		public static FacilityStructureEntity ToEntity(this FacilityStructureDTO dto, FacilityStructureEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static FacilityStructureEntity ToEntity(this FacilityStructureDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new FacilityStructureEntity(), cache);
		}
	
		public static FacilityStructureDTO ToDTO(this FacilityStructureEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static FacilityStructureEntity ToEntity(this FacilityStructureDTO dto, FacilityStructureEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (FacilityStructureEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			if (dto.AddressId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)FacilityStructureFieldIndex.AddressId, default(System.Int32));
				
			}
			
			newEnt.AddressId = dto.AddressId;
			
			

			
			
			newEnt.Archived = dto.Archived;
			
			

			
			
			if (dto.Description == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)FacilityStructureFieldIndex.Description, "");
				
			}
			
			newEnt.Description = dto.Description;
			
			

			
			
			if (dto.FloorHeight == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)FacilityStructureFieldIndex.FloorHeight, default(System.Decimal));
				
			}
			
			newEnt.FloorHeight = dto.FloorHeight;
			
			

			
			
			if (dto.FloorPlanId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)FacilityStructureFieldIndex.FloorPlanId, default(System.Int32));
				
			}
			
			newEnt.FloorPlanId = dto.FloorPlanId;
			
			

			
			
			if (dto.HierarchyId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)FacilityStructureFieldIndex.HierarchyId, default(System.Int32));
				
			}
			
			newEnt.HierarchyId = dto.HierarchyId;
			
			

			
			
			newEnt.Id = dto.Id;
			
			

			
			
			if (dto.Level == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)FacilityStructureFieldIndex.Level, default(System.Int32));
				
			}
			
			newEnt.Level = dto.Level;
			
			

			
			
			if (dto.MediaId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)FacilityStructureFieldIndex.MediaId, default(System.Guid));
				
			}
			
			newEnt.MediaId = dto.MediaId;
			
			

			
			
			if (dto.Name == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)FacilityStructureFieldIndex.Name, "");
				
			}
			
			newEnt.Name = dto.Name;
			
			

			
			
			if (dto.ParentId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)FacilityStructureFieldIndex.ParentId, default(System.Int32));
				
			}
			
			newEnt.ParentId = dto.ParentId;
			
			

			
			
			if (dto.SiteId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)FacilityStructureFieldIndex.SiteId, default(System.Guid));
				
			}
			
			newEnt.SiteId = dto.SiteId;
			
			

			
			
			newEnt.SortOrder = dto.SortOrder;
			
			

			
			
			newEnt.StructureType = dto.StructureType;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.Address = dto.Address.ToEntity(cache);
			
			newEnt.Parent = dto.Parent.ToEntity(cache);
			
			newEnt.FloorPlan = dto.FloorPlan.ToEntity(cache);
			
			newEnt.Hierarchy = dto.Hierarchy.ToEntity(cache);
			
			newEnt.Media = dto.Media.ToEntity(cache);
			
			newEnt.UserData = dto.UserData.ToEntity(cache);
			
			
			
			foreach (var related in dto.Beacons)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.Beacons.Contains(relatedEntity))
				{
					newEnt.Beacons.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.Children)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.Children.Contains(relatedEntity))
				{
					newEnt.Children.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ProjectJobTaskTypeFacilityOverrides)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProjectJobTaskTypeFacilityOverrides.Contains(relatedEntity))
				{
					newEnt.ProjectJobTaskTypeFacilityOverrides.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.Orders)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.Orders.Contains(relatedEntity))
				{
					newEnt.Orders.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ProductCatalogs)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProductCatalogs.Contains(relatedEntity))
				{
					newEnt.ProductCatalogs.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ProductCatalogFacilityStructureRules)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProductCatalogFacilityStructureRules.Contains(relatedEntity))
				{
					newEnt.ProductCatalogFacilityStructureRules.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static FacilityStructureDTO ToDTO(this FacilityStructureEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (FacilityStructureDTO)cache[ent];
			
			var newDTO = new FacilityStructureDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.AddressId = ent.AddressId;
			

			newDTO.Archived = ent.Archived;
			

			newDTO.Description = ent.Description;
			

			newDTO.FloorHeight = ent.FloorHeight;
			

			newDTO.FloorPlanId = ent.FloorPlanId;
			

			newDTO.HierarchyId = ent.HierarchyId;
			

			newDTO.Id = ent.Id;
			

			newDTO.Level = ent.Level;
			

			newDTO.MediaId = ent.MediaId;
			

			newDTO.Name = ent.Name;
			

			newDTO.ParentId = ent.ParentId;
			

			newDTO.SiteId = ent.SiteId;
			

			newDTO.SortOrder = ent.SortOrder;
			

			newDTO.StructureType = ent.StructureType;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.Address = ent.Address.ToDTO(cache);
			
			newDTO.Parent = ent.Parent.ToDTO(cache);
			
			newDTO.FloorPlan = ent.FloorPlan.ToDTO(cache);
			
			newDTO.Hierarchy = ent.Hierarchy.ToDTO(cache);
			
			newDTO.Media = ent.Media.ToDTO(cache);
			
			newDTO.UserData = ent.UserData.ToDTO(cache);
			
			
			
			foreach (var related in ent.Beacons)
			{
				newDTO.Beacons.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.Children)
			{
				newDTO.Children.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ProjectJobTaskTypeFacilityOverrides)
			{
				newDTO.ProjectJobTaskTypeFacilityOverrides.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.Orders)
			{
				newDTO.Orders.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ProductCatalogs)
			{
				newDTO.ProductCatalogs.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ProductCatalogFacilityStructureRules)
			{
				newDTO.ProductCatalogFacilityStructureRules.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}