﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class AspNetUserConverter
	{
	
		public static AspNetUserEntity ToEntity(this AspNetUserDTO dto)
		{
			return dto.ToEntity(new AspNetUserEntity(), new Dictionary<object, object>());
		}

		public static AspNetUserEntity ToEntity(this AspNetUserDTO dto, AspNetUserEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static AspNetUserEntity ToEntity(this AspNetUserDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new AspNetUserEntity(), cache);
		}
	
		public static AspNetUserDTO ToDTO(this AspNetUserEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static AspNetUserEntity ToEntity(this AspNetUserDTO dto, AspNetUserEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (AspNetUserEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.AccessFailedCount = dto.AccessFailedCount;
			
			

			
			
			if (dto.ConcurrencyStamp == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)AspNetUserFieldIndex.ConcurrencyStamp, "");
				
			}
			
			newEnt.ConcurrencyStamp = dto.ConcurrencyStamp;
			
			

			
			
			if (dto.Email == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)AspNetUserFieldIndex.Email, "");
				
			}
			
			newEnt.Email = dto.Email;
			
			

			
			
			newEnt.EmailConfirmed = dto.EmailConfirmed;
			
			

			
			
			newEnt.Id = dto.Id;
			
			

			
			
			newEnt.LockoutEnabled = dto.LockoutEnabled;
			
			

			
			
			if (dto.LockoutEnd == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)AspNetUserFieldIndex.LockoutEnd, default(System.DateTimeOffset));
				
			}
			
			newEnt.LockoutEnd = dto.LockoutEnd;
			
			

			
			
			if (dto.NormalizedEmail == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)AspNetUserFieldIndex.NormalizedEmail, "");
				
			}
			
			newEnt.NormalizedEmail = dto.NormalizedEmail;
			
			

			
			
			if (dto.NormalizedUserName == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)AspNetUserFieldIndex.NormalizedUserName, "");
				
			}
			
			newEnt.NormalizedUserName = dto.NormalizedUserName;
			
			

			
			
			if (dto.PasswordHash == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)AspNetUserFieldIndex.PasswordHash, "");
				
			}
			
			newEnt.PasswordHash = dto.PasswordHash;
			
			

			
			
			if (dto.PhoneNumber == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)AspNetUserFieldIndex.PhoneNumber, "");
				
			}
			
			newEnt.PhoneNumber = dto.PhoneNumber;
			
			

			
			
			newEnt.PhoneNumberConfirmed = dto.PhoneNumberConfirmed;
			
			

			
			
			if (dto.SecurityStamp == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)AspNetUserFieldIndex.SecurityStamp, "");
				
			}
			
			newEnt.SecurityStamp = dto.SecurityStamp;
			
			

			
			
			newEnt.TwoFactorEnabled = dto.TwoFactorEnabled;
			
			

			
			
			if (dto.UserName == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)AspNetUserFieldIndex.UserName, "");
				
			}
			
			newEnt.UserName = dto.UserName;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			
			
			foreach (var related in dto.AspNetUserClaims)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.AspNetUserClaims.Contains(relatedEntity))
				{
					newEnt.AspNetUserClaims.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.AspNetUserLogins)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.AspNetUserLogins.Contains(relatedEntity))
				{
					newEnt.AspNetUserLogins.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.AspNetUserRoles)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.AspNetUserRoles.Contains(relatedEntity))
				{
					newEnt.AspNetUserRoles.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.AspNetUserTokens)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.AspNetUserTokens.Contains(relatedEntity))
				{
					newEnt.AspNetUserTokens.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.UserDatas)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.UserDatas.Contains(relatedEntity))
				{
					newEnt.UserDatas.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static AspNetUserDTO ToDTO(this AspNetUserEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (AspNetUserDTO)cache[ent];
			
			var newDTO = new AspNetUserDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.AccessFailedCount = ent.AccessFailedCount;
			

			newDTO.ConcurrencyStamp = ent.ConcurrencyStamp;
			

			newDTO.Email = ent.Email;
			

			newDTO.EmailConfirmed = ent.EmailConfirmed;
			

			newDTO.Id = ent.Id;
			

			newDTO.LockoutEnabled = ent.LockoutEnabled;
			

			newDTO.LockoutEnd = ent.LockoutEnd;
			

			newDTO.NormalizedEmail = ent.NormalizedEmail;
			

			newDTO.NormalizedUserName = ent.NormalizedUserName;
			

			newDTO.PasswordHash = ent.PasswordHash;
			

			newDTO.PhoneNumber = ent.PhoneNumber;
			

			newDTO.PhoneNumberConfirmed = ent.PhoneNumberConfirmed;
			

			newDTO.SecurityStamp = ent.SecurityStamp;
			

			newDTO.TwoFactorEnabled = ent.TwoFactorEnabled;
			

			newDTO.UserName = ent.UserName;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			
			
			foreach (var related in ent.AspNetUserClaims)
			{
				newDTO.AspNetUserClaims.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.AspNetUserLogins)
			{
				newDTO.AspNetUserLogins.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.AspNetUserRoles)
			{
				newDTO.AspNetUserRoles.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.AspNetUserTokens)
			{
				newDTO.AspNetUserTokens.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.UserDatas)
			{
				newDTO.UserDatas.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}