﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class TabletUuidConverter
	{
	
		public static TabletUuidEntity ToEntity(this TabletUuidDTO dto)
		{
			return dto.ToEntity(new TabletUuidEntity(), new Dictionary<object, object>());
		}

		public static TabletUuidEntity ToEntity(this TabletUuidDTO dto, TabletUuidEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static TabletUuidEntity ToEntity(this TabletUuidDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new TabletUuidEntity(), cache);
		}
	
		public static TabletUuidDTO ToDTO(this TabletUuidEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static TabletUuidEntity ToEntity(this TabletUuidDTO dto, TabletUuidEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (TabletUuidEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			if (dto.Description == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)TabletUuidFieldIndex.Description, "");
				
			}
			
			newEnt.Description = dto.Description;
			
			

			
			
			if (dto.LastContactUtc == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)TabletUuidFieldIndex.LastContactUtc, default(System.DateTime));
				
			}
			
			newEnt.LastContactUtc = dto.LastContactUtc;
			
			

			
			
			if (dto.LastGpslocationId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)TabletUuidFieldIndex.LastGpslocationId, default(System.Int64));
				
			}
			
			newEnt.LastGpslocationId = dto.LastGpslocationId;
			
			

			
			
			if (dto.LastRange == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)TabletUuidFieldIndex.LastRange, default(System.Int32));
				
			}
			
			newEnt.LastRange = dto.LastRange;
			
			

			
			
			if (dto.LastRangeUtc == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)TabletUuidFieldIndex.LastRangeUtc, default(System.DateTime));
				
			}
			
			newEnt.LastRangeUtc = dto.LastRangeUtc;
			
			

			
			
			if (dto.LastUserId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)TabletUuidFieldIndex.LastUserId, default(System.Guid));
				
			}
			
			newEnt.LastUserId = dto.LastUserId;
			
			

			
			
			if (dto.TabletId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)TabletUuidFieldIndex.TabletId, default(System.Int32));
				
			}
			
			newEnt.TabletId = dto.TabletId;
			
			

			
			
			newEnt.Uuid = dto.Uuid;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.Tablet = dto.Tablet.ToEntity(cache);
			
			newEnt.UserData = dto.UserData.ToEntity(cache);
			
			
			
			foreach (var related in dto.BeaconSightings)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.BeaconSightings.Contains(relatedEntity))
				{
					newEnt.BeaconSightings.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.Tablets)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.Tablets.Contains(relatedEntity))
				{
					newEnt.Tablets.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ESSensorReadings)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ESSensorReadings.Contains(relatedEntity))
				{
					newEnt.ESSensorReadings.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static TabletUuidDTO ToDTO(this TabletUuidEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (TabletUuidDTO)cache[ent];
			
			var newDTO = new TabletUuidDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Description = ent.Description;
			

			newDTO.LastContactUtc = ent.LastContactUtc;
			

			newDTO.LastGpslocationId = ent.LastGpslocationId;
			

			newDTO.LastRange = ent.LastRange;
			

			newDTO.LastRangeUtc = ent.LastRangeUtc;
			

			newDTO.LastUserId = ent.LastUserId;
			

			newDTO.TabletId = ent.TabletId;
			

			newDTO.Uuid = ent.Uuid;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.Tablet = ent.Tablet.ToDTO(cache);
			
			newDTO.UserData = ent.UserData.ToDTO(cache);
			
			
			
			foreach (var related in ent.BeaconSightings)
			{
				newDTO.BeaconSightings.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.Tablets)
			{
				newDTO.Tablets.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ESSensorReadings)
			{
				newDTO.ESSensorReadings.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}