﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class AspNetRoleClaimConverter
	{
	
		public static AspNetRoleClaimEntity ToEntity(this AspNetRoleClaimDTO dto)
		{
			return dto.ToEntity(new AspNetRoleClaimEntity(), new Dictionary<object, object>());
		}

		public static AspNetRoleClaimEntity ToEntity(this AspNetRoleClaimDTO dto, AspNetRoleClaimEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static AspNetRoleClaimEntity ToEntity(this AspNetRoleClaimDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new AspNetRoleClaimEntity(), cache);
		}
	
		public static AspNetRoleClaimDTO ToDTO(this AspNetRoleClaimEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static AspNetRoleClaimEntity ToEntity(this AspNetRoleClaimDTO dto, AspNetRoleClaimEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (AspNetRoleClaimEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			if (dto.ClaimType == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)AspNetRoleClaimFieldIndex.ClaimType, "");
				
			}
			
			newEnt.ClaimType = dto.ClaimType;
			
			

			
			
			if (dto.ClaimValue == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)AspNetRoleClaimFieldIndex.ClaimValue, "");
				
			}
			
			newEnt.ClaimValue = dto.ClaimValue;
			
			

			
			
			newEnt.Id = dto.Id;
			
			
			

			
			
			newEnt.RoleId = dto.RoleId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.AspNetRole = dto.AspNetRole.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static AspNetRoleClaimDTO ToDTO(this AspNetRoleClaimEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (AspNetRoleClaimDTO)cache[ent];
			
			var newDTO = new AspNetRoleClaimDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.ClaimType = ent.ClaimType;
			

			newDTO.ClaimValue = ent.ClaimValue;
			

			newDTO.Id = ent.Id;
			

			newDTO.RoleId = ent.RoleId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.AspNetRole = ent.AspNetRole.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}