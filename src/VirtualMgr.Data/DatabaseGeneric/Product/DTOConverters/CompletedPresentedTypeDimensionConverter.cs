﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class CompletedPresentedTypeDimensionConverter
	{
	
		public static CompletedPresentedTypeDimensionEntity ToEntity(this CompletedPresentedTypeDimensionDTO dto)
		{
			return dto.ToEntity(new CompletedPresentedTypeDimensionEntity(), new Dictionary<object, object>());
		}

		public static CompletedPresentedTypeDimensionEntity ToEntity(this CompletedPresentedTypeDimensionDTO dto, CompletedPresentedTypeDimensionEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static CompletedPresentedTypeDimensionEntity ToEntity(this CompletedPresentedTypeDimensionDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new CompletedPresentedTypeDimensionEntity(), cache);
		}
	
		public static CompletedPresentedTypeDimensionDTO ToDTO(this CompletedPresentedTypeDimensionEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static CompletedPresentedTypeDimensionEntity ToEntity(this CompletedPresentedTypeDimensionDTO dto, CompletedPresentedTypeDimensionEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (CompletedPresentedTypeDimensionEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Id = dto.Id;
			
			

			
			
			newEnt.Type = dto.Type;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			
			
			foreach (var related in dto.CompletedWorkingDocumentPresentedFacts)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.CompletedWorkingDocumentPresentedFacts.Contains(relatedEntity))
				{
					newEnt.CompletedWorkingDocumentPresentedFacts.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static CompletedPresentedTypeDimensionDTO ToDTO(this CompletedPresentedTypeDimensionEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (CompletedPresentedTypeDimensionDTO)cache[ent];
			
			var newDTO = new CompletedPresentedTypeDimensionDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Id = ent.Id;
			

			newDTO.Type = ent.Type;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			
			
			foreach (var related in ent.CompletedWorkingDocumentPresentedFacts)
			{
				newDTO.CompletedWorkingDocumentPresentedFacts.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}