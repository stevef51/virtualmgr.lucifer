﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class LabelConverter
	{
	
		public static LabelEntity ToEntity(this LabelDTO dto)
		{
			return dto.ToEntity(new LabelEntity(), new Dictionary<object, object>());
		}

		public static LabelEntity ToEntity(this LabelDTO dto, LabelEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static LabelEntity ToEntity(this LabelDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new LabelEntity(), cache);
		}
	
		public static LabelDTO ToDTO(this LabelEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static LabelEntity ToEntity(this LabelDTO dto, LabelEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (LabelEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Backcolor = dto.Backcolor;
			
			

			
			
			newEnt.Forecolor = dto.Forecolor;
			
			

			
			
			newEnt.ForMedia = dto.ForMedia;
			
			

			
			
			newEnt.ForQuestions = dto.ForQuestions;
			
			

			
			
			newEnt.ForResources = dto.ForResources;
			
			

			
			
			newEnt.ForTasks = dto.ForTasks;
			
			

			
			
			newEnt.ForUsers = dto.ForUsers;
			
			

			
			
			newEnt.Id = dto.Id;
			
			

			
			
			newEnt.Name = dto.Name;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			
			
			foreach (var related in dto.AssetTypeCalendarTaskLabels)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.AssetTypeCalendarTaskLabels.Contains(relatedEntity))
				{
					newEnt.AssetTypeCalendarTaskLabels.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ProjectJobScheduledJobTaskUserLabels)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProjectJobScheduledJobTaskUserLabels.Contains(relatedEntity))
				{
					newEnt.ProjectJobScheduledJobTaskUserLabels.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ProjectJobScheduledTaskLabels)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProjectJobScheduledTaskLabels.Contains(relatedEntity))
				{
					newEnt.ProjectJobScheduledTaskLabels.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ProjectJobTaskLabels)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProjectJobTaskLabels.Contains(relatedEntity))
				{
					newEnt.ProjectJobTaskLabels.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.HierarchyBucketLabels)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.HierarchyBucketLabels.Contains(relatedEntity))
				{
					newEnt.HierarchyBucketLabels.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.LabelMedias)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.LabelMedias.Contains(relatedEntity))
				{
					newEnt.LabelMedias.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.LabelPublishingGroupResources)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.LabelPublishingGroupResources.Contains(relatedEntity))
				{
					newEnt.LabelPublishingGroupResources.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.LabelResources)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.LabelResources.Contains(relatedEntity))
				{
					newEnt.LabelResources.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.LabelUsers)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.LabelUsers.Contains(relatedEntity))
				{
					newEnt.LabelUsers.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.PublishingGroupActorLabels)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.PublishingGroupActorLabels.Contains(relatedEntity))
				{
					newEnt.PublishingGroupActorLabels.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.UserTypeDefaultLabels)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.UserTypeDefaultLabels.Contains(relatedEntity))
				{
					newEnt.UserTypeDefaultLabels.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static LabelDTO ToDTO(this LabelEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (LabelDTO)cache[ent];
			
			var newDTO = new LabelDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Backcolor = ent.Backcolor;
			

			newDTO.Forecolor = ent.Forecolor;
			

			newDTO.ForMedia = ent.ForMedia;
			

			newDTO.ForQuestions = ent.ForQuestions;
			

			newDTO.ForResources = ent.ForResources;
			

			newDTO.ForTasks = ent.ForTasks;
			

			newDTO.ForUsers = ent.ForUsers;
			

			newDTO.Id = ent.Id;
			

			newDTO.Name = ent.Name;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			
			
			foreach (var related in ent.AssetTypeCalendarTaskLabels)
			{
				newDTO.AssetTypeCalendarTaskLabels.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ProjectJobScheduledJobTaskUserLabels)
			{
				newDTO.ProjectJobScheduledJobTaskUserLabels.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ProjectJobScheduledTaskLabels)
			{
				newDTO.ProjectJobScheduledTaskLabels.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ProjectJobTaskLabels)
			{
				newDTO.ProjectJobTaskLabels.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.HierarchyBucketLabels)
			{
				newDTO.HierarchyBucketLabels.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.LabelMedias)
			{
				newDTO.LabelMedias.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.LabelPublishingGroupResources)
			{
				newDTO.LabelPublishingGroupResources.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.LabelResources)
			{
				newDTO.LabelResources.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.LabelUsers)
			{
				newDTO.LabelUsers.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.PublishingGroupActorLabels)
			{
				newDTO.PublishingGroupActorLabels.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.UserTypeDefaultLabels)
			{
				newDTO.UserTypeDefaultLabels.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}