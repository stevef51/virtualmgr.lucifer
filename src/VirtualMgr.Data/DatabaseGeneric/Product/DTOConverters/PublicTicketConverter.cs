﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class PublicTicketConverter
	{
	
		public static PublicTicketEntity ToEntity(this PublicTicketDTO dto)
		{
			return dto.ToEntity(new PublicTicketEntity(), new Dictionary<object, object>());
		}

		public static PublicTicketEntity ToEntity(this PublicTicketDTO dto, PublicTicketEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static PublicTicketEntity ToEntity(this PublicTicketDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new PublicTicketEntity(), cache);
		}
	
		public static PublicTicketDTO ToDTO(this PublicTicketEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static PublicTicketEntity ToEntity(this PublicTicketDTO dto, PublicTicketEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (PublicTicketEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Id = dto.Id;
			
			

			
			
			newEnt.Status = dto.Status;
			
			
			
			cache.Add(dto, newEnt);
			
			
			newEnt.PasswordResetTicket = dto.PasswordResetTicket.ToEntity(cache);
			
			
			
			
			
			foreach (var related in dto.PublicPublishingGroupResources)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.PublicPublishingGroupResources.Contains(relatedEntity))
				{
					newEnt.PublicPublishingGroupResources.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.PublicWorkingDocuments)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.PublicWorkingDocuments.Contains(relatedEntity))
				{
					newEnt.PublicWorkingDocuments.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static PublicTicketDTO ToDTO(this PublicTicketEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (PublicTicketDTO)cache[ent];
			
			var newDTO = new PublicTicketDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Id = ent.Id;
			

			newDTO.Status = ent.Status;
			
			
			
			cache.Add(ent, newDTO);
			
			
			newDTO.PasswordResetTicket = ent.PasswordResetTicket.ToDTO(cache);
			
			
			
			
			
			foreach (var related in ent.PublicPublishingGroupResources)
			{
				newDTO.PublicPublishingGroupResources.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.PublicWorkingDocuments)
			{
				newDTO.PublicWorkingDocuments.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}