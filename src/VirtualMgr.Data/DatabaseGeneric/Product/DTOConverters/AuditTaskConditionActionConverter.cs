﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class AuditTaskConditionActionConverter
	{
	
		public static AuditTaskConditionActionEntity ToEntity(this AuditTaskConditionActionDTO dto)
		{
			return dto.ToEntity(new AuditTaskConditionActionEntity(), new Dictionary<object, object>());
		}

		public static AuditTaskConditionActionEntity ToEntity(this AuditTaskConditionActionDTO dto, AuditTaskConditionActionEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static AuditTaskConditionActionEntity ToEntity(this AuditTaskConditionActionDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new AuditTaskConditionActionEntity(), cache);
		}
	
		public static AuditTaskConditionActionDTO ToDTO(this AuditTaskConditionActionEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static AuditTaskConditionActionEntity ToEntity(this AuditTaskConditionActionDTO dto, AuditTaskConditionActionEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (AuditTaskConditionActionEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Group = dto.Group;
			
			

			
			
			newEnt.Id = dto.Id;
			
			
			

			
			
			newEnt.ProjectJobScheduledTaskId = dto.ProjectJobScheduledTaskId;
			
			

			
			
			newEnt.SampleCount = dto.SampleCount;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.ProjectJobScheduledTask = dto.ProjectJobScheduledTask.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static AuditTaskConditionActionDTO ToDTO(this AuditTaskConditionActionEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (AuditTaskConditionActionDTO)cache[ent];
			
			var newDTO = new AuditTaskConditionActionDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Group = ent.Group;
			

			newDTO.Id = ent.Id;
			

			newDTO.ProjectJobScheduledTaskId = ent.ProjectJobScheduledTaskId;
			

			newDTO.SampleCount = ent.SampleCount;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.ProjectJobScheduledTask = ent.ProjectJobScheduledTask.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}