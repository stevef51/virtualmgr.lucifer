﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class NewsFeedConverter
	{
	
		public static NewsFeedEntity ToEntity(this NewsFeedDTO dto)
		{
			return dto.ToEntity(new NewsFeedEntity(), new Dictionary<object, object>());
		}

		public static NewsFeedEntity ToEntity(this NewsFeedDTO dto, NewsFeedEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static NewsFeedEntity ToEntity(this NewsFeedDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new NewsFeedEntity(), cache);
		}
	
		public static NewsFeedDTO ToDTO(this NewsFeedEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static NewsFeedEntity ToEntity(this NewsFeedDTO dto, NewsFeedEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (NewsFeedEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Channel = dto.Channel;
			
			

			
			
			newEnt.CreatedBy = dto.CreatedBy;
			
			

			
			
			newEnt.DateCreated = dto.DateCreated;
			
			

			
			
			newEnt.Headline = dto.Headline;
			
			

			
			
			newEnt.Id = dto.Id;
			
			

			
			
			newEnt.Program = dto.Program;
			
			

			
			
			if (dto.Title == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)NewsFeedFieldIndex.Title, "");
				
			}
			
			newEnt.Title = dto.Title;
			
			

			
			
			newEnt.ValidTo = dto.ValidTo;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.UserData = dto.UserData.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static NewsFeedDTO ToDTO(this NewsFeedEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (NewsFeedDTO)cache[ent];
			
			var newDTO = new NewsFeedDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Channel = ent.Channel;
			

			newDTO.CreatedBy = ent.CreatedBy;
			

			newDTO.DateCreated = ent.DateCreated;
			

			newDTO.Headline = ent.Headline;
			

			newDTO.Id = ent.Id;
			

			newDTO.Program = ent.Program;
			

			newDTO.Title = ent.Title;
			

			newDTO.ValidTo = ent.ValidTo;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.UserData = ent.UserData.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}