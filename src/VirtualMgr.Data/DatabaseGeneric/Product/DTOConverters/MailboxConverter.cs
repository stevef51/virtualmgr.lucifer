﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class MailboxConverter
	{
	
		public static MailboxEntity ToEntity(this MailboxDTO dto)
		{
			return dto.ToEntity(new MailboxEntity(), new Dictionary<object, object>());
		}

		public static MailboxEntity ToEntity(this MailboxDTO dto, MailboxEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static MailboxEntity ToEntity(this MailboxDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new MailboxEntity(), cache);
		}
	
		public static MailboxDTO ToDTO(this MailboxEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static MailboxEntity ToEntity(this MailboxDTO dto, MailboxEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (MailboxEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Id = dto.Id;
			
			

			
			
			if (dto.IsRead == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)MailboxFieldIndex.IsRead, default(System.Boolean));
				
			}
			
			newEnt.IsRead = dto.IsRead;
			
			

			
			
			newEnt.MessageId = dto.MessageId;
			
			

			
			
			newEnt.UserId = dto.UserId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.Message = dto.Message.ToEntity(cache);
			
			newEnt.User = dto.User.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static MailboxDTO ToDTO(this MailboxEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (MailboxDTO)cache[ent];
			
			var newDTO = new MailboxDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Id = ent.Id;
			

			newDTO.IsRead = ent.IsRead;
			

			newDTO.MessageId = ent.MessageId;
			

			newDTO.UserId = ent.UserId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.Message = ent.Message.ToDTO(cache);
			
			newDTO.User = ent.User.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}