﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class MergedTeamHierarchyWithLabelConverter
	{
	
		public static MergedTeamHierarchyWithLabelEntity ToEntity(this MergedTeamHierarchyWithLabelDTO dto)
		{
			return dto.ToEntity(new MergedTeamHierarchyWithLabelEntity(), new Dictionary<object, object>());
		}

		public static MergedTeamHierarchyWithLabelEntity ToEntity(this MergedTeamHierarchyWithLabelDTO dto, MergedTeamHierarchyWithLabelEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static MergedTeamHierarchyWithLabelEntity ToEntity(this MergedTeamHierarchyWithLabelDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new MergedTeamHierarchyWithLabelEntity(), cache);
		}
	
		public static MergedTeamHierarchyWithLabelDTO ToDTO(this MergedTeamHierarchyWithLabelEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static MergedTeamHierarchyWithLabelEntity ToEntity(this MergedTeamHierarchyWithLabelDTO dto, MergedTeamHierarchyWithLabelEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (MergedTeamHierarchyWithLabelEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.AssignTasksThroughLabels = dto.AssignTasksThroughLabels;
			
			

			
			
			newEnt.BucketName = dto.BucketName;
			
			

			
			
			if (dto.CompanyId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)MergedTeamHierarchyWithLabelFieldIndex.CompanyId, default(System.Guid));
				
			}
			
			newEnt.CompanyId = dto.CompanyId;
			
			

			
			
			if (dto.ExpiryDate == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)MergedTeamHierarchyWithLabelFieldIndex.ExpiryDate, default(System.DateTime));
				
			}
			
			newEnt.ExpiryDate = dto.ExpiryDate;
			
			

			
			
			newEnt.HierarchyId = dto.HierarchyId;
			
			

			
			
			newEnt.HierarchyName = dto.HierarchyName;
			
			

			
			
			newEnt.Id = dto.Id;
			
			

			
			
			if (dto.IsPrimary == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)MergedTeamHierarchyWithLabelFieldIndex.IsPrimary, default(System.Boolean));
				
			}
			
			newEnt.IsPrimary = dto.IsPrimary;
			
			

			
			
			newEnt.LeftIndex = dto.LeftIndex;
			
			

			
			
			if (dto.Name == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)MergedTeamHierarchyWithLabelFieldIndex.Name, "");
				
			}
			
			newEnt.Name = dto.Name;
			
			

			
			
			newEnt.OpenSecurityThroughLabels = dto.OpenSecurityThroughLabels;
			
			

			
			
			if (dto.ParentId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)MergedTeamHierarchyWithLabelFieldIndex.ParentId, default(System.Int32));
				
			}
			
			newEnt.ParentId = dto.ParentId;
			
			

			
			
			newEnt.PrimaryRecord = dto.PrimaryRecord;
			
			

			
			
			newEnt.RightIndex = dto.RightIndex;
			
			

			
			
			newEnt.RoleId = dto.RoleId;
			
			

			
			
			newEnt.RoleName = dto.RoleName;
			
			

			
			
			newEnt.RosterId = dto.RosterId;
			
			

			
			
			newEnt.RosterName = dto.RosterName;
			
			

			
			
			newEnt.TasksCanBeClaimed = dto.TasksCanBeClaimed;
			
			

			
			
			if (dto.TimeZone == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)MergedTeamHierarchyWithLabelFieldIndex.TimeZone, "");
				
			}
			
			newEnt.TimeZone = dto.TimeZone;
			
			

			
			
			if (dto.UserId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)MergedTeamHierarchyWithLabelFieldIndex.UserId, default(System.Guid));
				
			}
			
			newEnt.UserId = dto.UserId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static MergedTeamHierarchyWithLabelDTO ToDTO(this MergedTeamHierarchyWithLabelEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (MergedTeamHierarchyWithLabelDTO)cache[ent];
			
			var newDTO = new MergedTeamHierarchyWithLabelDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.AssignTasksThroughLabels = ent.AssignTasksThroughLabels;
			

			newDTO.BucketName = ent.BucketName;
			

			newDTO.CompanyId = ent.CompanyId;
			

			newDTO.ExpiryDate = ent.ExpiryDate;
			

			newDTO.HierarchyId = ent.HierarchyId;
			

			newDTO.HierarchyName = ent.HierarchyName;
			

			newDTO.Id = ent.Id;
			

			newDTO.IsPrimary = ent.IsPrimary;
			

			newDTO.LeftIndex = ent.LeftIndex;
			

			newDTO.Name = ent.Name;
			

			newDTO.OpenSecurityThroughLabels = ent.OpenSecurityThroughLabels;
			

			newDTO.ParentId = ent.ParentId;
			

			newDTO.PrimaryRecord = ent.PrimaryRecord;
			

			newDTO.RightIndex = ent.RightIndex;
			

			newDTO.RoleId = ent.RoleId;
			

			newDTO.RoleName = ent.RoleName;
			

			newDTO.RosterId = ent.RosterId;
			

			newDTO.RosterName = ent.RosterName;
			

			newDTO.TasksCanBeClaimed = ent.TasksCanBeClaimed;
			

			newDTO.TimeZone = ent.TimeZone;
			

			newDTO.UserId = ent.UserId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}