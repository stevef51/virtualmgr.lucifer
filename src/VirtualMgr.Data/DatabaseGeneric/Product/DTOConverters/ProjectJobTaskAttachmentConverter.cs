﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class ProjectJobTaskAttachmentConverter
	{
	
		public static ProjectJobTaskAttachmentEntity ToEntity(this ProjectJobTaskAttachmentDTO dto)
		{
			return dto.ToEntity(new ProjectJobTaskAttachmentEntity(), new Dictionary<object, object>());
		}

		public static ProjectJobTaskAttachmentEntity ToEntity(this ProjectJobTaskAttachmentDTO dto, ProjectJobTaskAttachmentEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static ProjectJobTaskAttachmentEntity ToEntity(this ProjectJobTaskAttachmentDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new ProjectJobTaskAttachmentEntity(), cache);
		}
	
		public static ProjectJobTaskAttachmentDTO ToDTO(this ProjectJobTaskAttachmentEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static ProjectJobTaskAttachmentEntity ToEntity(this ProjectJobTaskAttachmentDTO dto, ProjectJobTaskAttachmentEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (ProjectJobTaskAttachmentEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.MediaId = dto.MediaId;
			
			

			
			
			newEnt.ProjectJobTaskId = dto.ProjectJobTaskId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.ProjectJobTask = dto.ProjectJobTask.ToEntity(cache);
			
			newEnt.Medium = dto.Medium.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static ProjectJobTaskAttachmentDTO ToDTO(this ProjectJobTaskAttachmentEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (ProjectJobTaskAttachmentDTO)cache[ent];
			
			var newDTO = new ProjectJobTaskAttachmentDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.MediaId = ent.MediaId;
			

			newDTO.ProjectJobTaskId = ent.ProjectJobTaskId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.ProjectJobTask = ent.ProjectJobTask.ToDTO(cache);
			
			newDTO.Medium = ent.Medium.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}