﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class InvoiceLineItemConverter
	{
	
		public static InvoiceLineItemEntity ToEntity(this InvoiceLineItemDTO dto)
		{
			return dto.ToEntity(new InvoiceLineItemEntity(), new Dictionary<object, object>());
		}

		public static InvoiceLineItemEntity ToEntity(this InvoiceLineItemDTO dto, InvoiceLineItemEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static InvoiceLineItemEntity ToEntity(this InvoiceLineItemDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new InvoiceLineItemEntity(), cache);
		}
	
		public static InvoiceLineItemDTO ToDTO(this InvoiceLineItemEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static InvoiceLineItemEntity ToEntity(this InvoiceLineItemDTO dto, InvoiceLineItemEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (InvoiceLineItemEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Description = dto.Description;
			
			

			
			
			newEnt.DetailLevel = dto.DetailLevel;
			
			

			
			
			newEnt.Id = dto.Id;
			
			
			

			
			
			newEnt.InvoiceId = dto.InvoiceId;
			
			

			
			
			newEnt.Line = dto.Line;
			
			

			
			
			if (dto.ParentLine == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)InvoiceLineItemFieldIndex.ParentLine, "");
				
			}
			
			newEnt.ParentLine = dto.ParentLine;
			
			

			
			
			newEnt.Quantity = dto.Quantity;
			
			

			
			
			newEnt.Total = dto.Total;
			
			

			
			
			newEnt.UnitPrice = dto.UnitPrice;
			
			

			
			
			if (dto.UserId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)InvoiceLineItemFieldIndex.UserId, default(System.Guid));
				
			}
			
			newEnt.UserId = dto.UserId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.Invoice = dto.Invoice.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static InvoiceLineItemDTO ToDTO(this InvoiceLineItemEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (InvoiceLineItemDTO)cache[ent];
			
			var newDTO = new InvoiceLineItemDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Description = ent.Description;
			

			newDTO.DetailLevel = ent.DetailLevel;
			

			newDTO.Id = ent.Id;
			

			newDTO.InvoiceId = ent.InvoiceId;
			

			newDTO.Line = ent.Line;
			

			newDTO.ParentLine = ent.ParentLine;
			

			newDTO.Quantity = ent.Quantity;
			

			newDTO.Total = ent.Total;
			

			newDTO.UnitPrice = ent.UnitPrice;
			

			newDTO.UserId = ent.UserId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.Invoice = ent.Invoice.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}