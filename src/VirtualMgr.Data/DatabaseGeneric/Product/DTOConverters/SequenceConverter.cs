﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class SequenceConverter
	{
	
		public static SequenceEntity ToEntity(this SequenceDTO dto)
		{
			return dto.ToEntity(new SequenceEntity(), new Dictionary<object, object>());
		}

		public static SequenceEntity ToEntity(this SequenceDTO dto, SequenceEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static SequenceEntity ToEntity(this SequenceDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new SequenceEntity(), cache);
		}
	
		public static SequenceDTO ToDTO(this SequenceEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static SequenceEntity ToEntity(this SequenceDTO dto, SequenceEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (SequenceEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Name = dto.Name;
			
			

			
			
			newEnt.Value = dto.Value;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static SequenceDTO ToDTO(this SequenceEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (SequenceDTO)cache[ent];
			
			var newDTO = new SequenceDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Name = ent.Name;
			

			newDTO.Value = ent.Value;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}