﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class ProjectJobTaskTypeConverter
	{
	
		public static ProjectJobTaskTypeEntity ToEntity(this ProjectJobTaskTypeDTO dto)
		{
			return dto.ToEntity(new ProjectJobTaskTypeEntity(), new Dictionary<object, object>());
		}

		public static ProjectJobTaskTypeEntity ToEntity(this ProjectJobTaskTypeDTO dto, ProjectJobTaskTypeEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static ProjectJobTaskTypeEntity ToEntity(this ProjectJobTaskTypeDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new ProjectJobTaskTypeEntity(), cache);
		}
	
		public static ProjectJobTaskTypeDTO ToDTO(this ProjectJobTaskTypeEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static ProjectJobTaskTypeEntity ToEntity(this ProjectJobTaskTypeDTO dto, ProjectJobTaskTypeEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (ProjectJobTaskTypeEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.AutoFinishAfterLastChecklist = dto.AutoFinishAfterLastChecklist;
			
			

			
			
			newEnt.AutoFinishAfterStart = dto.AutoFinishAfterStart;
			
			

			
			
			newEnt.AutoStart1stChecklist = dto.AutoStart1stChecklist;
			
			

			
			
			newEnt.DefaultLengthInDays = dto.DefaultLengthInDays;
			
			

			
			
			if (dto.EstimatedDurationSeconds == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobTaskTypeFieldIndex.EstimatedDurationSeconds, default(System.Decimal));
				
			}
			
			newEnt.EstimatedDurationSeconds = dto.EstimatedDurationSeconds;
			
			

			
			
			newEnt.ExcludeFromTimesheets = dto.ExcludeFromTimesheets;
			
			

			
			
			newEnt.Id = dto.Id;
			
			

			
			
			if (dto.MediaId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobTaskTypeFieldIndex.MediaId, default(System.Guid));
				
			}
			
			newEnt.MediaId = dto.MediaId;
			
			

			
			
			if (dto.MinimumDuration == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobTaskTypeFieldIndex.MinimumDuration, "");
				
			}
			
			newEnt.MinimumDuration = dto.MinimumDuration;
			
			

			
			
			newEnt.Name = dto.Name;
			
			

			
			
			newEnt.PhotoSupport = dto.PhotoSupport;
			
			

			
			
			if (dto.ProductCatalogs == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobTaskTypeFieldIndex.ProductCatalogs, "");
				
			}
			
			newEnt.ProductCatalogs = dto.ProductCatalogs;
			
			

			
			
			newEnt.UserInterfaceType = dto.UserInterfaceType;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.Medium = dto.Medium.ToEntity(cache);
			
			
			
			foreach (var related in dto.AssetTypeCalendarTasks)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.AssetTypeCalendarTasks.Contains(relatedEntity))
				{
					newEnt.AssetTypeCalendarTasks.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.Beacons)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.Beacons.Contains(relatedEntity))
				{
					newEnt.Beacons.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ProjectJobScheduledTasks)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProjectJobScheduledTasks.Contains(relatedEntity))
				{
					newEnt.ProjectJobScheduledTasks.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ProjectJobTasks)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProjectJobTasks.Contains(relatedEntity))
				{
					newEnt.ProjectJobTasks.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ProjectJobTasks_)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProjectJobTasks_.Contains(relatedEntity))
				{
					newEnt.ProjectJobTasks_.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ProjectJobTaskTypeFacilityOverrides)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProjectJobTaskTypeFacilityOverrides.Contains(relatedEntity))
				{
					newEnt.ProjectJobTaskTypeFacilityOverrides.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ProjectJobTaskTypeFinishedStatuses)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProjectJobTaskTypeFinishedStatuses.Contains(relatedEntity))
				{
					newEnt.ProjectJobTaskTypeFinishedStatuses.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ProjectJobTaskTypeMediaFolders)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProjectJobTaskTypeMediaFolders.Contains(relatedEntity))
				{
					newEnt.ProjectJobTaskTypeMediaFolders.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ProjectJobTaskTypePublishingGroupResources)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProjectJobTaskTypePublishingGroupResources.Contains(relatedEntity))
				{
					newEnt.ProjectJobTaskTypePublishingGroupResources.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ProjectJobTaskTypeDestinationRelationships)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProjectJobTaskTypeDestinationRelationships.Contains(relatedEntity))
				{
					newEnt.ProjectJobTaskTypeDestinationRelationships.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ProjectJobTaskTypeSourceRelationships)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProjectJobTaskTypeSourceRelationships.Contains(relatedEntity))
				{
					newEnt.ProjectJobTaskTypeSourceRelationships.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ProjectJobTaskTypeStatuses)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProjectJobTaskTypeStatuses.Contains(relatedEntity))
				{
					newEnt.ProjectJobTaskTypeStatuses.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.UserTypeTaskTypeRelationships)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.UserTypeTaskTypeRelationships.Contains(relatedEntity))
				{
					newEnt.UserTypeTaskTypeRelationships.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.Qrcodes)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.Qrcodes.Contains(relatedEntity))
				{
					newEnt.Qrcodes.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static ProjectJobTaskTypeDTO ToDTO(this ProjectJobTaskTypeEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (ProjectJobTaskTypeDTO)cache[ent];
			
			var newDTO = new ProjectJobTaskTypeDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.AutoFinishAfterLastChecklist = ent.AutoFinishAfterLastChecklist;
			

			newDTO.AutoFinishAfterStart = ent.AutoFinishAfterStart;
			

			newDTO.AutoStart1stChecklist = ent.AutoStart1stChecklist;
			

			newDTO.DefaultLengthInDays = ent.DefaultLengthInDays;
			

			newDTO.EstimatedDurationSeconds = ent.EstimatedDurationSeconds;
			

			newDTO.ExcludeFromTimesheets = ent.ExcludeFromTimesheets;
			

			newDTO.Id = ent.Id;
			

			newDTO.MediaId = ent.MediaId;
			

			newDTO.MinimumDuration = ent.MinimumDuration;
			

			newDTO.Name = ent.Name;
			

			newDTO.PhotoSupport = ent.PhotoSupport;
			

			newDTO.ProductCatalogs = ent.ProductCatalogs;
			

			newDTO.UserInterfaceType = ent.UserInterfaceType;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.Medium = ent.Medium.ToDTO(cache);
			
			
			
			foreach (var related in ent.AssetTypeCalendarTasks)
			{
				newDTO.AssetTypeCalendarTasks.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.Beacons)
			{
				newDTO.Beacons.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ProjectJobScheduledTasks)
			{
				newDTO.ProjectJobScheduledTasks.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ProjectJobTasks)
			{
				newDTO.ProjectJobTasks.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ProjectJobTasks_)
			{
				newDTO.ProjectJobTasks_.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ProjectJobTaskTypeFacilityOverrides)
			{
				newDTO.ProjectJobTaskTypeFacilityOverrides.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ProjectJobTaskTypeFinishedStatuses)
			{
				newDTO.ProjectJobTaskTypeFinishedStatuses.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ProjectJobTaskTypeMediaFolders)
			{
				newDTO.ProjectJobTaskTypeMediaFolders.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ProjectJobTaskTypePublishingGroupResources)
			{
				newDTO.ProjectJobTaskTypePublishingGroupResources.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ProjectJobTaskTypeDestinationRelationships)
			{
				newDTO.ProjectJobTaskTypeDestinationRelationships.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ProjectJobTaskTypeSourceRelationships)
			{
				newDTO.ProjectJobTaskTypeSourceRelationships.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ProjectJobTaskTypeStatuses)
			{
				newDTO.ProjectJobTaskTypeStatuses.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.UserTypeTaskTypeRelationships)
			{
				newDTO.UserTypeTaskTypeRelationships.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.Qrcodes)
			{
				newDTO.Qrcodes.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}