﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class WorkingDocumentReferenceConverter
	{
	
		public static WorkingDocumentReferenceEntity ToEntity(this WorkingDocumentReferenceDTO dto)
		{
			return dto.ToEntity(new WorkingDocumentReferenceEntity(), new Dictionary<object, object>());
		}

		public static WorkingDocumentReferenceEntity ToEntity(this WorkingDocumentReferenceDTO dto, WorkingDocumentReferenceEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static WorkingDocumentReferenceEntity ToEntity(this WorkingDocumentReferenceDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new WorkingDocumentReferenceEntity(), cache);
		}
	
		public static WorkingDocumentReferenceDTO ToDTO(this WorkingDocumentReferenceEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static WorkingDocumentReferenceEntity ToEntity(this WorkingDocumentReferenceDTO dto, WorkingDocumentReferenceEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (WorkingDocumentReferenceEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Id = dto.Id;
			
			

			
			
			newEnt.Name = dto.Name;
			
			

			
			
			newEnt.Url = dto.Url;
			
			

			
			
			newEnt.WorkingDocumentId = dto.WorkingDocumentId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.WorkingDocument = dto.WorkingDocument.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static WorkingDocumentReferenceDTO ToDTO(this WorkingDocumentReferenceEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (WorkingDocumentReferenceDTO)cache[ent];
			
			var newDTO = new WorkingDocumentReferenceDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Id = ent.Id;
			

			newDTO.Name = ent.Name;
			

			newDTO.Url = ent.Url;
			

			newDTO.WorkingDocumentId = ent.WorkingDocumentId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.WorkingDocument = ent.WorkingDocument.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}