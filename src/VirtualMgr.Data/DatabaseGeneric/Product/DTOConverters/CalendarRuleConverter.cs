﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class CalendarRuleConverter
	{
	
		public static CalendarRuleEntity ToEntity(this CalendarRuleDTO dto)
		{
			return dto.ToEntity(new CalendarRuleEntity(), new Dictionary<object, object>());
		}

		public static CalendarRuleEntity ToEntity(this CalendarRuleDTO dto, CalendarRuleEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static CalendarRuleEntity ToEntity(this CalendarRuleDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new CalendarRuleEntity(), cache);
		}
	
		public static CalendarRuleDTO ToDTO(this CalendarRuleEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static CalendarRuleEntity ToEntity(this CalendarRuleDTO dto, CalendarRuleEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (CalendarRuleEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			if (dto.Cronexpression == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)CalendarRuleFieldIndex.Cronexpression, "");
				
			}
			
			newEnt.Cronexpression = dto.Cronexpression;
			
			

			
			
			newEnt.DateCreated = dto.DateCreated;
			
			

			
			
			if (dto.EndDate == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)CalendarRuleFieldIndex.EndDate, default(System.DateTime));
				
			}
			
			newEnt.EndDate = dto.EndDate;
			
			

			
			
			newEnt.ExecuteFriday = dto.ExecuteFriday;
			
			

			
			
			newEnt.ExecuteMonday = dto.ExecuteMonday;
			
			

			
			
			newEnt.ExecuteSaturday = dto.ExecuteSaturday;
			
			

			
			
			newEnt.ExecuteSunday = dto.ExecuteSunday;
			
			

			
			
			newEnt.ExecuteThursday = dto.ExecuteThursday;
			
			

			
			
			newEnt.ExecuteTuesday = dto.ExecuteTuesday;
			
			

			
			
			newEnt.ExecuteWednesday = dto.ExecuteWednesday;
			
			

			
			
			newEnt.Frequency = dto.Frequency;
			
			

			
			
			newEnt.Id = dto.Id;
			
			
			

			
			
			newEnt.Period = dto.Period;
			
			

			
			
			newEnt.StartDate = dto.StartDate;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			
			
			foreach (var related in dto.CalendarEvents)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.CalendarEvents.Contains(relatedEntity))
				{
					newEnt.CalendarEvents.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ProjectJobScheduledTasks)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProjectJobScheduledTasks.Contains(relatedEntity))
				{
					newEnt.ProjectJobScheduledTasks.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static CalendarRuleDTO ToDTO(this CalendarRuleEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (CalendarRuleDTO)cache[ent];
			
			var newDTO = new CalendarRuleDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Cronexpression = ent.Cronexpression;
			

			newDTO.DateCreated = ent.DateCreated;
			

			newDTO.EndDate = ent.EndDate;
			

			newDTO.ExecuteFriday = ent.ExecuteFriday;
			

			newDTO.ExecuteMonday = ent.ExecuteMonday;
			

			newDTO.ExecuteSaturday = ent.ExecuteSaturday;
			

			newDTO.ExecuteSunday = ent.ExecuteSunday;
			

			newDTO.ExecuteThursday = ent.ExecuteThursday;
			

			newDTO.ExecuteTuesday = ent.ExecuteTuesday;
			

			newDTO.ExecuteWednesday = ent.ExecuteWednesday;
			

			newDTO.Frequency = ent.Frequency;
			

			newDTO.Id = ent.Id;
			

			newDTO.Period = ent.Period;
			

			newDTO.StartDate = ent.StartDate;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			
			
			foreach (var related in ent.CalendarEvents)
			{
				newDTO.CalendarEvents.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ProjectJobScheduledTasks)
			{
				newDTO.ProjectJobScheduledTasks.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}