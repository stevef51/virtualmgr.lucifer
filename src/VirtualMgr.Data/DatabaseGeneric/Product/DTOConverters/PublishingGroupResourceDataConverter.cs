﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class PublishingGroupResourceDataConverter
	{
	
		public static PublishingGroupResourceDataEntity ToEntity(this PublishingGroupResourceDataDTO dto)
		{
			return dto.ToEntity(new PublishingGroupResourceDataEntity(), new Dictionary<object, object>());
		}

		public static PublishingGroupResourceDataEntity ToEntity(this PublishingGroupResourceDataDTO dto, PublishingGroupResourceDataEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static PublishingGroupResourceDataEntity ToEntity(this PublishingGroupResourceDataDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new PublishingGroupResourceDataEntity(), cache);
		}
	
		public static PublishingGroupResourceDataDTO ToDTO(this PublishingGroupResourceDataEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static PublishingGroupResourceDataEntity ToEntity(this PublishingGroupResourceDataDTO dto, PublishingGroupResourceDataEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (PublishingGroupResourceDataEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Data = dto.Data;
			
			

			
			
			newEnt.PublishingGroupResourceId = dto.PublishingGroupResourceId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			newEnt.PublishingGroupResource = dto.PublishingGroupResource.ToEntity(cache);
			
			
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static PublishingGroupResourceDataDTO ToDTO(this PublishingGroupResourceDataEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (PublishingGroupResourceDataDTO)cache[ent];
			
			var newDTO = new PublishingGroupResourceDataDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Data = ent.Data;
			

			newDTO.PublishingGroupResourceId = ent.PublishingGroupResourceId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			newDTO.PublishingGroupResource = ent.PublishingGroupResource.ToDTO(cache);
			
			
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}