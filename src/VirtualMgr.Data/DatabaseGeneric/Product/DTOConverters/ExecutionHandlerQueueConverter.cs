﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class ExecutionHandlerQueueConverter
	{
	
		public static ExecutionHandlerQueueEntity ToEntity(this ExecutionHandlerQueueDTO dto)
		{
			return dto.ToEntity(new ExecutionHandlerQueueEntity(), new Dictionary<object, object>());
		}

		public static ExecutionHandlerQueueEntity ToEntity(this ExecutionHandlerQueueDTO dto, ExecutionHandlerQueueEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static ExecutionHandlerQueueEntity ToEntity(this ExecutionHandlerQueueDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new ExecutionHandlerQueueEntity(), cache);
		}
	
		public static ExecutionHandlerQueueDTO ToDTO(this ExecutionHandlerQueueEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static ExecutionHandlerQueueEntity ToEntity(this ExecutionHandlerQueueDTO dto, ExecutionHandlerQueueEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (ExecutionHandlerQueueEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Data = dto.Data;
			
			

			
			
			newEnt.DateCreated = dto.DateCreated;
			
			

			
			
			newEnt.Handler = dto.Handler;
			
			

			
			
			newEnt.Id = dto.Id;
			
			

			
			
			if (dto.LatestException == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ExecutionHandlerQueueFieldIndex.LatestException, "");
				
			}
			
			newEnt.LatestException = dto.LatestException;
			
			

			
			
			newEnt.Method = dto.Method;
			
			

			
			
			newEnt.PrimaryEntityId = dto.PrimaryEntityId;
			
			

			
			
			newEnt.Processed = dto.Processed;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static ExecutionHandlerQueueDTO ToDTO(this ExecutionHandlerQueueEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (ExecutionHandlerQueueDTO)cache[ent];
			
			var newDTO = new ExecutionHandlerQueueDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Data = ent.Data;
			

			newDTO.DateCreated = ent.DateCreated;
			

			newDTO.Handler = ent.Handler;
			

			newDTO.Id = ent.Id;
			

			newDTO.LatestException = ent.LatestException;
			

			newDTO.Method = ent.Method;
			

			newDTO.PrimaryEntityId = ent.PrimaryEntityId;
			

			newDTO.Processed = ent.Processed;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}