﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class TabletProfileConverter
	{
	
		public static TabletProfileEntity ToEntity(this TabletProfileDTO dto)
		{
			return dto.ToEntity(new TabletProfileEntity(), new Dictionary<object, object>());
		}

		public static TabletProfileEntity ToEntity(this TabletProfileDTO dto, TabletProfileEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static TabletProfileEntity ToEntity(this TabletProfileDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new TabletProfileEntity(), cache);
		}
	
		public static TabletProfileDTO ToDTO(this TabletProfileEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static TabletProfileEntity ToEntity(this TabletProfileDTO dto, TabletProfileEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (TabletProfileEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.AllowNumberPadLogin = dto.AllowNumberPadLogin;
			
			

			
			
			newEnt.Archived = dto.Archived;
			
			

			
			
			if (dto.AutoLogoutLongSecs == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)TabletProfileFieldIndex.AutoLogoutLongSecs, default(System.Int32));
				
			}
			
			newEnt.AutoLogoutLongSecs = dto.AutoLogoutLongSecs;
			
			

			
			
			if (dto.AutoLogoutShortSecs == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)TabletProfileFieldIndex.AutoLogoutShortSecs, default(System.Int32));
				
			}
			
			newEnt.AutoLogoutShortSecs = dto.AutoLogoutShortSecs;
			
			

			
			
			newEnt.ConfirmLogout = dto.ConfirmLogout;
			
			

			
			
			newEnt.EnableEnvironmentalSensing = dto.EnableEnvironmentalSensing;
			
			

			
			
			newEnt.EnableFma = dto.EnableFma;
			
			

			
			
			newEnt.Id = dto.Id;
			
			
			

			
			
			newEnt.LoginPhotoEvery = dto.LoginPhotoEvery;
			
			

			
			
			if (dto.LogoutPassword == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)TabletProfileFieldIndex.LogoutPassword, "");
				
			}
			
			newEnt.LogoutPassword = dto.LogoutPassword;
			
			

			
			
			newEnt.Name = dto.Name;
			
			

			
			
			if (dto.SettingsPassword == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)TabletProfileFieldIndex.SettingsPassword, "");
				
			}
			
			newEnt.SettingsPassword = dto.SettingsPassword;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			
			
			foreach (var related in dto.Tablets)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.Tablets.Contains(relatedEntity))
				{
					newEnt.Tablets.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.UserDatas)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.UserDatas.Contains(relatedEntity))
				{
					newEnt.UserDatas.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static TabletProfileDTO ToDTO(this TabletProfileEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (TabletProfileDTO)cache[ent];
			
			var newDTO = new TabletProfileDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.AllowNumberPadLogin = ent.AllowNumberPadLogin;
			

			newDTO.Archived = ent.Archived;
			

			newDTO.AutoLogoutLongSecs = ent.AutoLogoutLongSecs;
			

			newDTO.AutoLogoutShortSecs = ent.AutoLogoutShortSecs;
			

			newDTO.ConfirmLogout = ent.ConfirmLogout;
			

			newDTO.EnableEnvironmentalSensing = ent.EnableEnvironmentalSensing;
			

			newDTO.EnableFma = ent.EnableFma;
			

			newDTO.Id = ent.Id;
			

			newDTO.LoginPhotoEvery = ent.LoginPhotoEvery;
			

			newDTO.LogoutPassword = ent.LogoutPassword;
			

			newDTO.Name = ent.Name;
			

			newDTO.SettingsPassword = ent.SettingsPassword;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			
			
			foreach (var related in ent.Tablets)
			{
				newDTO.Tablets.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.UserDatas)
			{
				newDTO.UserDatas.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}