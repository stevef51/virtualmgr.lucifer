﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class AspNetUserLoginConverter
	{
	
		public static AspNetUserLoginEntity ToEntity(this AspNetUserLoginDTO dto)
		{
			return dto.ToEntity(new AspNetUserLoginEntity(), new Dictionary<object, object>());
		}

		public static AspNetUserLoginEntity ToEntity(this AspNetUserLoginDTO dto, AspNetUserLoginEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static AspNetUserLoginEntity ToEntity(this AspNetUserLoginDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new AspNetUserLoginEntity(), cache);
		}
	
		public static AspNetUserLoginDTO ToDTO(this AspNetUserLoginEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static AspNetUserLoginEntity ToEntity(this AspNetUserLoginDTO dto, AspNetUserLoginEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (AspNetUserLoginEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.LoginProvider = dto.LoginProvider;
			
			

			
			
			if (dto.ProviderDisplayName == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)AspNetUserLoginFieldIndex.ProviderDisplayName, "");
				
			}
			
			newEnt.ProviderDisplayName = dto.ProviderDisplayName;
			
			

			
			
			newEnt.ProviderKey = dto.ProviderKey;
			
			

			
			
			newEnt.UserId = dto.UserId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.AspNetUser = dto.AspNetUser.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static AspNetUserLoginDTO ToDTO(this AspNetUserLoginEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (AspNetUserLoginDTO)cache[ent];
			
			var newDTO = new AspNetUserLoginDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.LoginProvider = ent.LoginProvider;
			

			newDTO.ProviderDisplayName = ent.ProviderDisplayName;
			

			newDTO.ProviderKey = ent.ProviderKey;
			

			newDTO.UserId = ent.UserId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.AspNetUser = ent.AspNetUser.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}