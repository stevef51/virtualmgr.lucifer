﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class ProjectJobTaskTypePublishingGroupResourceConverter
	{
	
		public static ProjectJobTaskTypePublishingGroupResourceEntity ToEntity(this ProjectJobTaskTypePublishingGroupResourceDTO dto)
		{
			return dto.ToEntity(new ProjectJobTaskTypePublishingGroupResourceEntity(), new Dictionary<object, object>());
		}

		public static ProjectJobTaskTypePublishingGroupResourceEntity ToEntity(this ProjectJobTaskTypePublishingGroupResourceDTO dto, ProjectJobTaskTypePublishingGroupResourceEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static ProjectJobTaskTypePublishingGroupResourceEntity ToEntity(this ProjectJobTaskTypePublishingGroupResourceDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new ProjectJobTaskTypePublishingGroupResourceEntity(), cache);
		}
	
		public static ProjectJobTaskTypePublishingGroupResourceDTO ToDTO(this ProjectJobTaskTypePublishingGroupResourceEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static ProjectJobTaskTypePublishingGroupResourceEntity ToEntity(this ProjectJobTaskTypePublishingGroupResourceDTO dto, ProjectJobTaskTypePublishingGroupResourceEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (ProjectJobTaskTypePublishingGroupResourceEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.AllowMultiple = dto.AllowMultiple;
			
			

			
			
			newEnt.Mandatory = dto.Mandatory;
			
			

			
			
			newEnt.PreventFinishing = dto.PreventFinishing;
			
			

			
			
			newEnt.PublishingGroupResourceId = dto.PublishingGroupResourceId;
			
			

			
			
			newEnt.TaskTypeId = dto.TaskTypeId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.ProjectJobTaskType = dto.ProjectJobTaskType.ToEntity(cache);
			
			newEnt.PublishingGroupResource = dto.PublishingGroupResource.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static ProjectJobTaskTypePublishingGroupResourceDTO ToDTO(this ProjectJobTaskTypePublishingGroupResourceEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (ProjectJobTaskTypePublishingGroupResourceDTO)cache[ent];
			
			var newDTO = new ProjectJobTaskTypePublishingGroupResourceDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.AllowMultiple = ent.AllowMultiple;
			

			newDTO.Mandatory = ent.Mandatory;
			

			newDTO.PreventFinishing = ent.PreventFinishing;
			

			newDTO.PublishingGroupResourceId = ent.PublishingGroupResourceId;
			

			newDTO.TaskTypeId = ent.TaskTypeId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.ProjectJobTaskType = ent.ProjectJobTaskType.ToDTO(cache);
			
			newDTO.PublishingGroupResource = ent.PublishingGroupResource.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}