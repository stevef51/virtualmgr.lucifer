﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class MimeTypeConverter
	{
	
		public static MimeTypeEntity ToEntity(this MimeTypeDTO dto)
		{
			return dto.ToEntity(new MimeTypeEntity(), new Dictionary<object, object>());
		}

		public static MimeTypeEntity ToEntity(this MimeTypeDTO dto, MimeTypeEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static MimeTypeEntity ToEntity(this MimeTypeDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new MimeTypeEntity(), cache);
		}
	
		public static MimeTypeDTO ToDTO(this MimeTypeEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static MimeTypeEntity ToEntity(this MimeTypeDTO dto, MimeTypeEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (MimeTypeEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Extension = dto.Extension;
			
			

			
			
			newEnt.Id = dto.Id;
			
			

			
			
			if (dto.MediaPreviewGenerator == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)MimeTypeFieldIndex.MediaPreviewGenerator, "");
				
			}
			
			newEnt.MediaPreviewGenerator = dto.MediaPreviewGenerator;
			
			

			
			
			newEnt.MimeType = dto.MimeType;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static MimeTypeDTO ToDTO(this MimeTypeEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (MimeTypeDTO)cache[ent];
			
			var newDTO = new MimeTypeDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Extension = ent.Extension;
			

			newDTO.Id = ent.Id;
			

			newDTO.MediaPreviewGenerator = ent.MediaPreviewGenerator;
			

			newDTO.MimeType = ent.MimeType;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}