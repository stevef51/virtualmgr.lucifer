﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class HierarchyConverter
	{
	
		public static HierarchyEntity ToEntity(this HierarchyDTO dto)
		{
			return dto.ToEntity(new HierarchyEntity(), new Dictionary<object, object>());
		}

		public static HierarchyEntity ToEntity(this HierarchyDTO dto, HierarchyEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static HierarchyEntity ToEntity(this HierarchyDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new HierarchyEntity(), cache);
		}
	
		public static HierarchyDTO ToDTO(this HierarchyEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static HierarchyEntity ToEntity(this HierarchyDTO dto, HierarchyEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (HierarchyEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Id = dto.Id;
			
			

			
			
			newEnt.Name = dto.Name;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			
			
			foreach (var related in dto.FacilityStructures)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.FacilityStructures.Contains(relatedEntity))
				{
					newEnt.FacilityStructures.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.Rosters)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.Rosters.Contains(relatedEntity))
				{
					newEnt.Rosters.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.HierarchyBuckets)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.HierarchyBuckets.Contains(relatedEntity))
				{
					newEnt.HierarchyBuckets.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static HierarchyDTO ToDTO(this HierarchyEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (HierarchyDTO)cache[ent];
			
			var newDTO = new HierarchyDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Id = ent.Id;
			

			newDTO.Name = ent.Name;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			
			
			foreach (var related in ent.FacilityStructures)
			{
				newDTO.FacilityStructures.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.Rosters)
			{
				newDTO.Rosters.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.HierarchyBuckets)
			{
				newDTO.HierarchyBuckets.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}