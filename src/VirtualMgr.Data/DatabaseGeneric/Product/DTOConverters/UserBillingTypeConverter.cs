﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class UserBillingTypeConverter
	{
	
		public static UserBillingTypeEntity ToEntity(this UserBillingTypeDTO dto)
		{
			return dto.ToEntity(new UserBillingTypeEntity(), new Dictionary<object, object>());
		}

		public static UserBillingTypeEntity ToEntity(this UserBillingTypeDTO dto, UserBillingTypeEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static UserBillingTypeEntity ToEntity(this UserBillingTypeDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new UserBillingTypeEntity(), cache);
		}
	
		public static UserBillingTypeDTO ToDTO(this UserBillingTypeEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static UserBillingTypeEntity ToEntity(this UserBillingTypeDTO dto, UserBillingTypeEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (UserBillingTypeEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.AmountPerMonth = dto.AmountPerMonth;
			
			

			
			
			newEnt.Archived = dto.Archived;
			
			

			
			
			newEnt.Id = dto.Id;
			
			

			
			
			newEnt.Name = dto.Name;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			
			
			foreach (var related in dto.UserTypes)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.UserTypes.Contains(relatedEntity))
				{
					newEnt.UserTypes.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static UserBillingTypeDTO ToDTO(this UserBillingTypeEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (UserBillingTypeDTO)cache[ent];
			
			var newDTO = new UserBillingTypeDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.AmountPerMonth = ent.AmountPerMonth;
			

			newDTO.Archived = ent.Archived;
			

			newDTO.Id = ent.Id;
			

			newDTO.Name = ent.Name;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			
			
			foreach (var related in ent.UserTypes)
			{
				newDTO.UserTypes.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}