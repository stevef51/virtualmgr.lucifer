﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class MediaTranslatedConverter
	{
	
		public static MediaTranslatedEntity ToEntity(this MediaTranslatedDTO dto)
		{
			return dto.ToEntity(new MediaTranslatedEntity(), new Dictionary<object, object>());
		}

		public static MediaTranslatedEntity ToEntity(this MediaTranslatedDTO dto, MediaTranslatedEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static MediaTranslatedEntity ToEntity(this MediaTranslatedDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new MediaTranslatedEntity(), cache);
		}
	
		public static MediaTranslatedDTO ToDTO(this MediaTranslatedEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static MediaTranslatedEntity ToEntity(this MediaTranslatedDTO dto, MediaTranslatedEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (MediaTranslatedEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.CultureName = dto.CultureName;
			
			

			
			
			newEnt.Description = dto.Description;
			
			

			
			
			newEnt.Filename = dto.Filename;
			
			

			
			
			newEnt.Id = dto.Id;
			
			

			
			
			newEnt.Name = dto.Name;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.Medium = dto.Medium.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static MediaTranslatedDTO ToDTO(this MediaTranslatedEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (MediaTranslatedDTO)cache[ent];
			
			var newDTO = new MediaTranslatedDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.CultureName = ent.CultureName;
			

			newDTO.Description = ent.Description;
			

			newDTO.Filename = ent.Filename;
			

			newDTO.Id = ent.Id;
			

			newDTO.Name = ent.Name;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.Medium = ent.Medium.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}