﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class CompletedUserDimensionConverter
	{
	
		public static CompletedUserDimensionEntity ToEntity(this CompletedUserDimensionDTO dto)
		{
			return dto.ToEntity(new CompletedUserDimensionEntity(), new Dictionary<object, object>());
		}

		public static CompletedUserDimensionEntity ToEntity(this CompletedUserDimensionDTO dto, CompletedUserDimensionEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static CompletedUserDimensionEntity ToEntity(this CompletedUserDimensionDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new CompletedUserDimensionEntity(), cache);
		}
	
		public static CompletedUserDimensionDTO ToDTO(this CompletedUserDimensionEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static CompletedUserDimensionEntity ToEntity(this CompletedUserDimensionDTO dto, CompletedUserDimensionEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (CompletedUserDimensionEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			if (dto.Email == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)CompletedUserDimensionFieldIndex.Email, "");
				
			}
			
			newEnt.Email = dto.Email;
			
			

			
			
			newEnt.Id = dto.Id;
			
			

			
			
			newEnt.Name = dto.Name;
			
			

			
			
			newEnt.TimeZone = dto.TimeZone;
			
			

			
			
			newEnt.Username = dto.Username;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			
			
			foreach (var related in dto.CompletedWorkingDocumentFactsAsReviewee)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.CompletedWorkingDocumentFactsAsReviewee.Contains(relatedEntity))
				{
					newEnt.CompletedWorkingDocumentFactsAsReviewee.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.CompletedWorkingDocumentFactsAsReviewer)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.CompletedWorkingDocumentFactsAsReviewer.Contains(relatedEntity))
				{
					newEnt.CompletedWorkingDocumentFactsAsReviewer.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.CompletedWorkingDocumentPresentedFactsReviewee)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.CompletedWorkingDocumentPresentedFactsReviewee.Contains(relatedEntity))
				{
					newEnt.CompletedWorkingDocumentPresentedFactsReviewee.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.CompletedWorkingDocumentPresentedFactsReviewer)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.CompletedWorkingDocumentPresentedFactsReviewer.Contains(relatedEntity))
				{
					newEnt.CompletedWorkingDocumentPresentedFactsReviewer.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static CompletedUserDimensionDTO ToDTO(this CompletedUserDimensionEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (CompletedUserDimensionDTO)cache[ent];
			
			var newDTO = new CompletedUserDimensionDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Email = ent.Email;
			

			newDTO.Id = ent.Id;
			

			newDTO.Name = ent.Name;
			

			newDTO.TimeZone = ent.TimeZone;
			

			newDTO.Username = ent.Username;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			
			
			foreach (var related in ent.CompletedWorkingDocumentFactsAsReviewee)
			{
				newDTO.CompletedWorkingDocumentFactsAsReviewee.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.CompletedWorkingDocumentFactsAsReviewer)
			{
				newDTO.CompletedWorkingDocumentFactsAsReviewer.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.CompletedWorkingDocumentPresentedFactsReviewee)
			{
				newDTO.CompletedWorkingDocumentPresentedFactsReviewee.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.CompletedWorkingDocumentPresentedFactsReviewer)
			{
				newDTO.CompletedWorkingDocumentPresentedFactsReviewer.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}