﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class TimesheetItemConverter
	{
	
		public static TimesheetItemEntity ToEntity(this TimesheetItemDTO dto)
		{
			return dto.ToEntity(new TimesheetItemEntity(), new Dictionary<object, object>());
		}

		public static TimesheetItemEntity ToEntity(this TimesheetItemDTO dto, TimesheetItemEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static TimesheetItemEntity ToEntity(this TimesheetItemDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new TimesheetItemEntity(), cache);
		}
	
		public static TimesheetItemDTO ToDTO(this TimesheetItemEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static TimesheetItemEntity ToEntity(this TimesheetItemDTO dto, TimesheetItemEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (TimesheetItemEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			if (dto.ActualDuration == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)TimesheetItemFieldIndex.ActualDuration, default(System.Decimal));
				
			}
			
			newEnt.ActualDuration = dto.ActualDuration;
			
			

			
			
			newEnt.ActualEndDate = dto.ActualEndDate;
			
			

			
			
			newEnt.ActualStartDate = dto.ActualStartDate;
			
			

			
			
			newEnt.ApprovedByUserId = dto.ApprovedByUserId;
			
			

			
			
			newEnt.ApprovedDuration = dto.ApprovedDuration;
			
			

			
			
			newEnt.ApprovedEndDate = dto.ApprovedEndDate;
			
			

			
			
			newEnt.ApprovedStartDate = dto.ApprovedStartDate;
			
			

			
			
			newEnt.Date = dto.Date;
			
			

			
			
			newEnt.DateCreated = dto.DateCreated;
			
			

			
			
			if (dto.ExportedDate == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)TimesheetItemFieldIndex.ExportedDate, default(System.DateTime));
				
			}
			
			newEnt.ExportedDate = dto.ExportedDate;
			
			

			
			
			if (dto.HierarchyBucketOverrideTypeId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)TimesheetItemFieldIndex.HierarchyBucketOverrideTypeId, default(System.Int32));
				
			}
			
			newEnt.HierarchyBucketOverrideTypeId = dto.HierarchyBucketOverrideTypeId;
			
			

			
			
			if (dto.HierarchyRoleId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)TimesheetItemFieldIndex.HierarchyRoleId, default(System.Int32));
				
			}
			
			newEnt.HierarchyRoleId = dto.HierarchyRoleId;
			
			

			
			
			newEnt.Id = dto.Id;
			
			
			

			
			
			newEnt.IsLunchPaid = dto.IsLunchPaid;
			
			

			
			
			if (dto.LunchDuration == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)TimesheetItemFieldIndex.LunchDuration, default(System.Decimal));
				
			}
			
			newEnt.LunchDuration = dto.LunchDuration;
			
			

			
			
			if (dto.PayAsTimesheetItemTypeId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)TimesheetItemFieldIndex.PayAsTimesheetItemTypeId, default(System.Int32));
				
			}
			
			newEnt.PayAsTimesheetItemTypeId = dto.PayAsTimesheetItemTypeId;
			
			

			
			
			newEnt.RosterId = dto.RosterId;
			
			

			
			
			if (dto.ScheduledDuration == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)TimesheetItemFieldIndex.ScheduledDuration, default(System.Decimal));
				
			}
			
			newEnt.ScheduledDuration = dto.ScheduledDuration;
			
			

			
			
			if (dto.ScheduledEndDate == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)TimesheetItemFieldIndex.ScheduledEndDate, default(System.DateTime));
				
			}
			
			newEnt.ScheduledEndDate = dto.ScheduledEndDate;
			
			

			
			
			if (dto.ScheduledLunchEndDate == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)TimesheetItemFieldIndex.ScheduledLunchEndDate, default(System.DateTime));
				
			}
			
			newEnt.ScheduledLunchEndDate = dto.ScheduledLunchEndDate;
			
			

			
			
			if (dto.ScheduledLunchStartDate == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)TimesheetItemFieldIndex.ScheduledLunchStartDate, default(System.DateTime));
				
			}
			
			newEnt.ScheduledLunchStartDate = dto.ScheduledLunchStartDate;
			
			

			
			
			if (dto.ScheduledStartDate == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)TimesheetItemFieldIndex.ScheduledStartDate, default(System.DateTime));
				
			}
			
			newEnt.ScheduledStartDate = dto.ScheduledStartDate;
			
			

			
			
			newEnt.TimesheetItemTypeId = dto.TimesheetItemTypeId;
			
			

			
			
			newEnt.UserId = dto.UserId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.Roster = dto.Roster.ToEntity(cache);
			
			newEnt.TimesheetItemType = dto.TimesheetItemType.ToEntity(cache);
			
			newEnt.TimesheetItemType_ = dto.TimesheetItemType_.ToEntity(cache);
			
			newEnt.HierarchyBucketOverrideType = dto.HierarchyBucketOverrideType.ToEntity(cache);
			
			newEnt.HierarchyRole = dto.HierarchyRole.ToEntity(cache);
			
			newEnt.UserData = dto.UserData.ToEntity(cache);
			
			newEnt.UserData_ = dto.UserData_.ToEntity(cache);
			
			
			
			foreach (var related in dto.TimesheetedTaskWorkLogs)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.TimesheetedTaskWorkLogs.Contains(relatedEntity))
				{
					newEnt.TimesheetedTaskWorkLogs.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static TimesheetItemDTO ToDTO(this TimesheetItemEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (TimesheetItemDTO)cache[ent];
			
			var newDTO = new TimesheetItemDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.ActualDuration = ent.ActualDuration;
			

			newDTO.ActualEndDate = ent.ActualEndDate;
			

			newDTO.ActualStartDate = ent.ActualStartDate;
			

			newDTO.ApprovedByUserId = ent.ApprovedByUserId;
			

			newDTO.ApprovedDuration = ent.ApprovedDuration;
			

			newDTO.ApprovedEndDate = ent.ApprovedEndDate;
			

			newDTO.ApprovedStartDate = ent.ApprovedStartDate;
			

			newDTO.Date = ent.Date;
			

			newDTO.DateCreated = ent.DateCreated;
			

			newDTO.ExportedDate = ent.ExportedDate;
			

			newDTO.HierarchyBucketOverrideTypeId = ent.HierarchyBucketOverrideTypeId;
			

			newDTO.HierarchyRoleId = ent.HierarchyRoleId;
			

			newDTO.Id = ent.Id;
			

			newDTO.IsLunchPaid = ent.IsLunchPaid;
			

			newDTO.LunchDuration = ent.LunchDuration;
			

			newDTO.PayAsTimesheetItemTypeId = ent.PayAsTimesheetItemTypeId;
			

			newDTO.RosterId = ent.RosterId;
			

			newDTO.ScheduledDuration = ent.ScheduledDuration;
			

			newDTO.ScheduledEndDate = ent.ScheduledEndDate;
			

			newDTO.ScheduledLunchEndDate = ent.ScheduledLunchEndDate;
			

			newDTO.ScheduledLunchStartDate = ent.ScheduledLunchStartDate;
			

			newDTO.ScheduledStartDate = ent.ScheduledStartDate;
			

			newDTO.TimesheetItemTypeId = ent.TimesheetItemTypeId;
			

			newDTO.UserId = ent.UserId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.Roster = ent.Roster.ToDTO(cache);
			
			newDTO.TimesheetItemType = ent.TimesheetItemType.ToDTO(cache);
			
			newDTO.TimesheetItemType_ = ent.TimesheetItemType_.ToDTO(cache);
			
			newDTO.HierarchyBucketOverrideType = ent.HierarchyBucketOverrideType.ToDTO(cache);
			
			newDTO.HierarchyRole = ent.HierarchyRole.ToDTO(cache);
			
			newDTO.UserData = ent.UserData.ToDTO(cache);
			
			newDTO.UserData_ = ent.UserData_.ToDTO(cache);
			
			
			
			foreach (var related in ent.TimesheetedTaskWorkLogs)
			{
				newDTO.TimesheetedTaskWorkLogs.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}