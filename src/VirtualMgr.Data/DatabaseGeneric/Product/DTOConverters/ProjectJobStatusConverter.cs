﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class ProjectJobStatusConverter
	{
	
		public static ProjectJobStatusEntity ToEntity(this ProjectJobStatusDTO dto)
		{
			return dto.ToEntity(new ProjectJobStatusEntity(), new Dictionary<object, object>());
		}

		public static ProjectJobStatusEntity ToEntity(this ProjectJobStatusDTO dto, ProjectJobStatusEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static ProjectJobStatusEntity ToEntity(this ProjectJobStatusDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new ProjectJobStatusEntity(), cache);
		}
	
		public static ProjectJobStatusDTO ToDTO(this ProjectJobStatusEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static ProjectJobStatusEntity ToEntity(this ProjectJobStatusDTO dto, ProjectJobStatusEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (ProjectJobStatusEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			if (dto.Description == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ProjectJobStatusFieldIndex.Description, "");
				
			}
			
			newEnt.Description = dto.Description;
			
			

			
			
			newEnt.Id = dto.Id;
			
			

			
			
			newEnt.Text = dto.Text;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			
			
			foreach (var related in dto.ProjectJobTasks)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProjectJobTasks.Contains(relatedEntity))
				{
					newEnt.ProjectJobTasks.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ProjectJobTaskTypeStatuses)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProjectJobTaskTypeStatuses.Contains(relatedEntity))
				{
					newEnt.ProjectJobTaskTypeStatuses.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static ProjectJobStatusDTO ToDTO(this ProjectJobStatusEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (ProjectJobStatusDTO)cache[ent];
			
			var newDTO = new ProjectJobStatusDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Description = ent.Description;
			

			newDTO.Id = ent.Id;
			

			newDTO.Text = ent.Text;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			
			
			foreach (var related in ent.ProjectJobTasks)
			{
				newDTO.ProjectJobTasks.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ProjectJobTaskTypeStatuses)
			{
				newDTO.ProjectJobTaskTypeStatuses.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}