﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class MediaFolderFileConverter
	{
	
		public static MediaFolderFileEntity ToEntity(this MediaFolderFileDTO dto)
		{
			return dto.ToEntity(new MediaFolderFileEntity(), new Dictionary<object, object>());
		}

		public static MediaFolderFileEntity ToEntity(this MediaFolderFileDTO dto, MediaFolderFileEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static MediaFolderFileEntity ToEntity(this MediaFolderFileDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new MediaFolderFileEntity(), cache);
		}
	
		public static MediaFolderFileDTO ToDTO(this MediaFolderFileEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static MediaFolderFileEntity ToEntity(this MediaFolderFileDTO dto, MediaFolderFileEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (MediaFolderFileEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.AutoCreated = dto.AutoCreated;
			
			

			
			
			newEnt.Hidden = dto.Hidden;
			
			

			
			
			newEnt.Id = dto.Id;
			
			

			
			
			newEnt.MediaFolderId = dto.MediaFolderId;
			
			

			
			
			newEnt.MediaId = dto.MediaId;
			
			

			
			
			if (dto.ShowViaChecklistId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)MediaFolderFileFieldIndex.ShowViaChecklistId, default(System.Int32));
				
			}
			
			newEnt.ShowViaChecklistId = dto.ShowViaChecklistId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.Medium = dto.Medium.ToEntity(cache);
			
			newEnt.MediaFolder = dto.MediaFolder.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static MediaFolderFileDTO ToDTO(this MediaFolderFileEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (MediaFolderFileDTO)cache[ent];
			
			var newDTO = new MediaFolderFileDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.AutoCreated = ent.AutoCreated;
			

			newDTO.Hidden = ent.Hidden;
			

			newDTO.Id = ent.Id;
			

			newDTO.MediaFolderId = ent.MediaFolderId;
			

			newDTO.MediaId = ent.MediaId;
			

			newDTO.ShowViaChecklistId = ent.ShowViaChecklistId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.Medium = ent.Medium.ToDTO(cache);
			
			newDTO.MediaFolder = ent.MediaFolder.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}