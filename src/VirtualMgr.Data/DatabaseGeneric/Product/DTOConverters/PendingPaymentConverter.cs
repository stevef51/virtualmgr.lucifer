﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class PendingPaymentConverter
	{
	
		public static PendingPaymentEntity ToEntity(this PendingPaymentDTO dto)
		{
			return dto.ToEntity(new PendingPaymentEntity(), new Dictionary<object, object>());
		}

		public static PendingPaymentEntity ToEntity(this PendingPaymentDTO dto, PendingPaymentEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static PendingPaymentEntity ToEntity(this PendingPaymentDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new PendingPaymentEntity(), cache);
		}
	
		public static PendingPaymentDTO ToDTO(this PendingPaymentEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static PendingPaymentEntity ToEntity(this PendingPaymentDTO dto, PendingPaymentEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (PendingPaymentEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			if (dto.ActivatedDateUtc == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)PendingPaymentFieldIndex.ActivatedDateUtc, default(System.DateTime));
				
			}
			
			newEnt.ActivatedDateUtc = dto.ActivatedDateUtc;
			
			

			
			
			newEnt.Archived = dto.Archived;
			
			

			
			
			if (dto.CancelledDateUtc == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)PendingPaymentFieldIndex.CancelledDateUtc, default(System.DateTime));
				
			}
			
			newEnt.CancelledDateUtc = dto.CancelledDateUtc;
			
			

			
			
			newEnt.CreatedDateUtc = dto.CreatedDateUtc;
			
			

			
			
			newEnt.Currency = dto.Currency;
			
			

			
			
			newEnt.Description = dto.Description;
			
			

			
			
			newEnt.DiscountAmount = dto.DiscountAmount;
			
			

			
			
			newEnt.DueAmount = dto.DueAmount;
			
			

			
			
			if (dto.DueDateUtc == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)PendingPaymentFieldIndex.DueDateUtc, default(System.DateTime));
				
			}
			
			newEnt.DueDateUtc = dto.DueDateUtc;
			
			

			
			
			if (dto.FromUtc == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)PendingPaymentFieldIndex.FromUtc, default(System.DateTime));
				
			}
			
			newEnt.FromUtc = dto.FromUtc;
			
			

			
			
			newEnt.FullAmount = dto.FullAmount;
			
			

			
			
			if (dto.GatewayContext == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)PendingPaymentFieldIndex.GatewayContext, "");
				
			}
			
			newEnt.GatewayContext = dto.GatewayContext;
			
			

			
			
			newEnt.GatewayName = dto.GatewayName;
			
			

			
			
			newEnt.Id = dto.Id;
			
			

			
			
			if (dto.InvoiceNumber == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)PendingPaymentFieldIndex.InvoiceNumber, default(System.Int32));
				
			}
			
			newEnt.InvoiceNumber = dto.InvoiceNumber;
			
			

			
			
			if (dto.LastFailedDateUtc == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)PendingPaymentFieldIndex.LastFailedDateUtc, default(System.DateTime));
				
			}
			
			newEnt.LastFailedDateUtc = dto.LastFailedDateUtc;
			
			

			
			
			newEnt.OrderBy = dto.OrderBy;
			
			

			
			
			newEnt.OrderId = dto.OrderId;
			
			

			
			
			if (dto.PaidInFullDateUtc == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)PendingPaymentFieldIndex.PaidInFullDateUtc, default(System.DateTime));
				
			}
			
			newEnt.PaidInFullDateUtc = dto.PaidInFullDateUtc;
			
			

			
			
			if (dto.Quantity == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)PendingPaymentFieldIndex.Quantity, default(System.Decimal));
				
			}
			
			newEnt.Quantity = dto.Quantity;
			
			

			
			
			newEnt.Sandbox = dto.Sandbox;
			
			

			
			
			newEnt.Status = dto.Status;
			
			

			
			
			newEnt.StatusDateUtc = dto.StatusDateUtc;
			
			

			
			
			if (dto.ToUtc == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)PendingPaymentFieldIndex.ToUtc, default(System.DateTime));
				
			}
			
			newEnt.ToUtc = dto.ToUtc;
			
			

			
			
			newEnt.UserId = dto.UserId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.UserData = dto.UserData.ToEntity(cache);
			
			
			
			foreach (var related in dto.PaymentResponses)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.PaymentResponses.Contains(relatedEntity))
				{
					newEnt.PaymentResponses.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static PendingPaymentDTO ToDTO(this PendingPaymentEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (PendingPaymentDTO)cache[ent];
			
			var newDTO = new PendingPaymentDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.ActivatedDateUtc = ent.ActivatedDateUtc;
			

			newDTO.Archived = ent.Archived;
			

			newDTO.CancelledDateUtc = ent.CancelledDateUtc;
			

			newDTO.CreatedDateUtc = ent.CreatedDateUtc;
			

			newDTO.Currency = ent.Currency;
			

			newDTO.Description = ent.Description;
			

			newDTO.DiscountAmount = ent.DiscountAmount;
			

			newDTO.DueAmount = ent.DueAmount;
			

			newDTO.DueDateUtc = ent.DueDateUtc;
			

			newDTO.FromUtc = ent.FromUtc;
			

			newDTO.FullAmount = ent.FullAmount;
			

			newDTO.GatewayContext = ent.GatewayContext;
			

			newDTO.GatewayName = ent.GatewayName;
			

			newDTO.Id = ent.Id;
			

			newDTO.InvoiceNumber = ent.InvoiceNumber;
			

			newDTO.LastFailedDateUtc = ent.LastFailedDateUtc;
			

			newDTO.OrderBy = ent.OrderBy;
			

			newDTO.OrderId = ent.OrderId;
			

			newDTO.PaidInFullDateUtc = ent.PaidInFullDateUtc;
			

			newDTO.Quantity = ent.Quantity;
			

			newDTO.Sandbox = ent.Sandbox;
			

			newDTO.Status = ent.Status;
			

			newDTO.StatusDateUtc = ent.StatusDateUtc;
			

			newDTO.ToUtc = ent.ToUtc;
			

			newDTO.UserId = ent.UserId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.UserData = ent.UserData.ToDTO(cache);
			
			
			
			foreach (var related in ent.PaymentResponses)
			{
				newDTO.PaymentResponses.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}