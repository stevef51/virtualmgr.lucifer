﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class SiteFeatureConverter
	{
	
		public static SiteFeatureEntity ToEntity(this SiteFeatureDTO dto)
		{
			return dto.ToEntity(new SiteFeatureEntity(), new Dictionary<object, object>());
		}

		public static SiteFeatureEntity ToEntity(this SiteFeatureDTO dto, SiteFeatureEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static SiteFeatureEntity ToEntity(this SiteFeatureDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new SiteFeatureEntity(), cache);
		}
	
		public static SiteFeatureDTO ToDTO(this SiteFeatureEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static SiteFeatureEntity ToEntity(this SiteFeatureDTO dto, SiteFeatureEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (SiteFeatureEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Id = dto.Id;
			
			

			
			
			newEnt.Measure = dto.Measure;
			
			

			
			
			newEnt.Name = dto.Name;
			
			

			
			
			newEnt.SiteAreaId = dto.SiteAreaId;
			
			

			
			
			newEnt.SiteId = dto.SiteId;
			
			

			
			
			newEnt.UnitId = dto.UnitId;
			
			

			
			
			newEnt.WorkLoadingFeatureId = dto.WorkLoadingFeatureId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.UserData = dto.UserData.ToEntity(cache);
			
			newEnt.MeasurementUnit = dto.MeasurementUnit.ToEntity(cache);
			
			newEnt.SiteArea = dto.SiteArea.ToEntity(cache);
			
			newEnt.WorkLoadingFeature = dto.WorkLoadingFeature.ToEntity(cache);
			
			
			
			foreach (var related in dto.ProjectJobScheduledTaskWorkloadingActivities)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProjectJobScheduledTaskWorkloadingActivities.Contains(relatedEntity))
				{
					newEnt.ProjectJobScheduledTaskWorkloadingActivities.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ProjectJobTaskWorkloadingActivities)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProjectJobTaskWorkloadingActivities.Contains(relatedEntity))
				{
					newEnt.ProjectJobTaskWorkloadingActivities.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static SiteFeatureDTO ToDTO(this SiteFeatureEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (SiteFeatureDTO)cache[ent];
			
			var newDTO = new SiteFeatureDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Id = ent.Id;
			

			newDTO.Measure = ent.Measure;
			

			newDTO.Name = ent.Name;
			

			newDTO.SiteAreaId = ent.SiteAreaId;
			

			newDTO.SiteId = ent.SiteId;
			

			newDTO.UnitId = ent.UnitId;
			

			newDTO.WorkLoadingFeatureId = ent.WorkLoadingFeatureId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.UserData = ent.UserData.ToDTO(cache);
			
			newDTO.MeasurementUnit = ent.MeasurementUnit.ToDTO(cache);
			
			newDTO.SiteArea = ent.SiteArea.ToDTO(cache);
			
			newDTO.WorkLoadingFeature = ent.WorkLoadingFeature.ToDTO(cache);
			
			
			
			foreach (var related in ent.ProjectJobScheduledTaskWorkloadingActivities)
			{
				newDTO.ProjectJobScheduledTaskWorkloadingActivities.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ProjectJobTaskWorkloadingActivities)
			{
				newDTO.ProjectJobTaskWorkloadingActivities.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}