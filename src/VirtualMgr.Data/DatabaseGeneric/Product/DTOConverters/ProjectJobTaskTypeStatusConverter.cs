﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class ProjectJobTaskTypeStatusConverter
	{
	
		public static ProjectJobTaskTypeStatusEntity ToEntity(this ProjectJobTaskTypeStatusDTO dto)
		{
			return dto.ToEntity(new ProjectJobTaskTypeStatusEntity(), new Dictionary<object, object>());
		}

		public static ProjectJobTaskTypeStatusEntity ToEntity(this ProjectJobTaskTypeStatusDTO dto, ProjectJobTaskTypeStatusEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static ProjectJobTaskTypeStatusEntity ToEntity(this ProjectJobTaskTypeStatusDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new ProjectJobTaskTypeStatusEntity(), cache);
		}
	
		public static ProjectJobTaskTypeStatusDTO ToDTO(this ProjectJobTaskTypeStatusEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static ProjectJobTaskTypeStatusEntity ToEntity(this ProjectJobTaskTypeStatusDTO dto, ProjectJobTaskTypeStatusEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (ProjectJobTaskTypeStatusEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.DefaultToWhenCreated = dto.DefaultToWhenCreated;
			
			

			
			
			newEnt.ReferenceNumberMode = dto.ReferenceNumberMode;
			
			

			
			
			newEnt.SetToWhenFinished = dto.SetToWhenFinished;
			
			

			
			
			newEnt.SortOrder = dto.SortOrder;
			
			

			
			
			newEnt.StatusId = dto.StatusId;
			
			

			
			
			newEnt.TaskTypeId = dto.TaskTypeId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.ProjectJobStatu = dto.ProjectJobStatu.ToEntity(cache);
			
			newEnt.ProjectJobTaskType = dto.ProjectJobTaskType.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static ProjectJobTaskTypeStatusDTO ToDTO(this ProjectJobTaskTypeStatusEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (ProjectJobTaskTypeStatusDTO)cache[ent];
			
			var newDTO = new ProjectJobTaskTypeStatusDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.DefaultToWhenCreated = ent.DefaultToWhenCreated;
			

			newDTO.ReferenceNumberMode = ent.ReferenceNumberMode;
			

			newDTO.SetToWhenFinished = ent.SetToWhenFinished;
			

			newDTO.SortOrder = ent.SortOrder;
			

			newDTO.StatusId = ent.StatusId;
			

			newDTO.TaskTypeId = ent.TaskTypeId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.ProjectJobStatu = ent.ProjectJobStatu.ToDTO(cache);
			
			newDTO.ProjectJobTaskType = ent.ProjectJobTaskType.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}