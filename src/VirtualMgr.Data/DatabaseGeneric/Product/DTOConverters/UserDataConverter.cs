﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class UserDataConverter
	{
	
		public static UserDataEntity ToEntity(this UserDataDTO dto)
		{
			return dto.ToEntity(new UserDataEntity(), new Dictionary<object, object>());
		}

		public static UserDataEntity ToEntity(this UserDataDTO dto, UserDataEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static UserDataEntity ToEntity(this UserDataDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new UserDataEntity(), cache);
		}
	
		public static UserDataDTO ToDTO(this UserDataEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static UserDataEntity ToEntity(this UserDataDTO dto, UserDataEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (UserDataEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			if (dto.AspNetUserId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)UserDataFieldIndex.AspNetUserId, "");
				
			}
			
			newEnt.AspNetUserId = dto.AspNetUserId;
			
			

			
			
			if (dto.AuthorizationProviderKey == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)UserDataFieldIndex.AuthorizationProviderKey, "");
				
			}
			
			newEnt.AuthorizationProviderKey = dto.AuthorizationProviderKey;
			
			

			
			
			if (dto.CommencementDate == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)UserDataFieldIndex.CommencementDate, default(System.DateTime));
				
			}
			
			newEnt.CommencementDate = dto.CommencementDate;
			
			

			
			
			if (dto.CompanyId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)UserDataFieldIndex.CompanyId, default(System.Guid));
				
			}
			
			newEnt.CompanyId = dto.CompanyId;
			
			

			
			
			newEnt.DefaultLanguageCode = dto.DefaultLanguageCode;
			
			

			
			
			if (dto.DefaultTabletProfileId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)UserDataFieldIndex.DefaultTabletProfileId, default(System.Int32));
				
			}
			
			newEnt.DefaultTabletProfileId = dto.DefaultTabletProfileId;
			
			

			
			
			if (dto.ExpiryDate == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)UserDataFieldIndex.ExpiryDate, default(System.DateTime));
				
			}
			
			newEnt.ExpiryDate = dto.ExpiryDate;
			
			

			
			
			if (dto.Latitude == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)UserDataFieldIndex.Latitude, default(System.Decimal));
				
			}
			
			newEnt.Latitude = dto.Latitude;
			
			

			
			
			if (dto.Longitude == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)UserDataFieldIndex.Longitude, default(System.Decimal));
				
			}
			
			newEnt.Longitude = dto.Longitude;
			
			

			
			
			if (dto.MediaId == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)UserDataFieldIndex.MediaId, default(System.Guid));
				
			}
			
			newEnt.MediaId = dto.MediaId;
			
			

			
			
			if (dto.Mobile == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)UserDataFieldIndex.Mobile, "");
				
			}
			
			newEnt.Mobile = dto.Mobile;
			
			

			
			
			if (dto.Name == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)UserDataFieldIndex.Name, "");
				
			}
			
			newEnt.Name = dto.Name;
			
			

			
			
			newEnt.TimeZone = dto.TimeZone;
			
			

			
			
			newEnt.UserId = dto.UserId;
			
			

			
			
			newEnt.UserTypeId = dto.UserTypeId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.AspNetUser = dto.AspNetUser.ToEntity(cache);
			
			newEnt.TabletProfile = dto.TabletProfile.ToEntity(cache);
			
			newEnt.Company = dto.Company.ToEntity(cache);
			
			newEnt.Medium = dto.Medium.ToEntity(cache);
			
			newEnt.UserType = dto.UserType.ToEntity(cache);
			
			
			
			foreach (var related in dto.Beacons)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.Beacons.Contains(relatedEntity))
				{
					newEnt.Beacons.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.FacilityStructures)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.FacilityStructures.Contains(relatedEntity))
				{
					newEnt.FacilityStructures.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.TabletUuids)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.TabletUuids.Contains(relatedEntity))
				{
					newEnt.TabletUuids.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.GatewayCustomers)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.GatewayCustomers.Contains(relatedEntity))
				{
					newEnt.GatewayCustomers.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.PendingPayments)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.PendingPayments.Contains(relatedEntity))
				{
					newEnt.PendingPayments.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ProjectJobScheduledTasks_)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProjectJobScheduledTasks_.Contains(relatedEntity))
				{
					newEnt.ProjectJobScheduledTasks_.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ProjectJobTasks)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProjectJobTasks.Contains(relatedEntity))
				{
					newEnt.ProjectJobTasks.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ProjectJobTasks_)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProjectJobTasks_.Contains(relatedEntity))
				{
					newEnt.ProjectJobTasks_.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ProjectJobTaskChangeRequestsProcessedBy)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProjectJobTaskChangeRequestsProcessedBy.Contains(relatedEntity))
				{
					newEnt.ProjectJobTaskChangeRequestsProcessedBy.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ProjectJobTaskChangeRequestsRequestedBy)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProjectJobTaskChangeRequestsRequestedBy.Contains(relatedEntity))
				{
					newEnt.ProjectJobTaskChangeRequestsRequestedBy.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ProjectJobTaskWorkLogs)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProjectJobTaskWorkLogs.Contains(relatedEntity))
				{
					newEnt.ProjectJobTaskWorkLogs.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.TimesheetItems)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.TimesheetItems.Contains(relatedEntity))
				{
					newEnt.TimesheetItems.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.TimesheetItems_)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.TimesheetItems_.Contains(relatedEntity))
				{
					newEnt.TimesheetItems_.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.HierarchyBuckets)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.HierarchyBuckets.Contains(relatedEntity))
				{
					newEnt.HierarchyBuckets.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.OriginalUserHierarchyBucketOverrides)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.OriginalUserHierarchyBucketOverrides.Contains(relatedEntity))
				{
					newEnt.OriginalUserHierarchyBucketOverrides.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.UserDataHierarchyBucketOverrides)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.UserDataHierarchyBucketOverrides.Contains(relatedEntity))
				{
					newEnt.UserDataHierarchyBucketOverrides.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.LabelUsers)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.LabelUsers.Contains(relatedEntity))
				{
					newEnt.LabelUsers.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.MailboxesReceived)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.MailboxesReceived.Contains(relatedEntity))
				{
					newEnt.MailboxesReceived.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.Medias)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.Medias.Contains(relatedEntity))
				{
					newEnt.Medias.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.MediaVersionsCreated)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.MediaVersionsCreated.Contains(relatedEntity))
				{
					newEnt.MediaVersionsCreated.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.MediaVersionsReplaced)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.MediaVersionsReplaced.Contains(relatedEntity))
				{
					newEnt.MediaVersionsReplaced.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.MediaViewings)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.MediaViewings.Contains(relatedEntity))
				{
					newEnt.MediaViewings.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.MessagesSent)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.MessagesSent.Contains(relatedEntity))
				{
					newEnt.MessagesSent.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.NewsFeeds)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.NewsFeeds.Contains(relatedEntity))
				{
					newEnt.NewsFeeds.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.PasswordResetTickets)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.PasswordResetTickets.Contains(relatedEntity))
				{
					newEnt.PasswordResetTickets.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.PublishingGroupActors)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.PublishingGroupActors.Contains(relatedEntity))
				{
					newEnt.PublishingGroupActors.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.Qrcodes)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.Qrcodes.Contains(relatedEntity))
				{
					newEnt.Qrcodes.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.Orders)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.Orders.Contains(relatedEntity))
				{
					newEnt.Orders.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.UserContextDatas)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.UserContextDatas.Contains(relatedEntity))
				{
					newEnt.UserContextDatas.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.UserLogs)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.UserLogs.Contains(relatedEntity))
				{
					newEnt.UserLogs.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.UserLogs_)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.UserLogs_.Contains(relatedEntity))
				{
					newEnt.UserLogs_.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.UserMedias)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.UserMedias.Contains(relatedEntity))
				{
					newEnt.UserMedias.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.SiteAreas)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.SiteAreas.Contains(relatedEntity))
				{
					newEnt.SiteAreas.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.SiteFeatures)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.SiteFeatures.Contains(relatedEntity))
				{
					newEnt.SiteFeatures.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.WorkingDocumentsReviewee)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.WorkingDocumentsReviewee.Contains(relatedEntity))
				{
					newEnt.WorkingDocumentsReviewee.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.WorkingDocumentsReviewer)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.WorkingDocumentsReviewer.Contains(relatedEntity))
				{
					newEnt.WorkingDocumentsReviewer.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static UserDataDTO ToDTO(this UserDataEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (UserDataDTO)cache[ent];
			
			var newDTO = new UserDataDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.AspNetUserId = ent.AspNetUserId;
			

			newDTO.AuthorizationProviderKey = ent.AuthorizationProviderKey;
			

			newDTO.CommencementDate = ent.CommencementDate;
			

			newDTO.CompanyId = ent.CompanyId;
			

			newDTO.DefaultLanguageCode = ent.DefaultLanguageCode;
			

			newDTO.DefaultTabletProfileId = ent.DefaultTabletProfileId;
			

			newDTO.ExpiryDate = ent.ExpiryDate;
			

			newDTO.Latitude = ent.Latitude;
			

			newDTO.Longitude = ent.Longitude;
			

			newDTO.MediaId = ent.MediaId;
			

			newDTO.Mobile = ent.Mobile;
			

			newDTO.Name = ent.Name;
			

			newDTO.TimeZone = ent.TimeZone;
			

			newDTO.UserId = ent.UserId;
			

			newDTO.UserTypeId = ent.UserTypeId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.AspNetUser = ent.AspNetUser.ToDTO(cache);
			
			newDTO.TabletProfile = ent.TabletProfile.ToDTO(cache);
			
			newDTO.Company = ent.Company.ToDTO(cache);
			
			newDTO.Medium = ent.Medium.ToDTO(cache);
			
			newDTO.UserType = ent.UserType.ToDTO(cache);
			
			
			
			foreach (var related in ent.Beacons)
			{
				newDTO.Beacons.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.FacilityStructures)
			{
				newDTO.FacilityStructures.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.TabletUuids)
			{
				newDTO.TabletUuids.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.GatewayCustomers)
			{
				newDTO.GatewayCustomers.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.PendingPayments)
			{
				newDTO.PendingPayments.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ProjectJobScheduledTasks_)
			{
				newDTO.ProjectJobScheduledTasks_.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ProjectJobTasks)
			{
				newDTO.ProjectJobTasks.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ProjectJobTasks_)
			{
				newDTO.ProjectJobTasks_.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ProjectJobTaskChangeRequestsProcessedBy)
			{
				newDTO.ProjectJobTaskChangeRequestsProcessedBy.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ProjectJobTaskChangeRequestsRequestedBy)
			{
				newDTO.ProjectJobTaskChangeRequestsRequestedBy.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ProjectJobTaskWorkLogs)
			{
				newDTO.ProjectJobTaskWorkLogs.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.TimesheetItems)
			{
				newDTO.TimesheetItems.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.TimesheetItems_)
			{
				newDTO.TimesheetItems_.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.HierarchyBuckets)
			{
				newDTO.HierarchyBuckets.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.OriginalUserHierarchyBucketOverrides)
			{
				newDTO.OriginalUserHierarchyBucketOverrides.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.UserDataHierarchyBucketOverrides)
			{
				newDTO.UserDataHierarchyBucketOverrides.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.LabelUsers)
			{
				newDTO.LabelUsers.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.MailboxesReceived)
			{
				newDTO.MailboxesReceived.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.Medias)
			{
				newDTO.Medias.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.MediaVersionsCreated)
			{
				newDTO.MediaVersionsCreated.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.MediaVersionsReplaced)
			{
				newDTO.MediaVersionsReplaced.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.MediaViewings)
			{
				newDTO.MediaViewings.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.MessagesSent)
			{
				newDTO.MessagesSent.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.NewsFeeds)
			{
				newDTO.NewsFeeds.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.PasswordResetTickets)
			{
				newDTO.PasswordResetTickets.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.PublishingGroupActors)
			{
				newDTO.PublishingGroupActors.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.Qrcodes)
			{
				newDTO.Qrcodes.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.Orders)
			{
				newDTO.Orders.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.UserContextDatas)
			{
				newDTO.UserContextDatas.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.UserLogs)
			{
				newDTO.UserLogs.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.UserLogs_)
			{
				newDTO.UserLogs_.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.UserMedias)
			{
				newDTO.UserMedias.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.SiteAreas)
			{
				newDTO.SiteAreas.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.SiteFeatures)
			{
				newDTO.SiteFeatures.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.WorkingDocumentsReviewee)
			{
				newDTO.WorkingDocumentsReviewee.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.WorkingDocumentsReviewer)
			{
				newDTO.WorkingDocumentsReviewer.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}