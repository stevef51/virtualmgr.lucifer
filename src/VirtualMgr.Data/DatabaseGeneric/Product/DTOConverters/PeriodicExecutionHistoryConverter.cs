﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class PeriodicExecutionHistoryConverter
	{
	
		public static PeriodicExecutionHistoryEntity ToEntity(this PeriodicExecutionHistoryDTO dto)
		{
			return dto.ToEntity(new PeriodicExecutionHistoryEntity(), new Dictionary<object, object>());
		}

		public static PeriodicExecutionHistoryEntity ToEntity(this PeriodicExecutionHistoryDTO dto, PeriodicExecutionHistoryEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static PeriodicExecutionHistoryEntity ToEntity(this PeriodicExecutionHistoryDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new PeriodicExecutionHistoryEntity(), cache);
		}
	
		public static PeriodicExecutionHistoryDTO ToDTO(this PeriodicExecutionHistoryEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static PeriodicExecutionHistoryEntity ToEntity(this PeriodicExecutionHistoryDTO dto, PeriodicExecutionHistoryEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (PeriodicExecutionHistoryEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			if (dto.FinishUtc == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)PeriodicExecutionHistoryFieldIndex.FinishUtc, default(System.DateTime));
				
			}
			
			newEnt.FinishUtc = dto.FinishUtc;
			
			

			
			
			newEnt.ForceFinished = dto.ForceFinished;
			
			

			
			
			newEnt.Id = dto.Id;
			
			
			

			
			
			newEnt.StartUtc = dto.StartUtc;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static PeriodicExecutionHistoryDTO ToDTO(this PeriodicExecutionHistoryEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (PeriodicExecutionHistoryDTO)cache[ent];
			
			var newDTO = new PeriodicExecutionHistoryDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.FinishUtc = ent.FinishUtc;
			

			newDTO.ForceFinished = ent.ForceFinished;
			

			newDTO.Id = ent.Id;
			

			newDTO.StartUtc = ent.StartUtc;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}