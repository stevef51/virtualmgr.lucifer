﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class CompletedPresentedGeoLocationFactValueConverter
	{
	
		public static CompletedPresentedGeoLocationFactValueEntity ToEntity(this CompletedPresentedGeoLocationFactValueDTO dto)
		{
			return dto.ToEntity(new CompletedPresentedGeoLocationFactValueEntity(), new Dictionary<object, object>());
		}

		public static CompletedPresentedGeoLocationFactValueEntity ToEntity(this CompletedPresentedGeoLocationFactValueDTO dto, CompletedPresentedGeoLocationFactValueEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static CompletedPresentedGeoLocationFactValueEntity ToEntity(this CompletedPresentedGeoLocationFactValueDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new CompletedPresentedGeoLocationFactValueEntity(), cache);
		}
	
		public static CompletedPresentedGeoLocationFactValueDTO ToDTO(this CompletedPresentedGeoLocationFactValueEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static CompletedPresentedGeoLocationFactValueEntity ToEntity(this CompletedPresentedGeoLocationFactValueDTO dto, CompletedPresentedGeoLocationFactValueEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (CompletedPresentedGeoLocationFactValueEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Accuracy = dto.Accuracy;
			
			

			
			
			if (dto.Altitude == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)CompletedPresentedGeoLocationFactValueFieldIndex.Altitude, default(System.Decimal));
				
			}
			
			newEnt.Altitude = dto.Altitude;
			
			

			
			
			if (dto.AltitudeAccuracy == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)CompletedPresentedGeoLocationFactValueFieldIndex.AltitudeAccuracy, default(System.Decimal));
				
			}
			
			newEnt.AltitudeAccuracy = dto.AltitudeAccuracy;
			
			

			
			
			newEnt.CompletedWorkingDocumentPresentedFactId = dto.CompletedWorkingDocumentPresentedFactId;
			
			

			
			
			if (dto.Heading == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)CompletedPresentedGeoLocationFactValueFieldIndex.Heading, default(System.Decimal));
				
			}
			
			newEnt.Heading = dto.Heading;
			
			

			
			
			newEnt.Id = dto.Id;
			
			

			
			
			newEnt.Latitude = dto.Latitude;
			
			

			
			
			newEnt.Longitude = dto.Longitude;
			
			

			
			
			if (dto.Speed == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)CompletedPresentedGeoLocationFactValueFieldIndex.Speed, default(System.Decimal));
				
			}
			
			newEnt.Speed = dto.Speed;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.CompletedWorkingDocumentPresentedFact = dto.CompletedWorkingDocumentPresentedFact.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static CompletedPresentedGeoLocationFactValueDTO ToDTO(this CompletedPresentedGeoLocationFactValueEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (CompletedPresentedGeoLocationFactValueDTO)cache[ent];
			
			var newDTO = new CompletedPresentedGeoLocationFactValueDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Accuracy = ent.Accuracy;
			

			newDTO.Altitude = ent.Altitude;
			

			newDTO.AltitudeAccuracy = ent.AltitudeAccuracy;
			

			newDTO.CompletedWorkingDocumentPresentedFactId = ent.CompletedWorkingDocumentPresentedFactId;
			

			newDTO.Heading = ent.Heading;
			

			newDTO.Id = ent.Id;
			

			newDTO.Latitude = ent.Latitude;
			

			newDTO.Longitude = ent.Longitude;
			

			newDTO.Speed = ent.Speed;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.CompletedWorkingDocumentPresentedFact = ent.CompletedWorkingDocumentPresentedFact.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}