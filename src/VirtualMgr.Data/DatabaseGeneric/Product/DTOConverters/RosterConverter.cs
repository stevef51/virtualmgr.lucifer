﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class RosterConverter
	{
	
		public static RosterEntity ToEntity(this RosterDTO dto)
		{
			return dto.ToEntity(new RosterEntity(), new Dictionary<object, object>());
		}

		public static RosterEntity ToEntity(this RosterDTO dto, RosterEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static RosterEntity ToEntity(this RosterDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new RosterEntity(), cache);
		}
	
		public static RosterDTO ToDTO(this RosterEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static RosterEntity ToEntity(this RosterDTO dto, RosterEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (RosterEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			if (dto.AutomaticEndOfDayTime == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)RosterFieldIndex.AutomaticEndOfDayTime, default(System.DateTime));
				
			}
			
			newEnt.AutomaticEndOfDayTime = dto.AutomaticEndOfDayTime;
			
			

			
			
			if (dto.AutomaticEndOfDayTimeLastRun == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)RosterFieldIndex.AutomaticEndOfDayTimeLastRun, default(System.DateTime));
				
			}
			
			newEnt.AutomaticEndOfDayTimeLastRun = dto.AutomaticEndOfDayTimeLastRun;
			
			

			
			
			if (dto.AutomaticScheduleApprovalTime == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)RosterFieldIndex.AutomaticScheduleApprovalTime, default(System.DateTime));
				
			}
			
			newEnt.AutomaticScheduleApprovalTime = dto.AutomaticScheduleApprovalTime;
			
			

			
			
			if (dto.AutomaticScheduleApprovalTimeLastRun == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)RosterFieldIndex.AutomaticScheduleApprovalTimeLastRun, default(System.DateTime));
				
			}
			
			newEnt.AutomaticScheduleApprovalTimeLastRun = dto.AutomaticScheduleApprovalTimeLastRun;
			
			

			
			
			newEnt.EndOfDayDefaultTaskStatus = dto.EndOfDayDefaultTaskStatus;
			
			

			
			
			newEnt.EndTime = dto.EndTime;
			
			

			
			
			newEnt.HierarchyId = dto.HierarchyId;
			
			

			
			
			newEnt.Id = dto.Id;
			
			
			

			
			
			newEnt.IncludeLunchInTimesheetDurations = dto.IncludeLunchInTimesheetDurations;
			
			

			
			
			newEnt.Name = dto.Name;
			
			

			
			
			newEnt.ProvidesClaimableDutyList = dto.ProvidesClaimableDutyList;
			
			

			
			
			newEnt.ReceivesClaimableDutyList = dto.ReceivesClaimableDutyList;
			
			

			
			
			newEnt.RoundTimesheetToNearestMethod = dto.RoundTimesheetToNearestMethod;
			
			

			
			
			newEnt.RoundTimesheetToNearestMins = dto.RoundTimesheetToNearestMins;
			
			

			
			
			newEnt.ScheduleApprovalDefaultTaskStatus = dto.ScheduleApprovalDefaultTaskStatus;
			
			

			
			
			newEnt.SortOrder = dto.SortOrder;
			
			

			
			
			newEnt.StartTime = dto.StartTime;
			
			

			
			
			newEnt.TimesheetApprovedSourceMethod = dto.TimesheetApprovedSourceMethod;
			
			

			
			
			newEnt.TimesheetUseScheduledEndWhenFinishedBySystem = dto.TimesheetUseScheduledEndWhenFinishedBySystem;
			
			

			
			
			newEnt.Timezone = dto.Timezone;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.Hierarchy = dto.Hierarchy.ToEntity(cache);
			
			
			
			foreach (var related in dto.ProjectJobScheduleDays)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProjectJobScheduleDays.Contains(relatedEntity))
				{
					newEnt.ProjectJobScheduleDays.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ProjectJobScheduledTasks)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProjectJobScheduledTasks.Contains(relatedEntity))
				{
					newEnt.ProjectJobScheduledTasks.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ProjectJobTasks)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ProjectJobTasks.Contains(relatedEntity))
				{
					newEnt.ProjectJobTasks.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.FromProjectJobTaskChangeRequests)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.FromProjectJobTaskChangeRequests.Contains(relatedEntity))
				{
					newEnt.FromProjectJobTaskChangeRequests.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.ToProjectJobTaskChangeRequests)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.ToProjectJobTaskChangeRequests.Contains(relatedEntity))
				{
					newEnt.ToProjectJobTaskChangeRequests.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.RosterEventConditions)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.RosterEventConditions.Contains(relatedEntity))
				{
					newEnt.RosterEventConditions.Add(relatedEntity);
				}
			}
			
			foreach (var related in dto.TimesheetItems)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.TimesheetItems.Contains(relatedEntity))
				{
					newEnt.TimesheetItems.Add(relatedEntity);
				}
			}
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static RosterDTO ToDTO(this RosterEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (RosterDTO)cache[ent];
			
			var newDTO = new RosterDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.AutomaticEndOfDayTime = ent.AutomaticEndOfDayTime;
			

			newDTO.AutomaticEndOfDayTimeLastRun = ent.AutomaticEndOfDayTimeLastRun;
			

			newDTO.AutomaticScheduleApprovalTime = ent.AutomaticScheduleApprovalTime;
			

			newDTO.AutomaticScheduleApprovalTimeLastRun = ent.AutomaticScheduleApprovalTimeLastRun;
			

			newDTO.EndOfDayDefaultTaskStatus = ent.EndOfDayDefaultTaskStatus;
			

			newDTO.EndTime = ent.EndTime;
			

			newDTO.HierarchyId = ent.HierarchyId;
			

			newDTO.Id = ent.Id;
			

			newDTO.IncludeLunchInTimesheetDurations = ent.IncludeLunchInTimesheetDurations;
			

			newDTO.Name = ent.Name;
			

			newDTO.ProvidesClaimableDutyList = ent.ProvidesClaimableDutyList;
			

			newDTO.ReceivesClaimableDutyList = ent.ReceivesClaimableDutyList;
			

			newDTO.RoundTimesheetToNearestMethod = ent.RoundTimesheetToNearestMethod;
			

			newDTO.RoundTimesheetToNearestMins = ent.RoundTimesheetToNearestMins;
			

			newDTO.ScheduleApprovalDefaultTaskStatus = ent.ScheduleApprovalDefaultTaskStatus;
			

			newDTO.SortOrder = ent.SortOrder;
			

			newDTO.StartTime = ent.StartTime;
			

			newDTO.TimesheetApprovedSourceMethod = ent.TimesheetApprovedSourceMethod;
			

			newDTO.TimesheetUseScheduledEndWhenFinishedBySystem = ent.TimesheetUseScheduledEndWhenFinishedBySystem;
			

			newDTO.Timezone = ent.Timezone;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.Hierarchy = ent.Hierarchy.ToDTO(cache);
			
			
			
			foreach (var related in ent.ProjectJobScheduleDays)
			{
				newDTO.ProjectJobScheduleDays.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ProjectJobScheduledTasks)
			{
				newDTO.ProjectJobScheduledTasks.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ProjectJobTasks)
			{
				newDTO.ProjectJobTasks.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.FromProjectJobTaskChangeRequests)
			{
				newDTO.FromProjectJobTaskChangeRequests.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.ToProjectJobTaskChangeRequests)
			{
				newDTO.ToProjectJobTaskChangeRequests.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.RosterEventConditions)
			{
				newDTO.RosterEventConditions.Add(related.ToDTO(cache));
			}
			
			foreach (var related in ent.TimesheetItems)
			{
				newDTO.TimesheetItems.Add(related.ToDTO(cache));
			}
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}