﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class ReportTicketConverter
	{
	
		public static ReportTicketEntity ToEntity(this ReportTicketDTO dto)
		{
			return dto.ToEntity(new ReportTicketEntity(), new Dictionary<object, object>());
		}

		public static ReportTicketEntity ToEntity(this ReportTicketDTO dto, ReportTicketEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static ReportTicketEntity ToEntity(this ReportTicketDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new ReportTicketEntity(), cache);
		}
	
		public static ReportTicketDTO ToDTO(this ReportTicketEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static ReportTicketEntity ToEntity(this ReportTicketDTO dto, ReportTicketEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (ReportTicketEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.DateCreatedUtc = dto.DateCreatedUtc;
			
			

			
			
			if (dto.DefaultFormat == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ReportTicketFieldIndex.DefaultFormat, "");
				
			}
			
			newEnt.DefaultFormat = dto.DefaultFormat;
			
			

			
			
			newEnt.Id = dto.Id;
			
			

			
			
			if (dto.JsonParameters == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ReportTicketFieldIndex.JsonParameters, "");
				
			}
			
			newEnt.JsonParameters = dto.JsonParameters;
			
			

			
			
			if (dto.LastRunOnUtc == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)ReportTicketFieldIndex.LastRunOnUtc, default(System.DateTime));
				
			}
			
			newEnt.LastRunOnUtc = dto.LastRunOnUtc;
			
			

			
			
			newEnt.ReportId = dto.ReportId;
			
			

			
			
			newEnt.RunAsUserId = dto.RunAsUserId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.Report = dto.Report.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static ReportTicketDTO ToDTO(this ReportTicketEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (ReportTicketDTO)cache[ent];
			
			var newDTO = new ReportTicketDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.DateCreatedUtc = ent.DateCreatedUtc;
			

			newDTO.DefaultFormat = ent.DefaultFormat;
			

			newDTO.Id = ent.Id;
			

			newDTO.JsonParameters = ent.JsonParameters;
			

			newDTO.LastRunOnUtc = ent.LastRunOnUtc;
			

			newDTO.ReportId = ent.ReportId;
			

			newDTO.RunAsUserId = ent.RunAsUserId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.Report = ent.Report.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}