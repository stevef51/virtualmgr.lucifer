﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class HierarchyRoleUserTypeConverter
	{
	
		public static HierarchyRoleUserTypeEntity ToEntity(this HierarchyRoleUserTypeDTO dto)
		{
			return dto.ToEntity(new HierarchyRoleUserTypeEntity(), new Dictionary<object, object>());
		}

		public static HierarchyRoleUserTypeEntity ToEntity(this HierarchyRoleUserTypeDTO dto, HierarchyRoleUserTypeEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static HierarchyRoleUserTypeEntity ToEntity(this HierarchyRoleUserTypeDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new HierarchyRoleUserTypeEntity(), cache);
		}
	
		public static HierarchyRoleUserTypeDTO ToDTO(this HierarchyRoleUserTypeEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static HierarchyRoleUserTypeEntity ToEntity(this HierarchyRoleUserTypeDTO dto, HierarchyRoleUserTypeEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (HierarchyRoleUserTypeEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.Id = dto.Id;
			
			
			

			
			
			newEnt.RoleId = dto.RoleId;
			
			

			
			
			newEnt.UserTypeId = dto.UserTypeId;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.HierarchyRole = dto.HierarchyRole.ToEntity(cache);
			
			newEnt.UserType = dto.UserType.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static HierarchyRoleUserTypeDTO ToDTO(this HierarchyRoleUserTypeEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (HierarchyRoleUserTypeDTO)cache[ent];
			
			var newDTO = new HierarchyRoleUserTypeDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.Id = ent.Id;
			

			newDTO.RoleId = ent.RoleId;
			

			newDTO.UserTypeId = ent.UserTypeId;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.HierarchyRole = ent.HierarchyRole.ToDTO(cache);
			
			newDTO.UserType = ent.UserType.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}