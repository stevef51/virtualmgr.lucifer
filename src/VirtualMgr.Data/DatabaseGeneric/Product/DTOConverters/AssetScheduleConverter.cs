﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class AssetScheduleConverter
	{
	
		public static AssetScheduleEntity ToEntity(this AssetScheduleDTO dto)
		{
			return dto.ToEntity(new AssetScheduleEntity(), new Dictionary<object, object>());
		}

		public static AssetScheduleEntity ToEntity(this AssetScheduleDTO dto, AssetScheduleEntity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static AssetScheduleEntity ToEntity(this AssetScheduleDTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new AssetScheduleEntity(), cache);
		}
	
		public static AssetScheduleDTO ToDTO(this AssetScheduleEntity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static AssetScheduleEntity ToEntity(this AssetScheduleDTO dto, AssetScheduleEntity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (AssetScheduleEntity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			
			
			
			newEnt.AssetId = dto.AssetId;
			
			

			
			
			newEnt.AssetTypeCalendarId = dto.AssetTypeCalendarId;
			
			

			
			
			newEnt.Id = dto.Id;
			
			

			
			
			if (dto.LastRunUtc == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)AssetScheduleFieldIndex.LastRunUtc, default(System.DateTime));
				
			}
			
			newEnt.LastRunUtc = dto.LastRunUtc;
			
			

			
			
			if (dto.NextRunUtc == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)AssetScheduleFieldIndex.NextRunUtc, default(System.DateTime));
				
			}
			
			newEnt.NextRunUtc = dto.NextRunUtc;
			
			

			
			
			if (dto.OverrideStartTimeLocal == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				
				newEnt.SetNewFieldValue((int)AssetScheduleFieldIndex.OverrideStartTimeLocal, default(System.TimeSpan));
				
			}
			
			newEnt.OverrideStartTimeLocal = dto.OverrideStartTimeLocal;
			
			
			
			cache.Add(dto, newEnt);
			
			
			
			
			newEnt.Asset = dto.Asset.ToEntity(cache);
			
			newEnt.AssetTypeCalendar = dto.AssetTypeCalendar.ToEntity(cache);
			
			
			
		
			
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static AssetScheduleDTO ToDTO(this AssetScheduleEntity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (AssetScheduleDTO)cache[ent];
			
			var newDTO = new AssetScheduleDTO();
			newDTO.__IsNew = ent.IsNew;
			
			
			newDTO.AssetId = ent.AssetId;
			

			newDTO.AssetTypeCalendarId = ent.AssetTypeCalendarId;
			

			newDTO.Id = ent.Id;
			

			newDTO.LastRunUtc = ent.LastRunUtc;
			

			newDTO.NextRunUtc = ent.NextRunUtc;
			

			newDTO.OverrideStartTimeLocal = ent.OverrideStartTimeLocal;
			
			
			
			cache.Add(ent, newDTO);
			
			
			
			
			newDTO.Asset = ent.Asset.ToDTO(cache);
			
			newDTO.AssetTypeCalendar = ent.AssetTypeCalendar.ToDTO(cache);
			
			
			
		
			
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}