﻿//////////////////////////////////////////////////////////////
// <auto-generated>This code was generated by LLBLGen Pro 5.5.</auto-generated>
//////////////////////////////////////////////////////////////
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Data.FactoryClasses;
using ConceptCave.Data.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace ConceptCave.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'ProductHierarchy'.<br/><br/></summary>
	[Serializable]
	public partial class ProductHierarchyEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		private ProductEntity _childProduct;
		private ProductEntity _parentProduct;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static ProductHierarchyEntityStaticMetaData _staticMetaData = new ProductHierarchyEntityStaticMetaData();
		private static ProductHierarchyRelations _relationsFactory = new ProductHierarchyRelations();

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ChildProduct</summary>
			public static readonly string ChildProduct = "ChildProduct";
			/// <summary>Member name ParentProduct</summary>
			public static readonly string ParentProduct = "ParentProduct";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class ProductHierarchyEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public ProductHierarchyEntityStaticMetaData()
			{
				SetEntityCoreInfo("ProductHierarchyEntity", InheritanceHierarchyType.None, false, (int)ConceptCave.Data.EntityType.ProductHierarchyEntity, typeof(ProductHierarchyEntity), typeof(ProductHierarchyEntityFactory), false);
				AddNavigatorMetaData<ProductHierarchyEntity, ProductEntity>("ChildProduct", "ChildProductHierarchies", (a, b) => a._childProduct = b, a => a._childProduct, (a, b) => a.ChildProduct = b, ConceptCave.Data.RelationClasses.StaticProductHierarchyRelations.ProductEntityUsingChildProductIdStatic, ()=>new ProductHierarchyRelations().ProductEntityUsingChildProductId, null, new int[] { (int)ProductHierarchyFieldIndex.ChildProductId }, null, true, (int)ConceptCave.Data.EntityType.ProductEntity);
				AddNavigatorMetaData<ProductHierarchyEntity, ProductEntity>("ParentProduct", "ParentProductHierarchies", (a, b) => a._parentProduct = b, a => a._parentProduct, (a, b) => a.ParentProduct = b, ConceptCave.Data.RelationClasses.StaticProductHierarchyRelations.ProductEntityUsingParentProductIdStatic, ()=>new ProductHierarchyRelations().ProductEntityUsingParentProductId, null, new int[] { (int)ProductHierarchyFieldIndex.ParentProductId }, null, true, (int)ConceptCave.Data.EntityType.ProductEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static ProductHierarchyEntity()
		{
		}

		/// <summary> CTor</summary>
		public ProductHierarchyEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public ProductHierarchyEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this ProductHierarchyEntity</param>
		public ProductHierarchyEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="childProductId">PK value for ProductHierarchy which data should be fetched into this ProductHierarchy object</param>
		/// <param name="parentProductId">PK value for ProductHierarchy which data should be fetched into this ProductHierarchy object</param>
		public ProductHierarchyEntity(System.Int32 childProductId, System.Int32 parentProductId) : this(childProductId, parentProductId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="childProductId">PK value for ProductHierarchy which data should be fetched into this ProductHierarchy object</param>
		/// <param name="parentProductId">PK value for ProductHierarchy which data should be fetched into this ProductHierarchy object</param>
		/// <param name="validator">The custom validator object for this ProductHierarchyEntity</param>
		public ProductHierarchyEntity(System.Int32 childProductId, System.Int32 parentProductId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.ChildProductId = childProductId;
			this.ParentProductId = parentProductId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ProductHierarchyEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Product' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoChildProduct() { return CreateRelationInfoForNavigator("ChildProduct"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Product' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoParentProduct() { return CreateRelationInfoForNavigator("ParentProduct"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this ProductHierarchyEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static ProductHierarchyRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Product' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathChildProduct { get { return _staticMetaData.GetPrefetchPathElement("ChildProduct", CommonEntityBase.CreateEntityCollection<ProductEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Product' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathParentProduct { get { return _staticMetaData.GetPrefetchPathElement("ParentProduct", CommonEntityBase.CreateEntityCollection<ProductEntity>()); } }

		/// <summary>The ChildProductId property of the Entity ProductHierarchy<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblProductHierarchy"."ChildProductId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int32 ChildProductId
		{
			get { return (System.Int32)GetValue((int)ProductHierarchyFieldIndex.ChildProductId, true); }
			set	{ SetValue((int)ProductHierarchyFieldIndex.ChildProductId, value); }
		}

		/// <summary>The Name property of the Entity ProductHierarchy<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblProductHierarchy"."Name".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)ProductHierarchyFieldIndex.Name, true); }
			set	{ SetValue((int)ProductHierarchyFieldIndex.Name, value); }
		}

		/// <summary>The ParentProductId property of the Entity ProductHierarchy<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblProductHierarchy"."ParentProductId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int32 ParentProductId
		{
			get { return (System.Int32)GetValue((int)ProductHierarchyFieldIndex.ParentProductId, true); }
			set	{ SetValue((int)ProductHierarchyFieldIndex.ParentProductId, value); }
		}

		/// <summary>Gets / sets related entity of type 'ProductEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ProductEntity ChildProduct
		{
			get { return _childProduct; }
			set { SetSingleRelatedEntityNavigator(value, "ChildProduct"); }
		}

		/// <summary>Gets / sets related entity of type 'ProductEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ProductEntity ParentProduct
		{
			get { return _parentProduct; }
			set { SetSingleRelatedEntityNavigator(value, "ParentProduct"); }
		}

		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END

	}
}

namespace ConceptCave.Data
{
	public enum ProductHierarchyFieldIndex
	{
		///<summary>ChildProductId. </summary>
		ChildProductId,
		///<summary>Name. </summary>
		Name,
		///<summary>ParentProductId. </summary>
		ParentProductId,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace ConceptCave.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ProductHierarchy. </summary>
	public partial class ProductHierarchyRelations: RelationFactory
	{

		/// <summary>Returns a new IEntityRelation object, between ProductHierarchyEntity and ProductEntity over the m:1 relation they have, using the relation between the fields: ProductHierarchy.ChildProductId - Product.Id</summary>
		public virtual IEntityRelation ProductEntityUsingChildProductId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "ChildProduct", false, new[] { ProductFields.Id, ProductHierarchyFields.ChildProductId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ProductHierarchyEntity and ProductEntity over the m:1 relation they have, using the relation between the fields: ProductHierarchy.ParentProductId - Product.Id</summary>
		public virtual IEntityRelation ProductEntityUsingParentProductId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "ParentProduct", false, new[] { ProductFields.Id, ProductHierarchyFields.ParentProductId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticProductHierarchyRelations
	{
		internal static readonly IEntityRelation ProductEntityUsingChildProductIdStatic = new ProductHierarchyRelations().ProductEntityUsingChildProductId;
		internal static readonly IEntityRelation ProductEntityUsingParentProductIdStatic = new ProductHierarchyRelations().ProductEntityUsingParentProductId;

		/// <summary>CTor</summary>
		static StaticProductHierarchyRelations() { }
	}
}
