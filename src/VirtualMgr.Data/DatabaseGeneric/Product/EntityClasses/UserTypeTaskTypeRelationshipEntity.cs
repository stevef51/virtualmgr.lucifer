﻿//////////////////////////////////////////////////////////////
// <auto-generated>This code was generated by LLBLGen Pro 5.5.</auto-generated>
//////////////////////////////////////////////////////////////
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Data.FactoryClasses;
using ConceptCave.Data.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace ConceptCave.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'UserTypeTaskTypeRelationship'.<br/><br/></summary>
	[Serializable]
	public partial class UserTypeTaskTypeRelationshipEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		private ProjectJobTaskTypeEntity _projectJobTaskType;
		private UserTypeEntity _userType;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static UserTypeTaskTypeRelationshipEntityStaticMetaData _staticMetaData = new UserTypeTaskTypeRelationshipEntityStaticMetaData();
		private static UserTypeTaskTypeRelationshipRelations _relationsFactory = new UserTypeTaskTypeRelationshipRelations();

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ProjectJobTaskType</summary>
			public static readonly string ProjectJobTaskType = "ProjectJobTaskType";
			/// <summary>Member name UserType</summary>
			public static readonly string UserType = "UserType";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class UserTypeTaskTypeRelationshipEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public UserTypeTaskTypeRelationshipEntityStaticMetaData()
			{
				SetEntityCoreInfo("UserTypeTaskTypeRelationshipEntity", InheritanceHierarchyType.None, false, (int)ConceptCave.Data.EntityType.UserTypeTaskTypeRelationshipEntity, typeof(UserTypeTaskTypeRelationshipEntity), typeof(UserTypeTaskTypeRelationshipEntityFactory), false);
				AddNavigatorMetaData<UserTypeTaskTypeRelationshipEntity, ProjectJobTaskTypeEntity>("ProjectJobTaskType", "UserTypeTaskTypeRelationships", (a, b) => a._projectJobTaskType = b, a => a._projectJobTaskType, (a, b) => a.ProjectJobTaskType = b, ConceptCave.Data.RelationClasses.StaticUserTypeTaskTypeRelationshipRelations.ProjectJobTaskTypeEntityUsingTaskTypeIdStatic, ()=>new UserTypeTaskTypeRelationshipRelations().ProjectJobTaskTypeEntityUsingTaskTypeId, null, new int[] { (int)UserTypeTaskTypeRelationshipFieldIndex.TaskTypeId }, null, true, (int)ConceptCave.Data.EntityType.ProjectJobTaskTypeEntity);
				AddNavigatorMetaData<UserTypeTaskTypeRelationshipEntity, UserTypeEntity>("UserType", "UserTypeTaskTypeRelationships", (a, b) => a._userType = b, a => a._userType, (a, b) => a.UserType = b, ConceptCave.Data.RelationClasses.StaticUserTypeTaskTypeRelationshipRelations.UserTypeEntityUsingUserTypeIdStatic, ()=>new UserTypeTaskTypeRelationshipRelations().UserTypeEntityUsingUserTypeId, null, new int[] { (int)UserTypeTaskTypeRelationshipFieldIndex.UserTypeId }, null, true, (int)ConceptCave.Data.EntityType.UserTypeEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static UserTypeTaskTypeRelationshipEntity()
		{
		}

		/// <summary> CTor</summary>
		public UserTypeTaskTypeRelationshipEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public UserTypeTaskTypeRelationshipEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this UserTypeTaskTypeRelationshipEntity</param>
		public UserTypeTaskTypeRelationshipEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="relationshipType">PK value for UserTypeTaskTypeRelationship which data should be fetched into this UserTypeTaskTypeRelationship object</param>
		/// <param name="taskTypeId">PK value for UserTypeTaskTypeRelationship which data should be fetched into this UserTypeTaskTypeRelationship object</param>
		/// <param name="userTypeId">PK value for UserTypeTaskTypeRelationship which data should be fetched into this UserTypeTaskTypeRelationship object</param>
		public UserTypeTaskTypeRelationshipEntity(System.Int32 relationshipType, System.Guid taskTypeId, System.Int32 userTypeId) : this(relationshipType, taskTypeId, userTypeId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="relationshipType">PK value for UserTypeTaskTypeRelationship which data should be fetched into this UserTypeTaskTypeRelationship object</param>
		/// <param name="taskTypeId">PK value for UserTypeTaskTypeRelationship which data should be fetched into this UserTypeTaskTypeRelationship object</param>
		/// <param name="userTypeId">PK value for UserTypeTaskTypeRelationship which data should be fetched into this UserTypeTaskTypeRelationship object</param>
		/// <param name="validator">The custom validator object for this UserTypeTaskTypeRelationshipEntity</param>
		public UserTypeTaskTypeRelationshipEntity(System.Int32 relationshipType, System.Guid taskTypeId, System.Int32 userTypeId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.RelationshipType = relationshipType;
			this.TaskTypeId = taskTypeId;
			this.UserTypeId = userTypeId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected UserTypeTaskTypeRelationshipEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'ProjectJobTaskType' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoProjectJobTaskType() { return CreateRelationInfoForNavigator("ProjectJobTaskType"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'UserType' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoUserType() { return CreateRelationInfoForNavigator("UserType"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this UserTypeTaskTypeRelationshipEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static UserTypeTaskTypeRelationshipRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ProjectJobTaskType' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathProjectJobTaskType { get { return _staticMetaData.GetPrefetchPathElement("ProjectJobTaskType", CommonEntityBase.CreateEntityCollection<ProjectJobTaskTypeEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'UserType' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathUserType { get { return _staticMetaData.GetPrefetchPathElement("UserType", CommonEntityBase.CreateEntityCollection<UserTypeEntity>()); } }

		/// <summary>The RelationshipType property of the Entity UserTypeTaskTypeRelationship<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblUserTypeTaskTypeRelationship"."RelationshipType".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int32 RelationshipType
		{
			get { return (System.Int32)GetValue((int)UserTypeTaskTypeRelationshipFieldIndex.RelationshipType, true); }
			set	{ SetValue((int)UserTypeTaskTypeRelationshipFieldIndex.RelationshipType, value); }
		}

		/// <summary>The TaskTypeId property of the Entity UserTypeTaskTypeRelationship<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblUserTypeTaskTypeRelationship"."TaskTypeId".<br/>Table field type characteristics (type, precision, scale, length): UniqueIdentifier, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Guid TaskTypeId
		{
			get { return (System.Guid)GetValue((int)UserTypeTaskTypeRelationshipFieldIndex.TaskTypeId, true); }
			set	{ SetValue((int)UserTypeTaskTypeRelationshipFieldIndex.TaskTypeId, value); }
		}

		/// <summary>The UserTypeId property of the Entity UserTypeTaskTypeRelationship<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblUserTypeTaskTypeRelationship"."UserTypeId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int32 UserTypeId
		{
			get { return (System.Int32)GetValue((int)UserTypeTaskTypeRelationshipFieldIndex.UserTypeId, true); }
			set	{ SetValue((int)UserTypeTaskTypeRelationshipFieldIndex.UserTypeId, value); }
		}

		/// <summary>Gets / sets related entity of type 'ProjectJobTaskTypeEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ProjectJobTaskTypeEntity ProjectJobTaskType
		{
			get { return _projectJobTaskType; }
			set { SetSingleRelatedEntityNavigator(value, "ProjectJobTaskType"); }
		}

		/// <summary>Gets / sets related entity of type 'UserTypeEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual UserTypeEntity UserType
		{
			get { return _userType; }
			set { SetSingleRelatedEntityNavigator(value, "UserType"); }
		}

		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END

	}
}

namespace ConceptCave.Data
{
	public enum UserTypeTaskTypeRelationshipFieldIndex
	{
		///<summary>RelationshipType. </summary>
		RelationshipType,
		///<summary>TaskTypeId. </summary>
		TaskTypeId,
		///<summary>UserTypeId. </summary>
		UserTypeId,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace ConceptCave.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: UserTypeTaskTypeRelationship. </summary>
	public partial class UserTypeTaskTypeRelationshipRelations: RelationFactory
	{

		/// <summary>Returns a new IEntityRelation object, between UserTypeTaskTypeRelationshipEntity and ProjectJobTaskTypeEntity over the m:1 relation they have, using the relation between the fields: UserTypeTaskTypeRelationship.TaskTypeId - ProjectJobTaskType.Id</summary>
		public virtual IEntityRelation ProjectJobTaskTypeEntityUsingTaskTypeId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "ProjectJobTaskType", false, new[] { ProjectJobTaskTypeFields.Id, UserTypeTaskTypeRelationshipFields.TaskTypeId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between UserTypeTaskTypeRelationshipEntity and UserTypeEntity over the m:1 relation they have, using the relation between the fields: UserTypeTaskTypeRelationship.UserTypeId - UserType.Id</summary>
		public virtual IEntityRelation UserTypeEntityUsingUserTypeId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "UserType", false, new[] { UserTypeFields.Id, UserTypeTaskTypeRelationshipFields.UserTypeId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticUserTypeTaskTypeRelationshipRelations
	{
		internal static readonly IEntityRelation ProjectJobTaskTypeEntityUsingTaskTypeIdStatic = new UserTypeTaskTypeRelationshipRelations().ProjectJobTaskTypeEntityUsingTaskTypeId;
		internal static readonly IEntityRelation UserTypeEntityUsingUserTypeIdStatic = new UserTypeTaskTypeRelationshipRelations().UserTypeEntityUsingUserTypeId;

		/// <summary>CTor</summary>
		static StaticUserTypeTaskTypeRelationshipRelations() { }
	}
}
