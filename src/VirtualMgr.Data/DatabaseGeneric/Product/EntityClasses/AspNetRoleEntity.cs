﻿//////////////////////////////////////////////////////////////
// <auto-generated>This code was generated by LLBLGen Pro 5.5.</auto-generated>
//////////////////////////////////////////////////////////////
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Data.FactoryClasses;
using ConceptCave.Data.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace ConceptCave.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'AspNetRole'.<br/><br/></summary>
	[Serializable]
	public partial class AspNetRoleEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		private EntityCollection<AspNetRoleClaimEntity> _aspNetRoleClaims;
		private EntityCollection<AspNetUserRoleEntity> _aspNetUserRoles;
		private EntityCollection<MediaFolderRoleEntity> _mediaFolderRoles;
		private EntityCollection<MediaRoleEntity> _mediaRoles;
		private EntityCollection<ReportRoleEntity> _reportRoles;
		private EntityCollection<UserTypeDefaultRoleEntity> _userTypeDefaultRoles;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static AspNetRoleEntityStaticMetaData _staticMetaData = new AspNetRoleEntityStaticMetaData();
		private static AspNetRoleRelations _relationsFactory = new AspNetRoleRelations();

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name AspNetRoleClaims</summary>
			public static readonly string AspNetRoleClaims = "AspNetRoleClaims";
			/// <summary>Member name AspNetUserRoles</summary>
			public static readonly string AspNetUserRoles = "AspNetUserRoles";
			/// <summary>Member name MediaFolderRoles</summary>
			public static readonly string MediaFolderRoles = "MediaFolderRoles";
			/// <summary>Member name MediaRoles</summary>
			public static readonly string MediaRoles = "MediaRoles";
			/// <summary>Member name ReportRoles</summary>
			public static readonly string ReportRoles = "ReportRoles";
			/// <summary>Member name UserTypeDefaultRoles</summary>
			public static readonly string UserTypeDefaultRoles = "UserTypeDefaultRoles";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class AspNetRoleEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public AspNetRoleEntityStaticMetaData()
			{
				SetEntityCoreInfo("AspNetRoleEntity", InheritanceHierarchyType.None, false, (int)ConceptCave.Data.EntityType.AspNetRoleEntity, typeof(AspNetRoleEntity), typeof(AspNetRoleEntityFactory), false);
				AddNavigatorMetaData<AspNetRoleEntity, EntityCollection<AspNetRoleClaimEntity>>("AspNetRoleClaims", a => a._aspNetRoleClaims, (a, b) => a._aspNetRoleClaims = b, a => a.AspNetRoleClaims, () => new AspNetRoleRelations().AspNetRoleClaimEntityUsingRoleId, typeof(AspNetRoleClaimEntity), (int)ConceptCave.Data.EntityType.AspNetRoleClaimEntity);
				AddNavigatorMetaData<AspNetRoleEntity, EntityCollection<AspNetUserRoleEntity>>("AspNetUserRoles", a => a._aspNetUserRoles, (a, b) => a._aspNetUserRoles = b, a => a.AspNetUserRoles, () => new AspNetRoleRelations().AspNetUserRoleEntityUsingRoleId, typeof(AspNetUserRoleEntity), (int)ConceptCave.Data.EntityType.AspNetUserRoleEntity);
				AddNavigatorMetaData<AspNetRoleEntity, EntityCollection<MediaFolderRoleEntity>>("MediaFolderRoles", a => a._mediaFolderRoles, (a, b) => a._mediaFolderRoles = b, a => a.MediaFolderRoles, () => new AspNetRoleRelations().MediaFolderRoleEntityUsingRoleId, typeof(MediaFolderRoleEntity), (int)ConceptCave.Data.EntityType.MediaFolderRoleEntity);
				AddNavigatorMetaData<AspNetRoleEntity, EntityCollection<MediaRoleEntity>>("MediaRoles", a => a._mediaRoles, (a, b) => a._mediaRoles = b, a => a.MediaRoles, () => new AspNetRoleRelations().MediaRoleEntityUsingRoleId, typeof(MediaRoleEntity), (int)ConceptCave.Data.EntityType.MediaRoleEntity);
				AddNavigatorMetaData<AspNetRoleEntity, EntityCollection<ReportRoleEntity>>("ReportRoles", a => a._reportRoles, (a, b) => a._reportRoles = b, a => a.ReportRoles, () => new AspNetRoleRelations().ReportRoleEntityUsingRoleId, typeof(ReportRoleEntity), (int)ConceptCave.Data.EntityType.ReportRoleEntity);
				AddNavigatorMetaData<AspNetRoleEntity, EntityCollection<UserTypeDefaultRoleEntity>>("UserTypeDefaultRoles", a => a._userTypeDefaultRoles, (a, b) => a._userTypeDefaultRoles = b, a => a.UserTypeDefaultRoles, () => new AspNetRoleRelations().UserTypeDefaultRoleEntityUsingRoleId, typeof(UserTypeDefaultRoleEntity), (int)ConceptCave.Data.EntityType.UserTypeDefaultRoleEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static AspNetRoleEntity()
		{
		}

		/// <summary> CTor</summary>
		public AspNetRoleEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public AspNetRoleEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this AspNetRoleEntity</param>
		public AspNetRoleEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="id">PK value for AspNetRole which data should be fetched into this AspNetRole object</param>
		public AspNetRoleEntity(System.String id) : this(id, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="id">PK value for AspNetRole which data should be fetched into this AspNetRole object</param>
		/// <param name="validator">The custom validator object for this AspNetRoleEntity</param>
		public AspNetRoleEntity(System.String id, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.Id = id;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected AspNetRoleEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'AspNetRoleClaim' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoAspNetRoleClaims() { return CreateRelationInfoForNavigator("AspNetRoleClaims"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'AspNetUserRole' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoAspNetUserRoles() { return CreateRelationInfoForNavigator("AspNetUserRoles"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'MediaFolderRole' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoMediaFolderRoles() { return CreateRelationInfoForNavigator("MediaFolderRoles"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'MediaRole' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoMediaRoles() { return CreateRelationInfoForNavigator("MediaRoles"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'ReportRole' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoReportRoles() { return CreateRelationInfoForNavigator("ReportRoles"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'UserTypeDefaultRole' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoUserTypeDefaultRoles() { return CreateRelationInfoForNavigator("UserTypeDefaultRoles"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this AspNetRoleEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static AspNetRoleRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'AspNetRoleClaim' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathAspNetRoleClaims { get { return _staticMetaData.GetPrefetchPathElement("AspNetRoleClaims", CommonEntityBase.CreateEntityCollection<AspNetRoleClaimEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'AspNetUserRole' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathAspNetUserRoles { get { return _staticMetaData.GetPrefetchPathElement("AspNetUserRoles", CommonEntityBase.CreateEntityCollection<AspNetUserRoleEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'MediaFolderRole' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathMediaFolderRoles { get { return _staticMetaData.GetPrefetchPathElement("MediaFolderRoles", CommonEntityBase.CreateEntityCollection<MediaFolderRoleEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'MediaRole' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathMediaRoles { get { return _staticMetaData.GetPrefetchPathElement("MediaRoles", CommonEntityBase.CreateEntityCollection<MediaRoleEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ReportRole' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathReportRoles { get { return _staticMetaData.GetPrefetchPathElement("ReportRoles", CommonEntityBase.CreateEntityCollection<ReportRoleEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'UserTypeDefaultRole' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathUserTypeDefaultRoles { get { return _staticMetaData.GetPrefetchPathElement("UserTypeDefaultRoles", CommonEntityBase.CreateEntityCollection<UserTypeDefaultRoleEntity>()); } }

		/// <summary>The ConcurrencyStamp property of the Entity AspNetRole<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AspNetRoles"."ConcurrencyStamp".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ConcurrencyStamp
		{
			get { return (System.String)GetValue((int)AspNetRoleFieldIndex.ConcurrencyStamp, true); }
			set	{ SetValue((int)AspNetRoleFieldIndex.ConcurrencyStamp, value); }
		}

		/// <summary>The Id property of the Entity AspNetRole<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AspNetRoles"."Id".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 128.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.String Id
		{
			get { return (System.String)GetValue((int)AspNetRoleFieldIndex.Id, true); }
			set	{ SetValue((int)AspNetRoleFieldIndex.Id, value); }
		}

		/// <summary>The Name property of the Entity AspNetRole<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AspNetRoles"."Name".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 256.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)AspNetRoleFieldIndex.Name, true); }
			set	{ SetValue((int)AspNetRoleFieldIndex.Name, value); }
		}

		/// <summary>The NormalizedName property of the Entity AspNetRole<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AspNetRoles"."NormalizedName".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 256.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String NormalizedName
		{
			get { return (System.String)GetValue((int)AspNetRoleFieldIndex.NormalizedName, true); }
			set	{ SetValue((int)AspNetRoleFieldIndex.NormalizedName, value); }
		}

		/// <summary>Gets the EntityCollection with the related entities of type 'AspNetRoleClaimEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(AspNetRoleClaimEntity))]
		public virtual EntityCollection<AspNetRoleClaimEntity> AspNetRoleClaims { get { return GetOrCreateEntityCollection<AspNetRoleClaimEntity, AspNetRoleClaimEntityFactory>("AspNetRole", true, false, ref _aspNetRoleClaims); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'AspNetUserRoleEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(AspNetUserRoleEntity))]
		public virtual EntityCollection<AspNetUserRoleEntity> AspNetUserRoles { get { return GetOrCreateEntityCollection<AspNetUserRoleEntity, AspNetUserRoleEntityFactory>("AspNetRole", true, false, ref _aspNetUserRoles); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'MediaFolderRoleEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(MediaFolderRoleEntity))]
		public virtual EntityCollection<MediaFolderRoleEntity> MediaFolderRoles { get { return GetOrCreateEntityCollection<MediaFolderRoleEntity, MediaFolderRoleEntityFactory>("AspNetRole", true, false, ref _mediaFolderRoles); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'MediaRoleEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(MediaRoleEntity))]
		public virtual EntityCollection<MediaRoleEntity> MediaRoles { get { return GetOrCreateEntityCollection<MediaRoleEntity, MediaRoleEntityFactory>("AspNetRole", true, false, ref _mediaRoles); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'ReportRoleEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(ReportRoleEntity))]
		public virtual EntityCollection<ReportRoleEntity> ReportRoles { get { return GetOrCreateEntityCollection<ReportRoleEntity, ReportRoleEntityFactory>("AspNetRole", true, false, ref _reportRoles); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'UserTypeDefaultRoleEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(UserTypeDefaultRoleEntity))]
		public virtual EntityCollection<UserTypeDefaultRoleEntity> UserTypeDefaultRoles { get { return GetOrCreateEntityCollection<UserTypeDefaultRoleEntity, UserTypeDefaultRoleEntityFactory>("AspNetRole", true, false, ref _userTypeDefaultRoles); } }

		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END

	}
}

namespace ConceptCave.Data
{
	public enum AspNetRoleFieldIndex
	{
		///<summary>ConcurrencyStamp. </summary>
		ConcurrencyStamp,
		///<summary>Id. </summary>
		Id,
		///<summary>Name. </summary>
		Name,
		///<summary>NormalizedName. </summary>
		NormalizedName,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace ConceptCave.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: AspNetRole. </summary>
	public partial class AspNetRoleRelations: RelationFactory
	{
		/// <summary>Returns a new IEntityRelation object, between AspNetRoleEntity and AspNetRoleClaimEntity over the 1:n relation they have, using the relation between the fields: AspNetRole.Id - AspNetRoleClaim.RoleId</summary>
		public virtual IEntityRelation AspNetRoleClaimEntityUsingRoleId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "AspNetRoleClaims", true, new[] { AspNetRoleFields.Id, AspNetRoleClaimFields.RoleId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between AspNetRoleEntity and AspNetUserRoleEntity over the 1:n relation they have, using the relation between the fields: AspNetRole.Id - AspNetUserRole.RoleId</summary>
		public virtual IEntityRelation AspNetUserRoleEntityUsingRoleId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "AspNetUserRoles", true, new[] { AspNetRoleFields.Id, AspNetUserRoleFields.RoleId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between AspNetRoleEntity and MediaFolderRoleEntity over the 1:n relation they have, using the relation between the fields: AspNetRole.Id - MediaFolderRole.RoleId</summary>
		public virtual IEntityRelation MediaFolderRoleEntityUsingRoleId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "MediaFolderRoles", true, new[] { AspNetRoleFields.Id, MediaFolderRoleFields.RoleId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between AspNetRoleEntity and MediaRoleEntity over the 1:n relation they have, using the relation between the fields: AspNetRole.Id - MediaRole.RoleId</summary>
		public virtual IEntityRelation MediaRoleEntityUsingRoleId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "MediaRoles", true, new[] { AspNetRoleFields.Id, MediaRoleFields.RoleId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between AspNetRoleEntity and ReportRoleEntity over the 1:n relation they have, using the relation between the fields: AspNetRole.Id - ReportRole.RoleId</summary>
		public virtual IEntityRelation ReportRoleEntityUsingRoleId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "ReportRoles", true, new[] { AspNetRoleFields.Id, ReportRoleFields.RoleId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between AspNetRoleEntity and UserTypeDefaultRoleEntity over the 1:n relation they have, using the relation between the fields: AspNetRole.Id - UserTypeDefaultRole.RoleId</summary>
		public virtual IEntityRelation UserTypeDefaultRoleEntityUsingRoleId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "UserTypeDefaultRoles", true, new[] { AspNetRoleFields.Id, UserTypeDefaultRoleFields.RoleId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticAspNetRoleRelations
	{
		internal static readonly IEntityRelation AspNetRoleClaimEntityUsingRoleIdStatic = new AspNetRoleRelations().AspNetRoleClaimEntityUsingRoleId;
		internal static readonly IEntityRelation AspNetUserRoleEntityUsingRoleIdStatic = new AspNetRoleRelations().AspNetUserRoleEntityUsingRoleId;
		internal static readonly IEntityRelation MediaFolderRoleEntityUsingRoleIdStatic = new AspNetRoleRelations().MediaFolderRoleEntityUsingRoleId;
		internal static readonly IEntityRelation MediaRoleEntityUsingRoleIdStatic = new AspNetRoleRelations().MediaRoleEntityUsingRoleId;
		internal static readonly IEntityRelation ReportRoleEntityUsingRoleIdStatic = new AspNetRoleRelations().ReportRoleEntityUsingRoleId;
		internal static readonly IEntityRelation UserTypeDefaultRoleEntityUsingRoleIdStatic = new AspNetRoleRelations().UserTypeDefaultRoleEntityUsingRoleId;

		/// <summary>CTor</summary>
		static StaticAspNetRoleRelations() { }
	}
}
