﻿//////////////////////////////////////////////////////////////
// <auto-generated>This code was generated by LLBLGen Pro 5.5.</auto-generated>
//////////////////////////////////////////////////////////////
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Data.FactoryClasses;
using ConceptCave.Data.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace ConceptCave.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'GlobalSetting'.<br/><br/></summary>
	[Serializable]
	public partial class GlobalSettingEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static GlobalSettingEntityStaticMetaData _staticMetaData = new GlobalSettingEntityStaticMetaData();
		private static GlobalSettingRelations _relationsFactory = new GlobalSettingRelations();

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class GlobalSettingEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public GlobalSettingEntityStaticMetaData()
			{
				SetEntityCoreInfo("GlobalSettingEntity", InheritanceHierarchyType.None, false, (int)ConceptCave.Data.EntityType.GlobalSettingEntity, typeof(GlobalSettingEntity), typeof(GlobalSettingEntityFactory), false);
			}
		}

		/// <summary>Static ctor</summary>
		static GlobalSettingEntity()
		{
		}

		/// <summary> CTor</summary>
		public GlobalSettingEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public GlobalSettingEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this GlobalSettingEntity</param>
		public GlobalSettingEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="id">PK value for GlobalSetting which data should be fetched into this GlobalSetting object</param>
		public GlobalSettingEntity(System.Int32 id) : this(id, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="id">PK value for GlobalSetting which data should be fetched into this GlobalSetting object</param>
		/// <param name="validator">The custom validator object for this GlobalSettingEntity</param>
		public GlobalSettingEntity(System.Int32 id, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.Id = id;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected GlobalSettingEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		/// <summary>Method which will construct a filter (predicate expression) for the unique constraint defined on the fields: Name .</summary>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public IPredicateExpression ConstructFilterForUCName()
		{
			var filter = new PredicateExpression();
			filter.Add(ConceptCave.Data.HelperClasses.GlobalSettingFields.Name == this.Fields.GetCurrentValue((int)GlobalSettingFieldIndex.Name));
 			return filter;
		}
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this GlobalSettingEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static GlobalSettingRelations Relations { get { return _relationsFactory; } }

		/// <summary>The Hidden property of the Entity GlobalSetting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblGlobalSetting"."Hidden".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Hidden
		{
			get { return (System.Boolean)GetValue((int)GlobalSettingFieldIndex.Hidden, true); }
			set	{ SetValue((int)GlobalSettingFieldIndex.Hidden, value); }
		}

		/// <summary>The Id property of the Entity GlobalSetting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblGlobalSetting"."Id".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 Id
		{
			get { return (System.Int32)GetValue((int)GlobalSettingFieldIndex.Id, true); }
			set { SetValue((int)GlobalSettingFieldIndex.Id, value); }		}

		/// <summary>The Name property of the Entity GlobalSetting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblGlobalSetting"."Name".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 256.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)GlobalSettingFieldIndex.Name, true); }
			set	{ SetValue((int)GlobalSettingFieldIndex.Name, value); }
		}

		/// <summary>The Value property of the Entity GlobalSetting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblGlobalSetting"."Value".<br/>Table field type characteristics (type, precision, scale, length): NText, 0, 0, 1073741823.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Value
		{
			get { return (System.String)GetValue((int)GlobalSettingFieldIndex.Value, true); }
			set	{ SetValue((int)GlobalSettingFieldIndex.Value, value); }
		}

		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END

	}
}

namespace ConceptCave.Data
{
	public enum GlobalSettingFieldIndex
	{
		///<summary>Hidden. </summary>
		Hidden,
		///<summary>Id. </summary>
		Id,
		///<summary>Name. </summary>
		Name,
		///<summary>Value. </summary>
		Value,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace ConceptCave.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: GlobalSetting. </summary>
	public partial class GlobalSettingRelations: RelationFactory
	{

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticGlobalSettingRelations
	{

		/// <summary>CTor</summary>
		static StaticGlobalSettingRelations() { }
	}
}
