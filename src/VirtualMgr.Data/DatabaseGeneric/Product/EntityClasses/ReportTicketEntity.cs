﻿//////////////////////////////////////////////////////////////
// <auto-generated>This code was generated by LLBLGen Pro 5.5.</auto-generated>
//////////////////////////////////////////////////////////////
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Data.FactoryClasses;
using ConceptCave.Data.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace ConceptCave.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'ReportTicket'.<br/><br/></summary>
	[Serializable]
	public partial class ReportTicketEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		private ReportEntity _report;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static ReportTicketEntityStaticMetaData _staticMetaData = new ReportTicketEntityStaticMetaData();
		private static ReportTicketRelations _relationsFactory = new ReportTicketRelations();

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Report</summary>
			public static readonly string Report = "Report";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class ReportTicketEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public ReportTicketEntityStaticMetaData()
			{
				SetEntityCoreInfo("ReportTicketEntity", InheritanceHierarchyType.None, false, (int)ConceptCave.Data.EntityType.ReportTicketEntity, typeof(ReportTicketEntity), typeof(ReportTicketEntityFactory), false);
				AddNavigatorMetaData<ReportTicketEntity, ReportEntity>("Report", "ReportTickets", (a, b) => a._report = b, a => a._report, (a, b) => a.Report = b, ConceptCave.Data.RelationClasses.StaticReportTicketRelations.ReportEntityUsingReportIdStatic, ()=>new ReportTicketRelations().ReportEntityUsingReportId, null, new int[] { (int)ReportTicketFieldIndex.ReportId }, null, true, (int)ConceptCave.Data.EntityType.ReportEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static ReportTicketEntity()
		{
		}

		/// <summary> CTor</summary>
		public ReportTicketEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public ReportTicketEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this ReportTicketEntity</param>
		public ReportTicketEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="id">PK value for ReportTicket which data should be fetched into this ReportTicket object</param>
		public ReportTicketEntity(System.Guid id) : this(id, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="id">PK value for ReportTicket which data should be fetched into this ReportTicket object</param>
		/// <param name="validator">The custom validator object for this ReportTicketEntity</param>
		public ReportTicketEntity(System.Guid id, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.Id = id;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ReportTicketEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Report' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoReport() { return CreateRelationInfoForNavigator("Report"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this ReportTicketEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static ReportTicketRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Report' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathReport { get { return _staticMetaData.GetPrefetchPathElement("Report", CommonEntityBase.CreateEntityCollection<ReportEntity>()); } }

		/// <summary>The DateCreatedUtc property of the Entity ReportTicket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblReportTicket"."DateCreatedUtc".<br/>Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime DateCreatedUtc
		{
			get { return (System.DateTime)GetValue((int)ReportTicketFieldIndex.DateCreatedUtc, true); }
			set	{ SetValue((int)ReportTicketFieldIndex.DateCreatedUtc, value); }
		}

		/// <summary>The DefaultFormat property of the Entity ReportTicket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblReportTicket"."DefaultFormat".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String DefaultFormat
		{
			get { return (System.String)GetValue((int)ReportTicketFieldIndex.DefaultFormat, true); }
			set	{ SetValue((int)ReportTicketFieldIndex.DefaultFormat, value); }
		}

		/// <summary>The Id property of the Entity ReportTicket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblReportTicket"."Id".<br/>Table field type characteristics (type, precision, scale, length): UniqueIdentifier, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Guid Id
		{
			get { return (System.Guid)GetValue((int)ReportTicketFieldIndex.Id, true); }
			set	{ SetValue((int)ReportTicketFieldIndex.Id, value); }
		}

		/// <summary>The JsonParameters property of the Entity ReportTicket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblReportTicket"."JsonParameters".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String JsonParameters
		{
			get { return (System.String)GetValue((int)ReportTicketFieldIndex.JsonParameters, true); }
			set	{ SetValue((int)ReportTicketFieldIndex.JsonParameters, value); }
		}

		/// <summary>The LastRunOnUtc property of the Entity ReportTicket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblReportTicket"."LastRunOnUtc".<br/>Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> LastRunOnUtc
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ReportTicketFieldIndex.LastRunOnUtc, false); }
			set	{ SetValue((int)ReportTicketFieldIndex.LastRunOnUtc, value); }
		}

		/// <summary>The ReportId property of the Entity ReportTicket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblReportTicket"."ReportId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ReportId
		{
			get { return (System.Int32)GetValue((int)ReportTicketFieldIndex.ReportId, true); }
			set	{ SetValue((int)ReportTicketFieldIndex.ReportId, value); }
		}

		/// <summary>The RunAsUserId property of the Entity ReportTicket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblReportTicket"."RunAsUserId".<br/>Table field type characteristics (type, precision, scale, length): UniqueIdentifier, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Guid RunAsUserId
		{
			get { return (System.Guid)GetValue((int)ReportTicketFieldIndex.RunAsUserId, true); }
			set	{ SetValue((int)ReportTicketFieldIndex.RunAsUserId, value); }
		}

		/// <summary>Gets / sets related entity of type 'ReportEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ReportEntity Report
		{
			get { return _report; }
			set { SetSingleRelatedEntityNavigator(value, "Report"); }
		}

		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END

	}
}

namespace ConceptCave.Data
{
	public enum ReportTicketFieldIndex
	{
		///<summary>DateCreatedUtc. </summary>
		DateCreatedUtc,
		///<summary>DefaultFormat. </summary>
		DefaultFormat,
		///<summary>Id. </summary>
		Id,
		///<summary>JsonParameters. </summary>
		JsonParameters,
		///<summary>LastRunOnUtc. </summary>
		LastRunOnUtc,
		///<summary>ReportId. </summary>
		ReportId,
		///<summary>RunAsUserId. </summary>
		RunAsUserId,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace ConceptCave.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ReportTicket. </summary>
	public partial class ReportTicketRelations: RelationFactory
	{

		/// <summary>Returns a new IEntityRelation object, between ReportTicketEntity and ReportEntity over the m:1 relation they have, using the relation between the fields: ReportTicket.ReportId - Report.Id</summary>
		public virtual IEntityRelation ReportEntityUsingReportId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Report", false, new[] { ReportFields.Id, ReportTicketFields.ReportId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticReportTicketRelations
	{
		internal static readonly IEntityRelation ReportEntityUsingReportIdStatic = new ReportTicketRelations().ReportEntityUsingReportId;

		/// <summary>CTor</summary>
		static StaticReportTicketRelations() { }
	}
}
