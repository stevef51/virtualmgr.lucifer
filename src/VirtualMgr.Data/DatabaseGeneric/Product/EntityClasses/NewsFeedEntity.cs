﻿//////////////////////////////////////////////////////////////
// <auto-generated>This code was generated by LLBLGen Pro 5.5.</auto-generated>
//////////////////////////////////////////////////////////////
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Data.FactoryClasses;
using ConceptCave.Data.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace ConceptCave.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'NewsFeed'.<br/><br/></summary>
	[Serializable]
	public partial class NewsFeedEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		private UserDataEntity _userData;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static NewsFeedEntityStaticMetaData _staticMetaData = new NewsFeedEntityStaticMetaData();
		private static NewsFeedRelations _relationsFactory = new NewsFeedRelations();

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name UserData</summary>
			public static readonly string UserData = "UserData";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class NewsFeedEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public NewsFeedEntityStaticMetaData()
			{
				SetEntityCoreInfo("NewsFeedEntity", InheritanceHierarchyType.None, false, (int)ConceptCave.Data.EntityType.NewsFeedEntity, typeof(NewsFeedEntity), typeof(NewsFeedEntityFactory), false);
				AddNavigatorMetaData<NewsFeedEntity, UserDataEntity>("UserData", "NewsFeeds", (a, b) => a._userData = b, a => a._userData, (a, b) => a.UserData = b, ConceptCave.Data.RelationClasses.StaticNewsFeedRelations.UserDataEntityUsingCreatedByStatic, ()=>new NewsFeedRelations().UserDataEntityUsingCreatedBy, null, new int[] { (int)NewsFeedFieldIndex.CreatedBy }, null, true, (int)ConceptCave.Data.EntityType.UserDataEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static NewsFeedEntity()
		{
		}

		/// <summary> CTor</summary>
		public NewsFeedEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public NewsFeedEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this NewsFeedEntity</param>
		public NewsFeedEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="id">PK value for NewsFeed which data should be fetched into this NewsFeed object</param>
		public NewsFeedEntity(System.Guid id) : this(id, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="id">PK value for NewsFeed which data should be fetched into this NewsFeed object</param>
		/// <param name="validator">The custom validator object for this NewsFeedEntity</param>
		public NewsFeedEntity(System.Guid id, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.Id = id;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected NewsFeedEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'UserData' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoUserData() { return CreateRelationInfoForNavigator("UserData"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this NewsFeedEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static NewsFeedRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'UserData' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathUserData { get { return _staticMetaData.GetPrefetchPathElement("UserData", CommonEntityBase.CreateEntityCollection<UserDataEntity>()); } }

		/// <summary>The Channel property of the Entity NewsFeed<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblNewsFeed"."Channel".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Channel
		{
			get { return (System.String)GetValue((int)NewsFeedFieldIndex.Channel, true); }
			set	{ SetValue((int)NewsFeedFieldIndex.Channel, value); }
		}

		/// <summary>The CreatedBy property of the Entity NewsFeed<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblNewsFeed"."CreatedBy".<br/>Table field type characteristics (type, precision, scale, length): UniqueIdentifier, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Guid CreatedBy
		{
			get { return (System.Guid)GetValue((int)NewsFeedFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)NewsFeedFieldIndex.CreatedBy, value); }
		}

		/// <summary>The DateCreated property of the Entity NewsFeed<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblNewsFeed"."DateCreated".<br/>Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime DateCreated
		{
			get { return (System.DateTime)GetValue((int)NewsFeedFieldIndex.DateCreated, true); }
			set	{ SetValue((int)NewsFeedFieldIndex.DateCreated, value); }
		}

		/// <summary>The Headline property of the Entity NewsFeed<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblNewsFeed"."Headline".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 500.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Headline
		{
			get { return (System.String)GetValue((int)NewsFeedFieldIndex.Headline, true); }
			set	{ SetValue((int)NewsFeedFieldIndex.Headline, value); }
		}

		/// <summary>The Id property of the Entity NewsFeed<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblNewsFeed"."Id".<br/>Table field type characteristics (type, precision, scale, length): UniqueIdentifier, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Guid Id
		{
			get { return (System.Guid)GetValue((int)NewsFeedFieldIndex.Id, true); }
			set	{ SetValue((int)NewsFeedFieldIndex.Id, value); }
		}

		/// <summary>The Program property of the Entity NewsFeed<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblNewsFeed"."Program".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Program
		{
			get { return (System.String)GetValue((int)NewsFeedFieldIndex.Program, true); }
			set	{ SetValue((int)NewsFeedFieldIndex.Program, value); }
		}

		/// <summary>The Title property of the Entity NewsFeed<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblNewsFeed"."Title".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Title
		{
			get { return (System.String)GetValue((int)NewsFeedFieldIndex.Title, true); }
			set	{ SetValue((int)NewsFeedFieldIndex.Title, value); }
		}

		/// <summary>The ValidTo property of the Entity NewsFeed<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblNewsFeed"."ValidTo".<br/>Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime ValidTo
		{
			get { return (System.DateTime)GetValue((int)NewsFeedFieldIndex.ValidTo, true); }
			set	{ SetValue((int)NewsFeedFieldIndex.ValidTo, value); }
		}

		/// <summary>Gets / sets related entity of type 'UserDataEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual UserDataEntity UserData
		{
			get { return _userData; }
			set { SetSingleRelatedEntityNavigator(value, "UserData"); }
		}

		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END

	}
}

namespace ConceptCave.Data
{
	public enum NewsFeedFieldIndex
	{
		///<summary>Channel. </summary>
		Channel,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>DateCreated. </summary>
		DateCreated,
		///<summary>Headline. </summary>
		Headline,
		///<summary>Id. </summary>
		Id,
		///<summary>Program. </summary>
		Program,
		///<summary>Title. </summary>
		Title,
		///<summary>ValidTo. </summary>
		ValidTo,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace ConceptCave.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: NewsFeed. </summary>
	public partial class NewsFeedRelations: RelationFactory
	{

		/// <summary>Returns a new IEntityRelation object, between NewsFeedEntity and UserDataEntity over the m:1 relation they have, using the relation between the fields: NewsFeed.CreatedBy - UserData.UserId</summary>
		public virtual IEntityRelation UserDataEntityUsingCreatedBy
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "UserData", false, new[] { UserDataFields.UserId, NewsFeedFields.CreatedBy }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticNewsFeedRelations
	{
		internal static readonly IEntityRelation UserDataEntityUsingCreatedByStatic = new NewsFeedRelations().UserDataEntityUsingCreatedBy;

		/// <summary>CTor</summary>
		static StaticNewsFeedRelations() { }
	}
}
