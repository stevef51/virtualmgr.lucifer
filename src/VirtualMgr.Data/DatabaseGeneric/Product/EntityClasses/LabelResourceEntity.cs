﻿//////////////////////////////////////////////////////////////
// <auto-generated>This code was generated by LLBLGen Pro 5.5.</auto-generated>
//////////////////////////////////////////////////////////////
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Data.FactoryClasses;
using ConceptCave.Data.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace ConceptCave.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'LabelResource'.<br/><br/></summary>
	[Serializable]
	public partial class LabelResourceEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		private LabelEntity _label;
		private ResourceEntity _resource;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static LabelResourceEntityStaticMetaData _staticMetaData = new LabelResourceEntityStaticMetaData();
		private static LabelResourceRelations _relationsFactory = new LabelResourceRelations();

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Label</summary>
			public static readonly string Label = "Label";
			/// <summary>Member name Resource</summary>
			public static readonly string Resource = "Resource";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class LabelResourceEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public LabelResourceEntityStaticMetaData()
			{
				SetEntityCoreInfo("LabelResourceEntity", InheritanceHierarchyType.None, false, (int)ConceptCave.Data.EntityType.LabelResourceEntity, typeof(LabelResourceEntity), typeof(LabelResourceEntityFactory), false);
				AddNavigatorMetaData<LabelResourceEntity, LabelEntity>("Label", "LabelResources", (a, b) => a._label = b, a => a._label, (a, b) => a.Label = b, ConceptCave.Data.RelationClasses.StaticLabelResourceRelations.LabelEntityUsingLabelIdStatic, ()=>new LabelResourceRelations().LabelEntityUsingLabelId, null, new int[] { (int)LabelResourceFieldIndex.LabelId }, null, true, (int)ConceptCave.Data.EntityType.LabelEntity);
				AddNavigatorMetaData<LabelResourceEntity, ResourceEntity>("Resource", "LabelResources", (a, b) => a._resource = b, a => a._resource, (a, b) => a.Resource = b, ConceptCave.Data.RelationClasses.StaticLabelResourceRelations.ResourceEntityUsingResourceIdStatic, ()=>new LabelResourceRelations().ResourceEntityUsingResourceId, null, new int[] { (int)LabelResourceFieldIndex.ResourceId }, null, true, (int)ConceptCave.Data.EntityType.ResourceEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static LabelResourceEntity()
		{
		}

		/// <summary> CTor</summary>
		public LabelResourceEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public LabelResourceEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this LabelResourceEntity</param>
		public LabelResourceEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="labelId">PK value for LabelResource which data should be fetched into this LabelResource object</param>
		/// <param name="resourceId">PK value for LabelResource which data should be fetched into this LabelResource object</param>
		public LabelResourceEntity(System.Guid labelId, System.Int32 resourceId) : this(labelId, resourceId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="labelId">PK value for LabelResource which data should be fetched into this LabelResource object</param>
		/// <param name="resourceId">PK value for LabelResource which data should be fetched into this LabelResource object</param>
		/// <param name="validator">The custom validator object for this LabelResourceEntity</param>
		public LabelResourceEntity(System.Guid labelId, System.Int32 resourceId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.LabelId = labelId;
			this.ResourceId = resourceId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected LabelResourceEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Label' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoLabel() { return CreateRelationInfoForNavigator("Label"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Resource' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoResource() { return CreateRelationInfoForNavigator("Resource"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this LabelResourceEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static LabelResourceRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Label' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathLabel { get { return _staticMetaData.GetPrefetchPathElement("Label", CommonEntityBase.CreateEntityCollection<LabelEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Resource' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathResource { get { return _staticMetaData.GetPrefetchPathElement("Resource", CommonEntityBase.CreateEntityCollection<ResourceEntity>()); } }

		/// <summary>The LabelId property of the Entity LabelResource<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblLabelResource"."LabelId".<br/>Table field type characteristics (type, precision, scale, length): UniqueIdentifier, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Guid LabelId
		{
			get { return (System.Guid)GetValue((int)LabelResourceFieldIndex.LabelId, true); }
			set	{ SetValue((int)LabelResourceFieldIndex.LabelId, value); }
		}

		/// <summary>The ResourceId property of the Entity LabelResource<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblLabelResource"."ResourceId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int32 ResourceId
		{
			get { return (System.Int32)GetValue((int)LabelResourceFieldIndex.ResourceId, true); }
			set	{ SetValue((int)LabelResourceFieldIndex.ResourceId, value); }
		}

		/// <summary>Gets / sets related entity of type 'LabelEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual LabelEntity Label
		{
			get { return _label; }
			set { SetSingleRelatedEntityNavigator(value, "Label"); }
		}

		/// <summary>Gets / sets related entity of type 'ResourceEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ResourceEntity Resource
		{
			get { return _resource; }
			set { SetSingleRelatedEntityNavigator(value, "Resource"); }
		}

		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END

	}
}

namespace ConceptCave.Data
{
	public enum LabelResourceFieldIndex
	{
		///<summary>LabelId. </summary>
		LabelId,
		///<summary>ResourceId. </summary>
		ResourceId,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace ConceptCave.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: LabelResource. </summary>
	public partial class LabelResourceRelations: RelationFactory
	{

		/// <summary>Returns a new IEntityRelation object, between LabelResourceEntity and LabelEntity over the m:1 relation they have, using the relation between the fields: LabelResource.LabelId - Label.Id</summary>
		public virtual IEntityRelation LabelEntityUsingLabelId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Label", false, new[] { LabelFields.Id, LabelResourceFields.LabelId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between LabelResourceEntity and ResourceEntity over the m:1 relation they have, using the relation between the fields: LabelResource.ResourceId - Resource.Id</summary>
		public virtual IEntityRelation ResourceEntityUsingResourceId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Resource", false, new[] { ResourceFields.Id, LabelResourceFields.ResourceId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticLabelResourceRelations
	{
		internal static readonly IEntityRelation LabelEntityUsingLabelIdStatic = new LabelResourceRelations().LabelEntityUsingLabelId;
		internal static readonly IEntityRelation ResourceEntityUsingResourceIdStatic = new LabelResourceRelations().ResourceEntityUsingResourceId;

		/// <summary>CTor</summary>
		static StaticLabelResourceRelations() { }
	}
}
