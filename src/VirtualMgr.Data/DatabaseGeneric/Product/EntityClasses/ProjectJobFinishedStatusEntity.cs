﻿//////////////////////////////////////////////////////////////
// <auto-generated>This code was generated by LLBLGen Pro 5.5.</auto-generated>
//////////////////////////////////////////////////////////////
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Data.FactoryClasses;
using ConceptCave.Data.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace ConceptCave.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'ProjectJobFinishedStatus'.<br/><br/></summary>
	[Serializable]
	public partial class ProjectJobFinishedStatusEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		private EntityCollection<ProjectJobTaskFinishedStatusEntity> _projectJobTaskFinishedStatuses;
		private EntityCollection<ProjectJobTaskTypeFinishedStatusEntity> _projectJobTaskTypeFinishedStatuses;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static ProjectJobFinishedStatusEntityStaticMetaData _staticMetaData = new ProjectJobFinishedStatusEntityStaticMetaData();
		private static ProjectJobFinishedStatusRelations _relationsFactory = new ProjectJobFinishedStatusRelations();

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ProjectJobTaskFinishedStatuses</summary>
			public static readonly string ProjectJobTaskFinishedStatuses = "ProjectJobTaskFinishedStatuses";
			/// <summary>Member name ProjectJobTaskTypeFinishedStatuses</summary>
			public static readonly string ProjectJobTaskTypeFinishedStatuses = "ProjectJobTaskTypeFinishedStatuses";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class ProjectJobFinishedStatusEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public ProjectJobFinishedStatusEntityStaticMetaData()
			{
				SetEntityCoreInfo("ProjectJobFinishedStatusEntity", InheritanceHierarchyType.None, false, (int)ConceptCave.Data.EntityType.ProjectJobFinishedStatusEntity, typeof(ProjectJobFinishedStatusEntity), typeof(ProjectJobFinishedStatusEntityFactory), false);
				AddNavigatorMetaData<ProjectJobFinishedStatusEntity, EntityCollection<ProjectJobTaskFinishedStatusEntity>>("ProjectJobTaskFinishedStatuses", a => a._projectJobTaskFinishedStatuses, (a, b) => a._projectJobTaskFinishedStatuses = b, a => a.ProjectJobTaskFinishedStatuses, () => new ProjectJobFinishedStatusRelations().ProjectJobTaskFinishedStatusEntityUsingProjectJobFinishedStatusId, typeof(ProjectJobTaskFinishedStatusEntity), (int)ConceptCave.Data.EntityType.ProjectJobTaskFinishedStatusEntity);
				AddNavigatorMetaData<ProjectJobFinishedStatusEntity, EntityCollection<ProjectJobTaskTypeFinishedStatusEntity>>("ProjectJobTaskTypeFinishedStatuses", a => a._projectJobTaskTypeFinishedStatuses, (a, b) => a._projectJobTaskTypeFinishedStatuses = b, a => a.ProjectJobTaskTypeFinishedStatuses, () => new ProjectJobFinishedStatusRelations().ProjectJobTaskTypeFinishedStatusEntityUsingProjectJobFinishedStatusId, typeof(ProjectJobTaskTypeFinishedStatusEntity), (int)ConceptCave.Data.EntityType.ProjectJobTaskTypeFinishedStatusEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static ProjectJobFinishedStatusEntity()
		{
		}

		/// <summary> CTor</summary>
		public ProjectJobFinishedStatusEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public ProjectJobFinishedStatusEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this ProjectJobFinishedStatusEntity</param>
		public ProjectJobFinishedStatusEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="id">PK value for ProjectJobFinishedStatus which data should be fetched into this ProjectJobFinishedStatus object</param>
		public ProjectJobFinishedStatusEntity(System.Guid id) : this(id, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="id">PK value for ProjectJobFinishedStatus which data should be fetched into this ProjectJobFinishedStatus object</param>
		/// <param name="validator">The custom validator object for this ProjectJobFinishedStatusEntity</param>
		public ProjectJobFinishedStatusEntity(System.Guid id, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.Id = id;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ProjectJobFinishedStatusEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'ProjectJobTaskFinishedStatus' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoProjectJobTaskFinishedStatuses() { return CreateRelationInfoForNavigator("ProjectJobTaskFinishedStatuses"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'ProjectJobTaskTypeFinishedStatus' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoProjectJobTaskTypeFinishedStatuses() { return CreateRelationInfoForNavigator("ProjectJobTaskTypeFinishedStatuses"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this ProjectJobFinishedStatusEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static ProjectJobFinishedStatusRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ProjectJobTaskFinishedStatus' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathProjectJobTaskFinishedStatuses { get { return _staticMetaData.GetPrefetchPathElement("ProjectJobTaskFinishedStatuses", CommonEntityBase.CreateEntityCollection<ProjectJobTaskFinishedStatusEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ProjectJobTaskTypeFinishedStatus' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathProjectJobTaskTypeFinishedStatuses { get { return _staticMetaData.GetPrefetchPathElement("ProjectJobTaskTypeFinishedStatuses", CommonEntityBase.CreateEntityCollection<ProjectJobTaskTypeFinishedStatusEntity>()); } }

		/// <summary>The Id property of the Entity ProjectJobFinishedStatus<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblProjectJobFinishedStatus"."Id".<br/>Table field type characteristics (type, precision, scale, length): UniqueIdentifier, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Guid Id
		{
			get { return (System.Guid)GetValue((int)ProjectJobFinishedStatusFieldIndex.Id, true); }
			set	{ SetValue((int)ProjectJobFinishedStatusFieldIndex.Id, value); }
		}

		/// <summary>The RequiresExtraNotes property of the Entity ProjectJobFinishedStatus<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblProjectJobFinishedStatus"."RequiresExtraNotes".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean RequiresExtraNotes
		{
			get { return (System.Boolean)GetValue((int)ProjectJobFinishedStatusFieldIndex.RequiresExtraNotes, true); }
			set	{ SetValue((int)ProjectJobFinishedStatusFieldIndex.RequiresExtraNotes, value); }
		}

		/// <summary>The Stage property of the Entity ProjectJobFinishedStatus<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblProjectJobFinishedStatus"."Stage".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Stage
		{
			get { return (System.Int32)GetValue((int)ProjectJobFinishedStatusFieldIndex.Stage, true); }
			set	{ SetValue((int)ProjectJobFinishedStatusFieldIndex.Stage, value); }
		}

		/// <summary>The Text property of the Entity ProjectJobFinishedStatus<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblProjectJobFinishedStatus"."Text".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Text
		{
			get { return (System.String)GetValue((int)ProjectJobFinishedStatusFieldIndex.Text, true); }
			set	{ SetValue((int)ProjectJobFinishedStatusFieldIndex.Text, value); }
		}

		/// <summary>Gets the EntityCollection with the related entities of type 'ProjectJobTaskFinishedStatusEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(ProjectJobTaskFinishedStatusEntity))]
		public virtual EntityCollection<ProjectJobTaskFinishedStatusEntity> ProjectJobTaskFinishedStatuses { get { return GetOrCreateEntityCollection<ProjectJobTaskFinishedStatusEntity, ProjectJobTaskFinishedStatusEntityFactory>("ProjectJobFinishedStatu", true, false, ref _projectJobTaskFinishedStatuses); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'ProjectJobTaskTypeFinishedStatusEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(ProjectJobTaskTypeFinishedStatusEntity))]
		public virtual EntityCollection<ProjectJobTaskTypeFinishedStatusEntity> ProjectJobTaskTypeFinishedStatuses { get { return GetOrCreateEntityCollection<ProjectJobTaskTypeFinishedStatusEntity, ProjectJobTaskTypeFinishedStatusEntityFactory>("ProjectJobFinishedStatu", true, false, ref _projectJobTaskTypeFinishedStatuses); } }

		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END

	}
}

namespace ConceptCave.Data
{
	public enum ProjectJobFinishedStatusFieldIndex
	{
		///<summary>Id. </summary>
		Id,
		///<summary>RequiresExtraNotes. </summary>
		RequiresExtraNotes,
		///<summary>Stage. </summary>
		Stage,
		///<summary>Text. </summary>
		Text,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace ConceptCave.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ProjectJobFinishedStatus. </summary>
	public partial class ProjectJobFinishedStatusRelations: RelationFactory
	{
		/// <summary>Returns a new IEntityRelation object, between ProjectJobFinishedStatusEntity and ProjectJobTaskFinishedStatusEntity over the 1:n relation they have, using the relation between the fields: ProjectJobFinishedStatus.Id - ProjectJobTaskFinishedStatus.ProjectJobFinishedStatusId</summary>
		public virtual IEntityRelation ProjectJobTaskFinishedStatusEntityUsingProjectJobFinishedStatusId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "ProjectJobTaskFinishedStatuses", true, new[] { ProjectJobFinishedStatusFields.Id, ProjectJobTaskFinishedStatusFields.ProjectJobFinishedStatusId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ProjectJobFinishedStatusEntity and ProjectJobTaskTypeFinishedStatusEntity over the 1:n relation they have, using the relation between the fields: ProjectJobFinishedStatus.Id - ProjectJobTaskTypeFinishedStatus.ProjectJobFinishedStatusId</summary>
		public virtual IEntityRelation ProjectJobTaskTypeFinishedStatusEntityUsingProjectJobFinishedStatusId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "ProjectJobTaskTypeFinishedStatuses", true, new[] { ProjectJobFinishedStatusFields.Id, ProjectJobTaskTypeFinishedStatusFields.ProjectJobFinishedStatusId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticProjectJobFinishedStatusRelations
	{
		internal static readonly IEntityRelation ProjectJobTaskFinishedStatusEntityUsingProjectJobFinishedStatusIdStatic = new ProjectJobFinishedStatusRelations().ProjectJobTaskFinishedStatusEntityUsingProjectJobFinishedStatusId;
		internal static readonly IEntityRelation ProjectJobTaskTypeFinishedStatusEntityUsingProjectJobFinishedStatusIdStatic = new ProjectJobFinishedStatusRelations().ProjectJobTaskTypeFinishedStatusEntityUsingProjectJobFinishedStatusId;

		/// <summary>CTor</summary>
		static StaticProjectJobFinishedStatusRelations() { }
	}
}
