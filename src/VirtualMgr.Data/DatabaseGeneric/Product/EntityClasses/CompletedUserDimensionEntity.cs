﻿//////////////////////////////////////////////////////////////
// <auto-generated>This code was generated by LLBLGen Pro 5.5.</auto-generated>
//////////////////////////////////////////////////////////////
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Data.FactoryClasses;
using ConceptCave.Data.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace ConceptCave.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'CompletedUserDimension'.<br/><br/></summary>
	[Serializable]
	public partial class CompletedUserDimensionEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		private EntityCollection<CompletedWorkingDocumentFactEntity> _completedWorkingDocumentFactsAsReviewee;
		private EntityCollection<CompletedWorkingDocumentFactEntity> _completedWorkingDocumentFactsAsReviewer;
		private EntityCollection<CompletedWorkingDocumentPresentedFactEntity> _completedWorkingDocumentPresentedFactsReviewee;
		private EntityCollection<CompletedWorkingDocumentPresentedFactEntity> _completedWorkingDocumentPresentedFactsReviewer;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static CompletedUserDimensionEntityStaticMetaData _staticMetaData = new CompletedUserDimensionEntityStaticMetaData();
		private static CompletedUserDimensionRelations _relationsFactory = new CompletedUserDimensionRelations();

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CompletedWorkingDocumentFactsAsReviewee</summary>
			public static readonly string CompletedWorkingDocumentFactsAsReviewee = "CompletedWorkingDocumentFactsAsReviewee";
			/// <summary>Member name CompletedWorkingDocumentFactsAsReviewer</summary>
			public static readonly string CompletedWorkingDocumentFactsAsReviewer = "CompletedWorkingDocumentFactsAsReviewer";
			/// <summary>Member name CompletedWorkingDocumentPresentedFactsReviewee</summary>
			public static readonly string CompletedWorkingDocumentPresentedFactsReviewee = "CompletedWorkingDocumentPresentedFactsReviewee";
			/// <summary>Member name CompletedWorkingDocumentPresentedFactsReviewer</summary>
			public static readonly string CompletedWorkingDocumentPresentedFactsReviewer = "CompletedWorkingDocumentPresentedFactsReviewer";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class CompletedUserDimensionEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public CompletedUserDimensionEntityStaticMetaData()
			{
				SetEntityCoreInfo("CompletedUserDimensionEntity", InheritanceHierarchyType.None, false, (int)ConceptCave.Data.EntityType.CompletedUserDimensionEntity, typeof(CompletedUserDimensionEntity), typeof(CompletedUserDimensionEntityFactory), false);
				AddNavigatorMetaData<CompletedUserDimensionEntity, EntityCollection<CompletedWorkingDocumentFactEntity>>("CompletedWorkingDocumentFactsAsReviewee", a => a._completedWorkingDocumentFactsAsReviewee, (a, b) => a._completedWorkingDocumentFactsAsReviewee = b, a => a.CompletedWorkingDocumentFactsAsReviewee, () => new CompletedUserDimensionRelations().CompletedWorkingDocumentFactEntityUsingReportingUserRevieweeId, typeof(CompletedWorkingDocumentFactEntity), (int)ConceptCave.Data.EntityType.CompletedWorkingDocumentFactEntity);
				AddNavigatorMetaData<CompletedUserDimensionEntity, EntityCollection<CompletedWorkingDocumentFactEntity>>("CompletedWorkingDocumentFactsAsReviewer", a => a._completedWorkingDocumentFactsAsReviewer, (a, b) => a._completedWorkingDocumentFactsAsReviewer = b, a => a.CompletedWorkingDocumentFactsAsReviewer, () => new CompletedUserDimensionRelations().CompletedWorkingDocumentFactEntityUsingReportingUserReviewerId, typeof(CompletedWorkingDocumentFactEntity), (int)ConceptCave.Data.EntityType.CompletedWorkingDocumentFactEntity);
				AddNavigatorMetaData<CompletedUserDimensionEntity, EntityCollection<CompletedWorkingDocumentPresentedFactEntity>>("CompletedWorkingDocumentPresentedFactsReviewee", a => a._completedWorkingDocumentPresentedFactsReviewee, (a, b) => a._completedWorkingDocumentPresentedFactsReviewee = b, a => a.CompletedWorkingDocumentPresentedFactsReviewee, () => new CompletedUserDimensionRelations().CompletedWorkingDocumentPresentedFactEntityUsingReportingUserRevieweeId, typeof(CompletedWorkingDocumentPresentedFactEntity), (int)ConceptCave.Data.EntityType.CompletedWorkingDocumentPresentedFactEntity);
				AddNavigatorMetaData<CompletedUserDimensionEntity, EntityCollection<CompletedWorkingDocumentPresentedFactEntity>>("CompletedWorkingDocumentPresentedFactsReviewer", a => a._completedWorkingDocumentPresentedFactsReviewer, (a, b) => a._completedWorkingDocumentPresentedFactsReviewer = b, a => a.CompletedWorkingDocumentPresentedFactsReviewer, () => new CompletedUserDimensionRelations().CompletedWorkingDocumentPresentedFactEntityUsingReportingUserReviewerId, typeof(CompletedWorkingDocumentPresentedFactEntity), (int)ConceptCave.Data.EntityType.CompletedWorkingDocumentPresentedFactEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static CompletedUserDimensionEntity()
		{
		}

		/// <summary> CTor</summary>
		public CompletedUserDimensionEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public CompletedUserDimensionEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this CompletedUserDimensionEntity</param>
		public CompletedUserDimensionEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="id">PK value for CompletedUserDimension which data should be fetched into this CompletedUserDimension object</param>
		public CompletedUserDimensionEntity(System.Guid id) : this(id, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="id">PK value for CompletedUserDimension which data should be fetched into this CompletedUserDimension object</param>
		/// <param name="validator">The custom validator object for this CompletedUserDimensionEntity</param>
		public CompletedUserDimensionEntity(System.Guid id, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.Id = id;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected CompletedUserDimensionEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'CompletedWorkingDocumentFact' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCompletedWorkingDocumentFactsAsReviewee() { return CreateRelationInfoForNavigator("CompletedWorkingDocumentFactsAsReviewee"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'CompletedWorkingDocumentFact' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCompletedWorkingDocumentFactsAsReviewer() { return CreateRelationInfoForNavigator("CompletedWorkingDocumentFactsAsReviewer"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'CompletedWorkingDocumentPresentedFact' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCompletedWorkingDocumentPresentedFactsReviewee() { return CreateRelationInfoForNavigator("CompletedWorkingDocumentPresentedFactsReviewee"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'CompletedWorkingDocumentPresentedFact' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoCompletedWorkingDocumentPresentedFactsReviewer() { return CreateRelationInfoForNavigator("CompletedWorkingDocumentPresentedFactsReviewer"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this CompletedUserDimensionEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static CompletedUserDimensionRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'CompletedWorkingDocumentFact' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCompletedWorkingDocumentFactsAsReviewee { get { return _staticMetaData.GetPrefetchPathElement("CompletedWorkingDocumentFactsAsReviewee", CommonEntityBase.CreateEntityCollection<CompletedWorkingDocumentFactEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'CompletedWorkingDocumentFact' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCompletedWorkingDocumentFactsAsReviewer { get { return _staticMetaData.GetPrefetchPathElement("CompletedWorkingDocumentFactsAsReviewer", CommonEntityBase.CreateEntityCollection<CompletedWorkingDocumentFactEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'CompletedWorkingDocumentPresentedFact' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCompletedWorkingDocumentPresentedFactsReviewee { get { return _staticMetaData.GetPrefetchPathElement("CompletedWorkingDocumentPresentedFactsReviewee", CommonEntityBase.CreateEntityCollection<CompletedWorkingDocumentPresentedFactEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'CompletedWorkingDocumentPresentedFact' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathCompletedWorkingDocumentPresentedFactsReviewer { get { return _staticMetaData.GetPrefetchPathElement("CompletedWorkingDocumentPresentedFactsReviewer", CommonEntityBase.CreateEntityCollection<CompletedWorkingDocumentPresentedFactEntity>()); } }

		/// <summary>The Email property of the Entity CompletedUserDimension<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblCompletedUserDimension"."Email".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 256.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Email
		{
			get { return (System.String)GetValue((int)CompletedUserDimensionFieldIndex.Email, true); }
			set	{ SetValue((int)CompletedUserDimensionFieldIndex.Email, value); }
		}

		/// <summary>The Id property of the Entity CompletedUserDimension<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblCompletedUserDimension"."Id".<br/>Table field type characteristics (type, precision, scale, length): UniqueIdentifier, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Guid Id
		{
			get { return (System.Guid)GetValue((int)CompletedUserDimensionFieldIndex.Id, true); }
			set	{ SetValue((int)CompletedUserDimensionFieldIndex.Id, value); }
		}

		/// <summary>The Name property of the Entity CompletedUserDimension<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblCompletedUserDimension"."Name".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 200.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)CompletedUserDimensionFieldIndex.Name, true); }
			set	{ SetValue((int)CompletedUserDimensionFieldIndex.Name, value); }
		}

		/// <summary>The TimeZone property of the Entity CompletedUserDimension<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblCompletedUserDimension"."TimeZone".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String TimeZone
		{
			get { return (System.String)GetValue((int)CompletedUserDimensionFieldIndex.TimeZone, true); }
			set	{ SetValue((int)CompletedUserDimensionFieldIndex.TimeZone, value); }
		}

		/// <summary>The Username property of the Entity CompletedUserDimension<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblCompletedUserDimension"."Username".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 256.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Username
		{
			get { return (System.String)GetValue((int)CompletedUserDimensionFieldIndex.Username, true); }
			set	{ SetValue((int)CompletedUserDimensionFieldIndex.Username, value); }
		}

		/// <summary>Gets the EntityCollection with the related entities of type 'CompletedWorkingDocumentFactEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(CompletedWorkingDocumentFactEntity))]
		public virtual EntityCollection<CompletedWorkingDocumentFactEntity> CompletedWorkingDocumentFactsAsReviewee { get { return GetOrCreateEntityCollection<CompletedWorkingDocumentFactEntity, CompletedWorkingDocumentFactEntityFactory>("RevieweeUser", true, false, ref _completedWorkingDocumentFactsAsReviewee); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'CompletedWorkingDocumentFactEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(CompletedWorkingDocumentFactEntity))]
		public virtual EntityCollection<CompletedWorkingDocumentFactEntity> CompletedWorkingDocumentFactsAsReviewer { get { return GetOrCreateEntityCollection<CompletedWorkingDocumentFactEntity, CompletedWorkingDocumentFactEntityFactory>("ReviewerUser", true, false, ref _completedWorkingDocumentFactsAsReviewer); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'CompletedWorkingDocumentPresentedFactEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(CompletedWorkingDocumentPresentedFactEntity))]
		public virtual EntityCollection<CompletedWorkingDocumentPresentedFactEntity> CompletedWorkingDocumentPresentedFactsReviewee { get { return GetOrCreateEntityCollection<CompletedWorkingDocumentPresentedFactEntity, CompletedWorkingDocumentPresentedFactEntityFactory>("CompletedUserDimensionReviewee", true, false, ref _completedWorkingDocumentPresentedFactsReviewee); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'CompletedWorkingDocumentPresentedFactEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(CompletedWorkingDocumentPresentedFactEntity))]
		public virtual EntityCollection<CompletedWorkingDocumentPresentedFactEntity> CompletedWorkingDocumentPresentedFactsReviewer { get { return GetOrCreateEntityCollection<CompletedWorkingDocumentPresentedFactEntity, CompletedWorkingDocumentPresentedFactEntityFactory>("CompletedUserDimensionReviewer", true, false, ref _completedWorkingDocumentPresentedFactsReviewer); } }

		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END

	}
}

namespace ConceptCave.Data
{
	public enum CompletedUserDimensionFieldIndex
	{
		///<summary>Email. </summary>
		Email,
		///<summary>Id. </summary>
		Id,
		///<summary>Name. </summary>
		Name,
		///<summary>TimeZone. </summary>
		TimeZone,
		///<summary>Username. </summary>
		Username,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace ConceptCave.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: CompletedUserDimension. </summary>
	public partial class CompletedUserDimensionRelations: RelationFactory
	{
		/// <summary>Returns a new IEntityRelation object, between CompletedUserDimensionEntity and CompletedWorkingDocumentFactEntity over the 1:n relation they have, using the relation between the fields: CompletedUserDimension.Id - CompletedWorkingDocumentFact.ReportingUserRevieweeId</summary>
		public virtual IEntityRelation CompletedWorkingDocumentFactEntityUsingReportingUserRevieweeId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "CompletedWorkingDocumentFactsAsReviewee", true, new[] { CompletedUserDimensionFields.Id, CompletedWorkingDocumentFactFields.ReportingUserRevieweeId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between CompletedUserDimensionEntity and CompletedWorkingDocumentFactEntity over the 1:n relation they have, using the relation between the fields: CompletedUserDimension.Id - CompletedWorkingDocumentFact.ReportingUserReviewerId</summary>
		public virtual IEntityRelation CompletedWorkingDocumentFactEntityUsingReportingUserReviewerId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "CompletedWorkingDocumentFactsAsReviewer", true, new[] { CompletedUserDimensionFields.Id, CompletedWorkingDocumentFactFields.ReportingUserReviewerId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between CompletedUserDimensionEntity and CompletedWorkingDocumentPresentedFactEntity over the 1:n relation they have, using the relation between the fields: CompletedUserDimension.Id - CompletedWorkingDocumentPresentedFact.ReportingUserRevieweeId</summary>
		public virtual IEntityRelation CompletedWorkingDocumentPresentedFactEntityUsingReportingUserRevieweeId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "CompletedWorkingDocumentPresentedFactsReviewee", true, new[] { CompletedUserDimensionFields.Id, CompletedWorkingDocumentPresentedFactFields.ReportingUserRevieweeId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between CompletedUserDimensionEntity and CompletedWorkingDocumentPresentedFactEntity over the 1:n relation they have, using the relation between the fields: CompletedUserDimension.Id - CompletedWorkingDocumentPresentedFact.ReportingUserReviewerId</summary>
		public virtual IEntityRelation CompletedWorkingDocumentPresentedFactEntityUsingReportingUserReviewerId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "CompletedWorkingDocumentPresentedFactsReviewer", true, new[] { CompletedUserDimensionFields.Id, CompletedWorkingDocumentPresentedFactFields.ReportingUserReviewerId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticCompletedUserDimensionRelations
	{
		internal static readonly IEntityRelation CompletedWorkingDocumentFactEntityUsingReportingUserRevieweeIdStatic = new CompletedUserDimensionRelations().CompletedWorkingDocumentFactEntityUsingReportingUserRevieweeId;
		internal static readonly IEntityRelation CompletedWorkingDocumentFactEntityUsingReportingUserReviewerIdStatic = new CompletedUserDimensionRelations().CompletedWorkingDocumentFactEntityUsingReportingUserReviewerId;
		internal static readonly IEntityRelation CompletedWorkingDocumentPresentedFactEntityUsingReportingUserRevieweeIdStatic = new CompletedUserDimensionRelations().CompletedWorkingDocumentPresentedFactEntityUsingReportingUserRevieweeId;
		internal static readonly IEntityRelation CompletedWorkingDocumentPresentedFactEntityUsingReportingUserReviewerIdStatic = new CompletedUserDimensionRelations().CompletedWorkingDocumentPresentedFactEntityUsingReportingUserReviewerId;

		/// <summary>CTor</summary>
		static StaticCompletedUserDimensionRelations() { }
	}
}
