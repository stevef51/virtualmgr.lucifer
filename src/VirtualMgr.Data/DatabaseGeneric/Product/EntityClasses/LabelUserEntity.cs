﻿//////////////////////////////////////////////////////////////
// <auto-generated>This code was generated by LLBLGen Pro 5.5.</auto-generated>
//////////////////////////////////////////////////////////////
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Data.FactoryClasses;
using ConceptCave.Data.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace ConceptCave.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'LabelUser'.<br/><br/></summary>
	[Serializable]
	public partial class LabelUserEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		private LabelEntity _label;
		private UserDataEntity _userData;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static LabelUserEntityStaticMetaData _staticMetaData = new LabelUserEntityStaticMetaData();
		private static LabelUserRelations _relationsFactory = new LabelUserRelations();

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Label</summary>
			public static readonly string Label = "Label";
			/// <summary>Member name UserData</summary>
			public static readonly string UserData = "UserData";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class LabelUserEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public LabelUserEntityStaticMetaData()
			{
				SetEntityCoreInfo("LabelUserEntity", InheritanceHierarchyType.None, false, (int)ConceptCave.Data.EntityType.LabelUserEntity, typeof(LabelUserEntity), typeof(LabelUserEntityFactory), false);
				AddNavigatorMetaData<LabelUserEntity, LabelEntity>("Label", "LabelUsers", (a, b) => a._label = b, a => a._label, (a, b) => a.Label = b, ConceptCave.Data.RelationClasses.StaticLabelUserRelations.LabelEntityUsingLabelIdStatic, ()=>new LabelUserRelations().LabelEntityUsingLabelId, null, new int[] { (int)LabelUserFieldIndex.LabelId }, null, true, (int)ConceptCave.Data.EntityType.LabelEntity);
				AddNavigatorMetaData<LabelUserEntity, UserDataEntity>("UserData", "LabelUsers", (a, b) => a._userData = b, a => a._userData, (a, b) => a.UserData = b, ConceptCave.Data.RelationClasses.StaticLabelUserRelations.UserDataEntityUsingUserIdStatic, ()=>new LabelUserRelations().UserDataEntityUsingUserId, null, new int[] { (int)LabelUserFieldIndex.UserId }, null, true, (int)ConceptCave.Data.EntityType.UserDataEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static LabelUserEntity()
		{
		}

		/// <summary> CTor</summary>
		public LabelUserEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public LabelUserEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this LabelUserEntity</param>
		public LabelUserEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="labelId">PK value for LabelUser which data should be fetched into this LabelUser object</param>
		/// <param name="userId">PK value for LabelUser which data should be fetched into this LabelUser object</param>
		public LabelUserEntity(System.Guid labelId, System.Guid userId) : this(labelId, userId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="labelId">PK value for LabelUser which data should be fetched into this LabelUser object</param>
		/// <param name="userId">PK value for LabelUser which data should be fetched into this LabelUser object</param>
		/// <param name="validator">The custom validator object for this LabelUserEntity</param>
		public LabelUserEntity(System.Guid labelId, System.Guid userId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.LabelId = labelId;
			this.UserId = userId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected LabelUserEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Label' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoLabel() { return CreateRelationInfoForNavigator("Label"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'UserData' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoUserData() { return CreateRelationInfoForNavigator("UserData"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this LabelUserEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static LabelUserRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Label' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathLabel { get { return _staticMetaData.GetPrefetchPathElement("Label", CommonEntityBase.CreateEntityCollection<LabelEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'UserData' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathUserData { get { return _staticMetaData.GetPrefetchPathElement("UserData", CommonEntityBase.CreateEntityCollection<UserDataEntity>()); } }

		/// <summary>The LabelId property of the Entity LabelUser<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblLabelUser"."LabelId".<br/>Table field type characteristics (type, precision, scale, length): UniqueIdentifier, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Guid LabelId
		{
			get { return (System.Guid)GetValue((int)LabelUserFieldIndex.LabelId, true); }
			set	{ SetValue((int)LabelUserFieldIndex.LabelId, value); }
		}

		/// <summary>The UserId property of the Entity LabelUser<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblLabelUser"."UserId".<br/>Table field type characteristics (type, precision, scale, length): UniqueIdentifier, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Guid UserId
		{
			get { return (System.Guid)GetValue((int)LabelUserFieldIndex.UserId, true); }
			set	{ SetValue((int)LabelUserFieldIndex.UserId, value); }
		}

		/// <summary>Gets / sets related entity of type 'LabelEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual LabelEntity Label
		{
			get { return _label; }
			set { SetSingleRelatedEntityNavigator(value, "Label"); }
		}

		/// <summary>Gets / sets related entity of type 'UserDataEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual UserDataEntity UserData
		{
			get { return _userData; }
			set { SetSingleRelatedEntityNavigator(value, "UserData"); }
		}

		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END

	}
}

namespace ConceptCave.Data
{
	public enum LabelUserFieldIndex
	{
		///<summary>LabelId. </summary>
		LabelId,
		///<summary>UserId. </summary>
		UserId,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace ConceptCave.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: LabelUser. </summary>
	public partial class LabelUserRelations: RelationFactory
	{

		/// <summary>Returns a new IEntityRelation object, between LabelUserEntity and LabelEntity over the m:1 relation they have, using the relation between the fields: LabelUser.LabelId - Label.Id</summary>
		public virtual IEntityRelation LabelEntityUsingLabelId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Label", false, new[] { LabelFields.Id, LabelUserFields.LabelId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between LabelUserEntity and UserDataEntity over the m:1 relation they have, using the relation between the fields: LabelUser.UserId - UserData.UserId</summary>
		public virtual IEntityRelation UserDataEntityUsingUserId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "UserData", false, new[] { UserDataFields.UserId, LabelUserFields.UserId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticLabelUserRelations
	{
		internal static readonly IEntityRelation LabelEntityUsingLabelIdStatic = new LabelUserRelations().LabelEntityUsingLabelId;
		internal static readonly IEntityRelation UserDataEntityUsingUserIdStatic = new LabelUserRelations().UserDataEntityUsingUserId;

		/// <summary>CTor</summary>
		static StaticLabelUserRelations() { }
	}
}
