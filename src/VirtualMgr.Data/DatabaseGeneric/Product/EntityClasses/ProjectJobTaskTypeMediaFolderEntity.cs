﻿//////////////////////////////////////////////////////////////
// <auto-generated>This code was generated by LLBLGen Pro 5.5.</auto-generated>
//////////////////////////////////////////////////////////////
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Data.FactoryClasses;
using ConceptCave.Data.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace ConceptCave.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'ProjectJobTaskTypeMediaFolder'.<br/><br/></summary>
	[Serializable]
	public partial class ProjectJobTaskTypeMediaFolderEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		private ProjectJobTaskTypeEntity _projectJobTaskType;
		private MediaFolderEntity _mediaFolder;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static ProjectJobTaskTypeMediaFolderEntityStaticMetaData _staticMetaData = new ProjectJobTaskTypeMediaFolderEntityStaticMetaData();
		private static ProjectJobTaskTypeMediaFolderRelations _relationsFactory = new ProjectJobTaskTypeMediaFolderRelations();

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ProjectJobTaskType</summary>
			public static readonly string ProjectJobTaskType = "ProjectJobTaskType";
			/// <summary>Member name MediaFolder</summary>
			public static readonly string MediaFolder = "MediaFolder";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class ProjectJobTaskTypeMediaFolderEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public ProjectJobTaskTypeMediaFolderEntityStaticMetaData()
			{
				SetEntityCoreInfo("ProjectJobTaskTypeMediaFolderEntity", InheritanceHierarchyType.None, false, (int)ConceptCave.Data.EntityType.ProjectJobTaskTypeMediaFolderEntity, typeof(ProjectJobTaskTypeMediaFolderEntity), typeof(ProjectJobTaskTypeMediaFolderEntityFactory), false);
				AddNavigatorMetaData<ProjectJobTaskTypeMediaFolderEntity, ProjectJobTaskTypeEntity>("ProjectJobTaskType", "ProjectJobTaskTypeMediaFolders", (a, b) => a._projectJobTaskType = b, a => a._projectJobTaskType, (a, b) => a.ProjectJobTaskType = b, ConceptCave.Data.RelationClasses.StaticProjectJobTaskTypeMediaFolderRelations.ProjectJobTaskTypeEntityUsingTaskTypeIdStatic, ()=>new ProjectJobTaskTypeMediaFolderRelations().ProjectJobTaskTypeEntityUsingTaskTypeId, null, new int[] { (int)ProjectJobTaskTypeMediaFolderFieldIndex.TaskTypeId }, null, true, (int)ConceptCave.Data.EntityType.ProjectJobTaskTypeEntity);
				AddNavigatorMetaData<ProjectJobTaskTypeMediaFolderEntity, MediaFolderEntity>("MediaFolder", "ProjectJobTaskTypeMediaFolders", (a, b) => a._mediaFolder = b, a => a._mediaFolder, (a, b) => a.MediaFolder = b, ConceptCave.Data.RelationClasses.StaticProjectJobTaskTypeMediaFolderRelations.MediaFolderEntityUsingMediaFolderIdStatic, ()=>new ProjectJobTaskTypeMediaFolderRelations().MediaFolderEntityUsingMediaFolderId, null, new int[] { (int)ProjectJobTaskTypeMediaFolderFieldIndex.MediaFolderId }, null, true, (int)ConceptCave.Data.EntityType.MediaFolderEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static ProjectJobTaskTypeMediaFolderEntity()
		{
		}

		/// <summary> CTor</summary>
		public ProjectJobTaskTypeMediaFolderEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public ProjectJobTaskTypeMediaFolderEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this ProjectJobTaskTypeMediaFolderEntity</param>
		public ProjectJobTaskTypeMediaFolderEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="mediaFolderId">PK value for ProjectJobTaskTypeMediaFolder which data should be fetched into this ProjectJobTaskTypeMediaFolder object</param>
		/// <param name="taskTypeId">PK value for ProjectJobTaskTypeMediaFolder which data should be fetched into this ProjectJobTaskTypeMediaFolder object</param>
		public ProjectJobTaskTypeMediaFolderEntity(System.Guid mediaFolderId, System.Guid taskTypeId) : this(mediaFolderId, taskTypeId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="mediaFolderId">PK value for ProjectJobTaskTypeMediaFolder which data should be fetched into this ProjectJobTaskTypeMediaFolder object</param>
		/// <param name="taskTypeId">PK value for ProjectJobTaskTypeMediaFolder which data should be fetched into this ProjectJobTaskTypeMediaFolder object</param>
		/// <param name="validator">The custom validator object for this ProjectJobTaskTypeMediaFolderEntity</param>
		public ProjectJobTaskTypeMediaFolderEntity(System.Guid mediaFolderId, System.Guid taskTypeId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.MediaFolderId = mediaFolderId;
			this.TaskTypeId = taskTypeId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ProjectJobTaskTypeMediaFolderEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'ProjectJobTaskType' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoProjectJobTaskType() { return CreateRelationInfoForNavigator("ProjectJobTaskType"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'MediaFolder' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoMediaFolder() { return CreateRelationInfoForNavigator("MediaFolder"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this ProjectJobTaskTypeMediaFolderEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static ProjectJobTaskTypeMediaFolderRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ProjectJobTaskType' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathProjectJobTaskType { get { return _staticMetaData.GetPrefetchPathElement("ProjectJobTaskType", CommonEntityBase.CreateEntityCollection<ProjectJobTaskTypeEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'MediaFolder' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathMediaFolder { get { return _staticMetaData.GetPrefetchPathElement("MediaFolder", CommonEntityBase.CreateEntityCollection<MediaFolderEntity>()); } }

		/// <summary>The MediaFolderId property of the Entity ProjectJobTaskTypeMediaFolder<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblProjectJobTaskTypeMediaFolder"."MediaFolderId".<br/>Table field type characteristics (type, precision, scale, length): UniqueIdentifier, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Guid MediaFolderId
		{
			get { return (System.Guid)GetValue((int)ProjectJobTaskTypeMediaFolderFieldIndex.MediaFolderId, true); }
			set	{ SetValue((int)ProjectJobTaskTypeMediaFolderFieldIndex.MediaFolderId, value); }
		}

		/// <summary>The TaskTypeId property of the Entity ProjectJobTaskTypeMediaFolder<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblProjectJobTaskTypeMediaFolder"."TaskTypeId".<br/>Table field type characteristics (type, precision, scale, length): UniqueIdentifier, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Guid TaskTypeId
		{
			get { return (System.Guid)GetValue((int)ProjectJobTaskTypeMediaFolderFieldIndex.TaskTypeId, true); }
			set	{ SetValue((int)ProjectJobTaskTypeMediaFolderFieldIndex.TaskTypeId, value); }
		}

		/// <summary>The Text property of the Entity ProjectJobTaskTypeMediaFolder<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblProjectJobTaskTypeMediaFolder"."Text".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Text
		{
			get { return (System.String)GetValue((int)ProjectJobTaskTypeMediaFolderFieldIndex.Text, true); }
			set	{ SetValue((int)ProjectJobTaskTypeMediaFolderFieldIndex.Text, value); }
		}

		/// <summary>Gets / sets related entity of type 'ProjectJobTaskTypeEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ProjectJobTaskTypeEntity ProjectJobTaskType
		{
			get { return _projectJobTaskType; }
			set { SetSingleRelatedEntityNavigator(value, "ProjectJobTaskType"); }
		}

		/// <summary>Gets / sets related entity of type 'MediaFolderEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual MediaFolderEntity MediaFolder
		{
			get { return _mediaFolder; }
			set { SetSingleRelatedEntityNavigator(value, "MediaFolder"); }
		}

		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END

	}
}

namespace ConceptCave.Data
{
	public enum ProjectJobTaskTypeMediaFolderFieldIndex
	{
		///<summary>MediaFolderId. </summary>
		MediaFolderId,
		///<summary>TaskTypeId. </summary>
		TaskTypeId,
		///<summary>Text. </summary>
		Text,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace ConceptCave.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ProjectJobTaskTypeMediaFolder. </summary>
	public partial class ProjectJobTaskTypeMediaFolderRelations: RelationFactory
	{

		/// <summary>Returns a new IEntityRelation object, between ProjectJobTaskTypeMediaFolderEntity and ProjectJobTaskTypeEntity over the m:1 relation they have, using the relation between the fields: ProjectJobTaskTypeMediaFolder.TaskTypeId - ProjectJobTaskType.Id</summary>
		public virtual IEntityRelation ProjectJobTaskTypeEntityUsingTaskTypeId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "ProjectJobTaskType", false, new[] { ProjectJobTaskTypeFields.Id, ProjectJobTaskTypeMediaFolderFields.TaskTypeId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ProjectJobTaskTypeMediaFolderEntity and MediaFolderEntity over the m:1 relation they have, using the relation between the fields: ProjectJobTaskTypeMediaFolder.MediaFolderId - MediaFolder.Id</summary>
		public virtual IEntityRelation MediaFolderEntityUsingMediaFolderId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "MediaFolder", false, new[] { MediaFolderFields.Id, ProjectJobTaskTypeMediaFolderFields.MediaFolderId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticProjectJobTaskTypeMediaFolderRelations
	{
		internal static readonly IEntityRelation ProjectJobTaskTypeEntityUsingTaskTypeIdStatic = new ProjectJobTaskTypeMediaFolderRelations().ProjectJobTaskTypeEntityUsingTaskTypeId;
		internal static readonly IEntityRelation MediaFolderEntityUsingMediaFolderIdStatic = new ProjectJobTaskTypeMediaFolderRelations().MediaFolderEntityUsingMediaFolderId;

		/// <summary>CTor</summary>
		static StaticProjectJobTaskTypeMediaFolderRelations() { }
	}
}
