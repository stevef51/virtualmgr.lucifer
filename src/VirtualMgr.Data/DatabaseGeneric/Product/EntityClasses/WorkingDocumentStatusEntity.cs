﻿//////////////////////////////////////////////////////////////
// <auto-generated>This code was generated by LLBLGen Pro 5.5.</auto-generated>
//////////////////////////////////////////////////////////////
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Data.FactoryClasses;
using ConceptCave.Data.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace ConceptCave.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'WorkingDocumentStatus'.<br/><br/></summary>
	[Serializable]
	public partial class WorkingDocumentStatusEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		private EntityCollection<WorkingDocumentEntity> _workingDocuments;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static WorkingDocumentStatusEntityStaticMetaData _staticMetaData = new WorkingDocumentStatusEntityStaticMetaData();
		private static WorkingDocumentStatusRelations _relationsFactory = new WorkingDocumentStatusRelations();

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name WorkingDocuments</summary>
			public static readonly string WorkingDocuments = "WorkingDocuments";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class WorkingDocumentStatusEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public WorkingDocumentStatusEntityStaticMetaData()
			{
				SetEntityCoreInfo("WorkingDocumentStatusEntity", InheritanceHierarchyType.None, false, (int)ConceptCave.Data.EntityType.WorkingDocumentStatusEntity, typeof(WorkingDocumentStatusEntity), typeof(WorkingDocumentStatusEntityFactory), false);
				AddNavigatorMetaData<WorkingDocumentStatusEntity, EntityCollection<WorkingDocumentEntity>>("WorkingDocuments", a => a._workingDocuments, (a, b) => a._workingDocuments = b, a => a.WorkingDocuments, () => new WorkingDocumentStatusRelations().WorkingDocumentEntityUsingStatus, typeof(WorkingDocumentEntity), (int)ConceptCave.Data.EntityType.WorkingDocumentEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static WorkingDocumentStatusEntity()
		{
		}

		/// <summary> CTor</summary>
		public WorkingDocumentStatusEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public WorkingDocumentStatusEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this WorkingDocumentStatusEntity</param>
		public WorkingDocumentStatusEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="id">PK value for WorkingDocumentStatus which data should be fetched into this WorkingDocumentStatus object</param>
		public WorkingDocumentStatusEntity(System.Int32 id) : this(id, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="id">PK value for WorkingDocumentStatus which data should be fetched into this WorkingDocumentStatus object</param>
		/// <param name="validator">The custom validator object for this WorkingDocumentStatusEntity</param>
		public WorkingDocumentStatusEntity(System.Int32 id, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.Id = id;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected WorkingDocumentStatusEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'WorkingDocument' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoWorkingDocuments() { return CreateRelationInfoForNavigator("WorkingDocuments"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this WorkingDocumentStatusEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static WorkingDocumentStatusRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'WorkingDocument' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathWorkingDocuments { get { return _staticMetaData.GetPrefetchPathElement("WorkingDocuments", CommonEntityBase.CreateEntityCollection<WorkingDocumentEntity>()); } }

		/// <summary>The Id property of the Entity WorkingDocumentStatus<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblWorkingDocumentStatus"."Id".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int32 Id
		{
			get { return (System.Int32)GetValue((int)WorkingDocumentStatusFieldIndex.Id, true); }
			set	{ SetValue((int)WorkingDocumentStatusFieldIndex.Id, value); }
		}

		/// <summary>The Text property of the Entity WorkingDocumentStatus<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblWorkingDocumentStatus"."Text".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 25.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Text
		{
			get { return (System.String)GetValue((int)WorkingDocumentStatusFieldIndex.Text, true); }
			set	{ SetValue((int)WorkingDocumentStatusFieldIndex.Text, value); }
		}

		/// <summary>Gets the EntityCollection with the related entities of type 'WorkingDocumentEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(WorkingDocumentEntity))]
		public virtual EntityCollection<WorkingDocumentEntity> WorkingDocuments { get { return GetOrCreateEntityCollection<WorkingDocumentEntity, WorkingDocumentEntityFactory>("WorkingDocumentStatu", true, false, ref _workingDocuments); } }

		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END

	}
}

namespace ConceptCave.Data
{
	public enum WorkingDocumentStatusFieldIndex
	{
		///<summary>Id. </summary>
		Id,
		///<summary>Text. </summary>
		Text,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace ConceptCave.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: WorkingDocumentStatus. </summary>
	public partial class WorkingDocumentStatusRelations: RelationFactory
	{
		/// <summary>Returns a new IEntityRelation object, between WorkingDocumentStatusEntity and WorkingDocumentEntity over the 1:n relation they have, using the relation between the fields: WorkingDocumentStatus.Id - WorkingDocument.Status</summary>
		public virtual IEntityRelation WorkingDocumentEntityUsingStatus
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "WorkingDocuments", true, new[] { WorkingDocumentStatusFields.Id, WorkingDocumentFields.Status }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticWorkingDocumentStatusRelations
	{
		internal static readonly IEntityRelation WorkingDocumentEntityUsingStatusStatic = new WorkingDocumentStatusRelations().WorkingDocumentEntityUsingStatus;

		/// <summary>CTor</summary>
		static StaticWorkingDocumentStatusRelations() { }
	}
}
