﻿//////////////////////////////////////////////////////////////
// <auto-generated>This code was generated by LLBLGen Pro 5.5.</auto-generated>
//////////////////////////////////////////////////////////////
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Data.FactoryClasses;
using ConceptCave.Data.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace ConceptCave.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'UserType'.<br/><br/></summary>
	[Serializable]
	public partial class UserTypeEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		private EntityCollection<UserTypeTaskTypeRelationshipEntity> _userTypeTaskTypeRelationships;
		private EntityCollection<HierarchyRoleUserTypeEntity> _hierarchyRoleUserTypes;
		private EntityCollection<UserDataEntity> _userDatas;
		private EntityCollection<UserTypeContextPublishedResourceEntity> _userTypeContextPublishedResources;
		private EntityCollection<UserTypeDashboardEntity> _userTypeDashboards;
		private EntityCollection<UserTypeDefaultLabelEntity> _userTypeDefaultLabels;
		private EntityCollection<UserTypeDefaultRoleEntity> _userTypeDefaultRoles;
		private UserBillingTypeEntity _userBillingType;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static UserTypeEntityStaticMetaData _staticMetaData = new UserTypeEntityStaticMetaData();
		private static UserTypeRelations _relationsFactory = new UserTypeRelations();

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name UserBillingType</summary>
			public static readonly string UserBillingType = "UserBillingType";
			/// <summary>Member name UserTypeTaskTypeRelationships</summary>
			public static readonly string UserTypeTaskTypeRelationships = "UserTypeTaskTypeRelationships";
			/// <summary>Member name HierarchyRoleUserTypes</summary>
			public static readonly string HierarchyRoleUserTypes = "HierarchyRoleUserTypes";
			/// <summary>Member name UserDatas</summary>
			public static readonly string UserDatas = "UserDatas";
			/// <summary>Member name UserTypeContextPublishedResources</summary>
			public static readonly string UserTypeContextPublishedResources = "UserTypeContextPublishedResources";
			/// <summary>Member name UserTypeDashboards</summary>
			public static readonly string UserTypeDashboards = "UserTypeDashboards";
			/// <summary>Member name UserTypeDefaultLabels</summary>
			public static readonly string UserTypeDefaultLabels = "UserTypeDefaultLabels";
			/// <summary>Member name UserTypeDefaultRoles</summary>
			public static readonly string UserTypeDefaultRoles = "UserTypeDefaultRoles";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class UserTypeEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public UserTypeEntityStaticMetaData()
			{
				SetEntityCoreInfo("UserTypeEntity", InheritanceHierarchyType.None, false, (int)ConceptCave.Data.EntityType.UserTypeEntity, typeof(UserTypeEntity), typeof(UserTypeEntityFactory), false);
				AddNavigatorMetaData<UserTypeEntity, EntityCollection<UserTypeTaskTypeRelationshipEntity>>("UserTypeTaskTypeRelationships", a => a._userTypeTaskTypeRelationships, (a, b) => a._userTypeTaskTypeRelationships = b, a => a.UserTypeTaskTypeRelationships, () => new UserTypeRelations().UserTypeTaskTypeRelationshipEntityUsingUserTypeId, typeof(UserTypeTaskTypeRelationshipEntity), (int)ConceptCave.Data.EntityType.UserTypeTaskTypeRelationshipEntity);
				AddNavigatorMetaData<UserTypeEntity, EntityCollection<HierarchyRoleUserTypeEntity>>("HierarchyRoleUserTypes", a => a._hierarchyRoleUserTypes, (a, b) => a._hierarchyRoleUserTypes = b, a => a.HierarchyRoleUserTypes, () => new UserTypeRelations().HierarchyRoleUserTypeEntityUsingUserTypeId, typeof(HierarchyRoleUserTypeEntity), (int)ConceptCave.Data.EntityType.HierarchyRoleUserTypeEntity);
				AddNavigatorMetaData<UserTypeEntity, EntityCollection<UserDataEntity>>("UserDatas", a => a._userDatas, (a, b) => a._userDatas = b, a => a.UserDatas, () => new UserTypeRelations().UserDataEntityUsingUserTypeId, typeof(UserDataEntity), (int)ConceptCave.Data.EntityType.UserDataEntity);
				AddNavigatorMetaData<UserTypeEntity, EntityCollection<UserTypeContextPublishedResourceEntity>>("UserTypeContextPublishedResources", a => a._userTypeContextPublishedResources, (a, b) => a._userTypeContextPublishedResources = b, a => a.UserTypeContextPublishedResources, () => new UserTypeRelations().UserTypeContextPublishedResourceEntityUsingUserTypeId, typeof(UserTypeContextPublishedResourceEntity), (int)ConceptCave.Data.EntityType.UserTypeContextPublishedResourceEntity);
				AddNavigatorMetaData<UserTypeEntity, EntityCollection<UserTypeDashboardEntity>>("UserTypeDashboards", a => a._userTypeDashboards, (a, b) => a._userTypeDashboards = b, a => a.UserTypeDashboards, () => new UserTypeRelations().UserTypeDashboardEntityUsingUserTypeId, typeof(UserTypeDashboardEntity), (int)ConceptCave.Data.EntityType.UserTypeDashboardEntity);
				AddNavigatorMetaData<UserTypeEntity, EntityCollection<UserTypeDefaultLabelEntity>>("UserTypeDefaultLabels", a => a._userTypeDefaultLabels, (a, b) => a._userTypeDefaultLabels = b, a => a.UserTypeDefaultLabels, () => new UserTypeRelations().UserTypeDefaultLabelEntityUsingUserTypeId, typeof(UserTypeDefaultLabelEntity), (int)ConceptCave.Data.EntityType.UserTypeDefaultLabelEntity);
				AddNavigatorMetaData<UserTypeEntity, EntityCollection<UserTypeDefaultRoleEntity>>("UserTypeDefaultRoles", a => a._userTypeDefaultRoles, (a, b) => a._userTypeDefaultRoles = b, a => a.UserTypeDefaultRoles, () => new UserTypeRelations().UserTypeDefaultRoleEntityUsingUserTypeId, typeof(UserTypeDefaultRoleEntity), (int)ConceptCave.Data.EntityType.UserTypeDefaultRoleEntity);
				AddNavigatorMetaData<UserTypeEntity, UserBillingTypeEntity>("UserBillingType", "UserTypes", (a, b) => a._userBillingType = b, a => a._userBillingType, (a, b) => a.UserBillingType = b, ConceptCave.Data.RelationClasses.StaticUserTypeRelations.UserBillingTypeEntityUsingUserBillingTypeIdStatic, ()=>new UserTypeRelations().UserBillingTypeEntityUsingUserBillingTypeId, null, new int[] { (int)UserTypeFieldIndex.UserBillingTypeId }, null, true, (int)ConceptCave.Data.EntityType.UserBillingTypeEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static UserTypeEntity()
		{
		}

		/// <summary> CTor</summary>
		public UserTypeEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public UserTypeEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this UserTypeEntity</param>
		public UserTypeEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="id">PK value for UserType which data should be fetched into this UserType object</param>
		public UserTypeEntity(System.Int32 id) : this(id, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="id">PK value for UserType which data should be fetched into this UserType object</param>
		/// <param name="validator">The custom validator object for this UserTypeEntity</param>
		public UserTypeEntity(System.Int32 id, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.Id = id;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected UserTypeEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'UserTypeTaskTypeRelationship' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoUserTypeTaskTypeRelationships() { return CreateRelationInfoForNavigator("UserTypeTaskTypeRelationships"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'HierarchyRoleUserType' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoHierarchyRoleUserTypes() { return CreateRelationInfoForNavigator("HierarchyRoleUserTypes"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'UserData' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoUserDatas() { return CreateRelationInfoForNavigator("UserDatas"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'UserTypeContextPublishedResource' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoUserTypeContextPublishedResources() { return CreateRelationInfoForNavigator("UserTypeContextPublishedResources"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'UserTypeDashboard' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoUserTypeDashboards() { return CreateRelationInfoForNavigator("UserTypeDashboards"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'UserTypeDefaultLabel' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoUserTypeDefaultLabels() { return CreateRelationInfoForNavigator("UserTypeDefaultLabels"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'UserTypeDefaultRole' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoUserTypeDefaultRoles() { return CreateRelationInfoForNavigator("UserTypeDefaultRoles"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'UserBillingType' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoUserBillingType() { return CreateRelationInfoForNavigator("UserBillingType"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this UserTypeEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static UserTypeRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'UserTypeTaskTypeRelationship' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathUserTypeTaskTypeRelationships { get { return _staticMetaData.GetPrefetchPathElement("UserTypeTaskTypeRelationships", CommonEntityBase.CreateEntityCollection<UserTypeTaskTypeRelationshipEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'HierarchyRoleUserType' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathHierarchyRoleUserTypes { get { return _staticMetaData.GetPrefetchPathElement("HierarchyRoleUserTypes", CommonEntityBase.CreateEntityCollection<HierarchyRoleUserTypeEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'UserData' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathUserDatas { get { return _staticMetaData.GetPrefetchPathElement("UserDatas", CommonEntityBase.CreateEntityCollection<UserDataEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'UserTypeContextPublishedResource' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathUserTypeContextPublishedResources { get { return _staticMetaData.GetPrefetchPathElement("UserTypeContextPublishedResources", CommonEntityBase.CreateEntityCollection<UserTypeContextPublishedResourceEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'UserTypeDashboard' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathUserTypeDashboards { get { return _staticMetaData.GetPrefetchPathElement("UserTypeDashboards", CommonEntityBase.CreateEntityCollection<UserTypeDashboardEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'UserTypeDefaultLabel' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathUserTypeDefaultLabels { get { return _staticMetaData.GetPrefetchPathElement("UserTypeDefaultLabels", CommonEntityBase.CreateEntityCollection<UserTypeDefaultLabelEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'UserTypeDefaultRole' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathUserTypeDefaultRoles { get { return _staticMetaData.GetPrefetchPathElement("UserTypeDefaultRoles", CommonEntityBase.CreateEntityCollection<UserTypeDefaultRoleEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'UserBillingType' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathUserBillingType { get { return _staticMetaData.GetPrefetchPathElement("UserBillingType", CommonEntityBase.CreateEntityCollection<UserBillingTypeEntity>()); } }

		/// <summary>The CanLogin property of the Entity UserType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblUserType"."CanLogin".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean CanLogin
		{
			get { return (System.Boolean)GetValue((int)UserTypeFieldIndex.CanLogin, true); }
			set	{ SetValue((int)UserTypeFieldIndex.CanLogin, value); }
		}

		/// <summary>The DefaultDashboard property of the Entity UserType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblUserType"."DefaultDashboard".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String DefaultDashboard
		{
			get { return (System.String)GetValue((int)UserTypeFieldIndex.DefaultDashboard, true); }
			set	{ SetValue((int)UserTypeFieldIndex.DefaultDashboard, value); }
		}

		/// <summary>The HasGeoCordinates property of the Entity UserType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblUserType"."HasGeoCordinates".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean HasGeoCordinates
		{
			get { return (System.Boolean)GetValue((int)UserTypeFieldIndex.HasGeoCordinates, true); }
			set	{ SetValue((int)UserTypeFieldIndex.HasGeoCordinates, value); }
		}

		/// <summary>The Id property of the Entity UserType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblUserType"."Id".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 Id
		{
			get { return (System.Int32)GetValue((int)UserTypeFieldIndex.Id, true); }
			set	{ SetValue((int)UserTypeFieldIndex.Id, value); }
		}

		/// <summary>The IsAsite property of the Entity UserType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblUserType"."IsASite".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsAsite
		{
			get { return (System.Boolean)GetValue((int)UserTypeFieldIndex.IsAsite, true); }
			set	{ SetValue((int)UserTypeFieldIndex.IsAsite, value); }
		}

		/// <summary>The Name property of the Entity UserType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblUserType"."Name".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)UserTypeFieldIndex.Name, true); }
			set	{ SetValue((int)UserTypeFieldIndex.Name, value); }
		}

		/// <summary>The UserBillingTypeId property of the Entity UserType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblUserType"."UserBillingTypeId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UserBillingTypeId
		{
			get { return (Nullable<System.Int32>)GetValue((int)UserTypeFieldIndex.UserBillingTypeId, false); }
			set	{ SetValue((int)UserTypeFieldIndex.UserBillingTypeId, value); }
		}

		/// <summary>Gets the EntityCollection with the related entities of type 'UserTypeTaskTypeRelationshipEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(UserTypeTaskTypeRelationshipEntity))]
		public virtual EntityCollection<UserTypeTaskTypeRelationshipEntity> UserTypeTaskTypeRelationships { get { return GetOrCreateEntityCollection<UserTypeTaskTypeRelationshipEntity, UserTypeTaskTypeRelationshipEntityFactory>("UserType", true, false, ref _userTypeTaskTypeRelationships); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'HierarchyRoleUserTypeEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(HierarchyRoleUserTypeEntity))]
		public virtual EntityCollection<HierarchyRoleUserTypeEntity> HierarchyRoleUserTypes { get { return GetOrCreateEntityCollection<HierarchyRoleUserTypeEntity, HierarchyRoleUserTypeEntityFactory>("UserType", true, false, ref _hierarchyRoleUserTypes); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'UserDataEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(UserDataEntity))]
		public virtual EntityCollection<UserDataEntity> UserDatas { get { return GetOrCreateEntityCollection<UserDataEntity, UserDataEntityFactory>("UserType", true, false, ref _userDatas); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'UserTypeContextPublishedResourceEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(UserTypeContextPublishedResourceEntity))]
		public virtual EntityCollection<UserTypeContextPublishedResourceEntity> UserTypeContextPublishedResources { get { return GetOrCreateEntityCollection<UserTypeContextPublishedResourceEntity, UserTypeContextPublishedResourceEntityFactory>("UserType", true, false, ref _userTypeContextPublishedResources); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'UserTypeDashboardEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(UserTypeDashboardEntity))]
		public virtual EntityCollection<UserTypeDashboardEntity> UserTypeDashboards { get { return GetOrCreateEntityCollection<UserTypeDashboardEntity, UserTypeDashboardEntityFactory>("UserType", true, false, ref _userTypeDashboards); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'UserTypeDefaultLabelEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(UserTypeDefaultLabelEntity))]
		public virtual EntityCollection<UserTypeDefaultLabelEntity> UserTypeDefaultLabels { get { return GetOrCreateEntityCollection<UserTypeDefaultLabelEntity, UserTypeDefaultLabelEntityFactory>("UserType", true, false, ref _userTypeDefaultLabels); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'UserTypeDefaultRoleEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(UserTypeDefaultRoleEntity))]
		public virtual EntityCollection<UserTypeDefaultRoleEntity> UserTypeDefaultRoles { get { return GetOrCreateEntityCollection<UserTypeDefaultRoleEntity, UserTypeDefaultRoleEntityFactory>("UserType", true, false, ref _userTypeDefaultRoles); } }

		/// <summary>Gets / sets related entity of type 'UserBillingTypeEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual UserBillingTypeEntity UserBillingType
		{
			get { return _userBillingType; }
			set { SetSingleRelatedEntityNavigator(value, "UserBillingType"); }
		}

		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END

	}
}

namespace ConceptCave.Data
{
	public enum UserTypeFieldIndex
	{
		///<summary>CanLogin. </summary>
		CanLogin,
		///<summary>DefaultDashboard. </summary>
		DefaultDashboard,
		///<summary>HasGeoCordinates. </summary>
		HasGeoCordinates,
		///<summary>Id. </summary>
		Id,
		///<summary>IsAsite. </summary>
		IsAsite,
		///<summary>Name. </summary>
		Name,
		///<summary>UserBillingTypeId. </summary>
		UserBillingTypeId,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace ConceptCave.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: UserType. </summary>
	public partial class UserTypeRelations: RelationFactory
	{
		/// <summary>Returns a new IEntityRelation object, between UserTypeEntity and UserTypeTaskTypeRelationshipEntity over the 1:n relation they have, using the relation between the fields: UserType.Id - UserTypeTaskTypeRelationship.UserTypeId</summary>
		public virtual IEntityRelation UserTypeTaskTypeRelationshipEntityUsingUserTypeId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "UserTypeTaskTypeRelationships", true, new[] { UserTypeFields.Id, UserTypeTaskTypeRelationshipFields.UserTypeId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between UserTypeEntity and HierarchyRoleUserTypeEntity over the 1:n relation they have, using the relation between the fields: UserType.Id - HierarchyRoleUserType.UserTypeId</summary>
		public virtual IEntityRelation HierarchyRoleUserTypeEntityUsingUserTypeId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "HierarchyRoleUserTypes", true, new[] { UserTypeFields.Id, HierarchyRoleUserTypeFields.UserTypeId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between UserTypeEntity and UserDataEntity over the 1:n relation they have, using the relation between the fields: UserType.Id - UserData.UserTypeId</summary>
		public virtual IEntityRelation UserDataEntityUsingUserTypeId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "UserDatas", true, new[] { UserTypeFields.Id, UserDataFields.UserTypeId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between UserTypeEntity and UserTypeContextPublishedResourceEntity over the 1:n relation they have, using the relation between the fields: UserType.Id - UserTypeContextPublishedResource.UserTypeId</summary>
		public virtual IEntityRelation UserTypeContextPublishedResourceEntityUsingUserTypeId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "UserTypeContextPublishedResources", true, new[] { UserTypeFields.Id, UserTypeContextPublishedResourceFields.UserTypeId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between UserTypeEntity and UserTypeDashboardEntity over the 1:n relation they have, using the relation between the fields: UserType.Id - UserTypeDashboard.UserTypeId</summary>
		public virtual IEntityRelation UserTypeDashboardEntityUsingUserTypeId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "UserTypeDashboards", true, new[] { UserTypeFields.Id, UserTypeDashboardFields.UserTypeId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between UserTypeEntity and UserTypeDefaultLabelEntity over the 1:n relation they have, using the relation between the fields: UserType.Id - UserTypeDefaultLabel.UserTypeId</summary>
		public virtual IEntityRelation UserTypeDefaultLabelEntityUsingUserTypeId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "UserTypeDefaultLabels", true, new[] { UserTypeFields.Id, UserTypeDefaultLabelFields.UserTypeId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between UserTypeEntity and UserTypeDefaultRoleEntity over the 1:n relation they have, using the relation between the fields: UserType.Id - UserTypeDefaultRole.UserTypeId</summary>
		public virtual IEntityRelation UserTypeDefaultRoleEntityUsingUserTypeId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "UserTypeDefaultRoles", true, new[] { UserTypeFields.Id, UserTypeDefaultRoleFields.UserTypeId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between UserTypeEntity and UserBillingTypeEntity over the m:1 relation they have, using the relation between the fields: UserType.UserBillingTypeId - UserBillingType.Id</summary>
		public virtual IEntityRelation UserBillingTypeEntityUsingUserBillingTypeId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "UserBillingType", false, new[] { UserBillingTypeFields.Id, UserTypeFields.UserBillingTypeId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticUserTypeRelations
	{
		internal static readonly IEntityRelation UserTypeTaskTypeRelationshipEntityUsingUserTypeIdStatic = new UserTypeRelations().UserTypeTaskTypeRelationshipEntityUsingUserTypeId;
		internal static readonly IEntityRelation HierarchyRoleUserTypeEntityUsingUserTypeIdStatic = new UserTypeRelations().HierarchyRoleUserTypeEntityUsingUserTypeId;
		internal static readonly IEntityRelation UserDataEntityUsingUserTypeIdStatic = new UserTypeRelations().UserDataEntityUsingUserTypeId;
		internal static readonly IEntityRelation UserTypeContextPublishedResourceEntityUsingUserTypeIdStatic = new UserTypeRelations().UserTypeContextPublishedResourceEntityUsingUserTypeId;
		internal static readonly IEntityRelation UserTypeDashboardEntityUsingUserTypeIdStatic = new UserTypeRelations().UserTypeDashboardEntityUsingUserTypeId;
		internal static readonly IEntityRelation UserTypeDefaultLabelEntityUsingUserTypeIdStatic = new UserTypeRelations().UserTypeDefaultLabelEntityUsingUserTypeId;
		internal static readonly IEntityRelation UserTypeDefaultRoleEntityUsingUserTypeIdStatic = new UserTypeRelations().UserTypeDefaultRoleEntityUsingUserTypeId;
		internal static readonly IEntityRelation UserBillingTypeEntityUsingUserBillingTypeIdStatic = new UserTypeRelations().UserBillingTypeEntityUsingUserBillingTypeId;

		/// <summary>CTor</summary>
		static StaticUserTypeRelations() { }
	}
}
