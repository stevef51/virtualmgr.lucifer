﻿//////////////////////////////////////////////////////////////
// <auto-generated>This code was generated by LLBLGen Pro 5.5.</auto-generated>
//////////////////////////////////////////////////////////////
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Data.FactoryClasses;
using ConceptCave.Data.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace ConceptCave.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'HierarchyRoleUserType'.<br/><br/></summary>
	[Serializable]
	public partial class HierarchyRoleUserTypeEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		private HierarchyRoleEntity _hierarchyRole;
		private UserTypeEntity _userType;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static HierarchyRoleUserTypeEntityStaticMetaData _staticMetaData = new HierarchyRoleUserTypeEntityStaticMetaData();
		private static HierarchyRoleUserTypeRelations _relationsFactory = new HierarchyRoleUserTypeRelations();

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name HierarchyRole</summary>
			public static readonly string HierarchyRole = "HierarchyRole";
			/// <summary>Member name UserType</summary>
			public static readonly string UserType = "UserType";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class HierarchyRoleUserTypeEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public HierarchyRoleUserTypeEntityStaticMetaData()
			{
				SetEntityCoreInfo("HierarchyRoleUserTypeEntity", InheritanceHierarchyType.None, false, (int)ConceptCave.Data.EntityType.HierarchyRoleUserTypeEntity, typeof(HierarchyRoleUserTypeEntity), typeof(HierarchyRoleUserTypeEntityFactory), false);
				AddNavigatorMetaData<HierarchyRoleUserTypeEntity, HierarchyRoleEntity>("HierarchyRole", "HierarchyRoleUserTypes", (a, b) => a._hierarchyRole = b, a => a._hierarchyRole, (a, b) => a.HierarchyRole = b, ConceptCave.Data.RelationClasses.StaticHierarchyRoleUserTypeRelations.HierarchyRoleEntityUsingRoleIdStatic, ()=>new HierarchyRoleUserTypeRelations().HierarchyRoleEntityUsingRoleId, null, new int[] { (int)HierarchyRoleUserTypeFieldIndex.RoleId }, null, true, (int)ConceptCave.Data.EntityType.HierarchyRoleEntity);
				AddNavigatorMetaData<HierarchyRoleUserTypeEntity, UserTypeEntity>("UserType", "HierarchyRoleUserTypes", (a, b) => a._userType = b, a => a._userType, (a, b) => a.UserType = b, ConceptCave.Data.RelationClasses.StaticHierarchyRoleUserTypeRelations.UserTypeEntityUsingUserTypeIdStatic, ()=>new HierarchyRoleUserTypeRelations().UserTypeEntityUsingUserTypeId, null, new int[] { (int)HierarchyRoleUserTypeFieldIndex.UserTypeId }, null, true, (int)ConceptCave.Data.EntityType.UserTypeEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static HierarchyRoleUserTypeEntity()
		{
		}

		/// <summary> CTor</summary>
		public HierarchyRoleUserTypeEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public HierarchyRoleUserTypeEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this HierarchyRoleUserTypeEntity</param>
		public HierarchyRoleUserTypeEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="id">PK value for HierarchyRoleUserType which data should be fetched into this HierarchyRoleUserType object</param>
		public HierarchyRoleUserTypeEntity(System.Int32 id) : this(id, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="id">PK value for HierarchyRoleUserType which data should be fetched into this HierarchyRoleUserType object</param>
		/// <param name="validator">The custom validator object for this HierarchyRoleUserTypeEntity</param>
		public HierarchyRoleUserTypeEntity(System.Int32 id, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.Id = id;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected HierarchyRoleUserTypeEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'HierarchyRole' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoHierarchyRole() { return CreateRelationInfoForNavigator("HierarchyRole"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'UserType' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoUserType() { return CreateRelationInfoForNavigator("UserType"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this HierarchyRoleUserTypeEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static HierarchyRoleUserTypeRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'HierarchyRole' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathHierarchyRole { get { return _staticMetaData.GetPrefetchPathElement("HierarchyRole", CommonEntityBase.CreateEntityCollection<HierarchyRoleEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'UserType' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathUserType { get { return _staticMetaData.GetPrefetchPathElement("UserType", CommonEntityBase.CreateEntityCollection<UserTypeEntity>()); } }

		/// <summary>The Id property of the Entity HierarchyRoleUserType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblHierarchyRoleUserType"."Id".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 Id
		{
			get { return (System.Int32)GetValue((int)HierarchyRoleUserTypeFieldIndex.Id, true); }
			set { SetValue((int)HierarchyRoleUserTypeFieldIndex.Id, value); }		}

		/// <summary>The RoleId property of the Entity HierarchyRoleUserType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblHierarchyRoleUserType"."RoleId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RoleId
		{
			get { return (System.Int32)GetValue((int)HierarchyRoleUserTypeFieldIndex.RoleId, true); }
			set	{ SetValue((int)HierarchyRoleUserTypeFieldIndex.RoleId, value); }
		}

		/// <summary>The UserTypeId property of the Entity HierarchyRoleUserType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblHierarchyRoleUserType"."UserTypeId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UserTypeId
		{
			get { return (System.Int32)GetValue((int)HierarchyRoleUserTypeFieldIndex.UserTypeId, true); }
			set	{ SetValue((int)HierarchyRoleUserTypeFieldIndex.UserTypeId, value); }
		}

		/// <summary>Gets / sets related entity of type 'HierarchyRoleEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual HierarchyRoleEntity HierarchyRole
		{
			get { return _hierarchyRole; }
			set { SetSingleRelatedEntityNavigator(value, "HierarchyRole"); }
		}

		/// <summary>Gets / sets related entity of type 'UserTypeEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual UserTypeEntity UserType
		{
			get { return _userType; }
			set { SetSingleRelatedEntityNavigator(value, "UserType"); }
		}

		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END

	}
}

namespace ConceptCave.Data
{
	public enum HierarchyRoleUserTypeFieldIndex
	{
		///<summary>Id. </summary>
		Id,
		///<summary>RoleId. </summary>
		RoleId,
		///<summary>UserTypeId. </summary>
		UserTypeId,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace ConceptCave.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: HierarchyRoleUserType. </summary>
	public partial class HierarchyRoleUserTypeRelations: RelationFactory
	{

		/// <summary>Returns a new IEntityRelation object, between HierarchyRoleUserTypeEntity and HierarchyRoleEntity over the m:1 relation they have, using the relation between the fields: HierarchyRoleUserType.RoleId - HierarchyRole.Id</summary>
		public virtual IEntityRelation HierarchyRoleEntityUsingRoleId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "HierarchyRole", false, new[] { HierarchyRoleFields.Id, HierarchyRoleUserTypeFields.RoleId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between HierarchyRoleUserTypeEntity and UserTypeEntity over the m:1 relation they have, using the relation between the fields: HierarchyRoleUserType.UserTypeId - UserType.Id</summary>
		public virtual IEntityRelation UserTypeEntityUsingUserTypeId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "UserType", false, new[] { UserTypeFields.Id, HierarchyRoleUserTypeFields.UserTypeId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticHierarchyRoleUserTypeRelations
	{
		internal static readonly IEntityRelation HierarchyRoleEntityUsingRoleIdStatic = new HierarchyRoleUserTypeRelations().HierarchyRoleEntityUsingRoleId;
		internal static readonly IEntityRelation UserTypeEntityUsingUserTypeIdStatic = new HierarchyRoleUserTypeRelations().UserTypeEntityUsingUserTypeId;

		/// <summary>CTor</summary>
		static StaticHierarchyRoleUserTypeRelations() { }
	}
}
