﻿//////////////////////////////////////////////////////////////
// <auto-generated>This code was generated by LLBLGen Pro 5.5.</auto-generated>
//////////////////////////////////////////////////////////////
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Data.FactoryClasses;
using ConceptCave.Data.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace ConceptCave.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'MediaViewing'.<br/><br/></summary>
	[Serializable]
	public partial class MediaViewingEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		private EntityCollection<MediaPageViewingEntity> _mediaPageViewings;
		private MediaEntity _media;
		private UserDataEntity _userData;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static MediaViewingEntityStaticMetaData _staticMetaData = new MediaViewingEntityStaticMetaData();
		private static MediaViewingRelations _relationsFactory = new MediaViewingRelations();

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Media</summary>
			public static readonly string Media = "Media";
			/// <summary>Member name UserData</summary>
			public static readonly string UserData = "UserData";
			/// <summary>Member name MediaPageViewings</summary>
			public static readonly string MediaPageViewings = "MediaPageViewings";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class MediaViewingEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public MediaViewingEntityStaticMetaData()
			{
				SetEntityCoreInfo("MediaViewingEntity", InheritanceHierarchyType.None, false, (int)ConceptCave.Data.EntityType.MediaViewingEntity, typeof(MediaViewingEntity), typeof(MediaViewingEntityFactory), false);
				AddNavigatorMetaData<MediaViewingEntity, EntityCollection<MediaPageViewingEntity>>("MediaPageViewings", a => a._mediaPageViewings, (a, b) => a._mediaPageViewings = b, a => a.MediaPageViewings, () => new MediaViewingRelations().MediaPageViewingEntityUsingMediaViewingId, typeof(MediaPageViewingEntity), (int)ConceptCave.Data.EntityType.MediaPageViewingEntity);
				AddNavigatorMetaData<MediaViewingEntity, MediaEntity>("Media", "MediaViewings", (a, b) => a._media = b, a => a._media, (a, b) => a.Media = b, ConceptCave.Data.RelationClasses.StaticMediaViewingRelations.MediaEntityUsingMediaIdStatic, ()=>new MediaViewingRelations().MediaEntityUsingMediaId, null, new int[] { (int)MediaViewingFieldIndex.MediaId }, null, true, (int)ConceptCave.Data.EntityType.MediaEntity);
				AddNavigatorMetaData<MediaViewingEntity, UserDataEntity>("UserData", "MediaViewings", (a, b) => a._userData = b, a => a._userData, (a, b) => a.UserData = b, ConceptCave.Data.RelationClasses.StaticMediaViewingRelations.UserDataEntityUsingUserIdStatic, ()=>new MediaViewingRelations().UserDataEntityUsingUserId, null, new int[] { (int)MediaViewingFieldIndex.UserId }, null, true, (int)ConceptCave.Data.EntityType.UserDataEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static MediaViewingEntity()
		{
		}

		/// <summary> CTor</summary>
		public MediaViewingEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public MediaViewingEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this MediaViewingEntity</param>
		public MediaViewingEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="id">PK value for MediaViewing which data should be fetched into this MediaViewing object</param>
		public MediaViewingEntity(System.Guid id) : this(id, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="id">PK value for MediaViewing which data should be fetched into this MediaViewing object</param>
		/// <param name="validator">The custom validator object for this MediaViewingEntity</param>
		public MediaViewingEntity(System.Guid id, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.Id = id;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected MediaViewingEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'MediaPageViewing' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoMediaPageViewings() { return CreateRelationInfoForNavigator("MediaPageViewings"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Media' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoMedia() { return CreateRelationInfoForNavigator("Media"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'UserData' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoUserData() { return CreateRelationInfoForNavigator("UserData"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this MediaViewingEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static MediaViewingRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'MediaPageViewing' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathMediaPageViewings { get { return _staticMetaData.GetPrefetchPathElement("MediaPageViewings", CommonEntityBase.CreateEntityCollection<MediaPageViewingEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Media' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathMedia { get { return _staticMetaData.GetPrefetchPathElement("Media", CommonEntityBase.CreateEntityCollection<MediaEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'UserData' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathUserData { get { return _staticMetaData.GetPrefetchPathElement("UserData", CommonEntityBase.CreateEntityCollection<UserDataEntity>()); } }

		/// <summary>The Accepted property of the Entity MediaViewing<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblMediaViewing"."Accepted".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Accepted
		{
			get { return (System.Boolean)GetValue((int)MediaViewingFieldIndex.Accepted, true); }
			set	{ SetValue((int)MediaViewingFieldIndex.Accepted, value); }
		}

		/// <summary>The AcceptionNotes property of the Entity MediaViewing<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblMediaViewing"."AcceptionNotes".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 4000.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String AcceptionNotes
		{
			get { return (System.String)GetValue((int)MediaViewingFieldIndex.AcceptionNotes, true); }
			set	{ SetValue((int)MediaViewingFieldIndex.AcceptionNotes, value); }
		}

		/// <summary>The DateCompleted property of the Entity MediaViewing<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblMediaViewing"."DateCompleted".<br/>Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime DateCompleted
		{
			get { return (System.DateTime)GetValue((int)MediaViewingFieldIndex.DateCompleted, true); }
			set	{ SetValue((int)MediaViewingFieldIndex.DateCompleted, value); }
		}

		/// <summary>The DateStarted property of the Entity MediaViewing<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblMediaViewing"."DateStarted".<br/>Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime DateStarted
		{
			get { return (System.DateTime)GetValue((int)MediaViewingFieldIndex.DateStarted, true); }
			set	{ SetValue((int)MediaViewingFieldIndex.DateStarted, value); }
		}

		/// <summary>The Id property of the Entity MediaViewing<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblMediaViewing"."Id".<br/>Table field type characteristics (type, precision, scale, length): UniqueIdentifier, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Guid Id
		{
			get { return (System.Guid)GetValue((int)MediaViewingFieldIndex.Id, true); }
			set	{ SetValue((int)MediaViewingFieldIndex.Id, value); }
		}

		/// <summary>The MediaId property of the Entity MediaViewing<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblMediaViewing"."MediaId".<br/>Table field type characteristics (type, precision, scale, length): UniqueIdentifier, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Guid MediaId
		{
			get { return (System.Guid)GetValue((int)MediaViewingFieldIndex.MediaId, true); }
			set	{ SetValue((int)MediaViewingFieldIndex.MediaId, value); }
		}

		/// <summary>The TotalSeconds property of the Entity MediaViewing<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblMediaViewing"."TotalSeconds".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 TotalSeconds
		{
			get { return (System.Int32)GetValue((int)MediaViewingFieldIndex.TotalSeconds, true); }
			set	{ SetValue((int)MediaViewingFieldIndex.TotalSeconds, value); }
		}

		/// <summary>The UserId property of the Entity MediaViewing<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblMediaViewing"."UserId".<br/>Table field type characteristics (type, precision, scale, length): UniqueIdentifier, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Guid UserId
		{
			get { return (System.Guid)GetValue((int)MediaViewingFieldIndex.UserId, true); }
			set	{ SetValue((int)MediaViewingFieldIndex.UserId, value); }
		}

		/// <summary>The Version property of the Entity MediaViewing<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblMediaViewing"."Version".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Version
		{
			get { return (System.Int32)GetValue((int)MediaViewingFieldIndex.Version, true); }
			set	{ SetValue((int)MediaViewingFieldIndex.Version, value); }
		}

		/// <summary>The WorkingDocumentId property of the Entity MediaViewing<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblMediaViewing"."WorkingDocumentId".<br/>Table field type characteristics (type, precision, scale, length): UniqueIdentifier, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Guid WorkingDocumentId
		{
			get { return (System.Guid)GetValue((int)MediaViewingFieldIndex.WorkingDocumentId, true); }
			set	{ SetValue((int)MediaViewingFieldIndex.WorkingDocumentId, value); }
		}

		/// <summary>Gets the EntityCollection with the related entities of type 'MediaPageViewingEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(MediaPageViewingEntity))]
		public virtual EntityCollection<MediaPageViewingEntity> MediaPageViewings { get { return GetOrCreateEntityCollection<MediaPageViewingEntity, MediaPageViewingEntityFactory>("MediaViewing", true, false, ref _mediaPageViewings); } }

		/// <summary>Gets / sets related entity of type 'MediaEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual MediaEntity Media
		{
			get { return _media; }
			set { SetSingleRelatedEntityNavigator(value, "Media"); }
		}

		/// <summary>Gets / sets related entity of type 'UserDataEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual UserDataEntity UserData
		{
			get { return _userData; }
			set { SetSingleRelatedEntityNavigator(value, "UserData"); }
		}

		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END

	}
}

namespace ConceptCave.Data
{
	public enum MediaViewingFieldIndex
	{
		///<summary>Accepted. </summary>
		Accepted,
		///<summary>AcceptionNotes. </summary>
		AcceptionNotes,
		///<summary>DateCompleted. </summary>
		DateCompleted,
		///<summary>DateStarted. </summary>
		DateStarted,
		///<summary>Id. </summary>
		Id,
		///<summary>MediaId. </summary>
		MediaId,
		///<summary>TotalSeconds. </summary>
		TotalSeconds,
		///<summary>UserId. </summary>
		UserId,
		///<summary>Version. </summary>
		Version,
		///<summary>WorkingDocumentId. </summary>
		WorkingDocumentId,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace ConceptCave.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: MediaViewing. </summary>
	public partial class MediaViewingRelations: RelationFactory
	{
		/// <summary>Returns a new IEntityRelation object, between MediaViewingEntity and MediaPageViewingEntity over the 1:n relation they have, using the relation between the fields: MediaViewing.Id - MediaPageViewing.MediaViewingId</summary>
		public virtual IEntityRelation MediaPageViewingEntityUsingMediaViewingId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "MediaPageViewings", true, new[] { MediaViewingFields.Id, MediaPageViewingFields.MediaViewingId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between MediaViewingEntity and MediaEntity over the m:1 relation they have, using the relation between the fields: MediaViewing.MediaId - Media.Id</summary>
		public virtual IEntityRelation MediaEntityUsingMediaId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Media", false, new[] { MediaFields.Id, MediaViewingFields.MediaId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between MediaViewingEntity and UserDataEntity over the m:1 relation they have, using the relation between the fields: MediaViewing.UserId - UserData.UserId</summary>
		public virtual IEntityRelation UserDataEntityUsingUserId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "UserData", false, new[] { UserDataFields.UserId, MediaViewingFields.UserId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticMediaViewingRelations
	{
		internal static readonly IEntityRelation MediaPageViewingEntityUsingMediaViewingIdStatic = new MediaViewingRelations().MediaPageViewingEntityUsingMediaViewingId;
		internal static readonly IEntityRelation MediaEntityUsingMediaIdStatic = new MediaViewingRelations().MediaEntityUsingMediaId;
		internal static readonly IEntityRelation UserDataEntityUsingUserIdStatic = new MediaViewingRelations().UserDataEntityUsingUserId;

		/// <summary>CTor</summary>
		static StaticMediaViewingRelations() { }
	}
}
