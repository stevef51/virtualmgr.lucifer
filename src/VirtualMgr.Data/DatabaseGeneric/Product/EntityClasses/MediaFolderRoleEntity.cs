﻿//////////////////////////////////////////////////////////////
// <auto-generated>This code was generated by LLBLGen Pro 5.5.</auto-generated>
//////////////////////////////////////////////////////////////
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Data.FactoryClasses;
using ConceptCave.Data.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace ConceptCave.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'MediaFolderRole'.<br/><br/></summary>
	[Serializable]
	public partial class MediaFolderRoleEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		private AspNetRoleEntity _aspNetRole;
		private MediaFolderEntity _mediaFolder;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static MediaFolderRoleEntityStaticMetaData _staticMetaData = new MediaFolderRoleEntityStaticMetaData();
		private static MediaFolderRoleRelations _relationsFactory = new MediaFolderRoleRelations();

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name AspNetRole</summary>
			public static readonly string AspNetRole = "AspNetRole";
			/// <summary>Member name MediaFolder</summary>
			public static readonly string MediaFolder = "MediaFolder";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class MediaFolderRoleEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public MediaFolderRoleEntityStaticMetaData()
			{
				SetEntityCoreInfo("MediaFolderRoleEntity", InheritanceHierarchyType.None, false, (int)ConceptCave.Data.EntityType.MediaFolderRoleEntity, typeof(MediaFolderRoleEntity), typeof(MediaFolderRoleEntityFactory), false);
				AddNavigatorMetaData<MediaFolderRoleEntity, AspNetRoleEntity>("AspNetRole", "MediaFolderRoles", (a, b) => a._aspNetRole = b, a => a._aspNetRole, (a, b) => a.AspNetRole = b, ConceptCave.Data.RelationClasses.StaticMediaFolderRoleRelations.AspNetRoleEntityUsingRoleIdStatic, ()=>new MediaFolderRoleRelations().AspNetRoleEntityUsingRoleId, null, new int[] { (int)MediaFolderRoleFieldIndex.RoleId }, null, true, (int)ConceptCave.Data.EntityType.AspNetRoleEntity);
				AddNavigatorMetaData<MediaFolderRoleEntity, MediaFolderEntity>("MediaFolder", "MediaFolderRoles", (a, b) => a._mediaFolder = b, a => a._mediaFolder, (a, b) => a.MediaFolder = b, ConceptCave.Data.RelationClasses.StaticMediaFolderRoleRelations.MediaFolderEntityUsingMediaFolderIdStatic, ()=>new MediaFolderRoleRelations().MediaFolderEntityUsingMediaFolderId, null, new int[] { (int)MediaFolderRoleFieldIndex.MediaFolderId }, null, true, (int)ConceptCave.Data.EntityType.MediaFolderEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static MediaFolderRoleEntity()
		{
		}

		/// <summary> CTor</summary>
		public MediaFolderRoleEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public MediaFolderRoleEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this MediaFolderRoleEntity</param>
		public MediaFolderRoleEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="mediaFolderId">PK value for MediaFolderRole which data should be fetched into this MediaFolderRole object</param>
		/// <param name="roleId">PK value for MediaFolderRole which data should be fetched into this MediaFolderRole object</param>
		public MediaFolderRoleEntity(System.Guid mediaFolderId, System.String roleId) : this(mediaFolderId, roleId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="mediaFolderId">PK value for MediaFolderRole which data should be fetched into this MediaFolderRole object</param>
		/// <param name="roleId">PK value for MediaFolderRole which data should be fetched into this MediaFolderRole object</param>
		/// <param name="validator">The custom validator object for this MediaFolderRoleEntity</param>
		public MediaFolderRoleEntity(System.Guid mediaFolderId, System.String roleId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.MediaFolderId = mediaFolderId;
			this.RoleId = roleId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected MediaFolderRoleEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'AspNetRole' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoAspNetRole() { return CreateRelationInfoForNavigator("AspNetRole"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'MediaFolder' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoMediaFolder() { return CreateRelationInfoForNavigator("MediaFolder"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this MediaFolderRoleEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static MediaFolderRoleRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'AspNetRole' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathAspNetRole { get { return _staticMetaData.GetPrefetchPathElement("AspNetRole", CommonEntityBase.CreateEntityCollection<AspNetRoleEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'MediaFolder' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathMediaFolder { get { return _staticMetaData.GetPrefetchPathElement("MediaFolder", CommonEntityBase.CreateEntityCollection<MediaFolderEntity>()); } }

		/// <summary>The MediaFolderId property of the Entity MediaFolderRole<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblMediaFolderRole"."MediaFolderId".<br/>Table field type characteristics (type, precision, scale, length): UniqueIdentifier, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Guid MediaFolderId
		{
			get { return (System.Guid)GetValue((int)MediaFolderRoleFieldIndex.MediaFolderId, true); }
			set	{ SetValue((int)MediaFolderRoleFieldIndex.MediaFolderId, value); }
		}

		/// <summary>The RoleId property of the Entity MediaFolderRole<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblMediaFolderRole"."RoleId".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 128.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.String RoleId
		{
			get { return (System.String)GetValue((int)MediaFolderRoleFieldIndex.RoleId, true); }
			set	{ SetValue((int)MediaFolderRoleFieldIndex.RoleId, value); }
		}

		/// <summary>Gets / sets related entity of type 'AspNetRoleEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual AspNetRoleEntity AspNetRole
		{
			get { return _aspNetRole; }
			set { SetSingleRelatedEntityNavigator(value, "AspNetRole"); }
		}

		/// <summary>Gets / sets related entity of type 'MediaFolderEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual MediaFolderEntity MediaFolder
		{
			get { return _mediaFolder; }
			set { SetSingleRelatedEntityNavigator(value, "MediaFolder"); }
		}

		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END

	}
}

namespace ConceptCave.Data
{
	public enum MediaFolderRoleFieldIndex
	{
		///<summary>MediaFolderId. </summary>
		MediaFolderId,
		///<summary>RoleId. </summary>
		RoleId,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace ConceptCave.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: MediaFolderRole. </summary>
	public partial class MediaFolderRoleRelations: RelationFactory
	{

		/// <summary>Returns a new IEntityRelation object, between MediaFolderRoleEntity and AspNetRoleEntity over the m:1 relation they have, using the relation between the fields: MediaFolderRole.RoleId - AspNetRole.Id</summary>
		public virtual IEntityRelation AspNetRoleEntityUsingRoleId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "AspNetRole", false, new[] { AspNetRoleFields.Id, MediaFolderRoleFields.RoleId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between MediaFolderRoleEntity and MediaFolderEntity over the m:1 relation they have, using the relation between the fields: MediaFolderRole.MediaFolderId - MediaFolder.Id</summary>
		public virtual IEntityRelation MediaFolderEntityUsingMediaFolderId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "MediaFolder", false, new[] { MediaFolderFields.Id, MediaFolderRoleFields.MediaFolderId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticMediaFolderRoleRelations
	{
		internal static readonly IEntityRelation AspNetRoleEntityUsingRoleIdStatic = new MediaFolderRoleRelations().AspNetRoleEntityUsingRoleId;
		internal static readonly IEntityRelation MediaFolderEntityUsingMediaFolderIdStatic = new MediaFolderRoleRelations().MediaFolderEntityUsingMediaFolderId;

		/// <summary>CTor</summary>
		static StaticMediaFolderRoleRelations() { }
	}
}
