﻿//////////////////////////////////////////////////////////////
// <auto-generated>This code was generated by LLBLGen Pro 5.5.</auto-generated>
//////////////////////////////////////////////////////////////
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Data.FactoryClasses;
using ConceptCave.Data.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace ConceptCave.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'AssetTypeCalendar'.<br/><br/></summary>
	[Serializable]
	public partial class AssetTypeCalendarEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		private EntityCollection<AssetScheduleEntity> _assetSchedules;
		private EntityCollection<AssetTypeCalendarTaskEntity> _assetTypeCalendarTasks;
		private AssetTypeEntity _assetType;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static AssetTypeCalendarEntityStaticMetaData _staticMetaData = new AssetTypeCalendarEntityStaticMetaData();
		private static AssetTypeCalendarRelations _relationsFactory = new AssetTypeCalendarRelations();

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name AssetType</summary>
			public static readonly string AssetType = "AssetType";
			/// <summary>Member name AssetSchedules</summary>
			public static readonly string AssetSchedules = "AssetSchedules";
			/// <summary>Member name AssetTypeCalendarTasks</summary>
			public static readonly string AssetTypeCalendarTasks = "AssetTypeCalendarTasks";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class AssetTypeCalendarEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public AssetTypeCalendarEntityStaticMetaData()
			{
				SetEntityCoreInfo("AssetTypeCalendarEntity", InheritanceHierarchyType.None, false, (int)ConceptCave.Data.EntityType.AssetTypeCalendarEntity, typeof(AssetTypeCalendarEntity), typeof(AssetTypeCalendarEntityFactory), false);
				AddNavigatorMetaData<AssetTypeCalendarEntity, EntityCollection<AssetScheduleEntity>>("AssetSchedules", a => a._assetSchedules, (a, b) => a._assetSchedules = b, a => a.AssetSchedules, () => new AssetTypeCalendarRelations().AssetScheduleEntityUsingAssetTypeCalendarId, typeof(AssetScheduleEntity), (int)ConceptCave.Data.EntityType.AssetScheduleEntity);
				AddNavigatorMetaData<AssetTypeCalendarEntity, EntityCollection<AssetTypeCalendarTaskEntity>>("AssetTypeCalendarTasks", a => a._assetTypeCalendarTasks, (a, b) => a._assetTypeCalendarTasks = b, a => a.AssetTypeCalendarTasks, () => new AssetTypeCalendarRelations().AssetTypeCalendarTaskEntityUsingAssetTypeCalendarId, typeof(AssetTypeCalendarTaskEntity), (int)ConceptCave.Data.EntityType.AssetTypeCalendarTaskEntity);
				AddNavigatorMetaData<AssetTypeCalendarEntity, AssetTypeEntity>("AssetType", "AssetTypeCalendars", (a, b) => a._assetType = b, a => a._assetType, (a, b) => a.AssetType = b, ConceptCave.Data.RelationClasses.StaticAssetTypeCalendarRelations.AssetTypeEntityUsingAssetTypeIdStatic, ()=>new AssetTypeCalendarRelations().AssetTypeEntityUsingAssetTypeId, null, new int[] { (int)AssetTypeCalendarFieldIndex.AssetTypeId }, null, true, (int)ConceptCave.Data.EntityType.AssetTypeEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static AssetTypeCalendarEntity()
		{
		}

		/// <summary> CTor</summary>
		public AssetTypeCalendarEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public AssetTypeCalendarEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this AssetTypeCalendarEntity</param>
		public AssetTypeCalendarEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="id">PK value for AssetTypeCalendar which data should be fetched into this AssetTypeCalendar object</param>
		public AssetTypeCalendarEntity(System.Int32 id) : this(id, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="id">PK value for AssetTypeCalendar which data should be fetched into this AssetTypeCalendar object</param>
		/// <param name="validator">The custom validator object for this AssetTypeCalendarEntity</param>
		public AssetTypeCalendarEntity(System.Int32 id, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.Id = id;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected AssetTypeCalendarEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'AssetSchedule' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoAssetSchedules() { return CreateRelationInfoForNavigator("AssetSchedules"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'AssetTypeCalendarTask' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoAssetTypeCalendarTasks() { return CreateRelationInfoForNavigator("AssetTypeCalendarTasks"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'AssetType' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoAssetType() { return CreateRelationInfoForNavigator("AssetType"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this AssetTypeCalendarEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static AssetTypeCalendarRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'AssetSchedule' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathAssetSchedules { get { return _staticMetaData.GetPrefetchPathElement("AssetSchedules", CommonEntityBase.CreateEntityCollection<AssetScheduleEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'AssetTypeCalendarTask' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathAssetTypeCalendarTasks { get { return _staticMetaData.GetPrefetchPathElement("AssetTypeCalendarTasks", CommonEntityBase.CreateEntityCollection<AssetTypeCalendarTaskEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'AssetType' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathAssetType { get { return _staticMetaData.GetPrefetchPathElement("AssetType", CommonEntityBase.CreateEntityCollection<AssetTypeEntity>()); } }

		/// <summary>The Archived property of the Entity AssetTypeCalendar<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblAssetTypeCalendar"."Archived".<br/>Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Archived
		{
			get { return (System.Boolean)GetValue((int)AssetTypeCalendarFieldIndex.Archived, true); }
			set	{ SetValue((int)AssetTypeCalendarFieldIndex.Archived, value); }
		}

		/// <summary>The AssetTypeId property of the Entity AssetTypeCalendar<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblAssetTypeCalendar"."AssetTypeId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 AssetTypeId
		{
			get { return (System.Int32)GetValue((int)AssetTypeCalendarFieldIndex.AssetTypeId, true); }
			set	{ SetValue((int)AssetTypeCalendarFieldIndex.AssetTypeId, value); }
		}

		/// <summary>The Id property of the Entity AssetTypeCalendar<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblAssetTypeCalendar"."Id".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 Id
		{
			get { return (System.Int32)GetValue((int)AssetTypeCalendarFieldIndex.Id, true); }
			set { SetValue((int)AssetTypeCalendarFieldIndex.Id, value); }		}

		/// <summary>The Name property of the Entity AssetTypeCalendar<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblAssetTypeCalendar"."Name".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)AssetTypeCalendarFieldIndex.Name, true); }
			set	{ SetValue((int)AssetTypeCalendarFieldIndex.Name, value); }
		}

		/// <summary>The ScheduleExpression property of the Entity AssetTypeCalendar<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblAssetTypeCalendar"."ScheduleExpression".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String ScheduleExpression
		{
			get { return (System.String)GetValue((int)AssetTypeCalendarFieldIndex.ScheduleExpression, true); }
			set	{ SetValue((int)AssetTypeCalendarFieldIndex.ScheduleExpression, value); }
		}

		/// <summary>The TimeZone property of the Entity AssetTypeCalendar<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblAssetTypeCalendar"."TimeZone".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String TimeZone
		{
			get { return (System.String)GetValue((int)AssetTypeCalendarFieldIndex.TimeZone, true); }
			set	{ SetValue((int)AssetTypeCalendarFieldIndex.TimeZone, value); }
		}

		/// <summary>Gets the EntityCollection with the related entities of type 'AssetScheduleEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(AssetScheduleEntity))]
		public virtual EntityCollection<AssetScheduleEntity> AssetSchedules { get { return GetOrCreateEntityCollection<AssetScheduleEntity, AssetScheduleEntityFactory>("AssetTypeCalendar", true, false, ref _assetSchedules); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'AssetTypeCalendarTaskEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(AssetTypeCalendarTaskEntity))]
		public virtual EntityCollection<AssetTypeCalendarTaskEntity> AssetTypeCalendarTasks { get { return GetOrCreateEntityCollection<AssetTypeCalendarTaskEntity, AssetTypeCalendarTaskEntityFactory>("AssetTypeCalendar", true, false, ref _assetTypeCalendarTasks); } }

		/// <summary>Gets / sets related entity of type 'AssetTypeEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual AssetTypeEntity AssetType
		{
			get { return _assetType; }
			set { SetSingleRelatedEntityNavigator(value, "AssetType"); }
		}

		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END

	}
}

namespace ConceptCave.Data
{
	public enum AssetTypeCalendarFieldIndex
	{
		///<summary>Archived. </summary>
		Archived,
		///<summary>AssetTypeId. </summary>
		AssetTypeId,
		///<summary>Id. </summary>
		Id,
		///<summary>Name. </summary>
		Name,
		///<summary>ScheduleExpression. </summary>
		ScheduleExpression,
		///<summary>TimeZone. </summary>
		TimeZone,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace ConceptCave.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: AssetTypeCalendar. </summary>
	public partial class AssetTypeCalendarRelations: RelationFactory
	{
		/// <summary>Returns a new IEntityRelation object, between AssetTypeCalendarEntity and AssetScheduleEntity over the 1:n relation they have, using the relation between the fields: AssetTypeCalendar.Id - AssetSchedule.AssetTypeCalendarId</summary>
		public virtual IEntityRelation AssetScheduleEntityUsingAssetTypeCalendarId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "AssetSchedules", true, new[] { AssetTypeCalendarFields.Id, AssetScheduleFields.AssetTypeCalendarId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between AssetTypeCalendarEntity and AssetTypeCalendarTaskEntity over the 1:n relation they have, using the relation between the fields: AssetTypeCalendar.Id - AssetTypeCalendarTask.AssetTypeCalendarId</summary>
		public virtual IEntityRelation AssetTypeCalendarTaskEntityUsingAssetTypeCalendarId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "AssetTypeCalendarTasks", true, new[] { AssetTypeCalendarFields.Id, AssetTypeCalendarTaskFields.AssetTypeCalendarId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between AssetTypeCalendarEntity and AssetTypeEntity over the m:1 relation they have, using the relation between the fields: AssetTypeCalendar.AssetTypeId - AssetType.Id</summary>
		public virtual IEntityRelation AssetTypeEntityUsingAssetTypeId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "AssetType", false, new[] { AssetTypeFields.Id, AssetTypeCalendarFields.AssetTypeId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticAssetTypeCalendarRelations
	{
		internal static readonly IEntityRelation AssetScheduleEntityUsingAssetTypeCalendarIdStatic = new AssetTypeCalendarRelations().AssetScheduleEntityUsingAssetTypeCalendarId;
		internal static readonly IEntityRelation AssetTypeCalendarTaskEntityUsingAssetTypeCalendarIdStatic = new AssetTypeCalendarRelations().AssetTypeCalendarTaskEntityUsingAssetTypeCalendarId;
		internal static readonly IEntityRelation AssetTypeEntityUsingAssetTypeIdStatic = new AssetTypeCalendarRelations().AssetTypeEntityUsingAssetTypeId;

		/// <summary>CTor</summary>
		static StaticAssetTypeCalendarRelations() { }
	}
}
