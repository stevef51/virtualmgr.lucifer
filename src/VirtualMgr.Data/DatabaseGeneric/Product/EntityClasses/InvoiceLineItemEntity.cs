﻿//////////////////////////////////////////////////////////////
// <auto-generated>This code was generated by LLBLGen Pro 5.5.</auto-generated>
//////////////////////////////////////////////////////////////
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Data.FactoryClasses;
using ConceptCave.Data.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace ConceptCave.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'InvoiceLineItem'.<br/><br/></summary>
	[Serializable]
	public partial class InvoiceLineItemEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		private InvoiceEntity _invoice;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static InvoiceLineItemEntityStaticMetaData _staticMetaData = new InvoiceLineItemEntityStaticMetaData();
		private static InvoiceLineItemRelations _relationsFactory = new InvoiceLineItemRelations();

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Invoice</summary>
			public static readonly string Invoice = "Invoice";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class InvoiceLineItemEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public InvoiceLineItemEntityStaticMetaData()
			{
				SetEntityCoreInfo("InvoiceLineItemEntity", InheritanceHierarchyType.None, false, (int)ConceptCave.Data.EntityType.InvoiceLineItemEntity, typeof(InvoiceLineItemEntity), typeof(InvoiceLineItemEntityFactory), false);
				AddNavigatorMetaData<InvoiceLineItemEntity, InvoiceEntity>("Invoice", "InvoiceLineItems", (a, b) => a._invoice = b, a => a._invoice, (a, b) => a.Invoice = b, ConceptCave.Data.RelationClasses.StaticInvoiceLineItemRelations.InvoiceEntityUsingInvoiceIdStatic, ()=>new InvoiceLineItemRelations().InvoiceEntityUsingInvoiceId, null, new int[] { (int)InvoiceLineItemFieldIndex.InvoiceId }, null, true, (int)ConceptCave.Data.EntityType.InvoiceEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static InvoiceLineItemEntity()
		{
		}

		/// <summary> CTor</summary>
		public InvoiceLineItemEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public InvoiceLineItemEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this InvoiceLineItemEntity</param>
		public InvoiceLineItemEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="id">PK value for InvoiceLineItem which data should be fetched into this InvoiceLineItem object</param>
		public InvoiceLineItemEntity(System.Int32 id) : this(id, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="id">PK value for InvoiceLineItem which data should be fetched into this InvoiceLineItem object</param>
		/// <param name="validator">The custom validator object for this InvoiceLineItemEntity</param>
		public InvoiceLineItemEntity(System.Int32 id, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.Id = id;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected InvoiceLineItemEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Invoice' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoInvoice() { return CreateRelationInfoForNavigator("Invoice"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this InvoiceLineItemEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static InvoiceLineItemRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Invoice' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathInvoice { get { return _staticMetaData.GetPrefetchPathElement("Invoice", CommonEntityBase.CreateEntityCollection<InvoiceEntity>()); } }

		/// <summary>The Description property of the Entity InvoiceLineItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblInvoiceLineItem"."Description".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 1024.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)InvoiceLineItemFieldIndex.Description, true); }
			set	{ SetValue((int)InvoiceLineItemFieldIndex.Description, value); }
		}

		/// <summary>The DetailLevel property of the Entity InvoiceLineItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblInvoiceLineItem"."DetailLevel".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DetailLevel
		{
			get { return (System.Int32)GetValue((int)InvoiceLineItemFieldIndex.DetailLevel, true); }
			set	{ SetValue((int)InvoiceLineItemFieldIndex.DetailLevel, value); }
		}

		/// <summary>The Id property of the Entity InvoiceLineItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblInvoiceLineItem"."Id".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 Id
		{
			get { return (System.Int32)GetValue((int)InvoiceLineItemFieldIndex.Id, true); }
			set { SetValue((int)InvoiceLineItemFieldIndex.Id, value); }		}

		/// <summary>The InvoiceId property of the Entity InvoiceLineItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblInvoiceLineItem"."InvoiceId".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 32.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String InvoiceId
		{
			get { return (System.String)GetValue((int)InvoiceLineItemFieldIndex.InvoiceId, true); }
			set	{ SetValue((int)InvoiceLineItemFieldIndex.InvoiceId, value); }
		}

		/// <summary>The Line property of the Entity InvoiceLineItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblInvoiceLineItem"."Line".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Line
		{
			get { return (System.String)GetValue((int)InvoiceLineItemFieldIndex.Line, true); }
			set	{ SetValue((int)InvoiceLineItemFieldIndex.Line, value); }
		}

		/// <summary>The ParentLine property of the Entity InvoiceLineItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblInvoiceLineItem"."ParentLine".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ParentLine
		{
			get { return (System.String)GetValue((int)InvoiceLineItemFieldIndex.ParentLine, true); }
			set	{ SetValue((int)InvoiceLineItemFieldIndex.ParentLine, value); }
		}

		/// <summary>The Quantity property of the Entity InvoiceLineItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblInvoiceLineItem"."Quantity".<br/>Table field type characteristics (type, precision, scale, length): Decimal, 19, 10, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal Quantity
		{
			get { return (System.Decimal)GetValue((int)InvoiceLineItemFieldIndex.Quantity, true); }
			set	{ SetValue((int)InvoiceLineItemFieldIndex.Quantity, value); }
		}

		/// <summary>The Total property of the Entity InvoiceLineItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblInvoiceLineItem"."Total".<br/>Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal Total
		{
			get { return (System.Decimal)GetValue((int)InvoiceLineItemFieldIndex.Total, true); }
			set	{ SetValue((int)InvoiceLineItemFieldIndex.Total, value); }
		}

		/// <summary>The UnitPrice property of the Entity InvoiceLineItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblInvoiceLineItem"."UnitPrice".<br/>Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal UnitPrice
		{
			get { return (System.Decimal)GetValue((int)InvoiceLineItemFieldIndex.UnitPrice, true); }
			set	{ SetValue((int)InvoiceLineItemFieldIndex.UnitPrice, value); }
		}

		/// <summary>The UserId property of the Entity InvoiceLineItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblInvoiceLineItem"."UserId".<br/>Table field type characteristics (type, precision, scale, length): UniqueIdentifier, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Guid> UserId
		{
			get { return (Nullable<System.Guid>)GetValue((int)InvoiceLineItemFieldIndex.UserId, false); }
			set	{ SetValue((int)InvoiceLineItemFieldIndex.UserId, value); }
		}

		/// <summary>Gets / sets related entity of type 'InvoiceEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual InvoiceEntity Invoice
		{
			get { return _invoice; }
			set { SetSingleRelatedEntityNavigator(value, "Invoice"); }
		}

		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END

	}
}

namespace ConceptCave.Data
{
	public enum InvoiceLineItemFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>DetailLevel. </summary>
		DetailLevel,
		///<summary>Id. </summary>
		Id,
		///<summary>InvoiceId. </summary>
		InvoiceId,
		///<summary>Line. </summary>
		Line,
		///<summary>ParentLine. </summary>
		ParentLine,
		///<summary>Quantity. </summary>
		Quantity,
		///<summary>Total. </summary>
		Total,
		///<summary>UnitPrice. </summary>
		UnitPrice,
		///<summary>UserId. </summary>
		UserId,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace ConceptCave.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: InvoiceLineItem. </summary>
	public partial class InvoiceLineItemRelations: RelationFactory
	{

		/// <summary>Returns a new IEntityRelation object, between InvoiceLineItemEntity and InvoiceEntity over the m:1 relation they have, using the relation between the fields: InvoiceLineItem.InvoiceId - Invoice.Id</summary>
		public virtual IEntityRelation InvoiceEntityUsingInvoiceId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Invoice", false, new[] { InvoiceFields.Id, InvoiceLineItemFields.InvoiceId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticInvoiceLineItemRelations
	{
		internal static readonly IEntityRelation InvoiceEntityUsingInvoiceIdStatic = new InvoiceLineItemRelations().InvoiceEntityUsingInvoiceId;

		/// <summary>CTor</summary>
		static StaticInvoiceLineItemRelations() { }
	}
}
