﻿//////////////////////////////////////////////////////////////
// <auto-generated>This code was generated by LLBLGen Pro 5.5.</auto-generated>
//////////////////////////////////////////////////////////////
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Data.FactoryClasses;
using ConceptCave.Data.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace ConceptCave.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'BeaconSighting'.<br/><br/></summary>
	[Serializable]
	public partial class BeaconSightingEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		private BeaconEntity _beacon;
		private TabletUuidEntity _tabletUuid_;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static BeaconSightingEntityStaticMetaData _staticMetaData = new BeaconSightingEntityStaticMetaData();
		private static BeaconSightingRelations _relationsFactory = new BeaconSightingRelations();

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Beacon</summary>
			public static readonly string Beacon = "Beacon";
			/// <summary>Member name TabletUuid_</summary>
			public static readonly string TabletUuid_ = "TabletUuid_";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class BeaconSightingEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public BeaconSightingEntityStaticMetaData()
			{
				SetEntityCoreInfo("BeaconSightingEntity", InheritanceHierarchyType.None, false, (int)ConceptCave.Data.EntityType.BeaconSightingEntity, typeof(BeaconSightingEntity), typeof(BeaconSightingEntityFactory), false);
				AddNavigatorMetaData<BeaconSightingEntity, BeaconEntity>("Beacon", "BeaconSightings", (a, b) => a._beacon = b, a => a._beacon, (a, b) => a.Beacon = b, ConceptCave.Data.RelationClasses.StaticBeaconSightingRelations.BeaconEntityUsingBeaconIdStatic, ()=>new BeaconSightingRelations().BeaconEntityUsingBeaconId, null, new int[] { (int)BeaconSightingFieldIndex.BeaconId }, null, true, (int)ConceptCave.Data.EntityType.BeaconEntity);
				AddNavigatorMetaData<BeaconSightingEntity, TabletUuidEntity>("TabletUuid_", "BeaconSightings", (a, b) => a._tabletUuid_ = b, a => a._tabletUuid_, (a, b) => a.TabletUuid_ = b, ConceptCave.Data.RelationClasses.StaticBeaconSightingRelations.TabletUuidEntityUsingTabletUuidStatic, ()=>new BeaconSightingRelations().TabletUuidEntityUsingTabletUuid, null, new int[] { (int)BeaconSightingFieldIndex.TabletUuid }, null, true, (int)ConceptCave.Data.EntityType.TabletUuidEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static BeaconSightingEntity()
		{
		}

		/// <summary> CTor</summary>
		public BeaconSightingEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public BeaconSightingEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this BeaconSightingEntity</param>
		public BeaconSightingEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="id">PK value for BeaconSighting which data should be fetched into this BeaconSighting object</param>
		public BeaconSightingEntity(System.Int64 id) : this(id, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="id">PK value for BeaconSighting which data should be fetched into this BeaconSighting object</param>
		/// <param name="validator">The custom validator object for this BeaconSightingEntity</param>
		public BeaconSightingEntity(System.Int64 id, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.Id = id;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected BeaconSightingEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Beacon' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoBeacon() { return CreateRelationInfoForNavigator("Beacon"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'TabletUuid' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoTabletUuid_() { return CreateRelationInfoForNavigator("TabletUuid_"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this BeaconSightingEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static BeaconSightingRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Beacon' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathBeacon { get { return _staticMetaData.GetPrefetchPathElement("Beacon", CommonEntityBase.CreateEntityCollection<BeaconEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'TabletUuid' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathTabletUuid_ { get { return _staticMetaData.GetPrefetchPathElement("TabletUuid_", CommonEntityBase.CreateEntityCollection<TabletUuidEntity>()); } }

		/// <summary>The BatteryPercent property of the Entity BeaconSighting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblBeaconSighting"."BatteryPercent".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> BatteryPercent
		{
			get { return (Nullable<System.Int32>)GetValue((int)BeaconSightingFieldIndex.BatteryPercent, false); }
			set	{ SetValue((int)BeaconSightingFieldIndex.BatteryPercent, value); }
		}

		/// <summary>The BeaconId property of the Entity BeaconSighting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblBeaconSighting"."BeaconId".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String BeaconId
		{
			get { return (System.String)GetValue((int)BeaconSightingFieldIndex.BeaconId, true); }
			set	{ SetValue((int)BeaconSightingFieldIndex.BeaconId, value); }
		}

		/// <summary>The BestRange property of the Entity BeaconSighting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblBeaconSighting"."BestRange".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 BestRange
		{
			get { return (System.Int32)GetValue((int)BeaconSightingFieldIndex.BestRange, true); }
			set	{ SetValue((int)BeaconSightingFieldIndex.BestRange, value); }
		}

		/// <summary>The BestRangeUtc property of the Entity BeaconSighting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblBeaconSighting"."BestRangeUtc".<br/>Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime BestRangeUtc
		{
			get { return (System.DateTime)GetValue((int)BeaconSightingFieldIndex.BestRangeUtc, true); }
			set	{ SetValue((int)BeaconSightingFieldIndex.BestRangeUtc, value); }
		}

		/// <summary>The FirstRangeUtc property of the Entity BeaconSighting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblBeaconSighting"."FirstRangeUtc".<br/>Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime FirstRangeUtc
		{
			get { return (System.DateTime)GetValue((int)BeaconSightingFieldIndex.FirstRangeUtc, true); }
			set	{ SetValue((int)BeaconSightingFieldIndex.FirstRangeUtc, value); }
		}

		/// <summary>The GoneUtc property of the Entity BeaconSighting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblBeaconSighting"."GoneUtc".<br/>Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> GoneUtc
		{
			get { return (Nullable<System.DateTime>)GetValue((int)BeaconSightingFieldIndex.GoneUtc, false); }
			set	{ SetValue((int)BeaconSightingFieldIndex.GoneUtc, value); }
		}

		/// <summary>The GpslocationId property of the Entity BeaconSighting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblBeaconSighting"."GPSLocationId".<br/>Table field type characteristics (type, precision, scale, length): BigInt, 19, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> GpslocationId
		{
			get { return (Nullable<System.Int64>)GetValue((int)BeaconSightingFieldIndex.GpslocationId, false); }
			set	{ SetValue((int)BeaconSightingFieldIndex.GpslocationId, value); }
		}

		/// <summary>The Id property of the Entity BeaconSighting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblBeaconSighting"."Id".<br/>Table field type characteristics (type, precision, scale, length): BigInt, 19, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 Id
		{
			get { return (System.Int64)GetValue((int)BeaconSightingFieldIndex.Id, true); }
			set { SetValue((int)BeaconSightingFieldIndex.Id, value); }		}

		/// <summary>The LoggedInUserId property of the Entity BeaconSighting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblBeaconSighting"."LoggedInUserId".<br/>Table field type characteristics (type, precision, scale, length): UniqueIdentifier, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Guid> LoggedInUserId
		{
			get { return (Nullable<System.Guid>)GetValue((int)BeaconSightingFieldIndex.LoggedInUserId, false); }
			set	{ SetValue((int)BeaconSightingFieldIndex.LoggedInUserId, value); }
		}

		/// <summary>The NextStaticBeaconSightingId property of the Entity BeaconSighting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblBeaconSighting"."NextStaticBeaconSightingId".<br/>Table field type characteristics (type, precision, scale, length): BigInt, 19, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> NextStaticBeaconSightingId
		{
			get { return (Nullable<System.Int64>)GetValue((int)BeaconSightingFieldIndex.NextStaticBeaconSightingId, false); }
			set	{ SetValue((int)BeaconSightingFieldIndex.NextStaticBeaconSightingId, value); }
		}

		/// <summary>The PrevStaticBeaconSightingId property of the Entity BeaconSighting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblBeaconSighting"."PrevStaticBeaconSightingId".<br/>Table field type characteristics (type, precision, scale, length): BigInt, 19, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> PrevStaticBeaconSightingId
		{
			get { return (Nullable<System.Int64>)GetValue((int)BeaconSightingFieldIndex.PrevStaticBeaconSightingId, false); }
			set	{ SetValue((int)BeaconSightingFieldIndex.PrevStaticBeaconSightingId, value); }
		}

		/// <summary>The TabletUuid property of the Entity BeaconSighting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblBeaconSighting"."TabletUUID".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 36.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String TabletUuid
		{
			get { return (System.String)GetValue((int)BeaconSightingFieldIndex.TabletUuid, true); }
			set	{ SetValue((int)BeaconSightingFieldIndex.TabletUuid, value); }
		}

		/// <summary>Gets / sets related entity of type 'BeaconEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual BeaconEntity Beacon
		{
			get { return _beacon; }
			set { SetSingleRelatedEntityNavigator(value, "Beacon"); }
		}

		/// <summary>Gets / sets related entity of type 'TabletUuidEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual TabletUuidEntity TabletUuid_
		{
			get { return _tabletUuid_; }
			set { SetSingleRelatedEntityNavigator(value, "TabletUuid_"); }
		}

		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END

	}
}

namespace ConceptCave.Data
{
	public enum BeaconSightingFieldIndex
	{
		///<summary>BatteryPercent. </summary>
		BatteryPercent,
		///<summary>BeaconId. </summary>
		BeaconId,
		///<summary>BestRange. </summary>
		BestRange,
		///<summary>BestRangeUtc. </summary>
		BestRangeUtc,
		///<summary>FirstRangeUtc. </summary>
		FirstRangeUtc,
		///<summary>GoneUtc. </summary>
		GoneUtc,
		///<summary>GpslocationId. </summary>
		GpslocationId,
		///<summary>Id. </summary>
		Id,
		///<summary>LoggedInUserId. </summary>
		LoggedInUserId,
		///<summary>NextStaticBeaconSightingId. </summary>
		NextStaticBeaconSightingId,
		///<summary>PrevStaticBeaconSightingId. </summary>
		PrevStaticBeaconSightingId,
		///<summary>TabletUuid. </summary>
		TabletUuid,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace ConceptCave.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: BeaconSighting. </summary>
	public partial class BeaconSightingRelations: RelationFactory
	{

		/// <summary>Returns a new IEntityRelation object, between BeaconSightingEntity and BeaconEntity over the m:1 relation they have, using the relation between the fields: BeaconSighting.BeaconId - Beacon.Id</summary>
		public virtual IEntityRelation BeaconEntityUsingBeaconId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Beacon", false, new[] { BeaconFields.Id, BeaconSightingFields.BeaconId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between BeaconSightingEntity and TabletUuidEntity over the m:1 relation they have, using the relation between the fields: BeaconSighting.TabletUuid - TabletUuid.Uuid</summary>
		public virtual IEntityRelation TabletUuidEntityUsingTabletUuid
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "TabletUuid_", false, new[] { TabletUuidFields.Uuid, BeaconSightingFields.TabletUuid }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticBeaconSightingRelations
	{
		internal static readonly IEntityRelation BeaconEntityUsingBeaconIdStatic = new BeaconSightingRelations().BeaconEntityUsingBeaconId;
		internal static readonly IEntityRelation TabletUuidEntityUsingTabletUuidStatic = new BeaconSightingRelations().TabletUuidEntityUsingTabletUuid;

		/// <summary>CTor</summary>
		static StaticBeaconSightingRelations() { }
	}
}
