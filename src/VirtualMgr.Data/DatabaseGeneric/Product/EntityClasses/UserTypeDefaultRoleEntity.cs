﻿//////////////////////////////////////////////////////////////
// <auto-generated>This code was generated by LLBLGen Pro 5.5.</auto-generated>
//////////////////////////////////////////////////////////////
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Data.FactoryClasses;
using ConceptCave.Data.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace ConceptCave.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'UserTypeDefaultRole'.<br/><br/></summary>
	[Serializable]
	public partial class UserTypeDefaultRoleEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		private AspNetRoleEntity _aspNetRole;
		private UserTypeEntity _userType;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static UserTypeDefaultRoleEntityStaticMetaData _staticMetaData = new UserTypeDefaultRoleEntityStaticMetaData();
		private static UserTypeDefaultRoleRelations _relationsFactory = new UserTypeDefaultRoleRelations();

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name AspNetRole</summary>
			public static readonly string AspNetRole = "AspNetRole";
			/// <summary>Member name UserType</summary>
			public static readonly string UserType = "UserType";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class UserTypeDefaultRoleEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public UserTypeDefaultRoleEntityStaticMetaData()
			{
				SetEntityCoreInfo("UserTypeDefaultRoleEntity", InheritanceHierarchyType.None, false, (int)ConceptCave.Data.EntityType.UserTypeDefaultRoleEntity, typeof(UserTypeDefaultRoleEntity), typeof(UserTypeDefaultRoleEntityFactory), false);
				AddNavigatorMetaData<UserTypeDefaultRoleEntity, AspNetRoleEntity>("AspNetRole", "UserTypeDefaultRoles", (a, b) => a._aspNetRole = b, a => a._aspNetRole, (a, b) => a.AspNetRole = b, ConceptCave.Data.RelationClasses.StaticUserTypeDefaultRoleRelations.AspNetRoleEntityUsingRoleIdStatic, ()=>new UserTypeDefaultRoleRelations().AspNetRoleEntityUsingRoleId, null, new int[] { (int)UserTypeDefaultRoleFieldIndex.RoleId }, null, true, (int)ConceptCave.Data.EntityType.AspNetRoleEntity);
				AddNavigatorMetaData<UserTypeDefaultRoleEntity, UserTypeEntity>("UserType", "UserTypeDefaultRoles", (a, b) => a._userType = b, a => a._userType, (a, b) => a.UserType = b, ConceptCave.Data.RelationClasses.StaticUserTypeDefaultRoleRelations.UserTypeEntityUsingUserTypeIdStatic, ()=>new UserTypeDefaultRoleRelations().UserTypeEntityUsingUserTypeId, null, new int[] { (int)UserTypeDefaultRoleFieldIndex.UserTypeId }, null, true, (int)ConceptCave.Data.EntityType.UserTypeEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static UserTypeDefaultRoleEntity()
		{
		}

		/// <summary> CTor</summary>
		public UserTypeDefaultRoleEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public UserTypeDefaultRoleEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this UserTypeDefaultRoleEntity</param>
		public UserTypeDefaultRoleEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="roleId">PK value for UserTypeDefaultRole which data should be fetched into this UserTypeDefaultRole object</param>
		/// <param name="userTypeId">PK value for UserTypeDefaultRole which data should be fetched into this UserTypeDefaultRole object</param>
		public UserTypeDefaultRoleEntity(System.String roleId, System.Int32 userTypeId) : this(roleId, userTypeId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="roleId">PK value for UserTypeDefaultRole which data should be fetched into this UserTypeDefaultRole object</param>
		/// <param name="userTypeId">PK value for UserTypeDefaultRole which data should be fetched into this UserTypeDefaultRole object</param>
		/// <param name="validator">The custom validator object for this UserTypeDefaultRoleEntity</param>
		public UserTypeDefaultRoleEntity(System.String roleId, System.Int32 userTypeId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.RoleId = roleId;
			this.UserTypeId = userTypeId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected UserTypeDefaultRoleEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'AspNetRole' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoAspNetRole() { return CreateRelationInfoForNavigator("AspNetRole"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'UserType' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoUserType() { return CreateRelationInfoForNavigator("UserType"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this UserTypeDefaultRoleEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static UserTypeDefaultRoleRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'AspNetRole' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathAspNetRole { get { return _staticMetaData.GetPrefetchPathElement("AspNetRole", CommonEntityBase.CreateEntityCollection<AspNetRoleEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'UserType' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathUserType { get { return _staticMetaData.GetPrefetchPathElement("UserType", CommonEntityBase.CreateEntityCollection<UserTypeEntity>()); } }

		/// <summary>The RoleId property of the Entity UserTypeDefaultRole<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblUserTypeDefaultRole"."RoleId".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 128.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.String RoleId
		{
			get { return (System.String)GetValue((int)UserTypeDefaultRoleFieldIndex.RoleId, true); }
			set	{ SetValue((int)UserTypeDefaultRoleFieldIndex.RoleId, value); }
		}

		/// <summary>The UserTypeId property of the Entity UserTypeDefaultRole<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblUserTypeDefaultRole"."UserTypeId".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int32 UserTypeId
		{
			get { return (System.Int32)GetValue((int)UserTypeDefaultRoleFieldIndex.UserTypeId, true); }
			set	{ SetValue((int)UserTypeDefaultRoleFieldIndex.UserTypeId, value); }
		}

		/// <summary>Gets / sets related entity of type 'AspNetRoleEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual AspNetRoleEntity AspNetRole
		{
			get { return _aspNetRole; }
			set { SetSingleRelatedEntityNavigator(value, "AspNetRole"); }
		}

		/// <summary>Gets / sets related entity of type 'UserTypeEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual UserTypeEntity UserType
		{
			get { return _userType; }
			set { SetSingleRelatedEntityNavigator(value, "UserType"); }
		}

		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END

	}
}

namespace ConceptCave.Data
{
	public enum UserTypeDefaultRoleFieldIndex
	{
		///<summary>RoleId. </summary>
		RoleId,
		///<summary>UserTypeId. </summary>
		UserTypeId,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace ConceptCave.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: UserTypeDefaultRole. </summary>
	public partial class UserTypeDefaultRoleRelations: RelationFactory
	{

		/// <summary>Returns a new IEntityRelation object, between UserTypeDefaultRoleEntity and AspNetRoleEntity over the m:1 relation they have, using the relation between the fields: UserTypeDefaultRole.RoleId - AspNetRole.Id</summary>
		public virtual IEntityRelation AspNetRoleEntityUsingRoleId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "AspNetRole", false, new[] { AspNetRoleFields.Id, UserTypeDefaultRoleFields.RoleId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between UserTypeDefaultRoleEntity and UserTypeEntity over the m:1 relation they have, using the relation between the fields: UserTypeDefaultRole.UserTypeId - UserType.Id</summary>
		public virtual IEntityRelation UserTypeEntityUsingUserTypeId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "UserType", false, new[] { UserTypeFields.Id, UserTypeDefaultRoleFields.UserTypeId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticUserTypeDefaultRoleRelations
	{
		internal static readonly IEntityRelation AspNetRoleEntityUsingRoleIdStatic = new UserTypeDefaultRoleRelations().AspNetRoleEntityUsingRoleId;
		internal static readonly IEntityRelation UserTypeEntityUsingUserTypeIdStatic = new UserTypeDefaultRoleRelations().UserTypeEntityUsingUserTypeId;

		/// <summary>CTor</summary>
		static StaticUserTypeDefaultRoleRelations() { }
	}
}
