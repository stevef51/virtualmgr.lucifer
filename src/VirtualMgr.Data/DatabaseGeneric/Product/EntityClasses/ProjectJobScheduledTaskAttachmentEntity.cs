﻿//////////////////////////////////////////////////////////////
// <auto-generated>This code was generated by LLBLGen Pro 5.5.</auto-generated>
//////////////////////////////////////////////////////////////
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Data.FactoryClasses;
using ConceptCave.Data.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace ConceptCave.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'ProjectJobScheduledTaskAttachment'.<br/><br/></summary>
	[Serializable]
	public partial class ProjectJobScheduledTaskAttachmentEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		private ProjectJobScheduledTaskEntity _projectJobScheduledTask;
		private MediaEntity _medium;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static ProjectJobScheduledTaskAttachmentEntityStaticMetaData _staticMetaData = new ProjectJobScheduledTaskAttachmentEntityStaticMetaData();
		private static ProjectJobScheduledTaskAttachmentRelations _relationsFactory = new ProjectJobScheduledTaskAttachmentRelations();

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ProjectJobScheduledTask</summary>
			public static readonly string ProjectJobScheduledTask = "ProjectJobScheduledTask";
			/// <summary>Member name Medium</summary>
			public static readonly string Medium = "Medium";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class ProjectJobScheduledTaskAttachmentEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public ProjectJobScheduledTaskAttachmentEntityStaticMetaData()
			{
				SetEntityCoreInfo("ProjectJobScheduledTaskAttachmentEntity", InheritanceHierarchyType.None, false, (int)ConceptCave.Data.EntityType.ProjectJobScheduledTaskAttachmentEntity, typeof(ProjectJobScheduledTaskAttachmentEntity), typeof(ProjectJobScheduledTaskAttachmentEntityFactory), false);
				AddNavigatorMetaData<ProjectJobScheduledTaskAttachmentEntity, ProjectJobScheduledTaskEntity>("ProjectJobScheduledTask", "ProjectJobScheduledTaskAttachments", (a, b) => a._projectJobScheduledTask = b, a => a._projectJobScheduledTask, (a, b) => a.ProjectJobScheduledTask = b, ConceptCave.Data.RelationClasses.StaticProjectJobScheduledTaskAttachmentRelations.ProjectJobScheduledTaskEntityUsingProjectJobScheduledTaskIdStatic, ()=>new ProjectJobScheduledTaskAttachmentRelations().ProjectJobScheduledTaskEntityUsingProjectJobScheduledTaskId, null, new int[] { (int)ProjectJobScheduledTaskAttachmentFieldIndex.ProjectJobScheduledTaskId }, null, true, (int)ConceptCave.Data.EntityType.ProjectJobScheduledTaskEntity);
				AddNavigatorMetaData<ProjectJobScheduledTaskAttachmentEntity, MediaEntity>("Medium", "ProjectJobScheduledTaskAttachments", (a, b) => a._medium = b, a => a._medium, (a, b) => a.Medium = b, ConceptCave.Data.RelationClasses.StaticProjectJobScheduledTaskAttachmentRelations.MediaEntityUsingMediaIdStatic, ()=>new ProjectJobScheduledTaskAttachmentRelations().MediaEntityUsingMediaId, null, new int[] { (int)ProjectJobScheduledTaskAttachmentFieldIndex.MediaId }, null, true, (int)ConceptCave.Data.EntityType.MediaEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static ProjectJobScheduledTaskAttachmentEntity()
		{
		}

		/// <summary> CTor</summary>
		public ProjectJobScheduledTaskAttachmentEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public ProjectJobScheduledTaskAttachmentEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this ProjectJobScheduledTaskAttachmentEntity</param>
		public ProjectJobScheduledTaskAttachmentEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="mediaId">PK value for ProjectJobScheduledTaskAttachment which data should be fetched into this ProjectJobScheduledTaskAttachment object</param>
		/// <param name="projectJobScheduledTaskId">PK value for ProjectJobScheduledTaskAttachment which data should be fetched into this ProjectJobScheduledTaskAttachment object</param>
		public ProjectJobScheduledTaskAttachmentEntity(System.Guid mediaId, System.Guid projectJobScheduledTaskId) : this(mediaId, projectJobScheduledTaskId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="mediaId">PK value for ProjectJobScheduledTaskAttachment which data should be fetched into this ProjectJobScheduledTaskAttachment object</param>
		/// <param name="projectJobScheduledTaskId">PK value for ProjectJobScheduledTaskAttachment which data should be fetched into this ProjectJobScheduledTaskAttachment object</param>
		/// <param name="validator">The custom validator object for this ProjectJobScheduledTaskAttachmentEntity</param>
		public ProjectJobScheduledTaskAttachmentEntity(System.Guid mediaId, System.Guid projectJobScheduledTaskId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.MediaId = mediaId;
			this.ProjectJobScheduledTaskId = projectJobScheduledTaskId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ProjectJobScheduledTaskAttachmentEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'ProjectJobScheduledTask' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoProjectJobScheduledTask() { return CreateRelationInfoForNavigator("ProjectJobScheduledTask"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Media' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoMedium() { return CreateRelationInfoForNavigator("Medium"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this ProjectJobScheduledTaskAttachmentEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static ProjectJobScheduledTaskAttachmentRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ProjectJobScheduledTask' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathProjectJobScheduledTask { get { return _staticMetaData.GetPrefetchPathElement("ProjectJobScheduledTask", CommonEntityBase.CreateEntityCollection<ProjectJobScheduledTaskEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Media' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathMedium { get { return _staticMetaData.GetPrefetchPathElement("Medium", CommonEntityBase.CreateEntityCollection<MediaEntity>()); } }

		/// <summary>The MediaId property of the Entity ProjectJobScheduledTaskAttachment<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblProjectJobScheduledTaskAttachment"."MediaId".<br/>Table field type characteristics (type, precision, scale, length): UniqueIdentifier, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Guid MediaId
		{
			get { return (System.Guid)GetValue((int)ProjectJobScheduledTaskAttachmentFieldIndex.MediaId, true); }
			set	{ SetValue((int)ProjectJobScheduledTaskAttachmentFieldIndex.MediaId, value); }
		}

		/// <summary>The ProjectJobScheduledTaskId property of the Entity ProjectJobScheduledTaskAttachment<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblProjectJobScheduledTaskAttachment"."ProjectJobScheduledTaskId".<br/>Table field type characteristics (type, precision, scale, length): UniqueIdentifier, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Guid ProjectJobScheduledTaskId
		{
			get { return (System.Guid)GetValue((int)ProjectJobScheduledTaskAttachmentFieldIndex.ProjectJobScheduledTaskId, true); }
			set	{ SetValue((int)ProjectJobScheduledTaskAttachmentFieldIndex.ProjectJobScheduledTaskId, value); }
		}

		/// <summary>Gets / sets related entity of type 'ProjectJobScheduledTaskEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual ProjectJobScheduledTaskEntity ProjectJobScheduledTask
		{
			get { return _projectJobScheduledTask; }
			set { SetSingleRelatedEntityNavigator(value, "ProjectJobScheduledTask"); }
		}

		/// <summary>Gets / sets related entity of type 'MediaEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual MediaEntity Medium
		{
			get { return _medium; }
			set { SetSingleRelatedEntityNavigator(value, "Medium"); }
		}

		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END

	}
}

namespace ConceptCave.Data
{
	public enum ProjectJobScheduledTaskAttachmentFieldIndex
	{
		///<summary>MediaId. </summary>
		MediaId,
		///<summary>ProjectJobScheduledTaskId. </summary>
		ProjectJobScheduledTaskId,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace ConceptCave.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ProjectJobScheduledTaskAttachment. </summary>
	public partial class ProjectJobScheduledTaskAttachmentRelations: RelationFactory
	{

		/// <summary>Returns a new IEntityRelation object, between ProjectJobScheduledTaskAttachmentEntity and ProjectJobScheduledTaskEntity over the m:1 relation they have, using the relation between the fields: ProjectJobScheduledTaskAttachment.ProjectJobScheduledTaskId - ProjectJobScheduledTask.Id</summary>
		public virtual IEntityRelation ProjectJobScheduledTaskEntityUsingProjectJobScheduledTaskId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "ProjectJobScheduledTask", false, new[] { ProjectJobScheduledTaskFields.Id, ProjectJobScheduledTaskAttachmentFields.ProjectJobScheduledTaskId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ProjectJobScheduledTaskAttachmentEntity and MediaEntity over the m:1 relation they have, using the relation between the fields: ProjectJobScheduledTaskAttachment.MediaId - Media.Id</summary>
		public virtual IEntityRelation MediaEntityUsingMediaId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Medium", false, new[] { MediaFields.Id, ProjectJobScheduledTaskAttachmentFields.MediaId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticProjectJobScheduledTaskAttachmentRelations
	{
		internal static readonly IEntityRelation ProjectJobScheduledTaskEntityUsingProjectJobScheduledTaskIdStatic = new ProjectJobScheduledTaskAttachmentRelations().ProjectJobScheduledTaskEntityUsingProjectJobScheduledTaskId;
		internal static readonly IEntityRelation MediaEntityUsingMediaIdStatic = new ProjectJobScheduledTaskAttachmentRelations().MediaEntityUsingMediaId;

		/// <summary>CTor</summary>
		static StaticProjectJobScheduledTaskAttachmentRelations() { }
	}
}
