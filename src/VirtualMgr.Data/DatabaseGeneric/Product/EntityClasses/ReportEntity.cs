﻿//////////////////////////////////////////////////////////////
// <auto-generated>This code was generated by LLBLGen Pro 5.5.</auto-generated>
//////////////////////////////////////////////////////////////
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Data.FactoryClasses;
using ConceptCave.Data.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace ConceptCave.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'Report'.<br/><br/></summary>
	[Serializable]
	public partial class ReportEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		private EntityCollection<ReportRoleEntity> _reportRoles;
		private EntityCollection<ReportTicketEntity> _reportTickets;
		private ReportDataEntity _reportData;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static ReportEntityStaticMetaData _staticMetaData = new ReportEntityStaticMetaData();
		private static ReportRelations _relationsFactory = new ReportRelations();

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ReportRoles</summary>
			public static readonly string ReportRoles = "ReportRoles";
			/// <summary>Member name ReportTickets</summary>
			public static readonly string ReportTickets = "ReportTickets";
			/// <summary>Member name ReportData</summary>
			public static readonly string ReportData = "ReportData";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class ReportEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public ReportEntityStaticMetaData()
			{
				SetEntityCoreInfo("ReportEntity", InheritanceHierarchyType.None, false, (int)ConceptCave.Data.EntityType.ReportEntity, typeof(ReportEntity), typeof(ReportEntityFactory), false);
				AddNavigatorMetaData<ReportEntity, EntityCollection<ReportRoleEntity>>("ReportRoles", a => a._reportRoles, (a, b) => a._reportRoles = b, a => a.ReportRoles, () => new ReportRelations().ReportRoleEntityUsingReportId, typeof(ReportRoleEntity), (int)ConceptCave.Data.EntityType.ReportRoleEntity);
				AddNavigatorMetaData<ReportEntity, EntityCollection<ReportTicketEntity>>("ReportTickets", a => a._reportTickets, (a, b) => a._reportTickets = b, a => a.ReportTickets, () => new ReportRelations().ReportTicketEntityUsingReportId, typeof(ReportTicketEntity), (int)ConceptCave.Data.EntityType.ReportTicketEntity);
				AddNavigatorMetaData<ReportEntity, ReportDataEntity>("ReportData", "Report", (a, b) => a._reportData = b, a => a._reportData, (a, b) => a.ReportData = b, ConceptCave.Data.RelationClasses.StaticReportRelations.ReportDataEntityUsingReportIdStatic, ()=>new ReportRelations().ReportDataEntityUsingReportId, null, null, null, true, (int)ConceptCave.Data.EntityType.ReportDataEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static ReportEntity()
		{
		}

		/// <summary> CTor</summary>
		public ReportEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public ReportEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this ReportEntity</param>
		public ReportEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="id">PK value for Report which data should be fetched into this Report object</param>
		public ReportEntity(System.Int32 id) : this(id, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="id">PK value for Report which data should be fetched into this Report object</param>
		/// <param name="validator">The custom validator object for this ReportEntity</param>
		public ReportEntity(System.Int32 id, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.Id = id;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ReportEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'ReportRole' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoReportRoles() { return CreateRelationInfoForNavigator("ReportRoles"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'ReportTicket' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoReportTickets() { return CreateRelationInfoForNavigator("ReportTickets"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'ReportData' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoReportData() { return CreateRelationInfoForNavigator("ReportData"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this ReportEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static ReportRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ReportRole' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathReportRoles { get { return _staticMetaData.GetPrefetchPathElement("ReportRoles", CommonEntityBase.CreateEntityCollection<ReportRoleEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ReportTicket' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathReportTickets { get { return _staticMetaData.GetPrefetchPathElement("ReportTickets", CommonEntityBase.CreateEntityCollection<ReportTicketEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ReportData' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathReportData { get { return _staticMetaData.GetPrefetchPathElement("ReportData", CommonEntityBase.CreateEntityCollection<ReportDataEntity>()); } }

		/// <summary>The Configuration property of the Entity Report<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblReport"."Description".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Configuration
		{
			get { return (System.String)GetValue((int)ReportFieldIndex.Configuration, true); }
			set	{ SetValue((int)ReportFieldIndex.Configuration, value); }
		}

		/// <summary>The Configuration_ property of the Entity Report<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblReport"."Configuration".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Configuration_
		{
			get { return (System.String)GetValue((int)ReportFieldIndex.Configuration_, true); }
			set	{ SetValue((int)ReportFieldIndex.Configuration_, value); }
		}

		/// <summary>The Description property of the Entity Report<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblReport"."Description".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647.<br/>Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)ReportFieldIndex.Description, true); }
			set	{ SetValue((int)ReportFieldIndex.Description, value); }
		}

		/// <summary>The Id property of the Entity Report<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblReport"."Id".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 Id
		{
			get { return (System.Int32)GetValue((int)ReportFieldIndex.Id, true); }
			set	{ SetValue((int)ReportFieldIndex.Id, value); }
		}

		/// <summary>The Name property of the Entity Report<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblReport"."Name".<br/>Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)ReportFieldIndex.Name, true); }
			set	{ SetValue((int)ReportFieldIndex.Name, value); }
		}

		/// <summary>The Type property of the Entity Report<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblReport"."Type".<br/>Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Type
		{
			get { return (System.Int32)GetValue((int)ReportFieldIndex.Type, true); }
			set	{ SetValue((int)ReportFieldIndex.Type, value); }
		}

		/// <summary>Gets the EntityCollection with the related entities of type 'ReportRoleEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(ReportRoleEntity))]
		public virtual EntityCollection<ReportRoleEntity> ReportRoles { get { return GetOrCreateEntityCollection<ReportRoleEntity, ReportRoleEntityFactory>("Report", true, false, ref _reportRoles); } }

		/// <summary>Gets the EntityCollection with the related entities of type 'ReportTicketEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(ReportTicketEntity))]
		public virtual EntityCollection<ReportTicketEntity> ReportTickets { get { return GetOrCreateEntityCollection<ReportTicketEntity, ReportTicketEntityFactory>("Report", true, false, ref _reportTickets); } }

		/// <summary>Gets / sets related entity of type 'ReportDataEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned.<br/><br/></summary>
		[Browsable(false)]
		public virtual ReportDataEntity ReportData
		{
			get { return _reportData; }
			set { SetSingleRelatedEntityNavigator(value, "ReportData"); }
		}

		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END

	}
}

namespace ConceptCave.Data
{
	public enum ReportFieldIndex
	{
		///<summary>Configuration. </summary>
		Configuration,
		///<summary>Configuration_. </summary>
		Configuration_,
		///<summary>Description. </summary>
		Description,
		///<summary>Id. </summary>
		Id,
		///<summary>Name. </summary>
		Name,
		///<summary>Type. </summary>
		Type,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace ConceptCave.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Report. </summary>
	public partial class ReportRelations: RelationFactory
	{
		/// <summary>Returns a new IEntityRelation object, between ReportEntity and ReportRoleEntity over the 1:n relation they have, using the relation between the fields: Report.Id - ReportRole.ReportId</summary>
		public virtual IEntityRelation ReportRoleEntityUsingReportId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "ReportRoles", true, new[] { ReportFields.Id, ReportRoleFields.ReportId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ReportEntity and ReportTicketEntity over the 1:n relation they have, using the relation between the fields: Report.Id - ReportTicket.ReportId</summary>
		public virtual IEntityRelation ReportTicketEntityUsingReportId
		{
			get { return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToMany, "ReportTickets", true, new[] { ReportFields.Id, ReportTicketFields.ReportId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between ReportEntity and ReportDataEntity over the 1:1 relation they have, using the relation between the fields: Report.Id - ReportData.ReportId</summary>
		public virtual IEntityRelation ReportDataEntityUsingReportId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.OneToOne, "ReportData", true, new[] { ReportFields.Id, ReportDataFields.ReportId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticReportRelations
	{
		internal static readonly IEntityRelation ReportRoleEntityUsingReportIdStatic = new ReportRelations().ReportRoleEntityUsingReportId;
		internal static readonly IEntityRelation ReportTicketEntityUsingReportIdStatic = new ReportRelations().ReportTicketEntityUsingReportId;
		internal static readonly IEntityRelation ReportDataEntityUsingReportIdStatic = new ReportRelations().ReportDataEntityUsingReportId;

		/// <summary>CTor</summary>
		static StaticReportRelations() { }
	}
}
