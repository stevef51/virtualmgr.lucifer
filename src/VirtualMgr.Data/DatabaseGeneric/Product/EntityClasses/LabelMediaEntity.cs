﻿//////////////////////////////////////////////////////////////
// <auto-generated>This code was generated by LLBLGen Pro 5.5.</auto-generated>
//////////////////////////////////////////////////////////////
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Data.FactoryClasses;
using ConceptCave.Data.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace ConceptCave.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'LabelMedia'.<br/><br/></summary>
	[Serializable]
	public partial class LabelMediaEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		private LabelEntity _label;
		private MediaEntity _medium;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static LabelMediaEntityStaticMetaData _staticMetaData = new LabelMediaEntityStaticMetaData();
		private static LabelMediaRelations _relationsFactory = new LabelMediaRelations();

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Label</summary>
			public static readonly string Label = "Label";
			/// <summary>Member name Medium</summary>
			public static readonly string Medium = "Medium";
		}

		/// <summary>Static meta-data storage for navigator related information</summary>
		protected class LabelMediaEntityStaticMetaData : EntityStaticMetaDataBase
		{
			public LabelMediaEntityStaticMetaData()
			{
				SetEntityCoreInfo("LabelMediaEntity", InheritanceHierarchyType.None, false, (int)ConceptCave.Data.EntityType.LabelMediaEntity, typeof(LabelMediaEntity), typeof(LabelMediaEntityFactory), false);
				AddNavigatorMetaData<LabelMediaEntity, LabelEntity>("Label", "LabelMedias", (a, b) => a._label = b, a => a._label, (a, b) => a.Label = b, ConceptCave.Data.RelationClasses.StaticLabelMediaRelations.LabelEntityUsingLabelIdStatic, ()=>new LabelMediaRelations().LabelEntityUsingLabelId, null, new int[] { (int)LabelMediaFieldIndex.LabelId }, null, true, (int)ConceptCave.Data.EntityType.LabelEntity);
				AddNavigatorMetaData<LabelMediaEntity, MediaEntity>("Medium", "LabelMedias", (a, b) => a._medium = b, a => a._medium, (a, b) => a.Medium = b, ConceptCave.Data.RelationClasses.StaticLabelMediaRelations.MediaEntityUsingMediaIdStatic, ()=>new LabelMediaRelations().MediaEntityUsingMediaId, null, new int[] { (int)LabelMediaFieldIndex.MediaId }, null, true, (int)ConceptCave.Data.EntityType.MediaEntity);
			}
		}

		/// <summary>Static ctor</summary>
		static LabelMediaEntity()
		{
		}

		/// <summary> CTor</summary>
		public LabelMediaEntity()
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public LabelMediaEntity(IEntityFields2 fields)
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this LabelMediaEntity</param>
		public LabelMediaEntity(IValidator validator)
		{
			InitClassEmpty(validator, null);
		}

		/// <summary> CTor</summary>
		/// <param name="labelId">PK value for LabelMedia which data should be fetched into this LabelMedia object</param>
		/// <param name="mediaId">PK value for LabelMedia which data should be fetched into this LabelMedia object</param>
		public LabelMediaEntity(System.Guid labelId, System.Guid mediaId) : this(labelId, mediaId, null)
		{
		}

		/// <summary> CTor</summary>
		/// <param name="labelId">PK value for LabelMedia which data should be fetched into this LabelMedia object</param>
		/// <param name="mediaId">PK value for LabelMedia which data should be fetched into this LabelMedia object</param>
		/// <param name="validator">The custom validator object for this LabelMediaEntity</param>
		public LabelMediaEntity(System.Guid labelId, System.Guid mediaId, IValidator validator)
		{
			InitClassEmpty(validator, null);
			this.LabelId = labelId;
			this.MediaId = mediaId;
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected LabelMediaEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Label' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoLabel() { return CreateRelationInfoForNavigator("Label"); }

		/// <summary>Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Media' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoMedium() { return CreateRelationInfoForNavigator("Medium"); }
		
		/// <inheritdoc/>
		protected override EntityStaticMetaDataBase GetEntityStaticMetaData() {	return _staticMetaData; }

		/// <summary>Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		/// <summary>Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this LabelMediaEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary>The relations object holding all relations of this entity with other entity classes.</summary>
		public static LabelMediaRelations Relations { get { return _relationsFactory; } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Label' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathLabel { get { return _staticMetaData.GetPrefetchPathElement("Label", CommonEntityBase.CreateEntityCollection<LabelEntity>()); } }

		/// <summary>Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Media' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathMedium { get { return _staticMetaData.GetPrefetchPathElement("Medium", CommonEntityBase.CreateEntityCollection<MediaEntity>()); } }

		/// <summary>The LabelId property of the Entity LabelMedia<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblLabelMedia"."LabelId".<br/>Table field type characteristics (type, precision, scale, length): UniqueIdentifier, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Guid LabelId
		{
			get { return (System.Guid)GetValue((int)LabelMediaFieldIndex.LabelId, true); }
			set	{ SetValue((int)LabelMediaFieldIndex.LabelId, value); }
		}

		/// <summary>The MediaId property of the Entity LabelMedia<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblLabelMedia"."MediaId".<br/>Table field type characteristics (type, precision, scale, length): UniqueIdentifier, 0, 0, 0.<br/>Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Guid MediaId
		{
			get { return (System.Guid)GetValue((int)LabelMediaFieldIndex.MediaId, true); }
			set	{ SetValue((int)LabelMediaFieldIndex.MediaId, value); }
		}

		/// <summary>Gets / sets related entity of type 'LabelEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual LabelEntity Label
		{
			get { return _label; }
			set { SetSingleRelatedEntityNavigator(value, "Label"); }
		}

		/// <summary>Gets / sets related entity of type 'MediaEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual MediaEntity Medium
		{
			get { return _medium; }
			set { SetSingleRelatedEntityNavigator(value, "Medium"); }
		}

		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END

	}
}

namespace ConceptCave.Data
{
	public enum LabelMediaFieldIndex
	{
		///<summary>LabelId. </summary>
		LabelId,
		///<summary>MediaId. </summary>
		MediaId,
		/// <summary></summary>
		AmountOfFields
	}
}

namespace ConceptCave.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: LabelMedia. </summary>
	public partial class LabelMediaRelations: RelationFactory
	{

		/// <summary>Returns a new IEntityRelation object, between LabelMediaEntity and LabelEntity over the m:1 relation they have, using the relation between the fields: LabelMedia.LabelId - Label.Id</summary>
		public virtual IEntityRelation LabelEntityUsingLabelId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Label", false, new[] { LabelFields.Id, LabelMediaFields.LabelId }); }
		}

		/// <summary>Returns a new IEntityRelation object, between LabelMediaEntity and MediaEntity over the m:1 relation they have, using the relation between the fields: LabelMedia.MediaId - Media.Id</summary>
		public virtual IEntityRelation MediaEntityUsingMediaId
		{
			get	{ return ModelInfoProviderSingleton.GetInstance().CreateRelation(RelationType.ManyToOne, "Medium", false, new[] { MediaFields.Id, LabelMediaFields.MediaId }); }
		}

	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticLabelMediaRelations
	{
		internal static readonly IEntityRelation LabelEntityUsingLabelIdStatic = new LabelMediaRelations().LabelEntityUsingLabelId;
		internal static readonly IEntityRelation MediaEntityUsingMediaIdStatic = new LabelMediaRelations().MediaEntityUsingMediaId;

		/// <summary>CTor</summary>
		static StaticLabelMediaRelations() { }
	}
}
