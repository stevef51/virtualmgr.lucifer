using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.Data.EntityClasses
{
    public partial class ProjectJobTaskEntity
    {
        public string MinimumDurationExpression { get; set; }

        /// <summary>
        /// Takes the MinimumDurationExpression and calculates the MinimumDuration based on the ExpectedDuration
        /// </summary>
        public void CalculateMinimumDuration()
        {
            if (string.IsNullOrEmpty(MinimumDurationExpression) == true)
            {
                MinimumDuration = null;
                return;
            }

            decimal val = 0;
            // so the MinimumDurationExpression is one of two formats XX% or XX where XX is in seconds
            if (MinimumDurationExpression.IndexOf("%") == -1)
            {

                if (decimal.TryParse(MinimumDurationExpression, out val) == true)
                {
                    MinimumDuration = val;
                }
            }
            else
            {
                if (this.EstimatedDurationSeconds.HasValue == false)
                {
                    MinimumDuration = null;
                    return;
                }

                if (decimal.TryParse(MinimumDurationExpression.Replace("%", ""), out val) == true)
                {
                    MinimumDuration = this.EstimatedDurationSeconds.Value * (val / 100);
                }
            }
        }
    }
}
