#!/bin/bash
echo Building...

# We have to move our .dockerignore file into /src below since docker only looks specifically for .dockerignore
cp .dockerignore ..
docker build -t lucifer_auth:latest -t virtualmgr.azurecr.io/lucifer_auth:latest -f ./Dockerfile ../
rm ../.dockerignore