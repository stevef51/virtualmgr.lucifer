﻿// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.


using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using VirtualMgr.Membership.DirectCore.IdentityOverrides;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection.Extensions;
using VirtualMgr.Membership.DirectCore;
using Microsoft.AspNetCore.Http;
using VirtualMgr.Identity.Server.Stores;
using Lamar;
using VirtualMgr.MultiTenant;
using VirtualMgr.AspNetCore;
using VirtualMgr.Identity.Server.Extensions;
using Serilog;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.Extensions.Logging;
using IdentityServer4.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Caching.StackExchangeRedis;
using ServiceStack.Redis;

namespace VirtualMgr.Identity.Server
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public IHostingEnvironment Environment { get; }

        public Startup(
            IHostingEnvironment environment,
            IConfiguration configuration)
        {
            Environment = environment;
            Configuration = configuration;
        }

        public void ConfigureContainer(ServiceRegistry services)
        {
            services.Configure<ServiceOptions>(Configuration.GetSection("Service"));
            services.Configure<ServiceOptions>(options => options.ServiceName = options.Services.Auth);

            var tempProvider = services.BuildServiceProvider();
            var serviceOptions = tempProvider.GetRequiredService<IOptions<ServiceOptions>>().Value;


            services.AddLogging();

            services.AddDbContext<ApplicationDbContext>();

            services.
                AddIdentity<IdentityUser, IdentityRole>().
                AddEntityFrameworkStores<ApplicationDbContext>().
                AddDefaultTokenProviders();
            services.Configure<PasswordHasherOptions>(options => options.CompatibilityMode = PasswordHasherCompatibilityMode.IdentityV2);

            services.Replace(new ServiceDescriptor(
                                typeof(IPasswordHasher<IdentityUser>),
                                typeof(SqlPasswordHasher),
                                ServiceLifetime.Scoped));

            services.AddTransient<IUserStore<IdentityUser>, ApplicationUserStore>();
            services.AddTransient<IRoleStore<IdentityRole>, ApplicationRoleStore>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.Configure<ForwardedHeadersOptions>(options =>
            {
                options.ForwardedHeaders =
                    ForwardedHeaders.XForwardedProto | ForwardedHeaders.XForwardedHost;
            });

            // uncomment, if you wan to add an MVC-based UI
            services.AddMvc().SetCompatibilityVersion(Microsoft.AspNetCore.Mvc.CompatibilityVersion.Version_2_1);


            var builder = services.AddIdentityServer(Configuration.GetSection("IdentityServer"))
                .AddInMemoryIdentityResources(Config.GetIdentityResources())
                .AddInMemoryApiResources(Config.GetApis())
                .AddClientStore<TenantClientStore>()
                //.AddInMemoryClients(Config.GetClients())
                .AddAspNetIdentity<IdentityUser>();

            services.AddModule(new VirtualMgr.Redis.InjectionModule(serviceOptions.Services.Redis));

            // Resolve tenant's domain 1st from "VM-TenantDomain" Header and then from Request Host
            services.AddSingleton<ITenantDomainResolver, ListTenantDomainResolver>(ctx =>
            {
                var identityServerOptions = ctx.GetRequiredService<IdentityServer4.Configuration.IdentityServerOptions>();
                var list = new ListTenantDomainResolver();
                //list.Resolvers.Add(new CustomHeaderTenantDomainResolver(identityServerOptions.IssuerUri, serviceOptions.TenantDomainHeader));
                list.Resolvers.Add(new RequestHostTenantDomainResolver());
                return list;
            });

            services.AddVirtualMgrAppTenant(Configuration);
            services.AddModule(new VirtualMgr.MassTransit.InjectionModule(Configuration, serviceOptions.ServiceName));

            services.AddLLBLGenRuntimeConfiguration(System.Diagnostics.TraceLevel.Error);
            services.AddVirtualMgrMultiTenantLLBLGen(Configuration);

            if (Environment.IsDevelopment())
            {
                builder.AddDeveloperSigningCredential();
            }
            else
            {
                builder.AddCertificateFromFile(Configuration.GetSection("SigninKeyCredentials"), Log.Logger);
            }
        }

        public void Configure(IApplicationBuilder app, IHttpContextAccessor httpContext, IOptions<ServiceOptions> serviceOptions)
        {
            foreach (var nvp in Configuration.AsEnumerable())
            {
                Log.Logger.Information($"{nvp.Key}={nvp.Value}");
            }

            app.UsePathBase($"/{serviceOptions.Value.ServiceName}");

            app.UseForwardedHeaders();
            app.UseVirtualMgrAppTenant(false);

            if (Environment.IsDevelopment())
            {
                app.LogRequestHeaders(app.ApplicationServices.GetService<ILoggerFactory>());
                app.UseDeveloperExceptionPage();
            }

            app.UseMiddleware<Diagnostics.SerilogMiddleware>();
            app.UseMiddleware<VirtualMgr.AspNetCore.Cors.CorsMiddleware>();

            app.UseStaticFiles();

            app.UseIdentityServer();

            // uncomment, if you wan to add an MVC-based UI
            app.UseMvcWithDefaultRoute();
        }
    }
}