﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IdentityServer4;
using IdentityServer4.Models;
using IdentityServer4.Stores;
using VirtualMgr.MultiTenant;
using Microsoft.Extensions.Options;
using VirtualMgr.AspNetCore;

namespace VirtualMgr.Identity.Server.Stores
{
    public class TenantClientStore : IClientStore
    {
        private readonly ServiceOptions _serviceOptions;
        private readonly AppTenant _appTenant;
        public TenantClientStore(AppTenant appTenant, IOptions<ServiceOptions> serviceOptions)
        {
            _appTenant = appTenant;
            _serviceOptions = serviceOptions.Value;
        }

        public Task<Client> FindClientByIdAsync(string clientId)
        {
            var client = new Client
            {
                ClientId = clientId,
                ClientName = _appTenant.PrimaryHostname,
                AllowedGrantTypes = GrantTypes.Hybrid.Concat(GrantTypes.ResourceOwnerPassword).ToList(),
                RequireConsent = false,

                ClientSecrets =
                {
                    new Secret("FAEB1AC0-3649-4EE4-BCDD-BDE1758BDFA1".Sha256())
                },

                // where to redirect to after login
                RedirectUris = { $"{_serviceOptions.IdentityServer.Scheme}{clientId}/signin-oidc" },

                // where to redirect to after logout
                PostLogoutRedirectUris = { $"{_serviceOptions.IdentityServer.Scheme}{clientId}/signout-callback-oidc" },

                AllowedScopes = new List<string>
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        "VirtualMgr.Api.Lingo"
                    },
                AllowOfflineAccess = true,

                RefreshTokenUsage = TokenUsage.ReUse,
            };

            return Task.FromResult(client);
        }
    }
}
