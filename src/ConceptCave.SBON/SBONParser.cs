//
//  Author:
//    Steve Fillingham steve.fillingham@bworkflow.com
//
//  Copyright (c) 2013, SquareIT Software PTY LTD
//
//
//
using System;
using System.IO;

namespace ConceptCave.SBON
{
	public class SBONParser
	{
		private SBONReader _reader;
		private int _depth;

		public SBONParser (Stream stream)
		{
			_reader = new SBONReader (stream);
		}

		public void Read(ISBONDelegate del)
		{
			ISBONDelegate root = del;
			do
			{
				try
				{
					Code c = _reader.ReadCode ();
					switch (c)
					{
						case Code.Bool:
							del = del.WriteBool (_reader.ReadBool ());
							break;
						case Code.ByteArray:
						{	
							Action<int, byte[]> fnBytes = null;
							int length = _reader.ReadByteArrayLength ();
							int chunkSize = length;
							del = del.WriteByteArray (length, ref chunkSize, ref fnBytes);
							for (int i = 0; del != null && i < length; i += chunkSize)
							{
								byte[] chunkData = _reader.ReadByteArrayData (i + chunkSize > length ? length - i : chunkSize);
								if (fnBytes != null)
									fnBytes (i, chunkData);
							}
							break;
						}
						case Code.DateTime:
							del = del.WriteDateTime (_reader.ReadDateTime ());
							break;
						case Code.Decimal:
							del = del.WriteDecimal (_reader.ReadDecimal ());
							break;
						case Code.Double:
							del = del.WriteDouble (_reader.ReadDouble ());
							break;
						case Code.EndArray:
							_depth--;
							_reader.ReadEndArray ();
							del = del.WriteEndArray ();
							break;
						case Code.EndObject:
							_depth--;
							_reader.ReadEndObject ();
							del = del.WriteEndObject ();
							break;
						case Code.Float:
							del = del.WriteFloat (_reader.ReadFloat ());
							break;
						case Code.Guid:
							del = del.WriteGuid (_reader.ReadGuid ());
							break;
						case Code.Int16:
							del = del.WriteInt16 (_reader.ReadInt16 ());
							break;
						case Code.Int32:
							del = del.WriteInt32 (_reader.ReadInt32 ());
							break;
						case Code.Int64:
							del = del.WriteInt64 (_reader.ReadInt64 ());
							break;
						case Code.Int8:
							del = del.WriteInt8 (_reader.ReadInt8 ());
							break;
						case Code.Name:
							del = del.WriteName (_reader.ReadName ());
							break;
						case Code.Null:
							_reader.ReadNull ();
							del = del.WriteNull ();
							break;
                        case Code.DBNull:                            
                            _reader.ReadDBNull();
                            del = del.WriteDBNull();
                            break;
						case Code.StartArray:
							_depth++;
							_reader.ReadStartArray ();
							del = del.WriteStartArray ();
							break;
						case Code.StartObject:
							_depth++;
							_reader.ReadStartObject ();
							del = del.WriteStartObject ();
							break;
						case Code.String:
							del = del.WriteString (_reader.ReadString ());
							break;
						case Code.UInt16:
							del = del.WriteUInt16 (_reader.ReadUInt16 ());
							break;
						case Code.UInt32:
							del = del.WriteUInt32 (_reader.ReadUInt32 ());
							break;
						case Code.UInt64:
							del = del.WriteUInt64 (_reader.ReadUInt64 ());
							break;
						case Code.UInt8:
							del = del.WriteUInt8 (_reader.ReadUInt8 ());
							break;
						case Code.Char:
							del = del.WriteChar(_reader.ReadChar());
							break;
						case Code.TimeSpan:
							del = del.WriteTimeSpan(_reader.ReadTimeSpan());
							break;
						case Code.StartAttribute:
							_depth++;
							del = del.WriteStartAttribute();
							break;
						case Code.EndAttribute:
							_depth--;
							del = del.WriteEndAttribute();
							break;
						default:
							throw new SBONException (string.Format ("Unexpected SBON Code {0}", c));
					}
				}
				catch(Exception ex)
				{
					if (!root.CatchException(ex))
						throw;
				}
			} while (del != null && _depth > 0);
		}
	}
}

