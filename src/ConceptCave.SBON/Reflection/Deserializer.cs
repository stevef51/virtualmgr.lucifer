//
//  Author:
//    Steve Fillingham steve.fillingham@bworkflow.com
//
//  Copyright (c) 2013, SquareIT Software PTY LTD
//
//
//
using System;
using System.IO;
using System.Collections.Generic;
using System.Collections;

namespace ConceptCave.SBON.Reflection
{
	public class Deserializer : RootDelegate
	{
		private Type _t;
		private object _o;
		private SBONParser _parser;
		public SBONParser Parser { get { return _parser; } }

		public Deserializer (Stream stream)
		{
			_parser = new SBONParser (stream);
		}

        public T Deserialize<T>(T t)
        {
            _o = t;
            _t = typeof(T);
            _parser.Read(this);
            return t;
        }

		public T Deserialize<T>()
		{
            return Deserialize((T)Activator.CreateInstance<T>());
		}

        public T[] DeserializeArray<T>()
        {
            var list = new List<T>();
            _o = list;
            _t = typeof(T);
            _parser.Read(this);
            return list.ToArray();
        }

		public override ISBONDelegate WriteStartObject()
		{
			return new ReflectedObjectDelegate (this, _o);
		}

		public override ISBONDelegate WriteStartArray()
		{
			return new ReflectedArrayDelegate (this, (IList)_o, _t);
		}
	}
}

