//
//  Author:
//    Steve Fillingham steve.fillingham@bworkflow.com
//
//  Copyright (c) 2013, SquareIT Software PTY LTD
//
//
//
using System;
using System.Reflection;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace ConceptCave.SBON.Reflection
{
	public class SerializerSettings
	{
		public bool IncludeNullValueProperties { get; set; }
	}

	public class Serializer
	{
		private static Dictionary<Type, Action<SBONWriter, object>> _valueWriters = new Dictionary<Type, Action<SBONWriter, object>>();

		public SerializerSettings Settings { get; set; }
		public SBONWriter Writer { get; private set; }

	    static Serializer()
		{
			_valueWriters [typeof(string)] = (w, o) => w.WriteString ((string)o);
			_valueWriters [typeof(bool)] = (w, o) => w.WriteBool ((bool)o);
			_valueWriters [typeof(byte[])] = (w, o) => w.WriteByteArray ((byte[])o);
			_valueWriters [typeof(DateTime)] = (w, o) => w.WriteDateTime ((DateTime)o);
			_valueWriters [typeof(decimal)] = (w, o) => w.WriteDecimal ((decimal)o);
			_valueWriters [typeof(double)] = (w, o) => w.WriteDouble ((double)o);
			_valueWriters [typeof(float)] = (w, o) => w.WriteFloat ((float)o);
			_valueWriters [typeof(Guid)] = (w, o) => w.WriteGuid ((Guid)o);
			_valueWriters [typeof(short)] = (w, o) => w.WriteInt16 ((short)o);
			_valueWriters [typeof(int)] = (w, o) => w.WriteInt32 ((int)o);
			_valueWriters [typeof(long)] = (w, o) => w.WriteInt64 ((long)o);
			_valueWriters [typeof(sbyte)] = (w, o) => w.WriteInt8 ((sbyte)o);
			_valueWriters [typeof(ushort)] = (w, o) => w.WriteUInt16 ((ushort)o);
			_valueWriters [typeof(uint)] = (w, o) => w.WriteUInt32 ((uint)o);
			_valueWriters [typeof(ulong)] = (w, o) => w.WriteUInt64 ((ulong)o);
			_valueWriters [typeof(byte)] = (w, o) => w.WriteUInt8 ((byte)o);
			_valueWriters [typeof(DBNull)] = (w, o) => w.WriteDBNull ();
			_valueWriters [typeof(char)] = (w, o) => w.WriteChar ((char)o);
			_valueWriters [typeof(TimeSpan)] = (w, o) => w.WriteTimeSpan ((TimeSpan)o);
		}

		public Serializer(SBONWriter writer)
		{
			Settings = new SerializerSettings ();
			Writer = writer;
		}

		public Serializer (Stream stream) : this(new SBONWriter(stream))
		{
		}

		public static bool TrySerializePrimitive(object o, SBONWriter writer)
		{
			if (o == null)
			{
				writer.WriteNull ();
				return true;
			}
			/*			else if (o == DBNull.Value)
			{
				writer.WriteDBNull ();
				return true;
			}*/
			else
			{
				Type t = o.GetType ();
				Action<SBONWriter,object> fnWriter = null;
				if (_valueWriters.TryGetValue (t, out fnWriter))
				{
					fnWriter (writer, o);
					return true;
				}
				if (t.IsEnum)
				{
					ulong l = Convert.ToUInt64 (o);
					if (l > 0xffffffff)
						writer.WriteUInt64 (l);
					else if (l > 0xffff)
						writer.WriteUInt32 ((uint)l);
					else
						writer.WriteUInt8 ((byte)l);
					return true;
				}
			}

			return false;
		}

		public void Serialize(object o)
		{
			if (!TrySerializePrimitive(o, Writer))
			{
				var dictionary = o as IDictionary;
				if (dictionary != null)
				{
					Writer.WriteStartObject ();
					var e = dictionary.GetEnumerator ();
					while (e.MoveNext())
					{
						Writer.WriteName (e.Key.ToString ());
						Serialize (e.Value);
					}
					Writer.WriteEndObject ();
				}
				else
				{
					var enumerable = o as IEnumerable;
					if (enumerable != null)
					{
						Writer.WriteStartArray ();
						foreach (var e in enumerable)
							Serialize (e);
						Writer.WriteEndArray ();
					}
					else
					{
						var writeable = o as ISBONWriteable;
						if (writeable != null)
							writeable.WriteTo (Writer, otherObject => Serialize(otherObject));
						else
							SerializeObject (o);
					}
				}
			}
		}

		private static Dictionary<Type, List<Tuple<PropertyInfo, string>>> _sbonAttributeCache = new Dictionary<Type, List<Tuple<PropertyInfo, string>>>();

		void SerializeObject(object o)
		{
			// We will cache the Ordered List of properties to SBON Write along with their optionally defined SBON name (otherwise Property name)
			Type t = o.GetType ();
			List<Tuple<PropertyInfo, string>> orderedProps = null;
			if (!_sbonAttributeCache.TryGetValue (t, out orderedProps))
			{
				lock (_sbonAttributeCache)
				{
					if (!_sbonAttributeCache.TryGetValue (t, out orderedProps))
					{
						orderedProps = new List<Tuple<PropertyInfo, string>> ();
						foreach (var pi in t.GetProperties()) //.OrderBy(pi => { var a = pi.GetCustomAttribute<SBONAttribute>(); return a == null ? 0 : a.Order; }))
						{
							var nameAttrib = pi.GetCustomAttribute<SBONAttribute> (true);
							if (nameAttrib != null)
							{
								if (nameAttrib.Omit)
									continue;
								orderedProps.Add (new Tuple<PropertyInfo, string> (pi, nameAttrib.Name));
							}
							else
								orderedProps.Add (new Tuple<PropertyInfo, string> (pi, pi.Name));
						}
						_sbonAttributeCache [t] = orderedProps;
					}
				}
			}

			Writer.WriteStartObject ();
			foreach (var tuple in orderedProps)
			{
				object po = tuple.Item1.GetValue(o);
				if (po != null || Settings.IncludeNullValueProperties)
				{
					Writer.WriteName (tuple.Item2);

					Serialize (po);
				}
			}
			Writer.WriteEndObject ();
		}
	}
}

