//
//  Author:
//    Steve Fillingham steve.fillingham@bworkflow.com
//
//  Copyright (c) 2013, SquareIT Software PTY LTD
//
//
//
using System;
using System.Collections.Generic;
using System.Collections;

namespace ConceptCave.SBON.Reflection
{
	public class ReflectedArrayDelegate : ValueDelegate
	{
		private IList _list;
		private Type _type;

		public ReflectedArrayDelegate (ISBONDelegate parent, IList list, Type type) : base(parent)
		{
			_list = list;
			_type = type;
		}

		protected override ISBONDelegate WriteValue(object v)
		{
			_list.Add (v);
			return this;
		}

		public override ISBONDelegate WriteStartObject()
		{
			object o = Activator.CreateInstance (_type);
			return new ReflectedObjectDelegate (this, o) {
				FnEndObject = () => 
				{
					_list.Add(o);
					return this;
				},
				ThrowOnNameNotFound = ThrowOnNameNotFound
			};
		}

		public override ISBONDelegate WriteEndArray()
		{
			if (FnEndArray != null)
				return FnEndArray ();

			return Parent;
		}

		public override ISBONDelegate WriteStartArray()
		{
			if (_type.IsArray)
			{
				Type type = _type.GetElementType ();
				IList list = (IList)Activator.CreateInstance (typeof(List<>).MakeGenericType (type));
				if (type.IsArray)
				{
					return new ReflectedArrayDelegate (this, list, type) {
						FnEndArray = () =>
						{
							Array array = Array.CreateInstance(type, list.Count);
							list.CopyTo(array, 0);
							_list.Add(array);
							return this;
						},
						ThrowOnNameNotFound = ThrowOnNameNotFound
					};
				}
			}
			throw new SBONException (string.Format("Type {0} is not an Array or IEnumerable", _type.Name));
		}
	}
}

