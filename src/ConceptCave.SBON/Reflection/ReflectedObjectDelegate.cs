//
//  Author:
//    Steve Fillingham steve.fillingham@bworkflow.com
//
//  Copyright (c) 2013, SquareIT Software PTY LTD
//
//
//
using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using ConceptCave.SBON.Collection;

namespace ConceptCave.SBON.Reflection
{
	public class ReflectedObjectDelegate : ValueDelegate
	{
		private Type _t;
		private object _o;
		private Dictionary<string, PropertyInfo> _propertyDict;

		public ReflectedObjectDelegate (ISBONDelegate parent, object o) : base(parent)
		{
			_o = o;
            _t = o.GetType();

            // If its a Dictionary then we dont want to use Property Reflection, just deserialize direct into the dictionary
            if (!(typeof(IDictionary).IsAssignableFrom(_t) && _t.IsGenericType))
            {
                // Its not a Dictionary, so cache its Properties so we can deserialize via reflection
                _propertyDict = new Dictionary<string, PropertyInfo>();
                foreach (var pi in _t.GetProperties())
                {
                    SBONAttribute nameAttrib = pi.GetCustomAttribute<SBONAttribute>(true);
                    if (nameAttrib == null)
                        _propertyDict.Add(pi.Name, pi);

                    else if (nameAttrib.Name != null)
                        _propertyDict.Add(nameAttrib.Name, pi);
                }
            }
		}

		#region ISBONDelegate implementation
		protected override ISBONDelegate WriteValue(object v)
		{
			throw new SBONException (string.Format ("Reflected {0} cannot WriteValue without a Property Name", _t.Name));
		}

		public override ISBONDelegate WriteName(string name)
		{
			PropertyInfo pi = null;
			if (_propertyDict == null)
			{
				// Dictionary must be of Dictionary<string, X> 
				Type[] genArgs = _t.GetGenericArguments ();
				if (genArgs [0] != typeof(string))
					throw new SBONException (string.Format ("Cannot Reflect Dictionary<{0},{1}> must be Dictionary<string,>", genArgs [0].Name, genArgs [1].Name));

				IDictionary dict = (IDictionary)_o;

				return new ReflectedValueDelegate (this, genArgs [1], v => dict [name] = v) { ThrowOnNameNotFound = ThrowOnNameNotFound };
            }
            else if (!_propertyDict.TryGetValue(name, out pi))
            {
				if (ThrowOnNameNotFound)
					throw new SBONException (string.Format ("Reflected Type {0} does not have Property {1}", _t.Name, name));

				// Ok, name not found (and no Exception thrown) use a VoidDelegate to swallow the upcoming Value
				return new VoidDelegate (this, this, this, this);
			}

			return new ReflectedValueDelegate (this, pi.PropertyType, v => 
			{
				object cv = v;
				if (v != null)
				{
					Type t = pi.PropertyType;
					if (t.IsGenericType && t.GetGenericTypeDefinition() == typeof(Nullable<>))
					{
                        if (v != null)
                        {
                            var typeParam = t.GetGenericArguments()[0];
                            if (typeParam.IsEnum)
                            {
                                var theEnum = Enum.ToObject(typeParam, v);
                                cv = Activator.CreateInstance(t, new object[] { theEnum });
                            }
                            else
                            {
                                cv = Activator.CreateInstance(t, new object[] { v });
                            }
                        }
							
					}
					else if (t.IsEnum)
						cv = Convert.ToInt32(v);
					else
						cv = Convert.ChangeType(v, t);
				}				

				pi.SetValue(_o, cv);
			}) { ThrowOnNameNotFound = ThrowOnNameNotFound };;
		}

		public override ISBONDelegate WriteEndObject()
		{
			if (FnEndObject != null)
				return FnEndObject ();
			return Parent;
		}
		#endregion
	}
}

