//
//  Author:
//    Steve Fillingham steve.fillingham@bworkflow.com
//
//  Copyright (c) 2013, SquareIT Software PTY LTD
//
//
//
using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Collections;

namespace ConceptCave.SBON.Reflection
{
	public class ReflectedValueDelegate : ValueDelegate
	{
		private Type _valueType;

		public ReflectedValueDelegate (ISBONDelegate parent, Type valueType, Action<object> fnValue) : base(parent)
		{
			_valueType = valueType;
			FnValue = fnValue;
		}

		protected override ISBONDelegate WriteValue(object v)
		{
			FnValue (v);
			return Parent;
		}

		public override ISBONDelegate WriteStartObject()
		{
			object propertyObject = Activator.CreateInstance (_valueType);
			return new ReflectedObjectDelegate (Parent, propertyObject) 
			{ 
				FnEndObject = () => 
				{
					FnValue(propertyObject);
					return Parent;
				},
				ThrowOnNameNotFound = ThrowOnNameNotFound
			};
		}

		public override ISBONDelegate WriteStartArray()
		{
			if (_valueType.IsArray)
			{
				Type elementType = _valueType.GetElementType ();
				IList list = (IList)Activator.CreateInstance (typeof(List<>).MakeGenericType (elementType));
				return new ReflectedArrayDelegate (Parent, list, elementType) {
					FnEndArray = () =>
					{
						Array array = Array.CreateInstance(elementType, list.Count);
						list.CopyTo(array, 0);
						FnValue(array);
						return Parent;
					},
					ThrowOnNameNotFound = ThrowOnNameNotFound
				};
			}
			else if (_valueType.IsGenericType && typeof(IEnumerable).IsAssignableFrom(_valueType))
			{
				Type elementType = _valueType.GetGenericArguments () [0];
				IList list = (IList)Activator.CreateInstance (typeof(List<>).MakeGenericType (elementType));
				return new ReflectedArrayDelegate (Parent, list, elementType) {
					FnEndArray = () =>
					{
						object genList = Activator.CreateInstance(_valueType, list);
						FnValue(genList);
						return Parent;
					},
					ThrowOnNameNotFound = ThrowOnNameNotFound
				};
			}

			throw new SBONException (string.Format("Type {0} is not an Array or IEnumerable", _valueType.Name));
		}
	}
}

