//
//  Author:
//    Steve Fillingham steve.fillingham@bworkflow.com
//
//  Copyright (c) 2013, SquareIT Software PTY LTD
//
//
//
using System;

namespace ConceptCave.SBON
{
	// Anything written into here is thrown to void
	public class VoidDelegate : ISBONDelegate
	{
		public ISBONDelegate ObjectParent { get; private set; }
		public ISBONDelegate ArrayParent { get; private set; }
		public ISBONDelegate ValueParent { get; private set; }
		public ISBONDelegate AttributeParent { get; private set; }

		public VoidDelegate (ISBONDelegate objectParent, ISBONDelegate arrayParent, ISBONDelegate valueParent, ISBONDelegate attributeParent)
		{
			ObjectParent = objectParent;
			ArrayParent = arrayParent;
			ValueParent = valueParent;
			AttributeParent = attributeParent;
		}

		#region ISBONDelegate implementation
		public ISBONDelegate WriteStartObject()
		{
			return new VoidDelegate (this, this, null, this);
		}
		public ISBONDelegate WriteEndObject()
		{
			return ObjectParent ?? this;
		}
		public ISBONDelegate WriteStartArray()
		{
			return new VoidDelegate (this, this, null, this);
		}
		public ISBONDelegate WriteEndArray()
		{
			return ArrayParent ?? this;
		}
		public ISBONDelegate WriteStartAttribute()
		{
			return new VoidDelegate (this, this, null, this);
		}
		public ISBONDelegate WriteEndAttribute()
		{
			return AttributeParent ?? this;
		}
		public ISBONDelegate WriteName(string name)
		{
			return new VoidDelegate (this, this, this, this);
		}
		public ISBONDelegate WriteBool(bool v)
		{
			return ValueParent ?? this;
		}
		public ISBONDelegate WriteInt8(sbyte v)
		{
			return ValueParent ?? this;
		}
		public ISBONDelegate WriteInt16(short v)
		{
			return ValueParent ?? this;
		}
		public ISBONDelegate WriteInt32(int v)
		{
			return ValueParent ?? this;
		}
		public ISBONDelegate WriteInt64(long v)
		{
			return ValueParent ?? this;
		}
		public ISBONDelegate WriteUInt8(byte v)
		{
			return ValueParent ?? this;
		}
		public ISBONDelegate WriteUInt16(ushort v)
		{
			return ValueParent ?? this;
		}
		public ISBONDelegate WriteUInt32(uint v)
		{
			return ValueParent ?? this;
		}
		public ISBONDelegate WriteUInt64(ulong v)
		{
			return ValueParent ?? this;
		}
		public ISBONDelegate WriteDecimal(decimal v)
		{
			return ValueParent ?? this;
		}
		public ISBONDelegate WriteString(string v)
		{
			return ValueParent ?? this;
		}
		public ISBONDelegate WriteDateTime(DateTime v)
		{
			return ValueParent ?? this;
		}
        public ISBONDelegate WriteNull()
        {
            return ValueParent ?? this;
        }
        public ISBONDelegate WriteDBNull()
        {
            return ValueParent ?? this;
        }
        public ISBONDelegate WriteGuid(Guid v)
		{
			return ValueParent ?? this;
		}
		public ISBONDelegate WriteFloat(float v)
		{
			return ValueParent ?? this;
		}
		public ISBONDelegate WriteDouble(double v)
		{
			return ValueParent ?? this;
		}
		public ISBONDelegate WriteByteArray(int length, ref int chunkSize, ref Action<int, byte[]> fnBytes)
		{
			return ValueParent ?? this;
		}
		public ISBONDelegate WriteChar(char v)
		{
			return ValueParent ?? this;
		}
		public ISBONDelegate WriteTimeSpan(TimeSpan v)
		{
			return ValueParent ?? this;
		}
		public bool CatchException(Exception ex)
		{
			return false;
		}
		#endregion
	}
}

