//
//  Author:
//    Steve Fillingham steve.fillingham@bworkflow.com
//
//  Copyright (c) 2013, SquareIT Software PTY LTD
//
//
//
using System;

namespace ConceptCave.SBON
{
	public class RootDelegate : ISBONDelegate
	{
		public ISBONDelegate Parent { get; private set; }

		public RootDelegate () : this(null)
		{
		}

		public RootDelegate(ISBONDelegate parent)
		{
			Parent = parent;
		}

		#region ISBONDelegate implementation
		public virtual ISBONDelegate WriteStartObject()
		{
			throw new SBONException (string.Format ("{0} cannot WriteStartObject", this.GetType ().Name));
		}
		public virtual ISBONDelegate WriteEndObject()
		{
			throw new SBONException (string.Format ("{0} cannot WriteEndObject", this.GetType ().Name));
		}
		public virtual ISBONDelegate WriteStartArray()
		{
			throw new SBONException (string.Format ("{0} cannot WriteStartArray", this.GetType ().Name));
		}
		public virtual ISBONDelegate WriteEndArray()
		{
			throw new SBONException (string.Format ("{0} cannot WriteEndArray", this.GetType ().Name));
		}
		public virtual ISBONDelegate WriteName(string name)
		{
			throw new SBONException (string.Format ("{0} cannot WriteName", this.GetType ().Name));
		}
		public virtual ISBONDelegate WriteBool(bool v)
		{
			throw new SBONException (string.Format ("{0} cannot WriteBool", this.GetType ().Name));
		}
		public virtual ISBONDelegate WriteInt8(sbyte v)
		{
			throw new SBONException (string.Format ("{0} cannot WriteInt8", this.GetType ().Name));
		}
		public virtual ISBONDelegate WriteInt16(short v)
		{
			throw new SBONException (string.Format ("{0} cannot WriteInt16", this.GetType ().Name));
		}
		public virtual ISBONDelegate WriteInt32(int v)
		{
			throw new SBONException (string.Format ("{0} cannot WriteInt32", this.GetType ().Name));
		}
		public virtual ISBONDelegate WriteInt64(long v)
		{
			throw new SBONException (string.Format ("{0} cannot WriteInt64", this.GetType ().Name));
		}
		public virtual ISBONDelegate WriteUInt8(byte v)
		{
			throw new SBONException (string.Format ("{0} cannot WriteUInt8", this.GetType ().Name));
		}
		public virtual ISBONDelegate WriteUInt16(ushort v)
		{
			throw new SBONException (string.Format ("{0} cannot WriteUInt16", this.GetType ().Name));
		}
		public virtual ISBONDelegate WriteUInt32(uint v)
		{
			throw new SBONException (string.Format ("{0} cannot WriteUInt32", this.GetType ().Name));
		}
		public virtual ISBONDelegate WriteUInt64(ulong v)
		{
			throw new SBONException (string.Format ("{0} cannot WriteUInt64", this.GetType ().Name));
		}
		public virtual ISBONDelegate WriteDecimal(decimal v)
		{
			throw new SBONException (string.Format ("{0} cannot WriteDecimal", this.GetType ().Name));
		}
		public virtual ISBONDelegate WriteString(string v)
		{
			throw new SBONException (string.Format ("{0} cannot WriteString", this.GetType ().Name));
		}
		public virtual ISBONDelegate WriteDateTime(DateTime v)
		{
			throw new SBONException (string.Format ("{0} cannot WriteDateTime", this.GetType ().Name));
		}
		public virtual ISBONDelegate WriteNull()
		{
			throw new SBONException (string.Format ("{0} cannot WriteNull", this.GetType ().Name));
		}
        public virtual ISBONDelegate WriteDBNull()
        {
            throw new SBONException(string.Format("{0} cannot WriteDBNull", this.GetType().Name));
        }
		public virtual ISBONDelegate WriteGuid(Guid v)
		{
			throw new SBONException (string.Format ("{0} cannot WriteGuid", this.GetType ().Name));
		}
		public virtual ISBONDelegate WriteFloat(float v)
		{
			throw new SBONException (string.Format ("{0} cannot WriteFloat", this.GetType ().Name));
		}
		public virtual ISBONDelegate WriteDouble(double v)
		{
			throw new SBONException (string.Format ("{0} cannot WriteDouble", this.GetType ().Name));
		}
		public virtual ISBONDelegate WriteByteArray(int length, ref int chunkSize, ref Action<int, byte[]> fnBytes)
		{
			throw new SBONException (string.Format ("{0} cannot WriteByteArray", this.GetType ().Name));
		}
		public virtual ISBONDelegate WriteChar(char v)
		{
			throw new SBONException (string.Format ("{0} cannot WriteChar", this.GetType ().Name));
		}
		public virtual ISBONDelegate WriteTimeSpan(TimeSpan v)
		{
			throw new SBONException (string.Format ("{0} cannot WriteTimeSpan", this.GetType ().Name));
		}
		public virtual ISBONDelegate WriteStartAttribute()
		{
			throw new SBONException (string.Format ("{0} cannot WriteStartAttribute", this.GetType ().Name));
		}
		public virtual ISBONDelegate WriteEndAttribute()
		{
			throw new SBONException (string.Format ("{0} cannot WriteEndAttribute", this.GetType ().Name));
		}
		public virtual bool CatchException(Exception ex)
		{
			return false;
		}
		#endregion
	}
}

