﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ConceptCave.SBON
{
    public class SBONJSONDelegate : ISBONDelegate
    {
        public Action<string> Fn;
        private int _tab;

		public static void Parse(Stream stream, Action<string> fn)
		{
			var p = new SBONParser (stream);
			p.Read (new SBONJSONDelegate () { Fn = fn });
		}

        private void Emit(string s)
        {
            if (Fn != null)
                Fn(s);
        }

        public ISBONDelegate WriteStartObject()
        {
            Emit(new string(' ', _tab++) + "{\n");
            return this;
        }

        public ISBONDelegate WriteEndObject()
        {
            Emit(new string(' ', --_tab) + "}\n");
            return this;
        }

        public ISBONDelegate WriteStartArray()
        {
            Emit(new string(' ', _tab++) + "[\n");
            return this;
        }

        public ISBONDelegate WriteEndArray()
        {
            Emit(new string(' ', --_tab) + "]\n");
            return this;
        }

        public ISBONDelegate WriteName(string name)
        {
            Emit(new string(' ', _tab) + name + " = ");
            return this;
        }

        public ISBONDelegate WriteBool(bool v)
        {
            Emit(new string(' ', _tab) + "(bool)" + v.ToString() + "\n");
            return this;
        }

        public ISBONDelegate WriteInt8(sbyte v)
        {
            Emit(new string(' ', _tab) + "(int8)" + v.ToString() + "\n");
            return this;
        }

        public ISBONDelegate WriteInt16(short v)
        {
            Emit(new string(' ', _tab) + "(int16)" + v.ToString() + "\n");
            return this;
        }

        public ISBONDelegate WriteInt32(int v)
        {
            Emit(new string(' ', _tab) + "(int32)" + v.ToString() + "\n");
            return this;
        }

        public ISBONDelegate WriteInt64(long v)
        {
            Emit(new string(' ', _tab) + "(int64)" + v.ToString() + "\n");
            return this;
        }

        public ISBONDelegate WriteUInt8(byte v)
        {
            Emit(new string(' ', _tab) + "(uint8)" + v.ToString() + "\n");
            return this;
        }

        public ISBONDelegate WriteUInt16(ushort v)
        {
            Emit(new string(' ', _tab) + "(uin16)" + v.ToString() + "\n");
            return this;
        }

        public ISBONDelegate WriteUInt32(uint v)
        {
            Emit(new string(' ', _tab) + "(uint32)" + v.ToString() + "\n");
            return this;
        }

        public ISBONDelegate WriteUInt64(ulong v)
        {
            Emit(new string(' ', _tab) + "(uint64)" + v.ToString() + "\n");
            return this;
        }

        public ISBONDelegate WriteDecimal(decimal v)
        {
            Emit(new string(' ', _tab) + "(decimal)" + v.ToString() + "\n");
            return this;
        }

        public ISBONDelegate WriteString(string v)
        {
            Emit(new string(' ', _tab) + "\"" + v.ToString() + "\"\n");
            return this;
        }

        public ISBONDelegate WriteDateTime(DateTime v)
        {
            Emit(new string(' ', _tab) + "(DateTime)" + v.ToString() + "\n");
            return this;
        }

        public ISBONDelegate WriteNull()
        {
            Emit(new string(' ', _tab) + "(null)" + "\n");
            return this;
        }

        public ISBONDelegate WriteDBNull()
        {
            Emit(new string(' ', _tab) + "(DBNull)" + "\n");
            return this;
        }

        public ISBONDelegate WriteGuid(Guid v)
        {
            Emit(new string(' ', _tab) + "(Guid)" + v.ToString() + "\n");
            return this;
        }

        public ISBONDelegate WriteFloat(float v)
        {
            Emit(new string(' ', _tab) + "(float)" + v.ToString() + "\n");
            return this;
        }

        public ISBONDelegate WriteDouble(double v)
        {
            Emit(new string(' ', _tab) + "(double)" + v.ToString() + "\n");
            return this;
        }

        public ISBONDelegate WriteByteArray(int length, ref int chunkSize, ref Action<int, byte[]> fnBytes)
        {
            Emit(new string(' ', _tab) + "(byte array)" + "\n");
            return this;
        }

		public ISBONDelegate WriteChar(char v)
		{
			Emit(new string(' ', _tab) + "(char)" + v.ToString() + "\n");
			return this;
		}

		public ISBONDelegate WriteTimeSpan(TimeSpan v)
		{
			Emit(new string(' ', _tab) + "(TimeSpan)" + v.ToString() + "\n");
			return this;
		}

		public ISBONDelegate WriteStartAttribute()
		{
			Emit(new string(' ', _tab++) + "<\n");
			return this;
		}

		public ISBONDelegate WriteEndAttribute()
		{
			Emit(new string(' ', --_tab) + ">\n");
			return this;
		}

		public bool CatchException(Exception ex)
		{
			Emit (ex.Message);
			return false;
		}
    }
}
