//
//  Author:
//    Steve Fillingham steve.fillingham@bworkflow.com
//
//  Copyright (c) 2013, SquareIT Software PTY LTD
//
//
//
using System;

namespace ConceptCave.SBON
{
    [AttributeUsage(AttributeTargets.Property)]
	public class SBONAttribute : Attribute
	{
		public string Name { get; private set; }
		public int Order { get; private set; }
        private bool _omit;
        public bool Omit { get { return _omit || Name == null; } }

		public SBONAttribute (string name = null, int order = 0, bool omit = false)
		{
			Name = name;
			Order = order;
            _omit = omit;
		}
	}
}

