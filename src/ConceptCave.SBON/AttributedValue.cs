//
//  Author:
//    Steve Fillingham steve.fillingham@bworkflow.com
//
//  Copyright (c) 2014, SquareIT Software PTY LTD
//
//
//
using System;
using System.Collections.Generic;

namespace ConceptCave.SBON
{
	public class AttributedValue
	{
		public Dictionary<string, object> Attributes { get; set; }
		public object Value { get; set; }

		public void AddAttribute(string key, object o)
		{
			if (Attributes == null)
				Attributes = new Dictionary<string, object> ();
			Attributes [key] = o;
		}

		public bool GetAttributeValue<T>(string key, ref T value)
		{
			object o = null;
			if (Attributes.TryGetValue (key, out o))
			{
				value = (T)o;
				return true;
			}
			return false;
		}
	}
}

