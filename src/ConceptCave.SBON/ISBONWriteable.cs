//
//  Author:
//    Steve Fillingham steve.fillingham@bworkflow.com
//
//  Copyright (c) 2014, SquareIT Software PTY LTD
//
//
//
using System;

namespace ConceptCave.SBON
{
	public interface ISBONWriteable
	{
		void WriteTo(SBONWriter writer, Action<object> fnWriteObject);
	}
}

