//
//  Author:
//    Steve Fillingham steve.fillingham@bworkflow.com
//
//  Copyright (c) 2013, SquareIT Software PTY LTD
//
//
//
using System;
using System.Collections.Generic;

namespace ConceptCave.SBON
{
	public class NameTable : ISBONWriteable
	{
		private static readonly string Base = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

		private Dictionary<string, string> _forwards = new Dictionary<string, string> ();
		private Dictionary<string, string> _backwards = new Dictionary<string, string> ();

		[SBON("T")]
		public Dictionary<string, string> Table
		{ 
			get { return _forwards; } 
			set
			{ 
				_forwards = value;
				_backwards = new Dictionary<string, string> ();
				foreach (var kvp in _forwards)
					_backwards [kvp.Value] = kvp.Key;
			}
		}

		public NameTable ()
		{
		}

		public string Add(string name)
		{
			if (_backwards.ContainsKey (name))
				return _backwards [name];

			string key = "";
			int i = _forwards.Count;
			while (i > 0)
			{
				int r = i % Base.Length;
				key = Base [r] + key;
				i = i / Base.Length;
			}
			if (key == "")
				key = Base[0].ToString();
			_forwards [key] = name;
			_backwards [name] = key;
			return key;
		}

		public string KeyForName(string name)
		{
			string key = null;
			_backwards.TryGetValue (name, out key);
			return key;
		}

		public string NameForKey(string key)
		{
			string name = null;
			_forwards.TryGetValue (key, out name);
			return name;
		}

		#region ISBONWriteable implementation
		public void WriteTo(SBONWriter writer, Action<object> fnWriteObject)
		{
			writer.WriteStartObject ();
				writer.WriteName ("T");
				writer.WriteStartObject ();
					foreach (var i in _forwards)
					{
						writer.WriteName (i.Key);
						writer.WriteString (i.Value);
					}
				writer.WriteEndObject ();
			writer.WriteEndObject ();
		}
		#endregion
	}
}

