//
//  Author:
//    Steve Fillingham steve.fillingham@bworkflow.com
//
//  Copyright (c) 2013, SquareIT Software PTY LTD
//
//
//
using System;
using System.IO;
using System.Text;

namespace ConceptCave.SBON
{
	public enum Code
	{
		Null,
		StartObject,
		StartObjectWithId,
		EndObject,
		StartArray,
		EndArray,
		Name,
		Bool,
		Int8,
		Int16,
		Int32,
		Int64,
		UInt8,
		UInt16,
		UInt32,
		UInt64,
		String,
		Float,
		Double,
		Decimal,
		DateTime,
		Guid,
		ByteArray,
        DBNull,
		Char,
		TimeSpan,

		StartAttribute,
		EndAttribute,
	}

	public class SBONException : Exception
	{
		public SBONException() : base()
		{
		}

		public SBONException(string message) : base(message)
		{
		}

		public SBONException(string message, Exception innerException) : base(message, innerException)
		{
		}
	}

}

