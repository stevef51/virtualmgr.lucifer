//
//  Author:
//    Steve Fillingham steve.fillingham@bworkflow.com
//
//  Copyright (c) 2013, SquareIT Software PTY LTD
//
//
//
using System;
using System.IO;
using System.Text;

namespace ConceptCave.SBON
{
	public class SBONWriter
	{
        public ISBONDelegate Mirror;
		private BinaryWriter _bw;
		public Stream Stream { get; private set; }

		public SBONWriter(Stream stream)
		{
			Stream = stream;
			_bw = new BinaryWriter (stream);
		}

		public void WriteStartObject()
		{
			_bw.Write ((byte)(Code.StartObject));
			if (Mirror != null)
			    Mirror.WriteStartObject();
		}

		public void WriteEndObject()
		{
			_bw.Write ((byte)(Code.EndObject));
			if (Mirror != null)
			   	Mirror.WriteEndObject();
        }

		public void WriteStartArray()
		{
			_bw.Write ((byte) (Code.StartArray));
			if (Mirror != null)
			  	Mirror.WriteStartArray();
        }

		public void WriteEndArray()
		{
			_bw.Write ((byte) (Code.EndArray));
			if (Mirror != null)
			  	Mirror.WriteEndArray();
        }

		public void WriteName(string name)
		{
			_bw.Write ((byte) (Code.Name));
			if (name.Length > 255)
				throw new SBONException ("Name cannot be longer than 255 characters");

			byte[] b = Encoding.UTF8.GetBytes (name);
			_bw.Write ((byte)b.Length);
			_bw.Write (b);
        
			if (Mirror != null)
				Mirror.WriteName(name);
        }

		public void WriteBool(bool v)
		{
			_bw.Write ((byte) (Code.Bool));
			_bw.Write (v);
			if (Mirror != null)
			 	Mirror.WriteBool(v);
        }

		public void WriteInt8(sbyte v)
		{
			_bw.Write ((byte) (Code.Int8));
			_bw.Write (v);
			if (Mirror != null)
				Mirror.WriteInt8(v);
        }

		public void WriteInt16(short v)
		{
			_bw.Write ((byte) (Code.Int16));
			_bw.Write (v);
			if (Mirror != null)
				Mirror.WriteInt16(v);
        }

		public void WriteInt32(int v)
		{
			_bw.Write ((byte) (Code.Int32));
			_bw.Write (v);
			if (Mirror != null)
				Mirror.WriteInt32(v);
        }

		public void WriteInt64(long v)
		{
			_bw.Write ((byte) (Code.Int64));
			_bw.Write (v);
			if (Mirror != null)
				Mirror.WriteInt64(v);
        }

		public void WriteUInt8(byte v)
		{
			_bw.Write ((byte) (Code.UInt8));
			_bw.Write (v);
			if (Mirror != null)
				Mirror.WriteUInt8(v);
        }

		public void WriteUInt16(ushort v)
		{
			_bw.Write ((byte) (Code.UInt16));
			_bw.Write (v);
			if (Mirror != null)
				Mirror.WriteUInt16(v);
        }

		public void WriteUInt32(uint v)
		{
			_bw.Write ((byte) (Code.UInt32));
			_bw.Write (v);
			if (Mirror != null)
				Mirror.WriteUInt32(v);
        }

		public void WriteUInt64(ulong v)
		{
			_bw.Write ((byte) (Code.UInt64));
			_bw.Write (v);
			if (Mirror != null)
				Mirror.WriteUInt64(v);
        }

		public void WriteDecimal(decimal v)
		{
			_bw.Write ((byte) (Code.Decimal));
			_bw.Write (v);
			if (Mirror != null)
				Mirror.WriteDecimal(v);
        }

		public void WriteString(string v)
		{
			_bw.Write ((byte) (Code.String));
			byte[] b = Encoding.UTF8.GetBytes (v);
			_bw.Write (b.Length);
			_bw.Write (b);
			if (Mirror != null)
				Mirror.WriteString(v);
        }

		public void WriteDateTime(DateTime v)
		{
			_bw.Write ((byte) (Code.DateTime));
			_bw.Write (v.ToBinary ());
			if (Mirror != null)
				Mirror.WriteDateTime(v);
        }

		public void WriteNull()
		{
			_bw.Write ((byte) (Code.Null));
			if (Mirror != null)
				Mirror.WriteNull();
        }

        public void WriteDBNull()
        {
			_bw.Write ((byte)(Code.DBNull));
			if (Mirror != null)
				Mirror.WriteDBNull();
        }

		public void WriteGuid(Guid v)
		{
			_bw.Write ((byte) (Code.Guid));
			_bw.Write (v.ToByteArray ());
			if (Mirror != null)
				Mirror.WriteGuid(v);
        }

		public void WriteFloat(float v)
		{
			_bw.Write ((byte) (Code.Float));
			_bw.Write (v);
			if (Mirror != null)
				Mirror.WriteFloat(v);
        }

		public void WriteDouble(double v)
		{
			_bw.Write ((byte) (Code.Double));
			_bw.Write (v);
			if (Mirror != null)
				Mirror.WriteDouble(v);
        }

		public void WriteByteArray(byte[] v)
		{
			_bw.Write ((byte) (Code.ByteArray));
			_bw.Write (v.Length);
			_bw.Write (v);
        }

		public void WriteByteArray(int length, int chunkSize, Func<int, int, byte[]> fn)
		{
			_bw.Write ((byte) (Code.ByteArray));
			_bw.Write (length);

			byte[] v = null;
			for (int i = 0; i < length; i += v.Length)
			{
				v = fn (i, i + chunkSize > length ? length - i : chunkSize);
				_bw.Write (v);
			}
        }

		public void WriteChar(char v)
		{
			_bw.Write ((byte)Code.Char);
			_bw.Write (v);
			if (Mirror != null)
				Mirror.WriteChar (v);
		}

		public void WriteTimeSpan(TimeSpan v)
		{
			_bw.Write ((byte)Code.TimeSpan);
			_bw.Write (v.Ticks);
		}

		public void WriteStartAttribute()
		{
			_bw.Write ((byte)Code.StartAttribute);
			if (Mirror != null)
				Mirror.WriteStartAttribute();
		}

		public void WriteEndAttribute()
		{
			_bw.Write ((byte)Code.EndAttribute);
			if (Mirror != null)
				Mirror.WriteEndAttribute();
		}
	}
}

