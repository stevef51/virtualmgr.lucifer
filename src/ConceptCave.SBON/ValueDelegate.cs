//
//  Author:
//    Steve Fillingham steve.fillingham@bworkflow.com
//
//  Copyright (c) 2013, SquareIT Software PTY LTD
//
//
//
using System;

namespace ConceptCave.SBON
{
	public class ValueDelegate : RootDelegate
	{
		public Action<object> FnValue { get; set; }
		public Func<ISBONDelegate> FnStartArray { get; set; }
		public Func<ISBONDelegate> FnEndArray { get; set; }
		public Func<ISBONDelegate> FnStartObject { get; set; }
		public Func<ISBONDelegate> FnEndObject { get; set; }
		public Func<ISBONDelegate> FnStartAttribute { get; set; }
		public Func<ISBONDelegate> FnEndAttribute { get; set; }
		public bool Ended { get; protected set; }

		public bool ThrowOnNameNotFound = false;			// By default if a Name is not found we ignore the Value

		public ValueDelegate(ISBONDelegate parent) : base(parent)
		{
		}

		#region ISBONDelegate implementation

		protected virtual ISBONDelegate WriteValue(object v)
		{
			if (Ended)
				throw new SBONException (string.Format("{0} already Ended", this.GetType().Name));

			FnValue (v);

			Ended = true;

			return Parent;
		}

		public override ISBONDelegate WriteStartArray()
		{
			if (FnStartArray != null)
				return FnStartArray ();
			else
				return base.WriteStartArray ();
		}

		public override ISBONDelegate WriteStartObject()
		{
			if (FnStartObject != null)
				return FnStartObject ();
			else
				return base.WriteStartObject ();
		}

		public override ISBONDelegate WriteEndArray()
		{
			if (FnEndArray != null)
				return FnEndArray ();
			else
				return base.WriteEndArray ();
		}

		public override ISBONDelegate WriteEndObject()
		{
			if (FnEndObject != null)
				return FnEndObject ();
			else
				return base.WriteEndObject ();
		}


		public override ISBONDelegate WriteBool(bool v)
		{
			return WriteValue(v);
		}

		public override ISBONDelegate WriteInt8(sbyte v)
		{
			return WriteValue(v);
		}

		public override ISBONDelegate WriteInt16(short v)
		{
			return WriteValue(v);
		}

		public override ISBONDelegate WriteInt32(int v)
		{
			return WriteValue(v);
		}

		public override ISBONDelegate WriteInt64(long v)
		{
			return WriteValue(v);
		}

		public override ISBONDelegate WriteUInt8(byte v)
		{
			return WriteValue(v);
		}

		public override ISBONDelegate WriteUInt16(ushort v)
		{
			return WriteValue(v);
		}

		public override ISBONDelegate WriteUInt32(uint v)
		{
			return WriteValue(v);
		}

		public override ISBONDelegate WriteUInt64(ulong v)
		{
			return WriteValue(v);
		}

		public override ISBONDelegate WriteDecimal(decimal v)
		{
			return WriteValue(v);
		}

		public override ISBONDelegate WriteString(string v)
		{
			return WriteValue(v);
		}

		public override ISBONDelegate WriteDateTime(DateTime v)
		{
			return WriteValue(v);
		}

		public override ISBONDelegate WriteNull()
		{
			return WriteValue(null);
		}

        public override ISBONDelegate WriteDBNull()
        {
            return WriteValue(DBNull.Value);
        }

		public override ISBONDelegate WriteGuid(Guid v)
		{
			return WriteValue(v);
		}

		public override ISBONDelegate WriteFloat(float v)
		{
			return WriteValue(v);
		}

		public override ISBONDelegate WriteDouble(double v)
		{
			return WriteValue(v);
		}

		public override ISBONDelegate WriteByteArray(int length, ref int chunkSize, ref Action<int, byte[]> fnBytes)
		{
			chunkSize = length;
			byte[] v = new byte[length];
			fnBytes = (offset, bytes) => Array.Copy (bytes, 0, v, offset, bytes.Length); 
			return WriteValue(v);
		}

		public override ISBONDelegate WriteChar(char v)
		{
			return WriteValue (v);
		}

		public override ISBONDelegate WriteTimeSpan(TimeSpan v)
		{
			return WriteValue (v);
		}

		public override ISBONDelegate WriteStartAttribute()
		{
			if (FnStartAttribute != null)
				return FnStartAttribute ();
			else
				return base.WriteStartAttribute ();
		}

		public override ISBONDelegate WriteEndAttribute()
		{
			if (FnEndAttribute != null)
				return FnEndAttribute ();
			else
				return base.WriteEndAttribute ();
		}


		#endregion
	}
}

