//
//  Author:
//    Steve Fillingham steve.fillingham@bworkflow.com
//
//  Copyright (c) 2013, SquareIT Software PTY LTD
//
//
//
using System;
using System.IO;

namespace ConceptCave.SBON
{
	public static class SBONConvert
	{
		public static object DeserializeObject(Stream stream)
		{
			var d = new Collection.Deserializer (stream);
			return d.DeserializeObject ();
		}

        public static T Deserialize<T>(Stream stream)
        {
            var d = new Reflection.Deserializer(stream);
            return d.Deserialize<T>();
        }

        public static T Deserialize<T>(Stream stream, T instance)
        {
            var d = new Reflection.Deserializer(stream);
            return d.Deserialize<T>(instance);
        }

        public static T[] DeserializeArray<T>(Stream stream)
		{
			var d = new Reflection.Deserializer (stream);
			return d.DeserializeArray<T> ();
		}

		public static void Serialize(Stream stream, object o)
		{
			var s = new Reflection.Serializer (stream);
			s.Serialize (o);
		}

		public static void Serialize(Stream stream, Reflection.SerializerSettings settings, object o)
		{
			var s = new Reflection.Serializer (stream) { Settings = settings };
			s.Serialize (o);
		}
	}
}

