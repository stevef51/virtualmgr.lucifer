//
//  Author:
//    Steve Fillingham steve.fillingham@bworkflow.com
//
//  Copyright (c) 2014, SquareIT Software PTY LTD
//
//
//
using System;

namespace ConceptCave.SBON.Collection
{
	public class AttributedValueDelegate : ValueDelegate
	{
		public AttributedValue AttributedValue { get; private set; }

		public AttributedValueDelegate (ISBONDelegate parent) : this(parent, new AttributedValue())
		{
		}

		public AttributedValueDelegate (ISBONDelegate parent, AttributedValue attributedValue) : base(parent)
		{
			AttributedValue = attributedValue;
		}

		public override ISBONDelegate WriteName(string name)
		{
			return new ValueDelegate (this) 
			{ 
				FnValue = o => AttributedValue.AddAttribute(name, o), 
				FnStartArray = () => 
				{
					var array = new ArrayDelegate(this);
					AttributedValue.Attributes[name] = array.Array;
					return array;
				},
				FnStartObject = () => 
				{
					var dict = new DictionaryDelegate(this);
					AttributedValue.Attributes[name] = dict.Dict;
					return dict;
				},
				FnStartAttribute = () =>
				{	
					var attr = new AttributedValueDelegate(this);
					AttributedValue.Attributes[name] = attr.AttributedValue;
					return attr;
				}
			};
		}

		public override ISBONDelegate WriteStartObject()
		{
			var dict = new DictionaryDelegate (this);
			AttributedValue.Value = dict.Dict;
			return dict;
		}

		protected override ISBONDelegate WriteValue(object v)
		{
			AttributedValue.Value = v;
			return this;
		}

		public override ISBONDelegate WriteEndAttribute()
		{
			if (FnEndAttribute != null)
				return FnEndAttribute ();

			return Parent;
		}
	}
}

