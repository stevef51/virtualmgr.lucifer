//
//  Author:
//    Steve Fillingham steve.fillingham@bworkflow.com
//
//  Copyright (c) 2013, SquareIT Software PTY LTD
//
//
//
using System;
using System.IO;
using System.Collections.Generic;

namespace ConceptCave.SBON.Collection
{
	public class Deserializer : RootDelegate
	{
		private object _root;
		private SBONParser _parser;

		public Deserializer (Stream stream)
		{
			_parser = new SBONParser (stream);
		}

		public object DeserializeObject()
		{
			_parser.Read (this);
			return _root;
		}

		#region ISBONDelegate implementation

		public override ISBONDelegate WriteStartObject()
		{
			var dict = new Dictionary<string, object> ();
			_root = dict;
			return new DictionaryDelegate (this, dict);
		}

		public override ISBONDelegate WriteEndObject()
		{
			return null;
		}

		public override ISBONDelegate WriteStartArray()
		{
			var array = new List<object> ();
			_root = array;
			return new ArrayDelegate (this, array);
		}

		public override ISBONDelegate WriteEndArray()
		{
			return null;
		}

		public override ISBONDelegate WriteStartAttribute()
		{
			var attr = new AttributedValue ();
			_root = attr;
			return new AttributedValueDelegate (this, attr);
		}

		public override ISBONDelegate WriteEndAttribute()
		{
			return null;
		}
		#endregion

	}
}

