//
//  Author:
//    Steve Fillingham steve.fillingham@bworkflow.com
//
//  Copyright (c) 2013, SquareIT Software PTY LTD
//
//
//
using System;
using System.Collections.Generic;

namespace ConceptCave.SBON.Collection
{
	public class ArrayDelegate : ValueDelegate
	{
		public List<object> Array { get; private set; }

		public ArrayDelegate(ISBONDelegate parent) : this(parent, new List<object>())
		{
		}

		public ArrayDelegate(ISBONDelegate parent, List<object> array) : base(parent)
		{
			Array = array;
		}

		protected override ISBONDelegate WriteValue(object v)
		{
			Array.Add (v);
			return this;
		}

		public override ISBONDelegate WriteStartArray()
		{
			var array = new ArrayDelegate (this);
			Array.Add (array.Array);
			return array;
		}

		public override ISBONDelegate WriteStartObject()
		{
			var dict = new DictionaryDelegate (this);
			Array.Add (dict.Dict);
			return dict;
		}

		public override ISBONDelegate WriteEndArray()
		{
			if (FnEndArray != null)
				return FnEndArray ();
			return Parent;
		}

		public override ISBONDelegate WriteStartAttribute()
		{
			var attr = new AttributedValueDelegate (this);
			Array.Add (attr.AttributedValue);
			return attr;
		}
	}
}

