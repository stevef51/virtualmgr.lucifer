//
//  Author:
//    Steve Fillingham steve.fillingham@bworkflow.com
//
//  Copyright (c) 2013, SquareIT Software PTY LTD
//
//
//
using System;
using System.Collections.Generic;

namespace ConceptCave.SBON.Collection
{
	public class DictionaryDelegate : ValueDelegate
	{
		public Dictionary<string, object> Dict { get; private set; }

		public DictionaryDelegate(ISBONDelegate parent) : this(parent, new Dictionary<string, object>())
		{
		}

		public DictionaryDelegate(ISBONDelegate parent, Dictionary<string, object> dict) : base(parent)
		{
			Dict = dict;
		}

		#region ISBONDelegate implementation

		public override ISBONDelegate WriteName(string name)
		{
			return new ValueDelegate (this) 
			{ 
				FnValue = o => Dict[name] = o, 
				FnStartArray = () => 
				{
					var array = new ArrayDelegate(this);
					Dict[name] = array.Array;
					return array;
				},
				FnStartObject = () => 
				{
					var dict = new DictionaryDelegate(this);
					Dict[name] = dict.Dict;
					return dict;
				},
				FnStartAttribute = () =>
				{	
					var attr = new AttributedValueDelegate(this);
					Dict[name] = attr.AttributedValue;
					return attr;
				}
			};
		}

		protected override ISBONDelegate WriteValue(object v)
		{
			throw new SBONException ("Dictionary cannot WriteValue without a Name");
		}

		public override ISBONDelegate WriteEndObject()
		{
			if (FnEndObject != null)
				return FnEndObject ();

			return Parent;
		}
		#endregion
	}
}

