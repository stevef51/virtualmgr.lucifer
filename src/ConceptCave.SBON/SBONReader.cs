//
//  Author:
//    Steve Fillingham steve.fillingham@bworkflow.com
//
//  Copyright (c) 2013, SquareIT Software PTY LTD
//
//
//
using System;
using System.IO;
using System.Text;

namespace ConceptCave.SBON
{
	public class SBONReader
	{
		private BinaryReader _br;
		public Stream Stream { get; private set; }

		public SBONReader (Stream stream)
		{
			Stream = stream;
			_br = new BinaryReader (Stream);
		}

		public Code ReadCode()
		{
			return (Code)_br.ReadByte ();
		}

		public bool ReadBool()
		{
			return _br.ReadBoolean ();
		}

		public void ReadStartObject()
		{
		}

		public int ReadObjectId()
		{
			return _br.ReadInt32 ();
		}

		public void ReadEndObject()
		{
		}

		public void ReadStartArray()
		{
		}

		public void ReadEndArray()
		{
		}

		public string ReadName()
		{
			byte len = _br.ReadByte ();
			byte[] b = _br.ReadBytes (len);
			return Encoding.UTF8.GetString (b);
		}

	 	public void ReadValue()
		{
		}

		public sbyte ReadInt8()
		{
			return _br.ReadSByte ();
		}

		public short ReadInt16()
		{
			return _br.ReadInt16 ();
		}

		public int ReadInt32()
		{
			return _br.ReadInt32 ();
		}

		public long ReadInt64()
		{
			return _br.ReadInt64 ();
		}

		public byte ReadUInt8()
		{
			return _br.ReadByte ();
		}

		public ushort ReadUInt16()
		{
			return _br.ReadUInt16 ();
		}

		public uint ReadUInt32()
		{
			return _br.ReadUInt32 ();
		}

		public ulong ReadUInt64()
		{
			return _br.ReadUInt64 ();
		}

		public decimal ReadDecimal()
		{
			return _br.ReadDecimal ();
		}

		public string ReadString()
		{
			int len = _br.ReadInt32 ();
			byte[] b = _br.ReadBytes (len);
			return Encoding.UTF8.GetString(b);
		}

		public DateTime ReadDateTime()
		{
			return DateTime.FromBinary (_br.ReadInt64 ());
		}

		public void ReadNull()
		{
		}

        public void ReadDBNull()
        {
        }

		public Guid ReadGuid()
		{
			return new Guid (_br.ReadBytes (16));
		}

		public float ReadFloat()
		{
			return _br.ReadSingle();
		}

		public double ReadDouble()
		{
			return _br.ReadDouble ();
		}

		public int ReadByteArrayLength()
		{
			return _br.ReadInt32 ();
		}

		public byte[] ReadByteArrayData(int size)
		{
			return _br.ReadBytes (size);
		}

		public char ReadChar()
		{
			return _br.ReadChar ();
		}

		public TimeSpan ReadTimeSpan()
		{
			return new TimeSpan (_br.ReadInt64 ());
		}
	}
}

