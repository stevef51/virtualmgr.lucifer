//
//  Author:
//    Steve Fillingham steve.fillingham@bworkflow.com
//
//  Copyright (c) 2013, SquareIT Software PTY LTD
//
//
//
using System;

namespace ConceptCave.SBON
{
	public interface ISBONDelegate
	{
		ISBONDelegate WriteStartObject();
		ISBONDelegate WriteEndObject();

		ISBONDelegate WriteStartArray();
		ISBONDelegate WriteEndArray();

		ISBONDelegate WriteName(string name);

		ISBONDelegate WriteBool(bool v);
		ISBONDelegate WriteInt8(sbyte v);
		ISBONDelegate WriteInt16(short v);
		ISBONDelegate WriteInt32(int v);
		ISBONDelegate WriteInt64(long v);
		ISBONDelegate WriteUInt8(byte v);
		ISBONDelegate WriteUInt16(ushort v);
		ISBONDelegate WriteUInt32(uint v);
		ISBONDelegate WriteUInt64(ulong v);
		ISBONDelegate WriteDecimal(decimal v);
		ISBONDelegate WriteString(string v);
		ISBONDelegate WriteDateTime(DateTime v);
		ISBONDelegate WriteNull();
        ISBONDelegate WriteDBNull();
		ISBONDelegate WriteGuid(Guid v);
		ISBONDelegate WriteFloat(float v);
		ISBONDelegate WriteDouble(double v);		
		ISBONDelegate WriteByteArray(int length, ref int chunkSize, ref Action<int,byte[]> fnBytes);
		ISBONDelegate WriteChar(char v);
		ISBONDelegate WriteTimeSpan(TimeSpan v);

		ISBONDelegate WriteStartAttribute();
		ISBONDelegate WriteEndAttribute();

		bool CatchException(Exception ex);
	}
}

