﻿const path = require('path');
const webpack = require('webpack');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const ManifestPlugin = require('webpack-manifest-plugin');
const ExtractTextWebpackPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const glob = require('glob');

const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

const { InjectManifest } = require('workbox-webpack-plugin');

const AppCachePlugin = require('appcache-webpack-plugin');

const ios_files = glob.sync("**/*.js", {
    cwd: path.resolve(__dirname, 'cordova/ios')
}).map(f => '/spa/cordova/ios/' + f);

module.exports = {
    entry: {
        app: './src/app.js'
    },
    output: {
        path: path.resolve(__dirname, 'wwwroot/spa'),
        filename: '[name].bundle.[hash].js',
        publicPath: '/spa/'
    },
    /*    watchOptions: {
            aggregateTimeout: 1000,
            poll: 5000
        }, */
    plugins: [
        new ManifestPlugin(),                   // Produces the Webpack manifest.json in ./wwwroot folder
        //        new CleanWebpackPlugin(),               // This cleans out the ./wwwroot folder on each build, only for dev really as prod uses Docker which is always clean anyway
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery'
        }),
        new ExtractTextWebpackPlugin('[name].[hash].css'),
        new HtmlWebpackPlugin({
            filename: 'index.html',
            inject: false,                          // We will insert the bundles manually in the template
            template: path.resolve(__dirname, 'src/index.ejs'),
            environment: process.env.NODE_ENV,
            determinePlatform: true,
            manifestAppcache: ``      // Not using AppCache, we use ServiceWorkers where possible
        }),
        new HtmlWebpackPlugin({
            filename: 'index-ios.html',
            inject: false,                          // We will insert the bundles manually in the template
            template: path.resolve(__dirname, 'src/index.ejs'),
            environment: process.env.NODE_ENV,
            determinePlatform: false,
            manifestAppcache: `manifest='/spa/index-manifest-ios.appcache'`
        }),
        new HtmlWebpackPlugin({
            filename: 'index-android.html',
            inject: false,                          // We will insert the bundles manually in the template
            template: path.resolve(__dirname, 'src/index.ejs'),
            environment: process.env.NODE_ENV,
            determinePlatform: false,
            manifestAppcache: ``      // Not using AppCache, we use ServiceWorkers where possible
        }),
        new webpack.EnvironmentPlugin({
            NODE_ENV: 'Production'              // Use 'Production' unless process.env.NODE_ENV is defined
        }),
        /* Uncomment this to allow BundleAnalyzer webapp to run to analyze size of the bundle
                new BundleAnalyzerPlugin({
                    analyzerHost: '0.0.0.0'
                }),
        */
        new InjectManifest({
            swSrc: 'static/service-worker.js',
        }),
        new AppCachePlugin({
            setting: ['prefer-online'],
            output: 'index-manifest.appcache',
            exclude: [/.*\.map$/]
        }),
        new AppCachePlugin({
            setting: ['prefer-online'],
            output: 'index-manifest-ios.appcache',
            cache: ios_files
        }),
        new CopyPlugin([{
            from: './cordova',
            to: path.resolve(__dirname, 'wwwroot/spa/cordova')
        }]),
        new CopyPlugin([{
            from: './static',
            to: path.resolve(__dirname, 'wwwroot/spa')
        }])
    ],
    module: {
        rules: [{
            test: /\.(js|jsd)?$/,
            exclude: /node_modules/,
            use: {
                loader: 'babel-loader',
                options: {
                    compact: true,
                    presets: [
                        '@babel/preset-env'
                    ],
                    plugins: ['angularjs-annotate']     // This will fix any Angular functions not using explicit DI 
                    // (eg function($scope) instead of ['$scope', function($scope)])
                }
            }
        },
        {
            test: /template\.html$/,
            use: ['raw-loader']
        },
        {
            test: /\.(css|scss)$/,
            use: ExtractTextWebpackPlugin.extract({
                use: ['css-loader', 'sass-loader']
            })
        },
        {
            test: /\.(png|svg|jpg|jpeg|gif)$/,
            use: [
                'file-loader'
            ]
        },
        {
            test: /\.(woff|woff2|eot|ttf|otf)$/,
            use: [
                {
                    loader: 'file-loader',
                    options: {
                        name: '[name].[ext]',
                        //                        outputPath: 'fonts/'
                    }
                }
            ]
        },
        /*
         * Necessary to be able to use angular 1 with webpack as explained in https://github.com/webpack/webpack/issues/2049
         */
        {
            test: require.resolve('angular'),
            loader: 'exports-loader?window.angular'
        }
        ]
    }
};
