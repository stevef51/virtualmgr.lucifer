cordova.define('cordova/plugin_list', function (require, exports, module) {
    module.exports = [
        {
            "id": "bluetherm-thermaq.BlueThermThermaQ",
            "file": "plugins/bluetherm-thermaq/www/BlueThermThermaQ.js",
            "pluginId": "bluetherm-thermaq",
            "clobbers": [
                "cordova.plugins.BlueThermThermaQ"
            ]
        },
        {
            "id": "com.pylonproducts.wifiwizard.WifiWizard",
            "file": "plugins/com.pylonproducts.wifiwizard/www/WifiWizard.js",
            "pluginId": "com.pylonproducts.wifiwizard",
            "clobbers": [
                "window.WifiWizard"
            ]
        },
        {
            "id": "com.rjfun.cordova.httpd.CorHttpd",
            "file": "plugins/com.rjfun.cordova.httpd/www/ios/CorHttpd.js",
            "pluginId": "com.rjfun.cordova.httpd",
            "clobbers": [
                "cordova.plugins.CorHttpd"
            ]
        },
        {
            "id": "com.virtualmgr.cordova.ble.VirtualManagerBLE",
            "file": "plugins/com.virtualmgr.cordova.ble/www/VirtualManagerBLE.js",
            "pluginId": "com.virtualmgr.cordova.ble",
            "clobbers": [
                "cordova.plugins.VirtualManagerBLE"
            ]
        },
        {
            "id": "com.virtualmgr.iOSExternalAccessory.iOSExternalAccessory",
            "file": "plugins/com.virtualmgr.iOSExternalAccessory/www/iOSExternalAccessory.js",
            "pluginId": "com.virtualmgr.iOSExternalAccessory",
            "clobbers": [
                "cordova.plugins.iOSExternalAccessory"
            ]
        },
        {
            "id": "cordova-plugin-app-version.AppVersionPlugin",
            "file": "plugins/cordova-plugin-app-version/www/AppVersionPlugin.js",
            "pluginId": "cordova-plugin-app-version",
            "clobbers": [
                "cordova.getAppVersion"
            ]
        },
        {
            "id": "cordova-plugin-appcenter-shared.AppCenter",
            "file": "plugins/cordova-plugin-appcenter-shared/www/AppCenter.js",
            "pluginId": "cordova-plugin-appcenter-shared",
            "clobbers": [
                "AppCenter"
            ]
        },
        {
            "id": "cordova-plugin-appcenter-analytics.Analytics",
            "file": "plugins/cordova-plugin-appcenter-analytics/www/Analytics.js",
            "pluginId": "cordova-plugin-appcenter-analytics",
            "clobbers": [
                "AppCenter.Analytics"
            ]
        },
        {
            "id": "cordova-plugin-appcenter-crashes.Crashes",
            "file": "plugins/cordova-plugin-appcenter-crashes/www/Crashes.js",
            "pluginId": "cordova-plugin-appcenter-crashes",
            "clobbers": [
                "AppCenter.Crashes"
            ]
        },
        {
            "id": "cordova-plugin-bluetooth-serial.bluetoothSerial",
            "file": "plugins/cordova-plugin-bluetooth-serial/www/bluetoothSerial.js",
            "pluginId": "cordova-plugin-bluetooth-serial",
            "clobbers": [
                "window.bluetoothSerial"
            ]
        },
        {
            "id": "cordova-plugin-camera.Camera",
            "file": "plugins/cordova-plugin-camera/www/CameraConstants.js",
            "pluginId": "cordova-plugin-camera",
            "clobbers": [
                "Camera"
            ]
        },
        {
            "id": "cordova-plugin-camera.CameraPopoverOptions",
            "file": "plugins/cordova-plugin-camera/www/CameraPopoverOptions.js",
            "pluginId": "cordova-plugin-camera",
            "clobbers": [
                "CameraPopoverOptions"
            ]
        },
        {
            "id": "cordova-plugin-camera.camera",
            "file": "plugins/cordova-plugin-camera/www/Camera.js",
            "pluginId": "cordova-plugin-camera",
            "clobbers": [
                "navigator.camera"
            ]
        },
        {
            "id": "cordova-plugin-camera.CameraPopoverHandle",
            "file": "plugins/cordova-plugin-camera/www/ios/CameraPopoverHandle.js",
            "pluginId": "cordova-plugin-camera",
            "clobbers": [
                "CameraPopoverHandle"
            ]
        },
        {
            "id": "cordova-plugin-camera-preview.CameraPreview",
            "file": "plugins/cordova-plugin-camera-preview/www/CameraPreview.js",
            "pluginId": "cordova-plugin-camera-preview",
            "clobbers": [
                "CameraPreview"
            ]
        },
        {
            "id": "cordova-plugin-device.device",
            "file": "plugins/cordova-plugin-device/www/device.js",
            "pluginId": "cordova-plugin-device",
            "clobbers": [
                "device"
            ]
        },
        {
            "id": "cordova-plugin-dialogs.notification",
            "file": "plugins/cordova-plugin-dialogs/www/notification.js",
            "pluginId": "cordova-plugin-dialogs",
            "merges": [
                "navigator.notification"
            ]
        },
        {
            "id": "cordova-plugin-file.DirectoryEntry",
            "file": "plugins/cordova-plugin-file/www/DirectoryEntry.js",
            "pluginId": "cordova-plugin-file",
            "clobbers": [
                "window.DirectoryEntry"
            ]
        },
        {
            "id": "cordova-plugin-file.DirectoryReader",
            "file": "plugins/cordova-plugin-file/www/DirectoryReader.js",
            "pluginId": "cordova-plugin-file",
            "clobbers": [
                "window.DirectoryReader"
            ]
        },
        {
            "id": "cordova-plugin-file.Entry",
            "file": "plugins/cordova-plugin-file/www/Entry.js",
            "pluginId": "cordova-plugin-file",
            "clobbers": [
                "window.Entry"
            ]
        },
        {
            "id": "cordova-plugin-file.File",
            "file": "plugins/cordova-plugin-file/www/File.js",
            "pluginId": "cordova-plugin-file",
            "clobbers": [
                "window.File"
            ]
        },
        {
            "id": "cordova-plugin-file.FileEntry",
            "file": "plugins/cordova-plugin-file/www/FileEntry.js",
            "pluginId": "cordova-plugin-file",
            "clobbers": [
                "window.FileEntry"
            ]
        },
        {
            "id": "cordova-plugin-file.FileError",
            "file": "plugins/cordova-plugin-file/www/FileError.js",
            "pluginId": "cordova-plugin-file",
            "clobbers": [
                "window.FileError"
            ]
        },
        {
            "id": "cordova-plugin-file.FileReader",
            "file": "plugins/cordova-plugin-file/www/FileReader.js",
            "pluginId": "cordova-plugin-file",
            "clobbers": [
                "window.FileReader"
            ]
        },
        {
            "id": "cordova-plugin-file.FileSystem",
            "file": "plugins/cordova-plugin-file/www/FileSystem.js",
            "pluginId": "cordova-plugin-file",
            "clobbers": [
                "window.FileSystem"
            ]
        },
        {
            "id": "cordova-plugin-file.FileUploadOptions",
            "file": "plugins/cordova-plugin-file/www/FileUploadOptions.js",
            "pluginId": "cordova-plugin-file",
            "clobbers": [
                "window.FileUploadOptions"
            ]
        },
        {
            "id": "cordova-plugin-file.FileUploadResult",
            "file": "plugins/cordova-plugin-file/www/FileUploadResult.js",
            "pluginId": "cordova-plugin-file",
            "clobbers": [
                "window.FileUploadResult"
            ]
        },
        {
            "id": "cordova-plugin-file.FileWriter",
            "file": "plugins/cordova-plugin-file/www/FileWriter.js",
            "pluginId": "cordova-plugin-file",
            "clobbers": [
                "window.FileWriter"
            ]
        },
        {
            "id": "cordova-plugin-file.Flags",
            "file": "plugins/cordova-plugin-file/www/Flags.js",
            "pluginId": "cordova-plugin-file",
            "clobbers": [
                "window.Flags"
            ]
        },
        {
            "id": "cordova-plugin-file.LocalFileSystem",
            "file": "plugins/cordova-plugin-file/www/LocalFileSystem.js",
            "pluginId": "cordova-plugin-file",
            "clobbers": [
                "window.LocalFileSystem"
            ],
            "merges": [
                "window"
            ]
        },
        {
            "id": "cordova-plugin-file.Metadata",
            "file": "plugins/cordova-plugin-file/www/Metadata.js",
            "pluginId": "cordova-plugin-file",
            "clobbers": [
                "window.Metadata"
            ]
        },
        {
            "id": "cordova-plugin-file.ProgressEvent",
            "file": "plugins/cordova-plugin-file/www/ProgressEvent.js",
            "pluginId": "cordova-plugin-file",
            "clobbers": [
                "window.ProgressEvent"
            ]
        },
        {
            "id": "cordova-plugin-file.fileSystems",
            "file": "plugins/cordova-plugin-file/www/fileSystems.js",
            "pluginId": "cordova-plugin-file"
        },
        {
            "id": "cordova-plugin-file.requestFileSystem",
            "file": "plugins/cordova-plugin-file/www/requestFileSystem.js",
            "pluginId": "cordova-plugin-file",
            "clobbers": [
                "window.requestFileSystem"
            ]
        },
        {
            "id": "cordova-plugin-file.resolveLocalFileSystemURI",
            "file": "plugins/cordova-plugin-file/www/resolveLocalFileSystemURI.js",
            "pluginId": "cordova-plugin-file",
            "merges": [
                "window"
            ]
        },
        {
            "id": "cordova-plugin-file.isChrome",
            "file": "plugins/cordova-plugin-file/www/browser/isChrome.js",
            "pluginId": "cordova-plugin-file",
            "runs": true
        },
        {
            "id": "cordova-plugin-file.iosFileSystem",
            "file": "plugins/cordova-plugin-file/www/ios/FileSystem.js",
            "pluginId": "cordova-plugin-file",
            "merges": [
                "FileSystem"
            ]
        },
        {
            "id": "cordova-plugin-file.fileSystems-roots",
            "file": "plugins/cordova-plugin-file/www/fileSystems-roots.js",
            "pluginId": "cordova-plugin-file",
            "runs": true
        },
        {
            "id": "cordova-plugin-file.fileSystemPaths",
            "file": "plugins/cordova-plugin-file/www/fileSystemPaths.js",
            "pluginId": "cordova-plugin-file",
            "merges": [
                "cordova"
            ],
            "runs": true
        },
        {
            "id": "cordova-plugin-file-transfer.FileTransferError",
            "file": "plugins/cordova-plugin-file-transfer/www/FileTransferError.js",
            "pluginId": "cordova-plugin-file-transfer",
            "clobbers": [
                "window.FileTransferError"
            ]
        },
        {
            "id": "cordova-plugin-file-transfer.FileTransfer",
            "file": "plugins/cordova-plugin-file-transfer/www/FileTransfer.js",
            "pluginId": "cordova-plugin-file-transfer",
            "clobbers": [
                "window.FileTransfer"
            ]
        },
        {
            "id": "cordova-plugin-geolocation.Coordinates",
            "file": "plugins/cordova-plugin-geolocation/www/Coordinates.js",
            "pluginId": "cordova-plugin-geolocation",
            "clobbers": [
                "Coordinates"
            ]
        },
        {
            "id": "cordova-plugin-geolocation.PositionError",
            "file": "plugins/cordova-plugin-geolocation/www/PositionError.js",
            "pluginId": "cordova-plugin-geolocation",
            "clobbers": [
                "PositionError"
            ]
        },
        {
            "id": "cordova-plugin-geolocation.Position",
            "file": "plugins/cordova-plugin-geolocation/www/Position.js",
            "pluginId": "cordova-plugin-geolocation",
            "clobbers": [
                "Position"
            ]
        },
        {
            "id": "cordova-plugin-geolocation.geolocation",
            "file": "plugins/cordova-plugin-geolocation/www/geolocation.js",
            "pluginId": "cordova-plugin-geolocation",
            "clobbers": [
                "navigator.geolocation"
            ]
        },
        {
            "id": "cordova-plugin-media-capture.CaptureAudioOptions",
            "file": "plugins/cordova-plugin-media-capture/www/CaptureAudioOptions.js",
            "pluginId": "cordova-plugin-media-capture",
            "clobbers": [
                "CaptureAudioOptions"
            ]
        },
        {
            "id": "cordova-plugin-media-capture.CaptureImageOptions",
            "file": "plugins/cordova-plugin-media-capture/www/CaptureImageOptions.js",
            "pluginId": "cordova-plugin-media-capture",
            "clobbers": [
                "CaptureImageOptions"
            ]
        },
        {
            "id": "cordova-plugin-media-capture.CaptureVideoOptions",
            "file": "plugins/cordova-plugin-media-capture/www/CaptureVideoOptions.js",
            "pluginId": "cordova-plugin-media-capture",
            "clobbers": [
                "CaptureVideoOptions"
            ]
        },
        {
            "id": "cordova-plugin-media-capture.CaptureError",
            "file": "plugins/cordova-plugin-media-capture/www/CaptureError.js",
            "pluginId": "cordova-plugin-media-capture",
            "clobbers": [
                "CaptureError"
            ]
        },
        {
            "id": "cordova-plugin-media-capture.MediaFileData",
            "file": "plugins/cordova-plugin-media-capture/www/MediaFileData.js",
            "pluginId": "cordova-plugin-media-capture",
            "clobbers": [
                "MediaFileData"
            ]
        },
        {
            "id": "cordova-plugin-media-capture.MediaFile",
            "file": "plugins/cordova-plugin-media-capture/www/MediaFile.js",
            "pluginId": "cordova-plugin-media-capture",
            "clobbers": [
                "MediaFile"
            ]
        },
        {
            "id": "cordova-plugin-media-capture.helpers",
            "file": "plugins/cordova-plugin-media-capture/www/helpers.js",
            "pluginId": "cordova-plugin-media-capture",
            "runs": true
        },
        {
            "id": "cordova-plugin-media-capture.capture",
            "file": "plugins/cordova-plugin-media-capture/www/capture.js",
            "pluginId": "cordova-plugin-media-capture",
            "clobbers": [
                "navigator.device.capture"
            ]
        },
        {
            "id": "cordova-plugin-screen-orientation.screenorientation",
            "file": "plugins/cordova-plugin-screen-orientation/www/screenorientation.js",
            "pluginId": "cordova-plugin-screen-orientation",
            "clobbers": [
                "cordova.plugins.screenorientation"
            ]
        },
        {
            "id": "cordova-plugin-spinner-dialog.SpinnerDialog",
            "file": "plugins/cordova-plugin-spinner-dialog/www/spinner.js",
            "pluginId": "cordova-plugin-spinner-dialog",
            "merges": [
                "window.plugins.spinnerDialog"
            ]
        },
        {
            "id": "cordova-plugin-splashscreen.SplashScreen",
            "file": "plugins/cordova-plugin-splashscreen/www/splashscreen.js",
            "pluginId": "cordova-plugin-splashscreen",
            "clobbers": [
                "navigator.splashscreen"
            ]
        },
        {
            "id": "cordova-plugin-statusbar.statusbar",
            "file": "plugins/cordova-plugin-statusbar/www/statusbar.js",
            "pluginId": "cordova-plugin-statusbar",
            "clobbers": [
                "window.StatusBar"
            ]
        },
        {
            "id": "cordova-plugin-vibration.notification",
            "file": "plugins/cordova-plugin-vibration/www/vibration.js",
            "pluginId": "cordova-plugin-vibration",
            "merges": [
                "navigator"
            ]
        },
        {
            "id": "cordova-plugin-wkwebview-engine.ios-wkwebview-exec",
            "file": "plugins/cordova-plugin-wkwebview-engine/src/www/ios/ios-wkwebview-exec.js",
            "pluginId": "cordova-plugin-wkwebview-engine",
            "clobbers": [
                "cordova.exec"
            ]
        },
        {
            "id": "cordova-plugin-zip.Zip",
            "file": "plugins/cordova-plugin-zip/zip.js",
            "pluginId": "cordova-plugin-zip",
            "clobbers": [
                "zip"
            ]
        },
        {
            "id": "es6-promise-plugin.Promise",
            "file": "plugins/es6-promise-plugin/www/promise.js",
            "pluginId": "es6-promise-plugin",
            "runs": true
        },
        {
            "id": "phonegap-plugin-barcodescanner.BarcodeScanner",
            "file": "plugins/phonegap-plugin-barcodescanner/www/barcodescanner.js",
            "pluginId": "phonegap-plugin-barcodescanner",
            "clobbers": [
                "cordova.plugins.barcodeScanner"
            ]
        }
    ];
    module.exports.metadata =
        // TOP OF METADATA
        {
            "bluetherm-thermaq": "1.1.3",
            "com.pylonproducts.wifiwizard": "0.2.11",
            "com.rjfun.cordova.httpd": "1.2.0",
            "com.virtualmgr.cordova.ble": "1.5.0",
            "com.virtualmgr.iOSExternalAccessory": "1.0.0",
            "com.virtualmgr.vmplayer-plugin": "1.0.0",
            "cordova-plugin-app-version": "0.1.9",
            "cordova-plugin-appcenter-shared": "0.1.9",
            "cordova-plugin-appcenter-analytics": "0.1.9",
            "cordova-plugin-appcenter-crashes": "0.1.9",
            "cordova-plugin-bluetooth-serial": "0.4.7",
            "cordova-plugin-camera": "4.0.3",
            "cordova-plugin-camera-preview": "0.10.0",
            "cordova-plugin-device": "1.1.7",
            "cordova-plugin-dialogs": "2.0.1",
            "cordova-plugin-file": "6.0.1",
            "cordova-plugin-file-transfer": "1.7.1",
            "cordova-plugin-geolocation": "4.0.1",
            "cordova-plugin-media-capture": "3.0.2",
            "cordova-plugin-screen-orientation": "3.0.2-dev",
            "cordova-plugin-spinner-dialog": "1.3.1",
            "cordova-plugin-splashscreen": "5.0.2",
            "cordova-plugin-statusbar": "2.4.2",
            "cordova-plugin-vibration": "3.1.0",
            "cordova-plugin-whitelist": "1.3.3",
            "cordova-plugin-wkwebview-engine": "1.1.4-dev",
            "cordova-plugin-zip": "3.1.0",
            "es6-promise-plugin": "4.2.2",
            "phonegap-plugin-barcodescanner": "8.0.1"
        };
    // BOTTOM OF METADATA
});
