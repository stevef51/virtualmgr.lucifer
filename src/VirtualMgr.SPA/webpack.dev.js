const merge = require('webpack-merge');
const common = require('./webpack.common');
const { WebpackPluginServe: Serve } = require('webpack-plugin-serve');

module.exports = merge(common, {
    entry: {
        app: './src/app-dev.js'
    },
    mode: 'development',
    devtool: 'source-map-inline',
    devServer: {
        contentBase: './'
    },
    output: {
        filename: '[name].bundle.js',
    },
    plugins: [
        new Serve({
            port: 80,
            static: '/src/VirtualMgr.SPA/wwwroot',
            hmr: false,
            https: null
        })
    ],
    watch: true
});

