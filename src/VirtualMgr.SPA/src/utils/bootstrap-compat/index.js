'use strict';

import app from './ngmodule';
import './connectable-device';
import './other';

export default app;