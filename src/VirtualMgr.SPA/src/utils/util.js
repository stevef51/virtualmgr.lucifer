﻿'use strict';

import moment from 'moment';

export function rtrim(str) {
    return str.replace(/\/*$/, '');
};

export function ltrim(str) {
    return str.replace(/^\/*/, '');
};

export function randomString(c) {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < c; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
};

//is it an image extension?
export function isImage(fname) {
    return (fname.toLowerCase().indexOf('.jpg', fname.length - 4) !== -1) ||
        (fname.toLowerCase().indexOf('.png', fname.length - 4) !== -1) ||
        (fname.toLowerCase().indexOf('.jpeg', fname.length - 5) !== -1);
};

//clamp width-height
export function clampDimensions(width, height, maxWidth, maxHeight) {
    var aspectRatio = width / height;

    var cWidth = width;
    var cHeight = height;

    //first scale maxWidth, then scale maxHeight
    if (maxWidth && cWidth > maxWidth) {
        var wsf = cWidth / maxWidth;
        cWidth = cWidth / wsf;
        cHeight = cHeight / wsf;
    }

    if (maxHeight && cHeight > maxHeight) {
        var hsf = cHeight / maxHeight;
        cWidth = cWidth / hsf;
        cHeight = cHeight / hsf;
    }

    return {
        width: cWidth,
        height: cHeight
    };
};

/**
 * Fast UUID generator, RFC4122 version 4 compliant.
 * @author Jeff Ward (jcward.com).
 * @license MIT license
 * @link http://stackoverflow.com/questions/105034/how-to-create-a-guid-uuid-in-javascript/21963136#21963136
 **/
export const UUID = (function () {
    var self = {};
    var lut = []; for (var i = 0; i < 256; i++) { lut[i] = (i < 16 ? '0' : '') + (i).toString(16); }
    self.generate = function () {
        var d0 = Math.random() * 0xffffffff | 0;
        var d1 = Math.random() * 0xffffffff | 0;
        var d2 = Math.random() * 0xffffffff | 0;
        var d3 = Math.random() * 0xffffffff | 0;
        return lut[d0 & 0xff] + lut[d0 >> 8 & 0xff] + lut[d0 >> 16 & 0xff] + lut[d0 >> 24 & 0xff] + '-' +
            lut[d1 & 0xff] + lut[d1 >> 8 & 0xff] + '-' + lut[d1 >> 16 & 0x0f | 0x40] + lut[d1 >> 24 & 0xff] + '-' +
            lut[d2 & 0x3f | 0x80] + lut[d2 >> 8 & 0xff] + '-' + lut[d2 >> 16 & 0xff] + lut[d2 >> 24 & 0xff] +
            lut[d3 & 0xff] + lut[d3 >> 8 & 0xff] + lut[d3 >> 16 & 0xff] + lut[d3 >> 24 & 0xff];
    }
    return self;
})();

export function generateCombGuid() {
    function LongToByteArray(/*long*/longvalue) {
        // we want to represent the input as a 8-bytes array
        var byteArray = [0, 0, 0, 0, 0, 0, 0, 0];

        for (var index = 0; index < byteArray.length; index++) {
            var abyte = longvalue & 0xff;
            byteArray[index] = abyte;
            longvalue = (longvalue - abyte) / 256;
        }
        return byteArray;
    }

    function BytesToHex(bytes) {
        for (var hex = [], i = 0; i < bytes.length; i++) {
            hex.push((bytes[i] >>> 4).toString(16));
            hex.push((bytes[i] & 0xF).toString(16));
        }
        return hex.join("");
    }

    //http://stackoverflow.com/a/2117523/173949
    var guid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0,
            v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });

    guid = guid.substr(0, 24);

    var days = moment().diff(moment([1900, 0, 1]), 'days');
    var daysBytes = LongToByteArray(days).reverse();
    daysBytes = daysBytes.slice(daysBytes.length - 2);
    var daysHex = BytesToHex(daysBytes);

    var msecs = Math.floor(moment().diff(moment().startOf('day')) / 3.333333);
    var msecsBytes = LongToByteArray(msecs).reverse();
    msecsBytes = msecsBytes.slice(msecsBytes.length - 4);
    var msecsHex = BytesToHex(msecsBytes);

    guid = guid + daysHex + msecsHex;

    return guid;
};