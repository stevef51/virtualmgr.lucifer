'use strict';

import appInit from './app-init';
import './dev';

import angular from 'angular';

appInit.then(function () {
    angular.bootstrap(document.documentElement, ['player'], {
        strictDi: true
    });
});

