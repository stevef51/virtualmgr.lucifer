'use strict';

import app from './ngmodule';
import { ltrim, rtrim } from '../utils/util';
import _ from 'lodash';

app.factory('webServiceUrl', function ($window) {
    const razordata = Object.assign({}, $window.razordata);

    razordata.apiprefix = rtrim(razordata.apiprefix);
    razordata.odataprefix = rtrim(razordata.odataprefix);
    razordata.mediaprefix = rtrim(razordata.mediaprefix);

    const svc = {
        api: function (path) {
            return `${razordata.apiprefix}/${ltrim(path)}`;
        },
        odata: function (feed, version) {
            return `${razordata.odataprefix}/odata${version || 3}/${ltrim(feed)}`;
        },
        mediaImage: function (mediaId, options) {
            options = options || {};
            var query = _.chain(options)
                .pick(options, 'width', 'height', 'zoom', 'format')
                .toPairs()
                .filter(pair => pair[1] !== undefined)
                .map(pair => `${pair[0]}=${pair[1]}`)
                .value();
            if (options.cache) {
                query.push('cache');
            }
            return `${razordata.mediaprefix}/image/${mediaId}?${query.join('&')}`
        },
        menuImage: function (mediaId, options) {
            return svc.mediaImage(mediaId, Object.assign({
                height: 30,
                format: 'png'
            }, options));
        },
        taskImage: function (mediaId, options) {
            return svc.mediaImage(mediaId, Object.assign({
                height: 75,
                format: 'png'
            }, options));
        },
        toolBarImage: function (mediaId, options) {
            return svc.mediaImage(mediaId, Object.assign({
                height: 40,
                format: 'png'
            }, options));
        },
        mediaImagePrefix: `${razordata.mediaprefix}/image`
    }

    return svc;
});