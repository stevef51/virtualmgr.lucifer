'use strict';

import angular from 'angular';
import '../services/bworkflow-api';
import 'angular-animate';
import 'angular-moment';
import 'angular-material-collapsible';
import '../device-drivers/instrument-works-ph-probe';
import '../device-drivers/temperature-probe-manager';
import '../device-drivers/bluetherm-dishtemp-blue';
import '../device-drivers/bluetherm-thermaq';
import '../filters';

var questionsModule = angular.module('questions', [
    'bworkflowApi',
    'angularMoment',
    'languageTranslation',
    'instrument-works-pH-probe',
    'temperature-probe-manager-module',
    'bluetherm-dishtemp-blue-module',
    'bluetherm-thermaq-module',
    'vm-filters',
    'ngAnimate',
    'ngMaterial',
    'ngMaterialCollapsible',
    'angular-cache'
]);

//Framework methods...



export default questionsModule;
