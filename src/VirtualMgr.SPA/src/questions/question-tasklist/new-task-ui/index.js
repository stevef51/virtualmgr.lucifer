'use strict';

import app from '../../ngmodule';

app.directive('questionTasklistNewtaskUi',
    function ($timeout, bworkflowApi, $animate, $q, sectionUtils, tabletProfile, appUpdateMonitor, createNewTaskWorkflow, webServiceUrl) {
        return {
            template: require('./template.html').default,
            restrict: 'E',
            require: 'ngModel',
            scope: {
                template: '=ngModel',
                presented: '=',
                onClose: '&'
            },
            link: function (scope, element, attrs) {
                scope.url = webServiceUrl;

                appUpdateMonitor.addPauser(scope); // Pause updates whilst our scope is alive

                scope.selection = {

                };

                scope.memberFeedTemplate = {
                    name: 'members',
                    feed: 'Members',
                    filter: "substringof('[[searchmemberssearch]]', Name) and IsASite eq true",
                    orderbyfields: 'Name',
                    idfields: ['UserId'],
                    selectfields: 'UserId,Name',
                    itemsperpage: 5,
                    datascope: {
                        ExcludeOutsideHierarchies: false,
                        IncludeCurrentUser: true,
                        ExcludeExpired: true
                    },
                    parameterdefinitions: [{
                        internalname: 'searchmemberssearch'
                    }],
                    cache: {
                        key: 'sites.searchByName'
                    }
                };

                scope.memberFeed = bworkflowApi.createDataFeed(scope.memberFeedTemplate, scope);

                var fnUnsubscribeTabletProfile;
                fnUnsubscribeTabletProfile = tabletProfile(function (profile) {
                    if (profile.siteid) {
                        var defaultMemberFeedTemplate = {
                            name: 'members',
                            feed: 'Members',
                            filter: "UserId eq guid'" + profile.siteid + "'",
                            orderbyfields: 'Name',
                            idfields: ['UserId'],
                            itemsperpage: 5
                        };

                        var defaultMemberFeed = bworkflowApi.createDataFeed(defaultMemberFeedTemplate, scope);
                        defaultMemberFeed.afterLoadHooks.push(function (feed) {
                            if (feed.data.length) {
                                scope.selection.site = feed.data[0];
                            }
                        });
                        defaultMemberFeed.getData(true);
                    }

                    fnUnsubscribeTabletProfile();
                });

                scope.create = function () {
                    // Prevent double-click immediately
                    scope.isCreateDisabled = true;
                    scope.isCreatingOnline = false;

                    if (scope.selection.site) {
                        scope.template.siteid = scope.selection.site.alldata.UserId;
                    }

                    createNewTaskWorkflow.createAndClockIn(scope.template, scope.presented.userid, scope.presented.template.enforcesingleclockin).then(function (data) {
                        scope.isCreateDisabled = false;
                        scope.isCreatingOnline = false;
                        scope.offline = false;
                        scope.onClose({ data });
                    }, function (error) {
                        scope.isCreateDisabled = false;
                        scope.isCreatingOnline = false;
                        scope.offline = true;
                        scope.onClose();
                    }, function (notify) {
                        scope.isCreatingOnline = !notify.offline;
                    });
                };
            }
        };
    }
);
