'use strict';

import app from '../../ngmodule';

app.config(function ($mdDialogProvider) {
    $mdDialogProvider.addPreset('createTaskDialog', {
        options: function () {
            return {
                parent: angular.element(document.body),
                fullscreen: true,
                clickOutsideToClose: true,
                multiple: true,                     // Does not close any parent dialog

                template: require('./template.html').default,
                controller: ['$scope', '$mdDialog', 'locals', function ($scope, $mdDialog, locals) {
                    $scope.currentTemplate = locals.currentTemplate;
                    $scope.presented = locals.presented;
                    $scope.cancel = () => $mdDialog.cancel();
                    $scope.close = data => $mdDialog.hide(data);
                }]
            }
        }
    })
})
