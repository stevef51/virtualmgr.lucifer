'use strict';

import app from '../../ngmodule';

app.config(function ($mdDialogProvider) {
    $mdDialogProvider.addPreset('showTaskDialog', {
        options: function () {
            return {
                parent: angular.element(document.body),
                fullscreen: true,
                clickOutsideToClose: true,
                multiple: true,                     // Does not close any parent dialog

                template: require('./template.html').default,
                controller: ['$scope', '$mdDialog', 'locals', 'TaskListUserInterfaceType', 'webServiceUrl', function ($scope, $mdDialog, locals, TaskListUserInterfaceType, webServiceUrl) {
                    $scope.task = locals.task;
                    $scope.parent = locals.parent;
                    $scope.presented = locals.presented;
                    $scope.url = webServiceUrl;
                    $scope.UserInterfaceType = TaskListUserInterfaceType;
                    $scope.cancel = () => $mdDialog.cancel();
                    $scope.close = data => $mdDialog.hide(data);
                }]
            }
        }
    })
})
