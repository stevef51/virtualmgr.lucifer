'use strict';

import app from '../../ngmodule';
import moment from 'moment';

app.directive('questionTasklistClockinClockoutUi', ['$timeout', 'appUpdateMonitor',
    function ($timeout, appUpdateMonitor) {
        return {
            template: require('./template.html').default,
            restrict: 'E',
            scope: {
                'task': '=',
                'presented': '=',
                'parent': '=',           // This is to get around scope isolation forced by Dialog
                'onClose': '&'
            },
            link: function (scope, element, attrs) {
                appUpdateMonitor.addPauser(scope); // Pause updates whilst our scope is alive

                scope.viewinggroupinformation = false;
                scope.showingdocumentation = false;
                scope.showSetTaskType = false;

                scope.showGroupInformation = function () {
                    scope.viewinggroupinformation = true;
                };

                scope.gotoTask = function (task) {
                    scope.viewinggroupinformation = false;

                    scope.parent.showTaskById(task.id);
                };

                scope.toggleShowDocuments = function () {
                    scope.showingdocumentation = !scope.showingdocumentation;
                };

                scope.showChangeTaskType = function () {
                    scope.showSetTaskType = true;
                };

                scope.$on('question-task-list-task-type-editor.changed', function () {
                    scope.showSetTaskType = false;
                });

                scope.canStart = function (task) {
                    if (task.starttime) {
                        return moment().isAfter(task.starttime);
                    }
                    return true;
                };

                scope.completeactivities = function (activities) {
                    if (activities == null) {
                        return;
                    }

                    angular.forEach(activities, function (activity) {
                        activity.completed = true;
                    });
                }

                // Hack selected scope.parent functions into our scope ..
                function callParent(args) {
                    args = angular.isObject(args) ? args : { func: args };
                    scope[args.func] = function () {
                        scope.parent[args.func].apply(scope.parent, arguments);
                        if (args.closeDialog) {
                            scope.onClose();
                        }
                    }
                }

                ([
                    'clockingout',
                    'clockin',
                    'cancelclockingout',
                    { func: 'pause', closeDialog: true },
                    'clockout',
                    { func: 'finish', closeDialog: true },
                    'finishing',
                    'finishingIncomplete',
                    'finishingGroup',
                    'cancelfinishingincomplete',
                    'deleteGroup',
                    'gotoChecklist'
                ]).forEach(callParent);
            }
        };
    }
]);
