'use strict';

import angular from 'angular';
import questionsModule from './ngmodule';

questionsModule.factory('sectionUtils', ['$rootScope', '$q', function ($rootScope, $q) {
    var _urls = {};
    var _deferreds = {};

    return {
        urls: _urls,

        addUrls: function (urls) {
            _urls = angular.extend(_urls, urls);

            $rootScope.$emit('sectionUtils.addUrls', _urls);

            angular.forEach(urls, function (value, key) {
                if (!angular.isDefined(_deferreds[key])) {
                    _deferreds[key] = $q.defer();
                }
                _deferreds[key].resolve(value);
            })
        },

        buildODataUrl: function (feed) {
            var url = _urls['odata'];

            if (url == null || url == '') {
                url = 'odata';
            }

            url = url + "/" + feed;

            return url;
        },

        getUrl: function (url) {
            if (!angular.isDefined(_deferreds[url])) {
                _deferreds[url] = $q.defer();
            }
            return _deferreds[url].promise;
        }
    }
}]);
