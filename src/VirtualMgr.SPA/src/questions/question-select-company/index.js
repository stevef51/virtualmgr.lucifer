'use strict';

import angular from 'angular';
import questionsModule from '../ngmodule';

questionsModule.directive('questionSelectCompany', ['bworkflowApi', '$timeout', 'languageTranslate', function (bworkflowApi, $timeout, languageTranslate) {
    return $.extend({}, questionDirectiveBase, {
        template: require('./template.html').default,
        link: function (scope, elt, attrs) {
            questionDirectiveBase.link(scope, undefined, bworkflowApi, languageTranslate);

            // this supports validation as each question type stores it's answer in a different way
            scope.getAnswerValue = function () {
                return scope.answer.SelectedId;
            };
        },
        controller: function ($scope) {
            $scope.answer = $scope.presented.Answer;

            $scope.options = {
                highlight: true
            };

            $scope.watchcontainer = {};
            $scope.watchcontainer.currentCompanyObject = {};

            if ($scope.answer.SelectedId != null) {
                $scope.watchcontainer.currentCompanyObject.id = $scope.answer.SelectedId;
                $scope.watchcontainer.currentCompanyObject.name = $scope.answer.SelectedName;
            }

            $scope.companySuggestion = new Bloodhound({
                datumTokenizer: function (d) {
                    return Bloodhound.tokenizers.whitespace(d.name);
                },
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                remote: {
                    url: window.razordata.apiprefix + 'Player/Execute?query=%QUERY',
                    replace: function (url, query) {
                        $scope.query = query;
                        return url + "#" + query;
                    },
                    ajax: {
                        beforeSend: function (jqXhr, settings) {
                            settings.data = JSON.stringify({
                                Handler: 'CompanyDirectory',
                                Method: 'Search',
                                parameters: {
                                    text: $scope.query,
                                    page: 1,
                                    count: 5,
                                    userid: $scope.presented.userid
                                },
                            });

                            jqXhr.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
                        },
                        type: "POST"
                    },
                    filter: function (data) {
                        return data.companies;
                    }
                }
            });

            $scope.companySuggestion.initialize();

            $scope.companyDataset = {
                displayKey: 'name',
                source: $scope.companySuggestion.ttAdapter(),
                templates: {
                    suggestion: Handlebars.compile('<strong>{{name}}</strong><br/><small><p class="muted">{{address.street}}, {{address.town}}, {{address.state}}</p></small>')
                }
            };

            $scope.$watch('watchcontainer.currentCompanyObject', function (newCompany) {
                if (!newCompany || angular.isDefined(newCompany.id) == false) {
                    return;
                }

                $scope.answer.SelectedId = newCompany.id;
                $scope.answer.SelectedName = newCompany.name;
            });

            $scope.clearAnswer = function () {
                $scope.watchcontainer.currentCompanyObject = {
                    id: null,
                    name: null
                };
            };
        }
    })
}]);