'use strict';

import '../section-utils';
import questionsModule from '../ngmodule';

questionsModule.constant('HtmlSectionLayout', {
    Flow: 0,
    Table: 1,
    TableRow: 2,
    Tabbed: 3,
    Modal: 4,
    FullScreen: 5
});

//Section renderer- works the same way as freetext below basically
questionsModule.directive('questionSection', function ($injector, bworkflowApi, sectionUtils, $timeout, playerButtons, HtmlSectionLayout) {
    var _count = 0;

    return {
        scope: {
            presented: '=',
            pageScope: '='
        },
        template: require('./template.html').default,
        restrict: 'EA',
        controller: function ($scope, $controller) {
            $scope.HtmlSectionLayout = HtmlSectionLayout;

            // ngControllerCode will only be defined at the Page root (Page is a Section)
            // We only want 1 pageScope at the Page level
            var ngControllerCode = $scope.presented.ngcontroller;
            var fn = function () { };
            if (ngControllerCode) {
                fn = eval('(' + ngControllerCode + ')');
            }

            $scope.SectionController = function () {
                // If we have a pageScope already then this is not the Page root section
                if ($scope.pageScope) {
                    return this;
                }

                // Must be Page root section, create a pageScope, this gets passed down to all questions..
                $scope.pageScope = $scope.$new();
                $scope.pageScope.scope = {}; // Bin for all named Questions scope objects
                $scope.pageScope.playerButtons = playerButtons;

                $scope.pageScope.onready = function () {
                    console.log('pageScope ready');
                };

                // Bypass strictDi for backwards compatability
                $injector.annotate(fn, false);

                var ctrl = $controller(fn, {
                    $scope: $scope.pageScope
                }).constructor;

                // Queue the onready call, this will occur once all Questions have been rendered into pageScope
                $timeout($scope.pageScope.onready);
            };
        },
        link: function (scope, elt, attrs) {
            scope.allquestionscategory = {
                Category: 'All Questions',
                ErrorCount: 0
            };
            scope.feeds = {};

            if (scope.presented.presentasfinal) {
                // let anyone who's interested know this is the last page as dictated by the dude/dudet that put the checklist together
                console.log('Final page detected');
                scope.$emit('section.finalpage', scope.presented);
            }

            if (scope.presented.urls) {
                sectionUtils.addUrls(scope.presented.urls);
            }

            scope.visibleFirstEnter = true;

            if (scope.presented.AllowFilterByCategory == true && scope.presented.ChildCategories.length > 0) {
                scope.selectedcategory = scope.presented.ChildCategories[0];
            }

            if (scope.presented.HtmlPresentation.Layout == 3) {
                // tabbed presentation
                angular.forEach(scope.presented.Children, function (child) {
                    if (child.PresentedType == 'Section') {
                        child.HidePrompt = true;
                        child.tabId = child.Id;
                    }
                });
            } else if (scope.presented.HtmlPresentation.Layout == 4 || scope.presented.HtmlPresentation.Layout == 5) {
                // modal, we set this to visible = false by default
                scope.presented.Visible = false;

                // we watch the visible property so that we can add/remove the noscroll
                // class from the body, which gets added/removed as the scroll bars
                // on the body interfer with the full screen view of a section
                scope.$watch('presented.Visible', function (newValue, oldValue) {
                    if (scope.visibleFirstEnter == true) {
                        scope.visibleFirstEnter = false;
                        return;
                    }

                    $('body').toggleClass('noscroll');
                });
            }

            scope.show = function () {
                if (scope.presented.HtmlPresentation.Layout == 4 || scope.presented.HtmlPresentation.Layout == 5) {
                    // Find our parent first
                    var parent = scope.$parent;
                    while (parent != null) {
                        if (angular.isDefined(parent.presented) && parent.presented !== scope.presented) {
                            // Find our sibling Sections and hide them
                            scope.hiddenSiblings = [];
                            parent.presented.Children.forEach(function (sibling) {
                                scope.hiddenSiblings.push({
                                    sibling: sibling,
                                    visible: sibling.Visible
                                });
                                sibling.Visible = sibling === scope.presented;
                            });
                            break;
                        }
                        parent = parent.$parent;
                    }

                }
                scope.presented.Visible = true;
            };

            scope.close = function () {
                scope.presented.Visible = false;
                if (angular.isDefined(scope.hiddenSiblings)) {
                    scope.hiddenSiblings.forEach(function (hs) {
                        hs.sibling.Visible = hs.visible;
                    });
                    delete scope.hiddenSiblings;
                }
            };

            if (scope.presented.Name !== null && scope.presented.Name !== '') {

                // Hook ourselves into pageScope so it can access us by name
                scope.pageScope[scope.presented.Name] = scope;

                scope.$on(scope.presented.Name, function (evt, args) {
                    if (args.action === 'show') {
                        scope.show();
                    }
                });
            }

            scope.setVisible = function (visible) {
                scope.presented.Visible = !!visible;
            };

            scope.selectcategory = function (category) {
                scope.selectedcategory = category;
            };

            scope.filterByCategory = function (c) {
                return function (child) {
                    if (angular.isDefined(scope.selectedcategory) == false) {
                        return true;
                    }

                    if (scope.selectedcategory === scope.allquestionscategory) {
                        return true;
                    }

                    if (!child.Categories) {
                        return true;
                    }

                    if (child.Categories.length === 0) {
                        return true;
                    }

                    return child.Categories.indexOf(scope.selectedcategory.Category) !== -1;
                };
            };

            if (scope.presented.datasource) {
                var split = scope.presented.datasource.split(',');

                // we store each of the feeds in a varibale based on their name
                angular.forEach(split, function (source) {
                    var promise = bworkflowApi.getDataFeed(source);

                    if (promise !== null) {
                        promise.then(function (feed) {
                            var key = feed.notifier.id.split('.').join('');

                            scope.feeds[key] = feed;

                            scope.$watch('feeds.' + key + '.notifier', function (newValue, oldValue) {
                                if (angular.isDefined(newValue) === false || newValue === null) {
                                    return;
                                }

                                var isVisible = false;
                                angular.forEach(scope.feeds, function (feed) {
                                    if (feed.data.length > 0) {
                                        isVisible = true;
                                    }
                                });

                                scope.presented.Visible = isVisible;
                            });
                        });
                    }
                });
            }

            // Hook all childscopes to our deepEdit mode
            function setDeepEditMode(s, deepEdit) {
                if (s === null || (s.hasOwnProperty('presented') && s.presented.enabledeepedit)) {
                    return;
                }
                s.deepEdit = deepEdit;
                var cs = s.$$childHead;
                while (cs !== null) {
                    setDeepEditMode(cs, deepEdit);
                    cs = cs.$$childHead;
                }
                if (s.$$nextSibling !== null) {
                    setDeepEditMode(s.$$nextSibling, deepEdit);
                }
            }

            if (scope.presented.enabledeepedit) {
                scope.deepEdit = {
                    readonly: true
                };

                scope.toggleDeepEditMode = function () {
                    scope.deepEdit.readonly = !scope.deepEdit.readonly;
                    playerButtons.update();
                };

                $timeout(function () {
                    setDeepEditMode(scope.$$childHead, scope.deepEdit);
                });
            }
        }
    };
});
