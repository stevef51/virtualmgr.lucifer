'use strict';

import angular from 'angular';
import questionDirectiveBase from '../question-directive-base';
import questionsModule from '../ngmodule';

questionsModule.directive('questionMultiChoice', function (bworkflowApi, languageTranslate, _) {
    return $.extend({}, questionDirectiveBase, {
        template: require('./template.html').default,
        link: function (scope, elt, attrs) {
            questionDirectiveBase.link(scope, undefined, bworkflowApi, languageTranslate);

            scope.presented.controlShowsValidationErrors = true;
            //            scope.presented.controlShowsPrompt = true;

            scope.answer = scope.presented.Answer;

            switch (scope.presented.DisplayFormat) {
                case 'List_Horizontal':
                case 'List':
                    scope.inputCtrlType = scope.presented.AllowMultiSelect ? 'checkbox' : 'radio';
                    break;
                case 'Compact':
                    scope.inputCtrlType = scope.presented.AllowMultiSelect ? 'compact_multiple' : 'compact_single';
                    if (scope.presented.AllowMultiSelect) {
                        scope.compact_multiple = {
                            answer: _.chain(scope.answer.Selected)
                                .toPairs()
                                .filter(i => !!i[1])
                                .map(i => i[0])
                                .value()
                        };

                        // compact_multiple stores its answers in an array, however we need an object with key: true values
                        scope.$watchCollection('compact_multiple.answer', (newValue, oldValue) => {
                            scope.answer.Selected = _.chain(newValue)
                                .map(i => [i, true])
                                .fromPairs()
                                .value();
                        });
                    }
                    break;
            }
            scope.buttonLayout = scope.presented.DisplayFormat == 'List_Horizontal' ? 'row' : 'column';


            // this supports validation as each question type stores it's answer in a different way
            scope.getAnswerValue = function () {
                return scope.answer.Selected;
            };

            scope.limitSelection = function (max) {
                scope.$watchCollection('answer.Selected', function () {
                    if (angular.isDefined(scope.answer) && angular.isDefined(scope.answer.Selected)) {
                        var count = Object.count(scope.answer.Selected, true);

                        angular.forEach(scope.presented.Choices, function (c) {
                            c.disabled = !scope.answer.Selected[c.Id] && count >= max;
                        });
                    }
                });
            };

        }
    });
});

