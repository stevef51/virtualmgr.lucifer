'use strict';

import app from '../../ngmodule';

app.config(function ($mdDialogProvider) {
    $mdDialogProvider.addPreset('selectTemperatureProbeDialog', {
        options: function () {
            return {
                parent: angular.element(document.body),
                fullscreen: true,
                clickOutsideToClose: true,
                multiple: true,                     // Does not close any parent dialog

                template: require('./template.html').default,
                controller: ['$scope', '$mdDialog', 'locals', function ($scope, $mdDialog, locals) {
                    const temperatureProbe = locals.temperatureProbe;
                    const filterProbes = locals.filterProbes;
                    let destroyFns = [];

                    if (angular.isFunction(temperatureProbe.stopScan)) {
                        destroyFns.push(temperatureProbe.stopScan.bind(temperatureProbe));
                    }

                    $scope.isActiveProbe = function (probe) {
                        return probe === temperatureProbe.probe;
                    }

                    $scope.connect = function (probe) {
                        temperatureProbe.activeProbe(probe);
                    };

                    $scope.probes = filterProbes(temperatureProbe.probes);

                    function destroy() {
                        destroyFns.forEach(fn => fn());
                    }

                    $scope.close = () => {
                        destroy();
                        $mdDialog.cancel();
                    }

                    temperatureProbe.startScan();

                    destroyFns.push(temperatureProbe.subscribe(function (event) {
                        switch (event.name) {
                            case 'button':
                                temperatureProbe.activeProbe(event.probe).then($scope.close);
                                break;

                            case 'removed':
                                $scope.probes = filterProbes(temperatureProbe.probes);
                                break;

                            case 'probes':
                                $scope.probes = filterProbes(event.probes);
                                break;

                            case 'connected':
                                temperatureProbe.stopScan();
                                $scope.close();
                                break;

                            case 'disconnected':
                                temperatureProbe.startScan();
                                break;
                        }
                    }));
                }]
            }
        }
    })
})


