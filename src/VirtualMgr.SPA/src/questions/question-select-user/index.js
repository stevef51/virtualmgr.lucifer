'use strict';

import angular from 'angular';
import questionsModule from '../ngmodule';
import questionDirectiveBase from '../question-directive-base';

questionsModule.directive('questionSelectUser', ['bworkflowApi', 'languageTranslate', function (bworkflowApi, languageTranslate) {
    return $.extend({}, questionDirectiveBase, {
        template: require('./template.html').default,
        link: function (scope, elt, attrs) {
            questionDirectiveBase.link(scope, undefined, bworkflowApi, languageTranslate);
            scope.answer = scope.presented.Answer;

            scope.currentSelection = null;

            // this supports validation as each question type stores it's answer in a different way
            scope.getAnswerValue = function () {
                return scope.answer.SelectedId;
            };

            scope.updateSelection = function (id, name) {
                scope.answer.SelectedId = id;
                scope.answer.SelectedName = name;
            };

            scope.options = [];
            bworkflowApi.labelUsers(scope.presented.AllowedLabels).then(function (r) {
                scope.options = r;
            });

            scope.clearSources = function () {
                if (scope.presented.cleardatasources != null && scope.presented.cleardatasources != '') {
                    var sources = scope.presented.cleardatasources.split(',');
                    var fields = null;
                    if (scope.currentSelection != null) {
                        fields = scope.currentSelection;
                    }

                    angular.forEach(sources, function (source) {
                        var promise = bworkflowApi.getDataFeed(source);

                        if (promise != null) {
                            promise.then(function (toUpdate) {
                                // copy across what was selected into the parameters for the feed
                                toUpdate.clearParameters(fields);
                            });
                        }
                    });
                }
            };

            scope.updateSources = function () {
                if (scope.presented.updatedatasources != null && scope.presented.updatedatasources != '') {
                    var sources = scope.presented.updatedatasources.split(',');

                    angular.forEach(sources, function (source) {
                        var promise = bworkflowApi.getDataFeed(source);

                        if (promise != null) {
                            promise.then(function (toUpdate) {
                                // we store what the current selection is so that we can clear only what we've set
                                // in the clear source method (if its called)
                                scope.currentSelection = angular.copy(scope.watchcontainer.currentUserObject.alldata);

                                angular.forEach(scope.watchcontainer.currentUserObject.alldata, function (value, key) {
                                    toUpdate.parameters[key] = value;
                                });

                                // get data and force a refresh
                                toUpdate.getData(true);
                            });
                        }
                    });
                }
            };

            scope.selectUser = function () {
                if (scope.watchcontainer == null || scope.watchcontainer.currentUserObject == null || scope.watchcontainer.currentUserObject.alldata == null || scope.watchcontainer.currentUserObject.alldata.UserId == null) {
                    return;
                }

                scope.answer.SelectedId = scope.watchcontainer.currentUserObject.alldata.UserId;
                scope.answer.SelectedName = scope.watchcontainer.currentUserObject.alldata.Name;

                scope.clearSources();
                scope.updateSources();
            };

            if (scope.presented.datasource != null) {
                scope.watchcontainer = {};
                scope.watchcontainer.currentUserObject = {
                    id: '',
                    name: ''
                };

                if (scope.presented.Answer.SelectedId != null) {
                    scope.watchcontainer.currentUserObject.id = scope.answer.SelectedId;
                    scope.watchcontainer.currentUserObject.name = scope.answer.SelectedName;
                }

                scope.$watch('watchcontainer.currentUserObject', function (newUser) {
                    if (!newUser || angular.isDefined(newUser.alldata) == false) {
                        return;
                    }

                    scope.answer.SelectedId = newUser.alldata.UserId;
                    scope.answer.SelectedName = newUser.alldata.Name;

                    scope.clearSources();
                    scope.updateSources();
                });
            }
        },
        controller: function ($scope) {
            if ($scope.presented.datasource == null) {
                return;
            }

            $scope.asyncResults = null;

            $scope.options = {
                highlight: true
            };

            $scope.userDataset = {
                displayKey: function (suggestion) {
                    if (angular.isDefined(suggestion.alldata) == false) {
                        return;
                    }

                    return suggestion.alldata.Name
                },
                source: function (query, syncResults, asyncResults) {
                    $scope.asyncResults = syncResults;

                    var fieldName = 'search';
                    if ($scope.presented.Name != null && $scope.presented.Name != '') {
                        fieldName = $scope.presented.Name + '-search';
                    }

                    $scope.feed.parameters[fieldName] = query;
                    $scope.feed.getData(true);
                },
                async: true,
                templates: {
                    suggestion: Handlebars.compile('{{alldata.Name}}')
                }
            };

            var promise = bworkflowApi.getDataFeed($scope.presented.datasource);

            if (promise != null) {
                promise.then(function (feed) {
                    feed.afterLoadHooks.push(function (feed) {
                        if ($scope.asyncResults != null) {
                            $scope.asyncResults(feed.data);
                        }
                    });

                    $scope.feed = feed;
                    $scope.feed.allowMultipleAjax = true; // EVS-1404 fix - For searching the user could type quicker than we can search, allowing multiple Ajax means we will always catch the users last search query
                });
            }
        }
    });
}]);