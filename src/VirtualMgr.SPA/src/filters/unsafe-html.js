'use strict';

import filtersModule from './ngmodule';

filtersModule.filter('unsafeHtml', function ($sce) {
    return $sce.trustAsHtml;
});
