'use strict';

import app from '../../player/ngmodule';
import { $IsStateFilter } from 'angular-ui-router/lib/stateFilters';

app.component('rootHeaderRightCtrl', {
    template: require('./template.html').default,
    controller: function (authSvc, $state, $window) {
        this.authSvc = authSvc;
        this.logout = function () {
            authSvc.logout().then(() => {
                $state.go('login');
            })
        }
        this.reload = function () {
            $window.location.reload();
        }
    }
})