'use strict';

import app from '../../player/ngmodule';

app.component('rootHeaderLeftCtrl', {
    template: require('./template.html').default,
    controller: function ($scope, $mdSidenav) {
        $scope.toggleSideNav = id => $mdSidenav(id).toggle();
    }
})