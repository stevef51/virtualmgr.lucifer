'use strict';

import app from '../../player/ngmodule';

app.component('rootLeftCtrl', {
    template: require('./template.html').default,
    controller: function ($element, $mdSidenav) {
        var events = ['click', 'touchend'];
        events.forEach(function (ev) {
            $element[0].addEventListener(ev, click);
        });

        this.$onDestroy = function () {
            events.forEach(function (ev) {
                $element[0].removeEventListener(ev, click);
            });
        }

        function click(event) {
            let el = event.target;
            while (el != null && el !== $element[0]) {
                let el$ = angular.element(el);
                if (el$.hasClass('close-sidenav')) {
                    $mdSidenav('left').close();
                    break;
                }
                el = el.parentElement;
            }
        }
    }
})