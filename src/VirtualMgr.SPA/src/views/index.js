'use strict';

import './body';
import './root-header';
import './root-header-right';
import './root-header-left';
import './root-left';
import './root-content';
import './login-ctrl';