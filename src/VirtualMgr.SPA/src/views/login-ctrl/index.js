'use strict';

import app from '../../player/ngmodule';

app.component('loginCtrl', {
    template: require('./template.html').default,
    controller: function ($scope, authSvc, $location, $state, $stateParams) {
        $scope.login = function () {
            $scope.loggingIn = true;
            $scope.resetError();

            authSvc.login($scope.username, $scope.password).then(user => {
                $scope.loggingIn = false;
                if (user.authenticated) {
                    if ($stateParams.returnUrl) {
                        console.log(`Login success, redirecting to ${$stateParams.returnUrl}`);
                        $location.path($stateParams.returnUrl);
                    } else {
                        console.log(`Login success, redirecting to Dashboard`);
                        $state.go('root.dashboard');
                    }
                } else {
                    $scope.invalidUsernamePassword = true;
                    console.log(`Login failed`);
                }
            }).catch(err => {
                $scope.loggingIn = false;
                $scope.invalidUsernamePassword = true;
                console.log(`Login failed`);
            });
        }

        $scope.resetError = () => {
            delete $scope.invalidUsernamePassword;
        }
    }
})