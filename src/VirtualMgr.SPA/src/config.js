'use strict';

import app from './player/ngmodule';

app.config(
    function ($httpProvider, $mdThemingProvider) {
        // we want any HTTP requests to send down our authentication cookies.
        $httpProvider.defaults.withCredentials = true;

        $mdThemingProvider.theme('default');
        $mdThemingProvider.theme('dark-grey').backgroundPalette('grey').dark();
    }
);
