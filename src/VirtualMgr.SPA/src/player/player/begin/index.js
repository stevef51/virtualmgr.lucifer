'use strict';

import app from '../../ngmodule';
import angular from 'angular';

app.controller('beginCtrl', ['$stateParams', 'bworkflowApi', '$scope',
    function ($stateParams, bworkflowApi, $scope) {

        //our job, is to fire off an api request to beginCtrl, then transition to present
        bworkflowApi.beginChecklist($stateParams.groupId, $stateParams.resourceId, $stateParams.revieweeId, $stateParams.args, $stateParams.publishingGroupResourceId)
            .then(function (r) {
                $scope.transitionWithPlayerModel(r);
            }, function (ex) {
                $scope.transitionWithPlayerModel(ex.data); //error handle
            });
    }
]);

