'use strict';

import app from '../ngmodule';

app.config(function ($stateProvider) {

    $stateProvider
        .state('root.select', {
            url: '/select',
            abstract: true
        })
        .state('root.select.groups', {
            url: '/groups',
            views: {
                'header-control@root': {
                    template: ''
                },
                'content@root': {
                    component: 'groupsCtrl'
                }
            },
            resolve: {
                groups: function (bworkflowApi) {
                    return bworkflowApi.getPublishedGroups();
                }
            }
        })
        .state('root.select.checklists', {
            url: '/checklists/:groupId',
            views: {
                'content@root': {
                    component: 'checklistsCtrl'
                }
            },
            resolve: {
                group: function (bworkflowApi, $stateParams) {
                    return bworkflowApi.getPublishedGroup($stateParams.groupId);
                }
            }
        })
        .state('root.select.reviewees', {
            url: '/reviewees/:groupId/:resourceId',
            views: {
                'content@root': {
                    component: 'revieweesCtrl'
                }
            },
            resolve: {
                group: function (bworkflowApi, $stateParams) {
                    return bworkflowApi.getPublishedGroup($stateParams.groupId);
                }
            }
        })
});


