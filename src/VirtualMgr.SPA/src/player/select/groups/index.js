﻿'use strict';

import app from '../../ngmodule';
import angular from 'angular';

app.component('groupsCtrl', {
    template: require('./template.html').default,
    bindings: {
        groups: '<'
    }
});

