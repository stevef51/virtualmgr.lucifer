﻿'use strict';

import app from '../../ngmodule';
import angular from 'angular';
import '../../../services/bworkflow-api';
import '../../../services/transition-cache';

app.controller('dashboardContentCtrl', ['$scope',
    '$stateParams',
    '$state',
    'bworkflowApi',
    'transitionCache',
    '$compile',
    '$timeout',
    '$rootScope',
    '$window',
    function ($scope, $stateParams, $state, bworkflowApi, transitionCache, $compile, $timeout, $rootScope, $window) {
        $scope.loading = true;

        $scope.introoptions = {};

        $scope.$on('player_broadcast', function (event, args) {
            if (args.__handled) {
                return;
            }
            // this event gets raised by questions when they want to notify other questions that something has changed.
            // typically this occurrs in a dashboard. We need to broadcast this back down the scopes to let others know
            // of the change. The event object created by who ever created the event should be of the form

            // args.name = name of event to broadcast out
            // args.data = object containing arguments relevant to the event
            $scope.$broadcast(args.name, args.data);
            args.__handled = true;
        });

        $scope.$on('player_broadcast_ajax_error', function (event, args) {
            // this allows code to handle the ajax errors themselves and then
            // to also broadcast it out and have it handled by the normal code as well.
            // An example of this is the odata feeds, which add the noUI flag to the object
            // so its logged on the server, but nothing is shown to the user
            $scope.$broadcast('ajax.error', args);
        });

        $scope.filterToCurrentDashboard = function (dashboards) {
            var result = [];
            angular.forEach(dashboards, function (dashboard) {
                if (dashboard.ChecklistName == $stateParams.name) {
                    result.push(dashboard);
                }
            });

            return result;
        }

        $scope.something = $stateParams.name;

        bworkflowApi.getDashboard()
            .then(function (dashboards) {
                $scope.dashboardModel = dashboards;
                $scope.answers = {};
                $scope.loading = false;

                $rootScope.bodyClass = 'dashboard';

                //Make answer model like in player
                $scope.computedDashboardModels = $.map($scope.filterToCurrentDashboard(dashboards.Dashboards), function (db) {
                    var am = {};

                    //enumerate all the answerable nodes in the answerModel
                    (function thisfunc(_, p) { //jquery each makes the actual value the second arg, so define the function this way
                        if (p.IsAnswerable) {
                            am[p.Id] = p.Answer || {};
                            p.Answer = am[p.Id];
                        }
                        if (p.Children) {
                            $.each(p.Children, thisfunc);
                        }
                    })(undefined, db.Presented);

                    return {
                        PlayerModel: db,
                        AnswerModel: am
                    };
                });
            });
    }
]);