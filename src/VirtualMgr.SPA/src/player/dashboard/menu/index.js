﻿'use strict';

import app from '../../ngmodule';
import '../../../services/bworkflow-api';
import './list';

app.controller('dashboardMenuCtrl',
    function ($scope, bworkflowApi) {
        bworkflowApi.getDashboard()
            .then(function (data) {
                $scope.dashboards = data.Dashboards;
            });
    }
);