'use strict';

import app from '../../ngmodule';

app.factory('auth-http-svc', function ($q, $injector) {
    var authSvc;

    return {
        request: function (config) {
            authSvc = authSvc || $injector.get('authSvc');
            return authSvc.getAccessToken().then(accessToken => {
                if (accessToken) {
                    config.headers = config.headers || {};
                    config.headers.Authorization = `Bearer ${accessToken}`;
                }
                //console.log(`Requesting ${config.method} ${config.url} ${config.headers.Authorization}`);
                return config;
            });
        },
        responseError: function (rejection) {
            authSvc = authSvc || $injector.get('authSvc');
            if (rejection.status === 401) {
                console.log('Unauthorised');
                if (authSvc.canReAuthorise()) {
                    return authSvc.reAuthorise().then(authenticated => {
                        if (authenticated) {
                            // Retry the original request
                            var $http = $injector.get('$http');
                            return $http(rejection.config);
                        }
                        return $q.reject(rejection);
                    })
                }
            }
            return $q.reject(rejection);
        }
    }
});



