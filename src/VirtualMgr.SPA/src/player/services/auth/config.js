'use strict';

import app from '../../../services/ngmodule';

app.config(
    function ($httpProvider) {
        $httpProvider.interceptors.push('auth-http-svc');
        $httpProvider.defaults.withCredentials = true;
    }
);
