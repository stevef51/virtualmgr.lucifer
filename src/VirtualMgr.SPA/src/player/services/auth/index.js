'use strict';

import './auth-svc';
import './auth-http-svc';
import './config';
import './run';