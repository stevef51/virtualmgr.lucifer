'use strict';

import app from '../../ngmodule';
import angular from 'angular';
import '../../../services/web-service-url';
import '../../../utils/persistant-storage';

app.factory('authSvc', function ($q, $http, $rootScope, webServiceUrl, $state, persistantStorage) {
    var svc = {};

    // Try to create namespaced storage
    const storage = persistantStorage.createNamespace('authSvc');

    const unknownUser = { authenticated: false, unknown: true }
    const anonymousUser = { authenticated: false, anonymous: true };

    svc.user = unknownUser;
    svc.tokens = {};

    svc.isAuthenticated = function () {
        if (!svc.user.unknown) {
            return $q.resolve(svc.user.authenticated);
        }
        return svc.getUser().then(user => {
            return svc.user.authenticated;
        }).catch(err => {
            return svc.user.authenticated;
        })
    }

    svc.getAccessToken = function () {
        if (svc.tokens.accessToken) {
            return $q.resolve(svc.tokens.accessToken);
        }
        return storage.getItem('tokens').then(tokens => {
            svc.tokens = tokens || {};
            console.log(`authSvc.getAccessToken = ${svc.tokens.accessToken}`);
            return svc.tokens.accessToken;
        })
    }

    // internal getUser can be called multiple times which may be needed for refreshToken to work
    function getUser() {
        return $http.get(webServiceUrl.api('Account/GetUser'))
            .then(function (response) {
                if (response.data.success) {
                    svc.user = response.data.result;
                    $rootScope.$emit('auth-login');
                    console.log(`authSvc.getUser = ${JSON.stringify(svc.user)}`);
                    return svc.user;
                } else {
                    return $q.reject(response);
                }
            })
            .catch(function (err) {
                console.error(`authSvc.getUser ERROR ${err}`);
                return $q.reject(err);
            });
    }

    // External getUser will gaurantee only 1 request is pending at a time
    svc.getUser = function () {
        if (!svc.$getUser && !svc.$login && svc.user.unknown) {
            svc.user = anonymousUser;

            svc.$getUser = getUser()
                .then(function (response) {
                    delete svc.$getUser;
                    return response;
                })
                .catch(function (err) {
                    delete svc.$getUser;
                    return $q.reject(err);
                });
            return svc.$getUser;
        } else if (svc.$getUser) {
            return svc.$getUser;
        } else if (svc.$login) {
            return svc.$login;
        } else {
            return $q.resolve(svc.user);
        }
    }

    svc.login = function (username, password) {
        if (!svc.$login) {
            svc.user = unknownUser;
            svc.$login = storage.removeItem('tokens').then(() => {
                return $http.post(webServiceUrl.api('Account/Login'), {
                    username,
                    password
                }).then(function (response) {
                    delete svc.$login;
                    if (response.data.success) {
                        svc.tokens = response.data.result;
                        console.log(`authSvc.login success`);
                        return storage.setItem('tokens', svc.tokens).then(getUser);
                    } else {
                        console.log(`authSvc.login failed ${JSON.stringify(response.data)}`);
                        return $q.reject(response.data);
                    }
                })
                    .catch(function (err) {
                        delete svc.$login;
                        console.error(`authSvc.login ERROR ${err.statusText}`);
                        return $q.reject(err);
                    });
            });
        }
        return svc.$login;
    }

    svc.refreshAccessToken = function () {
        if (!svc.$refreshAccessToken) {
            svc.$refreshAccessToken = $http.post(webServiceUrl.api('Account/RefreshToken'), { refreshToken: svc.tokens.refreshToken }).then(response => {
                delete svc.$refreshAccessToken;
                if (response.data.success) {
                    svc.tokens = response.data.result;
                    console.log(`authSvc.refreshAccessToken success`);
                    return storage.setItem('tokens', svc.tokens).then(getUser);
                } else {
                    console.log(`authSvc.refreshAccessToken failed ${JSON.stringify(response.data)}`);
                    return $q.reject(response);
                }
            }).catch(err => {
                delete svc.$refreshAccessToken;
                svc.user = anonymousUser;
                return storage.removeItem('tokens').then(() => svc.user);
            });
        }
        return svc.$refreshAccessToken;
    }

    svc.canReAuthorise = function () {
        return !svc.$refreshAccessToken;
    }

    svc.reAuthorise = function () {
        if (!$state.includes('login')) {
            var returnUrl = $state.href($state.current.name, $state.params);

            // not in the login state, we may need to login or refresh the accessToken
            if (!svc.user.authenticated && !svc.tokens.refreshToken) {
                console.log(`reAuthorise returnUrl = ${returnUrl}`);
                $state.go('login', { returnUrl });
            } else if (svc.tokens.refreshToken) {
                // we have a refreshToken ..
                return svc.refreshAccessToken().then(() => {
                    if (!svc.user.authenticated) {
                        console.log(`reAuthorise returnUrl = ${returnUrl}`);
                        $state.go('login', { returnUrl });
                    }
                    return svc.user.authenticated;
                })
            }
        }
        return Promise.resolve(false);
    }
    /*
            svc.changePassword = function (oldPassword, newPassword, confirmPassword) {
                var d = $q.defer();
    
                $http.post(rootUrl.API(1) + 'Account/ChangePassword', {
                    OldPassword: oldPassword,
                    NewPassword: newPassword,
                    ConfirmPassword: confirmPassword
                })
                    .success(function (data) {
                        if (data) {
                            d.resolve({ success: true });
                        } else
                            d.reject({ success: false });
                    })
                    .error(function () {
                        d.reject({ success: false });
                    });
    
                return d.promise;
            }
    */
    svc.logout = function () {
        return storage.removeItem('tokens').then(() => {
            return $http.post(webServiceUrl.api('Account/Logout'))
                .then(function (response) {
                    svc.user = anonymousUser;
                    $rootScope.$emit('auth-logout');
                    return {
                        success: true
                    };
                })
                .catch(function (err) {
                    svc.user = anonymousUser;
                    return $q.reject(err);
                });
        });
    }

    return svc;
});
