'use strict';

import app from '../../ngmodule';

app.run(function ($transitions, $state) {
    $transitions.onStart({ to: 'root.**' }, function (trans) {
        var authSvc = trans.injector().get('authSvc');
        return authSvc.isAuthenticated().then(authenticated => {
            if (!authenticated) {
                // User isn't authenticated. Redirect to a new Target State
                return trans.router.stateService.target('login', { returnUrl: $state.href(trans.to().name, trans.params()) });
            } else {
                return true;
            }
        });
    })
});
