﻿'use strict';

import app from './ngmodule';
import angular from 'angular';
import './services/transition-cache';

app.directive('autoLogoutPanel', ['autoLogoutSvc', 'ngToast', '$sce', '$window', function (autoLogoutSvc, ngToast, $sce, $window) {
    return {
        templateUrl: 'auto-logout-panel.html',
        restrict: 'E',
        scope: {},
        controller: function ($scope) {
            $scope.svc = autoLogoutSvc;

            var _toast = null;
            $scope.$watch('svc.autoLogoutTime', function (newValue, oldValue) {
                if (newValue == null) {
                    return;
                }
                if (newValue == autoLogoutSvc.logoutShortSecs) {
                    $scope.popToast();
                } else if (newValue > autoLogoutSvc.logoutShortSecs) {
                    if (_toast) {
                        ngToast.dismiss(_toast);
                        _toast = null;
                    }
                }
            })

            $scope.userExtend = function () {
                autoLogoutSvc.longRestart(true);
            }

            $scope.logoutNow = function () {
                autoLogoutSvc.logoutNow();
            }

            $window.addEventListener('touchmove', function () {
                autoLogoutSvc.shortRestart(false);
            });

            $scope.popToast = function () {
                if (_toast) {
                    return;
                }

                _toast = ngToast.create({
                    content: $sce.trustAsHtml('<div class="ng-cloak" style="width: 50vw"><div class="row-fluid"><div class="span12">Log out in {{ svc.autoLogoutTime }}</div></div><div class="row-fluid"><div class="span6"><button class="btn btn-large" ng-click="userExtend()">Delay Logout</button></div><div class="span6"><button class="btn btn-large" ng-click="logoutNow()">Logout Now</button></div></div>'),
                    compileContent: $scope,
                    horizontalPosition: 'center',
                    dismissOnClick: false,
                    dismissOnTimeout: false
                });
            }
        }
    }
}]);


app.directive('helpIntercept', [function () {
    return {
        restrict: 'A',
        link: function (scope, elt, attrs) {
            // this will get called if we are in single dashboard mode, the help intercept will get it
            scope.$on('help_start', function (event, args) {
                // we need to call down to get the options we are going to show
                var steps = [];
                scope.$broadcast('help_add_steps', steps);

                var intro = introJs();

                intro.setOptions({
                    steps: steps
                });

                intro.start();
            });
        }
    };
}]);
app.directive('helpLink', [function () {
    return {
        restrict: 'A',
        link: function (scope, elt, attrs) {
            elt.bind('click', function () {
                scope.$emit('help_start_current_tab', {});
            });
        }
    };
}]);

// simple filter to format a duration (in seconds) in a human readable way
app.filter('duration', [function () {
    return function (seconds, format) {
        var s = seconds;
        if (typeof seconds == 'string') {
            s = parseFloat(s);
        }

        var f = 'full';

        if (angular.isDefined(format)) {
            f = format.toLowerCase();
        }

        var dur = moment.duration(s, "seconds");

        switch (f) {
            case 'ashours':
                return dur.asHours() + ' hrs';
                break;
            case 'asmins':
                return dur.asMinutes() + ' mins';
            case 'asminutes':
                break;
            case 'assecs':
                return dur.asSeconds() + ' secs';
            case 'asseconds':
                break;
            case 'full':
                return dur.hours() + ' hrs ' + dur.minutes() + ' mins ' + dur.seconds() + ' secs';
                break;
        }
    };
}]);

// simple filter to total the values of a property in an array. So for example
// if you have the array somearray = [{num:10},{num:20},{num:30}] and you did
// somearray | total:'num' you'd get the sum of the values in num (60).
app.filter('total', function () {
    return function (input, property) {
        var i = input instanceof Array ? input.length : 0;
        if (typeof property === 'undefined' || i === 0) {
            return i;
        } else if (isNaN(input[0][property])) {
            throw 'filter total can count only numeric values';
        } else {
            var total = 0;
            while (i--)
                total += input[i][property];
            return total;
        }
    };
});

app.factory('beaconListSvc', ['$injector', '$window', function ($injector, $window) {

    function beaconAsset(asset) {
        if (!asset) {
            return asset;
        }
        if (angular.isDefined(asset.beacon)) {
            return asset;
        }

        var assetId = asset.alldata.Id;
        if (angular.isDefined(assetId)) {
            var beaconId = asset.alldata.BeaconId;
            if (angular.isDefined(beaconId)) {
                var beacon = svc.beacons.find(function (b) {
                    return b.beaconId == beaconId;
                });

                asset.hasBeacon = angular.isDefined(beacon);
                asset.beacon = function () {
                    return beacon;
                }
            }
        }
        return asset;
    }

    var svc = {
        beaconAsset: beaconAsset
    };

    if ($window.razordata.environment == 'mobile') {
        var beaconSvc = $injector.get('beaconSvc');
        svc.beacons = beaconSvc.getBeacons();
    } else {
        svc.beacons = [];
        svc.notSupported = true;
    }

    return svc;
}]);

app.controller('beaconListCtrl', ['$scope', 'beaconListSvc', function ($scope, beaconListSvc) {
    $scope.beacons = beaconListSvc.beacons;
    $scope.notSupported = beaconListSvc.notSupported;
    $scope.beaconAsset = beaconListSvc.beaconAsset;
}])

app.factory('geolocation-registry', [function () {
    var _byname = {};
    var _byindex = [];

    return {
        byname: _byname,
        byindex: _byindex,

        add: function (id, type, name, position) {
            var source = _byname[id];
            if (!source) {
                source = {
                    id: id,
                    type: type,
                    name: name,
                    position: position
                }
                _byname[id] = source;
                _byindex.push(source);
            }
            return source;
        }
    }
}]);

app.factory('jsFunctionLineItemPrice', [function () {
    return {
        calculate: function (item) {
            return item.quantity * item.price * (1.0 - item.discount + item.surcharge);
        },

        // function to allow quantity based price ranges, eg
        // quantitySlide(110, [
        //    [[0, 9], 1.2],          // 0..9 items @ $1.2
        //    [[10, 49], 1.1],        // 10..49 items @ $1.1
        //    [[50, 99], 1.0],        // 50..99 items @ $1.0
        //    [[100], 0.95])          // 100.. items @ $0.95
        //
        // quantitySlide(110, [
        //    [[0, 9], [1.2, '0..9'],          // 0..9 items @ $1.2
        //    [[10, 49], [1.1, '10..49'],        // 10..49 items @ $1.1
        //    [[50, 99], [1.0, '50..99'],        // 50..99 items @ $1.0
        //    [[100], [0.95, '100+'],          // 100.. items @ $0.95
        //    notes);
        // 
        quantitySlide: function (quantity, range, notes) {
            if (angular.isObject(quantity) && quantity.hasOwnProperty('quantity')) {
                quantity = quantity.quantity;
            }

            var price = 0;
            var q = quantity;
            var lastHigh = 0;
            var highestRange;
            range.forEach(function (r) {
                var lowHigh = r[0];
                var p = angular.isNumber(r[1]) ? r[1] : r[1][0];

                if (q >= lowHigh[0]) {
                    highestRange = r;
                    if (lowHigh.length == 2 && q > lowHigh[1]) {
                        price += (lowHigh[1] - lastHigh) * p;
                    } else {
                        price += (q - lastHigh) * p;
                    }
                    price = Math.round(price * 100) / 100.0;
                }
                if (lowHigh.length >= 2) {
                    lastHigh = lowHigh[1];
                }
            });

            if (angular.isArray(notes) && angular.isDefined(highestRange) && angular.isArray(highestRange[1]) && highestRange[1].length >= 2) {
                notes.push(highestRange[1][1]);
            }

            return price;
        }
    };
}]);

app.factory('jsFunctionSvc', ['$http', 'sectionUtils', '$injector', '$window', function ($http, sectionUtils, $injector, $window) {
    var svc = {
        getScript: function () {
            return $http({
                url: $window.razordata.siteprefix + 'api/v1/JsFunction/GetScript'
            }).then(function (response) {
                var fns = Object.create(null);
                fns.byId = Object.create(null);

                response.data.functions.forEach(function (d) {
                    var fn = eval('(' + d.script + ')');
                    var invokeableFn = function (locals) {
                        locals['functionId'] = d.id;
                        locals['js' + d.type] = fns[d.type]; // Allow a function to DI inject another by name of same type (eg LineItemPrice)
                        locals['jsFunctions'] = fns; // Allow a function to DI inject all functions (allows utility functions to be defined)
                        return $injector.invoke(fn, $injector.get('jsFunction' + d.type), locals);
                    };

                    // Function is callable by fns.byId[3]
                    fns.byId[d.id] = invokeableFn;

                    // Function is also callable by fns.LineItemPrice["50% Off"]
                    fns[d.type] = fns[d.type] || Object.create(null);
                    fns[d.type][d.name] = invokeableFn;
                });
                return {
                    functions: fns,
                    utcnow: moment(response.data.utcnow).utc()
                };
            });
        }
    };
    return svc;
}]);