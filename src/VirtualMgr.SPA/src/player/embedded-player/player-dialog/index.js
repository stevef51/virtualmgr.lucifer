'use strict';

import app from '../../ngmodule';

app.config(function ($mdDialogProvider) {
    $mdDialogProvider.addPreset('embeddedPlayerDialog', {
        options: function () {
            return {
                parent: angular.element(document.body),
                fullscreen: true,
                clickOutsideToClose: true,
                multiple: true,                     // Does not close any parent dialog

                template: require('./template.html').default,
                controller: ['$scope', '$mdDialog', 'locals', 'webServiceUrl', 'playerButtons', 'playerActions', 'ChecklistState', function ($scope, $mdDialog, locals, webServiceUrl, playerButtons, playerActions, ChecklistState) {
                    $scope.workingdocumentid = locals.workingdocumentid;
                    $scope.title = locals.title;
                    $scope.mediaId = locals.mediaId;
                    $scope.parent = locals.parent;
                    $scope.presented = locals.presented;
                    $scope.url = webServiceUrl;
                    $scope.playerButtons = playerButtons;
                    $scope.playerActions = playerActions;
                    $scope.cancel = () => $mdDialog.cancel();
                    $scope.close = data => $mdDialog.hide(data);
                    $scope.onState = state => {
                        if (state === ChecklistState.Finished) {
                            $mdDialog.hide(state);
                        }
                    }
                }]
            }
        }
    })
})
