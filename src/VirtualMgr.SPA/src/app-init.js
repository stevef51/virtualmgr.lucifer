'use strict';

import player from './player';
import angular from 'angular';
import './routes';
import './config';
import './views';
import './body.scss';
import '@fortawesome/fontawesome-free/js/all';
import 'typeface-roboto';

// Make Angular _ available to anyone
import './utils/angularise-libraries';

import LogRocket from 'logrocket';
import { $injector } from '@uirouter/core';

const environment = process.env.NODE_ENV;
console.log(`Environment is ${environment}`);

if (environment === `production`) {
    LogRocket.init(`spho80/lucifer`);
}

var loc = window.location;
var scheme = loc.protocol;

/*
// Uncomment if using sub-domain services
var tenantHostname = loc.hostname.replace(/^spa\./, '');
window.razordata = {
    apiprefix: `${scheme}//api.${tenantHostname}/`,
    odataprefix: `${scheme}//odata.${tenantHostname}/`,
    siteprefix: `${scheme}//site.${tenantHostname}`,                // This should be deprecated I think
    mediaprefix: `${scheme}//media.${tenantHostname}/`,
    environment: `desktop`
};
*/

// Uncomment if using sub-folder services
var tenantHostname = loc.hostname;
window.razordata = {
    apiprefix: `${scheme}//${tenantHostname}/api/`,
    odataprefix: `${scheme}//${tenantHostname}/odata/`,
    siteprefix: `${scheme}//${tenantHostname}/spa/`,                // This should be deprecated I think
    mediaprefix: `${scheme}//${tenantHostname}/media/`,
    environment: `desktop`
};

function loadScript(url) {
    return new Promise(function (resolve, reject) {
        var scriptTag = document.createElement(`script`);
        scriptTag.src = url;
        scriptTag.onload = resolve;
        scriptTag.onerror = reject;
        var head = document.getElementsByTagName(`head`)[0];
        head.appendChild(scriptTag);
    });
}

let init$ = new Promise(function (resolve, reject) {

    if (window.webkit && window.webkit.messageHandlers) {
        console.log(`Running in iOS VMPlayer, loading cordova.js ..`);

        document.addEventListener('deviceready', function () {
            resolve();
        }, false);

        loadScript(`cordova/ios/cordova.js`);
    } else if (typeof window._cordovaNative !== 'undefined') {
        // Not on iOS VMPlayer, this should be Android VMPlayer
        console.log(`Running in Android VMPlayer, loading cordova.js ..`);

        document.addEventListener('deviceready', function () {
            resolve();
        }, false);

        loadScript(`cordova/android/cordova.js`);
    } else {
        resolve();
    }
});

export default init$;
