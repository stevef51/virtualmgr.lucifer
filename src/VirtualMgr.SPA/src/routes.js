'use strict';

import app from './player/ngmodule';

app.config(
    function ($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state('root', {
                abstract: true,
                url: '',
                views: {
                    'body@': {
                        component: 'bodyCtrl'
                    },
                    'header-control@root': {
                        component: 'rootHeaderCtrl'
                    },
                    'left-control@root': {
                        component: 'rootLeftCtrl'
                    },
                    'content-control@root': {
                        component: 'rootContentCtrl'
                    },
                    'header-right@root': {
                        component: 'rootHeaderRightCtrl'
                    },
                    'header-left@root': {
                        component: 'rootHeaderLeftCtrl'
                    }
                }
            })

            .state('login', {
                url: '/login?returnUrl',
                views: {
                    'body@': {
                        component: 'loginCtrl'
                    }
                }
            })

        $urlRouterProvider.otherwise('/dashboard');

    }
);


