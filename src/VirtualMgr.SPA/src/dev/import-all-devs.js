'use strict';

const nameRex = new RegExp('^\.\/(.*)\/');

function importAll(r) {
    return r.keys().map(k => {
        var match = nameRex.exec(k);
        if (match) {
            return {
                module: k,
                name: match[1],
                export: r(k).default
            }
        }
    });
}

var questions = importAll(require.context('../', true, /dev\.jsd$/)).filter(q => !!q);

export default questions;