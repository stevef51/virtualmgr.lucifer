'use strict';

import app from '../../player';
import devQuestions from '../import-all-devs';
import '../../ace';

app.component('devQuestionContentCtrl', {
    template: require('./template.html').default,
    controller: function ($scope, $stateParams, $injector, persistantStorage) {
        this.name = $stateParams.question;
        var module = devQuestions.find(q => q.name === this.name);

        this.compile = module.export.compile;

        var originalInput = () => {
            return angular.copy(module.export.input, {});
        };

        var makeInput = input => {
            // Give the module a chance to modify the model (useful for $sce.trustAsHtml)
            if (module.export.makeInput) {
                $injector.invoke(module.export.makeInput, input);
            }
            if (angular.isUndefined(this.input)) {
                this.input = input;
            } else {
                angular.merge(this.input, input);
            }
        }
        makeInput(originalInput());

        this.pageScope = {};

        this.output = {};
        if (module.export.makeOutput) {
            $injector.annotate(module.export.makeOutput, false);
        }
        var makeOutput = () => {
            if (module.export.makeOutput) {
                return $injector.invoke(module.export.makeOutput, this.output, { input: this.input });
            }
        }

        // Reset the model to original value
        this.resetInput = () => {
            this.inputText = JSON.stringify(originalInput(), null, '\t');
        }

        // Retrieve stored model
        persistantStorage.getItem(`dev-${this.name}.input`, text => {
            if (text) {
                this.inputText = text;
            } else {
                this.resetInput();
            }
        });

        // Setup the ACE editor
        this.aceInputOptions = {
            mode: 'json',
            theme: 'monokai',
            onChange: e => {
                try {
                    makeInput(JSON.parse(this.inputText));

                    persistantStorage.setItem(`dev-${this.name}.input`, this.inputText);
                } catch {
                }
            }
        };

        this.aceOutputOptions = {
            mode: 'json',
            theme: 'monokai'
        };

        this.$doCheck = () => {
            this.outputText = JSON.stringify(makeOutput(), null, '\t');
        }
    }
})