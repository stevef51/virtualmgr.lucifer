'use strict';

import app from '../../player';
import devQuestions from '../import-all-devs';

app.component('devQuestionMenuCtrl', {
    template: require('./template.html').default,
    controller: function () {
        this.questions = devQuestions;
    }
})