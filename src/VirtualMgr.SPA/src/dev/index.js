'use strict';

import app from '../player';
import './menu';
import './content';
import './import-all-devs';

app.config(function ($stateProvider) {
    $stateProvider
        .state('root.dev', {
            url: '/dev',
            views: {
                'menu@root': {
                    component: 'devQuestionMenuCtrl'
                }
            }
        })
        .state(`root.dev.question`, {
            url: '/{question}',
            views: {
                'header-control@root': {
                    template: ''
                },
                'content-control@root': {
                    component: 'devQuestionContentCtrl'
                }
            }
        });
});