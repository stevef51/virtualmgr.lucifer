'use strict';

import app from '../ngmodule';

app.directive('qrCodeScanner', ['$timeout', function ($timeout) {
    return {
        template: function (tElement, tAttrs) {
            if (angular.isDefined(window.cordova) && angular.isDefined(cordova.plugins) && angular.isDefined(cordova.plugins.barcodeScanner)) {
                if (tElement[0].children.length) {
                    return tElement[0].children[0];
                } else {
                    return require('./template.html').default;
                }
            } else {
                return '';
            }
        },
        restrict: 'E',
        require: "ngModel",
        scope: {
            model: '=ngModel',
            onScan: '&',
            onError: '&',
            onResult: '&',
            onCancel: '&',
            options: '=',
            title: '@'
        },
        link: function (scope, element, attrs) {
            scope.title = scope.title || 'Scan';
            scope.options = angular.extend({
                preferFrontCamera: false,
                showFlipCameraButton: false,
                showTorchButton: true
            }, scope.options || {});

            scope.scan = function () {
                cordova.plugins.barcodeScanner.scan(
                    function (result) {
                        $timeout(function () {
                            if (!result.cancelled) {
                                scope.model = result.text;
                                scope.onScan({
                                    text: result.text
                                });
                            } else {
                                scope.onCancel();
                            }
                            scope.onResult({
                                result: result
                            });
                        });
                    },
                    function (error) {
                        scope.onError(error);
                    },
                    scope.options
                );
            }
        }
    }
}])

