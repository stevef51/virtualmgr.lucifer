'use strict';

import app from '../ngmodule';
import 'angular-cache';
import base64 from 'base64-arraybuffer';

app.component('mediaImage', {
    template: require('./template.html').default,
    restrict: 'E',
    bindings: {
        ngModel: '<',
        width: '<',
        height: '<',
        type: '<'
    },
    controller: function ($scope, $element, $attrs, webServiceUrl, $http, CacheFactory) {
        const imageCache = CacheFactory.get('media-image-cache') || CacheFactory.createCache('media-image-cache', {
            capacity: 50,
            deleteOnExpire: 'none',
            maxAge: 30000,
            storageMode: 'localStorage',
            storeOnResolve: true
        });

        const self = this;
        const img = $element.find('img')[0];
        const useCache = $attrs.cache != null;

        self.buildImageUrl = function () {
            let mediaId = self.ngModel || $attrs.mediaId;
            if (self.ngModel) {
                var makeUrl = webServiceUrl.mediaImage;

                switch (self.type) {
                    case "media":
                        makeUrl = webServiceUrl.mediaImage;
                        break;
                    case "membership":
                    case "user":
                        makeUrl = webServiceUrl.membershipImage;
                        break;
                    case "tasktype":
                        makeUrl = webServiceUrl.taskTypeImage;
                        break;
                }

                let opts = {
                    format: 'png',
                    width: self.width,
                    height: self.height,
                    cache: useCache
                }

                const url = makeUrl(mediaId, opts);
                if (self.url !== url) {
                    self.url = url;
                    self.loading = true;

                    // If we are using the cache then try to fetch the response directly first, Angular caching of binary responses seems to be
                    // overlooked as the cache requires responses to be JSON serializable which an ArrayBuffer is not, requesting the default (text)
                    // format does serialize correctly but the binarised string seems to be unworkable (ie there is nothing that can convert it something useful)                    
                    let result$;
                    if (useCache) {
                        let cacheResult = imageCache.get(url);
                        if (cacheResult != null) {
                            result$ = Promise.resolve(cacheResult);
                        }
                    }
                    if (!result$) {
                        result$ = $http.get(url, { responseType: 'arraybuffer' }).then(function (response) {
                            // We only need the base64 data and the contentType of the image, cache these
                            var imageData = {
                                base64: base64.encode(response.data),
                                contentType: response.headers()['content-type']
                            }
                            imageCache.put(url, imageData);
                            return imageData;
                        });
                    }

                    result$.then(function (imageData) {
                        // imageData could come from the cache or a fresh server hit
                        img.src = `data:${imageData.contentType};base64,${imageData.base64}`;
                        self.loading = false;
                    })
                }
            }
        }

        self.$onChanges = function (changes) {
            if (changes.ngModel || changes.width || changes.height || changes.type) {
                self.buildImageUrl();
            }
        }

        self.buildImageUrl();
    }
});
