'use strict';

export default {
    compile: `<media-image ng-model='$ctrl.input.mediaId' height='$ctrl.input.height' width='$ctrl.input.width'></media-image>`,
    input: {
        mediaId: '12f4e160-b3a5-42dc-b8db-a9de04ec4a78',
        height: 75,
        width: undefined
    }
}