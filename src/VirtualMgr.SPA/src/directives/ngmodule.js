'use strict';

import angular from 'angular';

export default angular.module('vm-directives', ['ngMaterial', 'vm-services']);
