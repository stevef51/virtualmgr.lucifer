'use strict';

import app from '../ngmodule';
import './style.scss';
import { stringify } from '@uirouter/core';

class ButtonBuilder {
    constructor(id) {
        this.button = {};
        this.button.id = id;
        this.button._visible = false;
        this.button.visible = () => this.button._visible;
        this.button.disabled = () => false;
        this.flex(33);
    }
    build() {
        return this.button;
    }
    text(text) {
        this.button._visible = true;
        this.button.text = text.toString();
        return this;
    }
    icon(font, icon) {
        this.button._visible = true;
        this.button.icon = { font, icon };
        return this;
    }
    cls(cls) {
        if (cls) {
            this.button.class = this.button.class || {};
            this.button.class[cls] = true;
        }
        return this;
    }
    click(click) {
        this.button.click = click.bind(this.button);
        return this;
    }
    flex(flex) {
        this.button.flex = flex;
        return this;
    }
    disabled(fn) {
        this.button.disabled = fn.bind(this.button);
        return this;
    }
    visible(fn) {
        this.button.visible = fn.bind(this.button);
        return this;
    }
}

app.component('materialKeypad', {
    restrict: 'E',
    require: {
        ngModelCtrl: 'ngModel'
    },
    bindings: {
        text: '=ngModel',
        step: '<',
        keypadLayout: '<'
    },
    template: require('./template.html').default,
    controller: function () {
        this.$onInit = () => {
            const self = this;

            const disableDigit = (digit) => {
                if (self.step) {
                    let places = 0;
                    let step = self.step;
                    while (step !== Math.floor(step)) {
                        step *= 10;
                        places++;
                    }

                    let dp = self.text.indexOf('.');
                    if (dp > 0) {
                        let check = self.text.replace('.', '') + digit;
                        let left = check.length - dp;
                        if (left > places) {
                            return true;
                        }

                        let n = Number(check + '0'.repeat(places - left));
                        return (n % step !== 0);
                    }
                }
                return false;
            }
            const textButton = text => new ButtonBuilder(text)
                .text(text)
                .click(function (event) {
                    self.text += this.text;
                })

            const numberButton = number => textButton(number)
                .disabled(function (event) {
                    return disableDigit(this.text);
                });

            const iconButton = (font, icon) => new ButtonBuilder(icon)
                .icon(font, icon);

            const backspaceButton = () => iconButton('fas', 'fa-backspace fa-lg')
                .cls('md-accent')
                .click(function (event) {
                    if (self.text.length) {
                        self.text = (self.text || '').substr(0, self.text.length - 1);
                    }
                })
                .disabled(() => !self.text);

            const decimalButton = () => textButton('.')
                .disabled(function () {
                    return (self.text || '').indexOf('.') >= 0;
                })
                .visible(function (event) {
                    return self.keypadLayout === 'decimal';
                });

            const normalKeypad = [
                [numberButton(1).build(), numberButton(2).build(), numberButton(3).build()],
                [numberButton(4).build(), numberButton(5).build(), numberButton(6).build()],
                [numberButton(7).build(), numberButton(8).build(), numberButton(9).build()],
                [backspaceButton().build(), numberButton(0).build(), decimalButton().build()]
            ];

            self.keypad = normalKeypad;
        }
    }
});
