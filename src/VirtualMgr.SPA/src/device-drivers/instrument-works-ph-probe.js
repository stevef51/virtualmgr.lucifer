﻿'use strict';

import angular from 'angular';
import '../utils/vmng-utils';
import '../utils/bootstrap-compat';
import moment from 'moment';

export default angular.module('instrument-works-pH-probe', ['vmngUtils', 'bootstrapCompat'])

    .constant('GATT', {
        services: {
            battery_service: '180F',
            device_information: '180A'
        },
        characteristics: {
            battery_level: '2A19',
            manufacturer_name_string: '2A29',
            model_number_string: '2A24',
            serial_number_string: '2A25',
            hardware_revision_string: '2A27',
            firmware_revision_string: '2A26',
            software_revision_string: '2A28',
            system_id: '2A23',
            ieee_11073_20601_regulatory_certification_data_list: '2A2A',
            pnp_id: '2A50'
        }
    })

    .factory('iOSInstrumentWorksBLEScanner', ['$log', 'ngTimeout', 'actionQueue', 'GATT', 'binUtils', function ($log, ngTimeout, actionQueue, GATT, binUtils) {
        var _bts = new cordova.plugins.VirtualManagerBLE.Client('pH-probe', {
            deleteExistingClient: true
        });

        // On start-up Stop Native code from scanning - it is possible that page refresh/logout etc could leave Native
        // code scanning in background when new page JS has not requested scan, Cordova will throw away unrecognised Native callbacks
        // but still leaves high CPU usage for no gain
        _bts.stopScanning();

        var PrimaryServiceUUID = '623001E0-9A90-11E3-A5E2-0800200C9A66';
        var pHCharacteristicUUID = '623001E1-9A90-11E3-A5E2-0800200C9A66';
        var TemperatureCharacteristicUUID = '623001E2-9A90-11E3-A5E2-0800200C9A66';
        var SensorConfigurationCharacteristicUUID = '623001E3-9A90-11E3-A5E2-0800200C9A66';
        var ReportPeriodCharacteristicUUID = '623001E4-9A90-11E3-A5E2-0800200C9A66';
        var SensorStatusCharacteristicUUID = '623001E5-9A90-11E3-A5E2-0800200C9A66';

        $log.info('instrument-works-pH-probe-svc starting up ..');

        var q = actionQueue();

        function iOSProbe(p) {
            this.peripheral = p;
            this.id = p.id;
        }

        iOSProbe.prototype.connect = function (success, error) {
            var self = this;
            var p = self.peripheral;
            if (p.connected) {
                success(p.connected);
            } else {

                self.characteristics = Object.create(null);
                p.connect(function (msg) {
                    // Connect & spurious Disconnect come through here
                    if (!p.connected) {
                        success(p.connected);
                    } else {
                        p.discoverServices([], function (services) {
                            var primaryService = p.services[PrimaryServiceUUID];
                            if (primaryService) {
                                primaryService.discoverCharacteristics([], function () {
                                    self.characteristics.pH = primaryService.characteristics[pHCharacteristicUUID];
                                    self.characteristics.temperature = primaryService.characteristics[TemperatureCharacteristicUUID];
                                    self.characteristics.sensorConfiguration = primaryService.characteristics[SensorConfigurationCharacteristicUUID];
                                    self.characteristics.reportPeriod = primaryService.characteristics[ReportPeriodCharacteristicUUID];
                                    self.characteristics.sensorStatus = primaryService.characteristics[SensorStatusCharacteristicUUID];

                                    var battery_service = p.services[GATT.services.battery_service];
                                    if (battery_service) {
                                        battery_service.discoverCharacteristics([], function () {
                                            self.characteristics.battery_level = battery_service.characteristics[GATT.characteristics.battery_level];

                                            var device_informationService = p.services[GATT.services.device_information];
                                            if (device_informationService) {
                                                device_informationService.discoverCharacteristics([], function () {
                                                    self.characteristics.serial_number_string = device_informationService.characteristics[GATT.characteristics.serial_number_string];
                                                    self.characteristics.hardware_revision_string = device_informationService.characteristics[GATT.characteristics.hardware_revision_string];
                                                    self.characteristics.firmware_revision_string = device_informationService.characteristics[GATT.characteristics.firmware_revision_string];
                                                    self.characteristics.manufacturer_name_string = device_informationService.characteristics[GATT.characteristics.manufacturer_name_string];
                                                    self.characteristics.model_number_string = device_informationService.characteristics[GATT.characteristics.model_number_string];

                                                    success(p.connected);
                                                })
                                            }
                                        })
                                    }
                                });
                            }
                        });
                    }
                }, error);
            }

            // Return a function that when called will Disconnect 
            return function () {
                self.disconnect();
            }
        }

        iOSProbe.prototype.disconnect = function (success, error) {
            return this.peripheral.disconnect(success, error);
        }

        function checkCharacteristic(self, characteristic, success, error) {
            if (!self.peripheral.connected) {
                if (error) {
                    error('Not connected');
                }
                return;
            }
            if (!self.characteristics[characteristic]) {
                if (error) {
                    error(characteristic + ' characteristic not discovered');
                }
                return;
            }
            return success();
        }

        function readCharacteristic(characteristic, canNotify, transform) {
            transform = transform || angular.identity;
            return function (success, error, notify) {
                var self = this;
                return checkCharacteristic(self, characteristic, function () {
                    if (notify && canNotify) {
                        self.characteristics[characteristic].subscribeRead(function (raw) {
                            success(transform(raw));
                        }, error);

                        return function () {
                            self.characteristics[characteristic].unsubscribeRead();
                        }
                    } else {
                        self.characteristics[characteristic].read(function (raw) {
                            success(transform(raw));
                        }, error);

                        return function () { }
                    }
                }, error);
            }
        }

        function writeCharacteristic(characteristic, transform) {
            transform = transform || angular.identity;
            return function (data, success, error) {
                var self = this;
                return checkCharacteristic(self, characteristic, function () {
                    return self.characteristics[characteristic].write(transform(data), success, error);
                }, error);
            }
        }

        iOSProbe.prototype.readBatteryLevel = readCharacteristic('battery_level', false);
        iOSProbe.prototype.readSerialNumber = readCharacteristic('serial_number_string', false);
        iOSProbe.prototype.reportPeriodWrite = writeCharacteristic('reportPeriod');
        iOSProbe.prototype.pHValueRead = readCharacteristic('pH', true);
        iOSProbe.prototype.temperatureValueRead = readCharacteristic('temperature', true);

        q(function (next) {
            _bts.subscribeStateChange(function (state) {
                $log.info('subscribeStateChange = ' + state);
                if (state == 'PoweredOn') {
                    next();
                }
            });
        });

        var probes = [];
        var svc = {
            startScanning: function (success, error) {
                q(function (next) {
                    // Inform caller we have no probes initially
                    probes = [];
                    success(probes);

                    _bts.startScanning([], {
                        allowDuplicate: true,
                        groupTimeout: 500,
                        groupSize: 0
                    }, function (peripherals) {
                        var before = probes.length;
                        if (peripherals) {
                            var blacklistIds = [];
                            angular.forEach(peripherals, function (p) {
                                p.timestamp = moment();
                                var findProbe = probes.find(function (f) {
                                    return f.peripheral.id == p.id;
                                });
                                if (angular.isUndefined(findProbe)) {
                                    if ((p.name == "pH Meter" || p.name == "pH-Meter") && p.advertisement && p.advertisement.connectable) {
                                        var probe = new iOSProbe(p);
                                        probes.push(probe);
                                    } else {
                                        blacklistIds.push(p);
                                    }
                                }
                            });
                            if (blacklistIds.length && angular.isFunction(_bts.blacklist)) {
                                _bts.blacklist(blacklistIds);
                            }
                        }

                        // Check expired scans
                        var now = moment();
                        var expireTime = moment(now).subtract(3, 'seconds');

                        probes = probes.filter(function (probe) {
                            return probe.peripheral.timestamp.isAfter(expireTime);
                        });

                        if (before != probes.length) {
                            success(probes);
                        }
                        next();
                    }, error);
                });
            },

            stopScanning: function (success, error) {
                q(function (next) {
                    probes = [];
                    _bts.stopScanning(function () {
                        next();
                        if (success) {
                            success();
                        }
                    }, function (msg) {
                        next();
                        if (error) {
                            error(msg);
                        }
                    });
                });
            }
        }

        return svc;
    }])

    .factory('InstrumentWorksBLEScanner', ['$injector', function ($injector) {
        if (window.cordova && window.cordova.plugins && window.cordova.plugins.VirtualManagerBLE && window.cordova.plugins.VirtualManagerBLE.Client) {
            return $injector.get('iOSInstrumentWorksBLEScanner');
        } else {
            return {
                notsupported: true
            }
        }
    }])

    .factory('InstrumentWorksPHProbe', ['binUtils', 'ngTimeout', '$q', 'ConnectableDevice', function (binUtils, ngTimeout, $q, ConnectableDevice) {
        var _0degreesKelvin = 273.15;
        var TidealKelvin = _0degreesKelvin + 25;

        function probe(probeImpl) {
            ConnectableDevice(this, probeImpl.id, '', null);
            this.probeImpl = probeImpl;
            this.zero = 0;
            this.slope = 1;
            this.calibrationStatus = 0;
        }

        probe.prototype.connect = function (success, error) {
            var self = this;
            if (!self.isError() && !self.isDisconnected()) {
                if (error) {
                    error(self.statusText);
                }
                return;
            }

            self.connecting();
            return this.probeImpl.connect(function (connected) {
                var callArgs = Array.prototype.slice.call(arguments);

                if (connected) {
                    self.connected();
                } else {
                    self.disconnected();
                }
                if (success) {
                    success.apply(null, callArgs);
                }
            }, function (msg) {
                self.error(msg);
                if (error) {
                    error(msg);
                }
            });
        };

        probe.prototype.disconnect = function (success, error) {
            var self = this;
            if (!self.isError() && !self.isConnected()) {
                if (error) {
                    error(self.statusText);
                }
                return;
            }

            self.disconnecting();
            return this.probeImpl.disconnect(function () {
                var callArgs = Array.prototype.slice.call(arguments);
                self.disconnected();
                if (success) {
                    success.apply(null, callArgs);
                }
            }, function (msg) {
                self.error(msg);
                if (error) {
                    error(msg);
                }
            });
        };

        probe.prototype.readSerialNumber = function (success, error) {
            var self = this;
            if (!self.isConnected()) {
                if (error) {
                    error(self.statusText);
                }
                return;
            }
            return this.probeImpl.readSerialNumber(function (raw) {
                self.serialNumber = binUtils.UTF8String(raw);
                success(self.serialNumber);
            }, error);
        };

        probe.prototype.readBatteryLevel = function (success, error) {
            var self = this;
            if (!self.isConnected()) {
                if (error) {
                    error(self.statusText);
                }
                return;
            }
            return this.probeImpl.readBatteryLevel(function (raw) {
                self.batteryLevel = raw[0];
                success(self.batteryLevel);
            }, error);
        };

        // pH = 7 - (mV - zero) / (slope * 59.16);
        // therefore
        // slope = (-mV - zero) / (59.16 * (pH - 7))
        function fnSlope(mv, ph, zero) {
            return (-mv - zero) / (59.16 * (ph - 7));
        }

        probe.prototype.setCalibration = function (data) {
            this.zero = data.zero;
            this.slope = data.slope;
            this.lastCalibratedOnUtc = data.lastCalibratedOnUtc;
            this.calibrationStatus = moment().subtract(7, 'days').isAfter(this.lastCalibratedOnUtc) ? 1 : 2;
        }

        probe.prototype.getCalibration = function () {
            return {
                zero: this.zero,
                slope: this.slope,
                lastCalibratedOnUtc: this.lastCalibratedOnUtc
            };
        }

        probe.prototype.calibrate = function (ph4, ph4_mv, ph7, ph7_mv, ph10, ph10_mv) {
            // formulae is
            // pH = 7 - (mV - zero) / (slope * 59.16);
            //
            // ideally, zero = 0, slope = 1 and pH = 7 when mV = 0
            // tf. zero = ph7_mv
            //     slope = slope of line of best fit through ph4_mv, ph7_mv, ph10_mv
            // it is possible 1 of ph4_mv, ph7_mv or ph10_mv are undefined in which case the slope is simply line between 2 points
            // if all 3 are defined the line of best fit is used that passes through ph7_mv
            var zero, slope4_7, slope7_10;
            if (angular.isDefined(ph7_mv)) {
                zero = ph7_mv;
            } else if (angular.isDefined(ph4_mv) && angular.isDefined(ph10_mv)) {
                zero = (ph4_mv + ph10_mv) / 2.0;
            } else {
                return false; // Could not calibrate with 0 ph_mv's
            }

            if (angular.isDefined(ph4_mv)) {
                slope4_7 = fnSlope(ph4_mv, ph4, zero);
            }
            if (angular.isDefined(ph10_mv)) {
                slope7_10 = fnSlope(ph10_mv, ph10, zero);
            }

            var slope;
            if (angular.isDefined(slope4_7) && angular.isDefined(slope7_10)) {
                slope = (slope4_7 + slope7_10) / 2.0;
            } else if (angular.isDefined(slope4_7)) {
                slope = slope4_7;
            } else {
                sloep = slope7_10;
            }

            this.setCalibration({
                zero: zero,
                slope: slope,
                lastCalibratedOnUtc: moment.utc().toDate()
            });
        }

        probe.prototype.setReportPeriod = function (periodMilliseconds, success, error) {
            var self = this;
            if (!self.isConnected()) {
                if (error) {
                    error(self.statusText);
                }
                return;
            }
            var period10ths = periodMilliseconds / 100;
            self.reportPeriod = period10ths;

            var data = new Uint8Array(1);
            data.set([period10ths], 0);

            // Note, we have to use a "success" function in order to make the characteristicWrite use a WriteWithResponse
            this.probeImpl.reportPeriodWrite(data, function () {
                if (success) {
                    success();
                }
            }, error);
        };

        probe.prototype.readTemperatureAdjusted_pHValue = function (success, error) {
            var self = this;
            if (!self.isConnected()) {
                if (error) {
                    error(self.statusText);
                }
                return;
            }

            var _raw_pH;
            var _kelvin;
            var _automaticTemperatureCompensation;

            function update() {
                if (angular.isDefined(_raw_pH) && angular.isDefined(_kelvin)) {
                    self.adjusted_pH = 7 + (_raw_pH - 7) * (_kelvin / TidealKelvin);

                    success(self.raw_pH, self.adjusted_pH, self.kelvin - _0degreesKelvin, _automaticTemperatureCompensation);

                    _raw_pH = undefined;
                    _kelvin = undefined;
                }
            }

            var cancelpHRead = this.read_pHValue(function (raw_pH) {
                _raw_pH = raw_pH;
                update();
            }, error);

            var cancelTempRead = this.readTemperature(function (kelvin, automaticTemperatureCompensation) {
                _kelvin = kelvin;
                _automaticTemperatureCompensation = automaticTemperatureCompensation;
                update();
            }, error);

            if (cancelpHRead || cancelTempRead) {
                return function () {
                    if (cancelpHRead) {
                        cancelpHRead();
                        cancelpHRead = null;
                    }
                    if (cancelTempRead) {
                        cancelTempRead();
                        cancelTempRead = null;
                    }
                }
            }
        }

        probe.prototype.readTemperature = function (success, error) {
            var a = 0.497269805;
            var b = 18.71687232;
            var self = this;
            return this.probeImpl.temperatureValueRead(function (bytes) {
                var raw = binUtils.littleEndian.readUInt16(bytes, 0);
                var kelvin = TidealKelvin;
                var automaticTemperatureCompensation = raw < 8190;
                if (automaticTemperatureCompensation) {
                    var resistance = a * raw + b;
                    var degrees = (resistance - 1000) / 3.845;
                    kelvin = degrees + _0degreesKelvin;
                }
                self.kelvin = kelvin;
                success(self.kelvin, automaticTemperatureCompensation);
            }, error, self.reportPeriod);
        };

        probe.prototype.read_pHValue = function (success, error) {
            var a = 0.25229283;
            var b = -1040.5521;
            var self = this;
            return this.probeImpl.pHValueRead(function (bytes) {
                var raw = binUtils.littleEndian.readUInt16(bytes, 0);
                var mV = a * raw + b;
                var pH = 7 - (mV - self.zero) / (self.slope * 59.16);
                self.pH_mv = mV;
                self.raw_pH = pH;
                success(self.raw_pH);
            }, error, self.reportPeriod);
        };

        return probe;
    }])

    .factory('pHCalibrationSequence', [function () {
        var statesEnum = {
            0: 'Not started',
            1: 'Calibrating 1st buffer',
            2: 'Calibrating 2nd buffer',
            3: 'Calibrating 3rd buffer',
            4: 'Finished',
            named: {
                NotStarted: 0,
                Calibrating1stBuffer: 1,
                Calibrating2ndBuffer: 2,
                Calibraging3rdBuffer: 3,
                Finished: 4
            }
        };

        function Calibration(parent, pHtext, pH) {
            this.parent = parent;
            this.pHtext = pHtext;
            this.pH = pH;
            this.calibrated = false;
        }

        Calibration.prototype.calibrate = function () {
            if (this.parent.state >= statesEnum.named.Calibrating1stBuffer && !this.calibrated) {
                this.calibrated_mV = this.parent.probe.pH_mv;
                this.calibrated = true;
                this.parent.calibrated.push(this);
                this.parent.uncalibrated.remove(this);
                this.parent.state++;
            }
        }

        function CalibrationSequence(probe) {
            this.probe = probe;
            this.calibrationBuffer1 = new Calibration(this, '4.01', 4.01);
            this.calibrationBuffer2 = new Calibration(this, '7.00', 7.00);
            this.calibrationBuffer3 = new Calibration(this, '10.00', 10.00)
            this.state = 0;
            this.canStart = true;
            this.canStop = false;
        }

        CalibrationSequence.prototype.start = function () {
            this.state = statesEnum.named.Calibrating1stBuffer;
            this.canStart = false;
            this.canStop = true;
            this.uncalibrated = [this.calibrationBuffer1, this.calibrationBuffer2, this.calibrationBuffer3];
            this.calibrated = [];
        }

        CalibrationSequence.prototype.stop = function () {
            this.canStop = false;

            this.probe.calibrate(
                this.calibrationBuffer1.pH, this.calibrationBuffer1.calibrated_mV,
                this.calibrationBuffer2.pH, this.calibrationBuffer2.calibrated_mV,
                this.calibrationBuffer3.pH, this.calibrationBuffer3.calibrated_mV);

            return this.probe.getCalibration();
        }

        CalibrationSequence.prototype.statesEnum = statesEnum;

        return CalibrationSequence;
    }])

    .factory('InstrumentWorksPHProbeSvc', ['InstrumentWorksBLEScanner', 'InstrumentWorksPHProbe', 'ngTimeout', 'persistantStorage', '$interval', 'pHCalibrationSequence', function (scanner, pHProbe, ngTimeout, persistantStorage, $interval, pHCalibrationSequence) {
        var _cachedProbes = {};
        persistantStorage.getItem('pHProbe.CachedProbes', function (cache) {
            _cachedProbes = cache || {};
        })

        var _probe = null;
        var _knownProbes = [];
        var _probesListeners = [];

        function notifyProbesListeners(probes) {
            _knownProbes = probes;
            _probesListeners.forEach(function (listener) {
                listener(probes);
            })
        }

        var _refreshPeriod = 1000;
        var _pH = {
            value: null,
            temperature: null,
            unadjusted_pH: null,
        }
        var _stopMonitor = null;
        var _battery = {
            value: null
        };

        function updateBattery() {
            if (_probe) {
                _probe.readBatteryLevel(function (level) {
                    _battery.percent = level / 100.0;
                });
            }
        }

        function startMonitor() {
            if (_probe) {
                _probe.setReportPeriod(_refreshPeriod);
                _stopMonitor = [_probe.readTemperatureAdjusted_pHValue(function (rawpH, adjustedPh, celcius, automaticTemperatureCompensation) {
                    _pH.value = adjustedPh;
                    _pH.celcius = celcius;
                    _pH.automaticTemperatureCompensation = automaticTemperatureCompensation;
                    _pH.unadjusted_pH = rawpH;
                }), function () {
                    var promise = $interval(updateBattery, 30000);
                    return function () {
                        $interval.cancel(promise);
                    }
                }()];

                updateBattery();
            }
        }

        var svc = {
            current_pH: _pH,
            battery: _battery,
            startMonitor: function (refreshPeriod) {
                _refreshPeriod = refreshPeriod;
                if (_probe) {
                    startMonitor();
                }
            },

            stopMonitor: function () {
                if (_stopMonitor) {
                    _stopMonitor.forEach(function (fn) {
                        fn();
                    });
                    _stopMonitor = [];
                }
            },

            getActiveProbe: function () {
                return _probe;
            },

            setActiveProbe: function (probe, onConnect) {
                if (probe === null) {
                    if (_probe) {
                        svc.stopMonitor();
                        _probe.disconnect();
                    }
                    _probe = null;
                } else if (angular.isDefined(probe)) {
                    _probe = probe;
                    scanner.stopScanning();

                    onConnect = ngTimeout(onConnect);

                    _probe.connect(function (connected) {
                        if (!connected) {
                            _knownProbes.remove(_probe);
                            notifyProbesListeners(_knownProbes);
                            _probe = null;
                            svc.stopMonitor();
                        } else {
                            startMonitor();
                        }
                        onConnect(connected);
                    });
                }
                return _probe;
            },

            addProbesListener: function (listener) {
                _probesListeners.push(listener);
                return function () {
                    _probesListeners.remove(listener);
                }
            },

            findProbes: function () {
                scanner.startScanning(function (probeImpls) {
                    var newProbes = [];
                    angular.forEach(probeImpls, function (probeImpl) {
                        var probe = new pHProbe(probeImpl);
                        newProbes.push(probe);
                    });

                    var knownProbes = [];
                    if (_probe) {
                        knownProbes.push(_probe);
                    }
                    newProbes.forEach(function (p) {
                        var pinfo = _cachedProbes[p.id];
                        if (angular.isUndefined(pinfo)) {
                            pinfo = {
                                id: p.id
                            };
                            p.connect(function (connected) {
                                if (connected) {
                                    p.readSerialNumber(function (serialNumber) {
                                        pinfo.serialNumber = serialNumber;
                                        _cachedProbes[p.id] = pinfo;

                                        persistantStorage.setItem('pHProbe.CachedProbes', _cachedProbes);

                                        p.disconnect(function () {
                                            knownProbes.push(p);
                                            if (knownProbes.length == probeImpls.length) {
                                                notifyProbesListeners(knownProbes);
                                            }
                                        });
                                    })
                                }
                            })
                        } else {
                            knownProbes.push(p);
                            p.serialNumber = pinfo.serialNumber;
                            if (knownProbes.length == probeImpls.length) {
                                notifyProbesListeners(knownProbes);
                            }
                        }
                    });
                    if (knownProbes.length == probeImpls.length) {
                        notifyProbesListeners(knownProbes);
                    }
                });

                return function () {
                    scanner.stopScanning();
                }
            },

            createCalibrationSequence: function () {
                return new pHCalibrationSequence(_probe);
            }
        }

        return svc;
    }])

    .factory('InstrumentWorksTest', ['InstrumentWorksPHProbeSvc', '$interval', '$log', '$timeout', function (svc, $interval, $log, $timeout) {

        function onFindProbes(probes) {
            if (svc.getActiveProbe() == null) {
                var probe = probes[0];
                svc.setActiveProbe(probe, function (connected) {
                    if (connected) {
                        probe.readSerialNumber(function (serial) {
                            $log.info('Serial ' + serial);
                        })

                        probe.readBatteryLevel(function (level) {
                            $log.info('Battery ' + level + '%');
                        })

                        probe.setReportPeriod(10);

                        var cancel = 0;
                        var cancelRead = probe.readTemperatureAdjusted_pHValue(function (rawpH, adjustedPh, temperature) {
                            $log.info('rawpH = ' + rawpH + ' adjusted pH = ' + adjustedPh + ' temperature = ' + temperature);

                            if (cancel++ > 10) {
                                cancelRead();
                                $timeout(function () {
                                    probe.disconnect(function () {
                                        $log.info('Disconnected');
                                    });
                                }, 5000);
                            }
                        });
                    } else {
                        svc.findProbes(onFindProbes);
                    }
                });
            }
        }

        svc.findProbes(onFindProbes);
        return svc;
    }])