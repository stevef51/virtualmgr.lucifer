﻿'use strict';

import angular from 'angular';
import '../utils/vmng-utils';
import '../utils/bootstrap-compat';
import moment from 'moment';

angular.module('bluetherm-thermaq-module', ['vmngUtils', 'bootstrapCompat'])

    .factory('bluetherm-thermaq-provider', ['$q', 'ngTimeout', '$log', 'actionQueue', '$timeout', function ($q, ngTimeout, $log, actionQueue, $timeout) {
        if (angular.isUndefined(window.cordova) || angular.isUndefined(cordova.plugins) || angular.isUndefined(cordova.plugins.BlueThermThermaQ)) {
            return {
                supported: false,
                notsupported: true
            };
        }
        var plugin = cordova.plugins.BlueThermThermaQ;

        var _client = new plugin.Client();

        var _manager = null;

        function Driver(device) {
            this.device = device;
            if (angular.isUndefined(device.getSerialNumber_defer)) {
                device.getSerialNumber_defer = $q.defer();
            }
        }

        var methods = ['connect', 'disconnect', 'identify', 'measure']
        methods.forEach(function (method) {
            Driver.prototype[method] = function () {
                var args = Array.prototype.slice.call(arguments);
                var defer = $q.defer();
                args.push(function () {
                    defer.resolve();
                });
                args.push(function (error) {
                    defer.reject(error);
                });
                this.device[method].apply(this.device, args);
                return defer.promise;
            }
        });

        Driver.prototype.configure = function (opts) {
            var defer = $q.defer();
            var options = Object.assign({}, opts);
            var temperatureSensor = options.temperature;
            options.sensors = [];
            if (angular.isDefined(temperatureSensor)) {
                options.sensors.push({
                    index: 0,
                    lowAlarm: angular.isUndefined(temperatureSensor.lowAlarm) ? null : temperatureSensor.lowAlarm,
                    highAlarm: angular.isUndefined(temperatureSensor.highAlarm) ? null : temperatureSensor.highAlarm,
                });
            }
            this.device.configure(options, function (result) {
                // result is specific to BlueTherm, caller is expecting Temperature options
                var temperatureSensor = result.sensors.find(function (s) { return s.index == 0; });
                result.temperature = temperatureSensor;
                delete result.sensors;
                defer.resolve(result);
            }, function (error) {
                defer.reject(error);
            });
            return defer.promise;
        }

        Driver.prototype.getSerialNumber = function () {
            return this.device.getSerialNumber_defer.promise;
        }

        function updateProbe(device, checkConnection) {
            if (angular.isDefined(device) && angular.isDefined(device.name)) {
                var probe = _manager.findProbe(device.id);

                // Ignore/Remove Unavailable probes, we cannot connect to them
                if (device.connectionState == 'Unavailable') {
                    device.delete();
                    if (angular.isDefined(probe)) {
                        probe.remove();
                    }
                    return;
                }

                if (angular.isUndefined(probe) && checkConnection) {
                    probe = _manager.addProbe(svc, device.id, device.name, new Driver(device));
                    if (!device.serialNumber) {
                        var match = /^(\d*)\s*(.*)/.exec(device.name);
                        if (match.length >= 1) {
                            device.serialNumber = match[1];
                            if (match.length >= 2) {
                                probe.name = match[2];
                            }
                        }
                    }
                    device.getSerialNumber_defer.resolve(device.serialNumber);
                }

                if (checkConnection && angular.isDefined(device.connectionState)) {
                    switch (device.connectionState) {
                        case "Available":
                            if (probe.isConnected()) {
                                ngTimeout(probe.disconnected)();
                            }
                            break;
                        case "Connected":
                            if (device.ready && !probe.isConnected()) {
                                ngTimeout(probe.connected)();
                            }
                            break;
                        case "Disconnected":
                            if (!probe.isDisconnected()) {
                                ngTimeout(probe.disconnected)();
                            }
                            break;
                        case "Connecting":
                            if (!probe.isConnecting()) {
                                ngTimeout(probe.connecting)();
                            }
                            break;
                        case "Disconnecting":
                            if (!probe.isDisconnecting()) {
                                ngTimeout(probe.disconnecting)();
                            }
                            break;
                    }
                }
                return probe;
            }
        }

        _client.registerCallback(ngTimeout(function (result) {
            var probe = updateProbe(result.device, result.command != 'deviceDeleted');

            switch (result.command) {
                case 'deviceDeleted':
                    if (angular.isDefined(probe) && angular.isFunction(probe.remove)) {
                        // Device is already deleted, remove the Probe ..
                        probe.remove();
                    }
                    break;

                case 'deviceNotification':
                    switch (result.notificationType) {
                        case 'BUTTON PRESSED':
                            if (angular.isDefined(probe) && angular.isFunction(probe.buttonPressed)) {
                                probe.buttonPressed();
                            }
                            break;

                        case 'SHUTDOWN':
                            // Probe is shutting down
                            if (angular.isDefined(probe) && angular.isFunction(probe.remove)) {
                                result.device.delete();      // Tell plugin to Delete the device
                                probe.remove();             // Remove the probe
                            }
                            break;
                    }
                    break;

                case 'scanComplete':
                    // Remove any Provider Probes that are not in the scan
                    var providerProbes = _manager.providerProbes(svc);
                    providerProbes.forEach(function (providerProbe) {
                        if (!result.devices.find(function (device) {
                            return device.id === providerProbe.id;
                        })) {
                            if (angular.isFunction(providerProbe.remove)) {
                                providerProbe.remove();
                            }
                        }
                    })

                    svc.scanState = 'Idle';
                    if (svc.$startScan) {
                        svc.$startScan.resolve();
                        delete svc.$startScan;
                    }
                    break;

                case 'deviceUpdated':
                    if (angular.isDefined(probe)) {
                        var temperatureSensor = result.device.sensors && result.device.sensors.find(function (s) { return s.index == 0; });
                        probe.updated({
                            temperature: {
                                rawValue: temperatureSensor ? temperatureSensor.reading : angular.undefined
                            },
                            battery: {
                                percent: result.device.batteryLevel / 100.0
                            },
                            updateTimestamp: moment.utc(),
                            readingTimestamp: moment.utc()
                        })

                    }
                    break;
            }
        }));

        function ThermaQPlugin() {
            this.id = 'BlueTherm-ThermaQ';
            this.scanState = 'Idle';
        }

        ThermaQPlugin.prototype.initialise = function (manager) {
            _manager = manager;
            _manager.registerProvider(this);
        }

        ThermaQPlugin.prototype.startup = function () {
            // not much to do, look for existing devices 
            // See what devices we have right at startup, might be connected to a device already
            _client.getDeviceList(function (result) {
                result.devices.forEach(function (d) {
                    updateProbe(d, true);
                });
            });

            if (this.$disconnectTimeout) {
                $timeout.cancel(this.$disconnectTimeout);
                delete this.$disconnectTimeout;
            }
        }

        ThermaQPlugin.prototype.shutdown = function (disconnectTimeout) {
            var self = this;
            self.$disconnectTimeout = $timeout(function () {
                _client.getDeviceList(function (result) {
                    result.devices.forEach(function (device) {
                        if (device.connectionState == 'Connected') {
                            device.disconnect();
                            var probe = _manager.findProbe(device.id);
                            if (angular.isDefined(probe) && angular.isFunction(probe.remove)) {
                                probe.remove();
                            }
                        }
                    });
                });
            }, disconnectTimeout);
        }

        ThermaQPlugin.prototype.startScan = function (timeoutMilliseconds) {
            if (this.$startScan) {
                return this.$startScan.promise;
            }

            this.$startScan = $q.defer();
            var self = this;

            // ThermaQ plugin keeps device list internally, so a scan will only return new devices, get the existing list first ..
            _client.getDeviceList(function (result) {
                result.devices.forEach(function (d) {
                    updateProbe(d, true);
                });

                _client.startScan(timeoutMilliseconds, ngTimeout(function () {
                    self.scanState = 'Scanning';
                }), ngTimeout(function (error) {
                    self.scanState = 'Idle';
                    self.$startScan.reject(error);
                    delete self.$startScan;
                }));
            });
            return this.$startScan.promise;
        }

        ThermaQPlugin.prototype.stopScan = function () {
            var deferred = $q.defer();
            var self = this;
            _client.stopScan(ngTimeout(function () {
                self.scanState = 'Idle';
                deferred.resolve();
            }), ngTimeout(function (error) {
                self.scanState = 'Idle';
                deferred.reject(error);
            }));
            return deferred.promise;
        }

        var svc = new ThermaQPlugin();

        return svc;
    }])

    .run(['temperature-probe-manager', 'bluetherm-thermaq-provider', function (manager, provider) {
        if (!provider.notsupported) {
            provider.initialise(manager);
        }
    }])

