
if (workbox) {
    let tenantDomain = self.location.hostname;

    console.log(`Yay! Workbox is loaded 🎉 running on ${self.location}`);

    workbox.precaching.precacheAndRoute(self.__precacheManifest)

    workbox.routing.registerRoute(
        // Cache Tenant themed files but update in the background.
        /\/tenant\/.*$/,
        new workbox.strategies.StaleWhileRevalidate({
            cacheName: 'tenant-cache',
        })
    );

    workbox.routing.registerRoute(
        // Cache fonts but update in the background.
        /\/fonts\/.*$/,
        new workbox.strategies.StaleWhileRevalidate({
            cacheName: 'font-cache',
        })
    );

    workbox.routing.registerRoute(
        // Cache media images that are labelled to be cached
        new RegExp(`http[s]?:\/\/${tenantDomain}\/media\/image\/.*&cache`),
        new workbox.strategies.StaleWhileRevalidate({
            cacheName: 'media-image-cache',
            plugins: [
                new workbox.cacheableResponse.Plugin({
                    statuses: [200]
                })
            ]
        })
    );
} else {
    console.log(`Boo! Workbox didn't load 😬`);
}

/*var CACHE_NAME = 'vm-lucifer-v1';
var urlsToCache = [
    '/'
];

self.addEventListener('install', function (event) {
    console.log('Installing service-worker');
    // Perform install steps
    event.waitUntil(
        fetch('manifest.json').then(response => response.json()).then(manifest => {
            for (let key in manifest) {
                if (manifest.hasOwnProperty(key)) {
                    urlsToCache.push(manifest[key]);
                }
            }
            console.log(`Caching ${urlsToCache.length} urls from manifest.json`);
            caches.open(CACHE_NAME)
                .then(function (cache) {
                    return cache.addAll(urlsToCache);
                })
        })
    );
});

self.addEventListener('activate', event => {
    console.log('Activating service-worker');
});

self.addEventListener('fetch', function (event) {
    console.log(`Requesting ${event.request}`);
    if (event.request.method != 'GET') {
        return;
    }
    event.respondWith(
        caches.match(event.request)
            .then(function (response) {
                // Cache hit - return response
                if (response) {
                    return response;
                }
                return fetch(event.request);
            })
    );
});
*/