using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using SD.LLBLGen.Pro.LinqSupportClasses;
using VirtualMgr.Central.Data.DatabaseSpecific;
using VirtualMgr.Central.Data.DTOConverters;
using VirtualMgr.Central.Data.Linq;

namespace VirtualMgr.MultiTenant
{
    public class FetchAppTenants : IFetchAppTenants
    {
        private readonly Func<SqlTenantMaster, DataAccessAdapter> _fnAdapter;
        private readonly GrandMasterConfig _grandMaster;
        public FetchAppTenants(GrandMasterConfig grandMaster, Func<SqlTenantMaster, DataAccessAdapter> fnAdapter)
        {
            _grandMaster = grandMaster;
            _fnAdapter = fnAdapter;
        }

        private async Task<IEnumerable<SqlTenantMaster>> GetTenantMastersAsync()
        {
            var client = new MongoClient(_grandMaster.MongoDb.Url);
            var db = client.GetDatabase(_grandMaster.MongoDb.DbName);
            var collection = db.GetCollection<SqlTenantMaster>("grand-master");
            var filter = Builders<SqlTenantMaster>.Filter.Eq("_type", "sql-tenant-master");
            var cursor = (await collection.FindAsync(filter)).ToList();
            return cursor;
        }

        public async Task<IEnumerable<AppTenant>> FetchAppTenantsAsync()
        {
            var allTenants = new List<AppTenant>();
            var tenantMasters = await GetTenantMastersAsync();
            foreach (var tenantMaster in tenantMasters)
            {
                using (var adapter = _fnAdapter(tenantMaster))
                {
                    var lmd = new LinqMetaData(adapter);

                    var profileDtos = await (from p in lmd.TenantProfile select p.ToDTO()).ToListAsync();
                    var tenantDtos = await (from t in lmd.Tenant where t.Enabled select t.ToDTO()).ToListAsync();

                    var profiles = profileDtos.Select(dto => new AppTenantProfile(
                        dto.Id,
                        dto.Name,
                        dto.EmailEnabled,
                        dto.EmailWhitelist,
                        dto.CanClone,
                        dto.CanDelete,
                        dto.Hidden,
                        dto.CreateAlertMessage,
                        string.Compare("Tenant Master", dto.Name, true) == 0,
                        string.Compare("Development", dto.Name, true) == 0
                    ));

                    Func<int?, AppTenantProfile> fnProfile = id => id.HasValue ? (from p in profiles where p.Id == id select p).FirstOrDefault() : null;
                    allTenants.AddRange(tenantDtos.Select(dto => new AppTenant(
                        tenantMaster,
                        dto.Id,
                        dto.Name,
                        dto.HostName,
                        dto.HostName,
                        dto.ConnectionString,
                        dto.TimeZone,
                        dto.Currency,
                        dto.EmailFromAddress,
                        fnProfile(dto.TenantProfileId),
                        fnProfile(dto.TargetProfileId)
                    )));
                }
            }
            return allTenants;
        }
    }
}