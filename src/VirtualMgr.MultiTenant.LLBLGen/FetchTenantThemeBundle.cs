using System;
using System.IO;
using System.Threading.Tasks;
using System.Linq;
using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.Data.Linq;
using SD.LLBLGen.Pro.LinqSupportClasses;

namespace VirtualMgr.MultiTenant
{
    public class FetchTenantThemeBundle : IFetchTenantThemeBundle
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public FetchTenantThemeBundle(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }

        public async Task<Stream> FetchBundleStreamAsync(AppTenant tenant)
        {
            using (var adapter = _fnAdapter())
            {
                var lmd = new LinqMetaData(adapter);
                var themeMediaId = (await (from gs in lmd.GlobalSetting
                                           where gs.Name == "ThemeMediaId"
                                           select gs)
                                    .ToListAsync())
                                    .FirstOrDefault();
                if (themeMediaId == null)
                    return null;

                Guid mediaId = Guid.Empty;
                if (!Guid.TryParse(themeMediaId.Value, out mediaId))
                    return null;

                var bundle = (await (from media in lmd.Media
                                     join mediaData in lmd.MediaData on media.Id equals mediaData.MediaId
                                     where media.Id == mediaId
                                     select mediaData)
                            .ToListAsync())
                            .FirstOrDefault();
                if (bundle == null)
                    return null;

                return new MemoryStream(bundle.Data);
            }
        }
    }
}