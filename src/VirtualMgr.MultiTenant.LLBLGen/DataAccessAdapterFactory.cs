using System.Data.SqlClient;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VirtualMgr.MultiTenant
{
    public static class DataAccessAdapterFactory
    {
        public static ConceptCave.Data.DatabaseSpecific.DataAccessAdapter CreateTenant(AppTenant tenant)
        {
            var adapter = new ConceptCave.Data.DatabaseSpecific.DataAccessAdapter(tenant.ConnectionString);
            var catalogMapping = new CatalogNameOverwriteHashtable();

            var builder = new SqlConnectionStringBuilder(tenant.ConnectionString);
            catalogMapping["gce-schema"] = builder.InitialCatalog;
            adapter.CatalogNameOverwrites = catalogMapping;

            return adapter;
        }

        public static VirtualMgr.Central.Data.DatabaseSpecific.DataAccessAdapter CreateTenantMaster(SqlTenantMaster options)
        {
            var adapter = new VirtualMgr.Central.Data.DatabaseSpecific.DataAccessAdapter(options.ConnectionString);

            var catalogMapping = new CatalogNameOverwriteHashtable();
            var builder = new SqlConnectionStringBuilder(options.ConnectionString);
            catalogMapping.Add("gce-schema", builder.InitialCatalog);
            catalogMapping.Add("vmgr-central", builder.InitialCatalog);

            adapter.CatalogNameOverwrites = catalogMapping;

            return adapter;
        }

    }
}