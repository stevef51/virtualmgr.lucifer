using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SD.LLBLGen.Pro.DQE.SqlServer;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VirtualMgr.MultiTenant
{
    public static class ServiceCollectionExtensions
    {
        public static void AddLLBLGenRuntimeConfiguration(this IServiceCollection services, System.Diagnostics.TraceLevel traceLevel)
        {
            RuntimeConfiguration.ConfigureDQE<SQLServerDQEConfiguration>(c =>
                c.SetTraceLevel(traceLevel)
                .AddDbProviderFactory(typeof(System.Data.SqlClient.SqlClientFactory))
                .SetDefaultCompatibilityLevel(SqlServerCompatibilityLevel.SqlServer2012));

        }

        public static void AddVirtualMgrMultiTenantLLBLGen(this IServiceCollection services, IConfiguration config)
        {
            services.AddScoped<SqlTenantMaster>(provider => provider.GetRequiredService<AppTenant>().TenantMaster);
            services.AddSingleton<GrandMasterConfig>(provider => config.GetSection("GrandMaster").Get<GrandMasterConfig>());

            services.AddTransient<ConceptCave.Data.DatabaseSpecific.DataAccessAdapter>(provider => DataAccessAdapterFactory.CreateTenant(provider.GetService<AppTenant>()));

            services.AddTransient<Func<ConceptCave.Data.DatabaseSpecific.DataAccessAdapter>>(provider => () => provider.GetRequiredService<ConceptCave.Data.DatabaseSpecific.DataAccessAdapter>());
            services.AddTransient<Func<SqlTenantMaster, VirtualMgr.Central.Data.DatabaseSpecific.DataAccessAdapter>>(provider => tenantMaster => DataAccessAdapterFactory.CreateTenantMaster(tenantMaster));

            services.AddTransient<IFetchAppTenants, FetchAppTenants>();
            services.AddTransient<IFetchTenantThemeBundle, FetchTenantThemeBundle>();
        }
    }
}
