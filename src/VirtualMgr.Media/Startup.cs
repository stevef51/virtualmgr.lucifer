﻿using System.IdentityModel.Tokens.Jwt;
using Lamar;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using VirtualMgr.AspNetCore;
using VirtualMgr.Common;
using VirtualMgr.Membership.DirectCore.IdentityOverrides;
using VirtualMgr.MultiTenant;
using SixLabors.ImageSharp.Web.Caching;
using SixLabors.ImageSharp.Web.Commands;
using SixLabors.ImageSharp.Web.DependencyInjection;
using SixLabors.ImageSharp.Web.Processors;
using Serilog;
using Microsoft.Extensions.Logging;

namespace VirtualMgr.Media
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureContainer(ServiceRegistry services)
        {
            services.Configure<ServiceOptions>(Configuration.GetSection("Service"));
            services.Configure<ServiceOptions>(options => options.ServiceName = options.Services.Media);

            services.AddLogging();

            var tempProvider = services.BuildServiceProvider();
            var serviceOptions = tempProvider.GetRequiredService<IOptions<ServiceOptions>>().Value;

            services.AddDbContext<ApplicationDbContext>();

            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.Configure<MediaImageProviderOptions>(options => options.Path = new PathString("/image"));
            services.AddImageSharpCore()
                .SetRequestParser<QueryCollectionRequestParser>()
                .SetMemoryAllocatorFromMiddlewareOptions()
                .SetCache<PhysicalFileSystemCache>()
                .SetCacheHash<CacheHash>()
                .AddProvider<MediaImageProvider>()
                .AddProcessor<ResizeWebProcessor>()
                .AddProcessor<ZoomWebProcessor>()
                .AddProcessor<FormatWebProcessor>()
                .AddProcessor<BackgroundColorWebProcessor>();

            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .AddJsonOptions(options =>
                {
                    options.SerializerSettings.Converters.Add(new Newtonsoft.Json.Converters.StringEnumConverter());
                    options.SerializerSettings.ContractResolver = null;//new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver();
                    options.SerializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;
                });

            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

            services.Configure<ForwardedHeadersOptions>(options =>
            {
                options.ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto;
            });

            services.AddModule(new VirtualMgr.Redis.InjectionModule(serviceOptions.Services.Redis));

            services.AddModule<VirtualMgr.Membership.DirectCore.InjectionModule>();
            services.AddModule(new CommonAuthenticationInjectionModule(serviceOptions));


            services.AddVirtualMgrAppTenant<RequestHostTenantDomainResolver>(Configuration);
            services.AddModule(new VirtualMgr.MassTransit.InjectionModule(Configuration, serviceOptions.ServiceName));

            services.AddLLBLGenRuntimeConfiguration(System.Diagnostics.TraceLevel.Error);
            services.AddVirtualMgrMultiTenantLLBLGen(Configuration);

            services.AddConceptCaveRepositoryLLBLGen();

            services.AddSingleton<ITimeZoneInfoResolver>(TimeZoneInfoResolver.Use(new LinuxTimeZoneInfoResolver()));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(
            IApplicationBuilder app,
            IHostingEnvironment env,
            IOptions<ServiceOptions> serviceOptions)
        {
            foreach (var nvp in Configuration.AsEnumerable())
            {
                Log.Logger.Information($"{nvp.Key}={nvp.Value}");
            }
            app.UseRequestLocalization(new RequestLocalizationOptions
            {
                DefaultRequestCulture = new RequestCulture("en")
            });

            app.UsePathBase($"/{serviceOptions.Value.ServiceName}");

            app.UseVirtualMgrAppTenant();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMiddleware<VirtualMgr.Media.MediaExtensionUrlRewriteAsFormatMiddleware>();
            app.UseMiddleware<VirtualMgr.AspNetCore.Cors.CorsMiddleware>();

            app.UseAuthentication();
            app.UseCookiePolicy();

            app.UseImageSharp();

            app.UseMvc();
            /* routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller}/{action=Index}/{id?}");
            });*/
        }
    }
}
