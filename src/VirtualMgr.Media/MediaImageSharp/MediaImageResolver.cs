using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Repositories;
using Microsoft.AspNetCore.Http;
using SixLabors.ImageSharp.Web;
using SixLabors.ImageSharp.Web.Providers;
using SixLabors.ImageSharp.Web.Resolvers;

namespace VirtualMgr.Media
{

    public class MediaImageResolver : IImageResolver
    {
        private MediaDTO _media;

        public MediaImageResolver(MediaDTO media)
        {
            _media = media;
        }

        public Task<ImageMetaData> GetMetaDataAsync()
        {
            return Task.FromResult<ImageMetaData>(new ImageMetaData(_media.DateVersionCreated));
        }

        public Task<Stream> OpenReadAsync()
        {
            var memoryStream = new MemoryStream(_media.MediaData.Data);
            return Task.FromResult<Stream>(memoryStream);
        }
    }
}