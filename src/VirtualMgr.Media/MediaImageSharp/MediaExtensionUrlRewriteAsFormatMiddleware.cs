using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Primitives;

namespace VirtualMgr.Media
{
    public class MediaExtensionUrlRewriteAsFormatMiddleware
    {
        private readonly ILogger _logger;
        private readonly RequestDelegate _next;
        private readonly Regex _pathRegex;
        private readonly MediaImageProviderOptions _options;
        public MediaExtensionUrlRewriteAsFormatMiddleware(RequestDelegate next, ILogger<MediaExtensionUrlRewriteAsFormatMiddleware> logger, IOptions<MediaImageProviderOptions> options)
        {
            _next = next ?? throw new ArgumentNullException(nameof(next));
            _logger = logger;
            _options = options.Value;
            _pathRegex = new Regex($@"^\{options.Value.Path.ToString()}\/(?<mediaid>.{{36}})\.(?<extension>\w*)");
        }

        public async Task Invoke(HttpContext context)
        {
            var match = _pathRegex.Match(context.Request.Path);
            if (match.Success)
            {
                var extension = match.Groups["extension"];
                context.Request.Path = _options.Path.Add(new PathString($"/{match.Groups["mediaid"].Value}"));

                var query = QueryHelpers.ParseQuery(context.Request.QueryString.Value);
                if (!query.ContainsKey("format"))
                {
                    query.Add("format", new StringValues(match.Groups["extension"].Value));
                }
                var newQuery = QueryHelpers.AddQueryString("", query.ToDictionary(nvp => nvp.Key, nvp => nvp.Value.ToString()));
                context.Request.QueryString = new QueryString(newQuery);

                _logger.LogInformation("MediaExtension rewrite {rewrite}", context.Request.GetDisplayUrl());
            }
            await _next(context);
        }
    }
}
