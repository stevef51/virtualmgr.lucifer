// Copyright (c) Six Labors and contributors.
// Licensed under the Apache License, Version 2.0.

using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using SixLabors.ImageSharp.Processing;
using SixLabors.ImageSharp.Processing.Processors.Transforms;
using SixLabors.ImageSharp.Web;
using SixLabors.ImageSharp.Web.Commands;
using SixLabors.ImageSharp.Web.Processors;
using SixLabors.Primitives;

namespace VirtualMgr.Media
{
    /// <summary>
    /// ImageSharp "zoom" command akin to ImageResizer
    /// </summary>
    public class ZoomWebProcessor : IImageWebProcessor
    {
        public const string Zoom = "zoom";

        private static readonly IEnumerable<string> ZoomCommands
            = new[]
            {
                Zoom
            };

        public IEnumerable<string> Commands { get; } = ZoomCommands;

        public FormattedImage Process(FormattedImage image, ILogger logger, IDictionary<string, string> commands)
        {
            var zoom = GetZoom(commands);

            if (zoom != null)
            {
                image.Image.Mutate(x =>
                {
                    var size = x.GetCurrentSize();
                    size.Width = (int)(size.Width * zoom.Value);
                    size.Height = (int)(size.Height * zoom.Value);
                    x.Resize(new ResizeOptions()
                    {
                        Size = size
                    });
                });
            }

            return image;
        }

        private static double? GetZoom(IDictionary<string, string> commands)
        {
            if (!commands.ContainsKey(Zoom))
            {
                return null;
            }
            CommandParser parser = CommandParser.Instance;
            return parser.ParseValue<double>(commands.GetValueOrDefault(Zoom));
        }
    }
}