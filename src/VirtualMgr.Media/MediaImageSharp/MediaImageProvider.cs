using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using SixLabors.ImageSharp.Web.Providers;
using SixLabors.ImageSharp.Web.Resolvers;

namespace VirtualMgr.Media
{

    public class MediaImageProvider : IImageProvider
    {
        private readonly MediaImageProviderOptions _options;
        private readonly IMediaRepository _mediaRepo;
        public MediaImageProvider(IMediaRepository mediaRepo, IOptions<MediaImageProviderOptions> options)
        {
            _mediaRepo = mediaRepo;
            _options = options.Value;
        }
        public Func<HttpContext, bool> Match { get; set; } = _ => true;
        public IDictionary<string, string> Settings { get; set; } = new Dictionary<string, string>();

        public Task<IImageResolver> GetAsync(HttpContext context)
        {
            MediaDTO media = null;
            var mediaId = GetMediaId(context);
            if (mediaId != null)
            {
                media = _mediaRepo.GetById(mediaId.Value, ConceptCave.RepositoryInterfaces.Enums.MediaLoadInstruction.Data);
            }
            if (media == null || media.MediaData == null)
            {
                return Task.FromResult<IImageResolver>(null);
            }

            return Task.FromResult<IImageResolver>(new MediaImageResolver(media));

        }

        private Guid? GetMediaId(HttpContext context)
        {

            PathString matched, remaining;
            if (context.Request.Path.StartsWithSegments(_options.Path, out matched, out remaining))
            {
                Guid mediaId;
                if (Guid.TryParse(remaining.ToString().Trim('/'), out mediaId))
                {
                    return mediaId;
                }
            }
            return null;
        }

        public bool IsValidRequest(HttpContext context)
        {
            return GetMediaId(context) != null;
        }
    }
}