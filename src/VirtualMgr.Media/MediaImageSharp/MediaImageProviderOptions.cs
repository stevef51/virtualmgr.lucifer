using Microsoft.AspNetCore.Http;

namespace VirtualMgr.Media
{
    public class MediaImageProviderOptions
    {
        public PathString Path { get; set; }
    }
}