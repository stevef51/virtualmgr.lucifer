import { ClusterApi, ClusterTenant, LuciferRelease, LuciferReleaseProfile } from '../types';
import * as _ from 'lodash';
import { shellExec } from './shell-exec';
import { MongoDb } from '../../mongo';
import createHttpError = require('http-errors');
import { log } from '../../logging';

const yaml = require('js-yaml');
const fs = require('fs');

const chart = yaml.safeLoad(fs.readFileSync('./helm/lucifer-tenant/Chart.yaml'));
const LUCIFER_TENANT_CHART = `${chart.name}-${chart.version}`;
const LUCIFER_RELEASE_CHART = `lucifer-release-`;

const IdleProfile: LuciferReleaseProfile = {
    name: 'Idle, 0 replicas',
    defaultReplicas: 0,
    services: []
}

const TestProfile: LuciferReleaseProfile = {
    name: 'Test, 1 replica',
    defaultReplicas: 1,
    services: []
}

const Production3Profile: LuciferReleaseProfile = {
    name: 'Production, 3 replicas across cluster',
    defaultReplicas: 3,
    services: []
}

export class k8sClusterAdmin implements ClusterApi {
    private mongo: MongoDb;

    constructor() {
        this.mongo = new MongoDb();
        log.info('Using Kubernetes tenants');
        log.info(`Using Chart ${LUCIFER_TENANT_CHART}`);
    }

    private async addOrUpdateClusterTenant(tenant: ClusterTenant): Promise<ClusterTenant> {
        // Try an upgrade first, this will fail if the tenant does not exist as a Chart Release 
        let cmd = `helm upgrade ${tenant.hostname} ./helm/lucifer-tenant --atomic --install --namespace default --force --set tenantHostname=${tenant.hostname} --set luciferRelease=${tenant.luciferRelease} --set legacyWebappHostname=${tenant.legacyWebappHostname} --set contextHash=${tenant.contextHash}`;
        return shellExec(cmd, { silent: false, json: false }).then(x => tenant);
    }

    async init(): Promise<void> {
        await this.mongo.init();

        // Create default profiles if they dont exist 
        let profiles = await this.getLuciferReleaseProfiles();
        if (profiles.length == 0) {
            profiles = [IdleProfile, TestProfile, Production3Profile];
            await Promise.all(profiles.map(p => this.mongo.updateLuciferReleaseProfile(p)));
        }
    }

    getClusterTenants(): Promise<ClusterTenant[]> {
        let cmd = `helm ls --output json --namespace default`;
        return shellExec(cmd, { silent: true, json: true }).then(charts => {
            let tenantCharts: ClusterTenant[] = _.chain(charts)
                .filter(c => c.chart === LUCIFER_TENANT_CHART)
                .map(c => ({ hostname: c.name }))
                .value();
            let chartsWithValues$ = _.chain(tenantCharts)
                .map(c => shellExec(`helm get values --output json ${c.hostname}`, { silent: true, json: true }).then(response => {
                    c.luciferRelease = response.luciferRelease;     // This is the actual release the tenant is running against
                    c.contextHash = response.contextHash;
                    return c;
                }))
                .value();

            return Promise.all(chartsWithValues$);
        });

    }

    getClusterTenant(hostname: String): Promise<ClusterTenant> {
        let cmd = `helm ls --output json --namespace default`;
        return shellExec(cmd, { silent: true, json: true }).then(charts => {
            let c: ClusterTenant = _.chain(charts)
                .filter(c => c.chart === LUCIFER_TENANT_CHART && c.name === hostname)
                .map(c => ({ hostname: c.name }))
                .first()
                .value();

            if (c) {
                return shellExec(`helm get values --output json ${hostname}`, { silent: true, json: true }).then(response => {
                    c.luciferRelease = response.luciferRelease;     // This is the actual release the tenant is running against
                    c.contextHash = response.contextHash;
                    return c;
                })
            }
        });
    }

    updateClusterTenant(tenant: ClusterTenant): Promise<ClusterTenant> {
        return this.addOrUpdateClusterTenant(tenant);
    }

    removeClusterTenant(tenant: ClusterTenant): Promise<boolean> {
        let cmd = `helm uninstall ${tenant.hostname} --namespace default`;
        return shellExec(cmd, { silent: false, json: false }).then(_ => true).catch(_ => false);
    }

    async getLuciferReleases(): Promise<LuciferRelease[]> {
        const profiles = await this.getLuciferReleaseProfiles();

        // Grab all tenant and their current releases
        let cmd = `helm ls --output json --namespace default`;
        let charts = await shellExec(cmd, { silent: true, json: true });
        let tenantCharts: ClusterTenant[] = await Promise.all(charts.filter(c => c.chart.startsWith(LUCIFER_TENANT_CHART)).map(t => {
            return shellExec(`helm get values --output json ${t.name}`, { silent: true, json: true }).then(response => {
                return {
                    hostname: t.name,
                    luciferRelease: response.luciferRelease
                }
            })
        }));
        let releaseCharts = charts.filter(c => c.chart.startsWith(LUCIFER_RELEASE_CHART)).map(c => c.name);

        // Grab all releases
        let releaseDeployments = await shellExec(`kubectl get deployments -o json`, { silent: true, json: true })
            .then(_ => _.items.filter(d => releaseCharts.indexOf(d.metadata?.labels?.release) >= 0));

        // By release, build its profile (Idle, Test, Production or Mixed)
        let result: LuciferRelease[] = [];
        _.chain(releaseDeployments)
            .groupBy(rd => rd.metadata.labels.release)
            .mapKeys((deployments, releaseId) => {
                let profile: LuciferReleaseProfile = profiles.find(p => {
                    let match = 0;
                    deployments.map(d => {
                        if (p.services[d.metadata.labels.app] != null) {
                            if (d.spec.replicas === p.services[d.metadata.labels.app]) {
                                match++;
                            }
                        } else if (d.spec.replicas === p.defaultReplicas) {
                            match++;
                        }
                    })
                    return match === deployments.length;
                })

                if (profile == null) {
                    profile = {
                        name: 'Unknown',
                        defaultReplicas: null,
                        services: deployments.map(d => ({
                            name: d.metadata.labels.app,
                            replicas: d.spec.replicas
                        }))
                    }
                }

                result.push({
                    id: releaseId,
                    profile,
                    tenantHostnames: tenantCharts.filter(t => t.luciferRelease === releaseId).map(t => t.hostname)
                });
            })
            .value();
        return result;
    }

    async getLuciferRelease(id: string): Promise<LuciferRelease> {
        // Easiest way is to grab all releases and filter, the extra work in grabbing all is negligible
        let all = await this.getLuciferReleases();
        return all.find(r => r.id === id);
    }

    private getReplicas(profile: LuciferReleaseProfile, service: string): number {
        if (profile.services[service] != null) {
            return profile.services[service].replicas;
        }
        return profile.defaultReplicas;
    }

    async updateLuciferRelease(release: LuciferRelease): Promise<LuciferRelease> {
        let current = await this.getLuciferRelease(release.id);
        if (current == null) {
            throw new createHttpError.NotFound();
        }

        if (JSON.stringify(current) === JSON.stringify(release)) {
            return release;
        }

        let deployments = await shellExec(`kubectl get -o json deployments`, { silent: false, json: true })
            .then(_ => _.items.filter(d => d.metadata?.labels?.release === release.id));


        await Promise.all(deployments
            .filter(d => d.spec.replicas !== this.getReplicas(release.profile, d.metadata.labels.app))
            .map(d => shellExec(`kubectl scale --replicas ${this.getReplicas(release.profile, d.metadata.labels.app)} deployment/${d.metadata.name}`, { silent: false, json: false })));

        return release;
    }

    async deleteLuciferRelease(id: string): Promise<boolean> {
        // Determine whether we can actually delete it or not, we cannot delete a release which has tenants running on it
        let releases = await this.getLuciferReleases();
        let release = releases.find(r => r.id === id);
        if (release == null) {
            throw new createHttpError.NotFound();
        }
        if (release.tenantHostnames.length != 0) {
            throw new createHttpError.Conflict(`Cannot delete Lucifer Release ${id} it has ${release.tenantHostnames.length} active tenants`);
        }
        await shellExec(`helm delete ${id}`, { silent: false });
        return true;
    }

    async getLuciferReleaseProfiles(): Promise<LuciferReleaseProfile[]> {
        return this.mongo.getLuciferReleaseProfiles();
    }

    async updateLuficerReleaseProfile(profile: LuciferReleaseProfile) {
        return this.mongo.updateLuciferReleaseProfile(profile);
    }
}
