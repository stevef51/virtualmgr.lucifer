const shell = require('shelljs');
import { log } from '../../logging';

export function shellExec(cmd, options): any {
    return new Promise((resolve, reject) => {
        if (!options.silent) {
            log.info(cmd);
        }
        shell.exec(cmd, options, (code, out, err) => {
            if (code !== 0) {
                log.error(err);
                reject(err);
            } else {
                resolve(options.json ? JSON.parse(out) : out);
            }
        })
    })
}