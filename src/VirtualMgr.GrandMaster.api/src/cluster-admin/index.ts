import { config } from '../config';
import * as _ from 'lodash';

import { k8sClusterAdmin } from './k8s-cluster-admin';
import { MemoryClusterAdmin } from './memory-cluster-admin';
import { ClusterApi } from './types';

const ClusterTenantAdmin: (new () => ClusterApi) = config.kubernetes ? k8sClusterAdmin : MemoryClusterAdmin;

export { ClusterTenantAdmin }

