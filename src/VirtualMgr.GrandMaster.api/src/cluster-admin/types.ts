export interface LuciferService {
    name: string;
    replicas: number;
}

export interface LuciferReleaseProfile {
    id?: number;
    name: string;
    defaultReplicas: number;
    services: LuciferService[];
}

export interface LuciferRelease {
    id: string;
    profile: LuciferReleaseProfile;
    tenantHostnames: string[];
}

export interface ClusterTenant {
    hostname: string;
    luciferRelease?: string;
    legacyWebappHostname?: string;
    contextHash?: string;
}

export interface ClusterApi {
    init(): Promise<void>;
    getClusterTenants(): Promise<ClusterTenant[]>;
    getClusterTenant(hostname: String): Promise<ClusterTenant>;
    updateClusterTenant(tenant: ClusterTenant): Promise<ClusterTenant>;
    removeClusterTenant(tenant: ClusterTenant): Promise<boolean>;

    getLuciferReleases(): Promise<LuciferRelease[]>;
    getLuciferRelease(id: string): Promise<LuciferRelease>;
    updateLuciferRelease(release: LuciferRelease): Promise<LuciferRelease>;
    deleteLuciferRelease(id: string): Promise<boolean>;

    getLuciferReleaseProfiles(): Promise<LuciferReleaseProfile[]>;
}