import { ClusterApi, ClusterTenant, LuciferRelease, LuciferReleaseProfile } from './types';

import { log } from '../logging';
import * as _ from 'lodash';

const devProfile: LuciferReleaseProfile = {
    name: 'dev',
    defaultReplicas: 1,
    services: []
}
const latest: LuciferRelease = {
    id: 'latest',
    profile: devProfile,
    tenantHostnames: ['localhost']
}

export class MemoryClusterAdmin implements ClusterApi {
    private memoryTenants: ClusterTenant[];

    constructor() {
        log.info('Using memory based tenants');
    }

    init(): Promise<void> {
        return Promise.resolve(null);
    }

    getClusterTenants(): Promise<ClusterTenant[]> {
        return Promise.resolve(this.memoryTenants);
    }

    getClusterTenant(hostname: String): Promise<ClusterTenant> {
        return Promise.resolve(_.first(this.memoryTenants.filter(t => t.hostname === hostname)));
    }

    updateClusterTenant(tenant: ClusterTenant): Promise<ClusterTenant> {
        this.removeClusterTenant(tenant);
        this.memoryTenants.push(tenant);
        return Promise.resolve(tenant);
    }

    removeClusterTenant(tenant: ClusterTenant): Promise<boolean> {
        _.remove(this.memoryTenants, t => t.hostname === tenant.hostname);
        return Promise.resolve(true);
    }

    async getLuciferReleases(): Promise<LuciferRelease[]> {
        return [latest];
    }

    async getLuciferRelease(id: string): Promise<LuciferRelease> {
        return latest;
    }

    async deleteLuciferRelease(id: string): Promise<boolean> {
        return false;
    }

    async getLuciferReleaseProfiles(): Promise<LuciferReleaseProfile[]> {
        return [devProfile]
    }

    async updateLuciferRelease(release: LuciferRelease): Promise<LuciferRelease> {
        return release;
    }
}
