import { genV4 } from "uuidjs";

export class OperationLogItem {
    constructor(private fnUpdate: (log: OperationLogItem) => Promise<void>, public description: string, indeterminate: boolean) {
        this.startedAt = new Date();
        this.indeterminate = indeterminate;
        this.progress = 0;
    }

    public readonly startedAt: Date;
    public readonly indeterminate: boolean;
    public progress: number;
    public finishedAt?: Date;
    public error?: string;

    async progressed(inc?: number): Promise<void> {
        this.progress += inc != null ? 1 : inc;
        await this.fnUpdate(this);
    }

    async finished(): Promise<void> {
        this.finishedAt = new Date();
        await this.fnUpdate(this);
    }

    async errored(err): Promise<void> {
        this.error = err.message;
        this.finishedAt = new Date();
        await this.fnUpdate(this);
    }
}

export class LongRunningOperation {
    public readonly id: string;
    public readonly description: string;
    public readonly startedAt: Date;
    public finishedAt?: Date;
    public readonly logs: OperationLogItem[] = [];

    async createIndeterminate(description: string): Promise<OperationLogItem> {
        let log = new OperationLogItem(this.update.bind(this), description, true);
        this.logs.push(log);
        await this.fnChanged(this);
        return log;
    }

    async createDeterminate(description: string): Promise<OperationLogItem> {
        let log = new OperationLogItem(this.update.bind(this), description, false);
        this.logs.push(log);
        await this.fnChanged(this);
        return log;
    }

    async update(): Promise<void> {
        let total = this.logs.length;
        let errors = this.logs.filter(l => l.error != null).length;
        let finished = this.logs.filter(l => l.finishedAt != null).length;
        if (finished == total) {
            this.finishedAt = new Date();
        } else {
            this.finishedAt = null;
        }
        await this.fnChanged(this);
    }

    constructor(private fnChanged: (lro: LongRunningOperation) => Promise<void>, description: string) {
        this.id = genV4().hexString;
        this.description = description;
        this.startedAt = new Date();
    }
}

