import { MongoClient, Db, Collection } from 'mongodb';
import { config } from '../config';
import { LuciferReleaseProfile, LuciferService } from '../cluster-admin/types';
import * as _ from 'lodash';
import { log } from '../logging';
import { genV4 } from 'uuidjs';

import { TYPES } from './types';
import { SqlTenantMaster } from '../sql-tenant-admin/types';
import { LongRunningOperation } from '../long-running-operation';
import moment = require('moment');

const GRANDMASTER = `grand-master`;
const COUNTERS = `counters`;
const TEMPORARY = `temporary`;

log.info(`MongoDb connecting to ${config.grandMaster.mongoDb.url} ${config.grandMaster.mongoDb.dbName}`);
const client = new MongoClient(config.grandMaster.mongoDb.url, { useNewUrlParser: true, useUnifiedTopology: true });

export class MongoDb {
    constructor() {
    }

    async connect(): Promise<Db> {
        while (!client.isConnected()) {
            try {
                await client.connect();
            } catch (err) {
                log.error(err);
            }
        }
        return client.db(config.grandMaster.mongoDb.dbName);
    }

    async collection(collection: string): Promise<Collection> {
        let db = await this.connect();
        return db.collection(collection);
    }

    async init(): Promise<void> {
        let db = await this.connect();
        let indexes: any = {
            'grand-master': [{
                _type: 1
            }]
        }

        await Promise.all(_.flattenDeep(_.forIn(indexes, async (value, key) => {
            let c = db.collection(key);
            return _.forEach(value, async index => {
                await c.createIndex(index);
            })
        })));
    }

    async getNextSequence(name: string): Promise<number> {
        let counters = await this.collection(COUNTERS);
        var sequenceDocument = await counters.findOneAndUpdate({
            _id: name
        }, {
            $inc: {
                sequence_value: 1
            }
        }, {
            returnOriginal: false,
            upsert: true
        });

        return sequenceDocument.value.sequence_value;
    }

    async getSqlTenantMasters(): Promise<SqlTenantMaster[]> {
        let grandMaster = await this.collection(GRANDMASTER);
        return await grandMaster.find<SqlTenantMaster>({ _type: TYPES.SQL_TENANT_MASTER }).toArray();
    }

    async getLuciferReleaseProfiles(): Promise<LuciferReleaseProfile[]> {
        let grandMaster = await this.collection(GRANDMASTER);
        return await grandMaster.find<LuciferReleaseProfile>({ _type: TYPES.LUCIFER_RELEASE_PROFILE }).toArray();
    }

    private async updateRecordNumber<TRecord extends { id?: number }>(record: TRecord, type: string, collectionName: string): Promise<TRecord> {
        let grandMaster = await this.collection(collectionName);
        if (record.id == null) {
            record.id = await this.getNextSequence(type);
        }
        const _id = `${type}/${record.id}`;
        return (await grandMaster.findOneAndReplace({ _id }, Object.assign({
            _id: _id,
            _type: type
        }, record), {
            upsert: true
        })).value;
    }

    private async updateRecordString<TRecord extends { id?: string }>(record: TRecord, type: string, collectionName: string): Promise<TRecord> {
        let grandMaster = await this.collection(collectionName);
        if (record.id == null) {
            record.id = genV4().hexString;
        }
        const _id = `${type}/${record.id}`;
        return (await grandMaster.findOneAndReplace({ _id }, Object.assign({
            _id: _id,
            _type: type
        }, record), {
            upsert: true
        })).value;
    }

    async updateLuciferReleaseProfile(profile: LuciferReleaseProfile): Promise<LuciferReleaseProfile> {
        return this.updateRecordNumber(profile, TYPES.LUCIFER_RELEASE_PROFILE, GRANDMASTER);
    }

    async getLongRunningOperations(opts?: {
        onlyRecent: boolean
    }): Promise<LongRunningOperation[]> {
        let grandMaster = await this.collection(TEMPORARY);
        let filter: any = { _type: TYPES.LONG_RUNNING_OPERATION };
        if (opts != null && !!opts.onlyRecent) {
            filter.startedAt = {
                $gt: moment().subtract(6, 'hours').toDate()
            }
        }
        return await grandMaster.find<LongRunningOperation>(filter).toArray();
    }

    async createLongRunningOperation(description: string): Promise<LongRunningOperation> {
        return new LongRunningOperation(async lro => {
            await this.updateLongRunningOperation(lro);
        }, description);
    }

    async updateLongRunningOperation(operation: LongRunningOperation): Promise<LongRunningOperation> {
        return this.updateRecordString(operation, TYPES.LONG_RUNNING_OPERATION, TEMPORARY);
    }

    async watch(type, callback) {
        let operations = await this.collection(TEMPORARY);
        let changeStream = operations.watch([], {});/*{
            $match: {
                _type: type
            }
        }], {});*/
        changeStream.on(`error`, err => {
            log.error(err);
        });
        changeStream.on(`change`, async data => {
            await callback(data);
        })
        return () => {
            changeStream.close();
        }
    }

    async watchOperations(callback) {
        return await this.watch(TYPES.LONG_RUNNING_OPERATION, callback);
    }

    async close() {
    }
}
