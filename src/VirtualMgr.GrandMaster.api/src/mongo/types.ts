import { genV4 } from "uuidjs"
import moment = require("moment");
import * as _ from 'lodash';

export const TYPES = {
    SQL_TENANT_MASTER: `sql-tenant-master`,
    LUCIFER_RELEASE_PROFILE: `lucifer-release-profile`,
    LONG_RUNNING_OPERATION: `long-running-operation`
}

