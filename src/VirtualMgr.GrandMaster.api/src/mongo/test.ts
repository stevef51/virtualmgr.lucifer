'use strict';

import { MongoDb } from './mongo-db';

export async function main() {
    const db = new MongoDb();
    await db.init();

    await db.watchOperations(data => {
        console.log(data);
    });
}