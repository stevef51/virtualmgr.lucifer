const express = require('express');
const app = express();
const server = require('http').createServer(app);
const io = require('socket.io')(server, {
    path: `/api/socket.io`
});
io.on('connection', () => {
    console.log('Connected');
});

app.use(express.json());
const api = express.Router();
api.get(`/tenants`, async (req, res) => {
    res.send('tenants');
});

export async function main() {
    server.listen(80);
    console.log('Listening....');
}