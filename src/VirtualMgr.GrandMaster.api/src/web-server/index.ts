import { TenantService } from '../tenant-service';
import { log } from '../logging';

const express = require('express');
require('express-async-errors');
const port = 80;
const app = express();

import { setup } from './io';
import { MongoDb } from '../mongo';
const ioServer = setup(app);

let tenantSvc: TenantService;
const api = express.Router();

const mongo = new MongoDb();

app.use(express.json());

api.get(`/tenants`, async (req, res) => {
    res.send(await tenantSvc.getTenants());
});
api.get(`/tenant/:hostname`, async (req, res) => {
    res.send(await tenantSvc.getTenant(req.params.hostname));
});
api.post(`/tenant`, async (req, res) => {
    res.send(await tenantSvc.saveTenant(req.body));
});
api.post(`/tenant/:hostname/enable`, async (req, res) => {
    res.send(await tenantSvc.enableTenant(req.params.hostname, true));
});
api.post(`/tenant/:hostname/disable`, async (req, res) => {
    res.send(await tenantSvc.enableTenant(req.params.hostname, false));
});
api.post(`/tenant/:hostname/available`, async (req, res) => {
    res.send(await tenantSvc.checkHostnameAvailable(req.params.hostname));
});
api.delete(`/tenant/:hostname`, async (req, res) => {
    res.send(await tenantSvc.deleteTenant(req.params.hostname));
});

api.get(`/lucifer-releases`, async (req, res) => {
    res.send(await tenantSvc.getLuciferReleases());
});
api.get(`/lucifer-release/:id`, async (req, res) => {
    res.send(await tenantSvc.getLuciferRelease(req.params.id));
});
api.get(`/lucifer-release-profiles`, async (req, res) => {
    res.send(await tenantSvc.getLuciferReleaseProfiles());
});
api.post(`/lucifer-release`, async (req, res) => {
    res.send(await tenantSvc.saveLuciferRelease(req.body));
});
api.delete(`/lucifer-release/:id`, async (req, res) => {
    res.send(await tenantSvc.deleteLuciferRelease(req.params.id));
});
api.get(`/tenant-masters`, async (req, res) => {
    res.send(await tenantSvc.getTenantMasters());
});
api.get(`/tenant-master/:id(\\d+)/elastic-pools`, async (req, res) => {
    res.send(await tenantSvc.getElasticPoolsForTenantMaster(Number(req.params.id)));
})
api.get(`/tenant-master/:id(\\d+)/tenant-profiles`, async (req, res) => {
    res.send(await tenantSvc.getTenantProfilesForTenantMaster(Number(req.params.id)));
})
api.post(`/clone-tenant`, async (req, res) => {
    res.send(await tenantSvc.cloneTenant(req.body));
})
api.get(`/long-running-operations`, async (req, res) => {
    res.send(await mongo.getLongRunningOperations());
})

app.use(`/api`, api);

app.use((err, req, res, next) => {
    if (err.status) {
        res.status(err.status);
        res.json({
            error: err.message
        });
    } else {
        res.status(503);
        res.json({
            error: err.message
        })
    }

    next(err);
});

export async function run() {
    tenantSvc = new TenantService();

    await tenantSvc.init();

    ioServer.http.listen(port, () => {
        log.info(`Listening on ${port}`);
    })
}