import * as socketio from 'socket.io';
import { log } from '../logging';
import { MongoDb } from '../mongo';
import * as _ from 'lodash';

const ROOMS = {
    LONG_RUNNING_OPERATIONS: `long-running-operations`
}

class SocketIo {
    public readonly http;
    public readonly io: socketio.Socket;
    private mongo: MongoDb;

    constructor(app) {
        this.mongo = new MongoDb();
        this.http = require('http').createServer(app);
        this.io = require('socket.io')(this.http, {
            path: `/api/socket.io`
        });

        this.io.on(`connect`, socket => this.onConnect(socket));

        this.mongo.watchOperations(async data => {
            await this[`get-${ROOMS.LONG_RUNNING_OPERATIONS}`](this.io.to(ROOMS.LONG_RUNNING_OPERATIONS), { onlyRecent: true });
        });
    }

    onConnect(socket) {
        log.info(`Connected client: ${socket.id}`);

        socket.on(`disconnect`, reason => {
            log.info(`Disconnected client: ${socket.id}, ${reason}`);
        });

        _.forIn(ROOMS, room => {
            socket.on(`join-${room}`, async () => {
                log.debug(`${socket.id} joining room ${room}`);
                socket.join(room);
            })
            socket.on(`leave-${room}`, () => {
                log.debug(`${socket.id} leaving room ${room}`);
                socket.leave(room);
            })

            socket.on(`get-${room}`, async request => {
                log.debug(`${socket.id} get-${room} with ${JSON.stringify(request)}`);
                await this[`get-${room}`](socket, request);
            });
        });
    }
}

SocketIo.prototype['get-long-running-operations'] = async function (socket, request) {
    let operations = await this.mongo.getLongRunningOperations(request);
    socket.emit(`long-running-operations`, operations);
}

export function setup(app) {
    return new SocketIo(app);
}