import { SqlTenantAdmin } from '../sql-tenant-admin';
import { ClusterTenantAdmin } from '../cluster-admin';
import { ClusterTenant, ClusterApi, LuciferRelease, LuciferReleaseProfile } from '../cluster-admin/types';
import { SqlTenant, SqlTenantProfile, SqlTenantMaster } from '../sql-tenant-admin/types';
import { AzureAdmin } from '../azure';
import { publisher } from '../mass-transit';
import * as request from 'request-promise-native';
import { ElasticPool } from '../azure/types';
import { deconstruct, mssqlDeconstruct } from '../sql-connection-string-builder';
import createHttpError = require('http-errors');
import { MongoDb } from '../mongo';
import moment = require('moment');
import { LongRunningOperation } from '../long-running-operation';

interface Tenant {
    hostname: string;
    sql?: SqlTenant;
    cluster?: ClusterTenant;
    running?: boolean;
    issues?: string[];
}

interface CloneTenantRequest {
    name: string;
    hostname: string;
    source: SqlTenant;
    tenantMaster: SqlTenantMaster;
    databaseName: string;
    elasticPool: ElasticPool;
    targetProfile: SqlTenantProfile;
    profile: SqlTenantProfile;
}

export class TenantService {
    private azure: AzureAdmin;
    private sql: SqlTenantAdmin;
    private cluster: ClusterApi;
    private mongo: MongoDb;

    constructor() {
        this.sql = new SqlTenantAdmin();
        this.cluster = new ClusterTenantAdmin();
        this.azure = new AzureAdmin();
        this.mongo = new MongoDb();
    }

    async init() {
        await Promise.all([
            this.sql.init(),
            this.cluster.init(),
            this.azure.init(),
            publisher.init()
        ]);
    }

    async getTenantMasters(): Promise<any> {
        let tms = await this.sql.getTenantMasters();
        return tms.map(tm => ({
            id: tm.id,
            name: tm.name
        }));
    }

    async getTenants(): Promise<Tenant[]> {
        // Get the 'desired' list of tenants from the database ..
        let [sqlTenants, clusterTenants] = await Promise.all<SqlTenant[], ClusterTenant[]>([this.sql.getSqlTenants(), this.cluster.getClusterTenants()]);

        let result: Tenant[] = [];

        sqlTenants.map(sqlT => {
            let t = result.find(i => i.hostname == sqlT.hostname);
            if (t == null) {
                t = {
                    hostname: sqlT.hostname
                }
                result.push(t);
            }
            t.sql = sqlT;
        });

        clusterTenants.map(clusterT => {
            let t = result.find(i => i.hostname == clusterT.hostname);
            if (t == null) {
                // We have found a cluster tenant who does not have an SQL tenant record - remove the cluster tenant
                t = {
                    hostname: clusterT.hostname
                }
                result.push(t);
            }
            t.cluster = clusterT;
        })

        result.map(t => {
            this.evaluateTenantIssues(t);
        });

        return result;
    }

    private evaluateTenantIssues(tenant: Tenant) {
        let issues = [];
        tenant.running = false;
        if (tenant.cluster == null) {
            // No cluster instance running
            if (tenant.sql.enabled) {
                issues.push(`Tenant is enabled but not running in the cluster - re-enable the tenant`);
            }
        } else {
            tenant.running = true;
            if (tenant.sql != null) {
                if (tenant.sql.enabled) {
                    if (!tenant.sql.status.online) {
                        issues.push(`Database is not online: ${tenant.sql.status.message}`);
                    }
                } else {
                    issues.push(`Tenant is running in the cluster but is disabled according to the database - re-enable the tenant`);
                }
            } else {
                issues.push(`Tenant is running in the cluster, but there is no database record matching the hostname ${tenant.cluster.hostname} - disable the tenant`);
            }
        }
        tenant.issues = issues;
    }

    private shouldClusterUpdate(t: Tenant): boolean {
        return (t.sql != null && t.sql.enabled) && (t.cluster == null || t.cluster.contextHash !== t.sql.contextHash);
    }

    private shouldClusterRemove(t: Tenant): boolean {
        return t.cluster != null && (t.sql == null || !t.sql.enabled);
    }

    private shouldApply(t: Tenant): boolean {
        return this.shouldClusterRemove(t) || this.shouldClusterUpdate(t);
    }

    async apply(): Promise<Tenant[]> {
        let tenants = await this.getTenants();

        let event = {
            Added: [],
            Removed: [],
            Changed: []
        }
        let changedTenants = await Promise.all(tenants.map(async t => {
            if (this.shouldClusterRemove(t)) {
                // We have a cluster tenant with no Sql record, remove the cluster tenant
                await this.cluster.removeClusterTenant(t.cluster);
                event.Removed.push(t.cluster.hostname);
                return t.sql ? t : null;
            } else if (this.shouldClusterUpdate(t)) {
                // We have a enabled Sql tenant but no cluster tenant, or something important has changed, add/update the cluster tenant
                let clusterT: ClusterTenant = {
                    hostname: t.sql.hostname,
                    contextHash: t.sql.contextHash,
                    legacyWebappHostname: t.sql.legacyWebappHostname,
                    luciferRelease: t.sql.luciferRelease
                }
                if (t.cluster == null) {
                    event.Added.push(t.sql.hostname);
                } else {
                    event.Changed.push(t.sql.hostname);
                }
                t.cluster = await this.cluster.updateClusterTenant(clusterT);
            }
            return t;
        }));

        await publisher.tenantConnectionsUpdated(event);

        return changedTenants.filter(t => t != null);
    }

    async getTenant(hostname: string): Promise<Tenant> {
        let [sqlTenant, clusterTenant] = await Promise.all<SqlTenant, ClusterTenant>([this.sql.getSqlTenant(hostname), this.cluster.getClusterTenant(hostname)]);

        let result: Tenant = {
            hostname: hostname,
            sql: sqlTenant,
            cluster: clusterTenant
        };

        this.evaluateTenantIssues(result);

        return result;
    }

    async enableTenant(hostname: string, enable: boolean): Promise<Tenant> {
        let t = await this.getTenant(hostname);
        if (t?.sql != null && t.sql.enabled !== enable) {
            t.sql.enabled = enable;
            await this.sql.saveSqlTenant(t.sql);
        }

        let event = {
            Added: [],
            Removed: [],
            Changed: []
        }
        if (this.shouldClusterRemove(t)) {
            // We have a cluster tenant with no Sql record, remove the cluster tenant
            if (await this.cluster.removeClusterTenant(t.cluster)) {
                event.Removed.push(t.cluster.hostname);
                t.cluster = null;
            }

        } else if (this.shouldClusterUpdate(t)) {
            // We have a enabled Sql tenant but no cluster tenant, or something important has changed, add/update the cluster tenant
            let clusterT: ClusterTenant = {
                hostname: t.sql.hostname,
                contextHash: t.sql.contextHash,
                legacyWebappHostname: t.sql.legacyWebappHostname,
                luciferRelease: t.sql.luciferRelease
            }
            if (t.cluster == null) {
                event.Added.push(t.sql.hostname);
            } else {
                event.Changed.push(t.sql.hostname);
            }
            t.cluster = await this.cluster.updateClusterTenant(clusterT);
        }

        this.evaluateTenantIssues(t);

        await publisher.tenantConnectionsUpdated(event);

        return t;

    }

    async deleteTenant(hostname: string): Promise<boolean> {
        let awaits = [];
        let sqlTenant = await this.sql.getSqlTenant(hostname);
        if (sqlTenant != null) {
            awaits.push(this.azure.deleteTenant(sqlTenant.connectionString));
            awaits.push(this.sql.deleteSqlTenant(sqlTenant));
        }
        let clusterT = await this.cluster.getClusterTenant(hostname);

        let event = {
            Added: [],
            Removed: [],
            Changed: []
        }
        if (clusterT != null) {
            awaits.push(this.cluster.removeClusterTenant(clusterT).then(removed => {
                if (removed) {
                    event.Removed.push(clusterT.hostname);
                }
            }))
        }

        await Promise.all(awaits);
        await publisher.tenantConnectionsUpdated(event);

        return true;
    }

    async saveTenant(tenant: Tenant): Promise<Tenant> {
        await this.sql.saveSqlTenant(tenant.sql);
        let t = await this.getTenant(tenant.sql.hostname);

        let event = {
            Added: [],
            Removed: [],
            Changed: []
        }
        if (this.shouldClusterRemove(t)) {
            // We have a cluster tenant with no Sql record, remove the cluster tenant
            await this.cluster.removeClusterTenant(t.cluster);
            event.Removed.push(t.cluster.hostname);

        } else if (this.shouldClusterUpdate(t)) {
            // We have a enabled Sql tenant but no cluster tenant, or something important has changed, add/update the cluster tenant
            let clusterT: ClusterTenant = {
                hostname: t.sql.hostname,
                contextHash: t.sql.contextHash,
                legacyWebappHostname: t.sql.legacyWebappHostname,
                luciferRelease: t.sql.luciferRelease
            }
            if (t.cluster == null) {
                event.Added.push(t.sql.hostname);
            } else {
                event.Changed.push(t.sql.hostname);
            }
            t.cluster = await this.cluster.updateClusterTenant(clusterT);
        }

        this.evaluateTenantIssues(t);
        await publisher.tenantConnectionsUpdated(event);

        return t;
    }

    async getLuciferReleases(): Promise<LuciferRelease[]> {
        return this.cluster.getLuciferReleases();
    }

    async getLuciferRelease(id: string): Promise<LuciferRelease> {
        return await this.cluster.getLuciferRelease(id);
    }

    async saveLuciferRelease(release: LuciferRelease): Promise<LuciferRelease> {
        return this.cluster.updateLuciferRelease(release);
    }

    async deleteLuciferRelease(id: string): Promise<boolean> {
        return this.cluster.deleteLuciferRelease(id);
    }

    async getLuciferReleaseProfiles(): Promise<LuciferReleaseProfile[]> {
        return this.cluster.getLuciferReleaseProfiles();
    }

    async checkHostnameAvailable(hostname: string): Promise<any> {
        let existing = await this.sql.getSqlTenant(hostname);
        if (existing != null) {
            return {
                result: false,
                message: `Tenant hostname already exists`
            };
        }

        return request.post(`https://${hostname}/api/public/validtenant`, { json: true })
            .then(response => {
                if (response) {
                    return {
                        result: false,
                        message: `Tenant hostname already exists`
                    }
                } else {
                    return {
                        result: true
                    }
                }
            })
            .catch(err => {
                if (err?.error?.code === `ENOTFOUND`) {
                    return {
                        result: false,
                        message: `Hostname not found`
                    }
                } else {
                    return {
                        result: false,
                        message: err.message
                    }
                }
            });
    }

    async getElasticPoolsForTenantMaster(id: number): Promise<ElasticPool[]> {
        let tenantMaster = this.sql.getSqlTenantMaster(id);
        if (tenantMaster == null) {
            throw new createHttpError.NotFound(`TenantMaster ${id} not found`);
        }
        let cfg = mssqlDeconstruct(tenantMaster.connectionString);
        return this.azure.getElasticPools(cfg.server);
    }

    async getTenantProfilesForTenantMaster(id: number): Promise<SqlTenantProfile[]> {
        return this.sql.getSqlTenantProfilesForTenantMaster(id);
    }

    async cloneTenant(details: CloneTenantRequest) {
        let srcTenant = await this.sql.getSqlTenant(details.source.hostname);
        if (srcTenant == null) {
            throw new createHttpError.NotFound(`Source tenant ${details.source.hostname} not found`);
        }

        let host = await this.sql.getSqlHostForTenantMaster(details.tenantMaster.id);
        let csb = deconstruct(host.connectionStringTemplate);
        csb.initialCatalog = details.databaseName;

        let lro = await this.mongo.createLongRunningOperation(`Clone tenant ${details.hostname} from ${details.source.hostname}`);

        // We don't await these so this call can return quickly..
        let azureOp = this.azure.cloneTenant(lro,
            srcTenant.connectionString,
            mssqlDeconstruct(host.connectionStringTemplate).server,
            details.elasticPool.id,
            details.databaseName
        ).then(() => {
            // Create the db record in the appropriate tenant master
            let sqlOp = this.sql.createNewTenant(lro, details.tenantMaster.id, {
                billingAddress: null,
                billingCompanyName: null,
                currency: details.source.currency,
                emailFromAddress: ``,
                enableDataWarehousePush: details.source.enableDataWarehousePush,
                enabled: false,
                hostname: details.hostname,
                invoiceIdPrefix: null,
                luciferRelease: details.source.luciferRelease,
                name: details.name,
                tenantProfileId: details.profile.id,
                timezone: details.source.timezone,
                connectionString: csb.connectionString,
                targetProfileId: details.tenantMaster.id,
                tenantHostId: host.id
            }).then(async () => {
                let enableLro = await lro.createIndeterminate(`Enabling ${details.hostname} in cluster`);
                try {
                    this.enableTenant(details.hostname, true);
                    await enableLro.finished();
                } catch (err) {
                    await enableLro.errored(err);
                }
            })
        })

        return lro.id;
    }
}