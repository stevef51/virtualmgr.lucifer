import { config } from '../config';
import { TenantService } from './index';

config.tenantMasters = [{
    id: 'lucifer-dev',
    connectionString: 'Server=tcp:getevs-sql.database.windows.net,1433;Data Source=getevs-sql.database.windows.net;Initial Catalog=lucifer-dev-tenant-master-db;Persist Security Info=False;User ID=getevs-admin;Password=Andromeda25;Pooling=False;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;'
}];

async function main() {
    const svc = new TenantService();
    await svc.init();

    let tenants = await svc.getTenants();

    console.log(tenants);
}

export { main }