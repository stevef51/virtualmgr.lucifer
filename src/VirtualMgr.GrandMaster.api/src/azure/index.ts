'use strict';

import { loginWithServicePrincipalSecret } from "@azure/ms-rest-nodeauth";
import { SqlManagementClient } from "@azure/arm-sql";
import { deconstruct, mssqlDeconstruct } from '../sql-connection-string-builder';
import * as _ from 'lodash';
import { config } from '../config';
import { ElasticPool } from './types';
import { ServersListResponse, Server, DatabasesGetResponse } from "@azure/arm-sql/esm/models";
import createHttpError = require('http-errors');
import { SqlTenantMaster } from "../sql-tenant-admin/types";
import * as moment from 'moment';
import { LongRunningOperation } from "../long-running-operation";
import { log } from '../logging';

const delay = require('delay');

// breakout an Azure resource Id into basic parts, eg
// /subscriptions/a25f9a86-51e8-42b3-801a-fe9e450c2964/resourceGroups/getevs/providers/Microsoft.Sql/servers/getevs-sql
function breakOutId(id) {
    const s = id.split('/');
    return {
        subscriptionId: s[2],
        resourceGroup: s[4],
        provider: s[6],
        resource: s[8]
    }
}

export class AzureAdmin {
    private client: SqlManagementClient;

    constructor() {
    }

    async init() {
        let creds = await loginWithServicePrincipalSecret(config.azure.clientId, config.azure.clientSecret, config.azure.tenantId);
        this.client = new SqlManagementClient(creds, config.azure.subscriptionId);
    }

    private async getSqlServer(sqlServerName: string): Promise<Server> {
        let servers = await this.client.servers.list();
        let server = _.first(servers.filter(s => s.fullyQualifiedDomainName === sqlServerName));
        if (server == null) {
            throw new createHttpError.NotFound(`Azure SQL Server ${sqlServerName} not found`);
        }
        return server;
    }

    async getTenantDatabase(connectionString): Promise<DatabasesGetResponse> {
        const sqlConfig = mssqlDeconstruct(connectionString);

        // we don't know the resource group that the server is in, so retrieve all servers and find by unique name
        let servers = await this.client.servers.list();
        let server = _.first(servers.filter(s => s.fullyQualifiedDomainName === sqlConfig.server));

        let breakOut = breakOutId(server.id);

        let db = await this.client.databases.get(breakOut.resourceGroup, server.name, sqlConfig.database);
        if (db == null) {
            throw new createHttpError.NotFound(`Database ${sqlConfig.database} not found on Azure SQL Server ${server.name}`);
        }
        return db;
    }

    async cloneTenant(lro: LongRunningOperation, srcConnectionString: string, sqlServerName: string, elasticPoolId: string, databaseName: string): Promise<boolean> {
        let sourceDb = await this.getTenantDatabase(srcConnectionString);
        let destSqlServer = await this.getSqlServer(sqlServerName);
        let destSqlServerBreakOut = breakOutId(destSqlServer.id);

        let lroLog = await lro.createIndeterminate(`Clone Azure database ${sourceDb.name} to ${databaseName}`);
        log.info(`Clone Azure database ${sourceDb.name} to ${databaseName}`);

        try {
            let azureLRO = await this.client.databases.beginCreateOrUpdate(destSqlServerBreakOut.resourceGroup, destSqlServerBreakOut.resource, databaseName, {
                location: destSqlServer.location,
                sourceDatabaseId: sourceDb.id,
                elasticPoolId: elasticPoolId
            });

            while (!azureLRO.isFinished()) {
                let poll = await azureLRO.poll();

                await delay(5000);
                await lroLog.progressed();
            }
            await lroLog.finished();
            return true;
        } catch (err) {
            log.error(err);
            await lroLog.errored(err);
            throw err;
        }
    }

    async deleteTenant(connectionString: string): Promise<boolean> {
        let db = await this.getTenantDatabase(connectionString);
        let breakout = breakOutId(db.id);

        log.info(`Deleting Azure database ${db.name} on ${breakout.resource}`);
        try {
            let azureLRO = await this.client.databases.beginDeleteMethod(breakout.resourceGroup, breakout.resource, db.name);
            while (!azureLRO.isFinished()) {
                let poll = await azureLRO.poll();

                await delay(5000);
            }
            return true;
        } catch (err) {
            log.error(err);
            return false;
        }
    }

    async getElasticPools(sqlServerName: string): Promise<ElasticPool[]> {
        let server = await this.getSqlServer(sqlServerName);
        if (server == null) {
            throw new createHttpError.NotFound(`Azure SQL Server ${sqlServerName} not found`);
        }
        let breakOut = breakOutId(server.id);
        let pools = await this.client.elasticPools.listByServer(breakOut.resourceGroup, server.name);
        return pools.map(p => ({
            id: p.id,
            name: p.name
        }))
    }
}

