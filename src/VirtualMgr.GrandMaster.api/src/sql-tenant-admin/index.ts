import * as mssql from 'mssql';
import { config } from '../config';
import { mssqlDeconstruct, deconstruct, construct } from '../sql-connection-string-builder';
import * as _ from 'lodash';
import * as sql from './sql';
import { SqlTenant, SqlTenantStatus, SqlTenantHost, SqlTenantMaster, SqlTenantProfile } from './types';
import createHttpError = require('http-errors');
import { MongoDb } from '../mongo';
import { log } from '../logging';
import { LongRunningOperation } from '../long-running-operation';

export class SqlTenantAdmin {
    private mongo: MongoDb;
    private tenantMasters: SqlTenantMaster[];

    constructor() {
        this.mongo = new MongoDb();
    }

    getSqlTenantMaster(id): SqlTenantMaster {
        return this.tenantMasters.find(i => i.id === id);
    }

    async init() {
        this.tenantMasters = await this.mongo.getSqlTenantMasters();
    }

    async getTenantMasters(): Promise<SqlTenantMaster[]> {
        return this.tenantMasters;
    }

    private async connect(tenantMaster: SqlTenantMaster, fn: (pool: mssql.ConnectionPool) => Promise<any>): Promise<any> {
        let cfg = mssqlDeconstruct(tenantMaster.connectionString);
        let pool = new mssql.ConnectionPool(cfg);
        pool.on(`error`, log.error);
        await pool.connect();
        try {
            return await fn(pool);
        } finally {
            pool.close();
        }
    }

    async getSqlTenants(): Promise<SqlTenant[]> {
        let result: SqlTenant[] = [];

        await Promise.all(this.tenantMasters.map(async tm => {
            await this.connect(tm, async pool => {

                let tenants = await sql.getSqlTenants(pool);

                tenants.map(t => result.push(Object.assign(t, {
                    sqlTenantMaster: tm
                })));
            })
        }));

        return result;
    }

    async getSqlTenant(hostname: string): Promise<SqlTenant> {
        let tenants = await this.getSqlTenants();
        return tenants.find(t => t.hostname === hostname);
    }

    async saveSqlTenant(t: SqlTenant): Promise<SqlTenant> {
        let master = this.getSqlTenantMaster(t.sqlTenantMaster?.id);
        if (master == null) {
            throw new Error(`Invalid TenantMaster`);
        }

        return this.connect(master, pool => sql.saveSqlTenant(pool, {
            sqlId: t.sqlId,
            billingAddress: t.billingAddress,
            billingCompanyName: t.billingCompanyName,
            currency: t.currency,
            emailFromAddress: t.emailFromAddress,
            enableDataWarehousePush: t.enableDataWarehousePush,
            enabled: t.enabled,
            hostname: t.hostname,
            invoiceIdPrefix: t.invoiceIdPrefix,
            luciferRelease: t.luciferRelease,
            name: t.name,
            tenantProfileId: t.tenantProfile.id,
            timezone: t.timezone
        }));
    }

    async deleteSqlTenant(t: SqlTenant): Promise<SqlTenant> {
        let master = this.getSqlTenantMaster(t.sqlTenantMaster?.id);
        if (master == null) {
            throw new Error(`Invalid TenantMaster`);
        }

        return this.connect(master, pool => sql.deleteSqlTenant(pool, t.sqlId));
    }

    async getSqlTenantProfilesForTenantMaster(id: number): Promise<SqlTenantProfile[]> {
        let master = this.getSqlTenantMaster(id);
        if (master == null) {
            throw new createHttpError.NotFound(`TenantMaster ${id} not found`);
        }
        return this.connect(master, pool => sql.getSqlTenantProfiles(pool));
    }

    async getSqlHostForTenantMaster(tenantMasterId: number): Promise<SqlTenantHost> {
        let master = this.getSqlTenantMaster(tenantMasterId);
        if (master == null) {
            throw new createHttpError.NotFound(`TenantMaster ${tenantMasterId} not found`);
        }
        return this.connect(master, async pool => {
            return _.first(await sql.getSqlHost(pool));
        });
    }

    async createNewTenant(lro: LongRunningOperation, tenantMasterId: number, newTenant: sql.SaveTenantRequest): Promise<SqlTenant> {
        let master = this.getSqlTenantMaster(tenantMasterId);
        if (master == null) {
            throw new createHttpError.NotFound(`TenantMaster ${tenantMasterId} not found`);
        }

        let lroLog = await lro.createIndeterminate(`Creating TenantMaster record`);
        try {
            return this.connect(master, async pool => {
                try {
                    let result = await sql.saveSqlTenant(pool, newTenant)
                    await lroLog.finished();
                    return result;
                } catch (err) {
                    await lroLog.errored(err);
                }
            });
        } catch (err) {
            await lroLog.errored(err);
        }
    }
}