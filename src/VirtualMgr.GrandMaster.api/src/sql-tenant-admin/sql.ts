import * as mssql from 'mssql';
import * as crypto from 'crypto';
import { deconstruct, mssqlDeconstruct } from '../sql-connection-string-builder';
import { SqlTenant, SqlTenantProfile, SqlTenantStatus, SqlTenantHost } from './types';
import * as _ from 'lodash';
import { log } from '../logging';

// Returns a unique hash for a tenant record so that connectionstring, profile or release changes can be detected
function makeContextHash(record: any): string {
    return crypto.createHash('sha256').update(`${record.Hostname},${record.Connectionstring},${record.ProfileName},${record.LuciferRelease}`).digest('hex');
}

export interface SaveTenantRequest {
    sqlId?: number;
    hostname: string;
    connectionString?: string;
    name: string;
    enabled: boolean;
    tenantHostId?: number;
    tenantProfileId: number;
    invoiceIdPrefix: string;
    timezone: string;
    billingAddress: string;
    billingCompanyName: string;
    currency: string;
    emailFromAddress: string;
    targetProfileId?: number;
    enableDataWarehousePush: boolean;
    luciferRelease: string;
}

export async function saveSqlTenant(pool: mssql.ConnectionPool, tenant: SaveTenantRequest): Promise<SqlTenant> {
    let request = new mssql.Request(pool);
    request.on('info', log.info);
    request.on('error', log.error);

    // For readability only - the order here is from the mtc.tblTenants table
    request.input('hostname', tenant.hostname);
    request.input('name', tenant.name);
    request.input('enabled', tenant.enabled);
    request.input('tenantProfileId', tenant.tenantProfileId);
    request.input('invoiceIdPrefix', tenant.invoiceIdPrefix);
    request.input('timezone', tenant.timezone);
    request.input('billingAddress', tenant.billingAddress);
    request.input('billingCompanyName', tenant.billingCompanyName);
    request.input('currency', tenant.currency);
    request.input('emailFromAddress', tenant.emailFromAddress);
    request.input('enableDataWarehousePush', tenant.enableDataWarehousePush);
    request.input('luciferRelease', tenant.luciferRelease);

    let query: string;
    // If no existing Id then INSERT 
    if (!tenant.sqlId) {
        request.input('connectionString', tenant.connectionString);
        request.input('targetProfileId', tenant.targetProfileId);
        request.input('tenantHostId', tenant.tenantHostId);

        query = `INSERT INTO mtc.tblTenants (
                Hostname,
                ConnectionString,
                Name,
                Enabled,
                TenantHostId,
                TenantProfileId,
                InvoiceIdPrefix,
                TimeZone,
                BillingAddress,
                BillingCompanyName,
                Currency,
                EmailFromAddress,
                TargetProfileId,
                EnableDataWarehousePush,
                LuciferRelease
            ) VALUES (
                @hostname,
                @connectionString,
                @name,
                @enabled,
                @tenantHostId,
                @tenantProfileId,
                @invoiceIdPrefix,
                @timezone,
                @billingAddress,
                @billingCompanyName,
                @currency,
                @emailFromAddress,
                @targetProfileId,
                @enableDataWarehousePush,
                @luciferRelease
            )`;
    } else {
        request.input('sqlId', tenant.sqlId);
        query = `UPDATE mtc.tblTenants SET 
                Hostname=@hostname,
                Name=@name,
                Enabled=@enabled,
                EnableDataWarehousePush=@enableDataWarehousePush,
                EmailFromAddress=@emailFromAddress,
                LuciferRelease=@luciferRelease,
                TimeZone=@timezone,
                TenantProfileId=@tenantProfileId
            WHERE id = @sqlId`;
    }

    let result = await request.query(query);

    if (result.rowsAffected[0] == 1) {
        return getSqlTenant(pool, tenant.hostname);
    }
}

export async function deleteSqlTenant(pool: mssql.ConnectionPool, tenantId: number): Promise<boolean> {
    let request = new mssql.Request(pool);
    request.on('info', log.info);
    request.on('error', log.error);

    request.input('id', tenantId);

    let query = `DELETE FROM mtc.tblTenants WHERE Id = @id`;
    let result = await request.query(query);

    return result.rowsAffected[0] == 1;
}

function makeTenantQuery(where: string): string {
    return `
        SELECT 
            t.id AS SqlId,
            t.Hostname,
            t.Name,
            t.ConnectionString,
            t.Enabled,
            t.LuciferRelease,
            t.TimeZone,
            t.EnableDataWarehousePush,
            t.EmailFromAddress,
            t.BillingAddress,
            t.BillingCompanyName,
            t.Currency,
            t.InvoiceIdPrefix,

            th.Id AS SqlHostId,
            th.Name AS SqlHostName,
            th.Location AS SqlHostLocation,
            th.WebAppResourceGroup AS SqlHostResourceGroup,
            th.ConnectionstringTemplate AS SqlHostConnectionStringTemplate,

            th.WebAppName,
            th.TrafficManagerName,
            
            tp.Id AS ProfileId,
            tp.Name AS ProfileName,
            tp.CanClone AS ProfileCanClone,
            tp.CanDelete AS ProfileCanDelete,
            tp.CanDisable AS ProfileCanDisable,
            tp.CanUpdateSchema AS ProfileCanUpdateSchema,
            tp.CreateAlertMessage AS ProfileCreateAlertMessage,
            tp.DatabaseAppend AS ProfileDatabaseAppend,
            tp.DomainAppend AS ProfileDomainAppend,
            tp.EmailEnabled AS ProfileEmailEnabled,
            tp.EmailWhitelist AS ProfileEmailWhitelist,
            tp.Hidden AS ProfileHidden,
                                    
            targetProfile.Id AS TargetProfileId,
            targetProfile.Name AS TargetProfileName,
            targetProfile.CanClone AS TargetProfileCanClone,
            targetProfile.CanDelete AS TargetProfileCanDelete,
            targetProfile.CanDisable AS TargetProfileCanDisable,
            targetProfile.CanUpdateSchema AS TargetProfileCanUpdateSchema,
            targetProfile.CreateAlertMessage AS TargetProfileCreateAlertMessage,
            targetProfile.DatabaseAppend AS TargetProfileDatabaseAppend,
            targetProfile.DomainAppend AS TargetProfileDomainAppend,
            targetProfile.EmailEnabled AS TargetProfileEmailEnabled,
            targetProfile.EmailWhitelist AS TargetProfileEmailWhitelist,
            targetProfile.Hidden AS TargetProfileHidden

        FROM mtc.tblTenants t
        INNER JOIN mtc.tblTenantHosts th ON t.TenantHostId = th.Id
        INNER JOIN mtc.tblTenantProfiles tp ON t.TenantProfileId = tp.Id
        INNER JOIN mtc.tblTenantProfiles targetProfile ON t.TargetProfileId = targetProfile.Id
        ${where} 
        ORDER BY Hostname`;
}

export async function refreshTenantStatus(tenant: SqlTenant): Promise<SqlTenant> {
    let online: boolean;
    let cfg = mssqlDeconstruct(tenant.connectionString);
    let pool = new mssql.ConnectionPool(cfg);
    try {
        await pool.connect();
        tenant.status = {
            databaseName: cfg.database,
            online: pool.connected,
            message: `Database online`
        }
    } catch (e) {
        tenant.status = {
            databaseName: cfg.database,
            online: false,
            message: e.message
        }
    } finally {
        await pool.close();
    }
    return tenant;
}

async function makeTenant(r: any): Promise<SqlTenant> {
    return refreshTenantStatus({
        sqlId: r.SqlId,
        hostname: r.Hostname,
        name: r.Name,
        enabled: r.Enabled,
        connectionString: r.ConnectionString,
        luciferRelease: r.LuciferRelease,        // This is the desired release for the tenant
        timezone: r.TimeZone,
        enableDataWarehousePush: r.EnableDataWarehousePush,
        emailFromAddress: r.EmailFromAddress,
        billingAddress: r.BillingAddress,
        billingCompanyName: r.BillingCompanyName,
        currency: r.Currency,
        invoiceIdPrefix: r.InvoiceIdPrefix,

        tenantProfile: {
            id: r.ProfileId,
            name: r.ProfileName,
            canClone: r.ProfileCanClone,
            canDelete: r.ProfileCanDelete,
            canDisable: r.ProfileCanDisable,
            canUpdateSchema: r.ProfileCanUpdateSchema,
            createAlertMessage: r.ProfileCreateAlertMessage,
            databaseAppend: r.ProfileDatabaseAppend,
            domainAppend: r.ProfileDomainAppend,
            emailEnabled: r.ProfileEmailEnabled,
            emailWhitelist: r.ProfileEmailWhitelist,
            hidden: r.ProfileHidden
        },

        targetProfile: {
            id: r.TargetProfileId,
            name: r.TargetProfileName,
            canClone: r.TargetProfileCanClone,
            canDelete: r.TargetProfileCanDelete,
            canDisable: r.TargetProfileCanDisable,
            canUpdateSchema: r.TargetProfileCanUpdateSchema,
            createAlertMessage: r.TargetProfileCreateAlertMessage,
            databaseAppend: r.TargetProfileDatabaseAppend,
            domainAppend: r.TargetProfileDomainAppend,
            emailEnabled: r.TargetProfileEmailEnabled,
            emailWhitelist: r.TargetProfileEmailWhitelist,
            hidden: r.TargetProfileHidden
        },

        sqlHost: {
            id: r.SqlHostId,
            name: r.SqlHostName,
            location: r.SqlHostLocation,
            resourceGroup: r.SqlHostResourceGroup,
            connectionStringTemplate: r.SqlHostConnectionStringTemplate
        },

        legacyWebappHostname: r.TrafficManagerName || `${r.WebAppName}.azurewebsites.net`,
        contextHash: makeContextHash(r),

        sqlTenantMaster: null,
        status: null
    })
}

export async function getSqlTenants(pool: mssql.ConnectionPool): Promise<SqlTenant[]> {
    let request = new mssql.Request(pool);
    request.on('info', log.info);
    request.on('error', log.error);

    let result = await request.query(makeTenantQuery(``));
    return Promise.all(result.recordset
        .map(async r => await makeTenant(r)));
}

export async function getSqlTenant(pool: mssql.ConnectionPool, hostname: string): Promise<SqlTenant> {
    let request = new mssql.Request(pool);
    request.on('info', log.info);
    request.on('error', log.error);

    request.input('hostname', hostname);
    let result = await request.query(makeTenantQuery(`WHERE Hostname = @hostname`));
    return _.first(result.recordset.map(r => makeTenant(r)));
}

export async function getSqlTenantProfiles(pool: mssql.ConnectionPool): Promise<SqlTenantProfile[]> {
    let request = new mssql.Request(pool);
    request.on('info', log.info);
    request.on('error', log.error);

    let result = await request.query(`SELECT Id, Name, CanClone, CanDelete, CanDisable, CanUpdateSchema, CreateAlertMessage, DatabaseAppend, DomainAppend, EmailEnabled, EmailWhitelist, Hidden FROM mtc.tblTenantProfiles`);
    return result.recordset.map(r => ({
        id: r.Id,
        name: r.Name,
        canClone: r.CanClone,
        canDelete: r.CanDelete,
        canDisable: r.CanDisable,
        canUpdateSchema: r.CanUpdateSchema,
        createAlertMessage: r.CreateAlertMessage,
        databaseAppend: r.DatabaseAppend,
        domainAppend: r.DomainAppend,
        emailEnabled: r.EmailEnabled,
        emailWhitelist: r.EmailWhitelist,
        hidden: r.Hidden
    }));
}

export async function getSqlHost(pool: mssql.ConnectionPool): Promise<SqlTenantHost[]> {
    let request = new mssql.Request(pool);
    request.on('info', log.info);
    request.on('error', log.error);

    let result = await request.query(`SELECT Id, Name, Location, WebAppResourceGroup AS ResourceGroup, ConnectionStringTemplate FROM mtc.tblTenantHosts`);
    return result.recordset.map(r => ({
        id: r.Id,
        name: r.Name,
        location: r.Location,
        resourceGroup: r.ResourceGroup,
        connectionStringTemplate: r.ConnectionStringTemplate
    }));
}