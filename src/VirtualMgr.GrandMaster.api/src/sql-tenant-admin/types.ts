export interface SqlTenantHost {
    id: number;
    name: string;
    location: string;
    resourceGroup: string;
    connectionStringTemplate: string;
}

export interface SqlTenantProfile {
    id: number;
    name: string;
    canDelete: boolean;
    emailEnabled: boolean;
    emailWhitelist: string;
    canClone: boolean;
    hidden: boolean;
    createAlertMessage: string;
    canUpdateSchema: boolean;
    canDisable: boolean;
    domainAppend: string;
    databaseAppend: string;
}

export interface SqlTenantMaster {
    id: number;
    name: string;
    connectionString: string;
}

export interface SqlTenantStatus {
    databaseName: string;
    online: boolean;
    message: string;
}

export interface SqlTenant {
    sqlId?: number;                          // This is unique within the SqlTenantMaster
    hostname: string;                       // This should be globally unique
    name: string;
    enabled: boolean;
    connectionString: string
    luciferRelease: string;                 // This is the desired release for the tenant
    timezone: string;
    enableDataWarehousePush: boolean;
    emailFromAddress: string;
    invoiceIdPrefix: string;
    currency: string;
    billingAddress: string;
    billingCompanyName: string;
    tenantProfile: SqlTenantProfile;              // The Profile from the SqlTenantMaster
    targetProfile: SqlTenantProfile;        // The Target Profile from the SqlTenantMaster of the Tenant
    sqlHost: SqlTenantHost;                 // Details of the Sql Host on the SqlTenantMaster

    legacyWebappHostname: string;
    contextHash: string;                    // Changes based on hostname, enabled, connectionString

    sqlTenantMaster: SqlTenantMaster;       // Details of the SqlTenantMaster

    status: SqlTenantStatus;
}
