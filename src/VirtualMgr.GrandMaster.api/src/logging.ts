'use strict';

import { config } from './config';
import { createLogger, stdSerializers } from 'bunyan';

const seq = require('bunyan-seq');

const bunyanLog = createLogger({
    name: `Lucifer.GrandMaster.api`,
    serializers: stdSerializers,
    streams: [{
        stream: process.stdout,
        level: `trace`
    }, seq.createStream({
        serverUrl: `http://seq:5341`,
        level: config.seq.level
    })]
})

let log;
if (config.consoleLoggingOnly) {
    log = console;
} else {
    log = bunyanLog;
}

export { log };