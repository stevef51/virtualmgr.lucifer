import { run as runWebServer } from './web-server';

import { config } from './config';
import { log } from './logging';

log.info(`Config = ${JSON.stringify(config, null, 2)}`);
log.trace(process.env);

async function main() {
    await runWebServer();
}

main();