import { config } from '../config';
import { MassTransitPublisher } from './mass-transit-publisher';
export { MassTransitPublisher } from './mass-transit-publisher';

const createPublisher: () => MassTransitPublisher = require(`./${config.massTransit.transport}-client`).default;

const publisher: MassTransitPublisher = createPublisher();

export { publisher };
