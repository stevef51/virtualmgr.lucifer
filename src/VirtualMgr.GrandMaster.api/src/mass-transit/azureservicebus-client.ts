import { MassTransitPublisher } from './mass-transit-publisher';
import { TenantConnectionsUpdatedMessage } from './publisher';
import { config } from '../config';
import { log } from '../logging';

// This module interfaces the tenant-watcher's "publisher" to MassTransit bus running RabbitMQ
const { ServiceBusClient } = require(`@azure/service-bus`);
const _config = config.massTransit.azureServiceBus;

function getConnectionString() {
    return `Endpoint=sb://${_config.namespace}.servicebus.windows.net/;SharedAccessKeyName=${_config.keyName};SharedAccessKey=${_config.sharedAccessKey}`;
}

class AzureServiceBusPublisher extends MassTransitPublisher {
    private sbc: any;
    private cn: any;

    async init() {
        this.cn = getConnectionString();
        log.debug(`Connecting to ${this.cn}`);
        this.sbc = ServiceBusClient.createFromConnectionString(this.cn);
    }

    // Cache exchange creations
    private topicClients = {};
    createTopicClient(name) {
        name = name.toLowerCase().replace(/:/g, `/`);
        if (!this.topicClients[name]) {
            this.topicClients[name] = this.sbc.createTopicClient(name);
        }
        return this.topicClients[name];
    }

    async publishMassTransit(exchangeName, msg) {
        let topicClient = this.createTopicClient(exchangeName);
        let sender = topicClient.createSender();

        try {
            await sender.send({
                body: msg,
                contentType: MassTransitPublisher.CONTENT_TYPE,
                correlationId: msg.conversationId,
                messageId: msg.messageId
            });
        } finally {
            await sender.close();
        }
    }


    makeDestinationAddress(msgTypes: string[]): string {
        return `sb://${_config.namespace}/${msgTypes[0]}`;
    }

    tenantConnectionsUpdated(message: TenantConnectionsUpdatedMessage): Promise<void> {
        return this.publishMassTransit(`VirtualMgr.MassTransit.Contracts:TenantConnectionsUpdated`, this.makeMassTransit(message,
            [
                "VirtualMgr.MassTransit.Contracts:TenantConnectionsUpdated",
                "VirtualMgr.MassTransit.Contracts:BaseEvent"
            ]));
    }
}

export default function createPublisher() {
    return new AzureServiceBusPublisher();
}
