import { Publisher, TenantConnectionsUpdatedMessage } from './publisher';
import { log } from '../logging';

const Timeout = require(`await-timeout`).Timeout;
const os = require(`os`);
const process = require(`process`);
const moment = require(`moment`);
const uuid = require(`uuidjs`);

export abstract class MassTransitPublisher implements Publisher {
    protected nextPublishId = 1;
    protected static readonly CONTENT_TYPE = `application/vnd.masstransit+json`;

    // Retry (with a fixed delay) the promise function until success 
    protected static async retryAndWait(fn, wait, log) {
        while (true) {
            try {
                return await fn();
            } catch (e) {
                log.error(e.toString());
                await Timeout.set(wait);
                log.info(`Retrying ${log || ''}`);
            }
        }
    }

    protected makeMassTransit(msg, msgTypes) {
        return {
            messageId: uuid.genV1().toString(),
            conversationId: uuid.genV1().toString(),
            destinationAddress: this.makeDestinationAddress(msgTypes),
            messageType: msgTypes.map(t => `urn:message:${t}`),
            message: msg,
            sentTime: moment.utc().format(),
            headers: {},
            host: {
                machineName: os.hostname(),
                processName: `VirtualMgr.GrandMaster`,
                processId: process.processId,
                massTransitVersion: "6.0.0.0",
                operatingSystemVersion: os.release()
            }
        }
    }

    abstract init(): Promise<void>;

    protected abstract makeDestinationAddress(msgTypes): String;

    abstract tenantConnectionsUpdated(message: TenantConnectionsUpdatedMessage): Promise<void>;
}