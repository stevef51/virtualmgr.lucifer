export interface TenantConnectionsUpdatedMessage {
    Added: string[];
    Removed: string[];
    Changed: string[];
}

export interface Publisher {
    init(): Promise<void>;
    tenantConnectionsUpdated(message: TenantConnectionsUpdatedMessage): Promise<void>;
}