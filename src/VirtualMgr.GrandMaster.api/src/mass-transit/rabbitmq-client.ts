import { MassTransitPublisher } from './mass-transit-publisher';
import { TenantConnectionsUpdatedMessage } from './publisher';
import { config } from '../config';

// This module interfaces the tenant-watcher's "publisher" to MassTransit bus running RabbitMQ
const amqp = require(`amqplib`);
const _config = config.massTransit.rabbitmq;

class RabbitMQPublisher extends MassTransitPublisher {
    private conn: any;
    private channel: any;

    // Cache exchange creations
    private exchanges = {};

    static connect() {
        let cn = `amqp://${_config.user}:${_config.password}@${_config.server}:5672`;
        console.debug(`Connecting to ${cn}`);
        return amqp.connect(cn);
    }

    async init() {
        // Wait indefinitely for RabbitMQ connection
        this.conn = await RabbitMQPublisher.retryAndWait(RabbitMQPublisher.connect, 5000, `Connect to RabbitMQ`);
        this.channel = await this.conn.createChannel();
    }

    makeDestinationAddress(msgTypes: string[]) {
        return `rabbitmq://${_config.server}/${msgTypes[0]}`;
    }

    async createExchangeAsync(name, type, options) {
        if (!this.exchanges[name]) {
            await this.channel.assertExchange(name, type, options);
            this.exchanges[name] = name;
        }
        return this.exchanges[name];
    }

    // Pubish a message to a RabbitMQ Exchange
    async publishMassTransit(exchangeName, msg) {
        let exchange = await this.createExchangeAsync(exchangeName, `fanout`, { durable: true });
        let msgText = JSON.stringify(msg, null, 2);

        return await this.channel.publish(exchange, ``, Buffer.from(msgText), {
            deliveryMode: 2,
            messageId: msg.messageId,
            contentType: RabbitMQPublisher.CONTENT_TYPE,
            headers: {
                "Content-Type": RabbitMQPublisher.CONTENT_TYPE,
                publishId: this.nextPublishId++
            }
        });
    }

    tenantConnectionsUpdated(message: TenantConnectionsUpdatedMessage): Promise<void> {
        return this.publishMassTransit(`VirtualMgr.MassTransit.Contracts:TenantConnectionsUpdated`, this.makeMassTransit(message,
            [
                "VirtualMgr.MassTransit.Contracts:TenantConnectionsUpdated",
                "VirtualMgr.MassTransit.Contracts:BaseEvent"
            ]));
    }
}

export default function createPublisher() {
    return new RabbitMQPublisher();
}
