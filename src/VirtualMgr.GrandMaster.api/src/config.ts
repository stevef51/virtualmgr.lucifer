const commonEnv = require('common-env')();
const program = require('commander');
import * as _ from 'lodash';

// The following configuration can be overriden by Environment variables
// eg
// VM__KUBERNETES = true:false
var env = commonEnv.getOrElseAll({
    VM: {
        consoleLoggingOnly: false,
        seq: {
            level: {
                $default: `info`,
                $aliases: [`VM_SEQ__LEVEL`]
            }
        },
        grandMaster: {
            mongoDb: {
                url: {
                    $default: `mongodb://mongodb:27017`,
                    $aliases: [`VM_GRANDMASTER__MONGODB__URL`]
                },
                dbName: {
                    $default: `lucifer`,
                    $aliases: [`VM_GRANDMASTER__MONGODB__DBNAME`]
                }
            }
        },
        azure: {
            subscriptionId: {
                $default: 'a25f9a86-51e8-42b3-801a-fe9e450c2964',
                $aliases: [`VM_AZURE__SUBSCRIPTIONID`]
            },
            clientId: {
                $default: 'a61e1b42-7efd-43df-a388-69b30febe844',
                $aliases: [`VM_AZURE__CLIENTID`]
            },
            clientSecret: {
                $default: '6OctHxGoSrvCxhS6u6J97uJkPw564nc24KJALgXn0Mw=',
                $aliases: [`VM_AZURE__CLIENTSECRET`]
            },
            tenantId: {
                $default: '4c35916c-de5e-4c46-b2c2-32b09f7e1c5f',
                $aliases: [`VM_AZURE__TENANTID`]
            }
        },
        kubernetes: {
            $default: false,
            $aliases: [`VM_KUBERNETES`]
        },
        massTransit: {
            transport: {
                $default: `rabbitmq`,
                $aliases: [`VM_MASSTRANSIT__TRANSPORT`]
            },
            rabbitmq: {
                server: {
                    $default: `rabbitmq`,
                    $aliases: [`VM_MASSTRANSIT__RABBITMQ__SERVER`]
                },
                user: {
                    $default: `guest`,
                    $aliases: [`VM_MASSTRANSIT__RABBITMQ__USER`]
                },
                password: {
                    $default: `guest`,
                    $aliases: [`VM_MASSTRANSIT__RABBITMQ__PASSWORD`]
                }
            },
            azureServiceBus: {
                namespace: {
                    $default: ``,
                    $aliases: [`VM_MASSTRANSIT__AZURESERVICEBUS__NAMESPACE`]
                },
                keyName: {
                    $default: ``,
                    $aliases: [`VM_MASSTRANSIT__AZURESERVICEBUS__KEYNAME`]
                },
                sharedAccessKey: {
                    $default: ``,
                    $aliases: [`VM_MASSTRANSIT__AZURESERVICEBUS__SHAREDACCESSKEY`]
                }
            }
        }
    }
});

// Grab any overrides from the command line ..
program
    .version('1.0.0.0')
    .option('--kubernetes')
    .option('--no-kubernetes')
    .parse(process.argv);

let config = Object.assign({}, env.VM, program);

export { config };
