const ConnectionStringBuilder = require('node-connection-string-builder');
import { config } from 'mssql';

export function mssqlDeconstruct(connectionString: string): config {
    let csb = new ConnectionStringBuilder(connectionString);
    let server = csb.dataSource.match(/(tcp:)?([^,]*)(,(\d*))?/);
    return {
        user: csb.userID,
        password: csb.password,
        server: server[2],
        port: server[4] ? Number(server[4]) : 1433,
        database: csb.initialCatalog,
        connectionTimeout: (csb.connectTimeout * 1000) || 30000,
        requestTimeout: 15000,
        options: {
            encrypt: true
        }
    }
}

export function deconstruct(connectionString: string): any {
    return new ConnectionStringBuilder(connectionString);
}

export function construct(csb: any): string {
    return csb.connectionString;
}
