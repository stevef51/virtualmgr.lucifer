using System;
using Microsoft.AspNetCore.Http;
using VirtualMgr.Membership.Interfaces;

namespace VirtualMgr.Membership.DirectCore
{
    public class MembershipCurrentContextProvider : ICurrentContextProvider
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public MembershipCurrentContextProvider(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public object ProviderUserKey
        {
            get
            {
                var claim = _httpContextAccessor.HttpContext?.User?.FindFirst("sub")?.Value;
                return claim;
            }
        }

        public Guid InternalUserId => Guid.Parse(ProviderUserKey.ToString());
    }
}