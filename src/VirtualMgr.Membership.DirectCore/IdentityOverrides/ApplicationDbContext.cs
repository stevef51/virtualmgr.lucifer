﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using VirtualMgr.MultiTenant;

namespace VirtualMgr.Membership.DirectCore.IdentityOverrides
{
    public class ApplicationDbContext : IdentityDbContext<IdentityUser, IdentityRole, string>
    {
        protected AppTenant _appTenant;

        public ApplicationDbContext(AppTenant appTenant)
        {
            _appTenant = appTenant;
        }

        public ApplicationDbContext(AppTenant appTenant,
            DbContextOptions<ApplicationDbContext> options) : base(options)
        {
            _appTenant = appTenant;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(_appTenant.ConnectionString);
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<IdentityUser>().HasKey(e => e.Id);
            builder.Entity<IdentityRole>().HasKey(e => e.Id);
        }
    }
}
