﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using VirtualMgr.Membership.DirectCore.IdentityOverrides;

namespace VirtualMgr.AspNetCore
{
    public class ApplicationClaimsTransformation : IClaimsTransformation
    {
        private readonly UserManager<IdentityUser> _userManager;

        public ApplicationClaimsTransformation(UserManager<IdentityUser> userManager)
        {
            _userManager = userManager;
        }

        public async Task<ClaimsPrincipal> TransformAsync(ClaimsPrincipal principal)
        {
            // Since this can be called multiple times in any request we cannot modify the principal, we have to return a new one
            var userId = principal.FindFirstValue("sub");
            var user = await _userManager.FindByIdAsync(userId);
            var userRoles = await _userManager.GetRolesAsync(user);

            var claimsIdentity = principal.Identity as ClaimsIdentity;
            var newClaims = claimsIdentity.Claims
                .Concat(from userRole in userRoles select new Claim("role", userRole))
                .Concat(new[] { new Claim("name", user.UserName) });
            var newIdentity = new ClaimsIdentity(newClaims, claimsIdentity.AuthenticationType, claimsIdentity.NameClaimType, claimsIdentity.RoleClaimType);

            return new ClaimsPrincipal(newIdentity);
        }
    }
}
