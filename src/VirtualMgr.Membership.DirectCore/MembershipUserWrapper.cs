﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using System;
using VirtualMgr.Membership.DirectCore.IdentityOverrides;

namespace VirtualMgr.Membership.DirectCore
{
    public class MembershipUserWrapper : IMembershipUser
    {
        private IdentityUser _membershipUser;
        private readonly UserManager<IdentityUser> _manager;

        public MembershipUserWrapper(IdentityUser membershipUser, UserManager<IdentityUser> manager)
        {
            _membershipUser = membershipUser;
            _manager = manager;
        }

        public object ProviderUserKey => _membershipUser.Id;

        public DateTime LastPasswordChangedDate => DateTime.MinValue;

        public DateTime LastActivityDate
        {
            get
            {
                return DateTime.MinValue;
            }
            set
            {

            }
        }
        public DateTime LastLoginDate
        {
            get
            {
                return DateTime.MinValue;
            }
            set
            {

            }
        }

        public DateTime CreationDate => DateTime.MinValue;

        public DateTime LastLockoutDate => DateTime.MinValue;

        public bool IsLockedOut
        {
            get
            {
                return _membershipUser.LockoutEnabled == true &&
                    _membershipUser.LockoutEnd.HasValue == true &&
                    _membershipUser.LockoutEnd.Value.UtcDateTime > DateTime.UtcNow;
            }
        }

        public bool IsApproved
        {
            get
            {
                return true;
            }
            set
            {

            }
        }
        public string Comment
        {
            get
            {
                return string.Empty;
            }
            set
            {

            }
        }

        public string PasswordQuestion => string.Empty;

        public string ProviderName => string.Empty;

        public bool IsOnline => true;

        public string UserName => _membershipUser.UserName;

        public string Email { get => _membershipUser.Email; set => _membershipUser.Email = value; }

        public bool ChangePassword(string oldPassword, string newPassword)
        {
            var result = _manager.ChangePasswordAsync(_membershipUser, oldPassword, newPassword).GetAwaiter().GetResult();

            return result.Succeeded;
        }

        public bool ChangePasswordQuestionAndAnswer(string password, string newPasswordQuestion, string newPasswordAnswer)
        {
            throw new NotImplementedException("This method is no longer supported under the identity framework");
        }

        public string GetPassword(string passwordAnswer)
        {
            throw new NotImplementedException("This method is no longer supported under the identity framework");
        }

        public string GetPassword()
        {
            throw new NotImplementedException("This method is no longer supported under the identity framework");
        }

        public string ResetPassword()
        {
            throw new NotImplementedException("This method is no longer supported under the identity framework");
        }

        public string ResetPassword(string passwordAnswer)
        {
            throw new NotImplementedException("This method is no longer supported under the identity framework");
        }

        public bool UnlockUser()
        {
            var result = _manager.SetLockoutEndDateAsync(_membershipUser, DateTimeOffset.UtcNow).GetAwaiter().GetResult();

            return result.Succeeded;
        }
    }
}
