﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VirtualMgr.Membership.DirectCore.IdentityOverrides;

namespace VirtualMgr.Membership.DirectCore
{
    public class RolesWrapper : IRoles, IDisposable
    {
        protected RoleManager<IdentityRole> _roleManager;
        protected RoleManager<IdentityRole> RoleManager
        {
            get
            {
                return _roleManager;
            }
        }

        protected UserManager<IdentityUser> _userManager;
        protected UserManager<IdentityUser> UserManager
        {
            get
            {
                return _userManager;
            }
        }

        public RolesWrapper(RoleManager<IdentityRole> roleManager, UserManager<IdentityUser> userManager)
        {
            _roleManager = roleManager;
            _userManager = userManager;
        }

        public string ApplicationName
        {
            get; set;
        }

        /// <summary>
        /// This should no longer be used
        /// </summary>
        public int CookieTimeout
        {
            get
            {
                return -1;
            }
        }

        public bool CacheRolesInCookie
        {
            get
            {
                return false;
            }
        }

        public string CookieName
        {
            get
            {
                return string.Empty;
            }
        }

        public bool Enabled { get; set; }

        public bool CookieSlidingExpiration => false;

        public string CookiePath => "";

        public bool CreatePersistentCookie => false;

        public string Domain => "";

        public int MaxCachedResults => 0;

        public bool CookieRequireSSL => true;

        public void AddUsersToRole(string[] usernames, string roleName)
        {
            usernames.ToList().ForEach(u =>
            {
                var user = UserManager.FindByNameAsync(u).GetAwaiter().GetResult();

                UserManager.AddToRoleAsync(user, roleName).GetAwaiter().GetResult();
            });
        }

        public void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            usernames.ToList().ForEach(u =>
            {
                var user = UserManager.FindByNameAsync(u).GetAwaiter().GetResult();

                if (user != null)
                {
                    UserManager.AddToRolesAsync(user, roleNames).GetAwaiter().GetResult();
                }
            });
        }

        public void AddUserToRole(string username, string roleName)
        {
            var user = UserManager.FindByNameAsync(username).GetAwaiter().GetResult();

            if (user != null)
            {
                UserManager.AddToRoleAsync(user, roleName).GetAwaiter().GetResult();
            }
        }

        public void AddUserToRoles(string username, string[] roleNames)
        {
            var user = UserManager.FindByNameAsync(username).GetAwaiter().GetResult();

            if (user != null)
            {
                UserManager.AddToRolesAsync(user, roleNames).GetAwaiter().GetResult();
            }
        }

        public void CreateRole(string roleName)
        {
            RoleManager.CreateAsync(new IdentityRole() { Name = roleName, Id = RT.Comb.Provider.Sql.Create().ToString() }).GetAwaiter().GetResult();
        }

        public void DeleteCookie()
        {

        }

        public bool DeleteRole(string roleName)
        {
            var role = RoleManager.FindByNameAsync(roleName).GetAwaiter().GetResult();

            if (role == null)
            {
                return false;
            }

            try
            {
                RoleManager.DeleteAsync(role).GetAwaiter().GetResult();
            }
            catch
            {
                return false;
            }


            return true;
        }

        public bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            if (throwOnPopulatedRole == false)
            {
                return DeleteRole(roleName);
            }

            var role = RoleManager.FindByNameAsync(roleName).GetAwaiter().GetResult();

            if (role == null)
            {
                return false;
            }

            RoleManager.DeleteAsync(role).GetAwaiter().GetResult();

            return true;
        }

        public string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException("This method is currently not implemented");
        }

        public string[] GetAllRoles()
        {
            return (from r in RoleManager.Roles orderby r.Name select r.Name).ToArray();
        }

        public string[] GetRolesForUser()
        {
            throw new NotImplementedException("This method is not implemented");
        }

        public string[] GetRolesForUser(string username)
        {
            var user = UserManager.FindByNameAsync(username).GetAwaiter().GetResult();

            return UserManager.GetRolesAsync(user).GetAwaiter().GetResult().ToArray();
        }

        public string[] GetUsersInRole(string roleName)
        {
            var role = RoleManager.FindByNameAsync(roleName).GetAwaiter().GetResult();

            if (role == null)
            {
                throw new ArgumentException(string.Format("{0} does not exist", roleName));
            }

            return UserManager.GetUsersInRoleAsync(roleName).GetAwaiter().GetResult().Select(u => u.Id).ToArray();
        }

        public bool IsUserInRole(string username, string roleName)
        {
            var user = UserManager.FindByNameAsync(username).GetAwaiter().GetResult();

            return UserManager.IsInRoleAsync(user, roleName).GetAwaiter().GetResult();
        }

        public bool IsUserInRole(string roleName)
        {
            throw new NotImplementedException("This method is not implemented");
        }

        public void RemoveUserFromRole(string username, string roleName)
        {
            var user = UserManager.FindByNameAsync(username).GetAwaiter().GetResult();
            UserManager.RemoveFromRoleAsync(user, roleName).GetAwaiter().GetResult();
        }

        public void RemoveUserFromRoles(string username, string[] roleNames)
        {
            var user = UserManager.FindByNameAsync(username).GetAwaiter().GetResult();
            UserManager.RemoveFromRolesAsync(user, roleNames).GetAwaiter().GetResult();
        }

        public void RemoveUsersFromRole(string[] usernames, string roleName)
        {
            usernames.ToList().ForEach(u =>
            {
                var user = UserManager.FindByNameAsync(u).GetAwaiter().GetResult();
                UserManager.RemoveFromRoleAsync(user, roleName).GetAwaiter().GetResult();
            });
        }

        public void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            usernames.ToList().ForEach(u =>
            {
                var user = UserManager.FindByNameAsync(u).GetAwaiter().GetResult();
                UserManager.RemoveFromRolesAsync(user, roleNames).GetAwaiter().GetResult();
            });
        }

        public bool RoleExists(string roleName)
        {
            return RoleManager.RoleExistsAsync(roleName).GetAwaiter().GetResult();
        }

        public void Dispose()
        {
            if (_roleManager != null)
                _roleManager.Dispose();

            if (_userManager != null)
                _userManager.Dispose();
        }
    }

}
