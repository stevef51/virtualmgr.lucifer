﻿using System;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using VirtualMgr.Membership.DirectCore.IdentityOverrides;
using VirtualMgr.Membership.Interfaces;

namespace VirtualMgr.Membership.DirectCore
{
    public class InjectionModule : ServiceCollection
    {
        public InjectionModule()
        {
            this.AddScoped<ICurrentContextProvider, MembershipCurrentContextProvider>();

            this.AddTransient<IdentityOverrides.ApplicationDbContext>();
            this.AddTransient<IMembership, MembershipWrapper>();
            this.AddTransient<IMembershipUser, MembershipUserWrapper>();
            this.AddTransient<IRoles, RolesWrapper>();

            //            this.AddIdentity<IdentityUser, IdentityRole>();
            this.AddTransient<UserManager<IdentityUser>>();
            this.AddTransient<RoleManager<IdentityRole>>();
            this.AddTransient<ILookupNormalizer, UpperInvariantLookupNormalizer>();
            this.AddTransient<IPasswordHasher<IdentityUser>, SqlPasswordHasher>();
            this.AddTransient<IUserStore<IdentityUser>, IdentityOverrides.ApplicationUserStore>();
            this.AddTransient<IRoleStore<IdentityRole>, IdentityOverrides.ApplicationRoleStore>();
            this.AddTransient<IdentityErrorDescriber>();

            this.AddTransient<IdentityUser>(provider =>
            {
                var currentContextProvider = provider.GetRequiredService<ICurrentContextProvider>();
                var userManager = provider.GetRequiredService<UserManager<IdentityUser>>();
                var userId = currentContextProvider.ProviderUserKey?.ToString();
                return userId == null ? null : userManager.FindByIdAsync(userId).GetAwaiter().GetResult();
            });
        }
    }
}
