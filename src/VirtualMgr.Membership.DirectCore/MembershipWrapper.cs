﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VirtualMgr.Membership.DirectCore.IdentityOverrides;
using VirtualMgr.Membership.Interfaces;

namespace VirtualMgr.Membership.DirectCore
{
    public class MembershipWrapper : IMembership
    {
        protected readonly UserManager<IdentityUser> _manager;
        protected readonly ICurrentContextProvider _currentUserProvider;

        public MembershipWrapper(UserManager<IdentityUser> manager, ICurrentContextProvider currentUserProvider)
        {
            _manager = manager;
            _currentUserProvider = currentUserProvider;
        }

        public int MinRequiredPasswordLength => 3;

        public IMembershipUser CreateUser(string username, string password)
        {
            var user = new IdentityUser();
            user.UserName = username;
            user.LockoutEnabled = true;

            var result = _manager.CreateAsync(user, password).GetAwaiter().GetResult();

            if (result == IdentityResult.Success)
            {
                var r = _manager.FindByNameAsync(username).GetAwaiter().GetResult();

                return new MembershipUserWrapper(r, _manager);
            }

            return null;
        }

        public bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            var result = _manager.FindByNameAsync(username).GetAwaiter().GetResult();

            return _manager.DeleteAsync(result).GetAwaiter().GetResult() == IdentityResult.Success;
        }

        public string GeneratePassword(int length, int numberOfNonAlphanumericCharacters)
        {
            throw new NotImplementedException("this method is not supported under the identity framework");
        }

        public IMembershipUser GetUser(object providerUserKey)
        {
            return new MembershipUserWrapper(_manager.FindByIdAsync(providerUserKey.ToString()).GetAwaiter().GetResult(), _manager);
        }

        public IMembershipUser GetUser(string username)
        {
            return new MembershipUserWrapper(_manager.FindByNameAsync(username).GetAwaiter().GetResult(), _manager);
        }

        public IMembershipUser GetUser()
        {
            return GetUser(_currentUserProvider.ProviderUserKey);
        }

        public IMembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            return GetUser(providerUserKey);
        }

        public bool ValidateUser(string username, string password)
        {
            var user = _manager.FindByNameAsync(username).GetAwaiter().GetResult();

            if (user == null || _manager.IsLockedOutAsync(user).GetAwaiter().GetResult())
            {
                return false;
            }

            // Valid user, verify password
            var result = _manager.PasswordHasher.VerifyHashedPassword(user, user.PasswordHash, password);
            if (result == PasswordVerificationResult.Success)
            {
                return true;
            }
            else if (result == PasswordVerificationResult.SuccessRehashNeeded)
            {
                // Logged in using old Membership credentials - update hashed password in database
                // Since we update the user on login anyway, we'll just set the new hash
                // Optionally could set password via the IdentityUserManager by using
                // RemovePassword() and AddPassword()
                user.PasswordHash = _manager.PasswordHasher.HashPassword(user, password);
                _manager.UpdateAsync(user).GetAwaiter().GetResult();

                return true;
            }
            else
            {
                // Failed login, increment failed login counter
                // Lockout for 15 minutes if more than 10 failed attempts
                user.AccessFailedCount++;

                //if (user.AccessFailedCount >= 10)
                //{
                //    user.LockoutEndDateUtc = DateTime.UtcNow.AddMinutes(15);
                //}

                _manager.UpdateAsync(user).GetAwaiter().GetResult();
                return false;
            }
        }
    }
}
