﻿using jsreport.Client;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace VirtualMgr.JsReport
{
    public class JsReportRequest
    {
        public string ShortId { get; set; }
        public string Recipe { get; set; }
        public string Token { get; set; }
        public bool Preview { get; set; }
        public Dictionary<string, Dictionary<string, object>> ParameterValues { get; set; } = new Dictionary<string, Dictionary<string, object>>();

        public JsReportRequest AddParameterValue(string paramId, object value)
        {
            Dictionary<string, object> parameter = null;
            if (!ParameterValues.TryGetValue(paramId, out parameter))
            {
                parameter = new Dictionary<string, object>();
                ParameterValues[paramId] = parameter;
            }
            parameter["value"] = value;
            return this;
        }

        public void ParseTemplateParameters(string json)
        {
            if (!string.IsNullOrEmpty(json))
            {
                var jObj = JObject.Parse(json);
                var jParameters = jObj["parameters"] ?? jObj["merge-parameters"];
                if (jParameters != null)
                {
                    foreach (var jParam in jParameters)
                    {
                        var jId = jParam["id"]?.ToString();
                        if (jId == null)
                        {
                            continue;
                        }
                        var jValue = jParam["value"];
                        if (jValue != null)
                        {
                            ParameterValues[jId] = new Dictionary<string, object>();
                            ParameterValues[jId]["value"] = jValue;
                        }
                    }
                }
            }
        }
    }

    public class JsReportGenerator
    {
        public string ServerUrl { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }

        public JsReportGenerator(string serverUrl, string username, string password)
        {
            ServerUrl = serverUrl;
            UserName = username;
            Password = password;
        }

        public class ReportResult
        {
            public Stream Content { get; set; }
            public HttpStatusCode StatusCode { get; set; }
            public string Message { get; set; }
            public string ContentType { get; set; }
            public string ContentDisposition { get; set; }
        }

        public async Task<ReportResult> GenerateAsync(JsReportRequest request)
        {
            var reportingService = new ReportingService(ServerUrl, UserName ?? "admin", Password ?? "0lerul3z");

            var body = new
            {
                template = new
                {
                    shortid = request.ShortId,
                    recipe = request.Recipe
                },
                options = new
                {
                    parameters = request.ParameterValues,
                    token = request.Token,
                    preview = request.Preview
                }
            };

            try
            {
                var report = await reportingService.RenderAsync(body);

                return new ReportResult()
                {
                    Content = report.Content,
                    ContentType = report.Meta.ContentType,
                    StatusCode = HttpStatusCode.OK,
                    ContentDisposition = report.Meta.ContentDisposition
                };
            }
            catch(JsReportException jsr)
            {
                return new ReportResult()
                {
                    StatusCode = jsr.Response.StatusCode,
                    Message = jsr.ResponseErrorMessage
                };
            }
        }
    }
}
