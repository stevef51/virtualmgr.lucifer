﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.RunTime;

namespace ConceptCave.Checklist.Editor
{
    public class VariableDataQuestion : Question, IVariableDataQuestion
    {
        public bool AllowMultiLine { get; set; }

        public override object DefaultAnswer
        {
            get
            {
                return _defaultAnswer;
            }
            set
            {
                _defaultAnswer = value;
            }
        }

        public override IAnswer CreateBlankAnswer(IPresentedQuestion presentedQuestion)
        {
            var result = new VariableDataAnswer() { PresentedQuestion = presentedQuestion };
            result.AnswerValue = DefaultAnswer;

            return result;
        }
    }
}
