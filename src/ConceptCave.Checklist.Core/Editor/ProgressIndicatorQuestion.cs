﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;

namespace ConceptCave.Checklist.Editor
{
    public class ProgressIndicatorQuestion : Question, IProgressIndicatorQuestion
    {
        public decimal MaxValue { get; set; }
        public decimal MinValue { get; set; }
        public decimal Value { get; set; }

        public ProgressIndicatorQuestion()
            : base()
        {
            MaxValue = 100;
        }

        public override object DefaultAnswer
        {
            get
            {
                return null;
            }
            set
            {
                
            }
        }

        public decimal ValueAsPercentage 
        {
            get
            {
                var value = MaxValue - MinValue;

                if (value == 0)
                {
                    return 0;
                }

                return Value / value;
            }
        }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("MaxValue", MaxValue);
            encoder.Encode("MinValue", MinValue);
            encoder.Encode("Value", Value);
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            MaxValue = decoder.Decode<decimal>("MaxValue");
            MinValue = decoder.Decode<decimal>("MinValue");
            Value = decoder.Decode<decimal>("Value");
        }

        public override IAnswer CreateBlankAnswer(IPresentedQuestion presentedQuestion)
        {
            return null;
        }

        public override bool IsAnswerable
        {
            get
            {
                return false;
            }
        }
    }
}
