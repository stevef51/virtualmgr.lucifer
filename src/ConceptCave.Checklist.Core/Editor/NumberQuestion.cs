﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;
using ConceptCave.Checklist.RunTime;
using System.Collections.Specialized;
using ConceptCave.Checklist.Reporting.LingoQuery;

namespace ConceptCave.Checklist.Editor
{
    public class NumberQuestion : Question, INumberQuestion
    {
        public int DecimalPlaces { get; set; }
        public decimal? MaxValue { get; set; }
        public decimal? MinValue { get; set; }

        public bool ShowKeypad { get; set; }
        public bool UseDecimalKeypad { get;set;}

        public override object DefaultAnswer
        {
            get
            {
                return _defaultAnswer;
            }
            set
            {
                if (value is decimal)
                {
                    _defaultAnswer = value;
                }
                else if (value is double || value is int)
                {
                    _defaultAnswer = decimal.Parse(value.ToString()); // we have to do this as doing a cast is throwing an unboxing exception
                }
                else if (value is string)
                {
                    decimal result = 0;
                    if (decimal.TryParse((string)value, out result) == true)
                    {
                        _defaultAnswer = result;
                    }
                }
                else if (value is INumberAnswer)
                {
                    _defaultAnswer = ((INumberAnswer)value).AnswerValue;
                }
                else if (value == null)
                {
                    _defaultAnswer = null;
                }
            }
        }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);
            encoder.Encode("DecimalPlaces", DecimalPlaces);
            encoder.Encode("MaxValue", MaxValue);
            encoder.Encode("MinValue", MinValue);
            encoder.Encode("ShowKeypad", ShowKeypad);
            encoder.Encode("UseDecimalKeypad", UseDecimalKeypad);
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);
            DecimalPlaces = decoder.Decode<int>("DecimalPlaces");
            MaxValue = decoder.Decode<decimal?>("MaxValue");
            MinValue = decoder.Decode<decimal?>("MinValue");
            ShowKeypad = decoder.Decode<bool>("ShowKeypad");
            UseDecimalKeypad = decoder.Decode<bool>("UseDecimalKeypad");
        }

        public override IAnswer CreateBlankAnswer(IPresentedQuestion presentedQuestion)
        {
            var result = new NumberAnswer() { PresentedQuestion = presentedQuestion };
            if (DefaultAnswer != null && DefaultAnswer is decimal)
            {
                result.AnswerValue = DefaultAnswer;
            }
            else if (DefaultAnswer != null && DefaultAnswer is NumberAnswer)
            {
                result.AnswerValue = ((NumberAnswer) DefaultAnswer).AnswerValue;
            }
            else
            {
                result.AnswerValue = null;
            }
            return result;
        }

        public override void FromNameValuePairs(System.Collections.Specialized.NameValueCollection pairs)
        {
            base.FromNameValuePairs(pairs);

            foreach (var name in pairs.AllKeys)
            {
                string n = name.ToLower();
                if (n == "decimalplaces")
                {
                    int p = 0;
                    if (int.TryParse(pairs[name], out p) == true)
                    {
                        this.DecimalPlaces = p;
                    }
                }
                else if (n == "maxvalue")
                {
                    decimal d = 0;
                    if (decimal.TryParse(pairs[name], out d))
                    {
                        this.MaxValue = d;
                    }
                }
                else if (n == "minvalue")
                {
                    decimal d = 0;
                    if (decimal.TryParse(pairs[name], out d))
                    {
                        this.MinValue = d;
                    }
                }
            }
        }

        public override List<NameValueCollection> ToNameValuePairs()
        {
            var result = base.ToNameValuePairs();

            if (this.DecimalPlaces != 0)
            {
                result[0].Add("decimalplaces", this.DecimalPlaces.ToString());
            }
            if (MaxValue.HasValue)
            {
                result[0].Add("maxvalue", this.MaxValue.ToString());
            }
            if (MinValue.HasValue)
            {
                result[0].Add("minvalue", this.MinValue.ToString());
            }
            return result;
        }
    }
}
