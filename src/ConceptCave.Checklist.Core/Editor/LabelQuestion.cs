﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.Editor;
using ConceptCave.Checklist.RunTime;
using System.Collections.Specialized;
using ConceptCave.Core;

namespace ConceptCave.Checklist.Editor
{
    public class LabelQuestion : Question, ILabelQuestion
    {
        public string Text { get { return LocalisableText.CurrentCultureValue; } set { LocalisableText.CurrentCultureValue = value; } }
        public ILocalisableString LocalisableText { get; private set; }
        public bool PopulateReporting { get; set; }

        /// <summary>
        /// Indicates wether or not the label should listen for event changes on the page. Used in dashboards to update the labels content
        /// </summary>
        public bool Listen { get; set; }

        /// <summary>
        /// The name of the question that the label should listen to
        /// </summary>
        public string ListenTo { get; set; }

        public bool HideUntilFirstListen { get; set; }

        public LabelQuestion()
        {
            LocalisableText = new LocalisableString();
        }

        public override void ChangeId(Guid newId)
        {
            base.ChangeId(newId);

            ((IIdAssignableUniqueNode)this.LocalisableText).ChangeId(CombFactory.NewComb());
        }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("LocalisableText", LocalisableText);
            encoder.Encode("PopulateReporting", PopulateReporting);
            encoder.Encode("Listen", Listen);
            encoder.Encode("ListenTo", ListenTo);
            encoder.Encode("HideUntilFirstListen", HideUntilFirstListen);
        }

        public override object DefaultAnswer
        {
            get
            {
                return null;
            }
            set
            {
                
            }
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            LocalisableText = LocalisableString.DecodeWithDefault(decoder, "Text", "");
            PopulateReporting = decoder.Decode<bool>("PopulateReporting");
            Listen = decoder.Decode<bool>("Listen");
            ListenTo = decoder.Decode<string>("ListenTo");
            HideUntilFirstListen = decoder.Decode<bool>("HideUntilFirstListen");
        }

        public override IAnswer CreateBlankAnswer(IPresentedQuestion presentedQuestion)
        {
            if (PopulateReporting == false)
            {
                return null;
            }

            return new FreeTextAnswer() { PresentedQuestion = presentedQuestion, AnswerValue = Text };
        }

        public override bool IsAnswerable
        {
            get
            {
                return PopulateReporting;
            }
        }

        public override void FromNameValuePairs(System.Collections.Specialized.NameValueCollection pairs)
        {
            base.FromNameValuePairs(pairs);

            foreach (var name in pairs.AllKeys)
            {
                string n = name.ToLower();

                if (n == "text")
                {
                    this.Text = pairs[name];
                }
                else if (n == "populatereporting")
                {
                    bool pop = false;
                    if(bool.TryParse(pairs[name], out pop) == true)
                    {
                        this.PopulateReporting = pop;
                    }
                }
            }
        }

        public override List<NameValueCollection> ToNameValuePairs()
        {
            var result = base.ToNameValuePairs();

            if (string.IsNullOrEmpty(this.Text) == false)
            {
                result[0].Add("text", this.Text);
            }

            if (this.PopulateReporting == true)
            {
                result[0].Add("populatereporting", this.PopulateReporting.ToString());
            }

            return result;
        }
    }
}
