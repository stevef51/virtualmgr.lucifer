﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;
using ConceptCave.Checklist.RunTime;
using System.Collections.Specialized;

namespace ConceptCave.Checklist.Editor
{
    public class MultiChoiceQuestion : Question, IMultiChoiceQuestion
    {
        public class Item : Node, IMultiChoiceItem, IConvertableToPrimitive
        {
            public string Answer { get; set; }
            public decimal Score { get; set; }
            public string Display { get { return LocalisableDisplay.CurrentCultureValue; } set { LocalisableDisplay.CurrentCultureValue = value; } }
            public ILocalisableString LocalisableDisplay { get; private set; }
            public bool? PassFail { get; set; }

            public Item()
            {
                LocalisableDisplay = new LocalisableString();
            }

            public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
            {
                base.Encode(encoder);

                encoder.Encode("Answer", Answer);
                encoder.Encode("Score", Score);
                encoder.Encode("LocalisableDisplay", LocalisableDisplay);
                encoder.Encode("PassFail", PassFail);
            }

            public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
            {
                base.Decode(decoder);

                Answer = decoder.Decode<string>("Answer", null);
                Score = decoder.Decode<decimal>("Score", 0);
                LocalisableDisplay = LocalisableString.DecodeWithDefault(decoder, "Display", "");
                PassFail = decoder.Decode<bool?>("PassFail", null);
            }

            public override string ToString()
            {
                return Display;
            }

            public override bool Equals(object obj)
            {
                if (obj is string)
                    return string.Compare(Answer, (string)obj, StringComparison.InvariantCultureIgnoreCase) == 0;

                return object.ReferenceEquals(this, obj);
            }

            public override int GetHashCode()
            {
                return Answer != null
                    ? Answer.ToLowerInvariant().GetHashCode()
                    : 1; //if null, need to always have the same hash code
            }

            #region IConvertableToPrimitive Members

            public object ConvertToPrimitive()
            {
                return Answer;
            }

            #endregion
        }

        public class ItemList : List<IMultiChoiceAnswer>
        {
            public override string ToString()
            {
                return string.Join(",", this);
            }

            public string ToString(string separator)
            {
                return string.Join(separator, this);
            }
        }

        public MultiChoiceQuestion()
        {
            NumberOfAnswersMustPass = 1;
        }

        private ListOf<IMultiChoiceItem> _choices = new ListOf<IMultiChoiceItem>();
        private int _numberOfChoicesAllowed = 1;

        #region IMultiChoiceQuestion Members

        public IListOf<IMultiChoiceItem> List
        {
            get { return _choices; }
        }

        public bool AllowMultiSelect { get; set; }
        
        private int _numberOfAnswersMustPass;

        public int NumberOfAnswersMustPass
        {
            get
            {
                if (AllowMultiSelect == false)
                {
                    return 1;
                }

                return _numberOfAnswersMustPass;
            }
            set
            {
                _numberOfAnswersMustPass = value;
            }
        }

        public int NumberOfChoicesAllowed
        {
            get
            {
                return AllowMultiSelect == false ? 1 : _numberOfChoicesAllowed;
            }
            set
            {
                _numberOfChoicesAllowed = value;
            }
        }

        public MultiChoiceQuestionFormat Format { get; set; }

        public bool RandomizeChoices { get; set; }

        #endregion

		private IEnumerable<IMultiChoiceItem> SelectDefaultAnswer(IMultiChoiceAnswer value)
		{
			return value.ChosenItems;
		}

		private IEnumerable<IMultiChoiceItem> SelectDefaultAnswer(IEnumerable<string> strings)
		{
			var list = new List<IMultiChoiceItem>();
			foreach (string v in strings)
			{
				var choice = (from c in this.List where string.Compare(c.Answer, v, true) == 0 select c).DefaultIfEmpty(null).First();

				if (choice == null)
				{
					continue;
				}

				list.Add(choice);
			}

			return list;
		}

        public override object DefaultAnswer
        {
            get
            {
                return _defaultAnswer;
            }
            set
            {
                IEnumerable<IMultiChoiceItem> items = null;

                if (value is IMultiChoiceAnswer)
                {
                    items = ((IMultiChoiceAnswer)value).ChosenItems;
                }
                else if (value is IEnumerable<IMultiChoiceItem>)
                {
                    items = (IEnumerable<IMultiChoiceItem>)value;
                }
                else if (value is IEnumerable<string>)
                {
                    IEnumerable<string> strings = (IEnumerable<string>)value;

                    var list = new List<IMultiChoiceItem>();
                    foreach (string v in strings)
                    {
                        var choice = (from c in this.List where string.Compare(c.Answer, v, true) == 0 select c).DefaultIfEmpty(null).First();

                        if (choice == null)
                        {
                            continue;
                        }

                        list.Add(choice);
                    }

                    items = list;
                }
                else if (value is string)
                {
                    string val = (string)value;

                    var list = new List<IMultiChoiceItem>();

                    var choice = (from c in this.List where string.Compare(c.Answer, val, true) == 0 select c).DefaultIfEmpty(null).First();

                    if (choice != null)
                    {
                        list.Add(choice);
                    }

                    items = list;
                }

                if (items == null)
                {
                    return;
                }

                var da = new ListOf<IMultiChoiceItem>();
                foreach (var val in ((IEnumerable<IMultiChoiceItem>)items))
                {
                    var item = (from o in this.Options where string.Compare(o.Answer, val.Answer, true) == 0 select o).DefaultIfEmpty(null).First();
                    if (item == null)
                    {
                        continue;
                    }

                    da.Items.Add(item);
                }

                _defaultAnswer = da;
            }
        }

        public override decimal PossibleScore
        {
            get
            {
                var o = (from c in _choices.Items orderby c.Score descending select c).Take(NumberOfChoicesAllowed);
                return o.Sum(i => i.Score);
            }
            set
            {
                base.PossibleScore = value;
            }
        }

        public override void ChangeId(Guid newId)
        {
            base.ChangeId(newId);

            foreach(var option in this.Options)
            {
                ((IIdAssignableUniqueNode)option.LocalisableDisplay).ChangeId(CombFactory.NewComb());
            }
        }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("AllowMultiSelect", AllowMultiSelect);
            encoder.Encode("NumberOfAnswersMustPass", NumberOfAnswersMustPass);
            encoder.Encode("NumberOfChoicesAllowed", NumberOfChoicesAllowed);
            encoder.Encode("ChoiceList", _choices);
            encoder.Encode("TemplatePublishedResourceId", TemplatePublishedResourceId);
            encoder.Encode("TemplateQuestionId", TemplateQuestionId);
            encoder.Encode("Format", Format);
            encoder.Encode("RandomizeChoices", RandomizeChoices);
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            AllowMultiSelect = decoder.Decode<bool>("AllowMultiSelect", false);
            NumberOfAnswersMustPass = decoder.Decode<int>("NumberOfAnswersMustPass", 1);
            NumberOfChoicesAllowed = decoder.Decode<int>("NumberOfChoicesAllowed", 1);
            _choices = decoder.Decode<ListOf<IMultiChoiceItem>>("ChoiceList", null);
			//var oChoices = decoder.Decode<object> ("ChoiceList", null);
			//_choices = (ListOf<IMultiChoiceItem>)oChoices;
            TemplatePublishedResourceId = decoder.Decode<int?>("TemplatePublishedResourceId");
            TemplateQuestionId = decoder.Decode<Guid?>("TemplateQuestionId");
            Format = decoder.Decode<MultiChoiceQuestionFormat>("Format", MultiChoiceQuestionFormat.List);
            RandomizeChoices = decoder.Decode<bool>("RandomizeChoices", false);
        }

        /// <summary>
        /// Returns the list of choices that should be presented. If the question is set to randomize the choices, this method will return a list in a random order, alternatively
        /// it will return a list in the same order as defined.
        /// </summary>
        /// <returns>Randomized or normal order list based on value of RandomizeChoices</returns>
        public virtual List<IMultiChoiceItem> GetChoicesForPresentation()
        {
            if (RandomizeChoices == true)
            {
                return GetRandomizedChoices();
            }

            return new List<IMultiChoiceItem>(this.List.Items);
        }

        /// <summary>
        /// Returns a list of the choices available in a random order. This method does not affect the order of the choices in the question, they
        /// are only in a random order in the returned list.
        /// </summary>
        /// <returns>List of choices for the question in a random order</returns>
        public virtual List<IMultiChoiceItem> GetRandomizedChoices()
        {
            List<IMultiChoiceItem> result = new List<IMultiChoiceItem>(this.List.Items);

            int n = result.Count;

            while (n > 1)
            {
                n--;
                int k = ThreadSafeRandom.ThisThreadsRandom.Next(n + 1);
                var value = result[k];
                result[k] = result[n];
                result[n] = value;
            }

            return result;
        }

        #region IMultiChoiceQuestion Members

        public override IAnswer CreateBlankAnswer(IPresentedQuestion presentedQuestion)
        {
            var answer = new MultiChoiceAnswer() { PresentedQuestion = presentedQuestion };

            if (DefaultAnswer != null && DefaultAnswer is ListOf<IMultiChoiceItem>)
            {
                answer.AnswerValue = DefaultAnswer;
            }
            else
            {
                answer.AnswerValue = null;
            }
            return answer;
        }

        #endregion

        #region Lingo helper stuff

        /// <summary>
        /// Returns the list of MultiChoiceItem objects, through an Lingo friendly property
        /// </summary>
        public List<IMultiChoiceItem> Options
        {
            get
            {
                return this.List.Items;
            }
        }

        /// <summary>
        /// Returns the list of MultiChoiceItem objects that have their passfail status == true
        /// </summary>
        public List<IMultiChoiceItem> CorrectOptions
        {
            get
            {
                return (from o in this.List.Items where o.PassFail.HasValue == true && o.PassFail.Value == true select o).ToList();
            }
        }

        /// <summary>
        /// Returns the list of MultiChoiceItem objects that have their passfail status == false
        /// </summary>
        public List<IMultiChoiceItem> IncorrectOptions
        {
            get
            {
                return (from o in this.List.Items where o.PassFail.HasValue == true && o.PassFail.Value == false select o).ToList();
            }
        }

        /// <summary>
        /// Lingo Helper property to get a list of the answer properties of the MultiChoiceItem objects
        /// </summary>
        public List<string> OptionAnswers
        {
            get
            {
                return (from c in List.Items select c.Answer).ToList();
            }
        }

        /// <summary>
        /// Lingo Helper property to get a list of the display properties of the MultiChoiceItem objects
        /// </summary>
        public List<string> OptionDisplays
        {
            get
            {
                return (from c in List.Items select c.Display).ToList();
            }
        }

        public List<string> CorrectOptionAnswers
        {
            get
            {
                return (from o in CorrectOptions select o.Answer).ToList();
            }
        }

        public List<string> CorrectOptionDisplays
        {
            get
            {
                return (from o in CorrectOptions select o.Display).ToList();
            }
        }

        public List<string> IncorrectOptionAnswers
        {
            get
            {
                return (from o in IncorrectOptions select o.Answer).ToList();
            }
        }

        public List<string> IncorrectOptionDisplays
        {
            get
            {
                return (from o in IncorrectOptions select o.Display).ToList();
            }
        }

        public bool IsOptionAnswerAvailable(string option)
        {
            return (from c in OptionAnswers where c == option select c).Count() > 0;
        }

        public bool IsOptionDisplayAvailable(string option)
        {
            return (from c in OptionDisplays where c == option select c).Count() > 0;
        }

        /// <summary>
        /// Just returns the total score of all of the items handed in. Typically used to prevent
        /// someone having to loop of the chosen set of answers in Lingo to get the score
        /// </summary>
        /// <param name="items"></param>
        /// <returns></returns>
        public decimal TotalScore(IEnumerable<IMultiChoiceItem> items)
        {
            return (from i in items select i.Score).Sum();
        }

        public void ClearOptions()
        {
            this.List.Items.Clear();
        }

        public Item AddOption(string display, string value)
        {
            Item i = new Item() { Display = display, Answer = value };

            List.Add(i);

            return i;
        }

        #endregion

        public int? TemplatePublishedResourceId { get; set; }

        public Guid? TemplateQuestionId { get; set; }

        public void MutateFromTemplate(ITemplatedPresentable template)
        {
            //template has to be a multi choice question
            var other = template as MultiChoiceQuestion;
            if (other == null)
                return;

            //copy: questions and answers
            (this.List as IList<IMultiChoiceItem>).Clear();
            foreach (var item in other.List)
                this.List.Add(item);
            //also copy: some config options
            this.AllowMultiSelect = other.AllowMultiSelect;
            this.AllowNotesOnAnswer = other.AllowNotesOnAnswer;
            this.HidePrompt = other.HidePrompt;
            this.NumberOfAnswersMustPass = other.NumberOfAnswersMustPass;
            this.NumberOfChoicesAllowed = other.NumberOfChoicesAllowed;
            
            //copy styling:
            //copy styling:
            if (this.HtmlPresentation.Alignment == HtmlPresentationHintAlignment.Default) // only override if its not set for us
            {
                this.HtmlPresentation.Alignment = other.HtmlPresentation.Alignment;
            }
            if (this.HtmlPresentation.Clear == HtmlPresentationHintClear.Both)
            {
                this.HtmlPresentation.Clear = other.HtmlPresentation.Clear;
            }
            if (string.IsNullOrEmpty(this.HtmlPresentation.CustomClass) == true)
            {
                this.HtmlPresentation.CustomClass = other.HtmlPresentation.CustomClass;
            }
            if (string.IsNullOrEmpty(this.HtmlPresentation.CustomStyling) == true)
            {
                this.HtmlPresentation.CustomStyling = other.HtmlPresentation.CustomStyling;
            }
            if (this.HtmlPresentation.LabelPlacement == HtmlPresentationHintLabelPlacement.Left)
            {
                this.HtmlPresentation.LabelPlacement = other.HtmlPresentation.LabelPlacement;
            }
            if (this.HtmlPresentation.Layout == HtmlSectionLayout.Flow)
            {
                this.HtmlPresentation.Layout = other.HtmlPresentation.Layout;
            }
        }

        public override void FromNameValuePairs(System.Collections.Specialized.NameValueCollection pairs)
        {
            base.FromNameValuePairs(pairs);

            List<string> options = new List<string>();
            List<string> optionsvalues = new List<string>();
            List<string> optionsscores = new List<string>();
            List<string> optionspasses = new List<string>();

            foreach (var name in pairs.AllKeys)
            {
                string n = name.ToLower();

                switch (n)
                {
                    case "options":
                        pairs[name].Split(',').ToList().ForEach(v => options.Add(v));
                        break;
                    case "options-values":
                        pairs[name].Split(',').ToList().ForEach(v => optionsvalues.Add(v));
                        break;
                    case "options-scores":
                        pairs[name].Split(',').ToList().ForEach(v => optionsscores.Add(v));
                        break;
                    case "options-passes":
                        pairs[name].Split(',').ToList().ForEach(v => optionspasses.Add(v));
                        break;
                    case "allowmultiselect":
                        bool allow = false;
                        if (bool.TryParse(pairs[name], out allow) == true)
                        {
                            this.AllowMultiSelect = allow;
                        }
                        break;
                    case "format":
                        MultiChoiceQuestionFormat f = MultiChoiceQuestionFormat.List;
                        if (Enum.TryParse(pairs[name], out f) == true)
                        {
                            this.Format = f;
                        }
                        break;
                }
            }

            // the options collection drives everything (if its got nothing, then no choices are added as there is no display value for them
            int i = 0;
            foreach (var option in options)
            {
                if(string.IsNullOrEmpty(option) == true)
                {
                    continue;
                }

                string val = null;
                string score = null;
                string pass = null;

                if (i < optionsvalues.Count - 1)
                {
                    val = optionsvalues[i];
                }
                else
                {
                    val = option;
                }

                if (i < optionsscores.Count - 1)
                {
                    score = optionsscores[i];
                }

                if (i < optionspasses.Count - 1)
                {
                    pass = optionspasses[i];
                }

                var choice = new Item() { Display = option, Answer = val };

                bool passfail = false;
                if (bool.TryParse(pass, out passfail) == true)
                {
                    choice.PassFail = passfail;
                }

                decimal v = 0;
                if (decimal.TryParse(score, out v) == true)
                {
                    choice.Score = v;
                }

                _choices.Items.Add(choice);

                i++;
            }
        }

        public override List<NameValueCollection> ToNameValuePairs()
        {
            var r = base.ToNameValuePairs();

            var result = r[0];

            if (this.AllowMultiSelect == true)
            {
                result.Add("allowmultiselect", this.AllowMultiSelect.ToString());
            }

            if (this.Format != MultiChoiceQuestionFormat.List)
            {
                result.Add("format", this.Format.ToString());
            }

            string options = "";
            string optionsValues = "";
            string optionsScores = "";
            string optionsPasses = "";

            var hasValues = (from o in this.Options where o.Answer != o.Display select o).Count() > 0;
            var hasScores = (from o in this.Options where o.Score != 0 select o).Count() > 0;
            var hasPasses = (from o in this.Options where o.PassFail.HasValue == true select o).Count() > 0;

            foreach (var option in this.Options)
            {
                options += option.Display + ",";

                if (hasValues == true)
                {
                    optionsValues += option.Answer + ",";
                }

                if (hasScores == true)
                {
                    optionsScores += option.Score.ToString("#") + ",";
                }

                if (hasPasses == true)
                {
                    optionsPasses += option.PassFail.HasValue ? option.PassFail.Value.ToString() : "";
                    optionsPasses += ",";
                }
            }

            options = options.TrimEnd(',');
            optionsValues = optionsValues.TrimEnd(',');
            optionsScores = optionsScores.TrimEnd(',');
            optionsPasses = optionsPasses.TrimEnd(',');

            result.Add("options", options);

            if (hasValues)
            {
                result.Add("options-values", optionsValues);
            }

            if (hasScores)
            {
                result.Add("options-scores", optionsScores);
            }

            if (hasPasses)
            {
                result.Add("options-passes", optionsPasses);
            }

            return r;
        }
    }
}
