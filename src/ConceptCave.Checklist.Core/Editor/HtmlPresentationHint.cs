﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core.Coding;

namespace ConceptCave.Checklist.Editor
{
    public class HtmlPresentationHint : IHtmlPresentationHint, ICodable
    {
        public string Prefix { get; private set; }

        public HtmlPresentationHint(string prefix = "") : this()
        {
            Prefix = prefix;
        }

        public HtmlPresentationHint()
        {
            Clear = HtmlPresentationHintClear.Both;
        }

        public HtmlSectionLayout Layout { get; set; }
        public HtmlPresentationHintAlignment Alignment { get; set; }
        public HtmlPresentationHintClear Clear { get; set; }
        public HtmlPresentationHintLabelPlacement LabelPlacement { get; set; }
        public string CustomStyling { get; set; }
        public string CustomClass { get; set; }

        public void Encode(IEncoder encoder)
        {
            if (Layout != HtmlSectionLayout.Flow)
                encoder.Encode(Prefix + "HtmlLayout", Layout);

            if (Alignment != HtmlPresentationHintAlignment.Default)
                encoder.Encode(Prefix + "HtmlAlignment", Alignment);

            if (Clear != HtmlPresentationHintClear.Both)
                encoder.Encode(Prefix + "HtmlClear", Clear);

            if (LabelPlacement != HtmlPresentationHintLabelPlacement.Right)
                encoder.Encode(Prefix + "HtmlLabelPlacement", LabelPlacement);

            if (CustomStyling != null && CustomStyling.Length > 0)
                encoder.Encode(Prefix + "HtmlCustomStyling", CustomStyling);

            if (CustomClass != null && CustomClass.Length > 0)
                encoder.Encode(Prefix + "HtmlCustomClass", CustomClass);
        }

        public void Decode(IDecoder decoder)
        {
            Layout = decoder.Decode<HtmlSectionLayout>(Prefix + "HtmlLayout", HtmlSectionLayout.Flow);
            Alignment = decoder.Decode<HtmlPresentationHintAlignment>(Prefix + "HtmlAlignment", HtmlPresentationHintAlignment.Default);
            Clear = decoder.Decode<HtmlPresentationHintClear>(Prefix + "HtmlClear", HtmlPresentationHintClear.Both);
            LabelPlacement = decoder.Decode<HtmlPresentationHintLabelPlacement>(Prefix + "HtmlLabelPlacement", HtmlPresentationHintLabelPlacement.Right);
            CustomStyling = decoder.Decode<string>(Prefix + "HtmlCustomStyling", "");
            CustomClass = decoder.Decode<string>(Prefix + "HtmlCustomClass", "");
        }
    }
}
