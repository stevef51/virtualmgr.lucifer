﻿using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;
using ConceptCave.Core.Coding;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConceptCave.Checklist.Editor
{
    public class LocalisableString : Node, ILocalisableString, IIdAssignableUniqueNode
    {
        private Dictionary<string, string> _translations;

        public LocalisableString()
        {
            _translations = new Dictionary<string, string>();
        }

        public string EnglishValue { get; set; }

        public string CurrentCultureValue
        {
            get
            {
                return GetCultureValue(Thread.CurrentThread.CurrentUICulture.Name);
            }
            set
            {
                SetCultureValue(Thread.CurrentThread.CurrentUICulture.Name, value);
            }
        }

        public string GetCultureValue(string cultureName)
        {
            string r = null;
            int dash = cultureName.Length;
            while (dash != -1)
            {
                string test = cultureName.Substring(0, dash);
                if (_translations.TryGetValue(test, out r))
                    return r;
                dash = cultureName.LastIndexOf('-', dash - 1);
            }

            if(string.IsNullOrEmpty(EnglishValue) && _translations.Count > 0)
            {
                return _translations.First().Value;
            }

            return EnglishValue;
        }

        public void SetCultureValue(string cultureName, string value)
        {
            _translations[cultureName] = value;
        }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);
            encoder.Encode("EnglishValue", EnglishValue);
            encoder.Encode("Translations", _translations);
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            EnglishValue = decoder.Decode<string>("EnglishValue", "");
            _translations = decoder.Decode<Dictionary<string, string>>("Translations", new Dictionary<string, string>());
        }


        public IDictionary<string, string> ToDictionary()
        {
            return _translations;
        }

        public void FromDictionary(IDictionary<string, string> translations)
        {
            _translations = new Dictionary<string, string>(translations);
        }

        public static LocalisableString DecodeWithDefault(IDecoder decoder, string name, string defaultValue)
        {
            var ls = decoder.Decode<LocalisableString>("Localisable" + name, null);
            if (ls == null)
            {
                ls = new LocalisableString();
                ls.EnglishValue = decoder.Decode<string>(name, defaultValue);
            }

            return ls;
        }


        public void ChangeId(Guid newId)
        {
            _id = newId;
        }
    }
}
