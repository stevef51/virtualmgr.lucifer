﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;
using ConceptCave.Checklist.RunTime;
using ConceptCave.Checklist.Lingo;
using ConceptCave.Core.XmlCodable;
using ConceptCave.Checklist.Core;
using System.Collections.Specialized;

namespace ConceptCave.Checklist.Editor
{
    public class Section : Node, ISection, IResource, IPresentableSource, IIdAssignableUniqueNode, IFromNameValues
    {
        private Func<int> _index = () => -1;
        public Func<int> Index { get { return _index; } set { _index = value; } }

        public HtmlPresentationHint HtmlPresentationHint;
        protected HtmlPresentationHintAlignment _alignment;
        protected HtmlPresentationHintClear _clear;
        public bool Enabled { get; set; }

        public bool AllowFilterByCategory { get; set; }

        /// <summary>
        /// Indicates if the section is the final section that will be presented as a part of a working document, this allows the
        /// UI to do anything special it needs to do for a final page of a working document.
        /// </summary>
        public bool PresentAsFinal { get; set; }

        public IClientModel ClientModel { get; private set; }

        public string Prompt { get { return LocalisablePrompt.CurrentCultureValue; } set { LocalisablePrompt.CurrentCultureValue = value; } }
        public ILocalisableString LocalisablePrompt { get; private set; }

        // The section uses the data source(s) to set its visibility.
        public string DataSource { get; set; }

        public string NgController { get; set; }

        public bool EnableDeepEdit { get; set; }

        public Section()
        {
            HtmlPresentationHint = new Editor.HtmlPresentationHint();

            ClientModel = new ClientModel();
            Enabled = true;

            LocalisablePrompt = new LocalisableString();
        }
        
        #region IHasName Members

        public string Name
        {
            get;
            set;
        }

        #endregion

        #region IQuestionSource Members

        public List<IPresentable> Children
        {
            get { return _children; }
        } List<IPresentable> _children = new List<IPresentable>();

        #endregion

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("Name", Name);
            encoder.Encode("LocalisablePrompt", LocalisablePrompt);

            HtmlPresentationHint.Encode(encoder);

            encoder.Encode("Children", _children);
            encoder.Encode("ClientModel", ClientModel);
            encoder.Encode("Enabled", Enabled);

            encoder.Encode("AllowFilterByCategory", AllowFilterByCategory);

            encoder.Encode("DataSource", DataSource);

            encoder.Encode("NgController", NgController);

            encoder.Encode("PresentAsFinal", PresentAsFinal);
            encoder.Encode("EnableDeepEdit", EnableDeepEdit);
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            Name = decoder.Decode<string>("Name", null);
            LocalisablePrompt = LocalisableString.DecodeWithDefault(decoder, "Prompt", "");

            HtmlPresentationHint.Decode(decoder);

            _children = decoder.Decode<List<IPresentable>>("Children", null);
            ClientModel = decoder.Decode<ClientModel>("ClientModel", new ClientModel());
            Enabled = decoder.Decode<bool>("Enabled", true); //default to enabled.

            AllowFilterByCategory = decoder.Decode<bool>("AllowFilterByCategory");

            DataSource = decoder.Decode<string>("DataSource");

            NgController = decoder.Decode<string>("NgController");

            PresentAsFinal = decoder.Decode<bool>("PresentAsFinal");
            EnableDeepEdit = decoder.Decode<bool>("EnableDeepEdit");
        }

        public IEnumerable<IPresentable> AsDepthFirstEnumerable()
        {
            yield return this;

            foreach (var c in Children)
            {
                foreach (var presentable in c.AsDepthFirstEnumerable())
                    yield return presentable;
            }
        }

        public IHtmlPresentationHint HtmlPresentation
        {
            get
            {
                return HtmlPresentationHint;
            }
        }

        public void ChangeId(Guid newId)
        {
            _id = newId;

            ((IIdAssignableUniqueNode)this.LocalisablePrompt).ChangeId(CombFactory.NewComb());
        }

        #region IPresentable Members

        public IPresented CreatePresented(IWorkingDocument document, string nameAs, IDisplayIndexProvider parent)
        {
            PresentedSection ps = new PresentedSection() { Section = this, Parent = parent };

            _children.Where(p => p.Enabled).ForEach(p => ps.Children.Add(p.CreatePresented(document, null, ps)));

            if (document.DefaultAnswerSource != null && (parent is PresentedSection) == false) // root level sections only want to do this
            {
                Dictionary<string, int> presentationCache = new Dictionary<string, int>();
                // populate answers from default answer source
                (from c in ps.AsDepthFirstEnumerable() where c is IPresentedQuestion select c).Cast<IPresentedQuestion>().ForEach(c =>
                {
                    object defaultAnswer = document.DefaultAnswerSource.Get(c.Question, document, this.Id, presentationCache);

                    if (defaultAnswer != null)
                    {
                        c.Question.DefaultAnswer = defaultAnswer;
                    }
                });
            }

            return ps;
        }

        #endregion

        public override string ToString()
        {
            return "Section: " + Name;
        }

        #region IPresentableSource Members

        public IPresentable GetPresentable(ILingoProgram program, ref string nameAs)
        {
            nameAs = Name;
            return this;
        }

        #endregion

        #region ISectionPresentationHint Members

        public HtmlPresentationHintAlignment Alignment
        {
            get { return HtmlPresentationHint.Alignment; }
            set { HtmlPresentationHint.Alignment = value; }
        }

        public HtmlPresentationHintClear Clear
        {
            get { return HtmlPresentationHint.Clear; }
            set { HtmlPresentationHint.Clear = value; }
        }

        public string CustomStyling
        {
            get { return HtmlPresentationHint.CustomStyling; }
            set { HtmlPresentationHint.CustomStyling = value; }
        }

        public HtmlSectionLayout Layout
        {
            get { return HtmlPresentationHint.Layout; }
            set { HtmlPresentationHint.Layout = value; }
        }

        #endregion

		protected ISection CopySection(ISection sourceSection)
		{
            var enc = CoderFactory.CreateEncoder();
			enc.Encode ("Copy", sourceSection);

			var dec = CoderFactory.GetDecoder (enc);

			return dec.Decode<ISection> ("Copy");
		}

        /// <summary>
        /// Lingo helper method to allow copying of contents from one section to another
        /// </summary>
        /// <param name="sourceSection"></param>
        public void CopyChildrenFrom(IPresentedSection sourceSection, bool copyAnswersToDefaults)
        {
			ISection s = CopySection (sourceSection.Section);

            _children = s.Children;

            foreach (var sq in (from p in sourceSection.AsDepthFirstEnumerable() where p is IPresentedQuestion select p).Cast<IPresentedQuestion>())
            {
                var questions = (from c in AsDepthFirstEnumerable() where c is IQuestion && ((IQuestion)c).Id == sq.Question.Id select c).Cast<IQuestion>();

                var answer = sq.GetAnswer();

                if (answer == null)
                {
                    continue;
                }

                questions.ForEach(q => q.DefaultAnswer = answer.AnswerValue);
            }
        }

        /// <summary>
        /// Lingo helper method to allow copying of contents from one section to another
        /// </summary>
        /// <param name="sourceSection"></param>
        public void CopyChildrenFrom(ISection sourceSection, bool copyAnswersToDefaults)
        {
			ISection s = CopySection (sourceSection);

            _children = s.Children;

            ManageCopiedChildren(sourceSection, copyAnswersToDefaults);
        }

        private void ManageCopiedChildren(ISection sourceSection, bool copyAnswersToDefaults)
        {
            if (copyAnswersToDefaults == true)
            {
                foreach (var sq in (from p in sourceSection.AsDepthFirstEnumerable() where p is IQuestion select p).Cast<IQuestion>())
                {
                    var questions = (from c in AsDepthFirstEnumerable() where c is IQuestion && ((IQuestion)c).Id == sq.Id select c).Cast<IQuestion>();

                    var answer = sq.DefaultAnswer;

                    if (answer == null)
                    {
                        continue;
                    }

                    questions.ForEach(q => q.DefaultAnswer = answer);
                }
            }

            // we need to change the id's of our children as otherwise we can have multiple presentables with the same id, which is likely to cause issues downline of this
            (from child in AsDepthFirstEnumerable() where child is IIdAssignableUniqueNode select child).Cast<IIdAssignableUniqueNode>().ForEach(c =>
            {
                c.ChangeId(CombFactory.NewComb());
            });
        }

        /// <summary>
        /// Similar to the CopyChildrenFrom method, though rather than copying over the top of what we have, this adds to what we have
        /// </summary>
        /// <param name="sourceSection"></param>
        /// <param name="copyAnswersToDefaults"></param>
        public void AddChildrenFrom(ISection sourceSection, bool copyAnswersToDefaults)
        {
			ISection s = CopySection (sourceSection);

            s.Children.ForEach(c => _children.Add(c));

            ManageCopiedChildren(sourceSection, copyAnswersToDefaults);
        }

        public void ClearChildren()
        {
            _children.Clear();
        }

        public void AssignDefaultAnswersFromPresentedSection(IPresentedSection sourceSection)
        {
            foreach (var quest in (from q in this.AsDepthFirstEnumerable() where q is IQuestion select q).Cast<IQuestion>())
            {
                var sq = (from q in ((PresentedSection)sourceSection).AsDepthFirstEnumerable() where q is IPresentedQuestion && ((IPresentedQuestion)q).Question.Id == quest.Id select q).Cast<IPresentedQuestion>();

                if (sq.Count() == 0)
                {
                    sq = (from q in ((PresentedSection)sourceSection).AsDepthFirstEnumerable() where q is IPresentedQuestion && ((IPresentedQuestion)q).Question.Name == quest.Name select q).Cast<IPresentedQuestion>();

                    if (sq.Count() == 0)
                    {
                        continue;
                    }
                }

                quest.DefaultAnswer = sq.First().GetAnswer();
            }
        }

        public void AssignDefaultAnswersFromSection(ISection sourceSection)
        {
            foreach (var quest in (from q in this.AsDepthFirstEnumerable() where q is IQuestion select q).Cast<IQuestion>())
            {
                var sq = (from q in ((Section)sourceSection).AsDepthFirstEnumerable() where q is IQuestion && ((IQuestion)q).Id == quest.Id select q).Cast<IQuestion>();

                if (sq.Count() == 0)
                {
                    sq = (from q in ((Section)sourceSection).AsDepthFirstEnumerable() where q is IQuestion && ((IQuestion)q).Name == quest.Name select q).Cast<IQuestion>();

                    if (sq.Count() == 0)
                    {
                        continue;
                    }
                }

                quest.DefaultAnswer = sq.First().DefaultAnswer;
            }
        }

        public void AssignDefaultAnswersFrom(object sourceSection)
        {
            if (sourceSection is IPresentedSection)
            {
                AssignDefaultAnswersFromPresentedSection((IPresentedSection)sourceSection);
            }
            else if (sourceSection is ISection)
            {
                AssignDefaultAnswersFromSection((ISection)sourceSection);
            }
        }

        /// <summary>
        /// For sections set to use the table presentation this method will perform the following transform
        /// A | B | C
        /// D | E | F
        /// 
        /// becomes
        /// 
        /// A | D
        /// B | E
        /// C | F
        /// </summary>
        public void TransformRowsToColumns()
        {
            switch (this.Layout)
            {
                case HtmlSectionLayout.Flow:
                    return;
                case HtmlSectionLayout.Table:
                    TransformTable();
                    break;
            }
        }

        protected void TransformTable()
        {
            var childSections = (from c in this.Children where c is Section select c).Cast<Section>();

            if (childSections.Count() == 0)
            {
                return; // ahh that was easy :)
            }

            var newRows = new List<Section>();

            // we'll use the first row to define ourselves. We will have as many rows as there are columns (children) in the first row
            for (var i = 0; i < childSections.First().Children.Count; i++)
            {
                newRows.Add(new Section() { Layout = HtmlSectionLayout.TableRow });
            }

            int index = 0;
            newRows.ForEach(r =>
            {
                childSections.ForEach(c =>
                {
                    if (index >= c.Children.Count)
                    {
                        return;
                    }

                    r.Children.Add(c.Children[index]);
                });

                index++;
            });

            this.ClearChildren();

            this.Children.AddRange(newRows);
        }

        public virtual void FromNameValuePairs(NameValueCollection pairs)
        {
            if (pairs == null || pairs.Count == 0)
            {
                return;
            }

            foreach (var name in pairs.AllKeys)
            {
                string value = pairs[name];
                
                switch (name.ToLower())
                {
                    case "name":
                        this.Name = value;
                        break;
                    case "prompt":
                        this.Prompt = value;
                        break;
                    case "customstyle":
                        this.HtmlPresentationHint.CustomStyling = value;
                        break;
                    case "customclass":
                        this.HtmlPresentationHint.CustomClass = value;
                        break;
                    case "layout":
                        HtmlSectionLayout l = HtmlSectionLayout.Flow;
                        if (Enum.TryParse(value, out l) == true)
                        {
                            this.HtmlPresentationHint.Layout = l;
                        }
                        break;
                    case "visible":
                        bool vis = true;
                        if (bool.TryParse(value, out vis) == true)
                        {
                            this.ClientModel.Visible = vis;
                        }
                        break;
                    case "enabled":
                        bool enble = true;
                        if (bool.TryParse(value, out enble) == true)
                        {
                            this.Enabled = enble;
                        }
                        break;
                    case "allowfilterbycategory":
                        bool filter = true;
                        if (bool.TryParse(value, out filter) == true)
                        {
                            this.AllowFilterByCategory = filter;
                        }
                        break;
                    case "enabledeepedit":
                        bool deepedit = true;
                        if (bool.TryParse(value, out deepedit) == true)
                        {
                            this.EnableDeepEdit = deepedit;
                        }
                        break;

                }
            }
        }

        public virtual List<NameValueCollection> ToNameValuePairs()
        {
            var result = new NameValueCollection();

            if (string.IsNullOrEmpty(Name) == false)
            {
                result.Add("name", Name);
            }

            if (string.IsNullOrEmpty(Prompt) == false)
            {
                result.Add("prompt", Prompt);
            }

            if (string.IsNullOrEmpty(HtmlPresentationHint.CustomStyling) == false)
            {
                result.Add("customstyle", HtmlPresentationHint.CustomStyling);
            }

            if (string.IsNullOrEmpty(HtmlPresentationHint.CustomClass) == false)
            {
                result.Add("customclass", HtmlPresentationHint.CustomClass);
            }

            if (HtmlPresentationHint.Layout != HtmlSectionLayout.Flow)
            {
                result.Add("layout", HtmlPresentationHint.Layout.ToString());
            }

            if (ClientModel.Visible == false)
            {
                result.Add("visible", ClientModel.Visible.ToString());
            }

            if (Enabled == false)
            {
                result.Add("enabled", Enabled.ToString());
            }

            if (AllowFilterByCategory == true)
            {
                result.Add("allowfilterbycategory", AllowFilterByCategory.ToString());
            }

            if (EnableDeepEdit == true)
            {
                result.Add("enabledeepedit", EnableDeepEdit.ToString());
            }

            return new List<NameValueCollection>(new NameValueCollection[] {result});
        }
    }
}
