﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.RunTime;

namespace ConceptCave.Checklist.Editor
{
    public class PayPalQuestion : Question, IPayPalQuestion
    {
        public string MerchantId { get; set; }

        public decimal Price { get; set; }

        public decimal? TaxRate { get; set; }

        public PayPalCurrency Currency { get; set; }

        /// <summary>
        /// Specifes wether or not the PayPal sandbox URL should be used or a live Url
        /// </summary>
        public bool UseSandbox { get; set; }

        /// <summary>
        /// Specifies a URL that should be sent to PayPal so that development can be tested (prevents localhost being sent to PayPal)
        /// </summary>
        public string DevelopmentUrl { get; set; }

        public override Interfaces.IAnswer CreateBlankAnswer(Interfaces.IPresentedQuestion presentedQuestion)
        {
            return new PayPalAnswer() { PresentedQuestion = presentedQuestion };
        }

        public override object DefaultAnswer
        {
            get
            {
                return _defaultAnswer;
            }
            set
            {
                if (value is PayPalAnswer)
                {
                    _defaultAnswer = ((PayPalAnswer)value).AnswerValue;
                }
            }
        }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("MerchantId", MerchantId);
            encoder.Encode("Price", Price);
            encoder.Encode("TaxRate", TaxRate);
            encoder.Encode("Currency", Currency);
            encoder.Encode("UseSandbox", UseSandbox);
            encoder.Encode("DevelopmentUrl", DevelopmentUrl);
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            MerchantId = decoder.Decode<string>("MerchantId");
            Price = decoder.Decode<decimal>("Price");
            TaxRate = decoder.Decode<decimal?>("TaxRate");
            Currency = decoder.Decode<PayPalCurrency>("Currency", PayPalCurrency.AUD);
            UseSandbox = decoder.Decode<bool>("UseSandbox");
            DevelopmentUrl = decoder.Decode<string>("DevelopmentUrl");
        }
    }
}
