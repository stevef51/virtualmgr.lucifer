﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;
using ConceptCave.Checklist.Core;
using ConceptCave.Checklist.Lingo;

namespace ConceptCave.Checklist.Editor
{
    public class DesignDocument : LingoDocument, IDesignDocument
    {
        public DesignDocument(Guid id)
            : this()
        {
            this._id = id;
        }

        public DesignDocument() : base()
        {
            _globals = new VariableBag();
            Variables = new VariableBag(_globals);
            _programDefinition = new LingoProgramDefinition();
        }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("Globals", _globals);
            encoder.Encode("ProgramDefinition", _programDefinition);
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            _globals = decoder.Decode<VariableBag>("Globals", null);
            _programDefinition = decoder.Decode<LingoProgramDefinition>("ProgramDefinition", null);
        }


        #region IDesignDocument Members

        public IVariableBag Globals
        {
            get { return _globals; }
        } IVariableBag _globals;

        public ILingoProgramDefinition ProgramDefinition
        {
            get { return _programDefinition; }
            set { _programDefinition = value; }
        } ILingoProgramDefinition _programDefinition;

        #endregion

    }
}
