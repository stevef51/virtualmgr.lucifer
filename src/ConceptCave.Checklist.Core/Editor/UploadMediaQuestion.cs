﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;
using ConceptCave.Checklist.RunTime;

namespace ConceptCave.Checklist.Editor
{
    public class UploadMediaQuestion : Question, IUploadMediaQuestion
    {
        private IMediaManager _mediaManager;
        public UploadMediaQuestion(IMediaManager mediaManager)
        {
            MaxUploadCount = 1;
            _mediaManager = mediaManager;
        }
        
        public int MaxUploadCount { get; set; }

        public int? MaxWidth { get; set; }
        public int? MaxHeight { get; set; }
        public bool KeepAspect { get; set; }
        public bool AllowImage { get; set; }
        public bool AllowVideo { get; set; }
        public bool AllowAudio { get; set; }
        public bool AllowAnyMediaType { get; set; }

        public string UploadPrompt { get; set; }

        public UploadMediaQuestionDestination Destination { get; set; }

        public override object DefaultAnswer
        {
            get
            {
                return _defaultAnswer;
            }
            set
            {
                if (value is IListOf<IUploadMediaAnswerItem>)
                {
                    _defaultAnswer = value;
                }
                else if (value is UploadMediaAnswer)
                {
                    _defaultAnswer = ((UploadMediaAnswer)value);
                }
            }
        }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("MaxUploadCount", MaxUploadCount);
            encoder.Encode("UploadPrompt", UploadPrompt);
            
            if (MaxWidth.HasValue)
                encoder.Encode("MaxWidth", MaxWidth);
            if (MaxHeight.HasValue)
                encoder.Encode("MaxHeight", MaxHeight);
            if (KeepAspect)
                encoder.Encode("KeepAspect", KeepAspect);
            if (AllowImage)
                encoder.Encode("AllowImage", AllowImage);
            if (AllowVideo)
                encoder.Encode("AllowVideo", AllowVideo);
            if (AllowAudio)
                encoder.Encode("AllowAudio", AllowAudio);
            if (AllowAnyMediaType)
                encoder.Encode("AllowAnyMediaType", AllowAnyMediaType);

            encoder.Encode("Destination", Destination);
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            MaxUploadCount = decoder.Decode<int>("MaxUploadCount");
            UploadPrompt = decoder.Decode<string>("UploadPrompt");
            MaxWidth = decoder.Decode<int?>("MaxWidth", null);
            MaxHeight = decoder.Decode<int?>("MaxHeight", null);
            KeepAspect = decoder.Decode<bool>("KeepAspect", false);
            AllowImage = decoder.Decode<bool>("AllowImage", false);
            AllowVideo = decoder.Decode<bool>("AllowVideo", false);
            AllowAudio = decoder.Decode<bool>("AllowAudio", false);
            AllowAnyMediaType = decoder.Decode<bool>("AllowAnyMediaType", false);
            Destination = decoder.Decode<UploadMediaQuestionDestination>("Destination");
        }

        public override IAnswer CreateBlankAnswer(IPresentedQuestion presentedQuestion)
        {
            var answer = new UploadMediaAnswer(_mediaManager) { PresentedQuestion = presentedQuestion };

            if (DefaultAnswer != null && DefaultAnswer is IListOf<IUploadMediaAnswerItem>)
            {
                answer.AnswerValue = DefaultAnswer;
            }
            else if (DefaultAnswer != null && DefaultAnswer is UploadMediaAnswer)
            {
                answer.AnswerValue = ((UploadMediaAnswer)DefaultAnswer).Items;
            }

            return answer;
        }
    }

}
