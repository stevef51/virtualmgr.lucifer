﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Core;
using ConceptCave.Checklist.Interfaces;

namespace ConceptCave.Checklist.Editor
{
    public class Dimension : Node, IDimension
    {
        public decimal Value { get; set; }
        public DimensionType DimensionType { get; set; }

        public Dimension() : base() 
        {
            DimensionType = Interfaces.DimensionType.Pixels;
        }

        public Dimension(decimal value)
            : this()
        {
            Value = value;
        }

        public Dimension(decimal value, DimensionType dimensionType) : this()
        {
            Value = value;
            DimensionType = dimensionType;
        }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);
            encoder.Encode("Value", Value);
            encoder.Encode("DimensionType", DimensionType);
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);
            Value = decoder.Decode<decimal>("Value");
            DimensionType = decoder.Decode<DimensionType>("DimensionType", Interfaces.DimensionType.Pixels);
        }
    }
}
