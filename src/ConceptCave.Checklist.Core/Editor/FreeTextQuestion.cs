﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;
using ConceptCave.Checklist.RunTime;

namespace ConceptCave.Checklist.Editor
{
    public class FreeTextQuestion : Question, IFreeTextQuestion
    {
        public bool AllowMultiLine { get; set; }

        public override object DefaultAnswer
        {
            get
            {
                return _defaultAnswer;
            }
            set
            {
                if (value is string)
                {
                    _defaultAnswer = value;
                }
                else if (value is IFreeTextAnswer)
                {
                    _defaultAnswer = ((IFreeTextAnswer)value).AnswerValue;
                }
            }
        }

        public override IAnswer CreateBlankAnswer(IPresentedQuestion presentedQuestion)
        {
            var result = new FreeTextAnswer() { PresentedQuestion = presentedQuestion };
            if(DefaultAnswer != null && DefaultAnswer is string)
            {
                result.AnswerValue = DefaultAnswer;
            }
            else if (DefaultAnswer != null && DefaultAnswer is FreeTextAnswer)
            {
                result.AnswerValue = ((FreeTextAnswer) DefaultAnswer).AnswerValue;
            }
            else
            {
                result.AnswerValue = null;
            }
            return result;
        }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("AllowMultiLine", AllowMultiLine);
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            AllowMultiLine = decoder.Decode<bool>("AllowMultiLine", false);
        }
    }
}
