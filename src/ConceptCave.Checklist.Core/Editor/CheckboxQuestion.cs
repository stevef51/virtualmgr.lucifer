﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;
using ConceptCave.Checklist.RunTime;
using System.Collections.Specialized;

namespace ConceptCave.Checklist.Editor
{
    public class CheckboxQuestion : Question, ICheckboxQuestion
    {
        public CheckboxQuestionNoAnswerBehaviour NoAnswerBehaviour { get; set; }

        public CheckboxQuestion()
        {
            NoAnswerBehaviour = CheckboxQuestionNoAnswerBehaviour.AnswerIsFalse;
        }

        public override object DefaultAnswer
        {
            get
            {
                return _defaultAnswer;
            }
            set
            {
                if (value is bool)
                {
                    _defaultAnswer = value;
                }
                else if (value is string)
                {
                    bool result = false;
                    if (bool.TryParse((string)value, out result) == true)
                    {
                        _defaultAnswer = result;
                    }
                }
                else if (value is ICheckboxAnswer)
                {
                    _defaultAnswer = ((ICheckboxAnswer)value).AnswerValue;
                }
            }
        }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("NoAnswerBehaviour", NoAnswerBehaviour);
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            NoAnswerBehaviour = decoder.Decode<CheckboxQuestionNoAnswerBehaviour>("NoAnswerBehaviour", CheckboxQuestionNoAnswerBehaviour.AnswerIsFalse);
        }

        public override IAnswer CreateBlankAnswer(IPresentedQuestion presentedQuestion)
        {
            var result = new CheckboxAnswer() { PresentedQuestion = presentedQuestion };

            if (DefaultAnswer != null)
            {
                result.AnswerValue = DefaultAnswer;
            }

            return result;
        }

        public override void FromNameValuePairs(System.Collections.Specialized.NameValueCollection pairs)
        {
            base.FromNameValuePairs(pairs);

            foreach (var name in pairs.AllKeys)
            {
                if (name.ToLower() == "noanswerbehaviour")
                {
                    CheckboxQuestionNoAnswerBehaviour behaviour = CheckboxQuestionNoAnswerBehaviour.AnswerIsNull;
                    if (Enum.TryParse(pairs[name], out behaviour) == true)
                    {
                        this.NoAnswerBehaviour = behaviour;
                    }
                }
            }
        }

        public override List<NameValueCollection> ToNameValuePairs()
        {
            var result = base.ToNameValuePairs();

            if (this.NoAnswerBehaviour != CheckboxQuestionNoAnswerBehaviour.AnswerIsNull)
            {
                result[0].Add("noanswerbehaviour", this.NoAnswerBehaviour.ToString());
            }

            return result;
        }

    }
}
