﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.RunTime;

namespace ConceptCave.Checklist.Editor
{
    public class PresentWordDocumentQuestion : Question, IPresentWordDocumentQuestion
    {
        public bool AllowDownload { get; set; }
        public WordDocumentPreviewType PreviewType { get; set; }

        public bool UseUseHighQualityRendering { get; set; }
        public bool UseAntiAliasing { get; set; }
        public float Resolution { get; set; }

        public bool MergeWithChecklist { get; set; }
        public Guid MediaId { get; set; }

        public bool TrackPageViewing { get; set; }

        public PresentWordDocumentQuestion()
        {
            Resolution = 96;
        }
        
        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);
            encoder.Encode("AllowDownload", AllowDownload);
            encoder.Encode("PreviewType", PreviewType);
            encoder.Encode("MergeWithChecklist", MergeWithChecklist);
            encoder.Encode("MediaId", MediaId);

            encoder.Encode("UseUseHighQualityRendering", UseUseHighQualityRendering);
            encoder.Encode("UseAntiAliasing", UseAntiAliasing);
            encoder.Encode("Resolution", Resolution);
            encoder.Encode("TrackPageViewing", TrackPageViewing);
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);
            AllowDownload = decoder.Decode<bool>("AllowDownload");
            PreviewType = decoder.Decode<WordDocumentPreviewType>("PreviewType");
            MergeWithChecklist = decoder.Decode<bool>("MergeWithChecklist");
            MediaId = decoder.Decode<Guid>("MediaId");

            UseUseHighQualityRendering = decoder.Decode<bool>("UseUseHighQualityRendering");
            UseAntiAliasing = decoder.Decode<bool>("UseAntiAliasing");
            Resolution = decoder.Decode<float>("Resolution");
            TrackPageViewing = decoder.Decode<bool>("TrackPageViewing");
        }

        public override object DefaultAnswer
        {
            get
            {
                return null;
            }
            set
            {

            }
        }

        public override bool IsAnswerable
        {
            get
            {
                return true;
            }
        }

        public override IAnswer CreateBlankAnswer(IPresentedQuestion presentedQuestion)
        {
            var result = new PresentWordDocumentAnswer() { PresentedQuestion = presentedQuestion };
            
            if (DefaultAnswer != null && DefaultAnswer is string)
            {
                result.LastPageRead = int.Parse((string)DefaultAnswer);
            }
            else if (DefaultAnswer != null && DefaultAnswer is int)
            {
                result.LastPageRead = (int)DefaultAnswer;
            }
            else
            {
                result.AnswerValue = null;
            }

            return result;
        }

        public override void FromNameValuePairs(System.Collections.Specialized.NameValueCollection pairs)
        {
            base.FromNameValuePairs(pairs);

            foreach (var name in pairs.AllKeys)
            {
                string n = name.ToLower();
                if (n == "allowdownload")
                {
                    bool allow = false;
                    if (bool.TryParse(pairs[name], out allow) == true)
                    {
                        this.AllowDownload = allow;
                    }
                }
                else if (n == "usehighquality")
                {
                    bool high = false;
                    if (bool.TryParse(pairs[name], out high) == true)
                    {
                        this.UseUseHighQualityRendering = high;
                    }
                }
                else if (n == "useantialiasing")
                {
                    bool anti = false;
                    if (bool.TryParse(pairs[name], out anti) == true)
                    {
                        this.UseAntiAliasing = anti;
                    }
                }
                else if (n == "resolution")
                {
                    float res = 0;
                    if (float.TryParse(pairs[name], out res) == true)
                    {
                        this.Resolution = res;
                    }
                }
                else if (n == "mergewithchecklist")
                {
                    bool merge = false;
                    if (bool.TryParse(pairs[name], out merge) == true)
                    {
                        this.MergeWithChecklist = merge;
                    }
                }
                else if (n == "mediaid")
                {
                    Guid mediaid = Guid.Empty;
                    if (Guid.TryParse(pairs[name], out mediaid) == true)
                    {
                        this.MediaId = mediaid;
                    }
                }
                else if (n == "trackpageviewing")
                {
                    bool track = false;
                    if (bool.TryParse(pairs[name], out track) == true)
                    {
                        this.TrackPageViewing = track;
                    }
                }
            }
        }

        public override List<NameValueCollection> ToNameValuePairs()
        {
            var r = base.ToNameValuePairs();

            var result = r[0];

            if (AllowDownload == true)
            {
                result.Add("allowdownload", AllowDownload.ToString());
            }

            if (UseUseHighQualityRendering == true)
            {
                result.Add("usehighquality", UseUseHighQualityRendering.ToString());
            }

            if (UseAntiAliasing == true)
            {
                result.Add("useantialiasing", UseAntiAliasing.ToString());
            }

            if(Resolution != 96)
            {
                result.Add("resolution", Resolution.ToString());
            }

            if (MergeWithChecklist == true)
            {
                result.Add("mergewithchecklist", MergeWithChecklist.ToString());
            }

            if (MediaId != Guid.Empty)
            {
                result.Add("mediaid", MediaId.ToString());
            }

            if (TrackPageViewing == true)
            {
                result.Add("trackpageviewing", TrackPageViewing.ToString());
            }

            return r;
        }
    }

}
