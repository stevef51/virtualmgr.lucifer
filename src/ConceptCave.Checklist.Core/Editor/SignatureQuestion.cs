﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.RunTime;
using ConceptCave.Core.XmlCodable;

namespace ConceptCave.Checklist.Editor
{
    public class SignatureQuestion : Question, ISignatureQuestion
    {
        public bool AllowTypedEntry { get; set; }

        public IDimension Width { get; set; }

        public IDimension Height { get; set; }

        public bool TakePictureWhenSigning { get; set; }
        public bool ShowTakenPicture { get; set; }

        private IMediaManager MediaManager { get; set; }

        public SignatureQuestion(IMediaManager mediaManager)
        {
            Width = new Dimension(198);
            Height = new Dimension(55);

            MediaManager = mediaManager;
        }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("AllowTypedEntry", AllowTypedEntry);
            encoder.Encode("Width", Width);
            encoder.Encode("Height", Height);

            encoder.Encode("TakePictureWhenSigning", TakePictureWhenSigning);
            encoder.Encode("ShowTakenPicture", ShowTakenPicture);
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            AllowTypedEntry = decoder.Decode<bool>("AllowTypedEntry");
            Width = decoder.Decode<Dimension>("Width");
            Height = decoder.Decode<Dimension>("Height");

            TakePictureWhenSigning = decoder.Decode<bool>("TakePictureWhenSigning");
            ShowTakenPicture = decoder.Decode<bool>("ShowTakenPicture");
        }

        public override object DefaultAnswer
        {
            get
            {
                return _defaultAnswer;
            }
            set
            {
                if (value is string)
                {
                    _defaultAnswer = value;
                }
                else if (value is IFreeTextAnswer)
                {
                    _defaultAnswer = ((ISignatureAnswer)value).AnswerAsJsonString;
                }
            }
        }

        public override IAnswer CreateBlankAnswer(IPresentedQuestion presentedQuestion)
        {
            var result = new SignatureAnswer(MediaManager) { PresentedQuestion = presentedQuestion };
            if (DefaultAnswer != null && DefaultAnswer is string)
            {
                result.AnswerValue = DefaultAnswer;
            }
            else if (DefaultAnswer != null && DefaultAnswer is SignatureAnswer)
            {
                result.AnswerValue = ((SignatureAnswer) DefaultAnswer).AnswerValue;
            }
            else
            {
                result.AnswerValue = null;
            }
            return result;
        }
    }
}
