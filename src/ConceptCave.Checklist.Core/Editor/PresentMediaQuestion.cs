﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.RunTime;
using ConceptCave.Core;

namespace ConceptCave.Checklist.Editor
{
    public class PresentMediaQuestion : Question, IPresentMediaQuestion
    {
        #region IPresentMediaQuestion Members

        public string Url { get; set; }
        public PresentMediaQuestionType MediaType { get; set; }
        public IDimension Width { get; set; }
        public IDimension Height { get; set; }
        public IPresentMediaQuestionSettings Settings { get; set; }

        #endregion

        public PresentMediaQuestion() : base()
        {
            Width = new Dimension(420);
            Height = new Dimension(315);
        }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("Url", Url);
            encoder.Encode("MediaType", MediaType);
            encoder.Encode("Width", Width);
            encoder.Encode("Height", Height);

            if (Settings != null)
            {
                encoder.Encode("Settings", Settings);
            }
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            Url = decoder.Decode<string>("Url");
            MediaType = decoder.Decode<PresentMediaQuestionType>("MediaType");
            Width = decoder.Decode<IDimension>("Width", new Dimension());
            Height = decoder.Decode<IDimension>("Height", new Dimension());
            Settings = decoder.Decode<IPresentMediaQuestionSettings>("Settings", null);
        }

        public override object DefaultAnswer
        {
            get
            {
                return _defaultAnswer;
            }
            set
            {
                if (value is decimal?)
                {
                    _defaultAnswer = value;
                }
                else if (value is IPresentMediaAnswer)
                {
                    _defaultAnswer = ((IPresentMediaAnswer)value).Position;
                }
            }
        }

        public override Interfaces.IAnswer CreateBlankAnswer(Interfaces.IPresentedQuestion presentedQuestion)
        {
            var result = new PresentMediaAnswer() { PresentedQuestion = presentedQuestion };
            if (DefaultAnswer != null && DefaultAnswer is decimal?)
            {
                result.AnswerValue = DefaultAnswer;
            }

            return result;
        }

        public override bool IsAnswerable
        {
            get
            {
                return true;
            }
        }
    }

    public class YouTubePresentMediaQuestionSettings : Node, IYouTubePresentMediaQuestionSettings
    {
        public bool AllowFullScreen { get; set; }
        public bool ModestBranding { get; set; }
        public bool ShowRelatedVideos { get; set; }
        public bool ShowInfo { get; set; }
        public YouTubePresentedMediaQuestionSettingsControls ControlsType { get; set; }

        public YouTubePresentMediaQuestionSettings()
            : base()
        {
            ModestBranding = true;
        }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("AllowFullScreen", AllowFullScreen);
            encoder.Encode("ModestBranding", ModestBranding);
            encoder.Encode("ShowRelatedVideos", ShowRelatedVideos);
            encoder.Encode("ShowInfo", ShowInfo);
            encoder.Encode("ControlsType", ControlsType);
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            AllowFullScreen = decoder.Decode<bool>("AllowFullScreen", false);
            ModestBranding = decoder.Decode<bool>("ModestBranding", true);
            ShowRelatedVideos = decoder.Decode<bool>("ShowRelatedVideos", false);
            ShowInfo = decoder.Decode<bool>("ShowInfo", false);
            ControlsType = decoder.Decode<YouTubePresentedMediaQuestionSettingsControls>("ControlsType", YouTubePresentedMediaQuestionSettingsControls.ImmediateControls);
        }
    }

    public class PresentedMediaQuestionTrackEvent : ClientModelEvent, IPresentedMediaQuestionTrackEvent
    {
        public decimal Start {get;set;}
        public decimal End { get; set; }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("Start", Start);
            encoder.Encode("End", End);
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            Start = decoder.Decode<decimal>("Start");
            End = decoder.Decode<decimal>("End");
        }
    }

    public class PauseMediaClientModelEventAction : ClientModelEventAction
    {

    }

    public class SeekToClientModelEventAction : ClientModelEventAction
    {
        public decimal SeekPosition { get; set; }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("SeekPosition", SeekPosition);
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            SeekPosition = decoder.Decode<decimal>("SeekPosition");
        }
    }
}
