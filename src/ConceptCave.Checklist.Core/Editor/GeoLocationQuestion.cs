﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.RunTime;
using ConceptCave.Core;

namespace ConceptCave.Checklist.Editor
{
    public class GeoLocationQuestion : Question, IGeoLocationQuestion
    {
        public override IAnswer CreateBlankAnswer(IPresentedQuestion presentedQuestion)
        {
            var a = new GeoLocationAnswer() { PresentedQuestion = presentedQuestion };
            a.AnswerValue = null;
            return a;
        }

        public override object DefaultAnswer
        {
            get
            {
                return _defaultAnswer;
            }
            set
            {
                if (value is ILatLng)
                {
                    _defaultAnswer = value;
                }
                else if (value is IGeoLocationAnswer)
                {
                    _defaultAnswer = ((IGeoLocationAnswer)value).Coordinates;
                }
            }
        }

        public bool LookUpAddress { get; set; }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            if (LookUpAddress)
                encoder.Encode("LookUpAddress", LookUpAddress);
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            LookUpAddress = decoder.Decode<bool>("LookUpAddress", false);
        }
    }
}
