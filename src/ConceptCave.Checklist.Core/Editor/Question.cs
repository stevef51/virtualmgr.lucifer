﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core.Coding;
using ConceptCave.Core;
using ConceptCave.Checklist.RunTime;
using ConceptCave.Checklist.Commands.Runtime;
using ConceptCave.Checklist.Lingo;
using System.Collections.Specialized;
using System.Threading;

namespace ConceptCave.Checklist.Editor
{
    public abstract class Question : Node, IQuestion, IResource, IPresentableSource, IIdAssignableUniqueNode, IFromNameValues
    {
        protected object _defaultAnswer;

        private Func<int> _index = () => -1;
        public Func<int> Index { get { return _index; } set { _index = value; } }
        public string Prompt {
            get
            {
                return LocalisablePrompt.CurrentCultureValue;
            }
            set
            {
                LocalisablePrompt.CurrentCultureValue = value;
            }
        }
        public ILocalisableString LocalisablePrompt { get; private set; }
        public bool HidePrompt { get; set; }
        public string Note { get { return LocalisableNote.CurrentCultureValue; } set { LocalisableNote.CurrentCultureValue = value; } }
        public ILocalisableString LocalisableNote { get; private set; }
        public string Name { get; set; }
        public bool AllowNotesOnAnswer { get; set; }
        public virtual decimal PossibleScore { get; set; }
        public List<IProcessingRule> Validators { get; private set; }
        public IClientModel ClientModel { get; private set; }
        public HtmlPresentationHint HtmlPresentationHint { get; private set; }
        public bool Enabled { get; set; }

        public abstract object DefaultAnswer { get; set; }

        public ISet<Guid> Labels { get; private set; }

        public HtmlPresentationHintLabelPlacement HelpPosition { get; set; }
        public string HelpContent { get { return LocalisableHelpContent.CurrentCultureValue; } set { LocalisableHelpContent.CurrentCultureValue = value; } }
        public ILocalisableString LocalisableHelpContent { get; private set; }


        /// <summary>
        /// Some types of Question dont have the ability for an Answer (LabelQuestion for example)
        /// </summary>
        public virtual bool IsAnswerable { get { return true; } }

        public Question()
        {
            Validators = new List<IProcessingRule>();
            AllowNotesOnAnswer = true;
            HtmlPresentationHint = new Editor.HtmlPresentationHint();

            ClientModel = new ClientModel();

            ClientModel.Events.Add(new ClientModelEvent() { Name = "AnswerChanged" });
            Enabled = true;

            Labels = new HashSet<Guid>();

            LocalisablePrompt = new LocalisableString();
            LocalisableNote = new LocalisableString();
            LocalisableHelpContent = new LocalisableString();
        }

        public override void Encode(IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("Name", Name);
            encoder.Encode("LocalisablePrompt", LocalisablePrompt);
            encoder.Encode("HidePrompt", HidePrompt);
            encoder.Encode("PossibleScore", PossibleScore);
            encoder.Encode("Validators", Validators);
            encoder.Encode("LocalisableNote", LocalisableNote);
            encoder.Encode("AllowNotesOnAnswer", AllowNotesOnAnswer);
            if (DefaultAnswer != null)
            {
                encoder.Encode("DefaultAnswer", DefaultAnswer);
            }
            encoder.Encode("ClientModel", ClientModel);
            encoder.Encode("Enabled", Enabled);

            HtmlPresentationHint.Encode(encoder);
            encoder.Encode("Labels", Labels);
            encoder.Encode("HelpPosition", HelpPosition);
            encoder.Encode("LocalisableHelpContent", LocalisableHelpContent);
        }

        public override void Decode(IDecoder decoder)
        {
            base.Decode(decoder);

            Name = decoder.Decode<string>("Name", "");
            LocalisablePrompt = LocalisableString.DecodeWithDefault(decoder, "Prompt", "");
            HidePrompt = decoder.Decode<bool>("HidePrompt", false);
            PossibleScore = decoder.Decode<decimal>("PossibleScore");
            Validators = decoder.Decode<List<IProcessingRule>>("Validators");
            LocalisableNote = LocalisableString.DecodeWithDefault(decoder, "Note", "");
            AllowNotesOnAnswer = decoder.Decode<bool>("AllowNotesOnAnswer");
            _defaultAnswer = decoder.Decode<object>("DefaultAnswer", null);
            ClientModel = decoder.Decode<ClientModel>("ClientModel", new ClientModel());
            Enabled = decoder.Decode<bool>("Enabled", true); //default to enabled

            Labels = decoder.Decode<ISet<Guid>>("Labels", new HashSet<Guid>());

            HelpPosition = decoder.Decode<HtmlPresentationHintLabelPlacement>("HelpPosition");
            LocalisableHelpContent = LocalisableString.DecodeWithDefault(decoder, "HelpContent", "");

            HtmlPresentationHint.Decode(decoder);
        }

        public IEnumerable<IQuestion> Questions
        {
            get { yield return this; }
        }

        public override string ToString()
        {
            return Prompt;
        }

        #region IQuestion Members

        public IEnumerable<IPresentable> AsDepthFirstEnumerable()
        {
            yield return this;
        }

        private int CountOfThis(List<IPresented> list)
        {
            return (from l in list where l.Presentable == this select l).Count();
        }

        public IPresented CreatePresented(IWorkingDocument workingDocument, string nameAs, IDisplayIndexProvider parent)
        {
            int sequence = CountOfThis(workingDocument.PresentedAsDepthFirstEnumerable().ToList());

            string n = string.IsNullOrEmpty(nameAs) == false ? nameAs : Name;
            n = workingDocument.Program.ExpandString(n);

            PresentedQuestion presentedQuestion = new PresentedQuestion(this, workingDocument.Program) { WorkingDocument = workingDocument, Sequence = sequence, NameAs = n, Parent = parent };

            return presentedQuestion;
        }

        public abstract IAnswer CreateBlankAnswer(IPresentedQuestion presentedQuestion);

        #endregion

        #region IPresentableSource Members

        public IPresentable GetPresentable(ILingoProgram program, ref string nameAs)
        {
            nameAs = Name;
            return this;
        }

        #endregion

        public IHtmlPresentationHint HtmlPresentation
        {
            get { return HtmlPresentationHint; }
        }

        /// <summary>
        /// Helper method so that Lingo can get at a named validator
        /// </summary>
        /// <param name="name">Name of validator to retrieve</param>
        /// <returns>null if no validator matches name, otherwise the validator named name</returns>
        public virtual object GetNamedValidator(string name)
        {
            if(string.IsNullOrEmpty(name))
            {
                return null;
            }

            var vals = (from v in Validators where v.Name == name select v);

            if(vals.Count() == 0)
            {
                return null;
            }

            return vals.First();
        }

        public virtual void ChangeId(Guid newId)
        {
            // that's us done
            _id = newId;

            ((IIdAssignableUniqueNode)this.LocalisablePrompt).ChangeId(CombFactory.NewComb());
            ((IIdAssignableUniqueNode)this.LocalisableNote).ChangeId(CombFactory.NewComb());
            ((IIdAssignableUniqueNode)this.LocalisableHelpContent).ChangeId(CombFactory.NewComb());
        }

        public virtual void FromNameValuePairs(NameValueCollection pairs)
        {
            if (pairs == null || pairs.Count == 0)
            {
                return;
            }

            // going to keep this simple and just use a switch statement rather than having
            // some funky reflection stuff.
            foreach (var name in pairs.AllKeys)
            {
                string value = pairs[name];
                switch (name.ToLower())
                {
                    case "name":
                        this.Name = value;
                        break;
                    case "prompt":
                        this.Prompt = value;
                        break;
                    case "note":
                        this.Note = value;
                        break;
                    case "allownotesonanswer":
                        bool allow = false;
                        if (bool.TryParse(value, out allow) == true)
                        {
                            this.AllowNotesOnAnswer = allow;
                        }
                        break;
                    case "mandatory":
                        this.Validators.Add(new ConceptCave.Checklist.Validators.MandatoryValidator());
                        break;
                    case "hideprompt":
                        bool hide = false;
                        if (bool.TryParse(value, out hide) == true)
                        {
                            this.HidePrompt = hide;
                        }
                        break;
                    case "visible":
                        bool vis = true;
                        if (bool.TryParse(value, out vis) == true)
                        {
                            this.ClientModel.Visible = vis;
                        }
                        break;
                    case "customstyle":
                        this.HtmlPresentationHint.CustomStyling = value;
                        break;
                    case "customclass":
                        this.HtmlPresentationHint.CustomClass = value;
                        break;
                    case "enabled":
                        bool enble = true;
                        if (bool.TryParse(value, out enble) == true)
                        {
                            this.Enabled = enble;
                        }
                        break;
                    case "defaultanswer":
                        this.DefaultAnswer = value;
                        break;
                    case "categories":
                        if (string.IsNullOrEmpty(value) == true)
                        {
                            break;
                        }

                        value.Split(',').ToList().ForEach(v =>
                        {
                            this.Validators.Add(new ConceptCave.Checklist.ProcessingRules.CategoryRule() { Text = v });
                        });

                        break;
                    case "helpposition":
                        HtmlPresentationHintLabelPlacement placement = HtmlPresentationHintLabelPlacement.Left;
                        if (Enum.TryParse(value, out placement) == true)
                        {
                            this.HelpPosition = placement;
                        }
                        break;
                    case "helpcontent":
                        this.HelpContent = value;
                        break;
                }
            }
        }

        public virtual List<NameValueCollection> ToNameValuePairs()
        {
            NameValueCollection result = new NameValueCollection();

            if (string.IsNullOrEmpty(this.Name) == false)
            {
                result.Add("Name", this.Name);
            }

            if (string.IsNullOrEmpty(this.Prompt) == false)
            {
                result.Add("Prompt", this.Prompt);
            }

            if (string.IsNullOrEmpty(this.Note) == false)
            {
                result.Add("Note", this.Note);
            }

            if (this.AllowNotesOnAnswer == true)
            {
                result.Add("allownotesonanswer", this.AllowNotesOnAnswer.ToString());
            }

            if ((from v in Validators where v is ConceptCave.Checklist.Validators.MandatoryValidator select v).Count() > 0)
            {
                result.Add("mandatory", true.ToString());
            }

            if (this.HidePrompt == true)
            {
                result.Add("hideprompt", this.HidePrompt.ToString());
            }

            if (this.ClientModel.Visible == false)
            {
                result.Add("visible", this.ClientModel.Visible.ToString());
            }

            if (string.IsNullOrEmpty(this.HtmlPresentationHint.CustomStyling) == false)
            {
                result.Add("customstyle", this.HtmlPresentationHint.CustomStyling);
            }

            if (string.IsNullOrEmpty(this.HtmlPresentationHint.CustomClass) == false)
            {
                result.Add("customclass", this.HtmlPresentationHint.CustomClass);
            }

            if (this.Enabled == false)
            {
                result.Add("enabled", this.Enabled.ToString());
            }

            if (this.DefaultAnswer != null)
            {
                result.Add("defaultanswer", this.DefaultAnswer.ToString());
            }

            var categories = (from v in Validators where v is ConceptCave.Checklist.ProcessingRules.CategoryRule select v);
            if (categories.Count() > 0)
            {
                result.Add("categories", string.Join(",", (from c in categories select ((ConceptCave.Checklist.ProcessingRules.CategoryRule)c).Text).ToArray()));
            }

            if(this.HelpPosition != HtmlPresentationHintLabelPlacement.Right)
            {
                result.Add("helpposition", this.HelpPosition.ToString());
            }

            if(string.IsNullOrEmpty(this.HelpContent) == false)
            {
                result.Add("helpcontent", this.HelpContent);
            }

            return new List<NameValueCollection>(new NameValueCollection[] {result});
        }
    }

}
