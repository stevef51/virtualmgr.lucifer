﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.Reporting.LingoQuery;

namespace ConceptCave.Checklist.Editor.Reporting
{
    public class PieChartQuestion : Question
    {
        //private LineChartSeries _series;

        public void SetData(LingoDatatable kvData)
        {
//            _series = new LineChartSeries()
//            {
//                Name = "Pie Chart Series",
////                XYData = kvData
//            };
        }

        //public LineChartSeries Series
        //{
        //    get { return _series; }
        //}

        public override object DefaultAnswer
        {
            get
            {
                return null;
            }
            set
            {

            }
        }
        public override IAnswer CreateBlankAnswer(IPresentedQuestion presentedQuestion)
        {
            return null;
        }
        public override bool IsAnswerable
        {
            get
            {
                return false;
            }
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);
            //_series = decoder.Decode<LineChartSeries>("Series");
        }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);
            //encoder.Encode("Series", _series);
        }
    }
}