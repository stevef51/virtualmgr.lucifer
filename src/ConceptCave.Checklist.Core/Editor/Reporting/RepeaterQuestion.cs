﻿using ConceptCave.Checklist.Editor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.Checklist.Editor.Reporting
{
    public class RepeaterQuestion : Question
    {
        public string DataSource { get; set; }

        public string RepeatContent { get; set; }

        public bool ShowControls { get; set; }

        public bool HideWhenNoData { get; set; }

        /// <summary>
        /// A comma seperated string of data sources, each in the form of datasource.feedname whose parameters should be updated
        /// when a row is selected in the table
        /// </summary>
        public string UpdateDataSources { get; set; }

        /// <summary>
        /// A comma seperated string of data sources, each in the form of datasource.feedname whose parameters should be cleared
        /// when a row is selected in the table
        /// </summary>
        public string ClearDataSources { get; set; }

        public override object DefaultAnswer
        {
            get
            {
                return null;
            }
            set
            {

            }
        }

        public override Checklist.Interfaces.IAnswer CreateBlankAnswer(Checklist.Interfaces.IPresentedQuestion presentedQuestion)
        {
            return null;
        }

        public override bool IsAnswerable
        {
            get
            {
                return false;
            }
        }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode<string>("DataSource", DataSource);
            encoder.Encode<string>("RepeatContent", RepeatContent);
            encoder.Encode<bool>("ShowControls", ShowControls);
            encoder.Encode<string>("UpdateDataSources", UpdateDataSources);
            encoder.Encode<string>("ClearDataSources", ClearDataSources);
            encoder.Encode<bool>("HideWhenNoData", HideWhenNoData);
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            DataSource = decoder.Decode<string>("DataSource");
            RepeatContent = decoder.Decode<string>("RepeatContent");
            ShowControls = decoder.Decode<bool>("ShowControls");
            UpdateDataSources = decoder.Decode<string>("UpdateDataSources");
            ClearDataSources = decoder.Decode<string>("ClearDataSources");
            HideWhenNoData = decoder.Decode<bool>("HideWhenNoData");
        }

        public override List<System.Collections.Specialized.NameValueCollection> ToNameValuePairs()
        {
            var r = base.ToNameValuePairs();

            var result = r[0];

            result.Add("datasource", DataSource);
            result.Add("updatedatasources", UpdateDataSources);
            result.Add("cleardatasources", ClearDataSources);
            result.Add("hidewhennodata", HideWhenNoData.ToString());
            result.Add("repeatcontent", RepeatContent);
            result.Add("showcontrols", ShowControls.ToString());

            return r;
        }

        public override void FromNameValuePairs(System.Collections.Specialized.NameValueCollection pairs)
        {
            base.FromNameValuePairs(pairs);

            foreach (var name in pairs.AllKeys)
            {
                string n = name.ToLower();
                bool val = false;

                switch (n)
                {
                    case "datasource":
                        DataSource = pairs[name];
                        break;
                    case "updatedatasources":
                        UpdateDataSources = pairs[name];
                        break;
                    case "cleardatasources":
                        ClearDataSources = pairs[name];
                        break;
                    case "hidewhennodata":
                        if (bool.TryParse(pairs[name], out val) == true)
                        {
                            this.HideWhenNoData = val;
                        }
                        break;
                    case "repeatcontent":
                        RepeatContent = pairs[name];
                        break;
                    case "showcontrols":
                        if (bool.TryParse(pairs[name], out val) == true)
                        {
                            this.ShowControls = val;
                        }
                        break;
                }
            }
        }
    }
}
