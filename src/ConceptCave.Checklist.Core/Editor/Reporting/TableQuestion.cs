﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.Reporting.LingoQuery;

namespace ConceptCave.Checklist.Editor.Reporting
{
    public class TableQuestion : Question
    {
        public TableQuestion()
        {
            Name = "Table";
            UseDataSourceVisibleFields = true;
        }

        public LingoDatatable DataTable { get; set; }
        public bool Stripes { get; set; }
        public bool Bordered { get; set; }
        public bool Hover { get; set; }
        public bool Condense { get; set; }

        public bool UseDataSourceVisibleFields { get; set; }
        public string VisibleFields { get; set; }
        public bool ShowControls { get; set; }

        public bool HideWhenNoData { get; set; }

        /// <summary>
        /// The path to a data source feed in the form of datasource.feedname
        /// </summary>
        public string DataSource { get; set; }

        /// <summary>
        /// A comma seperated string of data sources, each in the form of datasource.feedname whose parameters should be updated
        /// when a row is selected in the table
        /// </summary>
        public string UpdateDataSources { get; set; }

        /// <summary>
        /// A comma seperated string of data sources, each in the form of datasource.feedname whose parameters should be cleared
        /// when a row is selected in the table
        /// </summary>
        public string ClearDataSources { get; set; }

        public override object DefaultAnswer
        {
            get
            {
                return null;
            }
            set
            {
                
            }
        }

        public override IAnswer CreateBlankAnswer(IPresentedQuestion presentedQuestion)
        {
            return null;
        }

        public override bool IsAnswerable
        {
            get
            {
                return false ;
            }
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);
            DataTable = decoder.Decode<LingoDatatable>("DataTable");
            DataSource = decoder.Decode<string>("DataSource");
            UpdateDataSources = decoder.Decode<string>("UpdateDataSources");
            ClearDataSources = decoder.Decode<string>("ClearDataSources");
            HideWhenNoData = decoder.Decode<bool>("HideWhenNoData");

            UseDataSourceVisibleFields = decoder.Decode<bool>("UseDataSourceVisibleFields");
            VisibleFields = decoder.Decode<string>("VisibleFields");

            ShowControls = decoder.Decode<bool>("ShowControls");

            Stripes = decoder.Decode<bool>("Stripes");
            Bordered = decoder.Decode<bool>("Bordered");
            Hover = decoder.Decode<bool>("Hover");
            Condense = decoder.Decode<bool>("Condense");
        }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);
            encoder.Encode<LingoDatatable>("DataTable", DataTable);
            encoder.Encode<string>("DataSource", DataSource);
            encoder.Encode<string>("UpdateDataSources", UpdateDataSources);
            encoder.Encode<string>("ClearDataSources", ClearDataSources);
            encoder.Encode<bool>("HideWhenNoData", HideWhenNoData);

            encoder.Encode<bool>("UseDataSourceVisibleFields", UseDataSourceVisibleFields);
            encoder.Encode<string>("VisibleFields", VisibleFields);

            encoder.Encode<bool>("ShowControls", ShowControls);

            encoder.Encode<bool>("Stripes", Stripes);
            encoder.Encode<bool>("Bordered", Bordered);
            encoder.Encode<bool>("Hover", Hover);
            encoder.Encode<bool>("Condense", Condense);
        }

        public override List<System.Collections.Specialized.NameValueCollection> ToNameValuePairs()
        {
            var r = base.ToNameValuePairs();

            var result = r[0];

            result.Add("datasource", DataSource);
            result.Add("updatedatasources", UpdateDataSources);
            result.Add("cleardatasources", ClearDataSources);
            result.Add("hidewhennodata", HideWhenNoData.ToString());
            result.Add("usedatasourcevisiblefields", UseDataSourceVisibleFields.ToString());
            result.Add("visiblefields", VisibleFields);
            result.Add("showcontrols", ShowControls.ToString());
            result.Add("stripes", Stripes.ToString());
            result.Add("bordered", Bordered.ToString());
            result.Add("hover", Hover.ToString());
            result.Add("condense", Condense.ToString());

            return r;
        }

        public override void FromNameValuePairs(System.Collections.Specialized.NameValueCollection pairs)
        {
            base.FromNameValuePairs(pairs);

            foreach (var name in pairs.AllKeys)
            {
                string n = name.ToLower();
                bool val = false;

                switch (n)
                {
                    case "datasource":
                        DataSource = pairs[name];
                        break;
                    case "updatedatasources":
                        UpdateDataSources = pairs[name];
                        break;
                    case "cleardatasources":
                        ClearDataSources = pairs[name];
                        break;
                    case "hidewhennodata":
                        if (bool.TryParse(pairs[name], out val) == true)
                        {
                            this.HideWhenNoData = val;
                        }
                        break;
                    case "usedatasourcevisiblefields":
                        if (bool.TryParse(pairs[name], out val) == true)
                        {
                            this.UseDataSourceVisibleFields = val;
                        }
                        break;
                    case "visiblefields":
                        VisibleFields = pairs[name];
                        break;
                    case "showcontrols":
                        if (bool.TryParse(pairs[name], out val) == true)
                        {
                            this.ShowControls = val;
                        }
                        break;
                    case "stripes":
                        if (bool.TryParse(pairs[name], out val) == true)
                        {
                            this.Stripes = val;
                        }
                        break;
                    case "bordered":
                        if (bool.TryParse(pairs[name], out val) == true)
                        {
                            this.Bordered = val;
                        }
                        break;
                    case "hover":
                        if (bool.TryParse(pairs[name], out val) == true)
                        {
                            this.Hover = val;
                        }
                        break;
                    case "condense":
                        if (bool.TryParse(pairs[name], out val) == true)
                        {
                            this.Condense = val;
                        }
                        break;
                }
            }
        }
    }
}