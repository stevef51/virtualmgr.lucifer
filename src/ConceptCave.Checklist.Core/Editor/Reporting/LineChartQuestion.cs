﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;
using System.Drawing;
using ConceptCave.Checklist.Reporting.LingoQuery;
using ConceptCave.Core.Coding;
using System.Collections;
using ConceptCave.Checklist.Lingo.ObjectProviders;
using Newtonsoft.Json.Linq;

namespace ConceptCave.Checklist.Editor.Reporting
{
    public class LineChartQuestion : Question
    {
        private List<ChartSeries> _series;

        public IList<ChartSeries> Series {
            get { return _series.AsReadOnly(); }
        }

        public string XAxisName { get; set; }
        public string YAxisName { get; set; }
        public AxisUnits XAxisUnits { get; set; }

        public ChartSeries XAxes { get; set; }

        public string ChartType { get; set; }

        public ChartSeries AddXAxis(string type)
        {
            ChartSeries series = null;

            if (type.ToLower() == "odata")
            {
                series = new DataSourceSeries();
            }
            else if (type.ToLower() == "array")
            {
                series = new ArraySourceSeries();
            }
            else if (type.ToLower() == "xy")
            {
                series = new XYArraySourceSeries();
            }

            series.Question = this;

            XAxes = series;

            return XAxes;
        }

        public ChartSeries AddSeries(string name, string type)
        {
            ChartSeries series = null;

            var existing = (from s in _series where s.Name == name select s);
            if (existing.Any())
            {
                series = existing.First();
            }
            else
            {
                if(type.ToLower() == "odata")
                {
                    series = new DataSourceSeries();
                }
                else if(type.ToLower() == "array")
                {
                    series = new ArraySourceSeries();
                }
                else if(type.ToLower() == "xy")
                {
                    series = new XYArraySourceSeries();
                }

                series.Name = name;
                series.Question = this;

                _series.Add(series);
            }

            return series;
        }

        public ChartSeries AddSeries(string name, string datasource, string datasourceField, string linecolor)
        {
            DataSourceSeries series = (DataSourceSeries)AddSeries(name, "odata");

            series.Datasource = datasource;
            series.DatasourceField = datasourceField;
            series.LineColour = linecolor;

            return series;
        }

        public LineChartQuestion()
        {
            _series = new List<ChartSeries>();
            XAxisUnits = AxisUnits.DateTime;
            ChartType = "Line";
        }

        public override object DefaultAnswer
        {
            get
            {
                return null;
            }
            set
            {
                
            }
        }
        public override IAnswer CreateBlankAnswer(IPresentedQuestion presentedQuestion)
        {
            return null;
        }
        public override bool IsAnswerable
        {
            get
            {
                return false;
            }
        }

        public override void Decode(IDecoder decoder)
        {
            base.Decode(decoder);
            XAxisName = decoder.Decode<string>("XAxisName");
            YAxisName = decoder.Decode<string>("YAxisName");
            XAxisUnits = (AxisUnits)decoder.Decode<int>("XAxisUnits");
            ChartType = decoder.Decode<string>("ChartType");

            var series = decoder.Decode<List<ChartSeries>>("Series");
            if (series != null)
                _series = series;
        }

        public override void Encode(IEncoder encoder)
        {
            base.Encode(encoder);
            encoder.Encode("XAxisName", XAxisName);
            encoder.Encode("YAxisName", YAxisName);
            encoder.Encode("XAxisUnits", (int)XAxisUnits);
            encoder.Encode("ChartType", ChartType);
            encoder.Encode("Series", _series);
        }

    }

    public abstract class ChartSeries : Node
    {
        public LineChartQuestion Question { get; set; }
        public string Name { get; set; }
        public string LineColour { get; set; }

        public override void Decode(IDecoder decoder)
        {
            base.Decode(decoder);
            Name = decoder.Decode<string>("Name");
            LineColour = decoder.Decode<string>("LineColour");
            Question = decoder.Decode<LineChartQuestion>("Question");
        }

        public override void Encode(IEncoder encoder)
        {
            base.Encode(encoder);
            encoder.Encode("Name", Name);
            encoder.Encode("LineColour", LineColour);
            encoder.Encode("Question", Question);
        }

        public virtual void ToJson(JObject obj)
        {
            obj["name"] = Name;
            obj["linecolour"] = LineColour;
        }
    }

    public class DataSourceSeries : ChartSeries
    {
        public string Datasource { get; set; }
        public string DatasourceField { get; set; }

        /// <summary>
        /// Format for the field, for example {{value | date: 'dd/MM/yyyy'}}
        /// The format uses Angular to interpolate it, so needs to be in the form of an Angular expression, with value representing the
        /// value to be formatted.
        /// </summary>
        public string FieldFormat { get; set; }

        /// <summary>
        /// The field (if present) used to group and collate the data on.
        /// </summary>
        public string GroupByField { get; set; }


        public override void Decode(IDecoder decoder)
        {
            base.Decode(decoder);
            Datasource = decoder.Decode<string>("Datasource");
            DatasourceField = decoder.Decode<string>("DatasourceField");
            FieldFormat = decoder.Decode<string>("FieldFormat");
            GroupByField = decoder.Decode<string>("GroupByField");
        }

        public override void Encode(IEncoder encoder)
        {
            base.Encode(encoder);
            encoder.Encode("Datasource", Datasource);
            encoder.Encode("DatasourceField", DatasourceField);
            encoder.Encode("FieldFormat", FieldFormat);
            encoder.Encode("GroupByField", GroupByField);
        }

        public override void ToJson(JObject obj)
        {
            base.ToJson(obj);
            obj["datasource"] = Datasource;
            obj["datasourcefield"] = DatasourceField;
            obj["fieldformat"] = FieldFormat;
            obj["groupbyfield"] = GroupByField;
            obj["type"] = "datasource";
        }
    }

    public class ArraySourceSeries : ChartSeries
    {
        public IList<object> Data { get; set; }

        public ArraySourceSeries()
        {
            Data = new List<object>();
        }

        public void AddData(object data)
        {
            if (data is string)
            {
                foreach (var s in ((string)data).Split(','))
                {
                    if(Question.XAxisUnits == AxisUnits.Number)
                    {
                        decimal d = 0;
                        if (decimal.TryParse(s, out d))
                        {
                            Data.Add(d);
                        }
                    }
                    else if(Question.XAxisUnits == AxisUnits.DateTime)
                    {
                        DateTime dt = DateTime.MinValue;
                        if(DateTime.TryParse(s, out dt))
                        {
                            Data.Add(dt);
                        }
                    }
                    else
                    {
                        Data.Add(s);
                    }
                }
            }
            else if (data is int)
            {
                Data.Add((int)data);
            }
            else if (data is decimal)
            {
                Data.Add((decimal)data);
            }
        }

        public override void Decode(IDecoder decoder)
        {
            base.Decode(decoder);

            Data = decoder.Decode<IList<object>>("Data");

            if(Data == null)
            {
                Data = new List<object>();
            }
        }

        public override void Encode(IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("Data", Data);
        }
        public override void ToJson(JObject obj)
        {
            base.ToJson(obj);
            JArray data = new JArray();
            foreach(var d in Data)
            {
                data.Add(d);
            }

            obj["data"] = data;
            obj["type"] = "array";
        }
    }

    public class XYArraySourceSeries : ChartSeries
    {
        public List<Dictionary<object, object>> Data { get; set; }

        public void AddData(object data)
        {
            if(data is string)
            {

            }
            else if(data is DictionaryObjectProvider)
            {
                // we are expecting properties either, x,y or t
                var p = (DictionaryObjectProvider)data;
                string x = null;
                string y = null;
                foreach(var d in p.Dictionary.Keys)
                {
                    switch(d)
                    {
                        case "x":
                        case "X":
                            {
                                x = p.Dictionary[d].ToString();
                                break;
                            }
                        case "t":
                        case "T":
                            {
                                x = ((DateTime)p.Dictionary[d]).ToString("yyy");
                                break;
                            }
                        case "y":
                        case "Y":
                            {
                                y = p.Dictionary[d].ToString();
                                break;
                            }
                    }
                }
            }
            else if(data is ListObjectProvider)
            {
                foreach(var d in ((ListObjectProvider)data))
                {
                    AddData(d);
                }
            }
        }
    }

    public enum AxisUnits
    {
        Number = 0,
        DateTime = 1,
        String = 2
    }
}
