﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.Editor;
using ConceptCave.Checklist.RunTime;

namespace ConceptCave.Checklist.Editor
{
    public class DateTimeQuestion : Question, IDateTimeQuestion
    {
        public bool AllowTimeEntry { get; set; }

        public string Format { get; set; }

        public string UpdateDataSources { get; set; }

        public DateTimeQuestion()
        {
            AllowTimeEntry = false;

            Format = "dd/mm/yyyy";
        }

        public override object DefaultAnswer
        {
            get
            {
                return _defaultAnswer;
            }
            set
            {
                if (value is DateTime)
                {
                    _defaultAnswer = value;
                }
                else if (value is IDateTimeAnswer)
                {
                    _defaultAnswer = ((IDateTimeAnswer)value).AnswerValue;
                }
                else if (value is string)
                {
                    DateTime d = DateTime.MinValue;

                    if (DateTime.TryParse((string)value, out d))
                    {
                        _defaultAnswer = d;
                    }
                }
            }
        }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("AllowTimeEntry", AllowTimeEntry);
            encoder.Encode("Format", Format);
            encoder.Encode("UpdateDataSources", UpdateDataSources);
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            AllowTimeEntry = decoder.Decode("AllowTimeEntry", false);
            Format = decoder.Decode<string>("Format", "dd/mm/yyyy");
            UpdateDataSources = decoder.Decode<string>("UpdateDataSources");
        }

        public override IAnswer CreateBlankAnswer(IPresentedQuestion presentedQuestion)
        {
            var result = new DateTimeAnswer() { PresentedQuestion = presentedQuestion };

            if (DefaultAnswer != null)
            {
                result.AnswerValue = DefaultAnswer;
            }
            else
            {
                result.AnswerValue = null;
            }

            return result;
        }

        public override List<System.Collections.Specialized.NameValueCollection> ToNameValuePairs()
        {
            var r = base.ToNameValuePairs();

            var result = r[0];

            result.Add("updatedatasources", UpdateDataSources);

            return r;
        }

        public override void FromNameValuePairs(System.Collections.Specialized.NameValueCollection pairs)
        {
            base.FromNameValuePairs(pairs);

            foreach (var name in pairs.AllKeys)
            {
                string n = name.ToLower();

                switch (n)
                {
                    case "updatedatasources":
                        UpdateDataSources = pairs[name];
                        break;
                }
            }
        }
    }
}
