﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Core;
using ConceptCave.Checklist.Interfaces;

namespace ConceptCave.Checklist.Editor
{
    public class LatLng : Node, ILatLng
    {
        public decimal Latitude { get; protected set; }
        public decimal Longitude { get; protected set; }
        public decimal Accuracy { get; protected set; }
        public string Error { get; set; }

        public LatLng() { }

        public LatLng(decimal lattitude, decimal longitude, decimal accuracy)
        {
            Latitude = lattitude;
            Longitude = longitude;
            Accuracy = accuracy;
        }

        public LatLng(string error)
        {
            Error = error;
        }

        public bool IsValid
        {
            get
            {
                return string.IsNullOrEmpty(Error);
            }
        }

        public override string ToString()
        {
            if (string.IsNullOrEmpty(Error))
            {
                return string.Format("{0}, {1} \u00B1 {2}", Latitude, Longitude, Accuracy);
            }
            else
            {
                return Error;
            }
        }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);
            encoder.Encode("Lat", Latitude);
            encoder.Encode("Long", Longitude);
            encoder.Encode("Acc", Accuracy);
            encoder.Encode("Error", Error);
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);
            Latitude = decoder.Decode<decimal>("Lat");
            Longitude = decoder.Decode<decimal>("Long");
            Accuracy = decoder.Decode<decimal>("Acc");
            Error = decoder.Decode<string>("Error");
        }

        public decimal MetresDistanceFrom(decimal lat, decimal lng)
        {
            var l = new LatLng(lat, lng, 0);

            return MetresDistanceFrom(l);
        }

        public decimal MetresDistanceFrom(ILatLng latLng)
        {
            var R = 6371;
            var dLat = (Latitude - latLng.Latitude).ToRadians();
            var dLon = (Longitude - latLng.Longitude).ToRadians();
            var a = Math.Sin((double)dLat / 2) * Math.Sin((double)dLat / 2) +
                    Math.Cos((double)Latitude.ToRadians()) * Math.Cos((double)latLng.Latitude.ToRadians()) *
                    Math.Sin((double)dLon / 2) * Math.Sin((double)dLon / 2);
            var c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
            var d = R * c; // Distance in km

            return (decimal)d * 1000;
        }
    }

}
