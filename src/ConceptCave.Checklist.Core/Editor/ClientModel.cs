﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Core;
using ConceptCave.Checklist.Interfaces;

namespace ConceptCave.Checklist.Editor
{
    public class ClientModel : Node, IClientModel
    {
        protected List<IClientModelEvent> events;

        public bool Visible { get; set; }
        public List<IClientModelEvent> Events
        {
            get { return events; }
        }

        public ClientModel() : base()
        {
            events = new List<IClientModelEvent>();
            Visible = true;
        }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("Events", events);
            encoder.Encode("Visible", Visible);
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            events = decoder.Decode<List<IClientModelEvent>>("Events", new List<IClientModelEvent>());
            Visible = decoder.Decode<bool>("Visible");
        }
    }

    public class ClientModelEvent : Node, IClientModelEvent
    {
        public string Name {get;set;}
        public IClientModelEventAction Action { get; set; }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("Name", Name);
            encoder.Encode("Action", Action);
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            Name = decoder.Decode<string>("Name");
            Action = decoder.Decode<IClientModelEventAction>("Action", null);
        }
    }

    public abstract class ClientModelEventAction : Node, IClientModelEventAction
    {
        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);
        }
    }

    public abstract class TargettedClientModelEventAction : ClientModelEventAction, ITargettedClientModelEventAction
    {
        public string TargetName { get; set; }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("TargetName", TargetName);
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            TargetName = decoder.Decode<string>("TargetName");
        }
    }

    public class ChangeVisibilityClientModelEventAction : TargettedClientModelEventAction, IClientModelVisibilityAction
    {
        public ClienModelAnimationType EnterAnimation { get; set; }
        public ClienModelAnimationType LeaveAnimation { get; set; }
        public bool MakeVisible { get; set; }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("EnterAnimation", EnterAnimation);
            encoder.Encode("LeaveAnimation", LeaveAnimation);
            encoder.Encode("MakeVisible", MakeVisible);
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            EnterAnimation = decoder.Decode<ClienModelAnimationType>("EnterAnimation");
            LeaveAnimation = decoder.Decode<ClienModelAnimationType>("LeaveAnimation");
            MakeVisible = decoder.Decode<bool>("MakeVisible", true);
        }
    }
}
