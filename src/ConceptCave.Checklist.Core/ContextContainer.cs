﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace ConceptCave.Core
{
    public class ContextContainer : IContextContainer
    {
        public IContextContainer Parent { get; private set; }
        private Dictionary<Type, Dictionary<string, object>> _contexts = new Dictionary<Type, Dictionary<string, object>>();

        public ContextContainer()
        {
        }

        public ContextContainer(IContextContainer parent)
        {
            Parent = parent;
        }

        #region IContextContainer Members

        public bool Has<T>()
        {
            return Has<T>("");
        }

        public bool Has<T>(string name)
        {
            Dictionary<string, object> typeDict = null;
            if (!_contexts.TryGetValue(typeof(T), out typeDict))
            {
                if (Parent != null)
                    return Parent.Has<T>(name);

                return false;
            }
            return typeDict.ContainsKey(name);
        }

        public T Get<T>()
        {
            return Get<T>("", () => default(T));
        }

        public T Get<T>(Func<T> defaultValueCallback)
        {
            return Get<T>("", defaultValueCallback);
        }

        public T Get<T>(string name)
        {
            return Get<T>(name, () => default(T));
        }

        public T Get<T>(string name, Func<T> defaultValueCallback)
        {
            object result;
            Dictionary<string, object> typeDict = null;
            if (_contexts.TryGetValue(typeof(T), out typeDict))
                if (typeDict.TryGetValue(name, out result))
                    return (T)result;

            if (Parent != null)
                return Parent.Get<T>(name, defaultValueCallback);

            if (defaultValueCallback != null)
                return defaultValueCallback();

            return default(T);
        }

        public void Set<T>(string name, T context)
        {
            Set(typeof(T), name, context);
        }

        public void Set<T>(T context)
        {
            Set<T>("", context);
        }

        public void Set(Type T, string name, object context)
        {
            Dictionary<string, object> typeDict = null;
            if (!_contexts.TryGetValue(T, out typeDict))
            {
                typeDict = new Dictionary<string, object>();
                _contexts[T] = typeDict;
            }

            typeDict[name] = context;
        }

        public IList<Tuple<Type, string>> Keys()
        {
            var retlist = 
                from outerkvp in _contexts
                    let type = outerkvp.Key
                from innerkvp in outerkvp.Value
                    let name = innerkvp.Key
                select new Tuple<Type, string>(type, name);
            return retlist.ToList();
        }

        public void CopyTo(IContextContainer target)
        {
            foreach (var type in _contexts.Keys)
            {
                foreach (var name in _contexts[type].Keys)
                {
                    target.Set(type, name, _contexts[type][name]);
                }
            }
        }

        #endregion
    }
}
