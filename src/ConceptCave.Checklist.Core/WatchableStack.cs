﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Core.Coding;

namespace ConceptCave.Core
{
    public class WatchableStack<T> : IWatchableStack<T>, ICodable
    {
        private Stack<T> _stack = new Stack<T>();

        #region IWatchableStack<T> Members

        public event Action<IWatchableStack<T>,T> OnBeforePush;

        public event Action<IWatchableStack<T>> OnAfterPush;

        public event Action<IWatchableStack<T>> OnBeforePop;

        public event Action<IWatchableStack<T>, T> OnAfterPop;

        public void Push(T item)
        {
            if (OnBeforePush != null)
                OnBeforePush(this, item);
            _stack.Push(item);
            if (OnAfterPush != null)
                OnAfterPush(this);
        }

        public T Pop()
        {
            if (OnBeforePop != null)
                OnBeforePop(this);
            T item = _stack.Pop();
            if (OnAfterPop != null)
                OnAfterPop(this, item);
            return item;
        }

        public T Peek()
        {
            return _stack.Peek();
        }

        public T Peek(int indexFromTop)
        {
            return _stack.ElementAtOrDefault(indexFromTop);
        }

        public int Count
        {
            get { return _stack.Count; }
        }

        #endregion

        #region IEnumerable<T> Members

        public IEnumerator<T> GetEnumerator()
        {
            return _stack.GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _stack.GetEnumerator();
        }

        #endregion

        #region ICodable Members

        public void Encode(IEncoder encoder)
        {
            encoder.Encode("Stack", _stack);
        }

        public void Decode(IDecoder decoder)
        {
            _stack = decoder.Decode<Stack<T>>("Stack", null);
        }

        #endregion
    }
}
