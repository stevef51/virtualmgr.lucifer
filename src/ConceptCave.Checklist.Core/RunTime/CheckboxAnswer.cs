﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Core;
using ConceptCave.Checklist.Interfaces;

namespace ConceptCave.Checklist.RunTime
{
    public class CheckboxAnswer : Answer, ICheckboxAnswer
    {
        private bool? _answerBool;
        public bool? AnswerBool
        {
            get
            {
                CheckboxQuestionNoAnswerBehaviour behaviour = ((ICheckboxQuestion)this.PresentedQuestion.Question).NoAnswerBehaviour;

                if (behaviour == CheckboxQuestionNoAnswerBehaviour.AnswerIsNull && _answerBool.HasValue && _answerBool.Value == false)
                {
                    return null;
                }

                return _answerBool; 
            }
            set
            {
                UtcDateAnswered = DateTime.UtcNow;
                _answerBool = value;
                this.PassFail = value;
                this.Score = value.HasValue ? value.Value ? 1 : 0 : 0;
            }
        }
        
        public override object AnswerValue
        {
            get
            {
                return AnswerBool;
            }
            set
            {
                if (value != null)
                    AnswerBool = Convert.ToBoolean(value);
                else
                    AnswerBool = null;
            }
        }

        public override string AnswerAsString
        {
            get
            {
                return AnswerBool.HasValue ? AnswerBool.ToString() : "";
            }
        }
    }
}
