﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;

namespace ConceptCave.Checklist.RunTime
{
    public class SignatureAnswer : Answer, ISignatureAnswer
    {
        private string _answerAsJsonString;
        private Guid _photoMediaId;

        private IMediaItem _photoMedia;

        private IMediaManager _mediaManager;

        public SignatureAnswer(IMediaManager mediaManager)
        {
            _mediaManager = mediaManager;
        }

        public string AnswerAsJsonString
        {
            get { return _answerAsJsonString; }
            set
            {
                UtcDateAnswered = DateTime.UtcNow;
                _answerAsJsonString = value;
            }
        }

        public override object AnswerValue
        {
            get
            {
                return AnswerAsJsonString;
            }
            set
            {
                if (value == null)
                {
                    AnswerAsJsonString = null;
                }
                else
                {
                    AnswerAsJsonString = value.ToString();
                }
            }
        }

        public override string AnswerAsString
        {
            get { return _answerAsJsonString; }
        }

        public IMediaItem Picture 
        {
            get
            {
                if (_photoMedia == null)
                {
                    _photoMedia = _mediaManager.GetById(_photoMediaId);
                }

                return _photoMedia;
            }
        }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("PhotoMediaId", _photoMediaId);
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            _photoMediaId = decoder.Decode<Guid>("PhotoMediaId");
        }
    }
}
