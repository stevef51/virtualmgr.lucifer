﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;

namespace ConceptCave.Checklist.RunTime
{
    public abstract class Answer : Node, IAnswer, IConvertableToPrimitive
    {
        #region IAnswer Members

        public abstract object AnswerValue { get; set; }
        public abstract string AnswerAsString { get; }
        public virtual bool? PassFail { get; set; }
        public virtual decimal Score { get; set; }
        public virtual IAnswerNotes Notes { get; set; }

        private IPresentedQuestion _presentedQuestion;
        public virtual IPresentedQuestion PresentedQuestion 
        {
            get { return _presentedQuestion; }
            set
            {
                _presentedQuestion = value;

                if(_presentedQuestion == null)
                {
                    return;
                }

                if (_presentedQuestion.Hidden)
                    UtcDateAnswered = DateTime.UtcNow;
            }
        }

        public virtual void CopyFrom(IAnswer answer)
        {
            AnswerValue = answer.AnswerValue;
        }

        public virtual DateTime UtcDateAnswered { get; protected set; }

        #endregion

        // TODO: this needs to be done in another way, but am doing it here for speed at the moment.
        // Lingo can't reference null at the moment, I need a way of determining if PassFail is null or not
        // so this is a helper method for Lingo.
        public bool PassFailHasValue
        {
            get
            {
                return PassFail.HasValue;
            }
        }

        public void SetPassFail(bool passFail)
        {
            PassFail = passFail;
        }

        public void ClearPassFail()
        {
            PassFail = null;
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            AnswerValue = decoder.Decode<object>("Answer");
            PassFail = decoder.Decode<bool?>("PassFail");
            Score = decoder.Decode<decimal>("Score");
            PresentedQuestion = decoder.Decode<IPresentedQuestion>("PresentedQuestion");
            UtcDateAnswered = decoder.Decode<DateTime>("UtcDateAnswered");
            Notes = decoder.Decode<IAnswerNotes>("Notes");
        }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("Answer", AnswerValue);
            encoder.Encode("PassFail", PassFail);
            encoder.Encode("Score", Score);
            encoder.Encode("PresentedQuestion", PresentedQuestion);
            encoder.Encode("UtcDateAnswered", UtcDateAnswered);
            encoder.Encode("Notes", Notes);
        }

        public override string ToString()
        {
            return AnswerValue != null ? AnswerValue.ToString() : "";
        }

        public object ConvertToPrimitive()
        {
            return AnswerValue;
        }
    }

    public static class ListOfAnswerExtensions
    {
        public static IAnswer FindAnswer(this List<IAnswer> list, IQuestion question, int sequence)
        {
            return list.FirstOrDefault(a => a.PresentedQuestion.Question == question && a.PresentedQuestion.Sequence == sequence);
        }

        public static IAnswer FindAnswer(this List<IAnswer> list, IPresentedQuestion presentedQuestion)
        {
            return list.FirstOrDefault(a => a.PresentedQuestion == presentedQuestion);
        }
    }
}
