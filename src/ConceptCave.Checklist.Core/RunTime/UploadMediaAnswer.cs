﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;
using ConceptCave.Checklist.Facilities;
using System.IO;

namespace ConceptCave.Checklist.RunTime
{
    public class UploadMediaAnswer : Answer, IUploadMediaAnswer, IEnumerable<IMediaItem>
    {
        public ConceptCave.Core.IListOf<IUploadMediaAnswerItem> Items { get; protected set; }

        private IMediaManager _mediaManager;

        public UploadMediaAnswer(IMediaManager mediaMgr)
        {
            _mediaManager = mediaMgr;
            Items = new ListOf<IUploadMediaAnswerItem>();
        }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("Items", Items);
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);
            decoder.Decode<ListOf<IUploadMediaAnswerItem>>("Items");
        }

        public override object AnswerValue
        {
            get
            {
                return Items;
            }
            set
            {
                Items = value as ListOf<IUploadMediaAnswerItem>;
            }
        }

        public override void CopyFrom(IAnswer answer)
        {
            if(!(answer is UploadMediaAnswer))
            {
                return;
            }

            Items = new ListOf<IUploadMediaAnswerItem>();
            foreach (var item in ((UploadMediaAnswer)answer).Items)
            {
                var i = new UploadMediaAnswerItem(_mediaManager)
                {
                    MediaId = item.MediaId,
                    Filename = item.Filename,
                    Description = item.Description,
                    ContentType = item.ContentType,
                    Name = item.Name,
                    UploadDate = item.UploadDate
                };

                Items.Add(i);
            }
        }

        public override string AnswerAsString
        {
            get { 
                return Items.Items.Count == 0 ? string.Empty : Items.Items.Count.ToString(); 
            }
        }

        public override DateTime UtcDateAnswered
        {
            get
            {
                if(Items.Items.Count == 0)
                {
                    return DateTime.UtcNow;
                }

                var result = (from u in Items.Items select u.UploadDate).Min();

                return result;
            }
            protected set
            {
                // don't do anything
            }
        }

        public IEnumerator<IMediaItem> GetEnumerator()
        {
            return Items.Items.Cast<IMediaItem>().GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return Items.Items.GetEnumerator();
        }
    }

    public class UploadMediaAnswerItem : Node, IUploadMediaAnswerItem, IMediaItem
    {
        private Guid _mediaId;
        private readonly IMediaManager _mediaMgr;

        public string Filename { get; set; }

        public string ContentType { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime UploadDate { get; set; }

        public UploadMediaAnswerItem(IMediaManager mediaMgr)
        {
            _mediaMgr = mediaMgr;
        }

        public Guid MediaId
        {
            get
            {
                return _mediaId;
            }
            set
            {
                _mediaId = value;
                UploadDate = DateTime.UtcNow;
            }
        }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("Filename", Filename);
            encoder.Encode("ContentType", ContentType);
            encoder.Encode("Name", Name);
            encoder.Encode("Description", Description);
            encoder.Encode("MediaId", MediaId);
            encoder.Encode("UploadDate", UploadDate);
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            Filename = decoder.Decode<string>("Filename");
            ContentType = decoder.Decode<string>("ContentType");
            Name = decoder.Decode<string>("Name");
            Description = decoder.Decode<string>("Description");
            MediaId = decoder.Decode<Guid>("MediaId");
            UploadDate = decoder.Decode<DateTime>("UploadDate");
        }

        public string MediaType
        {
            get
            {
                return this.ContentType;
            }
            set
            {
                this.ContentType = value;
            }
        }

        public byte[] GetData(IContextContainer context)
        {
            var stream = _mediaMgr.GetStream(MediaId);

            var memoryStream = new MemoryStream();
            stream.CopyTo(memoryStream);

            return memoryStream.ToArray();
        }
    }
}
