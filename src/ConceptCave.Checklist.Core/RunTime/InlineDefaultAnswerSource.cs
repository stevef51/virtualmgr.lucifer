﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;

namespace ConceptCave.Checklist.RunTime
{
    /// <summary>
    /// A default answer source implementation that stores its data in the XML with the working document
    /// </summary>
    public class InlineDefaultAnswerSource : Node, IDefaultAnswerSource
    {
        protected Dictionary<string, Dictionary<int, object>> source;

        public InlineDefaultAnswerSource()
            : base()
        {
            source = new Dictionary<string, Dictionary<int, object>>();
        }

        protected object Get(string name, int sequence)
        {
            if (string.IsNullOrEmpty(name) == true)
            {
                return null;
            }

            Dictionary<int, object> list = null;

            if (source.TryGetValue(name, out list) == false)
            {
                return null;
            }

            object result = null;

            if (list.TryGetValue(sequence, out result) == false)
            {
                return null;
            }

            return result;
        }

        public object Get(IQuestion question, IWorkingDocument workingDocument, Guid containerId, Dictionary<string, int> presentationCache)
        {
            if (question == null)
            {
                return null;
            }

            // this gets us a sequence of previously presented questions
            int sequence = GetQuestionSequence(question, workingDocument);

            // we try by name first in case some sort of templating type thing has been used
            // where questions are copied from other sections. In this case the internal id is
            // different for each instance of the question, but the name will be the same
            string key = question.Name;

            if (string.IsNullOrEmpty(key))
            {
                key = question.Id.ToString();
            }

            key += containerId.ToString();

            // now if the current step has the same question (or a copy of) being presented multiple times on it, we
            // need to manage this as they don't feature in the presented collection and there isn't a presenting one
            // to reference
            if (presentationCache != null)
            {
                int v = 0;
                if (presentationCache.TryGetValue(key, out v) == true)
                {
                    sequence += v;
                }

                presentationCache[key] = v + 1;
            }

            object result = Get(key, sequence);

            return result;
        }

        protected int GetQuestionSequence(IQuestion question, IWorkingDocument workingDocument)
        {
            if (string.IsNullOrEmpty(question.Name) == false)
            {
                // if a name is available use that to work out the sequence. We do this in a couple of steps. First fin
                var byName = (from p in workingDocument.PresentedAsDepthFirstEnumerable() where p is IPresentedQuestion && ((IPresentedQuestion)p).Question.Name == question.Name select p);

                int count = 0;
                foreach (var p in byName)
                {
                    if (((IPresentedQuestion)p).Question != question)
                    {
                        count++;
                    }
                    else
                    {
                        break;
                    }
                }

                return count;
            }

            // otherwise use the instance of the question
            return (from p in workingDocument.PresentedAsDepthFirstEnumerable() where p is IPresentedQuestion && ((IPresentedQuestion)p).Question == question select p).Count();
        }

        public bool Add(IPresentedQuestion presented, object answer, Guid containerId, IWorkingDocument workingDocument)
        {
            if (presented == null)
            {
                return false;
            }

            if(presented.Question is IDoNotStoreInAnswerSource)
            {
                return false;
            }

            string key = presented.Question.Name;

            if (string.IsNullOrEmpty(key) == true)
            {
                key = presented.Question.Id.ToString();
            }

            key += containerId.ToString();

            Dictionary<int, object> list = null;

            if (source.TryGetValue(key, out list) == false)
            {
                list = new Dictionary<int, object>();
                source.Add(key, list);
            }

            int sequence = list.Keys.Count;

            list[sequence] = answer;

            return true;
        }

        public void PopulateFrom(IWorkingDocument wd)
        {
            wd.Presented.ForEach(s => {
                (from q in s.AsDepthFirstEnumerable() where q is IPresentedQuestion select q).Cast<IPresentedQuestion>().ForEach(q =>
                {
                    Add(q, q.GetAnswer(), ((IUniqueNode)s.Presentable).Id, wd);
                });
            });
        }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("Source", source);
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            source = decoder.Decode<Dictionary<string, Dictionary<int, object>>>("Source");
        }
    }
}
