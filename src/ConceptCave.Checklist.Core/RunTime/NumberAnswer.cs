﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Core;
using ConceptCave.Checklist.Interfaces;

namespace ConceptCave.Checklist.RunTime
{
    public class NumberAnswer : Answer, INumberAnswer
    {
        private decimal? _answerNumber;
        public decimal? AnswerNumber
        {
            get { return _answerNumber; }
            set
            {
                UtcDateAnswered = DateTime.UtcNow;
                _answerNumber = value;
                if (PresentedQuestion == null)
                {
                    return;
                }
                INumberQuestion q = PresentedQuestion.Question as INumberQuestion;
                if (_answerNumber.HasValue)
                {
                    if (q.MaxValue.HasValue && _answerNumber.Value > q.MaxValue.Value)
                    {
                        PassFail = false;
                    }
                    else if (q.MinValue.HasValue && _answerNumber.Value < q.MinValue.Value)
                    {
                        PassFail = false;
                    }
                    else
                    {
                        PassFail = true;
                    }
                }
                else
                {
                    this.PassFail = null;
                }
            }
        }
        
        public override object AnswerValue
        {
            get
            {
                return AnswerNumber;
            }
            set
            {
                if (value != null)
                    AnswerNumber = Convert.ToDecimal(value);
                else
                    AnswerNumber = null;
            }
        }

        public override string AnswerAsString
        {
            get
            {
                return AnswerNumber.HasValue ? AnswerNumber.ToString() : "";
            }
        }
    }
}
