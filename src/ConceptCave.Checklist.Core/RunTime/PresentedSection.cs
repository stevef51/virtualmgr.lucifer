﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;
using Newtonsoft.Json.Linq;

namespace ConceptCave.Checklist.RunTime
{
    public class PresentedSection : Node, IPresentedSection
    {
        public IDisplayIndexProvider Parent { get; set; }
        private List<IPresented> _children = new List<IPresented>();
        public DateTime UtcDatePresented { get; protected set; }

        private bool _hidden;
        public bool Hidden 
        {
            get { return _hidden; }
            set 
            { 
                _hidden = value;
                foreach (IPresented p in this.AsDepthFirstEnumerable())
                {
                    if (p == this)
                    {
                        continue;
                    }

                    p.Hidden = value;
                }

            }
        }

        public PresentedSection()
        {
            UtcDatePresented = DateTime.UtcNow;
        }

        #region IPresentedSection Members

        public List<IPresented> Children
        {
            get { return _children; }
        }

        public IEnumerable<IPresented> AsDepthFirstEnumerable()
        {
            yield return this;

            foreach (IPresented child in Children)
            {
                foreach (IPresented presented in child.AsDepthFirstEnumerable())
                    yield return presented;
            }
        }

        public ISection Section { get; set; }

        #endregion

        #region IPresented Members

        public IPresentable Presentable { get { return Section; } }

        #endregion

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            UtcDatePresented = decoder.Decode<DateTime>("UtcDatePresented");
            Section = decoder.Decode<ISection>("Section");
            _children = decoder.Decode<List<IPresented>>("Children");
            Parent = decoder.Decode<IDisplayIndexProvider>("Parent");
            Hidden = decoder.Decode<bool>("Hidden");
        }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("UtcDatePresented", UtcDatePresented);
            encoder.Encode("Section", Section);
            encoder.Encode("Children", _children);
            encoder.Encode("Parent", Parent);
            encoder.Encode("Hidden", Hidden);
        }

        #region IDisplayIndex Members

        public int IndexOfChild(IDisplayIndexProvider child)
        {
            return _children.IndexOf(child as IPresented);
        }

        public IEnumerable<int> DisplayIndexes
        {
            get 
            {
                if (Parent != null)
                {
                    foreach (int i in Parent.DisplayIndex.Indexes)
                        yield return i;

                    yield return Parent.IndexOfChild(this);
                }
            }
        }

        public IDisplayIndex DisplayIndex
        {
            get
            {
                if (_displayIndex == null)
                    _displayIndex = new DisplayIndex(DisplayIndexes, false);
                return _displayIndex;
            }
        } DisplayIndex _displayIndex;
        #endregion

        public void AssignAnswersFrom(IPresentedSection section)
        {
            foreach (var source in (from q in section.AsDepthFirstEnumerable() where q is IPresentedQuestion select q).Cast<IPresentedQuestion>())
            {
                var q = (from c in AsDepthFirstEnumerable() where c is IPresentedQuestion && ((IPresentedQuestion)c).Question.Id == source.Question.Id select c).Cast<IPresentedQuestion>();

                if (q.Count() == 0)
                {
                    // ok, by id didn't return anything, lets try by name
                    q = (from c in AsDepthFirstEnumerable() where c is IPresentedQuestion && ((IPresentedQuestion)c).NameAs == source.NameAs select c).Cast<IPresentedQuestion>();

                    if (q.Count() == 0)
                    {
                        // who the hell knows what to do, lets just assume the user expects this item not to be mapped across and move on our merry way
                        continue;
                    }
                }

                if(q.First().Question.IsAnswerable == false)
                {
                    continue;
                }

                q.First().GetAnswer().CopyFrom(source.GetAnswer());
            }
        }

        public JObject FlattenedJsonAnswers()
        {
            JObject result = new JObject();
            foreach(var c in (from p in AsDepthFirstEnumerable() where p is IPresentedQuestion select p).Cast<IPresentedQuestion>()) 
            {
                if (c.Question.IsAnswerable == false || string.IsNullOrEmpty(c.NameAs))
                {
                    continue;
                }
                result[c.NameAs] = c.GetAnswer().AnswerAsString;
            }
            return result;
        }
    }
}
