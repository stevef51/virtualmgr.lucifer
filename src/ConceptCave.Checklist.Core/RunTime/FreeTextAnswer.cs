﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Core;
using ConceptCave.Checklist.Interfaces;

namespace ConceptCave.Checklist.RunTime
{
    public class FreeTextAnswer : Answer, IFreeTextAnswer
    {
        private string _answerText;
        public string AnswerText 
        {
            get { return _answerText; }
            set
            {
                UtcDateAnswered = DateTime.UtcNow;
                _answerText = value;
            }
        }

        public override object AnswerValue
        {
            get 
            {
                return AnswerText; 
            }
            set 
            {
                if (value == null)
                    AnswerText = null;
                else
                    AnswerText = value.ToString(); 
            }
        }

        public void SetValue(string value)
        {
            AnswerText = value;
        }

        public override string AnswerAsString
        {
            get { return AnswerText; }
        }
    }
}
