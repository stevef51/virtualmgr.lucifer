﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;

namespace ConceptCave.Checklist.RunTime
{
    public class DateTimeAnswer : Answer, IDateTimeAnswer
    {
        private DateTime? _answerDateTime;

        public DateTime? AnswerDateTime 
        {
            get { return _answerDateTime; }
            set
            {
                UtcDateAnswered = DateTime.UtcNow;
                _answerDateTime = value;
            }
        }

        public override object AnswerValue
        {
            get
            {
                return AnswerDateTime;
            }
            set
            {
                if (value == null)
                {
                    AnswerDateTime = null;
                }
                else
                {
                    if (value is DateTime)
                        AnswerDateTime = (DateTime)value;

                    else
                        AnswerDateTime = DateTime.Parse(value.ToString());
                }
            }
        }

        public override string AnswerAsString
        {
            get 
            {
                if (AnswerDateTime.HasValue)
                {
                    return AnswerDateTime.Value.ToString();
                }

                return string.Empty;
            }
        }
    }
}
