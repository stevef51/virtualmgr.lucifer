﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;

namespace ConceptCave.Checklist.RunTime
{
    public class DisplayIndex : IDisplayIndex
    {
        public static DisplayIndexFormatter DefaultFormatter = DisplayIndexFormatter.Formatter1_1a;

        public bool IsLeaf { get; private set; }
        public IEnumerable<int> Indexes { get; private set; }           // list of ints such as [0, 2, 6, 1], note 0 base

        public DisplayIndex(IEnumerable<int> displayIndexes, bool isLeaf)
        {
            Indexes = displayIndexes;
            IsLeaf = isLeaf;
        }

        public override string ToString()
        {
            return DefaultFormatter.Format(this);
        }

        public string Format(IDisplayIndexFormatter formatter)
        {
            if (formatter == null)
                formatter = DefaultFormatter;
            
            return formatter.Format(this);
        }
    }

    public class DisplayIndexFormatter : IDisplayIndexFormatter
    {
        public Func<int, string> BranchText { get; set; }
        public Func<int, string> LeafText { get ; set; }
        public string BranchSeperator { get; set; }
        public string LeafSeperator { get; set; }

        public static readonly DisplayIndexFormatter Formatter1_1_1 = new DisplayIndexFormatter()
        {
            BranchText = (i) => NumberWithBase.Base10.AsString(i+1),
            BranchSeperator = ".",
            LeafText = (i) => NumberWithBase.Base10.AsString(i+1),
            LeafSeperator = ".",
        };

        public static readonly DisplayIndexFormatter Formatter1_1a = new DisplayIndexFormatter() 
        { 
            BranchText = (i) => NumberWithBase.Base10.AsString(i+1),        // +1 since we dont want "0" as 1st digit
            BranchSeperator = ".",
            LeafText = (i) => NumberWithBase.BaseAlpha.AsString(i),         // no +1 since "a" is 1st digit
            LeafSeperator = ""
        };

        public DisplayIndexFormatter()
        {
        }

        public string Format(IDisplayIndex displayIndex)
        {
            if (!displayIndex.IsLeaf)
                return string.Join(BranchSeperator, displayIndex.Indexes.Select(BranchText));
            else
                return string.Join(BranchSeperator, displayIndex.Indexes.Take(displayIndex.Indexes.Count() - 1).Select(BranchText)) + LeafSeperator + LeafText(displayIndex.Indexes.Last());
        }
    }
}
