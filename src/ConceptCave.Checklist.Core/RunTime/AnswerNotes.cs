﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;

namespace ConceptCave.Checklist.RunTime
{
    public class AnswerNotes : Node, IAnswerNotes
    {
        public string Text { get; set; }

        public List<IAnswerNotesAttachment> Attachments { get; protected set; }

        public AnswerNotes()
        {
            Attachments = new List<IAnswerNotesAttachment>();
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            Text = decoder.Decode<string>("Text");
            Attachments = decoder.Decode<List<IAnswerNotesAttachment>>("Attachments");
        }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("Text", Text);
            encoder.Encode("Attachments", Attachments);
        }
    }

    public class AnswerNotesAttachment : Node, IAnswerNotesAttachment
    {
        public string Name { get; set; }

        public string ContentType { get; set; }

        public Guid ProviderId { get; set; }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            Name = decoder.Decode<string>("Name");
            ContentType = decoder.Decode<string>("ContentType");
            ProviderId = decoder.Decode<Guid>("ProviderId");
        }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("Name", Name);
            encoder.Encode("ContentType", ContentType);
            encoder.Encode("ProviderId", ProviderId);
        }

    }
}
