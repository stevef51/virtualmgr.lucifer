﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;

namespace ConceptCave.Checklist.RunTime
{
    public class MultiChoiceAnswer : Answer, IMultiChoiceAnswer, IConvertableToPrimitive
    {
        protected bool Decoding { get; set; }
        public MultiChoiceAnswer()
        {
            ChosenItems = new ListOf<IMultiChoiceItem>();
        }

        #region IMultiChoiceAnswer Members

        private IListOf<IMultiChoiceItem> _chosenItems;
        public IListOf<IMultiChoiceItem> ChosenItems
        {
            get { return _chosenItems; }
            set
            {
                UtcDateAnswered = DateTime.UtcNow;
                _chosenItems = value;

                if (this.PresentedQuestion == null)
                {
                    return;
                }

                IMultiChoiceQuestion question = (IMultiChoiceQuestion)this.PresentedQuestion.Question;

                var passes = (from c in _chosenItems where c.PassFail == true select c).Count();
                var nas = (from c in _chosenItems where c.PassFail.HasValue == false select c).Count();
                var fails = (from c in _chosenItems where c.PassFail == false select c).Count();

                if (passes == 0)
                {
                    if (fails == 0)
                    {
                        PassFail = null;
                    }
                    else
                    {
                        PassFail = false;
                    }
                }
                else
                {
                    PassFail = passes >= question.NumberOfAnswersMustPass;
                }

                Score = (from c in _chosenItems select c.Score).Sum();
            }
        }

        #endregion

        public override void  Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            Decoding = true;
 	        base.Decode(decoder);

            ChosenItems = new ListOf<IMultiChoiceItem>(decoder.Decode<IList<IMultiChoiceItem>>("ChosenItems"));
            Decoding = false;
        }
        
        public override void  Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
 	         base.Encode(encoder);

             encoder.Encode("ChosenItems", ChosenItems);
        }

        public override object AnswerValue
        {
            get
            {
                return ChosenItems;
            }
            set
            {
                if(Decoding)
                {
                    return;
                }

                if (value is ListOf<IMultiChoiceItem>)
                {
                    ChosenItems = new ListOf<IMultiChoiceItem>();

                    IMultiChoiceQuestion question = (IMultiChoiceQuestion)this.PresentedQuestion.Question;

                    foreach(var i in (ListOf<IMultiChoiceItem>)value)
                    {
                        var item = (from a in question.List.Items where a.Answer.Equals(i.Answer, StringComparison.InvariantCultureIgnoreCase) == true select a).DefaultIfEmpty(null).First();

                        if(item != null)
                        {
                            ChosenItems.Add(item);
                        }
                    }
                }
                else if (value is string)
                {
                    string[] split = ((string)value).Split(',');
                    ChosenItems.Items.Clear();

                    IMultiChoiceQuestion question = (IMultiChoiceQuestion)this.PresentedQuestion.Question;

                    split.ForEach(s =>
                    {
                        // unescape escaped comma's
                        var u = s.Replace("&#44;", ",");

                        var item = (from a in question.List.Items where a.Answer.Equals(u, StringComparison.InvariantCultureIgnoreCase) == true select a).DefaultIfEmpty(null).First();

                        if (item != null)
                        {
                            ChosenItems.Add(item);
                        }
                    });
                }
            }
        }

        public override string AnswerAsString
        {
            get 
            {
                // we are going to escape , in the answer's with the HTML escape of a comma
                return string.Join(",", (from c in ChosenItems select c.Display).Select(s => s.Replace(",", "&#44;")));
            }
        }


        #region Lingo Helper stuff

        public List<IMultiChoiceItem> NotChosenItems
        {
            get
            {
                IMultiChoiceQuestion question = (IMultiChoiceQuestion)PresentedQuestion.Question;

                return question.List.Except(ChosenItems).ToList();
            }
        }

        public List<string> ChosenItemAnswers
        {
            get
            {
                return (from c in ChosenItems select c.Answer).ToList();
            }
        }

        public List<string> NotChosenItemAnswers
        {
            get
            {
                return (from c in NotChosenItems select c.Answer).ToList();
            }
        }

        public List<string> ChosenItemDisplays
        {
            get
            {
                return (from c in ChosenItems select c.Display).ToList();
            }
        }

        public List<string> NotChosenItemDisplays
        {
            get
            {
                return (from c in NotChosenItems select c.Display).ToList();
            }
        }

        public bool HasItemChosen(object value)
        {
            if (value is string)
            {
                var result = (from c in ChosenItems where c.Answer == (string)value select c).DefaultIfEmpty(null).First();

                return result != null;
            }
            else if (value is IMultiChoiceItem)
            {
                var result = (from c in ChosenItems where c == value select c).DefaultIfEmpty(null).First();

                return result != null;
            }

            return false;
        }

        public bool AnyChosen
        {
            get
            {
                return ChosenItems.Items.Count != 0;
            }
        }

        public bool AllChosen
        {
            get
            {
                return ChosenItems.Items.Count == NotChosenItems.Count;
            }
        }

        #endregion


        object IConvertableToPrimitive.ConvertToPrimitive()
        {
            return AnswerAsString;
        }
    }
}
