﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;

namespace ConceptCave.Checklist.RunTime
{
    public class PayPalAnswer : Answer, IPayPalAnswer
    {
        protected List<IPayPalIPN> _notifications = new List<IPayPalIPN>();

        public override object AnswerValue
        {
            get
            {
                return Status;
            }
            set
            {
                UtcDateAnswered = DateTime.UtcNow;
                Status = (PayPalAnswerStatus)value;
            }
        }

        public override string AnswerAsString
        {
            get { return Status.ToString(); }
        }

        public PayPalAnswerStatus Status { get; protected set; }

        public string StatusReason { get; protected set; }

        public List<IPayPalIPN> Notifications
        {
            get { return _notifications; }
        }

        public void AddNotification(string ipnRequestData)
        {
            PayPalIPN ipn = new PayPalIPN() { RawData = ipnRequestData };
            _notifications.Add(ipn);

            if (ipn.PaymentStatus == "Completed")
            {
                AnswerValue = PayPalAnswerStatus.Confirmed;
            }
            else if (ipn.PaymentStatus == "Pending")
            {
                AnswerValue = PayPalAnswerStatus.Pending;
                StatusReason = ipn.GetFieldFromRawData("pending_reason");
            }
            else if (ipn.PaymentStatus == "Refunded")
            {
                AnswerValue = PayPalAnswerStatus.Refunded;
                StatusReason = ipn.GetFieldFromRawData("reason_code");
            }
            else if (ipn.PaymentStatus == "Failed" || ipn.PaymentStatus == "Denied")
            {
                AnswerValue = PayPalAnswerStatus.Rejected;
            }
            else if (ipn.PaymentStatus == "Reversed")
            {
                AnswerValue = PayPalAnswerStatus.Rejected;
                StatusReason = ipn.GetFieldFromRawData("reason_code");
            }
        }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("StatusReason", StatusReason);
            encoder.Encode("Notifications", _notifications);
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            StatusReason = decoder.Decode<string>("StatusReason");
            _notifications = decoder.Decode<List<IPayPalIPN>>("Notifications");
        }
    }

    public class PayPalIPN : Node, IPayPalIPN
    {
        private Dictionary<string, string> _cachedFields;

        public string RawData { get; set; }

        public string TransactionId
        {
            get
            {
                return GetFieldFromRawData("txn_id");
            }
        }

        public string TrasnactionType
        {
            get
            {
                return GetFieldFromRawData("txn_type");
            }
        }

        public string PayerEmail
        {
            get
            {
                return GetFieldFromRawData("payer_email");
            }
        }

        public string PayerStatus
        {
            get
            {
                return GetFieldFromRawData("payer_status");
            }
        }

        public string PayerFirstname
        {
            get
            {
                return GetFieldFromRawData("first_name");
            }
        }

        public string PayerSurname
        {
            get
            {
                return GetFieldFromRawData("last_name");
            }
        }

        public string ItemName
        {
            get
            {
                return GetFieldFromRawData("item_name");
            }
        }

        public decimal Amount
        {
            get
            {
                return decimal.Parse(GetFieldFromRawData("mc_gross"));
            }
        }

        public string PaymentStatus
        {
            get
            {
                return GetFieldFromRawData("payment_status");
            }
        }

        public string PaymentType
        {
            get
            {
                return GetFieldFromRawData("payment_type");
            }
        }

        public string GetFieldFromRawData(string fieldName)
        {
            if(string.IsNullOrEmpty(RawData))
            {
                return null;
            }

            if (_cachedFields == null)
            {
                string[] fields = RawData.Split('&');
                _cachedFields = new Dictionary<string, string>();
                fields.ForEach(fvp =>
                {
                    string[] pair = fvp.Split('=');
                    _cachedFields.Add(pair[0], pair[1]);
                });
            }

            string result = string.Empty;
            if (_cachedFields.TryGetValue(fieldName, out result) == false)
            {
                return null;
            }

            return result;
        }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("RawData", RawData);
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            RawData = decoder.Decode<string>("RawData");
        }
    }
}
