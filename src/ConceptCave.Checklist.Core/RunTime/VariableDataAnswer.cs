﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core.Coding;
using ConceptCave.Core.XmlCodable;

namespace ConceptCave.Checklist.RunTime
{
    public class VariableDataAnswer : Answer, IVariableDataAnswer
    {
        private object _answerObject;

        public object AnswerData
        {
            get { return _answerObject; }
            set
            {
                UtcDateAnswered = DateTime.UtcNow;
                _answerObject = value;
            }
        }

        public override DateTime UtcDateAnswered
        {
            get
            {
                if (base.UtcDateAnswered == DateTime.MinValue)
                {
                    base.UtcDateAnswered = DateTime.Now;
                }

                return base.UtcDateAnswered;
            }
            protected set
            {
                base.UtcDateAnswered = value;
            }
        }

        public override object AnswerValue
        {
            get
            {
                return AnswerData;
            }
            set
            {
                AnswerData = value;
            }
        }

        public override string AnswerAsString
        {
            get 
            {
                return ObjectToString(AnswerData);
            }
        }

        public static string ObjectToString(object data)
        {
            if (data == null)
            {
                return string.Empty;
            }

            IEncoder encoder = CoderFactory.CreateEncoder();
            encoder.Encode<object>("Data", data);

            return encoder.ToString();
        }

        public static object StringToObject(string data)
        {
            if (string.IsNullOrEmpty(data) == true)
            {
                return null;
            }

            IDecoder decoder = CoderFactory.GetDecoder(data, null);
            object result = null;

            decoder.Decode<object>("Data", out result);

            return result;
        }
    }
}
