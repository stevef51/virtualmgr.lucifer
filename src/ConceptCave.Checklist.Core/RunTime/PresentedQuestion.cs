﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Core;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.Commands.Runtime;

namespace ConceptCave.Checklist.RunTime
{
    public class PresentedQuestion : Node, IPresentedQuestion
    {
        public IDisplayIndexProvider Parent { get; set; }
        public IWorkingDocument WorkingDocument { get; set; }
        public IQuestion Question { get; private set; }
        public int Sequence { get; set; }
        public DateTime UtcDatePresented { get; protected set; }
        public string Prompt { get; private set; }
        public string Note { get; private set; }
        public bool Hidden { get; set; }
        public bool WasHidden { get; set; }

        public PresentedQuestion()
        {
        }

        public PresentedQuestion(IQuestion question, ILingoProgram program)
        {
            UtcDatePresented = DateTime.UtcNow;
            Question = question;
            if (program != null)
            {
                Prompt = program.ExpandString(question.Prompt);
                Note = program.ExpandString(question.Note);
            }
        }

        private string _nameAs;
        public string NameAs 
        {
            get
            {
                if (_nameAs == null)
                    return Question.Name;

                return _nameAs;
            }

            set
            {
                _nameAs = value;
            }
        }

        private IAnswer _answer;

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("UtcDatePresented", UtcDatePresented);
            if (NameAs != null)
                encoder.Encode("NameAs", NameAs);
            encoder.Encode("Document", WorkingDocument);
            encoder.Encode("Question", Question);
            encoder.Encode("Sequence", Sequence);
            encoder.Encode("Answer", _answer);
            encoder.Encode("Parent", Parent);
            encoder.Encode("Prompt", Prompt);
            encoder.Encode("Note", Note);
            encoder.Encode("Hidden", Hidden);
            encoder.Encode("WasHidden", WasHidden);
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            UtcDatePresented = decoder.Decode<DateTime>("UtcDatePresented");
            WorkingDocument = decoder.Decode<IWorkingDocument>("Document");            
            NameAs = decoder.Decode<string>("NameAs");
            Question = decoder.Decode<IQuestion>("Question");
            Sequence = decoder.Decode<int>("Sequence");
            _answer = decoder.Decode<IAnswer>("Answer");
            Parent = decoder.Decode<IDisplayIndexProvider>("Parent");
            Prompt = decoder.Decode<string>("Prompt");
            Note = decoder.Decode<string>("Note");
            Hidden = decoder.Decode<bool>("Hidden");
            WasHidden = decoder.Decode<bool>("WasHidden");
        }

        public override string ToString()
        {
            return string.Format("{0} - {1}", DisplayIndex, Question);
        }

        #region IPresented Members

        public IPresentable Presentable
        {
            get { return Question; }
        }

        public IAnswer GetAnswer()
        {
            if (_answer == null)
            {
                _answer = Question.CreateBlankAnswer(this);
                if (_answer != null)
                    WorkingDocument.CommandManager.Do(new AddAnswerCommand() { Answer = _answer });
            }

            return _answer;
        }

        #endregion

        #region IPresented Members

        public IEnumerable<IPresented> AsDepthFirstEnumerable()
        {
            yield return this;
        }

        public void AddToDefaultAnswerSource(IDefaultAnswerSource source, Guid containerId, IWorkingDocument workingDocument)
        {
            if (source == null)
            {
                return;
            }

            source.Add(this, GetAnswer(), containerId, workingDocument);
        }

        #endregion

        #region IDisplayIndex Members

        public IEnumerable<int> DisplayIndexes
        {
            get
            {
                if (Parent != null)
                {
                    foreach (int i in Parent.DisplayIndex.Indexes)
                        yield return i;

                    yield return Parent.IndexOfChild(this);
                }
            }
        }

        public int IndexOfChild(IDisplayIndexProvider child)
        {
            // Questions dont have children, this should never be called
            return 0;
        }

        public IDisplayIndex DisplayIndex
        {
            get 
            {
                if (_displayIndex == null)
                    _displayIndex = new DisplayIndex(this.DisplayIndexes, true);
                return _displayIndex;
            }
        } DisplayIndex _displayIndex;

        #endregion
    }
}
