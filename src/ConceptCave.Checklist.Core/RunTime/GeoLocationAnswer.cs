﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;

namespace ConceptCave.Checklist.RunTime
{
    public class GeoLocationAnswer : Answer, IGeoLocationAnswer
    {
        private ILatLng _coordinates;

        #region IGeoLocationAnswer Members

        public ILatLng Coordinates
        {
            get { return _coordinates;  }
            set
            {
                UtcDateAnswered = DateTime.UtcNow;
                _coordinates = value;
            }
        }

        public decimal Latitude
        {
            get
            {
                return Coordinates.Latitude;
            }
        }

        public decimal Longitude
        {
            get
            {
                return Coordinates.Longitude;
            }
        }

        public override object AnswerValue
        {
            get
            {
                return _coordinates;
            }
            set
            {
                Coordinates = (ILatLng)value;
            }
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            _coordinates = decoder.Decode<ILatLng>("Cord");
        }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("Cord", _coordinates);
        }

        #endregion

        public override string AnswerAsString
        {
            get 
            {
                if (_coordinates == null)
                {
                    return string.Empty;
                }

                return _coordinates.ToString();
            }
        }

    }
}
