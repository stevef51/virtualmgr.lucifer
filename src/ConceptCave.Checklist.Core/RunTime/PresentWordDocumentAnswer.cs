﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;

namespace ConceptCave.Checklist.RunTime
{
    public class PresentWordDocumentAnswer : Answer, IMandatoryValidatorOverride
    {
        protected bool _allPagesRead;
        public int LastPageRead { get; set; }

        public bool Accepted { get; set; }
        public string AcceptionNotes { get; set; }

        public bool AllPagesRead
        {
            get { return _allPagesRead; }
            set
            {
                UtcDateAnswered = DateTime.UtcNow;
                _allPagesRead = value;
            }
        }

        public override object AnswerValue
        {
            get
            {
                return AllPagesRead;
            }
            set
            {
                if (value == null)
                    AllPagesRead = false;
                else
                    AllPagesRead = (bool)value;
            }
        }

        public override string AnswerAsString
        {
            get { return AllPagesRead.ToString(); }
        }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("LastPageRead", LastPageRead);
            encoder.Encode("Accepted", Accepted);
            encoder.Encode("AcceptionNotes", AcceptionNotes);
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            LastPageRead = decoder.Decode<int>("LastPageRead");
            Accepted = decoder.Decode<bool>("Accepted");
            AcceptionNotes = decoder.Decode<string>("AcceptionNotes");
        }

        public bool IsValid()
        {
            return AllPagesRead;
        }
    }
}
