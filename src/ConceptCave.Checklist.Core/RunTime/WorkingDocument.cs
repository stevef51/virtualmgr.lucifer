﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Core;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.Core;
using ConceptCave.Checklist.Lingo;
using ConceptCave.Checklist.Commands.Runtime;
using ConceptCave.Checklist.Commands;
using ConceptCave.Checklist.Validators;
using ConceptCave.Checklist.ProcessingRules;
using System.Xml;
using ConceptCave.Core.XmlCodable;
using ConceptCave.Checklist.Facilities;

namespace ConceptCave.Checklist.RunTime
{
    public class WorkingDocument : LingoDocument, IWorkingDocument
    {
        public static string WorkingDocumentRootElementName = "WorkingDocument";

        public static IWorkingDocument CreateFromString(string data, IReflectionFactory reflectionFactory = null)
        {
			var decoder = CoderFactory.GetDecoder (data, reflectionFactory);

            IWorkingDocument doc = null;
            decoder.Decode<IWorkingDocument>(WorkingDocumentRootElementName, out doc);

            return doc;
        }

        private BookmarkCommand _undoPoint = new BookmarkCommand() { Tag = "UndoPoint" };
        private Variable<int> _presentedIndex = new Variable<int>("PresentingIndex") { Value = -1 };

        public List<IPresented> Presented { get; private set; }

        private ILingoProgram _program;
        public ILingoProgram Program
        {
            get
            {
                if (_program == null && DesignDocument != null && DesignDocument.ProgramDefinition != null)
                {
                    _program = DesignDocument.ProgramDefinition.Compile(this);
                }

                return _program;
            }
        }

        public List<IAnswer> Answers { get; private set; }
        public IDesignDocument DesignDocument { get; private set; }
        public List<IFacilityAction> Actions { get; private set; }

        public IDefaultAnswerSource DefaultAnswerSource { get; set; }
        
        public WorkingDocument()
        {
            Presented = new List<IPresented>();
            Answers = new List<IAnswer>();
            Questions = new NamedList<IQuestion>(1);
            Sections = new NamedList<ISection>(1);
            Actions = new List<IFacilityAction>();
            Facilities = new NamedList<IFacility>(1);
            DefaultAnswerSource = new InlineDefaultAnswerSource();
            FinishMode = FinishMode.Manual;
        }

        public WorkingDocument(IDesignDocument designDocument)
        {
            Answers = new List<IAnswer>();
            Questions = designDocument.Questions;
            Sections = designDocument.Sections;
            Facilities = designDocument.Facilities;
            Variables = new VariableBag(designDocument.Globals);
            DesignDocument = designDocument;
            Actions = new List<IFacilityAction>();
            Presented = new List<IPresented>();

            DefaultAnswerSource = new InlineDefaultAnswerSource();
            FinishMode = designDocument.FinishMode;

            HookUpContextRequireds((from f in Facilities where f is IRequiresContextContainer select f).Cast<IRequiresContextContainer>());
        }

        public WorkingDocument(IDesignDocument designDocument, Guid workingDocumentId) : this(designDocument)
        {
            _id = workingDocumentId;
        }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("Presented", Presented);
            encoder.Encode("Answers", Answers);
            encoder.Encode("PresentedIndex", _presentedIndex);
            encoder.Encode("DesignDocument", DesignDocument);
            encoder.Encode("Program", _program);
            encoder.Encode("Actions", Actions);
            encoder.Encode("Facilities", Facilities);
            encoder.Encode("Blockers", _blockers);

            encoder.Encode("DefaultAnswerSource", DefaultAnswerSource);
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            Presented = decoder.Decode<List<IPresented>>("Presented", null);
            Answers = decoder.Decode<List<IAnswer>>("Answers", null);
            _presentedIndex = decoder.Decode<Variable<int>>("PresentedIndex", null);
            DesignDocument = decoder.Decode<IDesignDocument>("DesignDocument", null);
            _program = decoder.Decode<ILingoProgram>("Program", null);
            Actions = decoder.Decode<List<IFacilityAction>>("Actions");
            Facilities = decoder.Decode<NamedList<IFacility>>("Facilities");
            _blockers = decoder.Decode<List<IBlocker>>("Blockers");

            DefaultAnswerSource = decoder.Decode<IDefaultAnswerSource>("DefaultAnswerSource", null);

            // TODO: Need to work out why looping of the decoder.PostProcessingRequired doesn't always work
            // hook up to anything that might want to call back against us for the current context
            HookUpContextRequireds((from p in Facilities where p is IRequiresContextContainer select p).Cast<IRequiresContextContainer>());
        }

        private void HookUpContextRequireds(IEnumerable<IRequiresContextContainer> requires)
        {
            requires.ForEach(r => r.ContextRequired += WorkingDocument_ContextRequired);
        }

        private void WorkingDocument_ContextRequired(object source, RequiresContextContainerArgs args)
        {
            args.Context = this.Program;
        }

        public IEnumerable<IPresented> PresentedAsDepthFirstEnumerable()
        {
            foreach (IPresented presented in Presented)
            {
                if (presented is IPresentedSection)
                {
                    foreach (IPresented p in ((IPresentedSection)presented).AsDepthFirstEnumerable())
                    {
                        yield return p;
                    }
                }
                else
                    yield return presented;
            }
        }

        /// <summary>
        /// Helper method to simplify syntax of doing this from Lingo
        /// </summary>
        /// <param name="name">Name of question to get</param>
        /// <returns>Question</returns>
        public IQuestion QuestionByName(object name)
        {
            return Questions.Lookup(name);
        }

        #region IWorkingDocument Members

        public RunMode RunMode { get { return Program.RunMode.Value; } }

        public IPresented GetPresented()
        {
            if (_presentedIndex.Value >= 0)
                return Presented[_presentedIndex.Value];

            return null;
        }

        public RunMode Next()
        {
            switch(Program.RunMode.Value)
            {
                case RunMode.Finished:
                    return RunMode.Finished;

                case RunMode.Finishing:
                    CommandManager.Do(_undoPoint);
                    Program.RunFinisher();
                    //did we get a presenting run mode?
                    if (Program.RunMode.Value == Interfaces.RunMode.Presenting)
                    {
                        if (_presentedIndex.Value != Presented.Count - 1)
                            CommandManager.Do(new SetVariableCommand<int>() { Variable = _presentedIndex, NewValue = Presented.Count - 1 });
                    }
                    else
                    {
                        //kick GetPresented() into the "return null" branch
                        CommandManager.Do(new SetVariableCommand<int>() { Variable = _presentedIndex, NewValue = -1 });
                    }
                    //we are now definitively finished
                    Program.Do(new SetVariableCommand<RunMode>() { Variable = Program.RunMode, NewValue = RunMode.Finished });
                    break;

                case RunMode.Presenting:
                case RunMode.Running:
                    CommandManager.Do(_undoPoint);

                    Program.Do(new SetVariableCommand<RunMode>() { Variable = Program.RunMode, NewValue = RunMode.Running });

                    // Each run creates a new validation context
                    ValidatorResult validator = new ValidatorResult();
                    Program.Set<IValidatorResult>(validator);

                    InstructionResult result = Program.RunUntilStopped();
                    if (_presentedIndex.Value != Presented.Count - 1)
                        CommandManager.Do(new SetVariableCommand<int>() { Variable = _presentedIndex, NewValue = Presented.Count - 1 });

                    // If custom validation failed then back up
                    if (validator.Invalid)
                        this.Previous();

                    break;
            }

            return Program.RunMode.Value;
        }

        private bool IsUndoPoint(ICommand command)
        {
            BookmarkCommand bookmark = command as BookmarkCommand;
            if (bookmark == null)
                return false;

            return bookmark.Tag == "UndoPoint";
        }

        public bool Previous()
        {
            // if its supported, lets save the state of things at the moment before we back out of here
            if (DefaultAnswerSource != null)
            {
                var presented = GetPresented();
                if (presented != null)
                {
                    var pqs = (from p in presented.AsDepthFirstEnumerable() where p is IPresentedQuestion select p).Cast<IPresentedQuestion>();

                    pqs.ForEach(p =>
                    {
                        p.AddToDefaultAnswerSource(DefaultAnswerSource, ((IUniqueNode)presented.Presentable).Id, this);
                    });
                }
                
            }

            while (CommandManager.CanUndo && !IsUndoPoint(CommandManager.PeekUndo()))
                CommandManager.Undo();

            // Undo the UndoPoint also
            if (CommandManager.CanUndo)
                CommandManager.Undo();

            return CommandManager.CanUndo;
        }

        public bool CanPrevious()
        {
            return _presentedIndex.Value > 0;
        }

        public IValidatorResult ProcessRules()
        {
            return ProcessRules(ProcessDirection.Forward);
        }

        public IValidatorResult ProcessRules(ProcessDirection direction)
        {
            ValidatorResult result = new ValidatorResult();
            IPresented presented = GetPresented();
            if (presented != null)
            {
                var presentedQuestions = GetPresented().AsDepthFirstEnumerable().OfType<IPresentedQuestion>().Where(pq => !pq.WasHidden);

                foreach (var pq in presentedQuestions)
                {
                    ProcessingRuleContext prc = new ProcessingRuleContext() { Answer = pq.GetAnswer(), Context = Program, ValidatorResult = result, Direction = direction };

                    pq.Question.Validators.OrderBy(v => v.Order).ForEach(v => v.Process(prc));
                }
            }

            return result;
        }
        #endregion

        #region IDisplayIndexProvider Members

        public IDisplayIndexProvider Parent
        {
            get { return null; }
        }

        public int IndexOfChild(IDisplayIndexProvider child)
        {
            return Presented.IndexOf(child as IPresented);
        }

        public IDisplayIndex DisplayIndex
        {
            get { return _displayIndex; }
        } IDisplayIndex _displayIndex = new DisplayIndex(new int[0], false);

        #endregion

        #region IBlockable Members

        public bool IsBlocked
        {
            get { return _blockers.Count > 0; }
        }

        public IEnumerable<IBlocker> Blockers
        {
            get { return _blockers; }
        } List<IBlocker> _blockers = new List<IBlocker>();

        public IBlocker LastUnblockedBlocker { get; private set; }

        public void BlockOn(IBlocker blocker)
        {
            _blockers.Add(blocker);
            if (Program.RunMode.Value != RunMode.Blocked)
                    Program.Do(new SetVariableCommand<RunMode>() { Variable = Program.RunMode, NewValue = RunMode.Blocked });
        }

        public void UnblockFrom(IBlocker blocker)
        {
            if (_blockers.Contains(blocker))
            {
                LastUnblockedBlocker = blocker;
                _blockers.Remove(blocker);
 //               if (_blockers.Count == 0)
                {
                    if (Program.RunMode.Value == RunMode.Blocked)
                        Program.Do(new SetVariableCommand<RunMode>() { Variable = Program.RunMode, NewValue = RunMode.Running });
                }
            }
        }

        #endregion

        #region Lingo Helper Methods

        public void AssignDefaultAnswersFromWorkingDoucment(object source)
        {
            WorkingDocument doc = null;

            if (source is WorkingDocument)
            {
                doc = (WorkingDocument)source;
            }
            else if (source is WorkingDocumentObjectGetter)
            {
                doc = (WorkingDocument)((WorkingDocumentObjectGetter)source).WorkingDocument;
            }

            foreach (var ps in (from p in doc.PresentedAsDepthFirstEnumerable() where p is IPresentedSection select p).Cast<IPresentedSection>())
            {
                AssignDefaultAnswersFromPresentedSection(ps);
            }
        }

        public void AssignDefaultAnswersFromPresentedSection(IPresentedSection presented)
        {
            var section = (from s in this.AsDepthFirstEnumerable() where s is ConceptCave.Checklist.Editor.Section && ((ISection)s).Id == presented.Section.Id select s).Cast<ConceptCave.Checklist.Editor.Section>();

            if (section.Count() == 0)
            {
                // hmm, ok lets see if we can find things by name
                section = (from s in this.AsDepthFirstEnumerable() where s is ConceptCave.Checklist.Editor.Section && ((ISection)s).Name == presented.Section.Name select s).Cast<ConceptCave.Checklist.Editor.Section>();

                if (section.Count() == 0)
                {
                    return;
                }
            }

            section.First().AssignDefaultAnswersFrom(presented);
        }

        public void AssignDefaultAnswersFrom(object source)
        {
            if (source is WorkingDocumentObjectGetter || source is WorkingDocument)
            {
                AssignDefaultAnswersFromWorkingDoucment(source);
            }
            else if (source is IPresentedSection)
            {
                AssignDefaultAnswersFromPresentedSection((IPresentedSection)source);
            }
        }

        #endregion
    }
}
