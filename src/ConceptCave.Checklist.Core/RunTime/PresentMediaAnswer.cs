﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;

namespace ConceptCave.Checklist.RunTime
{
    public class PresentMediaAnswer : Answer, IPresentMediaAnswer
    {
        protected decimal? position;

        public override object AnswerValue
        {
            get
            {
                return Position;
            }
            set
            {
                if (value == null)
                    Position = null;
                else
                    Position = (decimal?)value;
            }
        }

        public override string AnswerAsString
        {
            get { return Position.ToString(); }
        }

        public decimal? Position
        {
            get
            {
                return position;
            }
            set
            {
                UtcDateAnswered = DateTime.UtcNow;
                position = value;
            }
        }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("Position", Position);
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            position = decoder.Decode<decimal?>("Position", null);
        }
    }
}
