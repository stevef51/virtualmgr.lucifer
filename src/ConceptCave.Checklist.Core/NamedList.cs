﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core.Coding;
using System.Collections;
using System.ComponentModel;

namespace ConceptCave.Checklist.Core
{
    public class NamedList<T> : INamedList<T>, IListSource, ICodable where T : IHasName, IHasIndex
    {
        private List<T> _list;
        private int _baseIndex = 0;

        public int BaseIndex { get { return _baseIndex; } }

        public NamedList()
        {
            _list = new List<T>();
        }

        public NamedList(int baseIndex)
        {
            _list = new List<T>();
            _baseIndex = baseIndex;
        }

        public NamedList(IEnumerable<T> collection)
        {
            _list = new List<T>(collection);
        }

        #region INamedList<T> Members

        public T Lookup(int index)
        {
            return _list[index - _baseIndex];
        }

        public T Lookup(object name)
        {
            if (name is int)
                return Lookup((int)name);

            string nameText = (string)name;
            return this.FirstOrDefault(t => string.Compare(nameText, t.Name, true) == 0);
        }

        #endregion

        #region ICodable Members

        public void Encode(IEncoder encoder)
        {
            if (_baseIndex != 0)
                encoder.Encode("BaseIndex", _baseIndex);
            encoder.Encode("List", _list);
        }

        public void Decode(IDecoder decoder)
        {
            _baseIndex = decoder.Decode<int>("BaseIndex", 0);
            _list = decoder.Decode<List<T>>("List", null);
            _list.ForEach(i => i.Index = () => IndexOf(i) + BaseIndex);
        }

        #endregion

        #region IList<T> Members

        public int IndexOf(T item)
        {
            return _list.IndexOf(item);
        }

        public void Insert(int index, T item)
        {
            item.Index = () => IndexOf(item) + BaseIndex;
            _list.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            _list.RemoveAt(index);
        }

        public T this[int index]
        {
            get
            {
                return _list[index];
            }
            set
            {
                _list[index] = value;
                value.Index = () => IndexOf(value) + BaseIndex;
            }
        }

        #endregion

        #region ICollection<T> Members

        public void Add(T item)
        {
            item.Index = () => IndexOf(item) + BaseIndex;
            _list.Add(item);
        }

        public void Clear()
        {
            _list.Clear();
        }

        public bool Contains(T item)
        {
            return _list.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            _list.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return _list.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public bool Remove(T item)
        {
            return _list.Remove(item);
        }

        #endregion

        #region IEnumerable<T> Members

        public IEnumerator<T> GetEnumerator()
        {
            return _list.GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _list.GetEnumerator();
        }

        #endregion

        #region IListSource Members

        public bool ContainsListCollection
        {
            get { return false; }
        }

        public IList GetList()
        {
            return _list;
        }

        #endregion
    }
}
