﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using ConceptCave.Core.Coding;
using ConceptCave.Core.SBONCodable;
using ConceptCave.Core.XmlCodable;

namespace ConceptCave.Checklist
{
    public interface ICoderFactory
    {
        IEncoder CreateEncoder();
    }

    public class CoderFactory
    {
        public static ICoderFactory DefaultFactory { get; set; }

        static CoderFactory()
        {
            DefaultFactory = new XmlDocumentCoderFactory();
        }

        public static IEncoder CreateEncoder()
        {
            return DefaultFactory.CreateEncoder();
        }

        public static IDecoder GetDecoder(IEncoder encoder)
        {
            SBONEncoder sbon = encoder as SBONEncoder;
            if (sbon != null)
            {
                MemoryStream ms = new MemoryStream();
                sbon.WriteTo(ms);
                ms.Position = 0;
                return new SBONDecoder(ms);
            }

            XmlDocumentEncoder xml = encoder as XmlDocumentEncoder;
            if (xml != null)
                return new XmlDocumentDecoder(xml.Document.DocumentElement);

            return null;
        }

        public static IDecoder GetDecoder(string data, IReflectionFactory reflectionFactory)
        {
            try
            {
                // Base64 alphabet does not contain the Xml < start tag character
                if (!data.StartsWith("<"))
                {
                    byte[] bytes = Convert.FromBase64String(data);
                    return new SBONDecoder(new MemoryStream(bytes));
                }
            }
            catch
            {
            }

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(data);
            return new XmlDocumentDecoder(doc.DocumentElement, reflectionFactory);
        }

        public static IDecoder GetDecoder(Stream stream)
        {
            long startPos = stream.Position;
            var ms = stream as MemoryStream;
            if (ms == null)
            {
                ms = new MemoryStream();
                stream.CopyTo(ms);
                startPos = 0;
            }

            ms.Position = startPos;

            try
            {
                return new SBONDecoder(ms);
            }
            catch
            {
                ms.Position = startPos;
                var doc = new XmlDocument();
                doc.Load(ms);
                return new XmlDocumentDecoder(doc.DocumentElement);
            }
        }
    }
}
