﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Core;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.Commands.Runtime;

namespace ConceptCave.Checklist.Facilities
{
    public abstract class Facility : Node, IFacility, IResource
    {
        public string Name { get; set; }
    
        private Func<int> _index = () => -1;
        public Func<int> Index { get { return _index; } set { _index = value; } }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            if (string.IsNullOrEmpty(Name) == false)
            {
                encoder.Encode("Name", Name);
            }
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            string nameValue = string.Empty;
            nameValue = decoder.Decode<string>("Name");

            if (string.IsNullOrEmpty(nameValue) == false)
            {
                Name = nameValue;
            }
        }

        public override string ToString()
        {
            return Name;
        }

        public static void AddServerBasedAction(IContextContainer context, IFacilityAction action)
        {
            ILingoProgram program = context.Get<ILingoProgram>();

            program.Do(new AddFacilityActionCommand() { FacilityAction = action });
        }

    }
}
