﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Core;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core.Coding;
using ConceptCave.Checklist.Lingo.ObjectProviders;
using ConceptCave.Checklist.RunTime;

namespace ConceptCave.Checklist.Facilities
{
    public class DocumentManipulationFacility : Facility
    {
        public DocumentManipulationFacility()
        {
            this.Name = "DocumentManipulation";
        }

        public CurrentWorkingDocumentObjectGetter CurrentDocument
        {
            get
            {
                return new CurrentWorkingDocumentObjectGetter();
            }
        }


        public WorkingDocumentGetter FromDesignDocument(IContextContainer container, string groupName, string resourceName)
        {
            throw new NotImplementedException("This was scratched from commit afcdb90");
        }
    }

    public abstract class WorkingDocumentGetter : Node, IObjectGetter
    {
        public abstract IWorkingDocument GetWorkingDocument(IContextContainer container);

        /// <summary>
        /// Lingo helper method to make it easy for users to get at a section
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public ISection GetSection(IContextContainer context, string name)
        {
            WorkingDocument wd = (WorkingDocument)GetWorkingDocument(context);
            var result = (from s in wd.AsDepthFirstEnumerable() where s is ISection && string.IsNullOrEmpty(((ISection)s).Name) == false && ((ISection)s).Name.Equals(name, StringComparison.CurrentCultureIgnoreCase) select s).Cast<ISection>();

            if (result.Count() == 0)
            {
                return null;
            }

            return result.First();
        }

        public IQuestion GetQuestion(IContextContainer context, string name)
        {
            IWorkingDocument wd = GetWorkingDocument(context);
            var result = (from s in wd.Questions where string.IsNullOrEmpty(s.Name) == false && s.Name.Equals(name, StringComparison.CurrentCultureIgnoreCase) select s);

            if (result.Count() == 0)
            {
                return null;
            }

            return result.First();
        }

        public IPresentedQuestion GetPresentedQuestion(IContextContainer context, Guid questionId)
        {
            var wd = GetWorkingDocument(context);
            return
                wd.PresentedAsDepthFirstEnumerable()
                    .OfType<IPresentedQuestion>()
                    .SingleOrDefault(pq => pq.Question.Id == questionId);
        }

        public IPresentedQuestion GetPresentedQuestion(IContextContainer context, string questionName)
        {
            var wd = GetWorkingDocument(context);
            return
                wd.PresentedAsDepthFirstEnumerable()
                    .OfType<IPresentedQuestion>()
                    .SingleOrDefault(pq => pq.Question.Name == questionName);
        }

        /// <summary>
        /// Lingo helper method to make it easy for users to get at a section
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public IPresentedSection GetPresentedSection(IContextContainer context, string name)
        {
            ConceptCave.Checklist.RunTime.WorkingDocument wd = (ConceptCave.Checklist.RunTime.WorkingDocument)GetWorkingDocument(context);

            var result = (from s in wd.PresentedAsDepthFirstEnumerable() where s is IPresentedSection && string.IsNullOrEmpty(((IPresentedSection)s).Section.Name) == false && ((IPresentedSection)s).Section.Name.Equals(name, StringComparison.CurrentCultureIgnoreCase) select s).Cast<IPresentedSection>();

            if (result.Count() == 0)
            {
                return null;
            }

            return result.First();
        }
                                       
        public List<IPresentedSection> GetPresentedSections(IContextContainer context, string name)
        {
            ConceptCave.Checklist.RunTime.WorkingDocument wd = (ConceptCave.Checklist.RunTime.WorkingDocument)GetWorkingDocument(context);

            var result = (from s in wd.PresentedAsDepthFirstEnumerable() where s is IPresentedSection && string.IsNullOrEmpty(((IPresentedSection)s).Section.Name) == false && ((IPresentedSection)s).Section.Name.Equals(name, StringComparison.CurrentCultureIgnoreCase) select s).Cast<IPresentedSection>();

            if (result.Count() == 0)
            {
                return null;
            }

            return result.ToList();
        }

        /// <summary>
        /// Lingo helper method to make it easy for users to get at a section
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public IPresentedSection GetPresentedSectionByIndex(IContextContainer context, int index)
        {
            ConceptCave.Checklist.RunTime.WorkingDocument wd = (ConceptCave.Checklist.RunTime.WorkingDocument)GetWorkingDocument(context);

            var result = (from s in wd.PresentedAsDepthFirstEnumerable() where s is IPresentedSection select s).Cast<IPresentedSection>();

            if (result.Count() == 0 || index > result.Count())
            {
                return null;
            }

            return result.ToList()[index - 1];
        }


        #region IObjectGetter Members

        public bool GetObject(IContextContainer context, string name, out object result)
        {
            result = null;

            IWorkingDocument wd = GetWorkingDocument(context);

            IdentifierObjectProvider objectProvider = new IdentifierObjectProvider();
            bool hasResult = objectProvider.GetObject(wd.Program, name, out result);

            if (hasResult == false)
            {
                ReflectionObjectProvider reflect = new ReflectionObjectProvider(wd);
                return reflect.GetObject(wd.Program, name, out result);
            }

            return true;
        }

        #endregion
    }

    public class CurrentWorkingDocumentObjectGetter : WorkingDocumentGetter
    {
        public override IWorkingDocument GetWorkingDocument(IContextContainer container)
        {
            return container.Get<IWorkingDocument>();
        }
    }

    public class WorkingDocumentObjectGetter : WorkingDocumentGetter
    {
        public IWorkingDocument WorkingDocument { get; protected set; }

        public WorkingDocumentObjectGetter() { }

        public WorkingDocumentObjectGetter(IWorkingDocument workingDocument)
        {
            WorkingDocument = workingDocument;
        }

        public WorkingDocumentObjectGetter(string workingXML)
        {
            WorkingDocument = ConceptCave.Checklist.RunTime.WorkingDocument.CreateFromString(workingXML);
        }

        public override IWorkingDocument GetWorkingDocument(IContextContainer container)
        {
            return WorkingDocument;
        }

        public override void Encode(IEncoder encoder)
        {
            base.Encode(encoder);

            // we save the working document to XML (which gives us a working document inside another working document)
            // as there is the possibility that script will change something in this working document, so just holding
            // the id and using that to load the original working document from the db isn't going to be enough as that
            // won't persist changes across postbacks etc
            encoder.Encode("WorkingDocument", WorkingDocument);
        }

        public override void Decode(IDecoder decoder)
        {
            base.Decode(decoder);

            WorkingDocument = decoder.Decode<IWorkingDocument>("WorkingDocument");
        }
    }

}
