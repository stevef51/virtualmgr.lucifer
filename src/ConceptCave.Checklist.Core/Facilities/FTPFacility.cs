﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;

namespace ConceptCave.Checklist.Facilities
{
    public class FTPFacility : Facility
    {
        public class FTPOperationResult : Node
        {
            public string StatusCode { get; set; }
            public bool Success { get; set; }
            public string Description { get; set; }

            public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
            {
                base.Encode(encoder);

                encoder.Encode<string>("StatusCode", StatusCode);
                encoder.Encode<bool>("Success", Success);
                encoder.Encode<string>("Description", Description);
            }

            public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
            {
                base.Decode(decoder);

                StatusCode = decoder.Decode<string>("StatusCode");
                Success = decoder.Decode<bool>("Success");
                Description = decoder.Decode<string>("Description");
            }
        }

        public class FTPClient : Node
        {
            public string Url { get; set; }
            public string Username { get; set; }
            public string Password { get; set; }
            public bool UsePassive { get; set; }
            public bool UseBinary { get; set; }

            public FTPOperationResult SendMedia(IContextContainer context, IMediaItem mediaItem)
            {
                try
                {
                    FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format("{0}/{1}", Url, mediaItem.Name));
                    request.Method = WebRequestMethods.Ftp.UploadFile;
                    request.UsePassive = UsePassive;
                    request.UseBinary = UseBinary;

                    request.Credentials = new NetworkCredential(Username, Password);

                    byte[] data = mediaItem.GetData(context);

                    request.ContentLength = data.Length;

                    FTPOperationResult result = new FTPOperationResult();

                    using (Stream requestStream = request.GetRequestStream())
                    {
                        requestStream.Write(data, 0, data.Length);
                        requestStream.Close();

                        FtpWebResponse response = (FtpWebResponse)request.GetResponse();

                        result.StatusCode = response.StatusCode.ToString();
                        result.Success = response.StatusCode == FtpStatusCode.ClosingData;
                        result.Description = response.StatusDescription;

                        response.Close();
                    }

                    return result;
                }
                catch(Exception e)
                {
                    return new FTPOperationResult() { StatusCode = FtpStatusCode.Undefined.ToString(), Success = false, Description = e.Message };
                }
            }

            public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
            {
                base.Encode(encoder);

                encoder.Encode<string>("Url", Url);
                encoder.Encode<string>("Username", Username);
                encoder.Encode<string>("Password", Password);
                encoder.Encode<bool>("UsePassive", UsePassive);
                encoder.Encode<bool>("UseBinary", UseBinary);
            }

            public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
            {
                base.Decode(decoder);

                Url = decoder.Decode<string>("Url");
                Username = decoder.Decode<string>("Username");
                Password = decoder.Decode<string>("Password");
                UsePassive = decoder.Decode<bool>("UsePassive");
                UseBinary = decoder.Decode<bool>("UseBinary");
            }
        }

        public FTPFacility()
        {
            Name = "Ftp";
        }

        public FTPClient NewFTPClient(string url, string username, string password)
        {
            FTPClient client = new FTPClient()
            {
                Url = url,
                Username = username,
                Password = password
            };

            return client;
        }

        public FTPClient NewFTPClient()
        {
            return new FTPClient();
        }
    }
}
