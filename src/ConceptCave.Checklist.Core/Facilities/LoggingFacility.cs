﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Core;

namespace ConceptCave.Checklist.Facilities
{
    public interface ILogMessageFormatter
    {
        string FormatMessage(IContextContainer container, ConceptCave.Checklist.Facilities.LoggingFacility.LoggingList logger);
    }

    public class LoggingFacility : Facility
    {
        public class LogItem : Node
        {
            public DateTime Created {get;set;}
            public string Title {get;set;}
            public string Message {get;set;}

            public LogItem()
            {
                Created = DateTime.Now;
            }

            public void ToHtmlRow(StringBuilder builder)
            {
                builder.Append("<tr><td>");
                builder.Append("<strong>");
                builder.Append(Title);
                builder.Append("</strong></td>");
                builder.Append("<td>");
                builder.Append(Created.ToString());
                builder.Append("</td><td>");
                builder.Append(Message);
                builder.Append("</td></tr>");
            }

            public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
            {
                base.Encode(encoder);

                encoder.Encode("Created", Created);
                encoder.Encode("Title", Title);
                encoder.Encode("Message", Message);
            }

            public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
            {
                base.Decode(decoder);

                Created = decoder.Decode<DateTime>("Created");
                Title = decoder.Decode<string>("Title");
                Message = decoder.Decode<string>("Message");
            }
        }

        public class LoggingList : Node
        {
            public List<LogItem> Items { get; protected set; }

            public LoggingList()
            {
                Items = new List<LogItem>();
            }

            public void Log(IContextContainer container, string title, object from)
            {
                LogItem item = new LogItem() { Title = title };
                Items.Add(item);

                item.Message = FormatMessage(container, from);
            }

            public string FormatMessage(IContextContainer container, object from)
            {
                if (from == null)
                {
                    return string.Empty;
                }

                if (from is string)
                {
                    return (string)from;
                }
                else if (from is ILogMessageFormatter)
                {
                    return ((ILogMessageFormatter)from).FormatMessage(container, this);
                }
                else
                {
                    return from.ToString();
                }
            }

            public string ToHtml()
            {
                StringBuilder builder = new StringBuilder();

                builder.Append("<table class=\"table\"><tr><th>Date</th><th>Title</th><th>Message</th></tr>");
                Items.ForEach(i => i.ToHtmlRow(builder));
                builder.Append("</table>");

                return builder.ToString();
            }

            public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
            {
                base.Encode(encoder);

                encoder.Encode("Items", Items);
            }

            public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
            {
                base.Decode(decoder);

                Items = decoder.Decode<List<LogItem>>("Items");
            }
        }

        public LoggingFacility()
        {
            Name = "Logging";
        }

        public LoggingList NewLog()
        {
            return new LoggingList();
        }
    }
}
