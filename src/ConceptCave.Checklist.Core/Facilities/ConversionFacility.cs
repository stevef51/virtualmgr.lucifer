﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;


namespace ConceptCave.Checklist.Facilities
{
    public class ConversionFacility : Facility
    {
        //This facility will wrap up the (int, bool, Guid, ...).Parse() methods to make them accessible in lingo
        //The primary motivation for this is to complement the GlobalSettings facility, which returns settings as strings.
        //This facility allows lingo to convert these strings to the correct type
        //WARNING: These methods throw exceptions if you're wrong about the type; these exceptions can't be currently caught in lingo

        public ConversionFacility()
        {
            Name = "Conversion";
        }


        //The CanBeXXX methods return true if a tryparse will succeed, false otherwise
        public bool CanBeBool(string input)
        {
            bool output;
            return bool.TryParse(input, out output);
        }

        //The StringToXXX methods do a XXX.parse() and throw an exception if not
        public bool StringToBool(string input)
        {
            return bool.Parse(input);
        }

        public bool CanBeGuid(string input)
        {
            Guid output;
            return Guid.TryParse(input, out output);
        }

        public Guid StringToGuid(string input)
        {
            return Guid.Parse(input);
        }

        public bool CanBeInt(string input)
        {
            int output;
            return int.TryParse(input, out output);
        }

        public int StringToInt(string input)
        {
            return int.Parse(input);
        }

        public bool CanBeDouble(string input)
        {
            double output;
            return double.TryParse(input, out output);
        }

        public double StringToDouble(string input)
        {
            return double.Parse(input);
        }

        public bool CanBeDecimal(string input)
        {
            decimal output;
            return decimal.TryParse(input, out output);
        }

        public decimal StringToDecimal(string input)
        {
            return decimal.Parse(input);
        }

        public bool CanBeLong(string input)
        {
            long output;
            return long.TryParse(input, out output);
        }

        public long StringTOLong(string input)
        {
            return long.Parse(input);
        }

        public bool CanBeChar(string input)
        {
            char output;
            return char.TryParse(input, out output);
        }

        public char StringToChar(string input)
        {
            return char.Parse(input);
        }
    }
}
