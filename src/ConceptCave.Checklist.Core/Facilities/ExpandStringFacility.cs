﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;

namespace ConceptCave.Checklist.Facilities
{
    /// <summary>
    /// Development only class, when triggered will raise an event with an Expanded String
    /// </summary>
    public class ExpandStringFacility : Facility
    {
        public string Format { get; set; }
        public Action<string> Action { get; set; }

        public string Expand(ConceptCave.Core.IContextContainer context)
        {
            string result = context.Get<ILingoProgram>().ExpandString(Format);
            if (Action != null)
                Action(result);
            return result;
        }
    }
}
