﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConceptCave.Checklist.Facilities
{
    /// <summary>
    /// Facility to help Lingo do string manipulation functions such as split and truncate.
    /// </summary>
    public class StringFacility : Facility
    {
        public StringFacility()
        {
            Name = "String";
        }

        public string[] Split(string source, string splitOn)
        {
            return source.Split(splitOn.ToCharArray());
        }

        public string Join(IEnumerable<string> source, string seperator)
        {
            return string.Join(seperator, source);
        }

        public string Join(IEnumerable<string> source, string seperator, string leadingSingular, string leadingMulti)
        {
            return Join(source, seperator, leadingSingular, leadingMulti, "", "");
        }

        public string Join(IEnumerable<string> source, string seperator, string leadingSingular, string leadingMulti, string trailingSingular, string trailingMulti)
        {
            var list = source.ToList();
            if (list.Count == 0)
                return "";
            else if (list.Count == 1)
                return leadingSingular + list[0] + trailingSingular;
            else
                return leadingMulti + string.Join(seperator, list) + trailingMulti;
        }

        public string Join(IEnumerable<object> source, string seperator)
        {
            return Join(source.Where(o => o != null).Select(o => o.ToString()), seperator);
        }

        public string Join(IEnumerable<object> source, string seperator, string leadingSingular, string leadingMulti)
        {
            return Join(source.Where(o => o != null).Select(o => o.ToString()), seperator, leadingSingular, leadingMulti);
        }

        public string Join(IEnumerable<object> source, string seperator, string leadingSingular, string leadingMulti, string trailingSingular, string trailingMulti)
        {
            return Join(source.Where(o => o != null).Select(o => o.ToString()), seperator, leadingSingular, leadingMulti, trailingSingular, trailingMulti);
        }

        public string Left(string source, int length)
        {
            if (length < 0 || length >= source.Length)
            {
                return string.Empty;
            }

            return source.Substring(0, length);
        }

        public string Right(string source, int length)
        {
            if (length < 0 || length >= source.Length)
            {
                return string.Empty;
            }

            return source.Substring(source.Length - length, length);
        }

        public string Mid(string source, int start, int end)
        {
            if (start < 0 || start >= source.Length)
            {
                return string.Empty;
            }

            if (end < 0 || end >= source.Length)
            {
                return string.Empty;
            }

            int aStart = start;
            int aEnd = end;

            if (end < start)
            {
                aStart = end;
                aEnd = start;
            }

            return source.Substring(aStart, aEnd);
        }

        public string Trim(string source)
        {
            return source.Trim();
        }
        
        public string TrimStart(string source)
        {
            return source.TrimStart();
        }

        public string TrimEnd(string source)
        {
            return source.TrimEnd();
        }

        public string Trim(string source, string truncate)
        {
            return source.Trim(truncate.ToCharArray());
        }

        public string TrimStart(string source, string truncate)
        {
            return source.TrimStart(truncate.ToCharArray());
        }

        public string TrimEnd(string source, string truncate)
        {
            return source.TrimEnd(truncate.ToCharArray());
        }

        public string Replace(string source, string replacing, string replacedWith)
        {
            return source.Replace(replacing, replacedWith);
        }

        public bool IsNullOrEmpty(string source)
        {
            return string.IsNullOrEmpty(source);
        }

        public string SafeFileName(string filename)
        {
            foreach (var c in System.IO.Path.GetInvalidFileNameChars())
            {
                filename = filename.Replace(c.ToString(), "");
            }
            return filename;
        }
        public string SafeFilePath(string filename)
        {
            foreach (var c in System.IO.Path.GetInvalidPathChars())
            {
                filename = filename.Replace(c.ToString(), "");
            }
            return filename;
        }


    }
}
