﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Core.Coding;
using ConceptCave.Core;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.Commands.Runtime;
using ConceptCave.Checklist.Lingo.ObjectProviders;

namespace ConceptCave.Checklist.Facilities
{
    public class ExecutionFacility : Facility, IDecoderPostProcessing, IRequiresContextContainer
    {

        public event RequiresContextContainerHandler ContextRequired = delegate { };

        public ExecutionFacility()
        {
            Name = "Execution";
        }

        protected IContextContainer _currentContext;
        protected IContextContainer ContextContainer
        {
            get
            {
                if (_currentContext == null)
                {
                    RequiresContextContainerArgs args = new RequiresContextContainerArgs();
                    ContextRequired(this, args);

                    _currentContext = args.Context;
                }

                return _currentContext;
            }
        }

        protected ICommandManager CommandManager
        {
            get
            {
                IWorkingDocument wd = ContextContainer.Get<IWorkingDocument>();
                return wd.CommandManager;
            }
        }

        /// <summary>
        /// Allows Lingo to turn off the recording of commands. This is typically used
        /// for long processing runs where the requirment to go backwards through it isn't required.
        /// For longish processes or loops, the storing of all of the commands that go along with things
        /// can cause memory issues. We've seen this when importing from CSV files
        /// </summary>
        public bool CommandRecordingDisabled
        {
            get
            {
                return CommandManager.RecordingDisabled;
            }
            set
            {
                CommandManager.RecordingDisabled = value;
            }
        }

        public void Set(object obj, string property, object value)
        {
            CommandManager.Do(new SetPropertyCommand(obj, property, value));
        }
    }
}
