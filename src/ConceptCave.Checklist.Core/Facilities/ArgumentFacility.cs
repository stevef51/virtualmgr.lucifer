﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;
using ConceptCave.Core.Coding;

namespace ConceptCave.Checklist.Facilities
{
    //Not actually a facility, but a bucket to put in with a SetVariableCommand
    public class ArgumentBucket : ICodable, IObjectGetter
    {
        private Dictionary<string, object> Arguments;

        public ArgumentBucket()
        {
            //empty constructor for being decoded into
            Arguments = new Dictionary<string, object>(StringComparer.InvariantCultureIgnoreCase);
        }

        public ArgumentBucket(IDictionary<string, object> inArguments)
        {
            Arguments = new Dictionary<string, object>(inArguments, StringComparer.InvariantCultureIgnoreCase);
        }

        public void Encode(IEncoder encoder)
        {
            encoder.Encode("Arguments", Arguments);
        }

        public void Decode(IDecoder decoder)
        {
            //Need to construct a new dictionary because i suspect the equalitycomparer does not stick
            Arguments = new Dictionary<string, object>(decoder.Decode<Dictionary<string, object>>("Arguments"), 
                StringComparer.InvariantCultureIgnoreCase);
        }

        public bool GetObject(IContextContainer context, string name, out object result)
        {
            try
            {
                result = Arguments[name];
                return true;
            }
            catch (KeyNotFoundException)
            {
                result = null;
                return false;
            }
        }
    }
}
