﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;

namespace ConceptCave.Checklist.Facilities
{
    public class PdfGenFacility : Facility
    {
        public PdfGenFacility()
        {
            Name = "PdfGeneration";
        }

        public byte[] Html2Pdf(IContextContainer context, string html)
        {
            var convertService = context.Get<IPdfServiceConnector>();
            var bytes = convertService.GetPdfBytes(html);
            return bytes;
        }

        public void AttachToUser(IContextContainer context, string html, Guid userId, string fileName, string name, string description)
        {
            var convertService = context.Get<IPdfServiceConnector>();
            convertService.AttachPdfToUser(html, userId, fileName, name, description);
            return;
        }
    }
}
