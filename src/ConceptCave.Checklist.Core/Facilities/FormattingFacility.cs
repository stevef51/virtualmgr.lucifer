﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Core;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.RunTime;
using System.Collections;
using ConceptCave.Checklist.Reporting.LingoQuery;

namespace ConceptCave.Checklist.Facilities
{
    public class FormattingFacility : Facility
    {
        /// <summary>
        /// Lingo helper class to allow for easy formatting of ol and ul HTML elements.
        /// </summary>
        public class ListFormattingHelper : Node
        {
            public class ListFormattingHelperItem : Node
            {
                public string Text { get; set; }

                public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
                {
                    base.Encode(encoder);

                    encoder.Encode("Text", Text);
                }

                public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
                {
                    base.Decode(decoder);

                    Text = decoder.Decode<string>("Text");
                }
            }

            public bool IsOrdered { get; set; }
            public List<ListFormattingHelperItem> Items { get; protected set; }

            public ListFormattingHelper()
            {
                Items = new List<ListFormattingHelperItem>();
            }

            public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
            {
                base.Encode(encoder);

                encoder.Encode("IsOrdered", IsOrdered);
                encoder.Encode("Items", Items);
            }

            public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
            {
                base.Decode(decoder);

                IsOrdered = decoder.Decode<bool>("IsOrdered");
                Items = decoder.Decode<List<ListFormattingHelperItem>>("Items");
            }

            public void Add(object data)
            {
                if (data is string)
                {
                    AddItem((string)data);
                }
                else if (data is IEnumerable<string>)
                {
                    AddItems((IEnumerable<string>)data);
                }
                else if (data is IAnswer)
                {
                    if (data is IMultiChoiceAnswer)
                    {
                        AddItems((from c in ((IMultiChoiceAnswer)data).ChosenItems select c.Display));
                    }
                    else
                    {
                        AddItem(((IAnswer)data).AnswerAsString);
                    }
                }
            }

            protected void AddItems(IEnumerable<string> texts)
            {
                texts.ToList().ForEach(t => AddItem(t));
            }

            /// <summary>
            /// Adds a new item to the list of items
            /// </summary>
            /// <param name="text">Text to be assigned to the new item</param>
            /// <returns>The new item that has been added to the list</returns>
            protected ListFormattingHelperItem AddItem(string text)
            {
                ListFormattingHelperItem item = new ListFormattingHelperItem() { Text = text };

                Items.Add(item);

                return item;
            }

            /// <summary>
            /// Removes any items that have text as their Text
            /// </summary>
            /// <param name="text">Text of items to be removed</param>
            public void Remove(string text)
            {
                var items = (from i in Items where i.Text == text select i);

                items.ToList().ForEach(i => Items.Remove(i));
            }

            public string ToHtml()
            {
                StringBuilder content = new StringBuilder(IsOrdered ? "<ol>" : "<ul>");

                Items.ForEach(i => content.AppendFormat("<li>{0}</li>", i.Text));

                content.Append(IsOrdered ? "</ol>" : "</ul>");

                return content.ToString();
            }
        }

        public class TableFormattingHelper : Node
        {
            public class TableFormattingHelperRow : Node
            {
                public bool IsHeader { get; set; }

                public new string Id { get; set; }

                public string CustomClass { get; set; }
                public string CustomStyle { get; set; }

                public List<TableFormattingHelperCell> Cells { get; set; }

                public TableFormattingHelperRow()
                {
                    Cells = new List<TableFormattingHelperCell>();
                    Id = "tablerow" + Guid.NewGuid();
                    CustomClass = "";
                    CustomStyle = "";
                }

                public TableFormattingHelperCell CreateCell(string text)
                {
                    TableFormattingHelperCell cell = new TableFormattingHelperCell() { Text = text };
                    Cells.Add(cell);

                    return cell;
                }

                public IEnumerable<TableFormattingHelperCell> CreateCells(IEnumerable<string> texts)
                {
                    List<TableFormattingHelperCell> result = new List<TableFormattingHelperCell>();

                    texts.ForEach(t =>
                    {
                        result.Add(CreateCell(t));
                    });

                    return result;
                }

                public void AddQuestionAndAnswer(IAnswer answer)
                {
                    CreateCell(answer.PresentedQuestion.Question.Prompt);
                    Add(answer);
                }

                protected bool IsQuestionVisible(IQuestion question)
                {
                    var rules = (from r in question.Validators where r is ConceptCave.Checklist.ProcessingRules.TableFormatterRule select r).Cast<ConceptCave.Checklist.ProcessingRules.TableFormatterRule>();

                    foreach (var r in rules)
                    {
                        if (r.Visible == false)
                        {
                            return false;
                        }
                    }

                    return true;
                }

                public void Add(object data)
                {
                    Add(data, false);
                }

                public void Add(object data, bool alwaysShowQuestionPrompts)
                {
                    if (data is string)
                    {
                        CreateCell((string)data);
                    }
                    else if (data is IEnumerable<string>)
                    {
                        CreateCells((IEnumerable<string>)data);
                    }
                    else if (data is IPresentedQuestion)
                    {
                        IPresentedQuestion pq = (IPresentedQuestion)data;

                        if (IsQuestionVisible(pq.Question) == false)
                        {
                            return;
                        }

                        if (pq.Question.HidePrompt == false || alwaysShowQuestionPrompts == true)
                        {
                            CreateCell(pq.Prompt);
                        }

                        if (pq.Question is ILabelQuestion)
                        {
                            // the prompt is the first cell, lets make the text the second one
                            Add(((ILabelQuestion)pq.Question).Text);
                        }
                        else
                        {
                            Add(pq.GetAnswer());
                        }
                    }
                    else if (data is IQuestion)
                    {
                        IQuestion q = (IQuestion)data;
                        if (IsQuestionVisible(q) == false)
                        {
                            return;
                        }

                        if (!q.HidePrompt || alwaysShowQuestionPrompts == true)
                            CreateCell(q.Prompt);

                        Add(q.DefaultAnswer ?? "");
                    }
                    else if (data is IAnswer)
                    {
                        if (data is IMultiChoiceAnswer)
                        {
                            CreateCells((from c in ((IMultiChoiceAnswer)data).ChosenItems select c.Display));
                        }
                        else
                        {
                            CreateCell(((IAnswer)data).AnswerAsString);
                        }
                    }
                    else if (data is IEnumerable<IMultiChoiceItem>)
                    {
                        CreateCells((from c in (IEnumerable<IMultiChoiceItem>)data select c.Display));
                    }
                    else
                    {
                        // who the hell knows what to do, let's fall back to something safe
                        CreateCell(data == null ? "" : data.ToString());
                    }
                }

                public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
                {
                    base.Encode(encoder);

                    encoder.Encode("IsHeader", IsHeader);
                    encoder.Encode("Cells", Cells);
                    encoder.Encode("CustomClass", CustomClass);
                    encoder.Encode("CustomStyle", CustomStyle);
                }

                public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
                {
                    base.Decode(decoder);

                    IsHeader = decoder.Decode<bool>("IsHeader");
                    Cells = decoder.Decode<List<TableFormattingHelperCell>>("Cells");
                    CustomClass = decoder.Decode<string>("CustomClass", "");
                    CustomStyle = decoder.Decode<string>("CustomStyle", "");
                }

                public string ToHtml(bool formatForEmail = false)
                {
                    var builder = new StringBuilder();
                    builder.AppendFormat("<tr id='{0}' class='{1}' style='{2}'>", Id, CustomClass, CustomStyle);

                    foreach (var cell in Cells)
                    {
                        if (IsHeader)
                        {
                            builder.Append("<th");
                            if (formatForEmail == true)
                            {
                                builder.Append(" style=\"padding: 8px;line-height: 18px;text-align: left;vertical-align: top;border-top: 1px solid #dddddd;font-weight: bold;\"");
                            }
                            builder.Append(">");
                        }
                        else
                        {
                            builder.Append("<td");
                            if (formatForEmail == true)
                            {
                                builder.Append(" style=\"padding: 8px;line-height: 18px;text-align: left;vertical-align: top;border-top: 1px solid #dddddd;\"");
                            }
                            builder.Append(">");
                        }

                        builder.Append(cell.Text);
                        builder.Append(IsHeader ? "</th>" : "</td>");
                    }

                    builder.Append("</tr>");

                    return builder.ToString();
                }
            }

            public class TableFormattingHelperCell : Node
            {
                public string Text { get; set; }

                public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
                {
                    base.Encode(encoder);
                    encoder.Encode("Text", Text);
                }

                public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
                {
                    base.Decode(decoder);
                    Text = decoder.Decode<string>("Text");
                }
            }

            public List<TableFormattingHelperRow> HeaderRows { get; set; }
            public List<TableFormattingHelperRow> BodyRows { get; set; }

            // Email clients don't tend to reference external css, so we have to embed in a style tag for these
            public bool FormatForEmail { get; set; }

            public bool AlwaysShowQuestionPrompts { get; set; }

            public bool ShowLabels { get; set; }

            public string CustomClass { get; set; }

            public TableFormattingHelper()
            {
                HeaderRows = new List<TableFormattingHelperRow>();
                BodyRows = new List<TableFormattingHelperRow>();
            }

            public TableFormattingHelperRow CreateHeaderRow()
            {
                TableFormattingHelperRow row = new TableFormattingHelperRow() { IsHeader = true };
                HeaderRows.Add(row);

                return row;
            }

            public TableFormattingHelperRow CreateBodyRow()
            {
                TableFormattingHelperRow row = new TableFormattingHelperRow();
                BodyRows.Add(row);

                return row;
            }

            public void Add(IContextContainer container, object data)
            {
                if (data is IEnumerable<ISection>)
                {
                    foreach (var section in ((IEnumerable<ISection>)data))
                    {
                        AddSection(container, section);
                    }
                }
                else if (data is ISection)
                {
                    AddSection(container, (ISection)data);
                }
                else if (data is IEnumerable<IPresentedSection>)
                {
                    foreach (var ps in ((IEnumerable<IPresentedSection>)data))
                    {
                        AddPresentedSection(container, ps);
                    }
                }
                else if (data is IPresentedSection)
                {
                    AddPresentedSection(container, (IPresentedSection)data);
                }
                else if (data is LingoDatatable)
                {
                    AddDataTable(container, (LingoDatatable)data);
                }
                else if (data is string)
                {
                    var row = CreateBodyRow();
                    row.Add(data);
                }
            }

            public void AddDataTable(IContextContainer container, LingoDatatable table)
            {
                var hr = CreateHeaderRow();

                table.ColumnNames.ForEach(r => hr.Add(r));

                foreach (var r in table)
                {
                    var row = CreateBodyRow();

                    table.ColumnNames.ForEach(c => row.Add(r.GetColumn(c)));
                }
            }

            public void AddPresentedSection(IContextContainer container, IPresentedSection ps, bool breakOutSubSections = false)
            {
                WorkingDocument wd = (WorkingDocument)container.Get<IWorkingDocument>();

                switch (ps.Section.HtmlPresentation.Layout)
                {
                    case HtmlSectionLayout.Flow:
                        {
                            foreach (var pq in (from quest in ps.AsDepthFirstEnumerable() where quest is ConceptCave.Checklist.RunTime.PresentedQuestion select quest).Cast<PresentedQuestion>())
                            {
                                if ((pq.Question is ILabelQuestion) == false && pq.Question.IsAnswerable == false)
                                {
                                    continue;
                                }

                                if ((pq.Question is ILabelQuestion) == true && ShowLabels == false)
                                {
                                    continue;
                                }

                                CreateBodyRow().Add(pq, AlwaysShowQuestionPrompts);
                            }
                            break;
                        }
                }
            }

            public void AddSection(IContextContainer container, ISection section, bool breakOutSubSections = false)
            {
                WorkingDocument wd = (WorkingDocument)container.Get<IWorkingDocument>();

                switch (section.HtmlPresentation.Layout)
                {
                    case HtmlSectionLayout.Flow:
                        {
                            foreach (var q in (from quest in section.AsDepthFirstEnumerable() where quest is ConceptCave.Checklist.Editor.Question select quest).Cast<ConceptCave.Checklist.Editor.Question>())
                            {
                                // find presenteds for q
                                var ps = (from p in wd.PresentedAsDepthFirstEnumerable() where p is IPresentedQuestion && ((IPresentedQuestion)p).Question == q select p).Cast<IPresentedQuestion>();

                                if (ps.Count() != 0)
                                {
                                    foreach (var pq in ps)
                                    {
                                        if ((pq.Question is ILabelQuestion) == false && pq.Question.IsAnswerable == false)
                                        {
                                            continue;
                                        }

                                        if ((pq.Question is ILabelQuestion) == true && ShowLabels == false)
                                        {
                                            continue;
                                        }

                                        CreateBodyRow().Add(pq, AlwaysShowQuestionPrompts);
                                    }
                                }
                                else
                                {
                                    CreateBodyRow().Add(q, AlwaysShowQuestionPrompts);
                                }
                            }
                            break;
                        }

                    case HtmlSectionLayout.Table:
                        {
                            foreach (ISection s in (from s in section.Children where s is ISection select s))
                            {
                                var row = CreateBodyRow();
                                foreach (IQuestion q in (from q in s.Children where q is IQuestion select q))
                                {
                                    // find presenteds for q
                                    var ps = (from p in wd.PresentedAsDepthFirstEnumerable() where p is IPresentedQuestion && ((IPresentedQuestion)p).Question == q select p).Cast<IPresentedQuestion>();

                                    if (ps.Count() != 0)
                                    {
                                        foreach (var pq in ps)
                                        {
                                            if ((pq.Question is ILabelQuestion) == false && pq.Question.IsAnswerable == false)
                                            {
                                                continue;
                                            }

                                            if ((pq.Question is ILabelQuestion) == true && ShowLabels == false)
                                            {
                                                continue;
                                            }

                                            row.Add(pq);
                                        }
                                    }
                                }
                            }
                            break;
                        }
                }
            }

            public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
            {
                base.Encode(encoder);

                encoder.Encode("HeaderRows", HeaderRows);
                encoder.Encode("BodyRows", BodyRows);
                encoder.Encode("FormatForEmail", FormatForEmail);
                encoder.Encode("CustomClass", CustomClass);
                encoder.Encode("AlwaysShowQuestionPrompts", AlwaysShowQuestionPrompts);
            }

            public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
            {
                base.Decode(decoder);

                HeaderRows = decoder.Decode<List<TableFormattingHelperRow>>("HeaderRows", new List<TableFormattingHelperRow>());
                BodyRows = decoder.Decode<List<TableFormattingHelperRow>>("BodyRows", new List<TableFormattingHelperRow>());
                FormatForEmail = decoder.Decode<bool>("FormatForEmail");
                CustomClass = decoder.Decode<string>("CustomClass");
                AlwaysShowQuestionPrompts = decoder.Decode<bool>("AlwaysShowQuestionPrompts");
            }

            public string ToHtml()
            {
                StringBuilder builder = new StringBuilder(string.Format("<table class=\"table{0}\"", string.IsNullOrEmpty(CustomClass) == false ? " " + CustomClass : ""));

                if (FormatForEmail == true)
                {
                    builder.Append(" style=\"width:100%;background-color: transparent;border-collapse: collapse;border-spacing: 0;margin-bottom: 18px;\"");
                }

                builder.Append("><thead>");

                foreach (var row in HeaderRows)
                {
                    builder.Append(row.ToHtml(FormatForEmail));
                }
                builder.Append("</thead><tbody>");

                foreach (var row in BodyRows)
                {
                    builder.Append(row.ToHtml(FormatForEmail));
                }

                builder.Append("</tbody></table>");

                return builder.ToString();
            }
        }

        public class HeaderFormattingHelper : Node
        {
            public int Level { get; set; }
            public string Text { get; set; }

            public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
            {
                base.Encode(encoder);

                encoder.Encode("Level", Level);
                encoder.Encode("Text", Text);
            }

            public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
            {
                base.Decode(decoder);

                Level = decoder.Decode<int>("Level");
                Text = decoder.Decode<string>("Text");
            }

            public string ToHtml()
            {
                StringBuilder content = new StringBuilder();
                content.AppendFormat("<h{0}>{1}</h{0}>", Level, Text);

                return content.ToString();
            }
        }

        public FormattingFacility()
        {
            Name = "Formatting";
        }

        public ListFormattingHelper CreateOrderedList()
        {
            return new ListFormattingHelper() { IsOrdered = true };
        }

        public ListFormattingHelper CreateUnorderedList()
        {
            return new ListFormattingHelper() { IsOrdered = false };
        }

        public HeaderFormattingHelper CreateHeader(int level)
        {
            return new HeaderFormattingHelper() { Level = level };
        }

        public TableFormattingHelper CreateTable()
        {
            return new TableFormattingHelper();
        }

        public TableFormattingHelper CreateTableForEmail()
        {
            return new TableFormattingHelper() { FormatForEmail = true };
        }

        public string ListToString(object source, string seperator)
        {
            if ((source is IEnumerable) == false)
            {
                return "";
            }

            var s = (IEnumerable)source;
            var result = new List<string>();
            foreach (var item in s)
            {
                result.Add(item.ToString());
            }

            return string.Join(seperator, (from c in result select c));
        }

        public TableFormattingHelper CreateTableFrom(IContextContainer container, object data, bool formatForEmail)
        {
            var table = formatForEmail == false ? CreateTable() : CreateTableForEmail();

            table.Add(container, data);

            return table;
        }

        public HtmlElm Elm(string tag)
        {
            return new HtmlElm(tag);
        }

        public class HtmlAttr : Node
        {
            public string Name { get; set; }
            public string Value { get; set; }

            public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
            {
                base.Encode(encoder);
                encoder.Encode("Name", Name);
                encoder.Encode("Value", Value);
            }

            public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
            {
                base.Decode(decoder);

                Name = decoder.Decode<string>("Name");
                Value = decoder.Decode<string>("Value");
            }

            public string EscapedValue
            {
                get
                {
                    var s = Value;
                    if (s.Contains("\""))
                        s = s.Replace("\"", "&#34;");
                    if (s.Contains("&"))
                        s = s.Replace("&", "&amp;");
                    if (s.Contains("<"))
                        s = s.Replace("<", "&lt;");
                    return s;
                }
            }
        }

        public class HtmlElm : Node
        {
            private List<HtmlElm> _children = new List<HtmlElm>();
            private List<HtmlAttr> _attributes = new List<HtmlAttr>();
            private string _tag;
            private string _value = "";

            public HtmlElm()
            {
            }

            public HtmlElm(string tag)
            {
                _tag = tag;
            }

            public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
            {
                base.Encode(encoder);

                encoder.Encode("Tag", _tag);
                if (!string.IsNullOrEmpty(_value))
                    encoder.Encode("Value", _value);

                if (_attributes.Count > 0)
                    encoder.Encode("Attributes", _attributes);

                if (_children.Count > 0)
                    encoder.Encode("Children", _children);
            }

            public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
            {
                base.Decode(decoder);

                _tag = decoder.Decode<string>("Tag");
                _value = decoder.Decode<string>("Value", "");
                _attributes = decoder.Decode<List<HtmlAttr>>("Attributes", null) ?? new List<HtmlAttr>();
                _children = decoder.Decode<List<HtmlElm>>("Children", null) ?? new List<HtmlElm>();
            }

            public HtmlElm Append(HtmlElm elm)
            {
                if (!_children.Contains(elm))
                    _children.Add(elm);
                return this;
            }

            public HtmlElm Append(string childTag)
            {
                var child = new HtmlElm(childTag);
                _children.Add(child);
                return child;
            }

            public HtmlElm Value(string value)
            {
                _value = value;
                return this;
            }

            public string Value()
            {
                return _value;
            }

            public HtmlElm Class(string value)
            {
                var attr = _attributes.FirstOrDefault(a => string.Compare("class", a.Name, true) == 0);
                if (attr == null)
                {
                    attr = new HtmlAttr() { Name = "class" };
                    _attributes.Add(attr);
                }
                attr.Value = value;
                return this;
            }

            public HtmlElm Style(string value)
            {
                var attr = _attributes.FirstOrDefault(a => string.Compare("style", a.Name, true) == 0);
                if (attr == null)
                {
                    attr = new HtmlAttr() { Name = "style" };
                    _attributes.Add(attr);
                }
                attr.Value = value;
                return this;
            }

            protected void AppendTo(StringBuilder sb)
            {
                sb.AppendFormat("<{0} ", _tag);
                foreach (var attr in _attributes)
                    sb.AppendFormat("{0}=\"{1}\" ", attr.Name, attr.EscapedValue);

                sb.AppendFormat(">{0}", _value);

                foreach (var child in _children)
                    child.AppendTo(sb);
                sb.AppendFormat("</{0}>", _tag);
            }

            public string ToHtml()
            {
                StringBuilder sb = new StringBuilder();
                AppendTo(sb);
                return sb.ToString();
            }
        }
    }
}
