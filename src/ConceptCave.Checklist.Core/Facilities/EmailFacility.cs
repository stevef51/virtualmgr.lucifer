﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using System.Net.Mail;
//using ConceptCave.Checklist.Properties;
using ConceptCave.Checklist.Commands.Runtime;
using ConceptCave.Core;
using ConceptCave.Checklist.Core;
using System.IO;
using System.Collections.Specialized;

namespace ConceptCave.Checklist.Facilities
{
    public class EmailFacility : Facility, IFromNameValues
    {
        public class ServerEmailAction : Node, IFacilityAction, IServerBasedAction
        {
            public string To { get; set; }
            public string CC { get; set; }
            public string BCC { get; set; }
            public string Subject { get; set; }
            public string Body { get; set; }
            public string From { get; set; }
            public List<IMediaItem> Attachments { get; set; }
            public List<IMediaItem> LinkedResources { get; set; }

            private IEmailConfiguration _emailConfiguration { get; set; }
            private ISendGridConfiguration _sendGridConfiguration { get; set; }

            public ServerEmailAction(IEmailConfiguration emailConfiguration, ISendGridConfiguration sendGridConfiguration)
            {
                _emailConfiguration = emailConfiguration;
                _sendGridConfiguration = sendGridConfiguration;
            }

            public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
            {
                base.Encode(encoder);

                encoder.Encode("To", To);
                encoder.Encode("CC", CC);
                encoder.Encode("BCC", BCC);
                encoder.Encode("Subject", Subject);
                encoder.Encode("Body", Body);
                encoder.Encode("From", From);
                encoder.Encode("Attachments", Attachments);
                encoder.Encode("LinkedResources", LinkedResources);
            }

            public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
            {
                base.Decode(decoder);

                To = decoder.Decode<string>("To");
                CC = decoder.Decode<string>("CC");
                BCC = decoder.Decode<string>("BCC");
                Subject = decoder.Decode<string>("Subject");
                Body = decoder.Decode<string>("Body");
                From = decoder.Decode<string>("From");

                Attachments = decoder.Decode<List<IMediaItem>>("Attachments");
                LinkedResources = decoder.Decode<List<IMediaItem>>("LinkedResources");
                if (LinkedResources == null)
                {
                    LinkedResources = new List<IMediaItem>();
                }
            }

            #region IServerBasedAction Members

            public void ServerExecute(ConceptCave.Core.IContextContainer context)
            {
                var client = context.Get<SmtpClient>(() => new SmtpClient());

                var message = new MailMessage();
                var to = _emailConfiguration.FilterAddress(To);
                if (to.Length > 0)
                    message.To.Add(to);

                if (string.IsNullOrEmpty(CC.Trim(',', ' ')) == false)
                {
                    var cc = _emailConfiguration.FilterAddress(CC);
                    if (cc.Length > 0)
                        message.CC.Add(cc);
                }

                if (string.IsNullOrEmpty(BCC.Trim(',', ' ')) == false)
                {
                    var bcc = _emailConfiguration.FilterAddress(BCC);
                    if (bcc.Length > 0)
                        message.Bcc.Add(bcc);
                }

                message.Subject = Subject + _emailConfiguration.SubjectAppend ?? "";

                if (string.IsNullOrEmpty(From) == true)
                {
                    if (_emailConfiguration.From != null)
                        message.From = _emailConfiguration.From;
                }
                else
                {
                    message.From = new MailAddress(From);
                }

                message.Body = Body;
                message.IsBodyHtml = true;

                if (_sendGridConfiguration != null)
                {
                    _sendGridConfiguration.AddXsmtpApiHeader(message);
                }

                if (Attachments != null)
                {
                    foreach (var a in Attachments)
                    {
                        byte[] data = a.GetData(context);

                        MemoryStream ms = new MemoryStream(data);

                        Attachment attach = new Attachment(ms, a.Name, a.MediaType);

                        message.Attachments.Add(attach);
                    }
                }

                if (LinkedResources.Count > 0)
                {
                    AlternateView view = AlternateView.CreateAlternateViewFromString(Body, null, "text/html");
                    message.AlternateViews.Add(view);

                    foreach (var l in LinkedResources)
                    {
                        byte[] data = l.GetData(context);

                        MemoryStream ms = new MemoryStream(data);

                        var lr = new LinkedResource(ms, l.MediaType) { ContentId = l.Id.ToString() };
                        view.LinkedResources.Add(lr);
                    }
                }

                if (message.To.Count > 0 && _emailConfiguration.Enabled)
                    client.Send(message);
            }

            #endregion
        }

        public class EmailTemplate : Node, IHasName, IHasIndex
        {
            public string To { get; set; }
            public string CC { get; set; }
            public string BCC { get; set; }
            public string Subject { get; set; }
            public string Body { get; set; }

            public string From { get; set; }

            public bool SendImmediately { get; set; }

            public string Name { get; set; }
            public Func<int> Index { get; set; }

            /// <summary>
            /// images that are going to be included in the HTML version of the email
            /// </summary>
            public List<IMediaItem> LinkedResources { get; protected set; }

            /// <summary>
            /// Any items that are to be included as attachments
            /// </summary>
            public List<IMediaItem> Attachments { get; protected set; }

            private IEmailConfiguration _emailConfiguration { get; set; }
            private ISendGridConfiguration _sendGridConfiguration { get; set; }

            public EmailTemplate(IEmailConfiguration emailConfiguration, ISendGridConfiguration sendGridConfiguration) : base()
            {
                _emailConfiguration = emailConfiguration;
                Attachments = new List<IMediaItem>();
                LinkedResources = new List<IMediaItem>();
                _sendGridConfiguration = sendGridConfiguration;
            }

            public EmailTemplate CopyFrom(EmailTemplate template)
            {
                To = template.To;
                Subject = template.Subject;
                Body = template.Body;
                SendImmediately = template.SendImmediately;
                Name = template.Name;
                From = template.From;

                template.LinkedResources.ForEach(a => LinkedResources.Add(a));
                template.Attachments.ForEach(a => Attachments.Add(a));
                return this;
            }

            public void Send(ConceptCave.Core.IContextContainer context)
            {
                ILingoProgram program = context.Get<ILingoProgram>();

                string to = program.ExpandString(To ?? "");
                if (to.Trim().Length == 0)
                    return;

                string from = program.ExpandString(From ?? "");

                string cc = program.ExpandString(CC ?? "");
                string bcc = program.ExpandString(BCC ?? "");

                // Create the Email and add it to the TriggeredAction list which will execute when the Checklist is finished
                var action = new ServerEmailAction(_emailConfiguration, _sendGridConfiguration)
                {
                    To = to,
                    CC = cc,
                    BCC = bcc,
                    Subject = program.ExpandString(Subject) ?? "",
                    Body = program.ExpandString(Body) ?? "",
                    From = from,
                    Attachments = Attachments,
                    LinkedResources = LinkedResources
                };

                if (SendImmediately == false)
                {
                    AddServerBasedAction(context, action);
                }
                else
                {
                    try
                    {
                        action.ServerExecute(context);
                    }
                    catch { }
                }
            }

            public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
            {
                base.Encode(encoder);

                encoder.Encode("Name", Name);
                encoder.Encode("To", To);
                encoder.Encode("CC", CC);
                encoder.Encode("BCC", BCC);
                encoder.Encode("Subject", Subject);
                encoder.Encode("Body", Body);
                encoder.Encode("From", From);
                encoder.Encode("SendImmediately", SendImmediately);

                encoder.Encode("Attachments", Attachments);
                encoder.Encode("LinkedResources", LinkedResources);
            }

            public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
            {
                base.Decode(decoder);

                Name = decoder.Decode<string>("Name");
                To = decoder.Decode<string>("To");
                CC = decoder.Decode<string>("CC");
                BCC = decoder.Decode<string>("BCC");
                Subject = decoder.Decode<string>("Subject");
                Body = decoder.Decode<string>("Body");
                From = decoder.Decode<string>("From");
                SendImmediately = decoder.Decode<bool>("SendImmediately");

                Attachments = decoder.Decode<List<IMediaItem>>("Attachments", new List<IMediaItem>());
                LinkedResources = decoder.Decode<List<IMediaItem>>("LinkedResources", new List<IMediaItem>());
            }

            public override string ToString()
            {
                return string.Format("Email: To={0}, Subject={1}, Body={2}", To, Subject, Body);
            }

            public void AddAttachments(ConceptCave.Core.IContextContainer context, object items)
            {
                if (items is IEnumerable<IMediaItem>)
                {
                    Attachments.AddRange((IEnumerable<IMediaItem>)items);
                }
                else if (items is IMediaItem)
                {
                    Attachments.Add((IMediaItem)items);
                }
            }

            public void AddLinkedResources(ConceptCave.Core.IContextContainer context, object items)
            {
                if (items is IEnumerable<IMediaItem>)
                {
                    LinkedResources.AddRange((IEnumerable<IMediaItem>)items);
                }
                else if (items is IMediaItem)
                {
                    LinkedResources.Add((IMediaItem)items);
                }
            }
        }

        private INamedList<EmailTemplate> _templates = new NamedList<EmailTemplate>();
        private IEmailConfiguration _emailConfiguration;
        private ISendGridConfiguration _sendGridConfiguration;

        public EmailFacility(IEmailConfiguration emailConfiguration, ISendGridConfiguration sendGridConfiguration)
        {
            Name = "Email";
            _emailConfiguration = emailConfiguration;
            _sendGridConfiguration = sendGridConfiguration;
        }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("Templates", _templates);
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            _templates = decoder.Decode<INamedList<EmailTemplate>>("Templates");
        }

        public EmailTemplate TemplateByName(IContextContainer context, string name)
        {
            EmailTemplate template = _templates.Lookup(name);
            if (template == null)
                return null;
            return new EmailTemplate(_emailConfiguration, _sendGridConfiguration).CopyFrom(template);
        }

        public EmailTemplate TemplateByIndex(IContextContainer context, int index)
        {
            EmailTemplate template = _templates.Lookup(index);
            if (template == null)
                return null;
            return new EmailTemplate(_emailConfiguration, _sendGridConfiguration).CopyFrom(template);
        }

        public string LinkedResourceId(IContextContainer context, object value)
        {
            if (value is IMediaItem == false)
            {
                return string.Empty;
            }

            return ((IMediaItem)value).Id.ToString();
        }

        public string LinkedResourceImageHtml(IContextContainer context, object value)
        {
            if (value is IEnumerable<IMediaItem>)
            {
                var multiples = (IEnumerable<IMediaItem>)value;

                string result = string.Empty;
                foreach (var m in multiples)
                {
                    result += LinkedResourceImageHtml(context, m);
                }

                return result;
            }

            string id = LinkedResourceId(context, value);

            if (string.IsNullOrEmpty(id) == true)
            {
                return string.Empty;
            }

            return string.Format("<img style='padding:2px' src='cid:{0}' />", id);
        }

        public INamedList<EmailTemplate> Templates
        {
            get
            {
                return _templates;
            }
        }

        public virtual void FromNameValuePairs(NameValueCollection pairs)
        {
            if (pairs == null || pairs.Count == 0)
            {
                return;
            }

            string tName = null;
            string tTo = null;
            string tSubject = null;
            string tBody = null;
            string tImmediate = null;
            string tCc = null;
            string tBcc = null;

            // going to keep this simple and just use a switch statement rather than having
            // some funky reflection stuff.
            foreach (var name in pairs.AllKeys)
            {
                string value = pairs[name];
                switch (name.ToLower())
                {
                    case "template":
                        tName = value;
                        break;
                    case "to":
                        tTo = value;
                        break;
                    case "subject":
                        tSubject = value;
                        break;
                    case "body":
                        tBody = value;
                        break;
                    case "immediate":
                        tImmediate = value;
                        break;
                    case "cc":
                        tCc = value;
                        break;
                    case "bcc":
                        tBcc = value;
                        break;
                }
            }

            if (string.IsNullOrEmpty(tName) || string.IsNullOrEmpty(tTo) || string.IsNullOrEmpty(tSubject) || string.IsNullOrEmpty(tBody))
            {
                return;
            }

            EmailTemplate t = new EmailTemplate(_emailConfiguration, _sendGridConfiguration)
            {
                Name = tName,
                To = tTo,
                Subject = tSubject,
                Body = tBody
            };

            if (string.IsNullOrEmpty(tImmediate) == false)
            {
                bool i = false;
                if (bool.TryParse(tImmediate, out i) == true)
                {
                    t.SendImmediately = i;
                }
            }

            if (string.IsNullOrEmpty(tCc) == false)
            {
                t.CC = tCc;
            }

            if (string.IsNullOrEmpty(tBcc) == false)
            {
                t.BCC = tBcc;
            }

            Templates.Add(t);
        }

        public virtual List<NameValueCollection> ToNameValuePairs()
        {
            var result = new List<NameValueCollection>();

            foreach (var template in this.Templates)
            {
                var r = new NameValueCollection();

                r.Add("template", template.Name);
                r.Add("to", template.To);
                r.Add("subject", template.Subject);
                r.Add("body", template.Body);
                r.Add("immediate", template.SendImmediately.ToString());
                r.Add("cc", template.CC);
                r.Add("bcc", template.BCC);

                result.Add(r);
            }

            return result;
        }
    }
}
