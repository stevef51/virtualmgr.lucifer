﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;
using ConceptCave.Checklist.ProcessingRules;
using ConceptCave.Checklist.RunTime;

namespace ConceptCave.Checklist.Facilities
{
    /// <summary>
    /// Facility to help Lingo scripts find questions and answers that are tied to particular categories (through the CategoryRule)
    /// </summary>
    public class CategoryFacility : Facility
    {
        public CategoryFacility()
        {
            Name = "Categories";
        }

        public IEnumerable<string> GetDistinctCategories(IContextContainer context, IWorkingDocument workingDoc)
        {
            var result = (from a in workingDoc.Answers from v in ((IQuestion)a.PresentedQuestion.Presentable).Validators where v is CategoryRule select ((CategoryRule)v).Text).Distinct();

            return result.Count() == 0 ? new List<string>() : result.ToList();
        }

        public IEnumerable<IQuestion> FindQuestionsForCategory(IContextContainer context, IWorkingDocument workingDoc, string text)
        {
            var result = (from a in workingDoc.Questions from v in a.Validators where v is CategoryRule && ((CategoryRule)v).Text == text select a);

            return result.Count() == 0 ? new List<IQuestion>() : result.ToList();
        }

        public IEnumerable<IAnswer> FindAnswersForCategory(IContextContainer context, IWorkingDocument workingDoc, string text)
        {
            var result = (from p in ((WorkingDocument)workingDoc).PresentedAsDepthFirstEnumerable() where p is IPresentedQuestion from v in ((IPresentedQuestion)p).Question.Validators where v is CategoryRule && ((CategoryRule)v).Text == text select ((IPresentedQuestion)p).GetAnswer());

            return result.Count() == 0 ? new List<IAnswer>() : result.ToList();
        }

        public IEnumerable<string> FindAnswersValuesForCategory(IContextContainer context, IWorkingDocument workingDoc, string text)
        {
            var answers = (from a in workingDoc.Answers from v in ((IQuestion)a.PresentedQuestion.Presentable).Validators where v is CategoryRule && ((CategoryRule)v).Text == text select a);

            List<string> result = new List<string>();

            answers.ToList().ForEach(a =>
            {
                if (a is IMultiChoiceAnswer)
                {
                    ((IMultiChoiceAnswer)a).ChosenItems.ForEach(c => result.Add(c.Display));
                }
                else
                {
                    result.Add(a.AnswerAsString);
                }
            });

            return result;
        }

        public IEnumerable<string> GetDistinctCategories(IContextContainer context, WorkingDocumentGetter workingDoc)
        {
            return this.GetDistinctCategories(context, workingDoc.GetWorkingDocument(context));
        }

        public IEnumerable<IQuestion> FindQuestionsForCategory(IContextContainer context, WorkingDocumentGetter workingDoc, string text)
        {
            return this.FindQuestionsForCategory(context, workingDoc.GetWorkingDocument(context), text);
        }

        public IEnumerable<IAnswer> FindAnswersForCategory(IContextContainer context, WorkingDocumentGetter workingDoc, string text)
        {
            return this.FindAnswersForCategory(context, workingDoc.GetWorkingDocument(context), text);
        }

        public IEnumerable<string> FindAnswersValuesForCategory(IContextContainer context, WorkingDocumentGetter workingDoc, string text)
        {
            return this.FindAnswersValuesForCategory(context, workingDoc.GetWorkingDocument(context), text);
        }

        public IEnumerable<string> GetDistinctCategories(IContextContainer context)
        {
            return this.GetDistinctCategories(context, context.Get<IWorkingDocument>());
        }

        public IEnumerable<IQuestion> FindQuestionsForCategory(IContextContainer context, string text)
        {
            return this.FindQuestionsForCategory(context, context.Get<IWorkingDocument>(), text);
        }

        public IEnumerable<IAnswer> FindAnswersForCategory(IContextContainer context, string text)
        {
            return this.FindAnswersForCategory(context, context.Get<IWorkingDocument>(), text);
        }

        public IEnumerable<string> FindAnswersValuesForCategory(IContextContainer context, string text)
        {
            return this.FindAnswersValuesForCategory(context, context.Get<IWorkingDocument>(), text);
        }

    }
}
