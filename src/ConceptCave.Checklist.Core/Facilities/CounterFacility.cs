﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;

namespace ConceptCave.Checklist.Facilities
{
    /// <summary>
    /// A Facility that offers a simple means to manage counters.
    /// 
    /// A typical useage of this facility would be to offer an item number when a single present statement is presenting multiple items. So for example,
    /// if there are 5 sections on a page, rather than giving each a prompt of section 1, section 2 etc, you can go section #{counter.somename.value} then if
    /// the sections are moved around the numbering remains consistent without manually having to re-adjust them.
    /// </summary>
    public class CounterFacility : Facility, IObjectGetter
    {
        public class Counter : Node
        {
            protected int _value;

            public int Value
            {
                get
                {
                    _value++;
                    return _value;
                }
                set
                {
                    _value = value;
                }
            }

            public int ValueNoIncrement
            {
                get
                {
                    return _value;
                }
            }

            public void Reset()
            {
                Value = 0;
            }

            public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
            {
                base.Encode(encoder);

                encoder.Encode<int>("Value", Value);
            }

            public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
            {
                base.Decode(decoder);

                Value = decoder.Decode<int>("Value");
            }
        }

        protected Dictionary<string, Counter> _counters;

        public CounterFacility()
        {
            Name = "Counter";
            _counters = new Dictionary<string, Counter>();
        }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode<Dictionary<string, Counter>>("Counters", _counters);
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            _counters = decoder.Decode<Dictionary<string, Counter>>("Counters");
        }

        /// <summary>
        /// Allows the counter facility to expose its counters through dot notation to Lingo and string expansion.
        /// 
        /// The facility will create a new counter if there isn't one of the same name. The counter will get inremented
        /// as a part of it being accessed. Counters start at 1.
        /// </summary>
        public bool GetObject(IContextContainer context, string name, out object result)
        {
            Counter c = null;
            string n = name.ToLower();

            if (_counters.TryGetValue(n, out c) == false)
            {
                c = new Counter();

                _counters.Add(n, c);
            }

            result = c;

            return true;
        }
    }
}
