﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConceptCave.Checklist.Reporting.LingoQuery
{
    public class LingoQuerySelectedItem
    {
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public object Value { get; set; }

        public LingoQuerySelectedItem() { }
        public LingoQuerySelectedItem(string name, string displayName, object value)
        {
            Name = name;
            DisplayName = displayName;
            Value = value;
        }
    }
}
