﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.Lingo.ObjectProviders;
using ConceptCave.Core;
using ConceptCave.Core.Coding;

namespace ConceptCave.Checklist.Reporting.LingoQuery
{
    public class LingoDatatable : Node,  IEnumerable<LingoDatatableRecord>
    {
        private IList<LingoDatatableRecord> records;
        public LingoDatatableColumnMappings DisplayNameMappings { get; private set; }
        public IList<string> ColumnNames { get; private set; }

        public LingoDatatable() { }

        public LingoDatatable(IQueryable<LingoQuerySelectedItem[]> resultset, IList<string> columnNames, IDictionary<string, string> displayNames)
        {
            //This is the constructor for concrete executed queries.
            //We enumerate the resultset and pull out one record per resultset item
            records = new List<LingoDatatableRecord>();
            foreach (var result in resultset)
            {
                //the record doesn't care about the display name; strip it out
                var kvp = new Dictionary<string, object>();
                foreach (var item in result)
                    kvp.Add(item.Name, item.Value);
                records.Add(new LingoDatatableRecord(columnNames, kvp));
            }

            
            //Set up our display name mappings
            ColumnNames = new List<string>(columnNames);
            DisplayNameMappings = new LingoDatatableColumnMappings();
            foreach (var c in ColumnNames)
                DisplayNameMappings.SetColumnMapping(c, displayNames[c]);
        }

        public int Count
        {
            get
            {
                return records.Count;
            }
        }

        public IEnumerator<LingoDatatableRecord> GetEnumerator()
        {
            foreach (var rec in records)
                yield return rec;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public LingoDatatable GroupByCount(int columnIndex)
        {
            var groupedData = records.GroupBy(r => r.GetColumn(columnIndex),
                                              r => r,
                                              (key, g) => new LingoQuerySelectedItem[]
                                                  {
                                                      new LingoQuerySelectedItem("Name", "Name", key),
                                                      new LingoQuerySelectedItem("Count", "Count", g.Count())
                                                  });
            var names = new string[] { "Name", "Count" };
            var displayDict = new Dictionary<string, string>();
            foreach (var n in names)
                displayDict.Add(n, n);
            return new LingoDatatable(groupedData.AsQueryable(), names.ToList(), displayDict);
        }

        /// <summary>
        /// This method transforms a table around a rotation column. The end result is grouped on the unique values in all other columns outside of the rotation and value columns.
        /// The values seen in the header column are used to create columns in the output table. The value column then populates the data. So for example, given
        /// 
        /// id  Question    value
        /// 2   name        fred
        /// 2   age         16
        /// 2   gender      male
        /// 3   name        john
        /// 3   age         23
        /// 3   gender      male
        /// 
        /// providing the question column was the rotation column and the value column was the valueColumn the result would be
        /// 
        /// id  name    age gender
        /// 2   fred    16  male
        /// 3   john    23  male
        /// </summary>
        /// <param name="rotateAround">Column who's values will be used to create new columns in the result</param>
        /// <param name="valueColumn">Column that will be used to populate the columns created by the rotateAround column</param>
        /// <returns>A new LingoDataTable which is the result of the transform.</returns>
        public LingoDatatable TransformAroundColumn(string rotateAround, string valueColumn)
        {
            if (string.IsNullOrEmpty(rotateAround) == true || string.IsNullOrEmpty(valueColumn))
            {
                return null;
            }

            if (ColumnNames.Contains(rotateAround) == false || ColumnNames.Contains(valueColumn) == false)
            {
                return null;
            }

            List<string> otherColumns = (from c in ColumnNames where c != rotateAround && c != valueColumn select c).ToList();

            // a function to build us a sort of unique value based on the other columns that don't form part of the two we are interested in
            Func<LingoDatatableRecord, string> getValuesForOtherColumns = (r) => {
                string result = string.Empty;

                otherColumns.ForEach(c => {
                    var v = r.GetColumnValue(c);

                    if(v == null)
                    {
                        result += "null";
                    }
                    else{
                        result += v.ToString();
                    }
                });

                return result;
            };

            // first the grouped stuff, Each of the groups will end up being a record for us in the final output
            var groupedData = (from r in records group r by getValuesForOtherColumns(r) into g select new { Key = g.Key, Items = g.ToList() });

            // now the unique values seen in headerColumn, each of these will be rotated to be a column in the final output
            var newColumns = (from r in records select r.GetColumnValue(rotateAround).ToString()).Distinct();

            Dictionary<string, string> displayNames = new Dictionary<string,string>();
            otherColumns.ForEach(c => {
               displayNames.Add(c, DisplayNameMappings.GetColumnMapping(c));
            });

            newColumns.ForEach(c => {
                displayNames.Add(c, c);
            });

            List<LingoQuerySelectedItem[]> rows = new List<LingoQuerySelectedItem[]>();

            groupedData.ForEach(g =>
            {
                // the fixed columns first
                List<LingoQuerySelectedItem> rowData = new List<LingoQuerySelectedItem>();
                foreach (var fixedColumn in otherColumns)
                {
                    // since the Items in g are grouped on these columns, we only need to look at the first item, all entries in items will have the 
                    // same values for these columns (its how they're grouped after all).
                    LingoQuerySelectedItem item = new LingoQuerySelectedItem(fixedColumn, displayNames[fixedColumn], g.Items[0].GetColumnValue(fixedColumn));

                    rowData.Add(item);
                }

                // and now the columns taken from the values of our rotateAround column
                foreach (var rotateColumn in newColumns)
                {
                    // so we need to select out the row that matches the column we are wanting to map to a column
                    var row = (from i in g.Items where i.GetColumnValue(rotateAround).ToString() == rotateColumn select i).DefaultIfEmpty(null).First();

                    if (row == null)
                    {
                        rowData.Add(new LingoQuerySelectedItem(rotateColumn, rotateColumn, null));
                    }

                    rowData.Add(new LingoQuerySelectedItem(rotateColumn, rotateColumn, row.GetColumnValue(valueColumn)));
                }

                rows.Add(rowData.ToArray());
            });

            otherColumns.AddRange((from c in newColumns select c));

            return new LingoDatatable(rows.AsQueryable(), otherColumns.ToList(), displayNames); 
        }

        public LingoDatatableRecord GetOneRowWhereEquals(string columnName, object value)
        {
            return GetRowsWhereEquals(columnName, value).DefaultIfEmpty(null).First();
        }

        public IEnumerable<LingoDatatableRecord> GetRowsWhereEquals(string columnName, object value)
        {
            var colIndex = ColumnNames.IndexOf(columnName) + 1;
            return records.Where(r => r.GetColumn(colIndex).Equals(value));
        }

        public object ColumnValues(string columnName)
        {
            if (records.Count == 0)
            {
                return new List<object>();
            }
            // since we need to be able to use this in the contains operator of a Lingo Query, we have to
            // return a List<T> however T is based on that type that is in columnName, so we need to discover
            // what type is in that column and build List<T> at runtime.

            // first up discover what type we are
            Type ct = null;
            foreach(var r in records)
            {
                var val = r.GetColumnValue(columnName);

                if (val != null)
                {
                    ct = val.GetType();
                    break;
                }
            };

            Type generic = typeof(List<>);
            Type listType = generic.MakeGenericType(ct);

            IList values = (IList)Activator.CreateInstance(listType);

            records.ForEach(r =>
            {
                values.Add(r.GetColumnValue(columnName));
            });

            return values;
        }

        public override void Encode(IEncoder encoder)
        {
            encoder.Encode<List<LingoDatatableRecord>>("records", records as List<LingoDatatableRecord>);
            encoder.Encode<LingoDatatableColumnMappings>("DisplayNameMappings", DisplayNameMappings);
        }

        public override void Decode(IDecoder decoder)
        {
            records = decoder.Decode<List<LingoDatatableRecord>>("records");
            DisplayNameMappings = decoder.Decode<LingoDatatableColumnMappings>("DisplayNameMappings");
        }

        /// <summary>
        /// Creates a Lingo Data table from a list object provider. The method expects the list to be made up of either other lists or dictionary object providers
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static LingoDatatable TableFromEnumerable(IEnumerable source)
        {
            List<LingoQuerySelectedItem[]> rows = new List<LingoQuerySelectedItem[]>();
            List<string> columnNames = new List<string>();
            Dictionary<string, string> displayNames = new Dictionary<string, string>();

            Func<object, LingoQuerySelectedItem[]> getValue = null;

            // first up build up our columns, we use the first element in the list to dictate what columns we are going to have
            foreach (var item in source)
            {
                if (item is DictionaryObjectProvider)
                {
                    ((DictionaryObjectProvider)item).Properties.ForEach(i => {
                        columnNames.Add(i.ToString());
                        displayNames.Add(i.ToString(), i.ToString());
                    });

                    // create our function to get items out the source, which in this case is a Dictionary Object Provider
                    getValue = (v) =>
                    {
                        DictionaryObjectProvider prov = (DictionaryObjectProvider)v;
                        List<LingoQuerySelectedItem> result = new List<LingoQuerySelectedItem>();

                        foreach (var prop in prov.Properties)
                        {
                            LingoQuerySelectedItem i = new LingoQuerySelectedItem(prop.ToString(), prop.ToString(), prov.GetValue(null, prop.ToString()));

                            result.Add(i);
                        }

                        return result.ToArray();
                    };

                }
                else if (item is ListObjectProvider)
                {
                    ((ListObjectProvider)item).ForEach(i =>
                    {
                        columnNames.Add(i.ToString());
                        displayNames.Add(i.ToString(), i.ToString());
                    });

                    getValue = (v) =>
                    {
                        ListObjectProvider prov = (ListObjectProvider)v;
                        List<LingoQuerySelectedItem> result = new List<LingoQuerySelectedItem>();

                        int i = 0;
                        foreach (var prop in prov)
                        {
                            LingoQuerySelectedItem field = new LingoQuerySelectedItem(i.ToString(), i.ToString(), prop);

                            result.Add(field);

                            i++;
                        }

                        return result.ToArray();
                    };
                }

                break;
            }

            if (columnNames.Count == 0)
            {
                return null; // its not anything we know how to process
            }

            foreach (var item in source)
            {
                rows.Add(getValue(item));
            }

            return new LingoDatatable(rows.AsQueryable(), columnNames, displayNames);
        }
    }

    public class LingoDatatableColumnMappings : Node, IObjectSetter
    {
        private IDictionary<string, string> mappings = new Dictionary<string, string>();

        public int Count { get { return mappings.Count; } }

        public void SetColumnMapping(string name, string displayName)
        {
            SetObject(null, name, displayName);
        }

        public string GetColumnMapping(string name)
        {
            object outvar;
            GetObject(null, name, out outvar);
            return (string)outvar;
        }

        public void RemoveColumnMapping(string name)
        {
            mappings.Remove(name);
        }

        public bool SetObject(IContextContainer context, string name, object value)
        {
            try
            {
                var strVal = (string)value;
                mappings[name] = strVal;
                return true;
            }
            catch (KeyNotFoundException)
            {
                mappings.Add(name, (string)value);
                return true;
            }
            catch (InvalidCastException)
            {
                return false;
            }
        }

        public bool GetObject(IContextContainer context, string name, out object result)
        {
            try
            {
                result = mappings[name];
                return true;
            }
            catch (KeyNotFoundException)
            {
                result = null;
                return false;
            }
        }

        public override void Decode(IDecoder decoder)
        {
            base.Decode(decoder);
            mappings = decoder.Decode<Dictionary<string, string>>("mappings");
        }

        public override void Encode(IEncoder encoder)
        {
            base.Encode(encoder);
            encoder.Encode<Dictionary<string, string>>("mappings", mappings as Dictionary<string, string>);
        }
    }

    public class LingoDatatableRecord : Node, IObjectSetter
    {
        private IDictionary<string, object> columns = new Dictionary<string, object>();
        internal IDictionary<int, string> indexMappings = new Dictionary<int, string>();

        public LingoDatatableRecord(IList<string> columnNameOrder, IDictionary<string, object> columnArgs)
        {
            int i = 1;
            foreach (var colk in columnNameOrder)
            {
                var key = colk;
                var value = columnArgs[key];
                columns.Add(key, value);
                indexMappings.Add(i, key);
                i++;
            }
        }

        public LingoDatatableRecord() { }

        public bool SetObject(IContextContainer context, string name, object value)
        {
            try
            {
                columns[name] = value;
            }
            catch (KeyNotFoundException)
            {
                columns.Add(name, value);
                indexMappings.Add(columns.Count, name);
            }
            return true;
        }

        public bool GetObject(IContextContainer context, string name, out object result)
        {
            result = null;
            try
            {
                result = columns[name];
                return true;
            }
            catch (KeyNotFoundException)
            {
                return false;
            }
        }

        public object GetColumn(int index)
        {
            var key = indexMappings[index];
            return GetColumn(key);
        }

        public object GetColumn(string name)
        {
            object ret;
            GetObject(null, name, out ret);
            return ret;
        }

        public void SetColumn(int index, object value)
        {
            var key = indexMappings[index];
            SetColumn(key, value);
        }

        public void SetColumn(string name, object value)
        {
            SetObject(null, name, value);
        }

        public int ColumnCount {
            get { return columns.Count; }
        }

        public object GetColumnValue(string column)
        {
            object value = null;

            GetObject(null, column, out value);

            return value;
        }

        public override void Decode(IDecoder decoder)
        {
            base.Decode(decoder);
            columns = decoder.Decode<Dictionary<string, object>>("columns");
            indexMappings = decoder.Decode<IDictionary<int, string>>("indexMappings");
        }

        public override void Encode(IEncoder encoder)
        {
            base.Encode(encoder);
            encoder.Encode<Dictionary<string, object>>("columns", columns as Dictionary<string, object>);
            encoder.Encode("indexMappings", indexMappings);
        }
    }
}
