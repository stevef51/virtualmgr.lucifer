﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;

namespace ConceptCave.Checklist.Reporting.LingoQuery
{
    public class LingoDatabaseColumn : ILingoDataColumn
    {
        public LingoDatabaseColumn()
        {
            SubColumns = new Dictionary<string, ILingoDataColumn>();
        }

        public string Name
        {
            get;
            set;
        }

        public Type EntityType
        {
            get;
            set;
        }

        public Type DataType
        {
            get { throw new NotImplementedException(); }
        }

        public Expression LinqReference(ParameterExpression pe)
        {
            return LinqReferenceGenerator(pe);
        }

        public bool GetObject(IContextContainer context, string name, out object result)
        {
            if (!SubColumns.Keys.Contains(name))
            {
                result = null;
                return false;
            }
            else
            {
                result = SubColumns[name];
                return true;
            }
        }

        public Func<ParameterExpression, Expression> LinqReferenceGenerator { get; set; }
        public Dictionary<string, ILingoDataColumn> SubColumns { get; private set; }
    }
}
