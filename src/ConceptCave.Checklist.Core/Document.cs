﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Core.Coding;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.Editor;

namespace ConceptCave.Core
{
    public class Document : Node, IDocument, IResource, IHasDisplayName
    {
        public ICommandManager CommandManager { get; private set; }

        public string Name { get; set; }
        public string DisplayName { get { return LocalisableDisplayName.CurrentCultureValue; } set { LocalisableDisplayName.CurrentCultureValue = value; } }
        public ILocalisableString LocalisableDisplayName { get; private set; }

        public Document()
        {
            CommandManager = new CommandManager(this);
            LocalisableDisplayName = new LocalisableString();
        }

        public override void Encode(IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("Name", Name);
            encoder.Encode("CommandManager", CommandManager);
            encoder.Encode("LocalisableDisplayName", LocalisableDisplayName);
        }

        public override void Decode(IDecoder decoder)
        {
            base.Decode(decoder);

            Name = decoder.Decode<string>("Name");
            CommandManager = decoder.Decode<CommandManager>("CommandManager");
            LocalisableDisplayName = LocalisableString.DecodeWithDefault(decoder, "DisplayName", Name);
        }
    }
}
