﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConceptCave.Checklist
{
    public class NumberWithBase
    {
        public static readonly NumberWithBase Base10 = new NumberWithBase("0123456789");
        public static readonly NumberWithBase BaseAlpha = new NumberWithBase("abcdefghijklmnopqrstuvwxyz");

        public string Digits { get; private set; }

        public NumberWithBase(string digits)
        {
            Digits = digits;
        }

        public string AsString(int number)
        {
            string s = "";
            do
            {
                s = (Digits[number % Digits.Length]) + s;
                number = number / Digits.Length;
            } while (number != 0);
            return s;
        }

        public int FromString(string s)
        {
            int number = 0;
            foreach (char c in s.Reverse())
            {
                number += Digits.IndexOf(c);
                number *= Digits.Length;
            }
            return number;
        }
    }
}
