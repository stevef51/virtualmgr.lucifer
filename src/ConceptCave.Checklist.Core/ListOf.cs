﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace ConceptCave.Core
{
    public class ListOf<TItem> : Node, IListOf<TItem>, IEnumerable<TItem>, IList<TItem>, ICollection<TItem>, IList, ICollection, IEnumerable
    {
        private List<TItem> _items;

        #region IListOf<TItem> Members

        public List<TItem> Items
        {
            get { return _items; }
        }

        #endregion

        public ListOf()
        {
            _items = new List<TItem>();
        }

        public ListOf(int capacity)
        {
            _items = new List<TItem>(capacity);
        }

        public ListOf(IEnumerable<TItem> collection)
        {
            _items = new List<TItem>(collection);
        }

        public override void Decode(Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            _items = decoder.Decode<List<TItem>>("Items", null);
        }

        public override void Encode(Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("Items", _items);
        }

        public IEnumerator<TItem> GetEnumerator()
        {
            return _items.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _items.GetEnumerator();
        }

        public override string ToString()
        {
            return string.Join(",", this);
        }

        public string ToString(string separator)
        {
            return string.Join(separator, this);
        }

        public int IndexOf(TItem item)
        {
            return _items.IndexOf(item);
        }

        public void Insert(int index, TItem item)
        {
            _items.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            _items.RemoveAt(index);
        }

        public TItem this[int index]
        {
            get
            {
                return _items[index];
            }
            set
            {
                _items[index] = value;
            }
        }

        public void Add(TItem item)
        {
            _items.Add(item);
        }

        public void Clear()
        {
            _items.Clear();
        }

        public bool Contains(TItem item)
        {
            return _items.Contains(item);
        }

        public void CopyTo(TItem[] array, int arrayIndex)
        {
            _items.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return _items.Count; }
        }

        public bool IsReadOnly
        {
            get { return ((IList)_items).IsReadOnly; }
        }

        public bool Remove(TItem item)
        {
            return _items.Remove(item);
        }

        public void CopyTo(Array array, int index)
        {
            ((IList)_items).CopyTo(array, index);
        }

        public bool IsSynchronized
        {
            get { return ((IList)_items).IsSynchronized; }
        }

        public object SyncRoot
        {
            get { return ((IList)_items).SyncRoot; }
        }

        int IList.Add(object value)
        {
            return ((IList)_items).Add(value);
        }

        void IList.Clear()
        {
            ((IList)_items).Clear();
        }

        bool IList.Contains(object value)
        {
            return ((IList)_items).Contains(value);
        }

        int IList.IndexOf(object value)
        {
            return ((IList)_items).IndexOf(value);
        }

        void IList.Insert(int index, object value)
        {
            ((IList)_items).Insert(index, value);
        }

        bool IList.IsFixedSize
        {
            get { return ((IList)_items).IsFixedSize; }
        }

        bool IList.IsReadOnly
        {
            get { return ((IList)_items).IsReadOnly; }
        }

        void IList.Remove(object value)
        {
            ((IList)_items).Remove(value);
        }

        void IList.RemoveAt(int index)
        {
            ((IList)_items).RemoveAt(index);
        }

        object IList.this[int index]
        {
            get
            {
                return ((IList)_items)[index];
            }
            set
            {
                ((IList)_items)[index] = value;
            }
        }

        void ICollection.CopyTo(Array array, int index)
        {
            ((IList)_items).CopyTo(array, index);
        }

        int ICollection.Count
        {
            get { return ((IList)_items).Count; }
        }

        bool ICollection.IsSynchronized
        {
            get { return ((IList)_items).IsSynchronized; }
        }

        object ICollection.SyncRoot
        {
            get { return ((IList)_items).SyncRoot; }
        }
    }
}
