﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;

namespace ConceptCave.Checklist
{
    public static class ListExtensions
    {
        public static IAnswer AnswerToQuestion(this List<IAnswer> list, object questionId, int sequence, string section = null)
        {
            IEnumerable<IAnswer> query = null;
            if (questionId is int)
                query = list.Where(a => a != null && a.PresentedQuestion.Question.Index() == (int)questionId && a.PresentedQuestion.Sequence == sequence);
            else if (questionId is string)
                query = list.Where(a => a != null && string.Compare(a.PresentedQuestion.NameAs, (string)questionId, true) == 0 && a.PresentedQuestion.Sequence == sequence);
            else if (questionId is Guid)
                query = list.Where(a => a != null && a.PresentedQuestion.Question.Id == (Guid)questionId && a.PresentedQuestion.Sequence == sequence);
            else
            {
                // ok so questionId isn't an index and it isn't the name of the question. So far we've seen this when questionId is actually the answervalue due to something higher up
                // having already resolved the value. We need to review things here, but for the moment, assume that questionId is an answervalue and use that to get the index

                query = list.Where(a => a != null && a.AnswerValue != null && a.AnswerValue.Equals(questionId) == true && a.PresentedQuestion.Sequence == sequence);
            }

            // Optional filter by section parent name
            if (section != null)
            {
                section = section.ToLower();
                query = query.Where(a =>
                {
                    var s = (a.PresentedQuestion.Parent as IPresentedSection);
                    while (s != null)
                    {
                        if (s.Section.Name.ToLower() == section)
                            return true;
                        s = s.Parent as IPresentedSection;
                    }
                    return false;
                });
            }

            return query.FirstOrDefault();
        }

        public static IEnumerable<IAnswer> AnswersWithName(this List<IAnswer> list, object questionId)
        {
            if (questionId is int)
                return list.Where(a => a != null && a.PresentedQuestion.Question.Index() == (int)questionId);
            else
                return list.Where(a => a != null && string.Compare(a.PresentedQuestion.NameAs, (string)questionId, true) == 0);
        }

        public static int AnswerCount(this List<IAnswer> list, object questionId, string section = null)
        {
            IEnumerable<IAnswer> query = null;
            if (questionId is int)
                query = list.Where(a => a != null && a.PresentedQuestion.Question.Index() == (int)questionId);
            else if (questionId is string)
                query = list.Where(a => a != null && string.Compare(a.PresentedQuestion.NameAs, (string)questionId, true) == 0);
            else if (questionId is Guid)
                query = list.Where(a => a != null && a.PresentedQuestion.Question.Id == (Guid)questionId);
            else
            {
                // ok so questionId isn't an index and it isn't the name of the question. So far we've seen this when questionId is actually the answervalue due to something higher up
                // having already resolved the value. We need to review things here, but for the moment, assume that questionId is an answervalue and use that to get the index

                query = list.Where(a => a != null && a.AnswerValue != null && a.AnswerValue.Equals(questionId) == true);
            }

            // Optional filter by section parent name
            if (section != null)
            {
                section = section.ToLower();
                query = query.Where(a =>
                {
                    var s = (a.PresentedQuestion.Parent as IPresentedSection);
                    while (s != null)
                    {
                        if (s.Section.Name.ToLower() == section)
                            return true;
                        s = s.Parent as IPresentedSection;
                    }
                    return false;
                });
            }

            return query.Count();
        }

        public static void ForEach<T>(this IEnumerable<T> list, Action<T> action)
        {
            foreach (T t in list)
                action(t);
        }
    }
}
