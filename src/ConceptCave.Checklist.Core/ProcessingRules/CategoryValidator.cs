﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Validators;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.ProcessingRules;
using ConceptCave.Checklist.RunTime;

namespace ConceptCave.Checklist.Validators
{
    public class CategoryValidator : ValidatorWithMessage
    {
        /// <summary>
        /// Questions marked in this category will be grouped together and this validator applied to them
        /// </summary>
        public string Category { get; set; }
        /// <summary>
        /// The number of questions in the category that must have a value for this validator to pass
        /// </summary>
        public int MandatoryCount { get; set; }

        public CategoryValidator()
        {
            MandatoryCount = 1;
        }

        public override void doProcess(ConceptCave.Checklist.Interfaces.IProcessingRuleContext context)
        {
            if (string.IsNullOrEmpty(Category) == true)
            {
                return;
            }

            if (MandatoryCount <= 0)
            {
                return;
            }

            WorkingDocument workingDoc = (WorkingDocument)context.Context.Get<IWorkingDocument>();

            IPresentedSection section = (from s in workingDoc.PresentedAsDepthFirstEnumerable() where s is IPresentedSection from p in s.AsDepthFirstEnumerable() where p is IPresentedQuestion && p == context.Answer.PresentedQuestion select s).Cast<IPresentedSection>().DefaultIfEmpty(null).First();

            if (section == null)
            {
                return; // should never happen
            }

            var presenteds = (from a in section.AsDepthFirstEnumerable() where a is IPresentedQuestion from v in ((IPresentedQuestion)a).Question.Validators where v is CategoryRule && ((CategoryRule)v).Text == Category select a).Cast<IPresentedQuestion>();

            int count = 0;
            foreach (var p in presenteds)
            {
                var answer = p.GetAnswer();
                if (answer.AnswerValue == null)
                {
                    continue;
                }

                if (string.IsNullOrEmpty(answer.AnswerAsString) == true)
                {
                    continue;
                }
                count++;
            }

            if (count < MandatoryCount)
            {
                IDisplayIndexFormatter formatter = context.Context.Get<IDisplayIndexFormatter>();

                foreach(var p in presenteds)
                {
                    if (string.IsNullOrEmpty(ErrorMessage))
                    {
                        context.ValidatorResult.AddInvalidReason(string.Format("At least {0} of these must be answered", MandatoryCount, p.DisplayIndex.Format(formatter)), p.GetAnswer());
                    }
                    else
                    {
                        context.ValidatorResult.AddInvalidReason(ErrorMessage, p.GetAnswer());
                    }
                }
            }
        }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("Category", Category);
            encoder.Encode("MandatoryCount", MandatoryCount);
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            Category = decoder.Decode<string>("Category");
            MandatoryCount = decoder.Decode<int>("MandatoryCount", 1);
        }
    }
}
