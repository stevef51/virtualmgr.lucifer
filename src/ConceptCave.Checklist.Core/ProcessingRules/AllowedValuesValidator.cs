﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core.Coding;
using Newtonsoft.Json.Linq;
using ConceptCave.Checklist.Reporting.LingoQuery;
using ConceptCave.Checklist.Lingo.ObjectProviders;

namespace ConceptCave.Checklist.Validators
{
    public class AllowedValuesValidator : ValidatorWithMessage
    {
        public List<string> AllowedValues { get; set; }
        public AllowedValuesValidator() : base()
        {
            ErrorMessage = "The value entered is not valid";
            AllowedValues = new List<string>();
        }

        public override void Encode(IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("AllowedValues", AllowedValues);
        }

        public override void Decode(IDecoder decoder)
        {
            base.Decode(decoder);

            AllowedValues = decoder.Decode<List<string>>("AllowedValues");
        }

        public override void doProcess(IProcessingRuleContext context)
        {
            if(AllowedValues.Count == 0)
            {
                return;
            }

            var val = context.Answer.AnswerAsString;

            if((from a in AllowedValues where a == val select a).Count() == 0)
            {
                IDisplayIndexFormatter formatter = context.Context.Get<IDisplayIndexFormatter>();
                context.ValidatorResult.AddInvalidReason(String.Format(ErrorMessage, context.Answer.PresentedQuestion.DisplayIndex.Format(formatter)), context.Answer);
            }
        }

        public void AddAllowedValue(object val)
        {
            AddAllowedValue(val, null);
        }

        public void AddAllowedValue(object val, string column)
        {
            if (val is string)
            {
                AllowedValues.Add((string)val);
            }
            else if (val is LingoDatatable)
            {
                LingoDatatable table = (LingoDatatable)val;

                table.ForEach(r => {
                    var rv = r.GetColumnValue(column);

                    AddAllowedValue(rv, null);
                });
            }
            else if(val is ListObjectProvider)
            {
                ListObjectProvider l = (ListObjectProvider)val;
                l.ForEach(r => {
                    if(r is DictionaryObjectProvider)
                    {
                        AddAllowedValue(r, column);
                    }
                });
            }
            else if (val is DictionaryObjectProvider)
            {
                DictionaryObjectProvider p = (DictionaryObjectProvider)val;
                AddAllowedValue(p.GetValue(null, column), column);
            }
            else
            {
                if(val == null)
                {
                    return;
                }

                AllowedValues.Add(val.ToString());
            }
        }

        public void ClearAllowedValues()
        {
            AllowedValues = new List<string>();
        }

        public override JObject ToJson()
        {
            JObject result = new JObject();

            JArray l = new JArray();

            AllowedValues.ForEach(a => l.Add(a));

            result["message"] = ErrorMessage;
            result["values"] = l;

            return result;
        }
    }
}
