﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Core;
using ConceptCave.Checklist.Interfaces;
using Newtonsoft.Json.Linq;
using ConceptCave.Core.Coding;

namespace ConceptCave.Checklist.Validators
{
    public abstract class Validator : Node, IProcessingRule
    {
        public string Name { get; set; }

        public virtual int Order
        {
            get
            {
                return 10;
            }
        }

        public virtual void Process(IProcessingRuleContext context)
        {
            if (context.Direction == ProcessDirection.Backward)
            {
                return;
            }

            doProcess(context);
        }

        public abstract void doProcess(IProcessingRuleContext context);

        public override void Encode(IEncoder encoder)
        {
            base.Encode(encoder);
            encoder.Encode("Name", Name);
        }

        public override void Decode(IDecoder decoder)
        {
            base.Decode(decoder);
            Name = decoder.Decode<string>("Name");
        }

        public virtual JObject ToJson()
        {
            return null;
        }
    }

    public abstract class ValidatorWithMessage : Validator
    {
        public string ErrorMessage { get; set; }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("ErrorMessage", ErrorMessage);
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            string message = decoder.Decode<string>("ErrorMessage");
            if(string.IsNullOrEmpty(message) == false)
            {
                ErrorMessage = message;
            }
        }
    }
}
