﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using Newtonsoft.Json.Linq;

namespace ConceptCave.Checklist.Validators
{
    public class DateRangeValidator : Validator
    {
        public DateTime? MinimumValue { get; set; }
        public DateTime? MaximumValue { get; set; }

        public override void doProcess(IProcessingRuleContext context)
        {
            if (context.Answer.AnswerValue == null)
                return;

            if (context.Answer.AnswerValue is DateTime)
            {
                DateTime number = (DateTime)context.Answer.AnswerValue;

                if (MinimumValue.HasValue && number < MinimumValue)
                    context.ValidatorResult.AddInvalidReason(string.Format("must be greater than or equal {1}", context.Answer.PresentedQuestion.DisplayIndex, MinimumValue), context.Answer);

                if (MaximumValue.HasValue && number > MaximumValue)
                    context.ValidatorResult.AddInvalidReason(string.Format("must be less than or equal {1}", context.Answer.PresentedQuestion.DisplayIndex, MaximumValue), context.Answer);
            }
        }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("MinimumValue", MinimumValue);
            encoder.Encode("MaximumValue", MaximumValue);
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            MinimumValue = decoder.Decode<DateTime?>("MinimumValue");
            MaximumValue = decoder.Decode<DateTime?>("MaximumValue");
        }

        public override JObject ToJson()
        {
            JObject result = new JObject();

            if(MinimumValue.HasValue)
            {
                result["minimumdate"] = MinimumValue.Value.ToString("YYYY-MM-dd");
            }

            if (MaximumValue.HasValue)
            {
                result["maximumdate"] = MaximumValue.Value.ToString("YYYY-MM-dd");
            }

            return result;
        }
    }
}
