﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Validators;
using ConceptCave.Checklist.Interfaces;

namespace ConceptCave.Checklist.Validators
{
    public class GeoCodeDistanceRule : Validator
    {
        public bool UseSpecifiedLocation { get; set; }
        public ILatLng SpecifiedLocation { get; set; }

        public bool EnforceMandatory { get; set; }

        public decimal Radius { get; set; }

        public override void doProcess(IProcessingRuleContext context)
        {
            ILatLng latLng = SpecifiedLocation;

            if (UseSpecifiedLocation == false)
            {

            }

            if (latLng == null)
            {
                context.ValidatorResult.AddInvalidReason("No location is defined for the distance rule to be applied against", context.Answer);
                return;
            }

            ILatLng answerLatLng = ((IGeoLocationAnswer)context.Answer).Coordinates;
            decimal distance = latLng.MetresDistanceFrom(answerLatLng);

            if (distance > Radius)
            {
                if (EnforceMandatory == true)
                {
                    context.ValidatorResult.AddInvalidReason(string.Format("Distance from expected location {0} to measured location {1} is greater than {2}m", latLng, answerLatLng, Radius), context.Answer);
                }
                else
                {
                    context.Answer.PassFail = false;
                }
            }
        }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("UseSpecifiedLocation", UseSpecifiedLocation);
            encoder.Encode("SpecifiedLocation", SpecifiedLocation);
            encoder.Encode("EnforceMandatory", EnforceMandatory);
            encoder.Encode("Radius", Radius);
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            UseSpecifiedLocation = decoder.Decode<bool>("UseSpecifiedLocation");
            SpecifiedLocation = decoder.Decode<ILatLng>("SpecifiedLocation");
            EnforceMandatory = decoder.Decode<bool>("EnforceMandatory");
            Radius = decoder.Decode<decimal>("Radius");
        }
    }
}
