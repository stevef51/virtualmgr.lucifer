﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Validators;

namespace ConceptCave.Checklist.ProcessingRules
{
    /// <summary>
    /// Rule to mark a question with various properties read by the table formatting class
    /// so that it can adjust its output of the question.
    /// </summary>
    public class TableFormatterRule : Validator
    {
        public TableFormatterRule()
        {
            Visible = true;
        }

        /// <summary>
        /// If false the question won't be rendered by the table formatter.
        /// </summary>
        public bool Visible { get; set; }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            Visible = decoder.Decode<bool>("Visible");
        }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("Visible", Visible);
        }

        public override void doProcess(Interfaces.IProcessingRuleContext context)
        {
            // we don't do anything
        }
    }
}
