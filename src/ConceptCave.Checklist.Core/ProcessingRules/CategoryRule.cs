﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Validators;

namespace ConceptCave.Checklist.ProcessingRules
{
    public class CategoryRule : Validator
    {
        protected string _text;

        public string Text
        {
            get
            {
                return string.IsNullOrEmpty(_text) == true ? "No Category" : _text;
            }
            set
            {
                _text = value;
            }
        }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("Text", Text);
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            Text = decoder.Decode<string>("Text");
        }

        public override void doProcess(Interfaces.IProcessingRuleContext context)
        {
            // we don't do anything
        }
    }
}
