﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;

namespace ConceptCave.Checklist.Validators
{
    public class AnswerOrNoteValidator : ValidatorWithMessage
    {
        public AnswerOrNoteValidator()
        {
            ErrorMessage = "Either the item must be checked, or a note left to explain";
        }

        public override void doProcess(IProcessingRuleContext context)
        {
            IDisplayIndexFormatter formatter = context.Context.Get<IDisplayIndexFormatter>();
            try
            {
                if ((bool?)context.Answer.AnswerValue != true && (context.Answer.Notes == null || String.IsNullOrWhiteSpace(context.Answer.Notes.Text)))
                {
                    context.ValidatorResult.AddInvalidReason(String.Format(ErrorMessage, context.Answer.PresentedQuestion.DisplayIndex.Format(formatter)), context.Answer);
                }
            }
            catch (InvalidCastException) { }
        }
    }
}
