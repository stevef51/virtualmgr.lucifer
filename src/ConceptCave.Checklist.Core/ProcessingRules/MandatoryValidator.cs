﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using Newtonsoft.Json.Linq;

namespace ConceptCave.Checklist.Validators
{
    public class MandatoryValidator : ValidatorWithMessage
    {
        public MandatoryValidator()
        {
            ErrorMessage = "Question is mandatory";
        }

        public override void doProcess(IProcessingRuleContext context)
        {
            IDisplayIndexFormatter formatter = context.Context.Get<IDisplayIndexFormatter>();

            if (context.Answer == null)
            {
                // we are going to assume that validator added to a question that doesn't have an answer
                // isn't meant to be validated, but rather than freaking out, let's just move on with our
                // lives. This has been added to support the PresentationQuestion, which will do custom mandatory
                // validation on the client side only
                return;
            }

            if (context.Answer is IMandatoryValidatorOverride)
            {
                if (((IMandatoryValidatorOverride)context.Answer).IsValid() == false)
                {
                    context.ValidatorResult.AddInvalidReason(string.Format(ErrorMessage, context.Answer.PresentedQuestion.DisplayIndex.Format(formatter)), context.Answer);
                }
            }
            else
            {
                if (context.Answer.AnswerValue == null)
                {
                    context.ValidatorResult.AddInvalidReason(string.Format(ErrorMessage, context.Answer.PresentedQuestion.DisplayIndex.Format(formatter)), context.Answer);
                    return;
                }

                if (string.IsNullOrEmpty(context.Answer.AnswerAsString))
                    context.ValidatorResult.AddInvalidReason(string.Format(ErrorMessage, context.Answer.PresentedQuestion.DisplayIndex.Format(formatter)), context.Answer);
            }
        }

        public override JObject ToJson()
        {
            JObject result = new JObject();

            result["message"] = ErrorMessage;

            return result;
        }
    }
}
