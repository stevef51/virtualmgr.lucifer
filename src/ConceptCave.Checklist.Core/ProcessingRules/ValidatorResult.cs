﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;

namespace ConceptCave.Checklist.Validators
{
    public class ValidatorResult : Node, IValidatorResult
    {
        public class InvalidReason : Node, IInvalidReason
        {
            public string Reason { get; set; }
            public IAnswer Answer { get; set; }

            public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
            {
                base.Encode(encoder);

                encoder.Encode("Reason", Reason);
                encoder.Encode("Answer", Answer);
            }

            public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
            {
                base.Decode(decoder);

                Reason = decoder.Decode<string>("Reason");
                Answer = decoder.Decode<IAnswer>("Answer");
            }
        }

        public bool Invalid { get { return InvalidReasons.Count > 0; } }
        public List<IInvalidReason> InvalidReasons { get; private set; }

        public ValidatorResult()
        {
            InvalidReasons = new List<IInvalidReason>();
        }

        public override string ToString()
        {
            return string.Join("\n", InvalidReasons.Select(i => i.Reason));
        }


        #region IValidatorResult Members


        public void AddInvalidReason(string reason, IAnswer answer)
        {
            InvalidReasons.Add(new InvalidReason() { Reason = reason, Answer = answer });
        }

        public void AddInvalidReason(string reason)
        {
            InvalidReasons.Add(new InvalidReason() { Reason = reason });
        }

        #endregion

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            InvalidReasons = decoder.Decode<List<IInvalidReason>>("InvalidReasons");
        }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("InvalidReasons", InvalidReasons);   
        }
    }
}
