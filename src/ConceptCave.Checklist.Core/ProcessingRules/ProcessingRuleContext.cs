﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;

namespace ConceptCave.Checklist.ProcessingRules
{
    public class ProcessingRuleContext : IProcessingRuleContext
    {
        public IAnswer Answer
        {
            get;
            set;
        }

        public IValidatorResult ValidatorResult
        {
            get;
            set;
        }

        public ConceptCave.Core.IContextContainer Context
        {
            get;
            set;
        }

        public ProcessDirection Direction { get; set; }
    }
}
