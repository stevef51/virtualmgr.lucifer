﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Validators;
using ConceptCave.Checklist.Interfaces;
using System.Text.RegularExpressions;

namespace ConceptCave.Checklist.Validators
{
    public class RegularExpressionValidator : ValidatorWithMessage
    {
        public string ValidationExpression { get; set; }

        public RegularExpressionValidator()
        {
            ErrorMessage = "Answer is not in the correct format";
        }

        public override void doProcess(ConceptCave.Checklist.Interfaces.IProcessingRuleContext context)
        {
            if (string.IsNullOrEmpty(ValidationExpression) == true)
            {
                return;
            }

            IDisplayIndexFormatter formatter = context.Context.Get<IDisplayIndexFormatter>();

            if (string.IsNullOrEmpty(context.Answer.AnswerAsString) == true)
            {
                return;
            }

            if (Regex.IsMatch(context.Answer.AnswerAsString, ValidationExpression, RegexOptions.IgnoreCase) == false)
            {
                context.ValidatorResult.AddInvalidReason(string.Format(ErrorMessage, context.Answer.PresentedQuestion.DisplayIndex.Format(formatter)), context.Answer);
            }
        }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("ValidationExpression", ValidationExpression);
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            ValidationExpression = decoder.Decode<string>("ValidationExpression", null);
        }
    }
}
