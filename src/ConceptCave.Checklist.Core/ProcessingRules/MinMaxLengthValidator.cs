﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Validators;
using Newtonsoft.Json.Linq;

namespace ConceptCave.Checklist.Validators
{
    public class MinMaxLengthValidator : Validator
    {
        public int? MinLength { get; set; }
        public int? MaxLength { get; set; }

        public override void doProcess(Interfaces.IProcessingRuleContext context)
        {
            if (context.Answer.AnswerValue == null)
                return;

            if (MinLength.HasValue)
            {
                if (context.Answer.AnswerAsString.Length < MinLength.Value)
                {
                    context.ValidatorResult.AddInvalidReason(string.Format("Must be at least {1} characters long", context.Answer.PresentedQuestion.DisplayIndex, MinLength.Value), context.Answer);
                }
            }

            if (MaxLength.HasValue)
            {
                if (context.Answer.AnswerAsString.Length > MaxLength.Value)
                {
                    context.ValidatorResult.AddInvalidReason(string.Format("Cannot be longer than {1} characters", context.Answer.PresentedQuestion.DisplayIndex, MaxLength.Value), context.Answer);
                }
            }
        }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("MinLength", MinLength);
            encoder.Encode("MaxLength", MaxLength);
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            MinLength = decoder.Decode<int?>("MinLength");
            MaxLength = decoder.Decode<int?>("MaxLength");
        }

        public override JObject ToJson()
        {
            JObject result = new JObject();

            if(MinLength.HasValue)
            {
                result["minlength"] = MinLength.Value;
            }

            if(MaxLength.HasValue)
            {
                result["maxlength"] = MaxLength.Value;
            }

            return result;
        }
    }
}
