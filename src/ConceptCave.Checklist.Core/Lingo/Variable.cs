﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Core.Coding;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;

namespace ConceptCave.Checklist.Lingo
{
    /// <summary>
    /// A simple Variable wrapper, meant to wrap primitive types that can be referenced in Commands etc
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Variable<T> : Node, IVariable<T>
    {
        public string Name { get; private set; }
        public object ObjectValue { get { return Value; } set { Value = (T)value; } }
        public virtual T Value { get; set; }

        public Variable() { }

        public Variable(string name)
        {
            Name = name;
        }

        public override void Encode(IEncoder encoder)
        {
            base.Encode(encoder);
            encoder.Encode("Name", Name);
            encoder.Encode("Value", Value);
        }

        public override void Decode(IDecoder decoder)
        {
            base.Decode(decoder);
            Name = decoder.Decode<string>("Name", null);
            Value = decoder.Decode<T>("Value", default(T));
        }

        public override string ToString()
        {
            return string.Format("{0} = {1}", Name, Value);
        }
    }
}
