﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using Irony.Parsing;
using ConceptCave.Core;
using Irony.Ast;

namespace ConceptCave.Checklist.Lingo
{
    public class GetAttributeAst : LingoAst, IEvaluatable
    {
        public IEvaluatable Identifier;
        public IEvaluatable AttributeName;

        public override void Init(AstContext context, Irony.Parsing.ParseTreeNode parseTreeNode)
        {
            base.Init(context, parseTreeNode);

            Identifier = AddChild(parseTreeNode.ChildNodes[0]) as IEvaluatable;
            AttributeName = AddChild(parseTreeNode.ChildNodes[2]) as IEvaluatable;
        }

        public object Evaluate(ConceptCave.Core.IContextContainer context)
        {
            throw new LingoException("The GetAttribute syntax is deprecated (maybe?)");
        }
    }
}
