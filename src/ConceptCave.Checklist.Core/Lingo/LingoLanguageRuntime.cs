﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Irony.Interpreter;
using Irony.Parsing;
using System.Numerics;
using Irony;
using System.Linq.Expressions;
using ConceptCave.Checklist.Editor;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.Lingo.Functions;
using ConceptCave.Core;

namespace ConceptCave.Checklist.Lingo
{
    public class LingoLanguageRuntime : LanguageRuntime
    {
        public Dictionary<string, ICallTargetFactory> FunctionCallTargets = new Dictionary<string, ICallTargetFactory>(StringComparer.InvariantCultureIgnoreCase);

        static TypeList _typesSequence = new TypeList(
            typeof(sbyte), typeof(Int16), typeof(Int32), typeof(Int64), typeof(BigInteger), // typeof(Rational)
            typeof(Single), typeof(Double), typeof(Decimal), typeof(DateTime), typeof(Complex),               // Decimal is not defined in LanguageRuntime
            typeof(bool), typeof(string), typeof(object[])
        );

        public LingoLanguageRuntime(LanguageData languageData)
            : base(languageData)
        {
            FunctionCallTargets.Add("datetime", new StdCallTargetFactory() { CallTarget = new DateTimeCallTarget() });
            FunctionCallTargets.Add("time", new StdCallTargetFactory() { CallTarget = new TimeCallTarget() });
            FunctionCallTargets.Add("answer", new AnswerCallTargetFactory()); 
            FunctionCallTargets.Add("answers", new AnswersCallTargetFactory());
            FunctionCallTargets.Add("facility", new FacilityCallTargetFactory());
            FunctionCallTargets.Add("getquestion", new QuestionCallTargetFactory());
            FunctionCallTargets.Add("subsection", new SubSectionCallTargetFactory());
            FunctionCallTargets.Add("isnull", new StdCallTargetFactory() { CallTarget = new IsNullCallTarget() });
            FunctionCallTargets.Add("ifnull", new StdCallTargetFactory() { CallTarget = new IfNullCallTarget() });
            FunctionCallTargets.Add("parent", new StdCallTargetFactory() { CallTarget = new ParentCallTarget() });
            FunctionCallTargets.Add("itemat", new StdCallTargetFactory() { CallTarget = new ItemAtCallTarget() });
            FunctionCallTargets.Add("list", new StdCallTargetFactory() { CallTarget = new ListCallTarget() });
            FunctionCallTargets.Add("dictionary", new StdCallTargetFactory() { CallTarget = new DictionaryCallTarget() });
            FunctionCallTargets.Add("expand", new StdCallTargetFactory() { CallTarget = new ExpandStringCallTarget() });
            FunctionCallTargets.Add("isoffline", new StdCallTargetFactory() {CallTarget = new OfflineCallTarget()});
            FunctionCallTargets.Add("apiversion", new StdCallTargetFactory(){CallTarget = new ApiVersionCallTarget()});
        }

        public ICallTarget GetCallTarget(IContextContainer context, string name)
        {
            ICallTargetFactory callTargetFactory = null;
            //look in context first
            var ct = context.Get<ICallTarget>(name, () => null);
            if (ct != null)
                return ct;
            FunctionCallTargets.TryGetValue(name, out callTargetFactory);
            if (callTargetFactory != null)
                return callTargetFactory.Create(context);
            return null;
        }

        protected override Type GetCommonTypeForOperator(System.Linq.Expressions.ExpressionType op, Type argType1, Type argType2)
        {
            if (argType1 == argType2)
                return argType1;
            //First check for unsigned types and convert to signed versions
            var t1 = GetSignedTypeForUnsigned(argType1);
            var t2 = GetSignedTypeForUnsigned(argType2);
            // The type with higher index in _typesSequence is the commont type 
            var index1 = _typesSequence.IndexOf(t1);
            var index2 = _typesSequence.IndexOf(t2);
            if (index1 >= 0 && index2 >= 0)
                return _typesSequence[Math.Max(index1, index2)];
            //If we have some custom type, 
            return null;
        }

        public override void InitTypeConverters()
        {
            base.InitTypeConverters();

            var targetType = typeof(decimal);
            AddConverter(typeof(bool), targetType, x => (bool)x ? 1m : 0m);
            AddConverter(typeof(sbyte), targetType, ConvertAnyIntToDecimal);
            AddConverter(typeof(byte), targetType, ConvertAnyIntToDecimal);
            AddConverter(typeof(Int16), targetType, ConvertAnyIntToDecimal);
            AddConverter(typeof(UInt16), targetType, ConvertAnyIntToDecimal);
            AddConverter(typeof(Int32), targetType, ConvertAnyIntToDecimal);
            AddConverter(typeof(UInt32), targetType, ConvertAnyIntToDecimal);
            AddConverter(typeof(Int64), targetType, ConvertAnyIntToDecimal);
            AddConverter(typeof(UInt64), targetType, ConvertAnyIntToDecimal);
            AddConverter(typeof(string), targetType, o =>
                {
                    decimal d;
                    if (decimal.TryParse(o.ToString(), out d))
                        return d;
                    return o;
                });

            targetType = typeof(string);
            AddConverter(typeof(decimal), targetType, ConvertAnyToString);

            targetType = typeof(object[]);
            AddConverter(typeof(bool), targetType, x => new object[] { x });
            AddConverter(typeof(sbyte), targetType, x => new object[] { x });
            AddConverter(typeof(byte), targetType, x => new object[] { x });
            AddConverter(typeof(Int16), targetType, x => new object[] { x });
            AddConverter(typeof(UInt16), targetType, x => new object[] { x });
            AddConverter(typeof(Int32), targetType, x => new object[] { x });
            AddConverter(typeof(UInt32), targetType, x => new object[] { x });
            AddConverter(typeof(Int64), targetType, x => new object[] { x });
            AddConverter(typeof(UInt64), targetType, x => new object[] { x });
            AddConverter(typeof(string), targetType, x => new object[] { x });

            AddBinary(ExpressionType.AddChecked, typeof(DateTime), typeof(TimeSpan), (d, t) => ((DateTime)d).Add((TimeSpan)t), null);
            AddBinary(ExpressionType.SubtractChecked, typeof(DateTime), typeof(TimeSpan), (d, t) => ((DateTime)d).Subtract((TimeSpan)t), null);
            AddBinary(ExpressionType.SubtractChecked, typeof(DateTime), typeof(DateTime), (d1, d2) => ((DateTime)d1).Subtract((DateTime)d2), null);
            AddBinary(ExpressionType.AddChecked, typeof(TimeSpan), typeof(TimeSpan), (d1, d2) => ((TimeSpan)d1).Add((TimeSpan)d2), null);
            AddBinary(ExpressionType.SubtractChecked, typeof(TimeSpan), typeof(TimeSpan), (d1, d2) => ((TimeSpan)d1).Subtract((TimeSpan)d2), null);

            AddBinary(ExpressionType.GreaterThan, typeof(DateTime), (d1, d2) => ((DateTime)d1) > ((DateTime)d2));
            AddBinary(ExpressionType.GreaterThanOrEqual, typeof(DateTime), (d1, d2) => ((DateTime)d1) >= ((DateTime)d2));
            AddBinary(ExpressionType.LessThan, typeof(DateTime), (d1, d2) => ((DateTime)d1) < ((DateTime)d2));
            AddBinary(ExpressionType.LessThanOrEqual, typeof(DateTime), (d1, d2) => ((DateTime)d1) <= ((DateTime)d2));
            AddBinary(ExpressionType.Equal, typeof(DateTime), (d1, d2) => ((DateTime)d1) == ((DateTime)d2));
            AddBinary(ExpressionType.NotEqual, typeof(DateTime), (d1, d2) => ((DateTime)d1) != ((DateTime)d2));

            AddBinary(ExpressionType.GreaterThan, typeof(TimeSpan), (d1, d2) => ((TimeSpan)d1) > ((TimeSpan)d2));
            AddBinary(ExpressionType.GreaterThanOrEqual, typeof(TimeSpan), (d1, d2) => ((TimeSpan)d1) >= ((TimeSpan)d2));
            AddBinary(ExpressionType.LessThan, typeof(TimeSpan), (d1, d2) => ((TimeSpan)d1) < ((TimeSpan)d2));
            AddBinary(ExpressionType.LessThanOrEqual, typeof(TimeSpan), (d1, d2) => ((TimeSpan)d1) <= ((TimeSpan)d2));
            AddBinary(ExpressionType.Equal, typeof(TimeSpan), (d1, d2) => ((TimeSpan)d1) == ((TimeSpan)d2));
            AddBinary(ExpressionType.NotEqual, typeof(TimeSpan), (d1, d2) => ((TimeSpan)d1) != ((TimeSpan)d2));

        }

        protected OperatorImplementation AddBinary(ExpressionType op, Type arg1Type, Type arg2Type,
                 BinaryOperatorMethod binaryMethod, UnaryOperatorMethod resultConverter)
        {
            var key = new OperatorDispatchKey(op, arg1Type, arg2Type);
            var impl = new OperatorImplementation(key, arg1Type, binaryMethod, null, null, resultConverter);
            OperatorImplementations[key] = impl;
            return impl;
        }

        public override void  InitBinaryOperatorImplementationsForMatchedTypes()
        {
 	        base.InitBinaryOperatorImplementationsForMatchedTypes();

            AddBinary(ExpressionType.Equal, typeof(string), (x, y) => string.Compare(x.ToString(), y.ToString(), StringComparison.InvariantCultureIgnoreCase) == 0);
            AddBinary(ExpressionType.NotEqual, typeof(string), (x, y) => string.Compare(x.ToString(), y.ToString(), StringComparison.InvariantCultureIgnoreCase) != 0);
            AddBinary(ExpressionType.LessThan, typeof(string), (x, y) => string.Compare(x.ToString(), y.ToString(), StringComparison.InvariantCultureIgnoreCase) < 0);
            AddBinary(ExpressionType.LessThanOrEqual, typeof(string), (x, y) => string.Compare(x.ToString(), y.ToString(), StringComparison.InvariantCultureIgnoreCase) <= 0);
            AddBinary(ExpressionType.GreaterThan, typeof(string), (x, y) => string.Compare(x.ToString(), y.ToString(), StringComparison.InvariantCultureIgnoreCase) > 0);
            AddBinary(ExpressionType.GreaterThanOrEqual, typeof(string), (x, y) => string.Compare(x.ToString(), y.ToString(), StringComparison.InvariantCultureIgnoreCase) >= 0);

            AddBinary(ExpressionType.Equal, typeof(object[]), (x, y) =>
                {
                    IEnumerable<object> xList = x as IEnumerable<object>;
                    IEnumerable<object> yList = y as IEnumerable<object>;

                    return xList.SequenceEqual(yList, new ObjectConverter());
                });

            AddBinary(ExpressionType.Equal, typeof(bool), (x, y) => ConvertAnyToBool(x) == ConvertAnyToBool(y));
            AddBinary(ExpressionType.NotEqual, typeof(bool), (x, y) => ConvertAnyToBool(x) != ConvertAnyToBool(y));
        }
        
        public static object ConvertAnyIntToDecimal(object value)
        {
            long l = Convert.ToInt64(value);
            return new decimal(l);
        }


        public static bool ConvertAnyToBool(object p)
        {
            if (p is bool)
                return (bool)p;
            if (p is decimal)
                return ((decimal)p) != 0M;
            if (p is string)
                return ((string)p).Length > 0;
            if (p is int)
                return ((int)p) != 0;
            if (p is uint)
                return ((uint)p) != 0;
            if (p is long)
                return ((long)p) != 0L;
            if (p is ulong)
                return ((ulong)p) != 0L;
            if (p is short)
                return ((short)p) != 0L;
            if (p is ushort)
                return ((ushort)p) != 0L;

            if (p is IConvertableToPrimitive)
                return ConvertAnyToBool(p.ConvertToPrimitive());

            return false;
        }
    }
}
