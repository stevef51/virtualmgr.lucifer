﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.Commands.Runtime;
using Irony.Ast;
using Irony.Parsing;

namespace ConceptCave.Checklist.Lingo
{
    public class FinishAst : LingoInstruction
    {
        private FinishWithAst FinisherBlock { get; set; }

        public override Interfaces.InstructionResult Execute(ConceptCave.Core.IContextContainer context)
        {
            base.PreExecute(context);
            ILingoProgram program = context.Get<ILingoProgram>();
            IWorkingDocument document = context.Get<IWorkingDocument>();
            var haveYieldedVar = PrivateVariable<bool>(program, "HaveYielded");
            var haveYielded = haveYieldedVar.Value;
            //Is this the first time we're here?
            if (haveYielded == false)
            {
                //yes
                //Say we're unfinished, but set runmode to finishing. The latter kicks us out of the RunUntilStopped() block
                //above us, but the former ensures execution will resume here
                program.Do(new SetVariableCommand<bool>() { Variable = haveYieldedVar, NewValue = true });
                program.Do(new SetVariableCommand<RunMode>() { Variable = program.RunMode, NewValue = RunMode.Finishing });
                return InstructionResult.Unfinished;
            }
            else
            {
                //no, either its the second time, or we are in auto finish mode
                //execute any finilaser attached to us
                if (FinisherBlock != null)
                {
                    var result = FinisherBlock.Execute(context);
                    if (result == InstructionResult.Finished)
                    {
                        //what's the current run mode? if it was changed to Presenting, leave it as such; let WorkingDocument
                        //set us to Finished
                        //This lets us detect whether or not we should present anything after completion
                        if (program.RunMode.Value != RunMode.Presenting)
                            program.Do(new SetVariableCommand<RunMode>() { Variable = program.RunMode, NewValue = RunMode.Finished });
                        return InstructionResult.Finished;
                    }
                    else
                    {
                        return InstructionResult.Unfinished;
                    }
                }
                else
                {
                    program.Do(new SetVariableCommand<RunMode>() { Variable = program.RunMode, NewValue = RunMode.Finished });
                    return InstructionResult.Finished;
                }
            }
        }

        public override void Reset(ILingoProgram program, bool recurse)
        {
            base.Reset(program, recurse);
            PrivateVariable<bool>(program, "HaveYielded").Value = false;
        }

        public override void Init(AstContext context, ParseTreeNode parseTreeNode)
        {
            base.Init(context, parseTreeNode);
            if (parseTreeNode.GetMappedChildNodes().Count > 1)
            {
                FinisherBlock = (FinishWithAst)parseTreeNode.GetMappedChildNodes()[1].AstNode;
                AddChild(parseTreeNode.GetMappedChildNodes()[1]);
            }
            else
                FinisherBlock = null;
        }
    }
}
