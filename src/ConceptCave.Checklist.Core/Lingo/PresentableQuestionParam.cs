﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using Irony.Ast;

namespace ConceptCave.Checklist.Lingo
{
    public class PresentableQuestionParam : LingoAst, IPresentableSource
    {
        public IEvaluatable Index { get; private set; }
        public IdentifierAst NameAs { get; private set; }

        public override void Init(AstContext context, Irony.Parsing.ParseTreeNode parseTreeNode)
        {
            base.Init(context, parseTreeNode);

            Index = parseTreeNode.ChildNodes[0].AstNode as IEvaluatable;
            if (parseTreeNode.ChildNodes.Count >= 3)
                NameAs = parseTreeNode.ChildNodes[2].AstNode as IdentifierAst;
        }

        #region IQuestionSource Members

        public Interfaces.IPresentable GetPresentable(ILingoProgram program, ref string nameAs)
        {
            if (NameAs != null)
                nameAs = NameAs.Name;
            return program.WorkingDocument.Questions.Lookup(Index.Evaluate(program));
        }

        #endregion
    }
}
