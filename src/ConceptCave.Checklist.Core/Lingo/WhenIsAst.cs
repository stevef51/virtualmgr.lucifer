﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Core;
using ConceptCave.Checklist.Interfaces;
using Irony.Ast;

namespace ConceptCave.Checklist.Lingo
{
    public class WhenIsAst : LingoInstruction, IBooleanTest
    {
        public List<IBooleanTest> Tests = new List<IBooleanTest>();
        public StatementBlockAst CodeBlock;
        public bool Not;

        public override void Init(AstContext context, Irony.Parsing.ParseTreeNode parseTreeNode)
        {
            base.Init(context, parseTreeNode);

            // either
            // is [not] <booleanTest> codeblock
            // is [not] (<booleanTest>),(<booleanTest>)... codeblock

            if (parseTreeNode.ChildNodes[1].ChildNodes.Count > 0)           // not | empty
                Not = true;

            if (parseTreeNode.ChildNodes[2].ChildNodes[0].AstNode is IBooleanTest)
                parseTreeNode.ChildNodes[2].ChildNodes.ForEach(c => Tests.Add(c.AstNode as IBooleanTest));
            else
                parseTreeNode.ChildNodes[2].ChildNodes[0].ChildNodes.ForEach(c => Tests.Add(c.AstNode as IBooleanTest));
            CodeBlock = (StatementBlockAst)AddChild(parseTreeNode.ChildNodes[3]);
        }

        public override Interfaces.InstructionResult Execute(IContextContainer context)
        {
            base.PreExecute(context);
            return CodeBlock.Execute(context);
        }

        #region IBooleanTest Members

        public bool Test(object value, IContextContainer context)
        {
            bool test = Tests.FirstOrDefault(e => e.Test(value, context)) != null;
            if (Not)
                test = !test;
            return test;
        }

        #endregion
    }
}
