﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using Irony.Ast;

namespace ConceptCave.Checklist.Lingo
{
    public class IntegerRangeAst : LingoAst, IEvaluatable
    {
        public IEvaluatable StartRange { get; private set; }
        public IEvaluatable EndRange { get; private set; }

        public override void Init(AstContext context, Irony.Parsing.ParseTreeNode parseTreeNode)
        {
            base.Init(context, parseTreeNode);

            StartRange = parseTreeNode.ChildNodes[0].AstNode as IEvaluatable;
            switch(parseTreeNode.ChildNodes.Count)
            {
                case 3:         // "start..end"
                {
                    EndRange = parseTreeNode.ChildNodes[2].AstNode as IEvaluatable;
                    break;
                }

                case 2:         // "start.." or "..end"
                {
                    EndRange = parseTreeNode.ChildNodes[1].AstNode as IEvaluatable;
                    break;
                }

                case 1:         // ".."
                    break;
            }
        }
        #region IEvaluatable Members

        public object Evaluate(ConceptCave.Core.IContextContainer context)
        {
            int start, end;
            if (StartRange == null)
                start = context.Get<int>("StartRange");
            else
                start = Convert.ToInt32(StartRange.Evaluate(context));

            if (EndRange == null)
                end = context.Get<int>("EndRange");
            else
                end = Convert.ToInt32(EndRange.Evaluate(context));

            int count = end - start + 1;
            if (count < 0)
                count = 0;
            
            object[] result = new object[count];
            for (int i = start; i <= end; i++)
                result[i - start] = i;

            return result;
        }

        #endregion
    }
}
