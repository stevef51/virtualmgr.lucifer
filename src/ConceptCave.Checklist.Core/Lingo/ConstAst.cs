﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;

namespace ConceptCave.Checklist.Lingo
{
    public class ConstAst<T> : LingoAst, IEvaluatable
    {
        public T Value { get; private set; }

        public ConstAst(T value)
        {
            Value = value;
        }

        public object Evaluate(ConceptCave.Core.IContextContainer context)
        {
            return Value;
        }
    }
}
