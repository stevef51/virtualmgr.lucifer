﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;
using Irony.Ast;
using Irony.Parsing;

namespace ConceptCave.Checklist.Lingo
{
    public class AssignmentListAst : LingoAst, IEvaluatable
    {
        public List<ILingoInstruction> AssignmentList { get; private set; }

        public override void Init(AstContext context, ParseTreeNode parseTreeNode)
        {
            base.Init(context, parseTreeNode);

            AssignmentList = new List<ILingoInstruction>();
            parseTreeNode.ChildNodes.ForEach(c =>
            {
                if (c.AstNode is ILingoInstruction)
                    AssignmentList.Add(c.AstNode as ILingoInstruction);
            });

        }

        #region IEvaluatable Members

        public object Evaluate(ConceptCave.Core.IContextContainer context)
        {
            ILingoProgram program = context.Get<ILingoProgram>();
            ContextContainer newContext = new ContextContainer(context);

            VariableBag variableBag = new VariableBag();
            newContext.Set<IVariableBag>("Set", variableBag);

            AssignmentList.ForEach(a => a.Execute(newContext));

            return variableBag;
        }

        #endregion
    }
}
