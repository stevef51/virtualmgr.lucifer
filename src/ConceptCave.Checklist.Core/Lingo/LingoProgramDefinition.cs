﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Irony.Parsing;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;
using ConceptCave.Core.Commands;

namespace ConceptCave.Checklist.Lingo
{
    // The LingoProgram will execute a single instruction at a time.  Some instructions do not advance the ProgramCounter after
    // being Executed ("Present Question 1, 2" will execute twice for example), so each Instruction returns whether it is Finished or Unfinished
    // the parent block of each Instruction is responsible for tracking which is the current Instruction and moving it on when the 
    // current one finishes.  So a program with multiple levels of StatementBlocks will have multiple ProgramCounters (1 for each depth of StatementBlock)
    public partial class LingoProgramDefinition : Node, ILingoProgramDefinition
    {
        public LingoGrammar LingoGrammar { get; private set; }
        private ParseTree _parseTree;

        private ProgramBlockAst _programBlock;       // All programs have a root Statement Block of instructions

        public LingoProgramDefinition()
        {
            LingoGrammar = LingoGrammar.Instance;
        }

        public ILingoInstruction ProgramRoot
        {
            get 
            {
                CheckCompiled();
                return _programBlock; 
            }
        }

        public List<ILingoInstruction> AllInstructions
        {
            get 
            {
                CheckCompiled();
                return _programBlock.AllInstructions; 
            }
        }

        public string SourceCode 
        { 
            get { return _sourceCode; }
            set
            {
                _sourceCode = value;
                _parseTree = null;
            }
        } string _sourceCode;

        private void CheckCompiled()
        {
            if (_parseTree == null)
            {
                lock (this)
                {
                    if (_parseTree == null)
                    {
                        Parser parser = new Parser(LingoGrammar.LanguageData);

                        ParseTree parseTree = parser.Parse(_sourceCode);

                        if (parseTree.HasErrors())
                            throw new LingoException(this, parseTree.ParserMessages);

                        _parseTree = parseTree;
                        _programBlock = _parseTree.Root.AstNode as ProgramBlockAst;

                        // Assign all Instructions a unique Program Counter Address
                        int address = 0;
                        _programBlock.AllInstructions.ForEach(i =>
                        {
                            i.Address = address++;
                        });
                    }
                }
            }
        }

        public ParseTree SyntaxCheck()
        {
            Parser parser = new Parser(LingoGrammar.LanguageData);
           
            return parser.Parse(_sourceCode);
        }

        public ILingoProgram Compile(IWorkingDocument document)
        {
            CheckCompiled();
            return new Instance(this, document);
        }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("SourceCode", _sourceCode);
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            SourceCode = decoder.Decode<string>("SourceCode", null);
        }
    }
}
