﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Commands.Runtime;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;
using Irony.Ast;

namespace ConceptCave.Checklist.Lingo
{
    public class LoopAst : LingoInstruction
    {
        public IEvaluatable LoopOver { get; private set; }
        public IdentifierAst LoopVariable { get; private set; }
        public StatementBlockAst CodeBlock { get; private set; }

        public override void Init(AstContext context, Irony.Parsing.ParseTreeNode parseTreeNode)
        {
            base.Init(context, parseTreeNode);

            LoopOver = parseTreeNode.ChildNodes[1].AstNode as IEvaluatable;
            
            // Either
            // loop <loopOver> <codeBlock> end
            // loop <loopOver> with <LoopVariable> <codeBlock> end
            if (parseTreeNode.ChildNodes[2].FindTokenAndGetText() == LingoGrammar.Keywords.With)
                LoopVariable = parseTreeNode.ChildNodes[2].ChildNodes[1].AstNode as IdentifierAst;

            CodeBlock = (StatementBlockAst)AddChild(parseTreeNode.ChildNodes[3]);
        }

        public override void Reset(Interfaces.ILingoProgram program, bool recurse)
        {
            if (recurse)
                base.Reset(program, recurse);

            PrivateVariable<int>(program, "LoopIndex").Value = 0;
            PrivateVariable<List<object>>(program, "LoopOver").Value = null;
        }

        public override Interfaces.InstructionResult Execute(IContextContainer context)
        {
            base.PreExecute(context);
            var program = context.Get<ILingoProgram>();
            var loopIndex = PrivateVariable<int>(program, "LoopIndex");
            var loopOver = PrivateVariable<List<object>>(program, "LoopOver");

            if (loopOver.Value == null)
            {
                program.Do(new SetVariableCommand<List<object>>() { Variable = loopOver, NewValue = new List<object>() });

                object loopOverEval = LoopOver.Evaluate(context);
                IEnumerable<object> loopOverEvalList = loopOverEval as IEnumerable<object>;
                if (loopOverEvalList != null)
                    loopOver.Value.AddRange(loopOverEvalList);
                else if (loopOverEval != null)
                    loopOver.Value.Add(loopOverEval);
            }

            if (loopIndex.Value < loopOver.Value.Count)
            {
                if (LoopVariable != null)
                    LoopVariable.Assign(context, loopOver.Value[loopIndex.Value]);

                if (CodeBlock.Execute(program) == InstructionResult.Finished)
                {
                    CodeBlock.Reset(program, true);
                    program.Do(new SetVariableCommand<int>() { Variable = loopIndex, NewValue = loopIndex.Value + 1 });
                }

                return InstructionResult.Unfinished;
            }

            return InstructionResult.Finished;
        }
    }
}
