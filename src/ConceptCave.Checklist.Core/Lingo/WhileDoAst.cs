﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Commands.Runtime;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;
using Irony.Ast;

namespace ConceptCave.Checklist.Lingo
{
    public class WhileDoAst : LingoInstruction
    {
        public IEvaluatable WhileExpression { get; private set; }
        public StatementBlockAst CodeBlock { get; private set; }

        public override void Init(AstContext context, Irony.Parsing.ParseTreeNode parseTreeNode)
        {
            base.Init(context, parseTreeNode);

            // while <expression> do <codeBlock> end
            WhileExpression = parseTreeNode.ChildNodes[1].AstNode as IEvaluatable;
            CodeBlock = (StatementBlockAst)AddChild(parseTreeNode.ChildNodes[3]);
        }

        public override void Reset(Interfaces.ILingoProgram program, bool recurse)
        {
            if (recurse)
                base.Reset(program, recurse);

            PrivateVariable<bool?>(program, "TestTrue").Value = null;
        }

        public override Interfaces.InstructionResult Execute(IContextContainer context)
        {
            base.PreExecute(context);
            var program = context.Get<ILingoProgram>();
            var testTrue = PrivateVariable<bool?>(program, "TestTrue");

            if (testTrue.Value == null)
            {
                bool test = LingoLanguageRuntime.ConvertAnyToBool(WhileExpression.Evaluate(context));
                    
                program.Do(new SetVariableCommand<bool?>() { Variable = testTrue, NewValue = test });
            }

            if (testTrue.Value == true)
            {
                if (CodeBlock.Execute(program) == InstructionResult.Finished)
                {
                    CodeBlock.Reset(program, true);
                    program.Do(new SetVariableCommand<bool?>() { Variable = testTrue, NewValue = null });
                }

                return InstructionResult.Unfinished;
            }

            return InstructionResult.Finished;
        }
    }
}
