﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.Reporting.LingoQuery;
using ConceptCave.Core;

namespace ConceptCave.Checklist.Lingo
{
    public static class QueryHelper
    {
        public static Expression MakeLinqTree(IEvaluatable node, IContextContainer context, ParameterExpression paramExpr)
        {
            //This method does postorder tree traversal on node.
            //After determining if node.left and node.right can be evaluated, if they both were,
            //then this is evaluated

            if (node is BinaryOperationAst)
            {
                var tnode = node as BinaryOperationAst;
                var leftExpr = MakeLinqTree(tnode.Left, context, paramExpr);
                var rightExpr = MakeLinqTree(tnode.Right, context, paramExpr);

                //We need to do some casting that should be automatic, but isn't.
                //Firstly, it'd be nice to compare decimal with other numeric types
                if (leftExpr.Type == typeof(decimal) && rightExpr.Type != typeof(decimal))
                    rightExpr = Expression.ConvertChecked(rightExpr, typeof(decimal));
                else if (leftExpr.Type != typeof(decimal) && rightExpr.Type == typeof(decimal))
                    leftExpr = Expression.ConvertChecked(leftExpr, typeof(decimal));
                else if (leftExpr.Type == typeof(Guid) && rightExpr.Type == typeof(string))
                    rightExpr = Expression.Call(typeof(Guid), "Parse", Type.EmptyTypes, rightExpr);
                else if (leftExpr.Type == typeof(string) && rightExpr.Type == typeof(Guid))
                    leftExpr = Expression.Call(typeof(Guid), "Parse", Type.EmptyTypes, leftExpr);

                return Expression.MakeBinary(tnode.Op, leftExpr, rightExpr);
            }
            else if (node is UnaryOperationAst)
            {
                var tnode = node as UnaryOperationAst;
                //recurse into the operator first
                var exprToAddTo = MakeLinqTree(tnode, context, paramExpr); ;
                //No need to manually reduce things, let LINQ handle it
                return Expression.MakeUnary(tnode.ExpressionType, exprToAddTo, null);
            }
            else if (node is ContainsExpressionAst)
            {
                var tnode = node as ContainsExpressionAst;
                var leftExpr = MakeLinqTree(tnode.ExpressionList, context, paramExpr);
                var rightExpr = MakeLinqTree(tnode.RightHS, context, paramExpr);

                if (leftExpr.Type == typeof(string))
                {
                    var containsCall = Expression.Call(
                        leftExpr, typeof(string).GetMethod("Contains", new Type[] { typeof(string) }), rightExpr);
                    return containsCall;
                }
                else
                {
                    var entityType = leftExpr.Type.GetGenericArguments()[0];
                    var anyParam = Expression.Parameter(entityType, "e");
                    var anyEq = Expression.Equal(anyParam, rightExpr);
                    var anyLambda = Expression.Lambda(anyEq, anyParam);

                    //TODO: Check if we should call Queryable.Any() or Enumerable.Any()
                    //now get the any call
                    var anyCall = Expression.Call(
                        typeof(Enumerable), "Any",
                        new Type[] { entityType },
                        leftExpr,
                        anyLambda
                    );

                    return anyCall;
                }

                
            }
            else if (node is InExpressionAst)
            {
                var tnode = node as InExpressionAst;
                //left expression is the what
                var leftExpr = MakeLinqTree(tnode.LeftHS, context, paramExpr);
                //the right hand side is an expression list, 
                var rightListExpr = tnode.ExpressionList;

                var rightList = rightListExpr.Evaluate(context) as IEnumerable;

                //expand this out to a bunch of OR expressions comparing equality
                //e.g. column in 2,3,4 ==> column = 2 or column = 3 or column = 4
                var equalityList = new Stack<Expression>();
                foreach (var item in rightList)
                {
                    var lhs = MakeLinqTree(tnode.LeftHS, context, paramExpr);
                    var rhs = Expression.Constant(item, item.GetType());
                    var eqExpr = Expression.Equal(lhs, rhs);
                    equalityList.Push(eqExpr);
                }
                var leftmost = Expression.OrElse(Expression.Constant(false), Expression.Constant(false));
                while (equalityList.Count > 0)
                {
                    var eqExpr = equalityList.Pop();
                    leftmost = Expression.OrElse(leftmost, eqExpr);
                }
                return leftmost;
            }
            else //nothing else modifies control flow (except for function calls, unsupported)
            {   //So just evaluate the thing
                //This is a base case of the recursion
                var tnode = node as IEvaluatable;
                var value = tnode.Evaluate(context);
                //special-case handling of this value:
                Expression retExpr;
                if (value.GetType().GetInterface(typeof(ILingoDataColumn).FullName) != null)
                {
                    //Yes, is column
                    //let the column decide how it should be referred to
                    retExpr = (value as ILingoDataColumn).LinqReference(paramExpr);
                }
                else
                {
                    retExpr = Expression.Constant(value);
                }
                return retExpr;
            }
        }
    }
}
