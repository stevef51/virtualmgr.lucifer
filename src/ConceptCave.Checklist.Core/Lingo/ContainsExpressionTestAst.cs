﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;
using Irony.Ast;

namespace ConceptCave.Checklist.Lingo
{
    public class ContainsExpressionTestAst : LingoAst, IBooleanTest
    {
        public IEvaluatable InValue { get; private set; }

        public override void Init(AstContext context, Irony.Parsing.ParseTreeNode parseTreeNode)
        {
            base.Init(context, parseTreeNode);

            InValue = parseTreeNode.ChildNodes[1].AstNode as IEvaluatable;
        }

        #region IBooleanTest Members

        public bool Test(object value, IContextContainer context)
        {
            var inValueEvald = InValue.Evaluate(context);
            var LHSValue = ObjectConverter.CreateEnumerablePrimitiveOf<object>(value);

            foreach (var o in LHSValue)
            {
                if (o.Equals(inValueEvald))
                    return true;
            }
            return false;
        }

        #endregion
    }
}
