﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Irony;
using Irony.Ast;
using Irony.Interpreter.Ast;
using Irony.Parsing;
using ConceptCave.Checklist.Interfaces;

namespace ConceptCave.Checklist.Lingo
{
    public class LingoAst : IAstNodeInit, IVisitee
    {
        protected List<LingoAst> _childNodes = new List<LingoAst>();
        public ParseTreeNode ParseTreeNode { get; private set; }
        

        #region IAstNodeInit Members

        public virtual void Init(AstContext context, Irony.Parsing.ParseTreeNode parseTreeNode)
        {
            ParseTreeNode = parseTreeNode;
            ParseTreeNode.AstNode = this;
        }

        #endregion

        public SourceLocation Location
        {
            get { return ParseTreeNode.Span.Location; }
        }

        public LingoAst AddChild(ParseTreeNode parseTreeNode)
        {
            LingoAst childNode = parseTreeNode.AstNode as LingoAst;         
            if (childNode == null)
                throw new InvalidOperationException("Not a LingoAst node");

            _childNodes.Add(childNode);

            return childNode;
        }

        public virtual void Reset(ILingoProgram program, bool recurse)
        {
            if (recurse)
                _childNodes.ForEach(child => child.Reset(program, recurse));
        }

        #region IVisitee Members

        public void AcceptVisitor(IVisitor visitor, Func<bool> recurse)
        {
            visitor.BeginVisit(this);
            if (recurse())
                _childNodes.ForEach(n => n.AcceptVisitor(visitor, recurse));
            visitor.EndVisit(this);
        }

        #endregion

       public bool IsTrue(object value)
        {
            if (value is bool)
                return (bool)value;

            if (value is string)
                return ((string)value).Equals("true", StringComparison.InvariantCultureIgnoreCase);

            if (value is int)
                return ((int)value != 0);

            return value != null;
        }
    }
}
