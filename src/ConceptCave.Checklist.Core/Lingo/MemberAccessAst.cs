﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using Irony.Parsing;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.Lingo.Functions;
using ConceptCave.Checklist.Lingo.ObjectProviders;
using Irony.Ast;
using ConceptCave.Core;

namespace ConceptCave.Checklist.Lingo
{

    //For now we do not support dotted namespace/type references like System.Collections or System.Collections.List.
    // Only references to objects like 'objFoo.Name' or 'objFoo.DoStuff()'
    public class MemberAccessAst : LingoAst, IEvaluatable, IAssignable
    {
        public IEvaluatable Left;
        public string MemberName;

        public override void Init(AstContext context, ParseTreeNode treeNode)
        {
            base.Init(context, treeNode);
            Left = AddChild(treeNode.ChildNodes[0]) as IEvaluatable;
            var right = treeNode.GetMappedChildNodes().Last();
            MemberName = right.FindTokenAndGetText();
        }

        /*
        public override void DoSetValue(ScriptThread thread, object value)
        {
            thread.CurrentNode = this;  //standard prolog
            var leftValue = _left.Evaluate(thread);
            if (leftValue == null)
                thread.ThrowScriptError("Target object is null.");
            var type = leftValue.GetType();
            var members = type.GetMember(_memberName);
            if (members == null || members.Length == 0)
                thread.ThrowScriptError("Member {0} not found in object of type {1}.", _memberName, type);
            var member = members[0];
            switch (member.MemberType)
            {
                case MemberTypes.Property:
                    var propInfo = member as PropertyInfo;
                    propInfo.SetValue(leftValue, value, null);
                    break;
                case MemberTypes.Field:
                    var fieldInfo = member as FieldInfo;
                    fieldInfo.SetValue(leftValue, value);
                    break;
                default:
                    thread.ThrowScriptError("Cannot assign to member {0} of type {1}.", _memberName, type);
                    break;
            }//switch
            thread.CurrentNode = Parent; //standard epilog
        }//method

    */
        public object Evaluate(ConceptCave.Core.IContextContainer context)
        {
            ContextContainer subContext = new ContextContainer(context);
            subContext.Set<bool>("IsLeafMember", Left is IdentifierAst);           // In "a := b.c.d", True when 'c' evaluates 'd' but False when 'b' evaluates 'c'

            var leftValue = Left.Evaluate(subContext);
            if (leftValue == null)
            {
                if (Left is LingoAst)
                {
                    throw new LingoException("Object is nil", (LingoAst)Left);
                }
                else
                {
                    throw new LingoException("Object is nil");
                }
            }

            object result;

            IObjectGetter objectProvider = new ReflectionObjectProvider(leftValue);
            if (objectProvider.GetObject(subContext, MemberName, out result))
                return result;

            objectProvider = leftValue as IObjectGetter;
            if (objectProvider != null)
            {
                try
                {
                    if (objectProvider.GetObject(subContext, MemberName, out result))
                        return result;
                }
                catch (Exception e)
                {
                    if (e is LingoException)
                    {
                        ((LingoException) e).Ast = this;
                        throw;
                    }
                    throw new LingoException(string.Format("Error while evaluating member '{0}'", MemberName), this, e);
                }
                
            }

            // Try extension methods as last resort ..
            objectProvider = new ReflectionObjectProvider(leftValue, ReflectionObjectProvider.GetExtensionMethods);
            if (objectProvider.GetObject(subContext, MemberName, out result))
                return result;

            throw new LingoException(string.Format("Member '{0}' not found on type {1}", MemberName, leftValue.GetType().ToString()), (LingoAst)Left);  
        }

        public void Assign(ConceptCave.Core.IContextContainer context, object value)
        {
            var leftValue = Left.Evaluate(context);
            if (leftValue == null)
            {
                if (Left is LingoAst)
                {
                    throw new LingoException("Object is nil", (LingoAst)Left);
                }
                else
                {
                    throw new LingoException("Object is nil");
                }
            }

            IObjectSetter objectProvider = new ReflectionObjectProvider(leftValue);
            if(objectProvider.SetObject(context, MemberName, value) == false)
            {
                objectProvider = leftValue as IObjectSetter;
                if(objectProvider != null)
                {
                    objectProvider.SetObject(context, MemberName, value);
                }
            }
        }
    }
}
