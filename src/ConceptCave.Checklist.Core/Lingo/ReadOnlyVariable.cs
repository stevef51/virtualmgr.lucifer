﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConceptCave.Checklist.Lingo
{
    public class ReadOnlyVariable<T> : Variable<T>
    {
        protected T _value;
        public override T Value
        {
            get
            {
                return _value;
            }
            set
            {
                throw new LingoException("Cannot write to lingo constants");
            }
        }

        public ReadOnlyVariable() : base() { }
        public ReadOnlyVariable(string name) : base(name) { }
        public ReadOnlyVariable(string name, T value)
        {
            _value = value;
        }
    }
}
