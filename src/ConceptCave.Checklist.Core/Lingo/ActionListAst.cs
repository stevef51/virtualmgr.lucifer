﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Core;
using ConceptCave.Checklist.Interfaces;
using Irony.Ast;

namespace ConceptCave.Checklist.Lingo
{
    public class ActionListAst : LingoAst, IEvaluatable
    {
        public bool All { get; private set; }
        public IEvaluatable Index { get; private set; }

        public override void Init(AstContext context, Irony.Parsing.ParseTreeNode parseTreeNode)
        {
            base.Init(context, parseTreeNode);

            if (parseTreeNode.ChildNodes[0].FindTokenAndGetText() == LingoGrammar.Keywords.All)
                All = true;
            else
                Index = parseTreeNode.ChildNodes[1].AstNode as IEvaluatable;
        }

        #region IEvaluatable Members

        public object Evaluate(ConceptCave.Core.IContextContainer context)
        {
            IWorkingDocument workingDocument = context.Get<IWorkingDocument>();
            if (All)
                return workingDocument.Facilities.ToArray();
            else
            {
                ContextContainer actionContext = new ContextContainer(context);
                actionContext.Set<int>("StartRange", 1);
                actionContext.Set<int>("EndRange", workingDocument.Facilities.Count);
                actionContext.Set<bool>("FlattenList", true);

                object eval = Index.Evaluate(actionContext);

                IEnumerable<object> enumerableEval = eval as IEnumerable<object>;
                if (enumerableEval != null)
                {
                    List<IFacility> actions = new List<IFacility>();
                    foreach (object index in enumerableEval)
                    {
                        IFacility action = workingDocument.Facilities.Lookup(index);
                        if (action != null)
                            actions.Add(action);
                    }

                    return actions.ToArray();
                }

                return workingDocument.Facilities.Lookup(eval);
            }
        }

        #endregion
    }
}
