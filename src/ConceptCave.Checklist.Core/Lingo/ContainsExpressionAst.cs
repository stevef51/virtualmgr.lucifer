﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using Irony.Ast;

namespace ConceptCave.Checklist.Lingo
{
    public class ContainsExpressionAst : LingoAst, IEvaluatable
    {
        public IEvaluatable RightHS { get; private set; }
        public IEvaluatable ExpressionList { get; private set; }

        public override void Init(AstContext context, Irony.Parsing.ParseTreeNode parseTreeNode)
        {
            base.Init(context, parseTreeNode);

            RightHS = parseTreeNode.ChildNodes[2].AstNode as IEvaluatable;
            ExpressionList = parseTreeNode.ChildNodes[0].AstNode as IEvaluatable;
        }

        public object Evaluate(ConceptCave.Core.IContextContainer context)
        {
            object rightValue = RightHS.Evaluate(context);
            object eval = ExpressionList.Evaluate(context);

            IEnumerable<object> evalList = eval as IEnumerable<object>;
            if (evalList != null)
            {
                foreach (object o in evalList)
                {
                    if (object.Equals(rightValue, o))
                        return true;
                }
                return false;
            }
            else
                return object.Equals(rightValue, eval);
        }
    }
}
