﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Irony.Parsing;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.Commands.Runtime;
using ConceptCave.Core;
using Irony.Ast;

namespace ConceptCave.Checklist.Lingo
{
    public class StatementBlockAst : LingoInstruction
    {

        public override void Init(AstContext context, Irony.Parsing.ParseTreeNode parseNode)
        {
            base.Init(context, parseNode);

            foreach (ParseTreeNode childNode in parseNode.ChildNodes)
            {
                if (childNode.AstNode is ILingoInstruction)
                    AddChild(childNode);
            }
        }

        private ILingoInstruction ChildInstruction(int index)
        {
            return _childNodes[index] as ILingoInstruction;
        }

        public override void Reset(ILingoProgram program, bool recurse)
        {
            if (recurse)
                base.Reset(program, recurse);
            
            // Initialise any state variables in the program now
            PrivateVariable<int>(program, "ProgramCounter").Value = 0;
        }

        public override InstructionResult Execute(IContextContainer context)
        {
            base.PreExecute(context);
            var program = context.Get<ILingoProgram>();
            IVariable<int> programCounter = PrivateVariable<int>(program, "ProgramCounter");
            if (programCounter.Value < _childNodes.Count)
            {
                if (ChildInstruction(programCounter.Value).Execute(context) == InstructionResult.Finished)
                    program.Do(new SetVariableCommand<int>() { Variable = programCounter, NewValue = programCounter.Value + 1 });

                return InstructionResult.Unfinished;
            }

            return InstructionResult.Finished;
        }


        #region IInstructionBlock Members

/*        public ILingoInstruction CurrentInstruction
        {
            get 
            {
                ILingoInstruction currentInstruction = ChildInstruction(_program.InstructionState<int>(this, "ProgramCounter").Value);
                ILingoInstructionBlock block = currentInstruction as ILingoInstructionBlock;
                if (block != null)
                    return block.CurrentInstruction;
                return currentInstruction;
            }
        }
        */
        #endregion
    }
}
