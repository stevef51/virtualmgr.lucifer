﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Core;
using ConceptCave.Checklist.Interfaces;
using Irony.Ast;

namespace ConceptCave.Checklist.Lingo
{
    public class QuestionListAst : LingoAst, IEvaluatable
    {
        public bool All { get; private set; }
        public IEvaluatable Index { get; private set; }

        public override void Init(AstContext context, Irony.Parsing.ParseTreeNode parseTreeNode)
        {
            base.Init(context, parseTreeNode);

            if (parseTreeNode.ChildNodes[0].FindTokenAndGetText() == LingoGrammar.Keywords.All)
                All = true;
            else
                Index = parseTreeNode.ChildNodes[1].AstNode as IEvaluatable;
        }

        #region IEvaluatable Members

        public object Evaluate(ConceptCave.Core.IContextContainer context)
        {
            IWorkingDocument workingDocument = context.Get<IWorkingDocument>();
            if (All)
                return workingDocument.Questions.ToArray();
            else
            {
                ContextContainer questionContext = new ContextContainer(context);
                questionContext.Set<int>("StartRange", 1);
                questionContext.Set<int>("EndRange", workingDocument.Questions.Count);
                questionContext.Set<bool>("FlattenList", true);

                object eval = Index.Evaluate(questionContext);

                IEnumerable<object> enumerableEval = eval as IEnumerable<object>;
                if (enumerableEval != null)
                {
                    List<IQuestion> questions = new List<IQuestion>();
                    foreach (object index in enumerableEval)
                    {
                        IQuestion question = workingDocument.Questions.Lookup(index);
                        if (question != null)
                            questions.Add(question);
                    }

                    return questions.ToArray();
                }

                return workingDocument.Questions.Lookup(eval);
            }
        }

        #endregion
    }
}
