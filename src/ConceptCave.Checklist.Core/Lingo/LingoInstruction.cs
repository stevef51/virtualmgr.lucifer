﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;

namespace ConceptCave.Checklist.Lingo
{
    public abstract class LingoInstruction : LingoAst, ILingoInstruction
    {
        public LingoInstruction()
        {
        }

        #region IInstruction Members

        public int Address
        {
            get { return _address; }
            set { _address = value; }
        } int _address;


        public abstract InstructionResult Execute(IContextContainer context);

        protected void PreExecute(IContextContainer context)
        {
			((LingoProgramDefinition.Instance) context.Get<ILingoProgram>()).CurrentInstruction = this;
        }

        #endregion

        protected IVariable<T> PrivateVariable<T>(ILingoProgram program, string name, T defaultValue)
        {
            return program.PrivateVariable<T>(this, name, defaultValue);
        }

        protected IVariable<T> PrivateVariable<T>(ILingoProgram program, string name)
        {
            return program.PrivateVariable<T>(this, name, default(T));
        }
    }
}
