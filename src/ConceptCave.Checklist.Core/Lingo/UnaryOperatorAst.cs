﻿
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Irony.Parsing;
using Irony.Interpreter;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;
using Irony.Ast;
using Irony.Interpreter.Ast;

namespace ConceptCave.Checklist.Lingo
{

    public class UnaryOperationAst : LingoAst, IEvaluatable
    {
        public string OpSymbol;
        public IEvaluatable Argument;
        private OperatorImplementation _lastUsed;
        public ExpressionType ExpressionType { get; private set; }

        public UnaryOperationAst() { }
        public override void Init(AstContext context, ParseTreeNode treeNode)
        {
            base.Init(context, treeNode);
            OpSymbol = treeNode.GetMappedChildNodes()[0].FindTokenAndGetText();
            Argument = AddChild(treeNode.GetMappedChildNodes()[1]) as IEvaluatable;
            ExpressionType = ((InterpreterAstContext)context).OperatorHandler.GetUnaryOperatorExpressionType(OpSymbol);
        }

        public object Evaluate(IContextContainer context)
        {
            var arg = Argument.Evaluate(context);

            LanguageRuntime runtime = context.Get<LanguageRuntime>("");

            var result = runtime.ExecuteUnaryOperator(ExpressionType, arg, ref _lastUsed);
            return result;
        }
    }
}
