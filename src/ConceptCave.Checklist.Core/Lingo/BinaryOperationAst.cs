﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Reflection;
using Irony.Interpreter;
using Irony.Parsing;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;
using Irony.Ast;
using Irony.Interpreter.Ast;

namespace ConceptCave.Checklist.Lingo
{
    public class BinaryOperationAst : LingoAst, IEvaluatable
    {
        public IEvaluatable Left, Right;
        public string OpSymbol;
        public ExpressionType Op;
        private OperatorImplementation _lastUsed;

        public BinaryOperationAst() { }

        public override void Init(AstContext context, ParseTreeNode treeNode)
        {
            base.Init(context, treeNode);
            Left = AddChild(treeNode.GetMappedChildNodes()[0]) as IEvaluatable;
            Right = AddChild(treeNode.GetMappedChildNodes()[2]) as IEvaluatable;
            var opToken = treeNode.GetMappedChildNodes()[1].FindToken();
            OpSymbol = opToken.Text;
            Op = ((InterpreterAstContext)context).OperatorHandler.GetOperatorExpressionType(OpSymbol);
            // Set error anchor to operator, so on error (Division by zero) the explorer will point to 
            // operator node as location, not to the very beginning of the first operand.
//            ErrorAnchor = opToken.Location;
        }

        public object Evaluate(IContextContainer context)
        {
            // Only evaluate exactly what is required, ie
            //
            // a && b .. if a is false then result is always false
            // a || b .. if a is true then result is always true
            var leftValue = Left.Evaluate(context);

            if (leftValue == null)
            {
                if (Left is LingoAst)
                {
                    throw new LingoException("Object is nil", (LingoAst)Left);
                }
                else
                {
                    throw new LingoException("Object is nil");
                }
            }

            switch (Op)
            {
                case ExpressionType.AndAlso:
                    if (!IsTrue(leftValue)) 
                        return leftValue; //if false return immediately
                    return Right.Evaluate(context);

                case ExpressionType.OrElse:
                    if (IsTrue(leftValue)) 
                        return leftValue;
                    return Right.Evaluate(context);
            }

            var rightValue = Right.Evaluate(context);

            if (rightValue == null)
            {
                if (Right is LingoAst)
                {
                    throw new LingoException("Object is nil", (LingoAst)Right);
                }
                else
                {
                    throw new LingoException("Object is nil");
                }
            }

            LanguageRuntime runTime = context.Get<LanguageRuntime>("");
            try
            {
                return runTime.ExecuteBinaryOperator(Op, leftValue.ConvertToPrimitive(), rightValue.ConvertToPrimitive(), ref _lastUsed);
            }
            catch (Exception ex)
            {
                throw new LingoException("Operator error : " + ex.Message, this, ex);
            }
        }

    }
}
