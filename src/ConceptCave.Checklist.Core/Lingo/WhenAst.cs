﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using Irony.Parsing;
using Irony.Interpreter;
using ConceptCave.Checklist.Commands.Runtime;
using ConceptCave.Core;
using System.Diagnostics;
using Irony.Ast;

namespace ConceptCave.Checklist.Lingo
{
    public class WhenAst : LingoInstruction
    {
        private IEvaluatable _expressionAst;
        
        public override void Init(AstContext context, Irony.Parsing.ParseTreeNode parseNode)
        {
            base.Init(context, parseNode);

            // tree will be one of
            //
            // isList
            // isList + otherwise
            // otherwise
            
            _expressionAst = parseNode.ChildNodes[1].AstNode as IEvaluatable;            
            switch(parseNode.ChildNodes[2].ChildNodes[0].ChildNodes[0].Term.Name)
            {
                case "IsList":
                    foreach (ParseTreeNode childNode in parseNode.ChildNodes[2].ChildNodes[0].ChildNodes[0].ChildNodes)
                    {
                        if (childNode.AstNode is IBooleanTest)
                            AddChild(childNode);
                    }

                    if (parseNode.ChildNodes[2].ChildNodes[0].ChildNodes.Count > 1)
                        AddChild(parseNode.ChildNodes[2].ChildNodes[0].ChildNodes[1]);
                    break;

                case "WhenOtherwise":
                    AddChild(parseNode.ChildNodes[2].ChildNodes[0].ChildNodes[0]);
                    break;
            }
        }

        public override Interfaces.InstructionResult Execute(IContextContainer context)
        {
            base.PreExecute(context);
            ILingoProgram program = context.Get<ILingoProgram>();
            var isIndex = PrivateVariable<int>(program, "IsIndex");

            if (isIndex.Value == -1)          // Expression has not been Evaluated yet
            {
                object caseResult = _expressionAst.Evaluate(context);
//                Console.WriteLine(string.Format("When: {0} eval {1}", _expressionAst, caseResult));

                for(int i = 0 ; i < _childNodes.Count ; i++)
                {
                    IBooleanTest test = _childNodes[i] as IBooleanTest;
                    if (test.Test(caseResult, context))
                    {
                        program.Do(new SetVariableCommand<int>() { Variable = isIndex, NewValue = i });
                        break;
                    }
                }
            }

            // If no Is matched and their was no Otherwise then this When is finished
            if (isIndex.Value == -1)
                return InstructionResult.Finished;
            
            // Execute the When/Default child until it finishes..
            ILingoInstruction childInstruction = _childNodes[isIndex.Value] as ILingoInstruction;
            return childInstruction.Execute(program);
        }

        public override void Reset(Interfaces.ILingoProgram program, bool recurse)
        {
            if (recurse)
                base.Reset(program, recurse);

            PrivateVariable<int>(program, "IsIndex").Value = -1;
        }
    }
}
