﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Irony.Parsing;
using Irony;
using Irony.Interpreter.Ast;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.Commands.Runtime;
using ConceptCave.Checklist.Editor;
using ConceptCave.Core;
using Irony.Ast;
using ConceptCave.Checklist.Validators;
using ConceptCave.Checklist.Commands;
//using ConceptCave.Checklist.Core.Commands.Runtime;

namespace ConceptCave.Checklist.Lingo
{
    public class PresentAst : LingoInstruction, IEvaluatable
    {
        public class CustomValidator : Node
        {
            public IValidatorResult ValidatorResult { get; set; }
            public PresentCommand PresentCommand { get; set; }

            public CustomValidator()
            {

            }

            public IPresented Presented
            {
                get
                {
                    return PresentCommand.Presented;
                }
            }

            public bool Invalid
            {
                get { return ValidatorResult.Invalid; }
            }

            public void AddInvalidReason(string reason, IAnswer answer)
            {
                ValidatorResult.AddInvalidReason(reason, answer);
            }

            public void AddInvalidReason(string reason)
            {
                ValidatorResult.AddInvalidReason(reason);
            }

            public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
            {
                base.Encode(encoder);

                encoder.Encode("PresentCommand", PresentCommand);
                encoder.Encode("ValidatorResult", ValidatorResult);
            }

            public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
            {
                base.Decode(decoder);

                PresentCommand = decoder.Decode<PresentCommand>("PresentCommand");
                ValidatorResult = decoder.Decode<IValidatorResult>("ValidatorResult");
            }
        }

        // Syntax 
        //
        // present <presentableList>

        public IEvaluatable Evaluatable;
        public bool Hidden;
        public bool Again;
        public IdentifierAst ValidationVariable { get; private set; }
        public StatementBlockAst ValidationCodeBlock { get; private set; }

        public PresentAst()
        {
        }

        #region IAstNodeInit Members

        public override void Init(AstContext context, ParseTreeNode parseNode)
        {
            base.Init(context, parseNode);

            // present [hidden] <expression> [validate with <validationVariable>
            // <validationCodeBlock>
            // end]
            Hidden = parseNode.ChildNodes[1].FindTokenAndGetText() == LingoGrammar.Keywords.Hidden;

            Evaluatable = parseNode.ChildNodes[2].AstNode as IEvaluatable;
            
            Again = parseNode.ChildNodes[3].FindTokenAndGetText() == LingoGrammar.Keywords.Again;

            if (parseNode.ChildNodes[4].ChildNodes.Count > 0 && parseNode.ChildNodes[4].ChildNodes[0].FindTokenAndGetText() == LingoGrammar.Keywords.Validate)
            {
                ValidationVariable = parseNode.ChildNodes[4].ChildNodes[2].AstNode as IdentifierAst;
                ValidationCodeBlock = AddChild(parseNode.ChildNodes[4].ChildNodes[3]) as StatementBlockAst;
            }
        }

        #endregion

        #region IInstruction<LingoProgram> Members

        public override void Reset(ILingoProgram program, bool recurse)
        {
            if (recurse)
                base.Reset(program, recurse);

            // Initialise any state variables in the program now
            PrivateVariable<int>(program, "CurrentIndex").Value = 0;
            PrivateVariable<List<IPresentableSource>>(program, "Presentables").Value = null;
            PrivateVariable<CustomValidator>(program, "ValidationInstance").Value = null;
        }

        public override InstructionResult Execute(IContextContainer context)
        {
            base.PreExecute(context);
            var program = context.Get<ILingoProgram>();
            var currentIndex = PrivateVariable<int>(program, "CurrentIndex");
            var presentables = PrivateVariable<List<IPresentableSource>>(program, "Presentables");
            var validationInstance = PrivateVariable<CustomValidator>(program, "ValidationInstance");

            // Evaluate the list of Presentables 1st time through
            if (presentables.Value == null)
            {
                program.Do(new SetVariableCommand<List<IPresentableSource>>() { Variable = presentables, NewValue = new List<IPresentableSource>() });
                ContextContainer presentingContext = new ContextContainer(context);
                presentingContext.Set<bool>("Presenting", true);
                object eval = Evaluatable.Evaluate(presentingContext);
                IEnumerable<object> oList = eval as IEnumerable<object>;
                if (oList != null)
                {
                    foreach (object o in oList)
                    {
                        if (o is IPresentable && (o as IPresentable).Enabled)
                            presentables.Value.Add(o as IPresentableSource);
                    }
                }
                else if (eval is IPresentable && (eval as IPresentable).Enabled)
                    presentables.Value.Add(eval as IPresentableSource);
            }

            if (validationInstance.Value == null)
            {
                if (currentIndex.Value < presentables.Value.Count)
                {
                    IPresentable presentable = null;
                    string nameAs = null;
                    presentable = presentables.Value[currentIndex.Value].GetPresentable(program, ref nameAs);

                    // Ok, present the next Question
                    if (Hidden == false)
                    {
                        program.Do(new SetVariableCommand<RunMode>() { Variable = program.RunMode, NewValue = RunMode.Presenting });
                    }

                    PresentCommand presentCommand = new PresentCommand() { Presentable = presentable, NameAs = nameAs, Hidden = Hidden };
                    program.Do(presentCommand);

                    // If no Validation CodeBlock then we can move onto next Question always ..
                    if (ValidationCodeBlock == null)
                        program.Do(new SetVariableCommand<int>() { Variable = currentIndex, NewValue = currentIndex.Value + 1 });
                    else
                    {
                        program.Do(new SetVariableCommand<CustomValidator> { Variable = validationInstance, NewValue = new CustomValidator() { PresentCommand = presentCommand } });
                        ValidationVariable.Assign(context, validationInstance.Value);
                    }

                    return InstructionResult.Unfinished;
                }

                return InstructionResult.Finished;    // Statement finished
            }
            else
            {
                //                if (validationInstance.Value.ValidatorResult == null)
                validationInstance.Value.ValidatorResult = context.Get<IValidatorResult>();

                if (ValidationCodeBlock.Execute(context) == InstructionResult.Finished)
                {
                    // Reset the codeblock so it can be run again
                    ValidationCodeBlock.Reset(program, true);

                    // If the validationInstance has no InvalidReasons then we can move onto next question
                    if (!validationInstance.Value.Invalid)
                        program.Do(new SetVariableCommand<int>() { Variable = currentIndex, NewValue = currentIndex.Value + 1 });

                    program.Do(new SetVariableCommand<CustomValidator>() { Variable = validationInstance, NewValue = null });
                }

                return InstructionResult.Unfinished;
            }
        }

        #endregion

        public object Evaluate(IContextContainer context)
        {
            //If we're in the evaluate branch, that means we're running as an RValue.
            //This means we should, rather than halting lingo execution and signaling a presentable,
            //create and render a presentable ourselves and return a string of HTML text.

            //We are going to do a similar logic to the Execute() function, except there is no need
            //to keep a complex state machine since we will not be yielding to the environment

            var program = context.Get<ILingoProgram>();
            var workingDocument = context.Get<IWorkingDocument>();
            var renderer = context.Get<IPlayerStringRenderer>();
            ContextContainer presentingContext = new ContextContainer(context);
            presentingContext.Set<bool>("Presenting", true);
            presentingContext.Set<bool>("Presenting.Evaluate", true); // in case any one needs to do something different between a normal present and one where its part of an evaluation
            object eval = Evaluatable.Evaluate(presentingContext);
            IEnumerable<object> oList = eval as IEnumerable<object>;

            var presentables = new List<IPresentableSource>();
            if (oList != null)
            {
                foreach (var o in oList)
                {
                    var presO = o as IPresentable;
                    if (presO != null && presO.Enabled)
                        presentables.Add(o as IPresentableSource);
                }
            }
            else
            {
                var presO = eval as IPresentable;
                if (presO != null && presO.Enabled)
                {
                    presentables.Add(eval as IPresentableSource);
                }
            }

            //Now we enumerate our presentable sources...
            var retList = new List<string>();
            foreach (var presentableSource in presentables)
            {
                string nameAs = null;

                IPresented presented = null;

                if(Again == true)
                {
                    var candidates = from p in workingDocument.PresentedAsDepthFirstEnumerable() where (p is IPresentedSection && ((IPresentedSection)p).Section == presentableSource) || (p is IPresentedQuestion && ((IPresentedQuestion)p).Question == presentableSource) select p;

                    if (candidates.Count() > 0)
                    {
                        // if its = 0, then it'll fall through to the logic below this and get presented normally
                        presented = candidates.First();
                    }
                }

                if(Again == false || presented == null)
                {
                    // we haven't been requested to represent the same presentable again, so we create things from scratch
                    IPresentable presentable = presentableSource.GetPresentable(program, ref nameAs);
                    presented = presentable.CreatePresented(workingDocument, nameAs, workingDocument);
                }
                
                retList.Add(renderer.RenderIntoString(presented, workingDocument, presentingContext));
            }
            if (retList.Count == 0)
                return "";
            else if (retList.Count == 1)
                return retList[0];
            else
                return retList;

        }
    }
}
