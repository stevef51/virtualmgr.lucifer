﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Commands.Runtime;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;
using Irony.Ast;

namespace ConceptCave.Checklist.Lingo
{
    public class AssignmentAst : LingoInstruction
    {
        private IAssignable _leftHS;
        private IEvaluatable _rightHS;

        public override void Init(AstContext context, Irony.Parsing.ParseTreeNode parseTreeNode)
        {
            base.Init(context, parseTreeNode);

            _leftHS = AddChild(parseTreeNode.ChildNodes[0]) as IAssignable;
            _rightHS = AddChild(parseTreeNode.ChildNodes[2]) as IEvaluatable;
        }

        public override Interfaces.InstructionResult Execute(IContextContainer context)
        {
            base.PreExecute(context);
            _leftHS.Assign(context, _rightHS.Evaluate(context));
            return InstructionResult.Finished;
        }
    }
}
