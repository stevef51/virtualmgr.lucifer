﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Core;
using ConceptCave.Checklist.Interfaces;
using Irony.Ast;

namespace ConceptCave.Checklist.Lingo
{
    public class SectionListAst : LingoAst, IEvaluatable
    {
        public bool All { get; private set; }
        public IEvaluatable Index { get; private set; }

        public override void Init(AstContext context, Irony.Parsing.ParseTreeNode parseTreeNode)
        {
            base.Init(context, parseTreeNode);

            if (parseTreeNode.ChildNodes[0].FindTokenAndGetText() == LingoGrammar.Keywords.All)
                All = true;
            else
                Index = parseTreeNode.ChildNodes[1].AstNode as IEvaluatable;
        }

        #region IEvaluatable Members

        public object Evaluate(ConceptCave.Core.IContextContainer context)
        {
            IWorkingDocument workingDocument = context.Get<IWorkingDocument>();
            if (All)
                return workingDocument.Sections.ToArray();
            else
            {
                ContextContainer sectionContext = new ContextContainer(context);
                sectionContext.Set<int>("StartRange", 1);
                sectionContext.Set<int>("EndRange", workingDocument.Sections.Count);
                sectionContext.Set<bool>("FlattenList", true);

                object eval = Index.Evaluate(sectionContext);

                IEnumerable<object> enumerableEval = eval as IEnumerable<object>;
                if (enumerableEval != null)
                {
                    List<ISection> sections = new List<ISection>();
                    foreach (object index in enumerableEval)
                    {
                        ISection section = workingDocument.Sections.Lookup(index);
                        if (section != null)
                            sections.Add(section);
                    }

                    return sections.ToArray();
                }

                return workingDocument.Sections.Lookup(eval);
            }
        }

        #endregion
    }
}
