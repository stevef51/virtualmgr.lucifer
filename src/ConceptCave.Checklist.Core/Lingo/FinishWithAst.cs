﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;
using Irony.Ast;
using Irony.Parsing;

namespace ConceptCave.Checklist.Lingo
{
    public class FinishWithAst : LingoAst
    {
        public StatementBlockAst CodeBlock { get; set; }

        public override void Init(AstContext context, ParseTreeNode parseTreeNode)
        {
            base.Init(context, parseTreeNode);
            CodeBlock = (StatementBlockAst)parseTreeNode.GetMappedChildNodes()[1].AstNode;
            AddChild(parseTreeNode.GetMappedChildNodes()[1]);
        }

        public InstructionResult Execute(IContextContainer context)
        {
            return CodeBlock.Execute(context);
        }
    }
}
