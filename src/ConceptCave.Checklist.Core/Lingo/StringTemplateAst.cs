﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Irony.Parsing;
using Irony.Interpreter;
using ConceptCave.Checklist.Interfaces;
using Irony;
using ConceptCave.Core;
using Irony.Ast;

namespace ConceptCave.Checklist.Lingo
{
    public class StringTemplateAst : LingoAst, IEvaluatable
    {
        public StringTemplateExpander Expander { get; private set; }

        public override void Init(AstContext context, ParseTreeNode treeNode)
        {
            base.Init(context, treeNode);

            Expander = new StringTemplateExpander(Location, context, treeNode);
        }

        public object Evaluate(IContextContainer context)
        {
            return Expander.Evaluate(context);
        }
    }
}
