﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using Irony;
using Irony.Parsing;

namespace ConceptCave.Checklist.Lingo
{
    public class LingoException : ApplicationException
    {
        public LingoAst Ast { get; set; }

        public LingoProgramDefinition.Instance Program { get; set; }

        public LingoProgramDefinition ProgramDefinition { get; set; }
        public LogMessageList ParserMessages { get; set; }

        public LingoException(string message)
            : base(message)
        {
        }

        public LingoException(string message, LingoAst ast)
            : base(message)
        {
            Ast = ast;
        }

        public LingoException(string message, LingoAst ast, Exception innerException) : base(message, innerException)
        {
            Ast = ast;
        }

        public LingoException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        public LingoException(SerializationInfo serializationInfo, StreamingContext streamingContext)
            : base(serializationInfo, streamingContext)
        {
        }

        public LingoException(LingoProgramDefinition definition, LogMessageList parserMessageList)
            : base("Syntax Error")
        {
            ProgramDefinition = definition;
            ParserMessages = parserMessageList;
        }

        public LingoException()
            : base()
        {
        }

        private static string MakeMessage(LogMessageList parserMessageList)
        {
            StringBuilder sb = new StringBuilder();
            parserMessageList.ForEach(i => sb.AppendFormat("{0} @{1} - {2}\n", i.Level, i.Location.ToUiString(), i.Message));
            return sb.ToString();
        }
    }
}
