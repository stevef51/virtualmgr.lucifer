﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Irony.Ast;
using Irony.Parsing;

namespace ConceptCave.Checklist.Lingo
{
    public class SelectAsAst : LingoAst
    {
        public string AsName { get; set; }
        public override void Init(AstContext context, ParseTreeNode parseTreeNode)
        {
            base.Init(context, parseTreeNode);
            try
            {
                //our 2nd child is the literal we're naming this thing
                if (parseTreeNode.GetMappedChildNodes().Count > 0)
                    AsName = parseTreeNode.GetMappedChildNodes()[1].Token.ValueString;
                else
                    AsName = null;
            }
            catch (ArgumentOutOfRangeException) { AsName = null; }
            catch (IndexOutOfRangeException) { AsName = null; }
            
        }
    }
}
