﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Irony.Parsing;
using Irony.Interpreter;
using ConceptCave.Checklist.Interfaces;
using Irony;
using ConceptCave.Core;
using Irony.Ast;

namespace ConceptCave.Checklist.Lingo
{

    //  Implements Ruby-like active strings with embedded expressions

    /* Example of use:
 
          //String literal with embedded expressions  ------------------------------------------------------------------
          var stringLit = new StringLiteral("string", "\"", StringOptions.AllowsAllEscapes | StringOptions.IsTemplate);
          stringLit.AstNodeType = typeof(StringTemplateNode);
          var Expr = new NonTerminal("Expr"); 
          var templateSettings = new StringTemplateSettings(); //by default set to Ruby-style settings 
          templateSettings.ExpressionRoot = Expr; //this defines how to evaluate expressions inside template
          this.SnippetRoots.Add(Expr);
          stringLit.AstNodeConfig = templateSettings;
        
          //define Expr as an expression non-terminal in your grammar
  
     */


    public class StringTemplateExpander
    {
        #region embedded classes
        enum SegmentType
        {
            Text,
            Expression
        }
        class TemplateSegment
        {
            public SegmentType Type;
            public string Text;
            public IEvaluatable ExpressionNode;
            public int Position; //Position in raw text of the token for error reporting
            public TemplateSegment(string text, IEvaluatable node, int position)
            {
                Type = node == null ? SegmentType.Text : SegmentType.Expression;
                Text = text;
                ExpressionNode = node;
                Position = position;
            }
        }
        class SegmentList : List<TemplateSegment> { }
        #endregion

        string _template;
        string _tokenText; //used for locating error 
        StringTemplateSettings _templateSettings; //copied from Terminal.AstNodeConfig 
        SegmentList _segments = new SegmentList();
        public SourceLocation Location { get; private set; }
        private Action<string> LogError = null;

        public StringTemplateExpander(SourceLocation location, AstContext context, ParseTreeNode treeNode)
        {
            Location = location;
            _template = treeNode.Token.ValueString;
            _tokenText = treeNode.Token.Text;
            _templateSettings = treeNode.Term.AstConfig.Data as StringTemplateSettings;
            LogError = (msg) =>
            {
                context.AddMessage(ErrorLevel.Error, Location, msg);
            };
            ParseSegments(context.Language);
        }

        public StringTemplateExpander(ParsingContext context, string template, StringTemplateSettings settings)
        {
            Location = new SourceLocation();
            _template = template;
            _tokenText = template;
            _templateSettings = settings;
            LogError = (msg) =>
            {
                context.AddParserMessage(ErrorLevel.Error, Location, msg);
            };
            ParseSegments(context.Language);
        }

        public string Evaluate(IContextContainer context)
        {
            return BuildString(context, s => s);
        }
        public string Evaluate(IContextContainer context, Func<string, string> fnEscape)
        {
            return BuildString(context, fnEscape);
        }

        private void ParseSegments(LanguageData language)
        {
            var exprParser = new Parser(language, _templateSettings.ExpressionRoot);
            // As we go along the "value text" (that has all escapes done), we track the position in raw token text  in the variable exprPosInTokenText.
            // This position is position in original text in source code, including original escaping sequences and open/close quotes. 
            // It will be passed to segment constructor, and maybe used later to compute the exact position of runtime error when it occurs. 
            int currentPos = 0, exprPosInTokenText = 0;
            while (true)
            {
                var startTagPos = _template.IndexOf(_templateSettings.StartTag, currentPos);
                if (startTagPos < 0)
                    startTagPos = _template.Length;

                var text = _template.Substring(currentPos, startTagPos - currentPos);
                if (!string.IsNullOrEmpty(text))
                    _segments.Add(new TemplateSegment(text, null, 0)); //for text segments position is not used

                if (startTagPos >= _template.Length)
                    break; //from while

                //We have a real start tag, grab the expression
                currentPos = startTagPos + _templateSettings.StartTag.Length;
                var endTagPos = _template.IndexOf(_templateSettings.EndTag, currentPos);
                if (endTagPos < 0)
                {
                    LogError(Resources.ErrNoEndTagInEmbExpr);
                    //"No ending tag '{0}' found in embedded expression."
                    return;
                }

                var exprText = _template.Substring(currentPos, endTagPos - currentPos);
                if (!string.IsNullOrEmpty(exprText))
                {
                    //parse the expression
                    //_expressionParser.context.Reset(); 

                    var exprTree = exprParser.Parse(exprText);
                    if (exprTree.HasErrors())
                    {
                        //we use original search in token text instead of currentPos in template to avoid distortions caused by opening quote and escaped sequences
                        var baseLocation = Location + _tokenText.IndexOf(exprText);
                        foreach (var msg in exprTree.ParserMessages)
                        {
                            LogError(Resources.ErrInvalidEmbeddedPrefix + msg.Message);
                        }
                        return;
                    }

                    //add the expression segment
                    exprPosInTokenText = _tokenText.IndexOf(_templateSettings.StartTag, exprPosInTokenText) + _templateSettings.StartTag.Length;
                    var segmNode = exprTree.Root.AstNode as IEvaluatable;
                    _segments.Add(new TemplateSegment(null, segmNode, exprPosInTokenText));

                    //advance position beyond the expression
                    exprPosInTokenText += exprText.Length + _templateSettings.EndTag.Length;

                }//if
                currentPos = endTagPos + _templateSettings.EndTag.Length;
            }//while
        }

        private string BuildString(IContextContainer context, Func<string, string> fnEscape)
        {
            string[] values = new string[_segments.Count];
            for (int i = 0; i < _segments.Count; i++)
            {
                var segment = _segments[i];
                switch (segment.Type)
                {
                    case SegmentType.Text:
                        values[i] = segment.Text;
                        break;
                    case SegmentType.Expression:
                        values[i] = fnEscape(EvaluateExpression(context, segment));
                        break;
                }//else
            }//for i

            var result = string.Join(string.Empty, values);
            return result;
        }//method

        private string EvaluateExpression(IContextContainer context, TemplateSegment segment)
        {
            //try
            {
                var value = segment.ExpressionNode.Evaluate(context);
                return value == null ? string.Empty : value.ToString();
            }/*
            catch
            {
                //We need to catch here and set current node; ExpressionNode may have reset it, and location would be wrong
                //TODO: fix this - set error location to exact location inside string. 
                //                thread.CurrentNode = this;
                throw;
            }*/

        }
    }//class
}
