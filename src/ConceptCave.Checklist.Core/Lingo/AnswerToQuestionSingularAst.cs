﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Irony.Parsing;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;
using Irony.Ast;

namespace ConceptCave.Checklist.Lingo
{
    public class AnswerToQuestionSingularAst : LingoAst, IEvaluatable
    {
        public IEvaluatable AnswerIndex { get; private set; }
        public IEvaluatable Sequence { get; private set; }

        public override void Init(AstContext context, ParseTreeNode treeNode)
        {
            base.Init(context, treeNode);

            // Can be
            //
            // answer 1    -> (last) Answer to Question 1 
            // answer 1#0  -> same as above
            // answer 1#1  -> first Answer to Question 1
            // answer 1#-1 -> Penultemate Answer to Question 1
            // howold#2    -> second Answer to Question alias As 'howold'


            if (treeNode.ChildNodes[0].FindTokenAndGetText() == LingoGrammar.Keywords.Answer)
            {
                AnswerIndex = treeNode.ChildNodes[1].AstNode as IEvaluatable;
                if (treeNode.ChildNodes.Count >= 4)
                    Sequence = treeNode.ChildNodes[3].AstNode as IEvaluatable;
            }
        }

        public object Evaluate(IContextContainer context)
        {
            IWorkingDocument workingDocument = context.Get<IWorkingDocument>();
            object answerIndex = AnswerIndex.Evaluate(context);

            int sequence = 0;
            if (Sequence != null)
                sequence = Convert.ToInt32(Sequence.Evaluate(context));

            // 0 or -ve numbers start from the last Answer, 1 and +ve numbers start from the 1st Answer
            if (sequence > 0)
                sequence -= 1;          // They are 0 based
            else
                sequence = workingDocument.Answers.AnswerCount(answerIndex) - 1 + sequence;

            IAnswer answer = workingDocument.Answers.AnswerToQuestion(answerIndex, sequence);
            if (answer != null)
                return answer.AnswerValue;

            return null;
        }
    }
}
