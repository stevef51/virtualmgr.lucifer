﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Irony.Parsing;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;
using ConceptCave.Core.Commands;
using ConceptCave.Checklist.Commands.Runtime;
using Irony.Interpreter;
using ConceptCave.Core.Coding;

namespace ConceptCave.Checklist.Lingo
{
    // The LingoProgram will execute a single instruction at a time.  Some instructions do not advance the ProgramCounter after
    // being Executed ("Present Question 1, 2" will execute twice for example), so each Instruction returns whether it is Finished or Unfinished
    // the parent block of each Instruction is responsible for tracking which is the current Instruction and moving it on when the 
    // current one finishes.  So a program with multiple levels of StatementBlocks will have multiple ProgramCounters (1 for each depth of StatementBlock)
    public partial class LingoProgramDefinition
    {

        public class Instance : Node, ILingoProgram
        {
            private ContextContainer _contextContainer = new ContextContainer();

            public ILingoProgramDefinition ProgramDefinition
            {
                get { return _programDefinition; }
            } LingoProgramDefinition _programDefinition;

            public IWorkingDocument WorkingDocument
            {
                get { return _workingDocument; }
            } IWorkingDocument _workingDocument;

            public IVariable<RunMode> RunMode
            {
                get { return _runMode; }
            } IVariable<RunMode> _runMode;

            //Whether or not the RunMode is Finishing because we ran off the bottom of the program
            public bool IsAutoFinishing { get; private set; }

            public LingoInstruction CurrentInstruction { get; internal set; }

            public void Do(ICommand command)
            {
                _workingDocument.CommandManager.Do(command);
            }

            public Instance()
            {
                IsAutoFinishing = false;
            }

            public Instance(LingoProgramDefinition definition, IWorkingDocument document) 
            {
                _programDefinition = definition;
                _workingDocument = document;
                _runMode = _workingDocument.Variables.Variable<RunMode>(".RunMode", Interfaces.RunMode.Running);
                IsAutoFinishing = false;

                definition.AllInstructions.ForEach(i => i.Reset(this, false));
                
                PostSetup();
            }

            private void PostSetup()
            {
                LingoLanguageRuntime languageRuntime = new LingoLanguageRuntime(LingoGrammar.LanguageData);
                _contextContainer.Set<LanguageRuntime>(languageRuntime);
                _contextContainer.Set<LingoLanguageRuntime>(languageRuntime);
                _contextContainer.Set<ILingoProgram>(this);
                _contextContainer.Set<IWorkingDocument>(_workingDocument);
                _contextContainer.Set<IVariableBag>("Set", _workingDocument.Variables);
                _contextContainer.Set<IVariableBag>("Get", _workingDocument.Variables);

                //Need to inject the globals at the root of the variable bag chain
                var rootbag = _workingDocument.Variables;
                while (rootbag.Parent != null)
                    rootbag = rootbag.Parent;
                rootbag.Parent = new LingoConstantsBag();
            }
                          

            /// <summary>
            /// Runs Instructions until one of them puts the Program into Waiting RunMode
            /// </summary>
            /// <returns>Finished (Program has ended) or Unfinished</returns>
            public InstructionResult RunUntilStopped()
            {
                try
                {
                    return RunUntilStopped(-1);
                }
                catch (LingoException e)
                {
                    if (e.Ast == null)
                    {
                        throw;
                    }

                    e.Program = this;
                    e.ProgramDefinition = (LingoProgramDefinition) this.ProgramDefinition;

                    throw;
                }
                catch (Exception e)
                {
                    var le = new LingoException("Unexpected error occured during execution", this.CurrentInstruction, e);
                    le.Program = this;
                    le.ProgramDefinition =  (LingoProgramDefinition) this.ProgramDefinition;
                    throw le;
                }
            }

            /// <summary>
            /// Runs Instructions until one of them puts the Program into Waiting RunMode
            /// </summary>
            /// <param name="maxIterations">Maximum number of Instruction iterations</param>
            /// <returns>Finished (Program has ended) or Unfinished</returns>
            public InstructionResult RunUntilStopped(int maxIterations)
            {
                InstructionResult result = InstructionResult.Unfinished;
                var programRoot = _programDefinition.ProgramRoot;
                
                while (result == InstructionResult.Unfinished && _runMode.Value == Interfaces.RunMode.Running && maxIterations != 0)
                {
                    result = programRoot.Execute(this);

                    if (maxIterations != -1)
                        maxIterations--;
                }

                if (result == InstructionResult.Finished)
                {
                    //Fell off the bottom
                    _runMode.Value = Interfaces.RunMode.Finishing;
                    IsAutoFinishing = true;
                }   

                return result;
            }

            public InstructionResult RunFinisher()
            {
                try
                {
                    if (RunMode.Value != Interfaces.RunMode.Finishing)
                        throw new InvalidOperationException("Finishers can only be run when a working document is in a Finishing state");
                    if (IsAutoFinishing) //don't need to do anything if we rean off the bottom
                        return InstructionResult.Finished;
                    //one more programRoot.Execute() will run the finisher, since it's queued up as the next action
                    var programRoot = _programDefinition.ProgramRoot;
                    var result = InstructionResult.Unfinished;

                    while (result == InstructionResult.Unfinished && _runMode.Value == Interfaces.RunMode.Finishing)
                        result = programRoot.Execute(this);

                    return result;
                }
                catch (LingoException e)
                {
                    if (e.Ast == null)
                    {
                        throw;
                    }

                    e.Program = this;
                    e.ProgramDefinition = (LingoProgramDefinition)this.ProgramDefinition;

                    throw;
                }
                catch (Exception e)
                {
                    throw new LingoException("Unexpected error occured during execution", this.CurrentInstruction, e);
                }
            }

            /// <summary>
            /// Creates or returns a Variable prepared for an Instruction to use across indefinite sessions
            /// </summary>
            /// <typeparam name="T">Type of primitive variable</typeparam>
            /// <param name="instruction">Requesting Instruction </param>
            /// <param name="key">Instructions private storage key</param>
            /// <returns></returns>
            public IVariable<T> PrivateVariable<T>(ILingoInstruction instruction, string key, T defaultValue)
            {
                return _workingDocument.Variables.Variable<T>(string.Format(".{0}.{1}", instruction.Address, key), defaultValue);
            }

            public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
            {
                base.Encode(encoder);

                encoder.Encode("ProgramDefinition", _programDefinition);
                encoder.Encode("WorkingDocument", _workingDocument);
                encoder.Encode("RunMode", _runMode);
                encoder.Encode("IsAutoFinishing", IsAutoFinishing);
            }

            public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
            {
                base.Decode(decoder);

                _programDefinition = decoder.Decode<LingoProgramDefinition>("ProgramDefinition", null);
                _workingDocument = decoder.Decode<IWorkingDocument>("WorkingDocument", null);
                _runMode = decoder.Decode<IVariable<RunMode>>("RunMode", null);
                IsAutoFinishing = decoder.Decode<bool>("IsAutoFinishing", false);
                PostSetup();
            }

            public string ExpandString(string template)
            {
                return ExpandString(template, s => s);
            }

            public string ExpandString(string template, Func<string, string> fnEscape)
            {
                if (template == null)
                    return null;

                try
                {
                    Parser parser = new Parser(LingoGrammar.LanguageData, _programDefinition.LingoGrammar.ExpressionRoot);
                    StringTemplateSettings settings = _programDefinition.LingoGrammar.StringTemplateSettings;

                    StringTemplateExpander expander = new StringTemplateExpander(parser.Context, template, _programDefinition.LingoGrammar.StringTemplateSettings);
                    return expander.Evaluate(this, fnEscape);
                }
                catch (Exception e)
                {
                    throw new ExpandStringException(template, this, e);
                }

            }

            #region IContextContainer Members

            public bool Has<T>(string name)
            {
                return _contextContainer.Has<T>(name);
            }

            public T Get<T>(string name)
            {
                return _contextContainer.Get<T>(name);
            }

            public void Set<T>(string name, T context)
            {
                _contextContainer.Set<T>(name, context);
            }

            public void Set<T>(T context)
            {
                _contextContainer.Set<T>(context);
            }

            public void Set(Type T, string name, object context)
            {
                _contextContainer.Set(T, name, context);
            }

            public void CopyTo(IContextContainer target)
            {
                _contextContainer.CopyTo(target);
            }

            public bool Has<T>()
            {
                return _contextContainer.Has<T>();
            }

            public T Get<T>()
            {
                return _contextContainer.Get<T>();
            }

            public T Get<T>(Func<T> defaultValueCallback)
            {
                return _contextContainer.Get<T>(defaultValueCallback);
            }

            public T Get<T>(string name, Func<T> defaultValueCallback)
            {
                return _contextContainer.Get<T>(name, defaultValueCallback);
            }

            #endregion
        }
    }
}
