﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Commands.Runtime;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;
using ConceptCave.Checklist.Lingo.ObjectProviders;
using Irony.Ast;

namespace ConceptCave.Checklist.Lingo
{
    public class IdentifierAst : LingoAst, IAssignable
    {
        public string Name { get; private set; }
        private IObjectSetter _objectProvider = new IdentifierObjectProvider();

        public override void Init(AstContext context, Irony.Parsing.ParseTreeNode parseTreeNode)
        {
            base.Init(context, parseTreeNode);

            Name = parseTreeNode.FindTokenAndGetText();
        }

        #region IAssignable Members

        public void Assign(IContextContainer context, object value)
        {
            _objectProvider.SetObject(context, Name, value);
        }

        #endregion

        #region IEvaluatable Members

        public object Evaluate(IContextContainer context)
        {
            object result;
            if (!_objectProvider.GetObject(context, Name, out result))
                return null;

            return result;
        }

        #endregion
    }
}
