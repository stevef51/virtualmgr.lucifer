﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using Irony.Ast;

namespace ConceptCave.Checklist.Lingo
{
    public class ProgramBlockAst : StatementBlockAst
    {
        private List<ILingoInstruction> _flattenedInstructions = new List<ILingoInstruction>();
        public List<ILingoInstruction> AllInstructions
        {
            get { return _flattenedInstructions; }
        }

        public override void Init(AstContext context, Irony.Parsing.ParseTreeNode parseNode)
        {
            base.Init(context, parseNode);

            Visitor<LingoAst> visitor = new Visitor<LingoAst>();
            visitor.BeginVisit = v =>
            {
                if (v is ILingoInstruction)
                    _flattenedInstructions.Add(v as ILingoInstruction);
            };

            ((IVisitee)parseNode.AstNode).AcceptVisitor(visitor, () => true);       // Recurse deeply to flatten all sub instructions
        }
    }
}
