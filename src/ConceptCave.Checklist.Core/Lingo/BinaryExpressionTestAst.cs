﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Reflection;
using Irony.Interpreter;
using Irony.Parsing;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;
using Irony.Ast;
using Irony.Interpreter.Ast;

namespace ConceptCave.Checklist.Lingo
{
    public class BinaryExpressionTestAst : LingoAst, IBooleanTest
    {
        public IEvaluatable Right;
        public string OpSymbol;
        public ExpressionType Op;
        private OperatorImplementation _lastUsed;

        public BinaryExpressionTestAst() { }

        public override void Init(AstContext context, ParseTreeNode treeNode)
        {
            base.Init(context, treeNode);
            Right = AddChild(treeNode.GetMappedChildNodes()[1]) as IEvaluatable;
            var opToken = treeNode.GetMappedChildNodes()[0].FindToken();
            OpSymbol = opToken.Text;
            Op = ((InterpreterAstContext)context).OperatorHandler.GetOperatorExpressionType(OpSymbol);
            // Set error anchor to operator, so on error (Division by zero) the explorer will point to 
            // operator node as location, not to the very beginning of the first operand.
//            ErrorAnchor = opToken.Location;
        }

        #region IBooleanTest Members

        public bool Test(object leftValue, IContextContainer context)
        {
            switch (Op)
            {
                case ExpressionType.AndAlso:
                    if (!IsTrue(leftValue))
                        return true; //if false return immediately
                    return IsTrue(Right.Evaluate(context));

                case ExpressionType.OrElse:
                    if (IsTrue(leftValue))
                        return true;
                    return IsTrue(Right.Evaluate(context));
            }

            var rightValue = Right.Evaluate(context);
            
            if (leftValue == null && rightValue == null)
                return true;
            
            if (leftValue == null || rightValue == null)
                return false;

            LanguageRuntime runTime = context.Get<LanguageRuntime>("");
            return IsTrue(runTime.ExecuteBinaryOperator(Op, leftValue.ConvertToPrimitive(), rightValue.ConvertToPrimitive(), ref _lastUsed));
        }

        #endregion
    }
}
