﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;

namespace ConceptCave.Checklist.Lingo
{
    public class VariableBag : Node, IVariableBag
    {
        protected Dictionary<string, IVariable> _variables = new Dictionary<string, IVariable>();
        public bool IgnoreCase { get; private set; }

        public VariableBag() : this(null, true)
        {
        }

        public VariableBag(IVariableBag parent = null, bool ignoreCase = true)
        {
            Parent = parent;
            IgnoreCase = ignoreCase;
        }

        public IVariableBag Parent { get; set; }

        public virtual IVariable<T> Variable<T>(string name)
        {
            IVariable result = null;
            if (!_variables.TryGetValue(IgnoreCase ? name.ToLowerInvariant() : name, out result))
                if (Parent != null)
                    result = Parent.Variable<T>(name);

			return result as IVariable<T>;
			//return (IVariable<T>)result;
        }

        public virtual IVariable<T> Variable<T>(string name, T defaultCreateValue)
        {
            string nameKey = IgnoreCase ? name.ToLowerInvariant() : name;
            IVariable result = null;
            if (_variables.TryGetValue(nameKey, out result))
                return (IVariable<T>)result;

            // Not found, check parent
            if (Parent != null)
                result = Parent.Variable<T>(name);          // Note Parent may not be case sensitive

            if (result == null)
            {
                result = new Variable<T>(name) { Value = defaultCreateValue };
                _variables[nameKey] = result;
            }

            return (IVariable<T>)result;
        }

        public virtual void Clear()
        {
            _variables.Clear();
        }

        public Dictionary<string, IVariable> Items
        {
            get
            {
                return _variables;
            }
        }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("Variables", _variables);
            if (Parent != null)
                encoder.Encode("Parent", Parent);
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            _variables = decoder.Decode<Dictionary<string, IVariable>>("Variables", null);
            Parent = decoder.Decode<IVariableBag>("Parent", null);
        }

        public IEnumerator<IVariable> GetEnumerator()
        {
            foreach (var v in _variables.Values) //loop through me
            {
                yield return v;
            }
            if (Parent != null)
            {
                foreach (var v in Parent) //then through parent
                {
                    yield return v;
                }
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
