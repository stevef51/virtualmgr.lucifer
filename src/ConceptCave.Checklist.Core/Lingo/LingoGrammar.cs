﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Irony.Parsing;
using Irony.Interpreter.Ast;
using Irony.Interpreter;
using Irony.Interpreter.Evaluator;
using ConceptCave.Checklist.Interfaces;
//using ConceptCave.Checklist.Reporting.Datastores;
using Irony.Ast;
using ConceptCave.Checklist.Reporting.LingoQuery;

namespace ConceptCave.Checklist.Lingo
{
    public class LingoGrammar : InterpretedLanguageGrammar
    {
        public static class Keywords
        {
            public const string Present = "present";
            public const string Hidden = "hidden";
            public const string Again = "again";
            public const string ForPrint = "forprint";
            public const string Question = "question";
            public const string Validate = "validate";
            public const string Answer = "answer";
            public const string Questions = "questions";
            public const string All = "all";
            public const string Section = "section";
            public const string Sections = "sections";
            public const string When = "when";
            public const string Is = "is";
            public const string Not = "not";
            public const string Otherwise = "otherwise";
            public const string As = "as";
            public const string End = "end";
            public const string In = "in";
            public const string Contains = "contains";
            public const string Loop = "loop";
            public const string With = "with";
            public const string Trigger = "trigger";
            public const string Action = "action";
            public const string WaitFor = "wait";
            public const string Finish = "finish";
            public const string True = "true";
            public const string False = "false";
            public const string Null = "null";
            public const string While = "while";
            public const string Do = "do";

            //query keywords
            public const string From = "from";
            public const string Where = "where";
            public const string Orderby = "orderby";
            public const string Select = "select";
            public const string Between = "between";
            public const string Ascending = "ascending";
            public const string Descending = "descending";
            public const string Display = "display";
            public const string This = "this";
            //query data stores
            public const string DBChecklists = "dbchecklists";
            public const string DBQuestions = "dbquestions";
            public const string DBScoredChecklists = "dbscoredchecklists";
            public const string DBThisChecklist = "dbthischecklist";
        }

        public NonTerminal ExpressionRoot { get; private set; }
        public NonTerminal ExpressionList { get; private set; }
        public NonTerminal QuestionList { get; private set; }
        public NonTerminal SectionList { get; private set; }
        public NonTerminal ActionList { get; private set; }
        public NonTerminal IntegerRange { get; private set; }
        public StringTemplateSettings StringTemplateSettings { get; private set; }

        public LingoGrammar() : base(false) // case insensititve
        {
            this.GrammarComments = "Lingo Grammar";

            this.UsesNewLine = true;

            //Terminals
            var integer = new NumberLiteral("Integer", NumberOptions.IntOnly | NumberOptions.AllowSign);
            var number = new NumberLiteral("Number", NumberOptions.Default);
            //ints that are too long for int32 are converted to int64
            number.DefaultIntTypes = new TypeCode[] {TypeCode.Int32, TypeCode.Int64};
            number.DefaultFloatType = TypeCode.Decimal;
            number.AddExponentSymbols("dD", TypeCode.Double);
            number.AddSuffix("d", TypeCode.Double);
            number.AddSuffix("D", TypeCode.Double);

            var stringLiteral = new StringLiteral("String", "\"",
                StringOptions.AllowsAllEscapes | StringOptions.IsTemplate);
            stringLiteral.AddStartEnd("'", StringOptions.AllowsAllEscapes | StringOptions.IsTemplate);
            stringLiteral.AstConfig.NodeType = typeof (StringTemplateAst);
            var expr = new NonTerminal("ExpressionSingular", typeof (ExpressionAst));
            ExpressionRoot = expr;
            StringTemplateSettings = new StringTemplateSettings(); //by default set to Ruby-style settings 
            StringTemplateSettings.ExpressionRoot = expr; //this defines how to evaluate expressions inside template
            this.SnippetRoots.Add(expr);
            stringLiteral.AstConfig.Data = StringTemplateSettings;

            var comment = new CommentTerminal("Comment", "//", "\n");
            comment.Flags |= TermFlags.NoAstNode;

            var comma = ToTerm(",", "comma");
            var semicolon = ToTerm(";", "semicolon");
            var lbracket = ToTerm("{");
            var rbracket = ToTerm("}");

            var identifier = new IdentifierTerminal("Identifier", IdOptions.IsNotKeyword);
            //this.   
            //this.DefaultIdentifierNodeType = typeof(IdentifierAst);

            // Non-terminals
            var program = new NonTerminal("Program", typeof (ProgramBlockAst));
            var statement = new NonTerminal("Statement");
            var statementList = new NonTerminal("StatementList", typeof (StatementBlockAst));
            var statementListOrEmpty = new NonTerminal("StatementListOrEmpty");
            //var Line_Content_Opt = new NonTerminal("Line_Content_Opt");
            // var Comment_Opt = new NonTerminal("Comment_Opt");
            var presentStatement = new NonTerminal("PresentStatement", typeof (PresentAst));
            var integerOrStringList = new NonTerminal("IntegerOrStringList");
            var integerOrString = new NonTerminal("IntegerOrString");
            var presentableQuestion = new NonTerminal("PresentableQuestionAsParam", typeof (PresentableQuestionParam));
            var presentableQuestionAsParamList = new NonTerminal("PresentableQuestionAsParamList");
            QuestionList = new NonTerminal("QuestionList", typeof (QuestionListAst));
            SnippetRoots.Add(QuestionList);

            SectionList = new NonTerminal("SectionList", typeof (SectionListAst));
            SnippetRoots.Add(SectionList);

            ActionList = new NonTerminal("ActionList", typeof (ActionListAst));
            SnippetRoots.Add(ActionList);

//            var answerSingular = new NonTerminal("AnswerSingular", typeof(AnswerToQuestionSingularAst));
            var whenStatement = new NonTerminal("When", typeof (WhenAst));
            var whenStatementBlock = new NonTerminal("WhenBlock");
            var whenOtherwise = new NonTerminal("WhenOtherwise", typeof (WhenOtherwiseAst));
            var isListWithDefault = new NonTerminal("IsListWithDefault");
            var isStatement = new NonTerminal("Is", typeof (WhenIsAst));
            var not = new NonTerminal("Not");
            var isList = new NonTerminal("IsList");
            var codeBlock = new NonTerminal("CodeBlock", typeof (StatementBlockAst));
            IntegerRange = new NonTerminal("IntegerRange", typeof (IntegerRangeAst));

            var inExpr = new NonTerminal("InExpr", typeof (InExpressionAst));
            var binExpr = new NonTerminal("BinExpr", typeof (BinaryOperationAst));
            var binOp = new NonTerminal("BinOp", "operator");
            var parExpr = new NonTerminal("ParExpr");
            var unExpr = new NonTerminal("UnExpr", typeof (UnaryOperationAst));
            var unOp = new NonTerminal("UnOp");
            var term = new NonTerminal("TermSingular");
            var assignmentTerm = new NonTerminal("AssignmentTerm");
            var binaryExprTest = new NonTerminal("BinaryExpressionTest", typeof (BinaryExpressionTestAst));
            var inExprTest = new NonTerminal("InExpressionTest", typeof (InExpressionTestAst));
            var binTest = new NonTerminal("BinaryTest");
            var parBinTest = new NonTerminal("ParBinaryTest");
            var binTestList = new NonTerminal("BinaryTestList");
            var memberAccess = new NonTerminal("MemberAccess", typeof (MemberAccessAst));

            var containsExpr = new NonTerminal("ContainsExpr", typeof (ContainsExpressionAst));
            var containsExprTest = new NonTerminal("ContainsExprTest", typeof (ContainsExpressionTestAst));

            var loopStatement = new NonTerminal("Loop", typeof (LoopAst));
            var whileDoStatement = new NonTerminal("While", typeof (WhileDoAst));
            var waitForStatement = new NonTerminal("WaitFor", typeof (WaitForAst));
            var functionCall = new NonTerminal("FunctionCall", typeof (FunctionCallAst));
            var getAttribute = new NonTerminal("GetAttribute", typeof (GetAttributeAst));

            var ArgList = new NonTerminal("ArgList", typeof (ArgListAst));
            ExpressionList = new NonTerminal("ExpressionList", typeof (ExpressionListAst));
            SnippetRoots.Add(ExpressionList);
            var assignmentStatement = new NonTerminal("Assignment", typeof (AssignmentAst));
            var assignmentStatementList = new NonTerminal("AssignmentList", typeof (AssignmentListAst));

            var finishStatement = new NonTerminal("Finish", typeof (FinishAst));
            var finishWithOrEmpty = new NonTerminal("FinishWithOrEmpty", typeof (LingoAst));
            var finishWithStatement = new NonTerminal("FinishWith", typeof (FinishWithAst));

            //Query nonterminals
            var fromExpr = new NonTerminal("From", typeof (FromAst));
            var whereClause = new NonTerminal("Where", typeof (WhereAst));
            var orderByClause = new NonTerminal("OrderBy", typeof (OrderbyAst));
            var selectClause = new NonTerminal("Select", typeof (SelectAst));
            var selectClausePlusRule = new NonTerminal("SelectPlus");
            var selectClauseItem = new NonTerminal("SelectItem", typeof (SelectItemAst));
            var selectAsClause = new NonTerminal("SelectAs", typeof (SelectAsAst));
            var selectDisplayClause = new NonTerminal("SelectDisplay", typeof (SelectDisplayAst));

            var newLineOrEmpty = new NonTerminal("NewLineOrEmpty");
            newLineOrEmpty.Rule = NewLine | Empty;

            // set the PROGRAM to be the root node of BASIC programs.
            this.Root = program;

            var statementLine = new NonTerminal("StatementLine");
            // BNF Rules
            program.Rule = MakePlusRule(program, null, statementLine);


            inExpr.Rule = expr + Keywords.In + ExpressionList;
            binExpr.Rule = expr + binOp + expr;
            binOp.Rule = ToTerm("+") | "-" | "*" | "/" | "%" | "power" | "=" | "<" | "<=" | ">" | ">=" | "!=" | "and" | "or";

            parExpr.Rule = "(" + expr + ")";
            unExpr.Rule = unOp + term + ReduceHere();
            unOp.Rule = ToTerm("+") | "-" | Keywords.Not;

            binaryExprTest.Rule = binOp + ExpressionList;
            inExprTest.Rule = Keywords.In + ExpressionList;
            binTest.Rule = binaryExprTest | inExprTest;
            parBinTest.Rule = "(" + binTest + ")";
            binTestList.Rule = MakePlusRule(binTestList, comma, parBinTest);

            containsExpr.Rule = expr + Keywords.Contains + expr;
            containsExprTest.Rule = Keywords.Contains + expr;

            // A line can be an empty line, or it's a number followed by a statement list ended by a new-line.
            statementLine.Rule = (statement + NewLine) | NewLine;
            statementList.Rule = MakePlusRule(statementList, null, statementLine);
            statementListOrEmpty.Rule = statementList | Empty;
            statement.Rule = presentStatement | whenStatement | loopStatement | whileDoStatement | comment |
                             assignmentStatement | waitForStatement | finishStatement | functionCall;
            statementLine.ErrorRule = SyntaxError + NewLine;

            IntegerRange.Rule = expr + ".." + expr | expr + ".." | ".." + expr | "..";
            SnippetRoots.Add(IntegerRange);

            integerOrStringList.Rule = MakePlusRule(integerOrStringList, comma, integerOrString);
            integerOrString.Rule = integer | stringLiteral;


            // A QuestionList can be a single question 'question 2' or a range 'question 3,4,7..12'
            QuestionList.Rule = Keywords.Question + ExpressionList | ToTerm(Keywords.All) + Keywords.Questions;
            SectionList.Rule = Keywords.Section + ExpressionList | ToTerm(Keywords.All) + Keywords.Sections;
            ActionList.Rule = Keywords.Action + ExpressionList;

            //           answerSingular.Rule = Keywords.Answer + integerOrString | Keywords.Answer + integerOrString + "#" + expr | identifier + "#" + expr;

            presentStatement.Rule = Keywords.Present + (Keywords.Hidden | Empty) + ExpressionList + (Empty | Keywords.Again) +
                                    (Empty |
                                     ToTerm(Keywords.Validate) + ToTerm(Keywords.With) + identifier + codeBlock +
                                     Keywords.End);

            expr.Rule = term | unExpr | binExpr | inExpr | containsExpr | presentStatement;
            term.Rule = IntegerRange | stringLiteral | number | /* answerSingular | */ identifier | parExpr
                        | QuestionList | SectionList | ActionList | functionCall | memberAccess
                        | getAttribute | fromExpr;

            memberAccess.Rule = expr + PreferShiftHere() + "." + identifier;


            ArgList.Rule = MakeStarRule(ArgList, comma, expr);
            ExpressionList.Rule = MakePlusRule(ExpressionList, comma, expr);

            // Case statement ..
            whenStatement.Rule = Keywords.When + ExpressionList + newLineOrEmpty + whenStatementBlock;
            isListWithDefault.Rule = isList | (isList + whenOtherwise) | whenOtherwise;
            whenStatementBlock.Rule = isListWithDefault + Keywords.End;
            whenOtherwise.Rule = Keywords.Otherwise + codeBlock;

            isStatement.Rule = Keywords.Is + (not | Empty) + (binTestList | binTest) + codeBlock;
            not.Rule = ToTerm(Keywords.Not);
            isList.Rule = MakePlusRule(isList, null, isStatement);

            // Loop statement ..
            loopStatement.Rule = Keywords.Loop + ExpressionList + (Empty | Keywords.With + identifier) + codeBlock +
                                 Keywords.End;

            // While statement ..
            whileDoStatement.Rule = Keywords.While + ExpressionList + Keywords.Do + codeBlock + Keywords.End;

            // Wait for statement ..
            // "wait a, b, c"
            // "wait a, b, c with d
            //   do something with 'd'
            // end"
            waitForStatement.Rule = Keywords.WaitFor + ExpressionList +
                                    (Empty | Keywords.With + identifier + codeBlock + Keywords.End);

            finishStatement.Rule = Keywords.Finish + finishWithOrEmpty;
            finishWithOrEmpty.Rule = Empty | finishWithStatement;
            finishWithStatement.Rule = Keywords.With + codeBlock + Keywords.End;

            functionCall.Rule = expr + PreferShiftHere() + "(" + ArgList + ")";

            getAttribute.Rule = expr + "@" + expr;

            // General
            codeBlock.Rule = statementListOrEmpty;

            assignmentTerm.Rule = identifier | memberAccess;
            assignmentStatement.Rule = assignmentTerm + ":=" + ExpressionList;
            assignmentStatementList.Rule = MakePlusRule(assignmentStatementList, semicolon, assignmentStatement);

            //Query rules
            //first the constants representing data stores
            fromExpr.Rule = Keywords.From + expr + whereClause + orderByClause + selectClause;
            whereClause.Rule = Keywords.Where + expr | Empty;
            orderByClause.Rule = Keywords.Orderby + identifier + (Keywords.Descending | Empty | Keywords.Ascending) |
                                 Empty;
            selectClause.Rule = Keywords.Select + (selectClausePlusRule | Keywords.This);
            selectClausePlusRule.Rule = MakePlusRule(selectClausePlusRule, comma, selectClauseItem);
            selectClausePlusRule.Precedence = 1000;
            selectClauseItem.Rule = expr + selectAsClause + selectDisplayClause;
            selectAsClause.Rule = ((Keywords.As + identifier) | Empty);
            selectDisplayClause.Rule = ((Keywords.Display + expr) | Empty);

            // 4. Operators precedence
            RegisterOperators(10, "?");
            RegisterOperators(15, "and", "or", "not");
            RegisterOperators(20, "=", "<", "<=", ">", ">=", "!=");
            RegisterOperators(30, "+", "-");
            RegisterOperators(40, "*", "/", "#");
            RegisterOperators(50, Associativity.Right, "power");

            //Punctuation and Transient elements
            MarkPunctuation("(", ")", ",", "[", "]");

            MarkTransient(newLineOrEmpty, statement, statementLine, integerOrString, term, expr, binOp, unOp,
                parExpr, statementListOrEmpty, binTest, parBinTest, assignmentTerm, finishWithOrEmpty);
//            Term, Expr, Statement, BinOp, UnOp, IncDecOp, AssignmentOp, ParExpr, ObjectRef
            this.LanguageFlags = LanguageFlags.NewLineBeforeEOF;
            this.LanguageFlags |= Irony.Parsing.LanguageFlags.CreateAst;
        }

        private static LingoGrammar _instance = new LingoGrammar();

        public static LingoGrammar Instance
        {
            get { return _instance; }
        }


        public static void PrecacheLanguageDataFromFile(BinaryReader reader)
        {
            lock (typeof (LingoGrammar))
            {
                if (_data == null)
                    _data = new LanguageData(Instance, reader);
            }
        }
    

    private static LanguageData _data;
        /// <summary>
        /// According to http://irony.codeplex.com/discussions/204477 the creation of the Language Data is an expensive operation, but is thread safe
        /// and can be cached.
        /// </summary>
        public static LanguageData LanguageData
        {
            get
            {
                if (_data == null)
                {
					lock (typeof(LingoGrammar))
					{
					    if (_data == null)
					    {
					        _data = new LanguageData (Instance);
					    }
							
					}
                }

                return _data;
            }
        }

        public override void BuildAst(LanguageData language, ParseTree parseTree)
        {
            var opHandler = new OperatorHandler(language.Grammar.CaseSensitive);
            var oid = opHandler.BuildDefaultOperatorMappings();
            oid[":="] = oid["="];
            oid["="] = oid["=="];
            oid.Remove("==");
            //and and or need to be short-circuit operators
            //otherwise, llblgen shits itself when trying to make a query with them
            oid["and"] = oid["&&"];
            oid["or"] = oid["||"];
            Util.Check(!parseTree.HasErrors(), "ParseTree has errors, cannot build AST.");
            var astContext = new InterpreterAstContext(language, opHandler);
            astContext.DefaultIdentifierNodeType = typeof(IdentifierAst);
            astContext.DefaultNodeType = typeof(LingoAst);
            astContext.DefaultLiteralNodeType = typeof(LiteralValueAst);

            var astBuilder = new AstBuilder(astContext);
            astBuilder.BuildAst(parseTree);
        }
    }
}
