﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using ConceptCave.Checklist.Reporting.LingoQuery;
using ConceptCave.Core;
using Irony.Ast;

namespace ConceptCave.Checklist.Lingo
{
    public class SelectAst : LingoAst
    {
        private IList<SelectItemAst> selectedItems { get; set; }
        private bool areSelectingThis { get; set; }
        private List<string> displayNames { get; set; }
        private List<string> columnNames { get; set; }

        public override void Init(AstContext context, Irony.Parsing.ParseTreeNode parseTreeNode)
        {
            base.Init(context, parseTreeNode);
            //our children are the select keyword + a list of select items
            selectedItems = new List<SelectItemAst>();
            var children = parseTreeNode.GetMappedChildNodes();
            //might be select this
            if (children.Count == 2 && children[1].Token != null && children[1].Token.Text.ToLower() == "this")
            {
                areSelectingThis = true;
            }
            else
            {
                areSelectingThis = false;
                //first child is select keyword, second child is the list of select items
                var selectItemGroup = children[1];
                var selectItemPlus = selectItemGroup.GetMappedChildNodes();
                var selectItemChildren = selectItemPlus[0].GetMappedChildNodes();
                for (int i = 0; i < selectItemChildren.Count; i++)
                {
                    var child = selectItemChildren[i].AstNode as SelectItemAst;
                    selectedItems.Add(child);
                }
            }
        }

        //NOTE: typeof returned expression is LingoQuerySelectedItem[]
        public Expression GetListExpression(IContextContainer context, ParameterExpression paramExpr)
        {
            if (areSelectingThis)
            {
                //"from x in y select x"
                return paramExpr;
            }
            else
            {
                //"from x in y select new {...}"
                //Firstly- pure context gives us enough info to figure out column names
                SetLingoColumnNames(context);
                SetLingoColumnDisplayNames(context);

                //get the list of lingo query selected item objects
                var lqsiExprList = new List<Expression>();
                foreach (var item in selectedItems)
                    lqsiExprList.Add(item.GetNewItemExpression(context, paramExpr));
                return Expression.NewArrayInit(typeof(LingoQuerySelectedItem), lqsiExprList);
            }
        }

        private void SetLingoColumnDisplayNames(IContextContainer context)
        {
            var retList = new List<string>();
            foreach (var item in selectedItems)
            {
                retList.Add(item.GetDisplayName(context));
            }
            displayNames = retList;
        }

        private void SetLingoColumnNames(IContextContainer context)
        {
            var retList = new List<string>();
            foreach (var item in selectedItems)
                retList.Add(item.GetSelectedName(context));
            columnNames = retList;
        }

        public IList<string> ColumnDisplayNames
        {
            get { return displayNames.AsReadOnly(); }
        }

        public IList<string> ColumnNames
        {
            get { return columnNames.AsReadOnly(); }
        }
    }
}
