﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Core;
using ConceptCave.Checklist.Interfaces;
using Irony.Ast;

namespace ConceptCave.Checklist.Lingo
{
    public class WhenOtherwiseAst : LingoInstruction, IBooleanTest
    {
        private StatementBlockAst _codeBlock;

        public override void Init(AstContext context, Irony.Parsing.ParseTreeNode parseTreeNode)
        {
            base.Init(context, parseTreeNode);

            _codeBlock = (StatementBlockAst)AddChild(parseTreeNode.ChildNodes[1]);
        }

        public override Interfaces.InstructionResult Execute(IContextContainer context)
        {
            base.PreExecute(context);
            return _codeBlock.Execute(context);
        }

        #region IBooleanTest Members

        public bool Test(object value, IContextContainer context)
        {
            return true;
        }

        #endregion
    }
}
