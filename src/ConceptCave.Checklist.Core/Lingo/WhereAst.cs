﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.Reporting.LingoQuery;
using ConceptCave.Core;
using Irony.Ast;

namespace ConceptCave.Checklist.Lingo
{
    public class WhereAst : LingoAst
    {
        private IEvaluatable expr { get; set; }

        public override void Init(AstContext context, Irony.Parsing.ParseTreeNode parseTreeNode)
        {
            base.Init(context, parseTreeNode);
            try
            {
                //We have two children: keyword and expression
                if (parseTreeNode.GetMappedChildNodes().Count > 0)
                    expr = (IEvaluatable)parseTreeNode.GetMappedChildNodes()[1].AstNode;
                else
                    expr = null;
            }
            catch (ArgumentOutOfRangeException) { expr = null; }
            catch (IndexOutOfRangeException) { expr = null; }
        }

        public Expression GetExpression(IContextContainer context, ParameterExpression paramExpr)
        {
            //recurse the tree postorder to build up a LINQ expresion tree
            //repreenting the AST.
            if (expr != null)
            {
                return QueryHelper.MakeLinqTree(expr, context, paramExpr);
            }
            else
            {
                return Expression.Constant(true, typeof(bool));
            }
        }



    }
}
