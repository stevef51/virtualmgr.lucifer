﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using Irony.Ast;

namespace ConceptCave.Checklist.Lingo
{
    public class InExpressionAst : LingoAst, IEvaluatable
    {
        public IEvaluatable LeftHS { get; private set; }
        public IEvaluatable ExpressionList { get; private set; }

        public override void Init(AstContext context, Irony.Parsing.ParseTreeNode parseTreeNode)
        {
            base.Init(context, parseTreeNode);

            LeftHS = parseTreeNode.ChildNodes[0].AstNode as IEvaluatable;
            ExpressionList = parseTreeNode.ChildNodes[2].AstNode as IEvaluatable;
        }

        #region IEvaluatable Members

        public object Evaluate(ConceptCave.Core.IContextContainer context)
        {
            object leftValue = LeftHS.Evaluate(context);
            object eval = ExpressionList.Evaluate(context);

            IEnumerable<object> evalList = eval as IEnumerable<object>;
            if (evalList != null)
            {
                foreach (object o in evalList)
                {
                    if (object.Equals(leftValue, o))
                        return true;
                }
                return false;
            }
            else
                return object.Equals(leftValue, eval);
        }

        #endregion
}
}
