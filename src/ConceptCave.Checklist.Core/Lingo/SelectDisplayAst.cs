﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;
using Irony.Ast;

namespace ConceptCave.Checklist.Lingo
{
    public class SelectDisplayAst : LingoAst
    {
        public IEvaluatable DisplayName { get; set; }

        public override void Init(AstContext context, Irony.Parsing.ParseTreeNode parseTreeNode)
        {
            base.Init(context, parseTreeNode);
            try
            {
                if (parseTreeNode.GetMappedChildNodes().Count > 0)
                    DisplayName = (IEvaluatable)parseTreeNode.GetMappedChildNodes()[1].AstNode;
                else
                    DisplayName = null;
            }
            catch (ArgumentOutOfRangeException) { DisplayName = null; }
            catch (IndexOutOfRangeException) { DisplayName = null; }
        }

        public string GetValue(IContextContainer context)
        {
            if (DisplayName != null)
                return (string)DisplayName.Evaluate(context);
            else
                return null;
        }
    }
}
