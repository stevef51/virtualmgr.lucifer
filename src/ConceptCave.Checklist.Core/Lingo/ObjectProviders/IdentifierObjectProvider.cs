﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;
using ConceptCave.Checklist.Commands.Runtime;

namespace ConceptCave.Checklist.Lingo.ObjectProviders
{
    public class IdentifierObjectProvider : IObjectSetter
    {
        public bool GetObject(ConceptCave.Core.IContextContainer context, string name, out object result)
        {
            var variableBag = context.Get<IVariableBag>("Get");
            var variable = variableBag.Variable<object>(name);
            if (variable != null)
            {
                result = variable.Value;
                return true;
            }

            var workingDocument = context.Get<IWorkingDocument>();
            if (context.Get<bool>("Presenting"))
            {
                // In a Present statement, search Questions/Sections for the name
                var question = workingDocument.Questions.Lookup(name);
                if (question != null)
                {
                    result = question;
                    return true;
                }

                var section = workingDocument.Sections.Lookup(name);
                if (section != null)
                {
                    result = section;
                    return true;
                }
            }

            // Try last Answers to Questions by name
            var answer = workingDocument.Answers.AnswerToQuestion(name, workingDocument.Answers.AnswerCount(name) - 1);
            if (answer != null)
            {
                if (!context.Get<bool>("GetAnswerObject", () => false))
                    result = answer.AnswerValue;
                else
                    result = answer;
                return true;
            }

            var facility = workingDocument.Facilities.Lookup(name);
            if (facility != null)
            {
                result = facility;
                return true;
            }

            var runtime = context.Get<LingoLanguageRuntime>();
            ICallTarget callTarget = runtime.GetCallTarget(context, name);
            if (callTarget != null)
            {
                result = callTarget;
                return true;
            }

            // ok everything else has failed, if we aren't presenting, then the only thing we haven't checked for is questions and sections, so lets give that a go
            if (context.Get<bool>("Presenting") == false)
            {
                // In a Present statement, search Questions/Sections for the name
                var question = workingDocument.Questions.Lookup(name);
                if (question != null)
                {
                    result = question;
                    return true;
                }

                var section = workingDocument.Sections.Lookup(name);
                if (section != null)
                {
                    result = section;
                    return true;
                }
            }

            result = null;
            return false;
        }

        public bool SetObject(IContextContainer context, string name, object value)
        {
            return SetObject(context, name, value, false);
        }

        public bool SetObject(IContextContainer context, string name, object value, bool ephermal)
        {
            var variableBag = context.Get<IVariableBag>("Set");
            var program = context.Get<ILingoProgram>();
            if (ephermal)
            {
                var variable = variableBag.Variable<object>(name, null);
                variable.Value = value;
            }
            else
            {
                program.Do(new SetVariableCommand<object>() { Variable = variableBag.Variable<object>(name, null), NewValue = value });
            }
            return true;
        }
    }
}
