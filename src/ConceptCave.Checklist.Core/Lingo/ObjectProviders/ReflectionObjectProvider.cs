﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using System.Reflection;
using ConceptCave.Checklist.Lingo.Functions;
using ConceptCave.Core;
using System.Runtime.CompilerServices;

namespace ConceptCave.Checklist.Lingo.ObjectProviders
{
    public class ReflectionObjectProvider : IObjectSetter
    {
        public object Instance { get; private set; }
        private Type _type;
        private Func<string, Type, IEnumerable<MemberInfo>> _fnMemberInfo;

        public ReflectionObjectProvider(object instance, Func<string, Type, IEnumerable<MemberInfo>> fnMemberInfo = null)
        {
            Instance = instance;
            _fnMemberInfo = fnMemberInfo;
            _type = Instance.GetType();
        }

        public bool GetObject(IContextContainer context, string name, out object result)
        {
            result = null;
            IEnumerable<MemberInfo> members = null;
            if (_fnMemberInfo == null)
                members = _type.GetMember(name, BindingFlags.Public | BindingFlags.Instance | BindingFlags.IgnoreCase);
            else
                members = _fnMemberInfo(name, _type);

            if (members == null)
                return false;

            var member = members.FirstOrDefault();
            if (member == null)
                return false;

            switch (member.MemberType)
            {
                case MemberTypes.Property:
                    var propInfo = member as PropertyInfo;
                    result = propInfo.GetValue(Instance, null);
                    return true;

                case MemberTypes.Field:
                    var fieldInfo = member as FieldInfo;
                    result = fieldInfo.GetValue(Instance);
                    return true;
                    
                case MemberTypes.Method:
                    result = new ReflectedMethodCallTarget()
                    {
                        Name = name,
                        Instance = Instance,
                        MethodInfo = members.Cast<MethodInfo>()
                    };
                    return true;
            }

            return false;
        }

        public static IEnumerable<Type> LoadableAssemblyTypes()
        {
            List<Type> types = new List<Type>();
            foreach(var assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                try
                {
                    foreach(var type in assembly.GetTypes())
                    {
                        types.Add(type);
                    }
                }
                catch
                {
                    // Ignore this assembly
                }
            }
            return types;
        }

        public static IEnumerable<MethodInfo> GetExtensionMethods(string name, Type thisType)
        {
            return
                from type in LoadableAssemblyTypes()
                where type.IsSealed && !type.IsGenericType && !type.IsNested
                from method in type.GetMethods(BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic)
                where string.Compare(method.Name, name, true) == 0
                where method.IsDefined(typeof(ExtensionAttribute), false)
                where method.GetParameters()[0].ParameterType.IsAssignableFrom(thisType)
                select method;
        }

        public bool SetObject(IContextContainer context, string name, object value)
        {
            var members = _type.GetMember(name, BindingFlags.Public | BindingFlags.Instance | BindingFlags.IgnoreCase);
            if (members == null || members.Length == 0)
                return false;

            try
            {
                var member = members[0];
                switch (member.MemberType)
                {
                    case MemberTypes.Property:
                        var propInfo = member as PropertyInfo;
                        if (!propInfo.CanWrite)
                            return false;

                        propInfo.SetValue(Instance, value.ConvertToType(propInfo.PropertyType), null);
                        return true;

                    case MemberTypes.Field:
                        var fieldInfo = member as FieldInfo;
                        fieldInfo.SetValue(Instance, value.ConvertToType(fieldInfo.FieldType));
                        return true;
                }
            }
            catch
            {
            }

            return false;
        }
    }
}
