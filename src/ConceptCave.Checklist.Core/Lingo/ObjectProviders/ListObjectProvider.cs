﻿using ConceptCave.Core;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Facilities;
using Newtonsoft.Json;

namespace ConceptCave.Checklist.Lingo.ObjectProviders
{
    public class ListObjectProvider : Node, IEnumerable, IEnumerable<object>, ILogMessageFormatter
    {
        [JsonProperty("List")]
        public List<object> UnderlyingList { get; private set; }

        public ListObjectProvider()
        {
            UnderlyingList = new List<object>();
        }

        public static ListObjectProvider From(IEnumerable<object> items)
        {
            var list = new ListObjectProvider();
            list.AddRange(items.ToArray());
            return list;    
        }

        public ListObjectProvider(params object[] args) : this()
        {
            UnderlyingList.AddRange(args);
        }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            encoder.Encode("UnderlyingList", UnderlyingList);
            base.Encode(encoder);
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            UnderlyingList = decoder.Decode("UnderlyingList", new List<object>());
            base.Decode(decoder);
        }
    
        public IEnumerator GetEnumerator()
        {
            return UnderlyingList.GetEnumerator();
        }

        IEnumerator<object> IEnumerable<object>.GetEnumerator()
        {
            return UnderlyingList.GetEnumerator();
        }

        public object ItemAt(int index)
        {
            try
            {
                return UnderlyingList[index];
            }
            catch (IndexOutOfRangeException)
            {
                return null;
            }
        }

        public void Add(params object[] args)
        {
            UnderlyingList.AddRange(args);
        }

        public void AddRange(object[] args)
        {
            UnderlyingList.AddRange(args);
        }

        public void Add(object item)
        {
            UnderlyingList.Add(item);
        }

        public void RemoveAt(int index)
        {
            try
            {
                UnderlyingList.RemoveAt(index);
            }
            catch (ArgumentOutOfRangeException)
            {

            }
        }

        public void Remove(params object[] args)
        {
            foreach (var o in args)
                UnderlyingList.Remove(args);
        }

        [JsonIgnore]
        public int Count
        {
            get
            {
                return UnderlyingList.Count;
            }
        }

        public bool HasItem(object item)
        {
            var result = (from u in UnderlyingList where u.Equals(item) select u);

            return result.Count() != 0;
        }

        public string FormatMessage(IContextContainer container, ConceptCave.Checklist.Facilities.LoggingFacility.LoggingList logger)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("<table class=\"table\"><tr><th>Index</th><th>Value</th></tr>");

            int i = 0;
            UnderlyingList.ForEach(d =>
            {
                builder.Append("<tr><td>");
                builder.Append(i.ToString());
                builder.Append("</td><td>");
                builder.Append(logger.FormatMessage(container, d));
                builder.Append("</td></tr>");

                i++;
            });

            builder.Append("</table>");

            return builder.ToString();
        }
		
        public ListObjectProvider InRandomOrder()
        {
            List<object> result = new List<object>(UnderlyingList);

            int n = result.Count;

            while (n > 1)
            {
                n--;
                int k = ThreadSafeRandom.ThisThreadsRandom.Next(n + 1);
                var value = result[k];
                result[k] = result[n];
                result[n] = value;
            }

            var items = new ListObjectProvider();
            result.ForEach(r => items.Add(r));

            return items;
        }

        public string Join(string seperator)
        {
            return string.Join(seperator, from i in UnderlyingList select i.ToString());
        }
        public string Join()
        {
            return Join("");
        }
    }
}
