﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Facilities;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;
using Newtonsoft.Json;

namespace ConceptCave.Checklist.Lingo.ObjectProviders
{
    public class DictionaryObjectProvider : Node, IObjectSetter, ILogMessageFormatter
    {
        private Dictionary<string, object> _dictionary;
        public IDictionary<string, object> Dictionary {
            get => _dictionary;
            set
            {
                _dictionary = new Dictionary<string, object>(value, StringComparer.OrdinalIgnoreCase);
            }
        }

        public DictionaryObjectProvider()            
        {
            _dictionary = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);
        }

        public DictionaryObjectProvider(IDictionary<string, object> init) : base()
        {
            _dictionary = new Dictionary<string, object>(init, StringComparer.OrdinalIgnoreCase);
        }


        public bool GetObject(IContextContainer context, string name, out object result)
        {
            return _dictionary.TryGetValue(name.ToLower(), out result);
        }

        public bool SetObject(IContextContainer context, string name, object value)
        {
            _dictionary[name.ToLower()] = value;
            return true;
        }

        public void SetValue(IContextContainer context, string name, object value)
        {
            SetObject(context, name, value);
        }

        public object GetValue(IContextContainer context, string name)
        {
            object result = null;

            if (GetObject(context, name, out result) == false)
            {
                return null;
            }

            return result;
        }

        public bool HasValueFor(string propertyName)
        {
            return _dictionary.Keys.Contains(propertyName.ToLower());
        }

        public object Default(string propertyName, object defaultValue)
        {
            object result;
            if (!_dictionary.TryGetValue(propertyName.ToLower(), out result))
            {
                return defaultValue;
            }
            return result;
        }

        [JsonIgnore]
        public ListObjectProvider Properties
        {
            get
            {
                var result = new ListObjectProvider();

                _dictionary.Keys.ForEach(k => result.Add(k));

                return result;
            }
        }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            if (Dictionary != null)
            {
                encoder.Encode("Dictionary", Dictionary);
            }
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            Dictionary = decoder.Decode<IDictionary<string, object>>("Dictionary");
        }

        public string FormatMessage(IContextContainer container, ConceptCave.Checklist.Facilities.LoggingFacility.LoggingList logger)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("<table class=\"table\"><tr><th>Property</th><th>Value</th></tr>");

            _dictionary.ForEach(d =>
            {
                builder.Append("<tr><td>");
                builder.Append(d.Key);
                builder.Append("</td><td>");
                builder.Append(logger.FormatMessage(container, d.Value));
                builder.Append("</td></tr>");
            });

            builder.Append("</table>");

            return builder.ToString();
        }

        public int Count()
        {
            return _dictionary.Count;
        }

        public override string ToString()
        {
            return string.Join(";", _dictionary);
        }
    }
}
