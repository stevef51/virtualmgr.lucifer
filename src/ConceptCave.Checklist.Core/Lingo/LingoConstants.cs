﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;

namespace ConceptCave.Checklist.Lingo
{
    public class LingoConstantsBag : VariableBag
    {
        public LingoConstantsBag() : this(null, true) { }
        
        public LingoConstantsBag(IVariableBag parent = null, bool ignoreCase = true) : base(parent, ignoreCase)
        {
            //Add some constant values to the bag
            _variables.Add("null", new ReadOnlyVariable<object>("Null", null));
            _variables.Add("true", new ReadOnlyVariable<object>("True", true));
            _variables.Add("false", new ReadOnlyVariable<object>("False", false ));
		}

        public override IVariable<T> Variable<T>(string name, T defaultCreateValue)
        {
            throw new LingoException("Cannot set a constant value");
        }

        public override void Clear()
        {
            throw new LingoException("Cannot clear Lingo constants");
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            //We don't need to save anything
        }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            //We don't need to save anything
        }
    }
}
