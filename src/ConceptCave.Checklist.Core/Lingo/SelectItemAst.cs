﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;
using Irony.Ast;
using Irony.Parsing;
using ConceptCave.Checklist.Reporting.LingoQuery;

namespace ConceptCave.Checklist.Lingo
{
    public class SelectItemAst : LingoAst
    {
        private IEvaluatable SelectedValue { get; set; }
        private SelectAsAst AsClause { get; set; }
        private SelectDisplayAst DisplayClause { get; set; }

        public override void Init(AstContext context, ParseTreeNode parseTreeNode)
        {
            base.Init(context, parseTreeNode);
            SelectedValue = (IEvaluatable)parseTreeNode.GetMappedChildNodes()[0].AstNode;
            AsClause = parseTreeNode.GetMappedChildNodes()[1].AstNode as SelectAsAst;
            DisplayClause = parseTreeNode.GetMappedChildNodes()[2].AstNode as SelectDisplayAst;
        }

        public Expression GetNewItemExpression(IContextContainer context, ParameterExpression paramExpr)
        {
            //need to reduce the selected expression to an etree
            var selectedEtree = QueryHelper.MakeLinqTree(SelectedValue, context, paramExpr);
            //was it selected as something? if so, call it that
            var selectedName = GetSelectedName(context);
            //was it displayed as something?
            var displayName = GetDisplayName(context);

            //Now an expression to build a new LingoQuerySelectedItem

            var lqsiConstructor = typeof(LingoQuerySelectedItem).GetConstructor(
                new Type[] { typeof(string), typeof(string), typeof(object) });
            //explicitly convert to object, so that even if selectedEtree is a value type, it can be boxed
            //to fit in the lqsiConstructor
            var boxedSelectedEtree = Expression.ConvertChecked(selectedEtree, typeof(object));
            return Expression.New(lqsiConstructor,
                Expression.Constant(selectedName, typeof(string)),
                Expression.Constant(displayName, typeof(string)),
                boxedSelectedEtree);
        }

        public string GetSelectedName(IContextContainer context)
        {
            if (!string.IsNullOrEmpty(AsClause.AsName))
            {
                return AsClause.AsName;
            }
            else
            {
                //we'd better hope that our select clause is just an identifier
                return (SelectedValue as IdentifierAst).Name;
            }
        }

        public string GetDisplayName(IContextContainer context)
        {
            var dname = DisplayClause.GetValue(context);
            if (string.IsNullOrEmpty(dname))
            {
                dname = GetSelectedName(context);
            }
            return dname;
        }
    }
}
