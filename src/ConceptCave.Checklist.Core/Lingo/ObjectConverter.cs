﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;

namespace ConceptCave.Checklist.Lingo
{
    public class ObjectConverter : IEqualityComparer<object>
    {
        #region IEqualityComparer<object> Members

        bool IEqualityComparer<object>.Equals(object x, object y)
        {
            if (x is IEnumerable<object> || y is IEnumerable<object>)
            {
                var xArray = CreateEnumerablePrimitiveOf<object>(x);
                var yArray = CreateEnumerablePrimitiveOf<object>(y);

                return xArray.SequenceEqual(yArray, this);
            }

            object xp = x.ConvertToPrimitive();
            object yp = y.ConvertToPrimitive();

            if (xp is string && yp is string)
                return string.Compare(xp.ToString(), yp.ToString(), StringComparison.InvariantCultureIgnoreCase) == 0;

            return object.Equals(xp, yp);
        }

        int IEqualityComparer<object>.GetHashCode(object obj)
        {
            return 0;       // We want to call Equals, it will try some casts
        }

        #endregion

        public static IEnumerable<T> CreateEnumerablePrimitiveOf<T>(object eval)
        {
            object p;
            IEnumerable<T> evalListT = eval as IEnumerable<T>;
            if (evalListT != null)
            {
                foreach (T t in evalListT)
                {
                    p = t.ConvertToPrimitive();
                    if (p is T)
                        yield return (T)p;
                }
                yield break;
            }

            IEnumerable<object> evalList = eval as IEnumerable<object>;
            if (evalList != null)
            {
                foreach (object o in evalList)
                {
                    p = o.ConvertToPrimitive();
                    if (p is T)
                        yield return (T)p;
                }
                yield break;
            }

            p = eval.ConvertToPrimitive();
            if (p is T)
            {
                yield return (T)p;
                yield break;
            }
        }

        public static IEnumerable<T> CreateEnumerableOf<T>(object eval)
        {
            IEnumerable<T> evalListT = eval as IEnumerable<T>;
            if (evalListT != null)
            {
                foreach (T t in evalListT)
                    yield return t;

                yield break;
            }

            IEnumerable<object> evalList = eval as IEnumerable<object>;
            if (evalList != null)
            {
                foreach (object o in evalList)
                    if (o is T)
                        yield return (T)o;

                yield break;
            }

            if (eval is T)
            {
                yield return (T)eval;
                yield break;
            }
        }
    }
}
