﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;
using Irony.Ast;

namespace ConceptCave.Checklist.Lingo
{
    public class FunctionCallAst : LingoInstruction, IEvaluatable
    {
        public IEvaluatable Target { get; private set; }
        public IEvaluatable ArgList { get; private set; }

        public override void Init(AstContext context, Irony.Parsing.ParseTreeNode parseTreeNode)
        {
            base.Init(context, parseTreeNode);

            Target = parseTreeNode.ChildNodes[0].AstNode as IEvaluatable;
            ArgList = parseTreeNode.ChildNodes[1].AstNode as IEvaluatable;
        }

        public object Evaluate(ConceptCave.Core.IContextContainer context)
        {
            ContextContainer newContext1 = new ContextContainer(context);
            newContext1.Set<bool>("IsFunctionCall", true);
            ICallTarget callTarget = Target.Evaluate(newContext1) as ICallTarget;
            if (callTarget != null)
            {
                ContextContainer newContext2 = new ContextContainer(context);
                newContext2.Set<bool>("AsArray", true);
                object[] args = (object[])ArgList.Evaluate(newContext2);
                try
                {
                    return callTarget.Call(context, args);
                }
                catch (LingoException lex)
                {
                    if (lex.Ast == null)
                        lex.Ast = this;
                    throw;
                }
            }
            return null;
        }

        public override InstructionResult Execute(IContextContainer context)
        {
            base.PreExecute(context);
            ContextContainer newContext1 = new ContextContainer(context);
            newContext1.Set<bool>("IsFunctionCall", true);
            ICallTarget callTarget = Target.Evaluate(newContext1) as ICallTarget;
            if (callTarget != null)
            {
                ContextContainer newContext2 = new ContextContainer(context);
                newContext2.Set<bool>("AsArray", true);
                object[] args = (object[])ArgList.Evaluate(newContext2);
                try
                {
                    callTarget.Call(context, args);
                }
                catch (LingoException lex)
                {
                    if (lex.Ast == null)
                        lex.Ast = this;
                    throw;
                }
            }
            return InstructionResult.Finished;
        }
    }
}
