﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Irony.Parsing;
using ConceptCave.Core;
using ConceptCave.Checklist.Interfaces;
using Irony.Ast;

namespace ConceptCave.Checklist.Lingo
{
    /// <summary>
    /// Ast that handles
    /// 
    /// </summary>
    public class ExpressionListAst : LingoAst, IEvaluatable
    {
        public List<IEvaluatable> ObjectList { get; private set; }

        public override void Init(AstContext context, ParseTreeNode parseTreeNode)
        {
            base.Init(context, parseTreeNode);  

            ObjectList = new List<IEvaluatable>();
            parseTreeNode.ChildNodes.ForEach(c => 
            { 
                if (c.AstNode is IEvaluatable) 
                    ObjectList.Add(c.AstNode as IEvaluatable); 
            });

        }
        #region IEvaluatable Members

        public object Evaluate(IContextContainer context)
        {
            List<object> values = new List<object>();

            if (context == null ||  !context.Get<bool>("FlattenList", () => false))
            {
                ObjectList.ForEach(e =>
                    {
                        object o = e.Evaluate(context);
                        values.Add(o);
                    });
            }
            else
            {
                ObjectList.ForEach(e =>
                {
                    object o = e.Evaluate(context);
                    if (o is IEnumerable<object>)
                        values.AddRange(o as IEnumerable<object>);
                    else
                        values.Add(o);
                });
            }
            switch (values.Count)
            {
                case 0:
                    return null;

                case 1:
                    return values[0];

                default:
                    return values.ToArray();
            }
        }

        #endregion
    }
}
