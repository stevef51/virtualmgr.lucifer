﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.Lingo.ObjectProviders;
using ConceptCave.Checklist.Reporting.LingoQuery;
using ConceptCave.Core;
using Irony.Ast;
using Irony.Parsing;

namespace ConceptCave.Checklist.Lingo
{
    public class FromAst : LingoAst, IEvaluatable
    {
        private IEvaluatable DataSourceNode { get; set; }
        private WhereAst WhereNode { get; set; }
        private OrderbyAst OrderbyNode { get; set; }
        private SelectAst SelectNode { get; set; }

        private IdentifierObjectProvider objectProvider = new IdentifierObjectProvider();
        private Dictionary<string, object> savedIdentifiers = new Dictionary<string, object>();

        public object Evaluate(IContextContainer context)
        {
            //Firstly, we need to find our data source.
            var dataSource = DataSourceNode.Evaluate(context) as ILingoDatastore;
            if (dataSource == null)
            {
                //nope, try context
                var dsNodeIdentifier = DataSourceNode as IdentifierAst;
                if (dsNodeIdentifier != null)
                {
                    dataSource = context.Get<ILingoDatastore>(dsNodeIdentifier.Name.ToLower());
                    if (dataSource == null)
                    {
                        var lazyDataSource = context.Get<Lazy<ILingoDatastore>>(dsNodeIdentifier.Name.ToLower());
                        if (lazyDataSource != null)
                        {
                            dataSource = lazyDataSource.Value;
                        }
                    }
                }
            }
            if (dataSource == null)
            {
                //Didn't get it there either. Bitch and moan
                throw new ArgumentException("No datasource found for this query`");
            }

            using (var transaction = dataSource.BeginTransaction(context))
            {
                var queryable = transaction.Queryable;

                var oldGetBag = context.Get<IVariableBag>("Get");
                var oldSetBag = context.Get<IVariableBag>("Set");
                VariableBag columnBag = new VariableBag();
                context.Set<IVariableBag>("Get", columnBag);
                context.Set<IVariableBag>("Set", columnBag);

                //now, inject symbols from this data source for name resolution purposes
                foreach (var kvp in dataSource.Columns)
                {
                    columnBag.Variable<object>(kvp.Key, null).Value = kvp.Value;
                }

                columnBag.Parent = oldGetBag;

                //the from entity in table bit
                var paramExpr = Expression.Parameter(dataSource.EntityType,
                    "entity");
                //Get the where clause
                var whereClauseExpression = WhereNode.GetExpression(context,
                    paramExpr);
                //and select something good
                var selectClauseExpression = SelectNode.GetListExpression(context, paramExpr);

                //wire everything up with linq extension methods
                var whereCallExpression = Expression.Call(typeof(Queryable), "Where",
                    new Type[] { queryable.ElementType }, queryable.Expression,
                    Expression.Lambda(whereClauseExpression, paramExpr));
                var orderByCallExpression = OrderbyNode.GetOrderbyCall(context,
                    paramExpr, whereCallExpression, queryable.ElementType);
                var selectCallExpression = Expression.Call(typeof(Queryable), "Select",
                    new Type[] { queryable.ElementType, typeof(LingoQuerySelectedItem[]) }, orderByCallExpression,
                    Expression.Lambda(selectClauseExpression, paramExpr));

                //get the datastore to execute the query
                var resultset = transaction.ExecuteQuery(selectCallExpression, queryable)
                                            .Cast<LingoQuerySelectedItem[]>();

                //lingo datatable constructor needs this info:
                var displayNameMappings = new Dictionary<string, string>();
                for (int i = 0; i < SelectNode.ColumnNames.Count; i++)
                {
                    displayNameMappings.Add(SelectNode.ColumnNames[i], SelectNode.ColumnDisplayNames[i]);
                }
                var datatable = new LingoDatatable(resultset, SelectNode.ColumnNames, displayNameMappings);

                context.Set<IVariableBag>("Get", oldGetBag);
                context.Set<IVariableBag>("Set", oldSetBag);

                return datatable;
            }

        }

        public override void Init(AstContext context, ParseTreeNode parseTreeNode)
        {
            base.Init(context, parseTreeNode);

            //There are three things that sit underneath us:
            //where, orderby, and select nodes
            DataSourceNode = (IEvaluatable)parseTreeNode.GetMappedChildNodes()[1].AstNode;
            WhereNode = (WhereAst)parseTreeNode.GetMappedChildNodes()[2].AstNode;
            OrderbyNode = (OrderbyAst)parseTreeNode.GetMappedChildNodes()[3].AstNode;
            SelectNode = (SelectAst)parseTreeNode.GetMappedChildNodes()[4].AstNode;
        }
    }
}