﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Checklist.Interfaces;

namespace ConceptCave.Checklist.Lingo
{
    public class ExpandStringException : LingoException
    {
        public string Code { get; private set; }
        public string QuestionName { get; set; }
        public string QuestionType { get; set; }

        public ExpandStringException(string code, LingoProgramDefinition.Instance program = null, Exception innerException = null) : base("An exception occurred while performing a string expansion", innerException)
        {
            Code = code;
            Program = program;
        }
    }
}
