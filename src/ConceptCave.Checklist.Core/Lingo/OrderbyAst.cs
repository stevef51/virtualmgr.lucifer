﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using ConceptCave.Core;
using Irony.Ast;
using Irony.Parsing;

namespace ConceptCave.Checklist.Lingo
{
    public class OrderbyAst : LingoAst
    {
        private enum Direction
        {
            Ascending,
            Descending
        }

        private IdentifierAst ident { get; set; }
        private Direction sortDir { get; set; }

        public override void Init(AstContext context, ParseTreeNode parseTreeNode)
        {
            base.Init(context, parseTreeNode);
            if (parseTreeNode.GetMappedChildNodes().Count > 0)
            {
                ident = (IdentifierAst)parseTreeNode.GetMappedChildNodes()[1].AstNode;
                if (parseTreeNode.GetMappedChildNodes().Count > 2 &&
                    !string.IsNullOrEmpty(
                        parseTreeNode.GetMappedChildNodes()[2].FindTokenAndGetText()))
                {
                    var sortkw = parseTreeNode.GetMappedChildNodes()[2].FindTokenAndGetText();
                    if (sortkw.ToLower() == "ascending")
                        sortDir = Direction.Ascending;
                    else if (sortkw.ToLower() == "descending")
                        sortDir = Direction.Descending;
                    else
                        throw new LingoException("Unknown sort direction " + sortkw);
                }
                else
                {
                    sortDir = Direction.Ascending;
                }
            }
            

        }

        public Expression GetOrderbyCall(IContextContainer context, ParameterExpression pe, Expression source, Type TSource)
        {
            //Did they actually want to order?
            if (ident == null)
                return source;

            string func = "";
            switch (sortDir)
            {
                case Direction.Descending:
                    func = "OrderByDescending";
                    break;
                case Direction.Ascending:
                    func = "OrderBy";
                    break;
            }

            var keyexpr = QueryHelper.MakeLinqTree(ident, context, pe);
            var keylambda = Expression.Lambda(keyexpr, pe);
            return Expression.Call(typeof(Queryable), func,
                new Type[] { TSource, keyexpr.Type }, source, keylambda);
        }
    }
}
