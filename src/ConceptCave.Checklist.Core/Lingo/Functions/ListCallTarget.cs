﻿using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.Lingo.ObjectProviders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConceptCave.Checklist.Lingo.Functions
{
    public class ListCallTarget : ICallTarget
    {
        public object Call(ConceptCave.Core.IContextContainer context, object[] args)
        {
            if (args.Length == 0 || (args[0] is string) == false)
            {
                return new ListObjectProvider(args);
            }

            string input = string.Empty;
            char seperator = ',';

            if (args[0] is string)
            {
                input = (string)args[0];
            }

            if (args.Length > 1 && args[1] is string)
            {
                seperator = ((string)args[1])[0];
            }

            if (input == null)
            {
                return new ListObjectProvider();
            }

            var result = new ListObjectProvider();
            input.Split(seperator).ForEach(i => result.Add(i));

            return result;
        }
    }
}
