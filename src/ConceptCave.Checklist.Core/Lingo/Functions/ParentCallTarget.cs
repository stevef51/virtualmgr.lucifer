﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.Editor;
using ConceptCave.Checklist.Facilities;
using ConceptCave.Core;

namespace ConceptCave.Checklist.Lingo.Functions
{
    public class ParentCallTarget : ICallTarget
    {
        public IContextContainer InCallContext { get; set; }

        public ParentCallTarget() { }

        public ParentCallTarget(IContextContainer context)
        {
            InCallContext = context;
        }

        #region ICallTarget Members

        public object Call(ConceptCave.Core.IContextContainer context, object[] args)
        {
            if (args.Length == 0)
            {
                return null;
            }

            if (args.Length >= 2 && args[0] is WorkingDocumentGetter)
            {
                var newContext = new ContextContainer(context);
                newContext.Set<IWorkingDocument>(((WorkingDocumentGetter)args[0]).GetWorkingDocument(context));

                var target = new ParentCallTarget(newContext);

                return target.Call(newContext, args.Skip(1).ToArray());
            }

            IWorkingDocument wd= context.Get<IWorkingDocument>();

            object source = args[0];

            if (source is IQuestion)
            {
                // looking for the parent of this question
                IQuestion question = source as IQuestion;
                DesignDocument dd = wd.DesignDocument as DesignDocument;

                var s = (from p in dd.AsDepthFirstEnumerable() where p is ISection && ((ISection)p).Children.Contains(question) select p);

                if (s.Count() == 0)
                {
                    return null;
                }

                return s.First();
            }

            return null;
        }

        #endregion
    }
}
