﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using System.Reflection;
using ConceptCave.Core;
using System.Runtime.CompilerServices;
using System.ComponentModel;

namespace ConceptCave.Checklist.Lingo.Functions
{
    public class ReflectedMethodCallTarget : ICallTarget
    {
        public IEnumerable<MethodInfo> MethodInfo;
        public object Instance;
        public string Name; 

        private class MethodWithArgs
        {
            public int ExactMatch;
            public int CastMatch;
            public int ArgsStartAt;

            public bool StaticMethod;
            public MethodInfo MethodInfo;
            public ParameterInfo[] Parameters;

            public static MethodWithArgs MatchMethodWithArgs(MethodInfo methodInfo, object[] args)
            {
                int exactMatch = 0;
                int castMatch = 0;
                int startAt = 0;
                ParameterInfo[] parameters = methodInfo.GetParameters();
                bool staticMethod = false;

                // Special case, if method is an extension method then we can inject that
                if (methodInfo.IsDefined(typeof(ExtensionAttribute), false))
                {
                    staticMethod = true;
                    startAt++;
                }

                // Special case, if 1st parameter is IContextContainer then we can inject that
                if (parameters.Length > 0 && parameters[startAt].ParameterType == typeof(IContextContainer))
                    startAt++;

                if (parameters.Length - startAt != args.Length)
                {
                    return null;
                }

                exactMatch = 0;
                castMatch = 0;

                // Count matches, check exact types ..
                for (int i = startAt, j = 0; i < parameters.Length; i++, j++)
                {
                    Type parameterType = parameters[i].ParameterType;
                    object arg = args[j];
                    Type argType = null;

                    if (arg == null)
                    {
                        if (parameterType.IsClass || parameterType.IsInterface)
                        {
                            castMatch++;
                            continue;
                        }
                        else
                            return null;           // Cant pass a null to a value type
                    }

                    // arg != null
                    argType = arg.GetType();

                    if (parameterType == argType)
                    {
                        exactMatch += 2;             // Exact match is perfect
                        continue;
                    }

                    // If both are reference type, then if argType is castable to parameterType then it matches
                    if (!parameterType.IsValueType && !argType.IsValueType)
                    {
                        if (parameterType.IsAssignableFrom(argType))
                        {
                            exactMatch++;            // Assignable match
                            continue;
                        }
                        else
                            return null;           // Cant assign this parameter, no good
                    }

                    if (parameterType.IsGenericType && parameterType.GetGenericTypeDefinition() == typeof(Nullable<>))
                    {
                        if (System.Nullable.GetUnderlyingType(parameterType).IsAssignableFrom(argType))
                        {
                            exactMatch++;
                            continue;
                        }
                    }

                    if (parameterType.IsValueType && argType.IsValueType)
                    {
                        // Both are Value type, we can try a cast
                        castMatch++;
                    }
                    else
                    {
                        if (parameterType == typeof(object))
                        {
                            castMatch++;
                            continue;
                        }
                        try
                        {
                            // Try the cast anyway ..
                            object cast = Convert.ChangeType(arg, parameterType);
                            castMatch++;
                            continue;
                        }
                        catch
                        {
                            // If its a string then we can try a Convert FromString ..
                            if (argType == typeof(string))
                            {
                                castMatch++;
                                continue;
                            }
                            // 1 is reference type, other is value type .. not assignable
                            return null;
                        }
                    }
                }

                // Some sort of match occured, we could call this method with these parameters
                return new MethodWithArgs() { MethodInfo = methodInfo, StaticMethod = staticMethod, Parameters = parameters, ArgsStartAt = startAt, CastMatch = castMatch, ExactMatch = exactMatch };

            }

            public object[] MakeArgs(IContextContainer context, object instance, object[] args)
            {
                List<object> madeArgs = new List<object>();

                if (StaticMethod)
                    madeArgs.Add(instance);

                if (ArgsStartAt > 0)
                    madeArgs.Add(context);
                
                if (CastMatch == 0)
                    madeArgs.AddRange(args);
                
                else
                {
                    // Some casting is required ..
                    for (int i = ArgsStartAt, j = 0; i < Parameters.Length; i++, j++)
                    {
                        if (args[j] == null)
                        {
                            madeArgs.Add(null);
                            continue;
                        }

                        if (Parameters[i].ParameterType.IsAssignableFrom(args[j].GetType()))
                        {
                            madeArgs.Add(args[j]);
                            continue;
                        }

                        // A cast must be tried ..
                        try
                        {
                            object castObj = Convert.ChangeType(args[j], Parameters[i].ParameterType);
                            madeArgs.Add(castObj);
                            continue;
                        }
                        catch(InvalidCastException)
                        {
                            if (args[j] is string)
                            {
                                try
                                {
                                    object convObj = TypeDescriptor.GetConverter(Parameters[i].ParameterType).ConvertFromInvariantString(args[j].ToString());
                                    madeArgs.Add(convObj);
                                    continue;
                                }
                                catch
                                {
                                    // fall through to below
                                }
                            }
                            object castObj = (object)args[j];
                            madeArgs.Add(castObj);
                            continue;
                        }
                    }
                }

                return madeArgs.ToArray();
            }
        }

        private MethodWithArgs FindBestMethod(IEnumerable<MethodInfo> methods, object[] args)
        {
            MethodWithArgs bestMethod = null;

            foreach (MethodInfo mi in methods)
            {
                if (mi == null)         // Shouldnt happen but possible a Property has same name as Method??
                    continue;

                MethodWithArgs mwa = MethodWithArgs.MatchMethodWithArgs(mi, args);
                if (mwa == null)
                    continue;

                if (bestMethod == null || mwa.ExactMatch > bestMethod.ExactMatch)
                {
                    bestMethod = mwa;
                    if (bestMethod.CastMatch == 0)
                        break;
                }
            }

            return bestMethod;
        }

        public object Call(ConceptCave.Core.IContextContainer context, object[] args)
        {
            var bestMethod = FindBestMethod(MethodInfo, args);

            if (bestMethod == null)
                throw new LingoException(string.Format("Could not Reflect method '{0}'", Name));

            object[] newArgs = bestMethod.MakeArgs(context, Instance, args);

            return bestMethod.MethodInfo.Invoke(Instance, newArgs);
        }
    }
}
