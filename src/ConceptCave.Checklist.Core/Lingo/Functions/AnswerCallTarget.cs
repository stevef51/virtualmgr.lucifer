﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Irony.Parsing;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;
using ConceptCave.Checklist.Facilities;

namespace ConceptCave.Checklist.Lingo.Functions
{
    public class AnswerCallTargetFactory : ICallTargetFactory
    {
        public ICallTarget Create(IContextContainer context)
        {
            return new AnswerCallTarget(context);
        }
    }

    public class AnswerCallTarget : ICallTarget
    {
        public IContextContainer InCallContext { get; set; }

        public AnswerCallTarget(IContextContainer context)
        {
            InCallContext = context;
        }

        public object Call(ConceptCave.Core.IContextContainer context, object[] args)
        {
            if (args.Length >= 2 && args[0] is WorkingDocumentGetter)
            {
                var newContext = new ContextContainer(context);
                newContext.Set<IWorkingDocument>(((WorkingDocumentGetter)args[0]).GetWorkingDocument(context));

                var target = new AnswerCallTarget(newContext);

                return target.Call(newContext, args.Skip(1).ToArray());
            }

            object name = null;
            int sequence = 0;
            string section = null;

            if (args.Length >= 1)
                name = args[0];

            if (args.Length >= 2)
                sequence = Convert.ToInt32(args[1]);

            if (args.Length >= 3)
                section = args[2].ToString();

            IWorkingDocument workingDocument = InCallContext.Get<IWorkingDocument>();

            // 0 or -ve numbers start from the last Answer, 1 and +ve numbers start from the 1st Answer
            if (sequence > 0)
                sequence -= 1;          // They are 0 based
            else
                sequence = workingDocument.Answers.AnswerCount(name, section) - 1 + sequence;

            IAnswer answer = workingDocument.Answers.AnswerToQuestion(name, sequence, section);

            return answer;
        }
    }
}
