﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Irony.Parsing;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;
using ConceptCave.Checklist.Lingo.ObjectProviders;

namespace ConceptCave.Checklist.Lingo.Functions
{
    public class AnswersCallTargetFactory : ICallTargetFactory
    {
        public ICallTarget Create(IContextContainer context)
        {
            return new AnswersCallTarget(context);
        }
    }

    public class AnswersCallTarget : ICallTarget
    {
        public IContextContainer InCallContext { get; set; }

        public AnswersCallTarget(IContextContainer context)
        {
            InCallContext = context;
        }

        public object Call(ConceptCave.Core.IContextContainer context, object[] args)
        {
            object name = null;
 
            if (args.Length >= 1)
                name = args[0];

            IWorkingDocument workingDocument = InCallContext.Get<IWorkingDocument>();
            IEnumerable<IAnswer> answers = workingDocument.Answers.AnswersWithName(name);

            ListObjectProvider result = new ListObjectProvider();
            answers.ForEach(a => result.Add(a));

            return result;
        }
    }
}
