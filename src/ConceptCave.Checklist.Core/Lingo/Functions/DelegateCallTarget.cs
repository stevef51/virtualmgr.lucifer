﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;

namespace ConceptCave.Checklist.Lingo.Functions
{
    public class DelegateCallTarget : ICallTarget
    {
        public Func<IContextContainer, object[], object> Callback;
    
        public object  Call(ConceptCave.Core.IContextContainer context, object[] args)
        {
            return Callback(context, args);
        }
    }
}
