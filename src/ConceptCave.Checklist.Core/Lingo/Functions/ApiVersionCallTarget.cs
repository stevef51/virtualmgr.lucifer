﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;

namespace ConceptCave.Checklist.Lingo.Functions
{
    public class ApiVersionCallTarget : ICallTarget
    {
        public object Call(IContextContainer context, object[] args)
        {
            return 1;
        }
    }
}
