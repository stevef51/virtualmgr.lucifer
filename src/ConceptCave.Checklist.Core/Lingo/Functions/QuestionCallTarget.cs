﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Irony.Parsing;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;
using ConceptCave.Checklist.Facilities;

namespace ConceptCave.Checklist.Lingo.Functions
{
    public class QuestionCallTargetFactory : ICallTargetFactory
    {
        public ICallTarget Create(IContextContainer context)
        {
            return new QuestionCallTarget(context);
        }
    }

    public class QuestionCallTarget : ICallTarget
    {
        public IContextContainer InCallContext { get; set; }

        public QuestionCallTarget(IContextContainer context)
        {
            InCallContext = context;
        }

        public object Call(ConceptCave.Core.IContextContainer context, object[] args)
        {
            if (args.Length >= 2 && args[0] is WorkingDocumentGetter)
            {
                var newContext = new ContextContainer(context);
                newContext.Set<IWorkingDocument>(((WorkingDocumentGetter)args[0]).GetWorkingDocument(context));

                var target = new QuestionCallTarget(newContext);

                return target.Call(newContext, args.Skip(1).ToArray());
            }

            object id = null;

            if (args.Length >= 1)
                id = args[0];

            IWorkingDocument workingDocument = InCallContext.Get<IWorkingDocument>();

            IQuestion question = workingDocument.Questions.Lookup(id);

            return question;
        }
    }
}
