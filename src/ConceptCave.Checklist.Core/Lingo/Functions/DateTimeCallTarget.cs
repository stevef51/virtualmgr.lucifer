﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;

namespace ConceptCave.Checklist.Lingo.Functions
{
    public class DateTimeCallTarget : ICallTarget
    {
        public object Call(ConceptCave.Core.IContextContainer context, object[] args)
        {
            if (args.Length == 0)
                //return DateTime.Now;
                return DateTime.UtcNow;

            else if (args.Length == 1)
            {
                if (args[0] == null)
                    return null;

                if (args[0] is DateTime)
                    return (DateTime)args[0];

                if (args[0] is string)
                {
                    return DateTime.Parse((string)args[0]);
                }

                return new DateTime(Convert.ToInt64(args[0]), DateTimeKind.Utc);
            }
            else if (args.Length == 3)
                return new DateTime(Convert.ToInt32(args[0]), Convert.ToInt32(args[1]), Convert.ToInt32(args[2]), 0, 0, 0, DateTimeKind.Utc);

            else if (args.Length == 6)
                return new DateTime(Convert.ToInt32(args[0]), Convert.ToInt32(args[1]), Convert.ToInt32(args[2]), Convert.ToInt32(args[3]), Convert.ToInt32(args[4]), Convert.ToInt32(args[5]), DateTimeKind.Utc);

            else
                throw new LingoException("Wrong number of arguments to DateTime()");
        }
    }
}
