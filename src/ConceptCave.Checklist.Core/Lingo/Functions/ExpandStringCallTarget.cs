﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;

namespace ConceptCave.Checklist.Lingo.Functions
{
    public class ExpandStringCallTarget : ICallTarget
    {
        public object Call(ConceptCave.Core.IContextContainer context, object[] args)
        {
            if (args.Length == 0)
            {
                return string.Empty;
            }

            if((args[0] is string) == false)
            {
                return string.Empty;
            }

            IWorkingDocument wd = null;

            if (args.Length == 2)
            {
                object arg2 = args[1];

                if (arg2 is ConceptCave.Checklist.Facilities.WorkingDocumentGetter)
                {
                    wd = ((ConceptCave.Checklist.Facilities.WorkingDocumentGetter)arg2).GetWorkingDocument(context);
                }
                else if (arg2 is IWorkingDocument)
                {
                    wd = (IWorkingDocument)arg2;
                }
            }
            else
            {
                wd = context.Get<IWorkingDocument>();
            }

            return wd.Program.ExpandString((string)args[0]);
        }
    }
}
