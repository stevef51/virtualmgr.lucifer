﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;

namespace ConceptCave.Checklist.Lingo.Functions
{
    public class ItemAtCallTarget : ICallTarget
    {
        #region ICallTarget Members

        public object Call(ConceptCave.Core.IContextContainer context, object[] args)
        {
            if (args.Length != 2)
            {
                return null;
            }

            int index = 0;

            if ((args[1] is int) == false)
            {
                if (args[1] is decimal || args[1] is double)
                {
                    if (int.TryParse(args[1].ToString(), out index) == false)
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }
            else
            {
                index = (int)args[1];
            }


            IList items = null;

            if (args[0] is IList)
            {
                items = (IList)args[0];
            }
            else if (args[0] is IEnumerable)
            {
                IEnumerable enumerable = (IEnumerable)args[0];
                items = new System.Collections.ArrayList();

                foreach (var item in enumerable)
                {
                    items.Add(item);
                }
            }

            // > not >= as index is 1 based to be consistent with the present question 1,2,3 type indexing
            // we don't accept 0 as we are 1 based
            if (Math.Abs(index) > items.Count || index == 0)
            {
                return null;
            }

            // support pulling stuff from the top of the list through -ve notation
            if (index < 0)
            {
                index = items.Count + index + 1; // remember its a + as we have count - -index so a + gets a subtraction
            }

            return items[index - 1];
        }

        #endregion
    }
}
