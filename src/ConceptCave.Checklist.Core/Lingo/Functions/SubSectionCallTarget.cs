﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Irony.Parsing;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;

namespace ConceptCave.Checklist.Lingo.Functions
{
    public class SubSectionCallTargetFactory : ICallTargetFactory
    {
        public ICallTarget Create(IContextContainer context)
        {
            return new SubSectionCallTarget(context);
        }
    }

    public class SubSectionCallTarget : ICallTarget
    {
        public IContextContainer InCallContext { get; set; }

        public SubSectionCallTarget(IContextContainer context)
        {
            InCallContext = context;
        }

        public object Call(ConceptCave.Core.IContextContainer context, object[] args)
        {
            string name = null;
 
            if (args.Length == 0)
                return null;

            name = args[0].ToString();

            IWorkingDocument workingDocument = InCallContext.Get<IWorkingDocument>();

            foreach(ISection rootSection in workingDocument.Sections)
            {
                var f = (from s in ((from s in rootSection.AsDepthFirstEnumerable() where s is ISection select s).Cast<ISection>()) where string.Compare(((ISection)s).Name, name, true) == 0 select s).FirstOrDefault();
                if (f != null)
                    return f;
            }

            return null;
        }
    }
}
