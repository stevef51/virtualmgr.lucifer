﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.Lingo.ObjectProviders;

namespace ConceptCave.Checklist.Lingo.Functions
{
    public class DictionaryCallTarget : ICallTarget
    {
        public object Call(ConceptCave.Core.IContextContainer context, object[] args)
        {
            if ((args.Length & 2) == 1)
                throw new LingoException("Dictionary arguments must be in name-value pairs (even number)");

            var d = new Dictionary<string, object>();
            for (int i = 0; i < args.Length; i += 2)
                d[args[i + 0].ToString()] = args[i + 1];
            return new DictionaryObjectProvider() { Dictionary = d };
        }
    }
}
