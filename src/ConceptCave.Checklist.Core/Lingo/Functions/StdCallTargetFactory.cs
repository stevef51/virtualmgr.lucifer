﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;

namespace ConceptCave.Checklist.Lingo.Functions
{
    public class StdCallTargetFactory : ICallTargetFactory
    {
        public ICallTarget CallTarget { get; set; }

        public ICallTarget Create(ConceptCave.Core.IContextContainer context)
        {
            return CallTarget;
        }
    }
}
