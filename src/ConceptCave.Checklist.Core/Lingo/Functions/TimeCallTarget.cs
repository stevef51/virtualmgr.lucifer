﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;

namespace ConceptCave.Checklist.Lingo.Functions
{
    public class TimeCallTarget : ICallTarget
    {
        public object Call(ConceptCave.Core.IContextContainer context, object[] args)
        {
            if (args.Length == 1)
            {
                if (args[0] is string)
                {
                    // doing a TimeSpan parse doesn't work with 07:30 AM type format, which seems like a valid use case here, DateTime parse is more robust with this sort of thing
                    DateTime temp = DateTime.Parse((string)args[0]); 
                    return temp.TimeOfDay;
                }

                return new TimeSpan(Convert.ToInt64(args[0]));
            }
            else if (args.Length == 3)
				return new TimeSpan(Convert.ToInt32(args[0]), Convert.ToInt32(args[1]), Convert.ToInt32(args[2]));

            else if (args.Length == 4)
                return new TimeSpan(Convert.ToInt32(args[0]), Convert.ToInt32(args[1]), Convert.ToInt32(args[2]), Convert.ToInt32(args[3]));

            else if (args.Length == 5)
				return new TimeSpan(Convert.ToInt32(args[0]), Convert.ToInt32(args[1]), Convert.ToInt32(args[2]), Convert.ToInt32(args[3]), Convert.ToInt32(args[4]));

            else
                throw new LingoException("Wrong number of arguments to Time()");
        }
    }
}
