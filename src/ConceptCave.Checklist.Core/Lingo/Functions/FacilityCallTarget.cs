﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Irony.Parsing;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;

namespace ConceptCave.Checklist.Lingo.Functions
{
    public class FacilityCallTargetFactory : ICallTargetFactory
    {
        public ICallTarget Create(IContextContainer context)
        {
            return new FacilityCallTarget(context);
        }
    }

    public class FacilityCallTarget : ICallTarget
    {
        public IContextContainer InCallContext { get; set; }

        public FacilityCallTarget(IContextContainer context)
        {
            InCallContext = context;
        }

        public object Call(ConceptCave.Core.IContextContainer context, object[] args)
        {
            object id = null;

            if (args.Length >= 1)
                id = args[0];

            IWorkingDocument workingDocument = InCallContext.Get<IWorkingDocument>();

            IFacility facility = workingDocument.Facilities.Lookup(id);

            return facility;
        }
    }
}
