﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;

namespace ConceptCave.Checklist.Lingo.Functions
{
    public class IsNullCallTarget : ICallTarget
    {
        public object Call(ConceptCave.Core.IContextContainer context, object[] args)
        {
            foreach (object o in args)
                if (o != null)
                    return false;
            return true;
        }
    }
}
