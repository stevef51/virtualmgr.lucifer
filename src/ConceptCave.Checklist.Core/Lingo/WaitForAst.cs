﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.Commands.Runtime;
using ConceptCave.Core;
using Irony.Ast;

namespace ConceptCave.Checklist.Lingo
{
    public class WaitForAst : LingoInstruction
    {
        public IEvaluatable WaitFor { get; private set; }
        public IdentifierAst WithIdentifier { get; private set; }
        public StatementBlockAst Codeblock { get; private set; }

        public override void Init(AstContext context, Irony.Parsing.ParseTreeNode parseTreeNode)
        {
            base.Init(context, parseTreeNode);

            WaitFor = parseTreeNode.ChildNodes[1].AstNode as IEvaluatable;

            if (parseTreeNode.ChildNodes[2].FindTokenAndGetText() == LingoGrammar.Keywords.With)
            {
                WithIdentifier = parseTreeNode.ChildNodes[2].ChildNodes[1].AstNode as IdentifierAst;
                Codeblock = AddChild(parseTreeNode.ChildNodes[2].ChildNodes[2]) as StatementBlockAst;
            }
        }

        public override Interfaces.InstructionResult Execute(IContextContainer context)
        {
            base.PreExecute(context);
            var program = context.Get<ILingoProgram>();
            var blockers = PrivateVariable<List<IBlocker>>(program, "Blockers");

            if (blockers.Value == null)
            {
                program.Do(new SetVariableCommand<List<IBlocker>>() { Variable = blockers, NewValue = new List<IBlocker>() });
                ContextContainer newContext = new ContextContainer(context);
                context.Set<bool>("WaitFor", true);
                object eval = WaitFor.Evaluate(newContext);
                IEnumerable<object> oList = eval as IEnumerable<object>;
                if (oList != null)
                {
                    foreach (object o in oList)
                    {
                        if (o is IBlocker)
                            blockers.Value.Add(o as IBlocker);
                    }
                }
                else if (eval is IBlocker)
                    blockers.Value.Add(eval as IBlocker);

                if (blockers.Value.Count > 0)
                    blockers.Value.ForEach(b => b.Block(program.WorkingDocument));
            }
            else if (Codeblock != null)
            {
                // Not the 1st time the Wait is executed, if we have a codeblock to run then evaluate the WithIdentifier and run codeblock
                if (WithIdentifier != null)
                    WithIdentifier.Assign(context, program.WorkingDocument.LastUnblockedBlocker);

                InstructionResult codeBlockResult = Codeblock.Execute(context);
                if (codeBlockResult != InstructionResult.Finished)
                    return codeBlockResult;
                else
                    Codeblock.Reset(program, true);
            }

            bool blocked = false;
            if (blockers.Value.Count > 0)
                blockers.Value.ForEach(b => blocked |= b.IsBlocked);

            if (blocked && program.RunMode.Value != RunMode.Blocked)
                program.Do(new SetVariableCommand<RunMode>() { Variable = program.RunMode, NewValue = RunMode.Blocked });

            return blocked ? InstructionResult.Unfinished : InstructionResult.Finished;
        }
    }
}
