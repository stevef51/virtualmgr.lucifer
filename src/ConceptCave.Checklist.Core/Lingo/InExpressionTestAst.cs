﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Core;
using ConceptCave.Checklist.Interfaces;
using Irony.Ast;

namespace ConceptCave.Checklist.Lingo
{
    public class InExpressionTestAst : LingoAst, IBooleanTest
    {
        public IEvaluatable ExpressionList { get; private set; }

        public override void Init(AstContext context, Irony.Parsing.ParseTreeNode parseTreeNode)
        {
            base.Init(context, parseTreeNode);

            ExpressionList = parseTreeNode.ChildNodes[1].AstNode as IEvaluatable;
        }

        #region IBooleanTest Members

        public bool Test(object value, IContextContainer context)
        {
            var evalList = ObjectConverter.CreateEnumerablePrimitiveOf<object>(ExpressionList.Evaluate(context));
            var valueList = ObjectConverter.CreateEnumerablePrimitiveOf<object>(value);

            int valueListCount = valueList.Count();

            if (valueListCount > 0)
            {
                var intersect = evalList.Intersect(valueList, new ObjectConverter());
                int intersectCount = intersect.Count();
                return intersectCount == valueListCount;
            }
            else
                return false;
        }

        #endregion
    }
}
