﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Core.Coding;
using System.Xml;
using ConceptCave.Core.XmlCodable;
using ConceptCave.Checklist.Core;
using System.IO;
using ConceptCave.Core.SBONCodable;
using ConceptCave.Checklist;

namespace ConceptCave.Core
{
    public class FrozenState
    {
		public readonly string State;
        
		public FrozenState(string state)
        {
            State = state;
        }
    }

    public static class FrozenStateExtensions
    {
        public static FrozenState Freeze(this ICodable codable)
        {
            var encoder = CoderFactory.CreateEncoder();
            encoder.Context.Set<bool>("ForFreezing", true);
            encoder.Encode(null, codable);

			return new FrozenState(encoder.ToString());
        }

        public static void Unfreeze(this ICodable codable, FrozenState state)
        {
			var decoder = CoderFactory.GetDecoder(state.State, null);
            decoder.Context.Set<bool>("ForFreezing", true);
            codable.Decode(decoder);
        }
    }
}
