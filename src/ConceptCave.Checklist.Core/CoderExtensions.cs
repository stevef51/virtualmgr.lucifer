﻿#define USE_SBON

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Core.Coding;
using ConceptCave.Core.XmlCodable;
using ConceptCave.Checklist.Core;
using System.IO;
using ConceptCave.Core.SBONCodable;


namespace ConceptCave.Checklist
{
    public static class CoderExtensions
    {
        public static bool IsForStorage(this ICoder coder)
        {
            return coder.Context.Get<bool>("IsForStorage");
        }

        public static bool IsForPosting(this ICoder coder)
        {
            return coder.Context.Get<bool>("IsForPosting");
        }

		public static string StateAsString(this ICodable codable)
        {
#if USE_SBON 
            var encoder = CoderFactory.CreateEncoder();
			encoder.Encode ("State", codable);

			return encoder.ToString();
			//return ms.ToArray ();
#else
            XmlDocumentEncoder encoder = new XmlDocumentEncoder();
            encoder.Encode("State", codable);
            return encoder.Document.OuterXml;
#endif
		}

		public static string StateAsString(this ICodable codable, params string[] trueContext)
        {
#if USE_SBON 
            var encoder = CoderFactory.CreateEncoder();
			foreach (string s in trueContext)
				encoder.Context.Set<bool>(s, true);
			encoder.Encode ("State", codable);

			return encoder.ToString();
			//return ms.ToArray ();
#else
			XmlDocumentEncoder encoder = new XmlDocumentEncoder();
            foreach (string s in trueContext)
                encoder.Context.Set<bool>(s, true);
            encoder.Encode("State", codable);
            return encoder.Document.OuterXml;
#endif
        }
    }
}
