﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;

namespace ConceptCave.Checklist.Commands.Runtime
{
    public class AddAnswerCommand : WorkingDocumentCommand
    {
        public IAnswer Answer { get; set; }

        protected override void DoIt()
        {
            WorkingDocument.Answers.Add(Answer);
        }

        protected override void UndoIt()
        {
            WorkingDocument.Answers.Remove(Answer);
        }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("Answer", Answer);
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            Answer = decoder.Decode<IAnswer>("Answer");
        }
    }
}
