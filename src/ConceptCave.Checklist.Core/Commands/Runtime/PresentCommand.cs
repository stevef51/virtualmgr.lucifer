﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Core;
using ConceptCave.Checklist.Interfaces;

namespace ConceptCave.Checklist.Commands.Runtime
{
    public class PresentCommand : WorkingDocumentCommand
    {
        public IPresentable Presentable { get; set; }
        public string NameAs { get; set; }
        public IPresented Presented { get; private set; }
        public bool Hidden { get; set; }

        protected override void DoIt()
        {
            Presented = Presentable.CreatePresented(WorkingDocument, NameAs, WorkingDocument);
            Presented.Hidden = Hidden;
            WorkingDocument.Presented.Add(Presented);

            if (Hidden)
            {
                foreach (IPresented p in Presented.AsDepthFirstEnumerable())
                    if (p is IPresentedQuestion)
                        ((IPresentedQuestion)p).GetAnswer();
            }
        }

        protected override void UndoIt()
        {
            WorkingDocument.Presented.Remove(Presented);
        }

        protected override void RedoIt()
        {
            WorkingDocument.Presented.Add(Presented);
           
            if (Hidden)
            {
                foreach (IPresented p in Presented.AsDepthFirstEnumerable())
                    if (p is IPresentedQuestion)
                        ((IPresentedQuestion)p).GetAnswer();
            }
        }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("Presentable", Presentable);
            encoder.Encode("Presented", Presented);
            encoder.Encode("NameAs", NameAs);
            encoder.Encode("Hidden", Hidden);
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            Presentable = decoder.Decode<IPresentable>("Presentable");
            Presented = decoder.Decode<IPresented>("Presented");
            NameAs = decoder.Decode<string>("NameAs");
            Hidden = decoder.Decode<bool>("Hidden");
        }
    }
}
