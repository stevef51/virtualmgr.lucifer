﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;

namespace ConceptCave.Checklist.Commands.Runtime
{
    public class AddFacilityActionCommand : WorkingDocumentCommand
    {
        public IFacilityAction FacilityAction { get; set; }

        protected override void DoIt()
        {
            WorkingDocument.Actions.Add(FacilityAction);
        }

        protected override void UndoIt()
        {
            WorkingDocument.Actions.Remove(FacilityAction);
        }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("FacilityAction", FacilityAction);
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            FacilityAction = decoder.Decode<IFacilityAction>("FacilityAction");
        }
    }
}
