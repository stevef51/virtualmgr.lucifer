﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;
using ConceptCave.Checklist.Lingo.ObjectProviders;

namespace ConceptCave.Checklist.Commands.Runtime
{
    public class SetPropertyCommand : WorkingDocumentCommand
    {
        public object Object { get; set; }
        public string Name { get; set; }
        public object NewValue { get; set; }
        public object OldValue { get; set; }

        private IObjectSetter _setter;

        public SetPropertyCommand()
        {

        }

        public SetPropertyCommand(object obj, string name, object newValue)
        {
            Object = obj;
            Name = name;
            NewValue = newValue;
        }

        protected override void DoIt()
        {
            if (_setter == null)
            {
                _setter = new ReflectionObjectProvider(Object);
            }
            object oldValue = null;
            if (_setter.GetObject(null, Name, out oldValue))
            {
                OldValue = oldValue;
                _setter.SetObject(null, Name, NewValue);
            }
        }

        protected override void UndoIt()
        {
            if (_setter == null)
            {
                _setter = new ReflectionObjectProvider(Object);
            }
            _setter.SetObject(null, Name, OldValue);
        }

        protected override void RedoIt()
        {
            if (_setter == null)
            {
                _setter = new ReflectionObjectProvider(Object);
            }
            _setter.SetObject(null, Name, NewValue);
        }

        public override string ToString()
        {
            return string.Format("SetPropertyCommand: Object={0}.{1} = {2}", Object, Name, NewValue);
        }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("Object", Object);
            encoder.Encode("Name", Name);
            encoder.Encode("OldValue", OldValue);
            encoder.Encode("NewValue", NewValue);
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            Object = decoder.Decode<object>("Object");
            Name = decoder.Decode<string>("Name");
            OldValue = decoder.Decode<object>("OldValue");
            NewValue = decoder.Decode<object>("NewValue");
        }
    }
}
