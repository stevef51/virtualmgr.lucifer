﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Core;
using ConceptCave.Checklist.Editor;
using ConceptCave.Checklist.RunTime;

namespace ConceptCave.Checklist.Commands.Runtime
{
    public abstract class WorkingDocumentCommand : Command
    {
        public WorkingDocument WorkingDocument { get { return (WorkingDocument)Document; } }
    }
}
