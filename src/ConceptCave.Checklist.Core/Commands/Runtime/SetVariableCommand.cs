﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;

namespace ConceptCave.Checklist.Commands.Runtime
{
    public class SetVariableCommand<T> : WorkingDocumentCommand
    {
        public IVariable<T> Variable { get; set; }
        public T NewValue { get; set; }
        public T OldValue { get; set; }

        protected override void DoIt()
        {
            OldValue = Variable.Value;
            Variable.Value = NewValue;
        }

        protected override void UndoIt()
        {
            Variable.Value = OldValue;
        }

        protected override void RedoIt()
        {
            Variable.Value = NewValue;
        }

        public override string ToString()
        {
            return string.Format("SetVariableCommand: Variable={0}, NewValue={1}, OldValue={2}", Variable, NewValue, OldValue);
        }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("Variable", Variable);
            encoder.Encode("NewValue", NewValue);
            encoder.Encode("OldValue", OldValue);
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            Variable = decoder.Decode<IVariable<T>>("Variable");
            NewValue = decoder.Decode<T>("NewValue");
            OldValue = decoder.Decode<T>("OldValue");
        }
    }
}
