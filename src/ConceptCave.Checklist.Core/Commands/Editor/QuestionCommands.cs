﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Core;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.Editor;

namespace ConceptCave.Checklist.Core.Commands
{
    public abstract class QuestionCommand<Q> : Command where Q : IQuestion
    {
        public Q Question { get; set; }

        public DesignDocument DesignDocument { get { return Document as DesignDocument; } }
    }

    public class InsertQuestionCommand<Q> : QuestionCommand<Q> where Q : IQuestion
    {
        public int InsertAt { get; set; }

        public InsertQuestionCommand()
        {
            InsertAt = -1;              // Append
        }

        protected override void DoIt()
        {
            if (InsertAt == -1)
                DesignDocument.Questions.Add(Question);
            else
                DesignDocument.Questions.Insert(InsertAt, Question);
        }

        protected override void UndoIt()
        {
            DesignDocument.Questions.Remove(Question);
        }
    }

    public class EditQuestionCommand<Q> : QuestionCommand<Q> where Q : IQuestion
    {
        private FrozenState _undoneState;
        private FrozenState _doneState;

        public Q NewQuestion { get; set; }

        protected override void DoIt()
        {
            _undoneState = Question.Freeze();
            _doneState = NewQuestion.Freeze();

            Question.Unfreeze(_doneState);
        }

        protected override void UndoIt()
        {
            Question.Unfreeze(_undoneState);
        }

        protected override void RedoIt()
        {
            Question.Unfreeze(_doneState);
        }
    }

    public class DeleteQuestionCommand<Q> : QuestionCommand<Q> where Q : IQuestion
    {
        private int _removedIndex;

        protected override void DoIt()
        {
            _removedIndex = DesignDocument.Questions.IndexOf(Question);
            if (_removedIndex != -1)
                DesignDocument.Questions.Remove(Question);
            else
                SetCan(CommandCan.DoNothing);
        }

        protected override void  UndoIt()
        {
            DesignDocument.Questions.Insert(_removedIndex, Question);
        }

        protected override void RedoIt()
        {
            DesignDocument.Questions.Remove(Question);
        }
    }
}
