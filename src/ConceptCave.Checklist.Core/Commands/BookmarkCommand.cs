﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Core;

namespace ConceptCave.Checklist.Commands
{
    /// <summary>
    /// BookmarkCommand's are inserted into a CommandManager which can then be searched for when Undoing to mark an Undo point
    /// </summary>
    public class BookmarkCommand : ICommand
    {
        private Guid _id;
        public string Tag { get; set; }

        public BookmarkCommand()
        {
            _id = Guid.NewGuid();
        }

        #region ICommand Members

        public IDocument Document { get; set; }

        public void Do()
        {
        }

        public void Undo()
        {
        }

        public void Redo()
        {
        }

        public bool CanDo
        {
            get { return true; }
        }

        public bool CanUndo
        {
            get { return true; }
        }

        public bool CanRedo
        {
            get { return true; }
        }

        #endregion

        #region IUniqueNode Members

        public Guid Id
        {
            get { return _id; }
        }

        public void UseDecodedId(Guid id)
        {
            _id = id;
        }

        #endregion

        #region ICodable Members

        public void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            encoder.Encode("Tag", Tag);
        }

        public void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            Tag = decoder.Decode<string>("Tag", null);
        }

        #endregion
    }
}
