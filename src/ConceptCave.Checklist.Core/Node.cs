﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using ConceptCave.Core.Coding;
using ConceptCave.Checklist;
#if !iOS
using NLog;
#endif

namespace ConceptCave.Core
{
    public abstract class Node : IUniqueNode
    {
        protected Guid _id;

#if !iOS
		protected static Logger logger = LogManager.GetCurrentClassLogger();
#endif

        public Node()
        {
            _id = CombFactory.NewComb();
        }

        void IUniqueNode.UseDecodedId(Guid id)
        {
            _id = id;
        }

        #region INode Members

        public Guid Id
        {
            get { return _id; }
        }

        #endregion
   
        #region ICodable Members

        public virtual void Encode(IEncoder encoder)
        {
        }

        public virtual void Decode(IDecoder decoder)
        {
        }

        #endregion
    }
}
