﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Core;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.Editor;
using ConceptCave.Checklist.Core;
using ConceptCave.Checklist.Lingo;

namespace ConceptCave.Checklist
{
    public class LingoDocument : Document, ILingoDocument
    {
        public INamedList<IQuestion> Questions { get; set; }
        public INamedList<ISection> Sections { get; set; }
        public IVariableBag Variables { get; set; }
        public INamedList<IFacility> Facilities { get; set; }

        public virtual FinishMode FinishMode { get; set; }

        public LingoDocument()
        {
            Variables = new VariableBag();
            Questions = new NamedList<IQuestion>(1);
            Sections = new NamedList<ISection>(1);
            Facilities = new NamedList<IFacility>(1);

            FinishMode = FinishMode.Manual;
        }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("Questions", Questions);
            encoder.Encode("Sections", Sections);
            encoder.Encode("Variables", Variables);
            encoder.Encode("Facilities", Facilities);
            encoder.Encode("FinishMode", FinishMode);
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            Questions = decoder.Decode<NamedList<IQuestion>>("Questions", new NamedList<IQuestion>(1));
            Sections = decoder.Decode<NamedList<ISection>>("Sections", new NamedList<ISection>(1));
            Variables = decoder.Decode<VariableBag>("Variables", new VariableBag());
            Facilities = decoder.Decode<NamedList<IFacility>>("Facilities", new NamedList<IFacility>(1));
            FinishMode = decoder.Decode<FinishMode>("FinishMode", FinishMode.Manual);
        }

        public IEnumerable<IPresentable> AsDepthFirstEnumerable()
        {
            foreach (Section section in Sections)
            {
                foreach (IPresentable p in section.AsDepthFirstEnumerable())
                {
                   yield return p;
                }
            }
        }

        /// <summary>
        /// Utility method to have the document populate its list of questions from the set of presentables that it holds.
        /// </summary>
        public void PopulateQuestionsFromPresentables()
        {
            var questions = (from p in this.AsDepthFirstEnumerable() where p is IQuestion select p as IQuestion);

            Questions.Clear();

            foreach (var question in questions)
            {
                Questions.Add(question);
            }
        }
    }
}
