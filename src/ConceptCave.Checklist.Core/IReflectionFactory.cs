﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.Checklist
{
    //This guy gets used by Checklist and BL to create types
    //basically a drop-in replacement for Activator.CreateInstance
    //Allows the front-end (i.e. www or mobile) to implement him and
    //DI inject stuff into created types
    public interface IReflectionFactory
    {
        T InstantiateObject<T>();
        object InstantiateObject(Type type);
    }
}
