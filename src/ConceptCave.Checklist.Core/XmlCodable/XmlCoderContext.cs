﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Core.Coding;

namespace ConceptCave.Core.XmlCodable
{
    public class XmlCoderContext
    {
        private XmlCoderTypeCache _typeCache;
        private XmlCoderObjectCache _objectCache;
        private IContextContainer _context;

        public event Action<IUniqueNode> OnUniqueNodeCreated;

        public XmlCoderTypeCache TypeCache { get { return _typeCache; } }
        public XmlCoderObjectCache ObjectCache { get { return _objectCache; } }
        public IContextContainer Context { get { return _context; } }

        public XmlCoderContext(XmlCoderTypeCache typeCache, XmlCoderObjectCache objectCache, IContextContainer context)
        {
            _typeCache = typeCache;
            _objectCache = objectCache;
            _context = context;
        }

        public void UniqueNodeCreated(IUniqueNode uniqueNode)
        {
            if (OnUniqueNodeCreated != null)
                OnUniqueNodeCreated(uniqueNode);
        }
    }
}
