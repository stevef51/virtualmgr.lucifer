﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Xml;
using ConceptCave.Checklist;
using ConceptCave.Core;
using System.Reflection;
using ConceptCave.Core.Coding;

namespace ConceptCave.Core.XmlCodable
{
	[AttributeUsage(AttributeTargets.Class, AllowMultiple=false)]
	public class XmlElementConverterAttribute : SelfRegisteringAttribute
	{
		public Type ConverterType;

		public XmlElementConverterAttribute(Type converterType)
		{
			ConverterType = converterType;
		}

		public override void RegisterAttributesOnType(Type type)
		{
			XmlElementConverter.RegisterConverterFactory(new XmlTypeConverterFactory() { TypeToConvert = type, Converter = Activator.CreateInstance(ConverterType) as IXmlElementConverter });
		}
	}

	public interface IXmlElementConverterFactory
	{
		bool CanConvertType(Type typeToConvert);
		IXmlElementConverter CreateConverter();
	}

	public interface IXmlElementConverter
	{
		void EncodeInXmlElement(XmlElement element, object value, XmlCoderContext context);
		object DecodeFromXmlElement(XmlElement element, Type typeToConvert, XmlCoderContext context);
	}

	public class XmlElementConverter
	{
		private static List<IXmlElementConverterFactory> _converterFactoryTypes = new List<IXmlElementConverterFactory>();
	    private static IReflectionFactory _globalReflectionFactory;

		public static void InitConverters(IReflectionFactory globalReflectionFactory)
		{
		    _globalReflectionFactory = globalReflectionFactory;

			try
			{
                // EVS-1014 - Dynamic registration not required, we manually load them below .. (faster)
//				SelfRegisteringAttribute.RegisterFromAppDomain(AppDomain.CurrentDomain);
				RegisterConverterFactory(new XmlElementDateTimeConverter());
				RegisterConverterFactory(new XmlElementStringConvertibleConverter());
				RegisterConverterFactory(new XmlElementUniqueNodeConverter(_globalReflectionFactory));
				RegisterConverterFactory(new XmlElementCodableConverter(_globalReflectionFactory));
				RegisterConverterFactory(new XmlElementDictionaryConverter());
				RegisterConverterFactory(new XmlElementDictionaryEntryConverter());
				RegisterConverterFactory(new XmlElementKeyValuePairConverter());
				RegisterConverterFactory(new XmlElementPrimitiveArrayConverter());
				RegisterConverterFactory(new XmlElementArrayConverter());
				RegisterConverterFactory(new XmlElementPrimitiveCollectionConverter());
				RegisterConverterFactory(new XmlElementCollectionConverter());
                RegisterConverterFactory(new XmlElementJValueConvertibleConverter());
			}
			catch (ReflectionTypeLoadException e)
			{
				throw e.LoaderExceptions.First();
			}
		}

		public static void RegisterConverterFactory(IXmlElementConverterFactory converterFactory)
		{
			_converterFactoryTypes.Add(converterFactory);
		}

		public static void EncodeInXmlElement(XmlElement element, object value, XmlCoderContext context)
		{
			Type typeToConvert = value.GetType();
			var converter = (from c in _converterFactoryTypes where c.CanConvertType(typeToConvert) select c).DefaultIfEmpty(null).First();

			if (converter == null)
			{
				throw new InvalidOperationException(string.Format("No XmlElementConverter defined for {0}", typeToConvert.FullName));
			}

			converter.CreateConverter().EncodeInXmlElement(element, value, context);
		}

		public static object DecodeFromXmlElement(XmlElement element, Type typeToConvert, XmlCoderContext context)
		{
			var converter = (from c in _converterFactoryTypes where c.CanConvertType(typeToConvert) select c).First();

			if (converter == null)
			{
				throw new InvalidOperationException(string.Format("No XmlElementConverter defined for {0}", typeToConvert.FullName));
			}

			return converter.CreateConverter().DecodeFromXmlElement(element, typeToConvert, context);
		}

		public static bool CanConvertType(Type typeToConvert)
		{
			return (from c in _converterFactoryTypes where c.CanConvertType(typeToConvert) select c).Count() > 0;
		}
	}

	/// <summary>
	/// Generic class which can convert any TypeConveter'able class to an XmlElement using its InvariantString representation (primarily primitive types)
	/// </summary>
	public class XmlElementStringConvertibleConverter : IXmlElementConverter, IXmlElementConverterFactory
	{
		public void EncodeInXmlElement(XmlElement element, object value, XmlCoderContext context)
		{
			TypeConverter converter = TypeDescriptor.GetConverter(value.GetType());
			element.InnerText = converter.ConvertToInvariantString(value);
		}

		public object DecodeFromXmlElement(XmlElement element, Type typeToConvert, XmlCoderContext context)
		{
			TypeConverter converter = TypeDescriptor.GetConverter(typeToConvert);
			return converter.ConvertFromInvariantString(element.InnerText);
		}

		public bool CanConvertType(Type typeToConvert)
		{
			TypeConverter converter = TypeDescriptor.GetConverter(typeToConvert);
			return converter != null && converter.CanConvertTo(typeof(String)) && converter.CanConvertFrom(typeof(String));
		}

		public IXmlElementConverter CreateConverter()
		{
			return this;
		}
	}

    public class XmlElementJValueConvertibleConverter : IXmlElementConverter, IXmlElementConverterFactory
    {
        public void EncodeInXmlElement(XmlElement element, object value, XmlCoderContext context)
        {
            TypeConverter converter = TypeDescriptor.GetConverter(value.GetType());
            element.InnerText = converter.ConvertToInvariantString(value);
        }

        public object DecodeFromXmlElement(XmlElement element, Type typeToConvert, XmlCoderContext context)
        {
            TypeConverter converter = TypeDescriptor.GetConverter(typeToConvert);
            return converter.ConvertFromInvariantString(element.InnerText);
        }

        public bool CanConvertType(Type typeToConvert)
        {
            return typeToConvert == typeof(Newtonsoft.Json.Linq.JValue);
        }

        public IXmlElementConverter CreateConverter()
        {
            return this;
        }
    }

    /// <summary>
    /// Generic class which can convert any TypeConveter'able class to an XmlElement using its InvariantString representation (primarily primitive types)
    /// </summary>
    public class XmlElementDateTimeConverter : IXmlElementConverter, IXmlElementConverterFactory
	{
		public void EncodeInXmlElement(XmlElement element, object value, XmlCoderContext context)
		{
			element.InnerText = System.Xml.XmlConvert.ToString((DateTime)value, XmlDateTimeSerializationMode.RoundtripKind);
		}

		public object DecodeFromXmlElement(XmlElement element, Type typeToConvert, XmlCoderContext context)
		{
			return System.Xml.XmlConvert.ToDateTime(element.InnerText, XmlDateTimeSerializationMode.RoundtripKind);
		}

		public bool CanConvertType(Type typeToConvert)
		{
			return typeToConvert == typeof(DateTime);
		}

		public IXmlElementConverter CreateConverter()
		{
			return this;
		}
	}

	public class XmlElementCodableConverter : IXmlElementConverter, IXmlElementConverterFactory
	{
	    private readonly IReflectionFactory _globalReflectionFactory;

	    public XmlElementCodableConverter(IReflectionFactory globalReflectionFactory)
	    {
	        _globalReflectionFactory = globalReflectionFactory;
	    }

		public void EncodeInXmlElement(XmlElement element, object value, XmlCoderContext coderContext)
		{
			ICodable codable = value as ICodable;
			if (codable != null)
				codable.Encode(new XmlDocumentEncoder(element, coderContext));
		}

		public object DecodeFromXmlElement(XmlElement element, Type typeToConvert, XmlCoderContext context)
		{
		    var reflectionFactory = context.Context.Get<IReflectionFactory>(() => _globalReflectionFactory);
			ICodable codable = reflectionFactory.InstantiateObject(typeToConvert) as ICodable;

			if (codable != null)
			{
				codable.Decode(new XmlDocumentDecoder(element, context));
			}

			return codable;
		}

		public bool CanConvertType(Type typeToConvert)
		{
			return typeof(ICodable).IsAssignableFrom(typeToConvert);
		}

		public IXmlElementConverter CreateConverter()
		{
			return this;
		}
	}


	public class XmlElementUniqueNodeConverter : IXmlElementConverter, IXmlElementConverterFactory
	{
        private readonly IReflectionFactory _globalReflectionFactory;

	    public XmlElementUniqueNodeConverter(IReflectionFactory globalReflectionFactory)
	    {
	        _globalReflectionFactory = globalReflectionFactory;
	    }


		public void EncodeInXmlElement(XmlElement element, object value, XmlCoderContext context)
		{
			IUniqueNode uniqueNode = value as IUniqueNode;
			if (uniqueNode != null)
			{
				element.SetAttribute(XmlDocumentCoder.IdAttribute, uniqueNode.Id.ToString());
				uniqueNode.Encode(new XmlDocumentEncoder(element, context));
			}
		}

		public object DecodeFromXmlElement(XmlElement element, Type typeToConvert, XmlCoderContext context)
		{
		    var reflectionFac = context.Context.Get<IReflectionFactory>(() => _globalReflectionFactory);
            IUniqueNode uniqueNode = reflectionFac.InstantiateObject(typeToConvert) as IUniqueNode;

			if (uniqueNode != null)
			{
				// Must set the objects Id
				uniqueNode.UseDecodedId(new Guid(element.GetAttribute(XmlDocumentCoder.IdAttribute)));

				context.UniqueNodeCreated(uniqueNode);

				// Now we can decode remainder of the object
				uniqueNode.Decode(new XmlDocumentDecoder(element, context));
			}

			return uniqueNode;
		}

		public bool CanConvertType(Type typeToConvert)
		{
			return typeof(IUniqueNode).IsAssignableFrom(typeToConvert);
		}

		public IXmlElementConverter CreateConverter()
		{
			return this;
		}
	}

	/// <summary>
	/// Factory Class to map a Type to its XmlElementConverter type
	/// </summary>
	public class XmlTypeConverterFactory : IXmlElementConverterFactory
	{
		public Type TypeToConvert;
		public IXmlElementConverter Converter;

		public bool CanConvertType(Type typeToConvert)
		{
			return typeToConvert == TypeToConvert;
		}

		public IXmlElementConverter CreateConverter()
		{
			return Converter;
		}
	}

	public class XmlElementArrayConverter : IXmlElementConverterFactory, IXmlElementConverter
	{
		public bool CanConvertType(Type typeToConvert)
		{
			return typeToConvert.IsArray && XmlElementConverter.CanConvertType(typeToConvert.GetElementType());
		}

		public IXmlElementConverter CreateConverter()
		{
			return this;
		}

		public void EncodeInXmlElement(XmlElement element, object value, XmlCoderContext context)
		{
			Array array = value as Array;
			foreach (object item in array)
			{
				XmlDocumentEncoder encoder = new XmlDocumentEncoder(element, context);
				encoder.Encode(null, item);
			}
		}

		public object DecodeFromXmlElement(XmlElement element, Type typeToConvert, XmlCoderContext context)
		{
			ArrayList list = new ArrayList();
			foreach (XmlElement childElement in element.ChildNodes)
			{
				if (childElement.NodeType != XmlNodeType.Element)
					continue;

				object item = null;
				XmlDocumentDecoder decoder = new XmlDocumentDecoder(childElement, context);
				decoder.Decode(null, out item);

				list.Add(item);
			}

			return list.ToArray(typeToConvert.GetElementType());
		}
	}

	public class XmlElementPrimitiveArrayConverter : IXmlElementConverterFactory, IXmlElementConverter
	{
		public bool CanConvertType(Type typeToConvert)
		{
			return typeToConvert.IsArray && typeToConvert.GetElementType().IsPrimitive;
		}

		public IXmlElementConverter CreateConverter()
		{
			return this;
		}

		public void EncodeInXmlElement(XmlElement element, object value, XmlCoderContext context)
		{
			TypeConverter typeConverter = TypeDescriptor.GetConverter(value.GetType().GetElementType());
			StringBuilder sb = new StringBuilder();
			Array array = value as Array;
			foreach (object item in array)
			{
				if (sb.Length > 0)
					sb.Append(",");
				sb.Append(typeConverter.ConvertToInvariantString(item));
			}
			element.InnerText = sb.ToString();
		}

		public object DecodeFromXmlElement(XmlElement element, Type typeToConvert, XmlCoderContext context)
		{
			TypeConverter typeConverter = TypeDescriptor.GetConverter(typeToConvert.GetElementType());
			string[] parts = element.InnerText.Split(',');
			ArrayList list = new ArrayList();
			foreach (string part in parts)
			{
				list.Add(typeConverter.ConvertFromInvariantString(part));
			}

			return list.ToArray(typeToConvert.GetElementType());
		}
	}


	public class XmlElementPrimitiveCollectionConverter : IXmlElementConverterFactory, IXmlElementConverter
	{
		public bool CanConvertType(Type typeToConvert)
		{
			// Must be a Primitive Collection 
			if (!typeToConvert.IsGenericType)
				return false;

			Type[] genericArgs = typeToConvert.GetGenericArguments();
			if (!genericArgs[0].IsPrimitive)
				return false;

			if (typeof(ICollection).IsAssignableFrom(typeToConvert))
				return true;

			// Catch ICollection<T> (covers Generic.HashSet<T>)
			return typeof(ICollection<>).MakeGenericType(genericArgs).IsAssignableFrom(typeToConvert); 
		}

		public IXmlElementConverter CreateConverter()
		{
			return this;
		}

		public void EncodeInXmlElement(XmlElement element, object value, XmlCoderContext context)
		{
			Type valueType = value.GetType();
			TypeConverter typeConverter = TypeDescriptor.GetConverter(valueType.GetGenericArguments()[0]);
			StringBuilder sb = new StringBuilder();
			IEnumerable list = value as IEnumerable;
			
			foreach (object item in list)
			{
				if (sb.Length > 0)
					sb.Append(",");
				sb.Append(typeConverter.ConvertToInvariantString(item));
			}
			element.InnerText = sb.ToString();
		}

		public object DecodeFromXmlElement(XmlElement element, Type typeToConvert, XmlCoderContext context)
		{
			TypeConverter typeConverter = TypeDescriptor.GetConverter(typeToConvert.GetGenericArguments()[0]);

			Type specificListType = typeof(List<>).MakeGenericType(typeToConvert.GetGenericArguments());
			IList list = (IList)Activator.CreateInstance(specificListType);

			if (!string.IsNullOrEmpty(element.InnerText))
			{
				string[] parts = element.InnerText.Split(',');

				bool requiresReversing = typeToConvert == typeof(Stack) || typeToConvert.IsGenericType && typeToConvert.GetGenericTypeDefinition() == typeof(Stack<>);
				foreach (string part in parts)
				{
					if (requiresReversing)
						list.Insert(0, typeConverter.ConvertFromInvariantString(part));
					else
						list.Add(typeConverter.ConvertFromInvariantString(part));
				}
			}
			return Activator.CreateInstance(typeToConvert, list);
		}
	}

	public class XmlElementCollectionConverter : IXmlElementConverterFactory, IXmlElementConverter
	{
		public bool CanConvertType(Type typeToConvert)
		{
			if (typeof(ICollection).IsAssignableFrom(typeToConvert))
				return true;

			if (!typeToConvert.IsGenericType)
				return false;

			// Catch ICollection<T> (covers Generic.HashSet<T>)
			return typeof(ICollection<>).MakeGenericType(typeToConvert.GetGenericArguments()).IsAssignableFrom(typeToConvert);
		}

		public IXmlElementConverter CreateConverter()
		{
			return this;
		}

		public void EncodeInXmlElement(XmlElement element, object value, XmlCoderContext context)
		{
			IEnumerable list = value as IEnumerable;
			foreach (object item in list)
			{
				XmlDocumentEncoder encoder = new XmlDocumentEncoder(element, context);
				encoder.Encode(null, item);
			}
		}

		public object DecodeFromXmlElement(XmlElement element, Type typeToConvert, XmlCoderContext context)
		{
			Type specificListType = typeof(List<>).MakeGenericType(typeToConvert.GetGenericArguments());
			IList list = (IList)Activator.CreateInstance(specificListType);
			bool requiresReversing = typeToConvert == typeof(Stack) || typeToConvert.IsGenericType && typeToConvert.GetGenericTypeDefinition() == typeof(Stack<>);
			foreach (XmlElement itemElement in element.ChildNodes)
			{
				XmlDocumentDecoder decoder = new XmlDocumentDecoder(itemElement, context);
				object item = null;
				if (decoder.Decode(null, out item))
				{
					if (requiresReversing)
						list.Insert(0, item);
					else
						list.Add(item);
				}
			}

			return Activator.CreateInstance(typeToConvert, list);
		}
	}

	public class XmlElementDictionaryConverter : IXmlElementConverterFactory, IXmlElementConverter
	{
		public bool CanConvertType(Type typeToConvert)
		{
			return typeof(IDictionary).IsAssignableFrom(typeToConvert);
		}

		public IXmlElementConverter CreateConverter()
		{
			return this;
		}

		public void EncodeInXmlElement(XmlElement element, object value, XmlCoderContext context)
		{
			IDictionary dictionary = value as IDictionary;
			foreach (DictionaryEntry entry in dictionary)
			{
				XmlDocumentEncoder encoder = new XmlDocumentEncoder(element, context);
				encoder.Encode(null, entry);
			}
		}

		public object DecodeFromXmlElement(XmlElement element, Type typeToConvert, XmlCoderContext context)
		{
			IDictionary dictionary = (IDictionary)Activator.CreateInstance(typeToConvert);

			foreach (XmlNode entryNode in element.ChildNodes)
			{
				if (!(entryNode is XmlElement))
					continue;

				XmlDocumentDecoder decoder = new XmlDocumentDecoder(entryNode as XmlElement, context);                
				DictionaryEntry entry;
				if (decoder.Decode<DictionaryEntry>(null, out entry))
					dictionary.Add(entry.Key, entry.Value);
			}

			return dictionary;
		}
	}

	public class XmlElementDictionaryEntryConverter : IXmlElementConverterFactory, IXmlElementConverter
	{
		public bool CanConvertType(Type typeToConvert)
		{
			return typeof(DictionaryEntry).IsAssignableFrom(typeToConvert);
		}

		public IXmlElementConverter CreateConverter()
		{
			return this;
		}

		public void EncodeInXmlElement(XmlElement element, object value, XmlCoderContext context)
		{
			DictionaryEntry entry = (DictionaryEntry)value;
			XmlDocumentEncoder encoder = new XmlDocumentEncoder(element, context);
			encoder.Encode("Key", entry.Key);
			encoder.Encode("Value", entry.Value);
		}

		public object DecodeFromXmlElement(XmlElement element, Type typeToConvert, XmlCoderContext context)
		{
			DictionaryEntry entry = (DictionaryEntry)Activator.CreateInstance(typeToConvert);
			XmlDocumentDecoder decoder = new XmlDocumentDecoder(element, context);
			entry.Key = decoder.Decode<object>("Key", null);
			entry.Value = decoder.Decode<object>("Value", null);
			return entry;
		}
	}

	public class XmlElementKeyValuePairConverter : IXmlElementConverterFactory, IXmlElementConverter
	{
		public bool CanConvertType(Type typeToConvert)
		{
			return typeToConvert.IsGenericType && typeToConvert.GetGenericTypeDefinition() == typeof(KeyValuePair<,>);
		}

		public IXmlElementConverter CreateConverter()
		{
			return this;
		}

		public void EncodeInXmlElement(XmlElement element, object value, XmlCoderContext context)
		{
			Type valueType = value.GetType();

			PropertyInfo piKey = valueType.GetProperty("Key");
			PropertyInfo piValue = valueType.GetProperty("Value");

			object keyValue = piKey.GetValue(value, new object[0]);
			object valueValue = piValue.GetValue(value, new object[0]);

			XmlDocumentEncoder encoder = new XmlDocumentEncoder(element, context);
			encoder.Encode("Key", keyValue);
			encoder.Encode("Value", valueValue);
		}

		public object DecodeFromXmlElement(XmlElement element, Type typeToConvert, XmlCoderContext context)
		{
			object entry = Activator.CreateInstance(typeToConvert);

			PropertyInfo piKey = typeToConvert.GetProperty("Key");
			PropertyInfo piValue = typeToConvert.GetProperty("Value");

			XmlDocumentDecoder decoder = new XmlDocumentDecoder(element, context);

			piKey.SetValue(entry, decoder.Decode<object>("Key", null), new object[0]);
			piValue.SetValue(entry, decoder.Decode<object>("Value", null), new object[0]);

			return entry;
		}
	}
}
