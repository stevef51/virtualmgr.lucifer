﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.ComponentModel;
using System.Collections;
using ConceptCave.Core.Coding;

namespace ConceptCave.Core.XmlCodable
{
    public class XmlDocumentEncoder : XmlDocumentCoder, IEncoder
    {
        public XmlDocumentEncoder()
        {
            XmlDocument doc = new XmlDocument();
            doc.AppendChild(doc.CreateXmlDeclaration("1.0", "utf-8", null));
            XmlElement rootElement = doc.CreateElement(DocumentRootElement);
            doc.AppendChild(rootElement);

            IContextContainer context = new ContextContainer();
            XmlCoderTypeCache typeCache = new XmlCoderTypeCache(doc.CreateElement(TypeCacheElement), context);
            rootElement.AppendChild(typeCache.Element);

            XmlCoderObjectCache objectCache = new XmlCoderObjectCache(doc.CreateElement(ObjectCacheElement), typeCache, context);
            rootElement.AppendChild(objectCache.Element);

            _coderContext = new XmlCoderContext(typeCache, objectCache, context);

            _element = doc.CreateElement(RootValueElement);
            rootElement.AppendChild(_element);
        }

        public XmlDocumentEncoder(XmlElement element, XmlCoderContext coderContext)
        {
            _element = element;
            if (coderContext != null)
                _coderContext = coderContext;
            else
            {
                XmlCoderTypeCache typeCache = new XmlCoderTypeCache(element.OwnerDocument.CreateElement(TypeCacheElement), coderContext.Context);
                element.AppendChild(typeCache.Element);

                XmlCoderObjectCache objectCache = new XmlCoderObjectCache(element.OwnerDocument.CreateElement(ObjectCacheElement), typeCache, coderContext.Context);
                element.AppendChild(objectCache.Element);

                _coderContext = new XmlCoderContext(typeCache, objectCache, coderContext.Context);
            }
        }

		public void EncodeObject(string name, object value)
		{
			XmlElement valueElement = TypeCache.CreateValueElement(value, _element);

			if (name != null)
				valueElement.SetAttribute(NameAttribute, name);

			IUniqueNode uniqueNode = value as IUniqueNode;
			if (uniqueNode != null && ObjectCache != null && !Context.Get<bool>("ForFreezing"))
			{
				ObjectCache.AddObject(uniqueNode);
				valueElement.SetAttribute(RefIdAttribute, uniqueNode.Id.ToString());
			}
			else if (value != null)
				XmlElementConverter.EncodeInXmlElement(valueElement, value, _coderContext);
		}

        public void Encode<T>(string name, T value)
        {
			EncodeObject (name, value);
        }

		public override string ToString()
		{
			return _element.OwnerDocument.OuterXml;
		}
    }
}
