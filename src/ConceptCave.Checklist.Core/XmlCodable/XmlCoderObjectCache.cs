﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using ConceptCave.Core.Coding;

namespace ConceptCave.Core.XmlCodable
{
    public class XmlCoderDeferredActionArgument
    {
        public Guid NodeId { get; set; }
        public IUniqueNode Node { get; set; }
        public bool IsHandled { get; set; }

        public XmlCoderDeferredActionArgument(Guid id)
        {
            NodeId = id;
        }

        public XmlCoderDeferredActionArgument(IUniqueNode node)
        {
            Node = node;
            NodeId = Node.Id;
        }
    }

    public delegate void XmlCoderDeferredAction(object sender, XmlCoderDeferredActionArgument e);

    public class XmlCoderObjectCache
    {
        /// <summary>
        /// Called when the object cache can't find a node in its cache. Handlers can use the NodeId to retrieve the relevant object and should set both the Node property (if an object is found)
        /// and the IsHandled property to true.
        /// </summary>
        public event XmlCoderDeferredAction NodeRequired = delegate { };

        /// <summary>
        /// Called when the object cache is about to save a unique node. Handlers of the event can choose to save the details themselves and by setting the IsHandled property to true side step
        /// the object cache holding the reference interanlly.
        /// </summary>
        public event XmlCoderDeferredAction NodeBeforeAdd = delegate { };

        /// <summary>
        /// The object cache
        /// </summary>
        private Dictionary<Guid, IUniqueNode> _cache;

        /// <summary>
        /// The object cache in XmlElement form (only none empty when Decoding from Xml)
        /// </summary>
        private Dictionary<Guid, XmlElement> _elementCache;

        /// <summary>
        /// Root element of the Object Cache
        /// </summary>
        private XmlElement _element;

        /// <summary>
        /// The type cache to store object types
        /// </summary>
        private XmlCoderTypeCache _typeCache;

        private IContextContainer _context;

        /// <summary>
        /// Construct object cache from an existing *ObjectCache* element and given typeCache
        /// </summary>
        /// <param name="objectCacheElement">ObjectCache element to decode from</param>
        /// <param name="typeCache">Pre-populated TypeCache used to construct objectCache with</param>
        public XmlCoderObjectCache(XmlElement objectCacheElement, XmlCoderTypeCache typeCache, IContextContainer context)
        {
            _cache = new Dictionary<Guid, IUniqueNode>();
            _elementCache = new Dictionary<Guid, XmlElement>();
            _context = context;

            XmlCoderContext coderContext = new XmlCoderContext(typeCache, null, context);
            foreach (XmlNode objectNode in objectCacheElement.ChildNodes)
            {
                XmlElement objectElement = objectNode as XmlElement;
                if (objectElement == null)
                    continue;

                Guid id = new Guid(objectElement.GetAttribute(XmlDocumentCoder.IdAttribute));
                _elementCache.Add(id, objectElement);
            }
 
            _typeCache = typeCache;
            _element = objectCacheElement;
        }

        /// <summary>
        /// The root Element of the Object Cache
        /// </summary>
        public XmlElement Element
        {
            get { return _element; }
        }

        /// <summary>
        /// The attached TypeCache
        /// </summary>
        public XmlCoderTypeCache TypeCache
        {
            get { return _typeCache; }
        }

        public IContextContainer Context
        {
            get { return _context; }
        }

        /// <summary>
        /// Does the Cache contain given UniqueNode Id
        /// </summary>
        /// <param name="id">Id of node to test</param>
        /// <returns>True if contained</returns>
        public bool Contains(Guid id)
        {
            return _cache.ContainsKey(id);
        }

        /// <summary>
        /// Get reference to UniqueNode given its Id
        /// </summary>
        /// <param name="id">If of node to retrieve</param>
        /// <returns>Null if not found, otherwise reference to cached object</returns>
        public IUniqueNode GetObject(Guid id)
        {
            IUniqueNode node = null;
            if (!_cache.TryGetValue(id, out node))
            {
                // Not in cache, see if its in the XmlElement cache where we can decode it from
                XmlElement element = null;
                if (_elementCache.TryGetValue(id, out element))
                {
                    Type typeToCreate = _typeCache[element.Name];

                    XmlCoderContext coderContext = new XmlCoderContext(_typeCache, this, _context);
                    coderContext.OnUniqueNodeCreated += delegate(IUniqueNode uniqueNode)
                    {
                        _cache[uniqueNode.Id] = uniqueNode;
                    };
                    node = XmlElementConverter.DecodeFromXmlElement(element, typeToCreate, coderContext) as IUniqueNode;
                }
            }
            else
            {
                XmlCoderDeferredActionArgument args = new XmlCoderDeferredActionArgument(id);
                NodeRequired(this, args);

                if (args.IsHandled)
                {
                    node = args.Node;
                }
            }
            return node;
        }

        /// <summary>
        /// Add a UniqueNode to the cache if it does not already exist.  Optionally encode it into Xml aswell if not found
        /// </summary>
        /// <param name="node">UniqueNode to Add</param>
        /// <param name="encodeIfNew">If True and node is new then it writes the node to the ObjectCache XmlElement</param>
        protected void AddObject(IUniqueNode node, bool encodeIfNew)
        {
            XmlCoderDeferredActionArgument args = new XmlCoderDeferredActionArgument(node);
            NodeBeforeAdd(this, args);

            if (args.IsHandled)
            {
                // who ever is catching the event recons they've got our backs, so nothing to do here
                return;
            }

            Guid id = node.Id;
            if (!_cache.ContainsKey(id))
            {
                lock (_cache)
                {
                    if (!_cache.ContainsKey(id))
                    {
                        _cache.Add(id, node);

                        if (encodeIfNew)
                        {
                            XmlElement objectElement = _typeCache.CreateValueElement(node, _element);
                            objectElement.SetAttribute(XmlDocumentCoder.IdAttribute, id.ToString());

                            XmlElementConverter.EncodeInXmlElement(objectElement, node, new XmlCoderContext(_typeCache, this, _context));
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Add a UniqueNode to the cache if it does not already exist.  Encode it into Xml aswell if not found
        /// </summary>
        /// <param name="node">UniqueNode to Add</param>
        public void AddObject(IUniqueNode node)
        {
            AddObject(node, true);
        }

        /// <summary>
        /// Mark a UniqueNode to be excluded from the Xml
        /// </summary>
        /// <param name="node">UniqueNode to Excluded</param>
        public void ExcludeFromXml(IUniqueNode node)
        {
            // Exclude it by adding it to the cache without writing Xml
            AddObject(node, false);
        }
    
        /// <summary>
        /// Mark a UniqueNode to be Included to the cache
        /// </summary>
        /// <param name="node">UniqueNode to Excluded</param>
        public void IncludeInCache(IUniqueNode node)
        {
            // Include it in the cache without writing the Xml (exact same as ExcludeFromXml)
            AddObject(node, false);
        }
    }
}
