﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.ComponentModel;
using ConceptCave.Checklist;
using ConceptCave.Core.Coding;

namespace ConceptCave.Core.XmlCodable
{
    public class XmlDocumentDecoder : XmlDocumentCoder, IDecoder
    {
        public List<IDecoderPostProcessing> PostProcessingRequired { get; protected set; }

        public XmlDocumentDecoder()
        {
            PostProcessingRequired = new List<IDecoderPostProcessing>();
        }

        public XmlDocumentDecoder(XmlElement xmlCodedElement, IReflectionFactory reflectionFactory) : this(xmlCodedElement)
        {
			if (reflectionFactory != null)
            	_coderContext.Context.Set<IReflectionFactory>(reflectionFactory);
        }

        public XmlDocumentDecoder(XmlElement xmlCodedElement) : this()
        {
            IContextContainer context = new ContextContainer();
            _element = xmlCodedElement.SelectSingleNode(RootValueElement) as XmlElement;
            XmlCoderTypeCache typeCache = new XmlCoderTypeCache(xmlCodedElement.SelectSingleNode(TypeCacheElement) as XmlElement, context);
            XmlCoderObjectCache objectCache = new XmlCoderObjectCache(xmlCodedElement.SelectSingleNode(ObjectCacheElement) as XmlElement, typeCache, context);
            _coderContext = new XmlCoderContext(typeCache, objectCache, context);
        }

        public XmlDocumentDecoder(XmlElement element, XmlCoderContext coderContext) : this()
        {
            _element = element;
            _coderContext = coderContext;
        }

        public static XmlDocumentDecoder CreateForRootElement(XmlDocument doc, IReflectionFactory reflectionFactory = null)
        {
            IContextContainer context = new ContextContainer();
            XmlElement element = doc.DocumentElement.SelectSingleNode(RootValueElement).FirstChild as XmlElement;
            XmlCoderTypeCache typeCache = new XmlCoderTypeCache(doc.DocumentElement.SelectSingleNode(TypeCacheElement) as XmlElement, context);
            XmlCoderObjectCache objectCache = new XmlCoderObjectCache(doc.DocumentElement.SelectSingleNode(ObjectCacheElement) as XmlElement, typeCache, context);

            var coderContext = new XmlCoderContext(typeCache, objectCache, context);
            if (reflectionFactory != null) context.Set<IReflectionFactory>(reflectionFactory);

            return new XmlDocumentDecoder(element, coderContext);
        }

        public T Decode<T>(string name)
        {
            T result;
            if (Decode(name, out result))
                return result;
            else
                return default(T);
        }

        public T Decode<T>(string name, T defaultValue)
        {
            T result;
            if (Decode(name, out result))
                return result;
            else
                return defaultValue;
        }

        protected void ProcessDecodedObject(object value)
        {
            // make a note of the object if its flagged for extra processing
            if (value is IDecoderPostProcessing)
            {
                PostProcessingRequired.Add((IDecoderPostProcessing)value);
            }
        }

        public bool Decode<T>(string name, out T value)
        {
            value = default(T);

            XmlElement valueElement = _element;
            if (name != null)
                valueElement = _element.SelectSingleNode("*[@" + NameAttribute + "='" + name + "']") as XmlElement;

            if (valueElement == null)
                return false;

            Type type = TypeCache[valueElement.Name];
            if (type == typeof(XmlCoderTypeCache.Null))
                return true;

            if (typeof(IUniqueNode).IsAssignableFrom(type) && ObjectCache != null && valueElement.HasAttribute(RefIdAttribute))
            {
                Guid id = new Guid(valueElement.GetAttribute(RefIdAttribute));
                value = (T)ObjectCache.GetObject(id);
                ProcessDecodedObject(value);
                return true;
            }
            if (type != null)
            {
				object o = XmlElementConverter.DecodeFromXmlElement(valueElement, type, _coderContext);
				if (type.IsEnum)
					value = (T)(object)((int)o);
				else
					value = (T)o;
                ProcessDecodedObject(value);
                return true;
            }

            return false;
        }
    }
}
