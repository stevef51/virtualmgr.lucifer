﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConceptCave.Checklist
{
    public static class NumberExtensions
    {
        public static decimal ToRadians(this decimal value)
        {
            return (decimal)Math.PI * value / 180.0m;
        }

        public static double ToRadians(this double value)
        {
            return Math.PI * value / 180.0;
        }
    }
}
