﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Checklist;
using ConceptCave.Core.Coding;

namespace ConceptCave.Core.SBONCodable
{
    public class SBONCoderFactory : ICoderFactory
    {
        public IEncoder CreateEncoder()
        {
            return new SBONEncoder();
        }
    }
}
