//
//  Author:
//    Steve Fillingham steve.fillingham@bworkflow.com
//
//  Copyright (c) 2013, SquareIT Software PTY LTD
//
//
//
using System;
using ConceptCave.Core.Coding;
using ConceptCave.SBON.Reflection;
using System.IO;
using System.Collections.Generic;
using ConceptCave.SBON;
using System.Linq;
using System.Collections;
using ConceptCave.Core;

namespace ConceptCave.Core.SBONCodable
{
    public class SBONEncoder : SBONCoder, IEncoder
    {
        private class ObjectEncoder : IEncoder
        {
            private MemoryStream _memory = new MemoryStream();
            public readonly SBONWriter Writer;
            public readonly SBONEncoder RootEncoder;
            public readonly int TypeRef;
            public readonly int ObjectRef;

            public ObjectEncoder(SBONEncoder rootEncoder, int typeRef, int objectRef)
            {
                TypeRef = typeRef;
                ObjectRef = objectRef;
                Writer = new SBONWriter(_memory);
                RootEncoder = rootEncoder;
            }

            #region IEncoder implementation

            public void Encode<T>(string name, T value)
            {
                RootEncoder.Encode(Writer, name, value);
            }

            public byte[] ToByte()
            {
                return _memory.ToArray();
            }

            #endregion

            #region ICoder implementation

            public ConceptCave.Core.IContextContainer Context
            {
                get
                {
                    return RootEncoder.Context;
                }
                set
                {
                    RootEncoder.Context = value;
                }
            }

            #endregion
        }

        private MemoryStream _rootStream;
        private SBONWriter _rootWriter;

        private Dictionary<Type, TypeTableItem> _typeTable = new Dictionary<Type, TypeTableItem>();
        private NameTable _nameTable = new NameTable();
        private Dictionary<object, ObjectEncoder> _objectEncoders = new Dictionary<object, ObjectEncoder>();

        public SBONEncoder()
        {
            Context = new ContextContainer();
        }

        private string AddNameCode(string name)
        {
            return _nameTable.Add(name);
        }

        private void WriteObject(SBONWriter writer, object value)
        {
            if (!Serializer.TrySerializePrimitive(value, writer))
            {
                // Must be an object of some sort (ICodable, IEnumerable) either way it is given its own Encoder to write to ..
                ObjectEncoder encoder = AddObjectEncoder(value);
                writer.WriteStartAttribute();
                writer.WriteName("#");
                writer.WriteInt32(encoder.ObjectRef);
                writer.WriteEndAttribute();
            }
        }

        private ObjectEncoder AddObjectEncoder(object o)
        {
            ObjectEncoder encoder = null;
            if (!_objectEncoders.TryGetValue(o, out encoder))
            {
                Type t = o.GetType();
                TypeTableItem tti = null;
                if (!_typeTable.TryGetValue(t, out tti))
                {
                    tti = new TypeTableItem() { Id = _typeTable.Count, TypeName = t.AssemblyQualifiedName };
                    _typeTable.Add(t, tti);
                }

                encoder = new ObjectEncoder(this, tti.Id, _objectEncoders.Count);
                _objectEncoders.Add(o, encoder);

                var w = encoder.Writer;
                ICodable codable = o as ICodable;
                if (codable != null)
                {
                    var uniqueNode = codable as IUniqueNode;
                    if (uniqueNode != null)
                    {
                        w.WriteStartAttribute();
                        w.WriteName("id");
                        w.WriteGuid(uniqueNode.Id);
                    }
                    w.WriteStartObject();
                    codable.Encode(encoder);
                    w.WriteEndObject();

                    if (uniqueNode != null)
                        w.WriteEndAttribute();
                }
                else
                {
                    var dictionary = o as IDictionary;
                    if (dictionary != null)
                    {
                        w.WriteStartArray();
                        foreach (var nvp in dictionary)
                            WriteObject(w, nvp);
                        w.WriteEndArray();
                    }
                    else
                    {
                        var enumerable = o as IEnumerable;
                        if (enumerable != null)
                        {
                            w.WriteStartArray();
                            foreach (object e in enumerable)
                                WriteObject(w, e);
                            w.WriteEndArray();
                        }
                        else
                        {
                            if (o is DictionaryEntry)
                            {
                                var dictionaryEntry = (DictionaryEntry)o;
                                w.WriteStartArray();
                                WriteObject(w, dictionaryEntry.Key);
                                WriteObject(w, dictionaryEntry.Value);
                                w.WriteEndArray();
                            }
                        }
                    }
                }
            }

            return encoder;
        }

        public void WriteTo(Stream stream)
        {
            if (_rootStream == null)
            {
                _rootStream = new MemoryStream();
                _rootWriter = new SBONWriter(_rootStream);
                _rootWriter.WriteStartObject();
            }
            _rootWriter.WriteEndObject();

            SerializableContent content = new SerializableContent();
            content.TypeTable = new List<TypeTableItem>(_typeTable.Values);
            content.ObjectTable = new List<ObjectTableItem>();
            foreach (var nvp in _objectEncoders)
                content.ObjectTable.Add(new ObjectTableItem()
                {
                    TypeRef = nvp.Value.TypeRef,
                    ObjectRef = nvp.Value.ObjectRef,
                    StreamData = nvp.Value.ToByte()
                });
            content.NameTable = _nameTable;
            content.RootObject = _rootStream.ToArray();

            Serializer s = new Serializer(stream);
            s.Serialize(content);
        }

        public byte[] ToArray()
        {
            var ms = new MemoryStream();
            WriteTo(ms);
            return ms.ToArray();
        }

        public override string ToString()
        {
            return Convert.ToBase64String(ToArray());
        }

        #region IEncoder implementation

        private void Encode(SBONWriter writer, string name, object value)
        {
            string nameCode = AddNameCode(name);

            writer.WriteName(nameCode);

            WriteObject(writer, value);
        }

        public void Encode<T>(string name, T value)
        {
            if (_rootStream == null)
            {
                _rootStream = new MemoryStream();
                _rootWriter = new SBONWriter(_rootStream);
                _rootWriter.WriteStartObject();
            }

            Encode(_rootWriter, name, value);
        }

        #endregion

        #region ICoder implementation

        public ConceptCave.Core.IContextContainer Context { get; set; }

        #endregion
    }

}

