//
//  Author:
//    Steve Fillingham steve.fillingham@bworkflow.com
//
//  Copyright (c) 2014, SquareIT Software PTY LTD
//
//
//
using System;
using System.Collections.Generic;
using ConceptCave.SBON;

namespace ConceptCave.Core.SBONCodable
{
    public class SBONCoder
    {
        protected class TypeTableItem : ISBONWriteable
        {
            [SBON("I")]
            public int Id { get; set; }
            [SBON("T")]
            public string TypeName { get; set; }

            #region ISBONWriteable implementation

            public void WriteTo(SBONWriter writer, Action<object> fnWriteObject)
            {
                writer.WriteStartObject();
                writer.WriteName("I");
                writer.WriteInt32(Id);

                writer.WriteName("T");
                writer.WriteString(TypeName);
                writer.WriteEndObject();
            }

            #endregion
        }

        protected class ObjectTableItem : ISBONWriteable
        {
            [SBON("T")]
            public int TypeRef { get; set; }                // Type of the object (via the TypeTable)
            [SBON("O")]
            public int ObjectRef { get; set; }              // Unique Id (within overall Stream) of the Object
            [SBON("D")]
            public byte[] StreamData { get; set; }          // The objects direct streamed data

            public void WriteTo(SBONWriter writer, Action<object> fnWriteObject)
            {
                writer.WriteStartObject();
                writer.WriteName("T");
                writer.WriteInt32(TypeRef);

                writer.WriteName("O");
                writer.WriteInt32(ObjectRef);

                writer.WriteName("D");
                writer.WriteByteArray(StreamData);
                writer.WriteEndObject();
            }
        }

        protected class SerializableContent : ISBONWriteable
        {
            [SBON("V")]
            public int Version { get; set; }
            [SBON("T")]
            public List<TypeTableItem> TypeTable { get; set; }
            [SBON("N")]
            public NameTable NameTable { get; set; }
            [SBON("O")]
            public List<ObjectTableItem> ObjectTable { get; set; }
            [SBON("R")]
            public byte[] RootObject { get; set; }

            #region ISBONWriteable implementation

            public void WriteTo(SBONWriter writer, Action<object> fnWriteObject)
            {
                writer.WriteStartObject();
                writer.WriteName("V");
                writer.WriteInt32(Version);

                writer.WriteName("T");
                writer.WriteStartArray();
                foreach (var tti in TypeTable)
                    tti.WriteTo(writer, fnWriteObject);
                writer.WriteEndArray();

                writer.WriteName("N");
                fnWriteObject(NameTable);

                writer.WriteName("O");
                writer.WriteStartArray();
                foreach (var oti in ObjectTable)
                    oti.WriteTo(writer, fnWriteObject);
                writer.WriteEndArray();

                writer.WriteName("R");
                writer.WriteByteArray(RootObject);
                writer.WriteEndObject();
            }

            #endregion
        }

        public SBONCoder()
        {
        }
    }
}

