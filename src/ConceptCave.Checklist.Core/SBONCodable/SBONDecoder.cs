//
//  Author:
//    Steve Fillingham steve.fillingham@bworkflow.com
//
//  Copyright (c) 2014, SquareIT Software PTY LTD
//
//
//
using System;
using ConceptCave.Core.Coding;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using ConceptCave.SBON;
using System.Collections;
using ConceptCave.Core.XmlCodable;
using System.Xml;
using System.Reflection;
using ConceptCave.Checklist;
using ConceptCave.Core;

namespace ConceptCave.Core.SBONCodable
{
	public class SBONDecoder : SBONCoder, IDecoder
	{
		public static Func<Type, object> FnGlobalInstantiate = t => Activator.CreateInstance (t);
		public Func<Type, object> FnInstantiate = FnGlobalInstantiate;
		  
		private class TypeDecoder
		{
			public Func<Type, bool> FnCanHandle { get; set; }
			public Func<Type, ObjectDecoder, Func<Type, object>, Action<object>, object> FnHandle { get; set; }
		}

		private static List<TypeDecoder> _searchDecoders = new List<TypeDecoder>();
		private static Dictionary<Type, TypeDecoder> _typeDecoders = new Dictionary<Type, TypeDecoder> ();

		static SBONDecoder()
		{
			// ICodable
			_searchDecoders.Add (new TypeDecoder () { 
				FnCanHandle = type => typeof(ICodable).IsAssignableFrom (type),
				FnHandle = (type, decoder, fnInstantiate, fnYield) =>
				{
					var codeable = fnInstantiate (type) as ICodable;
					var uniqueNode = codeable as IUniqueNode;
					if (uniqueNode != null && decoder.AttributedValue != null)
					{
						Guid id = Guid.Empty;
						if (decoder.AttributedValue.GetAttributeValue("id", ref id))
							uniqueNode.UseDecodedId(id);
					}

					fnYield(codeable);
					codeable.Decode (decoder);
					return codeable;
				}
			});

			// Array
			_searchDecoders.Add (new TypeDecoder () { 
				FnCanHandle = type => type.IsArray,
				FnHandle = (type, decoder, fnInstantiate, fnYield) =>
				{
					var array = (Array)Array.CreateInstance(type.GetElementType(), decoder.ArrayLength);
					fnYield(array);
					decoder.IterateArray((o,i) => array.SetValue(o, i));
					return array;
				}
			});

			// DictionaryEntry
			_searchDecoders.Add (new TypeDecoder () {
				FnCanHandle = type => typeof(DictionaryEntry).IsAssignableFrom (type),
				FnHandle = (type, decoder, fnInstantiate, fnYield) =>
				{
					var entry = (DictionaryEntry)Activator.CreateInstance(type);
					fnYield(entry);
					decoder.IterateArray((o, i) => 
						{ 
							if (i == 0)
								entry.Key = o;
							else
								entry.Value = o;
						});
					return entry;
				}
			});

			// Dictionary
			_searchDecoders.Add (new TypeDecoder () {
				FnCanHandle = type => typeof(IDictionary).IsAssignableFrom (type),
				FnHandle = (type, decoder, fnInstantiate, fnYield) =>
				{
					var dictionary = (IDictionary)Activator.CreateInstance(type);
					fnYield(dictionary);
					decoder.IterateArray((o, i) => dictionary.Add(((DictionaryEntry)o).Key, ((DictionaryEntry)o).Value));
					return dictionary;
				}
			});


			// Stack<T> - Special sort of list as items are added in reverse order
			_searchDecoders.Add (new TypeDecoder () {
				FnCanHandle = type => typeof(Stack).IsAssignableFrom(type) || (type.IsGenericType && typeof(Stack<>).MakeGenericType(type.GetGenericArguments()[0]).IsAssignableFrom (type)),
				FnHandle = (type, decoder, fnInstantiate, fnYield) =>
				{
					MethodInfo fnPush = type.GetMethod("Push");
					var stack = Activator.CreateInstance (type);
					fnYield (stack);
					decoder.IterateArrayReverse ((o, i) => fnPush.Invoke(stack, new object[] { o }));		
					return stack;
				}
			});


			// IList<T> - Note IList<T> is chosen before IEnumerable<T> since we can use IList<T> to add the objects to the actual instance (more efficient)
			_searchDecoders.Add (new TypeDecoder () {
				FnCanHandle = type => type.IsGenericType && typeof(IList<>).MakeGenericType(type.GetGenericArguments()[0]).IsAssignableFrom (type),
				FnHandle = (type, decoder, fnInstantiate, fnYield) =>
				{
					var list = (IList)Activator.CreateInstance (type);
					fnYield (list);
					decoder.IterateArray ((o, i) => list.Add (o));
					return list;
				}
			});


			// IEnumerable<T> - If it gets here we know its not IList<T> since that is handled above, IEnumerable<T> is immutable which means elements are passed into the Constructor
			_searchDecoders.Add(new TypeDecoder() {
				FnCanHandle = type => type.IsGenericType && typeof(IEnumerable<>).MakeGenericType(type.GetGenericArguments()[0]).IsAssignableFrom(type),
				FnHandle = (type, decoder, fnInstantiate, fnYield) =>
				{
					// To create the object we use the Constructor(IEnumerable<ElementType>) method, so we must have a IEnumerable<ElementType> to pass in, List<ElementType> will do just fine
					var listType = typeof(List<>).MakeGenericType(type.GetGenericArguments()[0]);
					var list = (IList)Activator.CreateInstance(listType);

					decoder.IterateArray((o, i) => list.Add(o));

					var collection = Activator.CreateInstance(type, list);
					fnYield(collection);
					return collection;
				}
			});

			// IList
			_searchDecoders.Add (new TypeDecoder () {
				FnCanHandle = type => typeof(IList).IsAssignableFrom (type),
				FnHandle = (type, decoder, fnInstantiate, fnYield) =>
				{
					var list = (IList)Activator.CreateInstance (type);
					fnYield (list);
					decoder.IterateArray ((o, i) => list.Add (o));
					return list;
				}
			});


			// IEnumerable - If it gets here we know its not IList since that is handled above, IEnumerable is immutable which means elements are passed into the Constructor
			_searchDecoders.Add(new TypeDecoder() {
				FnCanHandle = type => typeof(IEnumerable).IsAssignableFrom(type),
				FnHandle = (type, decoder, fnInstantiate, fnYield) =>
				{
					// To create the object we use the Constructor(IEnumerable) method, so we must have a IEnumerable to pass in, ArrayList will do just fine
					var list = new ArrayList();

					decoder.IterateArray((o, i) => list.Add(o));

					var collection = Activator.CreateInstance(type, list);
					fnYield(collection);
					return collection;
				}
			});
		}

		private class ObjectDecoder : IDecoder
		{
			public SBONDecoder RootDecoder { get; set; }
			private Dictionary<string, object> _dictionary;
			private List<object> _array;
			public AttributedValue AttributedValue { get; private set; }

			public ObjectDecoder(SBONDecoder rootDecoder, byte[] sbonData)
			{
				RootDecoder = rootDecoder;
				var ms = new MemoryStream(sbonData);
				var deserializer = new ConceptCave.SBON.Collection.Deserializer(ms);
				object value = deserializer.DeserializeObject();
			
				AttributedValue = value as AttributedValue;
				if (AttributedValue != null)
					value = AttributedValue.Value;

				_dictionary = value as Dictionary<string, object>;
				_array = value as List<object>;
			}

			#region IDecoder implementation

			public int ArrayLength
			{
				get 
				{
					if (_array == null)
						return 0;
					return _array.Count;
				}
			}

			public void IterateArray(Action<object, int> fn)
			{
				if (_array == null)
					return;

				int i = 0;
				foreach (object o in _array)
				{
					object value = o;
					AttributedValue attributedValue = o as AttributedValue;
					if (attributedValue != null)
					{
						int id = 0;
						if (attributedValue.GetAttributeValue ("#", ref id))
							value = RootDecoder.ObjectWithId (id);
						else
							value = attributedValue.Value;
					}
					fn (value, i++);
				}
			}

			public void IterateArrayReverse(Action<object, int> fn)
			{
				if (_array == null)
					return;

				int i = 0;
				foreach (object o in _array.Reverse<object>())
				{
					object value = o;
					AttributedValue attributedValue = o as AttributedValue;
					if (attributedValue != null)
					{
						int id = 0;
						if (attributedValue.GetAttributeValue ("#", ref id))
							value = RootDecoder.ObjectWithId (id);
						else
							value = attributedValue.Value;
					}
					fn (value, i++);
				}
			}

			public T Decode<T>(string name)
			{
				return Decode<T>(name, default(T));
			}

			public T Decode<T>(string name, T defaultValue)
			{
				T v = defaultValue;
				if (Decode<T> (name, out v))
					return v;
				else
					return defaultValue;
			}

			public bool Decode<T>(string name, out T value)
			{
				object o;
				string nameCode = RootDecoder.KeyForName (name);
				if (nameCode != null)
				{
					if (_dictionary.TryGetValue (nameCode, out o))
					{
						AttributedValue attributedValue = o as AttributedValue;
						if (attributedValue != null)
						{
							int id = 0;
							if (attributedValue.GetAttributeValue ("#", ref id))
								value = (T)RootDecoder.ObjectWithId (id);
							else
								value = (T)attributedValue.Value;
						}
						else if (typeof(T).IsEnum)
							value = (T)(object)(Convert.ToInt32 (o));
						else
							value = (T)o;
						return true;
					}
				}

				// Fail case
				value = default(T);
				return false;
			}

			public System.Collections.Generic.List<IDecoderPostProcessing> PostProcessingRequired
			{
				get
				{
					throw new NotImplementedException ();
				}
			}

			#endregion

			#region ICoder implementation

			public ConceptCave.Core.IContextContainer Context
			{
				get
				{
					return RootDecoder.Context;
				}
			}

			#endregion


		}

		private SerializableContent _content;
		private ObjectDecoder _rootObject;
		private Dictionary<int, object> _objectCache = new Dictionary<int, object> ();

		public SBONDecoder (Stream stream, IReflectionFactory reflectionFactory)
		{
			if (reflectionFactory != null)
				FnInstantiate = t => reflectionFactory.InstantiateObject (t);

			var deserializer = new ConceptCave.SBON.Reflection.Deserializer (stream);
			_content = deserializer.Deserialize<SerializableContent> ();
			_rootObject = new ObjectDecoder (this, _content.RootObject);
		}

		public SBONDecoder (Stream stream) : this(stream, null)
		{
		}

		public object ObjectWithId(int id)
		{
			object o = null;
			if (!_objectCache.TryGetValue (id, out o))
			{
				var objectItem = (from findObjectItem in _content.ObjectTable
					where findObjectItem.ObjectRef == id
					select findObjectItem).FirstOrDefault ();

				if (objectItem == null)
					return null;

				TypeTableItem typeItem = (from findTypeItem in _content.TypeTable
				                 where findTypeItem.Id == objectItem.TypeRef
				                 select findTypeItem).FirstOrDefault ();

				if (typeItem == null)
					return null;

				Type type = Type.GetType (typeItem.TypeName);
				if (type == null)
					return null;

				ObjectDecoder objectDecoder = new ObjectDecoder (this, objectItem.StreamData);

				// See if we can find the TypeDecoder quickly (if we have decoded this type before it will be in the quick-find-dictionary)
				TypeDecoder typeDecoder = null;
				if (!_typeDecoders.TryGetValue (type, out typeDecoder))
				{
					// New type, search for a suitable TypeDecoder
					foreach (var findDecoder in _searchDecoders)
					{
						if (findDecoder.FnCanHandle(type))
						{
							// Remember this decoder for this type for next time
							typeDecoder = findDecoder;
							_typeDecoders [type] = findDecoder;	
							break;
						}
					}
				}

				if (typeDecoder == null)
					return null;

				Func<Type, object> fnInstantiate = FnInstantiate;
				if (Context != null)
				{
					IReflectionFactory reflectionFactory = Context.Get<IReflectionFactory> ();
					if (reflectionFactory != null)
						fnInstantiate = t => reflectionFactory.InstantiateObject (t);
				}

				o = typeDecoder.FnHandle(type, objectDecoder, fnInstantiate, newObj => _objectCache.Add(id, newObj) );
			}
			return o;
		}

		public string KeyForName(string name)
		{
			return _content.NameTable.KeyForName (name);
		}

		#region IDecoder implementation

		public T Decode<T>(string name)
		{
			return _rootObject.Decode<T> (name);
		}

		public T Decode<T>(string name, T defaultValue)
		{
			return _rootObject.Decode<T> (name, defaultValue);
		}

		public bool Decode<T>(string name, out T value)
		{
			return _rootObject.Decode<T> (name, out value);
		}

		public System.Collections.Generic.List<IDecoderPostProcessing> PostProcessingRequired
		{
			get
			{
				throw new NotImplementedException ();
			}
		}

		#endregion

		#region ICoder implementation

		public ConceptCave.Core.IContextContainer Context
		{
			get;
			set; 
		}

		#endregion
	}
}

