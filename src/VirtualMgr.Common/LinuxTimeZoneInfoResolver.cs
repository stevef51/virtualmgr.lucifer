using System;
using TimeZoneConverter;

namespace VirtualMgr.Common
{
    public class LinuxTimeZoneInfoResolver : ITimeZoneInfoResolver
    {
        public TimeZoneInfo ResolveWindows(string windowsTimezone)
        {
            return TimeZoneInfo.FindSystemTimeZoneById(TZConvert.WindowsToIana(windowsTimezone));
        }

        public TimeZoneInfo ResolveIana(string ianaTimezone)
        {
            return TimeZoneInfo.FindSystemTimeZoneById(ianaTimezone);
        }
    }
}
