using System;

namespace VirtualMgr.Common
{
    public static class TimeZoneInfoResolver
    {
        private static ITimeZoneInfoResolver _resolver;
        public static ITimeZoneInfoResolver Use(ITimeZoneInfoResolver resolver)
        {
            _resolver = resolver;
            return resolver;
        }

        public static TimeZoneInfo ResolveIana(string ianaTimezone)
        {
            return _resolver.ResolveIana(ianaTimezone);
        }
        public static TimeZoneInfo ResolveWindows(string windowsTimezone)
        {
            return _resolver.ResolveWindows(windowsTimezone);
        }
    }
}