using System;
using TimeZoneConverter;

namespace VirtualMgr.Common
{
    public class WindowsTimeZoneInfoResolver : ITimeZoneInfoResolver
    {
        public TimeZoneInfo ResolveWindows(string windowsTimezone)
        {
            return TimeZoneInfo.FindSystemTimeZoneById(windowsTimezone);
        }

        public TimeZoneInfo ResolveIana(string ianaTimezone)
        {
            return TimeZoneInfo.FindSystemTimeZoneById(TZConvert.IanaToWindows(ianaTimezone));
        }
    }
}
