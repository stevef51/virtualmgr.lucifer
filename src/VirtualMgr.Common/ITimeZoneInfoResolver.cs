﻿using System;

namespace VirtualMgr.Common
{
    public interface ITimeZoneInfoResolver
    {
        TimeZoneInfo ResolveWindows(string windowsTimezone);
        TimeZoneInfo ResolveIana(string ianaTimezone);
    }
}
