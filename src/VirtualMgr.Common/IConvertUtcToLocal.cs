﻿using System;

namespace VirtualMgr.Common
{
    public interface IConvertUtcToLocal<T>
    {
        T ConvertUtcToLocal(T entity, Func<DateTime, DateTime> utcToLocal);
    }
}
