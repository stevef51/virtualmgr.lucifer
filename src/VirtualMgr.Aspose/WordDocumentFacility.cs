﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.BusinessLogic.Facilities;
using ConceptCave.Checklist.Facilities;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;
using System.IO;
using Aspose.Words.Reporting;
using ConceptCave.Checklist.Core;
using ConceptCave.Repository.Facilities.WordHelpers;

namespace ConceptCave.Repository.Facilities
{
    public class WordDocumentFacility : Facility
    {
        public class WordDocumentFacilityTemplate : Node, IHasName, IHasIndex
        {
            private readonly IMediaManager _mediaMgr;
            public string Name { get; set; }
            public Guid MediaId { get; set; }
            public Func<int> Index { get; set; }

            public WordDocumentFacilityTemplate(IMediaManager mediaMgr)
            {
                _mediaMgr = mediaMgr;
            }

            public AsposeWordDocument Create(IContextContainer context)
            {
                Stream templateStream = _mediaMgr.GetStream(MediaId);

                return new AsposeWordDocument(new Aspose.Words.Document(templateStream), _mediaMgr);
            }

            public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
            {
                base.Encode(encoder);
                
                encoder.Encode("Name", Name);
                encoder.Encode("MediaId", MediaId);
            }

            public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
            {
                base.Decode(decoder);

                Name = decoder.Decode<string>("Name");
                MediaId = decoder.Decode<Guid>("MediaId");
            }
        }

        private readonly IMediaManager _mediaMgr;

        public WordDocumentFacility(IMediaManager mediaMgr)
        {
            Name = "WordDocument";
            _mediaMgr = mediaMgr;
        }

        private INamedList<WordDocumentFacilityTemplate> _templates = new NamedList<WordDocumentFacilityTemplate>();
        

        public WordDocumentFacilityTemplate TemplateByName(IContextContainer context, string name)
        {
            WordDocumentFacilityTemplate template = _templates.Lookup(name);

            return template;
        }

        public WordDocumentFacilityTemplate TemplateByName(IContextContainer context, int index)
        {
            WordDocumentFacilityTemplate template = _templates.Lookup(index);

            return template;
        }

        public WordDocumentFacilityTemplate TemplateWithMedia(IContextContainer context, Guid mediaId)
        {
            return new WordDocumentFacilityTemplate(_mediaMgr) { MediaId = mediaId };
        }

        public AsposeWordDocument Create(IContextContainer context)
        {
            return new AsposeWordDocument(new Aspose.Words.Document(), _mediaMgr);
        }

        public HtmlToWordContainer CreateHtmlContainer(IContextContainer context)
        {
            return new HtmlToWordContainer();
        }

        public HtmlToWordContainer CreateHtmlContainer(IContextContainer context, object media)
        {
            string header = "<head></head>";
            Guid mediaId = Guid.Empty;

            if(media is string)
            {
                mediaId = Guid.Parse((string)media);
            }
            else if(media is Guid)
            {
                mediaId = (Guid)media;
            }

            var m = _mediaMgr.GetById(mediaId);

            if(m != null)
            {
                header = System.Text.Encoding.UTF8.GetString(m.GetData(context));

                IWorkingDocument doc = context.Get<IWorkingDocument>();
                header = doc.Program.ExpandString(header);

                IPlayerStringRenderer renderer = context.Get<IPlayerStringRenderer>();
                header = renderer.ReplaceUrls(header);
                header = renderer.ReplaceIcons(header);
            }

            return new HtmlToWordContainer() { Head = header };
        }

        public string ReplaceUrlsForHtml(IContextContainer context, string html)
        {
            IPlayerStringRenderer renderer = context.Get<IPlayerStringRenderer>();
            return renderer.ReplaceUrls(html);
        }

        public INamedList<WordDocumentFacilityTemplate> Templates
        {
            get
            {
                return _templates;
            }
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            _templates = decoder.Decode<INamedList<WordDocumentFacilityTemplate>>("Templates", new NamedList<WordDocumentFacilityTemplate>());
        }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("Templates", _templates);
        }
    }
}