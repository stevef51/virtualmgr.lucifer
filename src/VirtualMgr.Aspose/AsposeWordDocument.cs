﻿using Aspose.Words;
using Aspose.Words.MailMerging;
using ConceptCave.BusinessLogic.Facilities;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.Lingo.ObjectProviders;
using ConceptCave.Core;
using System;
using System.IO;

namespace ConceptCave.Repository.Facilities.WordHelpers
{
    public class AsposeWordDocument : ConceptCave.Core.Node, IFacilityAction, IMailMergeDataSource, IReplacingCallback
    {
        private byte[] _docData;

        private Aspose.Words.Document _doc;

        private IContextContainer _context;
        private int _count = 1;
        private readonly IMediaManager _mediaMgr;

        private HtmlToWordContainer _htmlToWord;

        public AsposeWordDocument(Aspose.Words.Document doc, IMediaManager mediaMgr)
            : this(mediaMgr)
        {
            Doc = doc;
        }

        public AsposeWordDocument(IMediaManager mediaMgr)
        {
            _mediaMgr = mediaMgr;
        }

        public Aspose.Words.Document Doc
        {
            get { return _doc; }
            private set { _doc = value; }
        }

        private void PrepareDocument(IContextContainer context)
        {
            if (_doc != null)
                return;

            using (MemoryStream ms = new MemoryStream(_docData))
                _doc = new Aspose.Words.Document(ms);
        }

        /// <summary>
        /// Merges the word document with the checklist using a mail merge
        /// </summary>
        /// <param name="context"></param>
        public void Merge(IContextContainer context)
        {
            Merge(context, true, null);
        }

        /// <summary>
        /// Merges teh word document with the checklsit using either a mail merge or a search and replace
        /// of the normal text in the document
        /// </summary>
        /// <param name="context"></param>
        /// <param name="useMailMerge">true to use a mail merge, false a search and replace</param>
        public void Merge(IContextContainer context, bool useMailMerge, HtmlToWordContainer htmlToWord)
        {
            _htmlToWord = htmlToWord;
            
            PrepareDocument(context);

            _context = context;

/* MailMerge is deprecated in favor of simple "Search n Replace of #{tag} 's"
            if(useMailMerge == true)
            {
                _context = context;
                Doc.MailMerge.Execute(this);
            }
            else */
            {
                // ok we are going to search the document for the Lingo string expansion stuff
                Doc.Range.Replace(new System.Text.RegularExpressions.Regex(@"#{(.*?)}|#{(.*?) \|(.*?)}"), this, false);
            }

            _context = null;
            _htmlToWord = null;
        }

        public ReplaceAction Replacing(ReplacingArgs args)
        {
            var builder = new DocumentBuilder((Aspose.Words.Document)args.MatchNode.Document);

            builder.MoveTo(args.MatchNode);

            var expression = args.Match.ToString();

            ILingoProgram program = _context.Get<ILingoProgram>();
            string replaceWith = string.Empty;
            try
            {
                var getter = new IdentifierObjectProvider();
                object o = null;

                var expressionLessTag = expression.Substring(2, expression.Length - 3);     // Remove leading #{ and trailing }
                ContextContainer childContext = new ContextContainer(program);
                childContext.Set("GetAnswerObject", true);
                if (getter.GetObject(childContext, expressionLessTag, out o))
                {
                    if (o is ISignatureAnswer)
                    {
                        var signature = o as ISignatureAnswer;
                        var json = signature.AnswerAsJsonString;
                        var sigToImg = new SignatureToImage();
                        var bmp = sigToImg.SigJsonToImage(json);
                        builder.InsertImage(bmp);
                        args.Replacement = "";
                        return ReplaceAction.Replace;
                    }
                    else if (o is ISignatureQuestion)
                    {
                        // Signature without an "Answer" - ie has not signed it yet
                        args.Replacement = "";
                        return ReplaceAction.Replace;
                    }
                    else if (o is IMediaItem)
                    {
                        var mediaItem = o as IMediaItem;
                        int width = -1, height = -1;
                        var sizeableMedia = mediaItem as ISizeableMediaItem;
                        if (sizeableMedia != null)
                        {
                            width = sizeableMedia.Width.HasValue ? sizeableMedia.Width.Value : -1;
                            height = sizeableMedia.Height.HasValue ? sizeableMedia.Height.Value : -1;
                        }
                        builder.InsertImage(mediaItem.GetData(program), width, height);
                        args.Replacement = "";
                        return ReplaceAction.Replace;
                    }
                }
                replaceWith = program.ExpandString(expression);
            }
            catch
            {
                replaceWith = string.Empty;
            }

/*            if (_htmlToWord != null)
            {
                replaceWith = "<html>" + _htmlToWord.Head + "<body>" + replaceWith + "</body></html>";
                builder.InsertHtml(replaceWith, false);

                args.Replacement = "";
            }
            else
 */
            args.Replacement = replaceWith;

            return ReplaceAction.Replace;
        }

        public void Add(IContextContainer context, HtmlToWordContainer html)
        {
            PrepareDocument(context);

            string content = html.ToHtml();

            DocumentBuilder builder = new DocumentBuilder(Doc);

            builder.InsertHtml(content, false);
        }

        public Guid Save(IContextContainer context, string filename)
        {
            PrepareDocument(context);
            IWorkingDocument referenced = context.Get<IWorkingDocument>();

            if(filename.IndexOf('.') == -1)
            {
                filename = filename + ".docx";
            }

            Aspose.Words.SaveFormat format = SaveFormat.Docx;
            var ext = filename.Substring(filename.LastIndexOf('.') + 1);

            switch(ext.ToLower())
            {
                case "pdf":
                    format = SaveFormat.Pdf;
                    break;
                case "html":
                    format = SaveFormat.Html;
                    break;
                case "jpeg":
                    format = SaveFormat.Jpeg;
                    break;
                case "png":
                    format = SaveFormat.Png;
                    break;
            }

            using (MemoryStream ms = new MemoryStream())
            {
                Doc.Save(ms, format);

                ms.Position = 0;
                return _mediaMgr.SaveStream(ms, filename, null, null, referenced);
            }
        }

        public MediaFacility.EmbeddedMediaItem GetAsMedia(IContextContainer context, string filename)
        {
            var result = new MediaFacility.EmbeddedMediaItem() { Name = string.Format("{0}.docx", filename), MediaType = "application/octet-stream" };

            byte[] data = null;

            using (MemoryStream ms = new MemoryStream())
            {
                Doc.Save(ms, Aspose.Words.SaveFormat.Docx);

                data = ms.ToArray();
            }

            result.SetData(data);

            return result;
        }

        public IMailMergeDataSource GetChildDataSource(string tableName)
        {
            return null;
        }

        public bool GetValue(string fieldName, out object fieldValue)
        {
            ILingoProgram program = _context.Get<ILingoProgram>();
            try
            {
                fieldValue = program.ExpandString(fieldName);

                return true;
            }
            catch
            {
                fieldValue = null;
                return false;
            }
        }

        public bool MoveNext()
        {
            return _count-- > 0;
        }

        public string TableName
        {
            get { return "BWorkflow"; }
        }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            if (_doc != null)
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    Doc.Save(ms, Aspose.Words.SaveFormat.Docx);

                    encoder.Encode("DocData", ms.ToArray());
                }
            }
            else
                encoder.Encode("DocData", _docData);
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            _docData = decoder.Decode<byte[]>("DocData");
        }
    }
}
