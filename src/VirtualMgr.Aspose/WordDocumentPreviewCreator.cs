﻿using Aspose.Words.Saving;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.HelperClasses;
using ConceptCave.RepositoryInterfaces;
using ConceptCave.RepositoryInterfaces.Enums;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualMgr.Central;

namespace VirtualMgr.Aspose
{
    public class WordDocumentPreviewCreator : IMediaPreviewCreator
    {
        public int? CreateForMedia(Guid mediaId, Stream mediaStream)
        {
            ImageSaveOptions options = new ImageSaveOptions(global::Aspose.Words.SaveFormat.Png);
            options.UseHighQualityRendering = true;
            options.UseAntiAliasing = true;
            options.Resolution = 200;
            options.PageCount = 1;

            EntityCollection<MediaPreviewEntity> previews = new EntityCollection<MediaPreviewEntity>();

            var doc = new global::Aspose.Words.Document(mediaStream);

            for (int i = 0; i < doc.PageCount; i++)
            {
                options.PageIndex = i;
                using (MemoryStream docStream = new MemoryStream())
                {
                    doc.Save(docStream, options);

                    docStream.Seek(0, SeekOrigin.Begin);

                    var preview = new MediaPreviewEntity()
                    {
                        MediaId = mediaId,
                        Page = i,
                        Data = docStream.ToArray()
                    };

                    previews.Add(preview);
                }
            }

            using (var adapter = MultiTenantManager.NewTenantDataAccessAdapter())
            {
                adapter.SaveEntityCollection(previews);
            }

            return doc.PageCount;
        }
    }
}
