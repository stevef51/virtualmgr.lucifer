﻿using Microsoft.EntityFrameworkCore;
using VirtualMgr.MultiTenant;

namespace VirtualMgr.ODatav4.Database
{
    public class TenantDataModel : DataModel
    {
        public readonly AppTenant AppTenant;

        public TenantDataModel(AppTenant appTenant) : base()
        {
            AppTenant = appTenant;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(AppTenant.ConnectionString);
        }
    }
}
