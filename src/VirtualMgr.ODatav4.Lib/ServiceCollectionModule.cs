using VirtualMgr.ODatav4.Database;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace VirtualMgr.EFData
{
    public static class ServiceCollectionExtensions
    {
        public static void AddVirtualMgrEFData(this IServiceCollection services, IConfiguration config)
        {
            services.AddTransient<SecurityAndTimezoneAwareDataModel>();
        }
    }
}