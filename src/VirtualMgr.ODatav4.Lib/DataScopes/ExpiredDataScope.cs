﻿using System.Collections.Generic;

namespace VirtualMgr.EFData.DataScopes
{

    public class ExpiredDataScope : DataScope
    {
        public bool ExcludeExpired { get; set; }
        public ExpiredDataScope()
        {
        }

        public override DataScope PopulateMetaData(Dictionary<string, DataScopeField> metaData)
        {
            metaData.Add("ExcludeExpired", new DataScopeBoolField("Exclude Expired") { group = "Filter", showHelp = true, description = "Exclude expired records based on todays date" });
            return this;
        }
        public static ExpiredDataScope FromDataScopeString(string datascope)
        {
            ExpiredDataScope scope = new ExpiredDataScope();
            if (string.IsNullOrEmpty(datascope) == false)
            {
                try
                {
                    scope = Newtonsoft.Json.JsonConvert.DeserializeObject<ExpiredDataScope>(datascope);
                }
                catch
                {
                }
            }
            return scope;
        }
    }

}