﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VirtualMgr.EFData.DataScopes
{
    public class AssetLocationHistoryDataScope : DataScope
    {
        public double Lat { get; set; }
        public double Lng { get; set; }

        public int Count { get; set; }

        public override DataScope PopulateMetaData(Dictionary<string, DataScopeField> metaData)
        {
            metaData.Add("Lat", new DataScopeNumberField("Lat") { group = "Coordinates", showHelp = true, description = "Lattitude to search for closest asset locations" });
            metaData.Add("Lng", new DataScopeNumberField("Lng") { group = "Coordinates", showHelp = true, description = "Longitude to search for closest asset locations" });
            metaData.Add("Count", new DataScopeNumberField("Count") { group = "Coordinates", showHelp = true, description = "Number of closest asset locations to return" });

            return this;
        }
        public static AssetLocationHistoryDataScope FromDataScopeString(string datascope)
        {
            AssetLocationHistoryDataScope scope = new AssetLocationHistoryDataScope();
            if (string.IsNullOrEmpty(datascope) == false)
            {
                try
                {
                    scope = Newtonsoft.Json.JsonConvert.DeserializeObject<AssetLocationHistoryDataScope>(datascope);
                }
                catch
                {
                }
            }

            return scope;
        }
    }
}