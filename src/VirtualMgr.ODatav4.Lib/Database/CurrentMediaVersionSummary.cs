
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace VirtualMgr.ODatav4.Database
{

    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using Microsoft.EntityFrameworkCore;

    public static class CurrentMediaVersionSummaryExtensions
    {
        public static ModelBuilder BuildCurrentMediaVersionSummary(this ModelBuilder builder)
        {
            return builder.Entity<CurrentMediaVersionSummary>(entity =>
            {
                entity.HasKey(e => e.UserId);
                entity.HasKey(e => e.MediaId);
            });
        }
    }

    [Table("CurrentMediaVersionSummary", Schema = "odata")]
    public partial class CurrentMediaVersionSummary
    {

        public System.Guid UserId { get; set; }

        public System.Guid MediaId { get; set; }

        public string Name { get; set; }

        public string Document { get; set; }

        public int CurrentVersion { get; set; }

        public Nullable<int> LatestVersionViewed { get; set; }

    }

}
