
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace VirtualMgr.ODatav4.Database
{

    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Orders", Schema = "odata")]

    public partial class Order
    {

        public Order()
        {

            this.OrderItems = new HashSet<OrderItem>();

            this.OrderMedias = new HashSet<OrderMedia>();

        }


        public System.Guid Id { get; set; }

        public int CatalogId { get; set; }

        public string Catalog { get; set; }

        public System.Guid OrderedById { get; set; }

        public string OrderedBy { get; set; }

        public Nullable<System.Guid> OrderedByCompanyId { get; set; }

        public int FacilityStructureId { get; set; }

        public string FacilityStructure { get; set; }

        [Column(TypeName = "datetime")]
        public System.DateTime DateCreated { get; set; }

        [Column(TypeName = "datetime")]
        public Nullable<System.DateTime> ExpectedDeliveryDate { get; set; }

        public Nullable<System.Guid> TaskId { get; set; }

        public string TaskName { get; set; }

        public Nullable<System.Guid> TaskTypeId { get; set; }

        public Nullable<int> HierarchyBucketId { get; set; }

        public string Name { get; set; }

        public Nullable<decimal> TotalPrice { get; set; }

        [Column(TypeName = "datetime")]
        public Nullable<System.DateTime> PendingExpiryDate { get; set; }

        public Nullable<System.Guid> ExtensionOfOrderId { get; set; }

        public Nullable<System.Guid> ExtensionOrderId { get; set; }

        [Column(TypeName = "datetime")]
        public Nullable<System.DateTime> CancelledDateUtc { get; set; }

        [Column(TypeName = "datetime")]
        public Nullable<System.DateTime> CancelledDateLocal { get; set; }



        public virtual ICollection<OrderItem> OrderItems { get; set; }

        public virtual ICollection<OrderMedia> OrderMedias { get; set; }

    }

}
