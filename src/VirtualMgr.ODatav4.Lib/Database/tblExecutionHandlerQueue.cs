
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace VirtualMgr.ODatav4.Database
{

    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class tblExecutionHandlerQueue
    {

        public System.Guid Id { get; set; }

        public string Handler { get; set; }

        public string Method { get; set; }

        public string PrimaryEntityId { get; set; }

        [Column(TypeName = "datetime")]
        public System.DateTime DateCreated { get; set; }

        public bool Processed { get; set; }

        public string Data { get; set; }

        public string LatestException { get; set; }

    }

}
