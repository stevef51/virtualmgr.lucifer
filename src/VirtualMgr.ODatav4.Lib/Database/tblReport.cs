
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace VirtualMgr.ODatav4.Database
{

    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class tblReport
    {

        public tblReport()
        {

            this.ReportTickets = new HashSet<tblReportTicket>();

            //this.AspNetRoles = new HashSet<AspNetRole>();

        }


        [Key()]
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int Type { get; set; }

        public string Configuration { get; set; }



        [ForeignKey(nameof(tblReportData.ReportId))]
        public virtual tblReportData ReportData { get; set; }

        public virtual ICollection<tblReportTicket> ReportTickets { get; set; }

        //public virtual ICollection<AspNetRole> AspNetRoles { get; set; }

    }

}
