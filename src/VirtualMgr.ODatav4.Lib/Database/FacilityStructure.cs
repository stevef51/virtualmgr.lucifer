
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace VirtualMgr.ODatav4.Database
{

    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Microsoft.EntityFrameworkCore;

    [Table("FacilityStructure", Schema = "odata")]
    public class FacilityStructure
    {
        [Key()]
        public int Id { get; set; }

        public bool Archived { get; set; }

        public int StructureType { get; set; }

        public Nullable<int> ParentId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public Nullable<int> Level { get; set; }

        public Nullable<int> AddressId { get; set; }

        public Nullable<System.Guid> SiteId { get; set; }

        public Nullable<int> FloorPlanId { get; set; }

        public Nullable<int> OnFloorId { get; set; }

        public Nullable<int> OnFloorPlanId { get; set; }

        public Nullable<decimal> FloorHeight { get; set; }

        public Nullable<System.Guid> MediaId { get; set; }

        public Nullable<int> FSFacilityId { get; set; }

        public Nullable<int> FSBuildingId { get; set; }

        public Nullable<int> FSFloorId { get; set; }

        public Nullable<int> FSZoneId { get; set; }

        public Nullable<int> FSSiteId { get; set; }

        public int SortOrder { get; set; }



        public virtual FloorPlan OnFloorPlan { get; set; }

        [ForeignKey(nameof(FSFacilityId))]
        public virtual FSFacility FSFacility { get; set; }

        [ForeignKey(nameof(FSBuildingId))]
        public virtual FSBuilding FSBuilding { get; set; }

        [ForeignKey(nameof(FSFloorId))]
        public virtual FSFloor FSFloor { get; set; }

        [ForeignKey(nameof(FSZoneId))]
        public virtual FSZone FSZone { get; set; }

        [ForeignKey(nameof(FSSiteId))]
        public virtual FSSite FSSite { get; set; }
    }
}

