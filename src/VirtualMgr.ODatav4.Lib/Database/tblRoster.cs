
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace VirtualMgr.ODatav4.Database
{

    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class tblRoster
    {

        public int Id { get; set; }

        public string Name { get; set; }

        public int HierarchyId { get; set; }

        [Column(TypeName = "datetime")]
        public System.DateTime StartTime { get; set; }

        [Column(TypeName = "datetime")]
        public System.DateTime EndTime { get; set; }

        public int EndOfDayDefaultTaskStatus { get; set; }

        public int ScheduleApprovalDefaultTaskStatus { get; set; }

        [Column(TypeName = "datetime")]
        public Nullable<System.DateTime> AutomaticScheduleApprovalTime { get; set; }

        [Column(TypeName = "datetime")]
        public Nullable<System.DateTime> AutomaticEndOfDayTime { get; set; }

        [Column(TypeName = "datetime")]
        public Nullable<System.DateTime> AutomaticScheduleApprovalTimeLastRun { get; set; }

        [Column(TypeName = "datetime")]
        public Nullable<System.DateTime> AutomaticEndOfDayTimeLastRun { get; set; }

        public string Timezone { get; set; }

        public bool IncludeLunchInTimesheetDurations { get; set; }

        public int RoundTimesheetToNearestMins { get; set; }

        public int RoundTimesheetToNearestMethod { get; set; }

        public int TimesheetApprovedSourceMethod { get; set; }

        public bool TimesheetUseScheduledEndWhenFinishedBySystem { get; set; }



        public virtual tblHierarchy tblHierarchy { get; set; }

    }

}
