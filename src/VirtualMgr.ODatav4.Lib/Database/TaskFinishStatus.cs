
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace VirtualMgr.ODatav4.Database
{

    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Microsoft.EntityFrameworkCore;

    public static class TaskFinishStatusExtensions
    {
        public static ModelBuilder BuildTaskFinishStatus(this ModelBuilder builder)
        {
            return builder.Entity<TaskFinishStatus>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.HasKey(e => e.ProjectJobFinishedStatusId);
            });
        }
    }

    [Table("TaskFinishStatus", Schema = "odata")]
    public partial class TaskFinishStatus
    {

        public System.Guid Id { get; set; }

        public System.Guid ProjectJobFinishedStatusId { get; set; }

        public int Stage { get; set; }

        public string Text { get; set; }

        public string Notes { get; set; }


        [ForeignKey(nameof(Id))]
        public virtual Task Task { get; set; }

    }

}
