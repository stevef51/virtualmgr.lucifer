using System;
using VirtualMgr.Common;
using VirtualMgr.ODatav4.Database;

namespace VirtualMgr.ODatav4.Converters
{
    public class ChecklistConverter : IConvertUtcToLocal<Checklist>
    {
        Checklist IConvertUtcToLocal<Checklist>.ConvertUtcToLocal(Checklist entity, Func<DateTime, DateTime> utcToLocal)
        {
            if (entity != null)
            {
                entity.DateStarted = utcToLocal(entity.DateStarted);
                entity.DateCompleted = utcToLocal(entity.DateCompleted);
                if (entity.TaskDateCreated.HasValue == true)
                {
                    entity.TaskDateCreated = utcToLocal(entity.TaskDateCreated.Value);
                }
                if (entity.TaskDateCompleted.HasValue == true)
                {
                    entity.TaskDateCompleted = utcToLocal(entity.TaskDateCompleted.Value);
                }
                if (entity.TaskStatusDate.HasValue == true)
                {
                    entity.TaskStatusDate = utcToLocal(entity.TaskStatusDate.Value);
                }
            }
            return entity;
        }
    }
}