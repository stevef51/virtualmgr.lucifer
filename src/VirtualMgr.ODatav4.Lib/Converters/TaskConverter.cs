using System;
using VirtualMgr.Common;
using VirtualMgr.ODatav4.Database;

namespace VirtualMgr.ODatav4.Converters
{
    public class TaskConverter : IConvertUtcToLocal<Task>
    {
        Task IConvertUtcToLocal<Task>.ConvertUtcToLocal(Task entity, Func<DateTime, DateTime> utcToLocal)
        {
            entity.DateCreatedLocal = utcToLocal(entity.DateCreatedUtc);
            entity.EndsOn = utcToLocal(entity.EndsOn);
            if (entity.DateStarted.HasValue == true)
            {
                entity.DateStarted = utcToLocal(entity.DateStarted.Value);
            }

            if (entity.DateCompleted.HasValue == true)
            {
                entity.DateCompleted = utcToLocal(entity.DateCompleted.Value);
            }
            entity.StatusDate = utcToLocal(entity.StatusDate);

            if (entity.StartTime.HasValue == true)
            {
                entity.StartTime = utcToLocal(entity.StartTime.Value);
            }

            if (entity.LateAfter.HasValue == true)
            {
                entity.LateAfter = utcToLocal(entity.LateAfter.Value);
            }
            return entity;
        }
    }
}