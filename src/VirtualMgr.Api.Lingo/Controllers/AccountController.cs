using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using VirtualMgr.MultiTenant;
using Microsoft.AspNetCore.Authorization;
using VirtualMgr.AspNetCore.Authorization;
using System;
using IdentityModel.Client;
using VirtualMgr.AspNetCore;
using Microsoft.Extensions.Options;
using System.Net.Http;
using Microsoft.Extensions.Logging;

namespace VirtualMgr.Api.Lingo.Controllers
{
    [Route("account")]
    public class AccountController : Controller
    {
        private readonly ILogger<AccountController> _log;
        private readonly AppTenant _appTenant;
        private readonly ServiceOptions _serviceOptions;

        public class UserModel
        {
            public bool authenticated { get; set; }
            public string userId { get; set; }
            public string username { get; set; }
            public string email { get; set; }
            public string authenticationType { get; set; }
            public string[] roles { get; set; }
        }

        public class AuthResult
        {
            public string accessToken { get; set; }
            public string refreshToken { get; set; }
        }


        public AccountController(ILogger<AccountController> log, AppTenant appTenant, IOptions<ServiceOptions> serviceOptions)
        {
            _log = log;
            _appTenant = appTenant;
            _serviceOptions = serviceOptions.Value;
        }

        [HttpPost()]
        [HttpOptions]
        [AuthorizeAny()]
        [Route("Logout")]
        public IActionResult Logout()
        {
            // Only sign out of Cookies, client should drop the JWT if they have one
            return SignOut(Consts.Cookies);
        }


        public class LoginRequest
        {
            public string username { get; set; }
            public string password { get; set; }
        }

        [HttpPost()]
        [HttpOptions]
        [Route("Login")]
        public async Task<IActionResult> Login([FromBody] LoginRequest request)
        {
            var client = new HttpClient();
            //            client.DefaultRequestHeaders.Add(_serviceOptions.TenantDomainHeader, $"{_serviceOptions.Services.Auth}.{_appTenant.PrimaryHostname}");
            var discoOptions = new DiscoveryDocumentRequest()
            {
                Address = $"{_serviceOptions.IdentityServer.Scheme}{_serviceOptions.IdentityServer.Hostname(_appTenant)}/{_serviceOptions.Services.Auth}",
                Policy = new DiscoveryPolicy()
                {
                    Authority = $"{_serviceOptions.IdentityServer.Scheme}{_serviceOptions.IdentityServer.Hostname(_appTenant)}/{_serviceOptions.Services.Auth}",
                    RequireHttps = _serviceOptions.IdentityServer.RequireHttps,
                    ValidateIssuerName = _serviceOptions.IdentityServer.ValidateIssuerName
                }
            };
            var disco = await client.GetDiscoveryDocumentAsync(discoOptions);
            if (disco.IsError)
            {
                return BadRequest(new ServiceResult(disco.Error));
            }

            var tokenResponse = await client.RequestPasswordTokenAsync(new PasswordTokenRequest()
            {
                Address = disco.TokenEndpoint,

                ClientId = _appTenant.PrimaryHostname,
                ClientSecret = "FAEB1AC0-3649-4EE4-BCDD-BDE1758BDFA1",

                UserName = request.username,
                Password = request.password,

                Scope = "openid profile VirtualMgr.Api.Lingo offline_access"
            });

            if (tokenResponse.IsError)
            {
                return BadRequest(new ServiceResult(tokenResponse.Error, tokenResponse.ErrorDescription));
            }

            return Ok(new ServiceResult<AuthResult>(new AuthResult()
            {
                accessToken = tokenResponse.AccessToken,
                refreshToken = tokenResponse.RefreshToken
            }));
        }

        public class RefreshTokenRequest
        {
            public string refreshToken { get; set; }
        }

        [HttpPost()]
        [HttpOptions]
        [Route("RefreshToken")]
        public async Task<IActionResult> RefreshToken([FromBody] RefreshTokenRequest request)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Add(_serviceOptions.TenantDomainHeader, $"{_serviceOptions.Services.Auth}.{_appTenant.PrimaryHostname}");
            var disco = await client.GetDiscoveryDocumentAsync(new DiscoveryDocumentRequest()
            {
                Address = $"{_serviceOptions.IdentityServer.Scheme}{_serviceOptions.IdentityServer.Hostname(_appTenant)}/{_serviceOptions.Services.Auth}",
                Policy = new DiscoveryPolicy()
                {
                    Authority = $"{_serviceOptions.IdentityServer.Scheme}{_serviceOptions.IdentityServer.Hostname(_appTenant)}/{_serviceOptions.Services.Auth}",
                    RequireHttps = _serviceOptions.IdentityServer.RequireHttps,
                    ValidateIssuerName = _serviceOptions.IdentityServer.ValidateIssuerName
                }
            });
            if (disco.IsError)
            {
                return BadRequest(new ServiceResult(disco.Error));
            }

            var tokenResponse = await client.RequestRefreshTokenAsync(new IdentityModel.Client.RefreshTokenRequest()
            {
                Address = disco.TokenEndpoint,

                ClientId = _appTenant.PrimaryHostname,
                ClientSecret = "FAEB1AC0-3649-4EE4-BCDD-BDE1758BDFA1",

                RefreshToken = request.refreshToken,

                Scope = "openid profile VirtualMgr.Api.Lingo offline_access"
            });

            if (tokenResponse.IsError)
            {
                if (tokenResponse.Error == "invalid_grant")     // Has refreshToken expired/invalid
                {
                    return Unauthorized();
                }
                return BadRequest(new ServiceResult(tokenResponse.Error, tokenResponse.ErrorDescription));
            }

            return Ok(new ServiceResult<AuthResult>(new AuthResult()
            {
                accessToken = tokenResponse.AccessToken,
                refreshToken = tokenResponse.RefreshToken
            }));
        }

        private UserModel GetUserModel()
        {
            return new UserModel()
            {
                authenticated = User.Identity.IsAuthenticated,
                userId = User.FindFirst("sub")?.Value,
                username = User.FindFirst("name")?.Value,
                email = User.FindFirst("email")?.Value,
                authenticationType = User.Identity.AuthenticationType,
                roles = (from identity in User.Identities from claim in identity.Claims where claim.Type == "role" select claim.Value).ToArray()
            };
        }

        [HttpGet()]
        [HttpOptions]
        [AuthorizeBearer()]
        [Route("GetUser")]
        public IActionResult GetUser()
        {
            return Ok(new ServiceResult<UserModel>(GetUserModel()));
        }
    }
}
