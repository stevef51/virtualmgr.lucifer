﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using VirtualMgr.MultiTenant;
using Microsoft.AspNetCore.Authorization;
using VirtualMgr.AspNetCore.Authorization;
using System;
using MassTransit;
using VirtualMgr.MassTransit.Contracts;

namespace VirtualMgr.Api.Lingo.Controllers
{
    [Route("life")]
    public class LifeController : Controller
    {
        private readonly AppTenant _appTenant;
        private readonly SqlTenantMaster _appTenantMaster;
        private readonly IBus _bus;

        public LifeController(AppTenant appTenant, SqlTenantMaster appTenantMaster, IBus bus)
        {
            _appTenant = appTenant;
            _appTenantMaster = appTenantMaster;
            _bus = bus;
        }

        [Route("Ping")]
        [HttpPost]
        [HttpGet]
        public string Ping()
        {
            return "Pong";
        }

        [AuthorizeBearer()]
        [Route("Tenant")]
        public AppTenant Tenant()
        {
            return _appTenant;
        }


        [AuthorizeBearer()]
        [Route("TenantMaster")]
        public SqlTenantMaster TenantMaster()
        {
            return _appTenantMaster;
        }

        [AuthorizeBearer(Roles = "User")]
        [Route("RefreshTenants")]
        public async Task<IActionResult> RefreshTenants()
        {
            await _bus.Publish<TenantConnectionsUpdated>(new { });
            return Ok($"Refreshed on {DateTime.Now.ToString()}");
        }
    }
}
