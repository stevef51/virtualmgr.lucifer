﻿using System.Collections.Generic;
using System.Linq;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.BusinessLogic.Models;
using VirtualMgr.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace VirtualMgr.Api.Lingo
{
    [AuthorizeAny(Roles = "User")]
    [Route("Published")]
    public class PublishedController : ControllerBase
    {
        private readonly IPublishingGroupResourceRepository _publishingGroupResourceRepository;
        private readonly IPublishingGroupRepository _publishingGroupRepository;

        public PublishedController(IPublishingGroupResourceRepository publishingGroupResourceRepository,
                                    IPublishingGroupRepository publishingGroupRepository)
        {
            _publishingGroupResourceRepository = publishingGroupResourceRepository;
            _publishingGroupRepository = publishingGroupRepository;
        }

        [HttpGet]
        [Route("Checklists")]
        public IList<EnumerateChecklistsResponseItem> Checklists()
        {
            var checklists = _publishingGroupResourceRepository.GetChecklistsUserCanDo(User.Identity.Name);
            return checklists.Select(c => new EnumerateChecklistsResponseItem(c)).ToList();
        }

        [HttpGet]
        [Route("GetById/{id}")]
        public IActionResult GetById(int id)
        {
            var result = _publishingGroupResourceRepository.GetById(id, PublishingGroupResourceLoadInstruction.Resource);
            if (result == null)
            {
                return NotFound();
            }
            return Ok(new EnumerateChecklistsResponseItem(result));
        }

        [HttpGet]
        [Route("Checklists/{id}")]
        public IActionResult Checklists(int id)
        {
            var clist = _publishingGroupResourceRepository.GetById(id, PublishingGroupResourceLoadInstruction.Resource);
            if (clist == null)
            {
                return NotFound();
            }

            if (!_publishingGroupRepository.UserIsAllowedGroup(User.Identity.Name, clist.PublishingGroupId, PublishingGroupActorType.Reviewer))
            {
                return Forbid();
            }

            var reviewees = _publishingGroupRepository.GetUsersForGroup(clist.PublishingGroupId, PublishingGroupActorType.Reviewee);
            var reviewers = _publishingGroupRepository.GetUsersForGroup(clist.PublishingGroupId, PublishingGroupActorType.Reviewer);
            return Ok(new DetailChecklistResponseItem(clist, reviewers, reviewees));
        }

        [HttpGet]
        [Route("Groups")]
        public IActionResult Groups()
        {
            var groupEnts = _publishingGroupRepository.GetGroupsUserCanDo(User.Identity.Name, PublishingGroupActorType.Reviewer);
            var result = from e in groupEnts
                         select new EnumerateGroupsResponseItem(e);
            return Ok(result.ToList());
        }

        [HttpGet]
        [Route("Groups/{id}")]
        public IActionResult Groups(int id)
        {
            var group = _publishingGroupRepository.GetById(id, PublishingGroupLoadInstruction.Resources | PublishingGroupLoadInstruction.Actors | PublishingGroupLoadInstruction.Labels);
            if (group == null)
            {
                return NotFound();
            }

            if (!_publishingGroupRepository.UserIsAllowedGroup(User.Identity.Name, group.Id, PublishingGroupActorType.Reviewer))
            {
                return Forbid();
            }

            var reviewees = _publishingGroupRepository.GetUsersForGroup(id, PublishingGroupActorType.Reviewee);
            return Ok(new DetailGroupResponseItem(group, group.PublishingGroupResources, reviewees));
        }
    }
}