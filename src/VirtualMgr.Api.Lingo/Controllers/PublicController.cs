﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using VirtualMgr.MultiTenant;
using Microsoft.AspNetCore.Authorization;
using VirtualMgr.AspNetCore.Authorization;
using System;
using MassTransit;
using VirtualMgr.MassTransit.Contracts;

namespace VirtualMgr.Api.Lingo.Controllers
{
    [Route("public")]
    public class PublicController : Controller
    {
        private Func<AppTenant> _fnAppTenant;
        public PublicController(Func<AppTenant> fnAppTenant)
        {
            _fnAppTenant = fnAppTenant;
        }

        [Route("Ping")]
        [HttpPost]
        public string Ping(string echo = null)
        {
            if (echo != null)
            {
                return echo;
            }
            return "Pong";
        }

        [Route("ValidTenant")]
        [HttpPost]
        public bool ValidTenant()
        {
            // Check if we have a tenant 
            var tenant = _fnAppTenant();
            return tenant != null && tenant.ValidTenant;
        }
    }
}
