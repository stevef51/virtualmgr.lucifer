﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Repository.LingoQuery.Datastores;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.BusinessLogic;
using ConceptCave.BusinessLogic.Models;
using Newtonsoft.Json.Linq;
using ConceptCave.BusinessLogic.Facilities;
using ConceptCave.Checklist;
using ConceptCave.BusinessLogic.Configuration;
using System.IO;
using Newtonsoft.Json;
using VirtualMgr.Membership;
using VirtualMgr.Membership.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Authorization;
using VirtualMgr.AspNetCore.Authorization;

namespace VirtualMgr.Api.Lingo
{
    //    [WebApiCorsAuthorize(Roles = "User")]
    //    [WebApiCorsOptionsAllow]
    [AuthorizeBearer(Roles = "User")]
    [Route("Player")]
    public class PlayerController : ControllerBase, IExecutionMethodHandlerContext, IWorkingDocumentFactory
    {
        private readonly IUserAgentRepository _userAgentRepo;
        private readonly IWorkingDocumentRepository _wdRepo;
        private readonly IWorkingDocumentStateManager _wdStateManager;
        //private readonly IMembershipRepository _memberRepo;
        private readonly IGlobalSettingsRepository _settingRepo;
        private readonly IMediaManager _mediaManager;
        private readonly IReflectionFactory _reflectionFactory;
        private readonly IRepositorySectionManager _repositorySectionManager;
        private readonly IErrorLogRepository _errorLogRepo;
        private readonly IExecutionHandlerQueueRepository _executionQueueRepo;
        private readonly IUrlParser _urlParser;
        private readonly IMembership _membership;
        private readonly ICurrentContextProvider _membershipContextProvider;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IServiceProvider _serviceProvider;

        public PlayerController(IUserAgentRepository userAgentRepo,
            IWorkingDocumentRepository wdRepo,
            IWorkingDocumentStateManager wdStateManager,
            IReflectionFactory reflectionFactory,
            IRepositorySectionManager repositorySectionManager,
            IMediaManager mediaManager,
            IGlobalSettingsRepository settingRepo,
            IErrorLogRepository errorLogRepo,
            IExecutionHandlerQueueRepository executionQueueRepo,
            IUrlParser urlParser,
            IMembership membership,
            ICurrentContextProvider membershipContextProvider,
            IHttpContextAccessor httpContextAccessor,
            IServiceProvider serviceProvider)
        {
            _userAgentRepo = userAgentRepo;
            _wdRepo = wdRepo;
            _wdStateManager = wdStateManager;
            _settingRepo = settingRepo;
            //_memberRepo = memberRepo;
            _mediaManager = mediaManager;

            _reflectionFactory = reflectionFactory;
            _repositorySectionManager = repositorySectionManager;
            _errorLogRepo = errorLogRepo;
            _executionQueueRepo = executionQueueRepo;
            _urlParser = urlParser;
            _membership = membership;
            _membershipContextProvider = membershipContextProvider;
            _httpContextAccessor = httpContextAccessor;
            _serviceProvider = serviceProvider;
        }


        public class BeginModel
        {
            public int groupId { get; set; }
            public int resourceId { get; set; }
            public Guid? revieweeId { get; set; }
            public string args { get; set; }
            public int? publishingGroupResourceId { get; set; }
            public string jsonArgs { get; set; }
        }

        public PlayerModel CreateStartAndSave(int groupId, int resourceId, string args)
        {
            Guid reviewerId = _membershipContextProvider.InternalUserId;

            return CreateStartAndSave(groupId, resourceId, reviewerId, reviewerId, args);
        }

        public PlayerModel CreateStartAndSave(int groupId, int resourceId, Guid reviewerId, Guid? revieweeId, string args, int? publishingGroupResourceId = null)
        {
            Dictionary<string, object> arguments = null;
            if (!string.IsNullOrEmpty(args))
            {
                try
                {
                    // 0 0
                    // 1 3
                    // 2 2
                    // 3 1
                    var argsb64 = args;
                    if ((argsb64.Length & 3) != 0)
                    {
                        argsb64 = argsb64.PadRight((args.Length + 4) & ~3, '=');
                    }
                    var b64 = Convert.FromBase64String(argsb64);
                    var ms = new MemoryStream(b64);
                    using (var sr = new StreamReader(ms, System.Text.Encoding.UTF8))
                    {
                        var b64s = sr.ReadToEnd();
                        arguments = JsonConvert.DeserializeObject<Dictionary<string, object>>(b64s);
                    }
                }
                catch
                {
                    arguments = HrefFacility.DecodeArguments(args);
                }
            }
            WorkingDocumentDTO documentEntity = null;
            IWorkingDocument documentObject = null;

            using (var scope = new TransactionScope())
            {
                var agent = _userAgentRepo.GetOrCreate(_httpContextAccessor.HttpContext.Request.Headers["User-Agent"].ToString());
                try
                {
                    WorkingDocumentStateManager.WorkingDocumentContainer docContainer;
                    if (publishingGroupResourceId.HasValue)
                        docContainer = _wdStateManager.BeginWorkingDocument(publishingGroupResourceId.Value, reviewerId, revieweeId, null, arguments);
                    else
                        docContainer = _wdStateManager.BeginWorkingDocument(groupId, resourceId, reviewerId, revieweeId, null, arguments);

                    documentEntity = docContainer.DocumentEntity;
                    documentObject = docContainer.DocumentObject;
                }
                catch (ArgumentException)
                {
                    return null;
                }


                documentEntity.RevieweeId = revieweeId;
                documentEntity.ClientIp = _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
                documentEntity.UserAgentId = agent.Id;
                documentEntity.DateCreated = DateTime.UtcNow;
                documentEntity = _wdStateManager.SaveWorkingDocument(documentObject, documentEntity);

                //now advance it to the first presentable
                InjectServerOnlyObjects(documentObject);
                var returnModel = _wdStateManager.AdvanceWorkingDocument(documentObject, documentEntity);

                //save WD after this
                _wdStateManager.SaveWorkingDocument(documentObject, documentEntity);
                scope.Complete();
                return returnModel;
            }
        }

        public IWorkingDocument CreateFrom(WorkingDocumentDTO dto)
        {
            var wd = _wdStateManager.WorkingDocumentFromString(dto.WorkingDocumentData.Data);
            //now advance the WD
            InjectServerOnlyObjects(wd);

            return wd;
        }

        //This method actually sets up the checklist to be started, and calls next through 
        [HttpPost]
        [HttpOptions]
        [Route("Begin")]
        public IActionResult Begin([FromBody] BeginModel beginModel)
        {
            var model = CreateStartAndSave(
                beginModel.groupId,
                beginModel.resourceId,
                _membershipContextProvider.InternalUserId,
                beginModel.revieweeId,
                beginModel.args,
                beginModel.publishingGroupResourceId);
            if (model == null) return NotFound($"The checklist specified by groupId {beginModel.groupId} and resourceId {beginModel.resourceId} was not found");
            return Ok(model);
        }

        [HttpPost]
        [HttpOptions]
        public IActionResult Save([FromBody] AnswerModel answerModel)
        {
            //load up WD
            var (documentEntity, error) = LoadWorkingDocument(answerModel.WorkingDocumentId);
            if (error != null) return error;

            var wd = _wdStateManager.WorkingDocumentFromString(documentEntity.WorkingDocumentData.Data);
            //now advance the WD
            InjectServerOnlyObjects(wd);

            using (var scope = new TransactionScope())
            {
                var returnModel = _wdStateManager.SaveState(wd, documentEntity, answerModel.Answers);

                //save the WD
                _wdStateManager.SaveWorkingDocument(wd, documentEntity);
                scope.Complete();
                return Ok(returnModel);
            }
        }

        [HttpPost]
        [HttpOptions]
        [Route("Next")]
        public IActionResult Next([FromBody] AnswerModel answerModel)
        {
            //load up WD
            var (documentEntity, error) = LoadWorkingDocument(answerModel.WorkingDocumentId);
            if (error != null) return error;
            var wd = _wdStateManager.WorkingDocumentFromString(documentEntity.WorkingDocumentData.Data);
            //now advance the WD
            InjectServerOnlyObjects(wd);

            using (var scope = new TransactionScope())
            {
                // Fix for EVS-699 "Mandatory" flag in checklist stops AutoFinish 
                // Pass the document DTO by reference since AdvanceWorkingDocument *can* (not always) actually save the DTO
                // and return a new instance, we need this new instance since we save it below, if we dont then we save stale data
                // which causes state issues like EVS-699
                var returnModel = _wdStateManager.AdvanceWorkingDocument(wd, ref documentEntity, answerModel.Answers);

                //save the WD
                _wdStateManager.SaveWorkingDocument(wd, documentEntity);
                scope.Complete();
                return Ok(returnModel);
            }
        }

        [HttpPost]
        [HttpOptions]
        [Route("Previous")]
        public IActionResult Previous([FromBody] AnswerModel answerModel)
        {
            var (documentEntity, error) = LoadWorkingDocument(answerModel.WorkingDocumentId);
            if (error != null) return error;
            var wd = _wdStateManager.WorkingDocumentFromString(documentEntity.WorkingDocumentData.Data);
            InjectServerOnlyObjects(wd);

            using (var scope = new TransactionScope())
            {
                var returnModel = _wdStateManager.ReverseWorkingDocument(wd, documentEntity);
                _wdStateManager.SaveWorkingDocument(wd, documentEntity);
                scope.Complete();
                return Ok(returnModel);
            }
        }

        [HttpPost]
        [HttpOptions]
        [Route("Cancel")]
        public IActionResult Cancel(Guid id)
        {
            using (var scope = new TransactionScope())
            {
                var result = _wdStateManager.CancelWorkingDocument(id);
                scope.Complete();
                return Ok(result);
            }
        }

        [HttpGet]
        [HttpOptions]
        [Route("Continue/{id}")]
        public IActionResult Continue(Guid id)
        {
            var (wddto, error) = LoadWorkingDocument(id);
            if (error != null) return error;
            var wdobj = _wdStateManager.WorkingDocumentFromString(wddto.WorkingDocumentData.Data);
            InjectServerOnlyObjects(wdobj);
            using (var scope = new TransactionScope())
            {
                var pm = _wdStateManager.ContinueWorkingDocument(wdobj, wddto);
                scope.Complete();
                return Ok(pm);
            }
        }

        [HttpGet]
        [HttpOptions]
        [Route("Dashboard")]
        public IActionResult Dashboard()
        {
            //who is this user, and what are their dashboards?
            var uid = _membershipContextProvider.InternalUserId;
            var dbList = _wdStateManager.PlayDashboards(uid, InjectServerOnlyObjects);
            var pm = new DashboardModel()
            {
                Dashboards = dbList,
                GeneratedAtUtc = DateTime.UtcNow,
                IsStale = false
            };
            return Ok(pm);
        }

        [HttpPost]
        [HttpOptions]
        [Route("Execute")]
        public JToken Execute([FromBody] ExecuteModel executeModel)
        {
            string type = _repositorySectionManager.ExecutionHandlerTypeForName(executeModel.Handler);

            IExecutionMethodHandler handler = (IExecutionMethodHandler)_reflectionFactory.InstantiateObject(Type.GetType(type));

            if (handler is IServerSideQueuedExecutionHandler)
            {
                // it does support server side queueing. So what we are trying to do here
                // is build in some resilliance to server side issues and let the client go ahead with
                // what its wanting to do
                var queueHandler = (IServerSideQueuedExecutionHandler)handler;

                if (queueHandler.MethodSupportsQueue(executeModel.Method, executeModel.Parameters) == true)
                {
                    string primaryEntityId = queueHandler.ExractPrimaryEntityId(executeModel.Method, executeModel.Parameters);

                    JObject queuedResult = new JObject();
                    queuedResult["queued"] = true;

                    if (string.IsNullOrEmpty(primaryEntityId) == false)
                    {
                        // ok so queuing is supported by the handler and method and we have an id

                        // the id here is that we queue as a last resort, however we want to play actions in the same order
                        // as they occurr against what ever the primary entity is. So if there are items not processed already
                        // queued up, then we add to that queue to guarantee sequentiality. If there aren't then we try and
                        // do the execution now and only attempt to queue if that fails. If the reason for the failure is some
                        // db problen, then that'll likely cause the queue operation to fail and the client will receive an exception
                        // but we've done our best to save things and the client should be queueing offline stuff anyway, so it
                        // will try again at some point.
                        if (_executionQueueRepo.GetUnProcessedCountForPrimaryEntity(executeModel.Handler, primaryEntityId) > 0)
                        {
                            // ther are already queued items for this primary entity that haven't been processed yet.
                            // to maintain the sequence of actions, we just queue this request and move on
                            _executionQueueRepo.Queue(executeModel.Handler, executeModel.Method, primaryEntityId, executeModel.Parameters.ToString(), "Previous actions for entity unprocessed");

                            return queuedResult;
                        }
                        else
                        {
                            try
                            {
                                // so nothng queued, so lets see if we can execute, if we can't we'll queue this request
                                return handler.Execute(executeModel.Method, executeModel.Parameters, this);
                            }
                            catch (Exception e)
                            {
                                _executionQueueRepo.Queue(executeModel.Handler, executeModel.Method, primaryEntityId, executeModel.Parameters.ToString(), e.Message);

                                return queuedResult;
                            }
                        }
                    }
                }
            }

            // doesn't support server side queueing, so just execute and be done with it
            return handler.Execute(executeModel.Method, executeModel.Parameters, this);
        }

        [HttpPost]
        [HttpOptions]
        [Route("LogError")]
        public void LogError(JToken request)
        {
            var agent = _userAgentRepo.GetOrCreate(_httpContextAccessor.HttpContext.Request.Headers["User-Agent"].ToString());

            Func<JToken, string, string> extract = (jtoken, key) =>
                {
                    var value = jtoken[key];
                    if (value == null)
                        return null;
                    return value.ToString();
                };

            var error = request["error"];
            var errorType = error["type"];
            var type = extract(errorType, "type") ?? "<Unknown Type>";
            var source = extract(errorType, "source") ?? "<Unknown Source>";
            var message = extract(errorType, "message") ?? "<Unknown Message>";
            var stackTrace = extract(errorType, "stack") ?? "<Unknown Stack Trace>";

            _errorLogRepo.LogError(new ErrorLogTypeDTO()
            {
                Type = type,
                Source = source,
                Message = message,
                StackTrace = stackTrace
            }, new ErrorLogItemDTO()
            {
                UserId = _membershipContextProvider.InternalUserId,
                UserAgentId = agent.Id,
                Data = extract(error, "data")
            });
        }

        //This method gets the workign dcoument from the DB
        //but alsoc hecks for appropriate permissions: can only load if your'e the reivewer
        private (WorkingDocumentDTO, IActionResult) LoadWorkingDocument(Guid id)
        {
            var entity = _wdRepo.GetById(id, WorkingDocumentLoadInstructions.Data);
            return (entity, entity == null ? NotFound($"WorkingDocument with id {id} not found") : null);
        }

        private void InjectServerOnlyObjects(IWorkingDocument doc)
        {
            doc.Program.Set<Lazy<ILingoDatastore>>("dbquestions", new Lazy<ILingoDatastore>(() => _serviceProvider.GetRequiredService<QuestionDatastore>()));
            doc.Program.Set<Lazy<ILingoDatastore>>("dbchecklists", new Lazy<ILingoDatastore>(() => _serviceProvider.GetRequiredService<ChecklistDatastore>()));
            doc.Program.Set<Lazy<ILingoDatastore>>("dbscoredchecklists", new Lazy<ILingoDatastore>(() => _serviceProvider.GetRequiredService<ScoredChecklistDatastore>()));
            doc.Program.Set<Lazy<ILingoDatastore>>("dbthischecklist", new Lazy<ILingoDatastore>(() => _serviceProvider.GetRequiredService<CurrentDocumentDatastore>()));
            doc.Program.Set<Lazy<ILingoDatastore>>("dbmemberships", new Lazy<ILingoDatastore>(() => _serviceProvider.GetRequiredService<MembershipDatastore>()));
            doc.Program.Set<Lazy<ILingoDatastore>>("dbpublishedresources", new Lazy<ILingoDatastore>(() => _serviceProvider.GetRequiredService<PublishedChecklistDatastore>()));
            doc.Program.Set<Lazy<ILingoDatastore>>("dbmembershipcontexts", new Lazy<ILingoDatastore>(() => _serviceProvider.GetRequiredService<MembershipContextDatastore>()));
            doc.Program.Set<Lazy<IPlayerStringRenderer>>(new Lazy<IPlayerStringRenderer>(() => _serviceProvider.GetRequiredService<PlayerStringRenderer>()));
        }

        public object CurrentUserId
        {
            get
            {
                return _membershipContextProvider.InternalUserId;
            }
        }
    }
}