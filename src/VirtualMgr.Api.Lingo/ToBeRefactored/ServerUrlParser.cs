﻿using System;
using ConceptCave.BusinessLogic.Configuration;
using ConceptCave.Checklist;
using ConceptCave.RepositoryInterfaces.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using VirtualMgr.AspNetCore;
using VirtualMgr.MultiTenant;

namespace VirtualMgr.Api.Lingo
{
    public class ServerUrlParser : IUrlParser, IServerUrls
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly AppTenant _appTenant;
        private readonly IGlobalSettingsRepository _globalSettingsRepo;
        private readonly ServiceOptions _serviceOptions;

        public ServerUrlParser(
            IHttpContextAccessor httpContextAccessor,
            AppTenant appTenant,
            IOptions<ServiceOptions> serviceOptions,
            IGlobalSettingsRepository globalSettingsRepo)
        {
            _httpContextAccessor = httpContextAccessor;
            _appTenant = appTenant;
            _globalSettingsRepo = globalSettingsRepo;
            _serviceOptions = serviceOptions.Value;
        }

        public string ResolveVirtualPath(string path)
        {
            // This is probably wrong since we dont know which service (subdomain) we are meant to return for, client needs to specify this
            var context = _httpContextAccessor.HttpContext;
            return $"https://{_appTenant.PrimaryHostname}/{path}";
        }

        // Default to V3 OData
        public string ODataUrl { get => ODataV3Url; }

        public string ODataV3Url
        {
            get
            {
                var context = _httpContextAccessor.HttpContext;
                return $"https://{_appTenant.PrimaryHostname}/{_serviceOptions.Services.OData}/odata-v3";
            }
        }

        public string ODataV4Url
        {
            get
            {
                var context = _httpContextAccessor.HttpContext;
                return $"https://{_appTenant.PrimaryHostname}/{_serviceOptions.Services.OData}/odata-v4";
            }
        }

        public string JsReportServerUrl
        {
            get
            {
                return _globalSettingsRepo.GetSetting<string>("JsReportServerUrl") ?? _serviceOptions.JsReportServerUrl;
            }
        }
    }
}