﻿using System;
using System.Collections.Generic;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core.Coding;
using System.Text;
using System.Text.RegularExpressions;
using ConceptCave.Repository.Facilities;
using ConceptCave.Core;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.BusinessLogic.Configuration;

namespace VirtualMgr.Api.Lingo
{
    public class PlayerStringRenderer : ICodable, IPlayerStringRenderer
    {
        protected StringRendererHandlers _handlers;
        protected IMediaManager _manager;
        protected IGlobalSettingsRepository _settingRepo;
        private readonly IUrlParser _urlParser;

        public PlayerStringRenderer(IMediaManager manager, IGlobalSettingsRepository settingRepo, IUrlParser urlParser)
        {
            _handlers = new StringRendererHandlers();
            _handlers.Add(new PresentedSectionRenderer());
            _handlers.Add(new LabelQuestionRenderer());
            _handlers.Add(new SignatureQuestionRenderer());
            _handlers.Add(new GenericQuestionRenderer());

            _manager = manager;
            _settingRepo = settingRepo;
            _urlParser = urlParser;
        }

        public virtual string RenderIntoString(IPresented presented, IWorkingDocument wd, IContextContainer context)
        {
            var h = _handlers.HandlerFor(presented);

            if (h == null)
            {
                return string.Empty;
            }

            StringBuilder builder = new StringBuilder();

            ContextContainer renderContext = new ContextContainer(context);
            renderContext.Set<IMediaManager>(_manager);
            renderContext.Set<IGlobalSettingsRepository>(_settingRepo);

            h.RenderIntoString(presented, wd, renderContext, _handlers, builder);

            // we are going to do a little extra work on URL's embedded in the html. These come out with the form
            // {{ '~/MediaImage/SOMEGUID.png' | serverUrl}}
            // so we are going to search for {{ ' occurances and replace what's between them and the }} with a full
            // URL
            string html = builder.ToString();
            html = ReplaceUrls(html);

            // now something similar(ish) to the above for font awesome images in the source
            html = ReplaceIcons(html);

            return html;
        }

        public string ReplaceUrls(string html)
        {
            string result = html;

            //Assemble all tokens to replace
            Regex tokenRegex = new Regex("{{(.*?)}}", RegexOptions.IgnoreCase | RegexOptions.Compiled);

            foreach (Match match in tokenRegex.Matches(html))
            {
                string source = match.ToString();

                // the only bit we are interested in is what's between the start and end '

                int start = source.IndexOf('\'');
                int end = source.LastIndexOf('\'');

                if (start == -1 || start == end)
                {
                    continue;
                }

                string portion = source.Substring(start + 1, end - start - 1);

                // we now want to replace the ~ portion of the string with a fully qualified URL
                portion = _urlParser.ResolveVirtualPath(portion);

                result = result.Replace(source, portion);
            }

            return result;
        }

        public string ReplaceIcons(string html)
        {
            string result = html;

            //Assemble all tokens to replace
            Regex tokenRegex = new Regex("<i class=\"icon-(.*?)</i>|<i class='icon-(.*?)</i>", RegexOptions.IgnoreCase | RegexOptions.Compiled);

            foreach (Match match in tokenRegex.Matches(html))
            {
                string source = match.ToString();

                // lets get rid of single quotes as a variable and any casing
                string replaced = source.Replace('\'', '"').ToLower();

                // so we have something like <i class="icon-search"></i> and we want <i>&#somenum</i>
                // or we have <i class="icon-search" style="color:green"></i> and want <i style="color:green">&#somenum</i>

                // we approach this by building our replacement
                StringBuilder builder = new StringBuilder("<i");

                var styleIndex = replaced.IndexOf("style=\"");
                if (styleIndex != -1)
                {
                    styleIndex = styleIndex + "style=\"".Length;
                    // there is a style on this sucker
                    builder.Append(" style=\"").Append(replaced.Substring(styleIndex, replaced.IndexOf('"', styleIndex + 1) - styleIndex)).Append("\"");
                }

                builder.Append(">");

                // now we need to do our thing with the class side of things
                var classIndex = replaced.IndexOf("class=\"");
                if (classIndex != -1)
                {
                    classIndex = classIndex + "class=\"".Length;
                    var classString = replaced.Substring(classIndex, replaced.IndexOf('"', classIndex + 1) - classIndex);

                    // see http://dmcritchie.mvps.org/rexx/htm/symbols.htm for more info on these codes
                    string code = "";
                    foreach (string c in classString.Split(' '))
                    {
                        switch (c)
                        {
                            case "icon-ok":
                                code = "&#10003;";
                                break;
                            case "icon-ok-heavy":
                                code = "&#10004;";
                                break;
                            case "icon-remove":
                                code = "&#10005;";
                                break;
                            case "icon-remove-heavy":
                                code = "&#10006;";
                                break;
                            case "icon-remove-ballot":
                                code = "&#10007;";
                                break;
                            case "icon-remove-ballot-heavy":
                                code = "&#10008;";
                                break;
                        }
                    }

                    builder.Append(code);
                }

                builder.Append("</i>");

                result = result.Replace(source, builder.ToString());
            }

            return result;
        }

        public void Encode(IEncoder encoder)
        {
            //These are here so we can inject the dependancy into context and not have
            //the decoder yell at us.
            //This class contains no state
        }

        public void Decode(IDecoder decoder)
        {

        }
    }

    public class StringRendererHandlers
    {
        protected List<StringRendererBase> _renderers;

        public StringRendererHandlers()
        {
            _renderers = new List<StringRendererBase>();
        }

        public void Add(StringRendererBase renderer)
        {
            _renderers.Add(renderer);
        }

        public StringRendererBase HandlerFor(IPresented presented)
        {
            StringRendererBase result = null;

            foreach (var r in _renderers)
            {
                if (r.Handles(presented) == true)
                {
                    result = r;
                    break;
                }
            }

            return result;
        }
    }

    public abstract class StringRendererBase
    {
        public abstract bool Handles(IPresented presented);

        public abstract void RenderIntoString(IPresented presented, IWorkingDocument wd, IContextContainer context, StringRendererHandlers handlers, StringBuilder builder);

        /// <summary>
        /// Searches through a string in the form of an html class="" attribute for classes that are a variant of the base class.
        /// So for example given class="alert alert-error span2" with alert as the base class, alert and alert-error would be returned.
        /// </summary>
        /// <param name="classes">string with class names</param>
        /// <param name="baseClass">base class to search for variants of</param>
        /// <returns></returns>
        public string[] GetClassVariants(string classes, string baseClass)
        {
            string[] parts = classes.Split(' ');

            List<string> result = new List<string>();

            var bc = baseClass.ToLower();

            foreach (var p in parts)
            {
                if (p.ToLower().StartsWith(bc))
                {
                    result.Add(p);
                }
            }

            return result.ToArray();
        }
    }

    /// <summary>
    /// Renderer to render sections
    /// </summary>
    public class PresentedSectionRenderer : StringRendererBase
    {
        public override bool Handles(IPresented presented)
        {
            return presented is IPresentedSection;
        }

        public override void RenderIntoString(IPresented presented, IWorkingDocument wd, IContextContainer context, StringRendererHandlers handlers, StringBuilder builder)
        {
            if ((presented is IPresentedSection) == false)
            {
                throw new ArgumentException("This renderer only handles presented sections");
            }

            IPresentedSection section = (IPresentedSection)presented;

            if (section.Children.Count == 0 && string.IsNullOrEmpty(section.Section.Prompt) == true)
            {
                // sections only render if there is some content to them
                return;
            }

            string[] alerts = GetClassVariants(section.Section.HtmlPresentation.CustomClass, "alert");
            string tag = "p";
            string endTag = "</p>";
            bool requiresTdWrapper = false;

            if (alerts.Length > 0)
            {
                // ok its got an alert on it, we are going to model this through a table
                builder.Append("<table style='width:100%'><tr><td");
                endTag = "</td></tr></table>";
            }
            else
            {
                if (section.Section.HtmlPresentation.Layout == HtmlSectionLayout.Table)
                {
                    tag = "table";
                    endTag = "</table>";
                }
                else if (section.Section.HtmlPresentation.Layout == HtmlSectionLayout.TableRow)
                {
                    tag = "tr";
                    endTag = "</tr>";
                    requiresTdWrapper = true;
                }

                builder.Append("<").Append(tag);
            }

            // we need to add the classes and any custom styling
            if (string.IsNullOrEmpty(section.Section.HtmlPresentation.CustomClass) == false)
            {
                builder.Append(" class='").Append(section.Section.HtmlPresentation.CustomClass.Replace("alert ", "")).Append("'");
            }

            if (string.IsNullOrEmpty(section.Section.HtmlPresentation.CustomStyling) == false)
            {
                builder.Append(" style='").Append(section.Section.HtmlPresentation.CustomStyling).Append("'");
            }

            builder.Append(">");

            if (string.IsNullOrEmpty(section.Section.Prompt) == false)
            {
                builder.Append("<h4>").Append(section.Section.Prompt).Append("</h4>");
            }

            foreach (var child in section.Children)
            {
                if (requiresTdWrapper)
                {
                    builder.Append("<td>");
                }

                var r = handlers.HandlerFor(child);

                if (r != null)
                {
                    r.RenderIntoString(child, wd, context, handlers, builder);
                }

                if (requiresTdWrapper)
                {
                    builder.Append("</td>");
                }
            }

            builder.Append(endTag);
        }
    }

    public class GenericQuestionRenderer : StringRendererBase
    {
        protected bool _wrapperRequired;

        public override bool Handles(IPresented presented)
        {
            return presented is IPresentedQuestion;
        }

        protected virtual void RenderStartWrapperHtml(IPresentedQuestion question, IWorkingDocument wd, IContextContainer context, StringRendererHandlers handlers, StringBuilder builder)
        {
            string[] alerts = GetClassVariants(question.Question.HtmlPresentation.CustomClass, "alert");

            if (alerts.Length == 0)
            {
                return;
            }

            // ok its got an alert on it, we are going to model this through a table
            builder.Append("<table style='width:100%'><tr><td");

            if (string.IsNullOrEmpty(question.Question.HtmlPresentation.CustomClass) == false)
            {
                builder.Append(" class='").Append(question.Question.HtmlPresentation.CustomClass.Replace("alert ", "")).Append("'");
            }

            builder.Append(">");

            _wrapperRequired = true;
        }

        protected virtual void RenderEndWrapperHtml(IPresentedQuestion question, IWorkingDocument wd, IContextContainer context, StringRendererHandlers handlers, StringBuilder builder)
        {
            if (_wrapperRequired == false)
            {
                return;
            }

            builder.Append("</td></tr></table>");
        }

        protected virtual void RenderPrompt(IPresentedQuestion question, IWorkingDocument wd, IContextContainer context, StringRendererHandlers handlers, StringBuilder builder)
        {
            if (string.IsNullOrEmpty(question.Prompt) == true)
            {
                return;
            }

            builder.Append("<h4");

            if (string.IsNullOrEmpty(question.Question.HtmlPresentation.CustomStyling) == false)
            {
                builder.Append(" style='").Append(question.Question.HtmlPresentation.CustomStyling).Append("'");
            }

            builder.Append(">").Append(question.Prompt).Append("</h4>");
        }

        protected virtual void RenderAnswer(IPresentedQuestion question, IWorkingDocument wd, IContextContainer context, StringRendererHandlers handlers, StringBuilder builder)
        {
            if (question.Question.IsAnswerable)
            {
                IAnswer answer = question.GetAnswer();

                builder.Append("<span");

                if (string.IsNullOrEmpty(question.Question.HtmlPresentation.CustomStyling) == false)
                {
                    builder.Append(" style='").Append(question.Question.HtmlPresentation.CustomStyling).Append("'");
                }

                builder.Append(">").Append(answer.AnswerAsString).Append("</span>");
            }
        }

        public override void RenderIntoString(IPresented presented, IWorkingDocument wd, IContextContainer context, StringRendererHandlers handlers, StringBuilder builder)
        {
            if ((presented is IPresentedQuestion) == false)
            {
                throw new ArgumentException("This renderer only handles presented questions");
            }

            _wrapperRequired = false;

            IPresentedQuestion question = (IPresentedQuestion)presented;

            RenderStartWrapperHtml(question, wd, context, handlers, builder);

            RenderPrompt(question, wd, context, handlers, builder);

            RenderAnswer(question, wd, context, handlers, builder);

            RenderEndWrapperHtml(question, wd, context, handlers, builder);
        }
    }

    public class LabelQuestionRenderer : GenericQuestionRenderer
    {
        public override bool Handles(IPresented presented)
        {
            var result = base.Handles(presented);

            if (result == false)
            {
                return result;
            }

            return ((IPresentedQuestion)presented).Question is ILabelQuestion;
        }

        protected override void RenderPrompt(IPresentedQuestion question, IWorkingDocument wd, IContextContainer context, StringRendererHandlers handlers, StringBuilder builder)
        {
            base.RenderPrompt(question, wd, context, handlers, builder);

            ILabelQuestion label = (ILabelQuestion)question.Question;
            if (string.IsNullOrEmpty(label.Text) == true)
            {
                return;
            }

            builder.Append("<span");

            if (string.IsNullOrEmpty(question.Question.HtmlPresentation.CustomStyling) == false)
            {
                builder.Append(" style='").Append(question.Question.HtmlPresentation.CustomStyling).Append("'");
            }

            builder.Append(">").Append(label.Text).Append("</span>");
        }
    }

    public class SignatureQuestionRenderer : GenericQuestionRenderer
    {
        public override bool Handles(IPresented presented)
        {
            var result = base.Handles(presented);

            if (result == false)
            {
                return result;
            }

            return ((IPresentedQuestion)presented).Question is ISignatureQuestion;
        }

        protected override void RenderAnswer(IPresentedQuestion question, IWorkingDocument wd, IContextContainer context, StringRendererHandlers handlers, StringBuilder builder)
        {
            ISignatureAnswer answer = (ISignatureAnswer)question.GetAnswer();

            IMediaManager manager = context.Get<IMediaManager>();
            IGlobalSettingsRepository settingRepo = context.Get<IGlobalSettingsRepository>();

            SignatureFacility fac = new SignatureFacility(manager, settingRepo);

            builder.Append(fac.AsHtmlImage(context, answer, "MediaImage"));
        }
    }

}