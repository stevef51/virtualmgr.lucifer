﻿using System;
using System.Security.Claims;
using System.Xml;
using ConceptCave.BusinessLogic;
using ConceptCave.BusinessLogic.Configuration;
using ConceptCave.BusinessLogic.ScheduleManagement;
using ConceptCave.Checklist;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core.XmlCodable;
using ConceptCave.Repository.Injected;
using ConceptCave.Repository.PeriodicExecution;
using ConceptCave.Repository.PolicyManagement;
using ConceptCave.RepositoryInterfaces.PolicyManagement;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.RepositoryInterfaces.ScheduleManagement;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using VirtualMgr.Membership.Interfaces;
using VirtualMgr.Common;

namespace VirtualMgr.Api.Lingo
{
    public class ServiceProviderReflectionFactory : IReflectionFactory
    {
        private readonly IServiceProvider _serviceProvider;

        public ServiceProviderReflectionFactory(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public T InstantiateObject<T>()
        {
            return _serviceProvider.GetService<T>();
        }

        public object InstantiateObject(Type type)
        {
            return _serviceProvider.GetService(type);
        }
    }

    public class InjectionModule : ServiceCollection
    {
        public InjectionModule()
        {
            /*            services.AddTransient<ILanguageTranslateService>().ToMethod(ctx =>
                        {
                            var apiKey = ConfigurationManager.AppSettings["AzureTranslateApiKey"] ?? "4aa859888a9b4e47ad0ba28f04606626";
                            return new AzureTranslateServiceV3(apiKey);
                        });
            */

            this.AddScoped<IWorkingDocumentFactory, PlayerController>();


            this.AddScoped<ServerUrlParser>();
            this.AddScoped<IUrlParser, ServerUrlParser>();
            this.AddScoped<IServerUrls, ServerUrlParser>();

            this.AddTransient<IMediaExporterFactoryAsync, TarGzMedia.ExporterFactoryAsync>();
            this.AddTransient<IMediaImporterFactoryAsync, TarGzMedia.ImporterFactoryAsync>();

            this.AddSingleton<IReflectionFactory, ServiceProviderReflectionFactory>();

            this.AddSingleton<IRepositorySectionManager, RepositorySectionManager>(ctx =>
            {
                var config = new XmlDocument();
                config.Load("./ConceptCave.Repository.Resources.xml");
                return new RepositorySectionManager(config.DocumentElement, ctx.GetRequiredService<IReflectionFactory>());
            });

            this.AddScoped<IEmailConfiguration, TenantEmailConfiguration>();

            this.AddScoped<ISendGridConfiguration, SendGridConfiguration>();

            /*
                        services.AddTransient<IPowerBiConfiguration, PowerBiConfigurationAppConfig>();
                        services.AddTransient<IPowerBiManager, PowerBiManager>();

                        services.AddTransient<IRosterTaskEventsManager, RosterTaskEventsManager>();

                              .ToMethod(
                                  ctx =>


                        // Custom PEJob bindings ..
                        services.AddTransient<IPeriodicExecutionHandler, ExecutePublishedChecklistPEJob>().Named("ExecutePublishedChecklist");
                        services.AddTransient<IPeriodicExecutionHandler, eWayPaymentPeriodicExecutionHandler>().Named("eWayProcessPendingPayment");
                        services.AddTransient<Func<CustomJobPeriodicExecutionType, IPeriodicExecutionHandler>>().ToMethod(ctx => jobType => ctx.Kernel.Get<IPeriodicExecutionHandler>(jobType.ToString()));
                        services.AddTransient<Func<string, IPeriodicExecutionHandler>>().ToMethod(ctx => jobType => ctx.Kernel.Get<IPeriodicExecutionHandler>(jobType));
                        services.AddTransient<API.v1.PeriodicExecutionLogic>();

                        services.AddTransient<IMediaPreviewCreator, WordDocumentPreviewCreator>().Named("application/msword");
                        services.AddTransient<IMediaPreviewCreator, WordDocumentPreviewCreator>().Named("application/vnd.openxmlformats-officedocument.wordprocessingml.document");

                        services.AddTransient<Func<string, IMediaPreviewCreator>>().ToMethod(ctx => mime =>
                        {
                            return ctx.Kernel.TryGet<IMediaPreviewCreator>(mime.ToLower());
                        });

                        services.AddTransient<Func<bool, eWAY.Rapid.IRapidClientSync>>().ToMethod(ctx => useSandbox =>
                        {
                            var rapidEndpoint = useSandbox ? "Sandbox" : "Production";
                            var merchantConfigJson = gsr.GetSetting<string>("eWayPayment_" + rapidEndpoint);

                            var merchantConfig = JsonConvert.DeserializeObject<eWayPaymentMerchantConfig>(merchantConfigJson);

                            return new RapidClientSync(RapidClientFactory.NewRapidClient(merchantConfig.APIKey, merchantConfig.Password, rapidEndpoint));
                        });


                        services.AddTransient<IPaymentGatewayFacility, eWayPaymentFacility>().Named(eWayPaymentFacility.GATEWAY_NAME);
                        services.AddTransient<Func<string, IPaymentGatewayFacility>>().ToMethod(ctx => gatewayName => ctx.Kernel.Get<IPaymentGatewayFacility>(gatewayName));
                        services.AddTransient<eWayPaymentFacility>();
                        services.AddTransient<PendingPaymentFacility>();
                        services.AddTransient<eWayPaymentPeriodicExecutionHandler>();

                        services.AddTransient<Func<PendingPaymentFacility>>().ToMethod(ctx => () => ctx.Kernel.Get<PendingPaymentFacility>());

                        services.AddTransient<ILoggerFactory, NLog.Extensions.Logging.NLogLoggerFactory>().InSingletonScope();

                        // Note a fix for a bug in Ninject's InSingletonScope where it creates new instances on every request since its bounds to a Method??
                        INodeServices nodeServicesSingleton = null;
                        services.AddTransient<INodeServices>().ToMethod(ctx =>
                        {
                            if (nodeServicesSingleton == null)
                            {
                                var options = new NodeServicesOptions(AspNetReflectionFactory.Current)
                                {
                                    ProjectPath = HostingEnvironment.ApplicationPhysicalPath,
                                    LaunchWithDebugging = false,
                                    ApplicationStoppingToken = NinjectWebCommon.ApplicationRunning.Token
                                };
                                options.EnvironmentVariables["NODE_ENV"] = HostingEnvironment.IsDevelopmentEnvironment ? "development" : "production";
                                nodeServicesSingleton = NodeServicesFactory.CreateNodeServices(options);
                            }
                            return nodeServicesSingleton;
                        });//.InSingletonScope();

                        services.AddTransient<JsFunctionService>().InSingletonScope();
                        services.AddTransient<IJsFunctionService>().ToMethod(ctx => ctx.Kernel.Get<JsFunctionService>());

                        services.AddTransient<TokenGenerator>().ToMethod(ctx => new TokenGenerator()
                        {
                            Secret = Encoding.UTF8.GetBytes(MultiTenantManager.CurrentTenant.BaseUrl.ToString() + "279B7487-7F3D-4E77-AE5E-FD6073508E32")
                        });
                        services.AddTransient<ITokenGenerator>().ToMethod(ctx => ctx.Kernel.Get<TokenGenerator>());
                        services.AddTransient<ITokenValidator>().ToMethod(ctx => ctx.Kernel.Get<TokenGenerator>());
                        services.AddTransient<Func<ITokenValidator>>().ToMethod(ctx => () => ctx.Kernel.Get<ITokenValidator>());
            */
        }
    }
}
