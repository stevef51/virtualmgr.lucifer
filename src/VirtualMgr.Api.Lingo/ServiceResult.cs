namespace VirtualMgr.Api.Lingo
{
    public class ServiceResult
    {
        public ServiceResult(bool success, string code, string message)
        {
            this.success = success;
            this.code = code;
            this.message = message;
        }

        public ServiceResult(string code, string message) : this(false, code, message)
        {
        }

        public ServiceResult(string message) : this(message, message)
        {
        }

        public bool success { get; set; }
        public string code { get; set; }
        public string message { get; set; }
    }

    public class ServiceResult<TResult> : ServiceResult
    {
        public ServiceResult(TResult result) : base(true, null, null)
        {
            this.result = result;
        }

        public ServiceResult(string message) : base(false, message, message)
        {
        }

        public ServiceResult(string code, string message) : base(false, code, message)
        {
        }

        public TResult result { get; set; }
    }
}