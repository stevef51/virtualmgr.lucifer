﻿using System;
using System.IdentityModel.Tokens.Jwt;
using ConceptCave.BusinessLogic;
using ConceptCave.BusinessLogic.Configuration;
using ConceptCave.Checklist;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core.XmlCodable;
using Lamar;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using VirtualMgr.AspNetCore;
using VirtualMgr.Common;
using VirtualMgr.Membership.DirectCore.IdentityOverrides;
using VirtualMgr.MultiTenant;
using Serilog;
using Microsoft.IdentityModel.Logging;
using VirtualMgr.MassTransit;

namespace VirtualMgr.Api.Lingo
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureContainer(ServiceRegistry services)
        {
            services.Configure<ServiceOptions>(Configuration.GetSection("Service"));
            services.Configure<ServiceOptions>(options => options.ServiceName = options.Services.API);

            services.AddLogging();

            var tempProvider = services.BuildServiceProvider();
            var serviceOptions = tempProvider.GetRequiredService<IOptions<ServiceOptions>>().Value;

            services.AddDbContext<ApplicationDbContext>();

            /*             var encryptionSettings = new Microsoft.AspNetCore.DataProtection.AuthenticatedEncryption.ConfigurationModel.AuthenticatedEncryptorConfiguration()
                        {
                            EncryptionAlgorithm = EncryptionAlgorithm.AES_256_CBC,
                            ValidationAlgorithm = ValidationAlgorithm.HMACSHA256
                        };

                         services.AddAuthentication("ApplicationCookie")
                            .AddCookie(options =>
                            {
                                options.Cookie.Name = ".VirtualManager.Cookies";
                                options.LoginPath = "1234";
                                options.Events.OnRedirectToLogin = (context) =>
                                {
                                    context.Response.StatusCode = 402;
                                    return Task.CompletedTask;
                                };
                            });
            */

            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .AddJsonOptions(options =>
                {
                    options.SerializerSettings.Converters.Add(new Newtonsoft.Json.Converters.StringEnumConverter());
                    options.SerializerSettings.ContractResolver = null;//new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver();
                    options.SerializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;
                });


            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

            services.Configure<ForwardedHeadersOptions>(options =>
            {
                options.ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto;
            });

            // Use Redis for Distributed Cache and PubSub
            services.AddModule(new VirtualMgr.Redis.InjectionModule(serviceOptions.Services.Redis));
            services.AddModule<VirtualMgr.Membership.DirectCore.InjectionModule>();
            services.AddModule(new CommonAuthenticationInjectionModule(serviceOptions));


            services.AddVirtualMgrAppTenant<RequestHostTenantDomainResolver>(Configuration);
            services.AddModule(new VirtualMgr.MassTransit.InjectionModule(Configuration, serviceOptions.ServiceName));

            services.AddLLBLGenRuntimeConfiguration(System.Diagnostics.TraceLevel.Error);
            services.AddVirtualMgrMultiTenantLLBLGen(Configuration);

            services.AddConceptCaveRepositoryLLBLGen();
            services.AddConceptCaveBusinessLogic();
            services.AddModule<VirtualMgr.Api.Lingo.InjectionModule>();

            services.AddTransient<IPlayerStringRenderer, PlayerStringRenderer>();

            services.AddSingleton<ITimeZoneInfoResolver>(TimeZoneInfoResolver.Use(new LinuxTimeZoneInfoResolver()));

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(
            IApplicationBuilder app,
            IHostingEnvironment env,
            IOptions<ServiceOptions> serviceOptions,
            IRepositorySectionManager repositorySectionManager,
            IReflectionFactory reflectionFactory)
        {
            foreach (var nvp in Configuration.AsEnumerable())
            {
                Log.Logger.Information($"{nvp.Key}={nvp.Value}");
            }
            app.UseRequestLocalization(new RequestLocalizationOptions
            {
                DefaultRequestCulture = new RequestCulture("en")
            });

            XmlCoderTypeCache.InitGlobalTypeCache(repositorySectionManager.OverrideResolver);
            XmlElementConverter.InitConverters(reflectionFactory);

            app.UsePathBase($"/{serviceOptions.Value.ServiceName}");

            app.UseVirtualMgrAppTenant();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            IdentityModelEventSource.ShowPII = true;

            app.UseMiddleware<VirtualMgr.AspNetCore.Cors.CorsMiddleware>();

            app.UseAuthentication();
            app.UseCookiePolicy();

            app.UseMvc();
            ;/* routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller}/{action=Index}/{id?}");
            });*/
        }
    }
}
