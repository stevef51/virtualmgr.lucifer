﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.RepositoryInterfaces.Repositories;
using Microsoft.AspNetCore.NodeServices;
using VirtualMgr.Central;
using VirtualMgr.Central.Interfaces;

namespace VirtualMgr.NodeService
{

    public class JsFunctionService : IJsFunctionService
    {
        protected readonly IJSFunctionRepository _jsFuncRepo;
        private readonly INodeServices _nodeServices;
        private DateTime _nextRunInitAt = DateTime.MinValue;
        private SemaphoreSlim _runInitLock = new SemaphoreSlim(1);
        private readonly Func<ITenantContext> _fnTenantContext;

        public JsFunctionService(IJSFunctionRepository jsFuncRepo, INodeServices nodeServices, Func<ITenantContext> fnTenantContext)
        {
            _jsFuncRepo = jsFuncRepo;
            _nodeServices = nodeServices;
            _fnTenantContext = fnTenantContext;
        }


        public void Reload()
        {
            _nextRunInitAt = DateTime.MinValue;
        }

        private async Task<bool> runInitAsync()
        {
            var allJsFuncs = (from dto in _jsFuncRepo.GetAll(JSFunctionType.Coupon)
                              select new
                              {
                                  id = dto.Id,
                                  name = dto.Name,
                                  script = dto.Script,
                                  type = dto.Type
                              }).ToList();

            return await _nodeServices.InvokeExportAsync<bool>("./NodeServices/jsFunction", "init", allJsFuncs);
        }
        private bool runInit()
        {
            var allJsFuncs = (from dto in _jsFuncRepo.GetAll(JSFunctionType.Coupon)
                              select new
                              {
                                  id = dto.Id,
                                  name = dto.Name,
                                  script = dto.Script,
                                  type = dto.Type
                              }).ToList();

            return Task.Run(async () => await _nodeServices.InvokeExportAsync<bool>("./NodeServices/jsFunction", "init", allJsFuncs)).Result;
        }

        private class _ExecuteResult<TResult>
        {
            public bool? runInit { get; set; }
            public TResult result { get; set; }
        }

        private async Task checkInitAsync(bool force)
        {
            if (force || DateTime.UtcNow > _nextRunInitAt)
            {
                await _runInitLock.WaitAsync();
                try
                {
                    if (force || DateTime.UtcNow > _nextRunInitAt)
                    {
                        _nextRunInitAt = DateTime.UtcNow.AddSeconds(5 * 60);
                        await runInitAsync();
                    }
                }
                finally
                {
                    _runInitLock.Release();
                }
            }
        }

        private void checkInit(bool force)
        {
            if (force || DateTime.UtcNow > _nextRunInitAt)
            {
                _runInitLock.Wait();
                try
                {
                    if (force || DateTime.UtcNow > _nextRunInitAt)
                    {
                        _nextRunInitAt = DateTime.UtcNow.AddSeconds(5 * 60);
                        runInit();
                    }
                }
                finally
                {
                    _runInitLock.Release();
                }
            }
        }

        protected object WrapArgs(object args)
        {
            var tenant = _fnTenantContext();
            return new
            {
                args,
                context = new
                {
                    tenant = new
                    {
                        baseUrl = tenant.BaseUrl,
                        connectionString = tenant.ConnectionString,
                        hostname = tenant.HostName,
                        id = tenant.Id,
                        name = tenant.Name,
                        profile = tenant.Profile,
                        timezone = new
                        {
                            id = tenant.TimeZoneInfo.Id
                        }
                    }
                }
            };
        }

        public async Task<JsExecuteResult<TResult>> ExecuteAsync<TResult>(string jsFuncName, object args)
        {
            await checkInitAsync(false);

            var wrappedArgs = WrapArgs(args);

            var er = await _nodeServices.InvokeExportAsync<_ExecuteResult<TResult>>("./NodeServices/jsFunction", "execute", jsFuncName, wrappedArgs);
            if (er.runInit.HasValue && er.runInit.Value)
            {
                await checkInitAsync(true);
                er = await _nodeServices.InvokeExportAsync<_ExecuteResult<TResult>>("./NodeServices/jsFunction", "execute", jsFuncName, wrappedArgs);
            }
            return new JsExecuteResult<TResult>() { result = er.result };
        }

        public async Task<JsExecuteResult<TResult>> ExecuteAsync<TResult>(int id, object args)
        {
            var dto = _jsFuncRepo.GetById(id);
            if (dto == null)
                throw new NullReferenceException("JsFunc not found");
            return await ExecuteAsync<TResult>(dto.Name, args);
        }

        public JsExecuteResult<TResult> Execute<TResult>(string jsFuncName, object args)
        {
            checkInit(false);

            var wrappedArgs = WrapArgs(args);

            var er = Task.Run(async () => await _nodeServices.InvokeExportAsync<_ExecuteResult<TResult>>("./NodeServices/jsFunction", "execute", jsFuncName, wrappedArgs)).Result;
            if (er.runInit.HasValue && er.runInit.Value)
            {
                checkInit(true);
                er = Task.Run(async () => await _nodeServices.InvokeExportAsync<_ExecuteResult<TResult>>("./NodeServices/jsFunction", "execute", jsFuncName, wrappedArgs)).Result;
            }
            return new JsExecuteResult<TResult>() { result = er.result };
        }

        public JsExecuteResult<TResult> Execute<TResult>(int id, object args)
        {
            var dto = _jsFuncRepo.GetById(id);
            if (dto == null)
                throw new NullReferenceException("JsFunc not found");
            return Execute<TResult>(dto.Name, args);
        }
    }
}
