﻿using System;

namespace VirtualMgr.Membership
{
    public interface IMembership
    {
        int MinRequiredPasswordLength { get; }
        IMembershipUser CreateUser(string username, string password);
        bool DeleteUser(string username, bool deleteAllRelatedData);
        string GeneratePassword(int length, int numberOfNonAlphanumericCharacters);
        IMembershipUser GetUser(object providerUserKey);
        IMembershipUser GetUser(string username);
        IMembershipUser GetUser();
        IMembershipUser GetUser(object providerUserKey, bool userIsOnline);
        bool ValidateUser(string username, string password);
    }
}
