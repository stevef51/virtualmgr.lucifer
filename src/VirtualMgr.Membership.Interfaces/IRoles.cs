﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualMgr.Membership
{
    public interface IRoles
    {
        string ApplicationName { get; set; }
        int CookieTimeout { get; }
        bool CacheRolesInCookie { get; }
        string CookieName { get; }
        bool Enabled { get; set; }
        bool CookieSlidingExpiration { get; }
        string CookiePath { get; }
        bool CreatePersistentCookie { get; }
        string Domain { get; }
        int MaxCachedResults { get; }
        bool CookieRequireSSL { get; }

        void AddUsersToRole(string[] usernames, string roleName);
        void AddUsersToRoles(string[] usernames, string[] roleNames);
        void AddUserToRole(string username, string roleName);
        void AddUserToRoles(string username, string[] roleNames);
        void CreateRole(string roleName);
        void DeleteCookie();
        bool DeleteRole(string roleName);
        bool DeleteRole(string roleName, bool throwOnPopulatedRole);
        string[] FindUsersInRole(string roleName, string usernameToMatch);
        string[] GetAllRoles();
        string[] GetRolesForUser();
        string[] GetRolesForUser(string username);
        string[] GetUsersInRole(string roleName);
        bool IsUserInRole(string username, string roleName);
        bool IsUserInRole(string roleName);
        void RemoveUserFromRole(string username, string roleName);
        void RemoveUserFromRoles(string username, string[] roleNames);
        void RemoveUsersFromRole(string[] usernames, string roleName);
        void RemoveUsersFromRoles(string[] usernames, string[] roleNames);
        bool RoleExists(string roleName);

    }
}
