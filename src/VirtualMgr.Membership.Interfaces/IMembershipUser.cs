﻿using System;

namespace VirtualMgr.Membership
{
    public interface IMembershipUser
    {
        object ProviderUserKey { get; }
        DateTime LastPasswordChangedDate { get; }
        DateTime LastActivityDate { get; set; }
        DateTime LastLoginDate { get; set; }
        DateTime CreationDate { get; }
        DateTime LastLockoutDate { get; }
        bool IsLockedOut { get; }
        bool IsApproved { get; set; }
        string Comment { get; set; }
        string PasswordQuestion { get; }
        string ProviderName { get; }
        bool IsOnline { get; }
        string UserName { get; }
        string Email { get; set; }
        bool ChangePassword(string oldPassword, string newPassword);
        bool ChangePasswordQuestionAndAnswer(string password, string newPasswordQuestion, string newPasswordAnswer);
        string GetPassword(string passwordAnswer);
        string GetPassword();
        string ResetPassword();
        string ResetPassword(string passwordAnswer);
        bool UnlockUser();
    }
}
