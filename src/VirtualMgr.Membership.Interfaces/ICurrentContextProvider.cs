﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualMgr.Membership.Interfaces
{
    public interface ICurrentContextProvider
    {
        object ProviderUserKey { get; }
        Guid InternalUserId { get; }
    }
}
