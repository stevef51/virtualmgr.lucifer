﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.DTO.DTOClasses
{
    public static class ProjectJobTaskDTOExtensions
    {
        /// <summary>
        /// Takes the MinimumDurationExpression and calculates the MinimumDuration based on the ExpectedDuration
        /// </summary>
        public static void CalculateMinimumDuration(this ProjectJobTaskDTO self)
        {
            if (string.IsNullOrEmpty(self.MinimumDurationExpression) == true)
            {
                self.MinimumDuration = null;
                return;
            }

            decimal val = 0;
            // so the MinimumDurationExpression is one of two formats XX% or XX where XX is in seconds
            if (self.MinimumDurationExpression.IndexOf("%") == -1)
            {

                if (decimal.TryParse(self.MinimumDurationExpression, out val) == true)
                {
                    self.MinimumDuration = val;
                }
            }
            else
            {
                if (self.EstimatedDurationSeconds.HasValue == false)
                {
                    self.MinimumDuration = null;
                    return;
                }

                if (decimal.TryParse(self.MinimumDurationExpression.Replace("%", ""), out val) == true)
                {
                    self.MinimumDuration = self.EstimatedDurationSeconds.Value * (val / 100);
                }
            }
        }
    }
}
