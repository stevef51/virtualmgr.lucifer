﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VirtualMgr.Common;

namespace ConceptCave.DTO.DTOClasses
{
    public static class UserDataDTOExtensions
    {
        public static TimeZoneInfo GetTimeZoneInfo(this UserDataDTO self)
        {
            return TimeZoneInfoResolver.ResolveWindows(self.TimeZone);
        }

        public static DateTime ConvertDateTimeToTimeZone(this UserDataDTO self, DateTime dateToConvert)
        {
            DateTime date = dateToConvert;
            if (date.Kind != DateTimeKind.Utc)
            {
                date = DateTime.SpecifyKind(date, DateTimeKind.Utc);
            }

            return TimeZoneInfo.ConvertTimeFromUtc(date, self.GetTimeZoneInfo());
        }
    }
}
