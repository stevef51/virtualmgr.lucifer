﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VirtualMgr.Common;

namespace ConceptCave.DTO.DTOClasses
{
    public static class CompletedUserDimensionDTOExtensions
    {
        public static TimeZoneInfo GetTimeZoneInfo(this CompletedUserDimensionDTO self)
        {
            return TimeZoneInfoResolver.ResolveWindows(self.TimeZone);
        }

        public static DateTime ConvertDateTimeToTimeZone(this CompletedUserDimensionDTO self, DateTime dateToConvert)
        {
            DateTime date = dateToConvert;
            if (date.Kind != DateTimeKind.Utc)
            {
                date = DateTime.SpecifyKind(date, DateTimeKind.Utc);
            }

            return TimeZoneInfo.ConvertTimeFromUtc(date, self.GetTimeZoneInfo());
        }
    }
}
