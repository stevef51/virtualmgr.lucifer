﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConceptCave.DTO.DTOClasses
{
    public static class WorkingDocumentDTOExtensions
    {
        public static DateTime DateCreatedInReviewerTimezone(this WorkingDocumentDTO self)
        {
            if (self.Reviewer == null)
            {
                return self.DateCreated;
            }

            DateTime date = DateTime.SpecifyKind(self.DateCreated, DateTimeKind.Utc);

            return TimeZoneInfo.ConvertTimeFromUtc(date, self.Reviewer.GetTimeZoneInfo());
        }

        public static DateTime DateCreatedInRevieweeTimezone(this WorkingDocumentDTO self)
        {
            if (self.Reviewee == null)
            {
                return self.DateCreated;
            }

            DateTime date = DateTime.SpecifyKind(self.DateCreated, DateTimeKind.Utc);

            return TimeZoneInfo.ConvertTimeFromUtc(date, self.Reviewee.GetTimeZoneInfo());
        }
    }
}
