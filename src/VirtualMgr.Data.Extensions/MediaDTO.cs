﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConceptCave.DTO.DTOClasses
{
    public static class MediaDTOExtensions
    {
        public static string GetExtension(this MediaDTO self)
        {
            return System.IO.Path.GetExtension(self.Filename);
        }
    }
}
