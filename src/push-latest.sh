#!/bin/bash
cd VirtualMgr.Api.Lingo && ./push.sh && cd .. && \
    cd VirtualMgr.Identity.Server && ./push.sh && cd .. && \
    cd VirtualMgr.Media && ./push.sh && cd .. && \
    cd VirtualMgr.ODatav4 && ./push.sh && cd .. && \
    cd VirtualMgr.SPA && ./push.sh && cd .. && \
    cd VirtualMgr.Tenant && ./push.sh && cd .. && \
    cd VirtualMgr.GrandMaster.api && ./push.sh && cd .. && \
    cd VirtualMgr.GrandMaster.spa && ./push.sh && cd ..
