﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Xml;

namespace ConceptCave.Repository
{
    public class PublishingSection : IConfigurationSectionHandler
    {
        public object Create(object parent, object configContext, System.Xml.XmlNode section)
        {
            XmlNodeList nodes = section.SelectNodes("//add");

            PublishingSectionManager manager = new PublishingSectionManager();

            foreach (XmlElement element in nodes)
            {
                PublishingSectionManagerItem item = new PublishingSectionManagerItem(element);
                manager.Items.Add(item);
            }

            return manager;
        }
    }

    public class PublishingSectionManager
    {
        public List<PublishingSectionManagerItem> Items { get; protected set; }

        public PublishingSectionManager()
        {
            Items = new List<PublishingSectionManagerItem>();
        }

        public PublishingSectionManagerItem ItemForAssemblyQualifiedName(string name)
        {
            return (from i in Items where i.Type == name select i).First();
        }

        public PublishingSectionManagerItem ItemForObjectType(Type objectType)
        {
            return ItemForAssemblyQualifiedName(objectType.AssemblyQualifiedName);
        }

        public PublishingSectionManagerItem ItemForObject(object o)
        {
            return ItemForObjectType(o.GetType());
        }

        public static PublishingSectionManager Current
        {
            get
            {
                return (PublishingSectionManager)System.Configuration.ConfigurationManager.GetSection("conceptcave.repository.publishing");
            }
        }
    }

    public class PublishingSectionManagerItem
    {
        public PublishingSectionManagerItem(XmlElement element)
        {
            Type = element.GetAttribute("type");
        }

        public string Type { get; set; }

        public IPublishingHandler CreateType()
        {
            return (IPublishingHandler)System.Type.GetType(this.Type).GetConstructor(System.Type.EmptyTypes).Invoke(null);
        }
    }
}
