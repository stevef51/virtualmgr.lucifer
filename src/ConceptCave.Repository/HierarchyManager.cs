﻿/* TODO Removed until WWW Admin is rewritten 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.Data.Linq;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SD.LLBLGen.Pro.LinqSupportClasses;
using ConceptCave.Data.HelperClasses;
using System.Transactions;
using ConceptCave.Data;
using VirtualMgr.Central;

namespace ConceptCave.Repository
{
    public static class HierarchyManager
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;

        //Methods used to get/save roles
        public static EntityCollection<HierarchyRoleEntity> GetAllRoles()
        {
            using (var adapter = _fnAdapter())
            {
                var collection = new EntityCollection<HierarchyRoleEntity>();
                adapter.FetchEntityCollection(collection, null);
                return collection;
            }
        }

        public static HierarchyRoleEntity GetRoleById(int id)
        {
            using (var adapter = _fnAdapter())
            {
                var ent = new HierarchyRoleEntity(id);
                var path = new PrefetchPath2(EntityType.HierarchyRoleEntity);
                path.Add(HierarchyRoleEntity.PrefetchPathHierarchyRoleUserTypes);
                adapter.FetchEntity(ent, path);
                return ent.IsNew ? null : ent;
            }
        }

        public static EntityCollection<HierarchyRoleEntity> GetRoleByName(string name)
        {
            using (var adapter = _fnAdapter())
            {
                var data = new LinqMetaData(adapter);
                var result = (from r in data.HierarchyRole
                              where r.Name == name
                              select r)
                             .WithPath(p => p.Prefetch(r => r.HierarchyRoleUserTypes));
                return ((ILLBLGenProQuery)result).Execute<EntityCollection<HierarchyRoleEntity>>();

            }
        }

        public static void SaveRole(HierarchyRoleEntity role)
        {
            SaveRole(role, false, false);
        }

        public static void SaveRole(HierarchyRoleEntity role, bool refetch, bool recurse)
        {
            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(role, refetch, recurse);
            }
        }

        public static void DeleteRole(int roleId)
        {
            using (var trans = new TransactionScope())
            {
                using (var adapter = _fnAdapter())
                {
                    DeleteAllUserTypesForRole(roleId);
                    var predicate = new PredicateExpression();
                    predicate.Add(HierarchyRoleFields.Id == roleId);
                    var bucket = new RelationPredicateBucket(predicate);
                    adapter.DeleteEntitiesDirectly(typeof(HierarchyRoleEntity), bucket);
                }
                trans.Complete();
            }
        }

        public static void AddRoleUserType(int roleId, int userTypeId)
        {
            //First delete it if it exists, to save race condition
            using (var trans = new TransactionScope())
            {
                using (var adapter = _fnAdapter())
                {
                    RemoveRoleUserType(roleId, userTypeId);
                    var newEntry = new HierarchyRoleUserTypeEntity();
                    newEntry.RoleId = roleId;
                    newEntry.UserTypeId = userTypeId;
                    adapter.SaveEntity(newEntry, false, false);
                }
                trans.Complete();
            }
        }

        public static void RemoveRoleUserType(int roleId, int userTypeId)
        {
            using (var adapter = _fnAdapter())
            {
                var predicate = new PredicateExpression();
                predicate.Add(HierarchyRoleUserTypeFields.RoleId == roleId &
                    HierarchyRoleUserTypeFields.UserTypeId == userTypeId);
                adapter.DeleteEntitiesDirectly(typeof(HierarchyRoleUserTypeEntity),
                    new RelationPredicateBucket(predicate));
            }
        }

        public static void DeleteAllUserTypesForRole(int roleId)
        {
            using (var adapter = _fnAdapter())
            {
                var predicate = new PredicateExpression();
                predicate.Add(HierarchyRoleUserTypeFields.RoleId == roleId);
                adapter.DeleteEntitiesDirectly(typeof(HierarchyRoleUserTypeEntity),
                    new RelationPredicateBucket(predicate));
            }
        }

        public static void SetUserTypesForRole(int roleId, IEnumerable<int> userTypeIds)
        {
            using (var trans = new TransactionScope())
            {
                using (var adapter = _fnAdapter())
                {
                    var entsToSave = new EntityCollection<HierarchyRoleUserTypeEntity>();
                    DeleteAllUserTypesForRole(roleId);
                    foreach (var userTypeId in userTypeIds)
                    {
                        entsToSave.Add(new HierarchyRoleUserTypeEntity()
                        {
                            RoleId = roleId,
                            UserTypeId = userTypeId
                        });
                    }
                    adapter.SaveEntityCollection(entsToSave);
                }
                trans.Complete();
            }
        }

        //Methods used in the hierarchy designer- they load/save the entire hierarchy
        public static HierarchyBucketEntity LoadInMemory(int id)
        {
            //Load every record from the bucket table in memory
            //then load up the relations manually
            var allBuckets = new EntityCollection<HierarchyBucketEntity>();
            using (var adapter = _fnAdapter())
            {
                //But we do need to do a bit of prefetching accross tables
                var prefetch = new PrefetchPath2(EntityType.HierarchyBucketEntity);
                prefetch.Add(HierarchyBucketEntity.PrefetchPathHierarchyRole);
                prefetch.Add(HierarchyBucketEntity.PrefetchPathUserData);

                adapter.FetchEntityCollection(allBuckets, new RelationPredicateBucket(new PredicateExpression(HierarchyBucketFields.HierarchyId == id)), prefetch);
            }
            HierarchyBucketEntity root = null;
            var entDict = new Dictionary<int, HierarchyBucketEntity>();
            foreach (var bucket in allBuckets)
            {
                entDict.Add(bucket.Id, bucket);
            }
            foreach (var bucket in allBuckets)
            {
                if (bucket.ParentId == null)
                {
                    root = bucket;
                }
                else
                {
                    bucket.Parent = entDict[bucket.ParentId.Value];
                    //The following line appears to be unnescessary, and causes bucket to be added ot its parents children twice
                    //llblgen automagically manages bucket.parent filling the appropriate children in the opposite direction
                    //bucket.Parent.Children.Add(bucket);
                }

            }
            return root;
        }

        public static void SetTreeNumbering(HierarchyBucketEntity root)
        {
            //This method traverses around the edges of the tree specified by root and numbers the left and right indexes
            int count = 1;
            Action<HierarchyBucketEntity> recurser = null;
            recurser = (node) =>
            {
                //number left first
                node.LeftIndex = count++;
                //number children
                if (node.Children != null)
                {
                    //assumption: foreach traverses in the right order
                    foreach (var child in node.Children)
                        recurser(child);
                }
                //then number right
                node.RightIndex = count++;
            };
            recurser(root);
        }

        public static IList<HierarchyBucketEntity> GetBucketsForHierarchy(int hierarchyId, bool loadOverrides)
        {
            PredicateExpression expression = new PredicateExpression(HierarchyBucketFields.HierarchyId == hierarchyId);

            PrefetchPath2 path = new PrefetchPath2((int)EntityType.HierarchyBucketEntity);
            if (loadOverrides == true)
            {
                path.Add(HierarchyBucketEntity.PrefetchPathHierarchyBucketOverrides);
            }

            EntityCollection<HierarchyBucketEntity> result = new EntityCollection<HierarchyBucketEntity>();

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), path);
            }

            return result.ToList();
        }

        public static void DumpAndSave(HierarchyBucketEntity newRoot)
        {
            var existing = GetBucketsForHierarchy(newRoot.HierarchyId, true);

            using (var scope = new TransactionScope())
            {
                using (var adapter = _fnAdapter())
                {
                    //now recursively save the root
                    List<int> ids = new List<int>();
                    Action<HierarchyBucketEntity> buildIds = null;
                    buildIds = (ent) =>
                    {
                        if (ent.IsNew == false)
                        {
                            ids.Add(ent.Id);
                        }

                        foreach (var c in ent.Children)
                            buildIds(c);
                    };

                    buildIds(newRoot);

                    var entsToSave = new EntityCollection<HierarchyBucketEntity>();
                    var overridesToDelete = new EntityCollection<HierarchyBucketOverrideEntity>();

                    var deleted = (from e in existing where ids.Contains(e.Id) == false orderby e.LeftIndex descending select e);

                    if (deleted.Count() > 0)
                    {
                        overridesToDelete.AddRange(from d in deleted from o in d.HierarchyBucketOverrides select o);
                    }

                    Action<HierarchyBucketEntity> addEntityToList = null;
                    addEntityToList = (ent) =>
                    {
                        entsToSave.Add(ent);

                        foreach (var c in ent.Children)
                            addEntityToList(c);
                    };
                    addEntityToList(newRoot);

                    adapter.SaveEntityCollection(entsToSave);

                    adapter.DeleteEntityCollection(overridesToDelete);

                    // we need to null out any tasks that are tied to any of the buckets being deleted
                    PredicateExpression toUpdateExpression = new PredicateExpression(ProjectJobTaskFields.HierarchyBucketId == (from d in deleted select d.Id).ToArray());
                    EntityCollection<ProjectJobTaskEntity> toUpdate = new EntityCollection<ProjectJobTaskEntity>();

                    adapter.FetchEntityCollection(toUpdate, new RelationPredicateBucket(toUpdateExpression));
                    foreach (var task in toUpdate)
                    {
                        task.HierarchyBucketId = null;
                    }
                    adapter.SaveEntityCollection(toUpdate);

                    foreach (var d in deleted)
                    {
                        PredicateExpression expression = new PredicateExpression(HierarchyBucketFields.Id == d.Id);
                        adapter.DeleteEntitiesDirectly("HierarchyBucketEntity", new RelationPredicateBucket(expression));
                    }
                }

                scope.Complete();
            }
        }


        //Methods used elsewhere for querying the hierarchy- these methods are in general (hopefully) much faster than the above methods

        //Get all of the buckets (people and not-people, possible duplicate Ids) directly or indirectly below this bucket
        public static EntityCollection<HierarchyBucketEntity> GetAllIndirectDescendantEntities(int parentBucketId)
        {
            //get the parent bucket...
            using (var adapter = _fnAdapter())
            {
                var data = new LinqMetaData(adapter);

                var parentBucket = (from h in data.HierarchyBucket
                                    where h.Id == parentBucketId
                                    select h).FirstOrDefault();
                if (parentBucket == null)
                    return null;
                var allDescendants = from h in data.HierarchyBucket
                                     where h.LeftIndex > parentBucket.LeftIndex &&
                                        h.RightIndex < parentBucket.RightIndex
                                     select h;
                return ((ILLBLGenProQuery)allDescendants).Execute<EntityCollection<HierarchyBucketEntity>>();
            }
        }

        //Get all of the buckets (people and not-people, possible duplicate Ids) directly below this bucket
        public static EntityCollection<HierarchyBucketEntity> GetAllDirectDescendantEntities(int parentBucketId)
        {
            using (var adapter = _fnAdapter())
            {
                var data = new LinqMetaData(adapter);
                var allDescendants = from h in data.HierarchyBucket
                                     where h.ParentId == parentBucketId
                                     select h;
                return ((ILLBLGenProQuery)allDescendants).Execute<EntityCollection<HierarchyBucketEntity>>();
            }
        }

        //Get the bucket directly above this bucket (people and not-people, could be the same user Id reporting to himself)
        public static HierarchyBucketEntity GetParentEntity(int childBucketId)
        {
            using (var adapter = _fnAdapter())
            {
                var data = new LinqMetaData(adapter);
                var thisBucket = (from h in data.HierarchyBucket
                                  where h.Id == childBucketId
                                  select h).FirstOrDefault();
                if (thisBucket == null)
                    return null;
                var parentBucket = (from h in data.HierarchyBucket
                                    where h.Id == thisBucket.ParentId
                                    select h).FirstOrDefault();
                return parentBucket;
            }
        }

        //Get all user entities that sit below this bucket in the hierarchy- distinct from GetAllIndirectDescendantEntities in that 
        //users are being returned and hence no duplicates
        public static EntityCollection<UserDataEntity> GetAllIndirectDescendantUsers(int parentBucketId)
        {
            using (var adapter = _fnAdapter())
            {
                var data = new LinqMetaData(adapter);

                var parentBucket = (from h in data.HierarchyBucket
                                    where h.Id == parentBucketId
                                    select h).FirstOrDefault();
                if (parentBucket == null)
                    return null;

                //Get all of the user entities
                var userEnts = (from h in data.HierarchyBucket
                                where h.LeftIndex > parentBucket.LeftIndex &&
                                    h.RightIndex < parentBucket.RightIndex &&
                                    h.UserId != null
                                join u in data.UserData on h.UserId equals u.UserId
                                select u).Distinct(); //entities with the same userId are otherwise identical too
                return ((ILLBLGenProQuery)userEnts).Execute<EntityCollection<UserDataEntity>>();

            }
        }

        //Get all user entities that sit directly below this bucket in the hierarchy- distinct from GetAllIndirectDescendantEntities in that 
        //users are being returned and hence no duplicates
        public static EntityCollection<UserDataEntity> GetAllDirectDescendantUsers(int parentBucketId)
        {
            using (var adapter = _fnAdapter())
            {
                var data = new LinqMetaData(adapter);

                //Get all of the user entities
                var userEnts = (from h in data.HierarchyBucket
                                where h.ParentId == parentBucketId &&
                                    h.UserId != null
                                join u in data.UserData on h.UserId equals u.UserId
                                select u).Distinct(); //entities with the same userId are otherwise identical too
                return ((ILLBLGenProQuery)userEnts).Execute<EntityCollection<UserDataEntity>>();

            }
        }

        //Get all user entites who'se PRIMARY (or only) bucket sit directly below this bucket
        //primary responsibility only => no dupes
        public static EntityCollection<UserDataEntity> GetAllDirectPrimaryDescendantUsers(int parentBucketId)
        {
            using (var adapter = _fnAdapter())
            {
                var data = new LinqMetaData(adapter);

                //Get all of the user entities
                var userEnts = (from h in data.HierarchyBucket
                                where h.ParentId == parentBucketId &&
                                    h.UserId != null &&
                                    h.IsPrimary.Value
                                join u in data.UserData on h.UserId equals u.UserId
                                select u).Distinct(); //entities with the same userId are otherwise identical too
                return ((ILLBLGenProQuery)userEnts).Execute<EntityCollection<UserDataEntity>>();

            }
        }

        //Equivalent to GetAllDirectPrimaryDescendantEntities(), except also "punches through" roles that are marked as not being a person
        //i.e. if A reports to B who reports to C, and B is a "site" or "department" etc, then GetAllDirectPrimaryDescenantEntitiesPunchThrough(C) will include A.
        //Also punches through empty roles
        public static EntityCollection<UserDataEntity> GetAllDirectPrimaryDescendantUsersPunchThrough(int parentBucketId)
        {
            using (var adapter = _fnAdapter())
            {
                var data = new LinqMetaData(adapter);


                Func<int, ISet<Guid>> PunchRecurser = null;
                PunchRecurser = (bucketId) =>
                {
                    var uids = new HashSet<Guid>();
                    //find this guy's children
                    var directReports = (from h in data.HierarchyBucket
                                         where h.ParentId == bucketId
                                         select h)
                                            .WithPath(p => p.Prefetch<HierarchyRoleEntity>(h => h.HierarchyRole));
                    foreach (var bucket in directReports)
                    {
                        //If there is a user in this bucket, and it's a person,
                        //add it to the hashset. Otherwise, punch through and recurse them
                        if (bucket.HierarchyRole.IsPerson && bucket.UserId.HasValue)
                            uids.Add(bucket.UserId.Value);
                        else
                            uids.UnionWith(PunchRecurser(bucket.Id));
                    }
                    return uids;
                };

                var uidList = PunchRecurser(parentBucketId);
                //Now we have a listof all uid's that satisfy the query. Just get entities and return
                var predicate = new PredicateExpression(UserDataFields.UserId == uidList);
                var retCollection = new EntityCollection<UserDataEntity>();
                adapter.FetchEntityCollection(retCollection, new RelationPredicateBucket(predicate));
                return retCollection;
            }
        }

        public static HierarchyBucketEntity GetById(int id)
        {
            HierarchyBucketEntity result = new HierarchyBucketEntity(id);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntity(result);
            }

            return result.IsNew == true ? null : result;
        }

        //Gets the hierarchy bucket that is marked as primary for this user.
        public static HierarchyBucketEntity GetPrimaryEntity(Guid userId)
        {
            using (var adapter = _fnAdapter())
            {
                var prefetch = new PrefetchPath2(EntityType.HierarchyBucketEntity);
                prefetch.Add(HierarchyBucketEntity.PrefetchPathParent)
                    .SubPath.Add(HierarchyBucketEntity.PrefetchPathHierarchyRole);
                prefetch.Add(HierarchyBucketEntity.PrefetchPathHierarchyRole);

                var data = new LinqMetaData(adapter);
                var entity = (from h in data.HierarchyBucket
                              where h.UserId.HasValue && h.UserId == userId && h.IsPrimary.Value
                              select h)
                                .WithPath(prefetch)
                                .SingleOrDefault();
                return entity;
            }
        }

        //Gets the hierarchy bucket that is marked as primary for this user, punching through non-people as in GetAllDirectPrimaryDescendantEntitiesPunchThrough()
        public static HierarchyBucketEntity GetPrimaryParentEntityPunchThrough(Guid userId)
        {
            using (var adapter = _fnAdapter())
            {
                var data = new LinqMetaData(adapter);

                //Get this guy's primary entity
                var primary = GetPrimaryEntity(userId);
                //now climb the tree until parent is a person
                var parent = primary.Parent;
                try
                {
                    var prefetch = new PrefetchPath2(EntityType.HierarchyBucketEntity);
                    prefetch.Add(HierarchyBucketEntity.PrefetchPathHierarchyRole);

                    while (parent != null && !(parent.HierarchyRole.IsPerson && parent.UserId.HasValue))
                    {
                        //climb the tree
                        var parent1 = parent;
                        parent = (from h in data.HierarchyBucket
                                  where h.Id == parent1.ParentId
                                  select h).WithPath(prefetch).Single();
                    }
                    return parent;
                }
                catch (InvalidOperationException)
                {
                    //This means Single() has thrown;
                    //We reached the top of the heirarchy. This user has no parent to report to
                    return null;
                }

            }
        }
    }
}
*/