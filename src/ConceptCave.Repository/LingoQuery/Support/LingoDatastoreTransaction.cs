﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Data;
using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.Data.Linq;

namespace ConceptCave.Repository.LingoQuery.Support
{
    public class LingoDatastoreTransaction : ILingoDatastoreTransaction
    {
        public delegate IQueryable QueryableFetchExpr(LinqMetaData data);

        private readonly Type _entityType;
        private readonly SD.LLBLGen.Pro.ORMSupportClasses.IDataAccessAdapter _adapter;
        private readonly LinqMetaData _data;
        private readonly QueryableFetchExpr _fetchExpr = null;

        public LingoDatastoreTransaction(DataAccessAdapter adapter, Type entityType)
        {
            _entityType = entityType;
            _adapter = adapter;
            _data = new LinqMetaData(_adapter);
        }

        public LingoDatastoreTransaction(DataAccessAdapter adapter, Type entityType, QueryableFetchExpr fetchExpr) : this(adapter, entityType)
        {
            _fetchExpr = fetchExpr;
        }


        public virtual IQueryable Queryable
        {
            get
            {
                if (_fetchExpr == null)
                    return _data.GetQueryableForEntity((int)Enum.Parse(typeof(EntityType), _entityType.Name))
                           as IQueryable;
                else
                    return _fetchExpr(_data);
            }
        }

        public virtual IQueryable ExecuteQuery(Expression queryExpr, IQueryable queryable)
        {
            //we jut execute the query
            //an overriding method might attach a prefetch path before calling base
            var query = queryable.Provider.CreateQuery(queryExpr);
            var list = new List<object>();
            //do something to cause evaluation so that we have an easy place to debug from
            foreach (var q in query)
            {
                list.Add(q);
            }
            return list.AsQueryable();
        }

        public virtual void Dispose()
        {
            _adapter.Dispose();
        }
    }
}
