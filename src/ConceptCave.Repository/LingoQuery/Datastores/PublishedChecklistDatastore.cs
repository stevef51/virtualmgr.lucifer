﻿using System;
using System.Linq;
using System.Linq.Expressions;
using ConceptCave.Checklist.Reporting.LingoQuery;
using ConceptCave.Core;
using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.Linq;

namespace ConceptCave.Repository.LingoQuery.Datastores
{
    public class PublishedChecklistDatastore : DatastoreBase
    {
        public PublishedChecklistDatastore(Func<DataAccessAdapter> fnAdapter) : base(fnAdapter)
        {
            var resourceIdInfo = EntityType.GetProperty("ResourceId");
            var publishingGroupIdInfo = EntityType.GetProperty("PublishingGroupId");
            var nameInfo = EntityType.GetProperty("Name");
            var descriptionInfo = EntityType.GetProperty("Description");
            var pictureIdInfo = EntityType.GetProperty("PictureId");

            var publishMemberInfo = EntityType.GetProperty("PublishingGroup");
            Func<ParameterExpression, Expression> publishExpr = (pe) =>
                Expression.MakeMemberAccess(pe, publishMemberInfo);
            var publishNameMemberInfo = typeof(PublishingGroupEntity).GetProperty("Name");

            _columns.Add("ResourceId", new LingoDatabaseColumn()
            {
                Name = "ResourceId",
                LinqReferenceGenerator = (pe) => Expression.MakeMemberAccess(pe, resourceIdInfo)
            });
            _columns.Add("PublishingGroupId", new LingoDatabaseColumn()
            {
                Name = "PublishingGroupId",
                LinqReferenceGenerator = (pe) => Expression.MakeMemberAccess(pe, publishingGroupIdInfo)
            });
            _columns.Add("Name", new LingoDatabaseColumn()
            {
                Name = "Name",
                LinqReferenceGenerator = (pe) => Expression.MakeMemberAccess(pe, nameInfo)
            });
            _columns.Add("Description", new LingoDatabaseColumn()
            {
                Name = "Description",
                LinqReferenceGenerator = (pe) => Expression.MakeMemberAccess(pe, descriptionInfo)
            });
            _columns.Add("PictureId", new LingoDatabaseColumn()
            {
                Name = "PictureId",
                LinqReferenceGenerator = (pe) => Expression.MakeMemberAccess(pe, pictureIdInfo)
            });

            _columns.Add("PublishingGroup", new LingoDatabaseColumn()
            {
                Name = "PublishingGroup",
                LinqReferenceGenerator = (pe) => Expression.MakeMemberAccess(publishExpr(pe), publishNameMemberInfo)
            });
        }

        public override Type EntityType
        {
            get { return typeof(PublishingGroupResourceEntity); }
        }
    }
}
