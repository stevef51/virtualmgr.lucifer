﻿using System;
using System.Linq;
using System.Linq.Expressions;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.Reporting.LingoQuery;
using ConceptCave.Core;
using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Repository.LingoQuery.Support;

namespace ConceptCave.Repository.LingoQuery.Datastores
{
    public class QuestionDatastore : DatastoreBase
    {
        public QuestionDatastore(Func<DataAccessAdapter> fnAdapter) : base(fnAdapter)
        {
            var dateStartedInfo = EntityType.GetProperty("DateStarted");
            var dateCompletedInfo = EntityType.GetProperty("DateCompleted");
            var scoreInfo = EntityType.GetProperty("Score");
            var possibleScoreInfo = EntityType.GetProperty("PossibleScore");
            var passFailInfo = EntityType.GetProperty("PassFail");
            var promptInfo = EntityType.GetProperty("Prompt");
            var presentedOrderInfo = EntityType.GetProperty("PresentedOrder");
            var workingDocumentIdInfo = EntityType.GetProperty("WorkingDocumentId");

            var userNameMemberInfo = typeof(CompletedUserDimensionEntity).GetProperty("Name");
            var userIdMemberInfo = typeof(CompletedUserDimensionEntity).GetProperty("Id");

            var reportingWDInfo = EntityType.GetProperty("CompletedWorkingDocumentDimension");
            Func<ParameterExpression, Expression> reportingWDLambda = (pe) =>
                Expression.MakeMemberAccess(pe, reportingWDInfo);
            var reportingWDNameInfo = typeof(CompletedWorkingDocumentDimensionEntity).GetProperty("Name");

            var reviewerMemberInfo = EntityType.GetProperty("CompletedUserDimensionReviewer");
            Func<ParameterExpression, Expression> reviewerExpr = (pe) =>
                Expression.MakeMemberAccess(pe, reviewerMemberInfo);

            var revieweeMemberInfo = EntityType.GetProperty("CompletedUserDimensionReviewee");
            Func<ParameterExpression, Expression> revieweeExpr = (pe) =>
                Expression.MakeMemberAccess(pe, revieweeMemberInfo);

            var valueInfo = EntityType.GetProperty("CompletedPresentedFactValues");
            Func<ParameterExpression, Expression> valueLambda = (pe) =>
                Expression.Call(typeof(Enumerable), "First", new Type[] { typeof(CompletedPresentedFactValueEntity) },
                    Expression.MakeMemberAccess(pe, valueInfo));
            var valueAsStringInfo = typeof(CompletedPresentedFactValueEntity).GetProperty("Value");

            var uploadMediaInfo = EntityType.GetProperty("CompletedPresentedUploadMediaFactValues");
            var uploadMediaSelectParameter = Expression.Parameter(
                typeof(CompletedPresentedUploadMediaFactValueEntity), "ume");
            var uploadMediaIdInfo = typeof(CompletedPresentedUploadMediaFactValueEntity).GetProperty("MediaId");
            var uploadMediaSelectExpr = Expression.MakeMemberAccess(uploadMediaSelectParameter, uploadMediaIdInfo);
            var uploadMediaSelectLambda = Expression.Lambda(uploadMediaSelectExpr, uploadMediaSelectParameter);
            var uploadMediaExpr = new Func<ParameterExpression, Expression>((pe) =>
                Expression.Call(typeof(Enumerable), "Select", new Type[]
                {
                    typeof (CompletedPresentedUploadMediaFactValueEntity),
                    typeof (Guid)
                }, Expression.MakeMemberAccess(pe, uploadMediaInfo), uploadMediaSelectLambda));

            var QTypeInfo = EntityType.GetProperty("CompletedPresentedTypeDimension");
            Func<ParameterExpression, Expression> QTypeLambda = (pe) =>
                Expression.MakeMemberAccess(pe, QTypeInfo);
            var QTypeNameInfo = typeof(CompletedPresentedTypeDimensionEntity).GetProperty("Type");

            var labelJoinDimensionInfo = EntityType.GetProperty("CompletedLabelWorkingDocumentPresentedDimensions");
            var labelDimensionInfo = typeof(CompletedLabelWorkingDocumentPresentedDimensionEntity).GetProperty("CompletedLabelDimension");
            var labelNameInfo = typeof(CompletedLabelDimensionEntity).GetProperty("Name");
            //entity.CompletedLabelWorkingDocumentPresentedDimensions.Select(e => e.CompletedLabelDimension.Name)
            var labelSelectParameter = Expression.Parameter(typeof(CompletedLabelWorkingDocumentPresentedDimensionEntity), "jt");
            var labelSelectMExpr = Expression.MakeMemberAccess(Expression.MakeMemberAccess(labelSelectParameter,
                                        labelDimensionInfo), labelNameInfo);
            var labelSelectMLambda = Expression.Lambda(labelSelectMExpr, labelSelectParameter);
            //entity collection is IEnumerable but not IQueryable.
            Func<ParameterExpression, Expression> labelDimensionExpr = (pe) =>
                Expression.Call(typeof(Enumerable), "Select", new Type[] {
                                                            typeof(CompletedLabelWorkingDocumentPresentedDimensionEntity),
                                                            typeof(string)},
                                Expression.MakeMemberAccess(pe, labelJoinDimensionInfo),
                                labelSelectMLambda);
            var internalIdInfo = EntityType.GetProperty("InternalId");
            var workingDocIdInfo = EntityType.GetProperty("WorkingDocumentId");

            var reviewerInfo = EntityType.GetProperty("CompletedUserDimensionReviewer");
            var revieweeInfo = EntityType.GetProperty("CompletedUserDimensionReviewer");
            var reviewerIdInfo = EntityType.GetProperty("ReportingUserReviewerId");
            var revieweeIdInfo = EntityType.GetProperty("ReportingUserRevieweeId");
            var userNameInfo = typeof(CompletedUserDimensionEntity).GetProperty("Name");
            var userUsernameInfo = typeof(CompletedUserDimensionEntity).GetProperty("Username");
            Func<ParameterExpression, Expression> reviewerLambda = (pe) =>
                                                                   Expression.MakeMemberAccess(pe, reviewerIdInfo);
            Func<ParameterExpression, Expression> revieweeLambda =
                (pe) => Expression.MakeMemberAccess(pe, revieweeIdInfo);

            _columns.Add("DateStarted", new LingoDatabaseColumn()
            {
                Name = "DateStarted",
                LinqReferenceGenerator = (pe) => Expression.MakeMemberAccess(pe, dateStartedInfo)
            });
            _columns.Add("DateCompleted", new LingoDatabaseColumn()
            {
                Name = "DateCompleted",
                LinqReferenceGenerator = (pe) => Expression.MakeMemberAccess(pe, dateCompletedInfo)
            });
            _columns.Add("Score", new LingoDatabaseColumn()
            {
                Name = "Score",
                LinqReferenceGenerator = (pe) => Expression.MakeMemberAccess(pe, scoreInfo)
            });
            _columns.Add("PossibleScore", new LingoDatabaseColumn()
            {
                Name = "PossibleScore",
                LinqReferenceGenerator = (pe) => Expression.MakeMemberAccess(pe, possibleScoreInfo)
            });
            _columns.Add("PassFail", new LingoDatabaseColumn()
            {
                Name = "PassFail",
                LinqReferenceGenerator = (pe) => Expression.MakeMemberAccess(pe, passFailInfo)
            });
            _columns.Add("Value", new LingoDatabaseColumn()
            {
                Name = "Value",
                LinqReferenceGenerator = (pe) => Expression.MakeMemberAccess(valueLambda(pe), valueAsStringInfo)
            });
            _columns.Add("ChecklistName", new LingoDatabaseColumn()
            {
                Name = "ChecklistName",
                LinqReferenceGenerator = (pe) => Expression.MakeMemberAccess(reportingWDLambda(pe), reportingWDNameInfo)
            });
            _columns.Add("Prompt", new LingoDatabaseColumn()
            {
                Name = "Prompt",
                LinqReferenceGenerator = (pe) => Expression.MakeMemberAccess(pe, promptInfo)
            });
            _columns.Add("Type", new LingoDatabaseColumn()
            {
                Name = "Type",
                LinqReferenceGenerator = (pe) => Expression.MakeMemberAccess(QTypeLambda(pe), QTypeNameInfo)
            });
            _columns.Add("Labels", new LingoDatabaseColumn()
            {
                Name = "Labels",
                LinqReferenceGenerator = labelDimensionExpr
            });
            _columns.Add("InternalId", new LingoDatabaseColumn()
            {
                Name = "InternalId",
                LinqReferenceGenerator = (pe) => Expression.MakeMemberAccess(pe, internalIdInfo)
            });
            _columns.Add("WorkingDocumentId", new LingoDatabaseColumn()
            {
                Name = "WorkingDocumentId",
                LinqReferenceGenerator = (pe) => Expression.MakeMemberAccess(pe, workingDocIdInfo)
            });
            _columns.Add("ReviewerId", new LingoDatabaseColumn()
            {
                Name = "ReviewerId",
                LinqReferenceGenerator = (pe) => Expression.MakeMemberAccess(reviewerExpr(pe), userIdMemberInfo)
            });
            _columns.Add("ReviewerName", new LingoDatabaseColumn()
            {
                Name = "ReviewerName",
                LinqReferenceGenerator = (pe) => Expression.MakeMemberAccess(reviewerExpr(pe), userNameMemberInfo)
            });
            _columns.Add("ReviewerUsername", new LingoDatabaseColumn()
            {
                Name = "ReviewerUsername",
                LinqReferenceGenerator = (pe) => Expression.MakeMemberAccess(reviewerLambda(pe), userUsernameInfo)
            });
            _columns.Add("RevieweeId", new LingoDatabaseColumn()
            {
                Name = "RevieweeId",
                LinqReferenceGenerator = (pe) => Expression.MakeMemberAccess(revieweeExpr(pe), userIdMemberInfo)
            });
            _columns.Add("RevieweeName", new LingoDatabaseColumn()
            {
                Name = "RevieweeName",
                LinqReferenceGenerator = (pe) => Expression.MakeMemberAccess(revieweeExpr(pe), userNameMemberInfo)
            });
            _columns.Add("RevieweeUsername", new LingoDatabaseColumn()
            {
                Name = "RevieweeUsername",
                LinqReferenceGenerator = (pe) => Expression.MakeMemberAccess(revieweeLambda(pe), userUsernameInfo)
            });
            _columns.Add("MediaIds", new LingoDatabaseColumn()
            {
                Name = "MediaIds",
                LinqReferenceGenerator = uploadMediaExpr
            });
            _columns.Add("PresentedOrder", new LingoDatabaseColumn()
            {
                Name = "PresentedOrder",
                LinqReferenceGenerator = (pe) => Expression.MakeMemberAccess(pe, presentedOrderInfo)
            });
        }

        public override sealed Type EntityType
        {
            get { return typeof(CompletedWorkingDocumentPresentedFactEntity); }
        }

        public override ILingoDatastoreTransaction BeginTransaction(IContextContainer context)
        {
            return new LingoDatastoreTransaction(_fnAdapter(), EntityType,
                d => d.CompletedWorkingDocumentPresentedFact.Where(e => !e.IsUserContext));
        }
    }
}
