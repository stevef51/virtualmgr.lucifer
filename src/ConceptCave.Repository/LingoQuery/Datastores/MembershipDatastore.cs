﻿using System;
using System.Linq;
using System.Linq.Expressions;
using ConceptCave.Checklist.Reporting.LingoQuery;
using ConceptCave.Core;
using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.Linq;

namespace ConceptCave.Repository.LingoQuery.Datastores
{
    public class MembershipDatastore : DatastoreBase
    {
        public MembershipDatastore(Func<DataAccessAdapter> fnAdapter) : base(fnAdapter)
        {
            var userIdInfo = EntityType.GetProperty("UserId");
            var userTypeIdInfo = EntityType.GetProperty("UserTypeId");
            var nameInfo = EntityType.GetProperty("Name");
            var timezoneInfo = EntityType.GetProperty("TimeZone");
            var lattitudeInfo = EntityType.GetProperty("Latitude");
            var longitudeInfo = EntityType.GetProperty("Longitude");
            var expiryDateInfo = EntityType.GetProperty("ExpiryDate");

            var userTypeInfo = EntityType.GetProperty("UserType");
            Func<ParameterExpression, Expression> userTypeExpr = (pe) =>
                Expression.MakeMemberAccess(pe, userTypeInfo);
            var userTypeNameMemberInfo = typeof(UserTypeEntity).GetProperty("Name");
            var userTypeIsSiteInfo = typeof(UserTypeEntity).GetProperty("IsAsite");
            var userTypeHasCoordsInfo = typeof(UserTypeEntity).GetProperty("HasGeoCordinates");

            var userUsernameInfo = typeof(AspNetUserEntity).GetProperty("UserName");

            var userInfo = EntityType.GetProperty("AspNetUser");
            Func<ParameterExpression, Expression> userExpr = (pe) =>
                Expression.MakeMemberAccess(pe, userInfo);

            var userMembershipInfo = typeof(AspNetUserEntity).GetProperty("Membership");
            Func<ParameterExpression, Expression> userMembershipExpr = (pe) =>
                Expression.MakeMemberAccess(userExpr(pe), userMembershipInfo);

            var userMembershipEmailInfo = typeof(AspNetUserEntity).GetProperty("Email");

            // ok the property in userdata that gets us out and into tblLabelUsers
            var labelUsersInfo = EntityType.GetProperty("LabelUsers");
            // the name of the label in tblLabels (this is our end point essentially)
            var labelNameInfo = typeof(LabelEntity).GetProperty("Name");
            // ok the property in tblLabelUser which joins across to labels
            var labelUserJoinInfo = typeof(LabelUserEntity).GetProperty("Label");
            // define the table that is doing the join for us
            var labelSelectParameter = Expression.Parameter(typeof(LabelUserEntity), "jt");

            var labelSelectMExpr = Expression.MakeMemberAccess(Expression.MakeMemberAccess(labelSelectParameter,
                                        labelUserJoinInfo), labelNameInfo);
            var labelSelectMLambda = Expression.Lambda(labelSelectMExpr, labelSelectParameter);
            //entity collection is IEnumerable but not IQueryable.
            Func<ParameterExpression, Expression> labelDimensionExpr = (pe) =>
                Expression.Call(typeof(Enumerable), "Select", new Type[] {
                                                            typeof(LabelUserEntity),
                                                            typeof(string)},
                                Expression.MakeMemberAccess(pe, labelUsersInfo),
                                labelSelectMLambda);

            _columns.Add("UserId", new LingoDatabaseColumn()
            {
                Name = "UserId",
                LinqReferenceGenerator = (pe) => Expression.MakeMemberAccess(pe, userIdInfo)
            });
            _columns.Add("UserTypeId", new LingoDatabaseColumn()
            {
                Name = "UserTypeId",
                LinqReferenceGenerator = (pe) => Expression.MakeMemberAccess(pe, userTypeIdInfo)
            });
            _columns.Add("Name", new LingoDatabaseColumn()
            {
                Name = "Name",
                LinqReferenceGenerator = (pe) => Expression.MakeMemberAccess(pe, nameInfo)
            });
            _columns.Add("Username", new LingoDatabaseColumn()
            {
                Name = "Username",
                LinqReferenceGenerator = (pe) => Expression.MakeMemberAccess(userExpr(pe), userUsernameInfo)
            });
            _columns.Add("TimeZone", new LingoDatabaseColumn()
            {
                Name = "TimeZone",
                LinqReferenceGenerator = (pe) => Expression.MakeMemberAccess(pe, timezoneInfo)
            });
            _columns.Add("Latitude", new LingoDatabaseColumn()
            {
                Name = "Latitude",
                LinqReferenceGenerator = (pe) => Expression.MakeMemberAccess(pe, lattitudeInfo)
            });
            _columns.Add("Longitude", new LingoDatabaseColumn()
            {
                Name = "Longitude",
                LinqReferenceGenerator = (pe) => Expression.MakeMemberAccess(pe, longitudeInfo)
            });
            _columns.Add("ExpiryDate", new LingoDatabaseColumn()
            {
                Name = "ExpiryDate",
                LinqReferenceGenerator = (pe) => Expression.MakeMemberAccess(pe, expiryDateInfo)
            });

            _columns.Add("UserType", new LingoDatabaseColumn()
            {
                Name = "UserType",
                LinqReferenceGenerator = (pe) => Expression.MakeMemberAccess(userTypeExpr(pe), userTypeNameMemberInfo)
            });
            _columns.Add("IsSite", new LingoDatabaseColumn()
            {
                Name = "IsSite",
                LinqReferenceGenerator = (pe) => Expression.MakeMemberAccess(userTypeExpr(pe), userTypeIsSiteInfo)
            });
            _columns.Add("HasLocationData", new LingoDatabaseColumn()
            {
                Name = "HasLocationData",
                LinqReferenceGenerator = (pe) => Expression.MakeMemberAccess(userTypeExpr(pe), userTypeHasCoordsInfo)
            });
            _columns.Add("Labels", new LingoDatabaseColumn()
            {
                Name = "Labels",
                LinqReferenceGenerator = labelDimensionExpr
            });
            _columns.Add("Email", new LingoDatabaseColumn()
            {
                Name = "Email",
                LinqReferenceGenerator = (pe) => Expression.MakeMemberAccess(userMembershipExpr(pe), userMembershipEmailInfo)
            });
        }

        public override Type EntityType
        {
            get { return typeof(UserDataEntity); }
        }
    }
}
