﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.Reporting.LingoQuery;
using ConceptCave.Core;
using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.Data.Linq;
using ConceptCave.Repository.LingoQuery.Support;
using SD.LLBLGen.Pro.LinqSupportClasses;

namespace ConceptCave.Repository.LingoQuery.Datastores
{
    public abstract class DatastoreBase : ILingoDatastore
    {
        protected readonly Func<DataAccessAdapter> _fnAdapter;
        protected Dictionary<string, ILingoDataColumn> _columns;

        public DatastoreBase(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
            _columns = new Dictionary<string, ILingoDataColumn>();
        }

        public IDictionary<string, ILingoDataColumn> Columns
        {
            get { return _columns; }
        }

        public virtual ILingoDatastoreTransaction BeginTransaction(IContextContainer context)
        {
            return new LingoDatastoreTransaction(_fnAdapter(), EntityType);
        }

        public abstract Type EntityType { get; }
    }
}