﻿using System;
using System.Linq;
using System.Linq.Expressions;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.Reporting.LingoQuery;
using ConceptCave.Core;
using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Repository.LingoQuery.Support;

namespace ConceptCave.Repository.LingoQuery.Datastores
{
    public class ChecklistDatastore : DatastoreBase
    {
        public ChecklistDatastore(Func<DataAccessAdapter> fnAdapter) : base(fnAdapter)
        {
            var revieweeMemberInfo = EntityType.GetProperty("RevieweeUser");
            Func<ParameterExpression, Expression> revieweeExpr = (pe) =>
                Expression.MakeMemberAccess(pe, revieweeMemberInfo);
            var reviewerMemberInfo = EntityType.GetProperty("ReviewerUser");
            Func<ParameterExpression, Expression> reviewerExpr = (pe) =>
                Expression.MakeMemberAccess(pe, reviewerMemberInfo);
            var userNameMemberInfo = typeof(CompletedUserDimensionEntity).GetProperty("Name");
            var userUsernameMemberInfo = typeof(CompletedUserDimensionEntity).GetProperty("Username");
            var userIdMemberInfo = typeof(CompletedUserDimensionEntity).GetProperty("Id");

            var completedWDDimensionInfo = EntityType.GetProperty("CompletedWorkingDocumentDimension");
            Func<ParameterExpression, Expression> completedWDDimExpr = (pe) =>
                Expression.MakeMemberAccess(pe, completedWDDimensionInfo);
            var wdNameInfo = typeof(CompletedWorkingDocumentDimensionEntity).GetProperty("Name");

            var labelJoinDimensionInfo = EntityType.GetProperty("CompletedLabelWorkingDocumentDimensions");
            var labelDimensionInfo = typeof(CompletedLabelWorkingDocumentDimensionEntity).GetProperty("CompletedLabelDimension");
            var labelNameInfo = typeof(CompletedLabelDimensionEntity).GetProperty("Name");
            //entity.CompletedLabelWorkingDocumentDimensions.Select(e => e.CompletedLabelDimension.Name)
            var labelSelectParameter = Expression.Parameter(typeof(CompletedLabelWorkingDocumentDimensionEntity), "jt");
            var labelSelectMExpr = Expression.MakeMemberAccess(Expression.MakeMemberAccess(labelSelectParameter,
                                        labelDimensionInfo), labelNameInfo);
            var labelSelectMLambda = Expression.Lambda(labelSelectMExpr, labelSelectParameter);
            //entity collection is IEnumerable but not IQueryable.
            Func<ParameterExpression, Expression> labelDimensionExpr = (pe) =>
                Expression.Call(typeof(Enumerable), "Select", new Type[] {
                                                            typeof(CompletedLabelWorkingDocumentDimensionEntity),
                                                            typeof(string)},
                                Expression.MakeMemberAccess(pe, labelJoinDimensionInfo),
                                labelSelectMLambda);

            var dateStartedInfo = EntityType.GetProperty("DateStarted");
            var dateCompletedInfo = EntityType.GetProperty("DateCompleted");
            var totalQuestionCountInfo = EntityType.GetProperty("TotalQuestionCount");
            var totalPossibleScoreInfo = EntityType.GetProperty("TotalPossibleScore");
            var totalScoreInfo = EntityType.GetProperty("TotalScore");
            var passedCountInfo = EntityType.GetProperty("PassedQuestionCount");
            var failedCountInfo = EntityType.GetProperty("FailedQuestionCount");
            var NACountInfo = EntityType.GetProperty("NaquestionCount");
            var reportingIdInfo = EntityType.GetProperty("ReportingWorkingDocumentId");

            var workingDocumentIdInfo = EntityType.GetProperty("WorkingDocumentId");

            _columns.Add("ReviewerId", new LingoDatabaseColumn()
            {
                Name = "ReviewerId",
                LinqReferenceGenerator = (pe) => Expression.MakeMemberAccess(reviewerExpr(pe), userIdMemberInfo)
            });
            _columns.Add("RevieweeName", new LingoDatabaseColumn()
            {
                Name = "RevieweeName",
                LinqReferenceGenerator = (pe) => Expression.MakeMemberAccess(revieweeExpr(pe), userNameMemberInfo)
            });
            _columns.Add("ReviewerName", new LingoDatabaseColumn()
            {
                Name = "ReviewerName",
                LinqReferenceGenerator = (pe) => Expression.MakeMemberAccess(reviewerExpr(pe), userNameMemberInfo)
            });
            _columns.Add("RevieweeId", new LingoDatabaseColumn()
            {
                Name = "RevieweeId",
                LinqReferenceGenerator = (pe) => Expression.MakeMemberAccess(revieweeExpr(pe), userIdMemberInfo)
            });
            _columns.Add("RevieweeUsername", new LingoDatabaseColumn()
            {
                Name = "RevieweeUsername",
                LinqReferenceGenerator = (pe) => Expression.MakeMemberAccess(revieweeExpr(pe), userUsernameMemberInfo)
            });
            _columns.Add("ReviewerUsername", new LingoDatabaseColumn()
            {
                Name = "ReviewerUsername",
                LinqReferenceGenerator = (pe) => Expression.MakeMemberAccess(reviewerExpr(pe), userUsernameMemberInfo)
            });
            _columns.Add("ChecklistName", new LingoDatabaseColumn()
            {
                Name = "ChecklistName",
                LinqReferenceGenerator = (pe) => Expression.MakeMemberAccess(completedWDDimExpr(pe), wdNameInfo)
            });
            _columns.Add("DateStarted", new LingoDatabaseColumn()
            {
                Name = "DateStarted",
                LinqReferenceGenerator = (pe) => Expression.MakeMemberAccess(pe, dateStartedInfo)
            });
            _columns.Add("DateCompleted", new LingoDatabaseColumn()
            {
                Name = "DateCompleted",
                LinqReferenceGenerator = (pe) => Expression.MakeMemberAccess(pe, dateCompletedInfo)
            });
            _columns.Add("TotalQuestionCount", new LingoDatabaseColumn()
            {
                Name = "TotalQuestionCount",
                LinqReferenceGenerator = (pe) => Expression.MakeMemberAccess(pe, totalQuestionCountInfo)
            });
            _columns.Add("TotalPossibleScore", new LingoDatabaseColumn()
            {
                Name = "TotalPossibleScore",
                LinqReferenceGenerator = (pe) => Expression.MakeMemberAccess(pe, totalPossibleScoreInfo)
            });
            _columns.Add("TotalScore", new LingoDatabaseColumn()
            {
                Name = "TotalScore",
                LinqReferenceGenerator = (pe) => Expression.MakeMemberAccess(pe, totalScoreInfo)
            });
            _columns.Add("PassedQuestionCount", new LingoDatabaseColumn()
            {
                Name = "PassedQuestionCount",
                LinqReferenceGenerator = (pe) => Expression.MakeMemberAccess(pe, passedCountInfo)
            });
            _columns.Add("FailedQuestionCount", new LingoDatabaseColumn()
            {
                Name = "FailedQuestionCount",
                LinqReferenceGenerator = (pe) => Expression.MakeMemberAccess(pe, failedCountInfo)
            });
            _columns.Add("NAQuestionCount", new LingoDatabaseColumn()
            {
                Name = "NAQuestionCount",
                LinqReferenceGenerator = (pe) => Expression.MakeMemberAccess(pe, NACountInfo)
            });
            _columns.Add("WorkingDocumentId", new LingoDatabaseColumn()
            {
                Name = "WorkingDocumentId",
                LinqReferenceGenerator = (pe) => Expression.MakeMemberAccess(pe, workingDocumentIdInfo)
            });
            _columns.Add("Labels", new LingoDatabaseColumn()
            {
                Name = "Labels",
                LinqReferenceGenerator = labelDimensionExpr
            });
            _columns.Add("ReportingDocumentId", new LingoDatabaseColumn()
            {
                Name = "ReportingDocumentId",
                LinqReferenceGenerator = (pe) => Expression.MakeMemberAccess(pe, reportingIdInfo)
            });
        }

        public override sealed Type EntityType
        {
            get { return typeof(CompletedWorkingDocumentFactEntity); }
        }

        public override ILingoDatastoreTransaction BeginTransaction(IContextContainer context)
        {
            return new LingoDatastoreTransaction(_fnAdapter(), EntityType,
                d => d.CompletedWorkingDocumentFact.Where(e => !e.IsUserContext));
        }
    }
}
