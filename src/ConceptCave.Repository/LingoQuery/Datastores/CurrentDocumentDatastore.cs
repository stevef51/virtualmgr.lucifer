﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;
using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.Data.Linq;
using ConceptCave.Repository.LingoQuery.Support;

namespace ConceptCave.Repository.LingoQuery.Datastores
{
    public class CurrentDocumentDatastore : QuestionDatastore, IObjectGetter
    {
        public CurrentDocumentDatastore(Func<DataAccessAdapter> fnAdapter) : base(fnAdapter)
        {
        }

        public override ILingoDatastoreTransaction BeginTransaction(IContextContainer context)
        {
            var id = GetId(context);
            return new LingoDatastoreTransaction(_fnAdapter(), EntityType,
                d => d.CompletedWorkingDocumentPresentedFact.Where(e => e.WorkingDocumentId == id));
        }

        public Guid GetId(IContextContainer context)
        {
            var id = context.Get<Guid>("ChecklistReportingId", () => Guid.Empty);
            if (id == Guid.Empty)
                throw new InvalidOperationException("The CurrentDocument Datastore can only be used in a Finish With block");
            return id;
        }

        public bool GetObject(IContextContainer context, string name, out object result)
        {
            switch (name)
            {
                case "id":
                    result = GetId(context);
                    return true;
                default:
                    //fire up a checklist datastore and do a query for this property

                    var id = GetId(context);
                    var clistDstore = new ChecklistDatastore(_fnAdapter);
                    var pe = Expression.Parameter(clistDstore.EntityType, "entity");
                    Expression reqExpression;
                    try
                    {
                        reqExpression = clistDstore.Columns[name].LinqReference(pe);
                    }
                    catch (KeyNotFoundException)
                    {
                        result = null;
                        return false;
                    }

                    using (var adapter = _fnAdapter())
                    {
                        var db = new LinqMetaData(adapter);
                        var whereClause = db.CompletedWorkingDocumentFact.Where(e => e.WorkingDocumentId == id);
                        var selectClause = Expression.Call(typeof(Queryable), "Select",
                            new Type[] { clistDstore.EntityType, reqExpression.Type }, Expression.Constant(whereClause),
                            Expression.Lambda(reqExpression, pe));

                        //There should only be one element
                        //So GetEnumerator().Current contains the result
                        var query = whereClause.Provider.CreateQuery(selectClause);
                        var it = query.GetEnumerator();
                        if (it.MoveNext())
                        {
                            result = it.Current;
                            return true;
                        }
                        else
                        {
                            result = null;
                            return false;
                        }
                    }
            }
        }
    }
}