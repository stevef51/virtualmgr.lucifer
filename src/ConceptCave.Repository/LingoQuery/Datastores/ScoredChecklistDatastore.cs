﻿using System;
using System.Linq;
using System.Linq.Expressions;
using ConceptCave.Checklist.Reporting.LingoQuery;
using ConceptCave.Core;
using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.Repository.LingoQuery.Support;

namespace ConceptCave.Repository.LingoQuery.Datastores
{
    public class ScoredChecklistDatastore : ChecklistDatastore
    {
        public ScoredChecklistDatastore(Func<DataAccessAdapter> fnAdapter) : base(fnAdapter)
        {
            _columns.Add("ScorePercentage", new LingoDatabaseColumn()
            {
                Name = "ScorePercentage",
                LinqReferenceGenerator = (pe) => Expression.Multiply(Expression.Constant(100m, typeof(decimal)),
                    Expression.Divide(_columns["TotalScore"].LinqReference(pe),
                        _columns["TotalPossibleScore"].LinqReference(pe)))
            });
        }

        public override Checklist.Interfaces.ILingoDatastoreTransaction BeginTransaction(IContextContainer context)
        {
            return new LingoDatastoreTransaction(_fnAdapter(), EntityType,
                d => d.CompletedWorkingDocumentFact.Where(e => e.TotalPossibleScore > 0));
        }
    }
}