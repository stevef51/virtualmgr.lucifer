﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;
using ConceptCave.RepositoryInterfaces.Repositories;
using Newtonsoft.Json.Linq;

namespace ConceptCave.Repository.ProcessingRules
{
    public class PublicVariable : Node, IProcessingRule
    {
        private readonly IWorkingDocumentPublicVariableRepository _workingDocumentPublicVariableRepo;
        protected string _name;

        public PublicVariable(IWorkingDocumentPublicVariableRepository workingDocumentPublicVariableRepo)
        {
            _workingDocumentPublicVariableRepo = workingDocumentPublicVariableRepo;
        }

        public string Name
        {
            get
            {
                return string.IsNullOrEmpty(_name) ? "Not Named" : _name;
            }
            set
            {
                _name = value;
            }
        }

        public int Order
        {
            get { return int.MaxValue; }
        }

        public void Process(IProcessingRuleContext context)
        {
            if (context.ValidatorResult.InvalidReasons.Count > 0)
            {
                return;
            }

            if (context.Direction == ProcessDirection.Forward)
            {
                _workingDocumentPublicVariableRepo.InsertOrUpdate(((ILingoProgram)context.Context).WorkingDocument.Id, this.Name, context.Answer.AnswerAsString, context.Answer.PassFail, context.Answer.Score);
            }
            else
            {
                _workingDocumentPublicVariableRepo.InsertOrUpdate(((ILingoProgram)context.Context).WorkingDocument.Id, this.Name, null, null, null);
            }
        }

        public JObject ToJson()
        {
            return null;
        }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("Name", Name);
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            Name = decoder.Decode<string>("Name");
        }
    }
}
