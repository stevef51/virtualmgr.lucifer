﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClosedXML.Excel;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;

namespace ConceptCave.Repository.DesignDocumentImport
{
    public class XlsxExporter
    {
        public XLWorkbook Export(IDesignDocument document)
        {
            XLWorkbook book = new XLWorkbook(XLEventTracking.Disabled);

            Export(book, document);

            return book;
        }

        public void Export(Stream stream, IDesignDocument document)
        {
            var book = Export(document);

            book.SaveAs(stream);

            stream.Position = 0;
        }

        public void Export(XLWorkbook workBook, IDesignDocument document)
        {
            // General first
            var settingsWorksheet = workBook.AddWorksheet(XlsxImporter.SettingsWorksheetName, 0);
            WriteSettings(settingsWorksheet, document);

            var pagesWorksheet = workBook.AddWorksheet(XlsxImporter.LayoutWorksheetName, 1);
            WritePages(pagesWorksheet, document);

            var sourceWorksheet = workBook.AddWorksheet(XlsxImporter.SourcecodeWorksheetName, 2);
            sourceWorksheet.Cell(1, "A").SetValue<string>(document.ProgramDefinition.SourceCode);

            var facilityWorksheet = workBook.AddWorksheet(XlsxImporter.FacilitiesWorksheetName, 3);
            WriteFacilities(facilityWorksheet, document);

            int i = 4;

            Dictionary<string, List<string>> cache = MakeNamesUnique(document);

            foreach (var section in document.Sections)
            {
                IXLWorksheet sheet = workBook.AddWorksheet(section.Name, i);

                WriteSection(sheet, section, cache);

                i++;
            }
        }

        protected Dictionary<string, List<string>> MakeNamesUnique(IDesignDocument document)
        {
            Dictionary<string, List<string>> cache = new Dictionary<string,List<string>>();

            foreach(var section in document.Sections)
            {
                foreach(var p in ((ConceptCave.Checklist.Editor.Section)section).AsDepthFirstEnumerable())
                {
                    if(p is IQuestion)
                    {
                        continue;
                    }

                    var s = (ConceptCave.Checklist.Editor.Section)p;

                    if (s.Name == null)
                        s.Name = "Section " + (cache.Count + 1).ToString();

                    if(cache.ContainsKey(s.Name) == false)
                    {
                        cache.Add(s.Name, new List<string>());
                    }
                    else
                    {
                        cache[s.Name].Add(s.Name);

                        // let's rename the document section, assuming this isn't referenced in script as
                        // there is a previous section of the same name.
                        s.Name = string.Format("{0} {1}", s.Name, cache[s.Name].Count.ToString());
                    }
                }
            }

            return cache;
        }

        public void WriteSettings(IXLWorksheet sheet, IDesignDocument document)
        {
            sheet.Cell(1, "A").SetValue<string>("Name");
            sheet.Cell(2, "A").SetValue<string>("Finish Mode");

            sheet.Cell(1, "B").SetValue<string>(document.Name);
            sheet.Cell(2, "B").SetValue<string>(document.FinishMode.ToString());
        }

        public void WritePages(IXLWorksheet sheet, IDesignDocument document)
        {
            sheet.Cell(1, "A").SetValue<string>("Name");
            sheet.Cell(1, "B").SetValue<string>("Prompt");
            sheet.Cell(1, "C").SetValue<string>("Layout");

            var i = 2;
            foreach (var section in document.Sections)
            {
                if(string.IsNullOrEmpty(section.Prompt) == true && section.HtmlPresentation.Layout == HtmlSectionLayout.Flow)
                {
                    // we only output something here if there are more than just a name for it
                    continue;
                }

                sheet.Cell(i, "A").SetValue<string>(section.Name);
                sheet.Cell(i, "B").SetValue<string>(section.Prompt);

                if (section.HtmlPresentation.Layout != HtmlSectionLayout.Flow)
                {
                    sheet.Cell(i, "C").SetValue<string>(section.HtmlPresentation.Layout.ToString());
                }

                i++;
            }
        }

        public void WriteFacilities(IXLWorksheet sheet, IDesignDocument document)
        {
            sheet.Cell(1, "A").SetValue<string>("Type");

            int i = 2;
            foreach (var facility in document.Facilities)
            {
                var item = RepositorySectionManager.Current.ItemForObject(facility);

                sheet.Cell(i, "A").SetValue<string>(item.FriendlyName);

                if (facility is IFromNameValues)
                {
                    var templates = ((IFromNameValues)facility).ToNameValuePairs();

                    foreach (var pairs in templates)
                    {
                        int j = 2;
                        foreach (var name in pairs.AllKeys)
                        {
                            sheet.Cell(i, j).SetValue<string>(name);
                            j++;
                            sheet.Cell(i, j).SetValue<string>(pairs[name]);
                            j++;
                        }

                        i++;
                    }
                }
            }
        }

        protected int WriteLevel(IXLWorksheet sheet, ISection parent, int row, bool isRoot, string[] fixedColumns, Dictionary<string, List<string>> cache)
        {
            int i = 0;
            foreach (var presentable in parent.Children)
            {
                var item = RepositorySectionManager.Current.ItemForObject(presentable);

                var templates = ((IFromNameValues)presentable).ToNameValuePairs();

                foreach (var pairs in templates)
                {
                    if (presentable is IQuestion)
                    {
                        // fixed columns first
                        IQuestion q = (IQuestion)presentable;

                        sheet.Cell(row + i, "A").SetValue<string>(q.Name);
                        sheet.Cell(row + i, "B").SetValue<string>(item.FriendlyName);
                        sheet.Cell(row + i, "C").SetValue<string>(q.Prompt);
                        sheet.Cell(row + i, "D").SetValue<string>(q.Note);
                        sheet.Cell(row + i, "E").SetValue<string>(q.AllowNotesOnAnswer == false ? "" : q.AllowNotesOnAnswer.ToString());

                        var mandatory = (from n in pairs.AllKeys where n == "mandatory" select n);
                        if (mandatory.Count() > 0)
                        {
                            sheet.Cell(row + i, "F").SetValue<string>(pairs[mandatory.First()]);
                        }

                        if (isRoot == false && string.IsNullOrEmpty(parent.Name) == false)
                        {
                            sheet.Cell(row + i, "G").SetValue<string>(parent.Name);
                        }

                        // now any the name/value pairs
                        int j = 8;
                        foreach (var pair in pairs.AllKeys)
                        {
                            if (fixedColumns.Contains(pair.ToLower()) == true)
                            {
                                continue; // already done it
                            }

                            sheet.Cell(row + i, j).SetValue<string>(pair);
                            j++;
                            sheet.Cell(row + i, j).SetValue<string>(pairs[pair]);
                            j++;
                        }

                        i++;
                    }
                    else if (presentable is ISection)
                    {
                        ISection s = (ISection)presentable;

                        // One of the issues we have with sections is that they get autmotically assigned a name when created in the designer.
                        // this name isn't unique unless the user changes it. This is fine as most of the time sections aren't referred to by their name in Lingo,
                        // but from an import point of view its then impossible to re-hook things back up with just a parent's name as there are lots of 'New Section' objects.
                        // To get around this we are going to assign new names to sections to make sure they are unique so the importer can reconstruct the correct format.
                        if (cache.ContainsKey(s.Name))
                        {
                            var newName = s.Name + cache[s.Name].Count.ToString(); ;

                            cache[s.Name].Add(newName);

                            s.Name = newName;
                        }
                        else
                        {
                            cache.Add(s.Name, new List<string>(new string[] {s.Name}));
                        }

                        sheet.Cell(row + i, "A").SetValue<string>(s.Name);
                        sheet.Cell(row + i, "B").SetValue<string>(item.FriendlyName);
                        sheet.Cell(row + i, "C").SetValue<string>(s.Prompt);

                        if (isRoot == false && string.IsNullOrEmpty(parent.Name) == false)
                        {
                            sheet.Cell(row + i, "G").SetValue<string>(parent.Name);
                        }

                        int j = 8;
                        foreach (var pair in pairs.AllKeys)
                        {
                            if (fixedColumns.Contains(pair.ToLower()) == true)
                            {
                                continue; // already done it
                            }

                            sheet.Cell(row + i, j).SetValue<string>(pair);
                            j++;
                            sheet.Cell(row + i, j).SetValue<string>(pairs[pair]);
                            j++;
                        }

                        i++; // one for ourselves
                        i += WriteLevel(sheet, s, row + i, false, fixedColumns, cache);
                    }
                }
            }

            return i;
        }

        public void WriteSection(IXLWorksheet sheet, ISection section, Dictionary<string, List<string>> cache)
        {
            string[] columns = new string[] { "name", "type", "prompt", "note", "allownotesonanswer", "mandatory", "parent" };

            sheet.Cell(1, "A").SetValue<string>("Name");
            sheet.Cell(1, "B").SetValue<string>("Type");
            sheet.Cell(1, "C").SetValue<string>("Prompt");
            sheet.Cell(1, "D").SetValue<string>("Note");
            sheet.Cell(1, "E").SetValue<string>("Allow Notes");
            sheet.Cell(1, "F").SetValue<string>("Mandatory");
            sheet.Cell(1, "G").SetValue<string>("Parent");

            WriteLevel(sheet, section, 2, true, columns, cache);
        }
    }
}
