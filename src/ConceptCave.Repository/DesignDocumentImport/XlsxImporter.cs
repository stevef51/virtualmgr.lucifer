﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClosedXML.Excel;
using ConceptCave.BusinessLogic.Configuration;
//using ConceptCave.BusinessLogic.Configuration;
using ConceptCave.Checklist.Editor;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.ProcessingRules;
using ConceptCave.Checklist.Validators;
using ConceptCave.Core;

namespace ConceptCave.Repository.DesignDocumentImport
{
    public class XlsxImporter
    {
        public const string SettingsWorksheetName = "-General-";
        public const string SourcecodeWorksheetName = "-Source Code-";
        public const string LayoutWorksheetName = "-Pages-";
        public const string FacilitiesWorksheetName = "-Facilities-";

        protected string[] SystemWorksheetNames;

        public XlsxImporter()
        {
            SystemWorksheetNames = new string[] { SettingsWorksheetName, SourcecodeWorksheetName, LayoutWorksheetName, FacilitiesWorksheetName };
        }

        /// <summary>
        /// Imports the Xcel file into a new design document
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        public IDesignDocument Import(Stream stream)
        {
            var dd = new DesignDocument();

            return Import(stream, dd);
        }

        /// <summary>
        /// Imports the Xcel file into a new design document with the same rootid as that contained in the design document parameter
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="dd"></param>
        /// <returns></returns>
        public IDesignDocument Import(Stream stream, IDesignDocument dd)
        {
            var document = new DesignDocument(dd.Id);

            var workBook = new ClosedXML.Excel.XLWorkbook(stream, ClosedXML.Excel.XLEventTracking.Disabled);

            // general settings first
            var settings = GetDefaultSettings();
            IXLWorksheet settingsSheet = null;

            if (workBook.TryGetWorksheet(SettingsWorksheetName, out settingsSheet) == true)
            {
                ImportSettings(settings, settingsSheet);
            }
            ApplySettings(settings, document);

            // sections next
            List<dynamic> sections = new List<dynamic>();
            IXLWorksheet layoutSheet = null;

            if (workBook.TryGetWorksheet(LayoutWorksheetName, out layoutSheet) == true)
            {
                LoadDeclaredSections(layoutSheet, sections);
            }
            LoadUndeclaredSections(workBook, sections);

            ApplySections(sections, document);

            // questions next
            List<dynamic> questions = new List<dynamic>();
            LoadQuestions(workBook, questions);
            ApplyQuestions(questions, document);

            List<dynamic> facilities = new List<dynamic>();
            IXLWorksheet facilitySheet = null;

            if (workBook.TryGetWorksheet(FacilitiesWorksheetName, out facilitySheet) == true)
            {
                LoadFacilities(facilitySheet, facilities);
            }
            ApplyFacilities(facilities, document);

            IXLWorksheet sourcecodeSheet = null;
            if (workBook.TryGetWorksheet(SourcecodeWorksheetName, out sourcecodeSheet) == true)
            {
                string code = LoadSourcecode(sourcecodeSheet);
                if (string.IsNullOrEmpty(code) == false)
                {
                    document.ProgramDefinition.SourceCode = code;
                }
            }

            return document;
        }

        protected dynamic GetDefaultSettings()
        {
            dynamic result = new ExpandoObject();

            result.Name = "New Imported Checklist";
            result.FinishMode = FinishMode.Manual;

            return result;
        }

        /// <summary>
        /// Imports Name, FinishMode and Labels from the settings worksheet. The expected format of this is
        /// Column B, row 1 to be the name
        /// Column B, row 2 to be the FinishMode
        /// </summary>
        /// <param name="settings">Dynamic object containing the default settings</param>
        /// <param name="settingsSheet">Worksheet that holds the settings</param>
        public void ImportSettings(dynamic settings, IXLWorksheet settingsSheet)
        {
            var name = settingsSheet.Cell(1, "B").GetString();
            var finishMode = settingsSheet.Cell(2, "B").GetString();

            if (string.IsNullOrEmpty(name) == false)
            {
                settings.Name = name;
            }

            if (string.IsNullOrEmpty(finishMode) == false)
            {
                var fm = FinishMode.Manual;
                if (Enum.TryParse(finishMode, out fm) == true)
                {
                    settings.FinishMode = fm;
                }
            }
        }

        /// <summary>
        /// Applies the settings to the design document
        /// </summary>
        /// <param name="settings">settings to apply</param>
        /// <param name="document">document they are to be applied to</param>
        public void ApplySettings(dynamic settings, IDesignDocument document)
        {
            document.Name = settings.Name;
            document.FinishMode = settings.FinishMode;
        }


        protected dynamic CreateSectionDynamic()
        {
            dynamic result = new ExpandoObject();
            result.Name = "";
            result.Prompt = "";
            result.Layout = HtmlSectionLayout.Flow;
            result.Pairs = new NameValueCollection();

            return result;
        }

        /// <summary>
        /// Runs through the worksheets in the workbook and creates a section representation for any sheets that aren't a part of the set of system worksheets
        /// </summary>
        /// <param name="workbook"></param>
        /// <param name="sections"></param>
        public void LoadUndeclaredSections(XLWorkbook workbook, List<dynamic> sections)
        {
            foreach (var sheet in from w in workbook.Worksheets where SystemWorksheetNames.Contains(w.Name) == false select w)
            {
                // make sure we've not seen this before (by loading up the sheets in the layout worksheet
                if ((from sh in sections where sh.Name == sheet.Name select sh).Count() > 0)
                {
                    continue;
                }

                dynamic s = CreateSectionDynamic();
                s.Name = sheet.Name;
                s.Pairs.Add("Name", sheet.Name);

                sections.Add(s);
            }
        }

        /// <summary>
        /// Loads the set of sections defined in the -Pages- worksheet. The expected format of the worksheet is
        /// A       B       C     
        /// ----------------------
        /// Name    Prompt  Layout
        /// 
        /// where
        /// Name is the name of the section
        /// Prompt is the prompt (if any) for the section
        /// Layout is the HtmlSectionLayout of the section (as a string)
        /// </summary>
        /// <param name="sheet">Sheet to parse</param>
        /// <param name="sections">List of translated object loaded from the worksheet</param>
        public void LoadDeclaredSections(IXLWorksheet sheet, List<dynamic> sections)
        {
            string[] fixedColumns = new string[] {"A","B","C"};
            // first row is the title row, so skip that one
            foreach (var row in sheet.RowsUsed().Skip(1))
            {
                string name = row.Cell("A").GetString();
                string prompt = row.Cell("B").GetString();
                string layout = row.Cell("C").GetString();

                if (string.IsNullOrEmpty(name) && string.IsNullOrEmpty(prompt) && string.IsNullOrEmpty(layout))
                {
                    continue;
                }

                dynamic s = null;

                // see if we already have the section declared (from an inspection of the worksheets in the workbook) previously and if so use that object
                if (string.IsNullOrEmpty(name) == false)
                {
                    var existing = (from sh in sections where sh.Name == name select sh);
                    if (existing.Count() > 0)
                    {
                        s = existing.First();
                    }
                }

                // not seen it before, create a new one
                if(s == null)
                {
                    s = CreateSectionDynamic();
                }
                s.Name = name;
                s.Pairs.Add("Name", name);
                s.Pairs.Add("Prompt", prompt);
                if (string.IsNullOrEmpty(layout) == false)
                {
                    s.Pairs.Add("Layout", layout);
                }

                // no we load up any name value pairs
                bool readingName = true;
                string currentName = null;
                foreach (var cell in row.CellsUsed())
                {
                    if (fixedColumns.Contains(cell.WorksheetColumn().ColumnLetter()) == true)
                    {
                        continue;
                    }

                    if (readingName == true)
                    {
                        currentName = cell.GetString();
                        readingName = false;
                    }
                    else
                    {
                        s.Pairs.Add(currentName, cell.GetString());
                        readingName = true;
                    }
                }

                sections.Add(s);
            }
        }

        public void ApplySections(List<dynamic> sections, IDesignDocument document)
        {
            foreach (var section in sections)
            {
                Section s = new Section();
                s.FromNameValuePairs(section.Pairs);

                document.Sections.Add(s);
            }
        }

        protected dynamic CreateQuestionDynamic()
        {
            dynamic result = new ExpandoObject();

            result.Pairs = new NameValueCollection();
            result.Parent = "";
            result.Type = "";

            return result;
        }

        /// <summary>
        /// Loads the questions from the workbook. The expected form of each worksheet with questions on them is
        /// A       B       C       D       E           F           G         
        /// ------------------------------------------------------------------
        /// Name    Type    Prompt  Note    AllowNotes  Mandatory   Parent
        /// Where
        /// Name is the name of the question
        /// Type is the type of question
        /// Prompt the prompt
        /// Note the notes section text
        /// AllowNotes the True/False whether or not the user can add notes to their answer
        /// Mandatory wether or not the question must be answered
        /// Categories a comma seperated list of categories for the question
        /// Parent the name of the parent section the question resides in. If blank the worksheet's name is used
        /// MultiSelect is for use with multi choice questions and specifies if the questiona allows multiple answers to be selected
        /// Format is for multi choice and specifies how it is to be presented
        /// 
        /// After Column G it takes the cells as name value pairs, so if H has something in it its a property name with I having the value.
        /// This allows us to support question specific properties, such as the text of a label etc. It also allows support of general properties
        /// that aren't set too often.
        /// 
        /// </summary>
        /// <param name="workbook"></param>
        /// <param name="questions"></param>
        public void LoadQuestions(XLWorkbook workbook, List<dynamic> questions)
        {
            string[] fixedColumns = new string[] {"A","B","C","D","E","F","G"};

            foreach (var sheet in from w in workbook.Worksheets where SystemWorksheetNames.Contains(w.Name) == false select w)
            {
                foreach (var row in sheet.RowsUsed().Skip(1))
                {
                    string name = row.Cell("A").GetString();
                    string type = row.Cell("B").GetString();
                    string prompt = row.Cell("C").GetString();
                    string note = row.Cell("D").GetString();
                    string allownotes = row.Cell("E").GetString();
                    string mandatory = row.Cell("F").GetString();
                    string parent = row.Cell("G").GetString();

                    if (string.IsNullOrEmpty(type) == true)
                    {
                        continue; // bad row
                    }

                    IRepositorySectionManagerItem item = RepositorySectionManager.Current.ItemForFriendlyType(type);

                    if (item == null)
                    {
                        continue; // bad row, we don't know what question type they are talking about
                    }

                    var q = CreateQuestionDynamic();
                    q.Pairs.Add("Name", name);
                    q.Type = item;
                    
                    if (string.IsNullOrEmpty(prompt) == false)
                    {
                        q.Pairs.Add("Prompt", prompt);
                    }

                    if (string.IsNullOrEmpty(note) == false)
                    {
                        q.Pairs.Add("Note", note);
                    }

                    if (string.IsNullOrEmpty(allownotes) == false)
                    {
                        q.Pairs.Add("allownotesonanswer", allownotes);
                    }
                    else
                    {
                        q.Pairs.Add("allownotesonanswer", false.ToString());
                    }

                    if (string.IsNullOrEmpty(mandatory) == false)
                    {
                        q.Pairs.Add("Mandatory", mandatory);
                    }

                    if (string.IsNullOrEmpty(parent) == true)
                    {
                        q.Parent = sheet.Name;
                    }
                    else
                    {
                        q.Parent = parent;
                    }

                    // no we load up any name value pairs
                    bool readingName = true;
                    string currentName = null;
                    int nameColNumber = -1;

                    foreach (var cell in row.CellsUsed())
                    {
                        if (fixedColumns.Contains(cell.WorksheetColumn().ColumnLetter()) == true)
                        {
                            continue;
                        }

                        if (readingName == true)
                        {
                            currentName = cell.GetString();
                            readingName = false;
                            nameColNumber = cell.Address.ColumnNumber;
                        }
                        else
                        {
                            // the cellsused method used to loop through the cells
                            // skips blank cells. So if a property was blank, then
                            // it's value gets skipped. To work out if this has occurred
                            // we use the columnnumber of the name, if we've moved more
                            // than 1 column, it's actually another property name we are reading
                            if (cell.Address.ColumnNumber == nameColNumber + 1)
                            {
                                q.Pairs.Add(currentName, cell.GetString());
                                readingName = true;
                            }
                            else
                            {
                                // enter a blank for the current value
                                q.Pairs.Add(currentName, "");
                                // we must be reading another property name, so react accordingly
                                currentName = cell.GetString();
                                readingName = false;
                                nameColNumber = cell.Address.ColumnNumber;
                            }
                        }
                    }

                    questions.Add(q);
                }
            }
        }

        public void ApplyQuestions(List<dynamic> questions, IDesignDocument document)
        {
            foreach (var question in questions)
            {
                IFromNameValues q = (IFromNameValues)question.Type.CreateResource();

                q.FromNameValuePairs(question.Pairs);

                var sections = (from p in document.AsDepthFirstEnumerable() where p is ISection && ((ISection)p).Name == question.Parent select p).Cast<ISection>();

                if (sections.Count() == 0)
                {
                    continue;
                }

                sections.First().Children.Add((IPresentable)q);
            }
        }

        protected dynamic CreateFacilityDynamic()
        {
            dynamic result = new ExpandoObject();

            result.Type = "";
            result.Pairs = new NameValueCollection();

            return result;
        }

        public void LoadFacilities(IXLWorksheet sheet, List<dynamic> facilities)
        {
            string[] fixedColumns = new string[] {"A"};
            // first row is the title row, so skip that one
            foreach (var row in sheet.RowsUsed().Skip(1))
            {
                string type = row.Cell("A").GetString();

                if (string.IsNullOrEmpty(type) == true)
                {
                    continue; // bad row
                }

                IRepositorySectionManagerItem item = RepositorySectionManager.Current.ItemForFriendlyType(type);

                if (item == null)
                {
                    continue; // bad row, we don't know what question type they are talking about
                }

                var f = CreateFacilityDynamic();
                f.Type = item;

                // no we load up any name value pairs
                bool readingName = true;
                string currentName = null;

                foreach (var cell in row.CellsUsed())
                {
                    if (fixedColumns.Contains(cell.WorksheetColumn().ColumnLetter()) == true)
                    {
                        continue;
                    }

                    if (readingName == true)
                    {
                        currentName = cell.GetString();
                        readingName = false;
                    }
                    else
                    {
                        f.Pairs.Add(currentName, cell.GetString());
                        readingName = true;
                    }
                }

                facilities.Add(f);
            }
        }

        public void ApplyFacilities(List<dynamic> facilities, IDesignDocument document)
        {
            Dictionary<string, IFacility> loaded = new Dictionary<string, IFacility>();

            foreach (var facility in facilities)
            {
                IFacility f = null;

                if (loaded.ContainsKey(facility.Type.FriendlyName) == false)
                {
                    f = facility.Type.CreateResource();
                    loaded.Add(facility.Type.FriendlyName, f);
                }
                else
                {
                    f = loaded[facility.Type.FriendlyName];
                }
                
                if (f is IFromNameValues)
                {
                    ((IFromNameValues)f).FromNameValuePairs(facility.Pairs);
                }

                document.Facilities.Add(f);
            }
        }

        public string LoadSourcecode(IXLWorksheet sheet)
        {
            string result = "";

            foreach (var row in sheet.RowsUsed())
            {
                string c = row.Cell("A").GetString();

                if (string.IsNullOrEmpty(c))
                {
                    continue;
                }

                result += string.Format("{0}\r\n", c); 
            }

            return result;
        }
    }
}
