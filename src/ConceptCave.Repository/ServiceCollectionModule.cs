using System;
using ConceptCave.Repository.ImportMapping;
using ConceptCave.Repository.Injected;
using ConceptCave.Repository.PeriodicExecution;
using ConceptCave.Repository.PolicyManagement;
using ConceptCave.RepositoryInterfaces.PolicyManagement;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.RepositoryInterfaces.ScheduleManagement;
using Microsoft.Extensions.Configuration;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionModule
    {
        public static void AddDataWarehousingTenantMasterPopulateLLBLGen(this IServiceCollection services)
        {
            services.AddTransient<ODWTenantMasterPopulateBuildingFloorsDimension>();
            services.AddTransient<ODWTenantMasterPopulateCompaniesDimension>();
            services.AddTransient<ODWTenantMasterPopulateDateTimesDimension>();
            services.AddTransient<ODWTenantMasterPopulateDatesDimension>();
            services.AddTransient<ODWTenantMasterPopulateFacilitiesDimension>();
            services.AddTransient<ODWTenantMasterPopulateFacilityBuildingsDimension>();
            services.AddTransient<ODWTenantMasterPopulateFloorZonesDimension>();
            services.AddTransient<ODWTenantMasterPopulateRostersDimension>();
            services.AddTransient<ODWTenantMasterPopulateSitesDimension>();
            services.AddTransient<ODWTenantMasterPopulateTaskCustomStatusDimension>();
            services.AddTransient<ODWTenantMasterPopulateTaskFacts>();
            services.AddTransient<ODWTenantMasterPopulateTaskStatusDimension>();
            services.AddTransient<ODWTenantMasterPopulateTaskTypesDimension>();
            services.AddTransient<ODWTenantMasterPopulateTenantsDimension>();
            services.AddTransient<ODWTenantMasterPopulateUsersDimension>();
            services.AddTransient<ODWTenantMasterPopulateZoneSitesDimension>();
        }
        public static void AddDataWarehousingTenantPopulateLLBLGen(this IServiceCollection services)
        {
            services.AddTransient<ODWTenantPopulateBuildingFloorsDimension>();
            services.AddTransient<ODWTenantPopulateCompaniesDimension>();
            services.AddTransient<ODWTenantPopulateDateTimesDimension>();
            services.AddTransient<ODWTenantPopulateDatesDimension>();
            services.AddTransient<ODWTenantPopulateFacilitiesDimension>();
            services.AddTransient<ODWTenantPopulateFacilityBuildingsDimension>();
            services.AddTransient<ODWTenantPopulateFloorZonesDimension>();
            services.AddTransient<ODWTenantPopulateRostersDimension>();
            services.AddTransient<ODWTenantPopulateSitesDimension>();
            services.AddTransient<ODWTenantPopulateTaskCustomStatusDimension>();
            services.AddTransient<ODWTenantPopulateTaskFacts>();
            services.AddTransient<ODWTenantPopulateTaskStatusDimension>();
            services.AddTransient<ODWTenantPopulateTaskTypesDimension>();
            services.AddTransient<ODWTenantPopulateUsersDimension>();
            services.AddTransient<ODWTenantPopulateZoneSitesDimension>();
        }

        public static void AddConceptCaveRepositoryLLBLGen(this IServiceCollection services)
        {
            services.AddTransient<IArticleRepository, OArticleRepository>();
            services.AddTransient<IAssetRepository, OAssetRepository>();
            services.AddTransient<IAssetScheduleRepository, OAssetScheduleRepository>();
            services.AddTransient<IAssetTrackerRepository, OAssetTrackerRepository>();
            services.AddTransient<IAssetTypeCalendarRepository, OAssetTypeCalendarRepository>();
            services.AddTransient<IAssetTypeCalendarTaskRepository, OAssetTypeCalendarTaskRepository>();
            services.AddTransient<IAssetTypeRepository, OAssetTypeRepository>();
            services.AddTransient<IAuditTaskConditionActionRepository, OAuditTaskConditionActionRepository>();
            services.AddTransient<IBlockedRepository, OBlockedRepository>();
            services.AddTransient<ICalendarRuleRepository, OCalendarRuleRepository>();
            services.AddTransient<ICompanyRepository, OCompanyRepository>();
            services.AddTransient<ICompletedLabelDimensionRepository, OCompletedLabelDimensionRepository>();
            services.AddTransient<ICompletedLabelWorkingDocumentDimensionRepository, OCompletedLabelWorkingDocumentDimensionRepository>();
            services.AddTransient<ICompletedLabelWorkingDocumentPresentedRepository, OCompletedLabelWorkingDocumentPresentedRepository>();
            services.AddTransient<ICompletedPresentedTypeDimensionRepository, OCompletedPresentedTypeDimensionRepository>();
            services.AddTransient<ICompletedUserDimensionRepository, OCompletedUserDimensionRepository>();
            services.AddTransient<ICompletedWorkingDocumentFactRepository, OCompletedWorkingDocumentFactRepository>();
            services.AddTransient<ICompletedWorkingDocumentPresentedFactRepository, OCompletedWorkingDocumentPresentedFactRepository>();
            services.AddTransient<ICountryStateRepository, OCountryStateRepository>();

            services.AddTransient<IErrorLogRepository, OErrorLogRepository>();
            services.AddTransient<IESProbeRepository, OESProbeRepository>();
            services.AddTransient<IESSensorReadingRepository, OESSensorReadingRepository>();
            services.AddTransient<IESSensorRepository, OESSensorRepository>();
            services.AddTransient<IExecutionHandlerQueueRepository, OExecutionHandlerQueueRepository>();
            services.AddTransient<IFacilityStructureRepository, OFacilityStructureRepository>();
            services.AddTransient<IFinishedStatusRepository, OFinishedStatusRepository>();
            services.AddTransient<IGatewayCustomerRepository, OGatewayCustomerRepository>();
            services.AddTransient<IGlobalSettingsRepository, OGlobalSettingsRepository>();
            services.AddTransient<IGpsLocationRepository, OGpsLocationRepository>();
            services.AddTransient<IHierarchyRepository, OHierarchyRepository>();
            services.AddTransient<IHierarchyBucketOverrideTypeRepository, OHierarchyBucketOverrideTypeRepository>();
            services.AddTransient<OImportFieldMappingRepository>();
            services.AddTransient<IInvoiceRepository, OInvoiceRepository>();
            services.AddTransient<IJSFunctionRepository, OJSFunctionRepository>();
            services.AddTransient<ILabelRepository, OLabelRepository>();
            services.AddTransient<ILanguageRepository, OLanguageRepository>();
            services.AddTransient<ILanguageTranslationRepository, OLanguageTranslationRepository>();
            services.AddTransient<IMeasurementUnitRepository, OMeasurementUnitRepository>();
            services.AddTransient<IMediaRepository, OMediaRepository>();
            services.AddTransient<IMembershipMediaRepository, OMembershipMediaRepository>();
            services.AddTransient<IMembershipRepository, OMembershipRepository>();
            services.AddTransient<IMembershipTypeContextPublishedResource, OMembershipTypeContextPublishedResource>();
            services.AddTransient<IMembershipTypeRepository, OMembershipTypeRepository>();
            services.AddTransient<IMimeTypeRepository, OMimeTypeRepository>();
            services.AddTransient<INewsFeedRepository, ONewsFeedRepository>();
            services.AddTransient<IOrderRepository, OOrderRepository>();
            services.AddTransient<IPaymentResponseRepository, OPaymentResponseRepository>();
            services.AddTransient<IPEJobRepository, OPEJobRepository>();
            services.AddTransient<IPendingPaymentRepository, OPendingPaymentRepository>();
            services.AddTransient<IPeriodicExecutionHistoryRepository, OPeriodicExecutionHistoryRepository>();
            services.AddTransient<IProductCatalogItemRepository, OProductCatalogItemRepository>();
            services.AddTransient<IProductCatalogRepository, OProductCatalogRepository>();
            services.AddTransient<IProductRepository, OProductRepository>();
            services.AddTransient<IProjectJobTaskWorkingDocumentRepository, OProjectJobTaskWorkingDocumentRepository>();
            services.AddTransient<IPublicTicketRepository, OPublicTicketRepository>();
            services.AddTransient<IPublishingGroupActorRepository, OPublishingGroupActorRepository>();
            services.AddTransient<IPublishingGroupRepository, OPublishingGroupRepository>();
            services.AddTransient<IPublishingGroupResourceRepository, OPublishingGroupResourceRepository>();
            services.AddTransient<IQRCodeRepository, OQRCodeRepository>();
            services.AddTransient<IReportingRepository, OReportingRepository>();
            services.AddTransient<IReportManagementRepository, OReportManagementRepository>();
            services.AddTransient<IReportTicketRepository, OReportTicketRepository>();
            services.AddTransient<IResourceRepository, OResourceRepository>();
            services.AddTransient<IRoleRepository, ORoleRepository>();
            services.AddTransient<IRosterRepository, ORosterRepository>();
            services.AddTransient<IScheduledTaskRepository, OScheduledTaskRepository>();
            services.AddTransient<ISequenceRepository, OSequenceRepository>();
            services.AddTransient<ISiteFeatureRepository, OSiteFeatureRepository>();
            services.AddTransient<IStatusRepository, OStatusRepository>();
            services.AddTransient<ITabletProfileRepository, OTabletProfileRepository>();
            services.AddTransient<ITabletRepository, OTabletRepository>();
            services.AddTransient<ITaskChangeRequestRepository, OTaskChangeRequestRepository>();
            services.AddTransient<ITaskRepository, OTaskRepository>();
            services.AddTransient<ITaskTypeRepository, OTaskTypeRepository>();
            services.AddTransient<ITaskWorkLogRepository, OTaskWorkLogRepository>();
            services.AddTransient<ITimesheetItemRepository, OTimesheetItemRepository>();
            services.AddTransient<ITimesheetItemTypeRepository, OTimesheetItemTypeRepository>();
            services.AddTransient<IUserAgentRepository, OUserAgentRepository>();
            services.AddTransient<IUserBillingTypeRepository, OUserBillingTypeRepository>();
            services.AddTransient<IVersionsRepository, OVersionsRepository>();
            services.AddTransient<IWorkingDocumentPublicVariableRepository, OWorkingDocumentPublicVariableRepository>();
            services.AddTransient<IWorkingDocumentReferenceRepository, OWorkingDocumentReferenceRepository>();
            services.AddTransient<IWorkingDocumentRepository, OWorkingDocumentRepository>();
            services.AddTransient<IWorkLoadingActivityRepository, OWorkLoadingActivityRepository>();
            services.AddTransient<IWorkLoadingBookRepository, OWorkLoadingBookRepository>();
            services.AddTransient<IWorkLoadingFeatureRepository, OWorkLoadingFeatureRepository>();

            services.AddDataWarehousingTenantMasterPopulateLLBLGen();
            services.AddDataWarehousingTenantPopulateLLBLGen();

            // None Repositories??
            services.AddTransient<IMediaPolicyManager, MediaPolicyManager>();
            services.AddTransient<MembershipImportParser>();
            services.AddTransient<ProductCatalogExportParser>();
            services.AddTransient<ProductCatalogImportWriter>();
            services.AddTransient<QRCodeImportWriter>();
            services.AddTransient<QRCodeExportParser>();
            services.AddTransient<Func<Type, IPeriodicExecutionHandler>>(serviceProvider => type => serviceProvider.GetService(type) as IPeriodicExecutionHandler);
        }
    }
}