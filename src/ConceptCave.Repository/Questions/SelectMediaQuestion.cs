﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Editor;
using ConceptCave.Checklist.RunTime;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.RepositoryInterfaces.Enums;

namespace ConceptCave.Repository.Questions
{
    public class SelectMediaQuestion : Question
    {
        public override object DefaultAnswer
        {
            get
            {
                return _defaultAnswer;
            }
            set
            {
                if (value is Guid)
                {
                    _defaultAnswer = value;
                }
                else if (value is SelectMediaAnswer)
                {
                    _defaultAnswer = ((SelectMediaAnswer)value).AnswerValue;
                }
            }
        }

        public override Checklist.Interfaces.IAnswer CreateBlankAnswer(Checklist.Interfaces.IPresentedQuestion presentedQuestion)
        {
            var result = new SelectMediaAnswer() { PresentedQuestion = presentedQuestion };

            if (DefaultAnswer != null && DefaultAnswer is Guid)
            {
                result.AnswerValue = DefaultAnswer;
            }
            else if (DefaultAnswer != null && DefaultAnswer is IHasMediaId)
            {
                result.AnswerValue = ((IHasMediaId)DefaultAnswer).MediaId;
            }

            return result;
        }
    }

    public class SelectMediaAnswer : Answer, IHasMediaId
    {
        protected MediaEntity _selectedMedia;
        protected Guid? _selectedMediaId;

        /* Can't do this here as SelectMediaAnswer is not injected??
                public MediaEntity SelectedMedia
                {
                    get
                    {
                        if (SelectedMediaId.HasValue == false)
                        {
                            return null;
                        }

                        if (_selectedMedia == null)
                        {
                            _selectedMedia = MediaRepository.GetById(_selectedMediaId.Value, MediaLoadInstruction.None);
                        }

                        return _selectedMedia;
                    }
                }
        */
        public Guid? SelectedMediaId
        {
            get
            {
                return _selectedMediaId;
            }
            set
            {
                UtcDateAnswered = DateTime.UtcNow;
                _selectedMediaId = value;

                if (_selectedMediaId == Guid.Empty)
                {
                    _selectedMediaId = null;
                }

                _selectedMedia = null;
            }
        }

        public override object AnswerValue
        {
            get
            {
                return SelectedMediaId;
            }
            set
            {
                if (value != null)
                {
                    SelectedMediaId = (Guid)value;
                }
                else
                {
                    SelectedMediaId = null;
                }
            }
        }

        public override string AnswerAsString
        {
            get
            {
                if (SelectedMediaId.HasValue == false)
                {
                    return string.Empty;
                }

                return SelectedMediaId.ToString();
                /*                 if (SelectedMedia == null)
                                {
                                    return "No Media";
                                }

                                return SelectedMedia.Name;
                */
            }
        }

        public override void Encode(Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("SelectedMediaId", SelectedMediaId);
        }

        public override void Decode(Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            SelectedMediaId = decoder.Decode<Guid?>("SelectedMediaId", null);
        }

        #region IHasMediaId Members

        public Guid MediaId
        {
            get { return SelectedMediaId.HasValue ? SelectedMediaId.Value : Guid.Empty; }
        }

        #endregion
    }
}
