using ConceptCave.Checklist;
using ConceptCave.Checklist.Editor;
using ConceptCave.Checklist.RunTime;
using ConceptCave.Core.Coding;

namespace ConceptCave.Repository
{
    public static class WorkingDocumentHelper
    {
        public static string WorkingDocumentToString(WorkingDocument workingDoc)
        {
            IEncoder encoder = CoderFactory.CreateEncoder();
            encoder.Encode(WorkingDocument.WorkingDocumentRootElementName, workingDoc);

            return encoder.ToString();
        }

        public static WorkingDocument CreateWorkingDocumentFromDesignDocument(DesignDocument designDocument)
        {
            if (string.IsNullOrEmpty(designDocument.ProgramDefinition.SourceCode) == true)
            {
                designDocument.ProgramDefinition.SourceCode = "present all sections";
            }

            WorkingDocument doc = new WorkingDocument(designDocument);

            return doc;
        }
    }
}