﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.BusinessLogic.Facilities;
using ConceptCave.Checklist.Facilities;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.Lingo.ObjectProviders;
using ConceptCave.Checklist.Reporting.LingoQuery;
using ConceptCave.Core;
using ConceptCave.Repository.Facilities.FileHelpers;

namespace ConceptCave.Repository.Facilities
{
    public class XlsxFacility : Facility
    {
        public class XlsxFacilityReader : MediaItemReader<StandardRecord, ReaderField>
        {
            internal ClosedXML.Excel.XLWorkbook workBook { get; set; }
            private ClosedXML.Excel.IXLWorksheet workSheet { get; set; }

            private ClosedXML.Excel.IXLRow currentRow { get; set; }

            /// <summary>
            /// Dictates wether or not the reader will throw an exception if an attempt to read a field that isn't present in the source spreadsheet is made.
            /// For example if this is true and there is a spreadsheet that has values in columns a,b,c,e,f and an attempt is made to access columd d, then
            /// the call will result in an exception. 
            /// </summary>
            public bool EnforceFieldsMatchSource { get; set; }

            protected IMediaItem MediaItem { get; set; }

            public XlsxFacilityReader() {
            
            }

            public XlsxFacilityReader(IContextContainer context, IMediaItem mediaItem)
            {
                MediaItem = mediaItem;

                CreateWorkBook(context, mediaItem);
                LoadWorkSheet(context, 1);
            }

            protected void CreateWorkBook(IContextContainer context, IMediaItem mediaItem)
            {
                using (var ms = new System.IO.MemoryStream(mediaItem.GetData(context)))
                {
                    workBook = new ClosedXML.Excel.XLWorkbook(ms, ClosedXML.Excel.XLEventTracking.Disabled);
                }
            }

            public void LoadWorkSheet(IContextContainer context, int index)
            {
                workSheet = workBook.Worksheet(index);
                currentRow = workSheet.FirstRowUsed();
            }

            public void LoadWorkSheet(IContextContainer context, string name)
            {
                workSheet = workBook.Worksheet(name);
                currentRow = workSheet.FirstRowUsed();
            }

            public virtual void MoveTo(int rowNum)
            {
                currentRow = workSheet.Row(rowNum);

                CreateCurrentRecord();
            }

            public virtual void MoveBy(int step)
            {
                currentRow = currentRow.RowBelow(step);

                CreateCurrentRecord();
            }

            public override bool MoveNext()
            {
                currentRow = currentRow.RowBelow();

                var result = currentRow.IsEmpty();

                if (result == false)
                {
                    CreateCurrentRecord();
                }

                return !result;
            }

            private void CreateCurrentRecord()
            {
                currentRecord = new StandardRecord();
                currentRecord.EnforceFieldsMatchSource = EnforceFieldsMatchSource;
                currentRecord.Fields = new ListOf<ReaderField>();

                foreach (var cell in currentRow.CellsUsed())
                {
                    ReaderField field = new ReaderField() { Name = cell.Address.ColumnLetter, Value = cell.Value.ToString() };

                    currentRecord.Fields.Items.Add(field);
                }
            }

            public override void Reset()
            {
                currentRow = workSheet.FirstRowUsed();
            }

            public override void Encode(Core.Coding.IEncoder encoder)
            {
                base.Encode(encoder);

                encoder.Encode("MediaItem", MediaItem);
                encoder.Encode("CurrentWorkSheet", workSheet.Name);
                encoder.Encode("CurrentRecordIndex", currentRow.RowNumber());
                encoder.Encode("EnforceFieldsMatchSource", EnforceFieldsMatchSource);
            }

            public override void Decode(Core.Coding.IDecoder decoder)
            {
                base.Decode(decoder);

                MediaItem = decoder.Decode<IMediaItem>("MediaItem");
                string currentSheet = decoder.Decode<string>("CurrentWorkSheet");
                int currentRec = decoder.Decode<int>("CurrentRecordIndex");
                EnforceFieldsMatchSource = decoder.Decode<bool>("EnforceFieldsMatchSource", false);

                CreateWorkBook(decoder.Context, MediaItem);
                LoadWorkSheet(decoder.Context, currentSheet);

                currentRow = workSheet.Row(currentRec);

                CreateCurrentRecord();
            }
        }

        public class XlsxFacilityWriter : Node, IMediaItem
        {
            ClosedXML.Excel.XLWorkbook WorkBook { get; set; }

            public XlsxFacilityWriter()
                : base()
            {
                WorkBook = new ClosedXML.Excel.XLWorkbook();

                MediaType = "application/vnd.ms-excel";
            }

            public XlsxFacilityWriter(IContextContainer context, IMediaItem source)
                : this()
            {
                System.IO.MemoryStream stream = new System.IO.MemoryStream(source.GetData(context));

                Name = source.Name;

                WorkBook = new ClosedXML.Excel.XLWorkbook(stream, ClosedXML.Excel.XLEventTracking.Disabled);
            }

            public override void Decode(Core.Coding.IDecoder decoder)
            {
                base.Decode(decoder);

                byte[] data = decoder.Decode<byte[]>("WorkBook");
                System.IO.MemoryStream stream = new System.IO.MemoryStream(data);

                Name = decoder.Decode<string>("Name");
                WorkBook = new ClosedXML.Excel.XLWorkbook(stream, ClosedXML.Excel.XLEventTracking.Disabled);
            }

            public override void Encode(Core.Coding.IEncoder encoder)
            {
                base.Encode(encoder);

                System.IO.MemoryStream stream = new System.IO.MemoryStream();
                WorkBook.SaveAs(stream);

                encoder.Encode<string>("Name", Name);
                encoder.Encode<byte[]>("WorkBook", stream.ToArray());
            }

            protected ClosedXML.Excel.IXLWorksheet GetOrCreateWorksheet(string workSheetName)
            {
                ClosedXML.Excel.IXLWorksheet sheet = null;

                if (string.IsNullOrEmpty(workSheetName) == false)
                {
                    if (WorkBook.Worksheets.TryGetWorksheet(workSheetName, out sheet) == false)
                    {
                        sheet = WorkBook.Worksheets.Add(workSheetName);
                    }
                }
                else
                {
                    sheet = WorkBook.Worksheets.Add("Sheet 1");
                }

                return sheet;
            }

            public void InsertDataTable(IContextContainer context, LingoDatatable data, int startRow, int startCol, string workSheetName)
            {
                InsertDataTable(context, data, data.ColumnNames, startRow, startCol, workSheetName);
            }

            public void InsertDataTable(IContextContainer context, LingoDatatable data, IEnumerable<string> columns, int startRow, int startCol, string workSheetName)
            {
                var sheet = GetOrCreateWorksheet(workSheetName);

                LingoDatatable table = (LingoDatatable)data;

                int row = startRow;
                foreach (var tableRow in table)
                {
                    int col = startCol;

                    foreach (var tableCol in columns)
                    {
                        sheet.Cell(row, col).Value = tableRow.GetColumn(tableCol);

                        col = col + 1;
                    }

                    row++;
                }
            }

            public void InsertList(IContextContainer context, IEnumerable list, int startRow, int startCol, string workSheetName)
            {
                List<string> columns = new List<string>();
                foreach (var item in list)
                {
                    if (item is DictionaryObjectProvider)
                    {
                        foreach (var prop in ((DictionaryObjectProvider)item).Properties)
                        {
                            columns.Add(prop.ToString());
                        }

                        break;
                    }
                }

                InsertList(context, list, columns, startRow, startCol, workSheetName);
            }

            protected ClosedXML.Excel.IXLWorksheet GetWorkSheet(string workSheetName)
            {
                ClosedXML.Excel.IXLWorksheet sheet = null;

                if (string.IsNullOrEmpty(workSheetName) == false)
                {
                    if (WorkBook.Worksheets.TryGetWorksheet(workSheetName, out sheet) == false)
                    {
                        sheet = WorkBook.Worksheets.Add(workSheetName);
                    }
                }

                if (sheet == null)
                {
                    sheet = WorkBook.Worksheet(0);
                }

                return sheet;
            }

            public void InsertList(IContextContainer context, IEnumerable list, IEnumerable<string> columns, int startRow, int startCol, string workSheetName)
            {
                ClosedXML.Excel.IXLWorksheet sheet = GetWorkSheet(workSheetName);

                int row = startRow;
                foreach (var item in list)
                {
                    if ((item is DictionaryObjectProvider) == false)
                    {
                        continue;
                    }

                    var i = (DictionaryObjectProvider)item;

                    int col = startCol;

                    foreach (var tableCol in columns)
                    {
                        sheet.Cell(row, col).Value = i.GetValue(context, tableCol);

                        col = col + 1;
                    }

                    row++;
                }
            }

            /// <summary>
            /// Merges the current working document into the Excel worksheet through string expansions placed in the worksheet
            /// </summary>
            /// <param name="context"></param>
            /// <param name="startRow"></param>
            /// <param name="startCol"></param>
            /// <param name="endRow"></param>
            /// <param name="endCol"></param>
            public void Merge(IContextContainer context, int? startRow, int? startCol, int? endRow, int? endCol)
            {
                foreach (var sheet in WorkBook.Worksheets)
                {
                    Merge(context, startRow, startCol, endRow, endCol, sheet);
                }
            }

            public void Merge(IContextContainer context, int? startRow, int? startCol, int? endRow, int? endCol, ClosedXML.Excel.IXLWorksheet sheet)
            {
                int sr = 0;
                int sc = 0;
                int er = 0;
                int ec = 0;

                if (startRow.HasValue == true)
                {
                    sr = startRow.Value;
                }
                else
                {
                    sr = sheet.FirstRowUsed().RowNumber();
                }

                if (startCol.HasValue == true)
                {
                    sc = startCol.Value;
                }
                else
                {
                    sc = sheet.FirstColumnUsed().ColumnNumber();
                }

                if (endRow.HasValue == true)
                {
                    er = endRow.Value;
                }
                else
                {
                    er = sheet.LastRowUsed().RowNumber();
                }

                if (endCol.HasValue == true)
                {
                    ec = endCol.Value;
                }
                else
                {
                    ec = sheet.LastColumnUsed().ColumnNumber();
                }

                var range = sheet.Range(sheet.Cell(sr, sc).Address, sheet.Cell(er, ec).Address);

                ILingoProgram program = context.Get<ILingoProgram>();
                foreach (var cell in range.CellsUsed())
                {
                    string value = program.ExpandString(cell.GetString());

                    cell.Value = value;
                }
            }

            public void Merge(IContextContainer context, int? startRow, int? startCol, int? endRow, int? endCol, string workSheetName)
            {
                ClosedXML.Excel.IXLWorksheet sheet = GetWorkSheet(workSheetName);

                Merge(context, startRow, startCol, endRow, endCol, sheet);
            }

            /// <summary>
            /// Copies all the data from one worksheet to another in another document.
            /// </summary>
            /// <param name="context">Current context</param>
            /// <param name="source">Source from which data is to be copied</param>
            /// <param name="sourceWorksheet">Name or worksheet in source to be copied</param>
            /// <param name="destinationWorksheet">Destination worksheet, if it doesn't exist it will be created</param>
            public void Copy(IContextContainer context, XlsxFacilityReader reader, string sourceWorksheet, string destinationWorksheet)
            {
                Copy(context, reader, sourceWorksheet, null, null, null, null, destinationWorksheet, null, null);
            }

            public void Copy(IContextContainer context, XlsxFacilityReader reader, string sourceWorksheet, object sourceLeft, object sourceTop, object sourceRight, object sourceBottom, string destinationWorksheet, object destinationLeft, object destinationTop)
            {
                ClosedXML.Excel.IXLWorksheet source = null;

                if (reader.workBook.TryGetWorksheet(sourceWorksheet, out source) == false)
                {
                    return;
                }

                var destination = GetOrCreateWorksheet(destinationWorksheet);

                var firstCell = source.FirstCellUsed();
                if(sourceLeft != null && sourceLeft is int && sourceTop != null && sourceTop is int)
                {
                    firstCell = source.Cell((int)sourceTop, (int)sourceLeft);
                }

                var lastCell = source.LastCellUsed();
                if(sourceRight != null && sourceRight is int && sourceBottom != null && sourceBottom is int)
                {
                    lastCell = source.Cell((int)sourceBottom, (int)sourceRight);
                }

                var range = source.Range(firstCell.Address, lastCell.Address);

                int l = 1;
                int t = 1;
                if(destinationLeft != null && destinationLeft is int)
                {
                    l = (int)destinationLeft;
                }
                if(destinationTop != null && destinationTop is int)
                {
                    t = (int)destinationTop;
                }

                range.CopyTo(destination.Cell(l, t));
                //destination.Cell(l,t).Value = range;
            }

            #region IMediaItem Members

            public string Name { get; set; }

            public string MediaType { get; set; }

            public byte[] GetData(IContextContainer context)
            {
                System.IO.MemoryStream stream = new System.IO.MemoryStream();

                WorkBook.SaveAs(stream);

                return stream.ToArray();
            }

            #endregion
        }

        public XlsxFacility()
        {
            Name = "Xlsx";
        }

        public XlsxFacilityReader ReadMedia(IContextContainer context, IMediaItem item)
        {
            return new XlsxFacilityReader(context, item);
        }

        public XlsxFacilityWriter Writer(IContextContainer context, IMediaItem item)
        {
            return new XlsxFacilityWriter(context, item);
        }

        public XlsxFacilityWriter WriteDataTable(IContextContainer context, LingoDatatable table)
        {
            XlsxFacilityWriter writer = new XlsxFacilityWriter();
            writer.InsertDataTable(context, table, 1, 1, null);

            return writer;
        }

        public XlsxFacilityWriter WriteList(IContextContainer context, object list)
        {
            XlsxFacilityWriter writer = new XlsxFacilityWriter();

            if (list is IEnumerable)
            {
                writer.InsertList(context, (IEnumerable)list, 1, 1, null);
            }

            return writer;
        }

        protected List<string> ExtractColumns(LingoDatatable table, object columns)
        {
            var enumerable = columns as System.Collections.IEnumerable;

            if (enumerable == null)
            {
                enumerable = table.ColumnNames;
            }

            List<string> cols = new List<string>();
            foreach (var c in enumerable)
            {
                cols.Add(c.ToString());
            }

            return cols;
        }

        protected List<string> ExtractColumns(object columns)
        {
            var enumerable = columns as System.Collections.IEnumerable;

            if (enumerable == null)
            {
                return null;
            }

            List<string> cols = new List<string>();
            foreach (var c in enumerable)
            {
                cols.Add(c.ToString());
            }

            return cols;
        }

        public XlsxFacilityWriter WriteDataTable(IContextContainer context, LingoDatatable table, object columns)
        {
            var cols = ExtractColumns(table, columns);

            XlsxFacilityWriter writer = new XlsxFacilityWriter();
            writer.InsertDataTable(context, table, cols, 1, 1, null);

            return writer;
        }

        public XlsxFacilityWriter WriteDataTable(IContextContainer context, IMediaItem template, LingoDatatable table, int startRow, int startCol, string workSheet, object columns)
        {
            var cols = ExtractColumns(table, columns);
            XlsxFacilityWriter writer = new XlsxFacilityWriter(context, template);
            writer.InsertDataTable(context, table, cols, startRow, startCol, workSheet);

            return writer;
        }

        public XlsxFacilityWriter WriteList(IContextContainer context, IMediaItem template, object list, int startRow, int startCol, string workSheet, object columns)
        {
            XlsxFacilityWriter writer = new XlsxFacilityWriter(context, template);

            if (list is IEnumerable)
            {
                var c = ExtractColumns(columns);

                if (c == null)
                {
                    writer.InsertList(context, (IEnumerable)list, startRow, startCol, workSheet);
                }
                else
                {
                    writer.InsertList(context, (IEnumerable)list, c, startRow, startCol, workSheet);
                }
            }

            return writer;
        }

        public XlsxFacilityWriter Merge(IContextContainer context, IMediaItem template, string workSheet)
        {
            XlsxFacilityWriter writer = new XlsxFacilityWriter(context, template);
            writer.Merge(context, null, null, null, null, workSheet);

            return writer;
        }

        public XlsxFacilityWriter Merge(IContextContainer context, IMediaItem template)
        {
            XlsxFacilityWriter writer = new XlsxFacilityWriter(context, template);
            writer.Merge(context, null, null, null, null);

            return writer;
        }

        public XlsxFacilityWriter Merge(IContextContainer context, IMediaItem template, string workSheet, int startRow, int startCol, int endRow, int endCol)
        {
            XlsxFacilityWriter writer = new XlsxFacilityWriter(context, template);
            writer.Merge(context, startRow, startCol, endRow, endCol, workSheet);

            return writer;
        }

        public XlsxFacilityWriter Merge(IContextContainer context, IMediaItem template, int startRow, int startCol, int endRow, int endCol)
        {
            XlsxFacilityWriter writer = new XlsxFacilityWriter(context, template);
            writer.Merge(context, startRow, startCol, endRow, endCol);

            return writer;
        }
    }
}
