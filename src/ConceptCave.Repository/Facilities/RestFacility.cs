﻿using System;
using System.Collections.Generic;
using ConceptCave.Checklist.Facilities;
using ConceptCave.Core;
using RestSharp;
using ConceptCave.Repository.Facilities.OData;
using RestSharp.Authenticators;
using ConceptCave.Checklist;

namespace ConceptCave.Repository.Facilities
{
    public interface IRestFacilityAuthenticationHeader
    {
        void AddAuthentication(RestClient client);
    }

    public class RestFacility : Facility
    {
        public class RestFacilityBasicAuthenticationHeader : Node, IRestFacilityAuthenticationHeader
        {
            public string Username { get; set; }
            public string Password { get; set; }

            public override void Encode(Core.Coding.IEncoder encoder)
            {
                base.Encode(encoder);

                encoder.Encode<string>("Username", Username);
                encoder.Encode<string>("Password", Password);
            }

            public override void Decode(Core.Coding.IDecoder decoder)
            {
                base.Decode(decoder);

                Username = decoder.Decode<string>("Username");
                Password = decoder.Decode<string>("Password");
            }

            public void AddAuthentication(RestClient client)
            {
                client.Authenticator = new HttpBasicAuthenticator(Username, Password);
            }
        }

        public class RestFacilityProxy : Node
        {
            public string Url { get; set; }
            public int Port { get; set; }

            public override void Encode(Core.Coding.IEncoder encoder)
            {
                base.Encode(encoder);

                encoder.Encode<string>("Url", Url);
                encoder.Encode<int>("Port", Port);
            }

            public override void Decode(Core.Coding.IDecoder decoder)
            {
                base.Decode(decoder);

                Url = decoder.Decode<string>("Url");
                Port = decoder.Decode<int>("Port");
            }
        }

        public class RestFacilityRequestParameter : Node
        {
            public string Name { get; set; }
            public string Value { get; set; }

            public override void Encode(Core.Coding.IEncoder encoder)
            {
                base.Encode(encoder);

                encoder.Encode<string>("Name", Name);
                encoder.Encode<string>("Value", Value);
            }

            public override void Decode(Core.Coding.IDecoder decoder)
            {
                base.Decode(decoder);

                Name = decoder.Decode<string>("Name");
                Value = decoder.Decode<string>("Value");
            }
        }

        public class RestFacilityResponse : Node
        {
            public bool HasError { get; set; }
            public string ErrorMessage { get; set; }

            public string Status { get; set; }
            public int StatusCode { get; set; }
            public string Content { get; set; }
            public bool IsOK
            {
                get
                {
                    return StatusCode >= 200 && StatusCode <= 299;
                }
            }

            public RestFacilityResponse() : base() { }

            public RestFacilityResponse(IRestResponse response)
                : base()
            {
                HasError = string.IsNullOrEmpty(response.ErrorMessage) == false;
                ErrorMessage = response.ErrorMessage;

                Status = response.StatusCode.ToString();
                StatusCode = (int)response.StatusCode;
                Content = response.Content;
            }

            public override void Encode(Core.Coding.IEncoder encoder)
            {
                base.Encode(encoder);

                encoder.Encode<bool>("HasError", HasError);
                encoder.Encode<string>("ErrorMessage", ErrorMessage);
                encoder.Encode<string>("Status", Status);
                encoder.Encode<int>("StatusCode", StatusCode);
                encoder.Encode<string>("Content", Content);
            }

            public override void Decode(Core.Coding.IDecoder decoder)
            {
                base.Decode(decoder);

                HasError = decoder.Decode<bool>("HasError");
                ErrorMessage = decoder.Decode<string>("ErrorMessage");
                Status = decoder.Decode<string>("Status");
                StatusCode = decoder.Decode<int>("StatusCode");
                Content = decoder.Decode<string>("Content");
            }
        }

        public class RestFacilityClient : Node
        {
            public string BaseUrl { get; set; }

            public IRestFacilityAuthenticationHeader Authenticator { get; set; }

            public RestFacilityProxy Proxy { get; set; }

            public override void Encode(Core.Coding.IEncoder encoder)
            {
                base.Encode(encoder);

                encoder.Encode<string>("BaseUrl", BaseUrl);
                encoder.Encode<IRestFacilityAuthenticationHeader>("Authenticator", Authenticator);
                encoder.Encode<RestFacilityProxy>("Proxy", Proxy);
            }

            public override void Decode(Core.Coding.IDecoder decoder)
            {
                base.Decode(decoder);

                BaseUrl = decoder.Decode<string>("BaseUrl");
                Authenticator = decoder.Decode<IRestFacilityAuthenticationHeader>("Authenticator");
                Proxy = decoder.Decode<RestFacilityProxy>("Proxy");
            }

            public virtual RestFacilityResponse Execute(IContextContainer context, RestFacilityRequest request)
            {
                var client = new RestClient(BaseUrl.TrimEnd('/'));
                if (Authenticator != null)
                {
                    Authenticator.AddAuthentication(client);
                }


                if (Proxy != null)
                {
                    client.Proxy = new System.Net.WebProxy(Proxy.Url, Proxy.Port);
                }

                return request.Execute(context, client);
            }
        }

        public class RestFacilityRequest : Node
        {
            protected Method Method { get; set; }
            protected DataFormat Format { get; set; }

            protected List<RestFacilityRequestParameter> Headers { get; set; }
            protected List<RestFacilityRequestParameter> Parameters { get; set; }
            public string Resource { get; set; }

            public string Body { get; set; }

            public RestFacilityRequest() : this(Method.POST, true)
            {
            }

            public RestFacilityRequest(Method method, bool useJson)
            {
                this.Method = method;
                this.Format = useJson == true ? DataFormat.Json : DataFormat.Xml;
                Headers = new List<RestFacilityRequestParameter>();
                Parameters = new List<RestFacilityRequestParameter>();
            }

            public override void Encode(Core.Coding.IEncoder encoder)
            {
                base.Encode(encoder);

                encoder.Encode<string>("Method", Method.ToString());
                encoder.Encode<string>("Format", Format.ToString());
                encoder.Encode<string>("Resource", Resource);
                encoder.Encode<List<RestFacilityRequestParameter>>("Headers", Headers);
                encoder.Encode<List<RestFacilityRequestParameter>>("Parameters", Parameters);
                encoder.Encode<string>("Body", Body);

            }

            public override void Decode(Core.Coding.IDecoder decoder)
            {
                base.Decode(decoder);

                Method = (Method)Enum.Parse(typeof(Method), decoder.Decode<string>("Method", Method.POST.ToString()));
                Format = (DataFormat)Enum.Parse(typeof(DataFormat), decoder.Decode<string>("Format", DataFormat.Json.ToString()));
                Resource = decoder.Decode<string>("Resource");
                Headers = decoder.Decode<List<RestFacilityRequestParameter>>("Headers", new List<RestFacilityRequestParameter>());
                Parameters = decoder.Decode<List<RestFacilityRequestParameter>>("Parameters", new List<RestFacilityRequestParameter>());
                Body = decoder.Decode<string>("Body");

            }

            public void AddHeader(string name, string value)
            {
                Headers.Add(new RestFacilityRequestParameter() { Name = name, Value = value });
            }

            public void AddParameter(string name, string value)
            {
                Parameters.Add(new RestFacilityRequestParameter() { Name = name, Value = value });
            }

            internal virtual RestFacilityResponse Execute(IContextContainer context, RestClient client)
            {
                return Execute(context, client, Headers, Parameters);
            }

            internal virtual RestFacilityResponse Execute(IContextContainer context, RestClient client, List<RestFacilityRequestParameter> headers, List<RestFacilityRequestParameter> parameters)
            {
                var request = new RestRequest();
                request.Resource = Resource.TrimStart('/');
                request.Method = this.Method;
                request.RequestFormat = this.Format;

                headers.ForEach(h => request.AddHeader(h.Name, h.Value));
                parameters.ForEach(p => request.AddParameter(p.Name, p.Value));
                if (string.IsNullOrEmpty(Body) == false)
                {
                    request.AddParameter(this.Format == DataFormat.Json ? "application/json" : "text/xml", Body, ParameterType.RequestBody);
                }

                var response = client.Execute(request);

                return WrapRestResponse(response);
            }

            internal virtual RestFacilityResponse WrapRestResponse(IRestResponse response)
            {
                return new RestFacilityResponse(response);
            }
        }

        private readonly IServerUrls _serverUrls;

        public RestFacility(IServerUrls serverUrls)
            : base()
        {
            Name = "Rest";
            _serverUrls = serverUrls;
        }

        public RestFacilityClient NewClient(string baseUrl)
        {
            return new RestFacilityClient() { BaseUrl = baseUrl };
        }

        public RestFacilityClient NewClient(string baseUrl, object authenticator)
        {
            return new RestFacilityClient() { BaseUrl = baseUrl, Authenticator = (IRestFacilityAuthenticationHeader)authenticator };
        }

        public RestFacilityClient NewODataClient(object authenticator)
        {
            return NewODataClient("", authenticator);
        }

        public RestFacilityClient NewODataClient(string baseUrl)
        {
            return new RestFacilityClient() { BaseUrl = string.IsNullOrEmpty(baseUrl) ? _serverUrls.ODataV3Url : baseUrl };
        }

        public RestFacilityClient NewODataClient(string baseUrl, object authenticator)
        {
            return new RestFacilityClient() { BaseUrl = string.IsNullOrEmpty(baseUrl) ? _serverUrls.ODataV3Url : baseUrl, Authenticator = (IRestFacilityAuthenticationHeader)authenticator };
        }

        public RestFacilityRequest NewPostRequest(string resource, bool useJson)
        {
            return new RestFacilityRequest(Method.POST, useJson) { Resource = resource };
        }

        public RestFacilityRequest NewGetRequest(string resource, bool useJson)
        {
            return new RestFacilityRequest(Method.GET, useJson) { Resource = resource };
        }

        public RestFacilityRequest NewODataRequest(string resource)
        {
            return new ODataRestFacilityRequest() { Resource = resource };
        }

        public RestFacilityBasicAuthenticationHeader BasicAuthentication(string username, string password)
        {
            return new RestFacilityBasicAuthenticationHeader() { Username = username, Password = password };
        }

        public RestFacilityProxy NewProxy(string url, int port)
        {
            return new RestFacilityProxy() { Url = url, Port = port };
        }
    }
}
