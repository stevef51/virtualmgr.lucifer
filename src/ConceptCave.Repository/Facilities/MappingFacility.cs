﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Facilities;
using ConceptCave.Core;
using RestSharp;

namespace ConceptCave.Repository.Facilities
{
    public class MappingFacility : Facility
    {
        public class GeoSearchCriteria : Node
        {
            public string Search { get; set; }
            public int ResultLimit { get; set; }
            public DataFormat Format { get; set; }
            public string Source { get; set; }
            public string Encoding { get; set; }
            public string Locale { get; set; }

            public GeoSearchCriteria()
                : base()
            {
                ResultLimit = 10;
                Format = DataFormat.Json;
            }

            public override void Encode(Core.Coding.IEncoder encoder)
            {
                base.Encode(encoder);

                encoder.Encode("Search", Search);
                encoder.Encode("ResultLimit", ResultLimit);
                encoder.Encode("Format", Format);
                encoder.Encode("Source", Source);
                encoder.Encode("Encoding", Encoding);
                encoder.Encode("Locale", Locale);
            }

            public override void Decode(Core.Coding.IDecoder decoder)
            {
                base.Decode(decoder);

                Search = decoder.Decode<string>("Search");
                ResultLimit = decoder.Decode<int>("ResultLimit", 10);
                Format = decoder.Decode<DataFormat>("Format");
                Source = decoder.Decode<string>("Source");
                Encoding = decoder.Decode<string>("Encoding");
                Locale = decoder.Decode<string>("Locale");
            }
        }

        /// <summary>
        /// Currently interacts with the CloudMade API, documented at http://cloudmade.com/documentation/geocoding
        /// </summary>
        public class GeoSearchClient : ConceptCave.Repository.Facilities.RestFacility.RestFacilityClient
        {
            /// <summary>
            /// Searches using an Address and returns geo coded information. 
            /// </summary>
            /// <param name="context"></param>
            /// <param name="criteria"></param>
            /// <returns>
            /// REST response containing the results of the request to CloudMade
            /// </returns>
            public ConceptCave.Repository.Facilities.RestFacility.RestFacilityResponse Search(IContextContainer context, GeoSearchCriteria criteria)
            {
                ConceptCave.Repository.Facilities.RestFacility.RestFacilityRequest request = new RestFacility.RestFacilityRequest(Method.GET, criteria.Format == DataFormat.Json);
                request.Resource = "geo.location.search.2";

                // ok the CloudMade v3 geocoding API expects a query string like format=json&source=OSM&enc=UTF-8&limit=10&locale=au&q=16%20Pendelton%20Place%20Lysterfield%20Australia%2012
                request.AddParameter("format", criteria.Format.ToString().ToLower());
                request.AddParameter("source", string.IsNullOrEmpty(criteria.Source) == true ? "OSM" : criteria.Source);
                request.AddParameter("enc", string.IsNullOrEmpty(criteria.Encoding) == true ? "UTF-8" : criteria.Encoding);
                request.AddParameter("limit", criteria.ResultLimit.ToString());
                request.AddParameter("locale", string.IsNullOrEmpty(criteria.Locale) == true ? "au" : criteria.Locale);
                request.AddParameter("q", criteria.Search);

                return this.Execute(context, request);
            }
        }

        public MappingFacility()
        {
            Name = "Mapping";
        }

        public GeoSearchClient NewGeoSearchClient(string baseUrl)
        {
            return new GeoSearchClient() { BaseUrl = baseUrl };
        }

        public GeoSearchCriteria NewGeoSearchCriteria()
        {
            return new GeoSearchCriteria();
        }
    }
}
