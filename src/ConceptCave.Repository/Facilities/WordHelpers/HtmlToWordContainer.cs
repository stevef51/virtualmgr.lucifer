﻿using ConceptCave.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.Repository.Facilities.WordHelpers
{
    public class HtmlToWordContainer : Node
    {
        public class HtmlToWordPage : Node
        {
            public string Name { get;set;}
            public string Content { get; set; }

            public bool PageBreakAfter { get; set; }

            public HtmlToWordPage()
            {
                PageBreakAfter = true;
            }

            public override void Encode(Core.Coding.IEncoder encoder)
            {
                base.Encode(encoder);

                encoder.Encode("Name", Name);
                encoder.Encode("Content", Content);
                encoder.Encode("PageBreakAfter", PageBreakAfter);
            }

            public override void Decode(Core.Coding.IDecoder decoder)
            {
                base.Decode(decoder);

                Name = decoder.Decode<string>("Name");
                Content = decoder.Decode<string>("Content");
                PageBreakAfter = decoder.Decode<bool>("PageBreakAfter");
            }
        }

        public string Head { get; set; }

        public List<HtmlToWordPage> Pages { get; protected set; }

        public HtmlToWordContainer()
        {
            Pages = new List<HtmlToWordPage>();
            Head = "<head></head>";
        }

        public void Add(object content)
        {
            if(content is string)
            {
                Pages.Add(new HtmlToWordPage() { Content = (string)content });
            }
            else if(content is IEnumerable<string>) // the present statement can return a list of strings if you do present section 1,2,3 etc
            {
                foreach(var c in ((IEnumerable<string>)content))
                {
                    Pages.Add(new HtmlToWordPage() { Content = c });
                }
            }
        }

        public void Add(string name, string content)
        {
            Pages.Add(new HtmlToWordPage() { Name = name, Content = content });
        }

        public void Add(string name, string content, bool pageBreakAfter)
        {
            Pages.Add(new HtmlToWordPage() { Name = name, Content = content, PageBreakAfter = pageBreakAfter });
        }

        public string ToHtml()
        {
            StringBuilder builder = new StringBuilder();

            builder.Append("<html>").Append(Head).Append("<body>");

            foreach(var p in Pages)
            {
                builder.Append(p.Content);

                if(p != Pages.Last() && p.PageBreakAfter == true)
                {
                    builder.Append("<br style='page-break-before:always; clear:both' />");
                }
            }

            builder.Append("</body></html>");

            return builder.ToString();
        }

        public override void Encode(Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("HeadHtml", Head);
            encoder.Encode("Pages", Pages);
        }

        public override void Decode(Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            Head = decoder.Decode<string>("HeadHtml");
            Pages = decoder.Decode<List<HtmlToWordPage>>("Pages");
        }
    }
}
