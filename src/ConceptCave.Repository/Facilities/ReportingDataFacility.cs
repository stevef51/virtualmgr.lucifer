﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConceptCave.Checklist.Facilities;
using ConceptCave.Checklist.Lingo.ObjectProviders;
using ConceptCave.Core;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Data.EntityClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.RunTime;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Repository.Facilities
{
    public class ReportingDataFacility : Facility
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        private readonly IWorkingDocumentRepository _workingDocumentRepo;
        private readonly IMediaRepository _mediaRepo;

        public ReportingDataFacility(Func<DataAccessAdapter> fnAdapter, IWorkingDocumentRepository workingDocumentRepo, IMediaRepository mediaRepo)
        {
            Name = "ReportingData";
            _fnAdapter = fnAdapter;
            _workingDocumentRepo = workingDocumentRepo;
            _mediaRepo = mediaRepo;
        }

        public bool IsAnswerUnique(IContextContainer container, string answer, object questionIdentifier, string checklistIdentifier, string label)
        {

            PredicateExpression expression = new PredicateExpression(CompletedPresentedFactValueFields.Value == answer);

            if (questionIdentifier != null)
            {
                Guid questionId = Guid.Empty;

                if (questionIdentifier is Guid)
                {
                    questionId = (Guid)questionIdentifier;
                }
                else if (questionIdentifier is IQuestion)
                {
                    questionId = ((IQuestion)questionIdentifier).Id;
                }
                else if (questionIdentifier is string)
                {
                    IWorkingDocument wd = container.Get<IWorkingDocument>();
                    string qName = (string)questionIdentifier;
                    var question = (from q in ((WorkingDocument)wd).AsDepthFirstEnumerable() where q is IQuestion && ((IQuestion)q).Name == qName select q).DefaultIfEmpty(null).First();

                    if (question == null)
                    {
                        return true;
                    }

                    questionId = ((IQuestion)question).Id;
                }

                PredicateExpression qExpression = new PredicateExpression(new FieldCompareSetPredicate(CompletedPresentedFactValueFields.CompletedWorkingDocumentPresentedFactId, null, CompletedWorkingDocumentPresentedFactFields.Id, null, SetOperator.In, CompletedWorkingDocumentPresentedFactFields.InternalId == questionId));
                expression.AddWithAnd(qExpression);
            }

            if (string.IsNullOrEmpty(checklistIdentifier) == false)
            {
                PredicateExpression innerExpression = new PredicateExpression(new FieldCompareSetPredicate(CompletedWorkingDocumentPresentedFactFields.ReportingWorkingDocumentId, null, CompletedWorkingDocumentDimensionFields.Id, null, SetOperator.In, CompletedWorkingDocumentDimensionFields.Name == checklistIdentifier));
                PredicateExpression cExpression = new PredicateExpression(new FieldCompareSetPredicate(CompletedPresentedFactValueFields.CompletedWorkingDocumentPresentedFactId, null, CompletedWorkingDocumentPresentedFactFields.Id, null, SetOperator.In, innerExpression));

                expression.AddWithAnd(cExpression);
            }

            if (string.IsNullOrEmpty(label) == false)
            {
                PredicateExpression innerExpression = new PredicateExpression(new FieldCompareSetPredicate(CompletedLabelWorkingDocumentPresentedDimensionFields.CompletedLabelDimensionId, null, CompletedLabelDimensionFields.Id, null, SetOperator.In, CompletedLabelDimensionFields.Name == label));
                PredicateExpression cExpression = new PredicateExpression(new FieldCompareSetPredicate(CompletedPresentedFactValueFields.CompletedWorkingDocumentPresentedFactId, null, CompletedLabelWorkingDocumentPresentedDimensionFields.CompletedWorkingDocumentPresentedFactId, null, SetOperator.In, innerExpression));

                expression.AddWithAnd(cExpression);
            }

            EntityCollection<CompletedPresentedFactValueEntity> result = new EntityCollection<CompletedPresentedFactValueEntity>();

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), 1);
            }

            return result.Count == 0;
        }

        //public List<DictionaryObjectProvider> LastCompletedWorkingDocumentAnswers(IContextContainer container, string workingDocumentName)
        //{
        //    // first up need to get the top 1 from the completed working document presented facts where the reporting document dimension's name = what we've been given

        //    PredicateExpression expression = new PredicateExpression(new FieldCompareSetPredicate(CompletedWorkingDocumentPresentedFactFields.ReportingWorkingDocumentId, null, CompletedWorkingDocumentDimensionFields.Id, null, SetOperator.In, CompletedWorkingDocumentDimensionFields.Name == workingDocumentName));
        //    SortExpression sort = new SortExpression(CompletedWorkingDocumentPresentedFactFields.DateCompleted | SortOperator.Descending);

        //    EntityCollection<CompletedWorkingDocumentPresentedFactEntity> result = new EntityCollection<CompletedWorkingDocumentPresentedFactEntity>();

        //    using (var adapter = _fnAdapter())
        //    {
        //        adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), 1, sort);
        //    }

        //    if (result.Count == 0)
        //    {
        //        return null;
        //    }

        //    expression = new PredicateExpression(CompletedWorkingDocumentPresentedFactFields.WorkingDocumentId == result[0].WorkingDocumentId);
        //    PrefetchPath2 path = new PrefetchPath2((int)EntityType.CompletedWorkingDocumentPresentedFactEntity);
        //    path.Add(CompletedWorkingDocumentPresentedFactEntity.PrefetchPathCompletedUserDimension);
        //    path.Add(CompletedWorkingDocumentPresentedFactEntity.PrefetchPathCompletedUserDimension_);
        //    path.Add(CompletedWorkingDocumentPresentedFactEntity.PrefetchPathCompletedPresentedFactValues);

        //    using (var adapter = _fnAdapter())
        //    {
        //        adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), 0, sort, path);
        //    }

        //    return ConvertCompletedWorkingDocumentAnswersForLingo(result);
        //}

        //protected List<DictionaryObjectProvider> ConvertCompletedWorkingDocumentAnswersForLingo(IEnumerable<CompletedWorkingDocumentPresentedFactEntity> entities)
        //{
        //    List<DictionaryObjectProvider> result = new List<DictionaryObjectProvider>();

        //    foreach (var answers in (from e in entities group e by e.WorkingDocumentId into g select new { wid = g.Key, items = g.ToList() }))
        //    {
        //        DictionaryObjectProvider prov = new DictionaryObjectProvider();
        //        prov.Dictionary = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);

        //        prov.Dictionary["ReportingId"] = answers.items[0].ReportingWorkingDocumentId;
        //        prov.Dictionary["WorkingDocumentId"] = answers.items[0].WorkingDocumentId;

        //        List<DictionaryObjectProvider> docAnswers = new List<DictionaryObjectProvider>();

        //        prov.Dictionary["Answers"] = docAnswers;

        //        answers.items.ForEach(a =>
        //        {
        //            DictionaryObjectProvider prov2 = new DictionaryObjectProvider();
        //            prov.Dictionary = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);
        //            prov.Dictionary["Prompt"] = a.Prompt;
        //            if (a.CompletedPresentedFactValues.Count > 0)
        //            {
        //                prov.Dictionary["Answer"] = a.CompletedPresentedFactValues[0].Value;
        //            }
        //            else
        //            {
        //                prov.Dictionary["Answer"] = null;
        //            }

        //            docAnswers.Add(prov2);
        //        });
        //    }

        //    return result;
        //}

        public List<DictionaryObjectProvider> CurrentWorkingDocuments(IContextContainer container, Guid userId, int pageNumber, int pageSize)
        {
            if (userId == Guid.Empty)
            {
                return null;
            }

            if (pageNumber < 1)
            {
                return null;
            }

            if (pageSize < 1)
            {
                return null;
            }

            var items = _workingDocumentRepo.GetCurrentWorkingDocuments(userId, WorkingDocumentLoadInstructions.Users);

            return ConvertWorkingDocumentsForLingo(items);
        }

        /// <summary>
        /// Returns the list of working documents that have the same parent id as the working document handed in
        /// </summary>
        /// <param name="container"></param>
        /// <param name="userId"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public List<DictionaryObjectProvider> CurrentSiblingWorkingDocuments(IContextContainer container, Guid parentId, int pageNumber, int pageSize)
        {
            if (parentId == Guid.Empty)
            {
                return null;
            }

            if (pageNumber < 1)
            {
                return null;
            }

            if (pageSize < 1)
            {
                return null;
            }

            var items = _workingDocumentRepo.GetByParentId(parentId, WorkingDocumentLoadInstructions.Users);

            // work out which are current (not completed)
            var currents = items.Where(i => i.Status == (int)WorkingDocumentStatus.Started);

            return ConvertWorkingDocumentsForLingo(currents);
        }

        protected List<DictionaryObjectProvider> ConvertWorkingDocumentsForLingo(IEnumerable<WorkingDocumentDTO> entities)
        {
            List<DictionaryObjectProvider> result = new List<DictionaryObjectProvider>();

            foreach (var entity in entities)
            {
                DictionaryObjectProvider prov = new DictionaryObjectProvider();
                prov.Dictionary = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);
                prov.Dictionary["WorkingDocumentId"] = entity.Id;
                prov.Dictionary["DateCreated"] = entity.DateCreated;
                prov.Dictionary["DateCreatedForReviewer"] = entity.DateCreatedInReviewerTimezone();
                prov.Dictionary["DateCreatedForReviewee"] = entity.DateCreatedInRevieweeTimezone();
                prov.Dictionary["RevieweeId"] = entity.RevieweeId;
                if (entity.RevieweeId.HasValue)
                {
                    prov.Dictionary["RevieweeName"] = entity.Reviewer.Name;
                }
                else
                {
                    prov.Dictionary["RevieweeName"] = null;
                }
                prov.Dictionary["ReviewerId"] = entity.ReviewerId;
                prov.Dictionary["ReviewerName"] = entity.Reviewer.Name;
                prov.Dictionary["ParentId"] = entity.ParentId;
                prov.Dictionary["Name"] = entity.Name;

                result.Add(prov);
            }

            return result;
        }

        public List<DictionaryObjectProvider> GetUserLogins(object userId, DateTime startDate, DateTime endDate)
        {
            if ((userId is Guid) == false)
            {
                return null;
            }

            Guid id = (Guid)userId;

            PredicateExpression expression = new PredicateExpression(UserLogFields.UserId == id);
            expression.AddWithAnd(new FieldBetweenPredicate(UserLogFields.LogInTime, null, startDate, endDate));

            EntityCollection<UserLogEntity> logs = new EntityCollection<UserLogEntity>();

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(logs, new RelationPredicateBucket(expression));
            }

            List<DictionaryObjectProvider> result = new List<DictionaryObjectProvider>();

            foreach (var l in logs)
            {
                DictionaryObjectProvider r = new DictionaryObjectProvider();
                r.Dictionary["Login"] = l.LogInTime;
                r.Dictionary["Logout"] = l.LogOutTime;
                r.Dictionary["Latitude"] = l.Latitude;
                r.Dictionary["Longitude"] = l.Longitude;
                r.Dictionary["NearestLocation"] = l.NearestLocation;

                result.Add(r);
            }

            return result;
        }

        public List<DictionaryObjectProvider> GetMediaHeaderForLabel(string label)
        {
            var results = _mediaRepo.GetForLabel(label, MediaLoadInstruction.None);

            List<DictionaryObjectProvider> result = new List<DictionaryObjectProvider>();

            foreach (var r in results)
            {
                DictionaryObjectProvider d = new DictionaryObjectProvider();
                d.Dictionary["Id"] = r.Id;
                d.Dictionary["Name"] = r.Name;
                d.Dictionary["Description"] = r.Description;

                result.Add(d);
            }

            return result;
        }
    }
}
