﻿using ConceptCave.BusinessLogic.Invoices;
using ConceptCave.Checklist.Facilities;
using ConceptCave.Core;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.Repository.Facilities
{
    public class InvoiceFacility : Facility
    {
        public class Invoice : Node
        {
            private readonly IInvoiceRepository _invoiceRepo;
            private InvoiceDTO _invoiceDTO;
            private InvoiceDTO DTO
            {
                get
                {
                    if (_invoiceDTO == null)
                    {
                        _invoiceDTO = _invoiceRepo.GetInvoiceById(InvoiceId);
                    }
                    return _invoiceDTO;
                }
            }

            public string InvoiceId { get; private set; }

            public void MarkAsIssued()
            {
                DTO.IssuedDateUtc = DateTime.UtcNow;
                _invoiceDTO = _invoiceRepo.SaveInvoice(DTO, true);
            }


            public Invoice()
            {

            }

            public Invoice(string id, IInvoiceRepository invoiceRepo)
            {
                InvoiceId = id;
                _invoiceRepo = invoiceRepo;
            }

            public override void Decode(Core.Coding.IDecoder decoder)
            {
                base.Decode(decoder);
                InvoiceId = decoder.Decode<string>("InvoiceId");
            }

            public override void Encode(Core.Coding.IEncoder encoder)
            {
                base.Encode(encoder);
                encoder.Encode("InvoiceId", InvoiceId);
            }
        }

        private readonly IInvoiceRepository _invoiceRepo;

        public InvoiceFacility(IInvoiceRepository invoiceRepo)
        {
            Name = "Invoice";
            _invoiceRepo = invoiceRepo;
        }

        public List<Invoice> FindNew()
        {
            return (from i in _invoiceRepo.FindNew() select new Invoice(i.Id, _invoiceRepo)).ToList();
        }
    }
}
