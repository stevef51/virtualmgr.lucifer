﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.BusinessLogic.Facilities;
using ConceptCave.Checklist.Facilities;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.Lingo.ObjectProviders;
using ConceptCave.Checklist.Reporting.LingoQuery;
using ConceptCave.Core;
using ConceptCave.Repository.Facilities.FileHelpers;
using ConceptCave.Core.Coding;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Repositories;
using static ConceptCave.BusinessLogic.Facilities.MediaFacility;
using ClosedXML.Excel;
using VirtualMgr.Common;

namespace ConceptCave.Repository.Facilities
{
    public class DBitTimesheetExportFacility : Facility
    {
        public enum TimesheetExportColumnType
        {
            EmployeeNumber,
            Date,
            Day,
            Employee,
            ShiftPayLevel,
            StartTime,
            EndTime,
            ApprovedDuration,
            ExpectedDuration,
            ActualDuration,
            Exception,
            PayType,
            Company,
            PreApprovalTotal,
            TotalToPay,
            PayTypeOverride,
            Custom
        }

        public class TimesheetExportColumn : Node
        {
            public string Title { get; set; }
            public int ColumnIndex { get; set; }
            public TimesheetExportColumnType ColumnType { get; set; }

            public override void Encode(IEncoder encoder)
            {
                base.Encode(encoder);

                encoder.Encode("Title", Title);
                encoder.Encode("ColumnIndex", ColumnIndex);
                encoder.Encode("ColumnType", ColumnType);
            }

            public override void Decode(IDecoder decoder)
            {
                base.Decode(decoder);

                Title = decoder.Decode<string>("Title");
                ColumnIndex = decoder.Decode<int>("ColumnIndex");
                ColumnType = decoder.Decode<TimesheetExportColumnType>("ColumnType");
            }
        }

        public IList<TimesheetExportColumn> Columns { get; set; }

        protected IList<RosterDTO> _rosters;
        protected IDictionary<Guid, UserDataDTO> _membershipCache;
        protected IDictionary<string, HierarchyBucketDTO> _bucketCache;
        protected IDictionary<int, RosterDTO> _rosterCache;
        protected IDictionary<int, TimesheetItemTypeDTO> _timesheetTypeCache;

        protected IRosterRepository _rosterRepo;
        protected ITimesheetItemRepository _timesheetItemRepo;
        protected ITimesheetItemTypeRepository _timesheetItemTypeRepo;
        protected IMembershipRepository _memberRepo;
        protected IHierarchyBucketOverrideTypeRepository _bucketTypeRepo;
        protected IHierarchyRepository _hierarchyRepo;

        public DBitTimesheetExportFacility(IRosterRepository rosterRepo,
            ITimesheetItemRepository timesheetItemRepo,
            ITimesheetItemTypeRepository timesheetItemTypeRepo,
            IMembershipRepository memberRepo,
            IHierarchyBucketOverrideTypeRepository bucketTypeRepo,
            IHierarchyRepository hierarchyRepo)
        {
            Name = "DBitTimesheetExport";

            _rosterRepo = rosterRepo;
            _timesheetItemRepo = timesheetItemRepo;
            _timesheetItemTypeRepo = timesheetItemTypeRepo;
            _memberRepo = memberRepo;
            _bucketTypeRepo = bucketTypeRepo;
            _hierarchyRepo = hierarchyRepo;

            _membershipCache = new Dictionary<Guid, UserDataDTO>();
            _bucketCache = new Dictionary<string, HierarchyBucketDTO>();
            _rosterCache = new Dictionary<int, RosterDTO>();
            _timesheetTypeCache = new Dictionary<int, TimesheetItemTypeDTO>();
        }

        public override void Encode(IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("Columns", Columns);
        }

        public override void Decode(IDecoder decoder)
        {
            base.Decode(decoder);

            Columns = decoder.Decode<IList<TimesheetExportColumn>>("Columns");

            if (Columns == null)
            {
                Columns = new List<TimesheetExportColumn>();
            }
        }

        public void AddColumn(string Tittle, int index, TimesheetExportColumnType columnType)
        {
            var c = new TimesheetExportColumn()
            {
                Title = Tittle,
                ColumnIndex = index,
                ColumnType = columnType
            };

            Columns.Add(c);
        }

        public void AddDefaultColumns()
        {
            AddColumn("Employee No", 1, TimesheetExportColumnType.EmployeeNumber);
            AddColumn("Date", 4, TimesheetExportColumnType.Date);
            AddColumn("Day", 5, TimesheetExportColumnType.Day);
            AddColumn("Cleaner", 6, TimesheetExportColumnType.Employee);
            AddColumn("Shift's Pay Level", 7, TimesheetExportColumnType.ShiftPayLevel);
            AddColumn("Start Time", 8, TimesheetExportColumnType.StartTime);
            AddColumn("End Time", 9, TimesheetExportColumnType.EndTime);
            AddColumn("Duration", 10, TimesheetExportColumnType.ApprovedDuration);
            AddColumn("Expected", 11, TimesheetExportColumnType.ExpectedDuration);
            AddColumn("Active", 12, TimesheetExportColumnType.ActualDuration);
            AddColumn("Exception", 13, TimesheetExportColumnType.Exception);
            AddColumn("Type", 14, TimesheetExportColumnType.PayType);
            AddColumn("Company", 15, TimesheetExportColumnType.Company);
            AddColumn("Pre Approval Total", 16, TimesheetExportColumnType.PreApprovalTotal);
            AddColumn("Total To Pay", 17, TimesheetExportColumnType.TotalToPay);
            AddColumn("Exception Reason (Shift Code)", 18, TimesheetExportColumnType.PayTypeOverride);
        }

        public TimesheetExportColumn GetColumn(int index)
        {
            var r = (from c in Columns where c.ColumnIndex == index select c);
            if (r.Count() == 0)
            {
                return null;
            }

            return r.First();
        }

        public void RemoveColumn(int index)
        {
            var r = (from c in Columns where c.ColumnIndex == index select c);
            if (r.Count() == 0)
            {
                return;
            }

            Columns.Remove(r.First());
        }

        protected UserDataDTO GetOrAddMembership(Guid id)
        {
            UserDataDTO result = null;

            if (_membershipCache.TryGetValue(id, out result) == false)
            {
                result = _memberRepo.GetById(id, RepositoryInterfaces.Enums.MembershipLoadInstructions.AspNetMembership |
                    RepositoryInterfaces.Enums.MembershipLoadInstructions.Company);

                _membershipCache.Add(id, result);
            }

            return result;
        }

        protected RosterDTO GetOrAddRoster(int rosterId)
        {
            RosterDTO result = null;

            if (_rosterCache.TryGetValue(rosterId, out result) == false)
            {
                result = _rosterRepo.GetById(rosterId);

                _rosterCache.Add(rosterId, result);
            }

            return result;
        }

        protected TimesheetItemTypeDTO GetOrAddTimesheetType(int typeId)
        {
            TimesheetItemTypeDTO result = null;

            if (_timesheetTypeCache.TryGetValue(typeId, out result) == false)
            {
                result = _timesheetItemTypeRepo.GetById(typeId);

                _timesheetTypeCache.Add(typeId, result);
            }

            return result;
        }

        protected HierarchyBucketDTO GetOrAddBucket(int rosterId, int roleId)
        {
            HierarchyBucketDTO result = null;

            string key = rosterId.ToString() + "-" + roleId.ToString();

            if (_bucketCache.TryGetValue(key, out result) == false)
            {
                var roster = GetOrAddRoster(rosterId);

                var results = _hierarchyRepo.GetBucketsForHierarchy(roster.HierarchyId, RepositoryInterfaces.Enums.HierarchyBucketLoadInstructions.None);

                foreach (var r in results)
                {
                    var k = rosterId.ToString() + "-" + r.RoleId.ToString();

                    _bucketCache.Add(k, r);
                }

                result = _bucketCache[key];
            }

            return result;
        }

        protected TimeZoneInfo GetTimeZone(RosterDTO roster)
        {
            return TimeZoneInfoResolver.ResolveWindows(roster.Timezone);
        }

        protected IList<int> FindNoneLeaveTypes(IList<TimesheetItemTypeDTO> types)
        {
            return (from t in types where t.IsLeave == false select t.Id).ToList();
        }

        protected IList<int> FindLeaveTypes(IList<TimesheetItemTypeDTO> types)
        {
            return (from t in types where t.IsLeave == true select t.Id).ToList();
        }

        public IMediaItem ProcessTimesheetItems(IContextContainer context,
            object rosters,
            bool markAsExported,
            IMediaItem source,
            string sheetName,
            int startRow,
            bool includeTitles)
        {
            IList<RosterDTO> rs = ExtractRosterIds(rosters);

            if (rs == null)
            {
                return null;
            }

            ClosedXML.Excel.XLWorkbook workBook = null;
            ClosedXML.Excel.IXLWorksheet sheet = null;

            if (source != null)
            {
                System.IO.MemoryStream stream = new System.IO.MemoryStream(source.GetData(context));

                workBook = new ClosedXML.Excel.XLWorkbook(stream, ClosedXML.Excel.XLEventTracking.Disabled);

                if (string.IsNullOrEmpty(sheetName) == false)
                {
                    if (workBook.TryGetWorksheet(sheetName, out sheet) == false)
                    {
                        sheet = workBook.Worksheets.Worksheet(0);
                    }
                }
                else
                {
                    sheet = workBook.Worksheets.Worksheet(0);
                }
            }
            else
            {
                workBook = new ClosedXML.Excel.XLWorkbook();

                if (string.IsNullOrEmpty(sheetName) == false)
                {
                    sheet = workBook.Worksheets.Add(sheetName);
                }
                else
                {
                    sheet = workBook.Worksheets.Add("Export");
                }
            }

            var timesheetTypes = _timesheetItemTypeRepo.GetAll();

            var noneLeaveTypes = FindNoneLeaveTypes(timesheetTypes);
            var leaveTypes = FindLeaveTypes(timesheetTypes);

            // grab all the timesheet items
            List<TimesheetItemDTO> items = new List<TimesheetItemDTO>();
            foreach (var roster in rs)
            {
                var timesheets = _timesheetItemRepo.GetForRosterForExport(roster.Id, RepositoryInterfaces.Enums.TimesheetItemLoadInstructions.Users);

                items.AddRange(timesheets);
            }

            int row = startRow;

            foreach (var ts in items)
            {
                var roster = GetOrAddRoster(ts.RosterId);
                ts.Date = TimeZoneInfo.ConvertTimeFromUtc(ts.Date, GetTimeZone(roster));
                ts.ApprovedStartDate = TimeZoneInfo.ConvertTimeFromUtc(ts.ApprovedStartDate, GetTimeZone(roster));
                ts.ApprovedEndDate = TimeZoneInfo.ConvertTimeFromUtc(ts.ApprovedEndDate, GetTimeZone(roster));
                ts.ActualStartDate = TimeZoneInfo.ConvertTimeFromUtc(ts.ActualStartDate, GetTimeZone(roster));
                ts.ActualEndDate = TimeZoneInfo.ConvertTimeFromUtc(ts.ActualEndDate, GetTimeZone(roster));
                if (ts.ScheduledStartDate.HasValue)
                {
                    ts.ScheduledStartDate = TimeZoneInfo.ConvertTimeFromUtc(ts.ScheduledStartDate.Value, GetTimeZone(roster));
                }
                if (ts.ScheduledEndDate.HasValue)
                {
                    ts.ScheduledEndDate = TimeZoneInfo.ConvertTimeFromUtc(ts.ScheduledEndDate.Value, GetTimeZone(roster));
                }

                foreach (var col in Columns)
                {
                    var cell = sheet.Cell(row, col.ColumnIndex);

                    SetCellValue(ts, col, cell);
                }

                row++;
            }

            System.IO.MemoryStream output = new System.IO.MemoryStream();

            workBook.SaveAs(output);

            EmbeddedMediaItem result = new EmbeddedMediaItem(output.ToArray());

            result.Name = "Timesheet Export.xlsx";
            result.MediaType = "application/vnd.ms-excel";

            return result;
        }

        protected void SetCellValue(TimesheetItemDTO item, TimesheetExportColumn column, IXLCell cell)
        {
            switch (column.ColumnType)
            {
                case TimesheetExportColumnType.EmployeeNumber:
                    var employee = GetOrAddMembership(item.UserId);
                    cell.SetValue(employee.AspNetUser.UserName);
                    break;
                case TimesheetExportColumnType.Date:
                    cell.SetValue(item.ApprovedStartDate.ToString("dd-MMM-yyyy"));
                    break;
                case TimesheetExportColumnType.Day:
                    cell.SetValue(item.ApprovedStartDate.ToString("dddd"));
                    break;
                case TimesheetExportColumnType.Employee:
                    employee = GetOrAddMembership(item.UserId);
                    cell.SetValue(employee.Name);
                    break;
                case TimesheetExportColumnType.ShiftPayLevel:
                    if (item.HierarchyRoleId.HasValue == false)
                    {
                        break;
                    }

                    cell.SetValue(GetOrAddBucket(item.RosterId, item.HierarchyRoleId.Value).Name);
                    break;
                case TimesheetExportColumnType.StartTime:
                    cell.SetValue(item.ApprovedStartDate.ToString("hh:mm tt"));
                    break;
                case TimesheetExportColumnType.EndTime:
                    cell.SetValue(item.ApprovedEndDate.ToString("hh:mm tt"));
                    break;
                case TimesheetExportColumnType.ApprovedDuration:
                    cell.SetValue(item.ApprovedDuration / 60);
                    break;
                case TimesheetExportColumnType.ExpectedDuration:
                    if (item.ScheduledDuration.HasValue)
                    {
                        cell.SetValue(item.ScheduledDuration / 60);
                    }

                    break;
                case TimesheetExportColumnType.ActualDuration:
                    if (item.ActualDuration.HasValue)
                    {
                        cell.SetValue(item.ActualDuration / 60);
                    }

                    break;
                case TimesheetExportColumnType.Exception:
                    var sd = 0m;
                    if (item.ScheduledDuration.HasValue)
                    {
                        sd = item.ScheduledDuration.Value;
                    }
                    cell.SetValue((item.ApprovedDuration - sd) / 60);
                    break;
                case TimesheetExportColumnType.PayType:
                    cell.SetValue(GetOrAddTimesheetType(item.TimesheetItemTypeId).Name);
                    break;
                case TimesheetExportColumnType.Company:
                    employee = GetOrAddMembership(item.UserId);

                    if (employee.CompanyId.HasValue)
                    {
                        cell.SetValue(employee.Company.Name);
                    }

                    break;
                case TimesheetExportColumnType.PreApprovalTotal:
                    cell.SetValue(item.ApprovedDuration / 60);
                    break;
                case TimesheetExportColumnType.TotalToPay:
                    cell.SetValue(item.ApprovedDuration / 60);
                    break;
                case TimesheetExportColumnType.PayTypeOverride:
                    if (item.PayAsTimesheetItemTypeId.HasValue && item.PayAsTimesheetItemTypeId.Value != item.TimesheetItemTypeId)
                    {
                        cell.SetValue(GetOrAddTimesheetType(item.PayAsTimesheetItemTypeId.Value).Name);
                    }

                    break;
            }
        }

        protected IList<RosterDTO> Rosters
        {
            get
            {
                if (_rosters == null)
                {
                    _rosters = _rosterRepo.GetAll();
                }

                return _rosters;
            }
        }

        protected IList<RosterDTO> ExtractRosterIds(object rosters)
        {
            if (rosters == null)
            {
                return Rosters;
            }

            if (rosters is int[])
            {
                var rs = (int[])rosters;
                return (from r in Rosters where rs.Contains(r.Id) select r).ToList();
            }
            else if (rosters is ListObjectProvider)
            {
                var rs = (ListObjectProvider)rosters;

                List<RosterDTO> result = new List<RosterDTO>();
                foreach (var r in rs)
                {
                    var find = (from a in Rosters where a.Id == (int)r select a);
                    if (find.Count() > 0)
                    {
                        result.Add(find.First());
                    }
                }

                return result;
            }
            else if (rosters is string)
            {
                var rs = (string)rosters;
                List<RosterDTO> result = new List<RosterDTO>();
                foreach (var r in rs.Split(','))
                {
                    var id = int.Parse(r);
                    var find = (from a in Rosters where a.Id == id select a);
                    if (find.Count() > 0)
                    {
                        result.Add(find.First());
                    }
                }

                return result;
            }

            return null;
        }
    }
}
