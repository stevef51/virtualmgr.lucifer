﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using ConceptCave.BusinessLogic.Facilities;
using ConceptCave.Checklist.Facilities;
using LumenWorks.Framework.IO.Csv;
using System.IO;
using ConceptCave.Core;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Repository.Facilities.FileHelpers;

namespace ConceptCave.Repository.Facilities
{
    public class CsvFacility : Facility
    {
        public class CsvFacilityReader : MediaItemReader<StandardRecord, ReaderField>
        {
            private CsvReader reader { get; set; }

            protected IMediaItem MediaItem { get; set; }
            protected bool HasHeaders { get; set; }

            private int fieldCount { get; set; }
            private string[] headers { get; set; }

            public CsvFacilityReader() { }

            public CsvFacilityReader(IContextContainer context, IMediaItem mediaItem, bool hasHeaders)
            {
                MediaItem = mediaItem;
                HasHeaders = hasHeaders;

                CreateCsvReader(context, mediaItem, hasHeaders);
            }

            protected void CreateCsvReader(IContextContainer context, IMediaItem mediaItem, bool hasHeaders)
            {
                reader = new CsvReader(new StreamReader(new MemoryStream(mediaItem.GetData(context))), hasHeaders);

                fieldCount = reader.FieldCount;
                if (hasHeaders)
                {
                    headers = reader.GetFieldHeaders();
                }
            }

            public override void Dispose()
            {
                if (reader.IsDisposed == true)
                {
                    return;
                }

                reader.Dispose();
            }

            public override bool MoveNext()
            {
                bool result = reader.ReadNextRecord();

                if (result == true)
                {
                    CreateCurrentRecord();
                }
                return result;
            }

            private void CreateCurrentRecord()
            {
                currentRecord = new StandardRecord();
                currentRecord.Fields = new ListOf<ReaderField>();
                for (int i = 0; i < fieldCount; i++)
                {
                    ReaderField field = new ReaderField() { Value = reader[i] };
                    if (headers != null)
                    {
                        field.Name = headers[i];
                    }

                    currentRecord.Fields.Items.Add(field);
                }
            }

            public override void Reset()
            {
                
            }

            public override void Encode(Core.Coding.IEncoder encoder)
            {
                base.Encode(encoder);

                encoder.Encode("MediaItem", MediaItem);
                encoder.Encode("HasHeaders", HasHeaders);
                encoder.Encode("CurrentRecordIndex", reader.CurrentRecordIndex);
            }

            public override void Decode(Core.Coding.IDecoder decoder)
            {
                base.Decode(decoder);

                MediaItem = decoder.Decode<IMediaItem>("MediaItem");
                HasHeaders = decoder.Decode<bool>("HasHeaders");
                long currentRec = decoder.Decode<long>("CurrentRecordIndex");

                CreateCsvReader(decoder.Context, MediaItem, HasHeaders);

                reader.MoveTo(currentRec);
            }
        }

        public class CsvFacilityWriter : Node, IMediaItem
        {
            private const string QUOTE = "\"";
            private const string ESCAPED_QUOTE = "\"\"";
            private static char[] CHARACTERS_THAT_MUST_BE_QUOTED = { ',', '"', '\n' };

            protected StringBuilder builder { get; set; }
            protected bool IsRecordStart { get; set; }

            public CsvFacilityWriter()
                : base()
            {
                IsRecordStart = true;
                builder = new StringBuilder();
            }

            public static string Escape(string s)
            {
                if(string.IsNullOrEmpty(s))
                {
                    return string.Empty;
                }

                if (s.Contains(QUOTE))
                    s = s.Replace(QUOTE, ESCAPED_QUOTE);

                if (s.IndexOfAny(CHARACTERS_THAT_MUST_BE_QUOTED) > -1)
                    s = QUOTE + s + QUOTE;

                return s;
            }

            public void AppendField(string data)
            {
                if (IsRecordStart == false)
                {
                    builder.Append(",");
                }

                builder.Append(Escape(data));
                IsRecordStart = false;
            }

            public void EndRecord()
            {
                builder.Append("\r\n");
                IsRecordStart = true;
            }

            public override string ToString()
            {
                return builder.ToString();
            }

            public override void Encode(Core.Coding.IEncoder encoder)
            {
                base.Encode(encoder);

                encoder.Encode("Data", builder.ToString());
                encoder.Encode("IsRecordStart", IsRecordStart);
                encoder.Encode("Name", Name);
            }

            public override void Decode(Core.Coding.IDecoder decoder)
            {
                base.Decode(decoder);

                string data = decoder.Decode<string>("Data");
                builder = new StringBuilder(data);
                IsRecordStart = decoder.Decode<bool>("IsRecordStart");
                Name = decoder.Decode<string>("Name");
            }


            public string Name
            {
                get;set;
            }

            public string MediaType
            {
                get
                {
                    return "text/csv";
                }
                set
                {

                }
            }

            public byte[] GetData(IContextContainer context)
            {
                return System.Text.UTF8Encoding.UTF8.GetBytes(this.ToString());
            }
        }

        public CsvFacility()
        {
            Name = "Csv";
        }

        public CsvFacilityReader ReadMedia(IContextContainer context, IMediaItem item, bool hasHeaders)
        {
            return new CsvFacilityReader(context, item, hasHeaders);
        }

        public CsvFacilityWriter NewWriter(IContextContainer context)
        {
            return new CsvFacilityWriter();
        }
    }
}
