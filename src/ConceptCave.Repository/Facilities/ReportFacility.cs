﻿using ConceptCave.Checklist;
using ConceptCave.Checklist.Facilities;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Repository.Facilities.ReportHelpers;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using System.Linq;
using VirtualMgr.MultiTenant;

namespace ConceptCave.Repository.Facilities
{
    public class ReportFacility : Facility
    {
        internal IReportTicketRepository _reportTicketRepo;
        internal readonly IGlobalSettingsRepository _globalSettingRepo;
        private readonly IReportManagementRepository _reportManagementRepo;
        internal readonly IServerUrls _serverUrls;
        internal readonly ITokenGenerator _tokenGenerator;
        internal readonly IMembershipRepository _membershipRepo;
        internal readonly AppTenant _appTenant;

        public ReportFacility(
            IReportTicketRepository reportTicketRepo,
            IGlobalSettingsRepository globalSettingRepo,
            IReportManagementRepository reportManagementRepo,
            IServerUrls serverUrls,
            ITokenGenerator tokenGenerator,
            IMembershipRepository membershipRepo,
            AppTenant appTenant)
        {
            _reportTicketRepo = reportTicketRepo;
            _globalSettingRepo = globalSettingRepo;
            _reportManagementRepo = reportManagementRepo;
            _serverUrls = serverUrls;
            _tokenGenerator = tokenGenerator;
            _membershipRepo = membershipRepo;
            _appTenant = appTenant;

            Name = "Report";
        }

        private object Create(ReportDTO reportDto)
        {
            if (reportDto == null)
                return null;

            switch ((ReportType)reportDto.Type)
            {
                case ReportType.JsReport:
                    // Have to get the ReportData to get the ShortId for JsReport
                    if (reportDto.ReportData == null)
                    {
                        reportDto = _reportManagementRepo.GetById(reportDto.Id, ReportLoadInstructions.Data);
                    }
                    var ticket = new JsReportTicket(this) { ReportId = reportDto.Id, ReportName = reportDto.Name, ShortId = System.Text.Encoding.UTF8.GetString(reportDto.ReportData.Data) };
                    ticket.TemplateParameters = reportDto.Configuration;
                    return ticket;
            }
            return null;
        }

        public object Create(int reportId)
        {
            return Create(_reportManagementRepo.GetById(reportId, ReportLoadInstructions.None));
        }

        public object Create(string reportName)
        {
            return Create(_reportManagementRepo.GetByName(reportName, ReportLoadInstructions.None).FirstOrDefault());
        }
    }
}
