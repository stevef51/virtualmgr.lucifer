﻿using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.Repository.Facilities.ReportHelpers
{
    public class ReportParameters : Node, IObjectSetter
    {
        public class ValueType
        {
            public string value { get; set; }
            public string type { get; set; }
        }

        private Dictionary<string, ValueType> _values = new Dictionary<string, ValueType>();
        public Dictionary<string, ValueType> Values { get => _values; }

        public override string ToString()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(_values);
        }

        public bool SetObject(IContextContainer context, string name, object value)
        {
            if (value == null)
                _values[name] = null;
            else
            {
                string text = null;
                if (value is DateTime)
                    text = string.Format("{0:o}", value);
                else
                    text = value.ToString();
                _values[name] = new ValueType() { value = text, type = value.GetType().Name };
            }
            return true;
        }

        public bool GetObject(IContextContainer context, string name, out object value)
        {
            ValueType vt = null;
            if (_values.TryGetValue(name, out vt))
            {
                string text = vt.value;
                value = text;

                switch (vt.type)
                {
                    case "Boolean":
                        value = Boolean.Parse(text);
                        break;

                    case "Guid":
                        value = Guid.Parse(text);
                        break;

                    case "DateTime":
                        value = DateTime.Parse(text);
                        break;

                    case "int":
                    case "int32":
                        value = Int32.Parse(text);
                        break;

                    case "short":
                    case "int16":
                        value = Int16.Parse(text);
                        break;

                    case "long":
                    case "int64":
                        value = Int64.Parse(text);
                        break;

                    case "double":
                        value = double.Parse(text);
                        break;

                    case "float":
                        value = float.Parse(text);
                        break;
                }
                return true;
            }

            value = null;
            return false;
        }
    }
}
