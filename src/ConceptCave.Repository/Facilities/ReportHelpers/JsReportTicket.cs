﻿using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;
using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using VirtualMgr.Central;
using VirtualMgr.JsReport;

namespace ConceptCave.Repository.Facilities.ReportHelpers
{
    public class JsReportTicket : Node
    {
        private ReportFacility _reportFacility;

        public JsReportTicket(ReportFacility reportFacility)
        {
            // EVS-466 - Google Inbox requires file extension
            Format = "pdf";         // Default to PDF 
            _reportFacility = reportFacility;
        }

        public int ReportId { get; set; }

        public string ReportName { get; set; }

        public string ShortId { get; set; }
        public string Recipe { get; set; }

        private ReportParameters _parameters = new ReportParameters();
        public ReportParameters Parameters { get { return _parameters; } }

        public string TemplateParameters { get; set; }

        public Guid RunAsUserId { get; set; }

        public string Format { get; set; }

        public override void Encode(Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("ReportId", ReportId);
            encoder.Encode("Parameters", Parameters);
            encoder.Encode("RunAsUserId", RunAsUserId);
            encoder.Encode("Format", Format);
        }

        public override void Decode(Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            ReportId = decoder.Decode<int>("ReportId");
            _parameters = decoder.Decode<ReportParameters>("Parameters");
            RunAsUserId = decoder.Decode<Guid>("RunAsUserId");
            Format = decoder.Decode<string>("Format");
        }


        public class ReportMediaItem : Node, IMediaItem
        {
            public string TenantHostname { get; set; }
            public string JsReportServerUrl { get; set; }
            public string JsReportUserName { get; set; }
            public string JsReportPassword { get; set; }
            public ReportParameters Parameters { get; set; }
            public string TemplateParameters { get; set; }
            public string ShortId { get; set; }
            public string Recipe { get; set; }
            public string Token { get; set; }
            public new Guid Id { get; set; }

            public string Name { get; set; }

            public HttpStatusCode StatusCode { get; private set; }
            public string StatusReason { get; private set; }

            private void populateParameters(JsReportRequest request)
            {
                request.AddParameterValue("tenantHostName", TenantHostname);

                foreach (var nvp in Parameters.Values)
                {
                    object value = null;
                    switch (nvp.Value.type.ToLower())
                    {
                        case "boolean":
                            value = Boolean.Parse(nvp.Value.value);
                            break;

                        case "int":
                        case "int32":
                            value = Int32.Parse(nvp.Value.value);
                            break;

                        case "short":
                        case "int16":
                            value = Int16.Parse(nvp.Value.value);
                            break;

                        case "long":
                        case "int64":
                            value = Int64.Parse(nvp.Value.value);
                            break;

                        case "double":
                            value = double.Parse(nvp.Value.value);
                            break;

                        case "float":
                            value = float.Parse(nvp.Value.value);
                            break;

                        case "datetime":
                            value = nvp.Value.value;
                            break;

                        default:
                            value = nvp.Value.value;
                            break;
                    }
                    request.AddParameterValue(nvp.Key, value);
                }
            }

            public void Dispatch()
            {
                var reportGenerator = new JsReportGenerator(JsReportServerUrl, JsReportUserName, JsReportPassword);
                var reportRequest = new JsReportRequest()
                {
                    ShortId = ShortId,
                    Recipe = Recipe,
                    Token = Token
                };
                if (!string.IsNullOrWhiteSpace(TemplateParameters))
                {
                    reportRequest.ParseTemplateParameters(TemplateParameters);
                }
                populateParameters(reportRequest);

                reportGenerator.GenerateAsync(reportRequest).RunAndForget();
            }

            private void Generate()
            {
                var reportGenerator = new JsReportGenerator(JsReportServerUrl, JsReportUserName, JsReportPassword);
                var reportRequest = new JsReportRequest()
                {
                    ShortId = ShortId,
                    Recipe = Recipe,
                    Token = Token
                };
                if (!string.IsNullOrWhiteSpace(TemplateParameters))
                {
                    reportRequest.ParseTemplateParameters(TemplateParameters);
                }
                populateParameters(reportRequest);

                var report = Task.Run(async () => await reportGenerator.GenerateAsync(reportRequest));

                StatusCode = report.Result.StatusCode;
                if (StatusCode == HttpStatusCode.OK)
                {
                    StatusReason = "OK";
                    _mediaType = report.Result.ContentType;

                    var memory = new MemoryStream();
                    report.Result.Content.CopyTo(memory);
                    ReportData = memory.ToArray();
                }
                else
                {
                    StatusReason = report.Result.Message;
                }
                _generated = true;
            }

            private bool _generated;

            private string _mediaType;
            public string MediaType
            {
                get
                {
                    if (!_generated && _mediaType == null)
                        Generate();
                    return _mediaType;
                }
                set
                {
                    _mediaType = value;
                }
            }

            // Valid test - this may invoke the report which will be cached (but not serialized)
            // if cached then it means it will not get regenerated unless a postback occurs (lingo "present")
            public bool Valid
            {
                get
                {
                    if (!_generated)
                        Generate();
                    return StatusCode == HttpStatusCode.OK && ReportData != null;
                }
            }

            public byte[] ReportData;
            public byte[] GetData(IContextContainer context)
            {
                if (!_generated && ReportData == null)
                    Generate();
                return ReportData;
            }

            public override void Encode(Core.Coding.IEncoder encoder)
            {
                base.Encode(encoder);

                // Since the reports should be able to be regenerated we only store enough info to regenerate it
                encoder.Encode("Id", Id);
                encoder.Encode("Name", Name);
                encoder.Encode("JsReportServerUrl", JsReportServerUrl);
                encoder.Encode("JsReportUserName", JsReportUserName);
                encoder.Encode("JsReportPassword", JsReportPassword);
                encoder.Encode("Parameters", Parameters);
                encoder.Encode("TemplateParameters", TemplateParameters);
                encoder.Encode("ShortId", ShortId);
                encoder.Encode("Recipe", Recipe);
                encoder.Encode("Token", Token);
                encoder.Encode("TenantHostname", TenantHostname);
            }

            public override void Decode(Core.Coding.IDecoder decoder)
            {
                base.Decode(decoder);

                Id = decoder.Decode<Guid>("Id");
                Name = decoder.Decode<string>("Name");
                JsReportServerUrl = decoder.Decode<string>("JsReportServerUrl");
                JsReportUserName = decoder.Decode<string>("JsReportUserName");
                JsReportPassword = decoder.Decode<string>("JsReportPassword");
                Parameters = decoder.Decode<ReportParameters>("Parameters");
                TemplateParameters = decoder.Decode<string>("TemplateParameters");
                ShortId = decoder.Decode<string>("ShortId");
                Recipe = decoder.Decode<string>("Recipe");
                Token = decoder.Decode<string>("Token");
                TenantHostname = decoder.Decode<string>("TenantHostname");
            }
        }

        private string jsReportRecipe(string format)
        {
            switch ((format ?? "pdf").ToLower())
            {
                case "html":
                    return "html";
                case "xlsx":
                    return "html-to-xlsx";
                default:
                    return "phantom-pdf";
            }
        }

        private string recipeToExtension(string recipe)
        {
            switch (recipe.ToLower())
            {
                case "phantom-image":
                    return "jpg";

                case "html-to-xlsx":
                    return "xlsx";

                case "html-embedded-in-docx":
                    return "docx";

                case "text":
                case "html-to-text":
                    return "txt";

                case "fop-pdf":
                case "electron-pdf":
                case "wkhtmltopdf":
                case "chrome-pdf":
                case "phantom-pdf":
                    return "pdf";

                case "html":
                case "html-with-browser-client":
                    return "html";

                default:
                    return recipe;
            }
        }
        public IMediaItem Generate()
        {
            var userDto = _reportFacility._membershipRepo.GetById(RunAsUserId, RepositoryInterfaces.Enums.MembershipLoadInstructions.AspNetMembership);

            return new ReportMediaItem()
            {
                Id = _id,
                TenantHostname = _reportFacility._appTenant.PrimaryHostname,
                // EVS-466 - Google Inbox requires file extension in filename
                Name = string.Format("{0}.{1}", ReportName, recipeToExtension(Recipe ?? jsReportRecipe(Format))),
                JsReportServerUrl = _reportFacility._serverUrls.JsReportServerUrl,
                JsReportUserName = _reportFacility._globalSettingRepo.GetSetting<string>("JsReportUserName"),
                JsReportPassword = _reportFacility._globalSettingRepo.GetSetting<string>("JsReportPassword"),
                ShortId = ShortId,
                Recipe = Recipe ?? jsReportRecipe(Format),
                Token = _reportFacility._tokenGenerator.GenerateToken(userDto.AspNetUser.UserName),
                TemplateParameters = TemplateParameters,
                Parameters = Parameters
            };
        }
    }
}
