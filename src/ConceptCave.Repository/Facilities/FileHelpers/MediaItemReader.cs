﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Core;

namespace ConceptCave.Repository.Facilities.FileHelpers
{
    public abstract class MediaItemReader<T, K> : Node, IEnumerable<T>, IEnumerator<T> where K : ReaderField where T : ReaderRecord<K>
    {
        protected T currentRecord { get; set; }

        #region IEnumerable<CsvFacilityReaderRecord> Members

        public IEnumerator<T> GetEnumerator()
        {
            return this;
        }

        #endregion

        #region IEnumerable Members

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this;
        }

        #endregion

        #region IEnumerator<CsvFacilityReaderRecord> Members

        public T Current
        {
            get { return currentRecord; }
        }

        #endregion

        #region IDisposable Members

        public virtual void Dispose()
        {

        }

        #endregion

        #region IEnumerator Members

        object System.Collections.IEnumerator.Current
        {
            get { return currentRecord; }
        }

        #endregion

        public abstract bool MoveNext();
        public abstract void Reset();

        public override void Encode(Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("CurrentRecord", currentRecord);
        }

        public override void Decode(Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            currentRecord = decoder.Decode<T>("CurrentRecord");
        }
    }
}
