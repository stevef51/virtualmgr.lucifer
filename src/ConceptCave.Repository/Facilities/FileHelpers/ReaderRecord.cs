﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;

namespace ConceptCave.Repository.Facilities.FileHelpers
{
    public class StandardRecord : ReaderRecord<ReaderField> { }

    public class ReaderRecord<T> : Node, IObjectGetter where T : ReaderField
    {
        public ListOf<T> Fields { get; set; }

        public bool EnforceFieldsMatchSource { get; set; }

        public T FieldByIndex(int index)
        {
            if (index < 0 || index >= Fields.Items.Count)
            {
                return null;
            }

            return Fields.Items[index];
        }

        public string ValueByIndex(int index)
        {
            T field = FieldByIndex(index);

            if (field == null)
            {
                return null;
            }

            return field.Value;
        }

        public T FieldByName(string name)
        {
            var result = (from f in Fields where f.Name.Equals(name, StringComparison.CurrentCultureIgnoreCase) select f).DefaultIfEmpty(null).First();

            if (result == null && name.IndexOf("_") != -1)
            {
                // lets be nice and test with underscores replaced with spaces
                string rep = name.Replace("_", " ");
                result = (from f in Fields where f.Name.Equals(rep, StringComparison.CurrentCultureIgnoreCase) select f).DefaultIfEmpty(null).First();
            }

            return result;
        }

        #region IObjectGetter Members

        public bool GetObject(IContextContainer context, string name, out object result)
        {
            result = (from f in Fields where f.Name.Equals(name, StringComparison.CurrentCultureIgnoreCase) select f.Value).DefaultIfEmpty(null).First();

            if (result == null)
            {
                if (name.IndexOf("_") != -1)
                {
                    // lets be nice and test with underscores replaced with spaces
                    string rep = name.Replace("_", " ");
                    result = (from f in Fields where f.Name.Equals(rep, StringComparison.CurrentCultureIgnoreCase) select f.Value).DefaultIfEmpty(null).First();

                    if (result != null)
                    {
                        return true;
                    }
                }

                result = "";
                return EnforceFieldsMatchSource == false ? true : false;
            }

            return true;
        }

        #endregion

        public override void Encode(Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("Fields", Fields);
            encoder.Encode("EnforceFieldsMatchSource", EnforceFieldsMatchSource);
        }

        public override void Decode(Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            Fields = decoder.Decode<ListOf<T>>("Fields");
            EnforceFieldsMatchSource = decoder.Decode<bool>("EnforceFieldsMatchSource");
        }
    }
}
