﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Core;

namespace ConceptCave.Repository.Facilities.FileHelpers
{
    /// <summary>
    /// Represents a field in a record based file structure
    /// </summary>
    public class ReaderField : Node
    {
        public string Name { get; set; }
        public string Value { get; set; }

        public override void Encode(Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("Name", Name);
            encoder.Encode("Value", Value);
        }

        public override void Decode(Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            Name = decoder.Decode<string>("Name");
            Value = decoder.Decode<string>("Value");
        }
    }
}
