﻿using ConceptCave.Checklist.Facilities;
using ConceptCave.Checklist.Lingo.ObjectProviders;
using ConceptCave.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ConceptCave.Repository.Facilities
{
    public class XMLFacility : Facility
    {
        public class XmlFacilityAttribute : Node
        {
            public String Name { get; set; }
            public String Value { get; set; }

            public override void Encode(Core.Coding.IEncoder encoder)
            {
                base.Encode(encoder);

                encoder.Encode<string>("Name", Name);
                encoder.Encode<string>("Value", Value);
            }

            public override void Decode(Core.Coding.IDecoder decoder)
            {
                base.Decode(decoder);

                Name = decoder.Decode<string>("Name");
                Value = decoder.Decode<string>("Value");
            }
        }

        public class XmlFacilityElement : XmlFacilityAttribute
        {
            public IList<XmlFacilityAttribute> Attributes { get; set; }
            public IList<XmlFacilityElement> Children { get; set; }

            public XmlFacilityElement()
            {
                Attributes = new List<XmlFacilityAttribute>();
                Children = new List<XmlFacilityElement>();
            }

            public XmlFacilityAttribute AddAttribute(string name, string value)
            {
                var a = new XmlFacilityAttribute() { Name = name, Value = value };
                Attributes.Add(a);

                return a;
            }

            internal void ToXml(XmlElement parent, XmlDocument doc)
            {
                var element = doc.CreateElement(Name);

                if(parent == null)
                {
                    doc.AppendChild(element);
                }
                else
                {
                    parent.AppendChild(element);
                }

                foreach(var attr in Attributes)
                {
                    var a = parent.OwnerDocument.CreateAttribute(attr.Name);
                    a.InnerText = attr.Value;

                    element.Attributes.Append(a);
                }

                if(string.IsNullOrEmpty(Value) == false)
                {
                    element.InnerText = Value;
                }
                else
                {
                    foreach(var child in Children)
                    {
                        child.ToXml(element, doc);
                    }
                }
            }

            public override void Encode(Core.Coding.IEncoder encoder)
            {
                base.Encode(encoder);

                encoder.Encode<IList<XmlFacilityAttribute>>("Attributes", Attributes);
                encoder.Encode<IList<XmlFacilityElement>>("Children", Children);
            }

            public override void Decode(Core.Coding.IDecoder decoder)
            {
                base.Decode(decoder);

                Attributes = decoder.Decode<IList<XmlFacilityAttribute>>("Attributes", new List<XmlFacilityAttribute>());
                Children = decoder.Decode<IList<XmlFacilityElement>>("Children", new List<XmlFacilityElement>());
            }
        }

        public XMLFacility()
        {
            Name = "XML";
        }

        public string ToXml(IContextContainer context, object element)
        {
            if(element is XmlFacilityElement)
            {
                XmlDocument doc = new XmlDocument();
                ((XmlFacilityElement)element).ToXml(null, doc);

                return doc.OuterXml;
            }

            throw new InvalidOperationException("An XmlFacilityElement must be handed in to ToXml");
        }

        public XmlFacilityElement CreateElement(IContextContainer context, string name)
        {
            return new XmlFacilityElement() { Name = name };
        }

        public XmlFacilityAttribute CreateAttribute(IContextContainer context, string name)
        {
            return new XmlFacilityAttribute() { Name = name };
        }
    }
}
