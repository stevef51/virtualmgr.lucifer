﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.Repository.Facilities
{
    // This class has been moved to BusinessLogic where it is better placed, however a shim must always exist here since Checklist XML will reference
    // this fully qualified class name
    public class JsonFacility : ConceptCave.BusinessLogic.Facilities.JsonFacility
    {
    }
}
