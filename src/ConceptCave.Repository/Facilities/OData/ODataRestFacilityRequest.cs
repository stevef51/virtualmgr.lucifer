﻿using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.Repository.Facilities.OData
{
    public class ODataRestFacilityRequest : ConceptCave.Repository.Facilities.RestFacility.RestFacilityRequest
    {
        public string Filter { get; set; }
        public string OrderBy { get; set; }
        public string Expand { get; set; }
        public string Select { get; set; }
        public bool UsePaging { get; set; }
        public int ItemsPerPage { get; set; }
        public int Page { get; set; }
        public bool IncludeCount { get; set; }
        public string DataScope { get; set; }

        public int? Top { get; set; }
        public int? Skip { get; set; }

        public ODataRestFacilityRequest()
        {
            this.Format = RestSharp.DataFormat.Json;
            this.Method = RestSharp.Method.GET;
        }

        internal override RestFacility.RestFacilityResponse Execute(IContextContainer context, RestSharp.RestClient client)
        {
            ILingoProgram program = context.Get<ILingoProgram>();

            var filter = program.ExpandString(Filter);
            var orderby = program.ExpandString(OrderBy);
            var expand = program.ExpandString(Expand);
            var select = program.ExpandString(Select);
            var datascope = program.ExpandString(DataScope);

            List<ConceptCave.Repository.Facilities.RestFacility.RestFacilityRequestParameter> parameters = new List<ConceptCave.Repository.Facilities.RestFacility.RestFacilityRequestParameter>();
            Parameters.ForEach(h => parameters.Add(h));

            if(string.IsNullOrEmpty(filter) == false)
            {
                parameters.Add(new RestFacility.RestFacilityRequestParameter() { Name = "$filter", Value = filter });
            }
            if (string.IsNullOrEmpty(orderby) == false)
            {
                parameters.Add(new RestFacility.RestFacilityRequestParameter() { Name = "$orderby", Value = orderby });
            }
            if (string.IsNullOrEmpty(expand) == false)
            {
                parameters.Add(new RestFacility.RestFacilityRequestParameter() { Name = "$expand", Value = expand });
            }
            if (string.IsNullOrEmpty(select) == false)
            {
                parameters.Add(new RestFacility.RestFacilityRequestParameter() { Name = "$select", Value = select });
            }
            if (string.IsNullOrEmpty(datascope) == false)
            {
                parameters.Add(new RestFacility.RestFacilityRequestParameter() { Name = "datascope", Value = datascope });
            }

            if(IncludeCount)
            {
                parameters.Add(new RestFacility.RestFacilityRequestParameter() { Name = "$inlinecount", Value = "allpages" });

                // In order for $inlinecount to work we have to make sure the following header is present
                if (!Headers.Any(h => h.Name.ToLower() == "accept"))
                {
                    Headers.Add(new RestFacility.RestFacilityRequestParameter()
                    {
                        Name = "Accept",
                        Value = "application/json;odata=light, text/plain, */*"
                    });
                }
            }


            if (UsePaging)
            {
                parameters.Add(new RestFacility.RestFacilityRequestParameter() { Name = "$top", Value = ItemsPerPage.ToString() });
                parameters.Add(new RestFacility.RestFacilityRequestParameter() { Name = "$skip", Value = (ItemsPerPage * Page).ToString() });
            }
            else
            {
                // Otherwise Top & Skip can be specified independantly
                if (Top.HasValue)
                    parameters.Add(new RestFacility.RestFacilityRequestParameter() { Name = "$top", Value = Top.Value.ToString() });
                if (Skip.HasValue)
                    parameters.Add(new RestFacility.RestFacilityRequestParameter() { Name = "skip", Value = Skip.Value.ToString() });
            }
            List<ConceptCave.Repository.Facilities.RestFacility.RestFacilityRequestParameter> headers = new List<ConceptCave.Repository.Facilities.RestFacility.RestFacilityRequestParameter>();
            Headers.ForEach(h => headers.Add(h));

            return Execute(context, client, headers, parameters);
        }

        public override void Encode(Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode<string>("Filter", Filter);
            encoder.Encode<string>("OrderBy", OrderBy);
            encoder.Encode<string>("Expand", Expand);
            encoder.Encode<string>("Select", Select);
            encoder.Encode<bool>("UsePaging", UsePaging);
            encoder.Encode<int>("ItemsPerPage", ItemsPerPage);
            encoder.Encode<int>("Page", Page);
            encoder.Encode<bool>("IncludeCount", IncludeCount);
            encoder.Encode<string>("DataScope", DataScope);
        }

        public override void Decode(Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            Filter = decoder.Decode<string>("Filter");
            OrderBy = decoder.Decode<string>("OrderBy");
            Expand = decoder.Decode<string>("Expand");
            Select = decoder.Decode<string>("Select");
            UsePaging = decoder.Decode<bool>("UsePaging");
            ItemsPerPage = decoder.Decode<int>("ItemsPerPage");
            Page = decoder.Decode<int>("Page");
            IncludeCount = decoder.Decode<bool>("IncludeCount");
            DataScope = decoder.Decode<string>("DataScope");
        }
    }
}
