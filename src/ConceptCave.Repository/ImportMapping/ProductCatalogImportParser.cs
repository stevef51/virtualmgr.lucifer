﻿using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.RepositoryInterfaces.Importing;

namespace ConceptCave.Repository.ImportMapping
{
    public class ProductCatalogImportExportTransformFactory
    {
        public const string FacilityName = "Facility Name";
        public const string BuildingName = "Building Name";
        public const string FloorName = "Floor Name";
        public const string ZoneName = "Zone Name";
        public const string SiteName = "Site Name";

        public const string CatalogId = "Catalog Id";
        public const string CatalogName = "Catalog Name";
        public const string StockCountWhenOrdering = "Stock Count When Ordering";
        public const string ShowPricingWhenOrdering = "Show Pricing When Ordering";
        public const string ProductName = "Product Name";
        public const string DeleteItem = "Delete Item";
        public const string DeleteCatalog = "Delete Catalog";

        public static string[] All = new string[] 
        {
            FacilityName,
            BuildingName,
            FloorName,
            ZoneName,
            SiteName,
            CatalogId,
            CatalogName,
            StockCountWhenOrdering,
            ShowPricingWhenOrdering,
            ProductName,
            DeleteItem,
            DeleteCatalog
        };

        public static List<FieldTransform> Create()
        {
            List<FieldTransform> result = new List<FieldTransform>();

            for(var i = 1; i <= All.Length; i++)
            {
                result.Add(new FieldTransform(All[i - 1], "1", i));
            }
            return result;
        }
    }

    public class ProductCatalogImportWriter : IImportWriter
    {
        protected IFacilityStructureRepository _facilityStructureRepo;
        protected IProductRepository _productRepo;
        protected IProductCatalogRepository _productCatalogRepo;
        protected IProductCatalogItemRepository _productCatalogItemRepo;

        public ProductCatalogImportWriter(IFacilityStructureRepository facilityStructureRepo, IProductRepository productRepo, IProductCatalogRepository productCatalogRepo, IProductCatalogItemRepository productCatalogItemRepo)
        {
            _facilityStructureRepo = facilityStructureRepo;
            _productRepo = productRepo;
            _productCatalogRepo = productCatalogRepo;
            _productCatalogItemRepo = productCatalogItemRepo;
        }

        protected FacilityStructureDTO _facilityStructure;
        protected ProductCatalogDTO _productCatalog;
        protected ProductCatalogItemDTO _productCatalogItem;

        protected HashSet<ProductCatalogItemDTO> _saveProductCatalogItems = new HashSet<ProductCatalogItemDTO>();
        protected HashSet<ProductCatalogItemDTO> _deleteProductCatalogItems = new HashSet<ProductCatalogItemDTO>();
        protected HashSet<ProductCatalogDTO> _saveProductCatalogs = new HashSet<ProductCatalogDTO>();
        protected HashSet<ProductCatalogDTO> _deleteProductCatalogs = new HashSet<ProductCatalogDTO>();

        protected Dictionary<int, Dictionary<string, FacilityStructureDTO>> _facilityStructureCache = new Dictionary<int, Dictionary<string, FacilityStructureDTO>>();
        protected Dictionary<FacilityStructureDTO, Dictionary<int, ProductCatalogDTO>> _productCatalogCache = new Dictionary<FacilityStructureDTO, Dictionary<int, ProductCatalogDTO>>();

        protected bool ParseBool(string val)
        {
            // we accept both true/false and y/n
            string v = val.ToLower();

            if (v.Length == 1)
            {
                return v == "y";
            }
            else
            {
                return bool.Parse(v);
            }
        }


        public bool RequiresUniqueKey
        {
            get
            {
                return false;
            }
        }

        public void Complete()
        {
            foreach (var catalog in _saveProductCatalogs)
            {
                catalog.Id = _productCatalogRepo.Save(catalog, true).Id;
            }

            int sortOrder = 0;
            foreach(var item in _saveProductCatalogItems)
            {
                item.SortOrder = sortOrder++;
                _productCatalogItemRepo.Save(item, false);
            }
            foreach (var item in _deleteProductCatalogItems)
            {
                _productCatalogItemRepo.Remove(item.ProductId, item.ProductCatalogId);
            }
            foreach (var catalog in _deleteProductCatalogs)
            {
                bool archived = false;
                _productCatalogItemRepo.DeleteFromCatalog(catalog.Id);
                _productCatalogRepo.Delete(catalog.Id, true, out archived);
            }

            _saveProductCatalogs = null;
            _saveProductCatalogItems = null;
            _deleteProductCatalogItems = null;
            _deleteProductCatalogs = null;

            _facilityStructure = null;
            _productCatalog = null;
        }

        public FieldTransform GetUniqueKeyTransform(List<FieldTransform> transforms)
        {
            // Note we dont use this, but have to return something 
            var transform = (from t in transforms where t.Destination.Name.Equals(ProductCatalogImportExportTransformFactory.ProductName) select t).DefaultIfEmpty(null).First();

            return transform;
        }

        public void LoadEntity(string uniqueId)
        {
            // Do not reset FacilityStructure or ProductCatalog, this allows the Excel spreadsheet to set a Facility Structure
            // and use rows below it to add Catalogs and Items without respecifying the Facility Structure - same for Product Catalog
            _productCatalogItem = null;
        }

        public void SaveEntity(string uniqueId)
        {
            // Do nothing, all done in Complete
        }

        private ProductCatalogDTO FindOrCreateCatalog(FacilityStructureDTO fs, int catalogId)
        {
            ProductCatalogDTO pc = null;
            Dictionary<int, ProductCatalogDTO> dict = null;
            if (_productCatalogCache.TryGetValue(fs, out dict))
            {
                if (dict.TryGetValue(catalogId, out pc))
                {
                    return pc;
                }
            }

            if (dict == null)
            {
                dict = new Dictionary<int, ProductCatalogDTO>();
                _productCatalogCache[fs] = dict;
            }

            var pcs = new List<ProductCatalogDTO>();
            var rules = new List<ProductCatalogFacilityStructureRuleDTO>();
            _productCatalogRepo.GetCatalogsForFacilityStructure(fs.Id, pcs, rules);
            foreach(var i in pcs)
            {
                dict[i.Id] = i;
            }

            if (!dict.TryGetValue(catalogId, out pc))
            {
                throw new ArgumentException(string.Format("Product Catalog Id {0} does not belong to {1} {2}", catalogId, ((FacilityStructureType)fs.StructureType).ToString(), fs.Name));
            }
            return pc;
        }

        private FacilityStructureDTO FindFacilityStructure(FacilityStructureDTO parent, string name)
        {
            FacilityStructureDTO fs = null;
            Dictionary<string, FacilityStructureDTO> dict = null;
            if (_facilityStructureCache.TryGetValue(parent != null ? parent.Id : 0, out dict))
            {
                if (dict.TryGetValue(name, out fs))
                {
                    return fs;
                }
            }

            fs = _facilityStructureRepo.FindByName(parent != null ? (int?)parent.Id : null, name);
            if (fs == null)
            {
                if (parent != null)
                    throw new ArgumentException(string.Format("{0} not found inside {1} {2}", name, ((FacilityStructureType)parent.StructureType).ToString(), parent.Name));
                else
                    throw new ArgumentException(string.Format("Facility {0} not found", name));
            }

            if (dict == null)
            {
                dict = new Dictionary<string, FacilityStructureDTO>();
                _facilityStructureCache[parent != null ? parent.Id : 0] = dict;
            }
            dict[name] = fs;

            return fs;
        }

        public void WriteValue(FieldTransform transform, string value)
        {
            string field = transform.Destination.Name;

            switch (field)
            {
                case ProductCatalogImportExportTransformFactory.FacilityName:
                    if (!string.IsNullOrEmpty(value))
                    {
                        _facilityStructure = FindFacilityStructure(null, value);
                        _productCatalog = null;
                    }
                    break;
                case ProductCatalogImportExportTransformFactory.BuildingName:
                case ProductCatalogImportExportTransformFactory.FloorName:
                case ProductCatalogImportExportTransformFactory.SiteName:
                case ProductCatalogImportExportTransformFactory.ZoneName:
                    if (!string.IsNullOrEmpty(value))
                    {
                        _facilityStructure = FindFacilityStructure(_facilityStructure, value);
                        _productCatalog = null;
                    }
                    break;

                case ProductCatalogImportExportTransformFactory.CatalogId:
                    if (_facilityStructure == null)
                    {
                        throw new ArgumentException(string.Format("Product Catalog Id {0} does not have a Facility Structure defined", value));
                    }
                    if (!string.IsNullOrEmpty(value))
                    {
                        var id = int.Parse(value);
                        _productCatalog = FindOrCreateCatalog(_facilityStructure, id);
                        _productCatalogItem = null;
                    }
                    break;

                case ProductCatalogImportExportTransformFactory.CatalogName:
                    if (!string.IsNullOrEmpty(value))
                    {
                        if (_facilityStructure != null && _productCatalog == null)
                        {
                            _productCatalog = new ProductCatalogDTO();
                            _productCatalog.__IsNew = true;
                            _productCatalog.FacilityStructure = _facilityStructure;
                        }
                        if (_productCatalog.Name != value)
                        {
                            _productCatalog.Name = value;
                            _saveProductCatalogs.Add(_productCatalog);
                        }
                    }
                    break;

                case ProductCatalogImportExportTransformFactory.DeleteCatalog:
                    if (!string.IsNullOrEmpty(value) && ParseBool(value))
                    {
                        _deleteProductCatalogs.Add(_productCatalog);
                    }
                    break;

                case ProductCatalogImportExportTransformFactory.DeleteItem:
                    if (!string.IsNullOrEmpty(value) && ParseBool(value))
                    {
                        if (_productCatalogItem == null)
                        {
                            throw new ArgumentException("Product Item cannot be Deleted without a Product Name");
                        }
                        _deleteProductCatalogItems.Add(_productCatalogItem);
                    }
                    break;

                case ProductCatalogImportExportTransformFactory.ProductName:
                    if (!string.IsNullOrEmpty(value))
                    {
                        var product = _productRepo.GetByName(value);
                        if (product == null)
                        {
                            throw new ArgumentException(string.Format("No product called '{0}' exists", value));
                        }
                        _productCatalogItem = _productCatalogItemRepo.GetById(product.Id, _productCatalog.Id, RepositoryInterfaces.Enums.ProductCatalogItemLoadInstructions.None);
                        if (_productCatalogItem == null)
                        {
                            _productCatalogItem = new ProductCatalogItemDTO();
                            _productCatalogItem.__IsNew = true;
                            _productCatalogItem.Product = product;
                            _productCatalogItem.ProductCatalog = _productCatalog;
                        }
                        // Have to add it always so we can rewrite SortOrder column
                        _saveProductCatalogItems.Add(_productCatalogItem);
                    }
                    else
                    {
                        _productCatalogItem = null;
                    }
                    break;

                case ProductCatalogImportExportTransformFactory.ShowPricingWhenOrdering:
                    if (!string.IsNullOrEmpty(value))
                    {
                        _productCatalog.ShowPricingWhenOrdering = ParseBool(value);
                    }
                    break;

                case ProductCatalogImportExportTransformFactory.StockCountWhenOrdering:
                    if (!string.IsNullOrEmpty(value))
                    {
                        _productCatalog.StockCountWhenOrdering = ParseBool(value);
                    }
                    break;
            }
        }
    }

    public class ProductCatalogExportParser : ExportParser<IList<FacilityStructureDTO>>
    {
        protected IFacilityStructureRepository _facilityStructureRepo;
        protected IProductCatalogRepository _productCatalogRepo;
        protected IProductRepository _productRepo;
        protected IProductCatalogItemRepository _productCatalogItemRepo;

        private Dictionary<int, ProductCatalogDTO> _catalogCache = new Dictionary<int, ProductCatalogDTO>();
        private Dictionary<int, ProductDTO> _productCache = new Dictionary<int, ProductDTO>();
        private Dictionary<Tuple<int, FacilityStructureType>, FacilityStructureDTO> _facilityStructureCache = new Dictionary<Tuple<int, FacilityStructureType>, FacilityStructureDTO>();

        public ProductCatalogExportParser(IFacilityStructureRepository facilityStructureRepo, IProductRepository productRepo, IProductCatalogRepository productCatalogRepo, IProductCatalogItemRepository productCatalogItemRepo)
        {
            _facilityStructureRepo = facilityStructureRepo;
            _productRepo = productRepo;
            _productCatalogRepo = productCatalogRepo;
            _productCatalogItemRepo = productCatalogItemRepo;
        }

        public bool IncludeArchived { get; set; }

        public override IImportWriter GetWriter(IList<FacilityStructureDTO> source)
        {
            return null;
        }

        public override ImportParserResult Import(IList<FacilityStructureDTO> source, List<FieldTransform> transforms, IImportWriter writer, bool isTestOnly, Action<int> progressCallback)
        {
            ImportParserResult result = new ImportParserResult();

            if (isTestOnly == true)
            {
                return result;
            }

            WriteTitles(transforms, writer);

            WriteLookups(transforms, writer);

            foreach (var facilityStructure in source)
            {
                WriteFacilityStructure(facilityStructure, writer, transforms);
            }

            return result;
        }

        private void WriteLookups(List<FieldTransform> tranforms, IImportWriter writer)
        {
            var excel = writer as IImportExcelWriter;
            if (excel != null)
            {
                int total = 0;
                excel.AddLookupSheet("Products", _productRepo.Search("%", null, null, null, false, ref total).Select(p => p.Name).ToList());
            }
        }

        private FacilityStructureDTO Find(FacilityStructureDTO from, FacilityStructureType type)
        {
            if (from == null)
                return null;

            if (from.StructureType < (int)type)
                return null;

            FacilityStructureDTO ancestor = null;
            var key = new Tuple<int, FacilityStructureType>(from.Id, type);
            if (!_facilityStructureCache.TryGetValue(key, out ancestor))
            {
                ancestor = _facilityStructureRepo.GetDeepParent(from.Id, type);
                if (ancestor == null)
                {
                    throw new ArgumentException("Facility Structure has no Ancestor of type " + type.ToString());                       
                }
                _facilityStructureCache[key] = ancestor;
            }
            return ancestor;
        }

        public void WriteFacilityStructure(FacilityStructureDTO facilityStructure, IImportWriter writer, List<FieldTransform> transforms)
        {
            // We have to loop through the Facility Structure going down its children, and for each Facility Structure
            // dump its Product Catalogs
            var catalogs = _productCatalogRepo.GetFacilityStructureProductCatalogs(facilityStructure.Id);
            if (catalogs != null && catalogs.Any())
            {
                foreach (var catalog in catalogs)
                {
                    writer.LoadEntity(facilityStructure.Id.ToString());
                    WriteRow(facilityStructure, writer, transforms, catalog, null);

                    foreach (var productItem in catalog.ProductCatalogItems.OrderBy(pci => pci.SortOrder))
                    {
                        writer.LoadEntity(facilityStructure.Id.ToString());
                        WriteRow(null, writer, transforms, null, productItem);
                    }
                }
            }
        }

        private void WriteRow(FacilityStructureDTO facilityStructure, IImportWriter writer, List<FieldTransform> transforms, ProductCatalogDTO catalog, ProductCatalogItemDTO productItem)
        {
            var excel = writer as IImportExcelWriter;
            foreach (var transform in transforms)
            {
                string field = transform.Destination.Name;

                switch (field)
                {
                    case ProductCatalogImportExportTransformFactory.BuildingName:
                        var building = Find(facilityStructure, FacilityStructureType.Building);
                        writer.WriteValue(transform, building != null ? building.Name : null);
                        break;

                    case ProductCatalogImportExportTransformFactory.FacilityName:
                        var facility = Find(facilityStructure, FacilityStructureType.Facility);
                        writer.WriteValue(transform, facility != null ? facility.Name : null);
                        break;

                    case ProductCatalogImportExportTransformFactory.FloorName:
                        var floor = Find(facilityStructure, FacilityStructureType.Floor);
                        writer.WriteValue(transform, floor != null ? floor.Name : null);
                        break;

                    case ProductCatalogImportExportTransformFactory.SiteName:
                        var site = Find(facilityStructure, FacilityStructureType.Site);
                        writer.WriteValue(transform, site != null ? site.Name : null);
                        break;

                    case ProductCatalogImportExportTransformFactory.ZoneName:
                        var zone = Find(facilityStructure, FacilityStructureType.Zone);
                        writer.WriteValue(transform, zone != null ? zone.Name : null);
                        break;

                    case ProductCatalogImportExportTransformFactory.CatalogId:
                        writer.WriteValue(transform, catalog != null ? catalog.Id.ToString() : null);
                        break;

                    case ProductCatalogImportExportTransformFactory.CatalogName:
                        writer.WriteValue(transform, catalog != null ? catalog.Name : null);
                        break;

                    case ProductCatalogImportExportTransformFactory.DeleteCatalog:
                        writer.WriteValue(transform, catalog != null ? (catalog.Archived ? "Y" : "N") : "");
                        break;

                    case ProductCatalogImportExportTransformFactory.DeleteItem:
                        writer.WriteValue(transform, productItem != null ? "N" : "");
                        break;

                    case ProductCatalogImportExportTransformFactory.ProductName:
                        SetLookupColumn(writer, true, "Products");
                        writer.WriteValue(transform, productItem != null ? productItem.Product.Name : "");
                        break;

                    case ProductCatalogImportExportTransformFactory.ShowPricingWhenOrdering:
                        writer.WriteValue(transform, catalog != null ? (catalog.ShowPricingWhenOrdering ? "Y" : "N") : "");
                        break;

                    case ProductCatalogImportExportTransformFactory.StockCountWhenOrdering:
                        writer.WriteValue(transform, catalog != null ? (catalog.StockCountWhenOrdering ? "Y" : "N") : "");
                        break;
                }
            }
        }

        public override List<FieldMapping> Parse(IList<FacilityStructureDTO> source, List<FieldMapping> mappings)
        {
            return mappings;
        }

        public override ImportParserResult Verify(IList<FacilityStructureDTO> source, List<FieldMapping> mappings)
        {
            return new ImportParserResult();
        }
    }
}
