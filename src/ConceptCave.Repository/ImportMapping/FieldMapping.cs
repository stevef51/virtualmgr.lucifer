﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Core;

namespace ConceptCave.Repository.ImportMapping
{
    [Serializable()]
    public class FieldMapping
    {
        public string Name { get; set; }
        public string Group { get; set; }
        public object Data { get; set; }
    }

    [Serializable()]
    public class FieldTransform
    {
        public FieldTransform() { }

        public FieldTransform(string Name, string Group, object Data)
        {
            Source = new FieldMapping() {
                Name = Name,
                Group = Group,
                Data = Data
            };

            Destination = Source;
        }

        public FieldMapping Source { get; set; }
        public FieldMapping Destination { get; set; }
    }
}
