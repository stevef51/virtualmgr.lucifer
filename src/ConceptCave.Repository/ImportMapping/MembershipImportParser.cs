﻿using ConceptCave.BusinessLogic;
using ConceptCave.BusinessLogic.Facilities;
using ConceptCave.BusinessLogic.LingoDataObjects;
using ConceptCave.BusinessLogic.Context;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.RunTime;
using ConceptCave.Core;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.HelperClasses;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Importing;
using ConceptCave.RepositoryInterfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Repository.Injected;
using VirtualMgr.Common;

namespace ConceptCave.Repository.ImportMapping
{
    public class MembershipImportParser : ImportParser<int>, IImportWriter
    {
        public class FakeCurrentContext : IContextContainer
        {
            public bool Has<T>()
            {
                return false;
            }

            public bool Has<T>(string name)
            {
                return false;
            }

            public T Get<T>()
            {
                return default(T);
            }

            public T Get<T>(Func<T> defaultValueCallback)
            {
                return default(T);
            }

            public T Get<T>(string name)
            {
                return default(T);
            }

            public T Get<T>(string name, Func<T> defaultValueCallback)
            {
                return default(T);
            }

            public void Set<T>(string name, T context)
            {

            }

            public void Set<T>(T context)
            {

            }

            public void Set(Type T, string name, object context)
            {

            }

            public void CopyTo(IContextContainer target)
            {

            }
        }

        protected int CurrentUserTypeId { get; set; }
        protected FacilityUserHelper CurrentEntity { get; set; }
        protected List<string> ContextsLoaded { get; set; }


        public Dictionary<string, ConceptCave.BusinessLogic.Facilities.HierarchyFacility.HierachyFacilityBucketItem> HierachyRoots { get; set; }
        public Dictionary<string, Dictionary<string, List<ConceptCave.BusinessLogic.Facilities.HierarchyFacility.HierachyFacilityBucketItem>>> CachedHierachyItems { get; set; }
        public Dictionary<string, HierarchyRoleDTO> RoleCache { get; set; }

        public Dictionary<string, CompanyDTO> CompanyCache { get; set; }

        protected bool HasCurrentEntityHierachyParentUsername { get; set; }
        protected string CurrentEntityHierachyParentUsername { get; set; }
        protected bool HasCurrentEntityHierachyRole { get; set; }
        protected string CurrentEntityHierachyRole { get; set; }
        protected bool HasCurrenEntityHierachyIsPrimary { get; set; }
        protected string CurrentEntityHierachyIsPrimary { get; set; }
        protected bool HasCurrentEntityHierachyName { get; set; }
        protected string CurrentEntityHierarchyName { get; set; }
        protected bool HasCurrentEntityHierarchyIndex { get; set; }
        protected string CurrentEntityHierarchyIndex { get; set; }

        protected List<string> GenericFields { get; set; }
        protected List<string> HierachyFields { get; set; }

        protected IContextManagerFactory ContextManagerFactory { get; set; }
        protected IMembershipRepository MemberRepo { get; set; }
        protected ILabelRepository LabelRepo { get; set; }

        protected IHierarchyRepository HierarchyRepo { get; set; }

        protected IWorkingDocumentStateManager WorkingDocumentStateManager { get; set; }

        protected ICompanyRepository CompanyRepo { get; set; }

        protected IFacilityStructureRepository FacilityStructureRepo { get; set; }

        protected IMembershipTypeRepository MembershipTypeRepo { get; set; }

        protected IRoleRepository RoleRepo { get; set; }
        private readonly Func<MembershipFacility> _fnMembershipFacility;

        public MembershipImportParser(IContextManagerFactory contextManagerFactory,
            IMembershipRepository memberRepo,
            ILabelRepository labelRepo,
            IHierarchyRepository hierarchRepo,
            IWorkingDocumentStateManager workingDocumentStateManager,
            ICompanyRepository companyRepo,
            IFacilityStructureRepository facilityStructureRepo,
            IMembershipTypeRepository membershipTypeRepo,
            IRoleRepository roleRepo,
            Func<MembershipFacility> fnMembershipFacility)
        {
            GenericFields = new List<string>();
            GenericFields.Add("Username");
            GenericFields.Add("Name");
            GenericFields.Add("Email");
            GenericFields.Add("Latitude");
            GenericFields.Add("Longitude");
            GenericFields.Add("Timezone");
            GenericFields.Add("Expiry Date");
            GenericFields.Add("Labels");
            GenericFields.Add("Roles");
            GenericFields.Add("Company");
            GenericFields.Add("Facility Zone");

            HierachyFields = new List<string>();

            HierachyFields.Add("Parent");
            HierachyFields.Add("Role");
            HierachyFields.Add("IsPrimary");
            HierachyFields.Add("Hierarchy");
            HierachyFields.Add("Index");

            ContextManagerFactory = contextManagerFactory;
            MemberRepo = memberRepo;
            LabelRepo = labelRepo;
            HierarchyRepo = hierarchRepo;
            WorkingDocumentStateManager = workingDocumentStateManager;
            CompanyRepo = companyRepo;
            FacilityStructureRepo = facilityStructureRepo;
            MembershipTypeRepo = membershipTypeRepo;
            RoleRepo = roleRepo;
            _fnMembershipFacility = fnMembershipFacility;

            HierachyRoots = new Dictionary<string, HierarchyFacility.HierachyFacilityBucketItem>();

            CachedHierachyItems = new Dictionary<string, Dictionary<string, List<HierarchyFacility.HierachyFacilityBucketItem>>>();

            RoleCache = new Dictionary<string, HierarchyRoleDTO>();
            CompanyCache = new Dictionary<string, CompanyDTO>();
        }

        public bool RequiresUniqueKey
        {
            get
            {
                return true;
            }
        }

        public override List<FieldMapping> Parse(int source, List<FieldMapping> mappings)
        {
            List<FieldMapping> result = new List<FieldMapping>();

            // usual suspects first
            foreach (var fieldName in GenericFields)
            {
                AddWhenFieldNotIncluded(fieldName, "General", fieldName, mappings, result);
            }

            foreach (var fieldName in HierachyFields)
            {
                AddWhenFieldNotIncluded(fieldName, "Hierachy", fieldName, mappings, result);
            }

            var userTypes = MembershipTypeRepo.GetByUserType(source, UserTypeContextLoadInstructions.PublishingGroupResource | UserTypeContextLoadInstructions.PublishingGroupResourceData);

            foreach (var type in userTypes)
            {
                string docXml = type.PublishingGroupResource.PublishingGroupResourceData.Data;

                // get a design document
                var doc = OPublishingGroupResourceRepository.DesignDocumentFromString(docXml);
                WorkingDocumentStateManager.ResolveWorkingDocumentTemplates(doc.AsDepthFirstEnumerable());

                // now we just need to go through the questions and create a field for each
                foreach (var q in (from q in doc.AsDepthFirstEnumerable() where q is IQuestion select q).Cast<IQuestion>())
                {
                    AddWhenFieldNotIncluded(q.Prompt, type.PublishingGroupResource.Name, type.PublishingGroupResource.Name + ":" + q.Id.ToString(), mappings, result);
                }
            }

            return result;
        }

        protected void AddWhenFieldNotIncluded(string name, string group, object data, List<FieldMapping> source, List<FieldMapping> result)
        {
            var existing = (from m in source where m.Name == name select m).DefaultIfEmpty(null).First();

            if (existing != null)
            {
                existing.Data = data;
                result.Add(existing);
            }

            var mapping = new FieldMapping() { Name = name, Group = group, Data = data };
            result.Add(mapping);
        }

        public override ImportParserResult Verify(int source, List<FieldMapping> mappings)
        {
            return new ImportParserResult();
        }

        public override ImportParserResult Import(int source, List<FieldTransform> transforms, IImportWriter writer, bool isTestOnly, Action<int> progressCallback)
        {
            throw new NotImplementedException();
        }

        public override IImportWriter GetWriter(int source)
        {
            CurrentUserTypeId = source;
            return this;
        }

        public FieldTransform GetUniqueKeyTransform(List<FieldTransform> transforms)
        {
            var transform = (from t in transforms where t.Destination.Name.Equals("Username") select t).DefaultIfEmpty(null).First();

            return transform;
        }

        public void LoadEntity(string uniqueId)
        {
            var u = MemberRepo.GetByUsername((uniqueId), MembershipLoadInstructions.AspNetMembership |
                MembershipLoadInstructions.MembershipRole |
                MembershipLoadInstructions.Label |
                MembershipLoadInstructions.MembershipLabel |
                MembershipLoadInstructions.MembershipType |
                MembershipLoadInstructions.ContextResources | MembershipLoadInstructions.ContextData);

            if (u == null)
            {
                var fac = _fnMembershipFacility();
                CurrentEntity = fac.New(new FakeCurrentContext(), uniqueId, "12345678", "New Import", "", CurrentUserTypeId);
            }
            else
            {
                CurrentEntity = new FacilityUserHelper(MemberRepo, LabelRepo, ContextManagerFactory, u);
            }

            ContextsLoaded = new List<string>();
        }

        public void SaveEntity(string uniqueId)
        {
            CurrentEntity.Save();

            var fake = new FakeCurrentContext();

            foreach (var name in ContextsLoaded)
            {
                object context = null;
                if (CurrentEntity.GetObject(fake, name, out context) == true)
                {
                    ((UserContextWorkingDocumentObjectGetter)context).Save();
                }
            }

            HasCurrentEntityHierachyParentUsername = false;
            CurrentEntityHierachyParentUsername = null;
            HasCurrentEntityHierachyRole = false;
            CurrentEntityHierachyRole = null;
            HasCurrenEntityHierachyIsPrimary = false;
            CurrentEntityHierachyIsPrimary = null;
            HasCurrentEntityHierachyName = false;
            CurrentEntityHierarchyName = null;
            HasCurrentEntityHierarchyIndex = false;
            CurrentEntityHierarchyIndex = null;
        }

        public void WriteValue(FieldTransform transform, string value)
        {
            if (WriteGenericField(transform, value) == true)
            {
                return;
            }

            if (WriteHierachyField(transform, value) == true)
            {
                return;
            }

            string[] parts = transform.Destination.Data.ToString().Split(':');

            if (parts.Length != 2)
            {
                throw new ArgumentException("Destination transform does not map to this type of importer");
            }

            var fake = new FakeCurrentContext();

            string contextName = parts[0];
            object getter = null;

            if (CurrentEntity.GetObject(fake, contextName, out getter) == false)
            {
                throw new ArgumentException("Destination data does not map to a known context entry for the membership type");
            }

            UserContextWorkingDocumentObjectGetter context = (UserContextWorkingDocumentObjectGetter)getter;

            Guid qid = Guid.Parse(parts[1]);

            var question = (from p in context.WorkingDocument.Presented from q in p.AsDepthFirstEnumerable() where q is IPresentedQuestion && ((IPresentedQuestion)q).Question.Id.Equals(qid) select q).DefaultIfEmpty(null).First();

            if (question == null)
            {
                throw new ArgumentException(string.Format("Destination data does not map to a known question in context {0}", parts[0]));
            }

            var answer = (Answer)((IPresentedQuestion)question).GetAnswer();

            if (answer.AnswerValue.ToString() != value)
            {
                answer.AnswerValue = value;

                if (ContextsLoaded.Contains(contextName) == false)
                {
                    ContextsLoaded.Add(contextName);
                }
            }
        }

        public void Complete()
        {
            if (HierachyRoots.Count == 0)
            {
                return;
            }

            var fac = new HierarchyFacility(MemberRepo, HierarchyRepo);
            foreach (var h in HierachyRoots.Values)
            {
                fac.Save(h);
            }
        }

        protected bool WriteHierachyField(FieldTransform transform, string value)
        {
            var generic = (from g in HierachyFields where g.Equals(transform.Destination.Data) select g).DefaultIfEmpty(null).First();

            if (generic == null)
            {
                return false;
            }

            // we need a few fields for us to do our thing, lets just cache the current encounter for the moment, later on
            // if we have what we need we can do the work then.
            switch (generic)
            {
                case "Parent":
                    HasCurrentEntityHierachyParentUsername = true;
                    CurrentEntityHierachyParentUsername = value;
                    break;
                case "Role":
                    HasCurrentEntityHierachyRole = true;
                    CurrentEntityHierachyRole = value;
                    break;
                case "IsPrimary":
                    HasCurrenEntityHierachyIsPrimary = true;
                    CurrentEntityHierachyIsPrimary = value;
                    break;
                case "Hierarchy":
                    HasCurrentEntityHierachyName = true;
                    CurrentEntityHierarchyName = value;
                    break;
                case "Index":
                    HasCurrentEntityHierarchyIndex = true;
                    CurrentEntityHierarchyIndex = value;
                    break;
            }

            if (HasCurrentEntityHierachyParentUsername == true &&
                HasCurrentEntityHierachyRole == true &&
                HasCurrenEntityHierachyIsPrimary == true &&
                HasCurrentEntityHierachyName == true &&
                HasCurrentEntityHierarchyIndex == true)
            {
                bool isPrimary = false;
                if (bool.TryParse(CurrentEntityHierachyIsPrimary, out isPrimary) == false)
                {
                    throw new InvalidOperationException(string.Format("{0} is not a valid value for the IsPrimary field", value));
                }

                int index = -1;
                if (int.TryParse(CurrentEntityHierarchyIndex, out index) == false)
                {
                    throw new InvalidOperationException(string.Format("{0} is not a valid value for the hierarchy index field", value));
                }

                if (index < 1)
                {
                    throw new InvalidOperationException(string.Format("{0} is not a valid hierarchy index value", index));
                }

                // we have both bits we need now, so lets do our thing
                var hr = LoadHierachy();

                HierarchyRoleDTO role = null;
                if (RoleCache.TryGetValue(CurrentEntityHierachyRole, out role) == false)
                {
                    var roles = HierarchyRepo.GetRolesByName(CurrentEntityHierachyRole);

                    if (roles.Count == 0)
                    {
                        throw new InvalidOperationException(string.Format("{0} is not a recognised role in the hierarchy", CurrentEntityHierachyRole));
                    }

                    role = roles.First();
                    RoleCache.Add(CurrentEntityHierachyRole, role);
                }

                var parents = ParseCommaString(CurrentEntityHierachyParentUsername);

                // first things first, we'll loop through the parents and clean out any buckets of the relevant role
                foreach (var p in parents)
                {
                    if (string.IsNullOrEmpty(p) == true)
                    {
                        continue;
                    }

                    List<HierarchyFacility.HierachyFacilityBucketItem> bucket = null;

                    var cache = CachedHierachyItems[CurrentEntityHierarchyName]; // grab the cached hierarchy cache for the current hierarchy

                    if (cache.TryGetValue(p, out bucket) == false) // if we haven't encountered this parent in this hierarchy before, we have some work to do
                    {
                        bucket = hr.FindChildrenForUser(p);

                        if (bucket.Count == 0)
                        {
                            throw new InvalidOperationException(string.Format("No membership entry exists for username {0}", p));
                        }

                        cache.Add(p, bucket);
                    }

                    // for the moment I'm only catering to a parent and child being in the hierarchy in one spot, so am doing a first on the below
                    // select. Below we are going to select out the buckets from this parent that match the required role
                    var roleChildren = (from c in bucket.First().Children where (int)c.Role == role.Id select c).ToList();

                    if (index - 1 > roleChildren.Count)
                    {
                        throw new InvalidOperationException(string.Format("There aren't enough {0} entries under {1} to have an index of {2}", role.Name, p, index));
                    }

                    // just write our user into that entry in the hierarchy and job done.
                    roleChildren[index - 1].User = CurrentEntity.User.UserId;
                }
            }

            return true;
        }

        protected bool WriteGenericField(FieldTransform transform, string value)
        {
            var generic = (from g in GenericFields where g.Equals(transform.Destination.Data) select g).DefaultIfEmpty(null).First();

            if (generic == null)
            {
                return false;
            }

            switch (generic)
            {
                case "Username":
                    CurrentEntity.User.AspNetUser.UserName = value;
                    break;

                case "Name":
                    CurrentEntity.User.Name = value;
                    break;

                case "Email":
                    CurrentEntity.Email = value;
                    break;

                case "Latitude":
                    if (string.IsNullOrEmpty(value))
                    {
                        CurrentEntity.User.Latitude = null;
                        break;
                    }

                    decimal lat = 0;
                    if (decimal.TryParse(value, out lat) == true)
                    {
                        CurrentEntity.User.Latitude = lat;
                    }
                    else
                    {
                        throw new ArgumentException("Invalid latitude supplied");
                    }

                    break;

                case "Longitude":
                    if (string.IsNullOrEmpty(value))
                    {
                        CurrentEntity.User.Longitude = null;
                        break;
                    }

                    decimal lng = 0;
                    if (decimal.TryParse(value, out lng) == true)
                    {
                        CurrentEntity.User.Longitude = lng;
                    }
                    else
                    {
                        throw new ArgumentException("Invalid longitude supplied");
                    }
                    break;

                case "Timezone":
                    var tz = TimeZoneInfoResolver.ResolveWindows(value);

                    CurrentEntity.User.TimeZone = tz.Id;
                    break;

                case "Expiry Date":
                    if (string.IsNullOrEmpty(value))
                    {
                        CurrentEntity.User.ExpiryDate = null;
                        break;
                    }

                    DateTime d = DateTime.MinValue;

                    if (DateTime.TryParse(value, out d) == true)
                    {
                        CurrentEntity.User.ExpiryDate = d;
                    }

                    break;
                case "Labels":
                    if (string.IsNullOrEmpty(value))
                    {
                        var ul = (from l in CurrentEntity.User.LabelUsers select l.LabelId).ToArray();

                        if (ul.Length == 0)
                        {
                            break;
                        }

                        MemberRepo.RemoveFromLabels(ul, CurrentEntity.User.UserId);
                        break;
                    }

                    var ls = ParseCommaString(value);

                    // select out labels that the member is currently in, but aren't in the string given
                    var toDelete = (from l in CurrentEntity.User.LabelUsers where ls.Contains(l.Label.Name) == false select l.LabelId).ToArray();

                    // select out labels that the member is not in, but are in the string given
                    var toAdd = (from l in ls where (from u in CurrentEntity.User.LabelUsers select u.Label.Name).Contains(l) == false select l);

                    if (toDelete.Length != 0)
                    {
                        MemberRepo.RemoveFromLabels(toDelete, CurrentEntity.User.UserId);
                    }

                    List<Guid> newIds = new List<Guid>();
                    foreach (var lbl in toAdd)
                    {
                        var label = LabelRepo.GetByName(lbl);

                        if (label == null)
                        {
                            continue;
                        }

                        newIds.Add(label.Id);
                    }

                    if (newIds.Count != 0)
                    {
                        MemberRepo.AddLabels(CurrentEntity.User.UserId, newIds.ToArray());
                    }

                    break;
                case "Roles":
                    if (string.IsNullOrEmpty(value))
                    {
                        var rl = (from r in CurrentEntity.User.AspNetUser.AspNetUserRoles select r.RoleId).ToArray();

                        if (rl.Length == 0)
                        {
                            break;
                        }

                        MemberRepo.RemoveFromRoles(rl, CurrentEntity.User.UserId);
                        break;
                    }

                    var rs = ParseCommaString(value);

                    // select out roles that the member is currently in, but aren't in the string given
                    var td = (from l in CurrentEntity.User.AspNetUser.AspNetUserRoles where rs.Contains(l.AspNetRole.Name) == false select l.RoleId).ToArray();

                    // select out roles that the member is not in, but are in the string given
                    var ta = (from l in rs where (from u in CurrentEntity.User.AspNetUser.AspNetUserRoles select u.AspNetRole.Name).Contains(l) == false select l);

                    if (td.Length != 0)
                    {
                        MemberRepo.RemoveFromRoles(td, CurrentEntity.User.UserId);
                    }

                    List<string> newRoleIds = new List<string>();
                    foreach (var lbl in ta)
                    {
                        var role = RoleRepo.GetByName(lbl);

                        if (role == null)
                        {
                            continue;
                        }

                        newRoleIds.Add(role.Id);
                    }

                    if (newRoleIds.Count != 0)
                    {
                        MemberRepo.AddRoles(CurrentEntity.User.UserId, newRoleIds.ToArray());
                    }

                    break;
                case "Company":
                    CompanyDTO company = null;
                    if (CompanyCache.TryGetValue(value, out company) == false)
                    {
                        var companies = CompanyRepo.GetAll(CompanyLoadInstructions.None);

                        foreach (var c in companies)
                        {
                            if (CompanyCache.ContainsKey(c.Name) == true)
                            {
                                continue;
                            }

                            CompanyCache.Add(c.Name, c);
                        }

                        if (CompanyCache.TryGetValue(value, out company) == false)
                        {
                            throw new InvalidOperationException(string.Format("Company {0} does not exist", value));
                        }
                    }

                    CurrentEntity.User.CompanyId = company.Id;

                    break;

                case "Facility Zone":
                    if (string.IsNullOrEmpty(value))
                    {
                        // Delete the Facility Site that might map to this Membership Site
                        var site = FacilityStructureRepo.FindBySiteId(CurrentEntity.Id);
                        if (site != null)
                        {
                            bool archived = false;
                            FacilityStructureRepo.Delete(site.Id, true, out archived);
                        }
                    }
                    else
                    {
                        int zoneId = 0;
                        // This could be in 1 of 2 formats 
                        // a. Zone Name | Floor Name | Building Name | Facility Name
                        // b. Zone Id
                        System.Text.RegularExpressions.Regex rex = new System.Text.RegularExpressions.Regex(@"(?<zone>.+)\|(?<floor>.+)\|(?<building>.+)\|(?<facility>.+)");
                        var match = rex.Match(value);
                        if (match.Success)
                        {
                            var facilityName = match.Groups["facility"].Value.Trim();
                            var buildingName = match.Groups["building"].Value.Trim();
                            var floorName = match.Groups["floor"].Value.Trim();
                            var zoneName = match.Groups["zone"].Value.Trim();

                            // Find the Facility by name
                            var facility = FacilityStructureRepo.FindByName(FacilityStructureType.Facility, facilityName);
                            if (facility == null)
                            {
                                throw new InvalidOperationException(string.Format("Facility {0} not found", facilityName));
                            }
                            var building = FacilityStructureRepo.FindByName(facility.Id, buildingName);
                            if (building == null)
                            {
                                throw new InvalidOperationException(string.Format("Building '{0}' not found in Facility '{1}'", buildingName, facilityName));
                            }
                            var floor = FacilityStructureRepo.FindByName(building.Id, floorName);
                            if (floor == null)
                            {
                                throw new InvalidOperationException(string.Format("Floor '{0}' not found in Building '{1}' of Facility '{2}'", floorName, buildingName, facilityName));
                            }
                            var zone = FacilityStructureRepo.FindByName(floor.Id, zoneName);
                            if (zone == null)
                            {
                                throw new InvalidOperationException(string.Format("Zone '{0}' not found in Floor '{1}' of Building '{2}' of Facility '{3}'", zoneName, floorName, buildingName, facilityName));
                            }
                            zoneId = zone.Id;
                        }
                        else
                        {
                            if (!int.TryParse(value, out zoneId))
                            {
                                throw new InvalidOperationException(string.Format("Facility Zone '{0}' is not one of \na. Zone Name | Floor Name | Building Name | Facility Name\nb. Zone Id", value));
                            }

                            var zone = FacilityStructureRepo.GetById(zoneId);
                            if (zone == null)
                            {
                                throw new InvalidOperationException(string.Format("Facilty Zone with Id '{0}' does not exist", zoneId));
                            }
                        }

                        // See if there is a Facility Site under the Zone with same name
                        var site = FacilityStructureRepo.FindBySiteId(CurrentEntity.Id);
                        if (site == null)
                        {
                            site = new FacilityStructureDTO()
                            {
                                __IsNew = true,
                                ParentId = zoneId,
                                SiteId = CurrentEntity.Id,
                                StructureType = (int)(FacilityStructureType.Site)
                            };
                        }
                        else
                        {
                            // Since only Facility Sites can have be found by the Membership SiteId this record *should* be a Facility Site
                            // set its parent Zone
                            site.ParentId = zoneId;
                        }
                        site = FacilityStructureRepo.Save(site, true, false);
                    }
                    break;
            }

            return true;
        }

        protected HierarchyFacility.HierachyFacilityBucketItem LoadHierachy()
        {
            if (HierachyRoots.ContainsKey(CurrentEntityHierarchyName) == true)
            {
                return HierachyRoots[CurrentEntityHierarchyName];
            }

            var fac = new HierarchyFacility(MemberRepo, HierarchyRepo);
            var result = fac.Load(CurrentEntityHierarchyName);

            if (result == null)
            {
                return null;
            }

            HierachyRoots.Add(CurrentEntityHierarchyName, result);

            CachedHierachyItems.Add(CurrentEntityHierarchyName, new Dictionary<string, List<HierarchyFacility.HierachyFacilityBucketItem>>());

            return result;
        }

        protected string[] ParseCommaString(string toParse)
        {
            string[] split = toParse.Split(',');

            List<string> splits = new List<string>();
            foreach (string s in split)
            {
                splits.Add(s.Trim());
            }

            return splits.ToArray();
        }
    }
}
