﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Editor;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.RunTime;
using ConceptCave.Core;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Repository.Facilities;
using ConceptCave.BusinessLogic.LingoDataObjects;
using ConceptCave.BusinessLogic.Context;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.BusinessLogic.Facilities;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.BusinessLogic;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Importing;

namespace ConceptCave.Repository.ImportMapping
{
    public abstract class ImportParser<T>
    {
        public abstract List<FieldMapping> Parse(T source, List<FieldMapping> mappings);
        public abstract ImportParserResult Verify(T source, List<FieldMapping> mappings);
        public abstract ImportParserResult Import(T source, List<FieldTransform> transforms, IImportWriter writer, bool isTestOnly, Action<int> progressCallback);

        public abstract IImportWriter GetWriter(T source);
    }

    public abstract class ExportParser<T> : ImportParser<T>
    {
        public bool IncludeTitles { get; set; }
        public string[] ExportBoolsAs { get; set; }
        public string TimespanExportFormat { get; set; }
        public string DateTimeExportFormat { get; set; }

        protected Dictionary<string, string> LookupFormatCache { get; set; }

        public ExportParser()
        {
            ExportBoolsAs = new string[] { "Y", "N" };
            LookupFormatCache = new Dictionary<string, string>();
        }

        public virtual string BoolToExportString(bool value)
        {
            if (ExportBoolsAs == null || ExportBoolsAs.Length != 2)
            {
                ExportBoolsAs = new string[] { "Y", "N" };
            }

            return value == true ? ExportBoolsAs[0] : ExportBoolsAs[1];
        }

        public virtual string TimespanToExportString(TimeSpan? value)
        {
            if (value.HasValue == false)
            {
                return string.Empty;
            }

            if (string.IsNullOrEmpty(TimespanExportFormat) == true)
            {
                return value.Value.ToString();
            }
            else
            {
                return value.Value.ToString(TimespanExportFormat);
            }
        }

        public virtual string DateTimeToExportString(DateTime? value)
        {
            if (value.HasValue == false)
            {
                return string.Empty;
            }

            if (string.IsNullOrEmpty(DateTimeExportFormat) == true)
            {
                return value.Value.ToString();
            }
            else
            {
                return value.Value.ToString(DateTimeExportFormat);
            }
        }

        public virtual string IntToExportString(int? value)
        {
            if (value.HasValue == false)
            {
                return string.Empty;
            }

            return value.Value.ToString();
        }

        public void WriteTitles(List<FieldTransform> transforms, IImportWriter writer)
        {
            if (IncludeTitles == true)
            {
                writer.LoadEntity(null);

                foreach (var transform in transforms)
                {
                    writer.WriteValue(transform, transform.Destination.Name);
                }

                if (writer is IImportExcelWriter)
                {
                    ((IImportExcelWriter)writer).CurrentCell.WorksheetRow().Style.Font.Bold = true;
                }

                writer.SaveEntity(null);
            }
        }

        protected void SetLookupColumn(IImportWriter writer, bool useList, string lookupName)
        {
            if (LookupFormatCache.ContainsKey(lookupName))
            {
                return;
            }

            if (writer is IImportExcelWriter)
            {
                IImportExcelWriter excel = ((IImportExcelWriter)writer);

                var lookupSheet = excel.GetLookupSheet(lookupName);

                if (lookupSheet.FirstCellUsed() == null)
                {
                    return;
                }

                excel.CurrentCell.WorksheetColumn()
                    .SetDataValidation()
                    .List(lookupSheet.Range(lookupSheet.FirstCellUsed(), lookupSheet.LastCellUsed()), true);

                excel.CurrentCell.WorksheetColumn().SetDataValidation().InCellDropdown = useList;

                LookupFormatCache.Add(lookupName, "a");
            }
        }
    }

    public interface IImportWriter
    {
        FieldTransform GetUniqueKeyTransform(List<FieldTransform> transforms);

        void LoadEntity(string uniqueId);
        void SaveEntity(string uniqueId);

        void WriteValue(FieldTransform transform, string value);

        void Complete();

        bool RequiresUniqueKey { get; }
    }

    public interface IImportExcelWriter : IImportWriter
    {
        ClosedXML.Excel.IXLCell CurrentCell { get; }

        void AddLookupSheet(string name, List<string> values);
        ClosedXML.Excel.IXLWorksheet GetLookupSheet(string name);
    }



    /// <summary>
    /// Class that is used during an import process and is responsible for running through an Excel file and calling
    /// the relevant methods on a IWriter object that writes the relevant data to the relvant destination
    /// </summary>
    public class ExcelImportParser : ImportParser<Stream>
    {
        /// <summary>
        /// Takes a stream that contains an xlsx file, runs through the first row of the first worksheet populates the mappings
        /// list with fields where the field doesn't already exist in the mapping list
        /// </summary>
        /// <param name="template">Stream to be parsed</param>
        /// <param name="mappings">Collection of existing fields that should be altered</param>
        /// <returns>a new list of mappings based on the mappings handed in and the template</returns>
        public override List<FieldMapping> Parse(Stream source, List<FieldMapping> mappings)
        {
            var workBook = new ClosedXML.Excel.XLWorkbook(source, ClosedXML.Excel.XLEventTracking.Disabled);

            int workSheets = workBook.Worksheets.Count;

            List<FieldMapping> result = new List<FieldMapping>();

            for (int i = 1; i <= workSheets; i++)
            {
                var workSheet = workBook.Worksheet(i);

                // only import from things that are visible
                if (workSheet.Visibility != ClosedXML.Excel.XLWorksheetVisibility.Visible)
                {
                    continue;
                }

                var currentRow = workSheet.FirstRowUsed();

                // ok, pretty simple really, just run through the used cells, grab their contents and use this to create field mappings if required
                foreach (var cell in currentRow.CellsUsed())
                {
                    string name = cell.Value.ToString();

                    var existing = (from m in mappings where m.Name == name select m).DefaultIfEmpty(null).First();

                    if (existing != null)
                    {
                        if ((from r in result where r.Name == name select r).Count() > 0)
                        {
                            // we've already seen the field before in the excel document, we aren't going to add it again.
                            // NOTE: This means the unique key fields must be in the same column in each worksheet
                            existing.Group = string.Format("{0}, {1}", existing.Group, i);
                            continue;
                        }

                        existing.Group = i.ToString();
                        existing.Data = cell.Address.ColumnNumber;
                        result.Add(existing);

                        continue;
                    }

                    // ok there isn't a mapping with the same name present in the mappings, so create one
                    var mapping = new FieldMapping() { Name = name, Group = i.ToString(), Data = cell.Address.ColumnNumber };
                    result.Add(mapping);
                }
            }

            return result;
        }

        public override ImportParserResult Verify(Stream source, List<FieldMapping> mappings)
        {
            // ok we need to check that the stream matches the same format as we've been given.
            var sourceMappings = Parse(source, mappings);

            var result = new ImportParserResult();

            if (sourceMappings.Count != mappings.Count)
            {
                result.Errors.Add(string.Format("Import file has {0} columns, the template has {1} columns. Files do not match", sourceMappings.Count, mappings.Count));

                return result;
            }

            // loop through the mappings and make sure the column names are the same exist in both
            foreach (var m in mappings)
            {
                var sm = (from s in sourceMappings where s.Name.Equals(m.Name) select s).DefaultIfEmpty(null).First();

                if (sm == null)
                {
                    result.Errors.Add(string.Format("Template contains column '{0}' which is not present in the import file", m.Name));
                    continue;
                }

                if (m.Data.Equals(sm.Data) == false)
                {
                    result.Errors.Add(string.Format("Template column '{0}' maps to column {1} where as the import file has column {0} at {2}", m.Name, m.Data.ToString(), sm.Data.ToString()));
                    continue;
                }
            }

            return result;
        }

        public override IImportWriter GetWriter(Stream source)
        {
            return null;
        }

        public override ImportParserResult Import(Stream source, List<FieldTransform> transforms, IImportWriter writer, bool isTestOnly, Action<int> progressCallback)
        {
            var workBook = new ClosedXML.Excel.XLWorkbook(source, ClosedXML.Excel.XLEventTracking.Disabled);

            int workSheets = workBook.Worksheets.Count;

            var uniqueKeyTransform = writer.GetUniqueKeyTransform(transforms);

            var result = new ImportParserResult();

            ClosedXML.Excel.IXLRow currentRow = null;

            for (int i = 1; i <= workSheets; i++)
            {
                try
                {
                    var workSheet = workBook.Worksheet(i);

                    // only import from things that are visible
                    if (workSheet.Visibility != ClosedXML.Excel.XLWorksheetVisibility.Visible)
                    {
                        continue;
                    }

                    currentRow = workSheet.FirstRowUsed();

                    if (currentRow == null)
                    {
                        // we are just going to skip blank worksheets (the cause of IM-71)
                        continue;
                    }

                    currentRow = currentRow.RowBelow();
                }
                catch (Exception e)
                {
                    result.Errors.Add(string.Format("An issue was encountered attempting to read from the worksheet at index {0}, the message was {1}", i, e.Message));

                }

                var ts = (from t in transforms where t.Source.Group == i.ToString() select t); // we only want the transforms for our worksheet

                while (currentRow.IsEmpty() == false)
                {
                    var uniqueKey = currentRow.Cell((int)uniqueKeyTransform.Source.Data).Value.ToString();

                    if (string.IsNullOrEmpty(uniqueKey) == true && writer.RequiresUniqueKey == true)
                    {
                        // we have blank for the unique key cell, which doesn't make much sense from a writing to the database point of view
                        // assume its a blank row and move onto the next row
                        currentRow = currentRow.RowBelow();
                        continue;
                    }

                    try
                    {
                        writer.LoadEntity(uniqueKey);
                    }
                    catch (Exception e)
                    {
                        result.Errors.Add(string.Format("Record with id {0} in sheet {3} row {2} could not be loaded due to {1}", uniqueKey, e.Message, currentRow.FirstCell().Address.RowNumber, i));
                        currentRow = currentRow.RowBelow();
                        continue;
                    }

                    var lastUsedCell = currentRow.CellsUsed().Last().Address.ColumnNumber;

                    foreach (var t in ts)
                    {
                        string value = "";
                        try
                        {
                            value = currentRow.Cell((int)t.Source.Data).GetString();
                        }
                        catch (Exception)
                        {
                            result.Errors.Add(string.Format("Record with id {0} in sheet {2} row {1} does not conform to the import standard", uniqueKey, currentRow.FirstCell().Address.RowNumber, i));
                            currentRow = currentRow.RowBelow();
                            continue;
                        }

                        try
                        {
                            writer.WriteValue(t, value);
                        }
                        catch (Exception e)
                        {
                            result.Errors.Add(string.Format("An issue was encountered writing {0} for {1} for entity with unique key of {2} from sheet {4} at row {5} due to {3}", value, t.Source.Name, uniqueKey, e.Message, i, currentRow.FirstCell().Address.RowNumber));
                        }
                    }

                    if (isTestOnly == false)
                    {
                        try
                        {
                            writer.SaveEntity(uniqueKey);
                        }
                        catch (Exception e)
                        {
                            result.Errors.Add(string.Format("Record with id {0} in sheet {2} could not be written due to {1}", uniqueKey, e.Message, i));
                        }
                    }

                    result.Count++;

                    progressCallback(result.Count);

                    currentRow = currentRow.RowBelow();
                }
            }

            try
            {
                writer.Complete();
            }
            catch (Exception e)
            {
                result.Errors.Add(string.Format("There was an issue completing the write operation, the error message was {0}", e.Message));
            }


            return result;
        }
    }

    /// <summary>
    /// Class that is used during an export to Excel operation and is responsible for writing values to
    /// Excel.
    /// </summary>
    public class ExcelWriter : IImportWriter, IImportExcelWriter
    {
        public ClosedXML.Excel.XLWorkbook Book { get; protected set; }

        public ClosedXML.Excel.IXLWorksheet CurrentSheet { get; protected set; }

        public ClosedXML.Excel.IXLRow CurrentRow { get; protected set; }

        public ClosedXML.Excel.IXLCell CurrentCell { get; protected set; }

        public ExcelWriter()
        {
            Book = new ClosedXML.Excel.XLWorkbook(ClosedXML.Excel.XLEventTracking.Disabled);
            CurrentSheet = Book.AddWorksheet("Export");
        }

        public FieldTransform GetUniqueKeyTransform(List<FieldTransform> transforms)
        {
            return null;
        }

        public void LoadEntity(string uniqueId)
        {
            if (CurrentRow == null)
            {
                CurrentRow = CurrentSheet.FirstRow();
            }
            else
            {
                CurrentRow = CurrentRow.RowBelow();
            }

            CurrentCell = CurrentRow.FirstCell();
        }

        public void SaveEntity(string uniqueId)
        {
        }

        public void WriteValue(FieldTransform transform, string value)
        {
            // Fix for EVS-1035 based on answer in https://stackoverflow.com/questions/32203930/keep-excel-cell-format-as-text-with-date-like-data
            CurrentCell.SetValue<string>(value);
            CurrentCell = CurrentCell.CellRight();
        }

        public void Complete()
        {

        }

        public bool RequiresUniqueKey
        {
            get { return false; }
        }

        public void AddLookupSheet(string name, List<string> values)
        {
            var sheet = Book.AddWorksheet(name);

            var cell = sheet.FirstCell();
            foreach (var value in values)
            {
                cell.Value = value;

                cell = cell.CellBelow();
            }

            sheet.Visibility = ClosedXML.Excel.XLWorksheetVisibility.Hidden;
        }

        public ClosedXML.Excel.IXLWorksheet GetLookupSheet(string name)
        {
            return Book.Worksheet(name);
        }
    }
}
