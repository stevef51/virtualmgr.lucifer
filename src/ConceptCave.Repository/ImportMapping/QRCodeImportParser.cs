﻿using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.RepositoryInterfaces.Importing;

namespace ConceptCave.Repository.ImportMapping
{
    public class QRCodeImportExportTransformFactory
    {
        public const string Id = "Id";
        public const string QRCode = "QRCode";
        public const string UserOrSite = "User/Site";
        public const string TaskType = "Task Type";

        public static string[] All = new string[]
        {
            Id,
            QRCode,
            UserOrSite,
            TaskType
        };           

        public static List<FieldTransform> Create()
        {
            List<FieldTransform> result = new List<FieldTransform>();

            for(var i = 1; i <= All.Length; i++)
            {
                result.Add(new FieldTransform(All[i - 1], "1", i));
            }
            return result;
        }
    }

    public class QRCodeImportWriter : IImportWriter
    {
        protected IQRCodeRepository _qrcodeRepo;
        protected IMembershipRepository _memberRepo;
        protected ITaskTypeRepository _taskTypeRepo;
        protected IAssetRepository _assetRepo;

        public QRCodeImportWriter(IQRCodeRepository qrcodeRepo, IMembershipRepository memberRepo, ITaskTypeRepository taskTypeRepo, IAssetRepository assetRepo)
        {
            _qrcodeRepo = qrcodeRepo;
            _memberRepo = memberRepo;
            _taskTypeRepo = taskTypeRepo;
            _assetRepo = assetRepo;
        }

        protected QrcodeDTO _qrcode;

        protected bool ParseBool(string val)
        {
            // we accept both true/false and y/n
            string v = val.ToLower();

            if (v.Length == 1)
            {
                return v == "y";
            }
            else
            {
                return bool.Parse(v);
            }
        }


        public bool RequiresUniqueKey
        {
            get
            {
                return false;
            }
        }

        public void Complete()
        {
        }

        public FieldTransform GetUniqueKeyTransform(List<FieldTransform> transforms)
        {
            // Note we dont use this, but have to return something 
            var transform = (from t in transforms where t.Destination.Name.Equals(QRCodeImportExportTransformFactory.Id) select t).DefaultIfEmpty(null).First();

            return transform;
        }

        public void LoadEntity(string uniqueId)
        {
            if (string.IsNullOrEmpty(uniqueId))
            {
                _qrcode = new QrcodeDTO()
                {
                    __IsNew = true
                };
            }
            else
            {
                int id = -1;
                if (int.TryParse(uniqueId, out id) == false)
                {
                    throw new ArgumentException(string.Format("{0} is not a valid integer", uniqueId));
                }

                _qrcode = _qrcodeRepo.GetById(id);

                if (_qrcode == null)
                {
                    throw new ArgumentException(string.Format("{0} is not the id of an existing QR Code", uniqueId));
                }
            }
        }

        public void SaveEntity(string uniqueId)
        {
            _qrcodeRepo.Save(_qrcode, true, false);
        }

        public void WriteValue(FieldTransform transform, string value)
        {
            string field = transform.Destination.Name;

            switch (field)
            {
                case QRCodeImportExportTransformFactory.QRCode:
                    _qrcode.Qrcode = value;
                    break;

                case QRCodeImportExportTransformFactory.UserOrSite:
                    if (!string.IsNullOrEmpty(value))
                    {
                        var member = _memberRepo.GetByUsername(value, RepositoryInterfaces.Enums.MembershipLoadInstructions.None);
                        if (member != null)
                            _qrcode.UserId = member.UserId;
                        else
                            throw new ArgumentException("Username not found " + value);
                    }
                    else
                        _qrcode.UserId = null;
                    break;

                case QRCodeImportExportTransformFactory.TaskType:
                    if (!string.IsNullOrEmpty(value))
                    {
                        var tt = _taskTypeRepo.GetByName(value, RepositoryInterfaces.Enums.ProjectJobTaskTypeLoadInstructions.None);
                        if (tt != null)
                            _qrcode.TaskTypeId = tt.Id;
                        else
                            throw new ArgumentException("Task Type not found " + value);
                    }
                    else
                        _qrcode.TaskTypeId = null;
                    break;
            }
        }
    }

    public class QRCodeExportParser : ExportParser<IList<QrcodeDTO>>
    {
        private readonly IQRCodeRepository _qrcodeRepo;
        private readonly ITaskTypeRepository _taskTypeRepo;
        private readonly IMembershipRepository _memberRepo;

        public QRCodeExportParser(IQRCodeRepository qrcodeRepo, IMembershipRepository memberRepo, ITaskTypeRepository taskTypeRepo)
        {
            _qrcodeRepo = qrcodeRepo;
            _memberRepo = memberRepo;
            _taskTypeRepo = taskTypeRepo;
        }


        public bool IncludeArchived { get; set; }

        public override IImportWriter GetWriter(IList<QrcodeDTO> source)
        {
            return null;
        }

        public override ImportParserResult Import(IList<QrcodeDTO> source, List<FieldTransform> transforms, IImportWriter writer, bool isTestOnly, Action<int> progressCallback)
        {
            ImportParserResult result = new ImportParserResult();

            if (isTestOnly == true)
            {
                return result;
            }

            WriteTitles(transforms, writer);

            WriteLookups(transforms, writer);

            foreach (var qrcode in source)
            {
                WriteQRCode(qrcode, writer, transforms);
            }

            return result;
        }

        private void WriteLookups(List<FieldTransform> tranforms, IImportWriter writer)
        {
            var excel = writer as IImportExcelWriter;
            if (excel != null)
            {
                excel.AddLookupSheet("Task Types", (from tt in _taskTypeRepo.GetAll(RepositoryInterfaces.Enums.ProjectJobTaskTypeLoadInstructions.None) select tt.Name).ToList());
                excel.AddLookupSheet("Users", (from tt in _memberRepo.Search("%", 0, 0, RepositoryInterfaces.Enums.MembershipLoadInstructions.AspNetMembership) select tt.AspNetUser.UserName).ToList());
            }
        }

        private void WriteQRCode(QrcodeDTO qrcode, IImportWriter writer, List<FieldTransform> transforms)
        {
            writer.LoadEntity(qrcode.Id.ToString());
            var excel = writer as IImportExcelWriter;
            foreach (var transform in transforms)
            {
                string field = transform.Destination.Name;

                switch (field)
                {
                    case QRCodeImportExportTransformFactory.Id:
                        writer.WriteValue(transform, qrcode.Id.ToString());
                        break;

                    case QRCodeImportExportTransformFactory.QRCode:
                        writer.WriteValue(transform, qrcode.Qrcode);
                        break;

                    case QRCodeImportExportTransformFactory.UserOrSite:
                        SetLookupColumn(writer, true, "Users");
                        writer.WriteValue(transform, qrcode.UserData?.AspNetUser?.UserName);
                        break;

                    case QRCodeImportExportTransformFactory.TaskType:
                        SetLookupColumn(writer, true, "Task Types");
                        writer.WriteValue(transform, qrcode.ProjectJobTaskType?.Name);
                        break;
                }
            }
        }

        public override List<FieldMapping> Parse(IList<QrcodeDTO> source, List<FieldMapping> mappings)
        {
            return mappings;
        }

        public override ImportParserResult Verify(IList<QrcodeDTO> source, List<FieldMapping> mappings)
        {
            return new ImportParserResult();
        }
    }
}
