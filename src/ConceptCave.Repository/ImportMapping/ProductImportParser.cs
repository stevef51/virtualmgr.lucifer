﻿using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.RepositoryInterfaces.Importing;

namespace ConceptCave.Repository.ImportMapping
{
    public class ProductImportExportTransformFactory
    {
        public const string ProductId = "ProductId";
        public const string Name = "Name";
        public const string Description = "Description";
        public const string Code = "Code";
        public const string Price = "Price";
        public const string ProductType = "ProductType";

        public List<FieldTransform> Create()
        {
            List<FieldTransform> result = new List<FieldTransform>();

            FieldTransform tran = new FieldTransform(ProductId, "1", 1);
            result.Add(tran);

            tran = new FieldTransform(Name, "1", 2);
            result.Add(tran);

            tran = new FieldTransform(Description, "1", 3);
            result.Add(tran);

            tran = new FieldTransform(Code, "1", 4);
            result.Add(tran);

            tran = new FieldTransform(Price, "1", 5);
            result.Add(tran);

            tran = new FieldTransform(ProductType, "1", 6);
            result.Add(tran);

            return result;
        }
    }

    public class ProductImportWriter : IImportWriter
    {
        protected IProductRepository _productRepo;
        private IList<ProductTypeDTO> _productTypes;
        protected IList<ProductTypeDTO> ProductTypes
        {
            get
            {
                if (_productTypes == null)
                {
                    _productTypes = _productRepo.GetAllProductTypes();
                }
                return _productTypes;
            }
        }

        public ProductImportWriter(IProductRepository productRepo)
        {
            _productRepo = productRepo;
        }

        protected ProductDTO Current { get; set; }

        public bool RequiresUniqueKey
        {
            get
            {
                return false;
            }
        }

        public void Complete()
        {
            Current = null;
        }

        public FieldTransform GetUniqueKeyTransform(List<FieldTransform> transforms)
        {
            var transform = (from t in transforms where t.Destination.Name.Equals(ProductImportExportTransformFactory.ProductId) select t).DefaultIfEmpty(null).First();

            return transform;
        }

        public void LoadEntity(string uniqueId)
        {
            if (string.IsNullOrEmpty(uniqueId))
            {
                Current = new ProductDTO() { __IsNew = true };
            }
            else
            {
                int id = -1;
                if(int.TryParse(uniqueId, out id) == false)
                {
                    throw new ArgumentException(string.Format("{0} is not a valid integer", uniqueId));
                }

                Current = _productRepo.GetById(id);

                if(Current == null)
                {
                    throw new ArgumentException(string.Format("{0} is not the id of an existing product", uniqueId));
                }
            }
        }

        public void SaveEntity(string uniqueId)
        {
            _productRepo.Save(Current, false);
        }

        public void WriteValue(FieldTransform transform, string value)
        {
            string field = transform.Destination.Name;

            switch (field)
            {
                case ProductImportExportTransformFactory.Name:
                    Current.Name = value;
                    break;
                case ProductImportExportTransformFactory.Description:
                    Current.Description = value;
                    break;
                case ProductImportExportTransformFactory.Code:
                    Current.Code = value;
                    break;
                case ProductImportExportTransformFactory.Price:
                    if (string.IsNullOrEmpty(value))
                    {
                        Current.Price = null;
                    }
                    else
                    {
                        Current.Price = decimal.Parse(value);
                    }
                    break;
                case ProductImportExportTransformFactory.ProductType:
                    Current.ProductType = (from pt in ProductTypes where pt.Name == value select pt).FirstOrDefault();
                    break;
            }
        }
    }

    public class ProductExportParser : ExportParser<IList<ProductDTO>>
    {
        protected const string ProductTypesLookupName = "Product Types";

        private IProductRepository _productRepository;
        private IList<ProductTypeDTO> _productTypes;

        public ProductExportParser(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        public bool IncludeArchived { get; set; }

        public override IImportWriter GetWriter(IList<ProductDTO> source)
        {
            return null;
        }

        public override ImportParserResult Import(IList<ProductDTO> source, List<FieldTransform> transforms, IImportWriter writer, bool isTestOnly, Action<int> progressCallback)
        {
            ImportParserResult result = new ImportParserResult();

            if (isTestOnly == true)
            {
                return result;
            }

            _productTypes = _productRepository.GetAllProductTypes();
            if (writer is IImportExcelWriter)
            {
                IImportExcelWriter excelWriter = (IImportExcelWriter)writer;

                excelWriter.AddLookupSheet(ProductTypesLookupName, (from t in _productTypes select t.Name).ToList());
            }

            WriteTitles(transforms, writer);

            foreach (var product in source)
            {
                WriteProduct(product, writer, transforms);
            }

            return result;
        }

        public void WriteProduct(ProductDTO product, IImportWriter writer, List<FieldTransform> transforms)
        {
            writer.LoadEntity(product.Id.ToString());

            foreach (var transform in transforms)
            {
                string field = transform.Destination.Name;

                switch (field)
                {
                    case ProductImportExportTransformFactory.ProductId:
                        writer.WriteValue(transform, product.Id.ToString());
                        break;
                    case ProductImportExportTransformFactory.Name:
                        writer.WriteValue(transform, product.Name);
                        break;
                    case ProductImportExportTransformFactory.Description:
                        writer.WriteValue(transform, product.Description);
                        break;
                    case ProductImportExportTransformFactory.Code:
                        writer.WriteValue(transform, product.Code);
                        break;
                    case ProductImportExportTransformFactory.Price:
                        writer.WriteValue(transform, product.Price?.ToString() ?? "");
                        break;
                    case ProductImportExportTransformFactory.ProductType:
                        SetLookupColumn(writer, true, ProductTypesLookupName);
                        var productType = (from pt in _productTypes where pt.Id == product.ProductTypeId select pt).FirstOrDefault();
                        writer.WriteValue(transform, productType?.Name ?? "");
                        break;

                }
            }
        }

        public override List<FieldMapping> Parse(IList<ProductDTO> source, List<FieldMapping> mappings)
        {
            return mappings;
        }

        public override ImportParserResult Verify(IList<ProductDTO> source, List<FieldMapping> mappings)
        {
            return new ImportParserResult();
        }
    }
}
