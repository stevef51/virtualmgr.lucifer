﻿using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.RepositoryInterfaces.Importing;
using System.Transactions;

namespace ConceptCave.Repository.ImportMapping
{
    public class HierarchyImportExportTransformFactory
    {
        public const string HierarchyId = "Hierarchy";
        public const string BucketId = "BucketId";
        public const string BucketParentId = "ParentId";
        public const string BucketName = "Bucket Name";
        public const string BucketRole = "RoleName";
        public const string UserName = "Username";
        public const string IsPrimary = "IsPrimary";

        public List<FieldTransform> Create()
        {
            List<FieldTransform> result = new List<FieldTransform>();

            FieldTransform tran = new FieldTransform(HierarchyId, "1", 1);
            result.Add(tran);

            tran = new FieldTransform(BucketId, "1", 2);
            result.Add(tran);

            tran = new FieldTransform(BucketParentId, "1", 3);
            result.Add(tran);

            tran = new FieldTransform(BucketName, "1", 4);
            result.Add(tran);

            tran = new FieldTransform(BucketRole, "1", 5);
            result.Add(tran);

            tran = new FieldTransform(UserName, "1", 6);
            result.Add(tran);

            tran = new FieldTransform(IsPrimary, "1", 7);
            result.Add(tran);

            return result;
        }
    }

    public class HierarchyImportWriter : IImportWriter
    {
        protected class HierarchyImportHelper
        {
            public HierarchyBucketDTO Bucket { get; set; }
            public int RecordId { get; set; }
            public int? ParentId { get; set; }
        }

        protected IHierarchyRepository _hierarchyRepo;
        protected IMembershipRepository _memberRepo;
        private IList<HierarchyRoleDTO> _newRoles;
        private IDictionary<string, HierarchyRoleDTO> _roles;

        public HierarchyImportWriter(IHierarchyRepository hierarchyRepo, IMembershipRepository memberRepo)
        {
            _hierarchyRepo = hierarchyRepo;
            _memberRepo = memberRepo;
            BucketCache = new Dictionary<int, HierarchyImportHelper>();
            Buckets = new List<HierarchyImportHelper>();
            _newRoles = new List<HierarchyRoleDTO>();
        }

        protected IDictionary<int, HierarchyImportHelper> BucketCache { get; set; }
        protected IList<HierarchyImportHelper> Buckets { get; set; }
        protected HierarchyImportHelper Current { get; set; }

        public bool RequiresUniqueKey
        {
            get
            {
                return false;
            }
        }

        public void Complete()
        {
            using (TransactionScope scope = new TransactionScope())
            {
                HierarchyImportHelper root = null;
                foreach (var bucket in Buckets)
                {
                    if (bucket.ParentId.HasValue)
                    {
                        var parent = BucketCache[bucket.ParentId.Value];

                        parent.Bucket.Children.Add(bucket.Bucket);
                    }
                    else
                    {
                        root = bucket;
                    }
                }

                foreach (var r in _newRoles)
                {
                    var role = _hierarchyRepo.Save(r, true, false);

                    foreach (var bucket in Buckets)
                    {
                        if (bucket.Bucket.HierarchyRole == r)
                        {
                            bucket.Bucket.HierarchyRole = null;
                            bucket.Bucket.RoleId = role.Id;
                        }
                    }
                }

                _hierarchyRepo.SetTreeNumbering(root.Bucket);
                _hierarchyRepo.DumpAndSave(root.Bucket);

                scope.Complete();
            }

            BucketCache = new Dictionary<int, HierarchyImportHelper>();
            Buckets = new List<HierarchyImportHelper>();
            _newRoles = new List<HierarchyRoleDTO>();
            Current = null;
        }

        public FieldTransform GetUniqueKeyTransform(List<FieldTransform> transforms)
        {
            var transform = (from t in transforms where t.Destination.Name.Equals(HierarchyImportExportTransformFactory.BucketId) select t).DefaultIfEmpty(null).First();

            return transform;
        }

        protected bool ParseBool(string val)
        {
            // we accept both true/false and y/n
            string v = val.ToLower();

            if (v.Length == 1)
            {
                return v == "y";
            }
            else
            {
                return bool.Parse(v);
            }
        }

        protected HierarchyRoleDTO GetRole(string name)
        {
            if (_roles == null)
            {
                _roles = new Dictionary<string, HierarchyRoleDTO>();
                var rs = _hierarchyRepo.GetRoles();

                foreach (var r in rs)
                {
                    _roles.Add(r.Name, r);
                }
            }

            HierarchyRoleDTO result = null;
            if (_roles.TryGetValue(name, out result) == false)
            {
                return null;
            }

            return result;
        }

        protected HierarchyRoleDTO NewRole(string name)
        {
            var result = new HierarchyRoleDTO() { __IsNew = true, Name = name };

            _newRoles.Add(result);

            return result;
        }

        protected UserDataDTO GetUser(string username)
        {
            var u = _memberRepo.GetByUsername(username, RepositoryInterfaces.Enums.MembershipLoadInstructions.AspNetMembership);

            return u;
        }

        public void LoadEntity(string uniqueId)
        {
            int id = -1;
            if (int.TryParse(uniqueId, out id) == false)
            {
                throw new ArgumentException(string.Format("{0} is not a valid integer", uniqueId));
            }

            Current = new HierarchyImportHelper()
            {
                RecordId = int.Parse(uniqueId)
            };

            Current.Bucket = _hierarchyRepo.GetById(id, RepositoryInterfaces.Enums.HierarchyBucketLoadInstructions.None);

            if (Current.Bucket == null)
            {
                Current.Bucket = new HierarchyBucketDTO()
                {
                    __IsNew = true
                };
            }
        }

        public void SaveEntity(string uniqueId)
        {
            Buckets.Add(Current);
            BucketCache.Add(Current.RecordId, Current);
        }

        public void WriteValue(FieldTransform transform, string value)
        {
            string field = transform.Destination.Name;

            switch (field)
            {
                case HierarchyImportExportTransformFactory.BucketId:
                    int v = int.Parse(value);

                    // -ve ids used for new buckets so that the hierarchy can be defined for these
                    if (v > 0)
                    {
                        Current.RecordId = int.Parse(value);
                    }

                    break;
                case HierarchyImportExportTransformFactory.BucketName:
                    Current.Bucket.Name = value;
                    break;
                case HierarchyImportExportTransformFactory.BucketParentId:
                    if (string.IsNullOrEmpty(value))
                    {
                        break;
                    }

                    Current.ParentId = int.Parse(value);
                    break;
                case HierarchyImportExportTransformFactory.BucketRole:
                    var r = GetRole(value);

                    if (r == null)
                    {
                        r = NewRole(value);
                    }

                    Current.Bucket.HierarchyRole = r;

                    break;
                case HierarchyImportExportTransformFactory.HierarchyId:
                    Current.Bucket.HierarchyId = int.Parse(value);
                    break;
                case HierarchyImportExportTransformFactory.IsPrimary:
                    Current.Bucket.IsPrimary = ParseBool(value);
                    break;
                case HierarchyImportExportTransformFactory.UserName:
                    if (string.IsNullOrEmpty(value))
                    {
                        break;
                    }

                    Current.Bucket.UserId = GetUser(value).UserId;
                    break;
            }
        }
    }

    public class HierarchyExportParser : ExportParser<IList<HierarchyBucketDTO>>
    {
        private IHierarchyRepository _hierarchyRepository;
        private IMembershipRepository _memberRepo;
        private IDictionary<int, HierarchyRoleDTO> _roles;

        public HierarchyExportParser(IHierarchyRepository hierachyRepository, IMembershipRepository memberRepo)
        {
            _hierarchyRepository = hierachyRepository;
            _memberRepo = memberRepo;
        }

        public override IImportWriter GetWriter(IList<HierarchyBucketDTO> source)
        {
            return null;
        }

        protected HierarchyRoleDTO GetRole(int id)
        {
            if (_roles == null)
            {
                _roles = new Dictionary<int, HierarchyRoleDTO>();
                var rs = _hierarchyRepository.GetRoles();

                foreach (var r in rs)
                {
                    _roles.Add(r.Id, r);
                }
            }

            HierarchyRoleDTO result = null;
            if (_roles.TryGetValue(id, out result) == false)
            {
                return null;
            }

            return result;
        }

        protected UserDataDTO GetUser(Guid userid)
        {
            var u = _memberRepo.GetById(userid, RepositoryInterfaces.Enums.MembershipLoadInstructions.AspNetMembership);

            return u;
        }

        public override ImportParserResult Import(IList<HierarchyBucketDTO> source, List<FieldTransform> transforms, IImportWriter writer, bool isTestOnly, Action<int> progressCallback)
        {
            ImportParserResult result = new ImportParserResult();

            if (isTestOnly == true)
            {
                return result;
            }

            WriteTitles(transforms, writer);

            foreach (var h in source)
            {
                WriteBucket(h, writer, transforms);
            }

            return result;
        }

        public void WriteBucket(HierarchyBucketDTO h, IImportWriter writer, List<FieldTransform> transforms)
        {
            writer.LoadEntity(h.Id.ToString());

            foreach (var transform in transforms)
            {
                string field = transform.Destination.Name;

                switch (field)
                {
                    case HierarchyImportExportTransformFactory.HierarchyId:
                        writer.WriteValue(transform, h.HierarchyId.ToString());
                        break;
                    case HierarchyImportExportTransformFactory.BucketId:
                        writer.WriteValue(transform, h.Id.ToString());
                        break;
                    case HierarchyImportExportTransformFactory.BucketParentId:
                        writer.WriteValue(transform, h.ParentId.HasValue ? h.ParentId.Value.ToString() : "");
                        break;
                    case HierarchyImportExportTransformFactory.BucketName:
                        writer.WriteValue(transform, h.Name);
                        break;
                    case HierarchyImportExportTransformFactory.BucketRole:
                        var role = GetRole(h.RoleId);

                        writer.WriteValue(transform, role.Name);
                        break;
                    case HierarchyImportExportTransformFactory.UserName:
                        writer.WriteValue(transform, h.UserId.HasValue ? GetUser(h.UserId.Value).AspNetUser.UserName : "");
                        break;
                    case HierarchyImportExportTransformFactory.IsPrimary:
                        writer.WriteValue(transform, h.IsPrimary.HasValue ? h.IsPrimary.Value.ToString() : "");
                        break;

                }
            }
        }

        public override List<FieldMapping> Parse(IList<HierarchyBucketDTO> source, List<FieldMapping> mappings)
        {
            return mappings;
        }

        public override ImportParserResult Verify(IList<HierarchyBucketDTO> source, List<FieldMapping> mappings)
        {
            return new ImportParserResult();
        }
    }
}
