﻿using ConceptCave.Checklist;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
//using ConceptCave.Scheduling;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using ConceptCave.RepositoryInterfaces.Importing;

namespace ConceptCave.Repository.ImportMapping
{
    /// <summary>
    /// Factory class for centralising the creation of Field Transforms for the import and export
    /// of schedules.
    /// </summary>
    public class ScheduleImportExportTransformFactory
    {
        public const string ScheduleId = "ScheduleId";
        public const string Roster = "Roster";
        public const string Site = "Site";
        public const string Sunday = "Sunday";
        public const string Monday = "Monday";
        public const string Tuesday = "Tuesday";
        public const string Wednesday = "Wednesday";
        public const string Thursday = "Thursday";
        public const string Friday = "Friday";
        public const string Saturday = "Saturday";
        public const string Period = "Period";
        public const string LengthInDays = "Duration";
        public const string Role = "Role";
        public const string TaskType = "TaskType";
        public const string Name = "Name";
        public const string Description = "Description";
        public const string StartTime = "StartTime";
        public const string StartWindow = "StartWindow";
        public const string ExpectedDuration = "ExpectedDuration";
        public const string SortOrder = "SortOrder";
        public const string Level = "Level";
        public const string Labels = "Labels";
        public const string Area = "Area";
        public const string Activity = "Activity";
        public const string Feature = "Feature";
        public const string Book = "Book";
        public const string Archived = "Archived";

        public const string WorkLoadingSunday = "WLoading Sunday";
        public const string WorkLoadingMonday = "WLoading Monday";
        public const string WorkLoadingTuesday = "WLoading Tuesday";
        public const string WorkLoadingWednesday = "WLoading Wednesday";
        public const string WorkLoadingThursday = "WLoading Thursday";
        public const string WorkLoadingFriday = "WLoading Friday";
        public const string WorkLoadingSaturday = "WLoading Saturday";
        public const string WorkLoadingPeriod = "WLoading Period";


        public List<FieldTransform> Create()
        {
            List<FieldTransform> result = new List<FieldTransform>();

            FieldTransform tran = new FieldTransform(ScheduleId, "1", 1);
            result.Add(tran);

            tran = new FieldTransform(Roster, "1", 2);
            result.Add(tran);

            tran = new FieldTransform(Site, "1", 3);
            result.Add(tran);

            tran = new FieldTransform(Sunday, "1", 4);
            result.Add(tran);
            tran = new FieldTransform(Monday, "1", 5);
            result.Add(tran);
            tran = new FieldTransform(Tuesday, "1", 6);
            result.Add(tran);
            tran = new FieldTransform(Wednesday, "1", 7);
            result.Add(tran);
            tran = new FieldTransform(Thursday, "1", 8);
            result.Add(tran);
            tran = new FieldTransform(Friday, "1", 9);
            result.Add(tran);
            tran = new FieldTransform(Saturday, "1", 10);
            result.Add(tran);
            tran = new FieldTransform(Period, "1", 11);
            result.Add(tran);
            tran = new FieldTransform(LengthInDays, "1", 12);
            result.Add(tran);

            tran = new FieldTransform(Role, "1", 13);
            result.Add(tran);

            tran = new FieldTransform(TaskType, "1", 14);
            result.Add(tran);

            tran = new FieldTransform(Name, "1", 15);
            result.Add(tran);

            tran = new FieldTransform(Description, "1", 16);
            result.Add(tran);

            tran = new FieldTransform(StartTime, "1", 17);
            result.Add(tran);

            tran = new FieldTransform(StartWindow, "1", 18);
            result.Add(tran);

            tran = new FieldTransform(ExpectedDuration, "1", 19);
            result.Add(tran);

            tran = new FieldTransform(SortOrder, "1", 20);
            result.Add(tran);

            tran = new FieldTransform(Level, "1", 21);
            result.Add(tran);

            tran = new FieldTransform(Labels, "1", 22);
            result.Add(tran);

            tran = new FieldTransform(Area, "1", 23);
            result.Add(tran);
            tran = new FieldTransform(Activity, "1", 24);
            result.Add(tran);
            tran = new FieldTransform(Feature, "1", 25);
            result.Add(tran);
            tran = new FieldTransform(Book, "1", 26);
            result.Add(tran);

            tran = new FieldTransform(WorkLoadingSunday, "1", 27);
            result.Add(tran);
            tran = new FieldTransform(WorkLoadingMonday, "1", 28);
            result.Add(tran);
            tran = new FieldTransform(WorkLoadingTuesday, "1", 29);
            result.Add(tran);
            tran = new FieldTransform(WorkLoadingWednesday, "1", 30);
            result.Add(tran);
            tran = new FieldTransform(WorkLoadingThursday, "1", 31);
            result.Add(tran);
            tran = new FieldTransform(WorkLoadingFriday, "1", 32);
            result.Add(tran);
            tran = new FieldTransform(WorkLoadingSaturday, "1", 33);
            result.Add(tran);
            tran = new FieldTransform(WorkLoadingPeriod, "1", 34);
            result.Add(tran);

            tran = new FieldTransform(Archived, "1", 35);
            result.Add(tran);

            return result;
        }
    }
    /// <summary>
    /// Used during a schedule import to write schedule data to the database
    /// </summary>
    public class ScheduleImportWriter : IImportWriter
    {
        public class ScheduleWorkloadingHelper
        {
            public string SiteName { get; set; }
            public string Area { get; set; }
            public string Feature { get; set; }
            public string Activity { get; set; }
            public string Book { get; set; }

            public bool Completed { get; set; }

            public bool? ExecuteSunday { get; set; }
            public bool? ExecuteMonday { get; set; }
            public bool? ExecuteTuesday { get; set; }
            public bool? ExecuteWednesday { get; set; }
            public bool? ExecuteThursday { get; set; }
            public bool? ExecuteFriday { get; set; }
            public bool? ExecuteSaturday { get; set; }
            public int Period { get; set; }
            public int Duration { get; set; }

            public bool SeenSunday { get; set; }
            public bool SeenMonday { get; set; }
            public bool SeenTuesday { get; set; }
            public bool SeenWednesday { get; set; }
            public bool SeenThursday { get; set; }
            public bool SeenFriday { get; set; }
            public bool SeenSaturday { get; set; }
            public bool SeenPeriod { get; set; }
        }

        protected IScheduledTaskRepository _scheduleRepo;
        protected IRosterRepository _rosterRepo;
        protected IMembershipRepository _memberRepo;
        protected IHierarchyRepository _hierarchyRepo;
        protected ITaskTypeRepository _taskTypeRepo;
        protected IWorkLoadingFeatureRepository _featureRepo;
        protected ISiteFeatureRepository _siteFeatureRepo;
        protected IWorkLoadingActivityRepository _activityRepo;
        protected IWorkLoadingBookRepository _bookRepo;
        protected ILabelRepository _labelRepo;

        public ScheduleImportWriter(IScheduledTaskRepository scheduleRepo,
            IRosterRepository rosterRepo,
            IMembershipRepository memberRepo,
            IHierarchyRepository hierarchyRepo,
            ITaskTypeRepository taskTypeRepo,
            IWorkLoadingFeatureRepository featureRepo,
            ISiteFeatureRepository siteFeatureRepo,
            IWorkLoadingActivityRepository activityRepo,
            IWorkLoadingBookRepository bookRepo,
            ILabelRepository labelRepo)
            : base()
        {
            _scheduleRepo = scheduleRepo;
            _rosterRepo = rosterRepo;
            _memberRepo = memberRepo;
            _hierarchyRepo = hierarchyRepo;
            _taskTypeRepo = taskTypeRepo;
            _featureRepo = featureRepo;
            _siteFeatureRepo = siteFeatureRepo;
            _activityRepo = activityRepo;
            _bookRepo = bookRepo;
            _labelRepo = labelRepo;

            _siteCache = new Dictionary<string, UserDataDTO>();
            _roleCache = new Dictionary<string, HierarchyRoleDTO>();
            _labelCache = new Dictionary<string, LabelDTO>();

            _labelsToRemove = new List<Guid>();
        }

        // start of IImportWriter

        public bool RequiresUniqueKey
        {
            get
            {
                return false;
            }
        }

        protected ProjectJobScheduledTaskDTO Current { get; set; }
        protected ScheduleWorkloadingHelper CurrentWorkloadingHelper { get; set; }

        protected Dictionary<string, ProjectJobScheduledTaskDTO> _scheduleCache { get; set; }
        protected List<Guid> _labelsToRemove { get; set; }

        protected Dictionary<string, UserDataDTO> _siteCache { get; set; }
        protected Dictionary<string, HierarchyRoleDTO> _roleCache { get; set; }
        protected Dictionary<string, LabelDTO> _labelCache { get; set; }

        protected IList<WorkLoadingActivityDTO> _activities { get; set; }
        protected IList<WorkLoadingBookDTO> _books { get; set; }

        protected Dictionary<string, WorkLoadingFeatureDTO> _featureCache { get; set; }

        protected IList<RosterDTO> _rosters;
        protected IList<RosterDTO> Rosters
        {
            get
            {
                if (_rosters == null)
                {
                    _rosters = _rosterRepo.GetAll();
                }

                return _rosters;
            }
        }

        protected IList<ProjectJobTaskTypeDTO> _taskTypes;
        protected IList<ProjectJobTaskTypeDTO> TaskTypes
        {
            get
            {
                if (_taskTypes == null)
                {
                    _taskTypes = _taskTypeRepo.GetAll(RepositoryInterfaces.Enums.ProjectJobTaskTypeLoadInstructions.None);
                }

                return _taskTypes;
            }
        }

        protected IList<WorkLoadingActivityDTO> Activities
        {
            get
            {
                if (_activities == null)
                {
                    _activities = _activityRepo.GetAllActivities(RepositoryInterfaces.Enums.WorkLoadingActivityLoadInstructions.None);
                }

                return _activities;
            }
        }

        protected IList<WorkLoadingBookDTO> Books
        {
            get
            {
                if (_books == null)
                {
                    _books = _bookRepo.GetAllBooks(RepositoryInterfaces.Enums.WorkLoadingBookLoadInstructions.None);
                }

                return _books;
            }
        }

        protected Dictionary<string, ProjectJobScheduledTaskDTO> ScheduleCache
        {
            get
            {
                if (_scheduleCache == null)
                {
                    _scheduleCache = new Dictionary<string, ProjectJobScheduledTaskDTO>();
                }

                return _scheduleCache;
            }
        }

        protected UserDataDTO GetSite(string name)
        {
            UserDataDTO result = null;

            if (_siteCache.TryGetValue(name, out result) == false)
            {
                result = _memberRepo.GetByName(name, RepositoryInterfaces.Enums.MembershipLoadInstructions.Features | RepositoryInterfaces.Enums.MembershipLoadInstructions.Areas);

                _siteCache.Add(name, result);
            }

            return result;
        }

        protected SiteFeatureDTO GetSiteFeature(string siteName, string area, string feature)
        {
            var site = GetSite(siteName);

            if (site == null)
            {
                throw new ArgumentException("{0} site does not exist", siteName);
            }

            var fs = (from f in site.SiteFeatures where 
                          f.Name.Equals(feature, StringComparison.InvariantCultureIgnoreCase) &&
                      f.SiteArea.Name.Equals(area, StringComparison.InvariantCultureIgnoreCase) select f);

            if (fs.Count() == 0)
            {
                return null;
            }

            return fs.First();
        }

        protected HierarchyRoleDTO GetRole(string name)
        {
            HierarchyRoleDTO result = null;

            if (_roleCache.TryGetValue(name, out result) == false)
            {
                var roles = _hierarchyRepo.GetRolesByName(name);

                if (roles.Count() > 0)
                {
                    result = roles.First();
                }

                _roleCache.Add(name, result);
            }

            return result;
        }

        protected IEnumerable<LabelDTO> GetLabels(string value)
        {
            LabelDTO result = null;
            var names = value.Split(',');
            foreach (var name in names)
            {
                if (_labelCache.TryGetValue(name.ToLower(), out result) == false)
                {
                    result = _labelRepo.GetByName(name);

                    _labelCache.Add(name.ToLower(), result);
                }

                yield return result;
            }
        }

        protected bool ParseBool(string val)
        {
            // we accept both true/false and y/n
            string v = val.ToLower();

            if (v.Length == 1)
            {
                return v == "y";
            }
            else
            {
                return bool.Parse(v);
            }
        }

        protected void AttachWorkloading()
        {
            if (CurrentWorkloadingHelper.Completed == true)
            {
                return;
            }

            if (Current.SiteId.HasValue == false)
            {
                return;
            }

            if (string.IsNullOrEmpty(CurrentWorkloadingHelper.Activity) == true ||
                string.IsNullOrEmpty(CurrentWorkloadingHelper.Area) == true ||
                string.IsNullOrEmpty(CurrentWorkloadingHelper.Book) == true ||
                string.IsNullOrEmpty(CurrentWorkloadingHelper.Feature) == true ||
                CurrentWorkloadingHelper.SeenSunday == false ||
                CurrentWorkloadingHelper.SeenMonday == false ||
                CurrentWorkloadingHelper.SeenTuesday == false ||
                CurrentWorkloadingHelper.SeenWednesday == false ||
                CurrentWorkloadingHelper.SeenThursday == false ||
                CurrentWorkloadingHelper.SeenFriday == false ||
                CurrentWorkloadingHelper.SeenSaturday == false ||
                CurrentWorkloadingHelper.SeenPeriod == false)
            {
                return; // we don't have enough info
            }

            var parts = CurrentWorkloadingHelper.Activity.Split(',');

            foreach (string p in parts)
            {
                var part = p.Trim(); // spaces
                part = part.Trim('.');
                // we do have enough info
                ProjectJobScheduledTaskWorkloadingActivityDTO activity = new ProjectJobScheduledTaskWorkloadingActivityDTO()
                {
                    __IsNew = true,
                    ProjectJobScheduledTaskId = Current.Id,
                    Frequency = 1
                };

                var sf = GetSiteFeature(CurrentWorkloadingHelper.SiteName, CurrentWorkloadingHelper.Area, CurrentWorkloadingHelper.Feature);

                if (sf == null)
                {
                    throw new ArgumentException(string.Format("Site {0} does not have an area called {1}", CurrentWorkloadingHelper.SiteName, CurrentWorkloadingHelper.Area));
                }

                activity.SiteFeatureId = sf.Id;

                var act = (from a in Activities where a.Name.Equals(part, StringComparison.InvariantCultureIgnoreCase) select a);

                if (act.Count() == 0)
                {
                    throw new ArgumentException(string.Format("Activity {0} does not exist", part));
                }

                activity.Text = act.First().Name;

                var book = (from b in Books where b.Name.Equals(CurrentWorkloadingHelper.Book, StringComparison.InvariantCultureIgnoreCase) select b);

                if (book.Count() == 0)
                {
                    throw new ArgumentException(string.Format("Book {0} does not exist", CurrentWorkloadingHelper.Book));
                }

                var standard = _bookRepo.GetStandard(book.First().Id, sf.WorkLoadingFeatureId, act.First().Id, RepositoryInterfaces.Enums.WorkLoadingStandardLoadInstructions.None);

                if (standard == null)
                {
                    throw new ArgumentException(string.Format("There is no standard for book {0} for feature {1} with activity {2} and units of M", CurrentWorkloadingHelper.Book, CurrentWorkloadingHelper.Feature, part));
                }

                activity.WorkloadingStandardId = standard.Id;
                activity.ExecuteSunday = CurrentWorkloadingHelper.ExecuteSunday;
                activity.ExecuteMonday = CurrentWorkloadingHelper.ExecuteMonday;
                activity.ExecuteTuesday = CurrentWorkloadingHelper.ExecuteTuesday;
                activity.ExecuteWednesday = CurrentWorkloadingHelper.ExecuteWednesday;
                activity.ExecuteThursday = CurrentWorkloadingHelper.ExecuteThursday;
                activity.ExecuteFriday = CurrentWorkloadingHelper.ExecuteFriday;
                activity.ExecuteSaturday = CurrentWorkloadingHelper.ExecuteSaturday;
                activity.Frequency = CurrentWorkloadingHelper.Period;

                Current.ProjectJobScheduledTaskWorkloadingActivities.Add(activity);
            }

            CurrentWorkloadingHelper.Completed = true;
        }

        public FieldTransform GetUniqueKeyTransform(List<FieldTransform> transforms)
        {
            var transform = (from t in transforms where t.Destination.Name.Equals(ScheduleImportExportTransformFactory.ScheduleId) select t).DefaultIfEmpty(null).First();

            return transform;
        }

        protected ProjectJobScheduledTaskDTO CreateNewSchedule()
        {
            return new ProjectJobScheduledTaskDTO()
            {
                __IsNew = true,
                Id = CombFactory.NewComb(),
                Level = 0,
                RequireSignature = false,
                PhotoDuringSignature = false,
                SortOrder = 0,
                IsArchived = false,
                LengthInDays = "1",
                CalendarRule = new CalendarRuleDTO()
                {
                    __IsNew = true,
                    StartDate = DateTime.UtcNow.AddDays(-1),
                    DateCreated = DateTime.UtcNow,
                    Frequency = ConceptCave.RepositoryInterfaces.Enums.CalendarRuleFrequency.Daily.ToString()
                }
            };
        }

        public void LoadEntity(string uniqueId)
        {
            if (string.IsNullOrEmpty(uniqueId))
            {
                // we don't support updates at the moment
                Current = CreateNewSchedule();

                ScheduleCache.Add(Current.Id.ToString(), Current);
            }
            else
            {
                if (ScheduleCache.ContainsKey(uniqueId) == false)
                {
                    // we don't have it in our cache, some more work to do
                    Guid id = Guid.Empty;
                    if (Guid.TryParse(uniqueId, out id) == false)
                    {
                        // we have something that isn't an id, so we treat this as a new
                        // task that's likely got multiple areas to be cleaned.
                        Current = CreateNewSchedule();

                        ScheduleCache.Add(uniqueId, Current);
                    }
                    else
                    {
                        // see if we can get it from the db
                        Current = _scheduleRepo.GetById(id, RepositoryInterfaces.Enums.ProjectJobScheduledTaskLoadInstructions.CalendarRule | ProjectJobScheduledTaskLoadInstructions.AllTranslated);

                        if (Current == null)
                        {
                            throw new ArgumentException(string.Format("No schedule exists for {0}", uniqueId));
                        }

                        ScheduleCache.Add(uniqueId, Current);

                        // we are going to remove all labels from schedules and re-add
                        _labelsToRemove.Add(Current.Id);
                    }
                }
                else
                {
                    // its in the cache, easy
                    Current = ScheduleCache[uniqueId];
                }
            }

            CurrentWorkloadingHelper = new ScheduleWorkloadingHelper();
        }

        public void SaveEntity(string uniqueId)
        {
            if ((string.IsNullOrEmpty(CurrentWorkloadingHelper.Activity) == false ||
                string.IsNullOrEmpty(CurrentWorkloadingHelper.Area) == false ||
                string.IsNullOrEmpty(CurrentWorkloadingHelper.Book) == false ||
                string.IsNullOrEmpty(CurrentWorkloadingHelper.Feature) == false ||
                CurrentWorkloadingHelper.SeenSunday == false ||
                CurrentWorkloadingHelper.SeenMonday == false ||
                CurrentWorkloadingHelper.SeenTuesday == false ||
                CurrentWorkloadingHelper.SeenWednesday == false ||
                CurrentWorkloadingHelper.SeenThursday == false ||
                CurrentWorkloadingHelper.SeenFriday == false ||
                CurrentWorkloadingHelper.SeenSaturday == false ||
                CurrentWorkloadingHelper.SeenPeriod == false) &&
                CurrentWorkloadingHelper.Completed == false)
            {
                throw new ArgumentException("Workloading information is only partially defined");
            }

            // since we support a scheduled task being defined across multiple lines
            // so that multiple work loading items can be created throug the import.
            // We don't actually save here as this is called for each row.
            // we instead do the save for everything in our complete method.

            Current = null;
        }

        public void WriteValue(FieldTransform transform, string value)
        {
            string field = transform.Destination.Name;

            switch (field)
            {
                case ScheduleImportExportTransformFactory.Roster:
                    var roster = (from r in Rosters where r.Name.Equals(value, StringComparison.InvariantCultureIgnoreCase) select r.Id);

                    if (roster.Count() == 0)
                    {
                        throw new ArgumentException(string.Format("No roster called {0} exists", value));
                    }

                    Current.RosterId = roster.First();
                    break;
                case ScheduleImportExportTransformFactory.Role:
                    var role = GetRole(value);

                    if (role == null)
                    {
                        throw new ArgumentException(string.Format("No role called {0} exists", value));
                    }

                    Current.RoleId = role.Id;

                    break;
                case ScheduleImportExportTransformFactory.Site:
                    if (string.IsNullOrEmpty(value) == true)
                    {
                        Current.SiteId = null;
                        CurrentWorkloadingHelper.SiteName = null;
                        break;
                    }

                    var site = GetSite(value);

                    if (site == null)
                    {
                        throw new ArgumentException(string.Format("No site called {0} exists", value));
                    }

                    Current.SiteId = site.UserId;
                    CurrentWorkloadingHelper.SiteName = value;

                    AttachWorkloading();

                    break;
                case ScheduleImportExportTransformFactory.TaskType:
                    var tt = (from t in TaskTypes where t.Name == value select t.Id);

                    if (tt.Count() == 0)
                    {
                        throw new ArgumentException(string.Format("No task type called {0} exists", value));
                    }
                    Current.TaskTypeId = tt.First();
                    break;
                case ScheduleImportExportTransformFactory.Name:
                    if (Current.Translated.Count > 0)
                    {
                        Current.Translated.First().Name = value;
                    }
                    Current.Name = value;
                    break;
                case ScheduleImportExportTransformFactory.Description:
                    if (Current.Translated.Count > 0)
                    {
                        Current.Translated.First().Description = value;
                    }
                    Current.Description = value;
                    break;
                case ScheduleImportExportTransformFactory.Sunday:
                    Current.CalendarRule.ExecuteSunday = ParseBool(value);
                    break;
                case ScheduleImportExportTransformFactory.Monday:
                    Current.CalendarRule.ExecuteMonday = ParseBool(value);
                    break;
                case ScheduleImportExportTransformFactory.Tuesday:
                    Current.CalendarRule.ExecuteTuesday = ParseBool(value);
                    break;
                case ScheduleImportExportTransformFactory.Wednesday:
                    Current.CalendarRule.ExecuteWednesday = ParseBool(value);
                    break;
                case ScheduleImportExportTransformFactory.Thursday:
                    Current.CalendarRule.ExecuteThursday = ParseBool(value);
                    break;
                case ScheduleImportExportTransformFactory.Friday:
                    Current.CalendarRule.ExecuteFriday = ParseBool(value);
                    break;
                case ScheduleImportExportTransformFactory.Saturday:
                    Current.CalendarRule.ExecuteSaturday = ParseBool(value);
                    break;
                case ScheduleImportExportTransformFactory.Period:
                    Current.CalendarRule.Period = int.Parse(value);
                    break;
                case ScheduleImportExportTransformFactory.LengthInDays:
                    Current.LengthInDays = value;
                    break;
                case ScheduleImportExportTransformFactory.StartTime:
                    if (string.IsNullOrEmpty(value))
                    {
                        Current.StartTime = null;
                        break;
                    }

                    try
                    {
                        Current.StartTime = TimeSpan.Parse(value);
                    }
                    catch (Exception)
                    {
                        Current.StartTime = DateTime.Parse(value).TimeOfDay;
                    }

                    break;
                case ScheduleImportExportTransformFactory.StartWindow:
                    if (string.IsNullOrEmpty(value))
                    {
                        Current.StartTimeWindowSeconds = null;
                        break;
                    }

                    Current.StartTimeWindowSeconds = 60 * int.Parse(value); // value defined in mins
                    break;
                case ScheduleImportExportTransformFactory.ExpectedDuration:
                    if (string.IsNullOrEmpty(value))
                    {
                        Current.EstimatedDurationSeconds = null;
                        break;
                    }

                    Current.EstimatedDurationSeconds = 60 * int.Parse(value); // value defined in mins
                    break;
                case ScheduleImportExportTransformFactory.SortOrder:
                    if (string.IsNullOrEmpty(value))
                    {
                        Current.SortOrder = 0;
                        break;
                    }
                    Current.SortOrder = int.Parse(value);
                    break;
                case ScheduleImportExportTransformFactory.Level:
                    if (string.IsNullOrEmpty(value))
                    {
                        Current.Level = 0;
                        break;
                    }
                    Current.Level = int.Parse(value);
                    break;
                case ScheduleImportExportTransformFactory.Labels:
                    if (string.IsNullOrEmpty(value) == true)
                    {
                        break;
                    }

                    foreach (var label in GetLabels(value))
                    {
                        if (!(from s in Current.ProjectJobScheduledTaskLabels where s.LabelId == label.Id select s).Any())
                        {
                            Current.ProjectJobScheduledTaskLabels.Add(new ProjectJobScheduledTaskLabelDTO() { LabelId = label.Id, __IsNew = true });
                        }
                    }
                    break;
                case ScheduleImportExportTransformFactory.Book:
                    CurrentWorkloadingHelper.Book = value;
                    AttachWorkloading();
                    break;
                case ScheduleImportExportTransformFactory.Feature:
                    CurrentWorkloadingHelper.Feature = value;
                    AttachWorkloading();
                    break;
                case ScheduleImportExportTransformFactory.Activity:
                    CurrentWorkloadingHelper.Activity = value;
                    AttachWorkloading();
                    break;
                case ScheduleImportExportTransformFactory.Area:
                    CurrentWorkloadingHelper.Area = value;
                    AttachWorkloading();
                    break;
                case ScheduleImportExportTransformFactory.WorkLoadingSunday:
                    CurrentWorkloadingHelper.SeenSunday = true;
                    if (string.IsNullOrEmpty(value))
                    {
                        CurrentWorkloadingHelper.ExecuteSunday = null;
                        AttachWorkloading();
                        break;
                    }

                    CurrentWorkloadingHelper.ExecuteSunday = ParseBool(value);
                    AttachWorkloading();
                    break;
                case ScheduleImportExportTransformFactory.WorkLoadingMonday:
                    CurrentWorkloadingHelper.SeenMonday = true;
                    if (string.IsNullOrEmpty(value))
                    {
                        CurrentWorkloadingHelper.ExecuteMonday = null;
                        AttachWorkloading();
                        break;
                    }

                    CurrentWorkloadingHelper.ExecuteMonday = ParseBool(value);
                    AttachWorkloading();
                    break;
                case ScheduleImportExportTransformFactory.WorkLoadingTuesday:
                    CurrentWorkloadingHelper.SeenTuesday = true;
                    if (string.IsNullOrEmpty(value))
                    {
                        CurrentWorkloadingHelper.ExecuteTuesday = null;
                        AttachWorkloading();
                        break;
                    }

                    CurrentWorkloadingHelper.ExecuteTuesday = ParseBool(value);
                    AttachWorkloading();
                    break;
                case ScheduleImportExportTransformFactory.WorkLoadingWednesday:
                    CurrentWorkloadingHelper.SeenWednesday = true;
                    if (string.IsNullOrEmpty(value))
                    {
                        CurrentWorkloadingHelper.ExecuteWednesday = null;
                        AttachWorkloading();
                        break;
                    }

                    CurrentWorkloadingHelper.ExecuteWednesday = ParseBool(value);
                    AttachWorkloading();
                    break;
                case ScheduleImportExportTransformFactory.WorkLoadingThursday:
                    CurrentWorkloadingHelper.SeenThursday = true;
                    if (string.IsNullOrEmpty(value))
                    {
                        CurrentWorkloadingHelper.ExecuteThursday = null;
                        AttachWorkloading();
                        break;
                    }

                    CurrentWorkloadingHelper.ExecuteThursday = ParseBool(value);
                    AttachWorkloading();
                    break;
                case ScheduleImportExportTransformFactory.WorkLoadingFriday:
                    CurrentWorkloadingHelper.SeenFriday = true;
                    if (string.IsNullOrEmpty(value))
                    {
                        CurrentWorkloadingHelper.ExecuteFriday = null;
                        AttachWorkloading();
                        break;
                    }

                    CurrentWorkloadingHelper.ExecuteFriday = ParseBool(value);
                    AttachWorkloading();
                    break;
                case ScheduleImportExportTransformFactory.WorkLoadingSaturday:
                    CurrentWorkloadingHelper.SeenSaturday = true;
                    if (string.IsNullOrEmpty(value))
                    {
                        CurrentWorkloadingHelper.ExecuteSaturday = null;
                        AttachWorkloading();
                        break;
                    }

                    CurrentWorkloadingHelper.ExecuteSaturday = ParseBool(value);
                    AttachWorkloading();
                    break;
                case ScheduleImportExportTransformFactory.WorkLoadingPeriod:
                    CurrentWorkloadingHelper.SeenPeriod = true;
                    if (string.IsNullOrEmpty(value))
                    {
                        CurrentWorkloadingHelper.Period = 1;
                        AttachWorkloading();
                        break;
                    }

                    CurrentWorkloadingHelper.Period = int.Parse(value);
                    AttachWorkloading();
                    break;
                case ScheduleImportExportTransformFactory.Archived:
                    Current.IsArchived = ParseBool(value);
                    break;
            }
        }

        public void Complete()
        {
            using (TransactionScope scope = new TransactionScope())
            {
                foreach (var label in _labelsToRemove)
                {
                    _scheduleRepo.RemoveLabels(label);
                }

                foreach (var pair in ScheduleCache)
                {
                    // we delete any workloading info for the current schedule as we only support
                    // a full import of each scheduled task
                    if (pair.Value.__IsNew == false)
                    {
                        _scheduleRepo.RemoveWorkloadingActivities(pair.Value.Id);
                    }

                    _scheduleRepo.Save(pair.Value, false, true);
                }

                scope.Complete();
            }
        }
    }

    /// <summary>
    /// Used during a schedule export to drive an IImportWriter to output the schedule information
    /// </summary>
    public class ScheduleExportParser : ExportParser<IList<ProjectJobScheduledTaskDTO>>
    {
        protected const string TaskTypesLookupName = "Task Types";
        protected const string SitesLookupName = "Sites";
        protected const string AreasLookupName = "Areas";
        protected const string ActivitiesLookupName = "Activities";
        protected const string FeaturesLookupName = "Features";
        protected const string BooksLookupName = "Books";
        protected const string RolesLookupName = "Roles";
        protected const string LabelsLookupName = "Labels";

        public bool IncludeArchived { get; set; }

        protected Dictionary<string, string> WeekdayConditionalFormatCache { get; set; }

        protected Dictionary<string, string> LevelConditionalFormatCache { get; set; }

        protected Dictionary<string, string> HintCache { get; set; }

        protected ITaskTypeRepository _taskTypeRepo;
        protected IMembershipRepository _memberRepo;
        protected IWorkLoadingActivityRepository _activtyRepo;
        protected IWorkLoadingFeatureRepository _featureRepo;
        protected IWorkLoadingBookRepository _bookRepo;
        protected ISiteFeatureRepository _siteFeatureRepo;
        protected IRosterRepository _rosterRepo;
        protected IHierarchyRepository _hierarchyRepo;
        protected ILabelRepository _labelRepo;

        protected int RosterId { get; set; }

        public ScheduleExportParser(ITaskTypeRepository taskTypeRepo,
            IMembershipRepository memberRepo,
            IWorkLoadingActivityRepository activityRepo,
            IWorkLoadingFeatureRepository featureRepo,
            IWorkLoadingBookRepository bookRepo,
            ISiteFeatureRepository siteFeatureRepo,
            IRosterRepository rosterRepo,
            IHierarchyRepository hierarchyRepo,
            ILabelRepository labelRepo,
            int rosterId) : base()
        {
            _taskTypeRepo = taskTypeRepo;
            _memberRepo = memberRepo;
            _activtyRepo = activityRepo;
            _featureRepo = featureRepo;
            _bookRepo = bookRepo;
            _siteFeatureRepo = siteFeatureRepo;
            _rosterRepo = rosterRepo;
            _hierarchyRepo = hierarchyRepo;
            _labelRepo = labelRepo;

            RosterId = rosterId;

            IncludeArchived = false;
            IncludeTitles = true;

            WeekdayConditionalFormatCache = new Dictionary<string, string>();
            LevelConditionalFormatCache = new Dictionary<string, string>();
            HintCache = new Dictionary<string, string>();
        }

        public ProjectJobScheduledTaskLoadInstructions ScheduledTaskLoadInstructions
        {
            get
            {
                return ProjectJobScheduledTaskLoadInstructions.AllTranslated |
                    ProjectJobScheduledTaskLoadInstructions.CalendarRule |
                    ProjectJobScheduledTaskLoadInstructions.TaskType |
                    ProjectJobScheduledTaskLoadInstructions.Users |
                    ProjectJobScheduledTaskLoadInstructions.Roster |
                    ProjectJobScheduledTaskLoadInstructions.HierarchyRole |
                    ProjectJobScheduledTaskLoadInstructions.ScheduleLabels |
                    ProjectJobScheduledTaskLoadInstructions.Labels |
                    ProjectJobScheduledTaskLoadInstructions.WorkloadingActivities |
                    ProjectJobScheduledTaskLoadInstructions.WorkloadingActivitiesSiteFeatures |
                    ProjectJobScheduledTaskLoadInstructions.WorkloadingActivitiesStandards;
            }
        }

        public override List<FieldMapping> Parse(IList<ProjectJobScheduledTaskDTO> source, List<FieldMapping> mappings)
        {
            return mappings;
        }

        public override ImportParserResult Verify(IList<ProjectJobScheduledTaskDTO> source, List<FieldMapping> mappings)
        {
            return new ImportParserResult();
        }

        public override ImportParserResult Import(IList<ProjectJobScheduledTaskDTO> source, List<FieldTransform> transforms, IImportWriter writer, bool isTestOnly, Action<int> progressCallback)
        {
            ImportParserResult result = new ImportParserResult();

            if (isTestOnly == true)
            {
                return result;
            }

            if (writer is IImportExcelWriter)
            {
                IImportExcelWriter excelWriter = (IImportExcelWriter)writer;

                var types = _taskTypeRepo.GetAll(ProjectJobTaskTypeLoadInstructions.None);
                excelWriter.AddLookupSheet(TaskTypesLookupName, (from t in types select t.Name).ToList());

                var members = _memberRepo.GetAllSites(MembershipLoadInstructions.None);
                excelWriter.AddLookupSheet(SitesLookupName, (from m in members select m.Name).ToList());

                var areas = _siteFeatureRepo.GetAllAreas(SiteAreaLoadInstructions.None);
                excelWriter.AddLookupSheet(AreasLookupName, (from a in areas select a.Name).Distinct().ToList());

                var activities = _activtyRepo.GetAllActivities(WorkLoadingActivityLoadInstructions.None);
                excelWriter.AddLookupSheet(ActivitiesLookupName, (from a in activities select a.Name).Distinct().ToList());

                var features = _featureRepo.GetAll(WorkLoadingFeatureLoadInstructions.None);
                excelWriter.AddLookupSheet(FeaturesLookupName, (from a in features select a.Name).Distinct().ToList());

                var books = _bookRepo.GetAllBooks(WorkLoadingBookLoadInstructions.None);
                excelWriter.AddLookupSheet(BooksLookupName, (from a in books select a.Name).Distinct().ToList());

                var labels = _labelRepo.GetAll(LabelFor.Tasks);
                excelWriter.AddLookupSheet(LabelsLookupName, (from l in labels select l.Name).Distinct().ToList());

                var roster = _rosterRepo.GetById(RosterId);
                var buckets = _hierarchyRepo.GetBucketsForHierarchy(roster.HierarchyId, HierarchyBucketLoadInstructions.BucketRole);
                excelWriter.AddLookupSheet(RolesLookupName, (from a in buckets select a.HierarchyRole.Name).Distinct().ToList());
            }

            WriteTitles(transforms, writer);

            foreach (var schedule in source)
            {
                if (schedule.IsArchived == true && IncludeArchived == false)
                {
                    continue;
                }

                WriteSchedule(schedule, writer, transforms);
            }

            return result;
        }

        public void WriteSchedule(ProjectJobScheduledTaskDTO schedule, IImportWriter writer, List<FieldTransform> transforms)
        {
            if (schedule.ProjectJobScheduledTaskWorkloadingActivities.Count == 0)
            {
                WriteSchedule(schedule, null, writer, transforms);
                return;
            }

            foreach (var activity in schedule.ProjectJobScheduledTaskWorkloadingActivities)
            {
                WriteSchedule(schedule, activity, writer, transforms);
            }

            if (writer is IImportExcelWriter)
            {
                // we are going to highlight the end of the group
                ((IImportExcelWriter)writer).CurrentCell.WorksheetRow().FirstCell().Style.Fill.SetBackgroundColor(ClosedXML.Excel.XLColor.Blond);
            }
        }

        public void WriteSchedule(ProjectJobScheduledTaskDTO schedule, ProjectJobScheduledTaskWorkloadingActivityDTO activity, IImportWriter writer, List<FieldTransform> transforms)
        {
            writer.LoadEntity(schedule.Id.ToString());

            foreach (var transform in transforms)
            {
                string field = transform.Destination.Name;

                switch (field)
                {
                    case ScheduleImportExportTransformFactory.ScheduleId:
                        writer.WriteValue(transform, schedule.Id.ToString());
                        break;
                    case ScheduleImportExportTransformFactory.Roster:
                        writer.WriteValue(transform, schedule.Roster.Name);
                        break;
                    case ScheduleImportExportTransformFactory.Role:
                        SetLookupColumn(writer, true, RolesLookupName);
                        writer.WriteValue(transform, schedule.HierarchyRole.Name);
                        break;
                    case ScheduleImportExportTransformFactory.Site:
                        SetLookupColumn(writer, false, SitesLookupName);
                        if (schedule.SiteId.HasValue == false)
                        {
                            writer.WriteValue(transform, "");
                            break;
                        }

                        writer.WriteValue(transform, schedule.UserData_.Name);
                        break;
                    case ScheduleImportExportTransformFactory.TaskType:
                        SetLookupColumn(writer, true, TaskTypesLookupName);
                        writer.WriteValue(transform, schedule.ProjectJobTaskType.Name);
                        break;
                    case ScheduleImportExportTransformFactory.Name:
                        writer.WriteValue(transform, schedule.Name);
                        break;
                    case ScheduleImportExportTransformFactory.Description:
                        writer.WriteValue(transform, schedule.Description);
                        break;
                    case ScheduleImportExportTransformFactory.Sunday:
                        SetWeekdayConditionalFormatting(writer);
                        writer.WriteValue(transform, BoolToExportString(schedule.CalendarRule.ExecuteSunday));
                        break;
                    case ScheduleImportExportTransformFactory.Monday:
                        SetWeekdayConditionalFormatting(writer);
                        writer.WriteValue(transform, BoolToExportString(schedule.CalendarRule.ExecuteMonday));
                        break;
                    case ScheduleImportExportTransformFactory.Tuesday:
                        SetWeekdayConditionalFormatting(writer);
                        writer.WriteValue(transform, BoolToExportString(schedule.CalendarRule.ExecuteTuesday));
                        break;
                    case ScheduleImportExportTransformFactory.Wednesday:
                        SetWeekdayConditionalFormatting(writer);
                        writer.WriteValue(transform, BoolToExportString(schedule.CalendarRule.ExecuteWednesday));
                        break;
                    case ScheduleImportExportTransformFactory.Thursday:
                        SetWeekdayConditionalFormatting(writer);
                        writer.WriteValue(transform, BoolToExportString(schedule.CalendarRule.ExecuteThursday));
                        break;
                    case ScheduleImportExportTransformFactory.Friday:
                        SetWeekdayConditionalFormatting(writer);
                        writer.WriteValue(transform, BoolToExportString(schedule.CalendarRule.ExecuteFriday));
                        break;
                    case ScheduleImportExportTransformFactory.Saturday:
                        SetWeekdayConditionalFormatting(writer);
                        writer.WriteValue(transform, BoolToExportString(schedule.CalendarRule.ExecuteSaturday));
                        break;
                    case ScheduleImportExportTransformFactory.Period:
                        SetHintForColumn(writer, "Hint", "How often the schedule should occur. For example 1 means every week, 2 every other week.");
                        writer.WriteValue(transform, schedule.CalendarRule.Period.ToString());
                        break;
                    case ScheduleImportExportTransformFactory.LengthInDays:
                        SetHintForColumn(writer, "Hint", "The number of days the task is open for, if 1 the task must be completed on the day it's created, 2 it must be completed before the end of the next day etc.");
                        writer.WriteValue(transform, schedule.LengthInDays.ToString());
                        break;
                    case ScheduleImportExportTransformFactory.StartTime:
                        SetHintForColumn(writer, "Hint", "The time the task is expected to start at, in the format HH:mm:ss");
                        writer.WriteValue(transform, TimespanToExportString(schedule.StartTime));
                        break;
                    case ScheduleImportExportTransformFactory.StartWindow:
                        SetHintForColumn(writer, "Hint", "The amount of time in mins after the Start Time that the task is marked as late");
                        if (schedule.StartTimeWindowSeconds.HasValue == true)
                        {
                            writer.WriteValue(transform, (schedule.StartTimeWindowSeconds / 60).ToString()); // convert to mins
                        }
                        else
                        {
                            writer.WriteValue(transform, "");
                        }
                        break;
                    case ScheduleImportExportTransformFactory.ExpectedDuration:
                        SetHintForColumn(writer, "Hint", "The estimated amount of time, in mins, the task is expected to take");
                        if (schedule.EstimatedDurationSeconds.HasValue == true)
                        {
                            writer.WriteValue(transform, (schedule.EstimatedDurationSeconds / 60).ToString()); // convert to mins
                        }
                        else
                        {
                            writer.WriteValue(transform, "");
                        }
                        break;
                    case ScheduleImportExportTransformFactory.SortOrder:
                        writer.WriteValue(transform, schedule.SortOrder.ToString());
                        break;
                    case ScheduleImportExportTransformFactory.Level:
                        SetLevelConditionalFormatting(writer);
                        writer.WriteValue(transform, schedule.Level.ToString());
                        break;
                    case ScheduleImportExportTransformFactory.Labels:
                        //                        SetLookupColumn(writer, true, LabelsLookupName);
                        if (schedule.ProjectJobScheduledTaskLabels.Count > 0)
                        {
                            writer.WriteValue(transform, string.Join(",", from l in schedule.ProjectJobScheduledTaskLabels select l.Label.Name));
                        }
                        else
                        {
                            writer.WriteValue(transform, "");
                        }
                        break;
                    case ScheduleImportExportTransformFactory.Book:
                        SetLookupColumn(writer, true, BooksLookupName);
                        if (activity == null)
                        {
                            writer.WriteValue(transform, "");
                            break;
                        }
                        writer.WriteValue(transform, activity.WorkLoadingStandard.WorkLoadingBook.Name);
                        break;
                    case ScheduleImportExportTransformFactory.Feature:
                        SetLookupColumn(writer, true, FeaturesLookupName);
                        if (activity == null)
                        {
                            writer.WriteValue(transform, "");
                            break;
                        }
                        writer.WriteValue(transform, activity.WorkLoadingStandard.WorkLoadingFeature.Name);
                        break;
                    case ScheduleImportExportTransformFactory.Activity:
                        SetLookupColumn(writer, true, ActivitiesLookupName);
                        if (activity == null)
                        {
                            writer.WriteValue(transform, "");
                            break;
                        }
                        writer.WriteValue(transform, activity.WorkLoadingStandard.WorkLoadingActivity.Name);
                        break;
                    case ScheduleImportExportTransformFactory.Area:
                        SetLookupColumn(writer, true, AreasLookupName);
                        if (activity == null)
                        {
                            writer.WriteValue(transform, "");
                            break;
                        }
                        writer.WriteValue(transform, activity.SiteFeature.SiteArea.Name);
                        break;
                    case ScheduleImportExportTransformFactory.Archived:
                        writer.WriteValue(transform, BoolToExportString(schedule.IsArchived));
                        break;
                    case ScheduleImportExportTransformFactory.WorkLoadingSunday:
                        SetHintForColumn(writer, "Hint", "Whether or not you want the activity to only occur on specifc dates. For all dates leave all Sun, Mon, Tue, Wed, Thr, Fri, Sat columns blank.");
                        SetWeekdayConditionalFormatting(writer);
                        if (activity == null || activity.ExecuteSunday.HasValue == false)
                        {
                            writer.WriteValue(transform, "");
                            break;
                        }
                        writer.WriteValue(transform, BoolToExportString(activity.ExecuteSunday.Value));

                        break;
                    case ScheduleImportExportTransformFactory.WorkLoadingMonday:
                        SetHintForColumn(writer, "Hint", "Whether or not you want the activity to only occur on specifc dates. For all dates leave all Sun, Mon, Tue, Wed, Thr, Fri, Sat columns blank.");
                        SetWeekdayConditionalFormatting(writer);
                        if (activity == null || activity.ExecuteMonday.HasValue == false)
                        {
                            writer.WriteValue(transform, "");
                            break;
                        }

                        writer.WriteValue(transform, BoolToExportString(activity.ExecuteMonday.Value));

                        break;
                    case ScheduleImportExportTransformFactory.WorkLoadingTuesday:
                        SetHintForColumn(writer, "Hint", "Whether or not you want the activity to only occur on specifc dates. For all dates leave all Sun, Mon, Tue, Wed, Thr, Fri, Sat columns blank.");
                        SetWeekdayConditionalFormatting(writer);

                        if (activity == null || activity.ExecuteTuesday.HasValue == false)
                        {
                            writer.WriteValue(transform, "");
                            break;
                        }

                        writer.WriteValue(transform, BoolToExportString(activity.ExecuteTuesday.Value));

                        break;
                    case ScheduleImportExportTransformFactory.WorkLoadingWednesday:
                        SetHintForColumn(writer, "Hint", "Whether or not you want the activity to only occur on specifc dates. For all dates leave all Sun, Mon, Tue, Wed, Thr, Fri, Sat columns blank.");
                        SetWeekdayConditionalFormatting(writer);
                        if (activity == null || activity.ExecuteWednesday.HasValue == false)
                        {
                            writer.WriteValue(transform, "");
                            break;
                        }
                        writer.WriteValue(transform, BoolToExportString(activity.ExecuteWednesday.Value));

                        break;
                    case ScheduleImportExportTransformFactory.WorkLoadingThursday:
                        SetHintForColumn(writer, "Hint", "Whether or not you want the activity to only occur on specifc dates. For all dates leave all Sun, Mon, Tue, Wed, Thr, Fri, Sat columns blank.");
                        SetWeekdayConditionalFormatting(writer);
                        if (activity == null || activity.ExecuteThursday.HasValue == false)
                        {
                            writer.WriteValue(transform, "");
                            break;
                        }

                        writer.WriteValue(transform, BoolToExportString(activity.ExecuteThursday.Value));

                        break;
                    case ScheduleImportExportTransformFactory.WorkLoadingFriday:
                        SetHintForColumn(writer, "Hint", "Whether or not you want the activity to only occur on specifc dates. For all dates leave all Sun, Mon, Tue, Wed, Thr, Fri, Sat columns blank.");
                        SetWeekdayConditionalFormatting(writer);
                        if (activity == null || activity.ExecuteFriday.HasValue == false)
                        {
                            writer.WriteValue(transform, "");
                            break;
                        }

                        writer.WriteValue(transform, BoolToExportString(activity.ExecuteFriday.Value));

                        break;
                    case ScheduleImportExportTransformFactory.WorkLoadingSaturday:
                        SetHintForColumn(writer, "Hint", "Whether or not you want the activity to only occur on specifc dates. For all dates leave all Sun, Mon, Tue, Wed, Thr, Fri, Sat columns blank.");
                        SetWeekdayConditionalFormatting(writer);
                        if (activity == null || activity.ExecuteSaturday.HasValue == false)
                        {
                            writer.WriteValue(transform, "");
                            break;
                        }
                        writer.WriteValue(transform, BoolToExportString(activity.ExecuteSaturday.Value));

                        break;
                    case ScheduleImportExportTransformFactory.WorkLoadingPeriod:
                        SetHintForColumn(writer, "Hint", "How often the schedule should occur. For example 1 means every week, 2 every other week.");
                        if (activity == null)
                        {
                            writer.WriteValue(transform, "");
                            break;
                        }
                        writer.WriteValue(transform, activity.Frequency.ToString());
                        break;
                }
            }

            writer.SaveEntity(schedule.Id.ToString());
        }


        protected void SetWeekdayConditionalFormatting(IImportWriter writer)
        {
            if (writer is IImportExcelWriter)
            {
                IImportExcelWriter excel = ((IImportExcelWriter)writer);

                var col = excel.CurrentCell.WorksheetColumn();
                if (WeekdayConditionalFormatCache.ContainsKey(col.ColumnLetter()) == true)
                {
                    return;
                }

                // Green for days they are working
                col.AddConditionalFormat().WhenEquals(ExportBoolsAs[0]).Fill.SetBackgroundColor(ClosedXML.Excel.XLColor.LightGreen);
                col.AddConditionalFormat().WhenEquals(ExportBoolsAs[1]).Fill.SetBackgroundColor(ClosedXML.Excel.XLColor.Silver);

                WeekdayConditionalFormatCache.Add(col.ColumnLetter(), "a");
            }
        }

        protected void SetLevelConditionalFormatting(IImportWriter writer)
        {
            if (writer is IImportExcelWriter)
            {
                IImportExcelWriter excel = ((IImportExcelWriter)writer);

                var col = excel.CurrentCell.WorksheetColumn();
                if (LevelConditionalFormatCache.ContainsKey(col.ColumnLetter()) == true)
                {
                    return;
                }

                // Green for days they are working
                col.AddConditionalFormat().WhenGreaterThan(0).Fill.SetBackgroundColor(ClosedXML.Excel.XLColor.Orange).Font.SetFontColor(ClosedXML.Excel.XLColor.White);

                LevelConditionalFormatCache.Add(col.ColumnLetter(), "a");
            }
        }

        protected void SetHintForColumn(IImportWriter writer, string title, string text)
        {
            if (writer is IImportExcelWriter)
            {
                IImportExcelWriter excel = ((IImportExcelWriter)writer);

                var col = excel.CurrentCell.WorksheetColumn();
                if (HintCache.ContainsKey(col.ColumnLetter()) == true)
                {
                    return;
                }

                col.SetDataValidation().InputTitle = title;
                col.SetDataValidation().InputMessage = text;
                col.SetDataValidation().ShowInputMessage = true;

                HintCache.Add(col.ColumnLetter(), "a");
            }
        }

        public override IImportWriter GetWriter(IList<ProjectJobScheduledTaskDTO> source)
        {
            return null;
        }
    }
}
