﻿using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.RepositoryInterfaces.Importing;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.BusinessLogic.Facilities;
using ConceptCave.BusinessLogic.LingoDataObjects;
using VirtualMgr.Central;
using VirtualMgr.MultiTenant;
using VirtualMgr.Common;

namespace ConceptCave.Repository.ImportMapping
{
    public class SimpleImportExportTransformFactory
    {
        public const string Username = "Username";
        public const string Name = "Name";
        public const string Email = "Email";
        public const string Latitude = "Latitude";
        public const string Longitude = "Longitude";
        public const string Timezone = "Timezone";
        public const string ExpiryDate = "Expiry Date";
        public const string Labels = "Labels";
        public const string Roles = "Roles";
        public const string Company = "Company";
        public const string FacilityZone = "Facility Zone";
        public const string UserType = "User Type";
        public const string TabletProfile = "Tablet Profile";

        public List<FieldTransform> Create()
        {
            List<FieldTransform> result = new List<FieldTransform>();

            FieldTransform tran = new FieldTransform(Username, "1", 1);
            result.Add(tran);

            tran = new FieldTransform(Name, "1", 2);
            result.Add(tran);

            tran = new FieldTransform(UserType, "1", 3);
            result.Add(tran);


            tran = new FieldTransform(Email, "1", 4);
            result.Add(tran);

            tran = new FieldTransform(Latitude, "1", 5);
            result.Add(tran);

            tran = new FieldTransform(Longitude, "1", 6);
            result.Add(tran);

            tran = new FieldTransform(Timezone, "1", 7);
            result.Add(tran);

            tran = new FieldTransform(ExpiryDate, "1", 8);
            result.Add(tran);

            tran = new FieldTransform(Labels, "1", 9);
            result.Add(tran);

            tran = new FieldTransform(Roles, "1", 10);
            result.Add(tran);

            tran = new FieldTransform(Company, "1", 11);
            result.Add(tran);

            tran = new FieldTransform(FacilityZone, "1", 12);
            result.Add(tran);

            tran = new FieldTransform(TabletProfile, "1", 13);
            result.Add(tran);

            return result;
        }
    }

    public class SimpleMembershipImportHelper
    {
        private int SeenCount { get; set; }

        private string _username;
        private int _userType;
        private string _name;
        private string _email;
        private decimal? _latitude;
        private decimal? _longitude;
        private string _timezone;
        private DateTime? _expirydate;
        private string _labels;
        private string _roles;
        private Guid? _company;
        private string _facilityzone;
        private int? _tabletprofile;

        public bool HasSeenAllFields
        {
            get
            {
                return SeenCount == 13;
            }
        }

        public string Username
        {
            get
            {
                return _username;
            }
            set
            {
                _username = value;
                SeenCount++;
            }
        }

        public int UserType
        {
            get
            {
                return _userType;
            }
            set
            {
                _userType = value;
                SeenCount++;
            }
        }

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
                SeenCount++;
            }
        }
        public string Email
        {
            get
            {
                return _email;
            }
            set
            {
                _email = value;
                SeenCount++;
            }
        }
        public decimal? Latitude
        {
            get
            {
                return _latitude;
            }
            set
            {
                _latitude = value;
                SeenCount++;
            }
        }
        public decimal? Longitude
        {
            get
            {
                return _longitude;
            }
            set
            {
                _longitude = value;
                SeenCount++;
            }
        }
        public string Timezone
        {
            get
            {
                return _timezone;
            }
            set
            {
                _timezone = value;
                SeenCount++;
            }
        }
        public DateTime? ExpiryDate
        {
            get
            {
                return _expirydate;
            }
            set
            {
                _expirydate = value;
                SeenCount++;
            }
        }
        public string Labels
        {
            get
            {
                return _labels;
            }
            set
            {
                _labels = value;
                SeenCount++;
            }
        }
        public string Roles
        {
            get
            {
                return _roles;
            }
            set
            {
                _roles = value;
                SeenCount++;
            }
        }
        public Guid? CompanyId
        {
            get
            {
                return _company;
            }
            set
            {
                _company = value;
                SeenCount++;
            }
        }
        public string FacilityZone
        {
            get
            {
                return _facilityzone;
            }
            set
            {
                _facilityzone = value;
                SeenCount++;
            }
        }

        public int? TabletProfile
        {
            get
            {
                return _tabletprofile;
            }
            set
            {
                _tabletprofile = value;
                SeenCount++;
            }
        }

        public SimpleMembershipImportHelper()
        {
            SeenCount = 0;
        }
    }

    public class SimpleMembershipImportWriter : IImportWriter
    {
        protected IMembershipRepository _memberRepo;
        protected ICompanyRepository _companyRepo;
        protected ILabelRepository _labelRepo;
        protected IFacilityStructureRepository _facRepo;
        protected IMembershipTypeRepository _typeRepo;
        protected ITabletProfileRepository _tabletRepo;
        protected readonly IRoleRepository _roleRepo;
        private readonly AppTenant _appTenant;

        public SimpleMembershipImportWriter(IMembershipRepository memberRepo,
            ICompanyRepository companyRepo,
            ILabelRepository labelRepo,
            IFacilityStructureRepository facRepo,
            IMembershipTypeRepository typeRepo,
            ITabletProfileRepository tabletRepo,
            IRoleRepository roleRepo,
            AppTenant appTenant)
        {
            _memberRepo = memberRepo;
            _companyRepo = companyRepo;
            _labelRepo = labelRepo;
            _facRepo = facRepo;
            _typeRepo = typeRepo;
            _tabletRepo = tabletRepo;
            _roleRepo = roleRepo;
            _appTenant = appTenant;
            CompanyCache = new Dictionary<string, CompanyDTO>();
        }
        protected SimpleMembershipImportHelper Current { get; set; }
        protected UserDataDTO CurrentUser { get; set; }

        protected IList<UserTypeDTO> UserTypes { get; set; }
        protected IList<TabletProfileDTO> TabletProfiles { get; set; }

        protected IList<TimeZoneInfo> TimeZones { get; set; }
        protected Dictionary<string, CompanyDTO> CompanyCache { get; set; }

        public bool RequiresUniqueKey
        {
            get
            {
                return false;
            }
        }

        public void Complete()
        {
            Current = null;
            CurrentUser = null;
        }

        public FieldTransform GetUniqueKeyTransform(List<FieldTransform> transforms)
        {
            var transform = (from t in transforms where t.Destination.Name.Equals(SimpleImportExportTransformFactory.Username) select t).DefaultIfEmpty(null).First();

            return transform;
        }

        public static MembershipLoadInstructions GetLoadInstructions()
        {
            return MembershipLoadInstructions.AspNetMembership |
                        MembershipLoadInstructions.MembershipRole |
                        MembershipLoadInstructions.Label |
                        MembershipLoadInstructions.MembershipLabel |
                        MembershipLoadInstructions.MembershipType |
                        MembershipLoadInstructions.ContextResources |
                        MembershipLoadInstructions.ContextData |
                        MembershipLoadInstructions.TabletProfile |
                        MembershipLoadInstructions.Company;
        }

        public void LoadEntity(string uniqueId)
        {
            CurrentUser = _memberRepo.GetByUsername((uniqueId), GetLoadInstructions());


            Current = new SimpleMembershipImportHelper() { Username = uniqueId };
        }

        protected string[] ParseCommaString(string toParse)
        {
            string[] split = toParse.Split(',');

            List<string> splits = new List<string>();
            foreach (string s in split)
            {
                splits.Add(s.Trim());
            }

            return splits.ToArray();
        }

        protected void SaveLabels(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                var ul = (from l in CurrentUser.LabelUsers select l.LabelId).ToArray();

                if (ul.Length == 0)
                {
                    return;
                }

                _memberRepo.RemoveFromLabels(ul, CurrentUser.UserId);
                return;
            }

            var ls = ParseCommaString(value);

            // select out labels that the member is currently in, but aren't in the string given
            var toDelete = (from l in CurrentUser.LabelUsers where ls.Contains(l.Label.Name) == false select l.LabelId).ToArray();

            // select out labels that the member is not in, but are in the string given
            var toAdd = (from l in ls where (from u in CurrentUser.LabelUsers select u.Label.Name).Contains(l) == false select l);

            if (toDelete.Length != 0)
            {
                _memberRepo.RemoveFromLabels(toDelete, CurrentUser.UserId);
            }

            List<Guid> newIds = new List<Guid>();
            foreach (var lbl in toAdd)
            {
                var label = _labelRepo.GetByName(lbl);

                if (label == null)
                {
                    continue;
                }

                newIds.Add(label.Id);
            }

            if (newIds.Count != 0)
            {
                _memberRepo.AddLabels(CurrentUser.UserId, newIds.ToArray());
            }
        }

        protected void SaveRoles(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                var rl = (from r in CurrentUser.AspNetUser.AspNetUserRoles select r.RoleId).ToArray();

                if (rl.Length == 0)
                {
                    return;
                }

                _memberRepo.RemoveFromRoles(rl, CurrentUser.UserId);
                return;
            }

            var rs = ParseCommaString(value);

            // select out roles that the member is currently in, but aren't in the string given
            var td = (from l in CurrentUser.AspNetUser.AspNetUserRoles where rs.Contains(l.AspNetRole.Name) == false select l.RoleId).ToArray();

            // select out roles that the member is not in, but are in the string given
            var ta = (from l in rs where (from u in CurrentUser.AspNetUser.AspNetUserRoles select u.AspNetRole.Name).Contains(l) == false select l);

            if (td.Length != 0)
            {
                _memberRepo.RemoveFromRoles(td, CurrentUser.UserId);
            }

            List<string> newRoleIds = new List<string>();
            foreach (var lbl in ta)
            {
                var role = _roleRepo.GetByName(lbl);

                if (role == null)
                {
                    continue;
                }

                newRoleIds.Add(role.Id);
            }

            if (newRoleIds.Count != 0)
            {
                _memberRepo.AddRoles(CurrentUser.UserId, newRoleIds.ToArray());
            }
        }

        protected void SaveFacilityZone(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                // Delete the Facility Site that might map to this Membership Site
                var site = _facRepo.FindBySiteId(CurrentUser.UserId);
                if (site != null)
                {
                    bool archived = false;
                    _facRepo.Delete(site.Id, true, out archived);
                }
            }
            else
            {
                int zoneId = 0;
                // This could be in 1 of 2 formats 
                // a. Zone Name | Floor Name | Building Name | Facility Name
                // b. Zone Id
                System.Text.RegularExpressions.Regex rex = new System.Text.RegularExpressions.Regex(@"(?<zone>.+)\|(?<floor>.+)\|(?<building>.+)\|(?<facility>.+)");
                var match = rex.Match(value);
                if (match.Success)
                {
                    var facilityName = match.Groups["facility"].Value.Trim();
                    var buildingName = match.Groups["building"].Value.Trim();
                    var floorName = match.Groups["floor"].Value.Trim();
                    var zoneName = match.Groups["zone"].Value.Trim();

                    // Find the Facility by name
                    var facility = _facRepo.FindByName(FacilityStructureType.Facility, facilityName);
                    if (facility == null)
                    {
                        throw new InvalidOperationException(string.Format("Facility {0} not found", facilityName));
                    }
                    var building = _facRepo.FindByName(facility.Id, buildingName);
                    if (building == null)
                    {
                        throw new InvalidOperationException(string.Format("Building '{0}' not found in Facility '{1}'", buildingName, facilityName));
                    }
                    var floor = _facRepo.FindByName(building.Id, floorName);
                    if (floor == null)
                    {
                        throw new InvalidOperationException(string.Format("Floor '{0}' not found in Building '{1}' of Facility '{2}'", floorName, buildingName, facilityName));
                    }
                    var zone = _facRepo.FindByName(floor.Id, zoneName);
                    if (zone == null)
                    {
                        throw new InvalidOperationException(string.Format("Zone '{0}' not found in Floor '{1}' of Building '{2}' of Facility '{3}'", zoneName, floorName, buildingName, facilityName));
                    }
                    zoneId = zone.Id;
                }
                else
                {
                    if (!int.TryParse(value, out zoneId))
                    {
                        throw new InvalidOperationException(string.Format("Facility Zone '{0}' is not one of \na. Zone Name | Floor Name | Building Name | Facility Name\nb. Zone Id", value));
                    }

                    var zone = _facRepo.GetById(zoneId);
                    if (zone == null)
                    {
                        throw new InvalidOperationException(string.Format("Facilty Zone with Id '{0}' does not exist", zoneId));
                    }
                }

                // See if there is a Facility Site under the Zone with same name
                var site = _facRepo.FindBySiteId(CurrentUser.UserId);
                if (site == null)
                {
                    site = new FacilityStructureDTO()
                    {
                        __IsNew = true,
                        ParentId = zoneId,
                        SiteId = CurrentUser.UserId,
                        StructureType = (int)(FacilityStructureType.Site)
                    };
                }
                else
                {
                    // Since only Facility Sites can have be found by the Membership SiteId this record *should* be a Facility Site
                    // set its parent Zone
                    site.ParentId = zoneId;
                }
                site = _facRepo.Save(site, true, false);
            }
        }

        public void SaveEntity(string uniqueId)
        {
            if (CurrentUser == null)
            {
                CurrentUser = _memberRepo.Create(Current.Username, "12345678", Current.Timezone, GetLoadInstructions());
            }

            CurrentUser.UserType = null;        // remove this since we explicitly set id next ..
            CurrentUser.UserTypeId = Current.UserType;
            CurrentUser.Name = Current.Name;
            CurrentUser.AspNetUser.Email = Current.Email;
            CurrentUser.Latitude = Current.Latitude;
            CurrentUser.Longitude = Current.Longitude;
            CurrentUser.TimeZone = Current.Timezone;
            CurrentUser.ExpiryDate = Current.ExpiryDate;

            if (CurrentUser.Company != null)
            {
                CurrentUser.Company = null;
                CurrentUser.CompanyId = null;
            }

            CurrentUser.CompanyId = Current.CompanyId;
            CurrentUser.DefaultTabletProfileId = Current.TabletProfile;

            SaveLabels(Current.Labels);
            SaveRoles(Current.Roles);
            SaveFacilityZone(Current.FacilityZone);

            _memberRepo.Save(CurrentUser, false, true);
        }

        public void WriteValue(FieldTransform transform, string value)
        {
            if (UserTypes == null)
            {
                UserTypes = _typeRepo.GetAll();
            }

            if (TabletProfiles == null)
            {
                TabletProfiles = _tabletRepo.GetAll();
            }

            if (TimeZones == null)
            {
                TimeZones = TimeZoneInfo.GetSystemTimeZones();
            }

            decimal v = 0;

            string field = transform.Destination.Name;

            switch (field)
            {
                case SimpleImportExportTransformFactory.UserType:
                    var userType = (from u in UserTypes where u.Name == value select u);
                    if (userType.Count() == 0)
                    {
                        throw new ArgumentException(string.Format("{0} is not a valid user type", value));
                    }
                    Current.UserType = userType.First().Id;

                    break;
                case SimpleImportExportTransformFactory.Name:
                    Current.Name = value;
                    break;
                case SimpleImportExportTransformFactory.Email:
                    Current.Email = value;
                    break;
                case SimpleImportExportTransformFactory.Latitude:
                    if (string.IsNullOrEmpty(value))
                    {
                        Current.Latitude = null;
                        break;
                    }

                    if (decimal.TryParse(value, out v) == false)
                    {
                        throw new ArgumentException(string.Format("{0} is not a valid latitude", value));
                    }

                    Current.Latitude = v;
                    break;
                case SimpleImportExportTransformFactory.Longitude:
                    if (string.IsNullOrEmpty(value))
                    {
                        Current.Longitude = null;
                        break;
                    }

                    if (decimal.TryParse(value, out v) == false)
                    {
                        throw new ArgumentException(string.Format("{0} is not a valid longitude", value));
                    }

                    Current.Longitude = v;
                    break;
                case SimpleImportExportTransformFactory.Timezone:
                    TimeZoneInfo tz = _appTenant.TimeZoneInfo();

                    if (string.IsNullOrEmpty(value))
                    {
                        Current.Timezone = tz.Id;
                        break;
                    }

                    string tzDisplay = value;
                    if (tzDisplay == "UTC")
                    {
                        tzDisplay = "(UTC) Co-ordinated Universal Time";
                    }

                    var ids = (from t in TimeZones where t.DisplayName == tzDisplay select t.Id);

                    if (ids.Count() == 0)
                    {
                        throw new ArgumentException(string.Format("{0} is not a valid Timezone", value));
                    }

                    Current.Timezone = ids.First();
                    break;
                case SimpleImportExportTransformFactory.ExpiryDate:
                    if (string.IsNullOrEmpty(value))
                    {
                        Current.ExpiryDate = null;
                        break;
                    }

                    DateTime d = DateTime.MinValue;

                    if (DateTime.TryParse(value, out d) == false)
                    {
                        throw new ArgumentException(string.Format("{0} is not a valid date", value));
                    }

                    Current.ExpiryDate = d;
                    break;
                case SimpleImportExportTransformFactory.Labels:
                    Current.Labels = value;
                    break;
                case SimpleImportExportTransformFactory.Roles:
                    Current.Roles = value;
                    break;
                case SimpleImportExportTransformFactory.Company:
                    if (string.IsNullOrEmpty(value))
                    {
                        Current.CompanyId = null;
                        break;
                    }

                    CompanyDTO company = null;

                    if (CompanyCache.TryGetValue(value, out company) == false)
                    {
                        var companies = _companyRepo.GetAll(CompanyLoadInstructions.None);

                        foreach (var c in companies)
                        {
                            if (CompanyCache.ContainsKey(c.Name) == true)
                            {
                                continue;
                            }

                            CompanyCache.Add(c.Name, c);
                        }

                        if (CompanyCache.TryGetValue(value, out company) == false)
                        {
                            throw new InvalidOperationException(string.Format("Company {0} does not exist", value));
                        }
                    }

                    Current.CompanyId = company.Id;

                    break;
                case SimpleImportExportTransformFactory.FacilityZone:
                    Current.FacilityZone = value;
                    break;
                case SimpleImportExportTransformFactory.TabletProfile:
                    if (string.IsNullOrEmpty(value))
                    {
                        Current.TabletProfile = null;
                        break;
                    }

                    var p = (from t in TabletProfiles where t.Name == value select t);

                    if (p.Count() == 0)
                    {
                        throw new InvalidOperationException(string.Format("Tablet profile {0} does not exist", value));
                    }

                    Current.TabletProfile = p.First().Id;
                    break;
            }
        }
    }

    public class SimpleMembershipExportParser : ExportParser<IList<UserDataDTO>>
    {
        protected const string TypesLookupName = "Membership Types";
        protected const string CompanyLookupName = "Companies";
        protected const string TabletLookupName = "Tablet Profiles";
        protected const string TimezoneLookupName = "Timezones";

        protected readonly IFacilityStructureRepository _facRepo;
        protected readonly IMembershipTypeRepository _typeRepo;
        protected readonly ICompanyRepository _companyRepo;
        protected readonly ITabletProfileRepository _tabletRepo;

        public bool IncludeArchived { get; set; }

        public SimpleMembershipExportParser(IFacilityStructureRepository facRepo,
            IMembershipTypeRepository typeRepo,
            ICompanyRepository companyRepo,
            ITabletProfileRepository tabletRepo)
        {
            _facRepo = facRepo;
            _typeRepo = typeRepo;
            _companyRepo = companyRepo;
            _tabletRepo = tabletRepo;
        }

        public override IImportWriter GetWriter(IList<UserDataDTO> source)
        {
            return null;
        }

        public override ImportParserResult Import(IList<UserDataDTO> source, List<FieldTransform> transforms, IImportWriter writer, bool isTestOnly, Action<int> progressCallback)
        {
            ImportParserResult result = new ImportParserResult();

            if (isTestOnly == true)
            {
                return result;
            }

            if (writer is IImportExcelWriter)
            {
                IImportExcelWriter excelWriter = (IImportExcelWriter)writer;

                var types = _typeRepo.GetAll();
                excelWriter.AddLookupSheet(TypesLookupName, (from t in types select t.Name).ToList());

                var companies = _companyRepo.GetAll(CompanyLoadInstructions.None);
                excelWriter.AddLookupSheet(CompanyLookupName, (from c in companies select c.Name).ToList());

                var profiles = _tabletRepo.GetAll();
                excelWriter.AddLookupSheet(TabletLookupName, (from p in profiles select p.Name).ToList());

                var timezones = TimeZoneInfo.GetSystemTimeZones();
                excelWriter.AddLookupSheet(TimezoneLookupName, (from t in timezones select t.DisplayName).ToList());

            }

            WriteTitles(transforms, writer);

            foreach (var user in source)
            {
                WriteUser(user, writer, transforms);
            }

            return result;
        }

        public void WriteUser(UserDataDTO user, IImportWriter writer, List<FieldTransform> transforms)
        {
            writer.LoadEntity(user.UserId.ToString());

            foreach (var transform in transforms)
            {
                string field = transform.Destination.Name;

                switch (field)
                {
                    case SimpleImportExportTransformFactory.Username:
                        writer.WriteValue(transform, user.AspNetUser.UserName);
                        break;
                    case SimpleImportExportTransformFactory.UserType:
                        SetLookupColumn(writer, true, TypesLookupName);
                        writer.WriteValue(transform, user.UserType.Name);
                        break;
                    case SimpleImportExportTransformFactory.Name:
                        writer.WriteValue(transform, user.Name);
                        break;
                    case SimpleImportExportTransformFactory.Email:
                        if (user.AspNetUser == null)
                        {
                            writer.WriteValue(transform, "");
                            break;
                        }
                        writer.WriteValue(transform, user.AspNetUser.Email);
                        break;
                    case SimpleImportExportTransformFactory.Latitude:
                        if (user.Latitude.HasValue)
                        {
                            writer.WriteValue(transform, user.Latitude.Value.ToString());
                            break;
                        }

                        writer.WriteValue(transform, "");

                        break;
                    case SimpleImportExportTransformFactory.Longitude:
                        if (user.Longitude.HasValue)
                        {
                            writer.WriteValue(transform, user.Longitude.Value.ToString());
                            break;
                        }

                        writer.WriteValue(transform, "");

                        break;
                    case SimpleImportExportTransformFactory.Timezone:
                        SetLookupColumn(writer, true, TimezoneLookupName);

                        TimeZoneInfo tz = TimeZoneInfoResolver.ResolveWindows(user.TimeZone);

                        writer.WriteValue(transform, tz.DisplayName);
                        break;
                    case SimpleImportExportTransformFactory.ExpiryDate:
                        writer.WriteValue(transform, DateTimeToExportString(user.ExpiryDate));
                        break;
                    case SimpleImportExportTransformFactory.Labels:
                        if (user.LabelUsers.Count == 0)
                        {
                            writer.WriteValue(transform, "");
                            break;
                        }

                        writer.WriteValue(transform, string.Join(",", (from l in user.LabelUsers select l.Label.Name)));
                        break;
                    case SimpleImportExportTransformFactory.Roles:
                        if (user.AspNetUser.AspNetUserRoles.Count == 0)
                        {
                            writer.WriteValue(transform, "");
                            break;
                        }

                        writer.WriteValue(transform, string.Join(",", (from r in user.AspNetUser.AspNetUserRoles select r.AspNetRole.Name)));
                        break;
                    case SimpleImportExportTransformFactory.Company:
                        SetLookupColumn(writer, true, CompanyLookupName);
                        if (user.CompanyId.HasValue == false)
                        {
                            writer.WriteValue(transform, "");
                            break;
                        }

                        writer.WriteValue(transform, user.Company.Name);
                        break;
                    case SimpleImportExportTransformFactory.FacilityZone:
                        if (user.FacilityStructures.Count == 0)
                        {
                            writer.WriteValue(transform, "");
                            break;
                        }

                        var fs = _facRepo.GetById(user.FacilityStructures.First().Id, FacilityStructureLoadInstructions.ParentDeep);
                        // we are going to output Zone Name | Floor Name | Building Name | Facility Name

                        writer.WriteValue(transform, string.Format("{0} | {1} | {2} | {3}", fs.Parent.Name, fs.Parent.Parent.Name, fs.Parent.Parent.Parent.Name, fs.Parent.Parent.Parent.Parent.Name));

                        break;
                    case SimpleImportExportTransformFactory.TabletProfile:
                        SetLookupColumn(writer, true, TabletLookupName);
                        if (user.DefaultTabletProfileId.HasValue == false)
                        {
                            writer.WriteValue(transform, "");
                            break;
                        }

                        writer.WriteValue(transform, user.TabletProfile.Name);
                        break;
                }
            }
        }

        public override List<FieldMapping> Parse(IList<UserDataDTO> source, List<FieldMapping> mappings)
        {
            return mappings;
        }

        public override ImportParserResult Verify(IList<UserDataDTO> source, List<FieldMapping> mappings)
        {
            return new ImportParserResult();
        }
    }
}
