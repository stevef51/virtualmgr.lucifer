﻿using System.Collections.Generic;

namespace ConceptCave.Repository
{
    public static class RoleHelper
    {
        public const string COREROLE_USER = "User";
        public const string COREROLE_NAVIGATION = "Navigation";

        public const string COREROLE_RESOURCES = "ResourceManager";
        public const string COREROLE_PUBlISHING = "PublishingManager";

        public const string COREROLE_REPORTS = "CoreReports";
        public const string COREROLE_VIEWREPORTS = "ViewReports";

        public const string COREROLE_ORGANISATION_ROLES = "OrganisationRoles";
        public const string COREROLE_ORGANISATION_HIERARCHY = "OrganisationHierarchy";

        public const string COREROLE_LABELS = "LabelManager";
        public const string COREROLE_COMPANIES = "CompanyManager";
        public const string COREROLE_MEMBERSHIP = "MembershipManager";
        public const string COREROLE_MEMBERSHIPIMPORTER = "MembershipImportManager";
        public const string COREROLE_MEMBERSHIPTYPE = "MembershipTypeManager";
        public const string COREROLE_MEDIA = "MediaManager";
        public const string COREROLE_SETTINGS = "Settings";
        public const string COREROLE_ROLES = "Roles";
        public const string COREROLE_NEWSFEED = "NewsFeed";

        public const string COREROLE_TASKTYPE = "TaskTypeManager";
        public const string COREROLE_ROSTERING = "RosterManager";
        public const string COREROLE_SCHEDULING = "ScheduleManager";
        public const string COREROLE_SCHEDULINGAPPROVAL = "ScheduleApprovalManager";
        public const string COREROLE_TASKDELETE = "TaskDelete";
        public const string COREROLE_ENDOFDAY = "EndOfDayManager";

        public const string COREROLE_STANDARDS = "StandardsManager";

        public const string COREROLE_STOCK_PRODUCT = "ProductManager";
        public const string COREROLE_JSFUNCTION = "JSFunctionManager";

        public const string COREROLE_TIMESHEETITEMTYPE = "TimesheetItemTypeManager";

        public const string COREROLE_DEVICEMANAGEMENT = "DeviceManagement";

        public const string COREROLE_DESIGNER = "ResourceManager,PublishingManager";
        public const string COREROLE_ORGANISATION = "OrganisationRoles,OrganisationHierarchy";
        public const string COREROLE_LANGUAGE = "LanguageManager";

        public const string COREROLE_ADMIN = COREROLE_ADMIN_USERSANDROLES + "," + COREROLE_ADMIN_LISTS + "," + COREROLE_ADMIN_MEDIAANDNEWS + "," + COREROLE_ADMIN_ROSTERSANDSCHEDULES + "," + COREROLE_ADMIN_SYSTEM;

        public const string COREROLE_ADMIN_USERSANDROLES = "CompanyManager,MembershipManager,MembershipTypeManager,MembershipImportManager,UserBillingType,InvoiceAdmin,Roles,DeviceManagement,ProductManager";
        public const string COREROLE_ADMIN_LISTS = "LabelManager,TaskTypeManager,ScheduleManager,TimeshetItemTypeManager,ProductManager";
        public const string COREROLE_ADMIN_MEDIAANDNEWS = "MediaManager,NewsFeed";
        public const string COREROLE_ADMIN_ROSTERSANDSCHEDULES = "RosterManager,ScheduleManager,ScheduleApprovalManager,EndOfDayManager";
        public const string COREROLE_ADMIN_SYSTEM = "Settings,LanguageManager,CronManager,CoreReports,AssetAdmin,TabletAdmin,AssetTypeAdmin,FacilityStructureAdmin";

        public const string COREROLE_NOTLIMITEDTOHIERARCHY = "NotLimitedToHierarchy";

        public const string COREROLE_CRON = "CronManager";

        public const string COREROLE_WORKLOADING = "WorkLoading";
        public const string COREROLE_MULTI_TENANT_ADMIN = "MultiTenantAdmin";
        public const string COREROLE_DELETE_TENANT = "DeleteTenant";

        public const string COREROLE_USERBILLINGTYPE = "UserBillingType";
        public const string COREROLE_INVOICEADMIN = "InvoiceAdmin";
        public const string COREROLE_TABLETADMIN = "TabletAdmin";
        public const string COREROLE_TABLETPROFILEADMIN = "TabletProfileAdmin";
        public const string COREROLE_ASSETADMIN = "AssetAdmin";
        public const string COREROLE_ASSETTYPEADMIN = "AssetTypeAdmin";
        public const string COREROLE_FACILITYSTRUCTUREADMIN = "FacilityStructureAdmin";
        public const string COREROLE_QRCODEADMIN = "QRCodeAdmin";

        public const string ALLROLES = "User,Navigation,ResourceManager,PublishingManager,CoreReports,OrganisationRoles,OrganisationHierarchy,LabelManager,CompanyManager,MembershipManager,MembershipTypeManager,MediaManager,Settings,Roles,RosterManager,ScheduleManager,CronManager,ViewReports,UserBillingType,TabletAdmin,AssetAdmin,AssetTypeAdmin,FacilityStructureAdmin,TabletProfileAdmin,QRCodeAdmin";

        /// <summary>
        /// Acts as a store for the roles so that we can check to see if all the roles are in the db
        /// </summary>
        public static Dictionary<string, string> CoreRoles { get; set; }

        public static Dictionary<string, string> OptionalRoles { get; set; }

        static RoleHelper()
        {
            CoreRoles = new Dictionary<string, string>();
            OptionalRoles = new Dictionary<string, string>();

            CoreRoles.Add("COREROLE_USER", COREROLE_USER);
            CoreRoles.Add("COREROLE_NAVIGATION", COREROLE_NAVIGATION);
            CoreRoles.Add("COREROLE_RESOURCES", COREROLE_RESOURCES);
            CoreRoles.Add("COREROLE_PUBlISHING", COREROLE_PUBlISHING);
            CoreRoles.Add("COREROLE_REPORTS", COREROLE_REPORTS);
            CoreRoles.Add("COREROLE_VIEWREPORTS", COREROLE_VIEWREPORTS);
            CoreRoles.Add("COREROLE_ORGANISATION_ROLES", COREROLE_ORGANISATION_ROLES);
            CoreRoles.Add("COREROLE_ORGANISATION_HIERARCHY", COREROLE_ORGANISATION_HIERARCHY);
            CoreRoles.Add("COREROLE_LABELS", COREROLE_LABELS);
            CoreRoles.Add("COREROLE_COMPANIES", COREROLE_COMPANIES);
            CoreRoles.Add("COREROLE_MEMBERSHIP", COREROLE_MEMBERSHIP);
            CoreRoles.Add("COREROLE_MEMBERSHIPIMPORTER", COREROLE_MEMBERSHIPIMPORTER);
            CoreRoles.Add("COREROLE_MEMBERSHIPTYPE", COREROLE_MEMBERSHIPTYPE);
            CoreRoles.Add("COREROLE_MEDIA", COREROLE_MEDIA);
            CoreRoles.Add("COREROLE_SETTINGS", COREROLE_SETTINGS);
            CoreRoles.Add("COREROLE_ROLES", COREROLE_ROLES);
            CoreRoles.Add("COREROLE_NEWSFEED", COREROLE_NEWSFEED);
            CoreRoles.Add("COREROLE_TASKTYPE", COREROLE_TASKTYPE);
            CoreRoles.Add("COREROLE_ROSTERING", COREROLE_ROSTERING);
            CoreRoles.Add("COREROLE_SCHEDULING", COREROLE_SCHEDULING);
            CoreRoles.Add("COREROLE_SCHEDULINGAPPROVAL", COREROLE_SCHEDULINGAPPROVAL);
            CoreRoles.Add("COREROLE_TASKDELETE", COREROLE_TASKDELETE);
            CoreRoles.Add("COREROLE_ENDOFDAY", COREROLE_ENDOFDAY);
            CoreRoles.Add("COREROLE_STOCK_PRODUCT", COREROLE_STOCK_PRODUCT);
            CoreRoles.Add("COREROLE_TIMESHEETITEMTYPE", COREROLE_TIMESHEETITEMTYPE);
            CoreRoles.Add("COREROLE_DEVICEMANAGEMENT", COREROLE_DEVICEMANAGEMENT);
            CoreRoles.Add("COREROLE_NOTLIMITEDTOHIERARCHY", COREROLE_NOTLIMITEDTOHIERARCHY);
            CoreRoles.Add("COREROLE_LANGUAGE", COREROLE_LANGUAGE);
            CoreRoles.Add("COREROLE_CRON", COREROLE_CRON);
            CoreRoles.Add("COREROLE_WORKLOADING", COREROLE_WORKLOADING);
            CoreRoles.Add("COREROLE_MULTI_TENANT_ADMIN", COREROLE_MULTI_TENANT_ADMIN);
            CoreRoles.Add("COREROLE_USERBILLINGTYPE", COREROLE_USERBILLINGTYPE);
            CoreRoles.Add("COREROLE_INVOICEADMIN", COREROLE_INVOICEADMIN);
            CoreRoles.Add("COREROLE_TABLETADMIN", COREROLE_TABLETADMIN);
            CoreRoles.Add("COREROLE_ASSETADMIN", COREROLE_ASSETADMIN);
            CoreRoles.Add("COREROLE_ASSETTYPEADMIN", COREROLE_ASSETTYPEADMIN);
            CoreRoles.Add("COREROLE_FACILITYSTRUCTUREADMIN", COREROLE_FACILITYSTRUCTUREADMIN);
            CoreRoles.Add("COREROLE_JSFUNCTION", COREROLE_JSFUNCTION);
            CoreRoles.Add("COREROLE_TABLETPROFILEADMIN", COREROLE_TABLETPROFILEADMIN);

            CoreRoles.Add("COREROLE_STANDARDS", COREROLE_STANDARDS);

            CoreRoles.Add("TaskUtilityOperationsAssignTasksToOthers", "TaskUtilityOperationsAssignTasksToOthers");
            CoreRoles.Add("TaskUtilityOperationsAcquireTasksFromOthers", "TaskUtilityOperationsAcquireTasksFromOthers");
            CoreRoles.Add("TaskUtilityOperationsCancel", "TaskUtilityOperationsCancel");
            CoreRoles.Add("TaskUtilityOperationsFinishByManagement", "TaskUtilityOperationsFinishByManagement");
            CoreRoles.Add("TaskUtilityOperationsDelete", "TaskUtilityOperationsDelete");
            CoreRoles.Add("TaskUtilityOperationsClearStartTime", "TaskUtilityOperationsClearStartTime");

            CoreRoles.Add("CanSideStepHierarchy", "CanSideStepHierarchy");
            CoreRoles.Add("NotLimitedToCompany", "NotLimitedToCompany");
            CoreRoles.Add("QRCodeAdmin", "QRCodeAdmin");

            OptionalRoles.Add("COREROLE_MULTI_TENANT_ADMIN", COREROLE_MULTI_TENANT_ADMIN);
            OptionalRoles.Add("COREROLE_DELETE_TENANT", COREROLE_DELETE_TENANT);
        }
    }
}
