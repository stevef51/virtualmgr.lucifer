﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Editor;
using ConceptCave.Data.EntityClasses;

namespace ConceptCave.Repository
{
    public interface IPublishingHandler
    {
        void Handle(DesignDocument document, PublishingGroupResourceEntity entity);
    }
}
