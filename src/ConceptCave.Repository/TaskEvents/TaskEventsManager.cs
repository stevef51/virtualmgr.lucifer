﻿using ConceptCave.BusinessLogic.Configuration;
using ConceptCave.Checklist;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.RepositoryInterfaces.TaskEvents;
using ConceptCave.RepositoryInterfaces.TaskEvents.Actions;
using ConceptCave.RepositoryInterfaces.TaskEvents.Conditions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.Repository.TaskEvents
{
    public class TaskEventsManager : ITaskEventsManager, ITaskEventConditionActionFactory
    {
        protected IReflectionFactory _reflectionFactory;
        protected IRepositorySectionManager _sectionManager;
        protected ITaskRepository _taskRepo;

        public TaskEventsManager(IReflectionFactory reflectionFactory, 
            IRepositorySectionManager sectionManager,
            ITaskRepository taskRepo)
        {
            _reflectionFactory = reflectionFactory;
            _sectionManager = sectionManager;
            _taskRepo = taskRepo;
        }

        public void Run(TaskEventStage stage, IList<ProjectJobTaskDTO> tasks, IList<ITaskEventCondition> conditions)
        {
            if(tasks == null || tasks.Count == 0)
            {
                return;
            }
            // we re-load the tasks with a known set of information, so that any actions
            // that get called have a known object graph for each task
            IList<ProjectJobTaskDTO> ts = _taskRepo.GetById((from t in tasks select t.Id).ToArray(), 
                RepositoryInterfaces.Enums.ProjectJobTaskLoadInstructions.TaskType | 
                RepositoryInterfaces.Enums.ProjectJobTaskLoadInstructions.Users | 
                RepositoryInterfaces.Enums.ProjectJobTaskLoadInstructions.Roster |
                RepositoryInterfaces.Enums.ProjectJobTaskLoadInstructions.AllTranslated,
                new Dictionary<object,object>());

            foreach(var condition in conditions)
            {
                if(condition.Applies(stage) == false)
                {
                    continue;
                }

                var matches = condition.Matches(ts);

                if(matches.Count == 0)
                {
                    continue;
                }

                var notMatched = (from t in ts where (from m in matches select m.Id).Contains(t.Id) == false select t).ToList();

                foreach(var action in condition.Actions)
                {
                    action.Apply(matches, notMatched);
                }
            }
        }

        public ITaskEventConditionAction Create(string id)
        {
            var handler = _sectionManager.RosterConditionActionHandlerForId(id);

            return (ITaskEventConditionAction)_reflectionFactory.InstantiateObject(Type.GetType(handler));
        }
    }
}
