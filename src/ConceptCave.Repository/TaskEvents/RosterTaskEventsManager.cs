﻿using ConceptCave.BusinessLogic.Configuration;
using ConceptCave.Checklist;
using ConceptCave.Repository.TaskEvents.Conditions;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.RepositoryInterfaces.TaskEvents;
using ConceptCave.RepositoryInterfaces.TaskEvents.Actions;
using ConceptCave.RepositoryInterfaces.TaskEvents.Conditions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.Repository.TaskEvents
{
    public class RosterTaskEventsManager : TaskEventsManager, IRosterTaskEventsManager
    {
        public RosterTaskEventsManager(IReflectionFactory reflectionFactory,
            IRepositorySectionManager sectionManager,
            ITaskRepository taskRepo)
            : base(reflectionFactory, sectionManager, taskRepo)
        {

        }

        public void Run(TaskEventStage stage, IList<DTO.DTOClasses.ProjectJobTaskDTO> tasks, IList<DTO.DTOClasses.RosterEventConditionDTO> conditions)
        {
            if(tasks == null || tasks.Count == 0)
            {
                return;
            }

            if(conditions == null || conditions.Count == 0)
            {
                return;
            }

            IList<ITaskEventCondition> eventConditions = new List<ITaskEventCondition>();

            foreach(var condition in conditions)
            {
                var c = CreateTaskFilterCondition(condition);

                eventConditions.Add(c);
            }

            Run(stage, tasks, eventConditions);
        }

        private TaskFilterCondition CreateTaskFilterCondition(DTO.DTOClasses.RosterEventConditionDTO condition)
        {
            var c = new TaskFilterCondition(this);

            IList<ITaskEventConditionAction> actions = new List<ITaskEventConditionAction>();
            foreach (var action in condition.RosterEventConditionActions)
            {
                var a = Create(action.Action);

                a.Configure(action.Data);

                actions.Add(a);
            }

            c.Configure(condition.Data, (TaskEventStage)condition.RosterProjectJobTaskEvents, actions);
            return c;
        }

        public void SetNextUTCRunTime(DateTime referenceDate, TimeZoneInfo timezone, IList<ConceptCave.DTO.DTOClasses.RosterEventConditionDTO> conditions)
        {
            foreach(var condition in conditions)
            {
                var c = CreateTaskFilterCondition(condition);

                condition.NextRunTime = c.NextLocalRunTime(referenceDate);
                if(condition.NextRunTime.HasValue)
                {
                    condition.NextRunTime = TimeZoneInfo.ConvertTimeToUtc(condition.NextRunTime.Value, timezone);
                }
            }
        }
    }
}
