﻿using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.TaskEvents;
using ConceptCave.RepositoryInterfaces.TaskEvents.Actions;
using ConceptCave.RepositoryInterfaces.TaskEvents.Conditions;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.Repository.TaskEvents.Conditions
{
    public class TaskFilterCondition : ITaskEventCondition
    {
        protected ITaskEventConditionActionFactory _actionFactory;
        protected List<int> Levels { get; set; }
        protected List<Guid> TaskTypeIds { get; set; }
        protected TaskEventStage Stage { get; set; }
        protected TimeSpan? RunTime { get; set; }

        public IList<ITaskEventConditionAction> Actions { get; protected set; }

        public TaskFilterCondition(ITaskEventConditionActionFactory actionFactory)
        {
            _actionFactory = actionFactory;
        }

        public void Configure(string data, TaskEventStage stage, IList<ITaskEventConditionAction> actions)
        {
            JObject d = JObject.Parse(data);

            Levels = new List<int>();
            TaskTypeIds = new List<Guid>();
            if(d["levels"] != null)
            {
                string l = d["levels"].ToString();

                
                if(string.IsNullOrEmpty(l) == false)
                {
                    foreach(string part in l.Split(','))
                    {
                        Levels.Add(int.Parse(part.Trim()));
                    }
                }
            }

            if (d["tasktypes"] != null)
            {
                JArray tt = (JArray)d["tasktypes"];

                foreach(JObject t in tt)
                {
                    TaskTypeIds.Add(Guid.Parse(t["id"].ToString()));
                }
            }

            RunTime = null;
            if(d["time"] != null)
            {
                var t = d["time"].ToString();
                if(string.IsNullOrEmpty(t) == false)
                {
                    RunTime = TimeSpan.Parse(d["time"].ToString());
                }
            }

            Stage = stage;

            Actions = actions;
        }

        public bool Applies(TaskEventStage stage)
        {
            if(stage != TaskEventStage.Time)
            {
                return (Stage & stage) == stage;
            }

            // it's timed, we assume who ever does the loading only loads
            // conditions due to run
            return true;
        }

        public IList<DTO.DTOClasses.ProjectJobTaskDTO> Matches(IList<DTO.DTOClasses.ProjectJobTaskDTO> tasks)
        {
            var m = (from t in tasks select t);

            if(Levels.Count > 0)
            {
                m = m.Where(t => Levels.Contains(t.Level));
            }

            if(TaskTypeIds.Count > 0)
            {
                m = m.Where(t => TaskTypeIds.Contains(t.TaskTypeId));
            }

            return m.ToList();
        }

        public DateTime? NextLocalRunTime(DateTime referenceDate)
        {
            if(RunTime.HasValue == false)
            {
                return null;
            }

            var result = new DateTime(referenceDate.Year, referenceDate.Month, referenceDate.Day, RunTime.Value.Hours, RunTime.Value.Minutes, RunTime.Value.Seconds, DateTimeKind.Unspecified);

            if(RunTime.Value <= referenceDate.TimeOfDay)
            {
                result = result.AddDays(1);
            }

            return result;
        }
    }
}
