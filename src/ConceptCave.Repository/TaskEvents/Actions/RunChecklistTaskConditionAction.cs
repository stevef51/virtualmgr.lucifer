﻿using ConceptCave.BusinessLogic;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.TaskEvents.Actions;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.Repository.TaskEvents.Actions
{
    public class RunChecklistTaskConditionAction : ITaskEventConditionAction
    {
        public class RunChecklistTaskConditionTaskHelper
        {
            public Guid Id { get; set; }
            public string Name { get; set; }
            public string Description { get; set; }
            public Guid OwnerId { get; set; }
            public string Owner { get; set; }
            public Guid? SiteId { get; set; }
            public string Site { get; set; }
            public string TaskType { get; set; }
            public int Level { get; set; }
            public ProjectJobTaskStatus Status { get; set; }
            public int RosterId { get; set; }
            public int? HierarchyBucketId { get; set; }

            public RunChecklistTaskConditionTaskHelper(ProjectJobTaskDTO task)
            {
                Id = task.Id;
                Name = task.Name;
                Description = task.Description;
                OwnerId = task.OwnerId;
                Owner = task.Owner.Name;
                SiteId = task.SiteId;
                if(SiteId.HasValue)
                {
                    Site = task.Site.Name;
                }
                TaskType = task.ProjectJobTaskType.Name;
                Level = task.Level;
                Status = (ProjectJobTaskStatus)task.Status;
                RosterId = task.RosterId;
                HierarchyBucketId = task.HierarchyBucketId;
            }
        }

        int? PublishedGroupResourceId { get; set; }
        private readonly IWorkingDocumentStateManager _stateMgr;

        public RunChecklistTaskConditionAction(IWorkingDocumentStateManager stateMgr)
        {
            _stateMgr = stateMgr;
        }

        public void Configure(string data)
        {
            JObject d = JObject.Parse(data);

            if(d["publishedresourceid"] == null)
            {
                return;
            }

            PublishedGroupResourceId = int.Parse(d["publishedresourceid"].ToString());
        }

        protected IList<RunChecklistTaskConditionTaskHelper> ToHelpers(IList<ProjectJobTaskDTO> tasks)
        {
            return tasks.Select(t => new RunChecklistTaskConditionTaskHelper(t)).ToList();
        }

        public void Apply(IList<DTO.DTOClasses.ProjectJobTaskDTO> matches, IList<DTO.DTOClasses.ProjectJobTaskDTO> notMatched)
        {
            _stateMgr.AllowSave = false;

            // we load the tasks as arguments into the working document, so that it can reference them and do what ever it needs to do
            Dictionary<string, object> arguments = new Dictionary<string,object>();

            arguments.Add("matched", ToHelpers(matches));
            arguments.Add("notmatched", ToHelpers(notMatched));

            var workingDocContainer = _stateMgr.BeginWorkingDocument(PublishedGroupResourceId.Value, Guid.Empty, null, null, arguments);
            var workingEntity = workingDocContainer.DocumentEntity;
            var wd = workingDocContainer.DocumentObject;

            wd.Next();

            _stateMgr.FinishWorkingDocument(wd, ref workingEntity);
        }
    }
}
