﻿using ConceptCave.Checklist;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.RepositoryInterfaces.TaskEvents.Actions;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace ConceptCave.Repository.TaskEvents.Actions
{
    public class AuditTaskConditionAction : ITaskEventConditionAction
    {
        public class EvenSpreadHelper
        {
            public ProjectJobTaskDTO Task { get; set; }
            public AuditTaskConditionActionDTO Audit { get; set; }
        }

        public class SimpleRandomSampleResult
        {
            public IList<ProjectJobTaskDTO> RemainingItems { get; set; }
            public List<ProjectJobTaskDTO> SampledItems { get; set; }
        }

        protected static Random RandomGen = new Random();

        protected ITaskRepository _taskRepo;
        protected IAuditTaskConditionActionRepository _actionRepo;

        protected bool EnforceEvenSampling { get; set; }
        protected string Group { get; set; }

        protected int RelationshipTypeId { get; set; }

        protected ProjectJobTaskRelationshipTypeDTO RelationshipType { get; set; }

        public AuditTaskConditionAction(ITaskRepository taskRepo, IAuditTaskConditionActionRepository actionRepo)
        {
            _taskRepo = taskRepo;
            _actionRepo = actionRepo;
        }

        public void Configure(string data)
        {
            JObject d = JObject.Parse(data);

            if (d["enforceevensampling"] != null)
            {
                EnforceEvenSampling = bool.Parse(d["enforceevensampling"].ToString());
            }

            if (d["evensamplinggroup"] != null)
            {
                Group = d["evensamplinggroup"].ToString();
            }

            if (d["relationshiptypeid"] != null)
            {
                RelationshipTypeId = int.Parse(d["relationshiptypeid"].ToString());

                RelationshipType = _taskRepo.GetRelationshipTypeById(RelationshipTypeId);
            }
        }

        public void Apply(IList<DTO.DTOClasses.ProjectJobTaskDTO> matches, IList<DTO.DTOClasses.ProjectJobTaskDTO> notMatched)
        {
            // we don't support auditing of spot tasks at the moment
            var source = from t in notMatched where t.ProjectJobScheduledTaskId.HasValue == true select t;

            if (source.Count() == 0)
            {
                return;
            }

            // so for each match that we have, we select one from the not matched list and put the two in the same
            // task group, so there is a relation between the two. The algorithm for the selection is either random
            // or even sampling (where we randommly select from the scheduled tasks that have been audited the least).
            if (EnforceEvenSampling == false)
            {
                SimpleRandomSampling(matches, source.ToList());
                return;
            }

            // we are using an even spread, to do this we need to group the tasks by how
            // many times their scheduled tasks have been audited.
            var previousSamples = _actionRepo.GetByIds((from t in source.ToList() select t.ProjectJobScheduledTaskId.Value).ToArray(), Group);

            // anything that hasn't been come across before needs to get a 'dummy' record added in here
            var helpers = CalculateHelpers(previousSamples, source.ToList());

            EvenRandomSampling(matches, helpers);
        }

        protected IList<EvenSpreadHelper> CalculateHelpers(IList<AuditTaskConditionActionDTO> previous, IList<ProjectJobTaskDTO> source)
        {
            List<EvenSpreadHelper> result = new List<EvenSpreadHelper>();

            foreach (var s in source)
            {
                AuditTaskConditionActionDTO a = null;

                var prev = from p in previous where p.ProjectJobScheduledTaskId == s.ProjectJobScheduledTaskId.Value select p;

                if (prev.Count() == 0)
                {
                    a = new AuditTaskConditionActionDTO()
                    {
                        __IsNew = true,
                        ProjectJobScheduledTaskId = s.ProjectJobScheduledTaskId.Value,
                        Group = Group,
                        SampleCount = 0
                    };
                }
                else
                {
                    a = prev.First();
                }

                result.Add(new EvenSpreadHelper()
                {
                    Task = s,
                    Audit = a
                });
            }

            return result;
        }

        protected void AssignRelationships(ProjectJobTaskDTO audit, ProjectJobTaskDTO destination)
        {
            if (destination.ProjectJobTaskGroupId.HasValue == true)
            {
                audit.ProjectJobTaskGroupId = destination.ProjectJobTaskGroupId;
            }
            else
            {
                Guid groupId = CombFactory.NewComb();
                audit.ProjectJobTaskGroupId = groupId;
                destination.ProjectJobTaskGroupId = groupId;
            }

            ProjectJobTaskRelationshipDTO relation = new ProjectJobTaskRelationshipDTO()
            {
                __IsNew = true,
                SourceProjectJobTaskId = audit.Id,
                DestinationProjectJobTaskId = destination.Id,
                ProjectJobTaskRelationshipTypeId = RelationshipTypeId
            };

            destination.ProjectJobTaskRelationships.Add(relation);

            // we now allow some adjustment of some of the properties on the task, so as
            // to allow for a sensible name etc of the audit. Just going to fake a lingo
            // like expression here for simplicity
            audit.Name = audit.Name.Replace("#{relationship}", RelationshipType.Name);
            audit.Name = audit.Name.Replace("#{audited}", destination.Name);

            foreach (var t in audit.Translated)
            {
                t.Name = t.Name.Replace("#{relationship}", RelationshipType.Name);
                t.Name = t.Name.Replace("#{audited}", destination.Name);
            }

            audit.SiteId = destination.SiteId;
            audit.SiteLatitude = destination.SiteLatitude;
            audit.SiteLongitude = destination.SiteLongitude;

            _taskRepo.Save(audit, false, true); // just the group id in here
            //_taskRepo.Save(destination, false, true);
        }

        protected bool HasExistingAudit(Guid id, bool lookInSource)
        {
            if (lookInSource)
            {
                return _taskRepo.GetRelationshipBySourceId(id, RelationshipTypeId) != null;
            }

            return _taskRepo.GetRelationshipByDestinationId(id, RelationshipTypeId) != null;
        }

        protected SimpleRandomSampleResult SimpleRandomSampling(IList<DTO.DTOClasses.ProjectJobTaskDTO> matches, IList<DTO.DTOClasses.ProjectJobTaskDTO> notMatched)
        {
            // make a copy of the not matched as we need a list to draw from and don't want to change
            // the not matched list
            List<DTO.DTOClasses.ProjectJobTaskDTO> source = new List<DTO.DTOClasses.ProjectJobTaskDTO>();

            notMatched.ToList().ForEach(n => source.Add(n));

            SimpleRandomSampleResult result = new SimpleRandomSampleResult()
            {
                RemainingItems = source,
                SampledItems = new List<ProjectJobTaskDTO>()
            };

            foreach (var match in matches)
            {
                if (source.Count == 0)
                {
                    // nothing more to pick from, so get out of here
                    break;
                }

                if (HasExistingAudit(match.Id, true) == true)
                {
                    continue; // its already set to be an audit of something
                }

                bool hasFind = false;

                ProjectJobTaskDTO s = null;

                while (hasFind == false)
                {
                    // pick an item at random from our source
                    var index = RandomGen.Next(0, source.Count);

                    s = source[index];

                    if (HasExistingAudit(s.Id, false) == false)
                    {
                        hasFind = true;
                    }
                    else
                    {
                        // its already got an audit, we'll repeat the find again, but remove this
                        // so it doesn't get picked
                        source.Remove(s);

                        if (source.Count == 0)
                        {
                            hasFind = true; // no more to pick from, get out of here
                        }
                    }
                }

                if (s == null)
                {
                    continue; // nothing found to audit against, next
                }

                // now we need to map match and s to each other, we are going to do this by puttin them in the same
                // group and then by creating a relationship between the two tasks of type Audit
                AssignRelationships(match, s);

                result.SampledItems.Add(s);

                source.Remove(s); // remove the item we've just picked so it can't be used again
            }

            return result;
        }

        protected int GetSamplesWithLowestCount(IList<EvenSpreadHelper> helpers, out IList<DTO.DTOClasses.ProjectJobTaskDTO> result)
        {
            int lowestCount = (from p in helpers orderby p.Audit.SampleCount ascending select p.Audit.SampleCount).Take(1).First();

            result = (from h in helpers where h.Audit.SampleCount == lowestCount select h.Task).ToList();

            return lowestCount;
        }

        protected void EvenRandomSampling(IList<DTO.DTOClasses.ProjectJobTaskDTO> matches, IList<EvenSpreadHelper> helpers)
        {
            var hs = new List<EvenSpreadHelper>();
            helpers.ForEach(h => hs.Add(h));

            IList<ProjectJobTaskDTO> leastSampled = new List<ProjectJobTaskDTO>();
            int lowestCount = GetSamplesWithLowestCount(hs, out leastSampled);

            foreach (var match in matches)
            {
                var mats = new List<ProjectJobTaskDTO>();
                mats.Add(match);

                var randomResult = SimpleRandomSampling(mats, leastSampled);

                // we need to loop through the sampled items, increment their count and remove them
                // from the pool
                foreach (var s in randomResult.SampledItems)
                {
                    var helper = (from h in hs where h.Task.Id == s.Id select h).First();

                    helper.Audit.SampleCount++;
                    hs.Remove(helper);
                }

                if (randomResult.RemainingItems.Count == 0)
                {
                    int lc = GetSamplesWithLowestCount(hs, out leastSampled);

                    if (lc == lowestCount)
                    {
                        // ok, so the level hasn't changed since last time, so we must not
                        // have any more tasks that haven't been audited before left
                        break;
                    }

                    lowestCount = lc;
                }
                else
                {
                    leastSampled = randomResult.RemainingItems;
                }
            }

            // now we save off the sample counts
            foreach (var helper in helpers)
            {
                if (helper.Audit.__IsNew == true && helper.Audit.SampleCount == 0)
                {
                    // its new and not been touched
                    continue;
                }

                _actionRepo.Save(helper.Audit, false, false);
            }
        }
    }
}
