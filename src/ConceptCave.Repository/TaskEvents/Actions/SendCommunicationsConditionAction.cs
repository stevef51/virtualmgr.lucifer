﻿using ConceptCave.Checklist.Facilities;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.Lingo;
using ConceptCave.Checklist.RunTime;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.RepositoryInterfaces.TaskEvents.Actions;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualMgr.Membership;

namespace ConceptCave.Repository.TaskEvents.Actions
{
    public enum SendCommunicationsConditionActionDestination
    {
        /// <summary>
        /// Communication is sent to the supervisor of the owner of the tasks
        /// </summary>
        TaskOwnerSupervisor = 0,
        /// <summary>
        /// Communication is sent to the owner of the tasks
        /// </summary>
        TaskOwner = 1,
        /// <summary>
        /// Communications is sent to the Email/Mobile configured in the action
        /// </summary>
        Custom = 2
    }

    public enum SendCommunicarionsConditionActionSendRule
    {
        /// <summary>
        /// There is no condition
        /// </summary>
        NoCondition = 0,
        /// <summary>
        /// Only send communication if the task owner doesn't have any started tasks
        /// </summary>
        TaskOwnerHasNotStarted = 1,
        /// <summary>
        /// Only send communication if any task was Finished By System
        /// </summary>
        FinishedBySystem = 2,
    }

    public class SendCommunicationsConditionAction : ITaskEventConditionAction
    {
        protected IMembershipRepository _memberRepo;
        protected IHierarchyRepository _hierarchyRepo;
        protected IGlobalSettingsRepository _globalRepo;
        protected IEmailConfiguration _mailConfig;
        protected ISendGridConfiguration _sendGridConfig;
        private readonly IMembership _membership;
        public string EmailSubject { get; set; }
        public string EmailBody { get; set; }
        public string SMSBody { get; set; }
        public SendCommunicationsConditionActionDestination Destination { get; set; }
        public string CustomEmailAddress { get; set; }
        public string CustomMobile { get; set; }
        public SendCommunicarionsConditionActionSendRule SendRule { get; set; }

        public string SMSEndpoint { get; set; }
        public string SMSApiKey { get; set; }

        public class DestinationHelper
        {
            public string EmailAddress { get; set; }
            public string Mobile { get; set; }

            public string Name { get; set; }

            protected string SMSEndPoint { get; set; }
            protected string SMSApiKey { get; set; }

            public DestinationHelper(string SMSEndpoint, string SMSAPIKey)
            {
                SMSEndPoint = SMSEndpoint;
                SMSApiKey = SMSAPIKey;
                Tasks = new List<ProjectJobTaskDTO>();
            }

            public string GetUniqueOwnerNames(string seperator)
            {
                var ts = (from t in Tasks select t.Owner.Name).Distinct().ToArray();

                return string.Join(seperator, ts);
            }

            public string GetOwnerDetails()
            {
                var table = new FormattingFacility.TableFormattingHelper() { FormatForEmail = true };
                var headerRow = table.CreateHeaderRow();
                headerRow.Add("#");
                headerRow.Add("Name");
                headerRow.Add("Task");
                headerRow.Add("Site");

                int cnt = 1;
                foreach (var task in Tasks.OrderBy(t => t.Owner.Name).OrderBy(t => t.Name))
                {
                    var row = table.CreateBodyRow();
                    row.Add(cnt);
                    row.Add(task.Owner != null ? task.Owner.Name : null);
                    row.Add(task.Name);
                    row.Add(task.Site != null ? task.Site.Name : null);
                    cnt++;
                }
                return table.ToHtml();
            }

            protected ILingoProgram CreateLingoProgram(bool forSMS)
            {
                LingoProgramDefinition definition = new LingoProgramDefinition() { SourceCode = "" };
                WorkingDocument wd = new WorkingDocument();
                ILingoProgram program = definition.Compile(wd);
                var vb = wd.Variables;

                vb.Variable<object>("Name", Name);
                vb.Variable<object>("OwnerNames", forSMS ? GetUniqueOwnerNames(", ") : GetUniqueOwnerNames("<br/>"));
                vb.Variable<object>("Roster", Tasks.First().Roster.Name);
                vb.Variable<object>("OwnerDetails", forSMS ? "" : GetOwnerDetails());

                return program;
            }

            public void Send(IEmailConfiguration mailConfig, ISendGridConfiguration sendGridConfig, SendCommunicationsConditionAction action)
            {
                if (Tasks.Count == 0)
                {
                    return;
                }

                SendEmail(mailConfig, sendGridConfig, action);
                SendSMS(mailConfig, action);
            }

            public void SendEmail(IEmailConfiguration mailConfig, ISendGridConfiguration sendGridConfig, SendCommunicationsConditionAction action)
            {
                if (string.IsNullOrEmpty(EmailAddress))
                {
                    return;
                }

                if (string.IsNullOrEmpty(action.EmailBody) || string.IsNullOrEmpty(action.EmailSubject))
                {
                    return;
                }

                ConceptCave.Checklist.Facilities.EmailFacility.EmailTemplate template = new Checklist.Facilities.EmailFacility.EmailTemplate(mailConfig, sendGridConfig);
                template.Subject = action.EmailSubject;
                template.Body = action.EmailBody;
                template.To = EmailAddress;
                template.SendImmediately = true;

                var program = CreateLingoProgram(false);

                template.Send(program);
            }

            public void SendSMS(IEmailConfiguration mailConfig, SendCommunicationsConditionAction action)
            {
                if (string.IsNullOrEmpty(SMSApiKey) || string.IsNullOrEmpty(SMSEndPoint))
                {
                    return;
                }

                if (string.IsNullOrEmpty(Mobile))
                {
                    return;
                }

                if (string.IsNullOrEmpty(action.SMSBody))
                {
                    return;
                }

                var program = CreateLingoProgram(true);

                var smsText = program.ExpandString(action.SMSBody);

                JObject message = new JObject();
                JArray mobiles = new JArray();
                foreach (var mobile in Mobile.Split(','))
                {
                    mobiles.Add(mobile);
                }
                message["content"] = smsText;
                message["to"] = mobiles;
                message["binary"] = false;
                message["charset"] = "UTF-8";

                RestClient client = new RestClient(SMSEndPoint);
                RestRequest request = new RestRequest("messages", Method.POST);
                request.AddHeader("Authorization", SMSApiKey);
                request.AddHeader("Accept", "application/json");
                request.AddParameter("application/json", message.ToString(), ParameterType.RequestBody);

                var response = client.Execute(request);
            }

            public IList<DTO.DTOClasses.ProjectJobTaskDTO> Tasks { get; set; }
        }

        public SendCommunicationsConditionAction(IMembershipRepository memberRepo,
            IHierarchyRepository hierarchyRepo,
            IGlobalSettingsRepository globalRepo,
            IEmailConfiguration mailConfig,
            ISendGridConfiguration sendGridConfig,
            IMembership membership)
        {
            _memberRepo = memberRepo;
            _hierarchyRepo = hierarchyRepo;
            _globalRepo = globalRepo;
            _mailConfig = mailConfig;
            _sendGridConfig = sendGridConfig;
            _membership = membership;

            SMSEndpoint = _globalRepo.GetSetting<string>("Clickatell.EndPoint");
            SMSApiKey = _globalRepo.GetSetting<string>("Clickatell.ApiKey");
        }

        public void Configure(string data)
        {
            JObject d = JObject.Parse(data);

            EmailSubject = d["emailsubject"].ToString();
            EmailBody = d["emailbody"].ToString();
            SMSBody = d["smsbody"].ToString();
            CustomEmailAddress = d["customemail"].ToString();
            CustomMobile = d["custommobile"].ToString();
            Destination = (SendCommunicationsConditionActionDestination)Enum.Parse(typeof(SendCommunicationsConditionActionDestination), d["destination"].ToString());
            SendRule = (SendCommunicarionsConditionActionSendRule)Enum.Parse(typeof(SendCommunicarionsConditionActionSendRule), d["sendrule"].ToString());
        }

        protected IList<DestinationHelper> GetDestinations(IList<DTO.DTOClasses.ProjectJobTaskDTO> matches)
        {
            IList<DestinationHelper> destinations = new List<DestinationHelper>();

            if (Destination == SendCommunicationsConditionActionDestination.Custom)
            {
                destinations.Add(new DestinationHelper(SMSEndpoint, SMSApiKey)
                {
                    EmailAddress = CustomEmailAddress,
                    Mobile = CustomMobile,
                    Tasks = matches
                });
            }
            else if (Destination == SendCommunicationsConditionActionDestination.TaskOwner)
            {
                foreach (var o in (from t in matches group t by t.OwnerId into g select new { OwnerId = g.Key, Tasks = g.ToList() }))
                {
                    var m = _membership.GetUser(o.OwnerId);

                    destinations.Add(new DestinationHelper(SMSEndpoint, SMSApiKey)
                    {
                        EmailAddress = m.Email,
                        Mobile = o.Tasks.First().Owner.Mobile,
                        Tasks = o.Tasks
                    });
                }
            }
            else if (Destination == SendCommunicationsConditionActionDestination.TaskOwnerSupervisor)
            {
                Dictionary<int, DestinationHelper> cache = new Dictionary<int, DestinationHelper>();

                foreach (var o in (from t in matches group t by t.HierarchyBucketId into g select new { BucketId = g.Key, Tasks = g.ToList() }))
                {
                    if (o.BucketId.HasValue == false)
                    {
                        continue;
                    }

                    var bucket = _hierarchyRepo.GetById(o.BucketId.Value, RepositoryInterfaces.Enums.HierarchyBucketLoadInstructions.None);

                    if (bucket.ParentId.HasValue == false)
                    {
                        // there is no supervisor, so skip
                        continue;
                    }

                    DestinationHelper dest = null;
                    if (cache.TryGetValue(bucket.ParentId.Value, out dest) == false)
                    {
                        HierarchyBucketDTO parent = _hierarchyRepo.GetById(bucket.ParentId.Value, RepositoryInterfaces.Enums.HierarchyBucketLoadInstructions.User);

                        if (parent.UserId.HasValue == false)
                        {
                            continue;
                        }

                        var m = _membership.GetUser(parent.UserId);
                        dest = new DestinationHelper(SMSEndpoint, SMSApiKey)
                        {
                            EmailAddress = m.Email,
                            Mobile = parent.UserData.Mobile,
                        };
                        destinations.Add(dest);
                        cache.Add(bucket.ParentId.Value, dest);
                    }

                    o.Tasks.ForEach(t => dest.Tasks.Add(t));
                }
            }

            return destinations;
        }

        protected void FilterForTaskOwnerHasNotStarted(DestinationHelper dest)
        {
            IList<DTO.DTOClasses.ProjectJobTaskDTO> processed = new List<DTO.DTOClasses.ProjectJobTaskDTO>();

            foreach (var t in (from o in dest.Tasks group o by o.OwnerId into g select new { OwnerId = g.Key, Tasks = g.ToList() }))
            {
                var notStarted = from o in t.Tasks where ProjectJobTaskStatusGroups.NotStarted.Contains((ProjectJobTaskStatus)o.Status) select o;

                // so if the notstarted.count == t.Tasks.count then all of the tasks for the owner aren't started and our requirements have been met for a communication
                if (notStarted.Count() == t.Tasks.Count)
                {
                    // something is started, so add to the processed count
                    t.Tasks.ForEach(o => processed.Add(o));
                }
            }

            // replace the set of tasks in the destination with only the ones that meet our criteria
            dest.Tasks = processed;
        }

        protected void FilterForTaskFinishedBySystem(DestinationHelper dest)
        {
            // Any Task which is finished by system will trigger a communication
            dest.Tasks = (from o in dest.Tasks where ((ProjectJobTaskStatus)o.Status) == ProjectJobTaskStatus.FinishedBySystem select o).ToList();
        }

        public void Apply(IList<DTO.DTOClasses.ProjectJobTaskDTO> matches, IList<DTO.DTOClasses.ProjectJobTaskDTO> notMatched)
        {
            // break things out so that we can process by a number of unique destinations
            var destinations = GetDestinations(matches);

            // now filter the tasks associated with those destinations down based on the send rule
            foreach (var dest in destinations)
            {
                switch (SendRule)
                {
                    case SendCommunicarionsConditionActionSendRule.TaskOwnerHasNotStarted:
                        FilterForTaskOwnerHasNotStarted(dest);
                        break;

                    case SendCommunicarionsConditionActionSendRule.FinishedBySystem:
                        FilterForTaskFinishedBySystem(dest);
                        break;
                }
            }

            // so we now have destinations, each with a set of tasks that meet the send rule requirements (what ever they are)
            // all we need to do now is send the comms to the destinations
            foreach (var dest in destinations)
            {
                if (string.IsNullOrEmpty(dest.EmailAddress) == false || string.IsNullOrEmpty(dest.Mobile) == false)
                {
                    dest.Send(_mailConfig, _sendGridConfig, this);
                }
            }
        }
    }
}
