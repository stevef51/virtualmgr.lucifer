﻿using System;
using SD.LLBLGen.Pro.ORMSupportClasses;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.Checklist;
using ConceptCave.RepositoryInterfaces.Repositories;

namespace ConceptCave.Repository.Injected
{
    public class OWorkingDocumentPublicVariableRepository : IWorkingDocumentPublicVariableRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public OWorkingDocumentPublicVariableRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }

        public void InsertOrUpdate(Guid workingDocumentId, string variableName, string variableValue, bool? variablePassFail, decimal? variableScore)
        {
            PredicateExpression expression = new PredicateExpression(WorkingDocumentPublicVariableFields.WorkingDocumentId == workingDocumentId & WorkingDocumentPublicVariableFields.Name == variableName);
            EntityCollection<WorkingDocumentPublicVariableEntity> result = new EntityCollection<WorkingDocumentPublicVariableEntity>();

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), 1);


                WorkingDocumentPublicVariableEntity entity = null;

                if (result.Count == 0)
                {
                    entity = new WorkingDocumentPublicVariableEntity();
                    entity.Id = CombFactory.NewComb();
                    entity.WorkingDocumentId = workingDocumentId;
                    entity.Name = variableName;
                }
                else
                {
                    entity = result[0];
                }

                entity.Value = variableValue;
                entity.PassFail = variablePassFail;
                entity.Score = variableScore;


                adapter.SaveEntity(entity);
            }
        }
    }
}
