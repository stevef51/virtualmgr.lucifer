﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Data.HelperClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace ConceptCave.Repository.Injected
{
    public abstract class CalendarRuleRepositoryBase
    {
        protected virtual PredicateExpression GetCalendarRulePredicateForDate(DateTime date)
        {
            PredicateExpression dayExecute = new PredicateExpression();

            var day = date.DayOfWeek;
            switch (day)
            {
                case DayOfWeek.Sunday:
                    dayExecute.Add(CalendarRuleFields.ExecuteSunday == true);
                    break;
                case DayOfWeek.Monday:
                    dayExecute.Add(CalendarRuleFields.ExecuteMonday == true);
                    break;
                case DayOfWeek.Tuesday:
                    dayExecute.Add(CalendarRuleFields.ExecuteTuesday == true);
                    break;
                case DayOfWeek.Wednesday:
                    dayExecute.Add(CalendarRuleFields.ExecuteWednesday == true);
                    break;
                case DayOfWeek.Thursday:
                    dayExecute.Add(CalendarRuleFields.ExecuteThursday == true);
                    break;
                case DayOfWeek.Friday:
                    dayExecute.Add(CalendarRuleFields.ExecuteFriday == true);
                    break;
                case DayOfWeek.Saturday:
                    dayExecute.Add(CalendarRuleFields.ExecuteSaturday == true);
                    break;
            }

            DateTime utcDate = date.ToUniversalTime();

            PredicateExpression dateBetween = new PredicateExpression(CalendarRuleFields.StartDate <= utcDate & (CalendarRuleFields.EndDate == DBNull.Value | CalendarRuleFields.EndDate > utcDate));

            PredicateExpression expression = new PredicateExpression();
            expression.AddWithAnd(dateBetween);
            expression.AddWithAnd(dayExecute);

            return expression;
        }

        protected virtual PredicateExpression GetCalendarRulePredicateForDateAndFrequency(DateTime date, CalendarRuleFrequency frequency)
        {
            var expression = GetCalendarRulePredicateForDate(date);

            expression.AddWithAnd(CalendarRuleFields.Frequency == frequency.ToString());

            return expression;
        }
    }
}
