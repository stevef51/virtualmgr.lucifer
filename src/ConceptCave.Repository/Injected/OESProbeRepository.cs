﻿using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.DTOConverters;
using ConceptCave.RepositoryInterfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualMgr.Central;
using SD.LLBLGen.Pro.ORMSupportClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.Data;
using ConceptCave.Data.Linq;
using ConceptCave.Data.HelperClasses;
using ConceptCave.DTO.DTOClasses;
using SD.LLBLGen.Pro.LinqSupportClasses;
using ConceptCave.Data.DatabaseSpecific;

namespace ConceptCave.Repository.Injected
{
    public class OESProbeRepository : IESProbeRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public OESProbeRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }

        private PrefetchPath2 BuildPrefetchPath(ESProbeLoadInstructions instructions)
        {
            PrefetchPath2 path = new PrefetchPath2(EntityType.ESProbeEntity);
            if ((instructions & ESProbeLoadInstructions.Sensors) != 0)
            {
                var a = path.Add(ESProbeEntity.PrefetchPathSensors);
            }

            return path;
        }

        public DTO.DTOClasses.ESProbeDTO GetById(int id, ESProbeLoadInstructions instructions = ESProbeLoadInstructions.None)
        {
            var e = new ESProbeEntity(id);
            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntity(e, BuildPrefetchPath(instructions));
            }
            return e.IsNew ? null : e.ToDTO();
        }

        public DTO.DTOClasses.ESProbeDTO GetByKey(string key, ESProbeLoadInstructions instructions = ESProbeLoadInstructions.None)
        {
            var e = new EntityCollection<ESProbeEntity>();
            var predicate = new PredicateExpression(ESProbeFields.Key == key);
            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(e, new RelationPredicateBucket(predicate), BuildPrefetchPath(instructions));
            }
            return e.Count == 0 ? null : e.First().ToDTO();
        }

        public DTO.DTOClasses.ESProbeDTO GetByModelSerialNumber(string model, string serialNumber, ESProbeLoadInstructions instructions = ESProbeLoadInstructions.None)
        {
            var e = new EntityCollection<ESProbeEntity>();
            var predicate = new PredicateExpression(ESProbeFields.Model == model & ESProbeFields.SerialNumber == serialNumber);
            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(e, new RelationPredicateBucket(predicate), BuildPrefetchPath(instructions));
            }
            return e.Count == 0 ? null : e.First().ToDTO();
        }

        public DTO.DTOClasses.ESProbeDTO Save(DTO.DTOClasses.ESProbeDTO dto, bool refetch, bool recurse)
        {
            var e = dto.__IsNew ? new ESProbeEntity() : new ESProbeEntity(dto.Id);

            using (var adapter = _fnAdapter())
            {
                if (!dto.__IsNew)
                {
                    adapter.FetchEntity(e);
                }

                dto.ToEntity(e);

                adapter.SaveEntity(e, refetch, recurse);
            }

            return refetch ? e.ToDTO() : dto;
        }

        public IList<DTO.DTOClasses.ESProbeDTO> Search(string nameLike, int? pageNumber, int? itemsPerPage, bool includeArchived, ref int totalItems, int? searchFlags, int? includeFlags)
        {
            using (var adapter = _fnAdapter())
            {
                var lmd = new LinqMetaData(adapter);
                var items = from r in lmd.ESProbe where r.Name == null || r.Name != null && r.Name.Contains(nameLike) select r.ToDTO();
                items = items.OrderBy(i => i.Name);

                if (searchFlags.HasValue && includeFlags.HasValue)
                {
                    if (searchFlags.Value == 1)
                    {
                        if (includeFlags.Value == 1)
                        {

                            items = (from prob in lmd.ESProbe
                                     join a in lmd.Asset on prob.AssetId equals a.Id
                                     join f in lmd.FacilityStructure on a.FacilityStructureId equals f.Id
                                     where f.SiteId != null && prob.Name.Contains(nameLike)
                                     select new ESProbeDTO
                                     {
                                         Id = prob.Id,
                                         AssetId = prob.Id,
                                         Name = prob.Name,
                                         SerialNumber = prob.SerialNumber,
                                         Archived = prob.Archived,
                                         Key = prob.Key,
                                         Model = prob.Model
                                     }).AsQueryable();
                        }
                        else
                        {

                            items = (from prob in lmd.ESProbe
                                     join a in lmd.Asset on prob.AssetId equals a.Id
                                     join f in lmd.FacilityStructure on a.FacilityStructureId equals f.Id
                                     where f.SiteId == null && prob.Name.Contains(nameLike)
                                     select new ESProbeDTO
                                     {
                                         Id = prob.Id,
                                         AssetId = prob.Id,
                                         Name = prob.Name,
                                         SerialNumber = prob.SerialNumber,
                                         Archived = prob.Archived,
                                         Key = prob.Key,
                                         Model = prob.Model
                                     }).AsQueryable();
                        }
                    }

                }

                if (!includeArchived)
                    items = items.Where(i => i.Archived == false);
                else if (includeArchived)
                    items = items.Where(i => i.Archived == true);


                if (pageNumber.HasValue && itemsPerPage.HasValue)
                    items = items.Skip((pageNumber.Value - 1) * itemsPerPage.Value).Take(itemsPerPage.Value);
                return items.ToList();
            }
        }

        public void Delete(int id, bool archiveIfRequired, out bool archived)
        {
            using (var adapter = _fnAdapter())
            {
                var entity = new ESProbeEntity(id);
                archived = false;
                try
                {
                    adapter.DeleteEntity(entity);
                }
                catch (Exception ex)
                {
                    if (archiveIfRequired)
                    {
                        adapter.FetchEntity(entity);
                        entity.Archived = true;
                        adapter.SaveEntity(entity, false);
                        archived = true;
                    }
                    else
                        throw ex;
                }
            }
        }
    }
}
