﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using ConceptCave.Repository.ImportMapping;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;

namespace ConceptCave.Repository.Injected
{
    public class OImportFieldMappingRepository
    {
        private IGlobalSettingsRepository _globalSettingsRepo;
        public OImportFieldMappingRepository(IGlobalSettingsRepository globalSettingsRepo)
        {
            _globalSettingsRepo = globalSettingsRepo;
        }
        public List<FieldMapping> GetFieldMappings(int userTypeId)
        {
            string name = GlobalSettingsConstants.SystemSetting_MembershipImportMapping + "_" + userTypeId;

            var xml = _globalSettingsRepo.GetSetting<string>(name);

            if (string.IsNullOrEmpty(xml) == true)
            {
                return new List<FieldMapping>();
            }

            List<FieldMapping> result = null;

            using (var stream = new MemoryStream(Convert.FromBase64String(xml)))
            {
                BinaryFormatter formatter = new BinaryFormatter();

                result = (List<FieldMapping>)formatter.Deserialize(stream);
            }

            return result;
        }

        public List<FieldTransform> GetFieldTransforms(int userTypeId)
        {
            string name = GlobalSettingsConstants.SystemSetting_MembershipImportTransform + "_" + userTypeId;

            var xml = _globalSettingsRepo.GetSetting<string>(name);

            if (string.IsNullOrEmpty(xml) == true)
            {
                return new List<FieldTransform>();
            }

            List<FieldTransform> result = null;

            using (var stream = new MemoryStream(Convert.FromBase64String(xml)))
            {
                BinaryFormatter formatter = new BinaryFormatter();

                result = (List<FieldTransform>)formatter.Deserialize(stream);
            }

            return result;
        }

        public void SaveFieldMappings(int userTypeId, List<FieldMapping> mappings)
        {
            string xml = null;

            using (var stream = new MemoryStream())
            {
                BinaryFormatter formatter = new BinaryFormatter();

                formatter.Serialize(stream, mappings);

                stream.Close();

                xml = Convert.ToBase64String(stream.ToArray());
            }

            string name = GlobalSettingsConstants.SystemSetting_MembershipImportMapping + "_" + userTypeId;

            _globalSettingsRepo.SetSetting(name, xml);
        }

        public void SaveFieldTransforms(int userTypeId, List<FieldTransform> transforms)
        {
            string xml = null;

            using (var stream = new MemoryStream())
            {
                BinaryFormatter formatter = new BinaryFormatter();

                formatter.Serialize(stream, transforms);

                stream.Close();

                xml = Convert.ToBase64String(stream.ToArray());
            }

            string name = GlobalSettingsConstants.SystemSetting_MembershipImportTransform + "_" + userTypeId;

            _globalSettingsRepo.SetSetting(name, xml);
        }
    }
}
