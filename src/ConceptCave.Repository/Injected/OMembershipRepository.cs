﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConceptCave.Data;
using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.Linq;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Data.DTOConverters;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.RepositoryInterfaces.ViewDefs;
using SD.LLBLGen.Pro.LinqSupportClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using ConceptCave.Data.HelperClasses;
using VirtualMgr.Membership;
using System.Transactions;
using ConceptCave.Data.TypedViewClasses;

namespace ConceptCave.Repository.Injected
{
    public class OMembershipRepository : IMembershipRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;

        private readonly Func<IMembership> _membershipResolver;
        private readonly Func<IRoles> _rolesResolver;

        private IMembership __membership;
        private IRoles __roles;

        private readonly IMembershipTypeRepository _membershipTypeRepo;

        private readonly IWorkingDocumentRepository _workingDocumentRepo;
        private readonly IRoleRepository _roleRepo;
        protected IMembership Membership
        {
            get
            {
                if (__membership == null)
                {
                    __membership = _membershipResolver();
                }

                return __membership;
            }
        }

        protected IRoles Roles
        {
            get
            {
                if (__roles == null)
                {
                    __roles = _rolesResolver();
                }

                return __roles;
            }
        }

        public OMembershipRepository(
            Func<DataAccessAdapter> fnAdapter,
            Func<IMembership> membershipResolver,
            Func<IRoles> rolesResolver,
            IMembershipTypeRepository membershipTypeRepo,
            IWorkingDocumentRepository workingDocumentRepo,
            IRoleRepository roleRepo)
        {
            _fnAdapter = fnAdapter;
            _membershipResolver = membershipResolver;
            _rolesResolver = rolesResolver;
            _membershipTypeRepo = membershipTypeRepo;
            _workingDocumentRepo = workingDocumentRepo;
            _roleRepo = roleRepo;
        }

        public static PrefetchPath2 BuildPrefetchPath(MembershipLoadInstructions instructions)
        {
            PrefetchPath2 path = new PrefetchPath2((int)EntityType.UserDataEntity);

            if ((instructions & MembershipLoadInstructions.MembershipType) == MembershipLoadInstructions.MembershipType)
            {
                var sub = path.Add(UserDataEntity.PrefetchPathUserType);

                if (instructions.HasFlag(MembershipLoadInstructions.ContextResources))
                {
                    sub.SubPath.Add(UserTypeEntity.PrefetchPathUserTypeContextPublishedResources)
                       .SubPath.Add(UserTypeContextPublishedResourceEntity.PrefetchPathPublishingGroupResource);
                }
            }

            if ((instructions & MembershipLoadInstructions.Company) == MembershipLoadInstructions.Company)
            {
                path.Add(UserDataEntity.PrefetchPathCompany);
            }

            if ((instructions & MembershipLoadInstructions.Features) == MembershipLoadInstructions.Features)
            {
                var featurePath = path.Add(UserDataEntity.PrefetchPathSiteFeatures);
                featurePath.SubPath.Add(SiteFeatureEntity.PrefetchPathMeasurementUnit);

                if ((instructions & MembershipLoadInstructions.Areas) == MembershipLoadInstructions.Areas)
                {
                    featurePath.SubPath.Add(SiteFeatureEntity.PrefetchPathSiteArea);
                }
            }

            if ((instructions & MembershipLoadInstructions.Media) == MembershipLoadInstructions.Media)
            {
                var mediaPath = path.Add(UserDataEntity.PrefetchPathMedium);

                if ((instructions & MembershipLoadInstructions.MediaData) == MembershipLoadInstructions.MediaData)
                {
                    mediaPath.SubPath.Add(MediaEntity.PrefetchPathMediaData);
                }
            }

            if ((instructions & MembershipLoadInstructions.ContextData) == MembershipLoadInstructions.ContextData)
            {
                var sp = path.Add(UserDataEntity.PrefetchPathUserContextDatas);
                sp.SubPath.Add(UserContextDataEntity.PrefetchPathUserTypeContextPublishedResource);
            }

            if ((instructions & MembershipLoadInstructions.RelatedMedia) == MembershipLoadInstructions.RelatedMedia)
            {
                path.Add(UserDataEntity.PrefetchPathUserMedias).SubPath.Add(UserMediaEntity.PrefetchPathMedium);
            }

            if (((instructions & MembershipLoadInstructions.AspNetMembership) == MembershipLoadInstructions.AspNetMembership) || ((instructions & MembershipLoadInstructions.MembershipRole) == MembershipLoadInstructions.MembershipRole))
            {
                IPrefetchPathElement2 sub = path.Add(UserDataEntity.PrefetchPathAspNetUser);

                if ((instructions & MembershipLoadInstructions.MembershipRole) == MembershipLoadInstructions.MembershipRole)
                {
                    sub.SubPath.Add(AspNetUserEntity.PrefetchPathAspNetUserRoles).SubPath.Add(AspNetUserRoleEntity.PrefetchPathAspNetRole);
                }
            }

            if ((instructions & MembershipLoadInstructions.MembershipLabel) == MembershipLoadInstructions.MembershipLabel)
            {
                IPrefetchPathElement2 sub = path.Add(UserDataEntity.PrefetchPathLabelUsers);
                if ((instructions & MembershipLoadInstructions.Label) == MembershipLoadInstructions.Label)
                {
                    sub.SubPath.Add(LabelUserEntity.PrefetchPathLabel);
                }
            }

            if ((instructions & MembershipLoadInstructions.TabletProfile) == MembershipLoadInstructions.TabletProfile)
            {
                path.Add(UserDataEntity.PrefetchPathTabletProfile);
            }
            return path;
        }

        public IEnumerable<UserDataDTO> Search(string text, int page, int count)
        {
            return Search(text, page, count, MembershipLoadInstructions.None);
        }

        public IEnumerable<UserDataDTO> Search(string text, int page, int count, MembershipLoadInstructions instructions = MembershipLoadInstructions.None, Guid? companyId = null)
        {
            return Search(text, true, page, count, instructions, companyId);
        }

        public IEnumerable<UserDataDTO> Search(string text, bool includeExpired, int page, int count, MembershipLoadInstructions instructions = MembershipLoadInstructions.None, Guid? companyId = null)
        {
            EntityCollection<UserDataEntity> result = new EntityCollection<UserDataEntity>();

            PredicateExpression expression = new PredicateExpression();

            if (companyId.HasValue == true)
            {
                expression.Add(UserDataFields.CompanyId == companyId.Value);
            }

            if (includeExpired == false)
            {
                expression.Add(UserDataFields.ExpiryDate == DBNull.Value | (UserDataFields.ExpiryDate != DBNull.Value & UserDataFields.ExpiryDate > DateTime.UtcNow));
            }

            if (string.IsNullOrEmpty(text) == false)
            {
                PredicateExpression textExpression = new PredicateExpression(UserDataFields.Name % text);

                // now join in any user context stuff, which is stored in the reporting tables
                // we need select * from userdata where userid in (select reviewerid from completedquestions where id in (select id from completedquestionvalues where value like 'XXX'))
                //PredicateExpression innerExpression = new PredicateExpression(new FieldCompareSetPredicate(CompletedWorkingDocumentPresentedFactFields.Id, null, CompletedPresentedFactValueFields.CompletedWorkingDocumentPresentedFactId, null, SetOperator.In, CompletedPresentedFactValueFields.SearchValue % text));
                //PredicateExpression outerExpression = new PredicateExpression(new FieldCompareSetPredicate(UserDataFields.UserId, null, CompletedWorkingDocumentPresentedFactFields.ReportingUserReviewerId, null, SetOperator.In, innerExpression));

                //textExpression.AddWithOr(outerExpression);

                PredicateExpression userNameExpression = new PredicateExpression(new FieldCompareSetPredicate(UserDataFields.UserId, null, AspNetUserFields.Id, null, SetOperator.In, AspNetUserFields.UserName % text));
                textExpression.AddWithOr(userNameExpression);

                expression.AddWithAnd(textExpression);
            }

            SortExpression sort = new SortExpression();

            sort.Add(UserDataFields.Name | SortOperator.Ascending);

            PrefetchPath2 path = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), 0, sort, path, page, count);
            }

            return result.Select(e => e.ToDTO());
        }

        public void SaveLogin(UserLogDTO loginLog)
        {
            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(loginLog.ToEntity());
            }
        }

        public UserDataDTO GetById(Guid id, MembershipLoadInstructions instructions = MembershipLoadInstructions.None)
        {
            return GetEntityById(id, instructions)?.ToDTO();
        }

        public UserDataDTO GetById(string id, MembershipLoadInstructions instructions = MembershipLoadInstructions.None)
        {
            return GetEntityById(id, instructions)?.ToDTO();
        }

        public IEnumerable<UserDataDTO> GetByIds(IEnumerable<Guid> ids, MembershipLoadInstructions instructions = MembershipLoadInstructions.None)
        {
            EntityCollection<UserDataEntity> result = new EntityCollection<UserDataEntity>();

            PredicateExpression expression = new PredicateExpression(UserDataFields.UserId == ids);

            PrefetchPath2 path = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), 0, null, path);
            }

            return result.Select(r => r.ToDTO());

        }

        private UserDataEntity GetEntityById(Guid id, MembershipLoadInstructions instructions = MembershipLoadInstructions.None)
        {
            var e = new UserDataEntity(id);
            PrefetchPath2 path = BuildPrefetchPath(instructions);
            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntity(e, path);
            }

            return e.IsNew ? null : e;
        }

        private UserDataEntity GetEntityById(string aspnetid, MembershipLoadInstructions instructions = MembershipLoadInstructions.None)
        {
            PredicateExpression expression = new PredicateExpression(UserDataFields.AspNetUserId == aspnetid);
            PrefetchPath2 path = BuildPrefetchPath(instructions);

            EntityCollection<UserDataEntity> result = new EntityCollection<UserDataEntity>();

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), 1, null, path);
            }

            return result.Count == 0 ? null : result.First();
        }

        public UserDataDTO FindByQrcode(string qrcode)
        {
            using (var adapter = _fnAdapter())
            {
                var meta = new LinqMetaData(adapter);

                return (from u in meta.Qrcode where u.Qrcode == qrcode && !u.UserId.HasValue select u.UserData.ToDTO()).FirstOrDefault();
            }
        }

        public UserDataDTO GetByName(string name, MembershipLoadInstructions instructions = MembershipLoadInstructions.None)
        {
            using (var adapter = _fnAdapter())
            {
                var data = new LinqMetaData(adapter);
                var path = BuildPrefetchPath(instructions);

                return (from u in data.UserData
                        where u.Name == name
                        select u).WithPath(path).FirstOrDefault().ToDTO();
            }
        }

        public UserDataDTO GetByUsername(string username, MembershipLoadInstructions instructions = MembershipLoadInstructions.None)
        {
            // we want select * from tbluser where id in (select userid from aspnetuser where username = username)
            PredicateExpression expression = new PredicateExpression(new FieldCompareSetPredicate(UserDataFields.UserId, null, AspNetUserFields.Id, null, SetOperator.In, AspNetUserFields.UserName == username));

            PrefetchPath2 path = BuildPrefetchPath(instructions);

            EntityCollection<UserDataEntity> result = new EntityCollection<UserDataEntity>();

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), 1, null, path);
            }

            return result.Count == 0 ? null : result[0].ToDTO();
        }

        public IList<UserDataDTO> GetUsersByType(int type, MembershipLoadInstructions instructions = MembershipLoadInstructions.None)
        {
            PredicateExpression expression = new PredicateExpression(UserDataFields.UserTypeId == type);
            EntityCollection<UserDataEntity> result = new EntityCollection<UserDataEntity>();
            var path = BuildPrefetchPath(instructions);

            SortExpression sort = new SortExpression(UserDataFields.Name | SortOperator.Ascending);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), 0, sort, path);
            }

            return result.ToList().Select(e => e.ToDTO()).ToList();
        }

        public IEnumerable<UserDataDTO> GetByUserType(int type, MembershipLoadInstructions instructions = MembershipLoadInstructions.None)
        {
            PredicateExpression expression = new PredicateExpression(UserDataFields.UserTypeId == type);
            EntityCollection<UserDataEntity> result = new EntityCollection<UserDataEntity>();
            var path = BuildPrefetchPath(instructions);

            SortExpression sort = new SortExpression(UserDataFields.Name | SortOperator.Ascending);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), 0, sort, path);
            }

            return result.Select(e => e.ToDTO());
        }

        public IEnumerable<UserDataDTO> GetByUserTypes(IEnumerable<int> userTypeIds, MembershipLoadInstructions instructions = MembershipLoadInstructions.None)
        {
            PredicateExpression expression = new PredicateExpression(UserDataFields.UserTypeId == userTypeIds);
            PrefetchPath2 path = BuildPrefetchPath(instructions);

            EntityCollection<UserDataEntity> result = new EntityCollection<UserDataEntity>();

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), 0, null, path);
            }

            return result.Select(r => r.ToDTO());
        }

        public UserDataDTO Save(UserDataDTO user, bool refetch, bool recurse)
        {
            var entity = user.ToEntity();
            UserDataEntity existing = GetEntityById(entity.UserId);

            bool changeRolesAndLabels = false;
            int newUserTypeId = 0; // we do this as if refetch is false then using the UserTypeId after the entity has been saved will result in an exception. So capture here.
            if (existing.UserTypeId != entity.UserTypeId)
            {
                changeRolesAndLabels = true;
                newUserTypeId = entity.UserTypeId;
            }

            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(entity, refetch, recurse);
            }

            if (changeRolesAndLabels == true)
            {
                existing = GetEntityById(entity.UserId, MembershipLoadInstructions.MembershipLabel | MembershipLoadInstructions.MembershipRole);
                var ut = _membershipTypeRepo.GetById(existing.UserTypeId, MembershipTypeLoadInstructions.DefaultLabels | MembershipTypeLoadInstructions.DefaultRoles | MembershipTypeLoadInstructions.Role);

                if (existing.LabelUsers.Count == 0 && ut.UserTypeDefaultLabels.Count > 0)
                {
                    AddLabels(existing.UserId, (from l in ut.UserTypeDefaultLabels select l.LabelId).ToArray());
                }

                if (existing.AspNetUser.AspNetUserRoles.Count == 0 && ut.UserTypeDefaultRoles.Count > 0)
                {
                    Roles.AddUserToRoles(existing.AspNetUser.UserName, (from d in ut.UserTypeDefaultRoles select d.AspNetRole.Name).ToArray());
                }
            }

            return refetch ? entity.ToDTO() : user;
        }

        public UserDataDTO Create(string username, string password, string defaultTimeZone = null,
            MembershipLoadInstructions instructions = MembershipLoadInstructions.None)
        {
            var member = Membership.CreateUser(username, password);

            var item = GetEntityById(member.ProviderUserKey.ToString(), MembershipLoadInstructions.None).ToDTO();

            if (string.IsNullOrEmpty(defaultTimeZone) == false)
            {
                item.TimeZone = defaultTimeZone;
                Save(item, true, false);
            }

            return GetById(item.UserId, instructions);
        }

        public UserContextDataDTO GetUserContextData(Guid userId, int userTypeContextPublishedResourceId)
        {
            PredicateExpression expression = new PredicateExpression(UserContextDataFields.UserId == userId & UserContextDataFields.UserTypeContextPublishedResourceId == userTypeContextPublishedResourceId);

            EntityCollection<UserContextDataEntity> result = new EntityCollection<UserContextDataEntity>();

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), 1);
            }

            return result.Count == 0 ? null : result[0].ToDTO();
        }

        public UserContextDataDTO GetUserContextData(Guid userId, string contextName)
        {
            PredicateExpression expression = new PredicateExpression(UserContextDataFields.UserId == userId);

            // need something of the form 
            // select * from usecontext where usertypecontextpubid in (select id from usertypecontextpubresource where publishinggroupresourceid in (select id from pubgroupresource where name = 'contextname')))

            // so inner part first
            PredicateExpression innerExpression = new PredicateExpression(new FieldCompareSetPredicate(UserTypeContextPublishedResourceFields.PublishingGroupResourceId, null, PublishingGroupResourceFields.Id, null, SetOperator.In, PublishingGroupResourceFields.Name == contextName));
            expression.AddWithAnd(new PredicateExpression(new FieldCompareSetPredicate(UserContextDataFields.UserTypeContextPublishedResourceId, null, UserTypeContextPublishedResourceFields.Id, null, SetOperator.In, innerExpression)));

            EntityCollection<UserContextDataEntity> result = new EntityCollection<UserContextDataEntity>();

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), 1);
            }

            return result.Count == 0 ? null : result[0].ToDTO();
        }

        public void Save(UserContextDataDTO entity, bool refetch, bool recurse)
        {
            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(entity.ToEntity(), refetch, recurse);
            }
        }


        public void Remove(Guid id)
        {
            var u = GetById(id);

            var member = Membership.GetUser(u.AspNetUserId, false);

            if (member == null)
            {
                return;
            }

            using (TransactionScope scope = new TransactionScope())
            {
                _workingDocumentRepo.DeleteWorkingDocumentsForUser(id);

                PredicateExpression expression = new PredicateExpression(UserLogFields.UserId == id);
                using (var adapter = _fnAdapter())
                {
                    adapter.DeleteEntitiesDirectly(typeof(UserLogEntity), new RelationPredicateBucket(expression));
                }

                Membership.DeleteUser(member.UserName, true);

                scope.Complete();
            }
        }

        public IEnumerable<UserDataDTO> GetForLabels(IEnumerable<Guid> labels)
        {
            // we need select * from tblMembers where id in (select memberid from tblLabelMembers where labelid == labels)
            PredicateExpression expression = new PredicateExpression(new FieldCompareSetPredicate(UserDataFields.UserId, null, LabelUserFields.UserId, null, SetOperator.In, LabelUserFields.LabelId == labels));

            EntityCollection<UserDataEntity> result = new EntityCollection<UserDataEntity>();

            SortExpression sort = new SortExpression();

            sort.Add(UserDataFields.Name | SortOperator.Ascending);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), 0, sort);
            }

            return result.Select(r => r.ToDTO());
        }

        public void RemoveFromRoles(IEnumerable<string> roles, Guid userId)
        {
            var user = GetById(userId);

            PredicateExpression expression = new PredicateExpression(AspNetUserRoleFields.RoleId == roles.ToList());
            expression.AddWithAnd(AspNetUserRoleFields.UserId == user.AspNetUserId);

            using (var adapter = _fnAdapter())
            {
                adapter.DeleteEntitiesDirectly("AspNetUserRoleEntity", new RelationPredicateBucket(expression));
            }
        }

        public void AddRoles(Guid membershipId, IEnumerable<string> roleIds)
        {
            EntityCollection<AspNetUserRoleEntity> roles = new EntityCollection<AspNetUserRoleEntity>();

            var user = GetById(membershipId);

            foreach (string r in roleIds)
            {
                roles.Add(new AspNetUserRoleEntity() { UserId = user.AspNetUserId, RoleId = r });
            }

            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntityCollection(roles);
            }
        }

        public void AddLabels(Guid uid, IEnumerable<Guid> labelIds)
        {
            EntityCollection<LabelUserEntity> labels = new EntityCollection<LabelUserEntity>();

            foreach (Guid l in labelIds)
            {
                labels.Add(new LabelUserEntity() { UserId = uid, LabelId = l });
            }

            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntityCollection(labels);
            }
        }

        public UserTypeDTO GetUserTypeById(int id, MembershipTypeLoadInstructions instructions = MembershipTypeLoadInstructions.None)
        {
            return _membershipTypeRepo.GetById(id, instructions);
        }

        public UserTypeDTO GetUserTypeByName(string name, MembershipTypeLoadInstructions instructions = MembershipTypeLoadInstructions.None)
        {
            using (var adapter = _fnAdapter())
            {
                var data = new LinqMetaData(adapter);
                var path = OMembershipTypeRepository.BuildPrefetchPath(instructions);
                return (from t in data.UserType
                        where t.Name == name
                        select t).WithPath(path).FirstOrDefault().ToDTO();
            }
        }

        public IEnumerable<PublishingGroupResourceDTO> GetUserContextResourcesForType(int typeId, PublishingGroupResourceLoadInstruction instructions)
        {
            using (var adapter = _fnAdapter())
            {
                var data = new LinqMetaData(adapter);
                var path = OPublishingGroupResourceRepository.BuildPrefetch(instructions);
                return (from r in data.PublishingGroupResource
                        join ut in data.UserTypeContextPublishedResource on
                            r.Id equals ut.PublishingGroupResourceId
                        where ut.Id == typeId
                        select r).WithPath(path).Select(e => e.ToDTO()).ToList();
            }
        }

        public UserContextDataDTO CreateOrUpdateUserCtxEntry(Guid userId, int publishedResourceid, Guid completedWorkingDocumentid)
        {
            using (var adapter = _fnAdapter())
            {
                var data = new LinqMetaData(adapter);
                //Need to get the usertypecontext id associated with this resourceId
                var typectx = (from u in data.UserData
                               where u.UserId == userId
                               join ctxj in data.UserTypeContextPublishedResource on u.UserTypeId equals ctxj.UserTypeId
                               where ctxj.PublishingGroupResourceId == publishedResourceid
                               select ctxj.Id).Single();
                var e = new UserContextDataEntity(completedWorkingDocumentid, userId, typectx);
                if (!adapter.FetchEntity(e))
                    e = new UserContextDataEntity(completedWorkingDocumentid, userId, typectx);
                adapter.SaveEntity(e, true, false);
                return e.ToDTO();
            }
        }

        public bool UserIsInRole(Guid uid, string roleName)
        {
            return _roleRepo.IsUserInRole(uid, roleName);
        }

        public void AddUserInRole(Guid uid, string roleName)
        {
            using (var adapter = _fnAdapter())
            {
                var u = new UserDataEntity(uid);
                adapter.FetchEntity(u);

                var data = new LinqMetaData(adapter);
                var roleEnt = data.AspNetRole.First(r => r.Name == roleName);
                var joinEnt = new AspNetUserRoleEntity(roleEnt.Id, u.AspNetUserId);
                adapter.SaveEntity(joinEnt);
            }
        }

        public IEnumerable<string> GetRolesForUser(Guid id)
        {
            var user = GetById(id, MembershipLoadInstructions.AspNetMembership | MembershipLoadInstructions.MembershipRole);

            return (from m in user.AspNetUser.AspNetUserRoles select m.AspNetRole.Name);
        }

        public IEnumerable<UserListResponse> GetAllUsers()
        {
            //We want to be clever and only load columns into memory we givea damn about
            //llblgen only includes fields in the projection we specified
            using (var adapter = _fnAdapter())
            {
                var data = new LinqMetaData(adapter);
                return (from u in data.UserData
                        join mu in data.AspNetUser on u.AspNetUserId equals mu.Id
                        select new UserListResponse()
                        {
                            Name = u.Name,
                            Username = mu.UserName,
                            UserId = u.UserId
                        }).ToList();
            }
        }

        public IEnumerable<UserListResponse> GetAllUsersByRoles(IEnumerable<string> roles, bool includeSites = false)
        {
            PredicateExpression expression = new PredicateExpression(new FieldCompareSetPredicate(UserDataFields.UserId, null, AspNetUserRoleFields.UserId, null, SetOperator.In, AspNetUserRoleFields.RoleId == roles));

            if (includeSites == false)
            {
                expression.AddWithAnd(new FieldCompareSetPredicate(UserDataFields.UserTypeId, null, UserTypeFields.Id, null, SetOperator.In, UserTypeFields.IsAsite == false));
            }

            SortExpression sort = new SortExpression(UserDataFields.Name | SortOperator.Ascending);

            EntityCollection<UserDataEntity> result = new EntityCollection<UserDataEntity>();

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), 0, sort);
            }

            return result.Select(r => new UserListResponse() { Name = r.Name, UserId = r.UserId });
        }

        public IEnumerable<UserListResponse> GetAllUsersByLabel(IEnumerable<Guid> labels)
        {
            using (var adapter = _fnAdapter())
            {
                var data = new LinqMetaData(adapter);

                var prefetch = new PrefetchPath2(EntityType.UserDataEntity);
                prefetch.Add(UserDataEntity.PrefetchPathAspNetUser);

                var users = (from u in data.UserData
                             join mu in data.AspNetUser on u.AspNetUserId equals mu.Id
                             join lu in data.LabelUser on u.UserId equals lu.UserId
                             where labels.Contains(lu.LabelId)
                             select u)
                    .WithPath(prefetch)
                    .Distinct(); //joining on label-users will return the same user multiple times otherwise

                return (from u in users
                        select new UserListResponse()
                        {
                            Name = u.Name,
                            Username = u.AspNetUser.UserName,
                            UserId = u.UserId
                        }).ToList();
            }
        }

        public IEnumerable<UserListResponse> GetAllRealUsers()
        {
            using (var adapter = _fnAdapter())
            {
                var data = new LinqMetaData(adapter);
                return (from u in data.UserData
                        join mu in data.AspNetUser on u.AspNetUserId equals mu.Id
                        join ut in data.UserType on u.UserTypeId equals ut.Id
                        where ut.CanLogin
                        select new UserListResponse()
                        {
                            Name = u.Name,
                            Username = mu.UserName,
                            UserId = u.UserId
                        }).ToList();
            }
        }

        public IEnumerable<UserDataDTO> GetAllSites(MembershipLoadInstructions instructions)
        {
            PredicateExpression expression = new PredicateExpression(UserTypeFields.IsAsite == true);

            RelationPredicateBucket bucket = new RelationPredicateBucket(expression);
            bucket.Relations.Add(UserDataEntity.Relations.UserTypeEntityUsingUserTypeId);

            EntityCollection<UserDataEntity> result = new EntityCollection<UserDataEntity>();

            var prefetch = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, bucket, 0, null, prefetch);
            }

            return result.Select(e => e.ToDTO());
        }

        public IEnumerable<int> GetDashboardResourceIds(Guid uid)
        {
            using (var adapter = _fnAdapter())
            {
                var data = new LinqMetaData(adapter);
                var pgrids = from j in data.UserTypeDashboard
                             from u in data.UserData
                             where u.UserId == uid

                             where j.UserTypeId == u.UserTypeId
                             orderby j.SortOrder ascending
                             select j.PublishingGroupResourceId;

                return pgrids.ToList();
            }
        }

        public IEnumerable<UserTypeDashboardDTO> GetDashboardData(Guid uid)
        {
            using (var adapter = _fnAdapter())
            {
                var data = new LinqMetaData(adapter);
                var pgrs = from j in data.UserTypeDashboard
                           from u in data.UserData
                           where u.UserId == uid
                           where j.UserTypeId == u.UserTypeId
                           orderby j.SortOrder ascending
                           select j;
                var ppath = new PrefetchPath2(EntityType.UserTypeDashboardEntity);
                ppath.Add(UserTypeDashboardEntity.PrefetchPathPublishingGroupResource)
                        .SubPath.Add(PublishingGroupResourceEntity.PrefetchPathPublishingGroupResourceData);

                pgrs = pgrs.WithPath(ppath);

                return pgrs.Select(e => e.ToDTO()).ToList();
            }
        }

        public void RemoveFromLabels(IEnumerable<Guid> labels, Guid userId)
        {
            PredicateExpression expression = new PredicateExpression(LabelUserFields.LabelId == labels.ToList());
            expression.AddWithAnd(LabelUserFields.UserId == userId);

            using (var adapter = _fnAdapter())
            {
                adapter.DeleteEntitiesDirectly("LabelUserEntity", new RelationPredicateBucket(expression));
            }
        }

        public void RemoveUserContextData(Guid userId)
        {
            PredicateExpression expression = new PredicateExpression(UserContextDataFields.UserId == userId);

            using (var adapter = _fnAdapter())
            {
                adapter.DeleteEntitiesDirectly("UserContextDataEntity", new RelationPredicateBucket(expression));
            }
        }

        public string GeneratePassword(int length, int numberOfNonNumericCharacters)
        {
            return Membership.GeneratePassword(length, numberOfNonNumericCharacters);
        }

        public ChangePasswordResult ChangePassword(Guid userId, string newPassword)
        {
            var member = Membership.GetUser(userId);

            ChangePasswordResult result = new ChangePasswordResult();

            try
            {
                // EVS-415 Change Password Checklist To Support "Unlocking" Locked Users
                // When ever password is changed successfully we unlock the user
                if (member.IsLockedOut)
                    member.UnlockUser();

                if (member.ChangePassword(member.ResetPassword(), newPassword) == true)
                {
                    result.Success = true;
                }
                else
                {
                    result.Success = false;
                }
            }
            catch (Exception e)
            {
                result.Success = false;
                result.Message = e.Message;
            }

            return result;
        }

        public bool ValidateUser(string username, string password)
        {
            return Membership.ValidateUser(username, password);
        }

        public Guid RecordLogin(Guid userId, decimal? latitude, decimal? longitude, bool resolveLocation, string tabletUUID, double maxDistanceForLogin)
        {
            using (var adapter = _fnAdapter())
            {
                UserLogEntity ent = new UserLogEntity(Guid.NewGuid());
                ent.LogInTime = DateTime.UtcNow;
                ent.LogOutTime = null;
                ent.UserId = userId;
                ent.Latitude = latitude;
                ent.Longitude = longitude;
                ent.NearestLocation = null;
                ent.TabletUuid = tabletUUID;

                //Now have to decide where is closest to us.
                if (latitude.HasValue && longitude.HasValue && resolveLocation)
                {
                    var closeSites = GetClosestUsers(latitude.Value, longitude.Value, 50, true);
                    var closest = closeSites.FirstOrDefault();
                    //cap out the distance
                    if (closest != null && closest.Distance <= maxDistanceForLogin)
                        ent.NearestLocation = closest.UserId;
                }


                adapter.SaveEntity(ent, false, false);

                if (tabletUUID != null)
                {
                    TabletUuidEntity tabletEntity = new TabletUuidEntity(tabletUUID);
                    adapter.FetchEntity(tabletEntity);
                    tabletEntity.LastUserId = userId;
                    adapter.SaveEntity(tabletEntity);
                }
                return ent.Id;
            }
        }

        public UserLogDTO GetLogin(Guid sessionId)
        {
            UserLogEntity ent = null;

            using (var adapter = _fnAdapter())
            {
                ent = new UserLogEntity(sessionId);
                adapter.FetchEntity(ent);
            }

            return ent.IsNew == true ? null : ent.ToDTO();
        }

        public void RecordLogOut(Guid userId, Guid sessionId)
        {
            var ent = GetLogin(sessionId);

            if (ent == null)
            {
                return;
            }

            using (var adapter = _fnAdapter())
            {
                ent.LogOutTime = DateTime.UtcNow;
                adapter.SaveEntity(ent.ToEntity());
            }
        }

        public IList<ClosestUsersResultEntry> GetClosestUsers(decimal latitude, decimal longitude, int count, bool? isAsite)
        {
            ClosestUsersTypedView users = new ClosestUsersTypedView();
            ClosestUsersTypedView sites = new ClosestUsersTypedView();

            using (var adapter = _fnAdapter())
            {
                if (!isAsite.HasValue || isAsite == false)
                {
                    adapter.FetchTypedView(users, RetrievalProcedures.GetGetClosestUsersCallAsQuery(latitude, longitude, count, adapter));
                }
                if (!isAsite.HasValue || isAsite == true)
                {
                    adapter.FetchTypedView(sites, RetrievalProcedures.GetGetClosestSitesCallAsQuery(latitude, longitude, count, adapter));
                }
            }

            return users.Concat(sites).Select(r => new ClosestUsersResultEntry()
            {
                UserId = r.Userid,
                Latitude = r.Latitude,
                Longitude = r.Longitude,
                Distance = r.Distance
            }).ToList();
        }
    }
}
