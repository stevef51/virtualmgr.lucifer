﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Data;
using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.HelperClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using SD.LLBLGen.Pro.ORMSupportClasses;
using ConceptCave.Data.DTOConverters;

namespace ConceptCave.Repository.Injected
{
    //public class OCalendarRepository : CalendarRepositoryBase, ICalendarRepository
    //{
    //    public static PrefetchPath2 BuildPrefetchPath(CalendarEventLoadInstructions instructions)
    //    {
    //        PrefetchPath2 path = new PrefetchPath2((int)EntityType.CalendarEventEntity);

    //        if ((instructions & CalendarEventLoadInstructions.Rules) == CalendarEventLoadInstructions.Rules)
    //        {
    //            path.Add(CalendarEventEntity.PrefetchPathCalendarRules);
    //        }

    //        return path;
    //    }

    //    public IList<DTO.DTOClasses.CalendarEventDTO> GetActiveEvents(DateTime date, RepositoryInterfaces.Enums.CalendarEventLoadInstructions instructions)
    //    {
    //        var expression = this.GetActiveEventsPredicate(date);

    //        var result = new EntityCollection<CalendarEventEntity>();

    //        var path = BuildPrefetchPath(instructions);

    //        using (var adapter = _fnAdapter())
    //        {
    //            adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), path);
    //        }

    //        return result.ToList().Select(e => e.ToDTO()).ToList();
    //    }

    //    public IList<DTO.DTOClasses.CalendarEventDTO> GetEvents(int[] ids, RepositoryInterfaces.Enums.CalendarEventLoadInstructions instructions)
    //    {
    //        EntityCollection<CalendarEventEntity> result = new EntityCollection<CalendarEventEntity>();

    //        PredicateExpression expression = new PredicateExpression(CalendarEventFields.Id == ids);

    //        var path = BuildPrefetchPath(instructions);

    //        using (var adapter = _fnAdapter())
    //        {
    //            adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), path);
    //        }

    //        return result.ToList().Select(e => e.ToDTO()).ToList();
    //    }
    //}
}
