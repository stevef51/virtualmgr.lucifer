﻿using ConceptCave.Data.Linq;
using ConceptCave.RepositoryInterfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualMgr.Central;
using ConceptCave.Data.DTOConverters;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.DatabaseSpecific;

namespace ConceptCave.Repository.Injected
{
    public class OJSFunctionRepository : IJSFunctionRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public OJSFunctionRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }

        public DTO.DTOClasses.JsfunctionDTO GetById(int id)
        {
            using (var adapter = _fnAdapter())
            {
                var data = new LinqMetaData(adapter);
                return (from jsf in data.Jsfunction
                        where jsf.Id == id
                        select jsf).FirstOrDefault().ToDTO();
            }
        }
        public DTO.DTOClasses.JsfunctionDTO GetByName(string name)
        {
            using (var adapter = _fnAdapter())
            {
                var data = new LinqMetaData(adapter);
                return (from jsf in data.Jsfunction
                        where jsf.Name == name
                        select jsf).FirstOrDefault().ToDTO();
            }
        }

        public DTO.DTOClasses.JsfunctionDTO Save(DTO.DTOClasses.JsfunctionDTO jsf, bool refetch)
        {
            using (var adapter = _fnAdapter())
            {
                var e = jsf.ToEntity();
                adapter.SaveEntity(e, refetch, false);
                return refetch ? e.ToDTO() : jsf;
            }
        }

        public IList<JsfunctionDTO> GetAll(JSFunctionType? type)
        {
            using (var adapter = _fnAdapter())
            {
                var data = new LinqMetaData(adapter);
                var query = from jsf in data.Jsfunction select jsf;
                if (type.HasValue)
                {
                    query = query.Where(jsf => jsf.Type == (int)type.Value);
                }
                return (from jsf in query select jsf.ToDTO()).ToList();
            }
        }

        public IList<JsfunctionDTO> Search(string nameLike, int? pageNumber, int? itemsPerPage, bool includeArchived, ref int totalItems)
        {
            using (var adapter = _fnAdapter())
            {
                var lmd = new LinqMetaData(adapter);
                var items = from r in lmd.Jsfunction where r.Name == null || r.Name != null && r.Name.Contains(nameLike) select r.ToDTO();
                items = items.OrderBy(i => i.Name);

                if (!includeArchived)
                    items = items.Where(i => i.Archived == false);

                totalItems = items.Count();

                if (pageNumber.HasValue && itemsPerPage.HasValue)
                    items = items.Skip((pageNumber.Value - 1) * itemsPerPage.Value).Take(itemsPerPage.Value);

                return items.ToList();
            }
        }

        public void Delete(int id, bool archiveIfRequired, out bool archived)
        {
            using (var adapter = _fnAdapter())
            {
                var entity = new JsfunctionEntity(id);
                archived = false;
                try
                {
                    adapter.DeleteEntity(entity);
                }
                catch (Exception ex)
                {
                    if (archiveIfRequired)
                    {
                        adapter.FetchEntity(entity);
                        entity.Archived = true;
                        adapter.SaveEntity(entity, false);
                        archived = true;
                    }
                    else
                        throw ex;
                }
            }
        }
    }
}
