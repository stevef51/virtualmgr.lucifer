﻿using System;
using System.Linq;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Data.EntityClasses;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.Data.DTOConverters;
using SD.LLBLGen.Pro.ORMSupportClasses;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Data.DatabaseSpecific;

namespace ConceptCave.Repository.Injected
{
    public class OUserAgentRepository : IUserAgentRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public OUserAgentRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }

        public string LimitLength(string userAgent)
        {
            if (userAgent.Length > 198)
            {
                return userAgent.Substring(0, 198);
            }

            return userAgent;
        }

        public UserAgentDTO GetOrCreate(string userAgent)
        {
            var agent = GetByUserAgent(LimitLength(userAgent));

            if (agent != null)
            {
                return agent;
            }

            agent = new UserAgentDTO() { __IsNew = true, UserAgent = LimitLength(userAgent) };

            return Save(agent, true, false);
        }

        public UserAgentDTO GetByUserAgent(string userAgent)
        {
            PredicateExpression expression = new PredicateExpression(UserAgentFields.UserAgent == LimitLength(userAgent));

            EntityCollection<UserAgentEntity> result = new EntityCollection<UserAgentEntity>();

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression));
            }

            return result.FirstOrDefault()?.ToDTO();
        }

        public UserAgentDTO Save(UserAgentDTO dto, bool refetch, bool recurse)
        {
            using (var adapter = _fnAdapter())
            {
                var entity = dto.ToEntity();
                adapter.SaveEntity(entity, refetch, recurse);
                return entity.ToDTO();
            }
        }
    }
}
