﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Data.Linq;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using SD.LLBLGen.Pro.LinqSupportClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using ConceptCave.Data.DTOConverters;
using ConceptCave.Data;
using VirtualMgr.Central;


namespace ConceptCave.Repository.Injected
{
    public class OPublishingGroupRepository : IPublishingGroupRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;

        private readonly IMembershipRepository _membershipRepo;
        private readonly IPublishingGroupActorRepository _publishingGroupActorRepo;

        public OPublishingGroupRepository(Func<DataAccessAdapter> fnAdapter, IMembershipRepository membershipRepo, IPublishingGroupActorRepository publishingGroupActorRepo)
        {
            _fnAdapter = fnAdapter;
            _membershipRepo = membershipRepo;
            _publishingGroupActorRepo = publishingGroupActorRepo;
        }

        private static PrefetchPath2 BuildPrefetchPath(PublishingGroupLoadInstruction instructions)
        {
            PrefetchPath2 path = new PrefetchPath2((int)EntityType.PublishingGroupEntity);

            if ((instructions & PublishingGroupLoadInstruction.Actors) == PublishingGroupLoadInstruction.Actors)
            {
                IPrefetchPathElement2 subpath = path.Add(PublishingGroupEntity.PrefetchPathPublishingGroupActors);
                subpath.SubPath.Add(PublishingGroupActorEntity.PrefetchPathUserData);
                subpath.SubPath.Add(PublishingGroupActorEntity.PrefetchPathPublishingGroupActorType);

                subpath = path.Add(PublishingGroupEntity.PrefetchPathPublishingGroupActorLabels);
                if ((instructions & PublishingGroupLoadInstruction.Labels) == PublishingGroupLoadInstruction.Labels)
                {
                    subpath.SubPath.Add(PublishingGroupActorLabelEntity.PrefetchPathLabel);
                    subpath.SubPath.Add(PublishingGroupActorLabelEntity.PrefetchPathPublishingGroupActorType);
                }
            }

            if ((instructions & PublishingGroupLoadInstruction.Resources) == PublishingGroupLoadInstruction.Resources)
            {
                IPrefetchPathElement2 subPath = path.Add(PublishingGroupEntity.PrefetchPathPublishingGroupResources);
                subPath.SubPath.Add(PublishingGroupResourceEntity.PrefetchPathResource);

                if ((instructions & PublishingGroupLoadInstruction.ResourceData) == PublishingGroupLoadInstruction.ResourceData)
                {
                    subPath.SubPath.Add(PublishingGroupResourceEntity.PrefetchPathPublishingGroupResourceData);
                }

                if ((instructions & PublishingGroupLoadInstruction.ResourceTickets) == PublishingGroupLoadInstruction.ResourceTickets)
                {
                    subPath.SubPath.Add(PublishingGroupResourceEntity.PrefetchPathPublicPublishingGroupResources);
                }
            }

            return path;
        }

        public IEnumerable<PublishingGroupDTO> GetAll(PublishingGroupLoadInstruction instructions = PublishingGroupLoadInstruction.None)
        {
            EntityCollection<PublishingGroupEntity> result = new EntityCollection<PublishingGroupEntity>();

            PrefetchPath2 path = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(), 0, null, path);
            }

            return result.Select(r => r.ToDTO());
        }

        public IEnumerable<PublishingGroupDTO> GetByAllowedForUserContext(PublishingGroupLoadInstruction instructions = PublishingGroupLoadInstruction.None)
        {
            EntityCollection<PublishingGroupEntity> result = new EntityCollection<PublishingGroupEntity>();

            PrefetchPath2 path = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(new PredicateExpression(PublishingGroupFields.AllowUseAsUserContext == true)), 0, null, path);
            }

            return result.Select(r => r.ToDTO());
        }

        public IEnumerable<PublishingGroupDTO> GetByAllowedForDashboard(PublishingGroupLoadInstruction instructions = PublishingGroupLoadInstruction.None)
        {
            var path = BuildPrefetchPath(instructions);
            using (var adapter = _fnAdapter())
            {
                var data = new LinqMetaData(adapter);
                var resultset = (from p in data.PublishingGroup
                                 where p.AllowUseAsDashboard
                                 select p).WithPath(path);
                return ((ILLBLGenProQuery)resultset).Execute<EntityCollection<PublishingGroupEntity>>().Select(r => r.ToDTO());
            }
        }


        public PublishingGroupDTO GetById(int id, PublishingGroupLoadInstruction instructions = PublishingGroupLoadInstruction.None)
        {
            PublishingGroupEntity result = new PublishingGroupEntity(id);

            PrefetchPath2 path = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntity(result, path);
            }

            return result.IsNew == true ? null : result.ToDTO();
        }

        public IEnumerable<PublishingGroupDTO> GetForLabels(Guid[] labels, PublishingGroupActorType type, PublishingGroupLoadInstruction instructions = PublishingGroupLoadInstruction.None)
        {
            PredicateExpression innerExpression = new PredicateExpression(PublishingGroupActorLabelFields.LabelId == labels);
            innerExpression.AddWithAnd(new FieldCompareSetPredicate(PublishingGroupActorLabelFields.PublishingTypeId, null, PublishingGroupActorTypeFields.Id, null, SetOperator.In, PublishingGroupActorTypeFields.Name == type.ToString()));
            // we need select * from pg where id in (select labelid from pgl where labelid in (lables))
            PredicateExpression expression = new PredicateExpression(new FieldCompareSetPredicate(PublishingGroupFields.Id, null, PublishingGroupActorLabelFields.PublishingGroupId, null, SetOperator.In, innerExpression));

            PrefetchPath2 path = BuildPrefetchPath(instructions);

            EntityCollection<PublishingGroupEntity> result = new EntityCollection<PublishingGroupEntity>();

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), 0, null, path);
            }

            return result.Select(r => r.ToDTO());
        }

        public PublishingGroupDTO GetByName(string name, PublishingGroupLoadInstruction instructions = PublishingGroupLoadInstruction.None)
        {
            EntityCollection<PublishingGroupEntity> result = new EntityCollection<PublishingGroupEntity>();

            PredicateExpression expression = new PredicateExpression(PublishingGroupFields.Name == name);

            PrefetchPath2 path = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), 1, null, path);
            }

            return result.Count == 0 ? null : result[0].ToDTO();
        }

        public IEnumerable<PublishingGroupDTO> GetLikeName(string name)
        {
            using (var adapter = _fnAdapter())
            {
                var predicate = new PredicateExpression(PublishingGroupFields.Name % name);
                var result = new EntityCollection<PublishingGroupEntity>();
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(predicate));
                return result.Select(r => r.ToDTO());
            }
        }

        public PublishingGroupDTO Save(PublishingGroupDTO publish, bool refetch, bool recurse)
        {
            var entity = publish.ToEntity();
            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(entity, refetch, recurse);
            }
            return entity.ToDTO();
        }

        public void Remove(int id)
        {
            PredicateExpression expression = new PredicateExpression(PublishingGroupFields.Id == id);

            using (var adapter = _fnAdapter())
            {
                adapter.DeleteEntitiesDirectly("PublishingGroupEntity", new RelationPredicateBucket(expression));
            }
        }

        public IEnumerable<PublishingGroupDTO> GetGroupsUserCanDo(UserDataDTO member, PublishingGroupActorType actorType)
        {
            // first get the groups that the user is directly tied to
            var actors = _publishingGroupActorRepo.GetForMembershipAndType(member.UserId, actorType,
                PublishingGroupActorLoadInstruction.PublishingGroup | PublishingGroupActorLoadInstruction.Resource);

            var actorGroups = from a in actors select a.PublishingGroup;

            // now we need to get the groups the user is tied to via any labels they are associated with
            var labelGroups = GetForLabels((from l in member.LabelUsers select l.LabelId).ToArray(),
                PublishingGroupActorType.Reviewer, PublishingGroupLoadInstruction.Resources);

            // we want everything we don't already have
            var newGroups = actorGroups.Union(labelGroups);

            return newGroups.ToList();
        }

        public IEnumerable<PublishingGroupDTO> GetGroupsUserCanDo(Guid userId, PublishingGroupActorType actorType)
        {
            return GetGroupsUserCanDo(_membershipRepo.GetById(userId, MembershipLoadInstructions.Label |
                MembershipLoadInstructions.MembershipLabel), actorType);
        }

        public IEnumerable<PublishingGroupDTO> GetGroupsUserCanDo(string userName, PublishingGroupActorType actorType)
        {
            return GetGroupsUserCanDo(_membershipRepo.GetByUsername(userName, MembershipLoadInstructions.Label |
                MembershipLoadInstructions.MembershipLabel), actorType);
        }

        public bool UserIsAllowedGroup(UserDataDTO member, PublishingGroupDTO groupEnt, PublishingGroupActorType type)
        {
            using (var adapter = _fnAdapter())
            {
                var data = new LinqMetaData(adapter);

                //try explicit user first
                var allowedRecord = (from a in data.PublishingGroupActor
                                     where a.UserId == member.UserId && a.PublishingGroupActorType.Name == type.ToString() &&
                                        a.PublishingGroupId == groupEnt.Id
                                     select a).WithPath(p => p.Prefetch(a => a.PublishingGroupActorType));
                if (allowedRecord.Any())
                    return true;
                //otherwise try labels
                var allowedRecords = (from l in data.PublishingGroupActorLabel
                                      where l.PublishingGroupId == groupEnt.Id &&
                                        member.LabelUsers.Select(lbl => lbl.LabelId).Contains(l.LabelId) &&
                                        l.PublishingGroupActorType.Name == type.ToString()
                                      select l).WithPath(p => p.Prefetch(l => l.PublishingGroupActorType));
                return allowedRecords.Any();
            }
        }

        public bool UserIsAllowedGroup(string userName, int groupId, PublishingGroupActorType type)
        {
            var member = _membershipRepo.GetByUsername(userName, MembershipLoadInstructions.MembershipLabel);
            var group = GetById(groupId, PublishingGroupLoadInstruction.Actors | PublishingGroupLoadInstruction.Labels);
            return UserIsAllowedGroup(member, group, type);
        }

        public bool UserIsAllowedGroup(string userName, PublishingGroupDTO group, PublishingGroupActorType type)
        {
            var member = _membershipRepo.GetByUsername(userName, MembershipLoadInstructions.MembershipLabel);
            return UserIsAllowedGroup(member, group, type);
        }

        //Get all reviewees or reviewers for group
        public IEnumerable<UserDataDTO> GetUsersForGroup(int groupId, PublishingGroupActorType type)
        {
            using (var adapter = _fnAdapter())
            {
                var data = new LinqMetaData(adapter);

                var usersQuery = from g in data.PublishingGroup
                                 from a in data.PublishingGroupActor
                                 from at in data.PublishingGroupActorType
                                 where at.Name == type.ToString() &&
                                       a.PublishingTypeId == at.Id &&
                                       g.Id == groupId &&
                                       a.PublishingGroupId == g.Id
                                 select a.UserId;

                var labelsQuery = from g in data.PublishingGroup
                                  from lu in data.LabelUser
                                  from a in data.PublishingGroupActorLabel
                                  from at in data.PublishingGroupActorType
                                  where g.Id == groupId &&
                                        a.PublishingGroupId == g.Id &&
                                        at.Name == type.ToString() &&
                                        a.PublishingTypeId == at.Id &&
                                        lu.LabelId == a.LabelId
                                  select lu.UserId;

                //Need to evaluate them, because LLBLGen is braindead and can't do a LINQ union on the DB
                var ids = usersQuery.ToList().Union(labelsQuery);
                var resultQuery = from u in data.UserData
                                  where ids.Contains(u.UserId)
                                  select u;
                var res = resultQuery.WithPath(p => p.Prefetch(u => u.AspNetUser)).ToList();
                return res.Select(e => e.ToDTO()).ToList();
            }
        }

        public IEnumerable<PublishingGroupDTO> GetByAllowedForTaskWorkflows(PublishingGroupLoadInstruction instructions)
        {
            var path = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                var data = new LinqMetaData(adapter);
                var resultset = (from p in data.PublishingGroup
                                 where p.AllowUseAsTaskWorkflow
                                 select p).WithPath(path);

                return ((ILLBLGenProQuery)resultset).Execute<EntityCollection<PublishingGroupEntity>>().Select(e => e.ToDTO());
            }
        }

        public IEnumerable<PublishingGroupDTO> GetByAllowedForAssetContext(PublishingGroupLoadInstruction instructions)
        {
            var path = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                var data = new LinqMetaData(adapter);
                var resultset = (from p in data.PublishingGroup
                                 where p.AllowUseAsAssetContext
                                 select p).WithPath(path);

                return ((ILLBLGenProQuery)resultset).Execute<EntityCollection<PublishingGroupEntity>>().Select(e => e.ToDTO());
            }
        }

    }
}
