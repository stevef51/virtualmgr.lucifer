﻿using ConceptCave.Data;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Data.DTOConverters;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualMgr.Central;
using ConceptCave.Data.Linq;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Data.DatabaseSpecific;

namespace ConceptCave.Repository.Injected
{
    public class OInvoiceRepository : IInvoiceRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public OInvoiceRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }

        private PrefetchPath2 BuildPrefetchPath(InvoiceLoadInstructions instructions)
        {
            PrefetchPath2 path = new PrefetchPath2(EntityType.InvoiceEntity);
            if ((instructions & InvoiceLoadInstructions.LineItem) != 0)
            {
                path.Add(InvoiceEntity.PrefetchPathInvoiceLineItems);
            }
            return path;
        }

        private PrefetchPath2 BuildPrefetchPath(InvoiceLineItemLoadInstructions instructions)
        {
            PrefetchPath2 path = new PrefetchPath2(EntityType.InvoiceLineItemEntity);
            if ((instructions & InvoiceLineItemLoadInstructions.Invoice) != 0)
            {
                path.Add(InvoiceLineItemEntity.PrefetchPathInvoice);
            }
            return path;
        }

        public InvoiceDTO GetInvoiceById(string id, InvoiceLoadInstructions instructions = InvoiceLoadInstructions.None)
        {
            InvoiceEntity result = new InvoiceEntity(id);

            var path = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntity(result, path);
            }

            return result.IsNew == true ? null : result.ToDTO();
        }

        public InvoiceLineItemDTO GetLineItemById(int id, InvoiceLineItemLoadInstructions instructions = InvoiceLineItemLoadInstructions.None)
        {
            InvoiceLineItemEntity result = new InvoiceLineItemEntity(id);

            var path = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntity(result, path);
            }

            return result.IsNew == true ? null : result.ToDTO();
        }

        public InvoiceDTO SaveInvoice(DTO.DTOClasses.InvoiceDTO dto, bool refetch)
        {
            var e = dto.ToEntity();

            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(e, refetch);
            }

            return refetch ? e.ToDTO() : dto;
        }

        public InvoiceDTO MarkAsUnpaid(string id)
        {
            InvoiceEntity entity = new InvoiceEntity(id);

            using (var adapter = _fnAdapter())
            {
                if (!adapter.FetchEntity(entity))
                    return null;

                entity.SetNewFieldValue((int)InvoiceFieldIndex.PaymentReference, null);
                entity.SetNewFieldValue((int)InvoiceFieldIndex.PaymentDateUtc, null);

                adapter.SaveEntity(entity, true);
            }

            return entity.ToDTO();
        }

        public InvoiceDTO GetLastInvoice(InvoiceLoadInstructions instructions = InvoiceLoadInstructions.None)
        {
            using (var adapter = _fnAdapter())
            {
                LinqMetaData lmd = new LinqMetaData(adapter);
                var invoice = (from i in lmd.Invoice orderby i.ToDateUtc descending select i).FirstOrDefault();
                if (invoice == null)
                    return null;
                return invoice.ToDTO();
            }
        }

        public InvoiceDTO FindInvoiceCovering(DateTimeOffset date)
        {
            date = date.ToUniversalTime();
            using (var adapter = _fnAdapter())
            {
                LinqMetaData lmd = new LinqMetaData(adapter);
                var item = (from i in lmd.Invoice where i.FromDateUtc < date && i.ToDateUtc > date select i).FirstOrDefault();
                return item != null ? item.ToDTO() : null;
            }
        }

        public IEnumerable<InvoiceDTO> Search(string search, int? pageNumber, int? itemsPerPage, bool includeArchived, ref int totalItems)
        {
            using (var adapter = _fnAdapter())
            {
                LinqMetaData lmd = new LinqMetaData(adapter);
                var items = from e in lmd.Invoice where e.Description.Contains(search) orderby e.GeneratedDateUtc descending select e.ToDTO();

                totalItems = items.Count();
                if (pageNumber.HasValue && itemsPerPage.HasValue)
                    items = items.Skip((pageNumber.Value - 1) * itemsPerPage.Value).Take(itemsPerPage.Value);
                return items.ToList();
            }
        }

        public IEnumerable<InvoiceDTO> FindNew()
        {
            using (var adapter = _fnAdapter())
            {
                LinqMetaData lmd = new LinqMetaData(adapter);
                var items = from e in lmd.Invoice where e.IssuedDateUtc == null orderby e.GeneratedDateUtc descending select e.ToDTO();

                return items.ToList();
            }
        }

        public bool DeleteInvoice(string id)
        {
            PredicateExpression expression = new PredicateExpression(InvoiceFields.Id == id);

            using (var adapter = _fnAdapter())
            {
                return adapter.DeleteEntitiesDirectly("InvoiceEntity", new RelationPredicateBucket(expression)) > 0;
            }
        }
    }
}
