﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Data;
using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.HelperClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using SD.LLBLGen.Pro.ORMSupportClasses;
using ConceptCave.Data.DTOConverters;
using ConceptCave.Checklist;
using ConceptCave.DTO.DTOClasses;
using VirtualMgr.Central;

namespace ConceptCave.Repository.Injected
{
    public class OCompanyRepository : ICompanyRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public OCompanyRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }

        public static PrefetchPath2 BuildPrefetchPath(CompanyLoadInstructions instructions)
        {
            PrefetchPath2 path = new PrefetchPath2((int)EntityType.CompanyEntity);

            if ((instructions & CompanyLoadInstructions.CountryState) == CompanyLoadInstructions.CountryState)
            {
                path.Add(CompanyEntity.PrefetchPathCountryState);
            }

            return path;
        }

        public IList<CompanyDTO> GetAll(CompanyLoadInstructions instructions)
        {
            EntityCollection<CompanyEntity> result = new EntityCollection<CompanyEntity>();

            PrefetchPath2 path = BuildPrefetchPath(instructions);

            SortExpression sort = new SortExpression();

            sort.Add(CompanyFields.Name | SortOperator.Ascending);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(), 0, sort, path);
            }

            return result.ToList().Select(e => e.ToDTO()).ToList();
        }

        public IList<DTO.DTOClasses.CompanyDTO> Search(string text, int page, int count, CompanyLoadInstructions instructions)
        {
            EntityCollection<CompanyEntity> result = new EntityCollection<CompanyEntity>();

            PredicateExpression expression = new PredicateExpression();

            if (string.IsNullOrEmpty(text) == false)
            {
                expression.Add(CompanyFields.Name % text);
            }

            SortExpression sort = new SortExpression();

            sort.Add(CompanyFields.Name | SortOperator.Ascending);

            PrefetchPath2 path = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), 0, sort, path, page, count);
            }

            return result.ToList().Select(e => e.ToDTO()).ToList();
        }

        public DTO.DTOClasses.CompanyDTO GetById(Guid id, RepositoryInterfaces.Enums.CompanyLoadInstructions instructions)
        {
            CompanyEntity result = new CompanyEntity(id);

            PrefetchPath2 path = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntity(result, path);
            }

            return result.IsNew == true ? null : result.ToDTO();
        }

        public DTO.DTOClasses.CompanyDTO Save(DTO.DTOClasses.CompanyDTO company, bool refetch, bool recurse)
        {
            var e = company.ToEntity();

            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(e, refetch, recurse);
            }

            return refetch ? e.ToDTO() : company;
        }

        public DTO.DTOClasses.CompanyDTO New(string name)
        {
            CompanyEntity company = new CompanyEntity();

            company.Id = CombFactory.NewComb();
            company.Name = name;

            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(company, true, true);
            }

            return company.ToDTO();
        }
    }
}
