﻿using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.HelperClasses;
using ConceptCave.RepositoryInterfaces.Repositories;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualMgr.Central;
using ConceptCave.Data.DTOConverters;
using ConceptCave.Data.DatabaseSpecific;

namespace ConceptCave.Repository.Injected
{
    public class OHierarchyBucketOverrideTypeRepository : IHierarchyBucketOverrideTypeRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public OHierarchyBucketOverrideTypeRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }

        public IList<DTO.DTOClasses.HierarchyBucketOverrideTypeDTO> GetAll()
        {
            EntityCollection<HierarchyBucketOverrideTypeEntity> result = new EntityCollection<HierarchyBucketOverrideTypeEntity>();

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket());
            }

            return result.ToList().Select(r => r.ToDTO()).ToList();
        }

        public DTO.DTOClasses.HierarchyBucketOverrideTypeDTO GetById(int id)
        {
            HierarchyBucketOverrideTypeEntity result = new HierarchyBucketOverrideTypeEntity(id);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntity(result);
            }

            return result.IsNew == true ? null : result.ToDTO();
        }

        public DTO.DTOClasses.HierarchyBucketOverrideTypeDTO Save(DTO.DTOClasses.HierarchyBucketOverrideTypeDTO dto, bool refetch, bool recurse)
        {
            var e = dto.ToEntity();

            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(e, refetch, recurse);
            }

            return refetch ? e.ToDTO() : dto;
        }
    }
}
