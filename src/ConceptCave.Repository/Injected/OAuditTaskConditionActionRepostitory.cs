﻿using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.HelperClasses;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Repositories;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Data.DTOConverters;
using VirtualMgr.Central;

namespace ConceptCave.Repository.Injected
{
    public class OAuditTaskConditionActionRepository : IAuditTaskConditionActionRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public OAuditTaskConditionActionRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }

        public IList<AuditTaskConditionActionDTO> GetByIds(Guid[] scheduleIds, string group)
        {
            PredicateExpression expression = new PredicateExpression(AuditTaskConditionActionFields.ProjectJobScheduledTaskId == scheduleIds & AuditTaskConditionActionFields.Group == group);

            EntityCollection<AuditTaskConditionActionEntity> result = new EntityCollection<AuditTaskConditionActionEntity>();

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression));
            }

            return result.Select(r => r.ToDTO()).ToList();
        }

        public DTO.DTOClasses.AuditTaskConditionActionDTO Save(DTO.DTOClasses.AuditTaskConditionActionDTO dto, bool refetch, bool recurse)
        {
            var e = dto.ToEntity();
            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(e, refetch, recurse);
            }

            return refetch ? e.ToDTO() : dto;
        }
    }
}
