﻿using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.DTOConverters;
using ConceptCave.RepositoryInterfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualMgr.Central;
using SD.LLBLGen.Pro.ORMSupportClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.Data;
using ConceptCave.Data.Linq;
using ConceptCave.Data.HelperClasses;
using ConceptCave.DTO.DTOClasses;
using SD.LLBLGen.Pro.LinqSupportClasses;
using ConceptCave.Data.DatabaseSpecific;

namespace ConceptCave.Repository.Injected
{
    public class OAssetRepository : IAssetRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public OAssetRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }
        private PrefetchPath2 BuildPrefetchPath(AssetLoadInstructions instructions)
        {
            PrefetchPath2 path = new PrefetchPath2(EntityType.AssetEntity);
            if ((instructions & AssetLoadInstructions.AssetType) != 0)
            {
                var a = path.Add(AssetEntity.PrefetchPathAssetType);

                if ((instructions & AssetLoadInstructions.AssetTypeContextPublishedResource) != 0)
                {
                    a.SubPath.Add(AssetTypeEntity.PrefetchPathAssetTypeContextPublishedResources).SubPath.Add(AssetTypeContextPublishedResourceEntity.PrefetchPathPublishingGroupResource);
                }
            }

            if ((instructions & AssetLoadInstructions.Beacon) != 0)
            {
                path.Add(AssetEntity.PrefetchPathBeacon);
            }

            if ((instructions & AssetLoadInstructions.ContextData) != 0)
            {
                var sp = path.Add(AssetEntity.PrefetchPathAssetContextDatas);

                if ((instructions & AssetLoadInstructions.PublishingGroupResource) != 0)
                {
                    var sps = sp.SubPath.Add(AssetContextDataEntity.PrefetchPathAssetTypeContextPublishedResource);
                    var ats = sps.SubPath.Add(AssetTypeContextPublishedResourceEntity.PrefetchPathPublishingGroupResource);

                    if ((instructions & AssetLoadInstructions.PublishingGroupResourceData) != 0)
                    {
                        ats.SubPath.Add(PublishingGroupResourceEntity.PrefetchPathPublishingGroupResourceData);
                    }
                }
            }

            if ((instructions & AssetLoadInstructions.Schedule) != 0)
            {
                var sub = path.Add(AssetEntity.PrefetchPathAssetSchedules);
                if ((instructions & AssetLoadInstructions.ScheduleCalendar) != 0)
                {
                    sub.SubPath.Add(AssetScheduleEntity.PrefetchPathAssetTypeCalendar);
                }
            }
            return path;
        }

        public DTO.DTOClasses.AssetDTO GetById(int id, AssetLoadInstructions instructions = AssetLoadInstructions.None)
        {
            var e = new AssetEntity(id);
            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntity(e, BuildPrefetchPath(instructions));
            }
            return e.IsNew ? null : e.ToDTO();
        }

        public DTO.DTOClasses.AssetDTO SaveAsset(DTO.DTOClasses.AssetDTO dto, bool refetch, bool recurse)
        {
            var e = dto.__IsNew ? new AssetEntity() : new AssetEntity(dto.Id);

            using (var adapter = _fnAdapter())
            {
                if (!dto.__IsNew)
                {
                    adapter.FetchEntity(e);
                }

                dto.ToEntity(e);

                adapter.SaveEntity(e, refetch, recurse);
            }

            return refetch ? e.ToDTO() : dto;
        }

        public IList<DTO.DTOClasses.AssetDTO> Search(string nameLike, int? pageNumber, int? itemsPerPage, bool includeArchived, ref int totalItems)
        {
            using (var adapter = _fnAdapter())
            {
                var lmd = new LinqMetaData(adapter);
                var items = from r in lmd.Asset where r.Name == null || r.Name != null && r.Name.Contains(nameLike) select r.ToDTO();
                items = items.OrderBy(i => i.Name);

                if (!includeArchived)
                    items = items.Where(i => i.Archived == false);

                totalItems = items.Count();
                if (pageNumber.HasValue && itemsPerPage.HasValue)
                    items = items.Skip((pageNumber.Value - 1) * itemsPerPage.Value).Take(itemsPerPage.Value);
                return items.ToList();
            }
        }


        public void Delete(int id, bool archiveIfRequired, out bool archived)
        {
            using (var adapter = _fnAdapter())
            {
                var entity = new AssetEntity(id);
                archived = false;
                try
                {
                    adapter.DeleteEntity(entity);
                }
                catch (Exception ex)
                {
                    if (archiveIfRequired)
                    {
                        adapter.FetchEntity(entity);
                        entity.Archived = true;
                        adapter.SaveEntity(entity, false);
                        archived = true;
                    }
                    else
                        throw ex;
                }
            }
        }

        public AssetContextDataDTO CreateOrUpdateAssetCtxEntry(int assetId, int publishingGroupResourceId, Guid workingDocumentId)
        {
            using (var adapter = _fnAdapter())
            {
                var data = new LinqMetaData(adapter);
                //Need to get the usertypecontext id associated with this resourceId
                var typectx = (from u in data.Asset
                               where u.Id == assetId
                               join ctxj in data.AssetTypeContextPublishedResource on u.AssetTypeId equals ctxj.AssetTypeId
                               where ctxj.PublishingGroupResourceId == publishingGroupResourceId
                               select ctxj.Id).Single();

                var e = new AssetContextDataEntity(assetId, typectx, workingDocumentId);

                if (!adapter.FetchEntity(e))
                    e = new AssetContextDataEntity(assetId, typectx, workingDocumentId);

                adapter.SaveEntity(e, true, false);

                return e.ToDTO();
            }
        }

        public IList<AssetDTO> GetByAssetType(int assetTypeId, AssetLoadInstructions instructions = AssetLoadInstructions.None)
        {
            using (var adapter = _fnAdapter())
            {
                var predicate = new PredicateExpression(AssetFields.AssetTypeId == assetTypeId);
                var path = BuildPrefetchPath(instructions);

                var records = new EntityCollection<AssetEntity>();

                adapter.FetchEntityCollection(records, new RelationPredicateBucket(predicate), 0, null, path);

                return records.Select(r => r.ToDTO()).ToList();
            }
        }

        public IList<Tuple<AssetDTO, AssetTypeCalendarDTO>> GetAssetsWithMissingCalendarSchedules()
        {
            using (var adapter = _fnAdapter())
            {
                // Find Assets that are missing Schedules for the Calendars specified by the Assets Asset Type
                // This may occur if 
                // 1. a new Asset is added
                // 2. a new Calendar is added to an existing Asset Type                
                var lmd = new LinqMetaData(adapter);
                var items = from asset in lmd.Asset
                            join atc in lmd.AssetTypeCalendar on asset.AssetTypeId equals atc.AssetTypeId into assetAtcJoin
                            from atc in assetAtcJoin.DefaultIfEmpty()
                            join assetSchedule in lmd.AssetSchedule on new { assetId = asset.Id, calendarId = atc.Id } equals new { assetId = assetSchedule.AssetId, calendarId = assetSchedule.AssetTypeCalendarId } into resultJoin
                            from assetSchedule in resultJoin.DefaultIfEmpty()
                            where asset.Archived == false && atc.Archived == false && assetSchedule == null
                            select new { Asset = asset.ToDTO(), Calendar = atc.ToDTO() };

                var records = items.ToList();

                return records.Select(r => new Tuple<AssetDTO, AssetTypeCalendarDTO>(r.Asset, r.Calendar)).ToList();
            }

        }
    }
}
