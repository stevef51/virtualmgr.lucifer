﻿using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.DTOConverters;
using ConceptCave.RepositoryInterfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualMgr.Central;
using SD.LLBLGen.Pro.ORMSupportClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.Data;
using ConceptCave.Data.Linq;
using ConceptCave.Data.HelperClasses;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Data.DatabaseSpecific;

namespace ConceptCave.Repository.Injected
{
    public class OGpsLocationRepository : IGpsLocationRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public OGpsLocationRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }

        private PrefetchPath2 BuildPrefetchPath(GpsLocationLoadInstructions instructions)
        {
            PrefetchPath2 path = new PrefetchPath2(EntityType.GpsLocationEntity);
            return path;
        }

        public GpsLocationDTO GetById(long id, GpsLocationLoadInstructions instructions = GpsLocationLoadInstructions.None)
        {
            var e = new GpsLocationEntity(id);
            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntity(e, BuildPrefetchPath(instructions));
            }
            return e.IsNew ? null : e.ToDTO();
        }

        public GpsLocationDTO Save(GpsLocationDTO dto, bool refetch, bool recurse)
        {
            using (var adapter = _fnAdapter())
            {
                var e = dto.ToEntity();
                adapter.SaveEntity(e, refetch, recurse);
                return refetch ? e.ToDTO() : dto;
            }
        }

        public GpsLocationDTO FindLastGPSLocation(GpsLocationType locationType, string tabletUUID)
        {
            using (var adapter = _fnAdapter())
            {
                var lmd = new LinqMetaData(adapter);
                var query = from r in lmd.GpsLocation
                            where
                                r.LocationType == (int)locationType &&
                                r.TabletUuid == tabletUUID
                            orderby
                                r.Id descending
                            select r.ToDTO();
                return query.FirstOrDefault();
            }
        }


        public void Delete(long id, bool archiveIfRequired, out bool archived)
        {
            using (var adapter = _fnAdapter())
            {
                var entity = new GpsLocationEntity(id);
                archived = false;
                try
                {
                    adapter.DeleteEntity(entity);
                }
                catch (Exception ex)
                {
                    if (archiveIfRequired)
                    {
                        adapter.FetchEntity(entity);
                        entity.Archived = true;
                        adapter.SaveEntity(entity, false);
                        archived = true;
                    }
                    else
                        throw ex;
                }
            }
        }
    }
}
