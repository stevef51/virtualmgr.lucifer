﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using ConceptCave.Data;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.HelperClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using SD.LLBLGen.Pro.ORMSupportClasses;
using VirtualMgr.MultiTenant;

namespace ConceptCave.Repository.Injected
{
    public class OGlobalSettingsRepository : IGlobalSettingsRepository
    {
        private readonly Func<Data.DatabaseSpecific.DataAccessAdapter> _fnAdapter;
        Func<VirtualMgr.Central.Data.DatabaseSpecific.DataAccessAdapter> _fnTenantMasterAdapter;
        private readonly AppTenant _appTenant;
        public OGlobalSettingsRepository(Func<Data.DatabaseSpecific.DataAccessAdapter> fnAdapter, Func<VirtualMgr.Central.Data.DatabaseSpecific.DataAccessAdapter> fnTenantMasterAdapter, AppTenant appTenant)
        {
            _fnAdapter = fnAdapter;
            _fnTenantMasterAdapter = fnTenantMasterAdapter;
            _appTenant = appTenant;
        }

        public T GetSetting<T>(string name)
        {
            // Try a setting specific to this tenant first, this allows a Production Db to be copied to a Staging tenant
            // and without modification the Staging tenant can use 
            var entity = GetSettingEntity($"{_appTenant.PrimaryHostname}:{name}");
            if (entity == null)
            {
                entity = GetSettingEntity(name);
            }
            return CastSettingResult<T>(entity);
        }

        public void SetSetting(string name, object value)
        {
            try
            {
                using (var tscope = new TransactionScope())
                {
                    using (var adapter = _fnAdapter())
                    {
                        //try read this name already
                        var entity = new GlobalSettingEntity();
                        entity.Name = name;
                        adapter.FetchEntityUsingUniqueConstraint(entity, entity.ConstructFilterForUCName());
                        entity.Value = value.ToString();
                        adapter.SaveEntity(entity);
                        tscope.Complete();
                    }
                }
            }
            catch (ORMEntityIsDeletedException)
            {
                //don't worry, someone else tried to save this at the same time. Life sucks sometimes
            }
            catch (ORMConcurrencyException) { }
            catch (ORMEntityOutOfSyncException) { }

        }


        private GlobalSettingEntity GetSettingEntity(string name)
        {
            PredicateExpression expression = new PredicateExpression(GlobalSettingFields.Name == name);

            EntityCollection<GlobalSettingEntity> result = new EntityCollection<GlobalSettingEntity>();

            PrefetchPath2 path = new PrefetchPath2((int)EntityType.GlobalSettingEntity);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), 1);
            }

            return result.Count == 0 ? null : result[0];
        }

        private GlobalSettingEntity GetTenantMasterSettingEntity(string name)
        {
            PredicateExpression expression = new PredicateExpression(GlobalSettingFields.Name == name);

            EntityCollection<GlobalSettingEntity> result = new EntityCollection<GlobalSettingEntity>();

            PrefetchPath2 path = new PrefetchPath2((int)EntityType.GlobalSettingEntity);

            using (var adapter = _fnTenantMasterAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), 1);
            }

            return result.Count == 0 ? null : result[0];
        }

        private GlobalSettingEntity GetSettingEntity(int id)
        {
            GlobalSettingEntity entity = new GlobalSettingEntity(id);

            using (var adapter = _fnAdapter())
            {
                return adapter.FetchEntity(entity) ? entity : null;
            }
        }

        private static T CastSettingResult<T>(GlobalSettingEntity entity)
        {
            if (entity == null || entity.Value == null)
                return default(T);
            //Try cast the common ones
            if (typeof(T) == typeof(string))
                return (T)(object)entity.Value;
            if (typeof(T) == typeof(bool) || typeof(T) == typeof(bool?))
                return (T)(object)bool.Parse(entity.Value);
            if (typeof(T) == typeof(Int32) || typeof(T) == typeof(Int32?))
                return (T)(object)Int32.Parse(entity.Value);
            if (typeof(T) == typeof(Int64) || typeof(T) == typeof(Int64?))
                return (T)(object)Int64.Parse(entity.Value);
            if (typeof(T) == typeof(UInt32) || typeof(T) == typeof(UInt32?))
                return (T)(object)UInt32.Parse(entity.Value);
            if (typeof(T) == typeof(UInt64) || typeof(T) == typeof(UInt64?))
                return (T)(object)UInt64.Parse(entity.Value);
            if (typeof(T) == typeof(decimal) || typeof(T) == typeof(decimal?))
                return (T)(object)decimal.Parse(entity.Value);
            if (typeof(T) == typeof(double) || typeof(T) == typeof(double?))
                return (T)(object)double.Parse(entity.Value);
            if (typeof(T) == typeof(float) || typeof(T) == typeof(float?))
                return (T)(object)float.Parse(entity.Value);
            if (typeof(T) == typeof(Guid) || typeof(T) == typeof(Guid?))
                return (T)(object)Guid.Parse(entity.Value);
            if (typeof(T) == typeof(DateTime) || typeof(T) == typeof(DateTime?))
                return (T)(object)DateTime.Parse(entity.Value);
            return (T)(object)entity.Value; //obviously confident that there is a string-to-T conversion!
        }

        /*        This is commented out since this part of the interface is only used by WWW Admin GlobalSettings page which will eventually be fully redone
                /// <summary>
                /// Retrieves the set of global settings that don't have constants defined for them
                /// </summary>
                /// <returns></returns>
                public EntityCollection<GlobalSettingEntity> GetCustomSettings()
                {
                    EntityCollection<GlobalSettingEntity> result = new EntityCollection<GlobalSettingEntity>();

                    PredicateExpression expression = new PredicateExpression();
                    expression.AddWithAnd(GlobalSettingFields.Name != GlobalSettingsConstants.SystemSetting_StoreLoginSite);
                    expression.AddWithAnd(GlobalSettingFields.Name != GlobalSettingsConstants.SystemSetting_StoreLoginCoordinates);
                    expression.AddWithAnd(GlobalSettingFields.Name != GlobalSettingsConstants.SystemSetting_ScheduledProcessEnabled);
                    expression.AddWithAnd(GlobalSettingFields.Name != GlobalSettingsConstants.SystemSetting_ScheduledProcessThreadCount);
                    expression.AddWithAnd(GlobalSettingFields.Name != GlobalSettingsConstants.SystemSetting_SystemName);
                    expression.AddWithAnd(GlobalSettingFields.Name != GlobalSettingsConstants.SystemSetting_ThemeMediaId);
                    expression.AddWithAnd(GlobalSettingFields.Name != GlobalSettingsConstants.SystemSetting_FavIconId);
                    expression.AddWithAnd(GlobalSettingFields.Name != GlobalSettingsConstants.SystemSetting_SiteId);
                    expression.AddWithAnd(GlobalSettingFields.Name != GlobalSettingsConstants.SystemSetting_UTCLastModified);

                    expression.AddWithAnd(GlobalSettingFields.Hidden == 0);

                    using (var adapter = _fnAdapter())
                    {
                        adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression));
                    }

                    return result;
                }

                public static T GetTenantMasterSetting<T>(string name)
                {
                    var entity = GetTenantMasterSettingEntity(name);
                    return CastSettingResult<T>(entity);
                }

                public static void Save(GlobalSettingEntity entity, bool refetch, bool recurse)
                {
                    using (var adapter = _fnAdapter())
                    {
                        adapter.SaveEntity(entity, refetch, recurse);
                    }
                }

                public static void Delete(int id)
                {
                    PredicateExpression expression = new PredicateExpression(GlobalSettingFields.Id == id);

                    using (var adapter = _fnAdapter())
                    {
                        adapter.DeleteEntitiesDirectly("GlobalSettingEntity", new RelationPredicateBucket(expression));
                    }
                }

                public static void SetSetting(string name, object value)
                {
                }

                /// <summary>
                /// Just does a simple tests against the database to make sure we can connect to it
                /// </summary>
                /// <returns></returns>
                public static bool SystemHealthCheck()
                {
                    try
                    {
                        using (var adapter = _fnAdapter())
                        {
                            adapter.OpenConnection();
                        }

                        return true;
                    }
                    catch (Exception e)
                    {
                        return false;
                    }
                }
        */
    }
}
