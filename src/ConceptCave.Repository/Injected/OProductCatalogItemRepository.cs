﻿using ConceptCave.RepositoryInterfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.DTO.DTOClasses;
using VirtualMgr.Central;
using ConceptCave.Data.Linq;
using ConceptCave.Data.DTOConverters;
using ConceptCave.RepositoryInterfaces.Enums;
using SD.LLBLGen.Pro.LinqSupportClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using ConceptCave.Data;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Data.DatabaseSpecific;

namespace ConceptCave.Repository.Injected
{
    public class OProductCatalogItemRepository : IProductCatalogItemRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public OProductCatalogItemRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }

        private PrefetchPath2 BuildPrefetchPath(ProductCatalogItemLoadInstructions instructions)
        {
            PrefetchPath2 path = new PrefetchPath2((int)EntityType.ProductCatalogItemEntity);

            if ((instructions & ProductCatalogItemLoadInstructions.Product) == ProductCatalogItemLoadInstructions.Product)
            {
                path.Add(ProductCatalogItemEntity.PrefetchPathProduct);
            }

            return path;
        }
        public ProductCatalogItemDTO GetById(int productId, int catalogId, ProductCatalogItemLoadInstructions instructions)
        {
            using (var adapter = _fnAdapter())
            {
                var data = new LinqMetaData(adapter);
                return (from prd in data.ProductCatalogItem
                        where prd.ProductId == productId && prd.ProductCatalogId == catalogId
                        select prd).WithPath(BuildPrefetchPath(instructions)).FirstOrDefault().ToDTO();
            }
        }

        public ProductCatalogItemDTO Save(ProductCatalogItemDTO dto, bool refetch)
        {
            using (var adapter = _fnAdapter())
            {
                var e = dto.ToEntity();
                adapter.SaveEntity(e, refetch, false);
                return refetch ? e.ToDTO() : dto;
            }
        }

        public void Remove(int productId, int catalogId)
        {
            PredicateExpression expression = new PredicateExpression(ProductCatalogItemFields.ProductId == productId & ProductCatalogItemFields.ProductCatalogId == catalogId);

            using (var adapter = _fnAdapter())
            {
                adapter.DeleteEntitiesDirectly(typeof(ProductCatalogItemEntity), new RelationPredicateBucket(expression));
            }
        }
        public void DeleteFromCatalog(int catalogId)
        {
            PredicateExpression expression = new PredicateExpression(ProductCatalogItemFields.ProductCatalogId == catalogId);

            using (var adapter = _fnAdapter())
            {
                adapter.DeleteEntitiesDirectly(typeof(ProductCatalogItemEntity), new RelationPredicateBucket(expression));
            }
        }
    }
}
