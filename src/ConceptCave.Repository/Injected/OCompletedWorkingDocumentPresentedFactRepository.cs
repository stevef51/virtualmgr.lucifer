﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using ConceptCave.Data;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.Data.DTOConverters;
using ConceptCave.RepositoryInterfaces.Repositories;

namespace ConceptCave.Repository.Injected
{
    public class OCompletedWorkingDocumentPresentedFactRepository : ICompletedWorkingDocumentPresentedFactRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public OCompletedWorkingDocumentPresentedFactRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }

        public void Save(IEnumerable<CompletedWorkingDocumentPresentedFactDTO> dtos, bool refetch, bool recurse)
        {
            var collection = new EntityCollection<CompletedWorkingDocumentPresentedFactEntity>(dtos.Select(dto => dto.ToEntity()));
            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntityCollection(collection, refetch, recurse);
            }
        }

        private static PrefetchPath2 GetBasicPath(CompletedWorkingDocumentPresentedFactLoadInstructions instructions)
        {
            PrefetchPath2 path = new PrefetchPath2((int)EntityType.CompletedWorkingDocumentPresentedFactEntity);

            if ((instructions & CompletedWorkingDocumentPresentedFactLoadInstructions.CompletedPresentedFactValue) == CompletedWorkingDocumentPresentedFactLoadInstructions.CompletedPresentedFactValue)
            {
                path.Add(CompletedWorkingDocumentPresentedFactEntity.PrefetchPathCompletedPresentedFactValues);
            }

            if ((instructions & CompletedWorkingDocumentPresentedFactLoadInstructions.CompletedUploadMedia) == CompletedWorkingDocumentPresentedFactLoadInstructions.CompletedUploadMedia)
            {
                path.Add(CompletedWorkingDocumentPresentedFactEntity.PrefetchPathCompletedPresentedUploadMediaFactValues);
            }

            return path;
        }

        private static PrefetchPath2 GetPathWithQuestionReportDataProviders()
        {
            PrefetchPath2 path = GetBasicPath(CompletedWorkingDocumentPresentedFactLoadInstructions.CompletedPresentedFactValue
                | CompletedWorkingDocumentPresentedFactLoadInstructions.CompletedUploadMedia);
            //we don't do this anymore
            //we just load up the whole checklist with all special question answer tables

            /*foreach (var ap in RepositorySectionManager.Current.ItemsWithUserContextAnswerPopulater())
            {
                var instance = (IQuestionReportDataProvider)ap.CreateAnswerPopulater();
                instance.BuildPath(path);
            }*/

            return path;
        }

        public IList<CompletedWorkingDocumentPresentedFactDTO> Get(Guid workingDocumentId, CompletedWorkingDocumentPresentedFactLoadInstructions instructions)
        {
            PrefetchPath2 path = GetBasicPath(instructions);
            PredicateExpression expression = new PredicateExpression(CompletedWorkingDocumentPresentedFactFields.WorkingDocumentId == workingDocumentId);

            EntityCollection<CompletedWorkingDocumentPresentedFactEntity> result = new EntityCollection<CompletedWorkingDocumentPresentedFactEntity>();

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), 0, null, path);
            }

            return result.Select(e => e.ToDTO()).ToList();
        }
    }
}
