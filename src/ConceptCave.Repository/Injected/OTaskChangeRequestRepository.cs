﻿using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.HelperClasses;
using ConceptCave.RepositoryInterfaces.Repositories;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Data.DTOConverters;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.Data;
using VirtualMgr.Central;

namespace ConceptCave.Repository.Injected
{
    public class OTaskChangeRequestRepository : ITaskChangeRequestRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public OTaskChangeRequestRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }
        public static PrefetchPath2 BuildPrefetchPath(ProjectJobTaskChangeRequestLoadInstructions instructions)
        {
            PrefetchPath2 path = new PrefetchPath2((int)EntityType.ProjectJobTaskChangeRequestEntity);


            if ((instructions & ProjectJobTaskChangeRequestLoadInstructions.Rosters) == ProjectJobTaskChangeRequestLoadInstructions.Rosters)
            {
                path.Add(ProjectJobTaskChangeRequestEntity.PrefetchPathFromRoster);
                path.Add(ProjectJobTaskChangeRequestEntity.PrefetchPathToRoster);
            }

            if ((instructions & ProjectJobTaskChangeRequestLoadInstructions.Task) == ProjectJobTaskChangeRequestLoadInstructions.Task)
            {
                var subPath = path.Add(ProjectJobTaskChangeRequestEntity.PrefetchPathProjectJobTask);

                if ((instructions & ProjectJobTaskChangeRequestLoadInstructions.TaskType) == ProjectJobTaskChangeRequestLoadInstructions.TaskType)
                {
                    subPath.SubPath.Add(ProjectJobTaskEntity.PrefetchPathProjectJobTaskType);
                }

                if ((instructions & ProjectJobTaskChangeRequestLoadInstructions.TaskUsers) == ProjectJobTaskChangeRequestLoadInstructions.TaskUsers)
                {
                    subPath.SubPath.Add(ProjectJobTaskEntity.PrefetchPathOwner);
                    subPath.SubPath.Add(ProjectJobTaskEntity.PrefetchPathSite);
                }

                if ((instructions & ProjectJobTaskChangeRequestLoadInstructions.TaskRole) == ProjectJobTaskChangeRequestLoadInstructions.TaskRole)
                {
                    subPath.SubPath.Add(ProjectJobTaskEntity.PrefetchPathHierarchyRole);
                }
            }

            if ((instructions & ProjectJobTaskChangeRequestLoadInstructions.Users) == ProjectJobTaskChangeRequestLoadInstructions.Users)
            {
                path.Add(ProjectJobTaskChangeRequestEntity.PrefetchPathRequestedByUserData);
                path.Add(ProjectJobTaskChangeRequestEntity.PrefetchPathProcessedByUserData);
            }

            return path;
        }

        public IList<ProjectJobTaskChangeRequestDTO> GetOpenRequestsByTaskId(Guid taskId, ProjectJobTaskChangeRequestLoadInstructions instructions = ProjectJobTaskChangeRequestLoadInstructions.None)
        {
            PredicateExpression expression = new PredicateExpression(ProjectJobTaskChangeRequestFields.ProjectJobTaskId == taskId);
            expression.AddWithAnd(ProjectJobTaskChangeRequestFields.Status == (int)ProjectJobTaskChangeRequestStatus.Requested);

            EntityCollection<ProjectJobTaskChangeRequestEntity> result = new EntityCollection<ProjectJobTaskChangeRequestEntity>();

            var path = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), path);
            }

            return result.Select(r => r.ToDTO()).ToList();
        }

        public IList<ProjectJobTaskChangeRequestDTO> GetOpenRequestsByFromRosterId(int rosterId, ProjectJobTaskChangeRequestLoadInstructions instructions = ProjectJobTaskChangeRequestLoadInstructions.None)
        {
            PredicateExpression expression = new PredicateExpression(ProjectJobTaskChangeRequestFields.FromRosterId == rosterId);
            expression.AddWithAnd(ProjectJobTaskChangeRequestFields.Status == (int)ProjectJobTaskChangeRequestStatus.Requested);

            EntityCollection<ProjectJobTaskChangeRequestEntity> result = new EntityCollection<ProjectJobTaskChangeRequestEntity>();

            var path = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), path);
            }

            return result.Select(r => r.ToDTO()).ToList();
        }

        public IList<ProjectJobTaskChangeRequestDTO> GetOpenRequestsByToRosterId(int rosterId, ProjectJobTaskChangeRequestLoadInstructions instructions = ProjectJobTaskChangeRequestLoadInstructions.None)
        {
            PredicateExpression expression = new PredicateExpression(ProjectJobTaskChangeRequestFields.ToRosterId == rosterId);
            expression.AddWithAnd(ProjectJobTaskChangeRequestFields.Status == (int)ProjectJobTaskChangeRequestStatus.Requested);

            EntityCollection<ProjectJobTaskChangeRequestEntity> result = new EntityCollection<ProjectJobTaskChangeRequestEntity>();

            var path = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), path);
            }

            return result.Select(r => r.ToDTO()).ToList();
        }

        public void DeleteOpenRequestsByTaskId(Guid taskId)
        {
            PredicateExpression expression = new PredicateExpression(ProjectJobTaskChangeRequestFields.ProjectJobTaskId == taskId);
            expression.AddWithAnd(ProjectJobTaskChangeRequestFields.ProcessedByUserId == DBNull.Value);

            using (var adapter = _fnAdapter())
            {
                adapter.DeleteEntitiesDirectly("ProjectJobTaskChangeRequestEntity", new RelationPredicateBucket(expression));
            }
        }

        public ProjectJobTaskChangeRequestDTO Save(ProjectJobTaskChangeRequestDTO task, bool refetch, bool recurse)
        {
            var e = task.ToEntity();

            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(e, refetch, recurse);
            }

            return refetch ? e.ToDTO() : task;
        }
    }
}
