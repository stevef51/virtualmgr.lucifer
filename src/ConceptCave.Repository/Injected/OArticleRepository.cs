﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.Data.DTOConverters;
using ConceptCave.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using ConceptCave.Data;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Data.Linq;
using VirtualMgr.Central;

namespace ConceptCave.Repository.Injected
{
    public class OArticleRepository : IArticleRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public OArticleRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }

        private static Dictionary<ArticleLoadInstructions, PrefetchPath2> _staticPrefetchPaths;

        static OArticleRepository()
        {
            _staticPrefetchPaths = new Dictionary<ArticleLoadInstructions, PrefetchPath2>();

            var path = new PrefetchPath2(EntityType.ArticleEntity);
            _staticPrefetchPaths[ArticleLoadInstructions.None] = path;

            path = new PrefetchPath2(EntityType.ArticleEntity);
            path.Add(ArticleEntity.PrefetchPathArticleBools).Sorter = Sort(ArticleBoolFields.Name | SortOperator.Ascending, ArticleBoolFields.Index | SortOperator.Ascending);
            path.Add(ArticleEntity.PrefetchPathArticleBigInts).Sorter = Sort(ArticleBigIntFields.Name | SortOperator.Ascending, ArticleBigIntFields.Index | SortOperator.Ascending);
            path.Add(ArticleEntity.PrefetchPathArticleDateTimes).Sorter = Sort(ArticleDateTimeFields.Name | SortOperator.Ascending, ArticleDateTimeFields.Index | SortOperator.Ascending);
            path.Add(ArticleEntity.PrefetchPathArticleGuids).Sorter = Sort(ArticleGuidFields.Name | SortOperator.Ascending, ArticleGuidFields.Index | SortOperator.Ascending);
            path.Add(ArticleEntity.PrefetchPathArticleNumbers).Sorter = Sort(ArticleNumberFields.Name | SortOperator.Ascending, ArticleNumberFields.Index | SortOperator.Ascending);
            path.Add(ArticleEntity.PrefetchPathArticleStrings).Sorter = Sort(ArticleStringFields.Name | SortOperator.Ascending, ArticleStringFields.Index | SortOperator.Ascending);
            path.Add(ArticleEntity.PrefetchPathArticleSubArticles).Sorter = Sort(ArticleSubArticleFields.Name | SortOperator.Ascending, ArticleSubArticleFields.Index | SortOperator.Ascending);
            _staticPrefetchPaths[ArticleLoadInstructions.PrimaryFields] = path;
        }

        public void SaveArticle(DTO.DTOClasses.ArticleDTO dto)
        {
            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(dto.ToEntity(), true, true);
            }
        }

        private static SortExpression Sort(params ISortClause[] sortClauses)
        {
            var sort = new SortExpression();
            foreach (var clause in sortClauses)
                sort.Add(clause);
            return sort;
        }

        public ArticleDTO LoadArticle(Guid articleId, ArticleLoadInstructions loadInstructions = ArticleLoadInstructions.PrimaryFields)
        {
            var entity = new ArticleEntity(articleId);

            PrefetchPath2 path = _staticPrefetchPaths[loadInstructions];

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntity(entity, path);
            }

            return entity.IsNew ? null : entity.ToDTO();
        }


        public ArticleDTO LoadArticleWithPropertyNames(Guid articleId, string name)
        {
            var entity = new ArticleEntity(articleId);

            PrefetchPath2 path = new PrefetchPath2((int)EntityType.ArticleEntity);

            path.Add(ArticleEntity.PrefetchPathArticleBools).Filter = new PredicateExpression(ArticleBoolFields.Name == name);
            path.Add(ArticleEntity.PrefetchPathArticleBigInts).Filter = new PredicateExpression(ArticleBigIntFields.Name == name);
            path.Add(ArticleEntity.PrefetchPathArticleDateTimes).Filter = new PredicateExpression(ArticleDateTimeFields.Name == name);
            path.Add(ArticleEntity.PrefetchPathArticleGuids).Filter = new PredicateExpression(ArticleGuidFields.Name == name);
            path.Add(ArticleEntity.PrefetchPathArticleNumbers).Filter = new PredicateExpression(ArticleNumberFields.Name == name);
            path.Add(ArticleEntity.PrefetchPathArticleStrings).Filter = new PredicateExpression(ArticleStringFields.Name == name);
            path.Add(ArticleEntity.PrefetchPathArticleSubArticles).Filter = new PredicateExpression(ArticleSubArticleFields.Name == name);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntity(entity, path);
            }

            return entity.IsNew ? null : entity.ToDTO();
        }

        public void SaveArticleBool(ArticleBoolDTO dto)
        {
            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(dto.ToEntity(), false, false);
            }
        }

        public void SaveArticleBigInt(ArticleBigIntDTO dto)
        {
            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(dto.ToEntity(), false, false);
            }
        }

        public void SaveArticleString(ArticleStringDTO dto)
        {
            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(dto.ToEntity(), false, false);
            }
        }

        public void SaveArticleDateTime(ArticleDateTimeDTO dto)
        {
            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(dto.ToEntity(), false, false);
            }
        }

        public void SaveArticleNumber(ArticleNumberDTO dto)
        {
            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(dto.ToEntity(), false, false);
            }
        }

        public void SaveArticleGuid(ArticleGuidDTO dto)
        {
            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(dto.ToEntity(), false, false);
            }
        }

        public void SaveArticleSubArticle(ArticleSubArticleDTO dto)
        {
            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(dto.ToEntity(), false, false);
            }
        }

        public void DeleteArticlePropertyName(Guid articleId, string name)
        {
            using (var adapter = _fnAdapter())
            {
                adapter.DeleteEntitiesDirectly(typeof(ArticleBoolEntity), new RelationPredicateBucket(new PredicateExpression(ArticleBoolFields.ArticleId == articleId).AddWithAnd(ArticleBoolFields.Name == name)));
                adapter.DeleteEntitiesDirectly(typeof(ArticleBigIntEntity), new RelationPredicateBucket(new PredicateExpression(ArticleBigIntFields.ArticleId == articleId).AddWithAnd(ArticleBigIntFields.Name == name)));
                adapter.DeleteEntitiesDirectly(typeof(ArticleDateTimeEntity), new RelationPredicateBucket(new PredicateExpression(ArticleDateTimeFields.ArticleId == articleId).AddWithAnd(ArticleDateTimeFields.Name == name)));
                adapter.DeleteEntitiesDirectly(typeof(ArticleGuidEntity), new RelationPredicateBucket(new PredicateExpression(ArticleGuidFields.ArticleId == articleId).AddWithAnd(ArticleGuidFields.Name == name)));
                adapter.DeleteEntitiesDirectly(typeof(ArticleNumberEntity), new RelationPredicateBucket(new PredicateExpression(ArticleNumberFields.ArticleId == articleId).AddWithAnd(ArticleNumberFields.Name == name)));
                adapter.DeleteEntitiesDirectly(typeof(ArticleStringEntity), new RelationPredicateBucket(new PredicateExpression(ArticleStringFields.ArticleId == articleId).AddWithAnd(ArticleStringFields.Name == name)));
                adapter.DeleteEntitiesDirectly(typeof(ArticleSubArticleEntity), new RelationPredicateBucket(new PredicateExpression(ArticleSubArticleFields.ArticleId == articleId).AddWithAnd(ArticleSubArticleFields.Name == name)));
            }
        }


        public IEnumerable<ArticleDTO> Find(string[] fieldNames, object[] equalsValue)
        {
            EntityCollection<ArticleEntity> list = new EntityCollection<ArticleEntity>();

            PrefetchPath2 path = new PrefetchPath2((int)EntityType.ArticleEntity);

            IRelationPredicateBucket bucket = new RelationPredicateBucket();

            for (int i = 0; i < fieldNames.Length; i++)
            {
                object v = equalsValue[i];
                if (v is bool)
                {
                    bucket.Relations.Add(ArticleEntity.Relations.ArticleBoolEntityUsingArticleId, "Bool_" + fieldNames[i]);
                    bucket.PredicateExpression.Add(ArticleBoolFields.Name.SetObjectAlias("Bool_" + fieldNames[i]) == fieldNames[i] & ArticleBoolFields.Value.SetObjectAlias("Bool_" + fieldNames[i]) == Convert.ToInt64(v));
                }
                else if (v is byte || v is Int16 || v is UInt16 || v is Int32 || v is UInt32 || v is Int64 || v is UInt64)
                {
                    bucket.Relations.Add(ArticleEntity.Relations.ArticleBigIntEntityUsingArticleId, "BigInt_" + fieldNames[i]);
                    bucket.PredicateExpression.Add(ArticleBigIntFields.Name.SetObjectAlias("BigInt_" + fieldNames[i]) == fieldNames[i] & ArticleBigIntFields.Value.SetObjectAlias("BigInt_" + fieldNames[i]) == Convert.ToInt64(v));
                }
                else if (v is DateTime)
                {
                    bucket.Relations.Add(ArticleEntity.Relations.ArticleDateTimeEntityUsingArticleId, "DateTime_" + fieldNames[i]);
                    bucket.PredicateExpression.Add(ArticleDateTimeFields.Name.SetObjectAlias("DateTime_" + fieldNames[i]) == fieldNames[i] & ArticleDateTimeFields.Value.SetObjectAlias("DateTime_" + fieldNames[i]) == (DateTime)v);
                }

                else if (v is Guid)
                {
                    if (fieldNames[i].ToLower().EndsWith("guid"))
                    {
                        bucket.Relations.Add(ArticleEntity.Relations.ArticleGuidEntityUsingArticleId, "Guid_" + fieldNames[i]);
                        bucket.PredicateExpression.Add(ArticleGuidFields.Name.SetObjectAlias("Guid_" + fieldNames[i]) == fieldNames[i] & ArticleGuidFields.Value.SetObjectAlias("Guid_" + fieldNames[i]) == (Guid)v);
                    }
                    else
                    {
                        bucket.Relations.Add(ArticleEntity.Relations.ArticleSubArticleEntityUsingArticleId, "SubArticle_" + fieldNames[i]);
                        bucket.PredicateExpression.Add(ArticleSubArticleFields.Name.SetObjectAlias("SubArticle_" + fieldNames[i]) == fieldNames[i] & ArticleSubArticleFields.SubArticleId.SetObjectAlias("SubArticle_" + fieldNames[i]) == (Guid)v);
                    }
                }

                else if (v is Decimal || v is float || v is double)
                {
                    bucket.Relations.Add(ArticleEntity.Relations.ArticleNumberEntityUsingArticleId, "Number_" + fieldNames[i]);
                    bucket.PredicateExpression.Add(ArticleNumberFields.Name.SetObjectAlias("Number_" + fieldNames[i]) == fieldNames[i] & ArticleNumberFields.Value.SetObjectAlias("Number_" + fieldNames[i]) == Convert.ToDecimal(v));
                }
                else if (v != null)
                {
                    bucket.Relations.Add(ArticleEntity.Relations.ArticleStringEntityUsingArticleId, "String_" + fieldNames[i]);
                    bucket.PredicateExpression.Add(ArticleStringFields.Name.SetObjectAlias("String_" + fieldNames[i]) == fieldNames[i] & ArticleStringFields.Value.SetObjectAlias("String_" + fieldNames[i]) == v.ToString());
                }
            }

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(list, bucket);
            }

            return from e in list select e.ToDTO();
        }


        public IEnumerable<ArticleBoolDTO> LoadArticleBool(Guid articleId, string name, bool? value, int? index)
        {
            var list = new EntityCollection<ArticleBoolEntity>();

            PredicateExpression expression = new PredicateExpression(ArticleBoolFields.ArticleId == articleId);
            if (name != null)
                expression.AddWithAnd(ArticleBoolFields.Name == name);
            if (value != null)
                expression.AddWithAnd(ArticleBoolFields.Value == value.Value);
            if (index != null)
                expression.AddWithAnd(ArticleBoolFields.Index == index.Value);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(list, new RelationPredicateBucket(expression));
            }

            return from e in list select e.ToDTO();
        }

        public IEnumerable<ArticleBigIntDTO> LoadArticleBigInt(Guid articleId, string name, long? value, int? index)
        {
            var list = new EntityCollection<ArticleBigIntEntity>();

            PredicateExpression expression = new PredicateExpression(ArticleBigIntFields.ArticleId == articleId);
            if (name != null)
                expression.AddWithAnd(ArticleBigIntFields.Name == name);
            if (value != null)
                expression.AddWithAnd(ArticleBigIntFields.Value == value.Value);
            if (index != null)
                expression.AddWithAnd(ArticleBigIntFields.Index == index.Value);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(list, new RelationPredicateBucket(expression));
            }

            return from e in list select e.ToDTO();
        }

        public IEnumerable<ArticleStringDTO> LoadArticleString(Guid articleId, string name, string value, int? index)
        {
            var list = new EntityCollection<ArticleStringEntity>();

            PredicateExpression expression = new PredicateExpression(ArticleStringFields.ArticleId == articleId);
            if (name != null)
                expression.AddWithAnd(ArticleStringFields.Name == name);
            if (value != null)
                expression.AddWithAnd(ArticleStringFields.Value == value);
            if (index != null)
                expression.AddWithAnd(ArticleStringFields.Index == index.Value);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(list, new RelationPredicateBucket(expression));
            }

            return from e in list select e.ToDTO();
        }

        public IEnumerable<ArticleDateTimeDTO> LoadArticleDateTime(Guid articleId, string name, DateTime? value, int? index)
        {
            var list = new EntityCollection<ArticleDateTimeEntity>();

            PredicateExpression expression = new PredicateExpression(ArticleDateTimeFields.ArticleId == articleId);
            if (value != null)
                expression.AddWithAnd(ArticleDateTimeFields.Value == value.Value);
            if (index != null)
                expression.AddWithAnd(ArticleDateTimeFields.Index == index.Value);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(list, new RelationPredicateBucket(expression));
            }

            return from e in list select e.ToDTO();
        }

        public IEnumerable<ArticleNumberDTO> LoadArticleNumber(Guid articleId, string name, decimal? value, int? index)
        {
            var list = new EntityCollection<ArticleNumberEntity>();

            PredicateExpression expression = new PredicateExpression(ArticleNumberFields.ArticleId == articleId);
            if (name != null)
                expression.AddWithAnd(ArticleNumberFields.Name == name);
            if (value != null)
                expression.AddWithAnd(ArticleNumberFields.Value == value.Value);
            if (index != null)
                expression.AddWithAnd(ArticleNumberFields.Index == index.Value);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(list, new RelationPredicateBucket(expression));
            }

            return from e in list select e.ToDTO();
        }

        public IEnumerable<ArticleGuidDTO> LoadArticleGuid(Guid articleId, string name, Guid? value, int? index)
        {
            var list = new EntityCollection<ArticleGuidEntity>();

            PredicateExpression expression = new PredicateExpression(ArticleGuidFields.ArticleId == articleId);
            if (name != null)
                expression.AddWithAnd(ArticleGuidFields.Name == name);
            if (value != null)
                expression.AddWithAnd(ArticleGuidFields.Value == value.Value);
            if (index != null)
                expression.AddWithAnd(ArticleGuidFields.Index == index.Value);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(list, new RelationPredicateBucket(expression));
            }

            return from e in list select e.ToDTO();
        }

        public IEnumerable<ArticleSubArticleDTO> LoadArticleSubArticle(Guid articleId, string name, Guid? value, int? index)
        {
            var list = new EntityCollection<ArticleSubArticleEntity>();

            PredicateExpression expression = new PredicateExpression(ArticleSubArticleFields.ArticleId == articleId);
            if (name != null)
                expression.AddWithAnd(ArticleSubArticleFields.Name == name);
            if (value != null)
                expression.AddWithAnd(ArticleSubArticleFields.SubArticleId == value.Value);
            if (index != null)
                expression.AddWithAnd(ArticleSubArticleFields.Index == index.Value);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(list, new RelationPredicateBucket(expression));
            }

            return from e in list select e.ToDTO();
        }
    }
}
