﻿using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.HelperClasses;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.Data.DTOConverters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Data.Linq;
using SD.LLBLGen.Pro.LinqSupportClasses;
using ConceptCave.DTO.DTOClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using VirtualMgr.Central;

namespace ConceptCave.Repository.Injected
{
    public class OLanguageTranslationRepository : ILanguageTranslationRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public OLanguageTranslationRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }

        public DTO.DTOClasses.LanguageTranslationDTO Get(string cultureName, string translationId)
        {
            using (var adapter = _fnAdapter())
            {
                var data = new LinqMetaData(adapter);
                var result = from r in data.LanguageTranslation
                             where
                             r.CultureName == cultureName &&
                             r.TranslationId == translationId
                             select r;

                var e = ((ILLBLGenProQuery)result).Execute<EntityCollection<LanguageTranslationEntity>>().FirstOrDefault();
                if (e == null)
                    return null;
                return e.ToDTO();
            }
        }

        public void Set(string cultureName, string translationId, string nativeText)
        {
            var dto = new LanguageTranslationDTO();
            dto.__IsNew = true;
            dto.CultureName = cultureName;
            dto.TranslationId = translationId;
            dto.NativeText = nativeText;

            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(dto.ToEntity());
            }
        }

        public IList<DTO.DTOClasses.LanguageTranslationDTO> GetAllTranslations(string cultureName)
        {
            using (var adapter = _fnAdapter())
            {
                var data = new LinqMetaData(adapter);
                var native = ((ILLBLGenProQuery)from r in data.LanguageTranslation
                                                where r.CultureName == cultureName
                                                select r).Execute<EntityCollection<LanguageTranslationEntity>>();

                return native.Select(r => r.ToDTO()).ToList();
            }
        }

        public DTO.DTOClasses.LanguageTranslationDTO GetTranslation(string cultureName, string translationId)
        {
            return Get(cultureName, translationId);
        }

        public DTO.DTOClasses.LanguageTranslationDTO SaveTranslation(DTO.DTOClasses.LanguageTranslationDTO dto, bool refetch, bool recurse)
        {
            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(dto.ToEntity());
            }
            return dto;
        }

        public void SaveTranslations(IEnumerable<DTO.DTOClasses.LanguageTranslationDTO> dtos)
        {
            using (var adapter = _fnAdapter())
            {
                var collection = new EntityCollection<LanguageTranslationEntity>();
                collection.AddRange(dtos.Select(dto => dto.ToEntity()));
                adapter.SaveEntityCollection(collection);
            }
        }

        public void DeleteTranslation(string cultureName, string translationId)
        {
            using (var adapter = _fnAdapter())
            {
                var filter = new RelationPredicateBucket(new PredicateExpression(LanguageTranslationFields.CultureName == cultureName & LanguageTranslationFields.TranslationId == translationId));
                adapter.DeleteEntitiesDirectly(typeof(LanguageTranslationEntity), filter);
            }
        }
    }
}
