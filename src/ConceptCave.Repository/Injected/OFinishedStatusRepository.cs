﻿using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.HelperClasses;
using ConceptCave.RepositoryInterfaces.Repositories;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Data.DTOConverters;
using ConceptCave.DTO.DTOClasses;
using VirtualMgr.Central;

namespace ConceptCave.Repository.Injected
{
    public class OFinishedStatusRepository : IFinishedStatusRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public OFinishedStatusRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }
        public IList<DTO.DTOClasses.ProjectJobFinishedStatusDTO> GetAll()
        {
            EntityCollection<ProjectJobFinishedStatusEntity> result = new EntityCollection<ProjectJobFinishedStatusEntity>();

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket());
            }

            return result.OrderBy(r => r.Stage).Select(r => r.ToDTO()).ToList();
        }

        public ProjectJobFinishedStatusDTO GetById(Guid id)
        {
            ProjectJobFinishedStatusEntity result = new ProjectJobFinishedStatusEntity(id);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntity(result);
            }

            return result.IsNew == true ? null : result.ToDTO();
        }

        public ProjectJobFinishedStatusDTO Save(ProjectJobFinishedStatusDTO status, bool refetch, bool recurse)
        {
            var e = status.ToEntity();

            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(e, refetch, recurse);
            }

            return refetch ? e.ToDTO() : status;
        }
    }
}
