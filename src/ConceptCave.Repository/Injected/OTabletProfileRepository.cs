﻿using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.DTOConverters;
using ConceptCave.RepositoryInterfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualMgr.Central;
using SD.LLBLGen.Pro.ORMSupportClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.Data;
using ConceptCave.Data.HelperClasses;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Data.Linq;
using ConceptCave.Data.DatabaseSpecific;

namespace ConceptCave.Repository.Injected
{
    public class OTabletProfileRepository : ITabletProfileRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public OTabletProfileRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }
        private PrefetchPath2 BuildPrefetchPath(TabletProfileLoadInstructions instructions)
        {
            PrefetchPath2 path = new PrefetchPath2(EntityType.TabletProfileEntity);
            return path;
        }

        public IList<TabletProfileDTO> GetAll(TabletProfileLoadInstructions instructions = TabletProfileLoadInstructions.None)
        {
            var list = new EntityCollection<TabletProfileEntity>();
            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(list, null, BuildPrefetchPath(instructions));
            }
            return (from e in list select e.ToDTO()).ToList();
        }

        public DTO.DTOClasses.TabletProfileDTO GetTabletProfileById(int id, TabletProfileLoadInstructions instructions)
        {
            var e = new TabletProfileEntity(id);
            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntity(e, BuildPrefetchPath(instructions));
            }
            return e.IsNew ? null : e.ToDTO();
        }

        public DTO.DTOClasses.TabletProfileDTO SaveTabletProfile(DTO.DTOClasses.TabletProfileDTO dto, bool refetch, bool recurse)
        {
            var e = dto.__IsNew ? new TabletProfileEntity() : new TabletProfileEntity(dto.Id);

            using (var adapter = _fnAdapter())
            {
                if (!dto.__IsNew)
                {
                    adapter.FetchEntity(e);
                }

                dto.ToEntity(e);

                adapter.SaveEntity(e, refetch, recurse);
            }

            return refetch ? e.ToDTO() : dto;
        }


        public IList<DTO.DTOClasses.TabletProfileDTO> Search(string nameLike, int? pageNumber, int? itemsPerPage, bool includeArchived, ref int totalItems)
        {
            using (var adapter = _fnAdapter())
            {
                var lmd = new LinqMetaData(adapter);
                var items = from r in lmd.TabletProfile where r.Name == null || r.Name != null && r.Name.Contains(nameLike) select r.ToDTO();
                items = items.OrderBy(i => i.Name);

                if (!includeArchived)
                    items = items.Where(i => i.Archived == false);

                totalItems = items.Count();
                if (pageNumber.HasValue && itemsPerPage.HasValue)
                    items = items.Skip((pageNumber.Value - 1) * itemsPerPage.Value).Take(itemsPerPage.Value);
                return items.ToList();
            }
        }

        public void Delete(int id, bool archiveIfRequired, out bool archived)
        {
            using (var adapter = _fnAdapter())
            {
                var entity = new TabletProfileEntity(id);
                archived = false;
                try
                {
                    adapter.DeleteEntity(entity);
                }
                catch (Exception ex)
                {
                    if (archiveIfRequired)
                    {
                        adapter.FetchEntity(entity);
                        entity.Archived = true;
                        adapter.SaveEntity(entity, false);
                        archived = true;
                    }
                    else
                        throw ex;
                }
            }
        }

    }
}
