﻿using ConceptCave.Data.Linq;
using ConceptCave.RepositoryInterfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualMgr.Central;
using ConceptCave.Data.DTOConverters;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;
using ConceptCave.Data;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.HelperClasses;
using SD.LLBLGen.Pro.LinqSupportClasses;
using ConceptCave.Data.DatabaseSpecific;

namespace ConceptCave.Repository.Injected
{
    public class OProductRepository : IProductRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public OProductRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }

        private PrefetchPath2 BuildPrefetchPath(ProductLoadInstructions instructions)
        {
            PrefetchPath2 path = new PrefetchPath2((int)EntityType.ProductEntity);

            if ((instructions & ProductLoadInstructions.ChildProductHierarchy) == ProductLoadInstructions.ChildProductHierarchy)
            {
                path.Add(ProductEntity.PrefetchPathChildProductHierarchies);
            }

            if ((instructions & ProductLoadInstructions.ParentProductHierarchy) == ProductLoadInstructions.ParentProductHierarchy)
            {
                path.Add(ProductEntity.PrefetchPathParentProductHierarchies);
            }

            return path;
        }

        public DTO.DTOClasses.ProductDTO GetByName(string name)
        {
            using (var adapter = _fnAdapter())
            {
                var data = new LinqMetaData(adapter);
                return (from prd in data.Product
                        where prd.Name == name
                        select prd).FirstOrDefault().ToDTO();
            }
        }

        public DTO.DTOClasses.ProductDTO GetByCode(string code, bool isCoupon, ProductLoadInstructions instructions = ProductLoadInstructions.None)
        {
            PrefetchPath2 path = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                var data = new LinqMetaData(adapter);
                return (from prd in data.Product
                        where prd.Code == code && prd.IsCoupon == isCoupon
                        select prd).WithPath(path).FirstOrDefault().ToDTO();
            }
        }

        public DTO.DTOClasses.ProductDTO GetById(int id, ProductLoadInstructions instructions = ProductLoadInstructions.None)
        {
            PrefetchPath2 path = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                ProductEntity result = new ProductEntity(id);
                adapter.FetchEntity(result, path);

                return result.ToDTO();
            }
        }

        public IList<ProductDTO> GetById(int[] id, ProductLoadInstructions instructions = ProductLoadInstructions.None)
        {
            PrefetchPath2 path = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                EntityCollection<ProductEntity> result = new EntityCollection<ProductEntity>();

                PredicateExpression expression = new PredicateExpression(ProductFields.Id == id);

                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), 0, null, path);

                return result.Select(r => r.ToDTO()).ToList();
            }
        }

        public DTO.DTOClasses.ProductDTO Save(DTO.DTOClasses.ProductDTO product, bool refetch, bool recurse = false)
        {
            using (var adapter = _fnAdapter())
            {
                var e = product.ToEntity();
                adapter.SaveEntity(e, refetch, recurse);
                return refetch ? e.ToDTO() : product;
            }
        }

        public IList<ProductTypeDTO> GetAllProductTypes()
        {
            using (var adapter = _fnAdapter())
            {
                var data = new LinqMetaData(adapter);
                return (from prd in data.ProductType
                        select prd.ToDTO()).ToList();
            }
        }

        public IList<ProductDTO> Search(string nameLike, bool? isCoupon, int? pageNumber, int? itemsPerPage, bool includeArchived, ref int totalItems)
        {
            using (var adapter = _fnAdapter())
            {
                var lmd = new LinqMetaData(adapter);
                var items = from r in lmd.Product where r.Name == null || r.Name != null && r.Name.Contains(nameLike) select r.ToDTO();

                if (isCoupon.HasValue)
                {
                    items = items.Where(i => i.IsCoupon == isCoupon.Value);
                }
                items = items.OrderBy(i => i.Name);

                totalItems = items.Count();

                if (pageNumber.HasValue && itemsPerPage.HasValue)
                    items = items.Skip((pageNumber.Value - 1) * itemsPerPage.Value).Take(itemsPerPage.Value);

                return items.ToList();
            }
        }

        public void RemoveProductHierarchies(int parentId)
        {
            PredicateExpression expression = new PredicateExpression(ProductHierarchyFields.ParentProductId == parentId);

            using (var adapter = _fnAdapter())
            {
                adapter.DeleteEntitiesDirectly("ProductHierarchyEntity", new RelationPredicateBucket(expression));
            }
        }
    }
}
