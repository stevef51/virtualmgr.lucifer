﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConceptCave.Data.DTOConverters;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.RepositoryInterfaces.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;
using ConceptCave.Data.Linq;
using SD.LLBLGen.Pro.LinqSupportClasses;
using ConceptCave.Data;
using ConceptCave.RepositoryInterfaces.Repositories;

namespace ConceptCave.Repository.Injected
{
    public class OMembershipTypeRepository : IMembershipTypeRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public OMembershipTypeRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }

        public IList<UserTypeDTO> GetAll()
        {
            var result = new EntityCollection<UserTypeEntity>();

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, null);
            }

            return result.Select(e => e.ToDTO()).ToList();
        }

        public UserTypeDTO GetById(int id, MembershipTypeLoadInstructions instructions)
        {
            UserTypeEntity result = new UserTypeEntity(id);

            PrefetchPath2 path = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntity(result, path);
            }

            return result.IsNew == true ? null : result.ToDTO();
        }

        public UserTypeDTO GetByName(string name)
        {
            EntityCollection<UserTypeEntity> result = new EntityCollection<UserTypeEntity>();

            PredicateExpression expression = new PredicateExpression(UserTypeFields.Name == name);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), 1);
            }

            return result.FirstOrDefault()?.ToDTO();
        }

        public IList<UserTypeDTO> GetSiteTypes()
        {
            using (var adapter = _fnAdapter())
            {
                var data = new LinqMetaData(adapter);
                var sites = from s in data.UserType
                            where s.IsAsite == true
                            select s;
                return ((ILLBLGenProQuery)sites).Execute<EntityCollection<UserTypeEntity>>().Select(e => e.ToDTO()).ToList();
            }
        }

        public void Save(UserTypeDTO dto, bool refetch, bool recurse)
        {
            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(dto.ToEntity(), refetch, recurse);
            }
        }

        public IList<UserTypeContextPublishedResourceDTO> GetByPublishingGroupResource(int publishingGroupResource)
        {
            EntityCollection<UserTypeContextPublishedResourceEntity> result = new EntityCollection<UserTypeContextPublishedResourceEntity>();

            PredicateExpression expression = new PredicateExpression(UserTypeContextPublishedResourceFields.PublishingGroupResourceId == publishingGroupResource);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression));
            }

            return result.Select(e => e.ToDTO()).ToList();
        }

        public UserTypeContextPublishedResourceDTO GetByUserType(int userTypeId, string name, UserTypeContextLoadInstructions instructions = UserTypeContextLoadInstructions.None)
        {
            EntityCollection<UserTypeContextPublishedResourceEntity> result = new EntityCollection<UserTypeContextPublishedResourceEntity>();

            PredicateExpression innerExpression = new PredicateExpression(new FieldCompareSetPredicate(UserTypeContextPublishedResourceFields.PublishingGroupResourceId, null, PublishingGroupResourceFields.Id, null, SetOperator.In, PublishingGroupResourceFields.Name == name));
            PredicateExpression expression = new PredicateExpression(UserTypeContextPublishedResourceFields.UserTypeId == userTypeId);

            expression.AddWithAnd(innerExpression);

            PrefetchPath2 path = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), path);
            }

            return result.FirstOrDefault()?.ToDTO();
        }

        public IList<UserTypeContextPublishedResourceDTO> GetByUserType(int userTypeId, UserTypeContextLoadInstructions instructions = UserTypeContextLoadInstructions.None)
        {
            EntityCollection<UserTypeContextPublishedResourceEntity> result = new EntityCollection<UserTypeContextPublishedResourceEntity>();

            PredicateExpression expression = new PredicateExpression(UserTypeContextPublishedResourceFields.UserTypeId == userTypeId);

            PrefetchPath2 path = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), path);
            }

            return result.Select(e => e.ToDTO()).ToList();
        }

        internal static PrefetchPath2 BuildPrefetchPath(UserTypeContextLoadInstructions instructions)
        {
            PrefetchPath2 path = new PrefetchPath2((int)EntityType.UserTypeContextPublishedResourceEntity);

            if ((instructions & UserTypeContextLoadInstructions.PublishingGroupResource) == UserTypeContextLoadInstructions.PublishingGroupResource)
            {
                var subPath = path.Add(UserTypeContextPublishedResourceEntity.PrefetchPathPublishingGroupResource);

                if ((instructions & UserTypeContextLoadInstructions.PublishingGroupResourceData) == UserTypeContextLoadInstructions.PublishingGroupResourceData)
                {
                    subPath.SubPath.Add(PublishingGroupResourceEntity.PrefetchPathPublishingGroupResourceData);
                }

                if ((instructions & UserTypeContextLoadInstructions.Resource) == UserTypeContextLoadInstructions.Resource)
                {
                    subPath.SubPath.Add(PublishingGroupResourceEntity.PrefetchPathResource);
                }

            }
            return path;
        }

        public IList<UserTypeContextPublishedResourceDTO> AddContextsToUserType(int[] publishingGroupResources, int userTypeId)
        {
            if (publishingGroupResources == null || publishingGroupResources.Length == 0)
            {
                return null;
            }

            EntityCollection<UserTypeContextPublishedResourceEntity> items = new EntityCollection<UserTypeContextPublishedResourceEntity>();

            foreach (var id in publishingGroupResources)
            {
                items.Add(new UserTypeContextPublishedResourceEntity() { PublishingGroupResourceId = id, UserTypeId = userTypeId });
            }

            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntityCollection(items, true, false);
            }

            return items.Select(e => e.ToDTO()).ToList();
        }

        public void RemoveContextsFromUserType(int[] publishingGroupResources, int userTypeId)
        {
            PredicateExpression expression = new PredicateExpression(UserTypeContextPublishedResourceFields.UserTypeId == userTypeId);
            expression.AddWithAnd(UserTypeContextPublishedResourceFields.PublishingGroupResourceId == publishingGroupResources);

            using (var adapter = _fnAdapter())
            {
                adapter.DeleteEntitiesDirectly("UserTypeContextPublishedResourceEntity", new RelationPredicateBucket(expression));
            }
        }

        public void RemoveLabelsFromUserType(Guid[] labels, int userTypeId)
        {
            PredicateExpression expression = new PredicateExpression(UserTypeDefaultLabelFields.UserTypeId == userTypeId);
            expression.AddWithAnd(UserTypeDefaultLabelFields.LabelId == labels);

            using (var adapter = _fnAdapter())
            {
                adapter.DeleteEntitiesDirectly("UserTypeDefaultLabelEntity", new RelationPredicateBucket(expression));
            }
        }

        public void RemoveRolesFromUserType(string[] roles, int userTypeId)
        {
            PredicateExpression expression = new PredicateExpression(UserTypeDefaultRoleFields.UserTypeId == userTypeId);
            expression.AddWithAnd(UserTypeDefaultRoleFields.RoleId == roles);

            using (var adapter = _fnAdapter())
            {
                adapter.DeleteEntitiesDirectly("UserTypeDefaultRoleEntity", new RelationPredicateBucket(expression));
            }
        }

        public void AddDashboardsToUserType(int[] publishingGroupResources, int userTypeId)
        {
            using (var adapter = _fnAdapter())
            {
                var ecol = new EntityCollection<UserTypeDashboardEntity>();
                foreach (var resource in publishingGroupResources)
                {
                    ecol.Add(new UserTypeDashboardEntity
                    {
                        UserTypeId = userTypeId,
                        PublishingGroupResourceId = resource
                    });
                }
                adapter.SaveEntityCollection(ecol);
            }
        }

        public void RemoveDashboardsFromUserType(int[] publishingGroupResources, int userTypeId)
        {
            using (var adapter = _fnAdapter())
            {
                var filter = new PredicateExpression(UserTypeDashboardFields.UserTypeId == userTypeId);
                filter.AddWithAnd(UserTypeDashboardFields.PublishingGroupResourceId == publishingGroupResources);
                adapter.DeleteEntitiesDirectly("UserTypeDashboardEntity", new RelationPredicateBucket(filter));
            }
        }

        internal static PrefetchPath2 BuildPrefetchPath(MembershipTypeLoadInstructions instructions)
        {
            PrefetchPath2 path = new PrefetchPath2((int)EntityType.UserTypeEntity);

            if ((instructions & MembershipTypeLoadInstructions.DefaultLabels) == MembershipTypeLoadInstructions.DefaultLabels)
            {
                var labelPath = path.Add(UserTypeEntity.PrefetchPathUserTypeDefaultLabels);

                if ((instructions & MembershipTypeLoadInstructions.Label) == MembershipTypeLoadInstructions.Label)
                {
                    labelPath.SubPath.Add(UserTypeDefaultLabelEntity.PrefetchPathLabel);
                }
            }

            if ((instructions & MembershipTypeLoadInstructions.DefaultRoles) == MembershipTypeLoadInstructions.DefaultRoles)
            {
                var rolePath = path.Add(UserTypeEntity.PrefetchPathUserTypeDefaultRoles);

                if ((instructions & MembershipTypeLoadInstructions.Role) == MembershipTypeLoadInstructions.Role)
                {
                    rolePath.SubPath.Add(UserTypeDefaultRoleEntity.PrefetchPathAspNetRole);
                }
            }

            if ((instructions & MembershipTypeLoadInstructions.UserTypeContext) == MembershipTypeLoadInstructions.UserTypeContext)
            {
                path.Add(UserTypeEntity.PrefetchPathUserTypeContextPublishedResources);
            }

            if (instructions.HasFlag(MembershipTypeLoadInstructions.Dashboards))
            {
                var dashboardPath = path.Add(UserTypeEntity.PrefetchPathUserTypeDashboards);
                var dashboardResourcePath = dashboardPath.SubPath.Add(UserTypeDashboardEntity.PrefetchPathPublishingGroupResource);
                if (instructions.HasFlag(MembershipTypeLoadInstructions.DashboardData))
                    dashboardResourcePath.SubPath.Add(
                        PublishingGroupResourceEntity.PrefetchPathPublishingGroupResourceData);
            }

            return path;
        }
    }
}
