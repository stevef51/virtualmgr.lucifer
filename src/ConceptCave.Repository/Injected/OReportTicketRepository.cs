﻿using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.Data.DTOConverters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Data.EntityClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;
using ConceptCave.Data;
using VirtualMgr.Central;

namespace ConceptCave.Repository.Injected
{
    public class OReportTicketRepository : IReportTicketRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public OReportTicketRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }
        private static Dictionary<ReportTicketLoadInstructions, PrefetchPath2> _staticPrefetchPaths;

        static OReportTicketRepository()
        {
            _staticPrefetchPaths = new Dictionary<ReportTicketLoadInstructions, PrefetchPath2>();

            var path = new PrefetchPath2(EntityType.ReportTicketEntity);
            _staticPrefetchPaths[ReportTicketLoadInstructions.None] = path;

            path = new PrefetchPath2(EntityType.ReportTicketEntity);
            path.Add(ReportTicketEntity.PrefetchPathReport);
            _staticPrefetchPaths[ReportTicketLoadInstructions.Report] = path;
        }

        public void SaveTicket(DTO.DTOClasses.ReportTicketDTO dto)
        {
            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(dto.ToEntity(), true, true);
            }
        }

        public DTO.DTOClasses.ReportTicketDTO LoadTicket(Guid id, ReportTicketLoadInstructions loadInstructions)
        {
            var entity = new ReportTicketEntity(id);

            PrefetchPath2 path = _staticPrefetchPaths[loadInstructions];

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntity(entity, path);
            }

            return entity.IsNew ? null : entity.ToDTO();

        }

    }
}
