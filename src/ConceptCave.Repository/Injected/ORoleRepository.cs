﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.DatabaseSpecific;
using SD.LLBLGen.Pro.ORMSupportClasses;
using ConceptCave.Data.DTOConverters;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Repositories;

namespace ConceptCave.Repository.Injected
{
    public class ORoleRepository : IRoleRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public ORoleRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }

        public IList<AspNetRoleDTO> GetAll()
        {
            EntityCollection<AspNetRoleEntity> result = new EntityCollection<AspNetRoleEntity>();

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, null);
            }

            return result.Select(e => e.ToDTO()).ToList();
        }

        public AspNetRoleDTO GetByName(string name)
        {
            PredicateExpression expression = new PredicateExpression(AspNetRoleFields.Name == name);

            EntityCollection<AspNetRoleEntity> result = new EntityCollection<AspNetRoleEntity>();

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), 1);
            }

            return result.FirstOrDefault()?.ToDTO();
        }

        public AspNetRoleDTO Save(AspNetRoleDTO role)
        {
            using (var adapter = _fnAdapter())
            {
                var entity = role.ToEntity();
                adapter.SaveEntity(entity);
                return entity.ToDTO();
            }
        }

        public bool IsUserInRole(Guid userId, string role)
        {
            PredicateExpression expression = new PredicateExpression(AspNetUserRoleFields.UserId == userId);

            expression.AddWithAnd(new FieldCompareSetPredicate(AspNetUserRoleFields.RoleId, null, AspNetRoleFields.Id, null, SetOperator.In, AspNetRoleFields.Name == role));

            EntityCollection<AspNetUserRoleEntity> result = new EntityCollection<AspNetUserRoleEntity>();

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), 1);
            }

            return result.Count == 0 ? false : true;
        }

        /// <summary>
        /// Checks the roles defined in the database to see if all of the roles defined in CoreRoles are in the db
        /// </summary>
        /// <returns>True if all CoreRoles are in the db, otherwise false</returns>
        public string[] CheckCoreRolesExist(IDictionary<string, string> coreRoles, IDictionary<string, string> optionalRoles)
        {
            List<string> result = new List<string>();

            var dbRoles = GetAll();

            var roles = (from r in dbRoles select r.Name.ToLower());
            var options = (from r in optionalRoles select r.Value.ToLower());

            foreach (var cr in coreRoles)
            {
                if (roles.Contains(cr.Value.ToLower()) == false)
                {
                    // there are a set of core roles that aren't in tenants (example the multi tenant admin), so only list it as missing if its not a part of this set
                    if (options.Contains(cr.Value.ToLower()) == false)
                    {
                        result.Add(cr.Value);
                    }
                }
            }

            return result.ToArray();
        }
    }
}
