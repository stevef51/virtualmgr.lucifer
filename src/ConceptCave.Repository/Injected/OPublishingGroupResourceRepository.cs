﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Editor;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Data.Linq;
using ConceptCave.Repository;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.Data.DTOConverters;
using SD.LLBLGen.Pro.LinqSupportClasses;
using SD.LLBLGen.Pro.LinqSupportClasses.PrefetchPathAPI;
using SD.LLBLGen.Pro.ORMSupportClasses;
using VirtualMgr.Central;
using ConceptCave.Core.Coding;
using ConceptCave.Checklist;
using ConceptCave.Data;

namespace ConceptCave.Repository.Injected
{
    //At the moment, these guys just delegate to the static repositories
    //In time, however, they'll be axed, these will be renamed, and all the logic will be moved into here.
    //we can use this to mock out the repository in a test right now, however.
    public class OPublishingGroupResourceRepository : IPublishingGroupResourceRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;

        private readonly IPublishingGroupRepository _publishingGroupRepo;
        private readonly IResourceRepository _resourceRepo;

        private static readonly string _DesignDocumentRootElementName = "Document";

        public OPublishingGroupResourceRepository(Func<DataAccessAdapter> fnAdapter, IPublishingGroupRepository publishingGroupRepo, IResourceRepository resourceRepo)
        {
            _fnAdapter = fnAdapter;
            _publishingGroupRepo = publishingGroupRepo;
            _resourceRepo = resourceRepo;
        }

        public static PrefetchPath2 BuildPrefetch(PublishingGroupResourceLoadInstruction instructions)
        {
            PrefetchPath2 path = new PrefetchPath2((int)EntityType.PublishingGroupResourceEntity);

            if ((instructions & PublishingGroupResourceLoadInstruction.Data) == PublishingGroupResourceLoadInstruction.Data)
            {
                path.Add(PublishingGroupResourceEntity.PrefetchPathPublishingGroupResourceData);
            }

            if ((instructions & PublishingGroupResourceLoadInstruction.PublicTickets) == PublishingGroupResourceLoadInstruction.PublicTickets)
            {
                path.Add(PublishingGroupResourceEntity.PrefetchPathPublicPublishingGroupResources);
            }

            if ((instructions & PublishingGroupResourceLoadInstruction.Resource) == PublishingGroupResourceLoadInstruction.Resource)
            {
                IPrefetchPathElement2 sub = path.Add(PublishingGroupResourceEntity.PrefetchPathResource);

                if ((instructions & PublishingGroupResourceLoadInstruction.ResourceData) == PublishingGroupResourceLoadInstruction.ResourceData)
                {
                    sub.SubPath.Add(ResourceEntity.PrefetchPathResourceData);
                }
            }

            if ((instructions & PublishingGroupResourceLoadInstruction.PublicVariables) == PublishingGroupResourceLoadInstruction.PublicVariables)
            {
                path.Add(PublishingGroupResourceEntity.PrefetchPathPublishingGroupResourcePublicVariables);
            }

            if ((instructions & PublishingGroupResourceLoadInstruction.Group) == PublishingGroupResourceLoadInstruction.Group)
            {
                path.Add(PublishingGroupResourceEntity.PrefetchPathPublishingGroup);
            }

            if (instructions.HasFlag(PublishingGroupResourceLoadInstruction.ResourceLabels))
            {
                var sp = path.Add(PublishingGroupResourceEntity.PrefetchPathLabelPublishingGroupResources);
                sp.SubPath.Add(LabelPublishingGroupResourceEntity.PrefetchPathLabel);
            }
            return path;
        }


        public string DesignDocumentRootElementName { get => _DesignDocumentRootElementName; }

        public PublishingGroupResourceDTO Save(PublishingGroupResourceDTO resource, bool refetch, bool recurse)
        {
            var entity = resource.ToEntity();
            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(entity, refetch, recurse);
            }
            return entity.ToDTO();
        }

        public PublishingGroupResourceDTO GetById(int id, PublishingGroupResourceLoadInstruction instructions = PublishingGroupResourceLoadInstruction.None)
        {
            var result = GetByIds(new int[] { id }, instructions);

            return result.FirstOrDefault();
        }

        public IEnumerable<PublishingGroupResourceDTO> GetByIds(int[] id, PublishingGroupResourceLoadInstruction instructions = PublishingGroupResourceLoadInstruction.None)
        {
            PredicateExpression expression = new PredicateExpression(PublishingGroupResourceFields.Id == id);

            EntityCollection<PublishingGroupResourceEntity> result = new EntityCollection<PublishingGroupResourceEntity>();

            PrefetchPath2 path = BuildPrefetch(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), path);
            }

            return result.Select(r => r.ToDTO());
        }

        public PublishingGroupResourceDTO GetById(int publishedGroupId, int resourceId, PublishingGroupResourceLoadInstruction instructions = PublishingGroupResourceLoadInstruction.None)
        {
            PredicateExpression expression = new PredicateExpression(PublishingGroupResourceFields.PublishingGroupId == publishedGroupId & PublishingGroupResourceFields.ResourceId == resourceId);

            EntityCollection<PublishingGroupResourceEntity> result = new EntityCollection<PublishingGroupResourceEntity>();

            PrefetchPath2 path = BuildPrefetch(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), path);
            }

            return result.Select(r => r.ToDTO()).FirstOrDefault();
        }

        public PublishingGroupResourceEntity GetEntityById(int publishedGroupId, Guid resourceId, PublishingGroupResourceLoadInstruction instructions = PublishingGroupResourceLoadInstruction.None)
        {
            PredicateExpression expression = new PredicateExpression(PublishingGroupResourceFields.PublishingGroupId == publishedGroupId);
            expression.AddWithAnd(new FieldCompareSetPredicate(PublishingGroupResourceFields.ResourceId, null, ResourceFields.Id, null, SetOperator.In, ResourceFields.NodeId == resourceId));

            EntityCollection<PublishingGroupResourceEntity> result = new EntityCollection<PublishingGroupResourceEntity>();

            PrefetchPath2 path = BuildPrefetch(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), path);
            }

            return result.FirstOrDefault();
        }

        public PublishingGroupResourceDTO GetById(int publishedGroupId, Guid resourceId, PublishingGroupResourceLoadInstruction instructions = PublishingGroupResourceLoadInstruction.None)
        {
            PredicateExpression expression = new PredicateExpression(PublishingGroupResourceFields.PublishingGroupId == publishedGroupId);
            expression.AddWithAnd(new FieldCompareSetPredicate(PublishingGroupResourceFields.ResourceId, null, ResourceFields.Id, null, SetOperator.In, ResourceFields.NodeId == resourceId));

            EntityCollection<PublishingGroupResourceEntity> result = new EntityCollection<PublishingGroupResourceEntity>();

            PrefetchPath2 path = BuildPrefetch(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), path);
            }

            return result.Select(r => r.ToDTO()).FirstOrDefault();
        }

        public IEnumerable<PublishingGroupResourceDTO> GetLikeName(string query, int publishingGroupId)
        {
            using (var adapter = _fnAdapter())
            {
                var predicate = new PredicateExpression(PublishingGroupResourceFields.Name % query);
                predicate.AddWithAnd(new PredicateExpression(
                    PublishingGroupResourceFields.PublishingGroupId == publishingGroupId));
                var result = new EntityCollection<PublishingGroupResourceEntity>();
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(predicate));
                return result.Select(r => r.ToDTO());
            }
        }

        public PublishingGroupResourceDTO Add(int publishedGroupId, Guid resourceId)
        {
            var resource = _resourceRepo.GetByNodeId(resourceId, ResourceLoadInstructions.Data);

            PublishingGroupResourceEntity result = new PublishingGroupResourceEntity();
            result.PublishingGroupId = publishedGroupId;
            result.ResourceId = resource.Id;

            PublishingGroupResourceDataEntity data = new PublishingGroupResourceDataEntity();
            result.PublishingGroupResourceData = data;

            PopulatePublishedFromResource(resource, result);

            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(result, true, true);
            }

            return result.ToDTO();
        }

        private void PopulatePublishedFromResource(ResourceDTO resource, PublishingGroupResourceEntity result)
        {
            IEncoder encoder = CoderFactory.CreateEncoder();
            DesignDocument document = FullyResolvedDesignDocumentFromResource(resource);

            // get any handlers to do what ever it is they need to do
            PublishingSectionManager.Current.Items.ForEach(h => h.CreateType().Handle(document, result));

            encoder.Encode(_DesignDocumentRootElementName, document);

            result.Name = resource.Name;
            result.PublishingGroupResourceData.Data = encoder.ToString();
            result.EstimatedPageCount = document.Sections.Count();
        }

        public DesignDocument FullyResolvedDesignDocumentFromResource(ResourceDTO resourceEntity)
        {
            DesignDocument resource = (DesignDocument)ResourceHelper.ResolveResources(resourceEntity);

            resource.PopulateQuestionsFromPresentables();

            return resource;
        }

        public PublishingGroupResourceDTO RefreshFromResource(int publishedGroupId, Guid resourceId)
        {
            var resource = _resourceRepo.GetByNodeId(resourceId, ResourceLoadInstructions.Data);

            var entity = GetEntityById(publishedGroupId, resourceId, PublishingGroupResourceLoadInstruction.Data);

            PopulatePublishedFromResource(resource, entity);

            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(entity, true, true);
            }

            // refresh any user contexts that might be tied to the published working document
            //            EntityCollection<UserTypeContextPublishedResourceEntity> typeContexts = MembershipTypeRepository.GetByPublishingGroupResource(entity.Id);
            //            MembershipRepository.CreateAndMergeUserContextData(typeContexts, null);

            return entity.ToDTO();
        }

        public void Remove(int publishedGroupId, Guid resourceId)
        {
            var resource = _resourceRepo.GetByNodeId(resourceId, ResourceLoadInstructions.None);
            PredicateExpression expression = new PredicateExpression(PublishingGroupResourceFields.PublishingGroupId == publishedGroupId & PublishingGroupResourceFields.ResourceId == resource.Id);

            using (var adapter = _fnAdapter())
            {
                adapter.DeleteEntitiesDirectly("PublishingGroupResourceEntity", new RelationPredicateBucket(expression));
            }
        }

        public static DesignDocument DesignDocumentFromString(string codedString)
        {
            IDecoder decoder = CoderFactory.GetDecoder(codedString, null);

            DesignDocument doc = null;
            decoder.Decode<DesignDocument>(_DesignDocumentRootElementName, out doc);

            return doc;
        }

        public static DesignDocument DesignDocumentFromDTO(PublishingGroupResourceDTO resource)
        {
            return DesignDocumentFromString(resource.PublishingGroupResourceData.Data);
        }

        public void AddLabel(int publishingGroupId, Guid labelId, PublishingGroupActorType actorType)
        {
            PredicateExpression expression = new PredicateExpression(PublishingGroupActorTypeFields.Name == actorType.ToString());
            EntityCollection<PublishingGroupActorTypeEntity> types = new EntityCollection<PublishingGroupActorTypeEntity>();

            PublishingGroupActorLabelEntity item = new PublishingGroupActorLabelEntity();
            item.LabelId = labelId;
            item.PublishingGroupId = publishingGroupId;

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(types, new RelationPredicateBucket(expression), 1);

                if (types.Count == 0)
                {
                    throw new InvalidOperationException(string.Format("{0} could not be found as an actor type"));
                }

                item.PublishingTypeId = types[0].Id;

                adapter.SaveEntity(item);
            }
        }

        public void RemoveLabel(int publishingGroupId, Guid labelId, PublishingGroupActorType actorType)
        {
            PredicateExpression expression = new PredicateExpression(PublishingGroupActorTypeFields.Name == actorType.ToString());
            EntityCollection<PublishingGroupActorTypeEntity> types = new EntityCollection<PublishingGroupActorTypeEntity>();

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(types, new RelationPredicateBucket(expression), 1);

                if (types.Count == 0)
                {
                    throw new InvalidOperationException(string.Format("{0} could not be found as an actor type"));
                }

                PredicateExpression deleteExpression = new PredicateExpression(PublishingGroupActorLabelFields.LabelId == labelId & PublishingGroupActorLabelFields.PublishingGroupId == publishingGroupId & PublishingGroupActorLabelFields.PublishingTypeId == types[0].Id);

                adapter.DeleteEntitiesDirectly("PublishingGroupActorLabelEntity", new RelationPredicateBucket(deleteExpression));
            }
        }

        public void RemovePublicTicket(int publishingGroupResourceId)
        {
            PredicateExpression expression = new PredicateExpression(PublicPublishingGroupResourceFields.PublishingGroupResourceId == publishingGroupResourceId);

            using (var adapter = _fnAdapter())
            {
                adapter.DeleteEntitiesDirectly("PublicPublishingGroupResourceEntity", new RelationPredicateBucket(expression));
            }
        }

        public IEnumerable<PublishingGroupResourceDTO> GetChecklistsUserCanDo(string userName)
        {
            var groups = _publishingGroupRepo.GetGroupsUserCanDo(userName, PublishingGroupActorType.Reviewer);
            return groups.SelectMany(g => g.PublishingGroupResources);
        }

        public PublishingGroupResourceDTO GetByCompoundId(int groupId, int resourceId, PublishingGroupResourceLoadInstruction instructions)
        {
            using (var adapter = _fnAdapter())
            {
                var data = new LinqMetaData(adapter);

                var entity = from g in data.PublishingGroup
                             join r in data.PublishingGroupResource on
                                 g.Id equals r.PublishingGroupId
                             where g.Id == groupId && r.ResourceId == resourceId
                             select r;
                var path = BuildPrefetch(instructions);
                entity = entity.WithPath(path);
                if (!entity.Any())
                    throw new ArgumentException("No checklist found");
                return entity.Single().ToDTO();
            }
        }

        public PublishingGroupResourceDTO GetByGroupAndName(int groupId, string resourceName,
            PublishingGroupResourceLoadInstruction instructions = PublishingGroupResourceLoadInstruction.None)
        {
            using (var adapter = _fnAdapter())
            {
                var data = new LinqMetaData(adapter);
                var entity = from g in data.PublishingGroup
                             join r in data.PublishingGroupResource on
                                 g.Id equals r.PublishingGroupId
                             where g.Id == groupId && r.Name == resourceName
                             select r;
                var path = BuildPrefetch(instructions);
                entity = entity.WithPath(path);
                return entity.Single().ToDTO();
            }
        }

        public PublishingGroupResourceDTO GetByCompoundName(string groupName, string resourceName,
            PublishingGroupResourceLoadInstruction instructions = PublishingGroupResourceLoadInstruction.None)
        {
            using (var adapter = _fnAdapter())
            {
                var data = new LinqMetaData(adapter);
                var entity = from g in data.PublishingGroup
                             join r in data.PublishingGroupResource on
                                 g.Id equals r.PublishingGroupId
                             where g.Name == groupName && r.Name == resourceName
                             select r;
                var path = BuildPrefetch(instructions);
                entity = entity.WithPath(path);
                return entity.Single().ToDTO();
            }
        }

        public IList<PublishingGroupResourceDTO> GetLikeName(string name, PublishingGroupResourceLoadInstruction instructions, int pageNumber = 1, int pageSize = 20)
        {
            EntityCollection<PublishingGroupResourceEntity> result = new EntityCollection<PublishingGroupResourceEntity>();

            PredicateExpression expression = new PredicateExpression(PublishingGroupResourceFields.Name % name);

            SortExpression sort = new SortExpression(PublishingGroupResourceFields.Name | SortOperator.Ascending);

            var path = BuildPrefetch(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), pageSize, sort, path, pageNumber, pageSize);
            }

            return result.Select(r => r.ToDTO()).ToList();
        }
    }
}
