﻿using ConceptCave.RepositoryInterfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.DTO.DTOClasses;
using VirtualMgr.Central;
using ConceptCave.Data.Linq;
using ConceptCave.Data.DTOConverters;
using ConceptCave.RepositoryInterfaces.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Data;
using ConceptCave.Data.DatabaseSpecific;

namespace ConceptCave.Repository.Injected
{
    public class OOrderRepository : IOrderRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public OOrderRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }

        private PrefetchPath2 BuildPrefetchPath(OrderLoadInstructions instructions)
        {
            PrefetchPath2 path = new PrefetchPath2((int)EntityType.OrderEntity);

            if ((instructions & OrderLoadInstructions.Items) == OrderLoadInstructions.Items)
            {
                var itemsPath = path.Add(OrderEntity.PrefetchPathOrderItems);

                if ((instructions & OrderLoadInstructions.ItemProducts) == OrderLoadInstructions.ItemProducts)
                {
                    itemsPath.SubPath.Add(OrderItemEntity.PrefetchPathProduct);
                }
            }

            return path;
        }

        public OrderDTO GetById(Guid id, OrderLoadInstructions instructions)
        {
            PredicateExpression expression = new PredicateExpression(OrderFields.Id == id);

            var path = BuildPrefetchPath(instructions);

            EntityCollection<OrderEntity> result = new EntityCollection<OrderEntity>();

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), 1, null, path);
            }

            return result.Count == 0 ? null : result.First().ToDTO();
        }

        public void Remove(Guid orderId)
        {
            PredicateExpression expression = new PredicateExpression(OrderFields.Id == orderId);

            using (var adapter = _fnAdapter())
            {
                adapter.DeleteEntitiesDirectly(typeof(OrderEntity), new RelationPredicateBucket(expression));
            }
        }

        public void RemoveItems(Guid[] ids)
        {
            PredicateExpression expression = new PredicateExpression(OrderItemFields.Id == ids);

            using (var adapter = _fnAdapter())
            {
                adapter.DeleteEntitiesDirectly(typeof(OrderItemEntity), new RelationPredicateBucket(expression));
            }
        }

        public OrderDTO Save(OrderDTO dto, bool refetch, bool recurse)
        {
            using (var adapter = _fnAdapter())
            {
                var e = dto.ToEntity();
                adapter.SaveEntity(e, refetch, recurse);
                return refetch ? e.ToDTO() : dto;
            }
        }
    }
}
