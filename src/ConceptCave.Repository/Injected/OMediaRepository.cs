﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Transactions;
using ConceptCave.Checklist;
using ConceptCave.Data;
using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.Data.DTOConverters;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Data.Linq;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System.Threading;
using SD.LLBLGen.Pro.LinqSupportClasses;
using ConceptCave.RepositoryInterfaces;

namespace ConceptCave.Repository.Injected
{
    public class OMediaRepository : IMediaRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        private readonly IMimeTypeRepository _mimeTypeRepo;
        private readonly Func<string, IMediaPreviewCreator> _fnGetMediaPreviewCreator;

        public OMediaRepository(Func<DataAccessAdapter> fnAdapter, Func<string, IMediaPreviewCreator> fnGetMediaPreviewCreator, IMimeTypeRepository mimeTypeRepo)
        {
            _fnAdapter = fnAdapter;
            _fnGetMediaPreviewCreator = fnGetMediaPreviewCreator;
            _mimeTypeRepo = mimeTypeRepo;
        }

        protected static PrefetchPath2 BuildPrefetch(MediaLoadInstruction instructions)
        {
            PrefetchPath2 path = new PrefetchPath2((int)EntityType.MediaEntity);

            if ((instructions & MediaLoadInstruction.Data) == MediaLoadInstruction.Data)
            {
                path.Add(MediaEntity.PrefetchPathMediaData);
            }

            if ((instructions & MediaLoadInstruction.Roles) == MediaLoadInstruction.Roles)
            {
                path.Add(MediaEntity.PrefetchPathMediaRoles).SubPath.Add(MediaRoleEntity.PrefetchPathAspNetRole);
            }

            if ((instructions & MediaLoadInstruction.MediaLabel) == MediaLoadInstruction.MediaLabel)
            {
                var subPath = path.Add(MediaEntity.PrefetchPathLabelMedias);

                if ((instructions & MediaLoadInstruction.Label) == MediaLoadInstruction.Label)
                {
                    subPath.SubPath.Add(LabelMediaEntity.PrefetchPathLabel);
                }
            }

            if ((instructions & MediaLoadInstruction.MediaVersions) == MediaLoadInstruction.MediaVersions)
            {
                var versionPath = path.Add(MediaEntity.PrefetchPathMediaVersions);

                if ((instructions & MediaLoadInstruction.MediaVersionsData) == MediaLoadInstruction.MediaVersionsData)
                {
                    versionPath.SubPath.Add(MediaVersionEntity.PrefetchPathMediaVersionData);
                }
            }

            if ((instructions & MediaLoadInstruction.MediaPolicies) == MediaLoadInstruction.MediaPolicies)
            {
                var policiesPath = path.Add(MediaEntity.PrefetchPathMediaPolicy);
            }

            var translatedMediaPath = path.Add(MediaEntity.PrefetchPathTranslated);
            if ((instructions & MediaLoadInstruction.AllTranslated) == 0)
                translatedMediaPath.Filter = new PredicateExpression(MediaTranslatedFields.CultureName == System.Threading.Thread.CurrentThread.CurrentUICulture.Name);

            return path;
        }

        protected PrefetchPath2 BuildFolderPrefetchPath(MediaFolderLoadInstruction instructions, string[] roles, bool includeHidden)
        {
            PrefetchPath2 path = new PrefetchPath2((int)EntityType.MediaFolderEntity);

            if ((instructions & MediaFolderLoadInstruction.FolderFile) == MediaFolderLoadInstruction.FolderFile)
            {
                var sub = path.Add(MediaFolderEntity.PrefetchPathMediaFolderFiles);

                sub.FilterRelations.Add(MediaFolderFileEntity.Relations.MediaEntityUsingMediaId);

                SortExpression sort = new SortExpression(MediaFields.SortOrder | SortOperator.Ascending);

                sub.Sorter = sort;

                PredicateExpression mediaFileExpression = new PredicateExpression();
                if (!includeHidden)
                    mediaFileExpression.Add(MediaFolderFileFields.Hidden == false);

                if (roles != null && roles.Length > 0)
                {
                    // we need select * from mediafolderfile where mediaid in (select id from media where id in (select mediaid from tblmediarole where roleid in (select roleid from aspnetrole where rolename = roles)) 
                    // or not exists (select * from tblmediafolderrole where mediafolderid = folderid)
                    PredicateExpression roleExpression = new PredicateExpression(AspNetRoleFields.Name == roles);
                    PredicateExpression mediaRoleExpression = new PredicateExpression(new FieldCompareSetPredicate(MediaRoleFields.RoleId, null, AspNetRoleFields.Id, null, SetOperator.In, roleExpression));
                    PredicateExpression mediaExpression = new PredicateExpression(new FieldCompareSetPredicate(MediaFields.Id, null, MediaRoleFields.MediaId, null, SetOperator.In, mediaRoleExpression));
                    PredicateExpression mediaFileRolesExpression = new PredicateExpression(new FieldCompareSetPredicate(MediaFolderFileFields.MediaId, null, MediaFields.Id, null, SetOperator.In, mediaExpression));

                    PredicateExpression existsExpression = new PredicateExpression(new FieldCompareSetPredicate(MediaFolderFileFields.MediaId, null, MediaRoleFields.MediaId, null, SetOperator.Exist, MediaFolderFileFields.MediaId == MediaRoleFields.MediaId, true));
                    mediaFileRolesExpression.AddWithOr(existsExpression);

                    mediaFileExpression.AddWithAnd(mediaFileRolesExpression);
                }
                sub.Filter = mediaFileExpression;

                if ((instructions & MediaFolderLoadInstruction.FolderFileMedia) == MediaFolderLoadInstruction.FolderFileMedia)
                {
                    var mediaPath = sub.SubPath.Add(MediaFolderFileEntity.PrefetchPathMedium);

                    if ((instructions & MediaFolderLoadInstruction.FolderFileMediaPolicies) == MediaFolderLoadInstruction.FolderFileMediaPolicies)
                    {
                        mediaPath.SubPath.Add(MediaEntity.PrefetchPathMediaPolicy);
                    }

                    var translatedMediaPath = mediaPath.SubPath.Add(MediaEntity.PrefetchPathTranslated);
                    if ((instructions & MediaFolderLoadInstruction.AllTranslated) == 0)
                        translatedMediaPath.Filter = new PredicateExpression(MediaTranslatedFields.CultureName == System.Threading.Thread.CurrentThread.CurrentUICulture.Name);
                }
            }

            if ((instructions & MediaFolderLoadInstruction.MediaFolderPolicies) == MediaFolderLoadInstruction.MediaFolderPolicies)
            {
                var policiesPath = path.Add(MediaFolderEntity.PrefetchPathMediaFolderPolicy);
            }

            if ((instructions & MediaFolderLoadInstruction.Roles) == MediaFolderLoadInstruction.Roles)
            {
                path.Add(MediaFolderEntity.PrefetchPathMediaFolderRoles).SubPath.Add(MediaFolderRoleEntity.PrefetchPathAspNetRole);
            }

            var translatedFolderPath = path.Add(MediaFolderEntity.PrefetchPathTranslated);
            if ((instructions & MediaFolderLoadInstruction.AllTranslated) == 0)
                translatedFolderPath.Filter = new PredicateExpression(MediaFolderTranslatedFields.CultureName == System.Threading.Thread.CurrentThread.CurrentUICulture.Name);

            return path;
        }

        protected PrefetchPath2 BuildViewingPrefetchPath(MediaViewingLoadInstruction instructions)
        {
            PrefetchPath2 path = new PrefetchPath2((int)EntityType.MediaViewingEntity);

            if ((instructions & MediaViewingLoadInstruction.PageViews) == MediaViewingLoadInstruction.PageViews)
            {
                path.Add(MediaViewingEntity.PrefetchPathMediaPageViewings);
            }

            if ((instructions & MediaViewingLoadInstruction.Users) == MediaViewingLoadInstruction.Users)
            {
                path.Add(MediaViewingEntity.PrefetchPathUserData);
            }

            if ((instructions & MediaViewingLoadInstruction.Media) == MediaViewingLoadInstruction.Media)
            {
                path.Add(MediaViewingEntity.PrefetchPathMedia);
            }

            return path;
        }

        protected PrefetchPath2 BuildVersionPrefetchPath(MediaVersionLoadInstructions instructions)
        {
            PrefetchPath2 path = new PrefetchPath2((int)EntityType.MediaVersionEntity);

            if ((instructions & MediaVersionLoadInstructions.Data) == MediaVersionLoadInstructions.Data)
            {
                path.Add(MediaVersionEntity.PrefetchPathMediaVersionData);
            }

            return path;
        }

        protected PrefetchPath2 BuildFolderFilePrefetchPath(MediaFolderFileLoadInstruction instructions)
        {
            PrefetchPath2 path = new PrefetchPath2((int)EntityType.MediaFolderFileEntity);

            if ((instructions & MediaFolderFileLoadInstruction.Folder) == MediaFolderFileLoadInstruction.Folder)
            {
                path.Add(MediaFolderFileEntity.PrefetchPathMediaFolder);
            }

            if ((instructions & MediaFolderFileLoadInstruction.Media) == MediaFolderFileLoadInstruction.Media)
            {
                var sub = path.Add(MediaFolderFileEntity.PrefetchPathMedium);

                if ((instructions & MediaFolderFileLoadInstruction.MediaData) == MediaFolderFileLoadInstruction.MediaData)
                {
                    sub.SubPath.Add(MediaEntity.PrefetchPathMediaData);
                }

                if ((instructions & MediaFolderFileLoadInstruction.MediaLabels) == MediaFolderFileLoadInstruction.MediaLabels)
                {
                    sub.SubPath.Add(MediaEntity.PrefetchPathLabelMedias).SubPath.Add(LabelMediaEntity.PrefetchPathLabel);
                }


                if ((instructions & MediaFolderFileLoadInstruction.MediaRoles) == MediaFolderFileLoadInstruction.MediaRoles)
                {
                    sub.SubPath.Add(MediaEntity.PrefetchPathMediaRoles).SubPath.Add(MediaRoleEntity.PrefetchPathAspNetRole);
                }

                if ((instructions & MediaFolderFileLoadInstruction.MediaVersions) == MediaFolderFileLoadInstruction.MediaVersions)
                {
                    var versionsPath = sub.SubPath.Add(MediaEntity.PrefetchPathMediaVersions);

                    if ((instructions & MediaFolderFileLoadInstruction.MediaVersionsData) == MediaFolderFileLoadInstruction.MediaVersionsData)
                    {
                        versionsPath.SubPath.Add(MediaVersionEntity.PrefetchPathMediaVersionData);
                    }
                }

                if ((instructions & MediaFolderFileLoadInstruction.MediaPolicies) == MediaFolderFileLoadInstruction.MediaPolicies)
                {
                    var policiesPath = sub.SubPath.Add(MediaEntity.PrefetchPathMediaPolicy);
                }

                if ((instructions & MediaFolderFileLoadInstruction.AllTranslated) == MediaFolderFileLoadInstruction.AllTranslated)
                {
                    sub.SubPath.Add(MediaEntity.PrefetchPathTranslated);
                }
                else
                {
                    string culturename = Thread.CurrentThread.CurrentUICulture.Name;
                    PredicateExpression currentCulture = new PredicateExpression(MediaTranslatedFields.CultureName == culturename);
                    sub.SubPath.Add(MediaEntity.PrefetchPathTranslated).Filter = currentCulture;
                }
            }

            return path;
        }

        /*      
                static MediaRepository methods, should be removed once everything else compiles

                public static EntityCollection<MediaEntity> GetAll()
                {
                    EntityCollection<MediaEntity> result = new EntityCollection<MediaEntity>();

                    using (var adapter = _fnAdapter())
                    {
                        adapter.FetchEntityCollection(result, null);
                    }

                    return result;
                }

                public static MediaEntity GetByFileId(Guid id, MediaLoadInstruction instructions)
                {
                    MediaFolderFileEntity file = new MediaFolderFileEntity(id);

                    using (var adapter = _fnAdapter())
                    {
                        if (adapter.FetchEntity(file) == false)
                        {
                            return null;
                        }
                    }

                    return GetById(file.MediaId, instructions);
                }

                public static IMediaPreviewGenerator GetPreviewGeneratorForExtension(string extension)
                {
                    MimeTypeEntity mime = MimeTypeRepository.GetByExtension(extension);

                    if (mime == null)
                    {
                        return null;
                    }

                    if (string.IsNullOrEmpty(mime.MediaPreviewGenerator) == true)
                    {
                        return null;
                    }

                    return (IMediaPreviewGenerator)System.Type.GetType(mime.MediaPreviewGenerator).GetConstructor(System.Type.EmptyTypes).Invoke(null);
                }

                public static MediaEntity CreateMediaFromPostedFile(IPostedFile postedFile)
                {
                    return CreateMediaFromPostedFile(postedFile, null, null);
                }

                public static MediaEntity CreateMediaFromPostedFile(IPostedFile postedFile, string name, string description)
                {
                    byte[] data = new byte[postedFile.ContentLength];
                    postedFile.Read(data, 0, postedFile.ContentLength);

                    MediaEntity media = new MediaEntity();
                    media.Id = CombFactory.NewComb();
                    media.Filename = System.IO.Path.GetFileName(postedFile.FileName);
                    media.Name = string.IsNullOrEmpty(name) ? postedFile.FileName : name;
                    media.Description = description;
                    media.Type = postedFile.ContentType;
                    media.MediaData = new MediaDataEntity();
                    media.MediaData.Data = data;

                    Save(media, true, true);

                    return media;
                }

                public static MediaEntity GetMediaFromPostedFile(IPostedFile postedFile, Guid id, string name, string description)
                {
                    byte[] data = new byte[postedFile.ContentLength];
                    postedFile.Read(data, 0, postedFile.ContentLength);

                    MediaEntity media = GetById(id, MediaLoadInstruction.Data);
                    media.Filename = System.IO.Path.GetFileName(postedFile.FileName);
                    media.Name = string.IsNullOrEmpty(name) ? postedFile.FileName : name;
                    media.Description = description;
                    media.Type = postedFile.ContentType;
                    media.MediaData.Data = data;

                    Save(media, true, true);

                    return media;
                }

                public static EntityCollection<MediaEntity> GetLikeName(string search, MediaLoadInstruction instructions = MediaLoadInstruction.None, int pageNumber = 1, int pageSize = 20)
                {
                    EntityCollection<MediaEntity> result = new EntityCollection<MediaEntity>();

                    PredicateExpression expression = new PredicateExpression(MediaFields.Name % search);

                    SortExpression sort = new SortExpression(MediaFields.Name | SortOperator.Ascending);

                    PrefetchPath2 path = BuildPrefetch(instructions);

                    using (var adapter = _fnAdapter())
                    {
                        adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), 0, sort, path, pageNumber, pageSize);
                    }

                    return result;
                }

                public static void Remove(Guid id)
                {
                    PredicateExpression expression = new PredicateExpression(MediaFields.Id == id);

                    using (var adapter = _fnAdapter())
                    {
                        adapter.DeleteEntitiesDirectly("MediaEntity", new RelationPredicateBucket(expression));
                    }
                }


                public static MediaPreviewEntity GetMediaPreviewById(Guid id, int page)
                {
                    MediaPreviewEntity result = new MediaPreviewEntity(id, page);

                    using (var adapter = _fnAdapter())
                    {
                        adapter.FetchEntity(result);
                    }

                    return result.IsNew == true ? null : result;
                }

        */

        public MediaDTO GetById(Guid id, MediaLoadInstruction instructions)
        {
            MediaEntity result = new MediaEntity(id);

            PrefetchPath2 path = BuildPrefetch(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntity(result, path);
            }

            var dto = result.IsNew ? null : result.ToDTO();
            if (dto != null && (instructions & MediaLoadInstruction.AllTranslated) == 0)
                Translate(dto);

            return dto;
        }

        public MediaDTO GetByFileNameAndType(string filename, string mimetype, MediaLoadInstruction instructions)
        {
            EntityCollection<MediaEntity> result = new EntityCollection<MediaEntity>();

            PrefetchPath2 path = BuildPrefetch(instructions);

            var expression = new PredicateExpression(MediaFields.Filename == filename);
            if (string.IsNullOrEmpty(mimetype) == false)
            {
                expression.AddWithAnd(MediaFields.Type == mimetype);
            }

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), 1, null, path);
            }

            return result.FirstOrDefault()?.ToDTO();
        }

        public MediaDTO GetByNameAndType(string name, string mimetype, MediaLoadInstruction instructions)
        {
            EntityCollection<MediaEntity> result = new EntityCollection<MediaEntity>();

            PrefetchPath2 path = BuildPrefetch(instructions);

            var expression = new PredicateExpression(MediaFields.Name == name);
            if (string.IsNullOrEmpty(mimetype) == false)
            {
                expression.AddWithAnd(MediaFields.Type == mimetype);
            }

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), 1, null, path);
            }

            return result.FirstOrDefault()?.ToDTO();
        }

        public UserMediaDTO MapMediaToUser(Guid uid, Guid mediaId)
        {
            using (var adapter = _fnAdapter())
            {
                //jsut add a join table entry
                var ent = new UserMediaEntity(mediaId, uid);
                if (!adapter.FetchEntity(ent))
                {
                    ent = new UserMediaEntity(mediaId, uid);
                    ent.DateCreated = DateTime.UtcNow;
                    adapter.SaveEntity(ent, true);
                }
                return ent.ToDTO();
            }
        }

        public OrderMediaDTO MapMediaToOrder(Guid orderId, Guid mediaId, string tag = "")
        {
            using (var adapter = _fnAdapter())
            {
                var lmd = new LinqMetaData(adapter);
                var ent = (from m in lmd.OrderMedia where m.OrderId == orderId && m.MediaId == mediaId && m.Tag == tag select m).FirstOrDefault();
                if (ent == null)
                {
                    ent = new OrderMediaEntity();
                    ent.MediaId = mediaId;
                    ent.OrderId = orderId;
                    ent.Tag = tag;
                    ent.DateCreated = DateTime.UtcNow;
                    adapter.SaveEntity(ent, true);
                }
                return ent.ToDTO();
            }
        }

        public MediaDTO Save(MediaDTO media, bool refetch, bool recurse)
        {
            var en = media.Translated.FirstOrDefault(t => t.CultureName == "en");
            if (en != null)
            {
                media.Description = en.Description;
                media.Name = en.Name;
                media.Filename = en.Filename;
            }
            var entity = media.ToEntity();

            if (string.IsNullOrEmpty(entity.Type))
            {
                var mimeType = _mimeTypeRepo.GetByFilename(entity.Filename);

                if (mimeType != null)
                {
                    entity.Type = mimeType.MimeType;
                }
            }

            if (entity.DateVersionCreated == DateTime.MinValue)
            {
                entity.DateVersionCreated = DateTime.UtcNow;
            }

            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(entity, refetch, recurse);
            }

            return refetch ? entity.ToDTO() : media;
        }

        public void Delete(Guid mediaId)
        {
            using (var adapter = _fnAdapter())
            {
                var mediaFilter = new RelationPredicateBucket(new PredicateExpression(MediaFields.Id == mediaId));
                var mediaDataFilter = new RelationPredicateBucket(new PredicateExpression(MediaDataFields.MediaId == mediaId));

                adapter.DeleteEntitiesDirectly(typeof(MediaDataEntity), mediaDataFilter);
                adapter.DeleteEntitiesDirectly(typeof(MediaEntity), mediaFilter);
            }
        }

        public MediaDTO CreateMediaFromPostedFile(IPostedFile postedFile, string name = null, string description = null)
        {
            byte[] data = new byte[postedFile.ContentLength];
            postedFile.Read(data, 0, postedFile.ContentLength);

            var media = new MediaDTO();
            media.Id = CombFactory.NewComb();
            media.Filename = System.IO.Path.GetFileName(postedFile.FileName);
            media.Name = string.IsNullOrEmpty(name) ? postedFile.FileName : name;
            media.Description = description;
            media.Type = postedFile.ContentType;
            media.MediaData = new MediaDataDTO();
            media.MediaData.Data = data;

            Save(media, true, true);

            return media;
        }

        public MediaDTO CreateMediaFromPostedFile(byte[] data, string name, string description, string filename, string contenttype, bool useVersioning, Guid userId)
        {
            MediaEntity media = new MediaEntity();
            media.Id = CombFactory.NewComb();
            media.Filename = System.IO.Path.GetFileName(filename);
            media.Name = string.IsNullOrEmpty(name) ? filename : name;
            media.Description = description;
            media.Type = contenttype;
            media.DateCreated = DateTime.UtcNow;
            media.MediaData = new MediaDataEntity();
            media.MediaData.Data = data;
            media.CreatedBy = userId;
            media.DateVersionCreated = DateTime.UtcNow;
            media.UseVersioning = useVersioning;
            media.Version = 1;

            MediaDTO result = null;

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(0, 10, 0)))
            {
                result = media.ToDTO();
                result = Save(result, true, true);

                var pageCount = GeneratePreviewImages(result);

                if (pageCount.HasValue == true)
                {
                    result.PreviewCount = pageCount;
                    Save(result, true, true);
                }

                scope.Complete();
            }

            return result;
        }

        public MediaDTO UpdateMediaFromPostedFile(byte[] data, Guid id, string name, string description, string filename, string contenttype, bool useVersioning, Guid userId)
        {
            var media = GetById(id, MediaLoadInstruction.Data);

            // Only perform the update if the data is actually different
            if (data.SequenceEqual(media.MediaData.Data))
                return media;

            MediaDTO result = null;

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(0, 10, 0)))
            {
                media.UseVersioning = useVersioning;
                int versionNumber = ManageVersioning(media, userId);

                media.Filename = System.IO.Path.GetFileName(filename);
                media.Name = string.IsNullOrEmpty(name) ? filename : name;
                media.Description = description;
                media.Type = contenttype;
                media.MediaData.Data = data;
                media.Version = versionNumber;
                media.DateVersionCreated = DateTime.UtcNow;
                media.PreviewCount = GeneratePreviewImages(media);

                result = media;

                Save(result, true, true);

                scope.Complete();
            }

            return result;
        }

        protected int ManageVersioning(MediaDTO media, Guid userId)
        {
            if (media.UseVersioning == false)
            {
                return media.Version;
            }

            MediaVersionEntity version = new MediaVersionEntity()
            {
                Id = CombFactory.NewComb(),
                MediaId = media.Id,
                Version = media.Version,
                Filename = media.Filename,
                Name = media.Name,
                Description = media.Description,
                DateVersionCreated = media.DateVersionCreated,
                DateVersionClosed = DateTime.UtcNow,
                VersionCreatedBy = media.CreatedBy.Value,
                VersionReplacedBy = userId
            };

            version.MediaVersionData = new MediaVersionDataEntity()
            {
                Data = media.MediaData.Data
            };

            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(version, false, true);
            }

            return media.Version + 1;
        }

        protected int? GeneratePreviewImages(MediaDTO media)
        {
            PredicateExpression expression = new PredicateExpression(MediaPreviewFields.MediaId == media.Id);

            using (var adapter = _fnAdapter())
            {
                adapter.DeleteEntitiesDirectly("MediaPreviewEntity", new RelationPredicateBucket(expression));
            }

            var mime = _mimeTypeRepo.GetByExtension(System.IO.Path.GetExtension(media.Filename));

            if (mime != null)
            {
                var previewCreator = _fnGetMediaPreviewCreator(mime.MimeType);
                if (previewCreator != null)
                {
                    using (var ms = new MemoryStream(media.MediaData.Data))
                    {
                        return previewCreator.CreateForMedia(media.Id, ms);
                    }
                }
            }
            return null;
        }

        public void RemoveFromLabels(Guid[] labelIds, Guid id)
        {
            PredicateExpression expression = new PredicateExpression(LabelMediaFields.LabelId == labelIds);
            expression.AddWithAnd(LabelMediaFields.MediaId == id);

            using (var adapter = _fnAdapter())
            {
                adapter.DeleteEntitiesDirectly("LabelMediaEntity", new RelationPredicateBucket(expression));
            }
        }

        public void RemoveFromRoles(string[] roleIds, Guid id)
        {
            PredicateExpression expression = new PredicateExpression(MediaRoleFields.RoleId == roleIds);
            expression.AddWithAnd(MediaRoleFields.MediaId == id);

            using (var adapter = _fnAdapter())
            {
                adapter.DeleteEntitiesDirectly("MediaRoleEntity", new RelationPredicateBucket(expression));
            }
        }


        private MediaDTO Translate(MediaDTO dto)
        {
            if (dto != null && dto.Translated.Any())
            {
                var first = dto.Translated.First();
                dto.Name = first.Name;
                dto.Filename = first.Filename;
                dto.Description = first.Description;
            }
            return dto;
        }

        private MediaFolderDTO Translate(MediaFolderDTO dto, MediaFolderLoadInstruction instructions)
        {
            // If using current culture then copy the translation across to primary record field
            if ((instructions & MediaFolderLoadInstruction.AllTranslated) == 0)
            {
                if (dto.Translated.Any())
                    dto.Text = dto.Translated.First().Text;

                foreach (var media in dto.MediaFolderFiles)
                    Translate(media.Medium);

                foreach (var child in dto.MediaFolders)
                    Translate(child, instructions);
            }
            return dto;
        }

        public IList<MediaFolderDTO> GetFolders(bool includeNoneHierarchy, string[] roles, MediaFolderLoadInstruction instructions, bool includeHidden = false)
        {
            var roots = GetFolders(null, roles, instructions, includeHidden);

            foreach (var root in roots)
            {
                var thread = GetThread(root.Id, roles, instructions, includeHidden);

                thread.ForEach(t => root.MediaFolders.Add(t));
            }

            if (includeNoneHierarchy == true)
            {
                // ok so we need to create a virtaul folder in which to put anything that's not already in a folder
                MediaFolderDTO virtualFolder = new MediaFolderDTO()
                {
                    Id = Guid.Empty,
                    __IsNew = false,
                    Text = "Not Filed"
                };

                // we need something like select * from media where id not in (select mediaid from mediafiles)
                PredicateExpression expression = new PredicateExpression(new FieldCompareSetPredicate(MediaFields.Id, null, MediaFolderFileFields.MediaId, null, SetOperator.In, null, true));

                EntityCollection<MediaEntity> notFiled = new EntityCollection<MediaEntity>();
                using (var adapter = _fnAdapter())
                {
                    adapter.FetchEntityCollection(notFiled, new RelationPredicateBucket(expression), 0, null);
                }

                foreach (var not in notFiled)
                {
                    var dto = not.ToDTO();

                    var virtualFile = new MediaFolderFileDTO()
                    {
                        Id = dto.Id,
                        __IsNew = false,
                        MediaFolderId = Guid.Empty
                    };

                    virtualFile.Medium = dto;

                    virtualFolder.MediaFolderFiles.Add(virtualFile);
                }

                roots.Insert(0, virtualFolder);
            }

            return roots;
        }

        public IList<MediaFolderDTO> GetFolders(Guid? parentId, string[] roles, MediaFolderLoadInstruction instructions, bool includeHidden = false)
        {
            PredicateExpression expression = new PredicateExpression(MediaFolderFields.AutoCreated == false);

            if (parentId.HasValue == true)
            {
                expression.Add(MediaFolderFields.ParentId == parentId);
            }
            else
            {
                expression.Add(MediaFolderFields.ParentId == DBNull.Value);
            }

            if (!includeHidden)
                expression.AddWithAnd(MediaFolderFields.Hidden == false);

            if (roles != null && roles.Length > 0)
            {
                // we need select * from mediafolders where id in (select folderid from mediafolderrole where roleid in (select id from aspnetrole where rolename = roles)) or not exists (select * from tblmediafolderrole where mediafolderid = folderid)
                PredicateExpression roleExpression = new PredicateExpression(AspNetRoleFields.Name == roles);
                PredicateExpression folderRoleExpression = new PredicateExpression(new FieldCompareSetPredicate(MediaFolderRoleFields.RoleId, null, AspNetRoleFields.Id, null, SetOperator.In, roleExpression));
                PredicateExpression folderExpression = new PredicateExpression(new FieldCompareSetPredicate(MediaFolderFields.Id, null, MediaFolderRoleFields.MediaFolderId, null, SetOperator.In, folderRoleExpression));

                PredicateExpression existsExpression = new PredicateExpression(new FieldCompareSetPredicate(MediaFolderFields.Id, null, MediaFolderRoleFields.MediaFolderId, null, SetOperator.Exist, MediaFolderFields.Id == MediaFolderRoleFields.MediaFolderId, true));

                folderExpression.AddWithOr(existsExpression);

                expression.AddWithAnd(folderExpression);
            }

            var path = BuildFolderPrefetchPath(instructions, roles, includeHidden);

            EntityCollection<MediaFolderEntity> result = new EntityCollection<MediaFolderEntity>();

            SortExpression sort = new SortExpression(MediaFolderFields.SortOrder | SortOperator.Ascending);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), 0, sort, path);
            }

            var dtos = result.ToList().Select(e => e.ToDTO()).ToList();

            foreach (var dto in dtos)
                Translate(dto, instructions);

            return dtos;
        }

        public IList<MediaFolderDTO> GetThread(Guid parentId, string[] roles, MediaFolderLoadInstruction instructions, bool includeHidden = false)
        {
            var folders = GetFolders(parentId, roles, instructions, includeHidden);

            foreach (var folder in folders)
            {
                var sub = GetThread(folder.Id, roles, instructions, includeHidden);

                sub.ForEach(s => folder.MediaFolders.Add(s));
            }

            return folders;
        }

        public MediaFolderFileDTO GetFolderFile(Guid parentId, string name, MediaFolderFileLoadInstruction instructions)
        {
            using (var adapter = _fnAdapter())
            {
                var data = new LinqMetaData(adapter);

                var query = data.MediaFolderFile
                    .Where(f => f.MediaFolderId == parentId)
                    .Where(f => data.Media.Where(d => d.Name == name).Select(d => d.Id).Contains(f.MediaId)).Select(f => f);

                var path = BuildFolderFilePrefetchPath(instructions);
                query = query.WithPath(path);

                var entity = ((ILLBLGenProQuery)query).Execute<EntityCollection<MediaFolderFileEntity>>();

                return entity != null && entity.Count > 0 ? entity.First().ToDTO() : null;
            }
        }

        public MediaFolderDTO GetFolderById(Guid id, MediaFolderLoadInstruction instructions, bool includeHidden = false)
        {
            MediaFolderEntity folder = new MediaFolderEntity(id);

            PrefetchPath2 path = BuildFolderPrefetchPath(instructions, null, includeHidden);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntity(folder, path);
            }

            return Translate(folder.ToDTO(), instructions);
        }

        public MediaFolderDTO GetFolder(Guid? parentId, string folderName, MediaFolderLoadInstruction instructions, bool includeHidden = false)
        {
            PredicateExpression expression = new PredicateExpression(MediaFolderFields.ParentId == parentId & MediaFolderFields.Text == folderName);

            PrefetchPath2 path = BuildFolderPrefetchPath(instructions, null, includeHidden);

            EntityCollection<MediaFolderEntity> result = new EntityCollection<MediaFolderEntity>();

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), 1, null, path);
            }

            return result.Count == 0 ? null : result[0].ToDTO();
        }

        public MediaFolderDTO SaveFolder(MediaFolderDTO folder, bool refetch, bool recurse)
        {
            // Make sure we use English translation for the folder name
            var en = folder.Translated.FirstOrDefault(t => t.CultureName == "en");
            if (en != null)
                folder.Text = en.Text;

            var e = folder.ToEntity();

            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(e, refetch, recurse);
            }

            return refetch ? e.ToDTO() : folder;
        }

        public void DeleteFolder(Guid id)
        {
            // we need to do a recursive delete, so first get children
            using (TransactionScope scope = new TransactionScope())
            {
                RecursiveFolderDelete(id);

                scope.Complete();
            }
        }

        public void RemoveFolderFromRoles(Guid folderId, string[] roleIds)
        {
            PredicateExpression expression = new PredicateExpression(MediaFolderRoleFields.MediaFolderId == folderId & MediaFolderRoleFields.RoleId == roleIds);

            using (var adapter = _fnAdapter())
            {
                adapter.DeleteEntitiesDirectly("MediaFolderRoleEntity", new RelationPredicateBucket(expression));
            }
        }

        protected void RecursiveFolderDelete(Guid id)
        {
            PredicateExpression expression = new PredicateExpression(MediaFolderFields.ParentId == id);

            EntityCollection<MediaFolderEntity> folders = new EntityCollection<MediaFolderEntity>();

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(folders, new RelationPredicateBucket(expression));

                foreach (var folder in folders)
                {
                    RecursiveFolderDelete(folder.Id);
                }

                adapter.DeleteEntitiesDirectly("MediaFolderEntity", new RelationPredicateBucket(new PredicateExpression(MediaFolderFields.Id == id)));
            }
        }


        public MediaFolderFileDTO GetFolderFileById(Guid id, MediaFolderFileLoadInstruction instructions)
        {
            MediaFolderFileEntity result = new MediaFolderFileEntity(id);
            var path = BuildFolderFilePrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntity(result, path);
            }

            if (result.IsNew)
                return null;

            var dto = result.ToDTO();
            if ((instructions & MediaFolderFileLoadInstruction.AllTranslated) == 0)
                Translate(dto.Medium);

            return dto;
        }

        public MediaFolderFileDTO SaveFolderFile(MediaFolderFileDTO file, bool refetch, bool recurse)
        {
            var e = file.ToEntity();

            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(e, refetch, recurse);
            }

            return refetch ? e.ToDTO() : file;
        }

        public MediaVersionDTO GetMediaVersionById(Guid id, MediaVersionLoadInstructions instructions)
        {
            MediaVersionEntity result = new MediaVersionEntity(id);
            var path = BuildVersionPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntity(result, path);
            }

            return result.IsNew ? null : result.ToDTO();
        }


        public MediaPreviewDTO GetMediaPreviewById(Guid id, int page)
        {
            MediaPreviewEntity result = new MediaPreviewEntity(id, page);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntity(result);
            }

            return result.IsNew == true ? null : result.ToDTO();
        }

        public MediaViewingDTO GetMediaViewingById(Guid id, MediaViewingLoadInstruction instructions)
        {
            MediaViewingEntity result = new MediaViewingEntity(id);

            var path = BuildViewingPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntity(result, path);
            }

            return result.IsNew == true ? null : result.ToDTO();
        }

        public MediaViewingDTO GetMediaViewingByWorkingId(Guid id, MediaViewingLoadInstruction instructions)
        {
            EntityCollection<MediaViewingEntity> result = new EntityCollection<MediaViewingEntity>();

            var path = BuildViewingPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(new PredicateExpression(MediaViewingFields.WorkingDocumentId == id)), 1, null, path);
            }

            return result.Count == 0 ? null : result[0].ToDTO();
        }

        public MediaViewingDTO GetMediaViewingByWorkingId(Guid id, Guid mediaId, MediaViewingLoadInstruction instructions)
        {
            EntityCollection<MediaViewingEntity> result = new EntityCollection<MediaViewingEntity>();

            var path = BuildViewingPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(new PredicateExpression(MediaViewingFields.WorkingDocumentId == id &
                    MediaViewingFields.MediaId == mediaId)), 1, null, path);
            }

            return result.Count == 0 ? null : result[0].ToDTO();
        }

        public IList<MediaViewingDTO> GetLatestMediaViewingsForUser(Guid userId)
        {
            EntityCollection<MediaEntity> result = new EntityCollection<MediaEntity>();

            var path = new PrefetchPath2((int)EntityType.MediaEntity);
            var subPath = path.Add(MediaEntity.PrefetchPathMediaViewings);

            subPath.Sorter = new SortExpression(MediaViewingFields.Version | SortOperator.Descending);
            subPath.MaxAmountOfItemsToReturn = 1;
            subPath.Filter = new PredicateExpression(MediaViewingFields.UserId == userId);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(), 0, null, path);
            }

            var views = from m in result.ToList() from v in m.MediaViewings.ToList() select v;

            return views.Select(v => v.ToDTO()).ToList();
        }

        public IList<MediaViewingDTO> GetLatestMediaViewingsForUsers(Guid[] userId, Guid mediaId)
        {
            EntityCollection<MediaEntity> result = new EntityCollection<MediaEntity>();

            var path = new PrefetchPath2((int)EntityType.MediaEntity);
            var subPath = path.Add(MediaEntity.PrefetchPathMediaViewings);

            subPath.Sorter = new SortExpression(MediaViewingFields.Version | SortOperator.Descending);
            subPath.MaxAmountOfItemsToReturn = 1;
            subPath.Filter = new PredicateExpression(MediaViewingFields.UserId == userId);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(new PredicateExpression(MediaFields.Id == mediaId)), 0, null, path);
            }

            var views = from m in result.ToList() from v in m.MediaViewings.ToList() select v;

            return views.Select(v => v.ToDTO()).ToList();
        }

        public IList<MediaViewingDTO> GetLatestMediaViewingsForFolder(Guid folderid, MediaViewingLoadInstruction instructions)
        {
            EntityCollection<MediaViewingEntity> result = new EntityCollection<MediaViewingEntity>();

            // we want any viewings that are under the current folder
            PredicateExpression folderExpression = new PredicateExpression(new FieldCompareSetPredicate(MediaViewingFields.MediaId, null, MediaFolderFileFields.MediaId, null, SetOperator.In, MediaFolderFileFields.MediaFolderId == folderid));

            // and we want only the latest viewings of those pieces of media, we'll do this through a relationship mappings
            PredicateExpression latestExpression = new PredicateExpression(MediaViewingFields.Version == MediaFields.Version);

            PredicateExpression expression = new PredicateExpression();
            expression.Add(folderExpression);
            expression.AddWithAnd(latestExpression);

            RelationPredicateBucket bucket = new RelationPredicateBucket(expression);
            bucket.Relations.Add(MediaViewingEntity.Relations.MediaEntityUsingMediaId);

            var path = BuildViewingPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, bucket, 0, null, path);
            }

            return result.Select(v => v.ToDTO()).ToList();
        }

        public IList<MediaViewingDTO> GetLatestMediaViewingsForFolder(Guid folderid, Guid userId, MediaViewingLoadInstruction instructions)
        {
            EntityCollection<MediaViewingEntity> result = new EntityCollection<MediaViewingEntity>();

            // we want any viewings that are under the current folder
            PredicateExpression folderExpression = new PredicateExpression(new FieldCompareSetPredicate(MediaViewingFields.MediaId, null, MediaFolderFileFields.MediaId, null, SetOperator.In, MediaFolderFileFields.MediaFolderId == folderid));

            // and we want only the latest viewings of those pieces of media, we'll do this through a relationship mappings
            PredicateExpression latestExpression = new PredicateExpression(MediaViewingFields.Version == MediaFields.Version);
            latestExpression.AddWithAnd(MediaViewingFields.UserId == userId);

            PredicateExpression expression = new PredicateExpression();
            expression.Add(folderExpression);
            expression.AddWithAnd(latestExpression);

            RelationPredicateBucket bucket = new RelationPredicateBucket(expression);
            bucket.Relations.Add(MediaViewingEntity.Relations.MediaEntityUsingMediaId);

            var path = BuildViewingPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, bucket, 0, null, path);
            }

            return result.Select(v => v.ToDTO()).ToList();
        }

        public MediaViewingDTO SaveMediaViewing(MediaViewingDTO viewing, bool refetch, bool recurse)
        {
            var e = viewing.ToEntity();

            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(e, refetch, recurse);
            }

            return refetch ? e.ToDTO() : viewing;
        }

        public IList<MediaDTO> GetForLabel(string label, MediaLoadInstruction instructions = MediaLoadInstruction.None)
        {
            //
            PredicateExpression labelExpression = new PredicateExpression(LabelFields.Name == label);
            PredicateExpression labelMediaExpression = new PredicateExpression(new FieldCompareSetPredicate(LabelMediaFields.LabelId, null, LabelFields.Id, null, SetOperator.In, labelExpression));
            PredicateExpression expression = new PredicateExpression(new FieldCompareSetPredicate(MediaFields.Id, null, LabelMediaFields.MediaId, null, SetOperator.In, labelMediaExpression));

            EntityCollection<MediaEntity> result = new EntityCollection<MediaEntity>();

            PrefetchPath2 path = BuildPrefetch(instructions);

            SortExpression sort = new SortExpression(MediaFields.Name | SortOperator.Ascending);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), 0, sort, path);
            }

            return result.Select(e => e.ToDTO()).ToList();
        }

    }
}