﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Data;
using ConceptCave.Data.DTOConverters;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Data.Linq;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using SD.LLBLGen.Pro.LinqSupportClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using VirtualMgr.Central;
using ConceptCave.Data.DatabaseSpecific;

namespace ConceptCave.Repository.Injected
{
    public class OPaymentResponseRepository : IPaymentResponseRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public OPaymentResponseRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }

        private PrefetchPath2 BuildPrefetchPath(PaymentResponseLoadInstructions instructions)
        {
            PrefetchPath2 path = new PrefetchPath2((int)EntityType.PaymentResponseEntity);

            if ((instructions & PaymentResponseLoadInstructions.PendingPayment) == PaymentResponseLoadInstructions.PendingPayment)
            {
                var itemsPath = path.Add(PaymentResponseEntity.PrefetchPathPendingPayment);
            }

            return path;
        }

        public PaymentResponseDTO GetPaymentResponseById(Guid id, PaymentResponseLoadInstructions instructions = PaymentResponseLoadInstructions.None)
        {
            var e = new PaymentResponseEntity(id);
            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntity(e, BuildPrefetchPath(instructions));
            }
            return e.IsNew ? null : e.ToDTO();
        }

        public IList<PaymentResponseDTO> GetPaymentResponsesForPendingPayment(Guid pendingPaymentId, PaymentResponseLoadInstructions instructions = PaymentResponseLoadInstructions.None)
        {
            using (var adapter = _fnAdapter())
            {
                var path = BuildPrefetchPath(instructions);
                var lmd = new LinqMetaData(adapter);
                var items = from pr in lmd.PaymentResponse where pr.PendingPaymentId == pendingPaymentId select pr;
                var list = items.WithPath(path).ToList();
                return list.Select(i => i.ToDTO()).ToList();
            }
        }
        public PaymentResponseDTO GetPaymentResponseForTransactionId(string gatewayName, bool sandbox, string transactionId, PaymentResponseLoadInstructions instructions = PaymentResponseLoadInstructions.None)
        {
            using (var adapter = _fnAdapter())
            {
                instructions |= PaymentResponseLoadInstructions.PendingPayment;
                var path = BuildPrefetchPath(instructions);
                var lmd = new LinqMetaData(adapter);
                var items = from pr in lmd.PaymentResponse where pr.PendingPayment.GatewayName == gatewayName && pr.PendingPayment.Sandbox == sandbox && pr.GatewayTransactionId == transactionId select pr;
                var item = items.WithPath(path).FirstOrDefault();
                return item != null ? item.ToDTO() : null;
            }
        }
    }
}
