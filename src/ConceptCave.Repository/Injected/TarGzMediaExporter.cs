﻿using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Repositories;
using ICSharpCode.SharpZipLib.GZip;
using ICSharpCode.SharpZipLib.Tar;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace ConceptCave.Repository.Injected
{
    public partial class TarGzMedia
    {
        public enum MetaDataType
        {
            File,
            Folder,
        }

        private const string ExportFolder = "Export";
        private const string MetaDataFile = "Metadata.json";

        public class PathMetaData
        {
            public MetaDataType metadatatype { get; set; }
            public Guid id { get; set; }
            public Guid? parentId { get; set; }
            public string name { get; set; }
            public string filename { get; set; }
            public string mimetype { get; set; }
            public List<string> exportpath { get; set; }
            public bool useVersioning { get; set; }
            public string description { get; set; }
        }
        public class ExporterFactoryAsync : IMediaExporterFactoryAsync
        {
            public Task<IMediaExporterAsync> CreateAsync(IMediaRepository repo, System.IO.Stream toStream)
            {
                return Task.FromResult((IMediaExporterAsync)(new ExporterAsync(repo, toStream)));
            }
        }

        public class ExporterAsync : IMediaExporterAsync, IDisposable
        {
            private GZipOutputStream _gz;
            private TarOutputStream _tar;
            private IMediaRepository _mediaRepo;

            private List<PathMetaData> _metaData;


            public ExporterAsync(IMediaRepository mediaRepo, Stream toStream)
            {
                _mediaRepo = mediaRepo;
                _gz = new GZipOutputStream(toStream) { IsStreamOwner = false };
                _tar = new TarOutputStream(_gz) { IsStreamOwner = false };
                _metaData = new List<PathMetaData>();
            }

            private async Task ExportFileAsync(MediaFolderFileDTO file, IEnumerable<string> path)
            {
                var data = _mediaRepo.GetById(file.MediaId, RepositoryInterfaces.Enums.MediaLoadInstruction.Data);

                var header = new TarHeader()
                {
                    // Unique filename (in case multiple records have this filename) but use original filename extension so its easily Openable
                    Name = Path.Combine(ExportFolder, string.Join(Path.DirectorySeparatorChar.ToString(), path), data.Filename),
                    Size = data.MediaData.Data.Length,
                };

                _metaData.Add(new PathMetaData()
                {
                    metadatatype = MetaDataType.File,
                    exportpath = path.Concat(new[] { data.Filename }).ToList(),
                    filename = data.Filename,
                    id = file.MediaId,
                    parentId = file.MediaFolderId,
                    name = data.Name,
                    mimetype = data.Type,
                    useVersioning = data.UseVersioning,
                    description = data.Description
                });

                var entry = new TarEntry(header);
                _tar.PutNextEntry(entry);
                try
                {
                    await _tar.WriteAsync(data.MediaData.Data, 0, (int)header.Size);
                }
                finally
                {
                    _tar.CloseEntry();
                }
            }

            private void WriteMetaData()
            {
                var json = _metaData.JsonSerialize();
                var jsonBytes = Encoding.UTF8.GetBytes(json);

                var header = new TarHeader()
                {
                    Name = MetaDataFile,
                    Size = jsonBytes.Length,
                };

                var entry = new TarEntry(header);
                _tar.PutNextEntry(entry);
                try
                {
                    _tar.Write(jsonBytes, 0, (int)header.Size);
                }
                finally
                {
                    _tar.CloseEntry();
                }
            }

            public async Task<string> ExportFolderAsync(Guid fromFolderId)
            {
                var folder = _mediaRepo.GetFolderById(fromFolderId, RepositoryInterfaces.Enums.MediaFolderLoadInstruction.FolderFile);
                await ExportFolderContentAsync(folder, new string[0]);
                return folder.Text;
            }

            private async Task ExportFolderAsync(Guid folderId, IEnumerable<string> path)
            {
                var folder = _mediaRepo.GetFolderById(folderId, RepositoryInterfaces.Enums.MediaFolderLoadInstruction.FolderFile);

                var subPath = path.Concat(new[] { folder.Text });

                _metaData.Add(new PathMetaData()
                {
                    metadatatype = MetaDataType.Folder,
                    exportpath = subPath.ToList(),
                    id = folder.Id,
                    parentId = folder.ParentId,
                    name = folder.Text,
                });

                await ExportFolderContentAsync(folder, subPath);
            }

            private async Task ExportFolderContentAsync(MediaFolderDTO folder, IEnumerable<string> subPath)
            {
                foreach (var file in folder.MediaFolderFiles)
                {
                    await ExportFileAsync(file, subPath);
                }

                var subFolders = _mediaRepo.GetFolders(folder.Id, null, RepositoryInterfaces.Enums.MediaFolderLoadInstruction.FolderFile);
                foreach (var subFolder in subFolders)
                    await ExportFolderAsync(subFolder.Id, subPath);
            }

            public void Close()
            {
                if (_metaData != null)
                {
                    WriteMetaData();
                    _metaData = null;
                }

                if (_tar != null)
                    _tar.Close();

                if (_gz != null)
                    _gz.Close();
            }

            public void Dispose()
            {
                Close();

                if (_tar != null)
                {
                    _tar.Dispose();
                    _tar = null;
                }

                if (_gz != null)
                {
                    _gz.Dispose();
                    _gz = null;
                }
            }
        }
    }
}
