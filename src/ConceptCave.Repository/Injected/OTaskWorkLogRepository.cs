﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Data;
using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.Data.EntityClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using SD.LLBLGen.Pro.ORMSupportClasses;
using ConceptCave.Data.DTOConverters;
using ConceptCave.Checklist;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Data.HelperClasses;
using VirtualMgr.Central;

namespace ConceptCave.Repository.Injected
{
    public class OTaskWorkLogRepository : ITaskWorkLogRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public OTaskWorkLogRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }
        public static PrefetchPath2 BuildPrefetchPath(ProjectJobTaskWorkLogLoadInstructions instructions)
        {
            PrefetchPath2 path = new PrefetchPath2((int)EntityType.ProjectJobTaskWorkLogEntity);

            if ((instructions & ProjectJobTaskWorkLogLoadInstructions.User) == ProjectJobTaskWorkLogLoadInstructions.User)
            {
                path.Add(ProjectJobTaskWorkLogEntity.PrefetchPathUserData);
            }

            if ((instructions & ProjectJobTaskWorkLogLoadInstructions.Task) == ProjectJobTaskWorkLogLoadInstructions.Task)
            {
                var taskPath = path.Add(ProjectJobTaskWorkLogEntity.PrefetchPathProjectJobTask);

                if ((instructions & ProjectJobTaskWorkLogLoadInstructions.TaskSite) == ProjectJobTaskWorkLogLoadInstructions.TaskSite)
                {
                    taskPath.SubPath.Add(ProjectJobTaskEntity.PrefetchPathSite);
                }
            }

            return path;
        }

        public DTO.DTOClasses.ProjectJobTaskWorkLogDTO GetById(Guid id, RepositoryInterfaces.Enums.ProjectJobTaskWorkLogLoadInstructions instructions)
        {
            var result = GetById(new Guid[] { id }, instructions);

            return result.Count > 0 ? result.First() : null;
        }

        public IList<ProjectJobTaskWorkLogDTO> GetById(Guid[] id, ProjectJobTaskWorkLogLoadInstructions instructions)
        {
            EntityCollection<ProjectJobTaskWorkLogEntity> result = new EntityCollection<ProjectJobTaskWorkLogEntity>();

            PredicateExpression expression = new PredicateExpression(ProjectJobTaskWorkLogFields.Id == id);

            PrefetchPath2 path = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), 0, null, path);
            }

            return result.ToList().Select(e => e.ToDTO()).ToList();
        }

        public IList<ProjectJobTaskWorkLogDTO> GetBetweenDates(Guid? companyId, Guid? userId, DateTime startDate, DateTime endDate, RepositoryInterfaces.Enums.ProjectJobTaskWorkLogLoadInstructions instructions)
        {
            EntityCollection<ProjectJobTaskWorkLogEntity> result = new EntityCollection<ProjectJobTaskWorkLogEntity>();

            PredicateExpression expression = new PredicateExpression();
            PredicateExpression subExpression = new PredicateExpression(ProjectJobTaskWorkLogFields.DateCreated >= startDate & ProjectJobTaskWorkLogFields.DateCreated <= endDate);
            if (userId.HasValue == true)
            {
                subExpression.AddWithAnd(ProjectJobTaskWorkLogFields.UserId == userId.Value);
            }

            if (companyId.HasValue == true)
            {
                // we need something like select * from worklog where userid in (select userid from userdata where companyid = companyId)
                PredicateExpression companyExpression = new PredicateExpression(UserDataFields.CompanyId == companyId);
                subExpression.AddWithAnd(new FieldCompareSetPredicate(ProjectJobTaskWorkLogFields.UserId, null, UserDataFields.UserId, null, SetOperator.In, companyExpression));
            }

            expression.Add(subExpression);

            var prefetch = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), prefetch);
            }

            return result.ToList().Select(e => e.ToDTO()).ToList();
        }

        public IList<ProjectJobTaskWorkLogDTO> GetBetweenDates(int rosterId, Guid? userId, DateTime startDate, DateTime endDate, TaskStatusFilter status, RepositoryInterfaces.Enums.ProjectJobTaskWorkLogLoadInstructions instructions, bool forTimesheetsOnly = false)
        {
            EntityCollection<ProjectJobTaskWorkLogEntity> result = new EntityCollection<ProjectJobTaskWorkLogEntity>();

            PredicateExpression expression = new PredicateExpression(ProjectJobTaskFields.RosterId == rosterId);

            if (userId.HasValue)
            {
                expression.AddWithAnd(ProjectJobTaskWorkLogFields.UserId == userId.Value);
            }

            PredicateExpression filterExpression = GetFilterForTaskStatusFilter(status);
            expression.AddWithAnd(filterExpression);

            expression.AddWithAnd(ProjectJobTaskWorkLogFields.DateCreated >= startDate & ProjectJobTaskWorkLogFields.DateCreated < endDate);

            RelationPredicateBucket bucket = new RelationPredicateBucket(expression);
            bucket.Relations.Add(ProjectJobTaskWorkLogEntity.Relations.ProjectJobTaskEntityUsingProjectJobTaskId);

            if (forTimesheetsOnly == true)
            {
                bucket.Relations.Add(ProjectJobTaskEntity.Relations.ProjectJobTaskTypeEntityUsingOriginalTaskTypeId);

                expression.Add(ProjectJobTaskTypeFields.ExcludeFromTimesheets == false);
            }

            var prefetch = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, bucket, prefetch);
            }

            return result.ToList().Select(e => e.ToDTO()).ToList();
        }

        protected PredicateExpression GetFilterForTaskStatusFilter(TaskStatusFilter filter)
        {
            PredicateExpression filterExpression = new PredicateExpression();

            if ((filter & TaskStatusFilter.Unapproved) == TaskStatusFilter.Unapproved)
            {
                filterExpression.Add(ProjectJobTaskFields.Status == (int)ProjectJobTaskStatus.Unapproved);
            }

            if ((filter & TaskStatusFilter.Approved) == TaskStatusFilter.Approved)
            {
                filterExpression.AddWithOr(ProjectJobTaskFields.Status == (int)ProjectJobTaskStatus.Approved);
            }

            if ((filter & TaskStatusFilter.Started) == TaskStatusFilter.Started)
            {
                filterExpression.AddWithOr(ProjectJobTaskFields.Status == (int)ProjectJobTaskStatus.Started);
            }

            if ((filter & TaskStatusFilter.Paused) == TaskStatusFilter.Paused)
            {
                filterExpression.AddWithOr(ProjectJobTaskFields.Status == (int)ProjectJobTaskStatus.Paused);
            }

            if ((filter & TaskStatusFilter.FinishedComplete) == TaskStatusFilter.FinishedComplete)
            {
                filterExpression.AddWithOr(ProjectJobTaskFields.Status == (int)ProjectJobTaskStatus.FinishedComplete);
            }

            if ((filter & TaskStatusFilter.FinishedIncomplete) == TaskStatusFilter.FinishedIncomplete)
            {
                filterExpression.AddWithOr(ProjectJobTaskFields.Status == (int)ProjectJobTaskStatus.FinishedIncomplete);
            }

            if ((filter & TaskStatusFilter.FinishedByManagement) == TaskStatusFilter.FinishedByManagement)
            {
                filterExpression.AddWithOr(ProjectJobTaskFields.Status == (int)ProjectJobTaskStatus.FinishedByManagement);
            }

            if ((filter & TaskStatusFilter.FinishedBySystem) == TaskStatusFilter.FinishedBySystem)
            {
                filterExpression.AddWithOr(ProjectJobTaskFields.Status == (int)ProjectJobTaskStatus.FinishedBySystem);
            }

            if ((filter & TaskStatusFilter.ApprovedContinued) == TaskStatusFilter.ApprovedContinued)
            {
                filterExpression.AddWithOr(ProjectJobTaskFields.Status == (int)ProjectJobTaskStatus.ApprovedContinued);
            }

            if ((filter & TaskStatusFilter.ChangeRosterRequested) == TaskStatusFilter.ChangeRosterRequested)
            {
                filterExpression.AddWithOr(ProjectJobTaskFields.Status == (int)ProjectJobTaskStatus.ChangeRosterRequested);
            }

            if ((filter & TaskStatusFilter.ChangeRosterRejected) == TaskStatusFilter.ChangeRosterRejected)
            {
                filterExpression.AddWithOr(ProjectJobTaskFields.Status == (int)ProjectJobTaskStatus.ChangeRosterRejected);
            }

            if ((filter & TaskStatusFilter.Cancelled) == TaskStatusFilter.Cancelled)
            {
                filterExpression.AddWithOr(ProjectJobTaskFields.Status == (int)ProjectJobTaskStatus.Cancelled);
            }
            return filterExpression;
        }

        public ProjectJobTaskWorkLogDTO GetCurrentActive(Guid taskId, Guid userId, ProjectJobTaskWorkLogLoadInstructions instructions)
        {
            EntityCollection<ProjectJobTaskWorkLogEntity> result = new EntityCollection<ProjectJobTaskWorkLogEntity>();

            PredicateExpression expression = new PredicateExpression(
                ProjectJobTaskWorkLogFields.ProjectJobTaskId == taskId &
                ProjectJobTaskWorkLogFields.UserId == userId &
                ProjectJobTaskWorkLogFields.DateCompleted == System.DBNull.Value
            );

            RelationPredicateBucket bucket = new RelationPredicateBucket(expression);

            PrefetchPath2 path = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, bucket, 1, null, path);
            }

            return result.Count > 0 ? result[0].ToDTO() : null;
        }

        public IList<ProjectJobTaskWorkLogDTO> GetCurrentActive(Guid userId, ProjectJobTaskWorkLogLoadInstructions instructions)
        {
            EntityCollection<ProjectJobTaskWorkLogEntity> result = new EntityCollection<ProjectJobTaskWorkLogEntity>();

            PredicateExpression expression = new PredicateExpression(
                ProjectJobTaskWorkLogFields.UserId == userId &
                ProjectJobTaskWorkLogFields.DateCompleted == System.DBNull.Value);

            PrefetchPath2 path = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), path);

            }

            return result.ToList().Select(e => e.ToDTO()).ToList();
        }

        public ProjectJobTaskWorkLogDTO GetLast(Guid userId, ProjectJobTaskWorkLogLoadInstructions instructions)
        {
            EntityCollection<ProjectJobTaskWorkLogEntity> result = new EntityCollection<ProjectJobTaskWorkLogEntity>();

            PredicateExpression expression = new PredicateExpression(
                ProjectJobTaskWorkLogFields.UserId == userId);

            PrefetchPath2 path = BuildPrefetchPath(instructions);

            SortExpression sort = new SortExpression(ProjectJobTaskWorkLogFields.DateCreated | SortOperator.Descending);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), 1, sort, path);
            }

            return result.Count == 0 ? null : result[0].ToDTO();
        }

        public DTO.DTOClasses.ProjectJobTaskWorkLogDTO Save(DTO.DTOClasses.ProjectJobTaskWorkLogDTO log, bool refetch, bool recurse)
        {
            var e = log.ToEntity();

            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(e, refetch, recurse);
            }

            return refetch ? e.ToDTO() : log;
        }

        public DTO.DTOClasses.ProjectJobTaskWorkLogDTO New(Guid taskId, Guid userId, DateTime dateCreated)
        {
            ProjectJobTaskWorkLogEntity log = new ProjectJobTaskWorkLogEntity();

            log.Id = CombFactory.NewComb();
            log.ProjectJobTaskId = taskId;
            log.UserId = userId;
            log.DateCreated = dateCreated;

            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(log, true);
            }

            return log.ToDTO();
        }
    }
}
