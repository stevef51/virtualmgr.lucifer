﻿using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Repositories;
using ICSharpCode.SharpZipLib.GZip;
using ICSharpCode.SharpZipLib.Tar;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json;
using ConceptCave.SimpleExtensions;

namespace ConceptCave.Repository.Injected
{
    public partial class TarGzMedia
    {
        public class ImporterFactoryAsync : IMediaImporterFactoryAsync
        {
            public Task<IMediaImporterAsync> CreateAsync(IMediaRepository repo, System.IO.Stream fromStream, MediaImportInstructions instructions)
            {
                return Task.FromResult((IMediaImporterAsync)(new ImporterAsync(repo, fromStream, instructions)));
            }
        }

        public class ImporterAsync : IMediaImporterAsync, IDisposable
        {
            private MemoryStream _ms;
            private IMediaRepository _mediaRepo;
            private MediaImportInstructions _instructions;
            private MediaImportResult _result;

            public ImporterAsync(IMediaRepository mediaRepo, Stream fromStream, MediaImportInstructions instructions)
            {
                // We have to create memory copy of the stream so we can Seek
                _ms = new MemoryStream();
                fromStream.CopyTo(_ms);

                _mediaRepo = mediaRepo;
                _instructions = instructions;
            }

            private async Task<List<PathMetaData>> GetMetaDatasAsync()
            {
                List<PathMetaData> result = null;
                await IterateAsync((tar, tarEntry, stop) =>
                    {
                        if (!tarEntry.IsDirectory && tarEntry.Name == MetaDataFile)
                        {
                            result = tar.JsonDeserialize<List<PathMetaData>>();
                            stop.Value = true;
                        }
                        return Task.FromResult(0);
                    });
                return result;
            }

            private async Task IterateAsync(Func<TarInputStream, TarEntry, ValueRef<bool>, Task> fn)
            {
                _ms.Position = 0;
                using (var gz = new GZipInputStream(_ms) { IsStreamOwner = false })
                {
                    using (var tar = new TarInputStream(gz) { IsStreamOwner = false })
                    {
                        ValueRef<bool> stop = new ValueRef<bool>(false);
                        var tarEntry = tar.GetNextEntry();
                        while (tarEntry != null)
                        {
                            await fn(tar, tarEntry, stop);
                            if (stop.Value)
                                return;
                            tarEntry = tar.GetNextEntry();
                        }
                    }
                }
            }

            public async Task<MediaImportResult> ImportFolderAsync(Guid? toFolderId)
            {
                _result = new MediaImportResult();
                var metaDatas = await GetMetaDatasAsync();

                ImportFolders(toFolderId, metaDatas);
                await ImportFilesAsync(toFolderId, metaDatas);

                // Resort the results based on Path (name) ..
                _result.Items = new List<MediaImportResult.Item>(_result.Items.OrderBy(i => i.Name));

                return _result;
            }

            private async Task ImportFilesAsync(Guid? toFolderId, List<PathMetaData> metaDatas)
            {
                await IterateAsync(async (tar, tarEntry, stop) =>
                {
                    var tarPath = tarEntry.Name.Replace("/", Path.DirectorySeparatorChar.ToString());
                    var metaFile = metaDatas.FirstOrDefault(m => string.Compare(tarPath, Path.Combine((new [] { ExportFolder }.Concat(m.exportpath)).ToArray()), true) == 0);
                    if (metaFile != null)
                    {
                        var folderId = FindFolder(toFolderId, metaFile.exportpath.Take(metaFile.exportpath.Count() - 1));
                        if (folderId.HasValue)
                            await ImportFileAsync(folderId.Value, metaFile, tarEntry, tar);
                    }
                });
            }

            private async Task ImportFileAsync(Guid folderId, PathMetaData metaFile, TarEntry tarEntry, TarInputStream tar)
            {
                var resultName = metaFile.exportpath.Take(metaFile.exportpath.Count() - 1).Concat(new [] { metaFile.name });
                MediaImportResult.Item resultItem = new MediaImportResult.Item() { Name = Path.Combine(resultName.ToArray()) };

                byte[] data = new byte[tarEntry.Size];
                await tar.ReadAsync(data, 0, data.Length);

                var file = _mediaRepo.GetFolderFile(folderId, metaFile.name, RepositoryInterfaces.Enums.MediaFolderFileLoadInstruction.Media | RepositoryInterfaces.Enums.MediaFolderFileLoadInstruction.MediaData);
                if (file == null)
                {
                    if (!_instructions.TestOnly)
                    {
                        var media = _mediaRepo.CreateMediaFromPostedFile(data, metaFile.name, metaFile.description, metaFile.filename, metaFile.mimetype, metaFile.useVersioning, _instructions.UserId );
                        file = new MediaFolderFileDTO()
                        {
                            __IsNew = true,
                            Id = Guid.NewGuid(),
                            MediaFolderId = folderId,
                            MediaId = media.Id
                        };
                        _mediaRepo.SaveFolderFile(file, true, true);

                        resultItem.ResultType = MediaImportResult.ResultType.Created;
                        resultItem.Result = "Created";
                    }
                    else
                    {
                        resultItem.ResultType = MediaImportResult.ResultType.WouldCreate;
                        resultItem.Result = "Would Create";
                    }
                }
                else
                {
                    bool dataDifferent = !file.Medium.MediaData.Data.SequenceEqual(data);
                    if (dataDifferent && !_instructions.TestOnly)
                    {
                        _mediaRepo.UpdateMediaFromPostedFile(data, file.MediaId, metaFile.name, metaFile.description, metaFile.filename, metaFile.mimetype, metaFile.useVersioning, _instructions.UserId);

                        resultItem.ResultType = MediaImportResult.ResultType.Updated;
                        resultItem.Result = "Updated";
                    }
                    else if (dataDifferent)
                    {
                        resultItem.ResultType = MediaImportResult.ResultType.WouldUpdate;
                        resultItem.Result = "Would Update";
                    }
                    else
                    {
                        resultItem.ResultType = MediaImportResult.ResultType.NoChange;
                        resultItem.Result = "No Change";
                    }
                }

                _result.Items.Add(resultItem);
            }

            private Guid? FindFolder(Guid? parentId, IEnumerable<string> path)
            {
                foreach(var part in path)
                {
                    var folder = _mediaRepo.GetFolder(parentId, part, RepositoryInterfaces.Enums.MediaFolderLoadInstruction.None);
                    if (folder == null)
                        return null;
                    parentId = folder.Id;
                }
                return parentId;
            }

            private void CreateFolderStructure(Guid? parentId, IEnumerable<string> path)
            {
                MediaImportResult.Item resultItem = new MediaImportResult.Item() { Name = Path.Combine(path.ToArray()) };

                // Try finding the folder by name under our parent-to-be
                var folder = _mediaRepo.GetFolder(parentId, path.First(), RepositoryInterfaces.Enums.MediaFolderLoadInstruction.None, true);

                if (folder == null)
                {
                    if (!_instructions.TestOnly)
                    {
                        folder = new MediaFolderDTO()
                        {
                            __IsNew = true,
                            Id = Guid.NewGuid(),
                            ParentId = parentId,
                            Text = path.First()
                        };
                        _mediaRepo.SaveFolder(folder, true, true);

                        resultItem.Result = "Created Folder";
                        resultItem.ResultType = MediaImportResult.ResultType.Created;
                    }
                    else
                    {
                        resultItem.Result = "Would Create Folder";
                        resultItem.ResultType = MediaImportResult.ResultType.WouldCreate;
                    }
                }
                else
                {
                    resultItem.Result = "Folder Exists";
                    resultItem.ResultType = MediaImportResult.ResultType.NoChange;
                }

                _result.Items.Add(resultItem);

                var subPath = path.Skip(1);
                if (subPath.Any())
                    CreateFolderStructure(folder.Id, subPath);
            }

            private void ImportFolders(Guid? toFolderId, List<PathMetaData> metaDatas)
            {
                foreach(var metaData in from m in metaDatas where m.metadatatype == MetaDataType.Folder orderby m.exportpath.Count select m)
                {
                    CreateFolderStructure(toFolderId, metaData.exportpath);
                }
            }

            public void Dispose()
            {
                if (_ms != null)
                {
                    _ms.Dispose();
                    _ms = null;
                }
            }

        }
    }
}
