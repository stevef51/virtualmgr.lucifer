﻿using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.HelperClasses;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.Data.DTOConverters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Data.Linq;
using SD.LLBLGen.Pro.LinqSupportClasses;
using ConceptCave.DTO.DTOClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.Data;
using VirtualMgr.Central;

namespace ConceptCave.Repository.Injected
{
    public class OSiteFeatureRepository : ISiteFeatureRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public OSiteFeatureRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }
        public static PrefetchPath2 BuildPrefetchPath(SiteFeatureLoadInstructions instructions)
        {
            PrefetchPath2 path = new PrefetchPath2((int)EntityType.SiteFeatureEntity);

            if ((instructions & SiteFeatureLoadInstructions.Site) == SiteFeatureLoadInstructions.Site)
            {
                path.Add(SiteFeatureEntity.PrefetchPathUserData);
            }

            if ((instructions & SiteFeatureLoadInstructions.Feature) == SiteFeatureLoadInstructions.Feature)
            {
                path.Add(SiteFeatureEntity.PrefetchPathWorkLoadingFeature);
            }

            if ((instructions & SiteFeatureLoadInstructions.Units) == SiteFeatureLoadInstructions.Units)
            {
                path.Add(SiteFeatureEntity.PrefetchPathMeasurementUnit);
            }

            return path;
        }
        public static PrefetchPath2 BuildPrefetchPath(SiteAreaLoadInstructions instructions)
        {
            PrefetchPath2 path = new PrefetchPath2((int)EntityType.SiteAreaEntity);

            if ((instructions & SiteAreaLoadInstructions.Site) == SiteAreaLoadInstructions.Site)
            {
                path.Add(SiteAreaEntity.PrefetchPathUserData);
            }

            if ((instructions & SiteAreaLoadInstructions.SiteFeature) == SiteAreaLoadInstructions.SiteFeature)
            {
                path.Add(SiteAreaEntity.PrefetchPathSiteFeatures);
            }

            return path;
        }

        public SiteFeatureDTO GetFeatureById(Guid siteFeatureId, SiteFeatureLoadInstructions instructions)
        {
            SiteFeatureEntity result = new SiteFeatureEntity(siteFeatureId);

            var path = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntity(result, path);
            }

            return result.IsNew == true ? null : result.ToDTO();
        }

        public SiteAreaDTO GetAreaById(Guid siteAreaId, SiteAreaLoadInstructions instructions)
        {
            SiteAreaEntity result = new SiteAreaEntity(siteAreaId);

            var path = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntity(result, path);
            }

            return result.IsNew == true ? null : result.ToDTO();
        }

        public IList<SiteAreaDTO> GetAreasBySiteId(Guid siteId, SiteAreaLoadInstructions instructions)
        {
            EntityCollection<SiteAreaEntity> result = new EntityCollection<SiteAreaEntity>();

            PredicateExpression bySite = new PredicateExpression(SiteAreaFields.SiteId == siteId);
            var path = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(bySite), path);
            }

            return result.ToList().Select(r => r.ToDTO()).ToList();
        }

        public IList<SiteAreaDTO> GetAllAreas(SiteAreaLoadInstructions instructions)
        {
            EntityCollection<SiteAreaEntity> result = new EntityCollection<SiteAreaEntity>();
            var path = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(), path);
            }

            return result.ToList().Select(r => r.ToDTO()).ToList();
        }

        public IList<SiteFeatureDTO> GetFeaturesBySiteId(Guid siteId, SiteFeatureLoadInstructions instructions)
        {
            EntityCollection<SiteFeatureEntity> result = new EntityCollection<SiteFeatureEntity>();

            PredicateExpression bySite = new PredicateExpression(SiteFeatureFields.SiteId == siteId);
            var path = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(bySite), path);
            }

            return result.ToList().Select(r => r.ToDTO()).ToList();
        }

        public SiteFeatureDTO Save(SiteFeatureDTO dto, bool refetch, bool recurse)
        {
            var e = dto.ToEntity();

            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(e, refetch, recurse);
            }

            return refetch ? e.ToDTO() : dto;
        }

        public IList<UserDataDTO> SearchSites(string nameLike, int? pageNumber, int? itemsPerPage, ref int totalItems)
        {
            using (var adapter = _fnAdapter())
            {
                LinqMetaData lmd = new LinqMetaData(adapter);
                var items = (from u in lmd.UserData where u.Name.Contains(nameLike) && u.UserType.IsAsite orderby u.Name select u.ToDTO());
                totalItems = items.Count();
                if (pageNumber.HasValue && itemsPerPage.HasValue)
                    items = items.Skip((pageNumber.Value - 1) * itemsPerPage.Value).Take(itemsPerPage.Value);
                return items.ToList();
            }
        }


        public void DeleteSiteFeature(Guid siteFeatureId)
        {
            using (var adapter = _fnAdapter())
            {
                var entity = new SiteFeatureEntity(siteFeatureId);

                adapter.DeleteEntity(entity);
            }
        }

        public SiteAreaDTO Save(SiteAreaDTO dto, bool refetch, bool recurse)
        {
            var e = dto.ToEntity();

            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(e, refetch, recurse);
            }

            return refetch ? e.ToDTO() : dto;
        }

        public void DeleteSiteArea(Guid siteAreaId)
        {
            using (var adapter = _fnAdapter())
            {
                var entity = new SiteAreaEntity(siteAreaId);

                adapter.DeleteEntity(entity);
            }
        }
    }
}
