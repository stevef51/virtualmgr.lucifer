﻿using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.HelperClasses;
using ConceptCave.RepositoryInterfaces.Repositories;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Data.DTOConverters;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.Data;
using ConceptCave.DTO.DTOClasses;
using VirtualMgr.Central;

namespace ConceptCave.Repository.Injected
{
    public class OTaskTypeRepository : ITaskTypeRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;

        private readonly IFacilityStructureRepository _facilityStructureRepo;

        public OTaskTypeRepository(
            Func<DataAccessAdapter> fnAdapter,
            IFacilityStructureRepository facilityStructureRepo)
        {
            _fnAdapter = fnAdapter;
            _facilityStructureRepo = facilityStructureRepo;
        }

        public static PrefetchPath2 BuildPrefetchPath(ProjectJobTaskTypeLoadInstructions instructions)
        {
            PrefetchPath2 path = new PrefetchPath2((int)EntityType.ProjectJobTaskTypeEntity);

            if ((instructions & ProjectJobTaskTypeLoadInstructions.PublishedResources) == ProjectJobTaskTypeLoadInstructions.PublishedResources)
            {
                path.Add(ProjectJobTaskTypeEntity.PrefetchPathProjectJobTaskTypePublishingGroupResources).SubPath.Add(ProjectJobTaskTypePublishingGroupResourceEntity.PrefetchPathPublishingGroupResource);
            }

            if ((instructions & ProjectJobTaskTypeLoadInstructions.FinishedStatusses) == ProjectJobTaskTypeLoadInstructions.FinishedStatusses)
            {
                path.Add(ProjectJobTaskTypeEntity.PrefetchPathProjectJobTaskTypeFinishedStatuses);
            }

            if ((instructions & ProjectJobTaskTypeLoadInstructions.Statusses) == ProjectJobTaskTypeLoadInstructions.Statusses)
            {
                path.Add(ProjectJobTaskTypeEntity.PrefetchPathProjectJobTaskTypeStatuses);
            }

            if ((instructions & ProjectJobTaskTypeLoadInstructions.Relationships) == ProjectJobTaskTypeLoadInstructions.Relationships)
            {
                path.Add(ProjectJobTaskTypeEntity.PrefetchPathProjectJobTaskTypeSourceRelationships);
                path.Add(ProjectJobTaskTypeEntity.PrefetchPathProjectJobTaskTypeDestinationRelationships);
            }

            if ((instructions & ProjectJobTaskTypeLoadInstructions.Media) == ProjectJobTaskTypeLoadInstructions.Media)
            {
                var mediaPath = path.Add(ProjectJobTaskTypeEntity.PrefetchPathMedium);

                if ((instructions & ProjectJobTaskTypeLoadInstructions.MediaData) == ProjectJobTaskTypeLoadInstructions.MediaData)
                {
                    mediaPath.SubPath.Add(MediaEntity.PrefetchPathMediaData);
                }
            }

            if ((instructions & ProjectJobTaskTypeLoadInstructions.Documentation) == ProjectJobTaskTypeLoadInstructions.Documentation)
            {
                var folderPath = path.Add(ProjectJobTaskTypeEntity.PrefetchPathProjectJobTaskTypeMediaFolders);

                if ((instructions & ProjectJobTaskTypeLoadInstructions.DocumentationFolders) == ProjectJobTaskTypeLoadInstructions.DocumentationFolders)
                {
                    folderPath.SubPath.Add(ProjectJobTaskTypeMediaFolderEntity.PrefetchPathMediaFolder);
                }
            }

            if ((instructions & ProjectJobTaskTypeLoadInstructions.FacilityOverrides) == ProjectJobTaskTypeLoadInstructions.FacilityOverrides)
            {
                var foPath = path.Add(ProjectJobTaskTypeEntity.PrefetchPathProjectJobTaskTypeFacilityOverrides);
            }

            if ((instructions & ProjectJobTaskTypeLoadInstructions.RelatedUserTypes) == ProjectJobTaskTypeLoadInstructions.RelatedUserTypes)
            {
                var subPath = path.Add(ProjectJobTaskTypeEntity.PrefetchPathUserTypeTaskTypeRelationships);
                subPath.SubPath.Add(UserTypeTaskTypeRelationshipEntity.PrefetchPathUserType);
            }
            return path;
        }

        public IList<DTO.DTOClasses.ProjectJobTaskTypeDTO> GetAll(ProjectJobTaskTypeLoadInstructions instrcutions)
        {
            EntityCollection<ProjectJobTaskTypeEntity> result = new EntityCollection<ProjectJobTaskTypeEntity>();

            var path = BuildPrefetchPath(instrcutions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(), path);
            }

            return result.ToList().Select(r => r.ToDTO()).ToList();
        }

        public ProjectJobTaskTypeDTO GetById(Guid id, ProjectJobTaskTypeLoadInstructions instructions)
        {
            ProjectJobTaskTypeEntity result = new ProjectJobTaskTypeEntity(id);

            var path = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntity(result, path);
            }

            if (result.IsNew)
                return null;

            var dto = result.ToDTO();
            if (_facilityStructureRepo != null && (instructions & ProjectJobTaskTypeLoadInstructions.FacilityOverrideStructures) != 0)
            {
                foreach (var fo in dto.ProjectJobTaskTypeFacilityOverrides)
                {
                    fo.FacilityStructure = _facilityStructureRepo.GetById(fo.FacilityStructureId, FacilityStructureLoadInstructions.UserData | FacilityStructureLoadInstructions.ParentDeep);
                }
            }

            return dto;
        }

        public ProjectJobTaskTypeDTO GetByName(string name, ProjectJobTaskTypeLoadInstructions instructions)
        {
            PredicateExpression expression = new PredicateExpression(ProjectJobTaskTypeFields.Name == name);

            EntityCollection<ProjectJobTaskTypeEntity> result = new EntityCollection<ProjectJobTaskTypeEntity>();

            var path = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), 1, null, path);
            }

            return result.Count == 0 ? null : result[0].ToDTO();
        }

        public ProjectJobTaskTypeDTO Save(ProjectJobTaskTypeDTO type, bool refetch, bool recurse)
        {
            var e = type.ToEntity();

            if (string.IsNullOrEmpty(type.ProductCatalogs))
            {
                e.ProductCatalogs = "s";
                e.ProductCatalogs = null;
            }

            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(e, refetch, recurse);
            }

            return refetch ? e.ToDTO() : type;
        }

        public void RemoveTaskTypeResources(Guid taskTypeId, int[] resourceids)
        {
            PredicateExpression expression = new PredicateExpression(ProjectJobTaskTypePublishingGroupResourceFields.TaskTypeId == taskTypeId & ProjectJobTaskTypePublishingGroupResourceFields.PublishingGroupResourceId == resourceids);

            using (var adapter = _fnAdapter())
            {
                adapter.DeleteEntitiesDirectly(typeof(ProjectJobTaskTypePublishingGroupResourceEntity), new RelationPredicateBucket(expression));
            }
        }

        public void RemoveTaskTypeDocumentation(Guid taskTypeId, Guid[] mediaId)
        {
            PredicateExpression expression = new PredicateExpression(ProjectJobTaskTypeMediaFolderFields.TaskTypeId == taskTypeId & ProjectJobTaskTypeMediaFolderFields.MediaFolderId == mediaId);

            using (var adapter = _fnAdapter())
            {
                adapter.DeleteEntitiesDirectly(typeof(ProjectJobTaskTypeMediaFolderEntity), new RelationPredicateBucket(expression));
            }
        }

        public void RemoveTaskTypeFinishedStatuses(Guid taskTypeId, Guid[] finishedstatusesids)
        {
            PredicateExpression expression = new PredicateExpression(ProjectJobTaskTypeFinishedStatusFields.TaskTypeId == taskTypeId & ProjectJobTaskTypeFinishedStatusFields.ProjectJobFinishedStatusId == finishedstatusesids);

            using (var adapter = _fnAdapter())
            {
                adapter.DeleteEntitiesDirectly(typeof(ProjectJobTaskTypeFinishedStatusEntity), new RelationPredicateBucket(expression));
            }

        }

        public void RemoveTaskTypeStatuses(Guid taskTypeId, Guid[] statusesids)
        {
            PredicateExpression expression = new PredicateExpression(ProjectJobTaskTypeStatusFields.TaskTypeId == taskTypeId & ProjectJobTaskTypeStatusFields.StatusId == statusesids);

            using (var adapter = _fnAdapter())
            {
                adapter.DeleteEntitiesDirectly(typeof(ProjectJobTaskTypeStatusEntity), new RelationPredicateBucket(expression));
            }
        }

        public void RemoveTaskTypeRelationships(Guid taskTypeId, Guid[] destinationids)
        {
            PredicateExpression expression = new PredicateExpression(ProjectJobTaskTypeRelationshipFields.ProjectJobTaskTypeSourceId == taskTypeId & ProjectJobTaskTypeRelationshipFields.ProjectJobTaskTypeDestinationId == destinationids);

            using (var adapter = _fnAdapter())
            {
                adapter.DeleteEntitiesDirectly(typeof(ProjectJobTaskTypeRelationshipEntity), new RelationPredicateBucket(expression));
            }
        }


        public void RemoveTaskTypeFacilityOverrides(Guid taskTypeId, int[] facilityStructureIds)
        {
            PredicateExpression expression = new PredicateExpression(ProjectJobTaskTypeFacilityOverrideFields.TaskTypeId == taskTypeId & ProjectJobTaskTypeFacilityOverrideFields.FacilityStructureId == facilityStructureIds);

            using (var adapter = _fnAdapter())
            {
                adapter.DeleteEntitiesDirectly(typeof(ProjectJobTaskTypeFacilityOverrideEntity), new RelationPredicateBucket(expression));
            }
        }

        public void RemoveUserTypeRelationships(Guid taskTypeId, int[] userTypeIds, UserTypeTaskTypeRelationship relationship)
        {
            PredicateExpression expression = new PredicateExpression(UserTypeTaskTypeRelationshipFields.TaskTypeId == taskTypeId & UserTypeTaskTypeRelationshipFields.RelationshipType == (int)relationship & UserTypeTaskTypeRelationshipFields.UserTypeId == userTypeIds);

            using (var adapter = _fnAdapter())
            {
                adapter.DeleteEntitiesDirectly(typeof(UserTypeTaskTypeRelationshipEntity), new RelationPredicateBucket(expression));
            }
        }
    }
}
