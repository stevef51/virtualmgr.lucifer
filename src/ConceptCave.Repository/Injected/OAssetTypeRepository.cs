﻿using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.DTOConverters;
using ConceptCave.RepositoryInterfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualMgr.Central;
using SD.LLBLGen.Pro.ORMSupportClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.Data;
using ConceptCave.Data.Linq;
using ConceptCave.Data.HelperClasses;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Data.DatabaseSpecific;

namespace ConceptCave.Repository.Injected
{
    public class OAssetTypeRepository : IAssetTypeRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public OAssetTypeRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }

        private PrefetchPath2 BuildPrefetchPath(AssetTypeLoadInstructions instructions)
        {
            PrefetchPath2 path = new PrefetchPath2(EntityType.AssetTypeEntity);

            if ((instructions & AssetTypeLoadInstructions.AssetTypeContext) == AssetTypeLoadInstructions.AssetTypeContext)
            {
                path.Add(AssetTypeEntity.PrefetchPathAssetTypeContextPublishedResources);
            }
            if ((instructions & AssetTypeLoadInstructions.Calendar) == AssetTypeLoadInstructions.Calendar)
            {
                var sub = path.Add(AssetTypeEntity.PrefetchPathAssetTypeCalendars);
                if ((instructions & AssetTypeLoadInstructions.CalendarTask) == AssetTypeLoadInstructions.CalendarTask)
                {
                    var subTask = sub.SubPath.Add(AssetTypeCalendarEntity.PrefetchPathAssetTypeCalendarTasks);
                    subTask.SubPath.Add(AssetTypeCalendarTaskEntity.PrefetchPathProjectJobTaskType);
                    subTask.SubPath.Add(AssetTypeCalendarTaskEntity.PrefetchPathHierarchyRole);
                    subTask.SubPath.Add(AssetTypeCalendarTaskEntity.PrefetchPathAssetTypeCalendarTaskLabels);
                }
            }

            return path;
        }

        public DTO.DTOClasses.AssetTypeDTO GetById(int id, AssetTypeLoadInstructions instructions = AssetTypeLoadInstructions.None)
        {
            var e = new AssetTypeEntity(id);
            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntity(e, BuildPrefetchPath(instructions));
            }
            return e.IsNew ? null : e.ToDTO();
        }

        public DTO.DTOClasses.AssetTypeDTO SaveAssetType(DTO.DTOClasses.AssetTypeDTO dto, bool refetch, bool recurse)
        {
            var e = dto.__IsNew ? new AssetTypeEntity() : new AssetTypeEntity(dto.Id);

            using (var adapter = _fnAdapter())
            {
                if (!dto.__IsNew)
                {
                    adapter.FetchEntity(e);
                }

                dto.ToEntity(e);

                adapter.SaveEntity(e, refetch, recurse);
            }

            return refetch ? e.ToDTO() : dto;
        }

        public IList<DTO.DTOClasses.AssetTypeDTO> Search(string nameLike, int? pageNumber, int? itemsPerPage, bool includeArchived, ref int totalItems)
        {
            using (var adapter = _fnAdapter())
            {
                var lmd = new LinqMetaData(adapter);
                var items = from r in lmd.AssetType where r.Name == null || r.Name != null && r.Name.Contains(nameLike) select r.ToDTO();
                items = items.OrderBy(i => i.Name);

                if (!includeArchived)
                    items = items.Where(i => i.Archived == false);

                totalItems = items.Count();
                if (pageNumber.HasValue && itemsPerPage.HasValue)
                    items = items.Skip((pageNumber.Value - 1) * itemsPerPage.Value).Take(itemsPerPage.Value);
                return items.ToList();
            }
        }

        public void Delete(int id, bool archiveIfRequired, out bool archived)
        {
            using (var adapter = _fnAdapter())
            {
                var entity = new AssetTypeEntity(id);
                archived = false;
                try
                {
                    adapter.DeleteEntity(entity);
                }
                catch (Exception ex)
                {
                    if (archiveIfRequired)
                    {
                        adapter.FetchEntity(entity);
                        entity.Archived = true;
                        adapter.SaveEntity(entity, false);
                        archived = true;
                    }
                    else
                        throw ex;
                }
            }
        }

        public void RemoveContextsFromAssetType(int[] publishingGroupResourceId, int assetTypeId)
        {
            PredicateExpression expression = new PredicateExpression(AssetTypeContextPublishedResourceFields.AssetTypeId == assetTypeId);
            expression.AddWithAnd(AssetTypeContextPublishedResourceFields.PublishingGroupResourceId == publishingGroupResourceId);

            using (var adapter = _fnAdapter())
            {
                adapter.DeleteEntitiesDirectly("AssetTypeContextPublishedResourceEntity", new RelationPredicateBucket(expression));
            }
        }

        public IList<AssetTypeContextPublishedResourceDTO> AddContextsToAssetType(int[] publishingGroupResources, int assetTypeId)
        {
            if (publishingGroupResources == null || publishingGroupResources.Length == 0)
            {
                return null;
            }

            EntityCollection<AssetTypeContextPublishedResourceEntity> items = new EntityCollection<AssetTypeContextPublishedResourceEntity>();

            foreach (var id in publishingGroupResources)
            {
                items.Add(new AssetTypeContextPublishedResourceEntity() { PublishingGroupResourceId = id, AssetTypeId = assetTypeId });
            }

            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntityCollection(items, true, false);
            }

            return items.Select(i => i.ToDTO()).ToList();
        }

    }
}
