﻿using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.HelperClasses;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Data.DTOConverters;
using ConceptCave.RepositoryInterfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SD.LLBLGen.Pro.ORMSupportClasses;
using VirtualMgr.Central;

namespace ConceptCave.Repository.Injected
{
    public class OLanguageRepository : ILanguageRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public OLanguageRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }

        public IEnumerable<LanguageEntity> GetAll()
        {
            EntityCollection<LanguageEntity> result = new EntityCollection<LanguageEntity>();

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, null);
            }

            return result;
        }

        public IList<LanguageDTO> AllLanguages()
        {
            EntityCollection<LanguageEntity> result = new EntityCollection<LanguageEntity>();

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, null);
            }

            return result.ToList().Select(e => e.ToDTO()).ToList();
        }


        public LanguageDTO Save(LanguageDTO item, bool refetch, bool recurse)
        {
            var e = item.ToEntity();

            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(e, refetch, recurse);
            }

            return refetch ? e.ToDTO() : item;
        }

        public void Delete(string cultureName)
        {
            using (var adapter = _fnAdapter())
            {
                var filter = new RelationPredicateBucket(new PredicateExpression(LanguageFields.CultureName == cultureName));
                adapter.DeleteEntitiesDirectly(typeof(LanguageEntity), filter);
            }
        }
    }
}
