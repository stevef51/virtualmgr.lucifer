﻿using System;
using System.Linq;
using ConceptCave.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Checklist;
using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Data.DTOConverters;
using ConceptCave.RepositoryInterfaces.Repositories;

namespace ConceptCave.Repository.Injected
{
    public class OCompletedPresentedTypeDimensionRepository : ICompletedPresentedTypeDimensionRepository
    {

        private readonly Func<DataAccessAdapter> _fnAdapter;
        public OCompletedPresentedTypeDimensionRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }

        public CompletedPresentedTypeDimensionDTO GetByType(string type)
        {
            PredicateExpression expression = new PredicateExpression(CompletedPresentedTypeDimensionFields.Type == type);

            EntityCollection<CompletedPresentedTypeDimensionEntity> result = new EntityCollection<CompletedPresentedTypeDimensionEntity>();

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), 1, null);
            }

            return result.FirstOrDefault()?.ToDTO();
        }

        public CompletedPresentedTypeDimensionDTO CreateFromType(string type)
        {
            CompletedPresentedTypeDimensionEntity result = new CompletedPresentedTypeDimensionEntity()
            {
                Id = CombFactory.NewComb(),
                Type = type
            };

            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(result, true);
            }

            return result.ToDTO();
        }

        public CompletedPresentedTypeDimensionDTO GetOrCreate(string type)
        {
            return GetByType(type) ?? CreateFromType(type);
        }
    }
}
