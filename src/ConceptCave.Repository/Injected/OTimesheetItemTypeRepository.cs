﻿using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.HelperClasses;
using ConceptCave.RepositoryInterfaces.Repositories;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualMgr.Central;
using ConceptCave.Data.DTOConverters;
using ConceptCave.Data.DatabaseSpecific;

namespace ConceptCave.Repository.Injected
{
    public class OTimesheetItemTypeRepository : ITimesheetItemTypeRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public OTimesheetItemTypeRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }
        public IList<DTO.DTOClasses.TimesheetItemTypeDTO> GetAll()
        {
            EntityCollection<TimesheetItemTypeEntity> result = new EntityCollection<TimesheetItemTypeEntity>();

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket());
            }

            return result.ToList().Select(r => r.ToDTO()).ToList();
        }

        public DTO.DTOClasses.TimesheetItemTypeDTO GetById(int id)
        {
            TimesheetItemTypeEntity result = new TimesheetItemTypeEntity(id);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntity(result);
            }

            return result.IsNew == true ? null : result.ToDTO();
        }

        public DTO.DTOClasses.TimesheetItemTypeDTO Save(DTO.DTOClasses.TimesheetItemTypeDTO dto, bool refetch, bool recurse)
        {
            var e = dto.ToEntity();

            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(e, refetch, recurse);
            }

            return refetch ? e.ToDTO() : dto;
        }
    }
}
