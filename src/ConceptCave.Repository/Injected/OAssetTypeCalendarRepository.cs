﻿using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.DTOConverters;
using ConceptCave.RepositoryInterfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualMgr.Central;
using SD.LLBLGen.Pro.ORMSupportClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.Data;
using ConceptCave.Data.Linq;
using ConceptCave.Data.HelperClasses;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Data.DatabaseSpecific;

namespace ConceptCave.Repository.Injected
{
    public class OAssetTypeCalendarRepository : IAssetTypeCalendarRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public OAssetTypeCalendarRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }
        private PrefetchPath2 BuildPrefetchPath(AssetTypeCalendarLoadInstructions instructions)
        {
            PrefetchPath2 path = new PrefetchPath2(EntityType.AssetTypeCalendarEntity);

            if ((instructions & AssetTypeCalendarLoadInstructions.CalendarTask) == AssetTypeCalendarLoadInstructions.CalendarTask)
            {
                var sub = path.Add(AssetTypeCalendarEntity.PrefetchPathAssetTypeCalendarTasks);
                sub.Filter = new PredicateExpression(AssetTypeCalendarTaskFields.Archived == false);

                sub.SubPath.Add(AssetTypeCalendarTaskEntity.PrefetchPathProjectJobTaskType);
                sub.SubPath.Add(AssetTypeCalendarTaskEntity.PrefetchPathHierarchyRole);
                sub.SubPath.Add(AssetTypeCalendarTaskEntity.PrefetchPathAssetTypeCalendarTaskLabels);
            }

            return path;
        }

        public DTO.DTOClasses.AssetTypeCalendarDTO GetById(int id, AssetTypeCalendarLoadInstructions instructions = AssetTypeCalendarLoadInstructions.None)
        {
            var e = new AssetTypeCalendarEntity(id);
            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntity(e, BuildPrefetchPath(instructions));
            }
            return e.IsNew ? null : e.ToDTO();
        }

        public DTO.DTOClasses.AssetTypeCalendarDTO Save(DTO.DTOClasses.AssetTypeCalendarDTO dto, bool refetch, bool recurse)
        {
            var e = dto.__IsNew ? new AssetTypeCalendarEntity() : new AssetTypeCalendarEntity(dto.Id);

            using (var adapter = _fnAdapter())
            {
                if (!dto.__IsNew)
                {
                    adapter.FetchEntity(e, BuildPrefetchPath(AssetTypeCalendarLoadInstructions.CalendarTask));
                }

                dto.ToEntity(e);

                adapter.SaveEntity(e, refetch, recurse);
            }

            return refetch ? e.ToDTO() : dto;
        }

        public IList<DTO.DTOClasses.AssetTypeCalendarDTO> Search(string nameLike, int? pageNumber, int? itemsPerPage, bool includeArchived, ref int totalItems)
        {
            using (var adapter = _fnAdapter())
            {
                var lmd = new LinqMetaData(adapter);
                var items = from r in lmd.AssetTypeCalendar where r.Name == null || r.Name != null && r.Name.Contains(nameLike) select r.ToDTO();
                items = items.OrderBy(i => i.Name);

                if (!includeArchived)
                    items = items.Where(i => i.Archived == false);

                totalItems = items.Count();
                if (pageNumber.HasValue && itemsPerPage.HasValue)
                    items = items.Skip((pageNumber.Value - 1) * itemsPerPage.Value).Take(itemsPerPage.Value);
                return items.ToList();
            }
        }

        public void Delete(int id, bool archiveIfRequired, out bool archived)
        {
            using (var adapter = _fnAdapter())
            {
                var entity = new AssetTypeCalendarEntity(id);
                archived = false;
                try
                {
                    adapter.DeleteEntity(entity);
                }
                catch (Exception ex)
                {
                    if (archiveIfRequired)
                    {
                        adapter.FetchEntity(entity);
                        entity.Archived = true;
                        adapter.SaveEntity(entity, false);
                        archived = true;
                    }
                    else
                        throw ex;
                }
            }
        }

        public IList<AssetTypeCalendarDTO> GetAll(AssetTypeCalendarLoadInstructions instructions = AssetTypeCalendarLoadInstructions.None)
        {
            using (var adapter = _fnAdapter())
            {
                PredicateExpression predicate = new PredicateExpression(AssetTypeCalendarFields.Archived == false);

                EntityCollection<AssetTypeCalendarEntity> records = new EntityCollection<AssetTypeCalendarEntity>();

                adapter.FetchEntityCollection(records, new RelationPredicateBucket(predicate), BuildPrefetchPath(instructions));

                return records.Select(r => r.ToDTO()).ToList();
            }

        }
    }
}
