﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.HelperClasses;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.Data.DTOConverters;
using ConceptCave.Data.DatabaseSpecific;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace ConceptCave.Repository.Injected
{
    public class OBlockedRepository : IBlockedRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public OBlockedRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }
        public void Save(BlockedDTO dto)
        {
            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(dto.ToEntity(), false, false);
            }
        }

        public BlockedDTO Load(Guid blockedId, Guid blockedById)
        {
            BlockedEntity entity = new BlockedEntity(blockedById, blockedId);
            using (var adapter = _fnAdapter())
                adapter.FetchEntity(entity);
            return entity.ToDTO();
        }

        public IList<BlockedDTO> GetByWaitingForId(Guid blockedById)
        {
            EntityCollection<BlockedEntity> result = new EntityCollection<BlockedEntity>();

            PredicateExpression expression = new PredicateExpression(BlockedFields.BlockedById == blockedById);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression));
            }

            return result.Select(e => e.ToDTO()).ToList();
        }

        public IList<BlockedDTO> GetByBlockedId(Guid blockedId)
        {
            EntityCollection<BlockedEntity> result = new EntityCollection<BlockedEntity>();

            PredicateExpression expression = new PredicateExpression(BlockedFields.BlockedId == blockedId);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression));
            }

            return result.Select(e => e.ToDTO()).ToList();
        }

        public void Delete(BlockedDTO dto)
        {
            using (var adapter = _fnAdapter())
            {
                adapter.DeleteEntity(dto.ToEntity());
            }
        }
    }
}
