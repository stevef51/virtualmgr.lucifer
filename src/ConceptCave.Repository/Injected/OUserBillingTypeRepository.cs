﻿using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.DTOConverters;
using ConceptCave.RepositoryInterfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using ConceptCave.Data.Linq;
using ConceptCave.Data;
using ConceptCave.Data.DatabaseSpecific;

namespace ConceptCave.Repository.Injected
{
    public class OUserBillingTypeRepository : IUserBillingTypeRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public OUserBillingTypeRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }
        private PrefetchPath2 BuildPrefetchPath(UserBillingTypeLoadInstructions instructions)
        {
            PrefetchPath2 path = new PrefetchPath2(EntityType.UserBillingTypeEntity);
            if ((instructions & UserBillingTypeLoadInstructions.MembershipType) != 0)
            {
                var userTypePath = path.Add(UserBillingTypeEntity.PrefetchPathUserTypes);
                if ((instructions & UserBillingTypeLoadInstructions.AndMembership) != 0)
                {
                    userTypePath.SubPath.Add(UserTypeEntity.PrefetchPathUserDatas);
                }
            }
            return path;
        }

        public UserBillingTypeDTO GetById(int id, UserBillingTypeLoadInstructions instructions)
        {
            UserBillingTypeEntity result = new UserBillingTypeEntity(id);

            var path = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntity(result, path);
            }

            return result.IsNew == true ? null : result.ToDTO();
        }

        public IList<UserBillingTypeDTO> GetAll(UserBillingTypeLoadInstructions instructions)
        {
            EntityCollection<UserBillingTypeEntity> result = new EntityCollection<UserBillingTypeEntity>();

            var path = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(), path);
            }

            return result.ToList().Select(r => r.ToDTO()).ToList();
        }

        public UserBillingTypeDTO Save(UserBillingTypeDTO dto, bool refetch)
        {
            var e = dto.ToEntity();

            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(e, refetch);
            }

            return refetch ? e.ToDTO() : dto;
        }


        public IList<UserBillingTypeDTO> Search(string nameLike, int? pageNumber, int? itemsPerPage, bool includeArchived, ref int totalItems, UserBillingTypeLoadInstructions userBillingTypeLoadInstructions)
        {
            using (var adapter = _fnAdapter())
            {
                LinqMetaData lmd = new LinqMetaData(adapter);
                var items = from e in lmd.UserBillingType where e.Name.Contains(nameLike) select e.ToDTO();
                if (!includeArchived)
                    items = items.Where(i => i.Archived == false);
                items = items.OrderBy(i => i.Name);

                totalItems = items.Count();
                if (pageNumber.HasValue && itemsPerPage.HasValue)
                    items = items.Skip((pageNumber.Value - 1) * itemsPerPage.Value).Take(itemsPerPage.Value);
                return items.ToList();
            }
        }
    }
}
