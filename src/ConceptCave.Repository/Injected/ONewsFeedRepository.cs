﻿using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.HelperClasses;
using ConceptCave.RepositoryInterfaces.Repositories;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Data.DTOConverters;
using ConceptCave.DTO.DTOClasses;
using VirtualMgr.Central;

namespace ConceptCave.Repository.Injected
{
    public class ONewsFeedRepository : INewsFeedRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public ONewsFeedRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }
        public IList<DTO.DTOClasses.NewsFeedDTO> Get(string channel, string program, DateTime? startDate, DateTime? endDate, int count)
        {
            EntityCollection<NewsFeedEntity> result = new EntityCollection<NewsFeedEntity>();

            PredicateExpression expression = new PredicateExpression();
            if (string.IsNullOrEmpty(channel) == false)
            {
                expression.Add(NewsFeedFields.Channel == channel);

                if (string.IsNullOrEmpty(program) == false)
                {
                    expression.Add(NewsFeedFields.Program == program);
                }
            }

            if (startDate.HasValue == true)
            {
                expression.AddWithAnd(NewsFeedFields.DateCreated >= startDate.Value);
            }

            if (endDate.HasValue == true)
            {
                expression.AddWithAnd(NewsFeedFields.DateCreated <= endDate.Value);
            }

            SortExpression sort = new SortExpression(NewsFeedFields.DateCreated | SortOperator.Descending);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), count, sort);
            }

            return result.ToList().Select(e => e.ToDTO()).ToList();
        }

        public IList<NewsFeedDTO> Get(string[] channels, DateTime? validTo, int count)
        {
            EntityCollection<NewsFeedEntity> result = new EntityCollection<NewsFeedEntity>();

            PredicateExpression expression = new PredicateExpression();

            if (channels != null && channels.Length > 0)
            {
                expression.Add(NewsFeedFields.Channel == channels);
            }

            if (validTo.HasValue)
            {
                expression.AddWithAnd(NewsFeedFields.ValidTo >= validTo.Value);
            }

            SortExpression sort = new SortExpression(NewsFeedFields.DateCreated | SortOperator.Descending);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), count, sort);
            }

            return result.ToList().Select(e => e.ToDTO()).ToList();
        }

        public NewsFeedDTO Save(NewsFeedDTO item, bool refetch, bool recurse)
        {
            var e = item.ToEntity();

            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(e, refetch, recurse);
            }

            return refetch ? e.ToDTO() : item;
        }

        public IList<string> AvailableChannels()
        {
            ResultsetFields fields = new ResultsetFields(1);
            fields.DefineField(NewsFeedFields.Channel, 0);

            DataTable table = new DataTable();

            using (var adapter = _fnAdapter())
            {
                adapter.FetchTypedList(fields, table, null, false);
            }

            List<string> result = new List<string>();
            foreach (DataRow row in table.Rows)
            {
                result.Add(row[0] as string);
            }

            return result;
        }

        public IList<string> AvailableProgramsForChannel(string channel)
        {
            ResultsetFields fields = new ResultsetFields(1);
            fields.DefineField(NewsFeedFields.Program, 0);

            DataTable table = new DataTable();

            using (var adapter = _fnAdapter())
            {
                adapter.FetchTypedList(fields, table, new RelationPredicateBucket(new PredicateExpression(NewsFeedFields.Channel == channel)), false);
            }

            List<string> result = new List<string>();
            foreach (DataRow row in table.Rows)
            {
                result.Add(row[0] as string);
            }

            return result;
        }
    }
}
