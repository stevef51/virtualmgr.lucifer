﻿using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.DTOConverters;
using ConceptCave.RepositoryInterfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualMgr.Central;
using SD.LLBLGen.Pro.ORMSupportClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.Data;
using ConceptCave.Data.Linq;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Data.DatabaseSpecific;

namespace ConceptCave.Repository.Injected
{
    public class OTabletRepository : ITabletRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public OTabletRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }
        private PrefetchPath2 BuildPrefetchPath(TabletLoadInstructions instructions)
        {
            PrefetchPath2 path = new PrefetchPath2(EntityType.TabletEntity);
            if ((instructions & TabletLoadInstructions.TabletProfile) != 0)
            {
                path.Add(TabletEntity.PrefetchPathTabletProfile);
            }
            if ((instructions & TabletLoadInstructions.TabletUUID) != 0)
            {
                path.Add(TabletEntity.PrefetchPathTabletUuid);
            }
            return path;
        }

        private PrefetchPath2 BuildPrefetchPath(TabletUuidLoadInstructions instructions)
        {
            PrefetchPath2 path = new PrefetchPath2(EntityType.TabletUuidEntity);
            if ((instructions & TabletUuidLoadInstructions.Tablet) != 0)
            {
                var tabletPath = path.Add(TabletUuidEntity.PrefetchPathTablet);
                if ((instructions & TabletUuidLoadInstructions.AndTabletProfile) != 0)
                {
                    tabletPath.SubPath.Add(TabletEntity.PrefetchPathTabletProfile);
                }
            }
            return path;
        }

        public DTO.DTOClasses.TabletDTO GetTabletById(int id, TabletLoadInstructions instructions)
        {
            TabletEntity e = new TabletEntity(id);
            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntity(e, BuildPrefetchPath(instructions));
            }
            return e.IsNew ? null : e.ToDTO();
        }

        public DTO.DTOClasses.TabletDTO SaveTablet(DTO.DTOClasses.TabletDTO dto, bool refetch, bool recurse)
        {
            TabletEntity e = dto.__IsNew ? new TabletEntity() : new TabletEntity(dto.Id);

            using (var adapter = _fnAdapter())
            {
                if (!dto.__IsNew)
                {
                    adapter.FetchEntity(e);
                }

                dto.ToEntity(e);

                adapter.SaveEntity(e, refetch, recurse);
            }

            return refetch ? e.ToDTO() : dto;
        }


        public IList<DTO.DTOClasses.TabletDTO> Search(string nameLike, int? pageNumber, int? itemsPerPage, bool includeArchived, ref int totalItems)
        {
            using (var adapter = _fnAdapter())
            {
                var lmd = new LinqMetaData(adapter);
                var items = from r in lmd.Tablet where r.Name == null || r.Name != null && r.Name.Contains(nameLike) select r.ToDTO();
                items = items.OrderBy(i => i.Name);

                if (!includeArchived)
                    items = items.Where(i => i.Archived == false);

                totalItems = items.Count();
                if (pageNumber.HasValue && itemsPerPage.HasValue)
                    items = items.Skip((pageNumber.Value - 1) * itemsPerPage.Value).Take(itemsPerPage.Value);
                return items.ToList();
            }
        }

        public DTO.DTOClasses.TabletUuidDTO GetTabletUuid(string uuid, TabletUuidLoadInstructions instructions)
        {
            var e = new TabletUuidEntity(uuid);
            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntity(e, BuildPrefetchPath(instructions));
            }
            return e.IsNew ? null : e.ToDTO();
        }

        public DTO.DTOClasses.TabletUuidDTO SaveTabletUuid(DTO.DTOClasses.TabletUuidDTO dto, bool refetch, bool recurse)
        {
            var e = dto.__IsNew ? new TabletUuidEntity() : new TabletUuidEntity(dto.Uuid);

            using (var adapter = _fnAdapter())
            {
                if (!dto.__IsNew)
                {
                    adapter.FetchEntity(e);
                }

                dto.ToEntity(e);

                adapter.SaveEntity(e, refetch, recurse);
            }

            return refetch ? e.ToDTO() : dto;

        }

        public IList<DTO.DTOClasses.TabletUuidDTO> FindUnassignedTabletUuids()
        {
            using (var adapter = _fnAdapter())
            {
                var lmd = new LinqMetaData(adapter);
                var items = from r in lmd.TabletUuid where r.TabletId == null select r.ToDTO();
                return items.ToList();
            }
        }

        public void Delete(int id, bool archiveIfRequired, out bool archived)
        {
            using (var adapter = _fnAdapter())
            {
                var entity = new TabletEntity(id);
                archived = false;
                try
                {
                    adapter.DeleteEntity(entity);
                }
                catch (Exception ex)
                {
                    if (archiveIfRequired)
                    {
                        adapter.FetchEntity(entity);
                        entity.Archived = true;
                        adapter.SaveEntity(entity, false);
                        archived = true;
                    }
                    else
                        throw ex;
                }
            }
        }


    }
}
