﻿using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.Data.Linq;
using ConceptCave.RepositoryInterfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualMgr.Central;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.DTOConverters;
using ConceptCave.Data.HelperClasses;
using System.Data.SqlClient;
using System.Data;
using ConceptCave.RepositoryInterfaces;
using VirtualMgr.MultiTenant;

namespace ConceptCave.Repository.Injected
{
    public class ODWTenantMasterPopulate : IDWPopulate
    {
        private AppTenant _appTenant;
        private SqlTenantMaster _appTenantMaster;

        private string _trackingTable;

        public ODWTenantMasterPopulate(
            AppTenant appTenant,
            SqlTenantMaster appTenantMaster,
            string trackingTable)
        {
            _appTenant = appTenant;
            _appTenantMaster = appTenantMaster;
            _trackingTable = trackingTable;
        }

        public int Populate(int tenantId, IContinuousProgressCategory progress)
        {
            var getRecordsSP = "dw.GetRecords_" + _trackingTable;
            var mergeSP = "dw.Merge_" + _trackingTable;
            int affectedCount = 0;
            using (var tenantCn = new SqlConnection(_appTenant.ConnectionString))
            {
                tenantCn.Open();

                using (var tenantMasterCn = new SqlConnection(_appTenantMaster.ConnectionString))
                {
                    tenantMasterCn.Open();

                    SqlCommand getTrackingCmd = new SqlCommand("dw.GetTableTracking", tenantMasterCn);
                    getTrackingCmd.CommandType = CommandType.StoredProcedure;
                    getTrackingCmd.Parameters.AddWithValue("@tenantId", tenantId).SqlDbType = SqlDbType.Int;
                    getTrackingCmd.Parameters.AddWithValue("@table", _trackingTable).SqlDbType = SqlDbType.NVarChar;
                    object lastTracking = getTrackingCmd.ExecuteScalar();

                    SqlCommand getRecordsCmd = new SqlCommand(getRecordsSP, tenantCn);
                    getRecordsCmd.CommandTimeout = 10 * 60;     // 10mins
                    getRecordsCmd.CommandType = CommandType.StoredProcedure;
                    getRecordsCmd.Parameters.AddWithValue("@tracking", lastTracking ?? DBNull.Value).SqlDbType = SqlDbType.Timestamp;

                    // 1st recordset should be MAX(Tracking) of the table, so we can track where we are upto
                    // 2nd recordset is the records we are syncing ..
                    var records = new DataTable();
                    object maxTracking = null;
                    using (var recordsReader = getRecordsCmd.ExecuteReader())
                    {
                        if (recordsReader.Read())
                        {
                            maxTracking = recordsReader.GetValue(recordsReader.GetOrdinal("Tracking"));
                        }
                        if (recordsReader.NextResult())
                        {
                            records.Load(recordsReader);
                        }
                    }
                    if (records.Rows.Count > 0)
                    {
                        SqlCommand mergeCmd = new SqlCommand(mergeSP, tenantMasterCn);
                        mergeCmd.CommandType = CommandType.StoredProcedure;
                        mergeCmd.Parameters.AddWithValue("@records", records).SqlDbType = SqlDbType.Structured;
                        affectedCount = (int)mergeCmd.ExecuteScalar();

                        SqlCommand updateTrackingCmd = new SqlCommand("dw.UpdateTableTracking", tenantMasterCn);
                        updateTrackingCmd.CommandType = CommandType.StoredProcedure;
                        updateTrackingCmd.Parameters.AddWithValue("@tenantId", tenantId).SqlDbType = SqlDbType.Int;
                        updateTrackingCmd.Parameters.AddWithValue("@table", _trackingTable).SqlDbType = SqlDbType.NVarChar;
                        updateTrackingCmd.Parameters.AddWithValue("@tracking", maxTracking ?? DBNull.Value).SqlDbType = SqlDbType.Timestamp;
                        updateTrackingCmd.ExecuteNonQuery();
                    }
                }
            }
            return affectedCount;
        }
    }
    public class ODWTenantMasterPopulateBuildingFloorsDimension : ODWTenantMasterPopulate
    {
        public ODWTenantMasterPopulateBuildingFloorsDimension(AppTenant appTenant, SqlTenantMaster appTenantMaster) : base(appTenant, appTenantMaster, "Dim_BuildingFloors")
        {
        }
    }
    public class ODWTenantMasterPopulateCompaniesDimension : ODWTenantMasterPopulate
    {
        public ODWTenantMasterPopulateCompaniesDimension(AppTenant appTenant, SqlTenantMaster appTenantMaster) : base(appTenant, appTenantMaster, "Dim_Companies")
        {
        }
    }
    public class ODWTenantMasterPopulateDatesDimension : ODWTenantMasterPopulate
    {
        public ODWTenantMasterPopulateDatesDimension(AppTenant appTenant, SqlTenantMaster appTenantMaster) : base(appTenant, appTenantMaster, "Dim_Dates")
        {
        }
    }
    public class ODWTenantMasterPopulateDateTimesDimension : ODWTenantMasterPopulate
    {
        public ODWTenantMasterPopulateDateTimesDimension(AppTenant appTenant, SqlTenantMaster appTenantMaster) : base(appTenant, appTenantMaster, "Dim_DateTimes")
        {
        }
    }
    public class ODWTenantMasterPopulateFacilitiesDimension : ODWTenantMasterPopulate
    {
        public ODWTenantMasterPopulateFacilitiesDimension(AppTenant appTenant, SqlTenantMaster appTenantMaster) : base(appTenant, appTenantMaster, "Dim_Facilities")
        {
        }
    }
    public class ODWTenantMasterPopulateFacilityBuildingsDimension : ODWTenantMasterPopulate
    {
        public ODWTenantMasterPopulateFacilityBuildingsDimension(AppTenant appTenant, SqlTenantMaster appTenantMaster) : base(appTenant, appTenantMaster, "Dim_FacilityBuildings")
        {
        }
    }
    public class ODWTenantMasterPopulateFloorZonesDimension : ODWTenantMasterPopulate
    {
        public ODWTenantMasterPopulateFloorZonesDimension(AppTenant appTenant, SqlTenantMaster appTenantMaster) : base(appTenant, appTenantMaster, "Dim_FloorZones")
        {
        }
    }
    public class ODWTenantMasterPopulateRostersDimension : ODWTenantMasterPopulate
    {
        public ODWTenantMasterPopulateRostersDimension(AppTenant appTenant, SqlTenantMaster appTenantMaster) : base(appTenant, appTenantMaster, "Dim_Rosters")
        {
        }
    }
    public class ODWTenantMasterPopulateSitesDimension : ODWTenantMasterPopulate
    {
        public ODWTenantMasterPopulateSitesDimension(AppTenant appTenant, SqlTenantMaster appTenantMaster) : base(appTenant, appTenantMaster, "Dim_Sites")
        {
        }
    }
    public class ODWTenantMasterPopulateTaskCustomStatusDimension : ODWTenantMasterPopulate
    {
        public ODWTenantMasterPopulateTaskCustomStatusDimension(AppTenant appTenant, SqlTenantMaster appTenantMaster) : base(appTenant, appTenantMaster, "Dim_TaskCustomStatus")
        {
        }
    }
    public class ODWTenantMasterPopulateTaskStatusDimension : ODWTenantMasterPopulate
    {
        public ODWTenantMasterPopulateTaskStatusDimension(AppTenant appTenant, SqlTenantMaster appTenantMaster) : base(appTenant, appTenantMaster, "Dim_TaskStatus")
        {
        }
    }
    public class ODWTenantMasterPopulateTaskTypesDimension : ODWTenantMasterPopulate
    {
        public ODWTenantMasterPopulateTaskTypesDimension(AppTenant appTenant, SqlTenantMaster appTenantMaster) : base(appTenant, appTenantMaster, "Dim_TaskTypes")
        {
        }
    }
    public class ODWTenantMasterPopulateUsersDimension : ODWTenantMasterPopulate
    {
        public ODWTenantMasterPopulateUsersDimension(AppTenant appTenant, SqlTenantMaster appTenantMaster) : base(appTenant, appTenantMaster, "Dim_Users")
        {
        }
    }
    public class ODWTenantMasterPopulateZoneSitesDimension : ODWTenantMasterPopulate
    {
        public ODWTenantMasterPopulateZoneSitesDimension(AppTenant appTenant, SqlTenantMaster appTenantMaster) : base(appTenant, appTenantMaster, "Dim_ZoneSites")
        {
        }
    }

    public class ODWTenantMasterPopulateTaskFacts : ODWTenantMasterPopulate
    {
        public ODWTenantMasterPopulateTaskFacts(AppTenant appTenant, SqlTenantMaster appTenantMaster) : base(appTenant, appTenantMaster, "Fact_Tasks")
        {
        }
    }
}
