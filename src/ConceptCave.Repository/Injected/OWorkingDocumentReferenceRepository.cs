﻿using System;
using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.Checklist;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Data.DTOConverters;
using ConceptCave.RepositoryInterfaces.Repositories;

namespace ConceptCave.Repository.Injected
{
    public class OWorkingDocumentReferenceRepository : IWorkingDocumentReferenceRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public OWorkingDocumentReferenceRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }

        public WorkingDocumentReferenceDTO Save(WorkingDocumentReferenceDTO dto, bool refetch, bool recurse)
        {
            if (dto.__IsNew && dto.Id == Guid.Empty)
            {
                dto.Id = CombFactory.NewComb();
            }

            using (var adapter = _fnAdapter())
            {
                var entity = dto.ToEntity();
                adapter.SaveEntity(entity, refetch, recurse);
                return entity.ToDTO();
            }
        }
    }
}
