﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.Data;
using ConceptCave.Core;
using ConceptCave.Core.Coding;
using ConceptCave.Checklist;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Data.DTOConverters;
using ConceptCave.RepositoryInterfaces.Repositories;

namespace ConceptCave.Repository.Injected
{
    public class OResourceRepository : IResourceRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public OResourceRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }

        public IList<ResourceDTO> GetLikeName(string name, string category, ResourceLoadInstructions instructions, int pageNumber = 1, int pageSize = 20)
        {
            EntityCollection<ResourceEntity> result = new EntityCollection<ResourceEntity>();

            PrefetchPath2 path = BuildPrefetch(instructions);

            PredicateExpression expression = new PredicateExpression(ResourceFields.Name % name);

            if (string.IsNullOrEmpty(category) == false)
            {
                var types = (from r in RepositorySectionManager.Current.ItemsForCategory(category) select r.FriendlyName).ToArray();
                expression.AddWithAnd(ResourceFields.Type == types);
            }

            SortExpression sort = new SortExpression(ResourceFields.Name | SortOperator.Ascending);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), 0, sort, path, pageNumber, pageSize);
            }

            return result.Select(e => e.ToDTO()).ToList();
        }

        public ResourceDTO GetById(int id, ResourceLoadInstructions instructions)
        {
            ResourceEntity resource = new ResourceEntity(id);

            PrefetchPath2 path = BuildPrefetch(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntity(resource, path);
            }

            return resource.IsNew ? null : resource.ToDTO();
        }

        public ResourceDTO GetByNodeId(Guid id, ResourceLoadInstructions instructions)
        {
            EntityCollection<ResourceEntity> result = new EntityCollection<ResourceEntity>();

            PredicateExpression expression = new PredicateExpression(ResourceFields.NodeId == id);

            PrefetchPath2 path = BuildPrefetch(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), 1, null, path);
            }

            return result.FirstOrDefault()?.ToDTO();
        }

        private static PrefetchPath2 BuildPrefetch(ResourceLoadInstructions instructions)
        {
            PrefetchPath2 path = new PrefetchPath2((int)EntityType.ResourceEntity);

            if ((instructions & ResourceLoadInstructions.Data) == ResourceLoadInstructions.Data)
            {
                path.Add(ResourceEntity.PrefetchPathResourceData);
            }

            if ((instructions & ResourceLoadInstructions.ResourceLabel) == ResourceLoadInstructions.ResourceLabel)
            {
                IPrefetchPathElement2 sub = path.Add(ResourceEntity.PrefetchPathLabelResources);

                if ((instructions & ResourceLoadInstructions.Label) == ResourceLoadInstructions.Label)
                {
                    sub.SubPath.Add(LabelResourceEntity.PrefetchPathLabel);
                }
            }

            return path;
        }

        public void Remove(Guid id)
        {
            PredicateExpression expression = new PredicateExpression(ResourceFields.NodeId == id);

            using (var adapter = _fnAdapter())
            {
                adapter.DeleteEntitiesDirectly("ResourceEntity", new RelationPredicateBucket(expression));
            }
        }

        public ResourceDTO Save(ResourceDTO resource, bool refetch, bool recurse)
        {
            using (var adapter = _fnAdapter())
            {
                var entity = resource.ToEntity();
                adapter.SaveEntity(entity, refetch, recurse);
                return entity.ToDTO();
            }
        }

        public void Save(IResource resource, ResourceDTO resourceEntity, bool refetch, bool recurse)
        {
            IEncoder encoder = CoderFactory.CreateEncoder();

            string category = RepositorySectionManager.Current.ItemForObjectType(resource.GetType()).FriendlyName;

            encoder.Encode(category, resource);

            if (resourceEntity.__IsNew && resourceEntity.ResourceData == null)
            {
                resourceEntity.ResourceData = new ResourceDataDTO();
            }

            resourceEntity.Type = category; // we'll use this to get things out, so lets make sure we explicitly set it here
            resourceEntity.Name = resource.Name;
            resourceEntity.ResourceData.Data = encoder.ToString();

            Save(resourceEntity, refetch, recurse);
        }

        public void RemoveFromLabels(Guid[] labelIds, int id)
        {
            PredicateExpression expression = new PredicateExpression(LabelResourceFields.LabelId == labelIds);
            expression.AddWithAnd(LabelResourceFields.ResourceId == id);

            using (var adapter = _fnAdapter())
            {
                adapter.DeleteEntitiesDirectly("LabelResourceEntity", new RelationPredicateBucket(expression));
            }
        }

    }
}
