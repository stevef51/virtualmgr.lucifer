﻿using System;
using ConceptCave.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.Data.DTOConverters;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Repository
{
    public class OCompletedWorkingDocumentDimensionRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public OCompletedWorkingDocumentDimensionRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }
        public CompletedWorkingDocumentDimensionDTO GetById(Guid id)
        {
            PredicateExpression expression = new PredicateExpression(CompletedWorkingDocumentDimensionFields.Id == id);

            CompletedWorkingDocumentDimensionEntity result = new CompletedWorkingDocumentDimensionEntity(id);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntity(result);
            }

            return result.IsNew == true ? null : result.ToDTO();
        }

        public CompletedWorkingDocumentDimensionDTO CreateFromWorkingDocument(WorkingDocumentDTO entity)
        {
            CompletedWorkingDocumentDimensionEntity result = new CompletedWorkingDocumentDimensionEntity()
            {
                Id = entity.PublishingGroupResource.Resource.NodeId,
                Name = entity.Name
            };

            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(result, true);
            }

            return result.ToDTO();
        }

        public CompletedWorkingDocumentDimensionDTO GetOrCreate(WorkingDocumentDTO working)
        {
            return GetById(working.PublishingGroupResource.Resource.NodeId) ?? CreateFromWorkingDocument(working);
        }
    }
}
