﻿using ConceptCave.DTO.DTOClasses;
using ConceptCave.Data.DTOConverters;
using ConceptCave.RepositoryInterfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SD.LLBLGen.Pro.LinqSupportClasses;
using ConceptCave.Data;
using ConceptCave.Data.Linq;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Data.DatabaseSpecific;
using System.Data.SqlClient;
using System.Data;
using VirtualMgr.MultiTenant;

namespace ConceptCave.Repository.Injected
{
    public class OAssetTrackerRepository : IAssetTrackerRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        private readonly AppTenant _appTenant;
        public OAssetTrackerRepository(Func<DataAccessAdapter> fnAdapter, AppTenant appTenant)
        {
            _fnAdapter = fnAdapter;
            _appTenant = appTenant;
        }

        private PrefetchPath2 BuildPrefetchPath(AssetTrackerBeaconLoadInstructions instructions)
        {
            PrefetchPath2 path = new PrefetchPath2(EntityType.BeaconEntity);
            if ((instructions & AssetTrackerBeaconLoadInstructions.StaticLocation) != 0)
            {
                path.Add(BeaconEntity.PrefetchPathStaticLocation);
            }
            if ((instructions & AssetTrackerBeaconLoadInstructions.FacilityStructure) != 0)
            {
                path.Add(BeaconEntity.PrefetchPathFacilityStructure);
            }
            if ((instructions & AssetTrackerBeaconLoadInstructions.User) != 0)
            {
                path.Add(BeaconEntity.PrefetchPathUserData);
            }
            if ((instructions & AssetTrackerBeaconLoadInstructions.TaskType) != 0)
            {
                path.Add(BeaconEntity.PrefetchPathProjectJobTaskType);
            }

            return path;
        }

        public BeaconDTO GetBeacon(string beaconId, AssetTrackerBeaconLoadInstructions instructions)
        {
            var result = new BeaconEntity(beaconId);

            var path = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntity(result, path);
            }

            return result.IsNew == true ? null : result.ToDTO();
        }

        public BeaconDTO Save(BeaconDTO dto, bool refetch, bool recurse)
        {
            var e = dto.__IsNew ? new BeaconEntity() : new BeaconEntity(dto.Id);

            using (var adapter = _fnAdapter())
            {
                if (!dto.__IsNew)
                {
                    adapter.FetchEntity(e);
                }

                dto.ToEntity(e);

                adapter.SaveEntity(e, refetch, recurse);
            }

            return refetch ? e.ToDTO() : dto;
        }
        public IList<BeaconDTO> FindUnassignedBeacons()
        {
            using (var adapter = _fnAdapter())
            {
                var lmd = new LinqMetaData(adapter);
                var query = from b in lmd.Beacon where b.AssetId == null && b.FacilityStructureId == null select b.ToDTO();
                query = query.OrderByDescending(b => b.LastButtonPressedUtc).ThenByDescending(b => b.LastContactUtc);
                return query.ToList();
            }
        }


        public BeaconDTO GetBeaconByAssetId(int assetId, AssetTrackerBeaconLoadInstructions loadInstructions)
        {
            using (var adapter = _fnAdapter())
            {
                var lmd = new LinqMetaData(adapter);
                return (from b in lmd.Beacon where b.AssetId == assetId select b.ToDTO()).FirstOrDefault();
            }
        }

        public BeaconDTO GetBeaconByFacilityStructureId(int facilityStructureId, AssetTrackerBeaconLoadInstructions loadInstructions)
        {
            using (var adapter = _fnAdapter())
            {
                var lmd = new LinqMetaData(adapter);
                return (from b in lmd.Beacon where b.FacilityStructureId == facilityStructureId select b.ToDTO()).FirstOrDefault();
            }
        }


        public IList<BeaconDTO> GetFacilityStructureBeacons(int facilityStructureId, AssetTrackerBeaconLoadInstructions instructions)
        {
            using (var adapter = _fnAdapter())
            {
                var path = BuildPrefetchPath(instructions);
                var lmd = new LinqMetaData(adapter);
                var query = from b in lmd.Beacon where b.FacilityStructureId == facilityStructureId select b;
                query = query.WithPath(path);

                query = query.OrderByDescending(b => b.LastButtonPressedUtc).OrderByDescending(b => b.LastContactUtc);

                var result = ((ILLBLGenProQuery)query).Execute<EntityCollection<BeaconEntity>>();

                return (from i in result select i.ToDTO()).ToList();
            }
        }

        public List<BeaconDTO> GetStaticBeacons(IEnumerable<string> beaconIds)
        {
            using (var adapter = _fnAdapter())
            {
                var prefetchPath = BuildPrefetchPath(AssetTrackerBeaconLoadInstructions.StaticLocation);
                var lmd = new LinqMetaData(adapter);
                var query = (from
                                beacon in lmd.Beacon
                             where
                                 beaconIds.Contains(beacon.Id)
                                 && beacon.StaticLocationId != 0
                             select beacon).WithPath(prefetchPath);

                var result = ((ILLBLGenProQuery)query).Execute<EntityCollection<BeaconEntity>>();

                return (from entity in result select entity.ToDTO()).ToList();
            }
        }

        public TabletLocationHistoryDTO Save(TabletLocationHistoryDTO dto, bool refetch, bool recurse)
        {
            using (var adapter = _fnAdapter())
            {
                var e = dto.ToEntity();
                adapter.SaveEntity(e, refetch, recurse);
                return refetch ? e.ToDTO() : dto;
            }
        }

        public TabletLocationHistoryDTO FindLastTabletLocation(string tabletUUID)
        {
            using (var adapter = _fnAdapter())
            {
                var lmd = new LinqMetaData(adapter);
                var query = from r in lmd.TabletLocationHistory
                            where
                                r.TabletUuid == tabletUUID
                            orderby
                                r.Id descending
                            select r.ToDTO();
                return query.FirstOrDefault();
            }
        }

        public void WriteFMARecords(IEnumerable<FMARecord> records, Dictionary<string, BeaconType> outBeacons)
        {
            using (var cn = new SqlConnection(_appTenant.ConnectionString))
            {
                cn.Open();

                // Can't use LLBGLGen since it does not handle Table parameters correctly
                DataTable table = new DataTable();
                table.Columns.AddRange(new[]
                {
                    new DataColumn("RecordType", typeof(int)),
                    new DataColumn("TabletUUID", typeof(string)),
                    new DataColumn("TabletUtc", typeof(DateTime)),
                    new DataColumn("BeaconId", typeof(string)),
                    new DataColumn("Range", typeof(int)),
                    new DataColumn("RangeUtc", typeof(DateTime)),
                    new DataColumn("FirstRangeUtc", typeof(DateTime)),
                    new DataColumn("TaskId", typeof(Guid)),
                    new DataColumn("BatteryPercent", typeof(int))
                });

                foreach (var r in records)
                {
                    table.Rows.Add(r.RecordType, r.TabletUUID, r.TabletUtc, r.BeaconId, r.Range, r.RangeUtc, r.FirstRangeUtc, r.TaskId, r.BatteryPercent);
                }

                SqlCommand cmd = new SqlCommand("asset.WriteFMARecords", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@table", table).SqlDbType = SqlDbType.Structured;

                using (var reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            bool hasStaticLocation = !reader.IsDBNull(1);
                            outBeacons[reader.GetString(0)] = hasStaticLocation ? BeaconType.StaticBeacon : BeaconType.AssetBeacon;
                        }
                    }
                }
            }
        }

        public IList<BeaconDTO> Search(string like, int? pageNumber, int? itemsPerPage, bool includeArchived, ref int totalItems, int? searchFlags, int? includeFlags, AssetTrackerBeaconLoadInstructions instructions = AssetTrackerBeaconLoadInstructions.None)
        {
            var result = new EntityCollection<BeaconEntity>();
            var expression = new PredicateExpression(BeaconFields.Id % like | BeaconFields.Name % like);

            if (searchFlags.HasValue && includeFlags.HasValue)
            {
                var filter = new PredicateExpression();
                if ((searchFlags.Value & (1 << (int)BeaconAttachmentType.UserSite)) != 0)
                {
                    if ((includeFlags.Value & (1 << (int)BeaconAttachmentType.UserSite)) != 0)
                    {
                        filter.Add(BeaconFields.UserId != DBNull.Value);
                    }
                    else
                    {
                        filter.Add(BeaconFields.UserId == DBNull.Value);
                    }
                }
                if ((searchFlags.Value & (1 << (int)BeaconAttachmentType.TaskType)) != 0)
                {
                    if ((includeFlags.Value & (1 << (int)BeaconAttachmentType.TaskType)) != 0)
                    {
                        filter.AddWithAnd(BeaconFields.TaskTypeId != DBNull.Value);
                    }
                    else
                    {
                        filter.AddWithAnd(BeaconFields.TaskTypeId == DBNull.Value);
                    }
                }
                expression.AddWithAnd(filter);
            }
            SortExpression sort = new SortExpression();

            sort.Add(BeaconFields.Id | SortOperator.Ascending);

            using (var adapter = _fnAdapter())
            {
                var predicate = new RelationPredicateBucket(expression);
                totalItems = adapter.GetDbCount(result, predicate);
                adapter.FetchEntityCollection(result, predicate, 0, sort, BuildPrefetchPath(instructions), pageNumber ?? 0, itemsPerPage ?? 0);
            }

            return (from r in result select r.ToDTO()).ToList();
        }
    }
}
