﻿using ConceptCave.Checklist;
using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.HelperClasses;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Repositories;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Data.DTOConverters;
using VirtualMgr.Central;

namespace ConceptCave.Repository.Injected
{
    public class OExecutionHandlerQueueRepository : IExecutionHandlerQueueRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public OExecutionHandlerQueueRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }

        public int GetUnProcessedCountForPrimaryEntity(string handler, string primaryEntityId)
        {
            PredicateExpression expression = new PredicateExpression(ExecutionHandlerQueueFields.Handler == handler &
                ExecutionHandlerQueueFields.PrimaryEntityId == primaryEntityId &
                ExecutionHandlerQueueFields.Processed == false);

            EntityCollection<ExecutionHandlerQueueEntity> result = new EntityCollection<ExecutionHandlerQueueEntity>();

            int r = 0;

            using (var adapter = _fnAdapter())
            {
                r = adapter.GetDbCount(result, new RelationPredicateBucket(expression));
            }

            return r;
        }

        public ExecutionHandlerQueueDTO GetById(Guid id)
        {
            ExecutionHandlerQueueEntity result = new ExecutionHandlerQueueEntity(id);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntity(result);
            }

            return result.ToDTO();
        }

        public void Save(ExecutionHandlerQueueDTO dto, bool refetch, bool recurse)
        {
            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(dto.ToEntity(), refetch, recurse);
            }
        }

        public void Queue(string handler, string method, string primaryEntityId, string data, string exceptionDetails)
        {
            ExecutionHandlerQueueEntity entity = new ExecutionHandlerQueueEntity()
            {
                Id = CombFactory.NewComb(),
                Handler = handler,
                Method = method,
                PrimaryEntityId = primaryEntityId,
                Data = data,
                DateCreated = DateTime.UtcNow,
                Processed = false,
                LatestException = exceptionDetails
            };

            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(entity, false);
            }
        }

        public IList<ExecutionHandlerQueueGroup> GetOpenHandlers()
        {
            PredicateExpression expression = new PredicateExpression(ExecutionHandlerQueueFields.Processed == false);

            EntityCollection<ExecutionHandlerQueueEntity> data = new EntityCollection<ExecutionHandlerQueueEntity>();

            SortExpression sort = new SortExpression(ExecutionHandlerQueueFields.Handler | SortOperator.Ascending);
            sort.Add(ExecutionHandlerQueueFields.PrimaryEntityId | SortOperator.Ascending);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(data, new RelationPredicateBucket(expression), 0, sort);
            }

            List<ExecutionHandlerQueueGroup> result = new List<ExecutionHandlerQueueGroup>();

            foreach (var item in (from r in data select r.Handler).Distinct())
            {
                foreach (var grouping in (from r in data where r.Handler == item select r.PrimaryEntityId).Distinct())
                {
                    var group = new ExecutionHandlerQueueGroup();
                    group.Handler = item;
                    group.PrimaryEntityId = grouping;

                    result.Add(group);
                }
            }

            return result;
        }

        public IList<ExecutionHandlerQueueDTO> GetOpenQueueItems(string handler, string primaryEntityId)
        {
            PredicateExpression expression = new PredicateExpression(ExecutionHandlerQueueFields.Processed == false &
            ExecutionHandlerQueueFields.Handler == handler &
            ExecutionHandlerQueueFields.PrimaryEntityId == primaryEntityId);

            EntityCollection<ExecutionHandlerQueueEntity> result = new EntityCollection<ExecutionHandlerQueueEntity>();

            SortExpression sort = new SortExpression(ExecutionHandlerQueueFields.DateCreated | SortOperator.Ascending);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), 0, sort);
            }

            return result.ToList().Select(e => e.ToDTO()).ToList();
        }
    }
}
