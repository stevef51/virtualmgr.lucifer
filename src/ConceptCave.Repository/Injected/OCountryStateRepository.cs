﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.HelperClasses;
using ConceptCave.RepositoryInterfaces.Repositories;
using SD.LLBLGen.Pro.ORMSupportClasses;
using ConceptCave.Data.DTOConverters;
using ConceptCave.DTO.DTOClasses;
using VirtualMgr.Central;

namespace ConceptCave.Repository.Injected
{
    public class OCountryStateRepository : ICountryStateRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public OCountryStateRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }

        public IList<DTO.DTOClasses.CountryStateDTO> GetAll()
        {
            EntityCollection<CountryStateEntity> result = new EntityCollection<CountryStateEntity>();

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket());
            }

            return result.ToList().Select(e => e.ToDTO()).ToList();
        }

        public CountryStateDTO GetById(int id)
        {
            CountryStateEntity result = new CountryStateEntity(id);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntity(result);
            }

            return result.IsNew ? null : result.ToDTO();
        }
    }
}
