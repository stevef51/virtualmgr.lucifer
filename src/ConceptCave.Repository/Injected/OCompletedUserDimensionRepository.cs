﻿using System;
using System.Linq;
using ConceptCave.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.Data.DTOConverters;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Repositories;

namespace ConceptCave.Repository.Injected
{
    public class OCompletedUserDimensionRepository : ICompletedUserDimensionRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public OCompletedUserDimensionRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }

        public CompletedUserDimensionDTO GetByUsername(string username)
        {
            PredicateExpression expression = new PredicateExpression(CompletedUserDimensionFields.Username == username);

            EntityCollection<CompletedUserDimensionEntity> result = new EntityCollection<CompletedUserDimensionEntity>();

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), 1, null);
            }

            return result.FirstOrDefault()?.ToDTO();
        }

        public CompletedUserDimensionDTO GetById(Guid id)
        {
            PredicateExpression expression = new PredicateExpression(CompletedUserDimensionFields.Id == id);

            EntityCollection<CompletedUserDimensionEntity> result = new EntityCollection<CompletedUserDimensionEntity>();

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), 1, null);
            }

            return result.FirstOrDefault()?.ToDTO();
        }

        public CompletedUserDimensionDTO CreateFromUser(UserDataDTO user)
        {
            CompletedUserDimensionEntity result = new CompletedUserDimensionEntity()
            {
                Username = user.AspNetUser.UserName,
                Name = user.Name,
                Id = user.UserId,
                Email = user.AspNetUser.Email
            };

            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(result, true);
            }

            return result.ToDTO();
        }

        public CompletedUserDimensionDTO GetOrCreate(UserDataDTO user)
        {
            var dto = GetByUsername(user.AspNetUser.UserName);
            if (dto == null)
            {
                dto = GetById(user.UserId);
            }

            if (dto == null)
            {
                dto = CreateFromUser(user);
            }
            else
            {
                bool changes = false;
                if (user.Name != dto.Name)
                {
                    dto.Name = user.Name;
                    changes = true;
                }

                if (user.TimeZone != dto.TimeZone)
                {
                    dto.TimeZone = user.TimeZone;
                    changes = true;
                }

                if (changes == true)
                {
                    using (var adapter = _fnAdapter())
                    {
                        adapter.SaveEntity(dto.ToEntity(), true);
                    }
                }
            }

            return dto;
        }
    }
}
