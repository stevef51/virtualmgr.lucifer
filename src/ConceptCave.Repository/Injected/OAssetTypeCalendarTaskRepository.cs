﻿using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.DTOConverters;
using ConceptCave.RepositoryInterfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualMgr.Central;
using SD.LLBLGen.Pro.ORMSupportClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.Data;
using ConceptCave.Data.Linq;
using ConceptCave.Data.HelperClasses;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Data.DatabaseSpecific;

namespace ConceptCave.Repository.Injected
{
    public class OAssetTypeCalendarTaskRepository : IAssetTypeCalendarTaskRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public OAssetTypeCalendarTaskRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }

        private PrefetchPath2 BuildPrefetchPath(AssetTypeCalendarTaskLoadInstructions instructions)
        {
            PrefetchPath2 path = new PrefetchPath2(EntityType.AssetTypeCalendarTaskEntity);

            if ((instructions & AssetTypeCalendarTaskLoadInstructions.Calendar) == AssetTypeCalendarTaskLoadInstructions.Calendar)
            {
                path.Add(AssetTypeCalendarTaskEntity.PrefetchPathAssetTypeCalendar);
            }
            if ((instructions & AssetTypeCalendarTaskLoadInstructions.HierarchyRole) == AssetTypeCalendarTaskLoadInstructions.HierarchyRole)
            {
                path.Add(AssetTypeCalendarTaskEntity.PrefetchPathHierarchyRole);
            }
            if ((instructions & AssetTypeCalendarTaskLoadInstructions.Task) == AssetTypeCalendarTaskLoadInstructions.Task)
            {
                path.Add(AssetTypeCalendarTaskEntity.PrefetchPathProjectJobTaskType);
            }
            if ((instructions & AssetTypeCalendarTaskLoadInstructions.Labels) == AssetTypeCalendarTaskLoadInstructions.Labels)
            {
                path.Add(AssetTypeCalendarTaskEntity.PrefetchPathAssetTypeCalendarTaskLabels);
            }
            return path;
        }

        public DTO.DTOClasses.AssetTypeCalendarTaskDTO GetById(int id, AssetTypeCalendarTaskLoadInstructions instructions = AssetTypeCalendarTaskLoadInstructions.None)
        {
            var e = new AssetTypeCalendarTaskEntity(id);
            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntity(e, BuildPrefetchPath(instructions));
            }
            return e.IsNew ? null : e.ToDTO();
        }

        public DTO.DTOClasses.AssetTypeCalendarTaskDTO Save(DTO.DTOClasses.AssetTypeCalendarTaskDTO dto, bool refetch, bool recurse)
        {
            var e = dto.__IsNew ? new AssetTypeCalendarTaskEntity() : new AssetTypeCalendarTaskEntity(dto.Id);

            using (var adapter = _fnAdapter())
            {
                if (!dto.__IsNew)
                {
                    adapter.FetchEntity(e);
                }

                dto.ToEntity(e);

                adapter.SaveEntity(e, refetch, recurse);
            }

            return refetch ? e.ToDTO() : dto;
        }
        public void Delete(int id, bool archiveIfRequired, out bool archived)
        {
            using (var adapter = _fnAdapter())
            {
                var entity = new AssetTypeCalendarTaskEntity(id);
                archived = false;
                try
                {
                    adapter.DeleteEntity(entity);
                }
                catch (Exception ex)
                {
                    if (archiveIfRequired)
                    {
                        adapter.FetchEntity(entity);
                        entity.Archived = true;
                        adapter.SaveEntity(entity, false);
                        archived = true;
                    }
                    else
                        throw ex;
                }
            }
        }

        public AssetTypeCalendarTaskLabelDTO SaveLabel(AssetTypeCalendarTaskLabelDTO dto, bool refetch, bool recurse)
        {
            var e = dto.__IsNew ? new AssetTypeCalendarTaskLabelEntity() : new AssetTypeCalendarTaskLabelEntity(dto.AssetTypeCalendarTaskId, dto.LabelId);

            using (var adapter = _fnAdapter())
            {
                if (!dto.__IsNew)
                {
                    adapter.FetchEntity(e);
                }

                dto.ToEntity(e);

                adapter.SaveEntity(e, refetch, recurse);
            }

            return refetch ? e.ToDTO() : dto;

        }

        public void DeleteLabel(int assetTypeCalendarTaskId, Guid labelId)
        {
            using (var adapter = _fnAdapter())
            {
                var entity = new AssetTypeCalendarTaskLabelEntity(assetTypeCalendarTaskId, labelId);
                adapter.DeleteEntity(entity);
            }
        }
    }
}
