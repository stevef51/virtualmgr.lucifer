﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.HelperClasses;
using ConceptCave.RepositoryInterfaces.Repositories;
using SD.LLBLGen.Pro.ORMSupportClasses;
using ConceptCave.Data.DTOConverters;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.Data;
using VirtualMgr.Central;
using VirtualMgr.Common;

namespace ConceptCave.Repository.Injected
{
    public class ORosterRepository : IRosterRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public ORosterRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }
        public static PrefetchPath2 BuildPrefetchPath(RosterLoadInstructions instructions)
        {
            PrefetchPath2 path = new PrefetchPath2((int)EntityType.RosterEntity);

            if ((instructions & RosterLoadInstructions.Conditions) == RosterLoadInstructions.Conditions)
            {
                var conditionsPath = path.Add(RosterEntity.PrefetchPathRosterEventConditions);

                if ((instructions & RosterLoadInstructions.ConditionActions) == RosterLoadInstructions.ConditionActions)
                {
                    conditionsPath.SubPath.Add(RosterEventConditionEntity.PrefetchPathRosterEventConditionActions);
                }
            }

            if ((instructions & RosterLoadInstructions.ScheduleDays) == RosterLoadInstructions.ScheduleDays)
            {
                path.Add(RosterEntity.PrefetchPathProjectJobScheduleDays);
            }

            return path;
        }
        public IList<DTO.DTOClasses.RosterDTO> GetAll()
        {
            return GetAll(RosterLoadInstructions.None);
        }

        public IList<RosterDTO> GetAll(RosterLoadInstructions instructions)
        {
            EntityCollection<RosterEntity> result = new EntityCollection<RosterEntity>();

            PrefetchPath2 path = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(), 0, null, path);
            }

            return result.ToList().Select(e => e.ToDTO()).ToList();
        }

        public RosterDTO FindByName(string name, RosterLoadInstructions instructions)
        {
            EntityCollection<RosterEntity> result = new EntityCollection<RosterEntity>();
            PredicateExpression expression = new PredicateExpression(RosterFields.Name == name);

            PrefetchPath2 path = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), 0, null, path);
            }

            return result.FirstOrDefault()?.ToDTO();
        }

        public IList<RosterDTO> GetForHierarchies(int[] ids, RosterLoadInstructions instructions = RosterLoadInstructions.None)
        {
            PredicateExpression expression = new PredicateExpression(RosterFields.HierarchyId == ids);

            PrefetchPath2 path = BuildPrefetchPath(instructions);

            EntityCollection<RosterEntity> result = new EntityCollection<RosterEntity>();

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), 0, null, path);
            }

            return result.ToList().Select(e => e.ToDTO()).ToList();
        }

        public RosterDTO GetById(int id, RosterLoadInstructions instructions = RosterLoadInstructions.None)
        {
            var result = new RosterEntity(id);

            var path = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntity(result, path);
            }

            return result.IsNew ? null : result.ToDTO();
        }

        public IList<RosterDTO> GetPendingAutomaticScheduleApprovalRosters(DateTime utcTime, RosterLoadInstructions instructions = RosterLoadInstructions.None)
        {
            List<RosterDTO> result = new List<RosterDTO>();

            var path = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                var allRosters = new EntityCollection<RosterEntity>();

                adapter.FetchEntityCollection(allRosters, null, 0, null, path);

                foreach (var roster in allRosters)
                {
                    if (!roster.AutomaticScheduleApprovalTime.HasValue)
                        continue;

                    var rosterTimezone = TimeZoneInfoResolver.ResolveWindows(roster.Timezone);
                    var rosterTime = TimeZoneInfo.ConvertTimeFromUtc(utcTime, rosterTimezone);

                    // this is the time that the roster is to do its schedule approval at, this is a fixed date with dynamic time
                    DateTime referenceTime = new DateTime(2000, 1, 1, rosterTime.Hour, rosterTime.Minute, rosterTime.Second, 0, DateTimeKind.Utc);
                    if (roster.AutomaticScheduleApprovalTime.Value > referenceTime)
                        continue;

                    var utcMaxTime = utcTime.AddHours(-24);
                    if (roster.AutomaticScheduleApprovalTimeLastRun.HasValue && roster.AutomaticScheduleApprovalTimeLastRun.Value > utcMaxTime)
                        continue;

                    result.Add(roster.ToDTO());
                }
            }
            return result;
        }

        public IList<RosterDTO> GetPendingAutomaticEndOfDayRosters(DateTime utcTime, RosterLoadInstructions instructions = RosterLoadInstructions.None)
        {
            List<RosterDTO> result = new List<RosterDTO>();

            var path = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                var allRosters = new EntityCollection<RosterEntity>();

                adapter.FetchEntityCollection(allRosters, null, 0, null, path);

                foreach (var roster in allRosters)
                {
                    if (!roster.AutomaticEndOfDayTime.HasValue)
                        continue;

                    var rosterTimezone = TimeZoneInfoResolver.ResolveWindows(roster.Timezone);
                    var rosterTime = TimeZoneInfo.ConvertTimeFromUtc(utcTime, rosterTimezone);

                    // this is the time that the roster is to do its schedule approval at, this is a fixed date with dynamic time
                    DateTime referenceTime = new DateTime(2000, 1, 1, rosterTime.Hour, rosterTime.Minute, rosterTime.Second, 0, DateTimeKind.Utc);
                    if (roster.AutomaticEndOfDayTime.Value > referenceTime)
                        continue;

                    var utcMaxTime = utcTime.AddHours(-24);
                    if (roster.AutomaticEndOfDayTimeLastRun.HasValue && roster.AutomaticEndOfDayTimeLastRun.Value > utcMaxTime)
                        continue;


                    result.Add(roster.ToDTO());
                }
            }
            return result;
        }

        public RosterDTO Save(RosterDTO roster, bool refetch, bool recurse)
        {
            var e = roster.ToEntity();

            // we need to adjust the automatic times as they get prefixed with the current date
            // let's give it a known starting point
            if (roster.AutomaticScheduleApprovalTime.HasValue == true)
            {
                e.AutomaticScheduleApprovalTime = new DateTime(2000, 1, 1, roster.AutomaticScheduleApprovalTime.Value.Hour, roster.AutomaticScheduleApprovalTime.Value.Minute, roster.AutomaticScheduleApprovalTime.Value.Second, 0, DateTimeKind.Utc);
            }
            else
            {
                e.AutomaticScheduleApprovalTime = new DateTime();
                e.AutomaticScheduleApprovalTime = null;
            }

            if (roster.AutomaticEndOfDayTime.HasValue == true)
            {
                e.AutomaticEndOfDayTime = new DateTime(2000, 1, 1, roster.AutomaticEndOfDayTime.Value.Hour, roster.AutomaticEndOfDayTime.Value.Minute, roster.AutomaticEndOfDayTime.Value.Second, 0, DateTimeKind.Utc);
            }
            else
            {
                e.AutomaticEndOfDayTime = new DateTime();
                e.AutomaticEndOfDayTime = null;
            }

            if (roster.AutomaticScheduleApprovalTimeLastRun.HasValue == false)
            {
                e.AutomaticScheduleApprovalTimeLastRun = new DateTime();
                e.AutomaticScheduleApprovalTimeLastRun = null;
            }

            if (roster.AutomaticEndOfDayTimeLastRun.HasValue == false)
            {
                e.AutomaticEndOfDayTimeLastRun = new DateTime();
                e.AutomaticEndOfDayTimeLastRun = null;
            }

            e.StartTime = new DateTime(2000, 1, 1, roster.StartTime.Hour, roster.StartTime.Minute, roster.StartTime.Second, 0, DateTimeKind.Utc);
            e.EndTime = new DateTime(2000, 1, 1, roster.EndTime.Hour, roster.EndTime.Minute, roster.EndTime.Second, 0, DateTimeKind.Utc);

            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(e, refetch, recurse);
            }

            return refetch ? e.ToDTO() : roster;
        }

        public IList<RosterProjectJobTaskEventDTO> GetAllTaskEventTypes()
        {
            EntityCollection<RosterProjectJobTaskEventEntity> result = new EntityCollection<RosterProjectJobTaskEventEntity>();

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, null);
            }

            return result.ToList().Select(r => r.ToDTO()).ToList();
        }

        public void RemoveRosterEventConditions(int[] ids)
        {
            PredicateExpression expression = new PredicateExpression(RosterEventConditionFields.Id == ids);

            using (var adapter = _fnAdapter())
            {
                adapter.DeleteEntitiesDirectly(typeof(RosterEventConditionEntity), new RelationPredicateBucket(expression));
            }
        }

        public void RemoveRosterEventConditionActions(int[] ids)
        {
            PredicateExpression expression = new PredicateExpression(RosterEventConditionActionFields.Id == ids);

            using (var adapter = _fnAdapter())
            {
                adapter.DeleteEntitiesDirectly(typeof(RosterEventConditionActionEntity), new RelationPredicateBucket(expression));
            }
        }
    }
}
