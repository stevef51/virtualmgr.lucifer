﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConceptCave.RepositoryInterfaces.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Data;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Data.DTOConverters;
using ConceptCave.RepositoryInterfaces.Repositories;

namespace ConceptCave.Repository.Injected
{
    public class OPublishingGroupActorRepository : IPublishingGroupActorRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;

        public OPublishingGroupActorRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }
        public PublishingGroupActorDTO Save(PublishingGroupActorDTO publish, bool refetch, bool recurse)
        {
            using (var adapter = _fnAdapter())
            {
                var e = publish.ToEntity();
                adapter.SaveEntity(e, refetch, recurse);
                return e.ToDTO();
            }
        }

        private static PrefetchPath2 BuildPrefetchPath(PublishingGroupActorLoadInstruction instructions)
        {
            PrefetchPath2 path = new PrefetchPath2((int)EntityType.PublishingGroupActorEntity);

            if ((instructions & PublishingGroupActorLoadInstruction.Membership) == PublishingGroupActorLoadInstruction.Membership)
            {
                path.Add(PublishingGroupActorEntity.PrefetchPathUserData);
            }

            if ((instructions & PublishingGroupActorLoadInstruction.PublishingGroup) == PublishingGroupActorLoadInstruction.PublishingGroup)
            {
                IPrefetchPathElement2 sub = path.Add(PublishingGroupActorEntity.PrefetchPathPublishingGroup);

                if ((instructions & PublishingGroupActorLoadInstruction.Resource) == PublishingGroupActorLoadInstruction.Resource)
                {
                    sub.SubPath.Add(PublishingGroupEntity.PrefetchPathPublishingGroupResources).SubPath.Add(PublishingGroupResourceEntity.PrefetchPathResource);
                }
            }
            return path;
        }

        public IList<PublishingGroupActorDTO> GetForMembershipAndType(Guid membershipId, PublishingGroupActorType type, PublishingGroupActorLoadInstruction instructions)
        {
            PredicateExpression expression = new PredicateExpression(PublishingGroupActorFields.UserId == membershipId);
            expression.AddWithAnd(new FieldCompareSetPredicate(PublishingGroupActorFields.PublishingTypeId, null, PublishingGroupActorTypeFields.Id, null, SetOperator.In, PublishingGroupActorTypeFields.Name == type.ToString()));

            EntityCollection<PublishingGroupActorEntity> result = new EntityCollection<PublishingGroupActorEntity>();

            PrefetchPath2 path = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), path);
            }

            return result.Select(e => e.ToDTO()).ToList();
        }


        public PublishingGroupActorDTO GetById(int publishedGroupId, Guid membershipId, PublishingGroupActorType type, PublishingGroupActorLoadInstruction instructions)
        {
            PredicateExpression expression = new PredicateExpression(PublishingGroupActorFields.PublishingGroupId == publishedGroupId & PublishingGroupActorFields.UserId == membershipId);
            expression.AddWithAnd(new FieldCompareSetPredicate(PublishingGroupActorFields.PublishingTypeId, null, PublishingGroupActorTypeFields.Id, null, SetOperator.In, PublishingGroupActorTypeFields.Name == type.ToString()));

            EntityCollection<PublishingGroupActorEntity> result = new EntityCollection<PublishingGroupActorEntity>();

            PrefetchPath2 path = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), path);
            }

            return result.FirstOrDefault()?.ToDTO();
        }

        public PublishingGroupActorDTO Add(int publishedGroupId, Guid membershipId, PublishingGroupActorType type)
        {
            PredicateExpression expression = new PredicateExpression(PublishingGroupActorTypeFields.Name == type.ToString());
            EntityCollection<PublishingGroupActorTypeEntity> types = new EntityCollection<PublishingGroupActorTypeEntity>();

            PublishingGroupActorEntity result = new PublishingGroupActorEntity();
            result.PublishingGroupId = publishedGroupId;
            result.UserId = membershipId;

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(types, new RelationPredicateBucket(expression), 1);

                if (types.Count == 0)
                {
                    throw new InvalidOperationException(string.Format("{0} could not be found as an actor type"));
                }

                result.PublishingTypeId = types[0].Id;

                return Save(result.ToDTO(), true, false);
            }
        }

        public void Remove(int publishedGroupId, Guid membershipId, PublishingGroupActorType type)
        {
            PredicateExpression expression = new PredicateExpression(PublishingGroupActorFields.PublishingGroupId == publishedGroupId & PublishingGroupActorFields.UserId == membershipId);
            expression.AddWithAnd(new FieldCompareSetPredicate(PublishingGroupActorFields.PublishingTypeId, null, PublishingGroupActorTypeFields.Id, null, SetOperator.In, PublishingGroupActorTypeFields.Name == type.ToString()));

            using (var adapter = _fnAdapter())
            {
                adapter.DeleteEntitiesDirectly("PublishingGroupActorEntity", new RelationPredicateBucket(expression));
            }
        }
    }
}
