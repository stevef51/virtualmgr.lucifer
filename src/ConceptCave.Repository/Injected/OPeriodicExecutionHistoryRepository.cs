﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Data.DTOConverters;
using ConceptCave.Data.Linq;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Repositories;
using VirtualMgr.Central;
using ConceptCave.Data.DatabaseSpecific;

namespace ConceptCave.Repository.Injected
{
    public class OPeriodicExecutionHistoryRepository : IPeriodicExecutionHistoryRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public OPeriodicExecutionHistoryRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }

        public PeriodicExecutionHistoryDTO GetUnfinished()
        {
            using (var adapter = _fnAdapter())
            {
                var lmd = new LinqMetaData(adapter);

                return (from i in lmd.PeriodicExecutionHistory where !i.FinishUtc.HasValue select i).FirstOrDefault()?.ToDTO();
            }
        }

        public PeriodicExecutionHistoryDTO Save(PeriodicExecutionHistoryDTO dto, bool refetch, bool recurse)
        {
            var e = dto.ToEntity();
            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(e, refetch, recurse);
            }
            return refetch ? e.ToDTO() : dto;
        }
    }
}
