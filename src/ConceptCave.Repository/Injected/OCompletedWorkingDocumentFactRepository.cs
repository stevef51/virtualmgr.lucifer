﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using ConceptCave.Data;
using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Data.DTOConverters;
using ConceptCave.RepositoryInterfaces.Repositories;

namespace ConceptCave.Repository.Injected
{
    public class OCompletedWorkingDocumentFactRepository : ICompletedWorkingDocumentFactRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public OCompletedWorkingDocumentFactRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }

        public void Save(CompletedWorkingDocumentFactDTO dto, bool refetch, bool recurse)
        {
            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(dto.ToEntity(), refetch, recurse);
            }
        }

        public CompletedWorkingDocumentFactDTO Get(Guid workingDocumentId, CompletedWorkingDocumentFactLoadInstructions instructions)
        {
            CompletedWorkingDocumentFactEntity result = new CompletedWorkingDocumentFactEntity(workingDocumentId);

            PrefetchPath2 path = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntity(result, path);
            }

            return result.IsNew == true ? null : result.ToDTO();
        }

        public IList<CompletedWorkingDocumentFactDTO> Get(DateTime startDate, DateTime endDate, CompletedWorkingDocumentFactLoadInstructions instructions)
        {
            EntityCollection<CompletedWorkingDocumentFactEntity> result = new EntityCollection<CompletedWorkingDocumentFactEntity>();

            PredicateExpression expression = new PredicateExpression(new FieldBetweenPredicate(CompletedWorkingDocumentFactFields.DateStarted, null, startDate, endDate));

            PrefetchPath2 path = BuildPrefetchPath(instructions);

            SortExpression sort = new SortExpression(CompletedWorkingDocumentFactFields.DateStarted | SortOperator.Descending);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), 0, sort, path);
            }

            return result.Select(e => e.ToDTO()).ToList();
        }

        private static PrefetchPath2 BuildPrefetchPath(CompletedWorkingDocumentFactLoadInstructions instructions)
        {
            PrefetchPath2 path = new PrefetchPath2((int)EntityType.CompletedWorkingDocumentFactEntity);
            if ((instructions & CompletedWorkingDocumentFactLoadInstructions.CompletedUserDimension) == CompletedWorkingDocumentFactLoadInstructions.CompletedUserDimension)
            {
                path.Add(CompletedWorkingDocumentFactEntity.PrefetchPathRevieweeUser);
                path.Add(CompletedWorkingDocumentFactEntity.PrefetchPathReviewerUser);
            }

            if ((instructions & CompletedWorkingDocumentFactLoadInstructions.CompletedLabelDimension) == CompletedWorkingDocumentFactLoadInstructions.CompletedLabelDimension)
            {
                path.Add(CompletedWorkingDocumentFactEntity.PrefetchPathCompletedLabelWorkingDocumentDimensions).SubPath.Add(CompletedLabelWorkingDocumentDimensionEntity.PrefetchPathCompletedLabelDimension);
            }

            if ((instructions & CompletedWorkingDocumentFactLoadInstructions.CompletedUserDimension) == CompletedWorkingDocumentFactLoadInstructions.CompletedUserDimension)
            {
                path.Add(CompletedWorkingDocumentFactEntity.PrefetchPathCompletedWorkingDocumentDimension);
            }

            return path;
        }
    }
}
