﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Data;
using ConceptCave.Data.DTOConverters;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Data.Linq;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using SD.LLBLGen.Pro.LinqSupportClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using VirtualMgr.Central;
using ConceptCave.Data.DatabaseSpecific;

namespace ConceptCave.Repository.Injected
{
    public class OPendingPaymentRepository : IPendingPaymentRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public OPendingPaymentRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }

        private PrefetchPath2 BuildPrefetchPath(PendingPaymentLoadInstructions instructions)
        {
            PrefetchPath2 path = new PrefetchPath2((int)EntityType.PendingPaymentEntity);

            if ((instructions & PendingPaymentLoadInstructions.PaymentResponses) == PendingPaymentLoadInstructions.PaymentResponses)
            {
                var itemsPath = path.Add(PendingPaymentEntity.PrefetchPathPaymentResponses);
            }

            return path;
        }

        public IList<PendingPaymentDTO> FindScheduled(string gatewayName, DateTime fromUtc, PendingPaymentLoadInstructions instructions = PendingPaymentLoadInstructions.None)
        {
            using (var adapter = _fnAdapter())
            {
                var lmd = new LinqMetaData(adapter);
                var query = from pp in lmd.PendingPayment.WithPath(BuildPrefetchPath(instructions)) where 
                            pp.GatewayName == gatewayName && 
                            pp.ActivatedDateUtc.HasValue && 
                            pp.DueDateUtc <= fromUtc &&
                            pp.Status == (int)PendingPaymentStatus.Scheduled
                            select pp;

                query = query.OrderBy(pp => pp.OrderId).ThenBy(pp => pp.OrderBy);
                return query.Select(pp => pp.ToDTO()).ToList();
            }
        }

        public PendingPaymentDTO GetPendingPaymentById(Guid id, PendingPaymentLoadInstructions instructions = PendingPaymentLoadInstructions.None)
        {
            var e = new PendingPaymentEntity(id);
            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntity(e, BuildPrefetchPath(instructions));
            }
            return e.IsNew ? null : e.ToDTO();
        }

        public PendingPaymentDTO Save(PendingPaymentDTO dto, bool refetch, bool recurse)
        {
            var e = dto.ToEntity();
            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(e, refetch, recurse);
            }
            return refetch ? e.ToDTO() : dto;
        }

        public IEnumerable<PendingPaymentDTO> Save(IEnumerable<PendingPaymentDTO> dtos, bool refetch, bool recurse)
        {
            var entities = new EntityCollection<PendingPaymentEntity>();
            foreach (var dto in dtos)
            {
                entities.Add(dto.ToEntity());
            }

            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntityCollection(entities, refetch, recurse);
            }
            return refetch ? from e in entities select e.ToDTO() : dtos;
        }

        public IList<PendingPaymentDTO> GetByOrderId(Guid orderId, PendingPaymentLoadInstructions instructions = PendingPaymentLoadInstructions.None)
        {
            using (var adapter = _fnAdapter())
            {
                var lmd = new LinqMetaData(adapter);
                var query = from pp in lmd.PendingPayment.WithPath(BuildPrefetchPath(instructions)) where pp.OrderId == orderId orderby pp.OrderBy select pp;

                return query.Select(pp => pp.ToDTO()).ToList();
            }
        }

        public IList<PendingPaymentDTO> FindProcessing(string gatewayName)
        {
            using (var adapter = _fnAdapter())
            {
                var lmd = new LinqMetaData(adapter);
                var query = from pp in lmd.PendingPayment
                            where
                                pp.GatewayName == gatewayName &&
                                pp.Status == (int)PendingPaymentStatus.Processing &&
                                pp.Archived == false
                            select pp;

                query = query.OrderBy(pp => pp.OrderId).ThenBy(pp => pp.OrderBy);
                return query.Select(pp => pp.ToDTO()).ToList();
            }
        }

    }
}
