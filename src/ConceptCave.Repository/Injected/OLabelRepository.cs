﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.Data.DTOConverters;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Data.Linq;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace ConceptCave.Repository.Injected
{
    public class OLabelRepository : ILabelRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public OLabelRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }

        public IList<LabelDTO> GetAll(LabelFor labelsFor)
        {
            EntityCollection<LabelEntity> result = new EntityCollection<LabelEntity>();

            PredicateExpression expression = new PredicateExpression();

            if ((labelsFor & LabelFor.Users) == LabelFor.Users)
            {
                expression.AddWithOr(LabelFields.ForUsers == true);
            }

            if ((labelsFor & LabelFor.Media) == LabelFor.Media)
            {
                expression.AddWithOr(LabelFields.ForMedia == true);
            }

            if ((labelsFor & LabelFor.Resources) == LabelFor.Resources)
            {
                expression.AddWithOr(LabelFields.ForResources == true);
            }
            if ((labelsFor & LabelFor.Questions) == LabelFor.Questions)
            {
                expression.AddWithOr(LabelFields.ForQuestions == true);
            }
            if ((labelsFor & LabelFor.Tasks) == LabelFor.Tasks)
            {
                expression.AddWithOr(LabelFields.ForTasks == true);
            }

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression));
            }

            return result.ToList().Select(e => e.ToDTO()).ToList();
        }

        public LabelDTO GetByName(string name)
        {
            using (var adapter = _fnAdapter())
            {
                var data = new LinqMetaData(adapter);
                return (from lbl in data.Label
                        where lbl.Name == name
                        select lbl).FirstOrDefault().ToDTO();
            }
        }

        public IList<LabelDTO> GetByIds(IList<Guid> ids)
        {
            using (var adapter = _fnAdapter())
            {
                var data = new LinqMetaData(adapter);
                return (from lbl in data.Label
                        where ids.Contains(lbl.Id)
                        select lbl).ToList().Select(e => e.ToDTO()).ToList();
            }
        }

        public LabelDTO GetById(Guid id)
        {
            using (var adapter = _fnAdapter())
            {
                var data = new LinqMetaData(adapter);
                return (from lbl in data.Label
                        where lbl.Id == id
                        select lbl).Single().ToDTO();
            }
        }

        public LabelDTO Save(LabelDTO label, bool refetch)
        {
            using (var adapter = _fnAdapter())
            {
                var e = label.ToEntity();
                adapter.SaveEntity(e, refetch, false);
                return refetch ? e.ToDTO() : label;
            }
        }

        public void Remove(Guid id)
        {
            PredicateExpression expression = new PredicateExpression(LabelFields.Id == id);

            using (var adapter = _fnAdapter())
            {
                adapter.DeleteEntitiesDirectly("LabelEntity", new RelationPredicateBucket(expression));
            }
        }
    }
}
