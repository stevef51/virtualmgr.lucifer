﻿using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.HelperClasses;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.Data.DTOConverters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Data.Linq;
using SD.LLBLGen.Pro.LinqSupportClasses;
using ConceptCave.DTO.DTOClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.Data;
using VirtualMgr.Central;

namespace ConceptCave.Repository.Injected
{
    public class OWorkLoadingBookRepository : IWorkLoadingBookRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public OWorkLoadingBookRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }
        public static PrefetchPath2 BuildPrefetchPath(WorkLoadingBookLoadInstructions instructions)
        {
            PrefetchPath2 path = new PrefetchPath2((int)EntityType.WorkLoadingBookEntity);

            if ((instructions & WorkLoadingBookLoadInstructions.Standard) != 0)
            {
                var subPath = path.Add(WorkLoadingBookEntity.PrefetchPathWorkLoadingStandards);

                if ((instructions & WorkLoadingBookLoadInstructions.StandardActivities) != 0)
                {
                    subPath.SubPath.Add(WorkLoadingStandardEntity.PrefetchPathWorkLoadingActivity);
                }

                if ((instructions & WorkLoadingBookLoadInstructions.StandardFeatures) != 0)
                {
                    subPath.SubPath.Add(WorkLoadingStandardEntity.PrefetchPathWorkLoadingFeature);
                }
            }

            return path;
        }

        public static PrefetchPath2 BuildPrefetchPath(WorkLoadingStandardLoadInstructions instructions)
        {
            PrefetchPath2 path = new PrefetchPath2((int)EntityType.WorkLoadingStandardEntity);

            if ((instructions & WorkLoadingStandardLoadInstructions.Books) == WorkLoadingStandardLoadInstructions.Books)
            {
                path.Add(WorkLoadingStandardEntity.PrefetchPathWorkLoadingBook);
            }

            if ((instructions & WorkLoadingStandardLoadInstructions.Features) == WorkLoadingStandardLoadInstructions.Features)
            {
                path.Add(WorkLoadingStandardEntity.PrefetchPathWorkLoadingFeature);
            }

            if ((instructions & WorkLoadingStandardLoadInstructions.Activities) == WorkLoadingStandardLoadInstructions.Activities)
            {
                path.Add(WorkLoadingStandardEntity.PrefetchPathWorkLoadingActivity);
            }

            if ((instructions & WorkLoadingStandardLoadInstructions.Units) == WorkLoadingStandardLoadInstructions.Units)
            {
                path.Add(WorkLoadingStandardEntity.PrefetchPathMeasurementUnit);
            }

            return path;
        }

        public WorkLoadingBookDTO GetById(Guid id, RepositoryInterfaces.Enums.WorkLoadingBookLoadInstructions instructions)
        {
            WorkLoadingBookEntity result = new WorkLoadingBookEntity(id);

            var path = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntity(result, path);
            }

            var dto = result.IsNew == true ? null : result.ToDTO();
            if (dto != null)
            {
                // For some reason LLBL prefetch paths go deeper than specified, remove unwanted depth ..
                foreach (var s in dto.WorkLoadingStandards)
                {
                    if (s.WorkLoadingActivity != null)
                    {
                        s.WorkLoadingActivity.WorkLoadingStandards = null;
                    }
                    if (s.WorkLoadingFeature != null)
                    {
                        s.WorkLoadingFeature.SiteFeatures = null;
                        s.WorkLoadingFeature.WorkLoadingStandards = null;
                    }
                }
            }
            return dto;
        }

        public IList<WorkLoadingBookDTO> GetAllBooks(RepositoryInterfaces.Enums.WorkLoadingBookLoadInstructions instructions)
        {
            EntityCollection<WorkLoadingBookEntity> result = new EntityCollection<WorkLoadingBookEntity>();

            var path = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(), path);
            }

            return result.ToList().Select(r => r.ToDTO()).ToList();
        }

        public IList<WorkLoadingBookDTO> Search(string nameLike, int? pageNumber, int? itemsPerPage, bool includeArchived, ref int totalItems, WorkLoadingBookLoadInstructions instructions)
        {
            using (var adapter = _fnAdapter())
            {
                LinqMetaData lmd = new LinqMetaData(adapter);
                var items = from e in lmd.WorkLoadingBook where e.Name.Contains(nameLike) select e.ToDTO();
                if (!includeArchived)
                    items = items.Where(i => i.Archived == false);
                items = items.OrderBy(i => i.Name);

                totalItems = items.Count();
                if (pageNumber.HasValue && itemsPerPage.HasValue)
                    items = items.Skip((pageNumber.Value - 1) * itemsPerPage.Value).Take(itemsPerPage.Value);
                return items.ToList();
            }
        }


        public WorkLoadingBookDTO Save(WorkLoadingBookDTO dto, bool refetch, bool recurse)
        {
            var e = dto.ToEntity();

            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(e, refetch, recurse);
            }

            return refetch ? e.ToDTO() : dto;
        }

        public WorkLoadingStandardDTO GetStandard(Guid bookId, Guid featureId, Guid activityId, WorkLoadingStandardLoadInstructions instructions)
        {
            EntityCollection<WorkLoadingStandardEntity> result = new EntityCollection<WorkLoadingStandardEntity>();

            PredicateExpression expression = new PredicateExpression(WorkLoadingStandardFields.BookId == bookId &
                WorkLoadingStandardFields.ActivityId == activityId &
            WorkLoadingStandardFields.FeatureId == featureId);

            var path = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), 1, null, path);
            }

            return result.Count == 0 ? null : result[0].ToDTO();
        }

        public WorkLoadingStandardDTO GetStandardById(Guid id, WorkLoadingStandardLoadInstructions instructions)
        {
            WorkLoadingStandardEntity result = new WorkLoadingStandardEntity(id);

            var path = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntity(result, path);
            }

            return result.IsNew == true ? null : result.ToDTO();
        }

        public IList<WorkLoadingStandardDTO> GetStandardsForFeatures(Guid[] featureids, WorkLoadingStandardLoadInstructions instructions)
        {
            EntityCollection<WorkLoadingStandardEntity> result = new EntityCollection<WorkLoadingStandardEntity>();

            PredicateExpression expression = new PredicateExpression(WorkLoadingStandardFields.FeatureId == featureids);

            var path = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), 0, null, path);
            }

            return result.ToList().Select(r => r.ToDTO()).ToList();
        }

        public WorkLoadingStandardDTO SaveStandard(WorkLoadingStandardDTO dto, bool refetch, bool recurse)
        {
            var e = dto.ToEntity();

            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(e, refetch, recurse);
            }

            return refetch ? e.ToDTO() : dto;
        }


        public void DeleteStandard(Guid id, bool archiveIfRequired, out bool archived)
        {
            using (var adapter = _fnAdapter())
            {
                var entity = new WorkLoadingStandardEntity(id);

                archived = false;
                try
                {
                    adapter.DeleteEntity(entity);
                }
                catch (Exception ex)
                {
                    if (archiveIfRequired)
                    {
                        adapter.FetchEntity(entity);
                        entity.Archived = true;
                        adapter.SaveEntity(entity, false);
                        archived = true;
                    }
                    else
                        throw ex;
                }
            }
        }


        public void DeleteBook(Guid id)
        {
            using (var adapter = _fnAdapter())
            {
                var entity = new WorkLoadingBookEntity(id);
                adapter.DeleteEntity(entity);
            }
        }
    }
}
