﻿using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.DTOConverters;
using ConceptCave.RepositoryInterfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualMgr.Central;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SD.LLBLGen.Pro.LinqSupportClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.Data;
using ConceptCave.Data.Linq;
using ConceptCave.Data.HelperClasses;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Data.DatabaseSpecific;

namespace ConceptCave.Repository.Injected
{
    public class OFacilityStructureRepository : IFacilityStructureRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public OFacilityStructureRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }
        private PrefetchPath2 BuildPrefetchPath(FacilityStructureLoadInstructions instructions)
        {
            PrefetchPath2 path = new PrefetchPath2(EntityType.FacilityStructureEntity);

            if ((instructions & FacilityStructureLoadInstructions.Parent) != 0)
            {
                path.Add(FacilityStructureEntity.PrefetchPathParent);
            }
            if ((instructions & FacilityStructureLoadInstructions.Children) != 0)
            {
                var childPath = path.Add(FacilityStructureEntity.PrefetchPathChildren);
                childPath.Sorter = new SortExpression(FacilityStructureFields.SortOrder | SortOperator.Ascending);
            }
            if ((instructions & FacilityStructureLoadInstructions.Address) != 0)
            {
                path.Add(FacilityStructureEntity.PrefetchPathAddress);
            }
            if ((instructions & FacilityStructureLoadInstructions.UserData) != 0)
            {
                path.Add(FacilityStructureEntity.PrefetchPathUserData);
            }
            if ((instructions & FacilityStructureLoadInstructions.FloorPlan) != 0)
            {
                path.Add(FacilityStructureEntity.PrefetchPathFloorPlan);
            }
            if ((instructions & FacilityStructureLoadInstructions.ProductCatalogs) != 0)
            {
                var catPath = path.Add(FacilityStructureEntity.PrefetchPathProductCatalogs);

                if ((instructions & FacilityStructureLoadInstructions.ProductCatalogItems) != 0)
                {
                    var itemPath = catPath.SubPath.Add(ProductCatalogEntity.PrefetchPathProductCatalogItems);
                    itemPath.Sorter = new SortExpression(ProductCatalogItemFields.SortOrder | SortOperator.Ascending);

                    if ((instructions & FacilityStructureLoadInstructions.ProductCatalogItemProducts) != 0)
                    {
                        var productPath = itemPath.SubPath.Add(ProductCatalogItemEntity.PrefetchPathProduct);

                        if ((instructions & FacilityStructureLoadInstructions.ProductCatalogItemProductHierarchies) != 0)
                        {
                            productPath.SubPath.Add(ProductEntity.PrefetchPathParentProductHierarchies).SubPath.Add(ProductHierarchyEntity.PrefetchPathChildProduct);
                        }

                        if ((instructions & FacilityStructureLoadInstructions.ProductCatalogItemProductTypes) != 0)
                        {
                            productPath.SubPath.Add(ProductEntity.PrefetchPathProductType);
                        }
                    }
                }
            }

            return path;
        }

        private void UseMembershipSiteName(IDataAccessAdapter adapter, FacilityStructureEntity entity)
        {
            if (entity.SiteId.HasValue && entity.Name == null)
            {
                if (entity.UserData == null)
                {
                    UserDataEntity ud = new UserDataEntity(entity.SiteId.Value);
                    adapter.FetchEntity(ud);
                    entity.UserData = ud;
                }
                entity.Name = entity.UserData.Name;
            }
            foreach (var child in entity.Children)
            {
                UseMembershipSiteName(adapter, child);
            }
        }

        public FacilityStructureDTO GetById(int id, FacilityStructureLoadInstructions instructions = FacilityStructureLoadInstructions.None)
        {
            var e = new FacilityStructureEntity(id);
            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntity(e, BuildPrefetchPath(instructions));

                if (!e.IsNew)
                {
                    UseMembershipSiteName(adapter, e);

                    if ((instructions & FacilityStructureLoadInstructions.ParentDeep) != 0)
                    {
                        var deep = e;
                        while (deep.ParentId.HasValue)
                        {
                            var parent = new FacilityStructureEntity(deep.ParentId.Value);
                            adapter.FetchEntity(parent, BuildPrefetchPath(instructions & FacilityStructureLoadInstructions.FloorPlan)); // Get Parent FloorPlan if requested
                            deep.Parent = parent;
                            deep = parent;
                        }
                    }
                }
            }
            return e.IsNew ? null : e.ToDTO();
        }

        public FacilityStructureDTO GetDeepParent(int id, FacilityStructureType parentType, FacilityStructureLoadInstructions instructions = FacilityStructureLoadInstructions.None)
        {
            var e = new FacilityStructureEntity(id);
            using (var adapter = _fnAdapter())
            {
                var path = BuildPrefetchPath(instructions);
                adapter.FetchEntity(e, path);

                while (!e.IsNew)
                {
                    if (e.StructureType == (int)parentType)
                        return e.ToDTO();

                    if (!e.ParentId.HasValue)
                        return null;

                    e = new FacilityStructureEntity(e.ParentId.Value);
                    adapter.FetchEntity(e, path);
                }
            }
            return null;
        }

        public FacilityStructureDTO Save(FacilityStructureDTO dto, bool refetch, bool recurse)
        {
            var e = dto.__IsNew ? new FacilityStructureEntity() : new FacilityStructureEntity(dto.Id);

            using (var adapter = _fnAdapter())
            {
                if (!dto.__IsNew)
                {
                    adapter.FetchEntity(e);
                }

                dto.ToEntity(e);

                // Facility Sites get their Name from the Membership Site, so set Facility Site name to null
                if (dto.StructureType == (int)FacilityStructureType.Site)
                    e.Name = null;

                adapter.SaveEntity(e, refetch, recurse);
            }

            return refetch ? e.ToDTO() : dto;
        }

        public FacilityStructureDTO FindBySiteId(Guid siteId, FacilityStructureLoadInstructions instructions = FacilityStructureLoadInstructions.None)
        {
            using (var adapter = _fnAdapter())
            {
                var lmd = new LinqMetaData(adapter);
                var path = BuildPrefetchPath(instructions);

                var items = from fs in lmd.FacilityStructure
                            join ud in lmd.UserData on fs.SiteId equals ud.UserId into fsud
                            from f in fsud.DefaultIfEmpty()
                            where fs.SiteId == siteId
                            select new { fs = fs, ud = f };

                var site = items.FirstOrDefault();
                if (site != null)
                {
                    site.fs.Name = site.ud.Name;
                    return site.fs.ToDTO();
                }
                return null;
            }
        }

        public IList<FacilityStructureDTO> Search(int? parentId, string nameLike, int? pageNumber, int? itemsPerPage, bool includeArchived, ref int totalItems, int? searchFlags)
        {
            using (var adapter = _fnAdapter())
            {
                var lmd = new LinqMetaData(adapter);
                var path = BuildPrefetchPath(FacilityStructureLoadInstructions.None);

                // We select from the FacilityStructureView which will map Membership Site Names to Facility Site records
                var items = from fs in lmd.FacilityStructure
                            join ud in lmd.UserData on fs.SiteId equals ud.UserId into fsud
                            from f in fsud.DefaultIfEmpty()
                            where fs.Name.Contains(nameLike) || (f != null && f.Name.Contains(nameLike))
                            select new { fs = fs, ud = f };

                if (parentId.HasValue)
                {
                    items = items.Where(r => r.fs.ParentId == parentId.Value);
                }
                if (searchFlags.HasValue)
                {
                    items = items.Where(r => ((1 << r.fs.StructureType) & searchFlags.Value) != 0);
                }
                items = items.OrderBy(i => i.fs.ParentId).OrderBy(i => i.fs.Level).ThenBy(i => i.fs.SortOrder).ThenBy(i => i.ud != null ? i.ud.Name : i.fs.Name);

                if (!includeArchived)
                    items = items.Where(i => i.fs.Archived == false);

                totalItems = items.Count();
                if (pageNumber.HasValue && itemsPerPage.HasValue)
                    items = items.Skip((pageNumber.Value - 1) * itemsPerPage.Value).Take(itemsPerPage.Value);

                var result = items.ToList();
                foreach (var i in result)
                {
                    var deep = i.fs;

                    // Use the Site name if populated (must be a Facility Site record)
                    if (deep.SiteId.HasValue)
                    {
                        deep.Name = i.ud.Name;
                    }
                    while (deep.ParentId.HasValue)
                    {
                        var parent = new FacilityStructureEntity(deep.ParentId.Value);
                        adapter.FetchEntity(parent);
                        deep.Parent = parent;
                        deep = parent;
                    }
                }

                return (from i in result select i.fs.ToDTO()).ToList();
            }
        }

        public void Delete(int id, bool archiveIfRequired, out bool archived)
        {
            using (var adapter = _fnAdapter())
            {
                var entity = new FacilityStructureEntity(id);
                archived = false;
                try
                {
                    adapter.DeleteEntity(entity);
                }
                catch (Exception ex)
                {
                    if (archiveIfRequired)
                    {
                        adapter.FetchEntity(entity);
                        entity.Archived = true;
                        adapter.SaveEntity(entity, false);
                        archived = true;
                    }
                    else
                        throw ex;
                }
            }
        }

        public IList<UserDataDTO> SearchSites(string search, int top)
        {
            using (var adapter = _fnAdapter())
            {
                var lmd = new LinqMetaData(adapter);
                var query = from ud in lmd.UserData
                            join ut in lmd.UserType on ud.UserTypeId equals ut.Id
                            where ud.UserType.IsAsite == true && ud.Name.Contains(search)
                            select ud;

                query = query.OrderBy(r => r.Name);

                query = query.Take(top);

                return (from e in query select e.ToDTO()).ToList();
            }
        }

        public FacilityStructureDTO FindFloorByIndoorAtlasFloorPlanId(string indoorAtlasFloorPlanId)
        {
            using (var adapter = _fnAdapter())
            {
                var lmd = new LinqMetaData(adapter);
                return (from fs in lmd.FacilityStructure where fs.FloorPlan.IndoorAtlasFloorPlanId == indoorAtlasFloorPlanId select fs.ToDTO()).FirstOrDefault();
            }
        }

        public FacilityStructureDTO FindByName(FacilityStructureType facilityStructureType, string exactName)
        {
            using (var adapter = _fnAdapter())
            {
                var lmd = new LinqMetaData(adapter);
                return (from fs in lmd.FacilityStructure where fs.StructureType == (int)facilityStructureType && fs.Name == exactName select fs.ToDTO()).FirstOrDefault();
            }
        }
        public FacilityStructureDTO FindByName(int? parentId, string exactName)
        {
            using (var adapter = _fnAdapter())
            {
                var lmd = new LinqMetaData(adapter);
                var query = from fs in lmd.FacilityStructure where fs.Name == exactName select fs.ToDTO();
                if (parentId.HasValue)
                {
                    query = query.Where(r => r.ParentId == parentId.Value);
                }
                return query.FirstOrDefault();
            }
        }
    }
}
