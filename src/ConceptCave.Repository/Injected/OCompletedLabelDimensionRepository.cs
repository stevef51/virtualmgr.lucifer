﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using ConceptCave.Data.HelperClasses;
using VirtualMgr.Central;
using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.Data.DTOConverters;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Repositories;

namespace ConceptCave.Repository.Injected
{
    public class OCompletedLabelDimensionRepository : ICompletedLabelDimensionRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public OCompletedLabelDimensionRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }
        public CompletedLabelDimensionDTO GetById(Guid id)
        {
            PredicateExpression expression = new PredicateExpression(CompletedLabelDimensionFields.Id == id);

            EntityCollection<CompletedLabelDimensionEntity> result = new EntityCollection<CompletedLabelDimensionEntity>();

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), 1, null);
            }

            return result.FirstOrDefault()?.ToDTO();
        }

        public CompletedLabelDimensionDTO CreateFromLabel(LabelDTO label)
        {
            CompletedLabelDimensionEntity result = new CompletedLabelDimensionEntity()
            {
                Id = label.Id,
                Name = label.Name,
                Forecolor = label.Forecolor,
                Backcolor = label.Backcolor
            };

            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(result, true);
            }

            return result.ToDTO();
        }

        public CompletedLabelDimensionDTO GetOrCreate(LabelDTO label)
        {
            return GetById(label.Id) ?? CreateFromLabel(label);
        }
    }
}
