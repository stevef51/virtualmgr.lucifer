﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Data;
using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.HelperClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using SD.LLBLGen.Pro.ORMSupportClasses;
using ConceptCave.Data.DTOConverters;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Checklist;
using ConceptCave.Repository.Facilities;
using ConceptCave.RepositoryInterfaces.ScheduleManagement;
using ConceptCave.RepositoryInterfaces.Workloading;
using ConceptCave.Checklist.Interfaces;
//using System.Web.Mvc;
using System.Transactions;
using VirtualMgr.Central;
using VirtualMgr.Common;

namespace ConceptCave.Repository.Injected
{
    public class OTaskRepository : CalendarRuleRepositoryBase, ITaskRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        private readonly ITaskTypeRepository _taskTypeRepo;
        private readonly IScheduleManagement _scheduleManagement;
        private readonly IWorkLoadingBookRepository _bookRepo;
        private readonly IWorkloadingDurationCalculator _durationCalculator;
        private readonly IWorkloadingActivityFactory _activityFactory;
        private readonly IFacilityStructureRepository _facilityStructureRepo;
        private readonly IHierarchyRepository _hierarchyRepo;

        public OTaskRepository(
            Func<DataAccessAdapter> fnAdapter,
            ITaskTypeRepository taskTypeRepo,
            IScheduleManagement scheduleManagement,
            IWorkLoadingBookRepository bookRepo,
            IWorkloadingDurationCalculator durationCalculator,
            IWorkloadingActivityFactory activityFactory,
            IFacilityStructureRepository facilityStructureRepo,
            IHierarchyRepository hierarchyRepo) : base()
        {
            _fnAdapter = fnAdapter;
            _taskTypeRepo = taskTypeRepo;
            _scheduleManagement = scheduleManagement;
            _bookRepo = bookRepo;
            _durationCalculator = durationCalculator;
            _activityFactory = activityFactory;
            _facilityStructureRepo = facilityStructureRepo;
            _hierarchyRepo = hierarchyRepo;
        }

        public static PrefetchPath2 BuildPrefetchPath(ProjectJobTaskLoadInstructions instructions)
        {
            PrefetchPath2 path = new PrefetchPath2((int)EntityType.ProjectJobTaskEntity);


            if ((instructions & ProjectJobTaskLoadInstructions.Attachments) == ProjectJobTaskLoadInstructions.Attachments)
            {
                path.Add(ProjectJobTaskEntity.PrefetchPathProjectJobTaskAttachments).SubPath.Add(ProjectJobTaskAttachmentEntity.PrefetchPathMedium);
            }

            if ((instructions & ProjectJobTaskLoadInstructions.Roster) == ProjectJobTaskLoadInstructions.Roster)
            {
                path.Add(ProjectJobTaskEntity.PrefetchPathRoster);
            }

            if ((instructions & ProjectJobTaskLoadInstructions.Labels) == ProjectJobTaskLoadInstructions.Labels)
            {
                path.Add(ProjectJobTaskEntity.PrefetchPathProjectJobTaskLabels).SubPath.Add(ProjectJobTaskLabelEntity.PrefetchPathLabel);
            }

            if ((instructions & ProjectJobTaskLoadInstructions.Users) == ProjectJobTaskLoadInstructions.Users)
            {
                path.Add(ProjectJobTaskEntity.PrefetchPathOwner);
                path.Add(ProjectJobTaskEntity.PrefetchPathSite);
            }

            if ((instructions & ProjectJobTaskLoadInstructions.WorkloadingActivities) == ProjectJobTaskLoadInstructions.WorkloadingActivities)
            {
                var activePath = path.Add(ProjectJobTaskEntity.PrefetchPathProjectJobTaskWorkloadingActivities);

                activePath.SubPath.Add(ProjectJobTaskWorkloadingActivityEntity.PrefetchPathSiteFeature).SubPath.Add(SiteFeatureEntity.PrefetchPathSiteArea);
            }

            if ((instructions & ProjectJobTaskLoadInstructions.TaskType) == ProjectJobTaskLoadInstructions.TaskType)
            {
                var typePath = path.Add(ProjectJobTaskEntity.PrefetchPathProjectJobTaskType);
                var origTypePath = path.Add(ProjectJobTaskEntity.PrefetchPathProjectJobTaskType_); //original task type

                if ((instructions & ProjectJobTaskLoadInstructions.TaskTypePublishedResources) == ProjectJobTaskLoadInstructions.TaskTypePublishedResources)
                {
                    typePath.SubPath.Add(ProjectJobTaskTypeEntity.PrefetchPathProjectJobTaskTypePublishingGroupResources).SubPath.Add(ProjectJobTaskTypePublishingGroupResourceEntity.PrefetchPathPublishingGroupResource);
                    origTypePath.SubPath.Add(ProjectJobTaskTypeEntity.PrefetchPathProjectJobTaskTypePublishingGroupResources).SubPath.Add(ProjectJobTaskTypePublishingGroupResourceEntity.PrefetchPathPublishingGroupResource);
                }

                if ((instructions & ProjectJobTaskLoadInstructions.TaskTypeRelationships) == ProjectJobTaskLoadInstructions.TaskTypeRelationships)
                {
                    typePath.SubPath.Add(ProjectJobTaskTypeEntity.PrefetchPathProjectJobTaskTypeDestinationRelationships);
                    typePath.SubPath.Add(ProjectJobTaskTypeEntity.PrefetchPathProjectJobTaskTypeSourceRelationships);

                    origTypePath.SubPath.Add(ProjectJobTaskTypeEntity.PrefetchPathProjectJobTaskTypeDestinationRelationships);
                    origTypePath.SubPath.Add(ProjectJobTaskTypeEntity.PrefetchPathProjectJobTaskTypeSourceRelationships);
                }

                if ((instructions & ProjectJobTaskLoadInstructions.TaskTypeDocumentation) == ProjectJobTaskLoadInstructions.TaskTypeDocumentation)
                {
                    var docPath = typePath.SubPath.Add(ProjectJobTaskTypeEntity.PrefetchPathProjectJobTaskTypeMediaFolders);
                    var origdocPath = origTypePath.SubPath.Add(ProjectJobTaskTypeEntity.PrefetchPathProjectJobTaskTypeMediaFolders);

                    if ((instructions & ProjectJobTaskLoadInstructions.TaskTypeDocumentationFolder) == ProjectJobTaskLoadInstructions.TaskTypeDocumentationFolder)
                    {
                        docPath.SubPath.Add(ProjectJobTaskTypeMediaFolderEntity.PrefetchPathMediaFolder);
                        origdocPath.SubPath.Add(ProjectJobTaskTypeMediaFolderEntity.PrefetchPathMediaFolder);
                    }
                }

                if ((instructions & ProjectJobTaskLoadInstructions.TaskTypeFinishedStatuses) == ProjectJobTaskLoadInstructions.TaskTypeFinishedStatuses)
                {
                    typePath.SubPath.Add(ProjectJobTaskTypeEntity.PrefetchPathProjectJobTaskTypeFinishedStatuses).SubPath.Add(ProjectJobTaskTypeFinishedStatusEntity.PrefetchPathProjectJobFinishedStatu);
                    origTypePath.SubPath.Add(ProjectJobTaskTypeEntity.PrefetchPathProjectJobTaskTypeFinishedStatuses).SubPath.Add(ProjectJobTaskTypeFinishedStatusEntity.PrefetchPathProjectJobFinishedStatu);
                }

                if ((instructions & ProjectJobTaskLoadInstructions.TaskTypeStatuses) == ProjectJobTaskLoadInstructions.TaskTypeStatuses)
                {
                    var statusPath = typePath.SubPath.Add(ProjectJobTaskTypeEntity.PrefetchPathProjectJobTaskTypeStatuses);
                    var origstatusPath = origTypePath.SubPath.Add(ProjectJobTaskTypeEntity.PrefetchPathProjectJobTaskTypeStatuses);

                    statusPath.Sorter = new SortExpression(ProjectJobTaskTypeStatusFields.SortOrder | SortOperator.Ascending);
                    origstatusPath.Sorter = new SortExpression(ProjectJobTaskTypeStatusFields.SortOrder | SortOperator.Ascending);

                    statusPath.SubPath.Add(ProjectJobTaskTypeStatusEntity.PrefetchPathProjectJobStatu);
                    origstatusPath.SubPath.Add(ProjectJobTaskTypeStatusEntity.PrefetchPathProjectJobStatu);
                }
            }

            if ((instructions & ProjectJobTaskLoadInstructions.Media) == ProjectJobTaskLoadInstructions.Media)
            {
                path.Add(ProjectJobTaskEntity.PrefetchPathProjectJobTaskMedias);
            }

            if ((instructions & ProjectJobTaskLoadInstructions.WorkLog) == ProjectJobTaskLoadInstructions.WorkLog)
            {
                var wpath = path.Add(ProjectJobTaskEntity.PrefetchPathProjectJobTaskWorkLogs);

                if ((instructions & ProjectJobTaskLoadInstructions.WorkLogUser) == ProjectJobTaskLoadInstructions.WorkLogUser)
                {
                    wpath.SubPath.Add(ProjectJobTaskWorkLogEntity.PrefetchPathUserData);
                }
            }

            if ((instructions & ProjectJobTaskLoadInstructions.WorkingDocuments) == ProjectJobTaskLoadInstructions.WorkingDocuments)
            {
                path.Add(ProjectJobTaskEntity.PrefetchPathProjectJobTaskWorkingDocuments).SubPath.Add(ProjectJobTaskWorkingDocumentEntity.PrefetchPathWorkingDocument);
            }

            if ((instructions & ProjectJobTaskLoadInstructions.Signature) == ProjectJobTaskLoadInstructions.Signature)
            {
                path.Add(ProjectJobTaskEntity.PrefetchPathTaskSignature);
            }

            if ((instructions & ProjectJobTaskLoadInstructions.Order) == ProjectJobTaskLoadInstructions.Order)
            {
                var ordersPath = path.Add(ProjectJobTaskEntity.PrefetchPathOrders);
                ordersPath.SubPath.Add(OrderEntity.PrefetchPathOrderItems);
            }

            if ((instructions & ProjectJobTaskLoadInstructions.Asset) == ProjectJobTaskLoadInstructions.Asset)
            {
                path.Add(ProjectJobTaskEntity.PrefetchPathAsset);
            }

            var translatedPath = path.Add(ProjectJobTaskEntity.PrefetchPathTranslated);
            if ((instructions & ProjectJobTaskLoadInstructions.AllTranslated) == 0)
                translatedPath.Filter = new PredicateExpression(ProjectJobTaskTranslatedFields.CultureName == System.Threading.Thread.CurrentThread.CurrentUICulture.Name);


            return path;
        }

        private ProjectJobTaskDTO Translate(ProjectJobTaskDTO dto, ProjectJobTaskLoadInstructions instructions)
        {
            // If using current culture then copy the translation across to primary record field
            if ((instructions & ProjectJobTaskLoadInstructions.AllTranslated) == ProjectJobTaskLoadInstructions.AllTranslated)
            {
                if (dto.Translated.Any())
                {
                    var first = dto.Translated.First();
                    dto.Name = string.IsNullOrEmpty(first.Name) == true ? dto.Name : first.Name;
                    dto.Description = string.IsNullOrEmpty(first.Name) == true ? dto.Description : first.Description;
                }
            }

            return dto;
        }

        public IList<DTO.DTOClasses.ProjectJobTaskDTO> Search(string text, int page, int count, RepositoryInterfaces.Enums.ProjectJobTaskLoadInstructions instructions)
        {
            EntityCollection<ProjectJobTaskEntity> result = new EntityCollection<ProjectJobTaskEntity>();

            PredicateExpression expression = new PredicateExpression();

            if (string.IsNullOrEmpty(text) == false)
            {
                expression.Add(ProjectJobTaskFields.Name % text);
            }

            SortExpression sort = new SortExpression();

            sort.Add(ProjectJobTaskFields.Name | SortOperator.Ascending);

            PrefetchPath2 path = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), 0, sort, path, page, count);
            }

            return result.ToList().Select(e => Translate(e.ToDTO(), instructions)).ToList();
        }

        public DTO.DTOClasses.ProjectJobTaskDTO GetById(Guid id,
            RepositoryInterfaces.Enums.ProjectJobTaskLoadInstructions instructions)
        {
            ProjectJobTaskEntity result = new ProjectJobTaskEntity(id);

            PrefetchPath2 path = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntity(result, path);
            }

            return result.IsNew == true ? null : Translate(result.ToDTO(), instructions);
        }

        public IList<ProjectJobTaskDTO> GetById(Guid[] id,
            ProjectJobTaskLoadInstructions instructions,
            IDictionary<object, object> cache = null)
        {
            EntityCollection<ProjectJobTaskEntity> result = new EntityCollection<ProjectJobTaskEntity>();

            PredicateExpression expression = new PredicateExpression(ProjectJobTaskFields.Id == id);

            PrefetchPath2 path = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), path);
            }

            if (cache == null)
            {
                return result.ToList().Select(e => Translate(e.ToDTO(), instructions)).ToList();
            }
            else
            {
                return result.ToList().Select(e => Translate(e.ToDTO(cache), instructions)).ToList();
            }

        }

        public IList<ProjectJobTaskDTO> GetByWorkingGroupId(Guid workingGroupId, ProjectJobTaskLoadInstructions instructions)
        {
            EntityCollection<ProjectJobTaskEntity> result = new EntityCollection<ProjectJobTaskEntity>();

            PredicateExpression expression = new PredicateExpression(ProjectJobTaskFields.WorkingGroupId == workingGroupId);

            PrefetchPath2 path = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), path);
            }

            return result.ToList().Select(e => Translate(e.ToDTO(), instructions)).ToList();
        }

        public IList<ProjectJobTaskDTO> GetForRoster(int rosterId, Guid? userId, DateTime startDateCreated, DateTime endDateCreated, TaskStatusFilter filter, ProjectJobTaskLoadInstructions instructions)
        {
            EntityCollection<ProjectJobTaskEntity> result = new EntityCollection<ProjectJobTaskEntity>();

            PredicateExpression expression = new PredicateExpression(ProjectJobTaskFields.RosterId == rosterId);
            if (userId.HasValue == true)
            {
                expression.AddWithAnd(ProjectJobTaskFields.OwnerId == userId.Value);
            }

            expression.AddWithAnd(ProjectJobTaskFields.DateCreated >= startDateCreated & ProjectJobTaskFields.DateCreated <= endDateCreated);
            PredicateExpression filterExpression = GetFilterForTaskStatusFilter(filter);
            expression.AddWithAnd(filterExpression);

            PrefetchPath2 path = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), path);
            }

            return result.ToList().Select(e => Translate(e.ToDTO(), instructions)).ToList();
        }

        public IList<ProjectJobTaskDTO> GetForRoster(int rosterId, DateTime startDateCreated, DateTime endDateCreated, TaskStatusFilter filter, ProjectJobTaskLoadInstructions instructions)
        {
            return GetForRoster(rosterId, null, startDateCreated, endDateCreated, filter, instructions);
        }

        public IList<ProjectJobTaskDTO> GetForRoster(int rosterId, DateTime endsOnUtc, TaskStatusFilter filter, ProjectJobTaskLoadInstructions instructions)
        {
            EntityCollection<ProjectJobTaskEntity> result = new EntityCollection<ProjectJobTaskEntity>();

            PredicateExpression expression = new PredicateExpression(ProjectJobTaskFields.RosterId == rosterId);
            expression.AddWithAnd(ProjectJobTaskFields.EndsOn <= endsOnUtc);
            PredicateExpression filterExpression = GetFilterForTaskStatusFilter(filter);
            expression.AddWithAnd(filterExpression);

            PrefetchPath2 path = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), path);
            }

            return result.ToList().Select(e => Translate(e.ToDTO(), instructions)).ToList();
        }

        public IList<ProjectJobTaskDTO> GetForRoster(int rosterId, TaskStatusFilter filter, ProjectJobTaskLoadInstructions instructions)
        {
            EntityCollection<ProjectJobTaskEntity> result = new EntityCollection<ProjectJobTaskEntity>();

            PredicateExpression expression = new PredicateExpression(ProjectJobTaskFields.RosterId == rosterId);
            PredicateExpression filterExpression = GetFilterForTaskStatusFilter(filter);
            expression.AddWithAnd(filterExpression);

            PrefetchPath2 path = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), path);
            }

            return result.ToList().Select(e => Translate(e.ToDTO(), instructions)).ToList();
        }

        public IList<ProjectJobTaskDTO> GetForUser(Guid userId, bool onlyOpenTasks, bool onlyTopLevel, TaskStatusFilter filter, ProjectJobTaskLoadInstructions instructions, int? bucketId, Guid[] labels = null, Guid[] statusses = null)
        {
            return GetForUserOrBucket(userId, onlyOpenTasks, onlyTopLevel, filter, instructions, bucketId, labels, statusses);
        }

        protected IList<ProjectJobTaskDTO> GetForUserOrBucket(Guid? userId, bool onlyOpenTasks, bool onlyTopLevel, TaskStatusFilter filter, ProjectJobTaskLoadInstructions instructions, int? bucketId, Guid[] labels = null, Guid[] statusses = null)
        {
            EntityCollection<ProjectJobTaskEntity> result = new EntityCollection<ProjectJobTaskEntity>();

            PredicateExpression expression = new PredicateExpression();

            if (userId.HasValue == true)
            {
                expression.Add(ProjectJobTaskFields.OwnerId == userId);
            }

            PredicateExpression filterExpression = GetFilterForTaskStatusFilter(filter);

            expression.AddWithAnd(filterExpression);

            if (bucketId.HasValue == true)
            {
                expression.AddWithAnd(ProjectJobTaskFields.HierarchyBucketId == bucketId.Value);
            }

            if (labels != null && labels.Length > 0)
            {
                // we need something like select * from tasks where id in (select taskid from tasklabels where labelid = labels)
                PredicateExpression labelExpression = new PredicateExpression(new FieldCompareSetPredicate(ProjectJobTaskFields.Id, null, ProjectJobTaskLabelFields.ProjectJobTaskId, null, SetOperator.In, ProjectJobTaskLabelFields.LabelId == labels));
                expression.AddWithAnd(labelExpression);
            }

            if (statusses != null && statusses.Length > 0)
            {
                expression.AddWithAnd(ProjectJobTaskFields.StatusId == statusses);
            }

            if (onlyOpenTasks == true)
            {
                expression.AddWithAnd(ProjectJobTaskFields.DateCompleted == DBNull.Value);
            }

            if (onlyTopLevel == true)
            {
                // we add an extra predicate to the select to get the top level. We work that out with a hit to the db for the highest level
                // that meet the same criteria as we are a going to use
                SortExpression sort = new SortExpression(ProjectJobTaskFields.Level | SortOperator.Descending);

                var levelResult = new EntityCollection<ProjectJobTaskEntity>();

                using (var adapter = _fnAdapter())
                {
                    adapter.FetchEntityCollection(levelResult, new RelationPredicateBucket(expression), 1, sort);
                }

                if (levelResult.Count > 0)
                {
                    expression.AddWithAnd(ProjectJobTaskFields.Level == levelResult[0].Level);
                }
            }

            PrefetchPath2 path = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), path);
            }

            return result.ToList().Select(e => Translate(e.ToDTO(), instructions)).ToList();
        }

        protected PredicateExpression GetFilterForTaskStatusFilter(TaskStatusFilter filter)
        {
            PredicateExpression filterExpression = new PredicateExpression();

            if ((filter & TaskStatusFilter.Unapproved) == TaskStatusFilter.Unapproved)
            {
                filterExpression.Add(ProjectJobTaskFields.Status == (int)ProjectJobTaskStatus.Unapproved);
            }

            if ((filter & TaskStatusFilter.Approved) == TaskStatusFilter.Approved)
            {
                filterExpression.AddWithOr(ProjectJobTaskFields.Status == (int)ProjectJobTaskStatus.Approved);
            }

            if ((filter & TaskStatusFilter.Started) == TaskStatusFilter.Started)
            {
                filterExpression.AddWithOr(ProjectJobTaskFields.Status == (int)ProjectJobTaskStatus.Started);
            }

            if ((filter & TaskStatusFilter.Paused) == TaskStatusFilter.Paused)
            {
                filterExpression.AddWithOr(ProjectJobTaskFields.Status == (int)ProjectJobTaskStatus.Paused);
            }

            if ((filter & TaskStatusFilter.FinishedComplete) == TaskStatusFilter.FinishedComplete)
            {
                filterExpression.AddWithOr(ProjectJobTaskFields.Status == (int)ProjectJobTaskStatus.FinishedComplete);
            }

            if ((filter & TaskStatusFilter.FinishedIncomplete) == TaskStatusFilter.FinishedIncomplete)
            {
                filterExpression.AddWithOr(ProjectJobTaskFields.Status == (int)ProjectJobTaskStatus.FinishedIncomplete);
            }

            if ((filter & TaskStatusFilter.FinishedByManagement) == TaskStatusFilter.FinishedByManagement)
            {
                filterExpression.AddWithOr(ProjectJobTaskFields.Status == (int)ProjectJobTaskStatus.FinishedByManagement);
            }

            if ((filter & TaskStatusFilter.FinishedBySystem) == TaskStatusFilter.FinishedBySystem)
            {
                filterExpression.AddWithOr(ProjectJobTaskFields.Status == (int)ProjectJobTaskStatus.FinishedBySystem);
            }

            if ((filter & TaskStatusFilter.ApprovedContinued) == TaskStatusFilter.ApprovedContinued)
            {
                filterExpression.AddWithOr(ProjectJobTaskFields.Status == (int)ProjectJobTaskStatus.ApprovedContinued);
            }

            if ((filter & TaskStatusFilter.ChangeRosterRequested) == TaskStatusFilter.ChangeRosterRequested)
            {
                filterExpression.AddWithOr(ProjectJobTaskFields.Status == (int)ProjectJobTaskStatus.ChangeRosterRequested);
            }

            if ((filter & TaskStatusFilter.ChangeRosterRejected) == TaskStatusFilter.ChangeRosterRejected)
            {
                filterExpression.AddWithOr(ProjectJobTaskFields.Status == (int)ProjectJobTaskStatus.ChangeRosterRejected);
            }

            if ((filter & TaskStatusFilter.Cancelled) == TaskStatusFilter.Cancelled)
            {
                filterExpression.AddWithOr(ProjectJobTaskFields.Status == (int)ProjectJobTaskStatus.Cancelled);
            }
            return filterExpression;
        }

        public IList<ProjectJobTaskDTO> GetForUser(Guid userId, DateTime date, TimeZoneInfo timezone, ProjectJobTaskStatus status, ProjectJobTaskLoadInstructions instructions, int? bucketId, Guid[] labels = null, Guid[] statusses = null)
        {
            return GetForUserOrBucket(userId, date, timezone, status, instructions, bucketId, labels, statusses);
        }

        protected IList<ProjectJobTaskDTO> GetForUserOrBucket(Guid? userId, DateTime date, TimeZoneInfo timezone, ProjectJobTaskStatus status, ProjectJobTaskLoadInstructions instructions, int? bucketId, Guid[] labels = null, Guid[] statusses = null)
        {
            DateTime utcDate = TimeZoneInfo.ConvertTimeToUtc(date, timezone);

            EntityCollection<ProjectJobTaskEntity> result = new EntityCollection<ProjectJobTaskEntity>();

            PredicateExpression expression = new PredicateExpression(ProjectJobTaskFields.Status == (int)status);

            if (userId.HasValue == true)
            {
                expression.AddWithAnd(ProjectJobTaskFields.OwnerId == userId);
            }

            if (bucketId.HasValue == true)
            {
                expression.AddWithAnd(ProjectJobTaskFields.HierarchyBucketId == bucketId.Value);
            }

            expression.AddWithAnd(new FieldBetweenPredicate(ProjectJobTaskFields.DateCreated, null, utcDate, utcDate.AddDays(1)));

            if (labels != null)
            {
                // we need something like select * from tasks where id in (select taskid from tasklabels where labelid = labels)
                PredicateExpression labelExpression = new PredicateExpression(new FieldCompareSetPredicate(ProjectJobTaskFields.Id, null, ProjectJobTaskLabelFields.ProjectJobTaskId, null, SetOperator.In, ProjectJobTaskLabelFields.LabelId == labels));
                expression.AddWithAnd(labelExpression);
            }

            if (statusses != null && statusses.Length > 0)
            {
                expression.AddWithAnd(ProjectJobTaskFields.StatusId == statusses);
            }

            PrefetchPath2 path = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), path);
            }

            return result.ToList().Select(e => Translate(e.ToDTO(), instructions)).ToList();
        }

        public ProjectJobTaskDTO GetForUser(Guid userId, Guid scheduleId, int rosterId, int roleId, DateTime date, TimeZoneInfo timezone, ProjectJobTaskLoadInstructions instructions, Guid[] labels = null, Guid[] statusses = null)
        {
            DateTime d = TimeZoneInfo.ConvertTimeToUtc(date.Date, timezone);

            PredicateExpression expression = new PredicateExpression(ProjectJobTaskFields.OwnerId == userId &
                ProjectJobTaskFields.ProjectJobScheduledTaskId == scheduleId &
                ProjectJobTaskFields.DateCreated >= d & ProjectJobTaskFields.DateCreated < d.AddDays(1) &
                ProjectJobTaskFields.RoleId == roleId &
                ProjectJobTaskFields.RosterId == rosterId);

            if (labels != null)
            {
                // we need something like select * from tasks where id in (select taskid from tasklabels where labelid = labels)
                PredicateExpression labelExpression = new PredicateExpression(new FieldCompareSetPredicate(ProjectJobTaskFields.Id, null, ProjectJobTaskLabelFields.ProjectJobTaskId, null, SetOperator.In, ProjectJobTaskLabelFields.LabelId == labels));
                expression.AddWithAnd(labelExpression);
            }

            if (statusses != null && statusses.Length > 0)
            {
                expression.AddWithAnd(ProjectJobTaskFields.StatusId == statusses);
            }

            var path = BuildPrefetchPath(instructions);

            EntityCollection<ProjectJobTaskEntity> result = new EntityCollection<ProjectJobTaskEntity>();

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), path);
            }

            return result.Count == 0 ? null : Translate(result.First().ToDTO(), instructions);
        }

        public IList<ProjectJobTaskDTO> GetForBucket(int bucketId, bool onlyOpenTasks, bool onlyTopLevel, TaskStatusFilter filter, ProjectJobTaskLoadInstructions instructions, Guid[] labels = null, Guid[] statusses = null)
        {
            return GetForUserOrBucket(null, onlyOpenTasks, onlyTopLevel, filter, instructions, bucketId, labels, statusses);
        }
        public IList<ProjectJobTaskDTO> GetForBucket(int bucketId, DateTime date, TimeZoneInfo timezone, ProjectJobTaskStatus status, ProjectJobTaskLoadInstructions instructions, Guid[] labels = null, Guid[] statusses = null)
        {
            return GetForUserOrBucket(null, date, timezone, status, instructions, bucketId, labels, statusses);
        }

        public ProjectJobTaskDTO GetByWorkingDocumentId(Guid workingDocumentId, ProjectJobTaskLoadInstructions instructions)
        {
            PredicateExpression innerExpression = new PredicateExpression(ProjectJobTaskWorkingDocumentFields.WorkingDocumentId == workingDocumentId);
            PredicateExpression outerExpression = new PredicateExpression(new FieldCompareSetPredicate(ProjectJobTaskFields.Id, null, ProjectJobTaskWorkingDocumentFields.ProjectJobTaskId, null, SetOperator.In, innerExpression));

            EntityCollection<ProjectJobTaskEntity> result = new EntityCollection<ProjectJobTaskEntity>();

            PrefetchPath2 path = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(outerExpression), path);
            }

            return result.Count == 0 ? null : Translate(result[0].ToDTO(), instructions);
        }

        public void ManageTasksForNewLeave(HierarchyBucketOverrideDTO leave, TimeZoneInfo timezone)
        {
            if (leave.UserId.HasValue == false || leave.OriginalUserId.HasValue == false)
            {
                return;
            }



            DateTime utcStart = leave.StartDate;
            if (utcStart.Kind == DateTimeKind.Local)
            {
                utcStart = TimeZoneInfo.ConvertTimeToUtc(leave.StartDate, timezone);
            }

            DateTime utcEnd = leave.EndDate;
            if (utcEnd.Kind == DateTimeKind.Local)
            {
                utcEnd = TimeZoneInfo.ConvertTimeToUtc(leave.EndDate, timezone);
            }

            var statusFilter = GetFilterForTaskStatusFilter(TaskStatusFilter.Approved | TaskStatusFilter.ApprovedContinued | TaskStatusFilter.ChangeRosterRejected | TaskStatusFilter.ChangeRosterRequested | TaskStatusFilter.Paused | TaskStatusFilter.Started | TaskStatusFilter.Unapproved);

            PredicateExpression expression = new PredicateExpression(ProjectJobTaskFields.DateCreated >= utcStart & ProjectJobTaskFields.DateCreated <= utcEnd);
            expression.AddWithAnd(ProjectJobTaskFields.OwnerId == leave.OriginalUserId);
            expression.AddWithAnd(statusFilter);

            EntityCollection<ProjectJobTaskEntity> result = new EntityCollection<ProjectJobTaskEntity>();

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression));
            }

            foreach (var task in result)
            {
                task.OwnerId = leave.UserId.Value;
                task.OriginalOwnerId = leave.UserId.Value;
            }

            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntityCollection(result);
            }
        }

        public void ManageTasksForLeaveRemoval(HierarchyBucketOverrideDTO leave, TimeZoneInfo timezone)
        {
            if (leave.UserId.HasValue == false || leave.OriginalUserId.HasValue == false)
            {
                return;
            }

            DateTime utcStart = leave.StartDate;
            if (utcStart.Kind == DateTimeKind.Local)
            {
                utcStart = TimeZoneInfo.ConvertTimeToUtc(leave.StartDate, timezone);
            }

            DateTime utcEnd = leave.EndDate;
            if (utcEnd.Kind == DateTimeKind.Local)
            {
                utcEnd = TimeZoneInfo.ConvertTimeToUtc(leave.EndDate, timezone);
            }

            var statusFilter = GetFilterForTaskStatusFilter(TaskStatusFilter.Approved | TaskStatusFilter.ApprovedContinued | TaskStatusFilter.ChangeRosterRejected | TaskStatusFilter.ChangeRosterRequested | TaskStatusFilter.Paused | TaskStatusFilter.Started | TaskStatusFilter.Unapproved);

            PredicateExpression expression = new PredicateExpression(ProjectJobTaskFields.DateCreated >= utcStart & ProjectJobTaskFields.DateCreated <= utcEnd);
            expression.AddWithAnd(ProjectJobTaskFields.ScheduledOwnerId == leave.OriginalUserId);
            expression.AddWithAnd(ProjectJobTaskFields.OwnerId == leave.UserId.Value);
            expression.AddWithAnd(statusFilter);

            EntityCollection<ProjectJobTaskEntity> result = new EntityCollection<ProjectJobTaskEntity>();

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression));
            }

            foreach (var task in result)
            {
                task.OwnerId = leave.OriginalUserId.Value;
                task.OriginalOwnerId = leave.OriginalUserId.Value;
            }

            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntityCollection(result);
            }
        }

        public DTO.DTOClasses.ProjectJobTaskDTO Save(DTO.DTOClasses.ProjectJobTaskDTO task, bool refetch, bool recurse)
        {
            var e = task.ToEntity();

            ManageTaskFields(task, e);

            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(e, refetch, recurse);
            }

            return refetch ? e.ToDTO() : task;
        }

        private static void ManageTaskFields(ProjectJobTaskDTO task, ProjectJobTaskEntity e)
        {
            if (task.DateCompleted.HasValue == false)
            {
                e.DateCompleted = DateTime.MinValue;
                e.DateCompleted = null;
            }

            if (task.WorkingGroupId.HasValue == false)
            {
                e.WorkingGroupId = Guid.NewGuid();
                e.WorkingGroupId = null;
            }

            if (task.StartTimeUtc.HasValue == false)
            {
                e.StartTimeUtc = DateTime.UtcNow;
                e.StartTimeUtc = null;
            }

            if (task.LateAfterUtc.HasValue == false)
            {
                e.LateAfterUtc = DateTime.UtcNow;
                e.LateAfterUtc = null;
            }
        }

        public IList<ProjectJobTaskDTO> Save(IList<ProjectJobTaskDTO> tasks, bool refetch, bool recurse)
        {
            EntityCollection<ProjectJobTaskEntity> result = new EntityCollection<ProjectJobTaskEntity>();

            tasks.ForEach(t =>
            {
                var e = t.ToEntity();
                ManageTaskFields(t, e);
                result.Add(e);
            });

            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntityCollection(result, refetch, recurse);
            }

            return refetch == true ? result.ToList().Select(e => e.ToDTO()).ToList() : tasks;
        }

        public ProjectJobTaskFinishedStatusDTO Save(ProjectJobTaskFinishedStatusDTO status, bool refetch, bool recurse)
        {
            ProjectJobTaskFinishedStatusEntity e = status.ToEntity();

            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(e, refetch, recurse);
            }

            return refetch ? e.ToDTO() : status;
        }

        public void Save(ProjectJobTaskWorkloadingActivityDTO[] activities, bool refetch, bool recurse)
        {
            EntityCollection<ProjectJobTaskWorkloadingActivityEntity> result = new EntityCollection<ProjectJobTaskWorkloadingActivityEntity>();

            foreach (var dto in activities)
            {
                var ent = dto.ToEntity();

                if (dto.Completed.HasValue == false)
                {
                    ent.Completed = true;
                    ent.Completed = null;
                }

                result.Add(ent);
            }

            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntityCollection(result, refetch, recurse);
            }
        }

        protected ProjectJobTaskEntity CreateTask(Guid? taskId, string name, Guid ownerId, Guid taskTypeId, Guid? originalOwnerId = null, Guid? scheduledOwnerId = null, ProjectJobTaskStatus status = ProjectJobTaskStatus.Unapproved, Guid? siteId = null)
        {
            return CreateTask(taskId, name, null, ownerId, taskTypeId, originalOwnerId, scheduledOwnerId, status, siteId);
        }

        protected ProjectJobTaskEntity CreateTask(Guid? taskId, string name, string description, Guid ownerId, Guid taskTypeId, Guid? originalOwnerId = null, Guid? scheduledOwnerId = null, ProjectJobTaskStatus status = ProjectJobTaskStatus.Unapproved, Guid? siteId = null)
        {
            ProjectJobTaskEntity task = new ProjectJobTaskEntity();

            task.Id = taskId ?? CombFactory.NewComb();
            task.OwnerId = ownerId;
            task.OriginalOwnerId = originalOwnerId.HasValue ? originalOwnerId.Value : ownerId;
            task.ScheduledOwnerId = scheduledOwnerId.HasValue ? scheduledOwnerId.Value : ownerId;
            task.TaskTypeId = taskTypeId;
            task.OriginalTaskTypeId = taskTypeId;
            task.Name = name;
            task.Description = description;
            task.Status = (int)status;
            task.SiteId = siteId;

            var taskType = _taskTypeRepo.GetById(taskTypeId, ProjectJobTaskTypeLoadInstructions.Statusses | ProjectJobTaskTypeLoadInstructions.FacilityOverrides);

            task.HasOrders = taskType.UserInterfaceType == (int)UserInterfaceType.Ordering;

            if (taskType.ProjectJobTaskTypeStatuses.Count > 0)
            {
                var defaultStatus = from s in taskType.ProjectJobTaskTypeStatuses where s.DefaultToWhenCreated == true select s;
                if (defaultStatus.Count() > 0)
                {
                    task.StatusId = defaultStatus.First().StatusId;
                }
            }

            // EVS-841 Ability to define estimated duration against a task type
            task.EstimatedDurationSeconds = taskType.EstimatedDurationSeconds;
            task.MinimumDurationExpression = taskType.MinimumDuration;

            // EVS-862 Crothall - Facility Hierarchy to Aid in Estimated Durations
            // See if there is a TaskTypeFacilityOverride record which defines the Estimated Duration ..
            if (siteId.HasValue && taskType.ProjectJobTaskTypeFacilityOverrides.Any())
            {
                var facilityStructure = _facilityStructureRepo.FindBySiteId(siteId.Value);
                while (facilityStructure != null)
                {
                    var facilityOverrideDto = taskType.ProjectJobTaskTypeFacilityOverrides.FirstOrDefault(fo => fo.FacilityStructureId == facilityStructure.Id);
                    if (facilityOverrideDto != null)
                    {
                        task.EstimatedDurationSeconds = facilityOverrideDto.EstimatedDurationSeconds;

                        if (string.IsNullOrEmpty(facilityOverrideDto.MinimumDuration) == false)
                        {
                            task.MinimumDurationExpression = facilityOverrideDto.MinimumDuration;
                        }
                        break;
                    }
                    if (facilityStructure.ParentId.HasValue)
                        facilityStructure = _facilityStructureRepo.GetById(facilityStructure.ParentId.Value, FacilityStructureLoadInstructions.None);
                    else
                        break;
                }
            }

            ProjectJobTaskTranslatedEntity translate = new ProjectJobTaskTranslatedEntity()
            {
                Id = task.Id,
                CultureName = System.Threading.Thread.CurrentThread.CurrentUICulture.Name,
                Name = name,
                Description = description
            };

            task.Translated.Add(translate);

            return task;
        }

        public ProjectJobTaskDTO New(string name, Guid ownerId, int rosterId, Guid taskTypeId, int? bucketId, IMembershipRepository memberRepo, Guid? originalOwnerId = null, Guid? siteId = null, ProjectJobTaskStatus status = ProjectJobTaskStatus.Unapproved, Guid? groupId = null, int? assetId = null, int? taskLevel = null, string lengthInDays = null)
        {
            return New(name, null, ownerId, rosterId, taskTypeId, bucketId, memberRepo, originalOwnerId, siteId, status, groupId, assetId, taskLevel, lengthInDays);
        }

        public ProjectJobTaskDTO New(Guid? taskId, string name, Guid ownerId, int rosterId, Guid taskTypeId, int? bucketId, IMembershipRepository memberRepo, Guid? originalOwnerId = null, Guid? siteId = null, ProjectJobTaskStatus status = ProjectJobTaskStatus.Unapproved, Guid? groupId = null, int? assetId = null, int? taskLevel = null, string lengthInDays = null)
        {
            return New(taskId, name, null, ownerId, rosterId, taskTypeId, bucketId, memberRepo, originalOwnerId, siteId, status, groupId, assetId, taskLevel, lengthInDays);
        }

        public ProjectJobTaskDTO New(string name, string description, Guid ownerId, int rosterId, Guid taskTypeId, int? bucketId, IMembershipRepository memberRepo, Guid? originalOwnerId = null, Guid? siteId = null, ProjectJobTaskStatus status = ProjectJobTaskStatus.Unapproved, Guid? groupId = null, int? assetId = null, int? taskLevel = null, string lengthInDays = null)
        {
            return New(null, name, description, ownerId, rosterId, taskTypeId, bucketId, memberRepo, originalOwnerId, siteId, status, groupId, assetId, taskLevel, lengthInDays);
        }
        public ProjectJobTaskDTO New(Guid? taskId, string name, string description, Guid ownerId, int rosterId, Guid taskTypeId, int? bucketId, IMembershipRepository memberRepo, Guid? originalOwnerId = null, Guid? siteId = null, ProjectJobTaskStatus status = ProjectJobTaskStatus.Unapproved, Guid? groupId = null, int? assetId = null, int? taskLevel = null, string lengthInDays = null)
        {
            var task = CreateTask(taskId, name, description, ownerId, taskTypeId, originalOwnerId, null, status, siteId);

            task.RosterId = rosterId;
            task.HierarchyBucketId = bucketId;
            if (bucketId.HasValue)
            {
                var bucket = _hierarchyRepo.GetById(bucketId.Value);
                task.RoleId = bucket?.RoleId;
            }
            task.ProjectJobTaskGroupId = groupId;
            task.AssetId = assetId;
            if (taskLevel.HasValue)
            {
                task.Level = taskLevel.Value;
            }
            var user = memberRepo.GetById(ownerId, MembershipLoadInstructions.None);

            var taskType = _taskTypeRepo.GetById(taskTypeId, ProjectJobTaskTypeLoadInstructions.None);

            IDictionary<int, RosterDTO> rosterCache = new Dictionary<int, RosterDTO>();

            // we set the end time of the task to be when the current shift ends
            task.EndsOn = _scheduleManagement.GetEndOfCurrentShiftInTimezone(rosterId, TimeZoneInfo.Utc, rosterCache);
            var useLengthInDays = lengthInDays ?? taskType.DefaultLengthInDays;
            if (string.IsNullOrEmpty(useLengthInDays) == false)
            {
                task.EndsOn = CalculateTaskLengthInDays(rosterId, useLengthInDays, rosterCache);
            }

            task.CalculateMinimumDuration();

            if (siteId.HasValue == true)
            {
                var site = memberRepo.GetById(siteId.Value);
                task.SiteLatitude = site.Latitude;
                task.SiteLongitude = site.Longitude;
            }

            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(task, true, true);
            }

            return task.ToDTO();
        }

        public DateTime CalculateTaskLengthInDays(int rosterId, string lengthInDays, IDictionary<int, RosterDTO> rosterCache)
        {
            rosterCache = rosterCache ?? new Dictionary<int, RosterDTO>();
            var endsOn = _scheduleManagement.GetEndOfCurrentShiftInTimezone(rosterId, TimeZoneInfo.Utc, rosterCache);

            if (string.IsNullOrEmpty(lengthInDays))
            {
                return endsOn;
            }

            int lid = 0;

            if (int.TryParse(lengthInDays, out lid) == true)
            {
                // easy we've been given an integer, subtract 1 off as a length in days of 1 means it ends at the
                // end of the shift it was scheduled on
                return endsOn.AddDays(lid - 1);
            }

            // ok, other strings we except are going to calculate a length in days based on really simple
            // expressions, we allow the following
            // EndOfMonth(XX) - where XX is some number of months to go forward. XX is zero based, so 0 means end of
            // of the month that the parameter date is in, 1 end of the following month
            // StartOfMonth(XX) - Similar to EndOfMonth, just the start of the month

            string fnc = lengthInDays.Substring(0, lengthInDays.IndexOf('('));
            string amount = lengthInDays.Replace(fnc, "").Replace("(", "").Replace(")", "");

            if (string.IsNullOrEmpty(amount))
            {
                lid = 0;
            }
            else if (int.TryParse(amount, out lid) == false)
            {
                lid = 0;
            }

            var roster = rosterCache[rosterId];
            var rosterTZ = TimeZoneInfoResolver.ResolveWindows(roster.Timezone);

            DateTime startOfShift = DateTime.Now;

            int month = endsOn.Month + lid;

            if (fnc.ToLower() == "endofmonth")
            {
                startOfShift = _scheduleManagement.GetDSTSafeStartTime(roster, new DateTime(endsOn.Year, month, DateTime.DaysInMonth(endsOn.Year, endsOn.Month)), rosterTZ);
            }
            else if (fnc.ToLower() == "startofmonth")
            {
                startOfShift = _scheduleManagement.GetDSTSafeStartTime(roster, new DateTime(endsOn.Year, month, 1), rosterTZ);
            }
            else
            {
                // we don't recognise it, so we'll ignore and go with default
                return endsOn;
            }

            var endDate = _scheduleManagement.GetEndOfShiftInTimezone(roster, startOfShift, rosterTZ, TimeZoneInfo.Utc);
            return endDate;
        }

        public ProjectJobTaskDTO New(ProjectJobScheduledTaskDTO dto, TimeZoneInfo tz, Guid ownerId, int? bucketId, IMembershipRepository memberRepo, Guid? originalOwnerId = null, Guid? scheduledOwnerId = null, ProjectJobTaskStatus status = ProjectJobTaskStatus.Unapproved)
        {
            var task = CreateTask(null, dto.Name, dto.Description, ownerId, dto.TaskTypeId, originalOwnerId, scheduledOwnerId, status, dto.SiteId);

            task.ProjectJobScheduledTaskId = dto.Id;
            task.Description = dto.Description;
            task.Level = dto.Level;
            task.RequireSignature = dto.RequireSignature;
            task.PhotoDuringSignature = dto.PhotoDuringSignature;
            task.RosterId = dto.RosterId;
            task.HierarchyBucketId = bucketId;
            if (bucketId.HasValue)
            {
                var bucket = _hierarchyRepo.GetById(bucketId.Value);
                task.RoleId = bucket?.RoleId;
            }

            task.SortOrder = dto.SortOrder;

            if (dto.StartTime.HasValue)
            {
                var localDate = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, tz).Date;
                var localStartTime = localDate.Add(dto.StartTime.Value);

                var utcStartTime = TimeZoneInfo.ConvertTimeToUtc(localStartTime, tz);

                task.StartTimeUtc = new DateTimeOffset(localStartTime, tz.GetUtcOffset(utcStartTime));

                if (dto.StartTimeWindowSeconds.HasValue == true)
                {
                    task.LateAfterUtc = task.StartTimeUtc.Value.Add(new TimeSpan(0, 0, 0, dto.StartTimeWindowSeconds.Value, 0));
                }
            }

            if (dto.EstimatedDurationSeconds.HasValue)
            {
                task.EstimatedDurationSeconds = dto.EstimatedDurationSeconds;
            }

            if (string.IsNullOrEmpty(dto.MinimumDuration) == false)
            {
                task.MinimumDurationExpression = dto.MinimumDuration;
            }

            task.CalculateMinimumDuration();

            var user = memberRepo.GetById(ownerId, MembershipLoadInstructions.None);

            // we set the end time of the task to be when the current shift ends
            IDictionary<int, RosterDTO> rosterCache = new Dictionary<int, RosterDTO>();
            task.EndsOn = _scheduleManagement.GetEndOfCurrentShiftInTimezone(dto.RosterId, TimeZoneInfo.Utc, rosterCache);
            if (string.IsNullOrEmpty(dto.LengthInDays) == false)
            {
                task.EndsOn = CalculateTaskLengthInDays(dto.RosterId, dto.LengthInDays, rosterCache);
            }

            if (dto.SiteId.HasValue == true)
            {
                var site = memberRepo.GetById(dto.SiteId.Value);
                task.SiteLatitude = site.Latitude;
                task.SiteLongitude = site.Longitude;
            }

            if (dto.RoleId.HasValue == true)
            {
                task.RoleId = dto.RoleId.Value;
            }

            ProjectJobTaskTranslatedEntity defaultTrans = null;
            if (task.Translated.Count > 0)
            {
                defaultTrans = task.Translated[0];
            }

            foreach (var trans in dto.Translated)
            {
                // CreateTask creates a translation for the default culture, so we need to avoid doing that
                if (defaultTrans != null && trans.CultureName == defaultTrans.CultureName)
                {
                    continue;
                }

                var t = new ProjectJobTaskTranslatedEntity()
                {
                    Id = task.Id,
                    CultureName = trans.CultureName,
                    Name = trans.Name,
                    Description = trans.Description
                };

                task.Translated.Add(t);
            }

            foreach (var attach in dto.ProjectJobScheduledTaskAttachments)
            {
                var a = new ProjectJobTaskAttachmentEntity()
                {
                    ProjectJobTaskId = task.Id,
                    MediaId = attach.MediaId
                };

                task.ProjectJobTaskAttachments.Add(a);
            }

            foreach (var label in dto.ProjectJobScheduledTaskLabels)
            {
                var l = new ProjectJobTaskLabelEntity()
                {
                    ProjectJobTaskId = task.Id,
                    LabelId = label.LabelId
                };

                task.ProjectJobTaskLabels.Add(l);
            }

            var date = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, tz);

            foreach (var activity in dto.ProjectJobScheduledTaskWorkloadingActivities)
            {
                if (_activityFactory.ShouldCreate(activity, date, tz) == false)
                {
                    continue;
                }

                IWorkloadingActivity wlActivity = _activityFactory.NewForFeature(activity.WorkloadingStandardId, activity.SiteFeatureId);

                WorkLoadingStandardDTO standard = (WorkLoadingStandardDTO)wlActivity.Data;

                if (activity.EstimatedDuration.HasValue == false)
                {
                    _durationCalculator.EstimateDuration(wlActivity);
                }

                var a = new ProjectJobTaskWorkloadingActivityEntity()
                {
                    ProjectJobTaskId = task.Id,
                    WorkLoadingStandardId = activity.WorkloadingStandardId,
                    SiteFeatureId = activity.SiteFeatureId,
                    WorkloadingStandardMeasure = standard.Measure,
                    WorkloadingStandardUnitId = standard.UnitId,
                    WorkloadingStandardSeconds = standard.Seconds,
                    Text = activity.Text,
                    EstimatedDuration = activity.EstimatedDuration.HasValue == true ? activity.EstimatedDuration.Value : wlActivity.EstimatedDuration
                };

                task.ProjectJobTaskWorkloadingActivities.Add(a);
            }

            task.AllActivitiesEstimatedDuration = (from w in task.ProjectJobTaskWorkloadingActivities select w.EstimatedDuration).Sum();

            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(task, true, true);
            }

            return task.ToDTO();
        }

        //[Authorize(Roles = RoleRepository.COREROLE_TASKDELETE)]
        public void Remove(Guid[] ids)
        {
            PredicateExpression taskExpression = new PredicateExpression(ProjectJobTaskFields.Id == ids);
            PredicateExpression translatedExpression = new PredicateExpression(ProjectJobTaskTranslatedFields.Id == ids);
            PredicateExpression attachmentExpression = new PredicateExpression(ProjectJobTaskAttachmentFields.ProjectJobTaskId == ids);
            PredicateExpression changeExpression = new PredicateExpression(ProjectJobTaskChangeRequestFields.ProjectJobTaskId == ids);
            PredicateExpression finishedExpression = new PredicateExpression(ProjectJobTaskFinishedStatusFields.TaskId == ids);
            PredicateExpression labelExpression = new PredicateExpression(ProjectJobTaskLabelFields.ProjectJobTaskId == ids);
            PredicateExpression relationExpression = new PredicateExpression(ProjectJobTaskRelationshipFields.SourceProjectJobTaskId == ids | ProjectJobTaskRelationshipFields.DestinationProjectJobTaskId == ids);
            PredicateExpression signatureExpression = new PredicateExpression(ProjectJobTaskSignatureFields.ProjectJobTaskId == ids);
            PredicateExpression wdExpression = new PredicateExpression(ProjectJobTaskWorkingDocumentFields.ProjectJobTaskId == ids);
            PredicateExpression wlExpression = new PredicateExpression(ProjectJobTaskWorkloadingActivityFields.ProjectJobTaskId == ids);
            PredicateExpression logExpression = new PredicateExpression(ProjectJobTaskWorkLogFields.ProjectJobTaskId == ids);

            using (TransactionScope scope = new TransactionScope())
            {
                using (var adapter = _fnAdapter())
                {
                    adapter.DeleteEntitiesDirectly("ProjectJobTaskTranslatedEntity", new RelationPredicateBucket(translatedExpression));
                    adapter.DeleteEntitiesDirectly("ProjectJobTaskAttachmentEntity", new RelationPredicateBucket(attachmentExpression));
                    adapter.DeleteEntitiesDirectly("ProjectJobTaskChangeRequestEntity", new RelationPredicateBucket(changeExpression));
                    adapter.DeleteEntitiesDirectly("ProjectJobTaskFinishedStatusEntity", new RelationPredicateBucket(finishedExpression));
                    adapter.DeleteEntitiesDirectly("ProjectJobTaskLabelEntity", new RelationPredicateBucket(labelExpression));
                    adapter.DeleteEntitiesDirectly("ProjectJobTaskRelationshipEntity", new RelationPredicateBucket(relationExpression));
                    adapter.DeleteEntitiesDirectly("ProjectJobTaskSignatureEntity", new RelationPredicateBucket(signatureExpression));
                    adapter.DeleteEntitiesDirectly("ProjectJobTaskWorkingDocumentEntity", new RelationPredicateBucket(wdExpression));
                    adapter.DeleteEntitiesDirectly("ProjectJobTaskWorkloadingActivityEntity", new RelationPredicateBucket(wlExpression));
                    adapter.DeleteEntitiesDirectly("ProjectJobTaskWorkLogEntity", new RelationPredicateBucket(logExpression));
                    adapter.DeleteEntitiesDirectly("ProjectJobTaskEntity", new RelationPredicateBucket(taskExpression));
                }

                scope.Complete();
            }
        }

        public ProjectJobTaskAttachmentDTO AddAttachment(Guid taskId, Guid mediaId)
        {
            var result = new ProjectJobTaskAttachmentEntity()
            {
                ProjectJobTaskId = taskId,
                MediaId = mediaId
            };

            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(result);
            }

            return result.ToDTO();
        }

        public void RemoveAttachment(Guid taskId, Guid mediaId)
        {
            PredicateExpression expression = new PredicateExpression(ProjectJobTaskAttachmentFields.ProjectJobTaskId == taskId & ProjectJobTaskAttachmentFields.MediaId == mediaId);

            using (var adapter = _fnAdapter())
            {
                adapter.DeleteEntitiesDirectly("ProjectJobTaskAttachmentEntity", new RelationPredicateBucket(expression));
            }
        }

        public byte[] SignatureToImage(string json, bool autoscale = true)
        {
            if (string.IsNullOrEmpty(json) == true)
            {
                return new byte[] { };
            }

            var converter = new SignatureToImage();

            var result = converter.SigJsonToImage(json, autoscale);

            return converter.ImageToByte2(result);
        }

        public void SetTaskStatusWhenFinishing(ProjectJobTaskDTO task)
        {
            // see if we need to move the task onto a particular user defined status as a part of finishing
            var taskType = _taskTypeRepo.GetById(task.TaskTypeId, ProjectJobTaskTypeLoadInstructions.Statusses);

            if (taskType.ProjectJobTaskTypeStatuses.Count > 0)
            {
                var setTo = from s in taskType.ProjectJobTaskTypeStatuses where s.SetToWhenFinished == true select s;
                if (setTo.Count() > 0)
                {
                    task.StatusId = setTo.First().StatusId;
                }
            }
        }

        public ProjectJobTaskRelationshipTypeDTO GetRelationshipTypeByName(string name)
        {
            PredicateExpression expression = new PredicateExpression(ProjectJobTaskRelationshipTypeFields.Name == name);

            EntityCollection<ProjectJobTaskRelationshipTypeEntity> result = new EntityCollection<ProjectJobTaskRelationshipTypeEntity>();

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), 1);
            }

            return result.Count == 0 ? null : result[0].ToDTO();
        }

        public ProjectJobTaskRelationshipTypeDTO GetRelationshipTypeById(int id)
        {
            PredicateExpression expression = new PredicateExpression(ProjectJobTaskRelationshipTypeFields.Id == id);

            EntityCollection<ProjectJobTaskRelationshipTypeEntity> result = new EntityCollection<ProjectJobTaskRelationshipTypeEntity>();

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), 1);
            }

            return result.Count == 0 ? null : result[0].ToDTO();
        }

        public ProjectJobTaskRelationshipDTO GetRelationshipBySourceId(Guid id, int relationshipType)
        {
            PredicateExpression expression = new PredicateExpression(ProjectJobTaskRelationshipFields.SourceProjectJobTaskId == id & ProjectJobTaskRelationshipFields.ProjectJobTaskRelationshipTypeId == relationshipType);

            EntityCollection<ProjectJobTaskRelationshipEntity> result = new EntityCollection<ProjectJobTaskRelationshipEntity>();

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), 1);
            }

            return result.Count == 0 ? null : result[0].ToDTO();
        }

        public ProjectJobTaskRelationshipDTO GetRelationshipByDestinationId(Guid id, int relationshipType)
        {
            PredicateExpression expression = new PredicateExpression(ProjectJobTaskRelationshipFields.DestinationProjectJobTaskId == id & ProjectJobTaskRelationshipFields.ProjectJobTaskRelationshipTypeId == relationshipType);

            EntityCollection<ProjectJobTaskRelationshipEntity> result = new EntityCollection<ProjectJobTaskRelationshipEntity>();

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), 1);
            }

            return result.Count == 0 ? null : result[0].ToDTO();
        }

        public void RemoveMedia(Guid taskId, Guid mediaId)
        {
            PredicateExpression expression = new PredicateExpression(ProjectJobTaskMediaFields.TaskId == taskId & ProjectJobTaskMediaFields.MediaId == mediaId);

            PredicateExpression mediaExpression = new PredicateExpression(MediaFields.Id == mediaId);

            using (TransactionScope scope = new TransactionScope())
            {
                using (var adapter = _fnAdapter())
                {
                    adapter.DeleteEntitiesDirectly("ProjectJobTaskMediaEntity", new RelationPredicateBucket(expression));
                    adapter.DeleteEntitiesDirectly("MediaEntity", new RelationPredicateBucket(mediaExpression));
                }

                scope.Complete();
            }
        }

        public ProjectJobTaskDTO CopyTask(ProjectJobTaskDTO source)
        {
            return source.ToEntity().ToDTO();
        }
    }
}
