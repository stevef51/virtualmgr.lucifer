﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.RunTime;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.HelperClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.Data.DTOConverters;
using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.Checklist;
using ConceptCave.Core.Coding;
using SD.LLBLGen.Pro.ORMSupportClasses;
using ConceptCave.Data;
using System.Transactions;
using System.Threading.Tasks;
using SD.LLBLGen.Pro.QuerySpec.Adapter;
using ConceptCave.Data.Linq;
using SD.LLBLGen.Pro.LinqSupportClasses;

namespace ConceptCave.Repository.Injected
{
    public class OWorkingDocumentRepository : IWorkingDocumentRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        private readonly IMediaRepository _mediaRepo;
        public OWorkingDocumentRepository(Func<DataAccessAdapter> fnAdapter, IMediaRepository mediaRepo)
        {
            _fnAdapter = fnAdapter;
            _mediaRepo = mediaRepo;
        }

        private static PrefetchPath2 BuildPrefetch(WorkingDocumentLoadInstructions instructions)
        {
            PrefetchPath2 path = new PrefetchPath2((int)EntityType.WorkingDocumentEntity);

            if ((instructions & WorkingDocumentLoadInstructions.Data) == WorkingDocumentLoadInstructions.Data)
            {
                path.Add(WorkingDocumentEntity.PrefetchPathWorkingDocumentData);
            }

            if ((instructions & WorkingDocumentLoadInstructions.References) == WorkingDocumentLoadInstructions.References)
            {
                path.Add(WorkingDocumentEntity.PrefetchPathWorkingDocumentReferences);
            }

            if ((instructions & WorkingDocumentLoadInstructions.Users) == WorkingDocumentLoadInstructions.Users)
            {
                path.Add(WorkingDocumentEntity.PrefetchPathReviewer);
                path.Add(WorkingDocumentEntity.PrefetchPathReviewee);
            }

            if ((instructions & WorkingDocumentLoadInstructions.Publishing) == WorkingDocumentLoadInstructions.Publishing)
            {
                IPrefetchPathElement2 sub = path.Add(WorkingDocumentEntity.PrefetchPathPublishingGroupResource);

                if ((instructions & WorkingDocumentLoadInstructions.Resource) == WorkingDocumentLoadInstructions.Resource)
                {
                    IPrefetchPathElement2 subsub = sub.SubPath.Add(PublishingGroupResourceEntity.PrefetchPathResource);

                    if ((instructions & WorkingDocumentLoadInstructions.ResourceLabels) == WorkingDocumentLoadInstructions.ResourceLabels)
                    {
                        subsub.SubPath.Add(ResourceEntity.PrefetchPathLabelResources).SubPath.Add(LabelResourceEntity.PrefetchPathLabel);
                    }
                }
            }

            if ((instructions & WorkingDocumentLoadInstructions.PublicVariables) == WorkingDocumentLoadInstructions.PublicVariables)
            {
                path.Add(WorkingDocumentEntity.PrefetchPathWorkingDocumentPublicVariables);
            }

            if ((instructions & WorkingDocumentLoadInstructions.CompletedData) == WorkingDocumentLoadInstructions.CompletedData)
            {
                throw new NotImplementedException();

                //path.Add(WorkingDocumentEntity.PrefetchPathCompletedWorkingDocument);
                //path.Add(WorkingDocumentEntity.PrefetchPathCompletedWorkingDocumentSections);
                //path.Add(WorkingDocumentEntity.PrefetchPathCompletedWorkingDocumentQuestions);
            }

            return path;
        }


        public void Save(IWorkingDocument document, WorkingDocumentDTO dto, bool refetch, bool recurse)
        {
            IEncoder encoder = CoderFactory.CreateEncoder();

            encoder.Encode(WorkingDocument.WorkingDocumentRootElementName, document);

            if (dto.__IsNew && dto.WorkingDocumentData == null)
            {
                dto.WorkingDocumentData = new WorkingDocumentDataDTO();
            }

            dto.WorkingDocumentData.Data = encoder.ToString();

            Save(dto, refetch, recurse);
        }

        public void DeleteWorkingDocumentsForUser(Guid userId)
        {
            PredicateExpression expression = new PredicateExpression(WorkingDocumentFields.ReviewerId == userId | WorkingDocumentFields.RevieweeId == userId);

            using (var adapter = _fnAdapter())
            {
                adapter.DeleteEntitiesDirectly(typeof(WorkingDocumentEntity), new RelationPredicateBucket(expression));
            }
        }

        public WorkingDocumentDTO Save(WorkingDocumentDTO dto, bool refetch, bool recurse)
        {
            dto.UtcLastModified = DateTime.UtcNow;
            if (dto.WorkingDocumentData != null)
            {
                dto.WorkingDocumentData.UtcLastModified = DateTime.UtcNow;
            }
            using (var adapter = _fnAdapter())
            {
                var entity = dto.ToEntity();
                adapter.SaveEntity(entity, refetch, recurse);
                return refetch ? entity.ToDTO() : dto;
            }
        }

        public WorkingDocumentDTO GetByInternalId(Guid id, WorkingDocumentLoadInstructions instructions)
        {
            PredicateExpression expression = new PredicateExpression(WorkingDocumentFields.Id == id);

            EntityCollection<WorkingDocumentEntity> result = new EntityCollection<WorkingDocumentEntity>();

            PrefetchPath2 path = BuildPrefetch(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), 1, null, path);
            }

            return result.FirstOrDefault()?.ToDTO();
        }

        public WorkingDocumentDTO GetById(Guid id, WorkingDocumentLoadInstructions instructions)
        {
            WorkingDocumentEntity entity = new WorkingDocumentEntity(id);

            PrefetchPath2 path = BuildPrefetch(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntity(entity, path);
            }

            return entity.IsNew ? null : entity.ToDTO();
        }

        public async Task<WorkingDocumentDTO> GetByIdAsync(Guid id, WorkingDocumentLoadInstructions instructions)
        {
            PrefetchPath2 path = BuildPrefetch(instructions);

            using (var adapter = _fnAdapter())
            {
                var lmd = new LinqMetaData(adapter);
                var results = await (from wd in lmd.WorkingDocument where wd.Id == id select wd).WithPath(path).ToListAsync();
                return results.Select(e => e.ToDTO()).FirstOrDefault();
            }
        }

        public List<WorkingDocumentDTO> GetByParentId(Guid parentId, WorkingDocumentLoadInstructions instructions)
        {
            PredicateExpression expression = new PredicateExpression(WorkingDocumentFields.ParentId == parentId);

            SortExpression sort = new SortExpression(WorkingDocumentFields.DateCreated | SortOperator.Ascending);

            PrefetchPath2 path = BuildPrefetch(instructions);

            EntityCollection<WorkingDocumentEntity> result = new EntityCollection<WorkingDocumentEntity>();

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), 0, sort, path);
            }

            return result.Select(e => e.ToDTO()).ToList();
        }

        protected List<WorkingDocumentDTOHierachyItem> LoadDescendants(Guid id, WorkingDocumentLoadInstructions instructions)
        {
            var result = new List<WorkingDocumentDTOHierachyItem>();

            var children = GetByParentId(id, instructions);

            foreach (var c in children)
            {
                var h = new WorkingDocumentDTOHierachyItem() { Dto = c };

                var grandChildren = LoadDescendants(c.Id, instructions);

                h.Children.AddRange(grandChildren);

                result.Add(h);
            }

            return result;
        }

        public WorkingDocumentDTOHierachyItem GetByIdWithDescendants(Guid id, WorkingDocumentLoadInstructions instructions)
        {
            WorkingDocumentDTOHierachyItem result = new WorkingDocumentDTOHierachyItem();

            var top = GetById(id, instructions);
            if (top == null)
            {
                return null;
            }

            result.Dto = top;

            var descendants = LoadDescendants(id, instructions);

            result.Children.AddRange(descendants);

            return result;
        }

        public void DeleteWorkingDocument(Guid id, bool deleteReportingData)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                using (var adapter = _fnAdapter())
                {
                    if (deleteReportingData == true)
                    {
                        adapter.DeleteEntitiesDirectly(typeof(CompletedWorkingDocumentPresentedFactEntity), new RelationPredicateBucket(new PredicateExpression(CompletedWorkingDocumentPresentedFactFields.WorkingDocumentId == id)));
                        adapter.DeleteEntitiesDirectly(typeof(CompletedWorkingDocumentFactEntity), new RelationPredicateBucket(new PredicateExpression(CompletedWorkingDocumentFactFields.WorkingDocumentId == id)));
                    }

                    adapter.DeleteEntitiesDirectly(typeof(WorkingDocumentEntity), new RelationPredicateBucket(new PredicateExpression(WorkingDocumentFields.Id == id)));
                }

                scope.Complete();
            }
        }

        public IList<WorkingDocumentDTO> GetCurrentWorkingDocuments(int groupid, int resourceid, Guid reviewerid, Guid? revieweeid,
                                                           WorkingDocumentLoadInstructions instructions)
        {
            PredicateExpression expression = new PredicateExpression(WorkingDocumentFields.ReviewerId == reviewerid);
            expression.AddWithAnd(new FieldCompareSetPredicate(WorkingDocumentFields.PublishingGroupResourceId, null, PublishingGroupResourceFields.Id, null, SetOperator.In, PublishingGroupResourceFields.PublishingGroupId == groupid & PublishingGroupResourceFields.ResourceId == resourceid));

            if (revieweeid.HasValue)
            {
                expression.AddWithAnd(WorkingDocumentFields.RevieweeId == revieweeid);
            }
            else
            {
                expression.AddWithAnd(WorkingDocumentFields.RevieweeId == DBNull.Value);
            }

            expression.AddWithAnd(WorkingDocumentFields.Status == (int)WorkingDocumentStatus.Started);

            SortExpression sort = new SortExpression(WorkingDocumentFields.DateCreated | SortOperator.Ascending);

            PrefetchPath2 path = BuildPrefetch(instructions);

            EntityCollection<WorkingDocumentEntity> result = new EntityCollection<WorkingDocumentEntity>();

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), 0, sort, path);
            }

            return result.Select(e => e.ToDTO()).ToList();
        }

        public IList<WorkingDocumentDTO> GetCurrentWorkingDocuments(int publishingGroupResourceId, WorkingDocumentLoadInstructions instructions)
        {
            PredicateExpression expression = new PredicateExpression(WorkingDocumentFields.PublishingGroupResourceId == publishingGroupResourceId & WorkingDocumentFields.Status == (int)WorkingDocumentStatus.Started);

            SortExpression sort = new SortExpression(WorkingDocumentFields.DateCreated | SortOperator.Ascending);

            PrefetchPath2 path = BuildPrefetch(instructions);

            EntityCollection<WorkingDocumentEntity> result = new EntityCollection<WorkingDocumentEntity>();

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), 0, sort, path);
            }

            return result.Select(e => e.ToDTO()).ToList();
        }

        public List<WorkingDocumentDTO> GetCurrentWorkingDocuments(Guid reviewerid, WorkingDocumentLoadInstructions instructions = WorkingDocumentLoadInstructions.None)
        {
            PredicateExpression expression = new PredicateExpression(WorkingDocumentFields.ReviewerId == reviewerid);

            expression.AddWithAnd(WorkingDocumentFields.Status == (int)WorkingDocumentStatus.Started);

            SortExpression sort = new SortExpression(WorkingDocumentFields.DateCreated | SortOperator.Ascending);

            EntityCollection<WorkingDocumentEntity> result = new EntityCollection<WorkingDocumentEntity>();

            PrefetchPath2 path = BuildPrefetch(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), 0, sort, path);
            }

            return result.Select(e => e.ToDTO()).ToList();
        }

        public void DeleteIncompleteWorkingDocuments(DateTime inMaxDate)
        {
            PredicateExpression expression = new PredicateExpression(WorkingDocumentFields.CleanUpIfNotFinishedAfter != DBNull.Value &
                WorkingDocumentFields.CleanUpIfNotFinishedAfter <= inMaxDate &
                WorkingDocumentFields.Status == (int)WorkingDocumentStatus.Started &
                ProjectJobTaskWorkingDocumentFields.WorkingDocumentId == DBNull.Value); // don't delete anything related to a task

            RelationPredicateBucket bucket = new RelationPredicateBucket(expression);
            bucket.Relations.Add(WorkingDocumentEntity.Relations.ProjectJobTaskWorkingDocumentEntityUsingWorkingDocumentId, JoinHint.Left);


            using (var adapter = _fnAdapter())
            {
                adapter.DeleteEntitiesDirectly("WorkingDocumentEntity", bucket);
            }
        }

        public void DeleteCompleteWorkingDocuments(DateTime inMaxDate)
        {
            PredicateExpression expression = new PredicateExpression(WorkingDocumentFields.CleanUpIfFinishedAfter != DBNull.Value &
                WorkingDocumentFields.CleanUpIfFinishedAfter <= inMaxDate &
                WorkingDocumentFields.Status == (int)WorkingDocumentStatus.Completed &
                ProjectJobTaskWorkingDocumentFields.WorkingDocumentId == DBNull.Value); // we don't delete anything tied to a task

            RelationPredicateBucket bucket = new RelationPredicateBucket(expression);
            bucket.Relations.Add(WorkingDocumentEntity.Relations.ProjectJobTaskWorkingDocumentEntityUsingWorkingDocumentId, JoinHint.Left);

            using (var adapter = _fnAdapter())
            {
                adapter.DeleteEntitiesDirectly("WorkingDocumentEntity", bucket);
            }
        }

        public IList<WorkingDocumentDTO> GetByDate(DateTime startDate, DateTime endDate, WorkingDocumentLoadInstructions instructions,
                                          WorkingDocumentStatus? status)
        {
            PredicateExpression expression = new PredicateExpression(WorkingDocumentFields.DateCreated >= startDate & WorkingDocumentFields.DateCreated < endDate);
            if (status.HasValue)
            {
                expression.AddWithAnd(WorkingDocumentFields.Status == (int)status.Value);
            }

            PrefetchPath2 path = BuildPrefetch(instructions);

            EntityCollection<WorkingDocumentEntity> result = new EntityCollection<WorkingDocumentEntity>();

            SortExpression sort = new SortExpression(WorkingDocumentFields.DateCreated | SortOperator.Descending);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), 0, sort, path);
            }

            return result.Select(e => e.ToDTO()).ToList();
        }

        public IAnswer SaveNotesAttachment(Guid rootId, Guid presentedId, IPostedFile fileUpload)
        {
            var entity = GetById(rootId, WorkingDocumentLoadInstructions.Data);

            WorkingDocument document = (WorkingDocument)WorkingDocument.CreateFromString(entity.WorkingDocumentData.Data);

            var currentPresented = document.GetPresented();

            var presented = (from p in currentPresented.AsDepthFirstEnumerable() where p.Id == presentedId select p);

            if (presented.Count() == 0)
            {
                throw new ArgumentException(string.Format("Could not find presented with id = {0}", presentedId));
            }

            // going to assume notes are only for questions at the moment
            IPresentedQuestion pq = (IPresentedQuestion)presented.First();
            Answer answer = (Answer)(pq).GetAnswer();

            if (answer.Notes == null)
            {
                answer.Notes = new AnswerNotes();
            }

            using (TransactionScope scope = new TransactionScope())
            {
                var media = _mediaRepo.CreateMediaFromPostedFile(fileUpload);

                AnswerNotesAttachment attachment = new AnswerNotesAttachment()
                {
                    Name = fileUpload.FileName,
                    ContentType = fileUpload.ContentType,
                    ProviderId = media.Id
                };
                answer.Notes.Attachments.Add(attachment);

                Save(document, entity, true, true);

                scope.Complete();
            }

            return answer;
        }
    }
}
