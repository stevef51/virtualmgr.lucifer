﻿using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.DTOConverters;
using ConceptCave.RepositoryInterfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualMgr.Central;
using SD.LLBLGen.Pro.ORMSupportClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.Data;
using ConceptCave.Data.Linq;
using ConceptCave.Data.HelperClasses;
using ConceptCave.DTO.DTOClasses;
using SD.LLBLGen.Pro.LinqSupportClasses;
using ConceptCave.Data.DatabaseSpecific;

namespace ConceptCave.Repository.Injected
{
    public class OESSensorRepository : IESSensorRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public OESSensorRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }

        private PrefetchPath2 BuildPrefetchPath(ESSensorLoadInstructions instructions)
        {
            PrefetchPath2 path = new PrefetchPath2(EntityType.ESSensorEntity);
            if ((instructions & ESSensorLoadInstructions.Probe) != 0)
            {
                var a = path.Add(ESSensorEntity.PrefetchPathProbe);
            }
            if ((instructions & ESSensorLoadInstructions.Readings) != 0)
            {
                var a = path.Add(ESSensorEntity.PrefetchPathSensorReadings);
            }

            return path;
        }

        public DTO.DTOClasses.ESSensorDTO GetById(int id, ESSensorLoadInstructions instructions = ESSensorLoadInstructions.None)
        {
            var e = new ESSensorEntity(id);
            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntity(e, BuildPrefetchPath(instructions));
            }
            return e.IsNew ? null : e.ToDTO();
        }

        public ESSensorDTO GetByProbeIdSensorIndex(int probeId, int sensorIndex, ESSensorLoadInstructions instructions = ESSensorLoadInstructions.None)
        {
            var e = new EntityCollection<ESSensorEntity>();
            var predicate = new PredicateExpression(ESSensorFields.ProbeId == probeId & ESSensorFields.SensorIndex == sensorIndex);
            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(e, new RelationPredicateBucket(predicate), BuildPrefetchPath(instructions));
            }
            return e.Count == 0 ? null : e.First().ToDTO();
        }

        public DTO.DTOClasses.ESSensorDTO Save(DTO.DTOClasses.ESSensorDTO dto, bool refetch, bool recurse)
        {
            var e = dto.__IsNew ? new ESSensorEntity() : new ESSensorEntity(dto.Id);

            using (var adapter = _fnAdapter())
            {
                if (!dto.__IsNew)
                {
                    adapter.FetchEntity(e);
                }

                dto.ToEntity(e);

                adapter.SaveEntity(e, refetch, recurse);
            }

            return refetch ? e.ToDTO() : dto;
        }
    }
}
