﻿using ConceptCave.Data.EntityClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;
using ConceptCave.Data;
using ConceptCave.Data.DTOConverters;
using System;
using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Repositories;

namespace ConceptCave.Repository.Injected
{
    public class OMembershipTypeContextPublishedResource : IMembershipTypeContextPublishedResource
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public OMembershipTypeContextPublishedResource(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }

        private static PrefetchPath2 BuildPrefetchPath(UserTypeContextLoadInstructions instructions)
        {
            PrefetchPath2 path = new PrefetchPath2((int)EntityType.UserTypeContextPublishedResourceEntity);

            if ((instructions & UserTypeContextLoadInstructions.PublishingGroupResource) == UserTypeContextLoadInstructions.PublishingGroupResource)
            {
                var subPath = path.Add(UserTypeContextPublishedResourceEntity.PrefetchPathPublishingGroupResource);

                if ((instructions & UserTypeContextLoadInstructions.PublishingGroupResourceData) == UserTypeContextLoadInstructions.PublishingGroupResourceData)
                {
                    subPath.SubPath.Add(PublishingGroupResourceEntity.PrefetchPathPublishingGroupResourceData);
                }

                if ((instructions & UserTypeContextLoadInstructions.Resource) == UserTypeContextLoadInstructions.Resource)
                {
                    subPath.SubPath.Add(PublishingGroupResourceEntity.PrefetchPathResource);
                }
            }
            return path;
        }

        public UserTypeContextPublishedResourceDTO GetById(int id, UserTypeContextLoadInstructions instructions)
        {
            var result = new UserTypeContextPublishedResourceEntity(id);

            PrefetchPath2 path = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntity(result, path);
            }

            return result.IsNew == true ? null : result.ToDTO();
        }

    }
}
