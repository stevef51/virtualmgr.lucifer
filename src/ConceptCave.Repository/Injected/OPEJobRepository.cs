﻿using ConceptCave.Data.Linq;
using ConceptCave.RepositoryInterfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualMgr.Central;
using ConceptCave.Data.DTOConverters;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Data.EntityClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;
using ConceptCave.Data;
using SD.LLBLGen.Pro.LinqSupportClasses;
using ConceptCave.Data.DatabaseSpecific;

namespace ConceptCave.Repository.Injected
{
    public class OPEJobRepository : IPEJobRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public OPEJobRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }
        private PrefetchPath2 BuildPrefetchPath(PEJobLoadInstructions instructions)
        {
            var path = new PrefetchPath2(EntityType.AssetTypeEntity);

            if ((instructions & PEJobLoadInstructions.History) == PEJobLoadInstructions.History)
            {
                path.Add(PEJobEntity.PrefetchPathJobHistories);
            }
            return path;
        }

        public PEJobDTO GetById(int id, PEJobLoadInstructions loadInstructions = PEJobLoadInstructions.None)
        {
            var prefetchPath = BuildPrefetchPath(loadInstructions);
            using (var adapter = _fnAdapter())
            {
                var data = new LinqMetaData(adapter);
                return (from r in data.PEJob.WithPath(prefetchPath)
                        where r.Id == id
                        select r).FirstOrDefault()?.ToDTO();
            }
        }

        public PEJobDTO Save(PEJobDTO dto, bool refetch)
        {
            using (var adapter = _fnAdapter())
            {
                var e = dto.ToEntity();
                adapter.SaveEntity(e, refetch, false);
                return refetch ? e.ToDTO() : dto;
            }
        }

        public IList<PEJobDTO> Search(string nameLike, int? pageNumber, int? itemsPerPage, bool includeArchived, ref int totalItems)
        {
            using (var adapter = _fnAdapter())
            {
                var lmd = new LinqMetaData(adapter);
                var items = from r in lmd.PEJob where r.Name == null || r.Name != null && r.Name.Contains(nameLike) select r.ToDTO();
                items = items.OrderBy(i => i.Name);

                if (!includeArchived)
                    items = items.Where(i => i.Archived == false);

                totalItems = items.Count();

                if (pageNumber.HasValue && itemsPerPage.HasValue)
                    items = items.Skip((pageNumber.Value - 1) * itemsPerPage.Value).Take(itemsPerPage.Value);

                return items.ToList();
            }
        }

        public void Delete(int id, bool archiveIfRequired, out bool archived)
        {
            using (var adapter = _fnAdapter())
            {
                var entity = new PEJobEntity(id);
                archived = false;
                try
                {
                    adapter.DeleteEntity(entity);
                }
                catch (Exception ex)
                {
                    if (archiveIfRequired)
                    {
                        adapter.FetchEntity(entity);
                        entity.Archived = true;
                        adapter.SaveEntity(entity, false);
                        archived = true;
                    }
                    else
                        throw ex;
                }
            }
        }

        public PEJobHistoryDTO Save(PEJobHistoryDTO dto, bool refetch)
        {
            using (var adapter = _fnAdapter())
            {
                var e = dto.ToEntity();
                adapter.SaveEntity(e, refetch, false);
                return refetch ? e.ToDTO() : dto;
            }
        }

        public IList<PEJobDTO> FindOverdue(DateTime nowUtc, PEJobLoadInstructions instructions = PEJobLoadInstructions.None)
        {
            using (var adapter = _fnAdapter())
            {
                var data = new LinqMetaData(adapter);
                var query = from e in data.PEJob.WithPath(BuildPrefetchPath(instructions)) where !e.Archived && e.Enabled && (!e.NextRunUtc.HasValue || nowUtc >= e.NextRunUtc.Value) select e;
                return (from e in query select e.ToDTO()).ToList();
            }
        }

        public IList<PEJobDTO> GetAll(PEJobLoadInstructions instructions = PEJobLoadInstructions.None)
        {
            using (var adapter = _fnAdapter())
            {
                var data = new LinqMetaData(adapter);
                var query = from e in data.PEJob.WithPath(BuildPrefetchPath(instructions)) select e;
                return (from e in query select e.ToDTO()).ToList();
            }
        }
    }
}
