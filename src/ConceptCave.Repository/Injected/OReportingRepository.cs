﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Checklist;
using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.Data.Linq;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Data.DTOConverters;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.HelperClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using DocumentFormat.OpenXml.Wordprocessing;
using SD.LLBLGen.Pro.ORMSupportClasses;
using ConceptCave.Repository;
using ConceptCave.RepositoryInterfaces;

namespace ConceptCave.Repository.Injected
{
    public class OReportingRepository : IReportingRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;

        private readonly IMembershipRepository _membershipRepo;
        private readonly ICompletedWorkingDocumentPresentedFactRepository _completedWorkingDocumentPresentedFactRepo;

        public OReportingRepository(Func<DataAccessAdapter> fnAdapter, IMembershipRepository membershipRepo, ICompletedWorkingDocumentPresentedFactRepository completedWorkingDocumentPresentedFactRepo)
        {
            _membershipRepo = membershipRepo;
            _fnAdapter = fnAdapter;
            _completedWorkingDocumentPresentedFactRepo = completedWorkingDocumentPresentedFactRepo;
        }

        public IList<CompletedWorkingDocumentPresentedFactDTO> GetContextAnswers(Guid workingDocumentId)
        {
            return _completedWorkingDocumentPresentedFactRepo.Get(workingDocumentId, CompletedWorkingDocumentPresentedFactLoadInstructions.CompletedPresentedFactValue
                | CompletedWorkingDocumentPresentedFactLoadInstructions.CompletedUploadMedia);
        }

        public IList<CompletedUserDimensionDTO> AddToUsersDimension(IList<Guid> userIds)
        {
            using (var adapter = _fnAdapter())
            {
                var entCollection = new EntityCollection<CompletedUserDimensionEntity>();

                foreach (var userId in userIds.Distinct())
                {
                    //We need to do it this way because there could be dupes, and we want to return the same
                    //number of entities as ids passed in
                    var user = _membershipRepo.GetById(userId, MembershipLoadInstructions.AspNetMembership);

                    var dimEnt = new CompletedUserDimensionEntity(user.UserId);
                    if (!adapter.FetchEntity(dimEnt))
                        dimEnt = new CompletedUserDimensionEntity(user.UserId);

                    dimEnt.Username = user.AspNetUser.UserName;
                    dimEnt.Name = user.Name;
                    dimEnt.Email = user.AspNetUser.Email;
                    dimEnt.TimeZone = user.TimeZone;

                    entCollection.Add(dimEnt);
                }

                //and we can save all of these now
                adapter.SaveEntityCollection(entCollection, true, false);

                //map input to output
                var retCollection = new List<CompletedUserDimensionDTO>();
                foreach (var id in userIds)
                    retCollection.Add(entCollection.Single(e => e.Id == id).ToDTO());

                return retCollection;
            }
        }

        public IList<CompletedLabelDimensionDTO> AddToLabelsDimension(IList<LabelDTO> labels)
        {
            using (var adapter = _fnAdapter())
            {
                var data = new LinqMetaData(adapter);
                var entCollection = new EntityCollection<CompletedLabelDimensionEntity>();
                var labelIds = labels.Select(l => l.Id).ToList();
                var lblEnts = data.Label.Where(lbl => labelIds.Contains(lbl.Id));

                foreach (var label in lblEnts.DistinctBy(l => l.Id))
                {
                    var dimEnt = new CompletedLabelDimensionEntity(label.Id);
                    if (!adapter.FetchEntity(dimEnt))
                        dimEnt = new CompletedLabelDimensionEntity(label.Id);

                    dimEnt.Name = label.Name;
                    dimEnt.Forecolor = label.Forecolor;
                    dimEnt.Backcolor = label.Backcolor;

                    entCollection.Add(dimEnt);
                }

                //and we can save all of these now
                adapter.SaveEntityCollection(entCollection, true, false);

                //map input to output
                var retCollection = new List<CompletedLabelDimensionDTO>();
                foreach (var id in labels.Select(l => l.Id))
                    retCollection.Add(entCollection.Single(e => e.Id == id).ToDTO());

                return retCollection;
            }
        }

        public CompletedWorkingDocumentDimensionDTO AddToWorkingDocumentDimension(WorkingDocumentDTO doc)
        {
            using (var adapter = _fnAdapter())
            {
                var joinEnt = new CompletedWorkingDocumentDimensionEntity(doc.PublishingGroupResource.Resource.NodeId);
                if (!adapter.FetchEntity(joinEnt))
                    joinEnt = new CompletedWorkingDocumentDimensionEntity(doc.PublishingGroupResource.Resource.NodeId);

                joinEnt.Name = doc.Name;

                adapter.SaveEntity(joinEnt, true);
                return joinEnt.ToDTO();
            }
        }

        public CompletedWorkingDocumentFactDTO WriteNewWorkingDocumentFact(WorkingDocumentDTO doc, CompletedUserDimensionDTO reviewer,
            CompletedUserDimensionDTO reviewee, CompletedWorkingDocumentDimensionDTO documentDimension)
        {
            using (var adapter = _fnAdapter())
            {
                //If they provided a doc with a WorkingDocumentId, we assume they want to overwrite
                var factEnt = new CompletedWorkingDocumentFactEntity(doc.Id);
                if (!adapter.FetchEntity(factEnt))
                    factEnt = new CompletedWorkingDocumentFactEntity(doc.Id);

                factEnt.WorkingDocumentId = doc.Id;
                factEnt.ReportingWorkingDocumentId = documentDimension.Id;
                factEnt.ReportingUserRevieweeId = reviewee.Id;
                factEnt.ReportingUserReviewerId = reviewer.Id;
                factEnt.ParentWorkingDocumentId = doc.ParentId;
                factEnt.DateStarted = doc.DateCreated;
                factEnt.DateCompleted = DateTime.UtcNow;
                factEnt.ClientIp = doc.ClientIp;
                factEnt.UserAgentId = doc.UserAgentId;
                factEnt.PassedQuestionCount = 0;
                factEnt.FailedQuestionCount = 0;
                factEnt.NaquestionCount = 0;
                factEnt.TotalQuestionCount = 0;
                factEnt.TotalPossibleScore = 0;
                factEnt.TotalScore = 0;
                factEnt.IsUserContext = false;

                adapter.SaveEntity(factEnt, true, false);
                return factEnt.ToDTO();
            }
        }

        public void WriteLabelRelations(CompletedWorkingDocumentFactDTO doc, IList<CompletedLabelDimensionDTO> labels)
        {
            using (var adapter = _fnAdapter())
            {
                var entCollection = new EntityCollection<CompletedLabelWorkingDocumentDimensionEntity>();
                foreach (var label in labels.DistinctBy(l => l.Id))
                {
                    var ent = new CompletedLabelWorkingDocumentDimensionEntity(label.Id, doc.WorkingDocumentId);
                    if (!adapter.FetchEntity(ent))
                        entCollection.Add(ent); //add if not exists
                }
                adapter.SaveEntityCollection(entCollection);
            }
        }

        public void WriteLabelRelations(CompletedWorkingDocumentPresentedFactDTO presented, IList<CompletedLabelDimensionDTO> labels)
        {
            using (var adapter = _fnAdapter())
            {
                var entCollection = new EntityCollection<CompletedLabelWorkingDocumentPresentedDimensionEntity>();
                foreach (var label in labels.DistinctBy(l => l.Id))
                {
                    var ent = new CompletedLabelWorkingDocumentPresentedDimensionEntity(label.Id, presented.Id);
                    if (!adapter.FetchEntity(ent))
                        entCollection.Add(ent); //add if not exists
                }
                adapter.SaveEntityCollection(entCollection);
            }
        }

        public IList<CompletedPresentedTypeDimensionDTO> AddToTypeDimension(IList<string> qTypeNames)
        {
            using (var adapter = _fnAdapter())
            {
                var entCollection = new EntityCollection<CompletedPresentedTypeDimensionEntity>();
                foreach (var name in qTypeNames.Distinct())
                {
                    entCollection.Add(new CompletedPresentedTypeDimensionEntity()
                    {
                        Type = name,
                        Id = CombFactory.NewComb()
                    });
                }
                adapter.SaveEntityCollection(entCollection, true, false);
                //map input to output
                var retCollection = new List<CompletedPresentedTypeDimensionDTO>();
                foreach (var name in qTypeNames)
                    retCollection.Add(entCollection.Single(e => e.Type == name).ToDTO());

                return retCollection;
            }
        }

        public CompletedWorkingDocumentFactDTO Save(CompletedWorkingDocumentFactDTO graph, bool refetch, bool recurse)
        {
            //This guy has to persist the entire object graph associated with graph, is always called with
            //recurse, i believe
            using (var adapter = _fnAdapter())
            {
                var e = graph.ToEntity();
                adapter.SaveEntity(e, refetch, recurse);
                return refetch ? e.ToDTO() : graph;
            }
        }

        //Used when overwriting existing answers from user context checklists
        public void DeletePresentedFactsForDocument(Guid completedWorkingDocumentId)
        {
            using (var adapter = _fnAdapter())
            {
                RelationPredicateBucket bucket;
                var predicate = CompletedWorkingDocumentPresentedFactFields.WorkingDocumentId ==
                                completedWorkingDocumentId;

                bucket = new RelationPredicateBucket(predicate);
                bucket.Relations.Add(
                    CompletedPresentedFactValueEntity.Relations
                        .CompletedWorkingDocumentPresentedFactEntityUsingCompletedWorkingDocumentPresentedFactId);
                adapter.DeleteEntitiesDirectly(typeof(CompletedPresentedGeoLocationFactValueEntity), bucket);
                adapter.DeleteEntitiesDirectly(typeof(CompletedPresentedFactExtraValueEntity), bucket);

                bucket = new RelationPredicateBucket(predicate);
                bucket.Relations.Add(
                    CompletedPresentedUploadMediaFactValueEntity.Relations
                        .CompletedWorkingDocumentPresentedFactEntityUsingCompletedWorkingDocumentPresentedFactId);
                adapter.DeleteEntitiesDirectly(typeof(CompletedPresentedUploadMediaFactValueEntity), bucket);

                bucket = new RelationPredicateBucket(predicate);
                bucket.Relations.Add(
                    CompletedPresentedFactValueEntity.Relations
                        .CompletedWorkingDocumentPresentedFactEntityUsingCompletedWorkingDocumentPresentedFactId);
                adapter.DeleteEntitiesDirectly(typeof(CompletedPresentedFactValueEntity), bucket);

                bucket = new RelationPredicateBucket(predicate);
                adapter.DeleteEntitiesDirectly(typeof(CompletedWorkingDocumentPresentedFactEntity), bucket);
            }
        }
    }
}
