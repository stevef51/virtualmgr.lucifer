﻿using System;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.Data.DTOConverters;
using ConceptCave.RepositoryInterfaces.Repositories;

namespace ConceptCave.Repository.Injected
{
    public class OMimeTypeRepository : IMimeTypeRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public OMimeTypeRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }
        public MimeTypeDTO GetByFilename(string filename)
        {
            if (string.IsNullOrEmpty(filename) == true)
            {
                return null;
            }

            string ext = System.IO.Path.GetExtension(filename);

            return GetByExtension(ext);
        }

        public MimeTypeDTO GetByExtension(string extension)
        {
            if (string.IsNullOrEmpty(extension) == true)
            {
                return null;
            }

            extension = extension.TrimStart('.');

            MimeTypeEntity entity = new MimeTypeEntity();
            entity.Extension = extension;

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityUsingUniqueConstraint(entity, entity.ConstructFilterForUCExtension());
            }

            return entity.IsNew == true ? null : entity.ToDTO();
        }
    }
}
