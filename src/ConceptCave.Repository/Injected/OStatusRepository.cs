﻿using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.HelperClasses;
using ConceptCave.RepositoryInterfaces.Repositories;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using ConceptCave.Data.DTOConverters;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Repository.Injected
{
    public class OStatusRepository : IStatusRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public OStatusRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }

        public IList<DTO.DTOClasses.ProjectJobStatusDTO> GetAll()
        {
            EntityCollection<ProjectJobStatusEntity> result = new EntityCollection<ProjectJobStatusEntity>();

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket());
            }

            return result.ToList().Select(r => r.ToDTO()).ToList();
        }

        public ProjectJobStatusDTO GetById(Guid id)
        {
            ProjectJobStatusEntity result = new ProjectJobStatusEntity(id);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntity(result);
            }

            return result.IsNew == true ? null : result.ToDTO();
        }
        public IList<DTO.DTOClasses.ProjectJobStatusDTO> GetById(IEnumerable<Guid> ids)
        {
            PredicateExpression expression = new PredicateExpression(new FieldCompareRangePredicate(ProjectJobStatusFields.Id, null, ids));
            EntityCollection<ProjectJobStatusEntity> result = new EntityCollection<ProjectJobStatusEntity>();

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression));
            }

            return result.ToList().Select(r => r.ToDTO()).ToList();
        }

        public ProjectJobStatusDTO Save(ProjectJobStatusDTO status, bool refetch, bool recurse)
        {
            var e = status.ToEntity();

            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(e, refetch, recurse);
            }

            return refetch ? e.ToDTO() : status;
        }
    }
}
