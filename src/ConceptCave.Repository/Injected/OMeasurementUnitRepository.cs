﻿using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.HelperClasses;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.Data.DTOConverters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Data.Linq;
using SD.LLBLGen.Pro.LinqSupportClasses;
using ConceptCave.DTO.DTOClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.Data;
using VirtualMgr.Central;

namespace ConceptCave.Repository.Injected
{
    public class OMeasurementUnitRepository : IMeasurementUnitRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public OMeasurementUnitRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }

        public MeasurementUnitDTO GetById(int id)
        {
            MeasurementUnitEntity result = new MeasurementUnitEntity(id);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntity(result);
            }

            return result.IsNew == true ? null : result.ToDTO();
        }

        public IList<MeasurementUnitDTO> GetAll()
        {
            EntityCollection<MeasurementUnitEntity> result = new EntityCollection<MeasurementUnitEntity>();

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket());
            }

            return result.ToList().Select(r => r.ToDTO()).ToList();
        }
    }
}
