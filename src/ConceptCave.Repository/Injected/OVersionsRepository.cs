﻿using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.Data.EntityClasses;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.Data.DTOConverters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Data.HelperClasses;
using VirtualMgr.Central;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;

namespace ConceptCave.Repository.Injected
{
    public class OVersionsRepository : IVersionsRepository
    {
        private readonly Func<ConceptCave.Data.DatabaseSpecific.DataAccessAdapter> _fnTenantAdapter;
        private readonly Func<VirtualMgr.Central.Data.DatabaseSpecific.DataAccessAdapter> _fnTenantMasterAdapter;

        public OVersionsRepository(
            Func<VirtualMgr.Central.Data.DatabaseSpecific.DataAccessAdapter> fnTenantMasterAdapter,
            Func<ConceptCave.Data.DatabaseSpecific.DataAccessAdapter> fnTenantAdapter)
        {
            _fnTenantMasterAdapter = fnTenantMasterAdapter;
            _fnTenantAdapter = fnTenantAdapter;
        }

        public DTO.DTOClasses.VersionDTO GetVersion(string id, RepositoryInterfaces.Enums.VersionLoadInstructions loadInstructions)
        {
            var entity = new VersionEntity(id);

            using (var adapter = _fnTenantAdapter())
            {
                adapter.FetchEntity(entity);
            }

            return entity.IsNew ? null : entity.ToDTO();
        }

        public void Save(DTO.DTOClasses.VersionDTO dto)
        {
            using (var adapter = _fnTenantAdapter())
            {
                adapter.SaveEntity(dto.ToEntity(), true, true);
            }
        }

        public IEnumerable<DTO.DTOClasses.VersionDTO> GetAll(RepositoryInterfaces.Enums.VersionLoadInstructions loadInstructions)
        {
            EntityCollection<VersionEntity> list = new EntityCollection<VersionEntity>();

            using (var adapter = _fnTenantAdapter())
            {
                adapter.FetchEntityCollection(list, null);
            }

            return from entity in list select entity.IsNew ? null : entity.ToDTO();
        }

        public VersionDTO GetTenantMasterVersion(string id, VersionLoadInstructions loadInstructions)
        {
            var entity = new VersionEntity(id);

            using (var adapter = _fnTenantMasterAdapter())
            {
                adapter.FetchEntity(entity);
            }

            return entity.IsNew ? null : entity.ToDTO();

        }
    }
}
