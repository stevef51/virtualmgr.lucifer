﻿using ConceptCave.RepositoryInterfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using VirtualMgr.MultiTenant;

namespace ConceptCave.Repository.Injected
{
    public class OESSensorReadingRepository : IESSensorReadingRepository
    {
        private AppTenant _appTenant;
        public OESSensorReadingRepository(AppTenant appTenant)
        {
            _appTenant = appTenant;
        }
        public void WriteSensorReadingRecords(IEnumerable<ESSensorReadingRecord> records)
        {
            using (var cn = new SqlConnection(_appTenant.ConnectionString))
            {
                cn.Open();

                // Can't use LLBGLGen since it does not handle Table parameters correctly
                DataTable table = new DataTable();
                table.Columns.AddRange(new[]
                {
                    new DataColumn("SensorId", typeof(int)),
                    new DataColumn("SensorTimestampUtc", typeof(DateTime)),
                    new DataColumn("Value", typeof(double)),
                    new DataColumn("TabletUUID", typeof(string))
                });

                foreach (var r in records)
                {
                    table.Rows.Add(r.SensorId, r.SensorTimestampUtc, r.Value, r.TabletUUID);
                }

                SqlCommand cmd = new SqlCommand("es.WriteSensorReadingRecords", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@table", table).SqlDbType = SqlDbType.Structured;

                cmd.ExecuteNonQuery();
            }
        }
    }
}
