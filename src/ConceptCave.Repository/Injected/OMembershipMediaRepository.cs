﻿using System;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.RepositoryInterfaces.Repositories;

namespace ConceptCave.Repository.Injected
{
    public class OMembershipMediaRepository : IMembershipMediaRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public OMembershipMediaRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }
        public void AddMapping(Guid mediaId, Guid userId)
        {
            UserMediaEntity mapping = new UserMediaEntity(mediaId, userId);

            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(mapping);
            }
        }
    }
}
