﻿using System;
using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.RepositoryInterfaces.Repositories;

namespace ConceptCave.Repository.Injected
{
    public class OSequenceRepository : ISequenceRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public OSequenceRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }

        public int NextValue(string sequence)
        {
            int result = -1;

            using (var adapter = _fnAdapter())
            {
                int r = ActionProcedures.GetNextSequenceValue(sequence, ref result, adapter);
            }

            return result;
        }
    }
}
