﻿using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.RepositoryInterfaces.Repositories;
using System;
using System.Linq;
using ConceptCave.Data.DTOConverters;
using ConceptCave.Data.Linq;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Data;
using ConceptCave.SimpleExtensions;

namespace ConceptCave.Repository.Injected
{
    public class OErrorLogRepository : IErrorLogRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public OErrorLogRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }

        public DTO.DTOClasses.ErrorLogTypeDTO Save(DTO.DTOClasses.ErrorLogTypeDTO dto, bool refetch)
        {
            using (var adapter = _fnAdapter())
            {
                if (dto.StackTrace != null)
                    dto.StackTraceHash = dto.StackTrace.GetHashCode();
                var e = dto.ToEntity();
                adapter.SaveEntity(e, refetch, false);
                return refetch ? e.ToDTO() : dto;
            }
        }

        public DTO.DTOClasses.ErrorLogItemDTO Save(DTO.DTOClasses.ErrorLogItemDTO dto, bool refetch)
        {
            using (var adapter = _fnAdapter())
            {
                var e = dto.ToEntity();
                adapter.SaveEntity(e, refetch, false);
                return refetch ? e.ToDTO() : dto;
            }
        }

        public DTO.DTOClasses.ErrorLogTypeDTO Find(string type, string source, string message, string stackTrace)
        {
            var stackTraceHash = stackTrace.GetHashCode();
            using (var adapter = _fnAdapter())
            {
                LinqMetaData lmd = new LinqMetaData(adapter);
                return (from elt in lmd.ErrorLogType where
                        elt.Type == type &&
                        elt.Source == source && 
                        elt.Message == message && 
                        elt.StackTraceHash == stackTraceHash && 
                        elt.StackTrace == stackTrace 
                        select elt.ToDTO()).FirstOrDefault();
            }
        }

        public void LogError(DTO.DTOClasses.ErrorLogTypeDTO type, DTO.DTOClasses.ErrorLogItemDTO item)
        {
            var errorLogType = this.Find(type.Type, type.Source, type.Message, type.StackTrace);
            if (errorLogType == null)
            {
                var te = new ErrorLogTypeEntity();

                errorLogType = new ErrorLogTypeDTO()
                {
                    __IsNew = true,
                    Type = type.Type != null ? type.Type.Truncate(te.Fields[(int)ErrorLogTypeFieldIndex.Type].MaxLength) : null,
                    Source = type.Source != null ? type.Source.Truncate(te.Fields[(int)ErrorLogTypeFieldIndex.Source].MaxLength) : null,
                    Message = type.Message != null ? type.Message.Truncate(te.Fields[(int)ErrorLogTypeFieldIndex.Message].MaxLength) : null,
                    StackTrace = type.StackTrace != null ? type.StackTrace.Truncate(te.Fields[(int)ErrorLogTypeFieldIndex.StackTrace].MaxLength) : null
                };
                errorLogType = this.Save(errorLogType, true);
            }
            var ie = new ErrorLogItemEntity();
            var dto = new ErrorLogItemDTO()
            {
                __IsNew = true,
                ErrorLogTypeId = errorLogType.Id,
                UserId = item.UserId,
                UserAgentId = item.UserAgentId,
                DateCreated = DateTime.UtcNow,
                Data = item.Data != null ? item.Data.Truncate(ie.Fields[(int)ErrorLogItemFieldIndex.Data].MaxLength) : null
            };

            this.Save(dto, false);
        }
    }
}
