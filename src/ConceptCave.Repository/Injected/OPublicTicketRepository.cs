﻿using System;
using ConceptCave.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.Data;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Data.DTOConverters;
using ConceptCave.RepositoryInterfaces.Repositories;

namespace ConceptCave.Repository.Injected
{

    public class OPublicTicketRepository : IPublicTicketRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public OPublicTicketRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }

        public PublicTicketDTO GetById(Guid id, PublicTicketLoadInstructions instructions = PublicTicketLoadInstructions.None)
        {
            PublicTicketEntity result = new PublicTicketEntity(id);

            PrefetchPath2 path = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntity(result, path);
            }

            return result.IsNew ? null : result.ToDTO();
        }

        public void Save(PublicTicketDTO ticket, bool refetch, bool recurse)
        {
            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(ticket.ToEntity(), refetch, recurse);
            }
        }

        private static PrefetchPath2 BuildPrefetchPath(PublicTicketLoadInstructions instructions)
        {
            PrefetchPath2 path = new PrefetchPath2((int)EntityType.PublicTicketEntity);

            if ((instructions & PublicTicketLoadInstructions.WorkingDocumentTickets) == PublicTicketLoadInstructions.WorkingDocumentTickets)
            {
                path.Add(PublicTicketEntity.PrefetchPathPublicWorkingDocuments);
            }

            if ((instructions & PublicTicketLoadInstructions.PublishingGroupResourceTickets) == PublicTicketLoadInstructions.PublishingGroupResourceTickets)
            {
                path.Add(PublicTicketEntity.PrefetchPathPublicPublishingGroupResources);
            }

            if ((instructions & PublicTicketLoadInstructions.PasswordReset) == PublicTicketLoadInstructions.PasswordReset)
            {
                path.Add(PublicTicketEntity.PrefetchPathPasswordResetTicket);
            }

            return path;
        }
    }
}
