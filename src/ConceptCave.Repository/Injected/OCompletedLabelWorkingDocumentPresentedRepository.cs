﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.Data.DTOConverters;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.HelperClasses;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Repositories;

namespace ConceptCave.Repository.Injected
{
    public class OCompletedLabelWorkingDocumentPresentedRepository : ICompletedLabelWorkingDocumentPresentedRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public OCompletedLabelWorkingDocumentPresentedRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }
        public void Save(IEnumerable<CompletedLabelWorkingDocumentPresentedDimensionDTO> items, bool refetch, bool recurse)
        {
            var collection = new EntityCollection<CompletedLabelWorkingDocumentPresentedDimensionEntity>(items.Select(i => i.ToEntity()));
            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntityCollection(collection, refetch, recurse);
            }
        }
    }
}
