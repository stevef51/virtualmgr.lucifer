﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Data;
using ConceptCave.Data.DTOConverters;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Data.Linq;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using SD.LLBLGen.Pro.LinqSupportClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using VirtualMgr.Central;
using ConceptCave.Data.DatabaseSpecific;

namespace ConceptCave.Repository.Injected
{
    public class OGatewayCustomerRepository : IGatewayCustomerRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public OGatewayCustomerRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }

        private PrefetchPath2 BuildPrefetchPath(GatewayCustomerLoadInstructions instructions)
        {
            PrefetchPath2 path = new PrefetchPath2((int)EntityType.GatewayCustomerEntity);

            return path;
        }

        public GatewayCustomerDTO GetById(Guid userId, string gatewayName, GatewayCustomerLoadInstructions instructions = GatewayCustomerLoadInstructions.None)
        {
            var e = new GatewayCustomerEntity(gatewayName, userId);
            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntity(e, BuildPrefetchPath(instructions));
            }
            return e.IsNew ? null : e.ToDTO();
        }

        public GatewayCustomerDTO Save(GatewayCustomerDTO dto, bool refetch, bool recurse)
        {
            var e = dto.ToEntity();
            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(e, refetch, recurse);
            }
            return refetch ? e.ToDTO() : dto;
        }
    }
}
