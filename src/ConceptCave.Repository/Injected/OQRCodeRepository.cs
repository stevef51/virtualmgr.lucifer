﻿using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.DTOConverters;
using ConceptCave.RepositoryInterfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using SD.LLBLGen.Pro.ORMSupportClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.Data;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Data.DatabaseSpecific;

namespace ConceptCave.Repository.Injected
{
    public class OQRCodeRepository : IQRCodeRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;

        public OQRCodeRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }
        private PrefetchPath2 BuildPrefetchPath(QRCodeLoadInstructions instructions)
        {
            PrefetchPath2 path = new PrefetchPath2(EntityType.QrcodeEntity);
            if ((instructions & QRCodeLoadInstructions.User) != 0)
            {
                path.Add(QrcodeEntity.PrefetchPathUserData);
            }
            if ((instructions & QRCodeLoadInstructions.TaskType) != 0)
            {
                path.Add(QrcodeEntity.PrefetchPathProjectJobTaskType);
            }
            if ((instructions & QRCodeLoadInstructions.Asset) != 0)
            {
                path.Add(QrcodeEntity.PrefetchPathAsset);
            }
            return path;
        }

        public DTO.DTOClasses.QrcodeDTO GetById(int id, QRCodeLoadInstructions instructions = QRCodeLoadInstructions.None)
        {
            var e = new QrcodeEntity(id);
            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntity(e, BuildPrefetchPath(instructions));
            }
            return e.IsNew ? null : e.ToDTO();
        }

        public DTO.DTOClasses.QrcodeDTO Save(DTO.DTOClasses.QrcodeDTO dto, bool refetch, bool recurse)
        {
            var e = dto.__IsNew ? new QrcodeEntity() : new QrcodeEntity(dto.Id);

            using (var adapter = _fnAdapter())
            {
                if (!dto.__IsNew)
                {
                    adapter.FetchEntity(e);
                }

                dto.ToEntity(e);

                adapter.SaveEntity(e, refetch, recurse);
            }

            return refetch ? e.ToDTO() : dto;
        }

        public IList<DTO.DTOClasses.QrcodeDTO> Search(string qrCodeLike, int? pageNumber, int? itemsPerPage, bool includeArchived, ref int totalItems, int? searchFlags, int? includeFlags, bool? showSelected, QRCodeLoadInstructions instructions = QRCodeLoadInstructions.None)
        {
            var result = new EntityCollection<QrcodeEntity>();
            var expression = new PredicateExpression(QrcodeFields.Qrcode % qrCodeLike);

            if (searchFlags.HasValue && includeFlags.HasValue)
            {
                var filter = new PredicateExpression();
                if ((searchFlags.Value & (1 << (int)IQRCodeAttachmentType.UserSite)) != 0)
                {
                    if ((includeFlags.Value & (1 << (int)IQRCodeAttachmentType.UserSite)) != 0)
                    {
                        filter.Add(QrcodeFields.UserId != DBNull.Value);
                    }
                    else
                    {
                        filter.Add(QrcodeFields.UserId == DBNull.Value);
                    }
                }
                if ((searchFlags.Value & (1 << (int)IQRCodeAttachmentType.TaskType)) != 0)
                {
                    if ((includeFlags.Value & (1 << (int)IQRCodeAttachmentType.TaskType)) != 0)
                    {
                        filter.AddWithAnd(QrcodeFields.TaskTypeId != DBNull.Value);
                    }
                    else
                    {
                        filter.AddWithAnd(QrcodeFields.TaskTypeId == DBNull.Value);
                    }
                }
                if ((searchFlags.Value & (1 << (int)IQRCodeAttachmentType.Asset)) != 0)
                {
                    if ((includeFlags.Value & (1 << (int)IQRCodeAttachmentType.Asset)) != 0)
                    {
                        filter.AddWithAnd(QrcodeFields.AssetId != DBNull.Value);
                    }
                    else
                    {
                        filter.AddWithAnd(QrcodeFields.AssetId == DBNull.Value);
                    }
                }
                expression.AddWithAnd(filter);
            }
            if (showSelected.HasValue)
            {
                expression.AddWithAnd(QrcodeFields.Selected == showSelected.Value);
            }
            SortExpression sort = new SortExpression();

            sort.Add(QrcodeFields.Qrcode | SortOperator.Ascending);

            using (var adapter = _fnAdapter())
            {
                var predicate = new RelationPredicateBucket(expression);
                totalItems = adapter.GetDbCount(result, predicate);
                adapter.FetchEntityCollection(result, predicate, 0, sort, BuildPrefetchPath(instructions), pageNumber ?? 0, itemsPerPage ?? 0);
            }

            return (from r in result select r.ToDTO()).ToList();
        }


        public void Delete(int id, bool archiveIfRequired, out bool archived)
        {
            using (var adapter = _fnAdapter())
            {
                var entity = new QrcodeEntity(id);
                archived = false;
                adapter.DeleteEntity(entity);
            }
        }
    }
}
