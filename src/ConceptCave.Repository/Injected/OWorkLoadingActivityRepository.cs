﻿using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.HelperClasses;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.Data.DTOConverters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Data.Linq;
using SD.LLBLGen.Pro.LinqSupportClasses;
using ConceptCave.DTO.DTOClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.Data;
using VirtualMgr.Central;

namespace ConceptCave.Repository.Injected
{
    public class OWorkLoadingActivityRepository : IWorkLoadingActivityRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public OWorkLoadingActivityRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }
        public static PrefetchPath2 BuildPrefetchPath(WorkLoadingActivityLoadInstructions instructions)
        {
            PrefetchPath2 path = new PrefetchPath2((int)EntityType.WorkLoadingActivityEntity);

            return path;
        }

        public WorkLoadingActivityDTO GetById(Guid id, RepositoryInterfaces.Enums.WorkLoadingActivityLoadInstructions instructions)
        {
            WorkLoadingActivityEntity result = new WorkLoadingActivityEntity(id);

            var path = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntity(result, path);
            }

            return result.IsNew == true ? null : result.ToDTO();
        }

        public IList<WorkLoadingActivityDTO> GetAllActivities(RepositoryInterfaces.Enums.WorkLoadingActivityLoadInstructions instructions)
        {
            EntityCollection<WorkLoadingActivityEntity> result = new EntityCollection<WorkLoadingActivityEntity>();

            var path = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(), path);
            }

            return result.ToList().Select(r => r.ToDTO()).ToList();
        }

        public WorkLoadingActivityDTO Save(WorkLoadingActivityDTO dto, bool refetch, bool recurse)
        {
            var e = dto.ToEntity();

            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(e, refetch, recurse);
            }

            return refetch ? e.ToDTO() : dto;
        }


        public IList<WorkLoadingActivityDTO> Search(string nameLike, int? pageNumber, int? itemsPerPage, bool includeArchived, ref int totalItems, WorkLoadingActivityLoadInstructions instructions)
        {
            using (var adapter = _fnAdapter())
            {
                LinqMetaData lmd = new LinqMetaData(adapter);
                var items = from e in lmd.WorkLoadingActivity where e.Name.Contains(nameLike) select e.ToDTO();
                if (!includeArchived)
                    items = items.Where(i => i.Archived == false);
                items = items.OrderBy(i => i.Name);

                totalItems = items.Count();
                if (pageNumber.HasValue && itemsPerPage.HasValue)
                    items = items.Skip((pageNumber.Value - 1) * itemsPerPage.Value).Take(itemsPerPage.Value);
                return items.ToList();
            }
        }


        public void Delete(Guid id, bool archiveIfRequired, out bool archived)
        {
            using (var adapter = _fnAdapter())
            {
                var entity = new WorkLoadingActivityEntity(id);
                archived = false;
                try
                {
                    adapter.DeleteEntity(entity);
                }
                catch (Exception ex)
                {
                    if (archiveIfRequired)
                    {
                        adapter.FetchEntity(entity);
                        entity.Archived = true;
                        adapter.SaveEntity(entity, false);
                        archived = true;
                    }
                    else
                        throw ex;
                }
            }
        }
    }
}
