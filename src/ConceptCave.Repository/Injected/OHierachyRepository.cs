﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Data.Linq;
using ConceptCave.RepositoryInterfaces.Repositories;
using SD.LLBLGen.Pro.LinqSupportClasses;
using ConceptCave.Data.DTOConverters;
using SD.LLBLGen.Pro.ORMSupportClasses;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.Data;
using VirtualMgr.Central;
using System.Transactions;

namespace ConceptCave.Repository.Injected
{
    public class OHierarchyRepository : IHierarchyRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public OHierarchyRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }

        public static PrefetchPath2 BuildPrefetchPathForBuckets(HierarchyBucketLoadInstructions instructions, DateTime? date = null)
        {
            PrefetchPath2 path = new PrefetchPath2((int)EntityType.HierarchyBucketEntity);

            if ((instructions & HierarchyBucketLoadInstructions.BucketRole) == HierarchyBucketLoadInstructions.BucketRole)
            {
                path.Add(HierarchyBucketEntity.PrefetchPathHierarchyRole);
            }

            if ((instructions & HierarchyBucketLoadInstructions.User) == HierarchyBucketLoadInstructions.User)
            {
                path.Add(HierarchyBucketEntity.PrefetchPathUserData);
            }

            if ((instructions & HierarchyBucketLoadInstructions.BucketLabel) == HierarchyBucketLoadInstructions.BucketLabel)
            {
                var labelPath = path.Add(HierarchyBucketEntity.PrefetchPathHierarchyBucketLabels);

                if ((instructions & HierarchyBucketLoadInstructions.Labels) == HierarchyBucketLoadInstructions.Labels)
                {
                    labelPath.SubPath.Add(HierarchyBucketLabelEntity.PrefetchPathLabel);
                }
            }

            if ((instructions & HierarchyBucketLoadInstructions.Overrides) == HierarchyBucketLoadInstructions.Overrides)
            {
                var overridePath = path.Add(HierarchyBucketEntity.PrefetchPathHierarchyBucketOverrides);

                if ((instructions & HierarchyBucketLoadInstructions.OverridesUsers) == HierarchyBucketLoadInstructions.OverridesUsers)
                {
                    overridePath.SubPath.Add(HierarchyBucketOverrideEntity.PrefetchPathOriginalUser);
                    overridePath.SubPath.Add(HierarchyBucketOverrideEntity.PrefetchPathUserData);
                }

                if (date.HasValue == true)
                {
                    var d = date.Value.ToUniversalTime();
                    overridePath.Filter = new PredicateExpression(HierarchyBucketOverrideFields.StartDate <= d & HierarchyBucketOverrideFields.EndDate > d);
                    overridePath.Sorter = new SortExpression(HierarchyBucketOverrideFields.DateCreated | SortOperator.Ascending);
                }
            }
            if ((instructions & HierarchyBucketLoadInstructions.Hierarchy) == HierarchyBucketLoadInstructions.Hierarchy)
            {
                var hierarchyPath = path.Add(HierarchyBucketEntity.PrefetchPathHierarchy);
                if ((instructions & HierarchyBucketLoadInstructions.HierarchyRosters) == HierarchyBucketLoadInstructions.HierarchyRosters)
                {
                    hierarchyPath.SubPath.Add(HierarchyEntity.PrefetchPathRosters);
                }
            }
            return path;
        }
        public static PrefetchPath2 BuildPrefetchPath(HierarchyLoadInstructions instructions)
        {
            PrefetchPath2 path = new PrefetchPath2(EntityType.HierarchyEntity);

            if ((instructions & HierarchyLoadInstructions.Rosters) == HierarchyLoadInstructions.Rosters)
            {
                var hierarchyPath = path.Add(HierarchyEntity.PrefetchPathRosters);
            }
            return path;
        }

        public IList<DTO.DTOClasses.HierarchyDTO> GetAllHierarchies(HierarchyLoadInstructions instructions = HierarchyLoadInstructions.None)
        {
            EntityCollection<HierarchyEntity> result = new EntityCollection<HierarchyEntity>();

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(), BuildPrefetchPath(instructions));
            }

            return result.ToList().Select(e => e.ToDTO()).ToList();
        }

        public HierarchyDTO GetHierarchy(int id)
        {
            HierarchyEntity result = new HierarchyEntity(id);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntity(result);
            }

            return result.IsNew == true ? null : result.ToDTO();
        }

        public IList<HierarchyDTO> GetHierarchiesForUser(Guid userId)
        {
            using (var adapter = _fnAdapter())
            {
                var lmd = new LinqMetaData(adapter);
                return (from h in lmd.Hierarchy
                        join hb in lmd.HierarchyBucket on h.Id equals hb.HierarchyId
                        where hb.UserId.HasValue && hb.UserId.Value == userId
                        select h.ToDTO()).DistinctBy(h => h.Id).ToList();
            }
        }

        public HierarchyDTO GetHierarchy(string name)
        {
            PredicateExpression expression = new PredicateExpression(HierarchyFields.Name == name);

            EntityCollection<HierarchyEntity> result = new EntityCollection<HierarchyEntity>();

            SortExpression sort = new SortExpression(HierarchyFields.Id | SortOperator.Ascending);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), 1, sort);
            }

            return result.Count == 0 ? null : result[0].ToDTO();
        }

        public IList<HierarchyDTO> Search(string name, int page, int itemsPerPage, ref int totalItems)
        {
            PredicateExpression expression = new PredicateExpression(HierarchyFields.Name % name);

            EntityCollection<HierarchyEntity> result = new EntityCollection<HierarchyEntity>();

            SortExpression sort = new SortExpression(HierarchyFields.Id | SortOperator.Ascending);

            using (var adapter = _fnAdapter())
            {
                totalItems = adapter.GetDbCount(result, new RelationPredicateBucket(expression));
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), 0, sort, page, itemsPerPage);
            }

            return result.ToList().Select(h => h.ToDTO()).ToList();
        }

        public IList<HierarchyBucketDTO> GetBucketsForHierarchy(int hierarchyId, HierarchyBucketLoadInstructions instructions)
        {
            return GetBucketsForHierarchy(hierarchyId, null, instructions);
        }

        public IList<HierarchyBucketDTO> GetBucketsForHierarchy(int hierarchyId, int? roleId, HierarchyBucketLoadInstructions instructions)
        {
            PredicateExpression expression = new PredicateExpression(HierarchyBucketFields.HierarchyId == hierarchyId);

            if (roleId.HasValue)
            {
                expression.AddWithAnd(HierarchyBucketFields.RoleId == roleId.Value);
            }

            EntityCollection<HierarchyBucketEntity> result = new EntityCollection<HierarchyBucketEntity>();

            var path = BuildPrefetchPathForBuckets(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), path);
            }

            return result.ToList().Select(e => e.ToDTO()).ToList();
        }

        public IList<HierarchyBucketDTO> GetBucketsForUser(Guid userId, int? hierarchyId, bool? isPrimary, HierarchyBucketLoadInstructions instructions, DateTime? date = null)
        {
            PredicateExpression expression = new PredicateExpression(HierarchyBucketFields.UserId == userId);

            if (isPrimary.HasValue == true)
            {
                expression.AddWithAnd(HierarchyBucketFields.IsPrimary == isPrimary.Value);
            }

            if (hierarchyId.HasValue == true)
            {
                expression.AddWithAnd(HierarchyBucketFields.HierarchyId == hierarchyId);
            }

            EntityCollection<HierarchyBucketEntity> result = new EntityCollection<HierarchyBucketEntity>();

            var path = BuildPrefetchPathForBuckets(instructions, date);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), path);
            }

            return result.ToList().Select(e => e.ToDTO()).ToList();
        }

        public IList<HierarchyBucketDTO> GetBucketsForUser(Guid userId, bool? isPrimary, HierarchyBucketLoadInstructions instructions)
        {
            return GetBucketsForUser(userId, null, isPrimary, instructions);
        }

        public HierarchyBucketDTO GetById(int id)
        {
            HierarchyBucketEntity result = new HierarchyBucketEntity(id);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntity(result);
            }

            return result.IsNew == true ? null : result.ToDTO();
        }
        public HierarchyBucketDTO GetById(int id, HierarchyBucketLoadInstructions instructions, DateTime? date = null)
        {
            HierarchyBucketEntity result = new HierarchyBucketEntity(id);

            var path = BuildPrefetchPathForBuckets(instructions, date);

            bool loaded = false;
            using (var adapter = _fnAdapter())
            {
                loaded = adapter.FetchEntity(result, path);
            }

            return loaded ? result.ToDTO() : null;
        }

        public HierarchyBucketDTO Save(HierarchyBucketDTO bucket, bool refetch, bool recurse)
        {
            var e = bucket.ToEntity();

            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(e, refetch, recurse);
            }

            return refetch ? e.ToDTO() : bucket;
        }

        public HierarchyDTO Save(HierarchyDTO hierarchy, bool refetch, bool recurse)
        {
            var e = hierarchy.ToEntity();

            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(e, refetch, recurse);
            }

            return refetch ? e.ToDTO() : hierarchy;
        }

        public HierarchyRoleDTO Save(HierarchyRoleDTO role, bool refetch, bool recurse)
        {
            var e = role.ToEntity();

            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(e, refetch, recurse);
            }

            return refetch ? e.ToDTO() : role;
        }

        public IList<HierarchyBucketDTO> GetSubTreeBuckets(int hierarchyId, int left, int right, HierarchyBucketLoadInstructions instructions, DateTime? date = null, bool inclusiveOfLeftAndRight = false)
        {
            PredicateExpression expression = null;

            if (inclusiveOfLeftAndRight == false)
            {
                expression = new PredicateExpression(HierarchyBucketFields.HierarchyId == hierarchyId & HierarchyBucketFields.LeftIndex > left & HierarchyBucketFields.RightIndex < right);
            }
            else
            {
                expression = new PredicateExpression(HierarchyBucketFields.HierarchyId == hierarchyId & HierarchyBucketFields.LeftIndex >= left & HierarchyBucketFields.RightIndex <= right);
            }

            EntityCollection<HierarchyBucketEntity> result = new EntityCollection<HierarchyBucketEntity>();

            var path = BuildPrefetchPathForBuckets(instructions, date);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), path);
            }

            return result.ToList().Select(e => e.ToDTO()).ToList();
        }

        public IList<DTO.DTOClasses.HierarchyRoleDTO> GetRolesByName(string name)
        {
            using (var adapter = _fnAdapter())
            {
                var data = new LinqMetaData(adapter);
                var result = (from r in data.HierarchyRole
                              where r.Name == name
                              select r)
                             .WithPath(p => p.Prefetch(r => r.HierarchyRoleUserTypes));
                return ((ILLBLGenProQuery)result).Execute<EntityCollection<HierarchyRoleEntity>>().Select(e => e.ToDTO()).ToList();
            }
        }
        public IList<DTO.DTOClasses.HierarchyRoleDTO> GetRoles()
        {
            using (var adapter = _fnAdapter())
            {
                var collection = new EntityCollection<HierarchyRoleEntity>();
                adapter.FetchEntityCollection(collection, null);
                return collection.Select(e => e.ToDTO()).ToList();
            }
        }

        public HierarchyRoleDTO GetRoleByName(string name)
        {
            return this.GetRolesByName(name).FirstOrDefault();
        }

        public HierarchyRoleDTO GetRoleById(int id)
        {
            using (var adapter = _fnAdapter())
            {
                var ent = new HierarchyRoleEntity(id);
                var path = new PrefetchPath2(EntityType.HierarchyRoleEntity);
                path.Add(HierarchyRoleEntity.PrefetchPathHierarchyRoleUserTypes);
                adapter.FetchEntity(ent, path);
                return ent.IsNew ? null : ent.ToDTO();
            }
        }

        public DTO.DTOClasses.HierarchyBucketDTO GetPrimaryEntity(Guid userId)
        {
            using (var adapter = _fnAdapter())
            {
                var prefetch = new PrefetchPath2(EntityType.HierarchyBucketEntity);
                prefetch.Add(HierarchyBucketEntity.PrefetchPathParent)
                    .SubPath.Add(HierarchyBucketEntity.PrefetchPathHierarchyRole);
                prefetch.Add(HierarchyBucketEntity.PrefetchPathHierarchyRole);

                var data = new LinqMetaData(adapter);
                var entity = (from h in data.HierarchyBucket
                              where h.UserId.HasValue && h.UserId == userId && h.IsPrimary.Value
                              select h)
                                .WithPath(prefetch)
                                .SingleOrDefault();
                return entity.ToDTO();
            }
        }

        public HierarchyBucketDTO GetRootBucket(int hierarchyId)
        {
            PredicateExpression expression = new PredicateExpression(HierarchyBucketFields.HierarchyId == hierarchyId & HierarchyBucketFields.ParentId == DBNull.Value);

            EntityCollection<HierarchyBucketEntity> result = new EntityCollection<HierarchyBucketEntity>();

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), 1);
            }

            return result.Count == 0 ? null : result[0].ToDTO();
        }

        public IList<DTO.DTOClasses.HierarchyBucketDTO> GetAllDirectDescendantEntities(int parentBucketId)
        {
            using (var adapter = _fnAdapter())
            {
                var data = new LinqMetaData(adapter);
                var allDescendants = from h in data.HierarchyBucket
                                     where h.ParentId == parentBucketId
                                     select h;
                return ((ILLBLGenProQuery)allDescendants).Execute<EntityCollection<HierarchyBucketEntity>>().Select(i => i.ToDTO()).ToList();
            }
        }

        public DTO.DTOClasses.HierarchyBucketDTO GetPrimaryParentEntityPunchThrough(Guid userId)
        {
            using (var adapter = _fnAdapter())
            {
                var data = new LinqMetaData(adapter);

                //Get this guy's primary entity
                var primary = GetPrimaryEntity(userId);
                //now climb the tree until parent is a person
                var parent = primary.Parent;
                try
                {
                    var prefetch = new PrefetchPath2(EntityType.HierarchyBucketEntity);
                    prefetch.Add(HierarchyBucketEntity.PrefetchPathHierarchyRole);

                    while (parent != null && !(parent.HierarchyRole.IsPerson && parent.UserId.HasValue))
                    {
                        //climb the tree
                        var parent1 = parent;
                        parent = (from h in data.HierarchyBucket
                                  where h.Id == parent1.ParentId
                                  select h).WithPath(prefetch).Single().ToDTO();
                    }
                    return parent;
                }
                catch (InvalidOperationException)
                {
                    //This means Single() has thrown;
                    //We reached the top of the heirarchy. This user has no parent to report to
                    return null;
                }
            }
        }

        public DTO.DTOClasses.HierarchyBucketDTO LoadInMemory(int id)
        {
            //Load every record from the bucket table in memory
            //then load up the relations manually
            var allBuckets = new EntityCollection<HierarchyBucketEntity>();
            using (var adapter = _fnAdapter())
            {
                //But we do need to do a bit of prefetching accross tables
                var prefetch = new PrefetchPath2(EntityType.HierarchyBucketEntity);
                prefetch.Add(HierarchyBucketEntity.PrefetchPathHierarchyRole);
                prefetch.Add(HierarchyBucketEntity.PrefetchPathUserData);

                adapter.FetchEntityCollection(allBuckets, new RelationPredicateBucket(new PredicateExpression(HierarchyBucketFields.HierarchyId == id)), prefetch);
            }
            HierarchyBucketEntity root = null;
            var entDict = new Dictionary<int, HierarchyBucketEntity>();
            foreach (var bucket in allBuckets)
            {
                entDict.Add(bucket.Id, bucket);
            }
            foreach (var bucket in allBuckets)
            {
                if (bucket.ParentId == null)
                {
                    root = bucket;
                }
                else
                {
                    bucket.Parent = entDict[bucket.ParentId.Value];
                    //The following line appears to be unnescessary, and causes bucket to be added ot its parents children twice
                    //llblgen automagically manages bucket.parent filling the appropriate children in the opposite direction
                    //bucket.Parent.Children.Add(bucket);
                }

            }
            return root.ToDTO();
        }

        public void SetTreeNumbering(DTO.DTOClasses.HierarchyBucketDTO root)
        {
            //This method traverses around the edges of the tree specified by root and numbers the left and right indexes
            int count = 1;
            Action<DTO.DTOClasses.HierarchyBucketDTO> recurser = null;
            recurser = (node) =>
            {
                //number left first
                node.LeftIndex = count++;
                //number children
                if (node.Children != null)
                {
                    //assumption: foreach traverses in the right order
                    foreach (var child in node.Children)
                        recurser(child);
                }
                //then number right
                node.RightIndex = count++;
            };
            recurser(root);
        }

        private IList<HierarchyBucketEntity> GetBucketsForHierarchy(int hierarchyId, bool loadOverrides)
        {
            PredicateExpression expression = new PredicateExpression(HierarchyBucketFields.HierarchyId == hierarchyId);

            PrefetchPath2 path = new PrefetchPath2((int)EntityType.HierarchyBucketEntity);
            if (loadOverrides == true)
            {
                path.Add(HierarchyBucketEntity.PrefetchPathHierarchyBucketOverrides);
            }

            EntityCollection<HierarchyBucketEntity> result = new EntityCollection<HierarchyBucketEntity>();

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), path);
            }

            return result.ToList();
        }

        public void DumpAndSave(DTO.DTOClasses.HierarchyBucketDTO rootDto)
        {
            var newRoot = rootDto.ToEntity();
            var existing = GetBucketsForHierarchy(newRoot.HierarchyId, true);

            using (var scope = new TransactionScope())
            {
                using (var adapter = _fnAdapter())
                {
                    //now recursively save the root
                    List<int> ids = new List<int>();
                    Action<HierarchyBucketEntity> buildIds = null;
                    buildIds = (ent) =>
                    {
                        if (ent.IsNew == false)
                        {
                            ids.Add(ent.Id);
                        }

                        foreach (var c in ent.Children)
                            buildIds(c);
                    };

                    buildIds(newRoot);

                    var entsToSave = new EntityCollection<HierarchyBucketEntity>();
                    var overridesToDelete = new EntityCollection<HierarchyBucketOverrideEntity>();

                    var deleted = (from e in existing where ids.Contains(e.Id) == false orderby e.LeftIndex descending select e);

                    if (deleted.Count() > 0)
                    {
                        overridesToDelete.AddRange(from d in deleted from o in d.HierarchyBucketOverrides select o);
                    }

                    Action<HierarchyBucketEntity> addEntityToList = null;
                    addEntityToList = (ent) =>
                    {
                        entsToSave.Add(ent);

                        foreach (var c in ent.Children)
                            addEntityToList(c);
                    };
                    addEntityToList(newRoot);

                    adapter.SaveEntityCollection(entsToSave);

                    adapter.DeleteEntityCollection(overridesToDelete);

                    // we need to null out any tasks that are tied to any of the buckets being deleted
                    PredicateExpression toUpdateExpression = new PredicateExpression(ProjectJobTaskFields.HierarchyBucketId == (from d in deleted select d.Id).ToArray());
                    EntityCollection<ProjectJobTaskEntity> toUpdate = new EntityCollection<ProjectJobTaskEntity>();

                    adapter.FetchEntityCollection(toUpdate, new RelationPredicateBucket(toUpdateExpression));
                    foreach (var task in toUpdate)
                    {
                        task.HierarchyBucketId = null;
                    }
                    adapter.SaveEntityCollection(toUpdate);

                    foreach (var d in deleted)
                    {
                        PredicateExpression expression = new PredicateExpression(HierarchyBucketFields.Id == d.Id);
                        adapter.DeleteEntitiesDirectly("HierarchyBucketEntity", new RelationPredicateBucket(expression));
                    }
                }

                scope.Complete();
            }
        }

        public HierarchyBucketOverrideDTO Save(HierarchyBucketOverrideDTO hierarchy, bool refetch, bool recurse)
        {
            var e = hierarchy.ToEntity();

            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(e, refetch, recurse);
            }

            return refetch ? e.ToDTO() : hierarchy;
        }

        public IList<HierarchyBucketOverrideDTO> GetBucketOverrides(DateTime date, Guid userId, int hierarchyBucketId)
        {
            PredicateExpression expression = new PredicateExpression(HierarchyBucketOverrideFields.StartDate <= date & HierarchyBucketOverrideFields.EndDate >= date & HierarchyBucketOverrideFields.OriginalUserId == userId & HierarchyBucketOverrideFields.HierarchyBucketId == hierarchyBucketId);
            EntityCollection<HierarchyBucketOverrideEntity> result = new EntityCollection<HierarchyBucketOverrideEntity>();

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression));
            }

            return result.ToList().Select(e => e.ToDTO()).ToList();
        }

        public IList<HierarchyBucketOverrideDTO> GetBucketOverrides(DateTime startDate, DateTime endDate, Guid userId, int hierarchyBucketId)
        {
            /* there are a few options
             *                       startDate ----------------------- endDate
             *           sd ---- ed                                                           no overlap
             *                           sd ---------- ed                                     partial overlap (ed >= startDate && ed <= endDate)
             *                                    sd ---------- ed                            complete overlap (modelled by above and below anyway)
             *                                                   sd --------------- ed        partial overlap (sd >= startDate && sd <= endDate)
             *                                                                  sd ------ ed  no overlap
            */

            PredicateExpression expression = new PredicateExpression(HierarchyBucketOverrideFields.OriginalUserId == userId & HierarchyBucketOverrideFields.HierarchyBucketId == hierarchyBucketId);
            PredicateExpression e1 = new PredicateExpression();
            PredicateExpression e2 = new PredicateExpression(HierarchyBucketOverrideFields.StartDate >= startDate & HierarchyBucketOverrideFields.StartDate <= endDate);
            PredicateExpression e1e2 = new PredicateExpression();
            e1e2.Add(e1);
            e1e2.AddWithOr(e2);

            expression.AddWithAnd(e1e2);

            EntityCollection<HierarchyBucketOverrideEntity> result = new EntityCollection<HierarchyBucketOverrideEntity>();

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression));
            }

            return result.ToList().Select(e => e.ToDTO()).ToList();
        }

        public HierarchyBucketOverrideDTO GetBucketOverride(int id)
        {
            HierarchyBucketOverrideEntity result = new HierarchyBucketOverrideEntity(id);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntity(result);
            }

            return result.ToDTO();
        }

        public void DeleteHierarchyBucketOverride(int id)
        {
            PredicateExpression expression = new PredicateExpression(HierarchyBucketOverrideFields.Id == id);

            using (var adapter = _fnAdapter())
            {
                adapter.DeleteEntitiesDirectly(typeof(HierarchyBucketOverrideEntity), new RelationPredicateBucket(expression));
            }
        }

        public void AddBucketLabels(HierarchyBucketLabelDTO[] labels)
        {
            EntityCollection<HierarchyBucketLabelEntity> result = new EntityCollection<HierarchyBucketLabelEntity>();

            labels.ToList().ForEach(l => result.Add(l.ToEntity()));

            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntityCollection(result);
            }
        }
        public void RemoveBucketLabels(HierarchyBucketLabelDTO[] labels)
        {
            EntityCollection<HierarchyBucketLabelEntity> result = new EntityCollection<HierarchyBucketLabelEntity>();

            labels.ToList().ForEach(l => result.Add(l.ToEntity()));

            using (var adapter = _fnAdapter())
            {
                adapter.DeleteEntityCollection(result);
            }
        }

        public IEnumerable<HierarchyBucketDTO> GetBucketsWithLabel(Guid labelId, HierarchyBucketLoadInstructions loadInstructions)
        {
            var prefetch = BuildPrefetchPathForBuckets(loadInstructions);
            using (var adapter = _fnAdapter())
            {
                var lmd = new LinqMetaData(adapter);
                var r = (from b in lmd.HierarchyBucket join bl in lmd.HierarchyBucketLabel on b.Id equals bl.HierarchyBucketId where bl.LabelId == labelId select b)
                    .WithPath(prefetch)
                    .Distinct()
                    .ToList();

                return r.Select(b => b.ToDTO()).ToList();
            }
        }

        public IList<MergedTeamHierarchyWithLabelDTO> GetMergedHierarchyBuckets(int bucketId, bool includePrimaryRecord)
        {
            EntityCollection<MergedTeamHierarchyWithLabelEntity> result = new EntityCollection<MergedTeamHierarchyWithLabelEntity>();
            PredicateExpression expression = new PredicateExpression(MergedTeamHierarchyWithLabelFields.Id == bucketId);

            if (includePrimaryRecord == false)
            {
                expression.AddWithAnd(MergedTeamHierarchyWithLabelFields.PrimaryRecord == 0);
            }

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression));
            }

            return result.ToList().Select(e => e.ToDTO()).ToList();
        }

        public HierarchyBucketDTO GetParentDeep(Guid userId, int depth, int hierarchyId)
        {
            using (var adapter = _fnAdapter())
            {
                var lmd = new LinqMetaData(adapter);

                // Grab the entire hierarchy in memory rather than searching for each parent 
                var entireHierarchy = (from hb in lmd.HierarchyBucket where hb.HierarchyId == hierarchyId select hb).ToList();

                // Find the users bucket first
                var userBucket = (from hb in entireHierarchy where hb.UserId.HasValue && hb.UserId == userId select hb).FirstOrDefault();
                if (userBucket == null || !userBucket.ParentId.HasValue)
                {
                    return null;
                }

                // Walk up parents until depth is met or we reach root
                var parentBucket = (from hb in entireHierarchy where hb.Id == userBucket.ParentId.Value select hb).FirstOrDefault(); ;
                depth--;

                while (parentBucket != null && parentBucket.ParentId.HasValue && depth > 0)
                {
                    parentBucket = (from hb in entireHierarchy where hb.Id == parentBucket.ParentId.Value select hb).FirstOrDefault();
                    depth--;
                }
                if (depth != 0)
                {
                    return null;
                }
                return parentBucket.ToDTO();
            }
        }
    }
}
