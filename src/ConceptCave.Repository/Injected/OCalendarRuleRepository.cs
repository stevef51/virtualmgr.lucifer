﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.Data.DTOConverters;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.Data.EntityClasses;
using VirtualMgr.Central;

namespace ConceptCave.Repository.Injected
{
    public class OCalendarRuleRepository : ICalendarRuleRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public OCalendarRuleRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }

        public DTO.DTOClasses.CalendarRuleDTO Save(CalendarRuleDTO rule, bool refetch, bool recurse)
        {
            if (rule.StartDate.Kind != DateTimeKind.Utc || (rule.EndDate.HasValue == true && rule.EndDate.Value.Kind != DateTimeKind.Utc))
            {
                throw new ArgumentException("Start and End dates myst be specified in UTC");
            }

            var e = rule.ToEntity();

            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(e, refetch, recurse);
            }

            return refetch ? e.ToDTO() : rule;
        }

        public CalendarRuleDTO New(DateTime startDate, DateTime? endDate, CalendarRuleFrequency frequency, bool sunday, bool monday, bool tuesday, bool wednesday, bool thursday, bool friday, bool saturday)
        {
            if (startDate.Kind != DateTimeKind.Utc || (endDate.HasValue && endDate.Value.Kind != DateTimeKind.Utc))
            {
                throw new ArgumentException("Start and End dates must be specified in UTC");
            }

            CalendarRuleEntity rule = new CalendarRuleEntity();
            rule.StartDate = startDate;
            rule.EndDate = endDate;
            rule.Frequency = frequency.ToString();

            if (frequency == CalendarRuleFrequency.Daily)
            {
                rule.ExecuteSunday = sunday;
                rule.ExecuteMonday = monday;
                rule.ExecuteTuesday = tuesday;
                rule.ExecuteWednesday = wednesday;
                rule.ExecuteThursday = thursday;
                rule.ExecuteFriday = friday;
                rule.ExecuteSaturday = saturday;
            }

            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(rule, true);
            }

            return rule.ToDTO();
        }
    }
}
