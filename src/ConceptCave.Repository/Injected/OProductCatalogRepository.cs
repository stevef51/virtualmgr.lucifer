﻿using ConceptCave.RepositoryInterfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.DTO.DTOClasses;
using VirtualMgr.Central;
using ConceptCave.Data.Linq;
using ConceptCave.Data.DTOConverters;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.DatabaseSpecific;

namespace ConceptCave.Repository.Injected
{
    public class OProductCatalogRepository : IProductCatalogRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;

        protected readonly IFacilityStructureRepository _facRepo;

        public OProductCatalogRepository(IFacilityStructureRepository facRepo, Func<DataAccessAdapter> fnAdapter)
        {
            _facRepo = facRepo;
            _fnAdapter = fnAdapter;
        }
        public ProductCatalogDTO GetById(int id)
        {
            using (var adapter = _fnAdapter())
            {
                var data = new LinqMetaData(adapter);
                return (from prd in data.ProductCatalog
                        where prd.Id == id
                        select prd).FirstOrDefault().ToDTO();
            }
        }

        public ProductCatalogDTO Save(ProductCatalogDTO dto, bool refetch)
        {
            using (var adapter = _fnAdapter())
            {
                var e = dto.ToEntity();
                adapter.SaveEntity(e, refetch, false);
                return refetch ? e.ToDTO() : dto;
            }
        }

        public IList<ProductCatalogDTO> GetFacilityStructureProductCatalogs(int facilityStructureId)
        {
            var fs = _facRepo.GetById(facilityStructureId, FacilityStructureLoadInstructions.ProductCatalogs |
                FacilityStructureLoadInstructions.ProductCatalogItems |
                FacilityStructureLoadInstructions.ProductCatalogItemProducts | FacilityStructureLoadInstructions.ProductCatalogItemProductTypes);

            return fs != null ? fs.ProductCatalogs : null;
        }

        public void GetCatalogsForFacilityStructure(int id, IList<ProductCatalogDTO> catalogs, IList<ProductCatalogFacilityStructureRuleDTO> rules)
        {
            GetCatalogsForFacilityStructure(_facRepo.GetById(id, FacilityStructureLoadInstructions.ProductCatalogs |
                FacilityStructureLoadInstructions.ProductCatalogItems |
                FacilityStructureLoadInstructions.ProductCatalogItemProducts |
                FacilityStructureLoadInstructions.ProductCatalogItemProductHierarchies |
                FacilityStructureLoadInstructions.ProductCatalogItemProductTypes), catalogs, rules);
        }

        public void GetCatalogsForFacilityStructure(FacilityStructureDTO structure, IList<ProductCatalogDTO> catalogs, IList<ProductCatalogFacilityStructureRuleDTO> rules)
        {
            GetCatalogsForFacilityStructure(structure, catalogs, rules, 0);
        }

        /// <summary>
        /// This function merges the catalogs working up the facility structure tree. Catalogs from above are merged in only if their name is
        /// different to anything below it.
        /// </summary>
        /// <param name="structure">Current structure to merge catalogs in from</param>
        public void GetCatalogsForFacilityStructure(FacilityStructureDTO structure, IList<ProductCatalogDTO> catalogs, IList<ProductCatalogFacilityStructureRuleDTO> rules, int level = 0)
        {
            if (structure == null)
            {
                return;
            }

            if (structure.ProductCatalogs.Count > 0)
            {
                foreach (var c in structure.ProductCatalogs)
                {
                    if ((from a in catalogs where a.Excluded == false && a.Name == c.Name select a).Count() == 0)
                    {
                        catalogs.Add(c);
                    }
                }
            }

            // we don't have to worry about recursing up as we are doing that anyway
            GetCatalogStructureRules(structure.Id, rules, false);

            if (structure.ParentId.HasValue == false)
            {
                // now we set the excluded flag on any catalogs that need to be excluded, this just means callers of us
                // can work out exclusion/inclusion of a catalog through a simple property rather than having to look in the rules collection
                // themselves.
                foreach (var c in catalogs)
                {
                    var rs = (from r in rules where r.ProductCatalogId == c.Id select r);
                    if (rs.Count() == 0)
                    {
                        // no rules for the catalog, so we make it included as that's the default
                        c.Excluded = false;
                        continue;
                    }

                    // there is a rule, what does it say?
                    c.Excluded = rs.First().Exclude;
                }

                return;
            }

            // work our way up
            GetCatalogsForFacilityStructure(structure.ParentId.Value, catalogs, rules);

            // we can only do this calculation at the bottom, after the recurse up has been done as we need to have the full picture to work out
            // if a rule has been inherited from above.
            if (level == 0)
            {
                foreach (var c in catalogs)
                {
                    // it's inherited if there isn't a rule in the list of rules for the catalog at the structure (so in otherwords it's not inherited if there is a rule defined on the current structure)
                    var rs = (from r in rules where r.ProductCatalogId == c.Id && r.FacilityStructureId == structure.Id select r);
                    if (rs.Count() == 0)
                    {
                        c.InheritedRule = ProductCatalogFacilityStructureRuleType.Inherited.ToString();
                        continue;
                    }

                    // there is a rule defined on the structure, so what does the rule say?
                    c.InheritedRule = rs.First().Exclude == true ? ProductCatalogFacilityStructureRuleType.Excluded.ToString() : ProductCatalogFacilityStructureRuleType.Included.ToString();
                }
            }
        }

        public void GetCatalogsForSite(Guid id, IList<ProductCatalogDTO> catalogs, IList<ProductCatalogFacilityStructureRuleDTO> rules)
        {
            FacilityStructureDTO dto = _facRepo.FindBySiteId(id, FacilityStructureLoadInstructions.ProductCatalogs |
                FacilityStructureLoadInstructions.ProductCatalogItems |
                FacilityStructureLoadInstructions.ProductCatalogItemProducts | FacilityStructureLoadInstructions.ProductCatalogItemProductTypes);

            if (dto == null)
            {
                return;
            }

            GetCatalogsForFacilityStructure(dto, catalogs, rules);
        }

        public void GetCatalogStructureRules(int facilityStructureId, IList<ProductCatalogFacilityStructureRuleDTO> rules, bool recurseUp)
        {
            using (var adapter = _fnAdapter())
            {
                var data = new LinqMetaData(adapter);

                var rs = (from c in data.ProductCatalogFacilityStructureRule
                          where c.FacilityStructureId == facilityStructureId
                          select c).Select(c => c.ToDTO());

                // we only add something into the rules if there isn't already a rule there for that catalog. Since we work up the hierarchy this
                // means rule's lower down in the hierarchy get prescedence over higher up ones.
                (from r in rs where (from p in rules select p.ProductCatalogId).Contains(r.ProductCatalogId) == false select r).ToList().ForEach(r => rules.Add(r));
            }

            if (recurseUp == true)
            {
                var fac = _facRepo.GetById(facilityStructureId);

                if (fac.ParentId.HasValue == false)
                {
                    return;
                }

                GetCatalogStructureRules(fac.ParentId.Value, rules, true);
            }
        }

        public void Delete(int id, bool archiveIfRequired, out bool archived)
        {
            using (var adapter = _fnAdapter())
            {
                var entity = new ProductCatalogEntity(id);
                archived = false;
                try
                {
                    adapter.DeleteEntity(entity);
                }
                catch (Exception ex)
                {
                    if (archiveIfRequired)
                    {
                        adapter.FetchEntity(entity);
                        entity.Archived = true;
                        adapter.SaveEntity(entity, false);
                        archived = true;
                    }
                    else
                        throw ex;
                }
            }
        }

        public ProductCatalogFacilityStructureRuleDTO GetCatalogStructureRule(int facilityStructureId, int catalogid)
        {
            using (var adapter = _fnAdapter())
            {
                var data = new LinqMetaData(adapter);

                var rs = (from c in data.ProductCatalogFacilityStructureRule
                          where c.FacilityStructureId == facilityStructureId && c.ProductCatalogId == catalogid
                          select c).Select(c => c.ToDTO());

                return rs.Count() > 0 ? rs.First() : null;
            }
        }

        public ProductCatalogFacilityStructureRuleDTO Save(ProductCatalogFacilityStructureRuleDTO rule, bool refetch)
        {
            using (var adapter = _fnAdapter())
            {
                var e = rule.ToEntity();
                adapter.SaveEntity(e, refetch, false);
                return refetch ? e.ToDTO() : rule;
            }
        }

        public void Delete(int facilitystructureId, int catalogid)
        {
            using (var adapter = _fnAdapter())
            {
                var entity = new ProductCatalogFacilityStructureRuleEntity(facilitystructureId, catalogid);

                adapter.DeleteEntity(entity);
            }
        }

    }
}
