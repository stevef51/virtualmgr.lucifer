﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Checklist;
using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.Data.DTOConverters;
using ConceptCave.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using ConceptCave.Data;
using ConceptCave.Data.HelperClasses;
using System.Transactions;
using VirtualMgr.Central;

namespace ConceptCave.Repository.Injected
{
    public class ProjectJobScheduledTaskExlcusionHelper : ConceptCave.Scheduling.IScheduledTask
    {

        public ProjectJobScheduledTaskEntity Entity { get; protected set; }
        public Guid Id
        {
            get { return Entity.Id; }
        }

        public Guid? OwnerId
        {
            get { return null; }
        }

        public Guid? SiteId
        {
            get { return Entity.SiteId; }
        }

        public Guid TaskTypeId
        {
            get { return Entity.TaskTypeId; }
        }

        public int CalendarRuleId
        {
            get { return Entity.CalendarRuleId; }
        }

        public ConceptCave.Scheduling.ICalendarRule CalendarRule { get; protected set; }

        public int RosterId
        {
            get { return Entity.RosterId; }
        }

        public int Level
        {
            get { return Entity.Level; }
        }

        public int? RoleId
        {
            get { return Entity.RoleId; }
        }

        public bool IsArchived
        {
            get { return Entity.IsArchived; }
        }

        public string LengthInDays
        {
            get { return Entity.LengthInDays; }
        }

        public IList<ConceptCave.Scheduling.ITask> Tasks { get; protected set; }

        public ProjectJobScheduledTaskExlcusionHelper(ProjectJobScheduledTaskEntity entity)
        {
            Entity = entity;
            CalendarRule = new CalendarRuleExclusionHelper(entity.CalendarRule);

            Tasks = new List<ConceptCave.Scheduling.ITask>();

            foreach (var task in entity.ProjectJobTasks)
            {
                Tasks.Add(new TaskExclusionHelper(task));
            }
        }
    }

    public class TaskExclusionHelper : ConceptCave.Scheduling.ITask
    {
        public TaskExclusionHelper(ProjectJobTaskEntity entity)
        {
            Entity = entity;
        }

        public ProjectJobTaskEntity Entity { get; protected set; }
        public Guid Id
        {
            get { return Entity.Id; }
        }

        public Guid? ScheduledTaskId
        {
            get { return Entity.ProjectJobScheduledTaskId; }
        }

        public int RosterId
        {
            get { return Entity.RosterId; }
        }

        public Guid TaskTypeId
        {
            get { return Entity.TaskTypeId; }
        }

        public DateTime DateCreated
        {
            get { return Entity.DateCreated; }
        }

        public DateTime? DateCompleted
        {
            get { return Entity.DateCompleted; }
        }

        public Guid OwnerId
        {
            get { return Entity.OwnerId; }
        }

        public Guid OriginalOwnerId
        {
            get { return Entity.OriginalOwnerId; }
        }

        public Guid? SiteId
        {
            get { return Entity.SiteId; }
        }

        public int Status
        {
            get { return Entity.Status; }
        }
    }

    public class CalendarRuleExclusionHelper : ConceptCave.Scheduling.ICalendarRule
    {
        public CalendarRuleExclusionHelper(CalendarRuleEntity entity)
        {
            Entity = entity;
        }

        public CalendarRuleEntity Entity { get; protected set; }
        public int Id
        {
            get { return Entity.Id; }
        }

        public DateTime DateCreated
        {
            get { return Entity.DateCreated; }
        }

        public DateTime StartDate
        {
            get { return Entity.StartDate; }
        }

        public DateTime? EndDate
        {
            get { return Entity.EndDate; }
        }

        public string Frequency
        {
            get { return Entity.Frequency; }
        }

        public bool ExecuteSunday
        {
            get { return Entity.ExecuteSunday; }
        }

        public bool ExecuteMonday
        {
            get { return Entity.ExecuteMonday; }
        }

        public bool ExecuteTuesday
        {
            get { return Entity.ExecuteTuesday; }
        }

        public bool ExecuteWednesday
        {
            get { return Entity.ExecuteWednesday; }
        }

        public bool ExecuteThursday
        {
            get { return Entity.ExecuteThursday; }
        }

        public bool ExecuteFriday
        {
            get { return Entity.ExecuteFriday; }
        }

        public bool ExecuteSaturday
        {
            get { return Entity.ExecuteSaturday; }
        }

        public string CRONExpression
        {
            get { return Entity.Cronexpression; }
        }

        public int Period
        {
            get { return Entity.Period; }
        }
    }

    public class OScheduledTaskRepository : CalendarRuleRepositoryBase, IScheduledTaskRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public OScheduledTaskRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }
        public static PrefetchPath2 BuildPrefetchPath(ProjectJobScheduledTaskLoadInstructions instructions)
        {
            PrefetchPath2 path = new PrefetchPath2((int)EntityType.ProjectJobScheduledTaskEntity);


            if ((instructions & ProjectJobScheduledTaskLoadInstructions.Attachments) == ProjectJobScheduledTaskLoadInstructions.Attachments)
            {
                path.Add(ProjectJobScheduledTaskEntity.PrefetchPathProjectJobScheduledTaskAttachments).SubPath.Add(ProjectJobScheduledTaskAttachmentEntity.PrefetchPathMedium);
            }

            if ((instructions & ProjectJobScheduledTaskLoadInstructions.PublishedChecklists) == ProjectJobScheduledTaskLoadInstructions.PublishedChecklists)
            {
                path.Add(ProjectJobScheduledTaskEntity.PrefetchPathProjectJobScheduledTaskPublishingGroupResources).SubPath.Add(ProjectJobScheduledTaskPublishingGroupResourceEntity.PrefetchPathPublishingGroupResource);
            }

            if ((instructions & ProjectJobScheduledTaskLoadInstructions.UserLabels) == ProjectJobScheduledTaskLoadInstructions.UserLabels)
            {
                path.Add(ProjectJobScheduledTaskEntity.PrefetchPathProjectJobScheduledJobTaskUserLabels).SubPath.Add(ProjectJobScheduledJobTaskUserLabelEntity.PrefetchPathLabel);
            }

            if ((instructions & ProjectJobScheduledTaskLoadInstructions.CalendarRule) == ProjectJobScheduledTaskLoadInstructions.CalendarRule)
            {
                path.Add(ProjectJobScheduledTaskEntity.PrefetchPathCalendarRule);
            }

            if ((instructions & ProjectJobScheduledTaskLoadInstructions.ScheduleLabels) == ProjectJobScheduledTaskLoadInstructions.ScheduleLabels)
            {
                var labelPath = path.Add(ProjectJobScheduledTaskEntity.PrefetchPathProjectJobScheduledTaskLabels);

                if ((instructions & ProjectJobScheduledTaskLoadInstructions.Labels) == ProjectJobScheduledTaskLoadInstructions.Labels)
                {
                    labelPath.SubPath.Add(ProjectJobScheduledTaskLabelEntity.PrefetchPathLabel);
                }
            }

            if ((instructions & ProjectJobScheduledTaskLoadInstructions.TaskType) == ProjectJobScheduledTaskLoadInstructions.TaskType)
            {
                path.Add(ProjectJobScheduledTaskEntity.PrefetchPathProjectJobTaskType);
            }

            if ((instructions & ProjectJobScheduledTaskLoadInstructions.Roster) == ProjectJobScheduledTaskLoadInstructions.Roster)
            {
                path.Add(ProjectJobScheduledTaskEntity.PrefetchPathRoster);
            }

            if ((instructions & ProjectJobScheduledTaskLoadInstructions.HierarchyRole) == ProjectJobScheduledTaskLoadInstructions.HierarchyRole)
            {
                path.Add(ProjectJobScheduledTaskEntity.PrefetchPathHierarchyRole);
            }

            if ((instructions & ProjectJobScheduledTaskLoadInstructions.Users) == ProjectJobScheduledTaskLoadInstructions.Users)
            {
                var userPath = path.Add(ProjectJobScheduledTaskEntity.PrefetchPathUserData_);

                if ((instructions & ProjectJobScheduledTaskLoadInstructions.UsersWorkloadingFeatures) == ProjectJobScheduledTaskLoadInstructions.UsersWorkloadingFeatures)
                {
                    var featuresPath = userPath.SubPath.Add(UserDataEntity.PrefetchPathSiteFeatures);

                    if ((instructions & ProjectJobScheduledTaskLoadInstructions.UsersWorkloadingFeaturesAreas) == ProjectJobScheduledTaskLoadInstructions.UsersWorkloadingFeaturesAreas)
                    {
                        featuresPath.SubPath.Add(SiteFeatureEntity.PrefetchPathSiteArea);
                    }

                    if ((instructions & ProjectJobScheduledTaskLoadInstructions.UsersWorkloadingFeaturesUnits) == ProjectJobScheduledTaskLoadInstructions.UsersWorkloadingFeaturesUnits)
                    {
                        featuresPath.SubPath.Add(SiteFeatureEntity.PrefetchPathMeasurementUnit);
                    }
                }
            }

            if ((instructions & ProjectJobScheduledTaskLoadInstructions.WorkloadingActivities) == ProjectJobScheduledTaskLoadInstructions.WorkloadingActivities)
            {
                var activityPath = path.Add(ProjectJobScheduledTaskEntity.PrefetchPathProjectJobScheduledTaskWorkloadingActivities);

                if ((instructions & ProjectJobScheduledTaskLoadInstructions.WorkloadingActivitiesSiteFeatures) == ProjectJobScheduledTaskLoadInstructions.WorkloadingActivitiesSiteFeatures)
                {
                    activityPath.SubPath.Add(ProjectJobScheduledTaskWorkloadingActivityEntity.PrefetchPathSiteFeature).SubPath.Add(SiteFeatureEntity.PrefetchPathSiteArea);
                }

                if ((instructions & ProjectJobScheduledTaskLoadInstructions.WorkloadingActivitiesStandards) == ProjectJobScheduledTaskLoadInstructions.WorkloadingActivitiesStandards)
                {
                    var standardPath = activityPath.SubPath.Add(ProjectJobScheduledTaskWorkloadingActivityEntity.PrefetchPathWorkLoadingStandard);

                    standardPath.SubPath.Add(WorkLoadingStandardEntity.PrefetchPathWorkLoadingBook);
                    standardPath.SubPath.Add(WorkLoadingStandardEntity.PrefetchPathWorkLoadingFeature);
                    standardPath.SubPath.Add(WorkLoadingStandardEntity.PrefetchPathWorkLoadingActivity);
                }
            }

            if ((instructions & ProjectJobScheduledTaskLoadInstructions.AuditSampling) == ProjectJobScheduledTaskLoadInstructions.AuditSampling)
            {
                path.Add(ProjectJobScheduledTaskEntity.PrefetchPathAuditTaskConditionActions);
            }

            var translatedPath = path.Add(ProjectJobScheduledTaskEntity.PrefetchPathTranslated);
            if ((instructions & ProjectJobScheduledTaskLoadInstructions.AllTranslated) == 0)
                translatedPath.Filter = new PredicateExpression(ProjectJobScheduledTaskTranslatedFields.CultureName == System.Threading.Thread.CurrentThread.CurrentUICulture.Name);

            return path;
        }

        private ProjectJobScheduledTaskDTO Translate(ProjectJobScheduledTaskDTO dto, ProjectJobScheduledTaskLoadInstructions instructions)
        {
            // If using current culture then copy the translation across to primary record field
            if ((instructions & ProjectJobScheduledTaskLoadInstructions.AllTranslated) == ProjectJobScheduledTaskLoadInstructions.AllTranslated)
            {
                if (dto.Translated.Any())
                {
                    var first = dto.Translated.First();
                    dto.Name = string.IsNullOrEmpty(first.Name) == true ? dto.Name : first.Name;
                    dto.Description = string.IsNullOrEmpty(first.Description) == true ? dto.Description : first.Description;
                }
            }

            return dto;
        }

        public ProjectJobScheduledTaskDTO GetById(Guid id, ProjectJobScheduledTaskLoadInstructions instructions)
        {
            return GetById(new Guid[] { id }, instructions).DefaultIfEmpty(null).First();
        }

        public List<ProjectJobScheduledTaskDTO> GetById(Guid[] id, ProjectJobScheduledTaskLoadInstructions instructions)
        {
            PredicateExpression expression = new PredicateExpression(ProjectJobScheduledTaskFields.Id == id);

            EntityCollection<ProjectJobScheduledTaskEntity> result = new EntityCollection<ProjectJobScheduledTaskEntity>();

            PrefetchPath2 path = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), path);
            }

            return result.ToList().Select(e => Translate(e.ToDTO(), instructions)).ToList();
        }

        public ProjectJobScheduledTaskDTO Save(ProjectJobScheduledTaskDTO task, bool refetch, bool recurse)
        {
            var e = task.ToEntity();

            // bug fix for LLBLGEN being resistant to nulling out things in the db
            if (task.SiteId.HasValue == false)
            {
                e.SiteId = Guid.Empty;
                e.SiteId = null;
            }

            if (task.EstimatedDurationSeconds.HasValue == false)
            {
                e.EstimatedDurationSeconds = DateTime.Now.Ticks;
                e.EstimatedDurationSeconds = null;
            }

            if (!task.Backcolor.HasValue)
            {
                task.Backcolor = 0;
                task.Backcolor = null;
            }
            if (!task.Forecolor.HasValue)
            {
                task.Forecolor = 0;
                task.Forecolor = null;
            }
            if (!task.RoleId.HasValue)
            {
                task.RoleId = 0;
                task.RoleId = null;
            }
            if (!task.StartTime.HasValue)
            {
                e.StartTime = TimeSpan.Zero;
                e.StartTime = null;
            }
            if (!task.StartTimeWindowSeconds.HasValue)
            {
                e.StartTimeWindowSeconds = 0;
                e.StartTimeWindowSeconds = null;
            }

            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(e, refetch, recurse);
            }

            return refetch ? e.ToDTO() : task;
        }

        public ProjectJobScheduledTaskDTO New(string name, Guid jobId, int calendarRuleId)
        {
            ProjectJobScheduledTaskEntity task = new ProjectJobScheduledTaskEntity();

            task.Id = CombFactory.NewComb();
            task.CalendarRuleId = calendarRuleId;
            task.Name = name;

            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(task, true);
            }

            return task.ToDTO();
        }

        public void Delete(Guid id)
        {
            using (var adapter = _fnAdapter())
            {
                var task = new ProjectJobScheduledTaskEntity(id);

                adapter.FetchEntity(task);

                task.IsArchived = true;

                adapter.SaveEntity(task);
            }
        }

        public void DeleteScheduledTaskPublishedChecklists(Guid TaskId, int[] publishedGroupResourceIds)
        {
            PredicateExpression expresion = new PredicateExpression(ProjectJobScheduledTaskPublishingGroupResourceFields.ProjectJobScheduledTaskId == TaskId);
            expresion.AddWithAnd(ProjectJobScheduledTaskPublishingGroupResourceFields.PublishingGroupResourceId == publishedGroupResourceIds);

            using (var adapter = _fnAdapter())
            {
                adapter.DeleteEntitiesDirectly("ProjectJobScheduledTaskPublishingGroupResourceEntity", new RelationPredicateBucket(expresion));
            }
        }

        public void DeleteScheduledTaskUserLabels(Guid ScheduledTaskId, Guid[] labelIds)
        {
            PredicateExpression expresion = new PredicateExpression(ProjectJobScheduledJobTaskUserLabelFields.ProjectJobScheduledTaskId == ScheduledTaskId);
            expresion.AddWithAnd(ProjectJobScheduledJobTaskUserLabelFields.LabelId == labelIds);

            using (var adapter = _fnAdapter())
            {
                adapter.DeleteEntitiesDirectly("ProjectJobScheduledJobTaskUserLabelEntity", new RelationPredicateBucket(expresion));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="date">Date in local time, so that the day is preserved</param>
        /// <param name="onlyTopLevel"></param>
        /// <param name="removeSchedulesWithLiveTasks"></param>
        /// <param name="instructions"></param>
        /// <returns></returns>
        public IList<ProjectJobScheduledTaskDTO> GetForUser(Guid userId, int hierarchyRoleId, int rosterId, DateTime date, TimeZoneInfo timezone, bool onlyTopLevel, bool removeSchedulesWithLiveTasks, ProjectJobScheduledTaskLoadInstructions instructions)
        {
            EntityCollection<ProjectJobScheduledTaskEntity> result = new EntityCollection<ProjectJobScheduledTaskEntity>();

            var enforcer = new ConceptCave.Scheduling.ExclusionRuleEnforcer();

            enforcer.Rules.Add(new ConceptCave.Scheduling.ExclusionRules.ExpiredOrNotStartedExclusionRule());
            enforcer.Rules.Add(new ConceptCave.Scheduling.ExclusionRules.DailyScheduleExclusionRule());
            enforcer.Rules.Add(new ConceptCave.Scheduling.ExclusionRules.CronScheduleExclusionRule());

            if (onlyTopLevel == true)
            {
                enforcer.Rules.Add(new ConceptCave.Scheduling.ExclusionRules.TopLevelOnlyExclusionRule());
            }

            // we select everything for the roster and role, then let the rules decide what goes back up
            PredicateExpression select = new PredicateExpression(ProjectJobScheduledTaskFields.RosterId == rosterId &
                ProjectJobScheduledTaskFields.RoleId == hierarchyRoleId &
                ProjectJobScheduledTaskFields.IsArchived == false);

            var path = new PrefetchPath2((int)EntityType.ProjectJobScheduledTaskEntity);
            path.Add(ProjectJobScheduledTaskEntity.PrefetchPathCalendarRule);
            var subPath = path.Add(ProjectJobScheduledTaskEntity.PrefetchPathProjectJobTasks);
            // we are going with an approximate grab of tasks for the current user 
            // (accounting for timezones etc to block out none relevant tasks will occurr later).
            // we'll also pull in anything who's ends on hasn't passed as CRON based tasks can be in play
            // for quite sometime
            subPath.Filter = new PredicateExpression(ProjectJobTaskFields.OriginalOwnerId == userId &
                (ProjectJobTaskFields.DateCreated >= date.Date.AddDays(-2) | ProjectJobTaskFields.EndsOn >= date.Date)
            );

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(select), path);
            }

            List<ConceptCave.Scheduling.IScheduledTask> helpers = new List<ConceptCave.Scheduling.IScheduledTask>();
            foreach (var r in result)
            {
                helpers.Add(new ProjectJobScheduledTaskExlcusionHelper(r));
            }

            enforcer.Enforce(helpers, date, timezone, removeSchedulesWithLiveTasks);

            // we now refetch only those in the list so that we can obey the load instructions side of things
            PredicateExpression expression = new PredicateExpression(ProjectJobScheduledTaskFields.Id == (from r in helpers select r.Id).ToArray());

            path = BuildPrefetchPath(instructions);

            result = new EntityCollection<ProjectJobScheduledTaskEntity>();

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), path);
            }

            return result.ToList().Select(e => Translate(e.ToDTO(), instructions)).ToList();
        }

        public IList<ProjectJobScheduledTaskDTO> GetForUser(Guid userId, ProjectJobScheduledTaskLoadInstructions instructions)
        {
            throw new NotImplementedException();
            //// we want something like select * from tasks where jobid in(select id from jobs where userid == userid and projectid in (select id from tblproject where projectid == projectid)))
            //PredicateExpression expression = new PredicateExpression(new FieldCompareSetPredicate(ProjectJobScheduledTaskFields., null, ProjectJobFields.Id, null, SetOperator.In, jobExpression));

            //PrefetchPath2 path = BuildPrefetchPath(instructions);

            //EntityCollection<ProjectJobScheduledTaskEntity> result = new EntityCollection<ProjectJobScheduledTaskEntity>();

            //using (var adapter = _fnAdapter())
            //{
            //    adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), path);
            //}

            //return result.ToList().Select(e => e.ToDTO()).ToList();
        }

        public IList<ProjectJobScheduledTaskDTO> GetForRoster(int rosterId, int? roleId, ProjectJobScheduledTaskLoadInstructions instructions)
        {
            EntityCollection<ProjectJobScheduledTaskEntity> result = new EntityCollection<ProjectJobScheduledTaskEntity>();

            PredicateExpression expression = new PredicateExpression(ProjectJobScheduledTaskFields.RosterId == rosterId & ProjectJobScheduledTaskFields.IsArchived == false);

            if (roleId.HasValue == true)
            {
                expression.AddWithAnd(ProjectJobScheduledTaskFields.RoleId == roleId.Value);
            }

            PrefetchPath2 path = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), path);
            }

            return result.ToList().Select(e => Translate(e.ToDTO(), instructions)).ToList();
        }

        public IList<ProjectJobScheduledTaskDTO> GetForRoster(int rosterId, int[] roleId, ProjectJobScheduledTaskLoadInstructions instructions)
        {
            EntityCollection<ProjectJobScheduledTaskEntity> result = new EntityCollection<ProjectJobScheduledTaskEntity>();

            PredicateExpression expression = new PredicateExpression(ProjectJobScheduledTaskFields.RosterId == rosterId & ProjectJobScheduledTaskFields.IsArchived == false);

            expression.AddWithAnd(ProjectJobScheduledTaskFields.RoleId == roleId);

            PrefetchPath2 path = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), path);
            }

            var r = result.ToList().Select(e => Translate(e.ToDTO(), instructions)).ToList();

            return r;
        }

        public ProjectJobScheduledTaskAttachmentDTO AddAttachment(Guid taskId, Guid mediaId)
        {
            var result = new ProjectJobScheduledTaskAttachmentEntity()
            {
                ProjectJobScheduledTaskId = taskId,
                MediaId = mediaId
            };

            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(result);
            }

            return result.ToDTO();
        }

        public void RemoveAttachment(Guid taskId, Guid mediaId)
        {
            PredicateExpression expression = new PredicateExpression(ProjectJobScheduledTaskAttachmentFields.ProjectJobScheduledTaskId == taskId & ProjectJobScheduledTaskAttachmentFields.MediaId == mediaId);

            using (var adapter = _fnAdapter())
            {
                adapter.DeleteEntitiesDirectly("ProjectJobScheduledTaskAttachmentEntity", new RelationPredicateBucket(expression));
            }
        }

        public void RemoveLabels(Guid taskId)
        {
            PredicateExpression expression = new PredicateExpression(ProjectJobScheduledTaskLabelFields.ProjectJobScheduledTaskId == taskId);

            using (var adapter = _fnAdapter())
            {
                adapter.DeleteEntitiesDirectly("ProjectJobScheduledTaskLabelEntity", new RelationPredicateBucket(expression));
            }
        }

        public void RemoveWorkloadingActivities(Guid taskId)
        {
            PredicateExpression expression = new PredicateExpression(ProjectJobScheduledTaskWorkloadingActivityFields.ProjectJobScheduledTaskId == taskId);

            using (var adapter = _fnAdapter())
            {
                adapter.DeleteEntitiesDirectly("ProjectJobScheduledTaskWorkloadingActivityEntity", new RelationPredicateBucket(expression));
            }
        }

        public IList<ProjectJobScheduleDayDTO> GetDaySchedule(int rosterId, int? roleId, int? day)
        {
            PredicateExpression expression = new PredicateExpression(ProjectJobScheduleDayFields.RosterId == rosterId);

            if (roleId.HasValue)
            {
                expression.AddWithAnd(ProjectJobScheduleDayFields.RoleId == roleId.Value);
            }

            if (day.HasValue)
            {
                expression.AddWithAnd(ProjectJobScheduleDayFields.Day == day.Value);
            }

            var result = new EntityCollection<ProjectJobScheduleDayEntity>();

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression));
            }

            return result.ToList().Select(e => e.ToDTO()).ToList();
        }

        public ProjectJobScheduleDayDTO Save(ProjectJobScheduleDayDTO dto, bool refetch, bool recurse)
        {
            var e = dto.ToEntity();

            // we are only interested in the time side of things, so let's set the date to something known
            e.StartTime = new DateTime(2000, 1, 1, e.StartTime.Hour, e.StartTime.Minute, 0);
            e.EndTime = new DateTime(2000, 1, 1, e.EndTime.Hour, e.EndTime.Minute, 0);
            e.LunchStart = new DateTime(2000, 1, 1, e.LunchStart.Hour, e.LunchStart.Minute, 0);
            e.LunchEnd = new DateTime(2000, 1, 1, e.LunchEnd.Hour, e.LunchEnd.Minute, 0);

            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(e, refetch, recurse);
            }

            return refetch ? e.ToDTO() : dto;
        }
    }
}
