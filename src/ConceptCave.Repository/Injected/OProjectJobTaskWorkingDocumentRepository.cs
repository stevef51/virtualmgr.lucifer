﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.HelperClasses;
using ConceptCave.RepositoryInterfaces.Repositories;
using SD.LLBLGen.Pro.ORMSupportClasses;
using ConceptCave.Data.DTOConverters;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.Data;
using ConceptCave.DTO.DTOClasses;
using VirtualMgr.Central;

namespace ConceptCave.Repository.Injected
{
    public class OProjectJobTaskWorkingDocumentRepository : IProjectJobTaskWorkingDocumentRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;

        public OProjectJobTaskWorkingDocumentRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }

        public static PrefetchPath2 BuildPrefetchPath(ProjectJobTaskWorkingDocumentLoadInstructions instructions)
        {
            PrefetchPath2 path = new PrefetchPath2((int)EntityType.ProjectJobTaskWorkingDocumentEntity);

            if ((instructions & ProjectJobTaskWorkingDocumentLoadInstructions.Task) == ProjectJobTaskWorkingDocumentLoadInstructions.Task)
            {
                path.Add(ProjectJobTaskWorkingDocumentEntity.PrefetchPathProjectJobTask);
            }

            if ((instructions & ProjectJobTaskWorkingDocumentLoadInstructions.WorkingDocument) == ProjectJobTaskWorkingDocumentLoadInstructions.WorkingDocument)
            {
                path.Add(ProjectJobTaskWorkingDocumentEntity.PrefetchPathWorkingDocument);
            }

            if ((instructions & ProjectJobTaskWorkingDocumentLoadInstructions.PublishingGroupResource) == ProjectJobTaskWorkingDocumentLoadInstructions.PublishingGroupResource)
            {
                path.Add(ProjectJobTaskWorkingDocumentEntity.PrefetchPathPublishingGroupResource);
            }

            return path;
        }


        public IList<DTO.DTOClasses.ProjectJobTaskWorkingDocumentDTO> GetForTaskAndPublishingGroupResource(Guid taskId, int publishingGroupResourceId, ProjectJobTaskWorkingDocumentLoadInstructions instructions)
        {
            EntityCollection<ProjectJobTaskWorkingDocumentEntity> result = new EntityCollection<ProjectJobTaskWorkingDocumentEntity>();

            PredicateExpression expression = new PredicateExpression(ProjectJobTaskWorkingDocumentFields.ProjectJobTaskId == taskId & ProjectJobTaskWorkingDocumentFields.PublishingGroupResourceId == publishingGroupResourceId);

            var prefetch = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), prefetch);
            }

            return result.ToList().Select(e => e.ToDTO()).ToList();
        }

        public DTO.DTOClasses.ProjectJobTaskWorkingDocumentDTO GetForWorkingDocument(Guid workingDocumentId, ProjectJobTaskWorkingDocumentLoadInstructions instructions)
        {
            EntityCollection<ProjectJobTaskWorkingDocumentEntity> result = new EntityCollection<ProjectJobTaskWorkingDocumentEntity>();

            PredicateExpression expression = new PredicateExpression(ProjectJobTaskWorkingDocumentFields.WorkingDocumentId == workingDocumentId);

            var prefetch = BuildPrefetchPath(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), prefetch);
            }

            if (result.Count() == 0)
            {
                return null;
            }

            return result.First().ToDTO();

        }

        public ProjectJobTaskWorkingDocumentDTO Save(ProjectJobTaskWorkingDocumentDTO dto, bool refetch, bool recurse)
        {
            var e = dto.ToEntity();

            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(e, refetch, recurse);
            }

            return refetch ? e.ToDTO() : dto;
        }
    }
}
