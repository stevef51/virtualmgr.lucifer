﻿using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.HelperClasses;
using ConceptCave.RepositoryInterfaces.Repositories;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualMgr.Central;
using ConceptCave.Data.DTOConverters;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.Data;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Data.DatabaseSpecific;

namespace ConceptCave.Repository.Injected
{
    public class OTimesheetItemRepository : ITimesheetItemRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public OTimesheetItemRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }
        public static PrefetchPath2 BuildPrefetchPath(TimesheetItemLoadInstructions instructions)
        {
            PrefetchPath2 path = new PrefetchPath2((int)EntityType.TimesheetItemEntity);

            if ((instructions & TimesheetItemLoadInstructions.Roster) == TimesheetItemLoadInstructions.Roster)
            {
                path.Add(TimesheetItemEntity.PrefetchPathRoster);
            }

            if ((instructions & TimesheetItemLoadInstructions.Users) == TimesheetItemLoadInstructions.Users)
            {
                path.Add(TimesheetItemEntity.PrefetchPathUserData);
                path.Add(TimesheetItemEntity.PrefetchPathUserData_);
            }

            if ((instructions & TimesheetItemLoadInstructions.Worklogs) == TimesheetItemLoadInstructions.Worklogs)
            {
                path.Add(TimesheetItemEntity.PrefetchPathTimesheetedTaskWorkLogs);
            }

            return path;
        }

        public DTO.DTOClasses.TimesheetItemDTO GetById(int id)
        {
            TimesheetItemEntity result = new TimesheetItemEntity(id);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntity(result);
            }

            return result.IsNew == true ? null : result.ToDTO();
        }

        public IList<TimesheetItemDTO> GetForRosterBetweenDates(int rosterId, Guid? userId, DateTime startDate, DateTime endDate, TimesheetItemLoadInstructions instructions)
        {
            PredicateExpression expression = new PredicateExpression(TimesheetItemFields.Date >= startDate & TimesheetItemFields.Date <= endDate);
            expression.AddWithAnd(TimesheetItemFields.RosterId == rosterId);

            if (userId.HasValue)
            {
                expression.AddWithAnd(TimesheetItemFields.UserId == userId.Value);
            }

            var path = BuildPrefetchPath(instructions);

            EntityCollection<TimesheetItemEntity> result = new EntityCollection<TimesheetItemEntity>();

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), 0, null, path);
            }

            return result.ToList().Select(r => r.ToDTO()).ToList();
        }

        public IList<TimesheetItemDTO> GetForRosterForExport(int rosterId, TimesheetItemLoadInstructions instructions)
        {
            PredicateExpression expression = new PredicateExpression(TimesheetItemFields.RosterId == rosterId & TimesheetItemFields.ExportedDate == DBNull.Value);

            EntityCollection<TimesheetItemEntity> result = new EntityCollection<TimesheetItemEntity>();
            var path = BuildPrefetchPath(instructions);
            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), 0, null, path);
            }

            return result.ToList().Select(r => r.ToDTO()).ToList();
        }

        public DTO.DTOClasses.TimesheetItemDTO Save(DTO.DTOClasses.TimesheetItemDTO dto, bool refetch, bool recurse)
        {
            var e = dto.ToEntity();

            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(e, refetch, recurse);
            }

            return refetch ? e.ToDTO() : dto;
        }
    }
}
