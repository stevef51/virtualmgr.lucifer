﻿using ConceptCave.Data;
using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.HelperClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Data.DTOConverters;
using ConceptCave.DTO.DTOClasses;
using VirtualMgr.Central;
using ConceptCave.Data.Linq;
using SD.LLBLGen.Pro.LinqSupportClasses;

namespace ConceptCave.Repository.Injected
{
    public class OReportManagementRepository : IReportManagementRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;

        public OReportManagementRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }
        protected PrefetchPath2 BuildPrefetch(ReportLoadInstructions instructions)
        {
            PrefetchPath2 path = new PrefetchPath2((int)EntityType.ReportEntity);
            if ((instructions & ReportLoadInstructions.Data) == ReportLoadInstructions.Data)
            {
                path.Add(ReportEntity.PrefetchPathReportData);
            }

            if ((instructions & ReportLoadInstructions.Roles) == ReportLoadInstructions.Roles)
            {
                path.Add(ReportEntity.PrefetchPathReportRoles).SubPath.Add(ReportRoleEntity.PrefetchPathAspNetRole);
            }

            return path;
        }

        public IList<DTO.DTOClasses.ReportDTO> GetAll(int? reportType = null, ReportLoadInstructions instructions = ReportLoadInstructions.None)
        {
            var path = BuildPrefetch(instructions);

            using (var adapter = _fnAdapter())
            {
                var lmd = new LinqMetaData(adapter);
                var query = (from r in lmd.Report orderby r.Name select r).WithPath(path);
                if (reportType.HasValue)
                {
                    query = query.Where(r => r.Type == reportType.Value);
                }
                return query.ToList().Select(e => e.ToDTO()).ToList();
            }
        }

        public ReportDTO GetById(int id, ReportLoadInstructions instructions)
        {
            ReportEntity result = new ReportEntity(id);

            PrefetchPath2 path = BuildPrefetch(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntity(result, path);
            }

            return result.IsNew == true ? null : result.ToDTO();
        }

        public IList<ReportDTO> GetByName(string name, ReportLoadInstructions instructions)
        {
            EntityCollection<ReportEntity> result = new EntityCollection<ReportEntity>();

            PredicateExpression expression = new PredicateExpression(ReportFields.Name == name);
            PrefetchPath2 path = BuildPrefetch(instructions);

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), 0, null, path);
            }

            return (from r in result select r.ToDTO()).ToList();
        }

        public IList<ReportDTO> GetReports(string[] roles, ReportLoadInstructions instructions)
        {
            // we want select * from reports where id in (select reportid from reportroles where roleid in (select roleid from roles where name in roles)))

            PredicateExpression rolesExpression = new PredicateExpression(AspNetRoleFields.Name == roles);
            PredicateExpression reportRolesExpression = new PredicateExpression(new FieldCompareSetPredicate(ReportRoleFields.RoleId, null, AspNetRoleFields.Id, null, SetOperator.In, rolesExpression));
            PredicateExpression expression = new PredicateExpression(new FieldCompareSetPredicate(ReportFields.Id, null, ReportRoleFields.ReportId, null, SetOperator.In, reportRolesExpression));

            var path = BuildPrefetch(instructions);

            EntityCollection<ReportEntity> result = new EntityCollection<ReportEntity>();

            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, new RelationPredicateBucket(expression), path);
            }

            return result.ToList().Select(e => e.ToDTO()).ToList();
        }

        public DTO.DTOClasses.ReportDTO Save(DTO.DTOClasses.ReportDTO report, bool refetch, bool recurse)
        {
            var e = report.ToEntity();

            using (var adapter = _fnAdapter())
            {
                adapter.SaveEntity(e, refetch, recurse);
            }

            return refetch ? e.ToDTO() : report;
        }

        public void Remove(int reportId)
        {
            PredicateExpression expression = new PredicateExpression(ReportFields.Id == reportId);

            using (var adapter = _fnAdapter())
            {
                adapter.DeleteEntitiesDirectly("ReportEntity", new RelationPredicateBucket(expression));
            }
        }

        public void RemoveReportFromRoles(int id, string[] roleIds)
        {
            PredicateExpression expression = new PredicateExpression(ReportRoleFields.ReportId == id & ReportRoleFields.RoleId == roleIds);

            using (var adapter = _fnAdapter())
            {
                adapter.DeleteEntitiesDirectly("ReportRoleEntity", new RelationPredicateBucket(expression));
            }
        }

        public ReportDataDTO GetReportByData(Byte[] data)
        {
            EntityCollection<ReportDataEntity> result = new EntityCollection<ReportDataEntity>();

            IRelationPredicateBucket bucket = new RelationPredicateBucket();
            bucket.PredicateExpression.Add(new FieldCompareValuePredicate(ReportDataFields.Data, null, ComparisonOperator.Equal, data));
            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntityCollection(result, bucket);
            }

            return (from r in result select r.ToDTO()).FirstOrDefault();
        }
    }
}
