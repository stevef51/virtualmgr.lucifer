﻿using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.DTOConverters;
using ConceptCave.RepositoryInterfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualMgr.Central;
using SD.LLBLGen.Pro.ORMSupportClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.Data;
using ConceptCave.Data.Linq;
using ConceptCave.Data.HelperClasses;
using ConceptCave.DTO.DTOClasses;
using SD.LLBLGen.Pro.LinqSupportClasses;
using ConceptCave.Data.DatabaseSpecific;

namespace ConceptCave.Repository.Injected
{
    public class OAssetScheduleRepository : IAssetScheduleRepository
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;
        public OAssetScheduleRepository(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }
        private PrefetchPath2 BuildPrefetchPath(AssetScheduleLoadInstructions instructions)
        {
            PrefetchPath2 path = new PrefetchPath2(EntityType.AssetScheduleEntity);

            if ((instructions & AssetScheduleLoadInstructions.Calendar) == AssetScheduleLoadInstructions.Calendar)
            {
                path.Add(AssetScheduleEntity.PrefetchPathAssetTypeCalendar);
            }
            if ((instructions & AssetScheduleLoadInstructions.Asset) == AssetScheduleLoadInstructions.Asset)
            {
                path.Add(AssetScheduleEntity.PrefetchPathAsset);
            }

            return path;
        }

        public DTO.DTOClasses.AssetScheduleDTO GetById(int id, AssetScheduleLoadInstructions instructions = AssetScheduleLoadInstructions.None)
        {
            var e = new AssetScheduleEntity(id);
            using (var adapter = _fnAdapter())
            {
                adapter.FetchEntity(e, BuildPrefetchPath(instructions));
            }
            return e.IsNew ? null : e.ToDTO();
        }

        public DTO.DTOClasses.AssetScheduleDTO Save(DTO.DTOClasses.AssetScheduleDTO dto, bool refetch, bool recurse)
        {
            var e = dto.__IsNew ? new AssetScheduleEntity() : new AssetScheduleEntity(dto.Id);

            using (var adapter = _fnAdapter())
            {
                if (!dto.__IsNew)
                {
                    adapter.FetchEntity(e);
                }

                dto.ToEntity(e);

                adapter.SaveEntity(e, refetch, recurse);
            }

            return refetch ? e.ToDTO() : dto;
        }

        public IList<AssetScheduleDTO> FindExpired(DateTime utcNow, AssetScheduleLoadInstructions instructions = AssetScheduleLoadInstructions.None)
        {
            var path = BuildPrefetchPath(instructions);
            using (var adapter = _fnAdapter())
            {
                var lmd = new LinqMetaData(adapter);
                var items = from s in lmd.AssetSchedule where s.Asset.Archived == false && utcNow >= s.NextRunUtc select s;
                var list = items.WithPath(path).ToList();
                return list.Select(i => i.ToDTO()).ToList();
            }
        }
    }
}
