﻿using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.Data.Linq;
using ConceptCave.RepositoryInterfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.DTOConverters;
using ConceptCave.Data.HelperClasses;
using System.Data.SqlClient;
using System.Data;
using ConceptCave.RepositoryInterfaces;
using VirtualMgr.MultiTenant;
using VirtualMgr.Common;

namespace ConceptCave.Repository.Injected
{
    public class ODWTenantPopulate : IDWPopulate
    {
        private readonly string _trackingTable;
        private readonly string _populateSP;
        protected readonly Func<DataAccessAdapter> _fnAdapter;
        protected readonly SqlTenantMaster _appTenantMaster;

        public ODWTenantPopulate(Func<DataAccessAdapter> fnAdapter, SqlTenantMaster appTenantMaster, string trackingTable)
        {
            _fnAdapter = fnAdapter;
            _appTenantMaster = appTenantMaster;
            _trackingTable = trackingTable;
            _populateSP = "dw.Populate_" + _trackingTable;
        }

        public virtual int Populate(int tenantId, IContinuousProgressCategory progress)
        {
            long? beforeTracking = null;
            int affectedCount = 0;
            ExecutePopulateCommand(tenantId, out affectedCount, out beforeTracking, progress);
            return affectedCount;
        }

        protected virtual void ExecutePopulateCommand(int tenantId, out int affectedCount, out long? beforeTracking, IContinuousProgressCategory progress)
        {
            progress.Working("Populating...");
            using (var tenantCn = new SqlConnection(_appTenantMaster.ConnectionString))
            {
                tenantCn.Open();
                SqlCommand populateCmd = new SqlCommand(_populateSP, tenantCn);
                populateCmd.CommandTimeout = 10 * 60;       // 10mins
                populateCmd.CommandType = CommandType.StoredProcedure;
                populateCmd.Parameters.AddWithValue("@tenantId", tenantId).SqlDbType = SqlDbType.Int;

                affectedCount = 0;
                beforeTracking = null;
                using (var reader = populateCmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            switch (reader.GetName(i).ToLower())
                            {
                                case "affectedcount":
                                    affectedCount = reader.GetInt32(i);
                                    break;

                                case "beforetracking":
                                    var v = reader.GetSqlInt64(i);
                                    beforeTracking = v.IsNull ? null : (long?)v.Value;
                                    break;
                            }
                        }
                    }
                }
            }
        }
    }

    public class ODWTenantPopulateBuildingFloorsDimension : ODWTenantPopulate
    {
        public ODWTenantPopulateBuildingFloorsDimension(Func<DataAccessAdapter> fnAdapter, SqlTenantMaster appTenantMaster) : base(fnAdapter, appTenantMaster, "Dim_BuildingFloors")
        {
        }
    }
    public class ODWTenantPopulateCompaniesDimension : ODWTenantPopulate
    {
        public ODWTenantPopulateCompaniesDimension(Func<DataAccessAdapter> fnAdapter, SqlTenantMaster appTenantMaster) : base(fnAdapter, appTenantMaster, "Dim_Companies")
        {
        }
    }
    public class ODWTenantPopulateDatesDimension : ODWTenantPopulate
    {
        public ODWTenantPopulateDatesDimension(Func<DataAccessAdapter> fnAdapter, SqlTenantMaster appTenantMaster) : base(fnAdapter, appTenantMaster, "Dim_Dates")
        {
        }
    }
    public class ODWTenantPopulateDateTimesDimension : ODWTenantPopulate
    {
        public ODWTenantPopulateDateTimesDimension(Func<DataAccessAdapter> fnAdapter, SqlTenantMaster appTenantMaster) : base(fnAdapter, appTenantMaster, "Dim_DateTimes")
        {
        }
    }
    public class ODWTenantPopulateFacilitiesDimension : ODWTenantPopulate
    {
        public ODWTenantPopulateFacilitiesDimension(Func<DataAccessAdapter> fnAdapter, SqlTenantMaster appTenantMaster) : base(fnAdapter, appTenantMaster, "Dim_Facilities")
        {
        }
    }
    public class ODWTenantPopulateFacilityBuildingsDimension : ODWTenantPopulate
    {
        public ODWTenantPopulateFacilityBuildingsDimension(Func<DataAccessAdapter> fnAdapter, SqlTenantMaster appTenantMaster) : base(fnAdapter, appTenantMaster, "Dim_FacilityBuildings")
        {
        }
    }
    public class ODWTenantPopulateFloorZonesDimension : ODWTenantPopulate
    {
        public ODWTenantPopulateFloorZonesDimension(Func<DataAccessAdapter> fnAdapter, SqlTenantMaster appTenantMaster) : base(fnAdapter, appTenantMaster, "Dim_FloorZones")
        {
        }
    }
    public class ODWTenantPopulateRostersDimension : ODWTenantPopulate
    {
        public ODWTenantPopulateRostersDimension(Func<DataAccessAdapter> fnAdapter, SqlTenantMaster appTenantMaster) : base(fnAdapter, appTenantMaster, "Dim_Rosters")
        {
        }
    }
    public class ODWTenantPopulateSitesDimension : ODWTenantPopulate
    {
        public ODWTenantPopulateSitesDimension(Func<DataAccessAdapter> fnAdapter, SqlTenantMaster appTenantMaster) : base(fnAdapter, appTenantMaster, "Dim_Sites")
        {
        }
    }
    public class ODWTenantPopulateTaskCustomStatusDimension : ODWTenantPopulate
    {
        public ODWTenantPopulateTaskCustomStatusDimension(Func<DataAccessAdapter> fnAdapter, SqlTenantMaster appTenantMaster) : base(fnAdapter, appTenantMaster, "Dim_TaskCustomStatus")
        {
        }
    }
    public class ODWTenantPopulateTaskStatusDimension : ODWTenantPopulate
    {
        public ODWTenantPopulateTaskStatusDimension(Func<DataAccessAdapter> fnAdapter, SqlTenantMaster appTenantMaster) : base(fnAdapter, appTenantMaster, "Dim_TaskStatus")
        {
        }
    }
    public class ODWTenantPopulateTaskTypesDimension : ODWTenantPopulate
    {
        public ODWTenantPopulateTaskTypesDimension(Func<DataAccessAdapter> fnAdapter, SqlTenantMaster appTenantMaster) : base(fnAdapter, appTenantMaster, "Dim_TaskTypes")
        {
        }
    }
    public class ODWTenantPopulateUsersDimension : ODWTenantPopulate
    {
        public ODWTenantPopulateUsersDimension(Func<DataAccessAdapter> fnAdapter, SqlTenantMaster appTenantMaster) : base(fnAdapter, appTenantMaster, "Dim_Users")
        {
        }
    }
    public class ODWTenantPopulateZoneSitesDimension : ODWTenantPopulate
    {
        public ODWTenantPopulateZoneSitesDimension(Func<DataAccessAdapter> fnAdapter, SqlTenantMaster appTenantMaster) : base(fnAdapter, appTenantMaster, "Dim_ZoneSites")
        {
        }
    }

    // Although this is named ODWTenantMaster, it is actually a special Tenant Populate since we are already
    // running on the Tenant Master we are copying data from its mtc.tblTenants table to its own dw.Dim_Tenants dimension
    public class ODWTenantMasterPopulateTenantsDimension : ODWTenantPopulate
    {
        public ODWTenantMasterPopulateTenantsDimension(Func<DataAccessAdapter> fnAdapter, SqlTenantMaster appTenantMaster) : base(fnAdapter, appTenantMaster, "Dim_Tenants")
        {
        }
    }

    public class ODWTenantPopulateTaskFacts : ODWTenantPopulate
    {
        private readonly AppTenant _appTenant;
        public ODWTenantPopulateTaskFacts(Func<DataAccessAdapter> fnAdapter, AppTenant appTenant, SqlTenantMaster appTenantMaster) : base(fnAdapter, appTenantMaster, "Fact_Tasks")
        {
            _appTenant = appTenant;
        }

        protected override void ExecutePopulateCommand(int tenantId, out int affectedCount, out long? beforeTracking, IContinuousProgressCategory progress)
        {
            var tenantTimezone = _appTenant.TimeZoneInfo();

            base.ExecutePopulateCommand(tenantId, out affectedCount, out beforeTracking, progress);

            progress.Working("Fixing local date/times...");
            var totalEntities = 0;
            using (var adapter = _fnAdapter())
            {
                var trackingLong = beforeTracking ?? 0;
                // Populate LocalDateTime fields with task sites timezone
                var lmd = new LinqMetaData(adapter);
                var items = from ft in lmd.FactTask
                            where
    ft.TrackingLong > trackingLong &&
    (!ft.DimLocalDateCreated.HasValue || !ft.LocalDateTimeCreated.HasValue || !ft.LocalDateTimeCompleted.HasValue || !ft.LocalStatusDate.HasValue)
                            select ft;

                var entities = new EntityCollection<FactTaskEntity>();
                var siteTimeZones = new Dictionary<Guid, TimeZoneInfo>();
                foreach (var ft in items)
                {
                    TimeZoneInfo siteTimeZone;

                    if (ft.SiteId.HasValue == false)
                    {
                        siteTimeZone = tenantTimezone;
                    }
                    else
                    {
                        if (!siteTimeZones.TryGetValue(ft.SiteId.Value, out siteTimeZone))
                        {
                            var timezone = (from ud in lmd.UserData where ud.UserId == ft.SiteId.Value select ud.TimeZone).FirstOrDefault();
                            if (timezone == null)
                            {
                                // No timezone defined for this site, cannot populate localtime for it
                                continue;
                            }

                            siteTimeZone = TimeZoneInfoResolver.ResolveWindows(timezone);
                            if (siteTimeZone == null)
                            {
                                continue;
                            }

                            siteTimeZones[ft.SiteId.Value] = siteTimeZone;
                        }
                    }
                    bool changed = false;
                    if (!ft.DimLocalDateCreated.HasValue && ft.DimUtcDateCreated.HasValue)
                    {
                        var utc = DateTime.SpecifyKind(ft.DimUtcDateCreated.Value, DateTimeKind.Utc);
                        ft.DimLocalDateCreated = (TimeZoneInfo.ConvertTime(utc, siteTimeZone)).Date;
                        changed = true;
                    }
                    if (!ft.DimLocalDateTimeCreated.HasValue && ft.DimUtcDateTimeCreated.HasValue)
                    {
                        var utc = DateTime.SpecifyKind(ft.DimUtcDateTimeCreated.Value, DateTimeKind.Utc);
                        ft.DimLocalDateTimeCreated = TimeZoneInfo.ConvertTime(utc, siteTimeZone);
                        changed = true;
                    }
                    if (!ft.LocalDateTimeStarted.HasValue && ft.UtcDateTimeStarted.HasValue)
                    {
                        var utc = DateTime.SpecifyKind(ft.UtcDateTimeStarted.Value, DateTimeKind.Utc);
                        ft.LocalDateTimeStarted = TimeZoneInfo.ConvertTime(utc, siteTimeZone);
                        changed = true;
                    }
                    if (!ft.LocalDateTimeCompleted.HasValue && ft.UtcDateTimeCompleted.HasValue)
                    {
                        var utc = DateTime.SpecifyKind(ft.UtcDateTimeCompleted.Value, DateTimeKind.Utc);
                        ft.LocalDateTimeCompleted = TimeZoneInfo.ConvertTime(utc, siteTimeZone);
                        changed = true;
                    }
                    if (!ft.LocalDateTimeCreated.HasValue && ft.UtcDateTimeCreated.HasValue)
                    {
                        var utc = DateTime.SpecifyKind(ft.UtcDateTimeCreated.Value, DateTimeKind.Utc);
                        ft.LocalDateTimeCreated = TimeZoneInfo.ConvertTime(utc, siteTimeZone);
                        changed = true;
                    }
                    if (!ft.LocalStatusDate.HasValue)
                    {
                        var utc = DateTime.SpecifyKind(ft.UtcStatusDate, DateTimeKind.Utc);
                        ft.LocalStatusDate = TimeZoneInfo.ConvertTime(utc, siteTimeZone);
                        changed = true;
                    }

                    if (ft.ActualDuration < 0)
                    {
                        // this is a partial bug fix for EVS-1177, which is worked around by changes to the stored proc
                        // but a couple of entries still get through
                        ft.ActualDuration = 0;
                        changed = true;
                    }

                    if (changed)
                    {
                        entities.Add(ft);

                        if (entities.Count >= 1000)
                        {
                            adapter.SaveEntityCollection(entities);
                            totalEntities += entities.Count;
                            progress.Working(totalEntities.ToString());
                            entities.Clear();
                        }
                    }
                }

                if (entities.Count > 0)
                {
                    adapter.SaveEntityCollection(entities);
                    totalEntities += entities.Count;
                    progress.Working(totalEntities.ToString());
                }
            }
        }
    }
}
