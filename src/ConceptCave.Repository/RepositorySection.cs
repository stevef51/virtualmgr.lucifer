﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Xml;
using ConceptCave.BusinessLogic.Configuration;
using ConceptCave.Core;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Checklist.Interfaces;

namespace ConceptCave.Repository
{

    //Exists for legacy compatability with RepositorySectionManager.Current property
    public static class RepositorySectionManager
    {
        public static IRepositorySectionManager Current
        {
            get { return (IRepositorySectionManager) ConfigurationManager.GetSection("conceptcave.repository.resources"); }
        }
    }
}
