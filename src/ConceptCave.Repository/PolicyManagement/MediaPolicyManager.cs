﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.Lingo;
using ConceptCave.Checklist.RunTime;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.PolicyManagement;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.RepositoryInterfaces.ViewDefs;
using Newtonsoft.Json.Linq;

namespace ConceptCave.Repository.PolicyManagement
{
    public class MediaPolicyManager : IMediaPolicyManager
    {
        protected IMediaRepository _mediaRepo;
        protected IMembershipRepository _memberRepo;
        private readonly IEmailConfiguration _emailConfiguration;
        public MediaPolicyManager(IMediaRepository mediaRepo, IMembershipRepository memberRepo, IEmailConfiguration emailConfiguration)
        {
            _mediaRepo = mediaRepo;
            _memberRepo = memberRepo;
            _emailConfiguration = emailConfiguration;
        }

        public void Run()
        {
            var folders = _mediaRepo.GetFolders(false, null, RepositoryInterfaces.Enums.MediaFolderLoadInstruction.FolderFile | RepositoryInterfaces.Enums.MediaFolderLoadInstruction.FolderFileMedia | RepositoryInterfaces.Enums.MediaFolderLoadInstruction.MediaFolderPolicies | RepositoryInterfaces.Enums.MediaFolderLoadInstruction.FolderFileMediaPolicies);

            List<MediaPolicyManagerRun> results = new List<MediaPolicyManagerRun>();

            foreach (var folder in folders)
            {
                Run(folder, null, results);
            }

            Action(results);
        }

        public void Run(MediaFolderDTO current)
        {
            List<MediaPolicyManagerRun> results = new List<MediaPolicyManagerRun>();

            // guarantee we've loadded everything we need for this to work
            var folder = _mediaRepo.GetFolderById(current.Id, RepositoryInterfaces.Enums.MediaFolderLoadInstruction.FolderFile | RepositoryInterfaces.Enums.MediaFolderLoadInstruction.FolderFileMedia | RepositoryInterfaces.Enums.MediaFolderLoadInstruction.MediaFolderPolicies | RepositoryInterfaces.Enums.MediaFolderLoadInstruction.FolderFileMediaPolicies);

            Run(folder, null, results);

            Action(results);
        }

        public void Run(MediaFolderDTO current, MediaPolicyManagerRun run, List<MediaPolicyManagerRun> results)
        {
            var currentRun = run;
            if (current.MediaFolderPolicy != null && string.IsNullOrEmpty(current.MediaFolderPolicy.Data) == false)
            {
                // we define our own policy, so we override what ever we've been given
                var r = new MediaPolicyManagerRun(current.MediaFolderPolicy);

                if (r.IsEmpty == false)
                {
                    currentRun = r;
                    results.Add(currentRun);
                }
            }

            // folders first
            foreach (var folder in current.MediaFolders)
            {
                Run(folder, currentRun, results);
            }

            var folderRun = currentRun;

            // now files from us (the above call will have done files from our descendants
            foreach (var file in current.MediaFolderFiles)
            {
                currentRun = folderRun; // reset back to the default for the folder as it may have been overridden by the previous media

                if (file.Medium.MediaPolicy != null && string.IsNullOrEmpty(file.Medium.MediaPolicy.Data) == false)
                {
                    var r = new MediaPolicyManagerRun(file.Medium.MediaPolicy);

                    if (r.IsEmpty == false)
                    {
                        currentRun = r;
                        results.Add(currentRun);
                    }
                }

                if (currentRun == null)
                {
                    // there is no policy for the file, either anywhere above it in the hierarchy or on it's self
                    continue;
                }

                // there is a policy for it somewhere
                Run(file, currentRun);
            }
        }

        public void Run(MediaFolderFileDTO file, MediaPolicyManagerRun run)
        {
            foreach (var policy in run.Policies)
            {
                string type = policy.Policy["type"].Value<string>();
                // this needs to be changed to dynamically loading the handler via reflection or something else extensible,
                // however for the moment I'm going with a switch
                switch (type)
                {
                    case "currentversion-notreadfor":
                        CurrentMediaVersionNotReadForPolicy(file, policy);
                        break;

                    case "currentversion-older":
                        CurrentMediaVersionOlderPolicy(file, policy);
                        break;
                }
            }
        }

        protected DateTime GetExpirationDate(DateTime startDate, int age, string period)
        {
            switch (period)
            {
                case "day":
                    return startDate.AddDays(age);
                case "week":
                    return startDate.AddDays(age * 7);
                case "month":
                    return startDate.AddMonths(age);
                case "year":
                    return startDate.AddYears(age);
            }

            return DateTime.MaxValue;
        }

        protected void CurrentMediaVersionNotReadForPolicy(MediaFolderFileDTO file, MediaPolicyManagerRunItem run)
        {
            MediaPolicyVersionNotReadFor p = run.Policy.ToObject<MediaPolicyVersionNotReadFor>();

            if (run.Policy["labels"] == null)
            {
                return;
            }

            // ok we need to get all users that are in the set of labels defined on the policy
            List<Guid> labels = new List<Guid>();
            foreach (var id in run.Policy["labels"])
            {
                labels.Add(Guid.Parse(id.Value<string>()));
            }

            var users = _memberRepo.GetAllUsersByLabel(labels);

            // now its a matter of going through these users and working out if they meet our policy or not
            // to meet our policy they have to have read the current version of the media, if they haven't then
            // they imediately fail the policy. If they have, then the date they read it needs to be within the expiration
            // date.

            // list of viewings first
            var viewings = _mediaRepo.GetLatestMediaViewingsForUsers((from u in users select u.UserId).ToArray(), file.MediaId);

            Action<UserListResponse, MediaDTO, MediaPolicyManagerRunItem> addFailure = (user, media, r) =>
            {
                MediaPolicyVersionNotReadForResult policyResult = null;

                if (r.Results.ContainsKey(user.UserId) == false)
                {
                    policyResult = new MediaPolicyVersionNotReadForResult() { User = user };
                    r.Results.Add(user.UserId, policyResult);
                }
                else
                {
                    policyResult = (MediaPolicyVersionNotReadForResult)r.Results[user.UserId];
                }

                policyResult.Items.Add(media);
            };

            foreach (var user in users)
            {
                var viewing = from v in viewings where v.UserId == user.UserId select v;

                if (viewing.Count() == 0)
                {
                    addFailure(user, file.Medium, run);
                    continue;
                }

                var view = viewing.First();

                if (view.Version < file.Medium.Version)
                {
                    // failed the policy as they've not read the current version, they've read a previous one
                    addFailure(user, file.Medium, run);
                    continue;
                }

                DateTime expires = GetExpirationDate(view.DateStarted, p.Age, p.Period);

                if (expires > DateTime.UtcNow)
                {
                    // failed the policy as though they've read the current version, they've not read it in the expiry period
                    addFailure(user, file.Medium, run);
                    continue;
                }
            }
        }

        protected void CurrentMediaVersionOlderPolicy(MediaFolderFileDTO file, MediaPolicyManagerRunItem run)
        {
            MediaPolicyVersionOlderThan p = run.Policy.ToObject<MediaPolicyVersionOlderThan>();

            DateTime expires = GetExpirationDate(file.Medium.DateVersionCreated, p.Age, p.Period);

            if (expires > DateTime.UtcNow.Date)
            {
                return; // nothing to do
            }

            if (run.Results.ContainsKey(file.MediaId) == true)
            {
                return;
            }

            run.Results.Add(file.MediaId, file.Medium);
        }

        protected string BuildDocumentTable(IList<MediaDTO> medias)
        {
            var formatting = new ConceptCave.Checklist.Facilities.FormattingFacility();

            var table = formatting.CreateTableForEmail();
            var header = table.CreateHeaderRow();
            header.Add("Media");
            header.Add("Description");

            foreach (var m in medias)
            {
                var row = table.CreateBodyRow();
                row.Add(m.Name);
                row.Add(m.Description);
            }

            return table.ToHtml();
        }

        protected void CurrentMediaVersionOlderAction(MediaPolicyManagerRunItem run)
        {
            if (run.Policy["action"]["email"] == null)
            {
                return;
            }

            if (run.Policy["action"]["email"]["subject"] == null || string.IsNullOrEmpty(run.Policy["action"]["email"]["subject"].Value<string>()) == true)
            {
                return;
            }

            if (run.Policy["action"]["email"]["body"] == null || string.IsNullOrEmpty(run.Policy["action"]["email"]["body"].Value<string>()) == true)
            {
                return;
            }

            if(run.Policy["action"]["labels"] == null)
            {
                return;
            }

            List<Guid> labels = new List<Guid>();
            foreach(var id in run.Policy["action"]["labels"])
            {
                labels.Add(Guid.Parse(id.Value<string>()));
            }

            var users = _memberRepo.GetAllUsersByLabel(labels);

            LingoProgramDefinition definition = new LingoProgramDefinition() { SourceCode = "" };
            WorkingDocument wd = new WorkingDocument();
            ILingoProgram program = definition.Compile(wd);
            var vb = wd.Variables;

            List<MediaDTO> medias = new List<MediaDTO>();
            foreach (var m in run.Results.Values)
            {
                medias.Add((MediaDTO)m);
            }

            vb.Variable<object>("Documents", BuildDocumentTable(medias));

            string emailSubject = run.Policy["action"]["email"]["subject"].Value<string>();
            string emailBody = run.Policy["action"]["email"]["body"].Value<string>();

            SmtpClient client = new SmtpClient();

            foreach (var user in users)
            {
                var u = _memberRepo.GetById(user.UserId, RepositoryInterfaces.Enums.MembershipLoadInstructions.AspNetMembership);

                if (string.IsNullOrEmpty(u.AspNetUser.Email) == true)
                {
                    continue;
                }

                vb.Variable<object>("User", u);

                string expandedBody = program.ExpandString(emailBody);

                var mail = new MailMessage();
                var to = _emailConfiguration != null ? _emailConfiguration.FilterAddress(u.AspNetUser.Email) : u.AspNetUser.Email;
                if (to.Length > 0)
                {
                    mail.To.Add(to);
                }
                mail.Subject = emailSubject;
                mail.Body = expandedBody;
                mail.IsBodyHtml = true;
                if (_emailConfiguration != null)
                {
                    mail.From = _emailConfiguration.From;
                }
                if (mail.To.Count > 0 && (_emailConfiguration != null ? _emailConfiguration.Enabled : true))
                {
                    client.Send(mail);
                }
            }
        }

        protected void CurrentMediaVersionNotReadForAction(MediaPolicyManagerRunItem run)
        {
            if (run.Policy["action"]["email"] == null)
            {
                return;
            }

            if (run.Policy["action"]["email"]["subject"] == null || string.IsNullOrEmpty(run.Policy["action"]["email"]["subject"].Value<string>()) == true)
            {
                return;
            }

            if (run.Policy["action"]["email"]["body"] == null || string.IsNullOrEmpty(run.Policy["action"]["email"]["body"].Value<string>()) == true)
            {
                return;
            }

            LingoProgramDefinition definition = new LingoProgramDefinition() { SourceCode = "" };
            WorkingDocument wd = new WorkingDocument();
            ILingoProgram program = definition.Compile(wd);
            var vb = wd.Variables;

            string emailSubject = run.Policy["action"]["email"]["subject"].Value<string>();
            string emailBody = run.Policy["action"]["email"]["body"].Value<string>();

            SmtpClient client = new SmtpClient();

            foreach (MediaPolicyVersionNotReadForResult result in run.Results.Values)
            {
                var u = _memberRepo.GetById(result.User.UserId, RepositoryInterfaces.Enums.MembershipLoadInstructions.AspNetMembership);

                if (string.IsNullOrEmpty(u.AspNetUser.Email) == true)
                {
                    continue;
                }

                vb.Variable<object>("User", u);
                vb.Variable<object>("Documents", BuildDocumentTable(result.Items));

                string expandedBody = program.ExpandString(emailBody);

                var mail = new MailMessage();
                var to = _emailConfiguration != null ? _emailConfiguration.FilterAddress(u.AspNetUser.Email) : u.AspNetUser.Email;
                if (to.Length > 0)
                {
                    mail.To.Add(to);
                }
                mail.Subject = emailSubject;
                mail.Body = expandedBody;
                mail.IsBodyHtml = true;
                if (_emailConfiguration != null)
                {
                    mail.From = _emailConfiguration.From;
                }

                if (mail.To.Count > 0 && (_emailConfiguration != null ? _emailConfiguration.Enabled : true))
                {
                    client.Send(mail);
                }
            }
        }

        public void Action(List<MediaPolicyManagerRun> results)
        {
            foreach (var run in (from r in results from p in r.Policies select p))
            {
                if (run.Results.Count == 0)
                {
                    continue;
                }

                string type = run.Policy["type"].Value<string>();
                // this needs to be changed to dynamically loading the handler via reflection or something else extensible,
                // however for the moment I'm going with a switch
                switch (type)
                {
                    case "currentversion-notreadfor":
                        CurrentMediaVersionNotReadForAction(run);
                        break;

                    case "currentversion-older":
                        CurrentMediaVersionOlderAction(run);
                        break;
                }
            }
        }
    }
}
