﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.ViewDefs;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ConceptCave.Repository.PolicyManagement
{
    public class MediaPolicyManagerRun
    {
        public List<MediaPolicyManagerRunItem> Policies { get; protected set; }

        public MediaPolicyManagerRun() 
        {
            Policies = new List<MediaPolicyManagerRunItem>();
        }

        public MediaPolicyManagerRun(MediaFolderPolicyDTO policy) : this()
        {
            var ps = JArray.Parse(policy.Data);

            foreach (var p in ps)
            {
                Policies.Add(new MediaPolicyManagerRunItem((JObject)p));
            }
        }

        public MediaPolicyManagerRun(MediaPolicyDTO policy) : this()
        {
            var ps = JArray.Parse(policy.Data);

            foreach (var p in ps)
            {
                Policies.Add(new MediaPolicyManagerRunItem((JObject)p));
            }
        }

        public bool IsEmpty
        {
            get
            {
                return Policies.Count == 0;
            }
        }
    }

    public class MediaPolicyManagerRunItem
    {
        public Dictionary<Guid, object> Results { get; protected set; }
        public JObject Policy { get; set; }

        public MediaPolicyManagerRunItem(JObject policy)
        {
            Results = new Dictionary<Guid, object>();

            Policy = policy;
        }
    }

    [JsonObject]
    public class MediaPolicyVersionOlderThan
    {
        [JsonProperty("age")]
        public int Age { get; set; }

        [JsonProperty("period")]
        public string Period { get; set; }
    }

    [JsonObject]
    public class MediaPolicyVersionNotReadFor
    {
        [JsonProperty("age")]
        public int Age { get; set; }

        [JsonProperty("period")]
        public string Period { get; set; }
    }

    public class MediaPolicyVersionNotReadForResult
    {
        public UserListResponse User { get; set; }
        public List<MediaDTO> Items { get; set; }

        public MediaPolicyVersionNotReadForResult()
        {
            Items = new List<MediaDTO>();
        }
    }

    [JsonObject]
    public class PolicyEmailAction
    {
        [JsonProperty("subject")]
        public string Subject { get; set; }

        [JsonProperty("body")]
        public string Body { get; set; }
    }
}
