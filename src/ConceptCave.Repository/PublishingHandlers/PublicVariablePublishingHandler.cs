﻿using System;
using System.Linq;
using ConceptCave.Repository.ProcessingRules;
using ConceptCave.Checklist.Editor;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Checklist.Interfaces;
using SD.LLBLGen.Pro.ORMSupportClasses;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Data.DatabaseSpecific;

namespace ConceptCave.Repository.PublishingHandlers
{
    public class PublicVariablePublishingHandler : IPublishingHandler
    {
        private readonly Func<DataAccessAdapter> _fnAdapter;

        public PublicVariablePublishingHandler(Func<DataAccessAdapter> fnAdapter)
        {
            _fnAdapter = fnAdapter;
        }

        public bool CanHandle(object instance)
        {
            return instance is PublicVariable;
        }

        public void Handle(DesignDocument document, PublishingGroupResourceEntity entity)
        {
            if (entity.IsNew == false)
            {
                PredicateExpression expression = new PredicateExpression(PublishingGroupResourcePublicVariableFields.PublishingGroupResourceId == entity.Id);

                using (var adapter = _fnAdapter())
                {
                    adapter.DeleteEntitiesDirectly("PublishingGroupResourcePublicVariableEntity", new RelationPredicateBucket(expression));
                }
            }

            var vars = (from q in (from p in document.AsDepthFirstEnumerable() where p is IQuestion select p as IQuestion) from v in q.Validators where v is PublicVariable select v as PublicVariable);

            if (vars.Count() == 0)
            {
                return;
            }

            foreach (var v in vars)
            {
                PublishingGroupResourcePublicVariableEntity varEntity = new PublishingGroupResourcePublicVariableEntity();
                varEntity.Name = v.Name;
                entity.PublishingGroupResourcePublicVariables.Add(varEntity);
            }
        }
    }
}
