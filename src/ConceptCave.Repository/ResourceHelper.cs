using ConceptCave.Checklist;
using ConceptCave.Core;
using ConceptCave.Core.Coding;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Repository
{
    public static class ResourceHelper
    {
        public static IResource ResourceFromEntity(ResourceDTO resourceEntity)
        {
            return ResourceFromString(resourceEntity.ResourceData.Data, resourceEntity.Type);
        }

        public static IResource ResourceFromString(string codedString, string type)
        {
            IDecoder decoder = CoderFactory.GetDecoder(codedString, null);

            IResource resource = null;
            decoder.Decode<IResource>(type, out resource);

            return resource;
        }

        public static ResourceDTO New(IResource resource)
        {
            var result = new ResourceDTO();

            result.NodeId = resource.Id;
            result.Name = resource.Name;

            return result;
        }

        public static IResource ResolveResources(ResourceDTO resourceEntity)
        {
            // get the resource extracted from the entity
            IResource resource = ResourceFromEntity(resourceEntity);

            // TODO: need to find all of the resource references, then go through load up those resources and then put them into the object graph.

            return resource;
        }
    }
}