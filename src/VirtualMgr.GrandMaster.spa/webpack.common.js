﻿const path = require('path');
const webpack = require('webpack');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const ManifestPlugin = require('webpack-manifest-plugin');
const ExtractTextWebpackPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');

const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

const { InjectManifest } = require('workbox-webpack-plugin');

module.exports = {
    entry: {
        app: './src/app.js'
    },
    output: {
        path: path.resolve(__dirname, 'wwwroot/spa'),
        filename: '[name].bundle.[hash].js',
        publicPath: '/spa/'
    },
    plugins: [
        new ManifestPlugin(),                   // Produces the Webpack manifest.json in ./wwwroot folder
        new CleanWebpackPlugin(),               // This cleans out the ./wwwroot folder on each build, only for dev really as prod uses Docker which is always clean anyway
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery'
        }),
        new ExtractTextWebpackPlugin('[name].[hash].css'),
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: path.resolve(__dirname, 'src/index.ejs'),
            environment: process.env.NODE_ENV
        }),
        new webpack.EnvironmentPlugin({
            NODE_ENV: 'Production'              // Use 'Production' unless process.env.NODE_ENV is defined
        }),
        /*        new InjectManifest({
                    swSrc: 'static/service-worker.js',
                })
        */
    ],
    module: {
        rules: [{
            test: /\.(js|jsd)?$/,
            exclude: /node_modules/,
            use: {
                loader: 'babel-loader',
                options: {
                    compact: true,
                    presets: [
                        '@babel/preset-env'
                    ],
                    plugins: ['@babel/plugin-proposal-optional-chaining', 'angularjs-annotate']     // This will fix any Angular functions not using explicit DI 
                    // (eg function($scope) instead of ['$scope', function($scope)])
                }
            }
        },
        {
            test: /template\.html$/,
            use: ['raw-loader']
        },
        {
            test: /\.(css|scss)$/,
            use: ExtractTextWebpackPlugin.extract({
                use: ['css-loader', 'sass-loader']
            })
        },
        {
            test: /\.(png|svg|jpg|jpeg|gif)$/,
            use: [
                'file-loader'
            ]
        },
        {
            test: /\.(woff|woff2|eot|ttf|otf)$/,
            use: [
                {
                    loader: 'file-loader',
                    options: {
                        name: '[name].[ext]',
                        //                        outputPath: 'fonts/'
                    }
                }
            ]
        },
        /*
         * Necessary to be able to use angular 1 with webpack as explained in https://github.com/webpack/webpack/issues/2049
         */
        {
            test: require.resolve('angular'),
            loader: 'exports-loader?window.angular'
        }
        ]
    }
};
