'use strict';

import app from './ngmodule';

app.factory('http-error-interceptor-svc', function (notificationsSvc) {
    var authSvc;

    return {
        responseError: function (rejection) {
            if (rejection.xhrStatus !== `abort`) {
                notificationsSvc.notify({
                    title: `Request for ${rejection.config.url}`,
                    text: rejection.data?.error || rejection.data
                });
            }
            return Promise.reject(rejection);
        }
    }
});

app.config(
    function ($httpProvider) {
        $httpProvider.interceptors.push('http-error-interceptor-svc');
    }
);


