'use strict';

import app from './ngmodule';
import { BehaviorSubject, Subject } from 'rxjs';
import { filter, map, catchError, of } from 'rxjs/operators';

function makeSubjectCache() {
    let subjects = {};
    return function getSubject(key, source) {
        let s = subjects[key];
        if (s == null) {
            s = new DataSource();
            subjects[key] = s;
        }
        return s;
    }
}

function replaceElement($subject, element, fnFind) {
    let arr = $subject.getValue();
    let i = arr.findIndex(fnFind);
    if (i >= 0) {
        if (element != null) {
            arr[i] = element;
        } else {
            arr.splice(i, 1);
        }
    } else if (element != null) {
        arr.push(element);
    }
    $subject.next(arr);
}

function DataSource() {
    this.$data = new BehaviorSubject();
    this.$loading = new BehaviorSubject();
    this.$error = new BehaviorSubject();

    this.data = data => {
        this.$error.next(null);
        this.$loading.next(false);
        this.$data.next(data);
        return data;
    }
    this.loading = () => {
        this.$error.next(null);
        this.$loading.next(true);
    }
    this.error = err => {
        this.$error.next(err);
        this.$loading.next(false);
        return err;
    }
}

app.factory('tenantsSvc', function ($q, grandMasterHttp, notificationsSvc) {
    let tenantSubjects = makeSubjectCache();
    let luciferReleaseSubjects = makeSubjectCache();
    let $tenants, $luciferReleases, $luciferReleaseProfiles, $tenantMasters;

    const svc = {
        getTenants: refresh => {
            if (refresh || $tenants == null) {
                $tenants = $tenants || new DataSource();
                $tenants.loading();
                grandMasterHttp.getTenants()
                    .then(tenants => $tenants.data(tenants))
                    .catch(err => $tenants.error(err));
            }
            return $tenants;
        },

        getTenantByHostname: hostname => {
            let $subject = tenantSubjects(hostname);

            $subject.loading();
            grandMasterHttp.getTenantByHostname(hostname)
                .then(tenant => $subject.data(tenant))
                .catch(err => $subject.error(err));

            return $subject;
        },

        saveTenant: tenant => {
            notificationsSvc.notify({ text: `Saving tenant ${tenant.hostname}` });
            let $subject = tenantSubjects(tenant.hostname);
            $subject.loading();
            grandMasterHttp.saveTenant(tenant)
                .then(result => {
                    notificationsSvc.notify({ text: `Saved tenant ${tenant.hostname}` });
                    $subject.data(result);

                    replaceElement($tenants.$data, result, t => t.hostname === result.hostname);
                })
                .catch(err => $subject.error(err));
            return $subject;
        },

        deleteTenant: hostname => {
            notificationsSvc.notify({ text: `Deleting tenant ${hostname}` });
            let $subject = tenantSubjects(hostname);
            $subject.loading();
            return grandMasterHttp.deleteTenant(hostname)
                .then(result => {
                    notificationsSvc.notify({ text: `Deleted tenant ${hostname}` });
                    $subject.data(null);

                    replaceElement($tenants.$data, null, t => t.hostname === result.hostname);
                })
                .catch(err => $subject.error(err));
        },

        enableTenant: (hostname, enable) => {
            notificationsSvc.notify({ text: `${enable ? 'Enabling' : 'Disabling'} tenant ${hostname}` });
            let $subject = tenantSubjects(hostname);
            $subject.loading();
            grandMasterHttp.enableTenant(hostname, enable)
                .then(result => {
                    notificationsSvc.notify({ text: `Tenant ${result.cluster ? 'Enabled' : 'Disabled'} ${hostname}` });
                    $subject.data(result);

                    replaceElement($tenants.$data, result, t => t.hostname === result.hostname);
                })
                .catch(err => $subject.error(err));
            return $subject;
        },

        apply: () => {
            $tenants = $tenants || new DataSource();
            $tenants.loading();
            grandMasterHttp.apply()
                .then(result => $tenants.data(result))
                .catch(err => $tenants.error(err));
        },

        getLuciferReleases: refresh => {
            if (refresh || $luciferReleases == null) {
                $luciferReleases = $luciferReleases || new DataSource();
                $luciferReleases.loading();
                grandMasterHttp.getLuciferReleases()
                    .then(releases => $luciferReleases.data(releases))
                    .catch(err => $luciferReleases.error(err));
            }
            return $luciferReleases;
        },

        getLuciferRelease: id => {
            let $subject = luciferReleaseSubjects(id);
            $subject.loading();
            grandMasterHttp.getLuciferRelease(id)
                .then(release => $subject.data(release))
                .catch(err => $subject.error(err));
            return $subject;
        },

        saveLuciferRelease: release => {
            let $subject = luciferReleaseSubjects(release.id);
            $subject.loading();
            grandMasterHttp.saveLuciferRelease(release)
                .then(release => {
                    $subject.data(release);
                    replaceElement($luciferReleases.$data, release, t => t.id === result.id);
                }).catch(err => $subject.error(err));
            return $subject;
        },

        deleteLuciferRelease: id => {
            let $subject = luciferReleaseSubjects(id);
            $subject.loading();
            return grandMasterHttp.deleteLuciferRelease(id)
                .then(success => {
                    if (success) {
                        replaceElement($luciferReleases.$data, null, t => t.id === id);
                    }
                    return success;
                })
        },

        getLuciferReleaseProfiles: refresh => {
            if (refresh || $luciferReleaseProfiles == null) {
                $luciferReleaseProfiles = $luciferReleaseProfiles || new DataSource();
                $luciferReleaseProfiles.loading();
                grandMasterHttp.getLuciferReleaseProfiles()
                    .then(data => $luciferReleaseProfiles.data(data))
                    .catch(err => $luciferReleaseProfiles.error(err));
            }
            return $luciferReleaseProfiles;
        },

        checkHostnameAvailable: hostname => {
            let cancel = $q.defer();
            let $promise = grandMasterHttp.checkHostnameAvailable(hostname, cancel.promise)
                .catch(err => ({
                    result: false,
                    message: err.message
                }));
            $promise.cancel = () => {
                cancel.resolve();
            }
            return $promise;
        },

        getTenantMasters: refresh => {
            if (refresh || $tenantMasters == null) {
                $tenantMasters = $tenantMasters || new DataSource();
                $tenantMasters.loading();
                grandMasterHttp.getTenantMasters()
                    .then(data => $tenantMasters.data(data))
                    .catch(err => $tenantMasters.error(err));
            }
            return $tenantMasters;
        },

        getElasticPoolsForTenantMaster: id => {
            return grandMasterHttp.getElasticPoolsForTenantMaster(id);
        },
        getTenantProfilesForTenantMaster: id => {
            return grandMasterHttp.getTenantProfilesForTenantMaster(id);
        },
        cloneTenant: details => {
            return grandMasterHttp.cloneTenant(details);
        }
    }

    return svc;
})