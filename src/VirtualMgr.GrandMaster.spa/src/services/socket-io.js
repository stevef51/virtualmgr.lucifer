
'use strict';

import app from './ngmodule';
const io = require('socket.io-client');

app.factory('socketIo', function () {
    const socket = io.connect(new URL(window.location).hostname, {
        path: `/api/socket.io`
    });

    socket.on(`connect`, () => {
        console.log('Connected to socket.io');
    })

    socket.on(`disconnect`, () => {
        console.log('Disconnected from socket.io');
    })

    return socket;
})