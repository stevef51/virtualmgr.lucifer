'use strict';

import app from './ngmodule';
import _ from 'lodash';
import { BehaviorSubject } from 'rxjs';
import { genV4 } from 'uuidjs';

app.factory('notificationsSvc', function () {
    let $notifications = new BehaviorSubject();
    $notifications.next([]);
    let $notification = new BehaviorSubject();

    return {
        getNotifications: function () {
            return $notifications;
        },

        getLastNotification: function () {
            return $notification;
        },

        notify: function (notification) {
            notification.id = notification.id || genV4();
            let arr = $notifications.getValue();
            arr.push(notification);
            $notifications.next(arr);
            $notification.next(notification);
        }
    }
})