'use strict';

import app from './ngmodule';
import { BehaviorSubject, Subject } from 'rxjs';
import { filter, map, catchError, of } from 'rxjs/operators';

app.factory('operationsSvc', function (socketIo) {
    let $lro = new BehaviorSubject();

    return {
        getLongRunningOperations() {
            return $lro;
        }
    }
})