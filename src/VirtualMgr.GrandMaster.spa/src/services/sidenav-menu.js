'use strict';

import app from './ngmodule';
import _ from 'lodash';

app.factory('sidenavMenu', function ($state) {
    let menuItems = [];

    // Extract menuitems out of State which defines a menuitem ..
    _.forIn($state.router.stateRegistry.states, s => {
        if (s.data && s.data.menuItem && Object.keys(s.params).length === 0) {
            menuItems.push(Object.assign({
                uisref: s.name
            }, s.data.menuItem));
        }
    });

    return {
        getMenuItems: function () {
            return menuItems;
        }
    }
})