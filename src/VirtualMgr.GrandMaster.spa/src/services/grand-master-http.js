'use strict';

import app from './ngmodule';
import _ from 'lodash';

app.factory('grandMasterHttp', function ($http, $q) {
    const responseData = response => response.data;
    const svc = {
        getTenants: () => {
            return $http({ url: `/api/tenants`, method: `get` }).then(responseData);
        },

        getTenantByHostname: hostname => {
            return $http({ url: `/api/tenant/${hostname}`, method: `get` }).then(responseData);
        },

        saveTenant: tenant => {
            return $http({ url: `/api/tenant`, method: 'post', data: tenant }).then(responseData);
        },

        enableTenant: (hostname, enable) => {
            return $http({ url: `/api/tenant/${hostname}/${enable ? 'enable' : 'disable'}`, method: 'post' }).then(responseData);
        },

        deleteTenant: (hostname) => {
            return $http({ url: `/api/tenant/${hostname}`, method: 'delete' }).then(responseData);
        },

        checkHostnameAvailable: (hostname, $cancel) => {
            return $http({ url: `/api/tenant/${hostname}/available`, method: `post`, timeout: $cancel }).then(responseData);
        },

        getLuciferReleases: () => {
            return $http({ url: `/api/lucifer-releases`, method: `get` }).then(responseData);
        },

        getLuciferRelease: id => {
            return $http({ url: `/api/lucifer-release/${id}`, method: `get` }).then(responseData);
        },

        saveLuciferRelease: release => {
            return $http({ url: `/api/lucifer-release`, method: `post`, data: release }).then(responseData);
        },

        deleteLuciferRelease: id => {
            return $http({ url: `/api/lucifer-release/${id}`, method: `delete` }).then(responseData);
        },

        getLuciferReleaseProfiles: () => {
            return $http({ url: `/api/lucifer-release-profiles`, method: `get` }).then(responseData);
        },

        getTenantMasters: () => {
            return $http({ url: `/api/tenant-masters`, method: `get` }).then(responseData);
        },

        getElasticPoolsForTenantMaster: id => {
            return $http({ url: `/api/tenant-master/${id}/elastic-pools`, method: `get` }).then(responseData);
        },

        getTenantProfilesForTenantMaster: id => {
            return $http({ url: `/api/tenant-master/${id}/tenant-profiles`, method: `get` }).then(responseData);
        },

        cloneTenant: details => {
            return $http({ url: `/api/clone-tenant`, method: `post`, data: details }).then(responseData);
        }
    }
    return svc;
})