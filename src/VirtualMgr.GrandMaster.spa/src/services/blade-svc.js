'use strict';

import app from './ngmodule';
import _ from 'lodash';

app.factory('bladeSvc', function ($state, $transitions, $stateParams) {
    let _config = [];
    let _breadCrumbs = [];

    // Extract menuitems out of State which defines a menuitem ..
    _.forIn($state.router.stateRegistry.states, s => {
        if (s.data && s.data.bladeLayout && Object.keys(s.params).length === 0) {
            _config.push({
                uisref: s.name,
                bladeLayout: s.data.bladeLayout
            });
        }
    });

    function makeBreadCrumbs() {
        let currentPath = $state.getCurrentPath();
        let crumbs = currentPath
            .filter(p => p.state.self.data && p.state.self.data.breadCrumb)
            .map(p => {
                return {
                    text: p.state.self.data.breadCrumb($stateParams),
                    state: p.state
                }
            });
        Array.prototype.splice.apply(_breadCrumbs, [0, _breadCrumbs.length, ...crumbs]);
    }

    $transitions.onSuccess({}, makeBreadCrumbs);
    makeBreadCrumbs();

    return {
        getBladeConfig: function (uisref) {
            return _.first(_config.filter(b => b.uisref === uisref));
        },

        breadCrumbs: _breadCrumbs
    }
})