'use strict';

import app from '../ngmodule';
import './style.scss';


function makeBlades(bladeCount) {
    let template = require('./start-template.html').default;

    for (let n = 1; n <= bladeCount; n++) {
        template += require('./row-template.html').default.replace(/\$\{n\}/g, n);
    }

    template += require('./end-template.html').default;

    app.component('bladeLayout', {
        template: template,
        controller: function ($scope, $state, bladeSvc, $transitions, $element) {
            const self = this;

            function addClass(blade, cls) {
                blade = blade || [];
                cls = angular.isArray(cls) ? cls : [cls];
                return _.union(blade, cls);
            }

            function removeClass(blade, cls) {
                blade = blade || [];
                cls = angular.isArray(cls) ? cls : [cls];
                return _.difference(blade, cls);
            }

            function transitionSuccess() {
                let statePath = $state.getCurrentPath();

                for (let n = 1; n <= bladeCount; n++) {
                    self[`blade${n}detail`] = ['hidden-blade'];
                }

                let activeBladeName;
                let activeBlades = {};
                for (let i = 0; i < statePath.length; i++) {
                    const pathDist = (statePath.length - 1) - i;
                    const p = statePath[i];
                    _.forEach(p.views, v => {
                        let config = bladeSvc.getBladeConfig(p.state.name) || { bladeLayout: ['blade'] };
                        let bladeClass = config.bladeLayout[pathDist > config.bladeLayout.length ? config.bladeLayout.length - 1 : pathDist];
                        activeBladeName = v.viewDecl.$uiViewName
                        activeBlades[activeBladeName] = bladeClass;
                    })
                }

                _.forOwn(activeBlades, (bladeClass, key) => {
                    self[key] = bladeClass;
                })
            }

            $transitions.onSuccess({}, transitionSuccess);

            // Call now to resync on a page refresh
            transitionSuccess();
        }
    })
}

makeBlades(4);

