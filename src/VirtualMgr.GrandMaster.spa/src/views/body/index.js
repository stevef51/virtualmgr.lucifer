'use strict';

import app from '../ngmodule';
import './style.scss';
import { filter } from 'rxjs/operators';

app.component('bodyCtrl', {
    template: require('./template.html').default,
    controller: function ($mdSidenav, sidenavMenu, notificationsSvc, $mdToast, $element) {
        const mainContentArea = $element[0].querySelector(`#main-content-area`);

        this.menuItems = sidenavMenu.getMenuItems();

        this.toggleSideNav = function (id) {
            $mdSidenav(id).toggle();
        }

        this.menuClick = function () {
            this.toggleSideNav('left');
        }

        notificationsSvc.getLastNotification()
            .pipe(
                filter(n => n != null)
            ).subscribe(n => {
                $mdToast.show(
                    $mdToast.simple()
                        .parent(mainContentArea)
                        .position('top right')
                        .textContent(n.text)
                );
            })
    }
})
