'use strict';

import app from '../ngmodule';

app.config(
    function ($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state('root', {
                abstract: true,
                url: '',
                views: {
                    'body@': {
                        component: 'bodyCtrl'
                    },
                    'content@root': {
                        component: 'bladeLayout'
                    },
                    'content-header@root': {
                        component: 'topbarCtrl'
                    }
                }
            })

        $urlRouterProvider.otherwise('/tenants');

    }
);


