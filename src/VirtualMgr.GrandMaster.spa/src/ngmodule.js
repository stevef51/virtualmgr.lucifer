'use strict';

import angular from 'angular';
import 'angular-resource';
import 'angular-ui-router';
import 'angular-animate';
import 'angular-material';
import 'angular-material/angular-material.css';
import 'angular-filter';
import 'angular-ui-ace';
import 'angular-messages';
import 'angular-material-data-table';
import 'angular-material-data-table/dist/md-data-table.css';
import 'angular-cache';
import 'angular-loading-bar';
import 'angular-loading-bar/build/loading-bar.css';
import 'rx-angular';
import 'angular1-async-filter';
import 'angular-moment';

var app = null;

try {
    // in the IOS app, the player application has already been defined
    // by the bootstrapping we do there, so we don't want to write over the top
    // of that, so we get that module
    app = angular.module('dashboard');
} catch (e) {
    // ok so we must be running in a normal browser, so in that case we create the module
    app = angular.module('dashboard', []);
}

export default app;

// now no matter what, add our dependancies
app.requires.push(
    'ngResource',
    'ui.router',
    'ui.router.util',
    'ui.router.router',
    'ui.router.state',
    'ngAnimate',
    'angular.filter',
    'ngMaterial',
    'ui.ace',
    'ngMessages',
    'md.data.table',
    'angular-cache',
    'rx',
    'angular-loading-bar',
    'asyncFilter',
    'angularMoment'
);

