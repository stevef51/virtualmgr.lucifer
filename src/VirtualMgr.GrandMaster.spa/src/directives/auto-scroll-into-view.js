'use strict';

import app from './ngmodule';

app.component('autoScrollIntoView', {
    template: '',
    controller: function ($element) {
        this.$onInit = function () {
            $element[0].scrollIntoView();
        }
    }
});


