'use strict';

import app from '../ngmodule';

app.component('bladeContent', {
    transclude: true,
    template: require('./template.html').default
});