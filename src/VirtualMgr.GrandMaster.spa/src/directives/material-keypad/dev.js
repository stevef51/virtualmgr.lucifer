'use strict';

export default {
    compile: `
    <md-input-container>
    <input type='text' ng-model='$ctrl.input.number'></input>
    </md-input-container>
    <material-keypad ng-model='$ctrl.input.number' step='$ctrl.input.step' keypad-layout='$ctrl.input.keypadLayout' flex></material-keypad>`,
    input: {
        number: '123',
        step: 0.01,
        keypadLayout: 'decimal'
    }
}