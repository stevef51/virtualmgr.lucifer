'use strict';

import app from '../ngmodule';
import './style.scss';
import { Subscription } from 'rxjs';

app.component('notificationsList', {
    restrict: 'E',
    template: require('./template.html').default,
    controller: function (notificationsSvc) {
        const self = this;
        let subscription = new Subscription();
        subscription.add(notificationsSvc.getNotifications().subscribe(n => self.notifications = n));
        this.$onDestroy = () => subscription.unsubscribe();
    }
});
