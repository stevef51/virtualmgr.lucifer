'use strict';

import app from '../../ngmodule';

app.component('bladeBreadCrumbs', {
    restrict: 'E',
    template: require('./template.html').default,
    controller: function (bladeSvc) {
        const self = this;

        self.breadCrumbs = bladeSvc.breadCrumbs;
        self.onBreadCrumbClick = function (crumb) {
            console.log(crumb);
        }
    }
});
