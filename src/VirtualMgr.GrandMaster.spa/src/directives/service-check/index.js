'use strict';

import app from '../ngmodule';

const MODES = {
    NONE: 0,
    LOADING: 1,
    NOTOK: 2,
    OK: 3
}

app.component('serviceCheck', {
    restrict: 'E',
    template: require('./template.html').default,
    bindings: {
        promise: '<'
    },
    controller: function ($timeout) {
        const self = this;

        this.mode = MODES.NONE;

        this.$onChanges = changesObj => {
            if (changesObj.promise != null) {
                self.mode = MODES.NONE;
                if (changesObj.promise.currentValue != null) {
                    self.mode = MODES.LOADING;
                    changesObj.promise.currentValue
                        .then(response => {
                            if (changesObj.promise.currentValue === self.promise) {
                                if (response.result) {
                                    self.mode = MODES.OK;
                                } else {
                                    self.mode = MODES.NOTOK;
                                    self.message = response.message;
                                }
                            }
                        })
                        .catch(err => {
                            if (changesObj.promise.currentValue === self.promise) {
                                self.mode = MODES.NOTOK;
                                self.message = err.message;
                            }
                        });
                }
            }
        }
    }
});
