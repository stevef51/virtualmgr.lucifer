'use strict';

import app from '../ngmodule';

app.component('bladeToolbar', {
    transclude: true,
    template: require('./template.html').default
});