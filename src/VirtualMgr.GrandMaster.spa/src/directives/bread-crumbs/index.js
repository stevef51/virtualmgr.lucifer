'use strict';

import app from '../ngmodule';
import './style.scss';

app.component('breadCrumbs', {
    restrict: 'E',
    require: {
        ngModelCtrl: 'ngModel'
    },
    bindings: {
        crumbs: '=ngModel',
        onClick: '&'
    },
    template: require('./template.html').default,
    controller: function () {
    }
});
