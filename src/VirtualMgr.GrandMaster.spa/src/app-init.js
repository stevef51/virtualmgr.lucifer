'use strict';

import angular from 'angular';
import './directives';
import './filters';
import './services';
import './blades';
import './config';
import './views';
import './body.scss';
import '@fortawesome/fontawesome-free/js/all';
import 'typeface-roboto';


// Make Angular _ available to anyone
import './utils/angularise-libraries';

import LogRocket from 'logrocket';
import { $injector } from '@uirouter/core';

const environment = process.env.NODE_ENV;
console.log(`Environment is ${environment}`);

/*if (environment === `production`) {
    LogRocket.init(`spho80/lucifer`);
}*/

var loc = window.location;
var scheme = loc.protocol;

var tenantHostname = loc.hostname;
window.razordata = {
    apiprefix: `${scheme}//${tenantHostname}/api/`,
    environment: `desktop`
};

let init$ = new Promise(function (resolve, reject) {
    resolve();
});

export default init$;
