﻿'use strict';

import appInit from './app-init';
import angular from 'angular';

appInit.then(function () {
    angular.bootstrap(document.documentElement, ['dashboard'], {
        strictDi: true
    });
});
