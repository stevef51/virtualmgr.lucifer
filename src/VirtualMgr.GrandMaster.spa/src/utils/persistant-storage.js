'use strict';

import angular from "angular";
import app from './ngmodule';

app.factory('persistantStorage', function ($q, $timeout) {
    console.log('ng_persistantStorage from bootstrap-compat.js');

    var makeSvc;
    if (window.persistantStorage) {
        console.log('persistantStorage from bootstrap-compat.js');

        var _ps = window.persistantStorage;
        // Make persistantStorage callbacks Angular aware .. 
        makeSvc = function (namespace) {
            var storage = _ps;
            if (_ps.createNamespace) {
                storage = _ps.createNamespace(namespace);
                namespace = '';
            } else {
                namespace += '.';
            }
            return {
                createNamespace: _ps.createNamespace,
                getItem: function (key, success) {
                    storage.getItem(namespace + key, success ? function (data) {
                        $timeout(success, 0, true, data);
                    } : null);
                },
                setItem: function (key, data, success, failure) {
                    storage.setItem(namespace + key, data, success ? function (result) {
                        $timeout(success, 0, true, result);
                    } : null, failure ? function (err) {
                        $timeout(failure, 0, true, err);
                    } : null);
                },
                removeItem: function (key, success) {
                    storage.removeItem(namespace + key, success ? function (exists) {
                        $timeout(success, 0, true, exists);
                    } : null);
                },
                clear: function (success) {
                    storage.clear(success ? function () {
                        $timeout(success);
                    } : null);
                }
            }
        }
    } else {
        function dataObject(data) {
            if (data === undefined || data === null) {
                return data;
            }
            if (typeof data !== 'string') {
                return data;
            }
            try {
                return JSON.parse(data);
            } catch (err) {
                return data;
            }
        }

        // Revert to localStorage ..
        makeSvc = function (namespace) {
            return {
                getItem: function (key, success) {
                    var data = localStorage.getItem(namespace + '.' + key);
                    if (success) {
                        $timeout(success, 0, true, dataObject(data));
                    }
                },
                setItem: function (key, data, success, failure) {
                    try {
                        localStorage.setItem(namespace + '.' + key, JSON.stringify(data));
                        if (success) {
                            $timeout(success, 0, true, data);
                        }
                    } catch (err) {
                        if (failure) {
                            $timeout(failure, 0, true, err);
                        }
                    }
                },
                removeItem: function (key, success) {
                    var exists = localStorage.getItem(namespace + '.' + key) != null;
                    localStorage.removeItem(namespace + '.' + key);
                    if (success) {
                        $timeout(success, 0, true, exists);
                    }
                },
                // This is kinda dangerous as namespace clear is not possible, I don't think anyone calls .clear() though
                clear: function (success) {
                    localStorage.clear();
                    if (success) {
                        $timeout(success);
                    }
                }
            }
        }
    }

    var extendSvc = function (rawSvc) {        // We can add the following function which "watches" a key and calls the callback whenever setItem is called
        var _onKeyChanged = Object.create(null);
        return angular.extend({}, rawSvc, {
            onChange: function (key, callback) {
                var changeList = _onKeyChanged[key];
                if (angular.isUndefined(changeList)) {
                    changeList = [];
                    _onKeyChanged[key] = changeList;
                }
                changeList.push(callback);

                // Get the item now ..
                rawSvc.getItem(key, callback);

                // Return unsubscribe function
                return function () {
                    var i = changeList.indexOf(callback);
                    if (i >= 0) {
                        changeList.splice(i, 1);
                        if (changeList.length == 0) {
                            delete _onKeyChanged[key];
                        }
                    }
                }
            },
            getItem: function (key, success) {
                var deferred = $q.defer();
                rawSvc.getItem(key, data => {
                    if (success) {
                        success(data);
                    }
                    deferred.resolve(data);
                });
                return deferred.promise;
            },
            setItem: function (key, data, success, failure) {
                var deferred = $q.defer();
                rawSvc.setItem(key, data, function (result) {
                    if (success) {
                        success(result);
                    }
                    deferred.resolve(result);
                    var changeList = _onKeyChanged[key];
                    if (angular.isDefined(changeList)) {
                        angular.forEach(changeList, function (cb) {
                            cb(data);
                        })
                    }
                }, err => {
                    if (failure)
                        failure(err);
                    deferred.reject(err);
                });
                return deferred.promise;
            },
            removeItem: function (key, success) {
                var deferred = $q.defer();
                rawSvc.removeItem(key, function (exists) {
                    if (success) {
                        success(exists);
                    }
                    deferred.resolve(exists);
                    var changeList = _onKeyChanged[key];
                    if (angular.isDefined(changeList)) {
                        angular.forEach(changeList, function (cb) {
                            cb(null); // null -> removed
                        })
                    }
                })
                return deferred.promise;
            },
            clear: function (success) {
                var deferred = $q.defer();
                rawSvc.clear(() => {
                    if (success) {
                        success();
                    }
                    deferred.resolve();
                });
                return deferred.promise;
            }
        });
    }

    return angular.extend({}, extendSvc(makeSvc('')), {
        createNamespace: function (namespace) {
            return extendSvc(makeSvc(namespace));
        }
    });
});
