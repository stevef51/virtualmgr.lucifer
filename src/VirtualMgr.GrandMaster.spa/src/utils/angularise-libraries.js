'use strict';

import player from './ngmodule';
import _ from 'lodash';
import moment from 'moment';

// Make libraries available as a Angular services
player.factory('_', () => _);
player.factory('moment', () => moment);
