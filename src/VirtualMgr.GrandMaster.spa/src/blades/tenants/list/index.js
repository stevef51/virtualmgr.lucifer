'use strict';

import app from '../../ngmodule';
import { Subscription } from 'rxjs';
import { tap, filter, map } from 'rxjs/operators';
import * as _ from 'lodash';
import { createBladeCtrl } from '../../blade';

app.component('tenantsContentCtrl', createBladeCtrl({
    template: require('./template.html').default,
    controller: ['tenantsSvc', '$state', function (tenantsSvc, $state) {
        const self = this;
        self.order = 'name';
        self.search = '';

        self.filter = function (t) {
            let search = t.hostname
                + (t.sql?.name || '')
                + (t.sql?.profile?.name || '')
                + (t.sql?.sqlTenantMaster?.name || '')
                + (t.sql?.sqlHost?.name || '')
                + (t.cluster?.luciferRelease || '')
                + (t.sql?.luciferRelease || '')
                + (t.sql?.enabled || '');
            return search.toLowerCase().indexOf(self.search.toLowerCase()) >= 0;
        }

        let $tenants = tenantsSvc.getTenants();
        self.watchLoading($tenants);
        self.addSubscription($tenants.$data.subscribe(tenants => self.tenants = tenants));

        self.edit = tenant => $state.go('root.tenants.tenant', { hostname: tenant.hostname });

        self.canRefresh = () => !self.ajaxing;
        self.refresh = () => tenantsSvc.getTenants(true);
        self.canApply = () => !self.ajaxing && (self.tenants.filter(t => t.shouldApply).length != 0);
        self.apply = () => tenantsSvc.apply();

        self.rowColor = tenant => {
            if (tenant.issues.length) {
                return {
                    background: 'default-warn-900-0.43'
                }
            }
        }

        self.issues = tenant => {
            return (tenant.issues || []).join('<br/>');
        }
    }]
}))

