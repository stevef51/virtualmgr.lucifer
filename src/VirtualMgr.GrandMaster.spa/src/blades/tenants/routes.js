'use strict';

import app from '../../ngmodule';

app.config(function ($stateProvider) {
    $stateProvider
        .state('root.tenants', {
            url: '/tenants',
            views: {
                'blade1detail@root': {
                    component: 'tenantsContentCtrl'
                }
            },
            data: {
                menuItem: {
                    title: `Tenants`,
                    fontSet: 'fas',
                    fontIcon: 'fa-globe'
                },
                breadCrumb: () => 'Tenants'
            }
        })

        .state('root.tenants.tenant', {
            url: '/:hostname',
            views: {
                'blade2detail@root': {
                    component: 'tenantContentCtrl'
                }
            },
            data: {
                breadCrumb: $stateParams => $stateParams.hostname
            }
        })
});
