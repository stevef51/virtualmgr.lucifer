'use strict';

import app from '../../ngmodule';
import { fromEvent, of, Subscription } from 'rxjs';
import { debounceTime, map, distinctUntilChanged, switchMap, tap } from 'rxjs/operators';

app.config(function ($mdDialogProvider) {
    $mdDialogProvider.addPreset('cloneTenantDialog', {
        options: function () {
            return {
                parent: angular.element(document.body),

                template: require('./template.html').default,
                controller: ['$scope', '$mdDialog', 'tenantsSvc', 'locals', '$element', function ($scope, $mdDialog, tenantsSvc, locals, $element) {
                    const subscription = new Subscription();
                    $scope.$on(`$destroy`, subscription.unsubscribe);

                    $scope.source = locals.source;
                    $scope.domain = ``;         // This should probably be calculated from the Tenant Master, at the moment the full domain is required

                    subscription.add(fromEvent($element.find('#hostname')[0], 'keyup')
                        .pipe(
                            tap(() => {
                                if ($scope.$hostnameCheck) {
                                    $scope.$hostnameCheck.cancel();
                                }
                                $scope.$hostnameCheck = null;
                            }),
                            debounceTime(500),
                            map(e => e.target.value),
                            distinctUntilChanged()
                        ).subscribe(hostname => {
                            $scope.$hostnameCheck = tenantsSvc.checkHostnameAvailable(hostname);
                            $scope.$hostnameCheck.then(response => {
                                $scope.hostnameIsAvailable = response.result;
                                $scope.details.databaseName = $scope.hostnameIsAvailable ? `${hostname}-db` : ``;
                                return response;
                            });
                        }));

                    $scope.details = {
                        source: locals.source
                    }

                    $scope.$watch(`details.tenantMaster`, (newValue) => {
                        if (newValue) {
                            tenantsSvc.getElasticPoolsForTenantMaster(newValue.id).then(data => {
                                $scope.elasticPools = data;
                                $scope.elasticPool = data.length && data[0];
                            })
                            tenantsSvc.getTenantProfilesForTenantMaster(newValue.id).then(data => {
                                $scope.tenantProfiles = data;
                            })
                        }
                    });

                    let $tenantMasters = tenantsSvc.getTenantMasters(true);
                    subscription.add($tenantMasters.$data.subscribe(data => $scope.tenantMasters = data));

                    $scope.profileId = null;
                    $scope.cancel = () => $mdDialog.cancel();
                    $scope.clone = () => {
                        $mdDialog.hide($scope.details);
                    }
                    $scope.canClone = () => {
                        return $scope.details.hostname
                            && $scope.details.name
                            && $scope.details.tenantMaster
                            && $scope.details.elasticPool
                            && $scope.details.targetProfile
                            && $scope.details.profile;
                    }
                }]
            }
        }
    })
})
