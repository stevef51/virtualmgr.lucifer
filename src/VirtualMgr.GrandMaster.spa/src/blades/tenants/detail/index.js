'use strict';

import app from '../../ngmodule';
import './style.scss';
import { tap, filter } from 'rxjs/operators';
import { createBladeCtrl, compareOriginalToEdit } from '../../blade';

app.component('tenantContentCtrl', createBladeCtrl({
    template: require('./template.html').default,
    controller: ['$stateParams', 'tenantsSvc', '$timeout', '$mdDialog', '$state', function ($stateParams, tenantsSvc, $timeout, $mdDialog, $state) {
        const self = this;

        self.$releases = tenantsSvc.getLuciferReleases(true).$data;
        let $tenant = tenantsSvc.getTenantByHostname($stateParams.hostname);
        self.watchLoading($tenant);
        self.addSubscription($tenant.$data.subscribe(tenant => {
            $timeout(() => {
                if (tenant && tenant.sql) {
                    tenantsSvc.getTenantProfilesForTenantMaster(tenant.sql.sqlTenantMaster.id).then(data => {
                        self.edit = angular.copy(tenant);
                        self.tenantProfiles = data;

                        // In order for the md-select to work these have to lookup via ===
                        self.edit.sql.tenantProfile = data.find(p => p.id === self.edit.sql.tenantProfile.id);
                        self.edit.sql.targetProfile = data.find(p => p.id === self.edit.sql.targetProfile.id);

                        self.original = angular.copy(self.edit);
                    })
                } else {
                    self.edit = angular.copy(tenant);
                    self.original = angular.copy(self.edit);
                }
            });
        }));


        self.edit = null;
        self.canSave = () => !self.ajaxing && self.edit != null && !compareOriginalToEdit(self.original, self.edit);
        self.save = () => tenantsSvc.saveTenant(self.edit);
        self.canUndo = () => !self.ajaxing && self.edit != null && !compareOriginalToEdit(self.original, self.edit);
        self.undo = () => self.edit = angular.copy(self.original);
        self.canRefresh = () => !self.ajaxing && self.edit != null;
        self.refresh = () => tenantsSvc.getTenantByHostname($stateParams.hostname);

        self.canEnable = () => !self.ajaxing && self.edit != null && self.edit.cluster == null;
        self.enable = () => tenantsSvc.enableTenant(self.edit.hostname, true);
        self.canDisable = () => !self.ajaxing && self.edit != null && self.edit.cluster != null;
        self.disable = () => tenantsSvc.enableTenant(self.edit.hostname, false);

        self.canClone = () => !self.ajaxing && self.edit != null;
        self.clone = () => {
            $mdDialog.show($mdDialog.cloneTenantDialog({
                locals: {
                    source: self.original.sql
                }
            })).then(details => {
                tenantsSvc.cloneTenant(details);
            })
        }

        // Dont allow Delete if the user has changed anything without saving/undoing
        self.canDelete = () => !self.canUndo() && !!self.original?.sql?.tenantProfile?.canDelete;
        self.delete = () => {
            $mdDialog.show($mdDialog.confirm()
                .title(`Are you sure you want to delete tenant ${self.original.hostname} ?`)
                .ok('Delete')
                .cancel('Cancel')
            ).then(() => {
                tenantsSvc.deleteTenant(self.original.hostname).then(() => {
                    $state.go(`root.tenants`);
                });
            });
        }
    }]
}))

