'use strict';

import { Subscription } from 'rxjs';

export function ngJsonStringify(name, val) {
    if (name.startsWith('$$')) {
        return undefined;
    }
    return val;
}

export function compareOriginalToEdit(original, edit) {
    let o = JSON.stringify(original, ngJsonStringify);
    let e = JSON.stringify(edit, ngJsonStringify);
    return o === e;
}

export function createBladeCtrl(args) {
    let ctrl = Object.assign({}, args);

    ctrl.controller = ['$injector', function ($injector) {
        const self = this;
        self.ajaxing = false;

        const subscription = new Subscription();
        self.addSubscription = s => {
            subscription.add(s);
        }
        self.$onDestroy = () => {
            subscription.unsubscribe();
        }

        self.watchLoading = dataSource => {
            self.addSubscription(dataSource.$loading.subscribe(r => self.ajaxing = r));
        }

        self.ajax = function (fn) {
            self.ajaxing = true;
            try {
                let r = fn();
                if (angular.isFunction(r.then)) {
                    return r.then(res => {
                        self.ajaxing = false;
                        return res;
                    }).catch(err => {
                        self.ajaxing = false;
                        return err;
                    });
                } else if (angular.isFunction(r.subscribe)) {
                    let s = r.subscribe(res => {
                        self.ajaxing = false;
                        s.unsubscribe();
                    });
                }
            } catch (err) {
                self.ajaxing = false;
                throw err;
            }
        }

        return $injector.invoke(args.controller, this);
    }]

    return ctrl;
}
