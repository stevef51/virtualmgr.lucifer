'use strict';

import app from '../../ngmodule';
import { tap, filter, map } from 'rxjs/operators';
import * as _ from 'lodash';
import { createBladeCtrl } from '../../blade';

app.component('luciferReleasesContentCtrl', createBladeCtrl({
    template: require('./template.html').default,
    controller: ['tenantsSvc', '$state', function (tenantsSvc, $state) {
        const self = this;
        self.order = 'name';
        self.search = '';

        self.filter = function (t) {
            let search = t.id;
            return search.toLowerCase().indexOf(self.search.toLowerCase()) >= 0;
        }

        let $releases = tenantsSvc.getLuciferReleases();
        self.watchLoading($releases);
        self.addSubscription($releases.$data.subscribe(r => self.releases = r));

        self.edit = luciferRelease => $state.go('root.luciferReleases.release', { luciferRelease: luciferRelease.id });

        self.canRefresh = () => !self.ajaxing;
        self.refresh = () => tenantsSvc.getLuciferReleases(true);
    }]
}));

