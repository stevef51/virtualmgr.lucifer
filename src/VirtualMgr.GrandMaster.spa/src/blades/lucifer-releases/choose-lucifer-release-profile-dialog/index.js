'use strict';

import app from '../../ngmodule';

app.config(function ($mdDialogProvider) {
    $mdDialogProvider.addPreset('chooseLuciferReleaseProfileDialog', {
        options: function () {
            return {
                parent: angular.element(document.body),

                template: require('./template.html').default,
                controller: ['$scope', '$mdDialog', 'tenantsSvc', function ($scope, $mdDialog, tenantsSvc) {
                    $scope.profileId = null;
                    $scope.$profiles = tenantsSvc.getLuciferReleaseProfiles().$data;
                    $scope.cancel = () => $mdDialog.cancel();
                    $scope.save = () => {
                        let profile = $scope.$profiles.getValue().find(p => p._id == $scope.profileId);
                        $mdDialog.hide(profile);
                    }
                }]
            }
        }
    })
})
