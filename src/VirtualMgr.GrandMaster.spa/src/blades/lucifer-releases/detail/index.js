'use strict';

import app from '../../ngmodule';
import './style.scss';
import { tap, filter } from 'rxjs/operators';
import { createBladeCtrl } from '../../blade';

app.component('luciferReleaseContentCtrl', createBladeCtrl({
    template: require('./template.html').default,
    controller: ['$stateParams', 'tenantsSvc', '$timeout', '$state', '$mdDialog', function ($stateParams, tenantsSvc, $timeout, $state, $mdDialog) {
        const self = this;

        self.$profiles = tenantsSvc.getLuciferReleaseProfiles(true).$data;
        let $luciferReleases = tenantsSvc.getLuciferRelease($stateParams.luciferRelease);
        self.watchLoading($luciferReleases);
        self.addSubscription($luciferReleases.$data.subscribe(r => {
            self.edit = angular.copy(r);
            self.original = angular.copy(r);
        }))

        self.edit = null;
        self.canSave = () => !self.ajaxing && (self.edit != null && JSON.stringify(self.edit) !== JSON.stringify(self.original));
        self.save = () => tenantsSvc.saveLuciferRelease(self.edit);

        self.canUndo = self.canSave;
        self.undo = () => self.edit = angular.copy(self.original);

        self.canDelete = () => !self.ajaxing && (self.edit != null && self.edit.tenantHostnames.length == 0);
        self.delete = () => tenantsSvc.deleteLuciferRelease(self.edit.id).then(success => {
            if (success) {
                $state.go(`root.luciferReleases`);
            }
        });

        self.changeProfile = () => {
            $mdDialog.show($mdDialog.chooseLuciferReleaseProfileDialog({}))
                .then(r => {
                    self.edit.profile = r;
                });
        }
    }]
}));

