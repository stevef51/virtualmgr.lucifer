'use strict';

import app from '../../ngmodule';

app.config(function ($stateProvider) {
    $stateProvider
        .state('root.luciferReleases', {
            url: '/lucifer-releases',
            views: {
                'blade1detail@root': {
                    component: 'luciferReleasesContentCtrl'
                }
            },
            data: {
                menuItem: {
                    title: `Releases`,
                    fontSet: 'fas',
                    fontIcon: 'fa-code-branch'
                },
                breadCrumb: () => 'Releases'
            }
        })

        .state('root.luciferReleases.release', {
            url: '/:luciferRelease',
            views: {
                'blade2detail@root': {
                    component: 'luciferReleaseContentCtrl'
                }
            },
            data: {
                breadCrumb: $stateParams => $stateParams.luciferRelease
            }
        })
});
