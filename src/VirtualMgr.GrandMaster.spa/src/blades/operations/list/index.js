'use strict';

import app from '../../ngmodule';
import { Subscription } from 'rxjs';
import { tap, filter, map } from 'rxjs/operators';
import * as _ from 'lodash';
import { createBladeCtrl } from '../../blade';
import * as moment from 'moment';

app.component('operationsListCtrl', createBladeCtrl({
    template: require('./template.html').default,
    controller: ['socketIo', '$timeout', function (socketIo, $timeout) {
        const self = this;
        self.search = '';
        self.onlyRecent = true;

        self.refresh = () => {
            socketIo.emit(`get-long-running-operations`, { onlyRecent: self.onlyRecent });
        }

        socketIo.on('connect', () => {
            socketIo.emit(`join-long-running-operations`);
            socketIo.on(`long-running-operations`, data => {
                $timeout(() => {
                    self.operations = data;
                });
            })
            self.refresh();
        });
        self.$onDestroy = () => {
            socketIo.emit(`leave-long-running-operations`);
        }
    }]
}))

