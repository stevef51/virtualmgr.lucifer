'use strict';

import app from '../../ngmodule';

app.config(function ($stateProvider) {
    $stateProvider
        .state('root.operations', {
            url: '/operations',
            views: {
                'blade1detail@root': {
                    component: 'operationsListCtrl'
                }
            },
            data: {
                menuItem: {
                    title: `Operations`,
                    fontSet: 'fas',
                    fontIcon: 'fa-brain'
                },
                breadCrumb: () => 'Operations'
            }
        })

        .state('root.operations.operation', {
            url: '/:hostname',
            views: {
                'blade2detail@root': {
                    component: 'operationDetailCtrl'
                }
            },
            data: {
                breadCrumb: $stateParams => $stateParams.hostname
            }
        })
});
