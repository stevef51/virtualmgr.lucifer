IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'makeDataSyncTable')
DROP PROCEDURE makeDataSyncTable
GO

CREATE PROCEDURE [dbo].[MakeDataSyncTable]
	@table NVARCHAR(255),			-- Name of table to make a DataSync table for
	@createTriggers BIT	= 1,					-- if TRUE will drop the synching triggers
	@dropSyncTable BIT = 0
AS
BEGIN
	-- 
	DECLARE @tableDataSync NVARCHAR(MAX)
	SELECT @tableDataSync = @table + N'_DataSync'
	
	DECLARE 
		@rowIdField NVARCHAR(MAX), 
		@actionField NVARCHAR(MAX), 
		@instanceIdField NVARCHAR(MAX), 
		@lastChangedField NVARCHAR(MAX)
	SELECT 
		@rowIdField = N'_DataSync_RowId', 
		@actionField = N'_DataSync_Action', 
		@instanceIdField = N'_DataSync_InstanceId', 
		@lastChangedField = N'_DataSync_LastChanged' 

	DECLARE @masterInstanceId NVARCHAR(MAX)
	SELECT @masterInstanceId = '''' + CONVERT(NVARCHAR(36), [InstanceId]) + '''' FROM _DataSyncMaster
	
	DECLARE @c NVARCHAR(MAX)
	DECLARE @pk NVARCHAR(MAX), @pkJoinOn NVARCHAR(MAX), @pkIsNotNull NVARCHAR(MAX), @pkIsNull NVARCHAR(MAX)
	
	SELECT 
		@pk = COALESCE(@pk + ', ', '') + '@0' + QUOTENAME(column_name),
		@pkJoinOn = COALESCE(@pkJoinOn + ' AND ', '') + '@0.' + QUOTENAME(column_name) + ' = @1.' + QUOTENAME(column_name),
		@pkIsNotNull = COALESCE(@pkIsNotNull + ' AND ', '') + '@0.' + QUOTENAME(column_name) + ' IS NOT NULL',
		@pkIsNull = COALESCE(@pkIsNull + ' AND ', '') + '@0.' + QUOTENAME(column_name) + ' IS NULL'
		FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
		WHERE OBJECTPROPERTY(OBJECT_ID(constraint_name), 'IsPrimaryKey') = 1	
		AND table_name = @table
				
	IF @dropSyncTable = 1 AND EXISTS (SELECT table_name FROM information_schema.tables WHERE table_name = @tableDataSync)
	BEGIN
		SELECT @c = 'DROP TABLE ' + @tableDataSync
		PRINT @c
		EXEC sp_executesql @c
	END
		
	-- Create the _DataSync table if it doesnt already exist.  
	-- ** Any changes to the PrimaryKey of the source table and we should drop this table manually **
	IF NOT EXISTS (SELECT table_name FROM information_schema.tables WHERE table_name = @tableDataSync)
	BEGIN
		
		DECLARE @pkType NVARCHAR(MAX), @pkConstraint NVARCHAR(MAX), @pkConstraintDef NVARCHAR(MAX)
		SELECT @pkType = COALESCE(@pkType + ', ', '') + '[' + c.column_name + '] ' + c.data_type + CASE WHEN c.character_maximum_length IS NULL THEN '' ELSE ' (' + CONVERT(NVARCHAR, c.character_maximum_length) + ')' END,
			@pkConstraint = COALESCE(@pkConstraint + '_', '') + c.column_name,
			@pkConstraintDef = COALESCE(@pkConstraintDef + ', ', '') + '[' + c.column_name + '] ASC'
			FROM information_schema.columns c
			INNER JOIN information_schema.key_column_usage kcu
			ON c.table_name = kcu.table_name AND c.column_name = kcu.column_name
			WHERE c.table_name = @table AND OBJECTPROPERTY(OBJECT_ID(kcu.constraint_name), 'IsPrimaryKey') = 1
		
		SELECT @c = 'CREATE TABLE dbo.' + @tableDataSync + ' 
		(
		    ' + @rowIdField + ' BIGINT IDENTITY NOT NULL
			CONSTRAINT [PK_' + @tableDataSync + '_RowId] PRIMARY KEY CLUSTERED (' + @rowIdField + '),
			' + @lastChangedField + ' ROWVERSION,
			' + @pkType + ',
			' + @actionField + ' INT NOT NULL,
			' + @instanceIdField + ' UNIQUEIDENTIFIER NOT NULL
		)'
			
		PRINT 'CREATE TABLE dbo.' + @tableDataSync
		EXEC sp_executesql @c
		
		SELECT @c = 'CREATE INDEX IX_' + @tableDataSync + '_' + @pkConstraint + ' ON dbo.' + @tableDataSync + ' (' + @pkConstraintDef + ')'
--		PRINT @c
		EXEC sp_executesql @c
		
		SELECT @c = 'CREATE INDEX IX_' + @tableDataSync + '_' + @actionField + ' ON dbo.' + @tableDataSync + ' (' + @actionField + ')'
--		PRINT @c
		EXEC sp_executesql @c

		SELECT @c = 'CREATE INDEX IX_' + @tableDataSync + '_' + @lastChangedField + ' ON dbo.' + @tableDataSync + ' (' + @lastChangedField + ')'
--		PRINT @c
		EXEC sp_executesql @c

		SELECT @c = 'CREATE INDEX IX_' + @tableDataSync + '_' + @instanceIdField + ' ON dbo.' + @tableDataSync + ' (' + @instanceIdField + ')'
--		PRINT @c
		EXEC sp_executesql @c
		
	END
	ELSE
		PRINT '-- ' + @table + '_DataSync already exists '

	-- Any data already in the source table needs to be synched with current date into the _DataSync table ..
	SET @c = ('
		INSERT INTO dbo.' + @tableDataSync + ' (' + REPLACE(@pk, '@0', '') + ', ' + @actionField + ', ' + @instanceIdField + ') SELECT ' + REPLACE(@pk, '@0', 'td.') + ', 0, ' + @masterInstanceId + ' FROM ' + @table + ' td LEFT JOIN ' + @tableDataSync + ' tds
		ON ' + REPLACE(REPLACE(@pkJoinOn, '@0', 'td'), '@1', 'tds') + ' WHERE tds.' + @actionField + ' IS NULL')
	
--	PRINT @c
	EXEC sp_executesql @c



	-- Delete existing triggers so we can recreate them ..
	SELECT @c = '
	IF EXISTS (select * from sys.triggers where name = ''trg' + @table + '_Insert_DataSync'')
	BEGIN
		DROP TRIGGER dbo.trg' + @table + '_Insert_DataSync
	END
	
	IF EXISTS (select * from sys.triggers where name = ''trg' + @table + '_Update_DataSync'')
	BEGIN
		DROP TRIGGER dbo.trg' + @table + '_Update_DataSync
	END
	
	IF EXISTS (select * from sys.triggers where name = ''trg' + @table + '_Delete_DataSync'')
	BEGIN
		DROP TRIGGER dbo.trg' + @table + '_Delete_DataSync
	END
	'
	
--	PRINT @c
	EXEC sp_executesql @c
	
	IF @createTriggers = 1
	BEGIN
		SELECT @c = '
		CREATE TRIGGER dbo.trg' + @table + '_Insert_DataSync ON dbo.' + @table + ' AFTER INSERT
		AS
		BEGIN
			SET NOCOUNT ON
			
			DECLARE @InstanceID UNIQUEIDENTIFIER

			IF OBJECT_ID(''tempdb..#__DataSync_CtxInstanceId'') IS NOT NULL
				SELECT @InstanceID = InstanceID FROM #__DataSync_CtxInstanceId
			ELSE
				SELECT @InstanceID = [InstanceID] FROM _DataSyncMaster

			MERGE INTO dbo.' + @tableDataSync + ' AS dst USING (SELECT ' + REPLACE(@pk, '@0', 'i.') + ' FROM inserted i) AS src (' + REPLACE(@pk, '@0', '') + ')
			ON ' + REPLACE(REPLACE(@pkJoinOn, '@0', 'dst'), '@1', 'src') + '
			WHEN MATCHED
			  THEN UPDATE SET ' + @actionField + ' = 0, ' + @instanceIdField + ' = @InstanceID
			WHEN NOT MATCHED 
			  THEN INSERT (' + REPLACE(@pk, '@0', '') + ', ' + @actionField + ', ' + @instanceIdField + ') VALUES (' + REPLACE(@pk, '@0', '') + ', 0, @InstanceId);
		END'
			
--		PRINT @c
		EXEC sp_executesql @c
		
		SELECT @c = '
		CREATE TRIGGER dbo.trg' + @table + '_Update_DataSync ON dbo.' + @table + ' AFTER UPDATE
		AS
		BEGIN
			SET NOCOUNT ON

			DECLARE @InstanceID UNIQUEIDENTIFIER

			IF OBJECT_ID(''tempdb..#__DataSync_CtxInstanceId'') IS NOT NULL
				SELECT @InstanceID = InstanceID FROM #__DataSync_CtxInstanceId
			ELSE
				SELECT @InstanceID = [InstanceID] FROM _DataSyncMaster

			UPDATE dbo.' + @tableDataSync + ' SET ' + @actionField + ' = 2, ' + @instanceIdField + ' = @InstanceID FROM dbo.' + @tableDataSync + ' tds INNER JOIN deleted d ON ' + REPLACE(REPLACE(@pkJoinOn, '@0','tds'),'@1','d') + ' LEFT JOIN inserted i ON ' + REPLACE(REPLACE(@pkJoinOn, '@0', 'i'), '@1', 'd') + ' WHERE ' + REPLACE(@pkIsNull, '@0', 'i') + '

			MERGE INTO dbo.' + @tableDataSync + ' AS dst USING (SELECT ' + REPLACE(@pk, '@0', 'i.') + ' FROM inserted i INNER JOIN deleted d ON ' + REPLACE(REPLACE(@pkJoinOn, '@0', 'i'), '@1', 'd') + ') AS src (' + REPLACE(@pk, '@0', '') + ')
			ON ' + REPLACE(REPLACE(@pkJoinOn, '@0', 'dst'), '@1', 'src') + '
			WHEN MATCHED
			  THEN UPDATE SET ' + @actionField + ' = 1, ' + @instanceIdField + ' = @InstanceID
			WHEN NOT MATCHED 
			  THEN INSERT (' + REPLACE(@pk, '@0', '') + ', ' + @actionField + ', ' + @instanceIdField + ') VALUES (' + REPLACE(@pk, '@0', '') + ', 1, @InstanceId);

			UPDATE dbo.' + @tableDataSync + ' SET ' + @actionField + ' = 0, ' + @instanceIdField + ' = @InstanceID FROM dbo.' + @tableDataSync + ' tds INNER JOIN inserted i ON ' + REPLACE(REPLACE(@pkJoinOn, '@0','tds'),'@1','i') + ' LEFT JOIN deleted d ON ' + REPLACE(REPLACE(@pkJoinOn, '@0', 'i'), '@1', 'd') + ' WHERE ' + REPLACE(@pkIsNull, '@0', 'd') + '
		END'
	
--		PRINT @c
		EXEC sp_executesql @c
		
		SELECT @c = '
		CREATE TRIGGER dbo.trg' + @table + '_Delete_DataSync ON dbo.' + @table + ' AFTER DELETE
		AS
		BEGIN
			SET NOCOUNT ON

			DECLARE @InstanceID UNIQUEIDENTIFIER

			IF OBJECT_ID(''tempdb..#__DataSync_CtxInstanceId'') IS NOT NULL
				SELECT @InstanceID = InstanceID FROM #__DataSync_CtxInstanceId
			ELSE
				SELECT @InstanceID = [InstanceID] FROM _DataSyncMaster

			MERGE INTO dbo.' + @tableDataSync + ' AS dst USING (SELECT ' + REPLACE(@pk, '@0', 'd.') + ' FROM deleted d) AS src (' + REPLACE(@pk, '@0', '') + ')
			ON ' + REPLACE(REPLACE(@pkJoinOn, '@0', 'dst'), '@1', 'src') + '
			WHEN MATCHED
			  THEN UPDATE SET ' + @actionField + ' = 2, ' + @instanceIdField + ' = @InstanceID
			WHEN NOT MATCHED 
			  THEN INSERT (' + REPLACE(@pk, '@0', '') + ', ' + @actionField + ', ' + @instanceIdField + ') VALUES (' + REPLACE(@pk, '@0', '') + ', 2, @InstanceId);
		END'
		
--		PRINT @c
		EXEC sp_executesql @c
	END
END
