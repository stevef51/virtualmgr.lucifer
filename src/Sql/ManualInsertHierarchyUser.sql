CREATE PROC ManualInsertHierarchyBucket (
	@rid INT,							-- The RoleId
	@insertUserId UNIQUEIDENTIFIER,		-- The User to Insert
	@underBucketId INT,					-- The Bucket to Insert "Under"
	@newId INT OUTPUT				-- The resulting bucket id
) AS 

DECLARE @hid INT, @leftIndex INT, @rightIndex INT
SELECT @hid = HierarchyId, @leftIndex = LeftIndex, @rightIndex = RightIndex FROM tblHierarchyBucket WHERE Id = @underBucketId

IF @hid IS NOT NULL
BEGIN
	-- Display before ..
	SELECT * FROM tblHierarchyBucket hb LEFT JOIN tblUserData ud ON hb.UserId = ud.UserId
	WHERE HierarchyId = @hid

	-- Insert new bucket
	INSERT INTO tblHierarchyBucket 	
		(RoleId, UserId, HierarchyId, Name, ParentId, LeftIndex, RightIndex, IsPrimary) 
		VALUES 
		(@rid, @insertUserId, @hid, '', @underBucketId, @leftIndex + 1, @rightIndex + 1, 1)

	SELECT @newId = @@IDENTITY

	-- Modify the ParentId, LeftIndex, RightIndex of all affected records inside this hierarchy
	UPDATE tblHierarchyBucket SET ParentId = @newId WHERE ParentId = @underBucketId AND Id != @newId
	UPDATE tblHierarchyBucket SET LeftIndex = LeftIndex + 1 WHERE HierarchyId = @hid AND LeftIndex > @leftIndex AND Id != @newId
	UPDATE tblHierarchyBucket SET RightIndex = RightIndex + 1 WHERE HierarchyId = @hid AND RightIndex > @leftIndex AND Id != @newId
	UPDATE tblHierarchyBucket SET RightIndex = RightIndex + 1 WHERE HierarchyId = @hid AND RightIndex > @rightIndex AND Id != @newId

	-- Display after
	SELECT * FROM tblHierarchyBucket hb LEFT JOIN tblUserData ud ON hb.UserId = ud.UserId
	WHERE HierarchyId = @hid
END
GO

CREATE PROC ManualInsertHierarchyUser (
	@hid INT,			-- The HierarchyId 
	@rid INT,			-- The RoleId
	@insertUserId UNIQUEIDENTIFIER,		-- The User to Insert
	@underUserId UNIQUEIDENTIFIER,		-- The User to Insert "Under"
	@newId INT OUTPUT		-- The resulting bucket id
) AS

DECLARE @underBucketId INT
SELECT @underBucketId = Id FROM tblHierarchyBucket WHERE HierarchyId = @hid AND UserId = @underUserId

IF @underBucketId IS NOT NULL
BEGIN
	EXEC ManualInsertHierarchyBucket @rid, @insertUserId, @underBucketId, @newId OUTPUT
END
GO

CREATE PROC ManualDeleteHierarchyBucket (
	@hbid INT			-- The HierarchyBucketId 
) AS

DECLARE @hid INT, @parentId INT, @leftIndex INT, @rightIndex INT
SELECT @hid = HierarchyId, @parentId = ParentId, @leftIndex = LeftIndex, @rightIndex = RightIndex FROM tblHierarchyBucket WHERE Id = @hbid

IF @hid IS NOT NULL
BEGIN
	-- Display before ..
	SELECT * FROM tblHierarchyBucket hb LEFT JOIN tblUserData ud ON hb.UserId = ud.UserId
	WHERE HierarchyId = @hid

	-- Move its children to its parent
	UPDATE tblHierarchyBucket SET ParentId = @parentId WHERE ParentId = @hbid

	-- Delete the bucket
	DELETE FROM tblHierarchyBucket WHERE ID = @hbid

	-- Modify the LeftIndex, RightIndex of all affected records inside this hierarchy
	UPDATE tblHierarchyBucket SET LeftIndex = LeftIndex - 2 WHERE HierarchyId = @hid AND LeftIndex > @leftIndex 
	UPDATE tblHierarchyBucket SET RightIndex = RightIndex - 1 WHERE HierarchyId = @hid AND RightIndex > @leftIndex
	UPDATE tblHierarchyBucket SET RightIndex = RightIndex - 1 WHERE HierarchyId = @hid AND RightIndex > @rightIndex

	-- Display after
	SELECT * FROM tblHierarchyBucket hb LEFT JOIN tblUserData ud ON hb.UserId = ud.UserId
	WHERE HierarchyId = @hid
END


