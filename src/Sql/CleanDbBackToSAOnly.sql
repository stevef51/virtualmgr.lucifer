-- The following script will CLEAN OUT a PandP database back to blank
-- Deletes all but 'sa' user

BEGIN TRAN

DECLARE @saUserId UNIQUEIDENTIFIER

SELECT @saUserId = UserId FROM Users WHERE username = 'sa'

DELETE FROM tblUserLog WHERE UserId != @saUserId

-- Dont damage the Hierarchy, remove users from it though
UPDATE tblHierarchyBucket SET UserId = NULL WHERE UserId != @saUserId

DELETE FROM tblErrorLog WHERE UserId != @saUserId
DELETE FROM tbluserdata WHERE UserId != @saUserId
DELETE FROM UsersInRoles WHERE UserId != @saUserId
DELETE FROM Memberships WHERE UserId != @saUserId
DELETE FROM Users WHERE UserId != @saUserId

DELETE FROM tblMediaPageViewing 
DELETE FROM tblMediaViewing 

--ROLLBACK
--COMMIT