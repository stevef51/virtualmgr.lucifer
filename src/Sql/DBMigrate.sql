
BEGIN TRANSACTION

--Start by making the tables
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[_DataSyncMaster]') AND type in (N'U'))
 DROP TABLE [dbo].[_DataSyncMaster]

GO

CREATE TABLE [dbo].[_DataSyncMaster](
 [InstanceId] [uniqueidentifier] NOT NULL PRIMARY KEY CLUSTERED,
 [CreationDateUTC] [datetime] NOT NULL
)

INSERT INTO [dbo].[_DataSyncMaster] ([InstanceId], [CreationDateUTC])
VALUES (NEWID(), GETDATE())
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[_DataSyncSession]') AND type in (N'U'))
 DROP TABLE [dbo].[_DataSyncSession]
GO

CREATE TABLE [dbo].[_DataSyncSession](
 [SrcInstanceId] [uniqueidentifier] NOT NULL,
 [SyncDateUTC] [datetime] NOT NULL,
 [LastSequence] [bigint] NOT NULL
 CONSTRAINT [PK__DataSyncSession] PRIMARY KEY CLUSTERED 
(
 [SrcInstanceId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[_DataSyncSessionData]') AND type in (N'U'))
 DROP TABLE [dbo].[_DataSyncSessionData]
GO

CREATE TABLE [dbo].[_DataSyncSessionData](
 [Id] INT IDENTITY NOT NULL PRIMARY KEY CLUSTERED,
 [SessionId] [uniqueidentifier] NOT NULL,
 [Sequence] bigint NOT NULL,
 [SQL] [NTEXT] NOT NULL)
GO

CREATE INDEX [IX_DataSyncSessionData_SessionId] ON [_DataSyncSessionData] (SessionId)
GO

--Make the datasync trigger stored proc
IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'makeDataSyncTable')
DROP PROCEDURE makeDataSyncTable
GO

CREATE PROCEDURE [dbo].[MakeDataSyncTable]
 @table NVARCHAR(255),   -- Name of table to make a DataSync table for
 @createTriggers BIT = 1,     -- if TRUE will drop the synching triggers
 @dropSyncTable BIT = 0
AS
BEGIN
 -- 
 DECLARE @tableDataSync NVARCHAR(MAX)
 SELECT @tableDataSync = @table + N'_DataSync'

 PRINT 'Creating DataSync for ' + @table
 
 DECLARE 
  @rowIdField NVARCHAR(MAX), 
  @actionField NVARCHAR(MAX), 
  @instanceIdField NVARCHAR(MAX), 
  @lastChangedField NVARCHAR(MAX)
 SELECT 
  @rowIdField = N'_DataSync_RowId', 
  @actionField = N'_DataSync_Action', 
  @instanceIdField = N'_DataSync_InstanceId', 
  @lastChangedField = N'_DataSync_LastChanged' 

 DECLARE @masterInstanceId NVARCHAR(MAX)
 SELECT @masterInstanceId = '''' + CONVERT(NVARCHAR(36), [InstanceId]) + '''' FROM _DataSyncMaster
 
 DECLARE @c NVARCHAR(MAX)
 DECLARE @pk NVARCHAR(MAX), @pkJoinOn NVARCHAR(MAX), @pkIsNotNull NVARCHAR(MAX), @pkIsNull NVARCHAR(MAX)
 
 SELECT 
  @pk = COALESCE(@pk + ', ', '') + '@0' + QUOTENAME(column_name),
  @pkJoinOn = COALESCE(@pkJoinOn + ' AND ', '') + '@0.' + QUOTENAME(column_name) + ' = @1.' + QUOTENAME(column_name),
  @pkIsNotNull = COALESCE(@pkIsNotNull + ' AND ', '') + '@0.' + QUOTENAME(column_name) + ' IS NOT NULL',
  @pkIsNull = COALESCE(@pkIsNull + ' AND ', '') + '@0.' + QUOTENAME(column_name) + ' IS NULL'
  FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
  WHERE OBJECTPROPERTY(OBJECT_ID(constraint_name), 'IsPrimaryKey') = 1 
  AND table_name = @table
    
 IF @dropSyncTable = 1 AND EXISTS (SELECT table_name FROM information_schema.tables WHERE table_name = @tableDataSync)
 BEGIN
  SELECT @c = 'DROP TABLE ' + @tableDataSync
  --PRINT @c
  EXEC sp_executesql @c
 END
  
 -- Create the _DataSync table if it doesnt already exist.  
 -- ** Any changes to the PrimaryKey of the source table and we should drop this table manually **
 IF NOT EXISTS (SELECT table_name FROM information_schema.tables WHERE table_name = @tableDataSync)
 BEGIN
  
  DECLARE @pkType NVARCHAR(MAX), @pkConstraint NVARCHAR(MAX), @pkConstraintDef NVARCHAR(MAX)
  SELECT @pkType = COALESCE(@pkType + ', ', '') + '[' + c.column_name + '] ' + c.data_type + CASE WHEN c.character_maximum_length IS NULL THEN '' ELSE ' (' + CONVERT(NVARCHAR, c.character_maximum_length) + ')' END,
   @pkConstraint = COALESCE(@pkConstraint + '_', '') + c.column_name,
   @pkConstraintDef = COALESCE(@pkConstraintDef + ', ', '') + '[' + c.column_name + '] ASC'
   FROM information_schema.columns c
   INNER JOIN information_schema.key_column_usage kcu
   ON c.table_name = kcu.table_name AND c.column_name = kcu.column_name
   WHERE c.table_name = @table AND OBJECTPROPERTY(OBJECT_ID(kcu.constraint_name), 'IsPrimaryKey') = 1
  
  SELECT @c = 'CREATE TABLE dbo.' + @tableDataSync + ' 
  (
      ' + @rowIdField + ' BIGINT IDENTITY NOT NULL
   CONSTRAINT [PK_' + @tableDataSync + '_RowId] PRIMARY KEY CLUSTERED (' + @rowIdField + '),
   ' + @lastChangedField + ' ROWVERSION,
   ' + @pkType + ',
   ' + @actionField + ' INT NOT NULL,
   ' + @instanceIdField + ' UNIQUEIDENTIFIER NOT NULL
  )'
   
  --PRINT @c
  EXEC sp_executesql @c
  
  SELECT @c = 'CREATE INDEX IX_' + @tableDataSync + '_' + @pkConstraint + ' ON dbo.' + @tableDataSync + ' (' + @pkConstraintDef + ')'
  --PRINT @c
  EXEC sp_executesql @c
  
  SELECT @c = 'CREATE INDEX IX_' + @tableDataSync + '_' + @actionField + ' ON dbo.' + @tableDataSync + ' (' + @actionField + ')'
  --PRINT @c
  EXEC sp_executesql @c

  SELECT @c = 'CREATE INDEX IX_' + @tableDataSync + '_' + @lastChangedField + ' ON dbo.' + @tableDataSync + ' (' + @lastChangedField + ')'
  --PRINT @c
  EXEC sp_executesql @c

  SELECT @c = 'CREATE INDEX IX_' + @tableDataSync + '_' + @instanceIdField + ' ON dbo.' + @tableDataSync + ' (' + @instanceIdField + ')'
  --PRINT @c
  EXEC sp_executesql @c
  
 END
 ELSE
  PRINT '-- ' + @table + '_DataSync already exists '

 -- Any data already in the source table needs to be synched with current date into the _DataSync table ..
 SET @c = ('
  INSERT INTO dbo.' + @tableDataSync + ' (' + REPLACE(@pk, '@0', '') + ', ' + @actionField + ', ' + @instanceIdField + ') SELECT ' + REPLACE(@pk, '@0', 'td.') + ', 0, ' + @masterInstanceId + ' FROM ' + @table + ' td LEFT JOIN ' + @tableDataSync + ' tds
  ON ' + REPLACE(REPLACE(@pkJoinOn, '@0', 'td'), '@1', 'tds') + ' WHERE tds.' + @actionField + ' IS NULL')
 
 --PRINT @c
 EXEC sp_executesql @c



 -- Delete existing triggers so we can recreate them ..
 SELECT @c = '
 IF EXISTS (select * from sys.triggers where name = ''trg' + @table + '_Insert_DataSync'')
 BEGIN
  DROP TRIGGER dbo.trg' + @table + '_Insert_DataSync
 END
 
 IF EXISTS (select * from sys.triggers where name = ''trg' + @table + '_Update_DataSync'')
 BEGIN
  DROP TRIGGER dbo.trg' + @table + '_Update_DataSync
 END
 
 IF EXISTS (select * from sys.triggers where name = ''trg' + @table + '_Delete_DataSync'')
 BEGIN
  DROP TRIGGER dbo.trg' + @table + '_Delete_DataSync
 END
 '
 
 --PRINT @c
 EXEC sp_executesql @c
 
 IF @createTriggers = 1
 BEGIN
  SELECT @c = '
  CREATE TRIGGER dbo.trg' + @table + '_Insert_DataSync ON dbo.' + @table + ' AFTER INSERT
  AS
  BEGIN
   SET NOCOUNT ON
   
   DECLARE @InstanceID UNIQUEIDENTIFIER

   IF OBJECT_ID(''tempdb..#__DataSync_CtxInstanceId'') IS NOT NULL
    SELECT @InstanceID = InstanceID FROM #__DataSync_CtxInstanceId
   ELSE
    SELECT @InstanceID = [InstanceID] FROM _DataSyncMaster

   MERGE INTO dbo.' + @tableDataSync + ' AS dst USING (SELECT ' + REPLACE(@pk, '@0', 'i.') + ' FROM inserted i) AS src (' + REPLACE(@pk, '@0', '') + ')
   ON ' + REPLACE(REPLACE(@pkJoinOn, '@0', 'dst'), '@1', 'src') + '
   WHEN MATCHED
     THEN UPDATE SET ' + @actionField + ' = 0, ' + @instanceIdField + ' = @InstanceID
   WHEN NOT MATCHED 
     THEN INSERT (' + REPLACE(@pk, '@0', '') + ', ' + @actionField + ', ' + @instanceIdField + ') VALUES (' + REPLACE(@pk, '@0', '') + ', 0, @InstanceId);
  END'
   
  --PRINT @c
  EXEC sp_executesql @c
  
  SELECT @c = '
  CREATE TRIGGER dbo.trg' + @table + '_Update_DataSync ON dbo.' + @table + ' AFTER UPDATE
  AS
  BEGIN
   SET NOCOUNT ON

   DECLARE @InstanceID UNIQUEIDENTIFIER

   IF OBJECT_ID(''tempdb..#__DataSync_CtxInstanceId'') IS NOT NULL
    SELECT @InstanceID = InstanceID FROM #__DataSync_CtxInstanceId
   ELSE
    SELECT @InstanceID = [InstanceID] FROM _DataSyncMaster

   UPDATE dbo.' + @tableDataSync + ' SET ' + @actionField + ' = 2, ' + @instanceIdField + ' = @InstanceID FROM dbo.' + @tableDataSync + ' tds INNER JOIN deleted d ON ' + REPLACE(REPLACE(@pkJoinOn, '@0','tds'),'@1','d') + ' LEFT JOIN inserted i ON ' + REPLACE(REPLACE(@pkJoinOn, '@0', 'i'), '@1', 'd') + ' WHERE ' + REPLACE(@pkIsNull, '@0', 'i') + '

   MERGE INTO dbo.' + @tableDataSync + ' AS dst USING (SELECT ' + REPLACE(@pk, '@0', 'i.') + ' FROM inserted i INNER JOIN deleted d ON ' + REPLACE(REPLACE(@pkJoinOn, '@0', 'i'), '@1', 'd') + ') AS src (' + REPLACE(@pk, '@0', '') + ')
   ON ' + REPLACE(REPLACE(@pkJoinOn, '@0', 'dst'), '@1', 'src') + '
   WHEN MATCHED
     THEN UPDATE SET ' + @actionField + ' = 1, ' + @instanceIdField + ' = @InstanceID
   WHEN NOT MATCHED 
     THEN INSERT (' + REPLACE(@pk, '@0', '') + ', ' + @actionField + ', ' + @instanceIdField + ') VALUES (' + REPLACE(@pk, '@0', '') + ', 1, @InstanceId);

   UPDATE dbo.' + @tableDataSync + ' SET ' + @actionField + ' = 0, ' + @instanceIdField + ' = @InstanceID FROM dbo.' + @tableDataSync + ' tds INNER JOIN inserted i ON ' + REPLACE(REPLACE(@pkJoinOn, '@0','tds'),'@1','i') + ' LEFT JOIN deleted d ON ' + REPLACE(REPLACE(@pkJoinOn, '@0', 'i'), '@1', 'd') + ' WHERE ' + REPLACE(@pkIsNull, '@0', 'd') + '
  END'
 
  --PRINT @c
  EXEC sp_executesql @c
  
  SELECT @c = '
  CREATE TRIGGER dbo.trg' + @table + '_Delete_DataSync ON dbo.' + @table + ' AFTER DELETE
  AS
  BEGIN
   SET NOCOUNT ON

   DECLARE @InstanceID UNIQUEIDENTIFIER

   IF OBJECT_ID(''tempdb..#__DataSync_CtxInstanceId'') IS NOT NULL
    SELECT @InstanceID = InstanceID FROM #__DataSync_CtxInstanceId
   ELSE
    SELECT @InstanceID = [InstanceID] FROM _DataSyncMaster

   MERGE INTO dbo.' + @tableDataSync + ' AS dst USING (SELECT ' + REPLACE(@pk, '@0', 'd.') + ' FROM deleted d) AS src (' + REPLACE(@pk, '@0', '') + ')
   ON ' + REPLACE(REPLACE(@pkJoinOn, '@0', 'dst'), '@1', 'src') + '
   WHEN MATCHED
     THEN UPDATE SET ' + @actionField + ' = 2, ' + @instanceIdField + ' = @InstanceID
   WHEN NOT MATCHED 
     THEN INSERT (' + REPLACE(@pk, '@0', '') + ', ' + @actionField + ', ' + @instanceIdField + ') VALUES (' + REPLACE(@pk, '@0', '') + ', 2, @InstanceId);
  END'
  
  --PRINT @c
  EXEC sp_executesql @c
 END
END

GO

--Update working document status numbers
PRINT 'Updating working document status numbers'
--UPDATE tblWorkingDocumentStatus SET [Text] = 'Completed' WHERE Id = 2
INSERT INTO tblWorkingDocumentStatus VALUES (3, 'PendingCompletion')
GO

--UPDATE tblWorkingDocument SET [Status] = 3 WHERE [Status] = 2
GO
PRINT 'Done'

PRINT 'About to start altering tables'
--new tables for dashboard stuff
ALTER TABLE [tblPublishingGroup]
ADD [AllowUseAsDashboard] BIT NOT NULL DEFAULT 0

CREATE TABLE [tblUserTypeDashboard] (
    [id]                        INT IDENTITY (1, 1) NOT NULL,
    [PublishingGroupResourceId] INT NOT NULL,
    [UserTypeId]                INT NOT NULL,
	[PlayOffline]				BIT NOT NULL DEFAULT 0,
    PRIMARY KEY CLUSTERED ([id] ASC),
    FOREIGN KEY ([PublishingGroupResourceId]) REFERENCES [dbo].[tblPublishingGroupResource] ([Id]),
    FOREIGN KEY ([UserTypeId]) REFERENCES [dbo].[tblUserType] ([Id])
)

IF NOT EXISTS (
    SELECT * FROM sys.columns
    WHERE object_id = OBJECT_ID(N'[dbo].[tblCompletedPresentedFactValue]')
          AND name = 'SearchValue'
) BEGIN
 PRINT 'Adding SearchValue to schema'
    ALTER TABLE [tblCompletedPresentedFactValue]
    ADD [SearchValue] NVARCHAR(250) NULL
END

PRINT 'Table construction complete'
GO

--Migrate old dashboards to new dashboard tables
--First, work out what resources we need to publish
CREATE TABLE #TDashboardHolding (
 Id INT IDENTITY NOT NULL PRIMARY KEY CLUSTERED,
 UserTypeId INT NOT NULL,
 ResourceId INT NOT NULL
)

CREATE TABLE #TLabelUserTypeJoin (
 Id INT IDENTITY NOT NULL PRIMARY KEY CLUSTERED,
 UserTypeId INT NOT NULL,
 LabelId UNIQUEIDENTIFIER NOT NULL,
 LabelCount INT NOT NULL
)

INSERT INTO #TDashboardHolding
SELECT Id AS UserTypeId,  
CONVERT(INT, SUBSTRING(DefaultDashboard, CHARINDEX('=', DefaultDashboard) + 1, LEN(DefaultDashboard) - CHARINDEX('=', DefaultDashboard))) AS PublishingGroupResourceId  
FROM tblUserType
WHERE DefaultDashboard COLLATE Latin1_General_CI_AS LIKE '/Player/Play/Dashboard?id=%'

--Now publish all of those resources into a dashboard group
--We'll pick the top label count for each user type to be the label which can access this dashboard
INSERT INTO #TLabelUserTypeJoin
SELECT [tblUserType].Id AS UserTypeId, [tblLabel].Id AS LabelId, COUNT([tblLabelUser].LabelId) AS LabelCount
FROM [tblLabelUser]
INNER JOIN [tblUserData] ON
 [tblLabelUser].UserId = [tblUserData].UserId
INNER JOIN [tblUserType] ON
 [tblUserData].UserTypeId = [tblUserType].Id
INNER JOIN [tblLabel] ON
 [tblLabelUser].LabelId = [tblLabel].Id
GROUP BY [tblUserType].Id, [tblLabel].Id

--iterate for each user type beacuse I suck at sets
DECLARE ut_cursor CURSOR FOR
SELECT #TDashboardHolding.UserTypeId FROM #TDashboardHolding
FOR READ ONLY

DECLARE @utid INT
DECLARE @lblid UNIQUEIDENTIFIER
DECLARE @pgname NVARCHAR(200)
DECLARE @utname NVARCHAR(200)
DECLARE @pgid INT
DECLARE @resid INT
DECLARE @resname NVARCHAR(200)
DECLARE @pgrid INT

PRINT 'About to hit the cursors'

OPEN ut_cursor
FETCH NEXT FROM ut_cursor INTO @utid
WHILE @@FETCH_STATUS = 0
BEGIN
 --Work out what label to apply to this group
 SELECT TOP 1 @lblid = #TLabelUserTypeJoin.LabelId
 FROM #TLabelUserTypeJoin
 WHERE #TLabelUserTypeJoin.UserTypeId = @utid
 ORDER BY #TLabelUserTypeJoin.LabelCount DESC

 SELECT TOP 1 @utname = [tblUserType].Name
 FROM [tblUserType]
 WHERE [tblUserType].Id = @utid

 --We know the label and user type, make a new publishing group
 SET @pgname = 'Dashboard: ' + @utname
 INSERT INTO [tblPublishingGroup] (Name, AllowUseAsUserContext, [Description]) VALUES (
 @pgname, 0, '')
 SET @pgid = SCOPE_IDENTITY()

 --Let the right label into this group
 INSERT INTO [tblPublishingGroupActorLabel] (PublishingGroupId, LabelId, PublishingTypeId) VALUES
 (@pgid, @lblid, 1), (@pgid, @lblid, 2)

 --What resource are we interested in?
 SELECT TOP 1 @resid = #TDashboardHolding.ResourceId
 FROM #TDashboardHolding
 WHERE #TDashboardHolding.UserTypeId = @utid

 SELECT TOP 1 @resname = [tblResource].Name
 FROM [tblResource]
 WHERE [tblResource].Id = @resid
 
 --Put this resource into this group
 --INSERT INTO [tblPublishingGroupResource] (ResourceId, PublishingGroupId, Name, PopulateReportingData, AllowUseAsDashboard, UtcLastModified, [Description]) VALUES
 --(@resid, @pgid, @resname, 0, 1, GETDATE(), '')
 --SET @pgrid = SCOPE_IDENTITY()

 --INSERT INTO [tblPublishingGroupResourceData] (PublishingGroupResourceId, Data)
 --SELECT @pgrid AS PublishingGroupResourceId, [tblResourceData].Data
 --FROM [tblResourceData]
 --WHERE [tblResourceData].ResourceId = @resid
 
 --now bind the user type dashboard table to this published resource
 --INSERT INTO [tblUserTypeDashboard] (PublishingGroupResourceId, UserTypeId) VALUES
 --(@pgrid, @utid)

 FETCH NEXT FROM ut_cursor INTO @utid
END
CLOSE ut_cursor
DEALLOCATE ut_cursor

DROP TABLE #TDashboardHolding
DROP TABLE #TLabelUserTypeJoin
GO

PRINT 'Cursor thrashing complete. Making datasync now...'
--And make the correct tables syncable

DECLARE @createTrigger INT, @dropDataSync INT
SELECT @createTrigger = 1, @dropDataSync = 1

exec MakeDataSyncTable 'Memberships',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblBlocked', @createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblGlobalSetting',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblHierarchyBucket',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblHierarchyRole',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblHierarchyRoleUserType',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblLabel',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblLabelResource',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblLabelUser',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblMedia',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblMediaData',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblMimeType',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblPublishingGroup',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblPublishingGroupActor',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblPublishingGroupActorLabel',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblPublishingGroupActorType',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblPublishingGroupResource',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblPublishingGroupResourceData',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblResource',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblUserData',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblUserMedia',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblUserType',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblUserTypeDashboard',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblWorkingDocument',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblWorkingDocumentData',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'Users',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'UsersInRoles',@createTrigger,@dropDataSync

--For user context
exec MakeDataSyncTable 'tblCompletedLabelDimension',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblCompletedLabelWorkingDocumentDimension',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblCompletedLabelWorkingDocumentPresentedDimension',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblCompletedPresentedFactValue',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblCompletedPresentedGeoLocationFactValue',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblCompletedPresentedTypeDimension',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblCompletedPresentedUploadMediaFactValue',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblCompletedUserDimension',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblCompletedWorkingDocumentDimension',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblCompletedWorkingDocumentFact',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblCompletedWorkingDocumentPresentedFact',@createTrigger,@dropDataSync

exec MakeDataSyncTable 'tblUserTypeContextPublishedResource',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblUserContextData',@createTrigger,@dropDataSync

GO

Print 'Done.'

COMMIT TRANSACTION
GO