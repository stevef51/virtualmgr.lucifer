IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[_DataSyncMaster]') AND type in (N'U'))
	DROP TABLE [dbo].[_DataSyncMaster]

GO

CREATE TABLE [dbo].[_DataSyncMaster](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[InstanceId] [uniqueidentifier] NOT NULL,
	[CreationDateUTC] [datetime] NOT NULL,
 CONSTRAINT [PK__DataSyncMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
) 

INSERT INTO [dbo].[_DataSyncMaster] ([InstanceId], [CreationDateUTC])
VALUES (NEWID(), GETDATE())