IF NOT EXISTS (SELECT 1 FROM sys.tables WHERE name = 'tblArticleBool')
BEGIN
	CREATE TABLE [dbo].[tblArticleBool](
		[ArticleId] [uniqueidentifier] NOT NULL,
		[Name] [nvarchar](50) NOT NULL,
		[Index] [int] NOT NULL,
		[Value] [bit] NOT NULL,
	 CONSTRAINT [PK_tblArticleBool] PRIMARY KEY CLUSTERED 
	(
		[ArticleId] ASC,
		[Name] ASC,
		[Index] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
	)

	ALTER TABLE [dbo].[tblArticleBool]  WITH CHECK ADD  CONSTRAINT [FK_tblArticleBool_ArticleId] FOREIGN KEY([ArticleId])
	REFERENCES [dbo].[tblArticle] ([Id])
	ON DELETE CASCADE

	ALTER TABLE [dbo].[tblArticleBool] CHECK CONSTRAINT [FK_tblArticleBool_ArticleId]
END

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IX_tblArticleBool_Name' AND object_id = OBJECT_ID('tblArticleBool'))
BEGIN 
	CREATE INDEX IX_tblArticleBool_Name ON [tblArticleBool] ([Name] ASC)
END 

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IX_tblArticleBool_Index' AND object_id = OBJECT_ID('tblArticleBool'))
BEGIN 
	CREATE INDEX IX_tblArticleBool_Index ON [tblArticleBool] ([Index] ASC)
END 

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IX_tblArticleBool_Value' AND object_id = OBJECT_ID('tblArticleBool'))
BEGIN 
	CREATE INDEX IX_tblArticleBool_Value ON [tblArticleBool] ([Value] ASC)
END 



IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IX_tblArticleBigInt_Name' AND object_id = OBJECT_ID('tblArticleBigInt'))
BEGIN 
	CREATE INDEX IX_tblArticleBigInt_Name ON [tblArticleBigInt] ([Name] ASC)
END 

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IX_tblArticleBigInt_Index' AND object_id = OBJECT_ID('tblArticleBigInt'))
BEGIN 
	CREATE INDEX IX_tblArticleBigInt_Index ON [tblArticleBigInt] ([Index] ASC)
END 

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IX_tblArticleBigInt_Value' AND object_id = OBJECT_ID('tblArticleBigInt'))
BEGIN 
	CREATE INDEX IX_tblArticleBigInt_Value ON [tblArticleBigInt] ([Value] ASC)
END 




IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IX_tblArticleDateTime_Name' AND object_id = OBJECT_ID('tblArticleDateTime'))
BEGIN 
	CREATE INDEX IX_tblArticleDateTime_Name ON [tblArticleDateTime] ([Name] ASC)
END 

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IX_tblArticleDateTime_Index' AND object_id = OBJECT_ID('tblArticleDateTime'))
BEGIN 
	CREATE INDEX IX_tblArticleDateTime_Index ON [tblArticleDateTime] ([Index] ASC)
END 

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IX_tblArticleDateTime_Value' AND object_id = OBJECT_ID('tblArticleDateTime'))
BEGIN 
	CREATE INDEX IX_tblArticleDateTime_Value ON [tblArticleDateTime] ([Value] ASC)
END 



IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IX_tblArticleGuid_Name' AND object_id = OBJECT_ID('tblArticleGUID'))
BEGIN 
	CREATE INDEX IX_tblArticleGuid_Name ON [tblArticleGUID] ([Name] ASC)
END 

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IX_tblArticleGuid_Index' AND object_id = OBJECT_ID('tblArticleGUID'))
BEGIN 
	CREATE INDEX IX_tblArticleGuid_Index ON [tblArticleGUID] ([Index] ASC)
END 

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IX_tblArticleGuid_Value' AND object_id = OBJECT_ID('tblArticleGUID'))
BEGIN 
	CREATE INDEX IX_tblArticleGuid_Value ON [tblArticleGUID] ([Value] ASC)
END 



IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IX_tblArticleNumber_Name' AND object_id = OBJECT_ID('tblArticleNumber'))
BEGIN 
	CREATE INDEX IX_tblArticleNumber_Name ON [tblArticleNumber] ([Name] ASC)
END 

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IX_tblArticleNumber_Index' AND object_id = OBJECT_ID('tblArticleNumber'))
BEGIN 
	CREATE INDEX IX_tblArticleNumber_Index ON [tblArticleNumber] ([Index] ASC)
END 

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IX_tblArticleNumber_Value' AND object_id = OBJECT_ID('tblArticleNumber'))
BEGIN 
	CREATE INDEX IX_tblArticleNumber_Value ON [tblArticleNumber] ([Value] ASC)
END 


-- Strings dont need to be massive for Articles, if we keep them to 450 NVARCHAR then they are fully indexable (900 byte limit in a single index key)
IF EXISTS(select 1 from INFORMATION_SCHEMA.COLUMNS IC where TABLE_NAME = 'tblArticleString' and COLUMN_NAME = 'Value' AND CHARACTER_MAXIMUM_LENGTH != 450)
BEGIN
	IF EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IX_tblArticleString_Value' AND object_id = OBJECT_ID('tblArticleString'))
	BEGIN 
		DROP INDEX IX_tblArticleString_Value ON tblArticleString
	END

	ALTER TABLE tblArticleString DROP COLUMN Value
END

IF NOT EXISTS(select 1 from INFORMATION_SCHEMA.COLUMNS IC where TABLE_NAME = 'tblArticleString' and COLUMN_NAME = 'Value')
BEGIN
	ALTER TABLE tblArticleString ADD Value NVARCHAR(450)
END

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IX_tblArticleString_Name' AND object_id = OBJECT_ID('tblArticleString'))
BEGIN 
	CREATE INDEX IX_tblArticleString_Name ON [tblArticleString] ([Name] ASC)
END 

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IX_tblArticleString_Index' AND object_id = OBJECT_ID('tblArticleString'))
BEGIN 
	CREATE INDEX IX_tblArticleString_Index ON [tblArticleString] ([Index] ASC)
END 

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IX_tblArticleString_Value' AND object_id = OBJECT_ID('tblArticleString'))
BEGIN 
	CREATE INDEX IX_tblArticleString_Value ON [tblArticleString] ([Value] ASC)
END 


IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IX_tblArticleSubArticle_Name' AND object_id = OBJECT_ID('tblArticleSubArticle'))
BEGIN 
	CREATE INDEX IX_tblArticleSubArticle_Name ON [tblArticleSubArticle] ([Name] ASC)
END 

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IX_tblArticleSubArticle_Index' AND object_id = OBJECT_ID('tblArticleSubArticle'))
BEGIN 
	CREATE INDEX IX_tblArticleSubArticle_Index ON [tblArticleSubArticle] ([Index] ASC)
END 

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IX_tblArticleSubArticle_SubArticleId' AND object_id = OBJECT_ID('tblArticleSubArticle'))
BEGIN 
	CREATE INDEX IX_tblArticleSubArticle_SubArticleId ON [tblArticleSubArticle] ([SubArticleId] ASC)
END 


