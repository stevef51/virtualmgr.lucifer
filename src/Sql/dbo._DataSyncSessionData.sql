IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[_DataSyncSessionData]') AND type in (N'U'))
	DROP TABLE [dbo].[_DataSyncSessionData]
GO

CREATE TABLE [dbo].[_DataSyncSessionData](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SessionId] [uniqueidentifier] NOT NULL,
	[Sequence] [bigint] NOT NULL,
	[SQL] [ntext] NOT NULL,
 CONSTRAINT [PK__DataSyncSessionData] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) TEXTIMAGE_ON [PRIMARY]

CREATE INDEX [IX_DataSyncSessionData_SessionId] ON [_DataSyncSessionData] (SessionId)
GO



