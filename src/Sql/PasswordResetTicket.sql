CREATE TABLE [dbo].[tblPasswordResetTicket] (
    [PublicTicketId] UNIQUEIDENTIFIER NOT NULL,
    [UserId]         UNIQUEIDENTIFIER NOT NULL,
    [DateCreated]    DATETIME         CONSTRAINT [DF_tblPasswordResetTicket_DateCreated] DEFAULT (getutcdate()) NOT NULL,
    [ExpiryDate]     DATETIME         NOT NULL,
    CONSTRAINT [PK_tblPasswordResetTicket] PRIMARY KEY CLUSTERED ([PublicTicketId] ASC),
    CONSTRAINT [FK_tblPasswordResetTicket_tblPublicTicket] FOREIGN KEY ([PublicTicketId]) REFERENCES [dbo].[tblPublicTicket] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_tblPasswordResetTicket_tblUserData] FOREIGN KEY ([UserId]) REFERENCES [dbo].[tblUserData] ([UserId]) ON DELETE CASCADE
)