DROP PROCEDURE dbo.UnitTest_DataSync
GO

CREATE PROCEDURE [dbo].[UnitTest_DataSync]
AS
BEGIN
	if exists (select 1 from information_schema.tables where table_name = 'UnitTest_TestData')
		DROP TABLE UnitTest_TestData
		
	if exists (select 1 from information_schema.tables where table_name = 'UnitTest_TestData_DataSync')
		DROP TABLE UnitTest_TestData_DataSync

	CREATE TABLE dbo.UnitTest_TestData
	(
		pk1 int,
		pk2 int,
		constraint UnitTest_TestData_pk1_pk2 primary key clustered (pk1 asc, pk2 asc),
		data nvarchar(255)
	)

	DECLARE @failed BIT
	SET @failed = 0
		
	INSERT INTO UnitTest_TestData (pk1, pk2, data) VALUES (1, 2, 'ABC')
	
	exec MakeDataSyncTable 'UnitTest_TestData'
	
	IF NOT EXISTS (SELECT * FROM UnitTest_TestData_DataSync WHERE pk1 = 1 AND pk2 = 2 AND _DataSync_Action = 0)
	BEGIN
		RAISERROR('1 - Predefined data is not auto inserted to DataSync table', 16, 1)
		SET @failed = 1
	END

	IF @failed = 0
	BEGIN
		INSERT INTO UnitTest_TestData (pk1, pk2, data) VALUES (2, 3, 'BCD')
	
		IF NOT EXISTS (SELECT * FROM UnitTest_TestData_DataSync WHERE pk1 = 2 AND pk2 = 3 AND _DataSync_Action = 0)
		BEGIN
			RAISERROR('2 - Insert Trigger failed', 16, 1)
			SET @failed = 1
		END
	END

	IF @failed = 0
	BEGIN
		UPDATE UnitTest_TestData SET data = 'EFG' WHERE pk1 = 2 AND pk2 = 3
	
		IF NOT EXISTS (SELECT * FROM UnitTest_TestData_DataSync WHERE pk1 = 2 AND pk2 = 3 AND _DataSync_Action = 1)
		BEGIN
			RAISERROR('3 - Update Trigger failed', 16, 1)
			SET @failed = 1
		END
	END

	IF @failed = 0
	BEGIN
		DELETE UnitTest_TestData WHERE pk1 = 2 AND pk2 = 3
	
		IF NOT EXISTS (SELECT * FROM UnitTest_TestData_DataSync WHERE pk1 = 2 AND pk2 = 3 AND _DataSync_Action = 2)
		BEGIN
			RAISERROR('4 - Delete Trigger failed', 16, 1)
			SET @failed = 1
		END
	END

	IF @failed = 0
	BEGIN
		UPDATE UnitTest_TestData SET pk1 = 4 WHERE pk1 = 1 AND pk2 = 2
	
		IF NOT EXISTS (SELECT * FROM UnitTest_TestData_DataSync WHERE pk1 = 4 AND pk2 = 2 AND _DataSync_Action = 0)
		BEGIN
			RAISERROR('4.a - Update Primary Key Trigger failed', 16, 1)
			SET @failed = 1
		END

		IF NOT EXISTS (SELECT * FROM UnitTest_TestData_DataSync WHERE pk1 = 1 AND pk2 = 2 AND _DataSync_Action = 2)
		BEGIN
			RAISERROR('4.b - Update Primary Key Trigger failed', 16, 1)
			SET @failed = 1
		END
	END

	SELECT * FROM UnitTest_TestData_DataSync ORDER BY _DataSync_Identity
	
	DROP TABLE UnitTest_TestData
	DROP TABLE UnitTest_TestData_DataSync

	SELECT @failed AS result
				
END
GO

exec UnitTest_DataSync