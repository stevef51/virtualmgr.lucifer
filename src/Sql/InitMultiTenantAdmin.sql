-- This script will create the MultiTenantAdmin role (if not already exists)
-- and also add the superuser to this role (if not already in the role)
--
-- DO NOT RUN THIS ON A TENANTS DATABASE !!
-- This should only be run on Master Tenant databases which is used to administer the real tenants
--
BEGIN TRAN

DECLARE @sa_id UNIQUEIDENTIFIER, @multiTenantAdminRoleId UNIQUEIDENTIFIER
SELECT @sa_id = 'd32503e1-b4b8-4b4c-9903-a5656ad5a71f'
SELECT @multiTenantAdminRoleId = '0C3B8EB2-4C9B-44FC-997E-5B86228E8304'

IF NOT EXISTS(SELECT 1 FROM dbo.Roles WHERE RoleId = @multiTenantAdminRoleId) 
BEGIN
	INSERT INTO dbo.Roles (ApplicationId, RoleId, RoleName, Description) VALUES ('C2A59FC5-96FD-4881-ABEB-9B753DB84D3C', @multiTenantAdminRoleId, 'MultiTenantAdmin', NULL)
	PRINT 'Created MultiTenantAdmin Role'
END

IF NOT EXISTS(SELECT 1 FROM dbo.UsersInRoles WHERE UserId = @sa_id AND RoleId = @multiTenantAdminRoleId)
BEGIN
	INSERT INTO dbo.UsersInRoles (UserId, RoleId) VALUES (@sa_id, @multiTenantAdminRoleId)
	PRINT 'Put SuperUser in MultiTenantAdmin Role'
END

--COMMIT
--ROLLBACK