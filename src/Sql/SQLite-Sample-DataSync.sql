CREATE TABLE t1
(
p1 int,
p2 int,
i1 int,
constraint t1_pk_p1_p2 primary key (p1 asc, p2 asc)
);

CREATE TABLE t1_DataSync
(
p1 int,
p2 int,
Action int,
ActionTimeStampUTC datetime,
constraint t1_DataSync_pk_p1_2 primary key (p1 asc, p2 asc)
);

CREATE TRIGGER trg_t1_Delete
AFTER DELETE ON t1  FOR EACH ROW
BEGIN
UPDATE t1_DataSync SET ActionTimeStampUTC=datetime('now','utc'), Action=3 WHERE p1=OLD.p1 AND p2=OLD.p2;
END;

CREATE TRIGGER trg_t1_Update
AFTER UPDATE ON t1 FOR EACH ROW
BEGIN
UPDATE t1_DataSync SET p1=NEW.p1, p2=NEW.p2, ActionTimeStampUTC=datetime('now','utc'), Action=2 WHERE p1=OLD.p1 AND p2=OLD.p2;
INSERT OR IGNORE INTO t1_DataSync (p1, p2, ActionTimeStampUTC, Action) VALUES (OLD.p1, OLD.p2, datetime('now', 'utc'), 3);
END;

CREATE TRIGGER trg_t1_Insert
AFTER INSERT ON t1   FOR EACH ROW
BEGIN
INSERT INTO t1_DataSync (p1, p2, ActionTimeStampUTC, Action) VALUES (NEW.p1, NEW.p2, datetime('now', 'utc'), 1);
END;
