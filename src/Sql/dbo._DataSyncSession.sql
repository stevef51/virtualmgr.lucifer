IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[_DataSyncSession]') AND type in (N'U'))
	DROP TABLE [dbo].[_DataSyncSession]
GO

CREATE TABLE [dbo].[_DataSyncSession](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SrcInstanceId] [uniqueidentifier] NOT NULL,
	[SyncDateUTC] [datetime] NOT NULL,
	[LastSequence] [bigint] NOT NULL
 CONSTRAINT [PK__DataSyncSession] PRIMARY KEY CLUSTERED 
(
	[SrcInstanceId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) 
) 

GO


