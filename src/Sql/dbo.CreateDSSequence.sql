--IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].__DataSyncGlobalSequence') AND type in (N'SO'))
--	CREATE SEQUENCE dbo.__DataSyncGlobalSequence
--		AS bigint
--		START WITH 1000
--		INCREMENT BY 1
--		NO CYCLE
--		CACHE
--	GO

CREATE TABLE __DS_Sequence (
    SeqID INT IDENTITY(1,1) PRIMARY KEY,
    SeqVal VARCHAR(1)
)
GO

CREATE PROCEDURE NextDSSequence
AS
BEGIN
    DECLARE @NewSeqVal int
    SET NOCOUNT ON
    INSERT INTO __DS_Sequence (SeqVal) VALUES ('a')
    SET @NewSeqVal = scope_identity()
    DELETE FROM __DS_Sequence WITH (READPAST)
    RETURN @NewSeqVal
END