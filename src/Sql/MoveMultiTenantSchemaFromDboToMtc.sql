-- This will copy the MultiTenant tables over from [dbo] to [mtc] schema

merge into [mtc].[tblcurrency] tgt using (select * from [vmgr-central].[dbo].[tblCurrency]) as src
on (tgt.code collate SQL_Latin1_General_CP1_CI_AS = src.code collate SQL_Latin1_General_CP1_CI_AS)
when not matched then insert (code, description) values (src.code, src.description);

set identity_insert [mtc].[tblTenantHosts] on
merge into [mtc].[tblTenantHosts] tgt using (select * from [vmgr-central].[dbo].[tblTenantHosts]) as src
on (tgt.id = src.id)
when not matched then insert (id, name, location, sqlservername, connectionstringtemplate, webappname, webappresourcegroup) 
values (src.id, src.name, src.location, src.sqlservername, src.connectionstringtemplate, src.webappname, src.webappresourcegroup);
set identity_insert [mtc].[tblTenantHosts] off

set identity_insert [mtc].[tblTenantProfiles] on
merge into [mtc].[tblTenantProfiles] tgt using (select * from [vmgr-central].[dbo].[tblTenantProfiles]) as src
on (tgt.id = src.id)
when not matched then insert (id, name, description, candelete, emailwhitelistenabled, emailwhitelist, canclone, hidden, createalertmessage, canupdateschema, candisable) 
values (src.id, src.name, src.description, src.candelete, src.emailwhitelistenabled, src.emailwhitelist, src.canclone, src.hidden, src.createalertmessage, src.canupdateschema, src.candisable);
set identity_insert [mtc].[tblTenantProfiles] off

set identity_insert [mtc].[tblTenants] on
merge into [mtc].[tblTenants] tgt using (select * from [vmgr-central].[dbo].[tblTenants]) as src
on (tgt.id = src.id)
when not matched then insert (id, hostname, connectionstring, name, enabled, tenanthostid, tenantprofileid, invoiceidprefix, timezone, billingaddress, billingcompanyname, currency) 
values (src.id, src.hostname, src.connectionstring, src.name, src.enabled, src.tenanthostid, src.tenantprofileid, src.invoiceidprefix, src.timezone, src.billingaddress, src.billingcompanyname, src.currency);
set identity_insert [mtc].[tblTenants] off





