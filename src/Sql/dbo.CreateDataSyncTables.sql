DECLARE @createTrigger INT, @dropDataSync INT
SELECT @createTrigger = 1, @dropDataSync = 1

exec MakeDataSyncTable 'Memberships',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblBlocked', @createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblGlobalSetting',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblHierarchyBucket',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblHierarchyRole',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblHierarchyRoleUserType',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblLabel',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblLabelResource',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblLabelUser',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblMedia',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblMediaData',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblMimeType',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblPublishingGroup',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblPublishingGroupActor',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblPublishingGroupActorLabel',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblPublishingGroupActorType',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblPublishingGroupResource',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblPublishingGroupResourceData',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblResource',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblUserData',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblUserMedia',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblUserType',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblUserTypeDashboard',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblWorkingDocument',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblWorkingDocumentData',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'Users',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'UsersInRoles',@createTrigger,@dropDataSync

--For user context
exec MakeDataSyncTable 'tblCompletedLabelDimension',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblCompletedLabelWorkingDocumentDimension',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblCompletedLabelWorkingDocumentPresentedDimension',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblCompletedPresentedFactValue',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblCompletedPresentedGeoLocationFactValue',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblCompletedPresentedTypeDimension',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblCompletedPresentedUploadMediaFactValue',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblCompletedUserDimension',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblCompletedWorkingDocumentDimension',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblCompletedWorkingDocumentFact',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblCompletedWorkingDocumentPresentedFact',@createTrigger,@dropDataSync

exec MakeDataSyncTable 'tblUserTypeContextPublishedResource',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblUserContextData',@createTrigger,@dropDataSync

exec MakeDataSyncTable 'tblArticle',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblArticleBool',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblArticleBigInt',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblArticleDateTime',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblArticleGUID',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblArticleNumber',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblArticleString',@createTrigger,@dropDataSync
exec MakeDataSyncTable 'tblArticleSubArticle',@createTrigger,@dropDataSync


