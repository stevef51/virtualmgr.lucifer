-- This script will update the tblCompletedWorkingDocumentPresentedFact and tblCompletedPresentedTypeDimension
-- tables to use the 5 following TypeDimension records - for some reason (unbeknown yet) additional TypeDimension
-- records are being created
DECLARE @multiChoice UNIQUEIDENTIFIER, 
	@uploadMedia UNIQUEIDENTIFIER, 
	@freeText UNIQUEIDENTIFIER,
	@geocode UNIQUEIDENTIFIER, 
	@selectUser UNIQUEIDENTIFIER,
	@trimDate DATE
SELECT @multiChoice = '508EA63C-0F16-4D55-97FC-A1BE00EE6118',
	@uploadMedia = 'B3AA326B-105B-4A77-B56B-A1BE00EE6127',
	@freeText = '6FA0F1D8-555D-456D-9154-A1BE02060F5B',
	@geocode = 'CBC99C4E-C422-4207-B207-A1C003991B39',
	@selectUser = '14605F01-AC4D-4B00-84B5-A1C0042EE800',
	@trimDate = '2015-03-01'

BEGIN TRAN

UPDATE tblCompletedWorkingDocumentPresentedFact SET ReportingPresentedTypeId = @multiChoice 
	WHERE ReportingPresentedTypeId IN (SELECT Id FROM tblCompletedPresentedTypeDimension WHERE type = 'MultiChoice')

UPDATE tblCompletedWorkingDocumentPresentedFact SET ReportingPresentedTypeId = @uploadMedia 
	WHERE ReportingPresentedTypeId IN (SELECT Id FROM tblCompletedPresentedTypeDimension WHERE type = 'UploadMedia')

UPDATE tblCompletedWorkingDocumentPresentedFact SET ReportingPresentedTypeId = @freeText 
	WHERE ReportingPresentedTypeId IN (SELECT Id FROM tblCompletedPresentedTypeDimension WHERE type = 'FreeText')

UPDATE tblCompletedWorkingDocumentPresentedFact SET ReportingPresentedTypeId = @geocode 
	WHERE ReportingPresentedTypeId IN (SELECT Id FROM tblCompletedPresentedTypeDimension WHERE type = 'Geocode')

UPDATE tblCompletedWorkingDocumentPresentedFact SET ReportingPresentedTypeId = @selectUser 
	WHERE ReportingPresentedTypeId IN (SELECT Id FROM tblCompletedPresentedTypeDimension WHERE type = 'SelectUser')


DELETE FROM tblCompletedPresentedTypeDimension 
	WHERE ID NOT IN (@multiChoice, @uploadMedia, @freeText, @geocode, @selectUser)

--DELETE FROM tblCompletedWorkingDocumentFact WHERE DateCompleted < @trimDate AND IsUserContext = 0

UPDATE tblMedia SET WorkingDocumentId = '00000000-0000-0000-0000-000000000000' WHERE WorkingDocumentId IS NULL

DELETE FROM tblMediaData WHERE MediaId IN (SELECT ID FROM tblMedia WHERE DateCreated < @trimDate)
DELETE FROM tblMedia WHERE DateCreated < @trimDate


--ROLLBACK
--COMMIT
