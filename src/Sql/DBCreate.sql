/****** Object:  Table [dbo].[tblUserType]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblUserType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[DefaultDashboard] [nvarchar](255) NULL,
	[HasGeoCordinates] [bit] NOT NULL,
	[IsASite] [bit] NOT NULL,
 CONSTRAINT [PK_tblMembershipType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[tblUserData]    Script Date: 03/03/2013 11:18:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblUserData](
	[UserId] [uniqueidentifier] NOT NULL,
	[UserTypeId] [int] NOT NULL,
	[Name] [nvarchar](200) NULL,
	[AuthorizationProviderKey] [nvarchar](100) NULL,
	[TimeZone] [nvarchar](50) NOT NULL,
	[Latitude] [decimal](18, 8) NULL,
	[Longitude] [decimal](18, 8) NULL,
	[ExpiryDate] [datetime] NULL,
 CONSTRAINT [PK_tblMembership_1] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[tblCalendarEvent]    Script Date: 03/03/2013 11:18:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblCalendarEvent](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[Title] [nvarchar](400) NOT NULL,
	[Description] [nvarchar](4000) NULL,
 CONSTRAINT [PK_tblCalendarEvent] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
CREATE NONCLUSTERED INDEX [IX_tblCalendarEvent] ON [dbo].[tblCalendarEvent] 
(
	[UserId] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF)
GO
/****** Object:  Table [dbo].[tblCalendarRule]    Script Date: 03/03/2013 11:18:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblCalendarRule](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CalendarEventId] [int] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NULL,
	[Frequency] [varchar](8) NOT NULL,
 CONSTRAINT [PK_tblCalendarRule] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[tblUserLog]    Script Date: 03/03/2013 11:18:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblUserLog](
	[Id] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[LogInTime] [datetime] NOT NULL,
	[LogOutTime] [datetime] NULL,
	[Latitude] [decimal](18, 8) NULL,
	[Longitude] [decimal](18, 8) NULL,
	[NearestLocation] [uniqueidentifier] NULL,
 CONSTRAINT [PK_tblUserLog] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[tblMessages]    Script Date: 03/03/2013 11:18:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblMessages](
	[Id] [uniqueidentifier] NOT NULL,
	[Text] [nvarchar](max) NOT NULL,
	[InReplyTo] [uniqueidentifier] NULL,
	[SentAt] [datetime] NOT NULL,
	[Sender] [uniqueidentifier] NOT NULL,
	[RequiresAck] [bit] NOT NULL,
 CONSTRAINT [PK_tblMessages] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[tblMailbox]    Script Date: 03/03/2013 11:18:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblMailbox](
	[Id] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[MessageId] [uniqueidentifier] NOT NULL,
	[IsRead] [bit] NULL,
 CONSTRAINT [PK_tblMailbox] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  StoredProcedure [dbo].[spGetClosestUsers]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spGetClosestUsers]
	-- Add the parameters for the stored procedure here
	@latitude as decimal(18,8),
	@longitude as decimal(18,8),
	@count as integer
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT top (@count) userid, latitude, longitude, SQRT(
    POWER(69.1 * (latitude - @latitude), 2) +
    POWER(69.1 * (@longitude - longitude) * COS(latitude / 57.3), 2)) * 1.609344 AS distance
	FROM tbluserdata where latitude is not null and longitude is not null order by distance
	
END
GO
/****** Object:  Table [dbo].[Applications]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Applications](
	[ApplicationName] [nvarchar](235) NOT NULL,
	[ApplicationId] [uniqueidentifier] NOT NULL,
	[Description] [nvarchar](256) NULL,
PRIMARY KEY CLUSTERED 
(
	[ApplicationId] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[Users]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[ApplicationId] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[IsAnonymous] [bit] NOT NULL,
	[LastActivityDate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[Profiles]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Profiles](
	[UserId] [uniqueidentifier] NOT NULL,
	[PropertyNames] [nvarchar](4000) NOT NULL,
	[PropertyValueStrings] [nvarchar](4000) NOT NULL,
	[PropertyValueBinary] [image] NOT NULL,
	[LastUpdatedDate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Trigger [InsertUser]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[InsertUser] 
   ON  [dbo].[Users] 
   AFTER INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @UserId uniqueidentifier 

	SELECT @UserId = (SELECT UserId FROM inserted)

    INSERT INTO tblUserData (UserId, Name) VALUES (@UserId, 'New Member')

END
GO
/****** Object:  Trigger [DeleteUser]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[DeleteUser] 
   ON  [dbo].[Users] 
   AFTER DELETE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	delete from tblUserData where UserId IN (select UserId from deleted)
END
GO
/****** Object:  Table [dbo].[tblMedia]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblMedia](
	[Id] [uniqueidentifier] NOT NULL,
	[Filename] [nvarchar](200) NOT NULL,
	[Type] [nvarchar](200) NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[Name] [nvarchar](200) NOT NULL,
	[Description] [nvarchar](4000) NULL,
	[WorkingDocumentId] [uniqueidentifier] NULL,
 CONSTRAINT [PK_tblMedia] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[tblPublishingGroup]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblPublishingGroup](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) NOT NULL,
	[AllowUseAsUserContext] [bit] NOT NULL,
	[Description] [nvarchar](200) NULL,
 CONSTRAINT [PK_tblPublished] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[tblResource]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblResource](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NodeId] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Type] [nvarchar](200) NOT NULL,
 CONSTRAINT [PK_tblResource] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tblResource] ON [dbo].[tblResource] 
(
	[NodeId] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF)
GO
/****** Object:  Table [dbo].[tblPublishingGroupResource]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblPublishingGroupResource](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ResourceId] [int] NOT NULL,
	[PublishingGroupId] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[IfNotFinishedCleanUpAfter] [int] NULL,
	[IfFinishedCleanUpAfter] [int] NULL,
	[PopulateReportingData] [bit] NOT NULL,
	[UtcLastModified] [datetime] NOT NULL,
	[PictureId] [uniqueidentifier] NULL,
	[Description] [nvarchar](200) NOT NULL,
 CONSTRAINT [PK_tblPublishingGroupResource] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tblPublishingGroupResource] ON [dbo].[tblPublishingGroupResource] 
(
	[PublishingGroupId] ASC,
	[ResourceId] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF)
GO
/****** Object:  Table [dbo].[tblUserAgent]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblUserAgent](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserAgent] [nvarchar](200) NOT NULL,
 CONSTRAINT [PK_tblUserAgent] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[tblWorkingDocumentStatus]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblWorkingDocumentStatus](
	[Id] [int] NOT NULL,
	[Text] [nvarchar](25) NOT NULL,
 CONSTRAINT [PK_tblWorkingDocumentStatus] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[tblWorkingDocument]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblWorkingDocument](
	[Id] [uniqueidentifier] NOT NULL,
	[PublishingGroupResourceId] [int] NOT NULL,
	[ReviewerId] [uniqueidentifier] NOT NULL,
	[RevieweeId] [uniqueidentifier] NULL,
	[Name] [nvarchar](50) NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[Lat] [decimal](18, 7) NULL,
	[Long] [decimal](18, 7) NULL,
	[ClientIP] [nvarchar](48) NULL,
	[UserAgentId] [int] NULL,
	[Status] [int] NOT NULL,
	[ParentId] [uniqueidentifier] NULL,
	[CleanUpIfNotFinishedAfter] [datetime] NULL,
	[CleanUpIfFinishedAfter] [datetime] NULL,
	[UtcLastModified] [datetime] NOT NULL,
 CONSTRAINT [PK_tblWorkingDocument] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[tblWorkingDocumentReference]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblWorkingDocumentReference](
	[Id] [uniqueidentifier] NOT NULL,
	[WorkingDocumentId] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](200) NOT NULL,
	[Url] [nvarchar](2000) NOT NULL,
 CONSTRAINT [PK_tblWorkingDocumentReference] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[tblWorkingDocumentPublicVariable]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblWorkingDocumentPublicVariable](
	[Id] [uniqueidentifier] NOT NULL,
	[WorkingDocumentId] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[Value] [nvarchar](255) NULL,
	[PassFail] [bit] NULL,
	[Score] [decimal](18, 2) NULL,
 CONSTRAINT [PK_tblWorkingDocumentPublicVariable] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
CREATE NONCLUSTERED INDEX [IX_tblWorkingDocumentPublicVariable] ON [dbo].[tblWorkingDocumentPublicVariable] 
(
	[WorkingDocumentId] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF)
GO
/****** Object:  Table [dbo].[tblWorkingDocumentData]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblWorkingDocumentData](
	[WorkingDocumentId] [uniqueidentifier] NOT NULL,
	[Data] [text] NOT NULL,
	[UtcLastModified] [datetime] NOT NULL,
 CONSTRAINT [PK_tblWorkingDocumentData] PRIMARY KEY CLUSTERED 
(
	[WorkingDocumentId] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  StoredProcedure [dbo].[spGetClosestSites]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spGetClosestSites]
	-- Add the parameters for the stored procedure here
	@latitude as decimal(18,8),
	@longitude as decimal(18,8),
	@count as integer
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT top (@count) userid, latitude, longitude, SQRT(
    POWER(69.1 * (latitude - @latitude), 2) +
    POWER(69.1 * (@longitude - longitude) * COS(latitude / 57.3), 2)) * 1.609344 AS distance
	FROM tbluserdata
	INNER JOIN tblUserType ON tblUserType.Id = tblUserData.UserTypeId
	WHERE latitude IS NOT NULL AND longitude IS NOT NULL AND tblUserType.IsASite = 1
	ORDER BY distance
END
GO
/****** Object:  Table [dbo].[tblWait]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblWait](
	[BlockedId] [uniqueidentifier] NOT NULL,
	[WaitingForId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_tblWait] PRIMARY KEY CLUSTERED 
(
	[BlockedId] ASC,
	[WaitingForId] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  StoredProcedure [dbo].[usp_delete_cascade]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[usp_delete_cascade] (
	@base_table_name varchar(200), @base_criteria nvarchar(1000)
)
as begin
	-- Adapted from http://www.sqlteam.com/article/performing-a-cascade-delete-in-sql-server-7
	-- Expects the name of a table, and a conditional for selecting rows
	-- within that table that you want deleted.
	-- Produces SQL that, when run, deletes all table rows referencing the ones
	-- you initially selected, cascading into any number of tables,
	-- without the need for "ON DELETE CASCADE".
	-- Does not appear to work with self-referencing tables, but it will
	-- delete everything beneath them.
	-- To make it easy on the server, put a "GO" statement between each line.

	declare @to_delete table (
		id int identity(1, 1) primary key not null,
		criteria nvarchar(1000) not null,
		table_name varchar(200) not null,
		processed bit not null,
		delete_sql varchar(1000)
	)

	insert into @to_delete (criteria, table_name, processed) values (@base_criteria, @base_table_name, 0)

	declare @id int, @criteria nvarchar(1000), @table_name varchar(200)
	while exists(select 1 from @to_delete where processed = 0) begin
		select top 1 @id = id, @criteria = criteria, @table_name = table_name from @to_delete where processed = 0 order by id desc

		insert into @to_delete (criteria, table_name, processed)
			select referencing_column.name + ' in (select [' + referenced_column.name + '] from [' + @table_name +'] where ' + @criteria + ')',
				referencing_table.name,
				0
			from  sys.foreign_key_columns fk
				inner join sys.columns referencing_column on fk.parent_object_id = referencing_column.object_id 
					and fk.parent_column_id = referencing_column.column_id 
				inner join  sys.columns referenced_column on fk.referenced_object_id = referenced_column.object_id 
					and fk.referenced_column_id = referenced_column.column_id 
				inner join  sys.objects referencing_table on fk.parent_object_id = referencing_table.object_id 
				inner join  sys.objects referenced_table on fk.referenced_object_id = referenced_table.object_id 
				inner join  sys.objects constraint_object on fk.constraint_object_id = constraint_object.object_id
			where referenced_table.name = @table_name
				and referencing_table.name != referenced_table.name

		update @to_delete set
			processed = 1
		where id = @id
	end

	select 'print ''deleting from ' + table_name + '...''; delete from [' + table_name + '] where ' + criteria from @to_delete order by id desc
end
GO
/****** Object:  Table [dbo].[tblPublicTicket]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblPublicTicket](
	[Id] [uniqueidentifier] NOT NULL,
	[Status] [int] NOT NULL,
 CONSTRAINT [PK_tblPublicTicket] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[tblPublicWorkingDocument]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblPublicWorkingDocument](
	[WorkingDocumentId] [uniqueidentifier] NOT NULL,
	[PublicTicketId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_tblPublicWorkingDocument] PRIMARY KEY CLUSTERED 
(
	[WorkingDocumentId] ASC,
	[PublicTicketId] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[tblPublishingGroupActorType]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblPublishingGroupActorType](
	[Id] [int] NOT NULL,
	[Name] [nvarchar](25) NOT NULL,
 CONSTRAINT [PK_tblPublishingGroupActorType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[tblPublishingGroupActor]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblPublishingGroupActor](
	[PublishingGroupId] [int] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[PublishingTypeId] [int] NOT NULL,
 CONSTRAINT [PK_tblPublishingGroupActor_1] PRIMARY KEY CLUSTERED 
(
	[PublishingGroupId] ASC,
	[UserId] ASC,
	[PublishingTypeId] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[tblUserMedia]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblUserMedia](
	[UserId] [uniqueidentifier] NOT NULL,
	[MediaId] [uniqueidentifier] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
 CONSTRAINT [PK_tblUserMedia] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[MediaId] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[tblPlugin]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblPlugin](
	[Id] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[Description] [nvarchar](4000) NULL,
	[DateCreated] [datetime] NOT NULL,
 CONSTRAINT [PK_tblPlugin] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tblPlugin] ON [dbo].[tblPlugin] 
(
	[Name] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF)
GO
/****** Object:  Table [dbo].[tblMimeType]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblMimeType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Extension] [nvarchar](10) NOT NULL,
	[MimeType] [nvarchar](255) NOT NULL,
	[MediaPreviewGenerator] [nvarchar](500) NULL,
 CONSTRAINT [PK_tblMimeType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF),
 CONSTRAINT [IX_tblMimeType] UNIQUE NONCLUSTERED 
(
	[Extension] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[tblHierarchyRole]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblHierarchyRole](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[IsPerson] [bit] NOT NULL,
 CONSTRAINT [PK_tblHierarchyRole] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[tblHierarchyBucket]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblHierarchyBucket](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoleId] [int] NOT NULL,
	[UserId] [uniqueidentifier] NULL,
	[Name] [varchar](50) NOT NULL,
	[ParentId] [int] NULL,
	[LeftIndex] [int] NOT NULL,
	[RightIndex] [int] NOT NULL,
	[IsPrimary] [bit] NULL,
 CONSTRAINT [PK_tblHierarchyBucket] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[tblCompletedWorkingDocumentDimension]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblCompletedWorkingDocumentDimension](
	[Id] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_tblReportingWorkingDocument] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
CREATE NONCLUSTERED INDEX [IX_tblReportingWorkingDocument] ON [dbo].[tblCompletedWorkingDocumentDimension] 
(
	[Name] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF)
GO
/****** Object:  Table [dbo].[tblCompletedUserDimension]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblCompletedUserDimension](
	[Id] [uniqueidentifier] NOT NULL,
	[Username] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](200) NOT NULL,
	[Email] [nvarchar](256) NULL,
	[TimeZone] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_tblReportingUser] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
CREATE NONCLUSTERED INDEX [IX_tblCompletedUserDimension] ON [dbo].[tblCompletedUserDimension] 
(
	[Username] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF)
GO
CREATE NONCLUSTERED INDEX [IX_tblReportingUser] ON [dbo].[tblCompletedUserDimension] 
(
	[Name] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF)
GO
/****** Object:  Table [dbo].[tblLabel]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblLabel](
	[Id] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Forecolor] [int] NOT NULL,
	[Backcolor] [int] NOT NULL,
	[ForUsers] [bit] NOT NULL,
	[ForMedia] [bit] NOT NULL,
	[ForResources] [bit] NOT NULL,
 CONSTRAINT [PK_tblLabel] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
CREATE NONCLUSTERED INDEX [IX_tblLabel] ON [dbo].[tblLabel] 
(
	[Name] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF)
GO
/****** Object:  Table [dbo].[tblLabelUser]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblLabelUser](
	[LabelId] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_tblLabelUser] PRIMARY KEY CLUSTERED 
(
	[LabelId] ASC,
	[UserId] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[tblGlobalSetting]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblGlobalSetting](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nchar](50) NOT NULL,
	[Value] [ntext] NULL,
 CONSTRAINT [PK_tblGlobalSetting] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF),
 CONSTRAINT [IX_tblGlobalSetting] UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[tblBlocked]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblBlocked](
	[BlockedById] [uniqueidentifier] NOT NULL,
	[BlockedId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_tblBlocked] PRIMARY KEY CLUSTERED 
(
	[BlockedById] ASC,
	[BlockedId] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[QRTZ_SCHEDULER_STATE]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QRTZ_SCHEDULER_STATE](
	[SCHED_NAME] [nvarchar](100) NOT NULL,
	[INSTANCE_NAME] [nvarchar](200) NOT NULL,
	[LAST_CHECKIN_TIME] [bigint] NOT NULL,
	[CHECKIN_INTERVAL] [bigint] NOT NULL,
 CONSTRAINT [PK_QRTZ_SCHEDULER_STATE] PRIMARY KEY CLUSTERED 
(
	[SCHED_NAME] ASC,
	[INSTANCE_NAME] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[QRTZ_PAUSED_TRIGGER_GRPS]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QRTZ_PAUSED_TRIGGER_GRPS](
	[SCHED_NAME] [nvarchar](100) NOT NULL,
	[TRIGGER_GROUP] [nvarchar](150) NOT NULL,
 CONSTRAINT [PK_QRTZ_PAUSED_TRIGGER_GRPS] PRIMARY KEY CLUSTERED 
(
	[SCHED_NAME] ASC,
	[TRIGGER_GROUP] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[QRTZ_LOCKS]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QRTZ_LOCKS](
	[SCHED_NAME] [nvarchar](100) NOT NULL,
	[LOCK_NAME] [nvarchar](40) NOT NULL,
 CONSTRAINT [PK_QRTZ_LOCKS] PRIMARY KEY CLUSTERED 
(
	[SCHED_NAME] ASC,
	[LOCK_NAME] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[QRTZ_JOB_DETAILS]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QRTZ_JOB_DETAILS](
	[SCHED_NAME] [nvarchar](100) NOT NULL,
	[JOB_NAME] [nvarchar](150) NOT NULL,
	[JOB_GROUP] [nvarchar](150) NOT NULL,
	[DESCRIPTION] [nvarchar](250) NULL,
	[JOB_CLASS_NAME] [nvarchar](250) NOT NULL,
	[IS_DURABLE] [bit] NOT NULL,
	[IS_NONCONCURRENT] [bit] NOT NULL,
	[IS_UPDATE_DATA] [bit] NOT NULL,
	[REQUESTS_RECOVERY] [bit] NOT NULL,
	[JOB_DATA] [image] NULL,
 CONSTRAINT [PK_QRTZ_JOB_DETAILS] PRIMARY KEY CLUSTERED 
(
	[SCHED_NAME] ASC,
	[JOB_NAME] ASC,
	[JOB_GROUP] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[QRTZ_FIRED_TRIGGERS]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QRTZ_FIRED_TRIGGERS](
	[SCHED_NAME] [nvarchar](100) NOT NULL,
	[ENTRY_ID] [nvarchar](95) NOT NULL,
	[TRIGGER_NAME] [nvarchar](150) NOT NULL,
	[TRIGGER_GROUP] [nvarchar](150) NOT NULL,
	[INSTANCE_NAME] [nvarchar](200) NOT NULL,
	[FIRED_TIME] [bigint] NOT NULL,
	[PRIORITY] [int] NOT NULL,
	[STATE] [nvarchar](16) NOT NULL,
	[JOB_NAME] [nvarchar](150) NULL,
	[JOB_GROUP] [nvarchar](150) NULL,
	[IS_NONCONCURRENT] [bit] NULL,
	[REQUESTS_RECOVERY] [bit] NULL,
 CONSTRAINT [PK_QRTZ_FIRED_TRIGGERS] PRIMARY KEY CLUSTERED 
(
	[SCHED_NAME] ASC,
	[ENTRY_ID] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
CREATE NONCLUSTERED INDEX [IDX_QRTZ_FT_INST_JOB_REQ_RCVRY] ON [dbo].[QRTZ_FIRED_TRIGGERS] 
(
	[SCHED_NAME] ASC,
	[INSTANCE_NAME] ASC,
	[REQUESTS_RECOVERY] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF)
GO
CREATE NONCLUSTERED INDEX [IDX_QRTZ_FT_J_G] ON [dbo].[QRTZ_FIRED_TRIGGERS] 
(
	[SCHED_NAME] ASC,
	[JOB_NAME] ASC,
	[JOB_GROUP] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF)
GO
CREATE NONCLUSTERED INDEX [IDX_QRTZ_FT_JG] ON [dbo].[QRTZ_FIRED_TRIGGERS] 
(
	[SCHED_NAME] ASC,
	[JOB_GROUP] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF)
GO
CREATE NONCLUSTERED INDEX [IDX_QRTZ_FT_T_G] ON [dbo].[QRTZ_FIRED_TRIGGERS] 
(
	[SCHED_NAME] ASC,
	[TRIGGER_NAME] ASC,
	[TRIGGER_GROUP] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF)
GO
CREATE NONCLUSTERED INDEX [IDX_QRTZ_FT_TG] ON [dbo].[QRTZ_FIRED_TRIGGERS] 
(
	[SCHED_NAME] ASC,
	[TRIGGER_GROUP] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF)
GO
CREATE NONCLUSTERED INDEX [IDX_QRTZ_FT_TRIG_INST_NAME] ON [dbo].[QRTZ_FIRED_TRIGGERS] 
(
	[SCHED_NAME] ASC,
	[INSTANCE_NAME] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF)
GO
/****** Object:  Table [dbo].[tblCompletedLabelDimension]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblCompletedLabelDimension](
	[Id] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Forecolor] [int] NOT NULL,
	[Backcolor] [int] NOT NULL,
 CONSTRAINT [PK_tblReportingLabel] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
CREATE NONCLUSTERED INDEX [IX_tblReportingLabel] ON [dbo].[tblCompletedLabelDimension] 
(
	[Name] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF)
GO
/****** Object:  Table [dbo].[QRTZ_CALENDARS]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QRTZ_CALENDARS](
	[SCHED_NAME] [nvarchar](100) NOT NULL,
	[CALENDAR_NAME] [nvarchar](200) NOT NULL,
	[CALENDAR] [image] NOT NULL,
 CONSTRAINT [PK_QRTZ_CALENDARS] PRIMARY KEY CLUSTERED 
(
	[SCHED_NAME] ASC,
	[CALENDAR_NAME] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[QRTZ_BLOB_TRIGGERS]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QRTZ_BLOB_TRIGGERS](
	[SCHED_NAME] [nvarchar](100) NOT NULL,
	[TRIGGER_NAME] [nvarchar](150) NOT NULL,
	[TRIGGER_GROUP] [nvarchar](150) NOT NULL,
	[BLOB_DATA] [image] NULL
)
GO
/****** Object:  Table [dbo].[tblCompletedPresentedTypeDimension]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblCompletedPresentedTypeDimension](
	[Id] [uniqueidentifier] NOT NULL,
	[Type] [nvarchar](200) NOT NULL,
 CONSTRAINT [PK_tblCompletedPresentedDimension] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
CREATE NONCLUSTERED INDEX [IX_tblCompletedPresentedDimension] ON [dbo].[tblCompletedPresentedTypeDimension] 
(
	[Type] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF)
GO
/****** Object:  Table [dbo].[Memberships]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Memberships](
	[ApplicationId] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[Password] [nvarchar](128) NOT NULL,
	[PasswordFormat] [int] NOT NULL,
	[PasswordSalt] [nvarchar](128) NOT NULL,
	[Email] [nvarchar](256) NULL,
	[PasswordQuestion] [nvarchar](256) NULL,
	[PasswordAnswer] [nvarchar](128) NULL,
	[IsApproved] [bit] NOT NULL,
	[IsLockedOut] [bit] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[LastLoginDate] [datetime] NOT NULL,
	[LastPasswordChangedDate] [datetime] NOT NULL,
	[LastLockoutDate] [datetime] NOT NULL,
	[FailedPasswordAttemptCount] [int] NOT NULL,
	[FailedPasswordAttemptWindowStart] [datetime] NOT NULL,
	[FailedPasswordAnswerAttemptCount] [int] NOT NULL,
	[FailedPasswordAnswerAttemptWindowsStart] [datetime] NOT NULL,
	[Comment] [nvarchar](256) NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[Roles]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Roles](
	[ApplicationId] [uniqueidentifier] NOT NULL,
	[RoleId] [uniqueidentifier] NOT NULL,
	[RoleName] [nvarchar](256) NOT NULL,
	[Description] [nvarchar](256) NULL,
PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[UsersInRoles]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UsersInRoles](
	[UserId] [uniqueidentifier] NOT NULL,
	[RoleId] [uniqueidentifier] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[QRTZ_TRIGGERS]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QRTZ_TRIGGERS](
	[SCHED_NAME] [nvarchar](100) NOT NULL,
	[TRIGGER_NAME] [nvarchar](150) NOT NULL,
	[TRIGGER_GROUP] [nvarchar](150) NOT NULL,
	[JOB_NAME] [nvarchar](150) NOT NULL,
	[JOB_GROUP] [nvarchar](150) NOT NULL,
	[DESCRIPTION] [nvarchar](250) NULL,
	[NEXT_FIRE_TIME] [bigint] NULL,
	[PREV_FIRE_TIME] [bigint] NULL,
	[PRIORITY] [int] NULL,
	[TRIGGER_STATE] [nvarchar](16) NOT NULL,
	[TRIGGER_TYPE] [nvarchar](8) NOT NULL,
	[START_TIME] [bigint] NOT NULL,
	[END_TIME] [bigint] NULL,
	[CALENDAR_NAME] [nvarchar](200) NULL,
	[MISFIRE_INSTR] [int] NULL,
	[JOB_DATA] [image] NULL,
 CONSTRAINT [PK_QRTZ_TRIGGERS] PRIMARY KEY CLUSTERED 
(
	[SCHED_NAME] ASC,
	[TRIGGER_NAME] ASC,
	[TRIGGER_GROUP] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
CREATE NONCLUSTERED INDEX [IDX_QRTZ_T_C] ON [dbo].[QRTZ_TRIGGERS] 
(
	[SCHED_NAME] ASC,
	[CALENDAR_NAME] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF)
GO
CREATE NONCLUSTERED INDEX [IDX_QRTZ_T_G] ON [dbo].[QRTZ_TRIGGERS] 
(
	[SCHED_NAME] ASC,
	[TRIGGER_GROUP] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF)
GO
CREATE NONCLUSTERED INDEX [IDX_QRTZ_T_J] ON [dbo].[QRTZ_TRIGGERS] 
(
	[SCHED_NAME] ASC,
	[JOB_NAME] ASC,
	[JOB_GROUP] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF)
GO
CREATE NONCLUSTERED INDEX [IDX_QRTZ_T_JG] ON [dbo].[QRTZ_TRIGGERS] 
(
	[SCHED_NAME] ASC,
	[JOB_GROUP] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF)
GO
CREATE NONCLUSTERED INDEX [IDX_QRTZ_T_N_G_STATE] ON [dbo].[QRTZ_TRIGGERS] 
(
	[SCHED_NAME] ASC,
	[TRIGGER_GROUP] ASC,
	[TRIGGER_STATE] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF)
GO
CREATE NONCLUSTERED INDEX [IDX_QRTZ_T_N_STATE] ON [dbo].[QRTZ_TRIGGERS] 
(
	[SCHED_NAME] ASC,
	[TRIGGER_NAME] ASC,
	[TRIGGER_GROUP] ASC,
	[TRIGGER_STATE] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF)
GO
CREATE NONCLUSTERED INDEX [IDX_QRTZ_T_NEXT_FIRE_TIME] ON [dbo].[QRTZ_TRIGGERS] 
(
	[SCHED_NAME] ASC,
	[NEXT_FIRE_TIME] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF)
GO
CREATE NONCLUSTERED INDEX [IDX_QRTZ_T_NFT_MISFIRE] ON [dbo].[QRTZ_TRIGGERS] 
(
	[SCHED_NAME] ASC,
	[MISFIRE_INSTR] ASC,
	[NEXT_FIRE_TIME] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF)
GO
CREATE NONCLUSTERED INDEX [IDX_QRTZ_T_NFT_ST] ON [dbo].[QRTZ_TRIGGERS] 
(
	[SCHED_NAME] ASC,
	[TRIGGER_STATE] ASC,
	[NEXT_FIRE_TIME] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF)
GO
CREATE NONCLUSTERED INDEX [IDX_QRTZ_T_NFT_ST_MISFIRE] ON [dbo].[QRTZ_TRIGGERS] 
(
	[SCHED_NAME] ASC,
	[MISFIRE_INSTR] ASC,
	[NEXT_FIRE_TIME] ASC,
	[TRIGGER_STATE] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF)
GO
CREATE NONCLUSTERED INDEX [IDX_QRTZ_T_NFT_ST_MISFIRE_GRP] ON [dbo].[QRTZ_TRIGGERS] 
(
	[SCHED_NAME] ASC,
	[MISFIRE_INSTR] ASC,
	[NEXT_FIRE_TIME] ASC,
	[TRIGGER_GROUP] ASC,
	[TRIGGER_STATE] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF)
GO
CREATE NONCLUSTERED INDEX [IDX_QRTZ_T_STATE] ON [dbo].[QRTZ_TRIGGERS] 
(
	[SCHED_NAME] ASC,
	[TRIGGER_STATE] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF)
GO
/****** Object:  Table [dbo].[tblCompletedWorkingDocumentPresentedFact]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblCompletedWorkingDocumentPresentedFact](
	[Id] [uniqueidentifier] NOT NULL,
	[WorkingDocumentId] [uniqueidentifier] NOT NULL,
	[PresentedId] [uniqueidentifier] NOT NULL,
	[InternalId] [uniqueidentifier] NOT NULL,
	[ReportingWorkingDocumentId] [uniqueidentifier] NOT NULL,
	[ReportingUserReviewerId] [uniqueidentifier] NOT NULL,
	[ReportingUserRevieweeId] [uniqueidentifier] NOT NULL,
	[ReportingPresentedTypeId] [uniqueidentifier] NOT NULL,
	[Prompt] [nvarchar](4000) NOT NULL,
	[DateStarted] [datetime] NOT NULL,
	[DateCompleted] [datetime] NOT NULL,
	[PossibleScore] [decimal](18, 2) NOT NULL,
	[Score] [decimal](18, 2) NOT NULL,
	[PassFail] [bit] NULL,
	[ParentWorkingDocumentId] [uniqueidentifier] NULL,
	[NotesText] [nvarchar](4000) NULL,
	[IsUserContext] [bit] NOT NULL,
 CONSTRAINT [PK_tblCompletedWorkingDocumentQuestionFact] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
CREATE NONCLUSTERED INDEX [IX_tblCompletedWorkingDocumentPresentedFact] ON [dbo].[tblCompletedWorkingDocumentPresentedFact] 
(
	[WorkingDocumentId] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF)
GO
CREATE NONCLUSTERED INDEX [IX_tblCompletedWorkingDocumentPresentedFact_1] ON [dbo].[tblCompletedWorkingDocumentPresentedFact] 
(
	[ReportingWorkingDocumentId] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF)
GO
CREATE NONCLUSTERED INDEX [IX_tblCompletedWorkingDocumentPresentedFact_2] ON [dbo].[tblCompletedWorkingDocumentPresentedFact] 
(
	[ReportingUserRevieweeId] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF)
GO
CREATE NONCLUSTERED INDEX [IX_tblCompletedWorkingDocumentPresentedFact_3] ON [dbo].[tblCompletedWorkingDocumentPresentedFact] 
(
	[ReportingUserRevieweeId] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF)
GO
CREATE NONCLUSTERED INDEX [IX_tblCompletedWorkingDocumentPresentedFact_4] ON [dbo].[tblCompletedWorkingDocumentPresentedFact] 
(
	[ReportingPresentedTypeId] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF)
GO
/****** Object:  Table [dbo].[tblCompletedWorkingDocumentFact]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblCompletedWorkingDocumentFact](
	[WorkingDocumentId] [uniqueidentifier] NOT NULL,
	[ReportingWorkingDocumentId] [uniqueidentifier] NOT NULL,
	[ReportingUserReviewerId] [uniqueidentifier] NOT NULL,
	[ReportingUserRevieweeId] [uniqueidentifier] NOT NULL,
	[DateStarted] [datetime] NOT NULL,
	[DateCompleted] [datetime] NOT NULL,
	[TotalQuestionCount] [int] NOT NULL,
	[TotalPossibleScore] [decimal](18, 2) NOT NULL,
	[TotalScore] [decimal](18, 2) NOT NULL,
	[PassedQuestionCount] [int] NOT NULL,
	[FailedQuestionCount] [int] NOT NULL,
	[NAQuestionCount] [int] NOT NULL,
	[ClientIP] [nvarchar](50) NULL,
	[UserAgentId] [int] NULL,
	[ParentWorkingDocumentId] [uniqueidentifier] NULL,
	[IsUserContext] [bit] NOT NULL,
 CONSTRAINT [PK_tblCompletedWorkingDocumentFact] PRIMARY KEY CLUSTERED 
(
	[WorkingDocumentId] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
CREATE NONCLUSTERED INDEX [IX_tblCompletedWorkingDocumentFact] ON [dbo].[tblCompletedWorkingDocumentFact] 
(
	[ReportingUserRevieweeId] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF)
GO
CREATE NONCLUSTERED INDEX [IX_tblCompletedWorkingDocumentFact_1] ON [dbo].[tblCompletedWorkingDocumentFact] 
(
	[ReportingUserReviewerId] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF)
GO
CREATE NONCLUSTERED INDEX [IX_tblCompletedWorkingDocumentFact_2] ON [dbo].[tblCompletedWorkingDocumentFact] 
(
	[ReportingWorkingDocumentId] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF)
GO
/****** Object:  Table [dbo].[tblHierarchyRoleUserType]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblHierarchyRoleUserType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoleId] [int] NOT NULL,
	[UserTypeId] [int] NOT NULL,
 CONSTRAINT [PK_tblHierarchyRoleUserType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[tblMediaPreview]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblMediaPreview](
	[MediaId] [uniqueidentifier] NOT NULL,
	[Data] [varbinary](max) NOT NULL,
 CONSTRAINT [PK_tblMediaPreview] PRIMARY KEY CLUSTERED 
(
	[MediaId] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[tblMediaData]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblMediaData](
	[MediaId] [uniqueidentifier] NOT NULL,
	[Data] [varbinary](max) NOT NULL,
 CONSTRAINT [PK_tblMediaData] PRIMARY KEY CLUSTERED 
(
	[MediaId] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[tblLabelResource]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblLabelResource](
	[LabelId] [uniqueidentifier] NOT NULL,
	[ResourceId] [int] NOT NULL,
 CONSTRAINT [PK_tblLabelResource] PRIMARY KEY CLUSTERED 
(
	[LabelId] ASC,
	[ResourceId] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[tblResourceData]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblResourceData](
	[ResourceId] [int] NOT NULL,
	[Data] [text] NOT NULL,
 CONSTRAINT [PK_tblResourceData] PRIMARY KEY CLUSTERED 
(
	[ResourceId] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[tblPublishingGroupActorLabel]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblPublishingGroupActorLabel](
	[PublishingGroupId] [int] NOT NULL,
	[LabelId] [uniqueidentifier] NOT NULL,
	[PublishingTypeId] [int] NOT NULL,
 CONSTRAINT [PK_tblPublishingGroupActorLabel] PRIMARY KEY CLUSTERED 
(
	[PublishingGroupId] ASC,
	[LabelId] ASC,
	[PublishingTypeId] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[tblPluginData]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblPluginData](
	[PluginId] [uniqueidentifier] NOT NULL,
	[Data] [varbinary](max) NOT NULL,
 CONSTRAINT [PK_tblPluginData] PRIMARY KEY CLUSTERED 
(
	[PluginId] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[tblUserTypeContextPublishedResource]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblUserTypeContextPublishedResource](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserTypeId] [int] NOT NULL,
	[PublishingGroupResourceId] [int] NOT NULL,
 CONSTRAINT [PK_tblUserTypeContextPublishedResource] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF),
 CONSTRAINT [IX_tblUserTypeContextPublishedResource] UNIQUE NONCLUSTERED 
(
	[UserTypeId] ASC,
	[PublishingGroupResourceId] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[tblUserContextData]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblUserContextData](
	[UserId] [uniqueidentifier] NOT NULL,
	[UserTypeContextPublishedResourceId] [int] NOT NULL,
	[CompletedWorkingDocumentId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_tblUserContextData] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[UserTypeContextPublishedResourceId] ASC,
	[CompletedWorkingDocumentId] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[tblPublishingGroupResourcePublicVariable]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblPublishingGroupResourcePublicVariable](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PublishingGroupResourceId] [int] NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_tblPublishingGroupResourcePublicVariable] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[tblPublishingGroupResourceData]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblPublishingGroupResourceData](
	[PublishingGroupResourceId] [int] NOT NULL,
	[Data] [text] NOT NULL,
 CONSTRAINT [PK_tblPublishingGroupResourceData] PRIMARY KEY CLUSTERED 
(
	[PublishingGroupResourceId] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[tblPublicPublishingGroupResource]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblPublicPublishingGroupResource](
	[PublishingGroupResourceId] [int] NOT NULL,
	[PublicTicketId] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_tblPublicPublishingGroupResource] PRIMARY KEY CLUSTERED 
(
	[PublishingGroupResourceId] ASC,
	[PublicTicketId] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[tblLabelPublishingGroupResource]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblLabelPublishingGroupResource](
	[LabelId] [uniqueidentifier] NOT NULL,
	[PublishingGroupResourceId] [int] NOT NULL,
 CONSTRAINT [PK_tblLabelPublishingGroupResource] PRIMARY KEY CLUSTERED 
(
	[LabelId] ASC,
	[PublishingGroupResourceId] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[tblCompletedPresentedUploadMediaFactValue]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblCompletedPresentedUploadMediaFactValue](
	[CompletedWorkingDocumentPresentedFactId] [uniqueidentifier] NOT NULL,
	[MediaId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_tblCompletedPresentedUploadMediaFactValue] PRIMARY KEY CLUSTERED 
(
	[CompletedWorkingDocumentPresentedFactId] ASC,
	[MediaId] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[tblCompletedPresentedGeoLocationFactValue]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblCompletedPresentedGeoLocationFactValue](
	[Id] [uniqueidentifier] NOT NULL,
	[CompletedWorkingDocumentPresentedFactId] [uniqueidentifier] NOT NULL,
	[Latitude] [decimal](18, 8) NOT NULL,
	[Longitude] [decimal](18, 8) NOT NULL,
	[Accuracy] [decimal](18, 4) NOT NULL,
	[Altitude] [decimal](18, 4) NULL,
	[AltitudeAccuracy] [decimal](18, 4) NULL,
	[Heading] [decimal](18, 4) NULL,
	[Speed] [decimal](18, 4) NULL,
 CONSTRAINT [PK_tblCompletedPresentedGeoLocationFactValue] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
CREATE NONCLUSTERED INDEX [IX_tblCompletedPresentedGeoLocationFactValue] ON [dbo].[tblCompletedPresentedGeoLocationFactValue] 
(
	[CompletedWorkingDocumentPresentedFactId] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF)
GO
/****** Object:  Table [dbo].[tblCompletedPresentedFactValue]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblCompletedPresentedFactValue](
	[Id] [uniqueidentifier] NOT NULL,
	[CompletedWorkingDocumentPresentedFactId] [uniqueidentifier] NOT NULL,
	[Value] [nvarchar](4000) NULL,
 CONSTRAINT [PK_tblCompletedPresentedFactValue] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
CREATE NONCLUSTERED INDEX [IX_tblCompletedPresentedFactValue] ON [dbo].[tblCompletedPresentedFactValue] 
(
	[CompletedWorkingDocumentPresentedFactId] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF)
GO
/****** Object:  Table [dbo].[tblCompletedLabelWorkingDocumentPresentedDimension]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblCompletedLabelWorkingDocumentPresentedDimension](
	[CompletedLabelDimensionId] [uniqueidentifier] NOT NULL,
	[CompletedWorkingDocumentPresentedFactId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_tblCompletedLabelWorkingDocumentPresentedDimension] PRIMARY KEY CLUSTERED 
(
	[CompletedLabelDimensionId] ASC,
	[CompletedWorkingDocumentPresentedFactId] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[tblCompletedLabelWorkingDocumentDimension]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblCompletedLabelWorkingDocumentDimension](
	[CompletedLabelDimensionId] [uniqueidentifier] NOT NULL,
	[CompletedWorkingDocumentFactId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_tblCompletedLabelWorkingDocumentDimension] PRIMARY KEY CLUSTERED 
(
	[CompletedLabelDimensionId] ASC,
	[CompletedWorkingDocumentFactId] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[QRTZ_CRON_TRIGGERS]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QRTZ_CRON_TRIGGERS](
	[SCHED_NAME] [nvarchar](100) NOT NULL,
	[TRIGGER_NAME] [nvarchar](150) NOT NULL,
	[TRIGGER_GROUP] [nvarchar](150) NOT NULL,
	[CRON_EXPRESSION] [nvarchar](120) NOT NULL,
	[TIME_ZONE_ID] [nvarchar](80) NULL,
 CONSTRAINT [PK_QRTZ_CRON_TRIGGERS] PRIMARY KEY CLUSTERED 
(
	[SCHED_NAME] ASC,
	[TRIGGER_NAME] ASC,
	[TRIGGER_GROUP] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[QRTZ_SIMPROP_TRIGGERS]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QRTZ_SIMPROP_TRIGGERS](
	[SCHED_NAME] [nvarchar](100) NOT NULL,
	[TRIGGER_NAME] [nvarchar](150) NOT NULL,
	[TRIGGER_GROUP] [nvarchar](150) NOT NULL,
	[STR_PROP_1] [nvarchar](512) NULL,
	[STR_PROP_2] [nvarchar](512) NULL,
	[STR_PROP_3] [nvarchar](512) NULL,
	[INT_PROP_1] [int] NULL,
	[INT_PROP_2] [int] NULL,
	[LONG_PROP_1] [bigint] NULL,
	[LONG_PROP_2] [bigint] NULL,
	[DEC_PROP_1] [numeric](13, 4) NULL,
	[DEC_PROP_2] [numeric](13, 4) NULL,
	[BOOL_PROP_1] [bit] NULL,
	[BOOL_PROP_2] [bit] NULL,
 CONSTRAINT [PK_QRTZ_SIMPROP_TRIGGERS] PRIMARY KEY CLUSTERED 
(
	[SCHED_NAME] ASC,
	[TRIGGER_NAME] ASC,
	[TRIGGER_GROUP] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[QRTZ_SIMPLE_TRIGGERS]    Script Date: 03/03/2013 11:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QRTZ_SIMPLE_TRIGGERS](
	[SCHED_NAME] [nvarchar](100) NOT NULL,
	[TRIGGER_NAME] [nvarchar](150) NOT NULL,
	[TRIGGER_GROUP] [nvarchar](150) NOT NULL,
	[REPEAT_COUNT] [int] NOT NULL,
	[REPEAT_INTERVAL] [bigint] NOT NULL,
	[TIMES_TRIGGERED] [int] NOT NULL,
 CONSTRAINT [PK_QRTZ_SIMPLE_TRIGGERS] PRIMARY KEY CLUSTERED 
(
	[SCHED_NAME] ASC,
	[TRIGGER_NAME] ASC,
	[TRIGGER_GROUP] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Default [DF_tblCalendarRule_DateCreated]    Script Date: 03/03/2013 11:18:44 ******/
ALTER TABLE [dbo].[tblCalendarRule] ADD  CONSTRAINT [DF_tblCalendarRule_DateCreated]  DEFAULT (getutcdate()) FOR [DateCreated]
GO
/****** Object:  Default [DF_tblCalendarRule_Frequency]    Script Date: 03/03/2013 11:18:44 ******/
ALTER TABLE [dbo].[tblCalendarRule] ADD  CONSTRAINT [DF_tblCalendarRule_Frequency]  DEFAULT ('single') FOR [Frequency]
GO
/****** Object:  Default [DF_tblMembership_MembershipTypeId]    Script Date: 03/03/2013 11:18:44 ******/
ALTER TABLE [dbo].[tblUserData] ADD  CONSTRAINT [DF_tblMembership_MembershipTypeId]  DEFAULT ((1)) FOR [UserTypeId]
GO
/****** Object:  Default [DF_tblUserData_TimeZone]    Script Date: 03/03/2013 11:18:44 ******/
ALTER TABLE [dbo].[tblUserData] ADD  CONSTRAINT [DF_tblUserData_TimeZone]  DEFAULT (N'UTC') FOR [TimeZone]
GO
/****** Object:  Default [DF_tblWorkingDocumentData_UtcLastModified]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblWorkingDocumentData] ADD  CONSTRAINT [DF_tblWorkingDocumentData_UtcLastModified]  DEFAULT (getutcdate()) FOR [UtcLastModified]
GO
/****** Object:  Default [DF_tblUserType_HasGeoCordinates]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblUserType] ADD  CONSTRAINT [DF_tblUserType_HasGeoCordinates]  DEFAULT ((0)) FOR [HasGeoCordinates]
GO
/****** Object:  Default [DF_tblUserType_IsASite]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblUserType] ADD  CONSTRAINT [DF_tblUserType_IsASite]  DEFAULT ((0)) FOR [IsASite]
GO
/****** Object:  Default [DF_tblPublishingGroup_AllowUseAsUserContext]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblPublishingGroup] ADD  CONSTRAINT [DF_tblPublishingGroup_AllowUseAsUserContext]  DEFAULT ((0)) FOR [AllowUseAsUserContext]
GO
/****** Object:  Default [DF__tblPublis__Descr__3DE82FB7]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblPublishingGroup] ADD  DEFAULT ('') FOR [Description]
GO
/****** Object:  Default [DF_tblPlugin_DateCreated]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblPlugin] ADD  CONSTRAINT [DF_tblPlugin_DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
/****** Object:  Default [DF_tblHierarchyBucket_IsPrimary]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblHierarchyBucket] ADD  CONSTRAINT [DF_tblHierarchyBucket_IsPrimary]  DEFAULT (NULL) FOR [IsPrimary]
GO
/****** Object:  Default [DF_tblHierarchyRole_IsPerson]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblHierarchyRole] ADD  CONSTRAINT [DF_tblHierarchyRole_IsPerson]  DEFAULT ((1)) FOR [IsPerson]
GO
/****** Object:  Default [DF_tblPublicTicket_Status]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblPublicTicket] ADD  CONSTRAINT [DF_tblPublicTicket_Status]  DEFAULT ((0)) FOR [Status]
GO
/****** Object:  Default [DF_tblUserMedia_DateCreated]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblUserMedia] ADD  CONSTRAINT [DF_tblUserMedia_DateCreated]  DEFAULT (getutcdate()) FOR [DateCreated]
GO
/****** Object:  Default [DF_tblMedia_DateCreated]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblMedia] ADD  CONSTRAINT [DF_tblMedia_DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
/****** Object:  Default [DF_tblMedia_Name]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblMedia] ADD  CONSTRAINT [DF_tblMedia_Name]  DEFAULT ('None') FOR [Name]
GO
/****** Object:  Default [DF_tblLabel_ForUsers]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblLabel] ADD  CONSTRAINT [DF_tblLabel_ForUsers]  DEFAULT ((0)) FOR [ForUsers]
GO
/****** Object:  Default [DF_tblLabel_ForMedia]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblLabel] ADD  CONSTRAINT [DF_tblLabel_ForMedia]  DEFAULT ((0)) FOR [ForMedia]
GO
/****** Object:  Default [DF_tblLabel_ForResources]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblLabel] ADD  CONSTRAINT [DF_tblLabel_ForResources]  DEFAULT ((0)) FOR [ForResources]
GO
/****** Object:  Default [DF_tblCompletedUserDimension_TimeZone]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblCompletedUserDimension] ADD  CONSTRAINT [DF_tblCompletedUserDimension_TimeZone]  DEFAULT ('UTC') FOR [TimeZone]
GO
/****** Object:  Default [DF_tblCompletedWorkingDocumentPresentedFact_IsUserContext]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblCompletedWorkingDocumentPresentedFact] ADD  CONSTRAINT [DF_tblCompletedWorkingDocumentPresentedFact_IsUserContext]  DEFAULT ((0)) FOR [IsUserContext]
GO
/****** Object:  Default [DF_tblCompletedWorkingDocumentFact_IsUserContext]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblCompletedWorkingDocumentFact] ADD  CONSTRAINT [DF_tblCompletedWorkingDocumentFact_IsUserContext]  DEFAULT ((0)) FOR [IsUserContext]
GO
/****** Object:  Default [DF_tblWorkingDocument_DateCreated]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblWorkingDocument] ADD  CONSTRAINT [DF_tblWorkingDocument_DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
/****** Object:  Default [DF_tblWorkingDocument_UtcLastModified]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblWorkingDocument] ADD  CONSTRAINT [DF_tblWorkingDocument_UtcLastModified]  DEFAULT (getutcdate()) FOR [UtcLastModified]
GO
/****** Object:  Default [DF_tblPublishingGroupResource_Name]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblPublishingGroupResource] ADD  CONSTRAINT [DF_tblPublishingGroupResource_Name]  DEFAULT ('') FOR [Name]
GO
/****** Object:  Default [DF_tblPublishingGroupResource_PopulateReportingData]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblPublishingGroupResource] ADD  CONSTRAINT [DF_tblPublishingGroupResource_PopulateReportingData]  DEFAULT ((1)) FOR [PopulateReportingData]
GO
/****** Object:  Default [DF_tblPublishingGroupResource_UtcLastModified]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblPublishingGroupResource] ADD  CONSTRAINT [DF_tblPublishingGroupResource_UtcLastModified]  DEFAULT (getutcdate()) FOR [UtcLastModified]
GO
/****** Object:  Default [DF__tblPublis__Descr__3EDC53F0]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblPublishingGroupResource] ADD  DEFAULT ('') FOR [Description]
GO
/****** Object:  ForeignKey [FK_tblCalendarRule_tblCalendarEvent]    Script Date: 03/03/2013 11:18:44 ******/
ALTER TABLE [dbo].[tblCalendarRule]  WITH CHECK ADD  CONSTRAINT [FK_tblCalendarRule_tblCalendarEvent] FOREIGN KEY([CalendarEventId])
REFERENCES [dbo].[tblCalendarEvent] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblCalendarRule] CHECK CONSTRAINT [FK_tblCalendarRule_tblCalendarEvent]
GO
/****** Object:  ForeignKey [FK_tblUserLog_tblUserData]    Script Date: 03/03/2013 11:18:44 ******/
ALTER TABLE [dbo].[tblUserLog]  WITH NOCHECK ADD  CONSTRAINT [FK_tblUserLog_tblUserData] FOREIGN KEY([UserId])
REFERENCES [dbo].[tblUserData] ([UserId])
GO
ALTER TABLE [dbo].[tblUserLog] CHECK CONSTRAINT [FK_tblUserLog_tblUserData]
GO
/****** Object:  ForeignKey [FK_tblUserLog_tblUserData1]    Script Date: 03/03/2013 11:18:44 ******/
ALTER TABLE [dbo].[tblUserLog]  WITH NOCHECK ADD  CONSTRAINT [FK_tblUserLog_tblUserData1] FOREIGN KEY([NearestLocation])
REFERENCES [dbo].[tblUserData] ([UserId])
GO
ALTER TABLE [dbo].[tblUserLog] CHECK CONSTRAINT [FK_tblUserLog_tblUserData1]
GO
/****** Object:  ForeignKey [FK_tblMessages_tblMessages]    Script Date: 03/03/2013 11:18:44 ******/
ALTER TABLE [dbo].[tblMessages]  WITH CHECK ADD  CONSTRAINT [FK_tblMessages_tblMessages] FOREIGN KEY([InReplyTo])
REFERENCES [dbo].[tblMessages] ([Id])
GO
ALTER TABLE [dbo].[tblMessages] CHECK CONSTRAINT [FK_tblMessages_tblMessages]
GO
/****** Object:  ForeignKey [FK_tblMessages_tblUserData]    Script Date: 03/03/2013 11:18:44 ******/
ALTER TABLE [dbo].[tblMessages]  WITH NOCHECK ADD  CONSTRAINT [FK_tblMessages_tblUserData] FOREIGN KEY([Sender])
REFERENCES [dbo].[tblUserData] ([UserId])
GO
ALTER TABLE [dbo].[tblMessages] CHECK CONSTRAINT [FK_tblMessages_tblUserData]
GO
/****** Object:  ForeignKey [FK_tblMailbox_tblMessages]    Script Date: 03/03/2013 11:18:44 ******/
ALTER TABLE [dbo].[tblMailbox]  WITH CHECK ADD  CONSTRAINT [FK_tblMailbox_tblMessages] FOREIGN KEY([MessageId])
REFERENCES [dbo].[tblMessages] ([Id])
GO
ALTER TABLE [dbo].[tblMailbox] CHECK CONSTRAINT [FK_tblMailbox_tblMessages]
GO
/****** Object:  ForeignKey [FK_tblMailbox_tblUserData]    Script Date: 03/03/2013 11:18:44 ******/
ALTER TABLE [dbo].[tblMailbox]  WITH NOCHECK ADD  CONSTRAINT [FK_tblMailbox_tblUserData] FOREIGN KEY([UserId])
REFERENCES [dbo].[tblUserData] ([UserId])
GO
ALTER TABLE [dbo].[tblMailbox] CHECK CONSTRAINT [FK_tblMailbox_tblUserData]
GO
/****** Object:  ForeignKey [FK_tblCalendarEvent_tblUserData]    Script Date: 03/03/2013 11:18:44 ******/
ALTER TABLE [dbo].[tblCalendarEvent]  WITH NOCHECK ADD  CONSTRAINT [FK_tblCalendarEvent_tblUserData] FOREIGN KEY([UserId])
REFERENCES [dbo].[tblUserData] ([UserId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblCalendarEvent] CHECK CONSTRAINT [FK_tblCalendarEvent_tblUserData]
GO
/****** Object:  ForeignKey [FK_tblMembership_tblMembershipType]    Script Date: 03/03/2013 11:18:44 ******/
ALTER TABLE [dbo].[tblUserData]  WITH CHECK ADD  CONSTRAINT [FK_tblMembership_tblMembershipType] FOREIGN KEY([UserTypeId])
REFERENCES [dbo].[tblUserType] ([Id])
GO
ALTER TABLE [dbo].[tblUserData] CHECK CONSTRAINT [FK_tblMembership_tblMembershipType]
GO
/****** Object:  ForeignKey [FK_tblUserData_Users]    Script Date: 03/03/2013 11:18:44 ******/
ALTER TABLE [dbo].[tblUserData]  WITH CHECK ADD  CONSTRAINT [FK_tblUserData_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([UserId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblUserData] CHECK CONSTRAINT [FK_tblUserData_Users]
GO
/****** Object:  ForeignKey [FK_tblWorkingDocumentReference_tblWorkingDocument]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblWorkingDocumentReference]  WITH CHECK ADD  CONSTRAINT [FK_tblWorkingDocumentReference_tblWorkingDocument] FOREIGN KEY([WorkingDocumentId])
REFERENCES [dbo].[tblWorkingDocument] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblWorkingDocumentReference] CHECK CONSTRAINT [FK_tblWorkingDocumentReference_tblWorkingDocument]
GO
/****** Object:  ForeignKey [FK_tblWorkingDocumentPublicVariable_tblWorkingDocument]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblWorkingDocumentPublicVariable]  WITH CHECK ADD  CONSTRAINT [FK_tblWorkingDocumentPublicVariable_tblWorkingDocument] FOREIGN KEY([WorkingDocumentId])
REFERENCES [dbo].[tblWorkingDocument] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblWorkingDocumentPublicVariable] CHECK CONSTRAINT [FK_tblWorkingDocumentPublicVariable_tblWorkingDocument]
GO
/****** Object:  ForeignKey [FK_tblWorkingDocumentData_tblWorkingDocument]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblWorkingDocumentData]  WITH CHECK ADD  CONSTRAINT [FK_tblWorkingDocumentData_tblWorkingDocument] FOREIGN KEY([WorkingDocumentId])
REFERENCES [dbo].[tblWorkingDocument] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblWorkingDocumentData] CHECK CONSTRAINT [FK_tblWorkingDocumentData_tblWorkingDocument]
GO
/****** Object:  ForeignKey [UserProfile]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[Profiles]  WITH CHECK ADD  CONSTRAINT [UserProfile] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[Profiles] CHECK CONSTRAINT [UserProfile]
GO
/****** Object:  ForeignKey [FK_tblPublishingGroupActor_tblPublishingGroup]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblPublishingGroupActor]  WITH NOCHECK ADD  CONSTRAINT [FK_tblPublishingGroupActor_tblPublishingGroup] FOREIGN KEY([PublishingGroupId])
REFERENCES [dbo].[tblPublishingGroup] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblPublishingGroupActor] CHECK CONSTRAINT [FK_tblPublishingGroupActor_tblPublishingGroup]
GO
/****** Object:  ForeignKey [FK_tblPublishingGroupActor_tblPublishingGroupActorType]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblPublishingGroupActor]  WITH NOCHECK ADD  CONSTRAINT [FK_tblPublishingGroupActor_tblPublishingGroupActorType] FOREIGN KEY([PublishingTypeId])
REFERENCES [dbo].[tblPublishingGroupActorType] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblPublishingGroupActor] CHECK CONSTRAINT [FK_tblPublishingGroupActor_tblPublishingGroupActorType]
GO
/****** Object:  ForeignKey [FK_tblPublishingGroupActor_tblUserData]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblPublishingGroupActor]  WITH NOCHECK ADD  CONSTRAINT [FK_tblPublishingGroupActor_tblUserData] FOREIGN KEY([UserId])
REFERENCES [dbo].[tblUserData] ([UserId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblPublishingGroupActor] CHECK CONSTRAINT [FK_tblPublishingGroupActor_tblUserData]
GO
/****** Object:  ForeignKey [FK_tblHierarchyBucket_tblHierarchyBucket]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblHierarchyBucket]  WITH CHECK ADD  CONSTRAINT [FK_tblHierarchyBucket_tblHierarchyBucket] FOREIGN KEY([ParentId])
REFERENCES [dbo].[tblHierarchyBucket] ([Id])
GO
ALTER TABLE [dbo].[tblHierarchyBucket] CHECK CONSTRAINT [FK_tblHierarchyBucket_tblHierarchyBucket]
GO
/****** Object:  ForeignKey [FK_tblHierarchyBucket_tblHierarchyRole]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblHierarchyBucket]  WITH CHECK ADD  CONSTRAINT [FK_tblHierarchyBucket_tblHierarchyRole] FOREIGN KEY([RoleId])
REFERENCES [dbo].[tblHierarchyRole] ([Id])
GO
ALTER TABLE [dbo].[tblHierarchyBucket] CHECK CONSTRAINT [FK_tblHierarchyBucket_tblHierarchyRole]
GO
/****** Object:  ForeignKey [FK_tblHierarchyBucket_tblUserData]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblHierarchyBucket]  WITH NOCHECK ADD  CONSTRAINT [FK_tblHierarchyBucket_tblUserData] FOREIGN KEY([UserId])
REFERENCES [dbo].[tblUserData] ([UserId])
GO
ALTER TABLE [dbo].[tblHierarchyBucket] CHECK CONSTRAINT [FK_tblHierarchyBucket_tblUserData]
GO
/****** Object:  ForeignKey [FK_tblPublicWorkingDocument_tblPublicTicket]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblPublicWorkingDocument]  WITH CHECK ADD  CONSTRAINT [FK_tblPublicWorkingDocument_tblPublicTicket] FOREIGN KEY([PublicTicketId])
REFERENCES [dbo].[tblPublicTicket] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblPublicWorkingDocument] CHECK CONSTRAINT [FK_tblPublicWorkingDocument_tblPublicTicket]
GO
/****** Object:  ForeignKey [FK_tblPublicWorkingDocument_tblWorkingDocument]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblPublicWorkingDocument]  WITH CHECK ADD  CONSTRAINT [FK_tblPublicWorkingDocument_tblWorkingDocument] FOREIGN KEY([WorkingDocumentId])
REFERENCES [dbo].[tblWorkingDocument] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblPublicWorkingDocument] CHECK CONSTRAINT [FK_tblPublicWorkingDocument_tblWorkingDocument]
GO
/****** Object:  ForeignKey [FK_tblUserMedia_tblMedia]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblUserMedia]  WITH NOCHECK ADD  CONSTRAINT [FK_tblUserMedia_tblMedia] FOREIGN KEY([MediaId])
REFERENCES [dbo].[tblMedia] ([Id])
GO
ALTER TABLE [dbo].[tblUserMedia] CHECK CONSTRAINT [FK_tblUserMedia_tblMedia]
GO
/****** Object:  ForeignKey [FK_tblUserMedia_tblUserData]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblUserMedia]  WITH NOCHECK ADD  CONSTRAINT [FK_tblUserMedia_tblUserData] FOREIGN KEY([UserId])
REFERENCES [dbo].[tblUserData] ([UserId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblUserMedia] CHECK CONSTRAINT [FK_tblUserMedia_tblUserData]
GO
/****** Object:  ForeignKey [FK_tblLabelMembership_tblLabel]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblLabelUser]  WITH CHECK ADD  CONSTRAINT [FK_tblLabelMembership_tblLabel] FOREIGN KEY([LabelId])
REFERENCES [dbo].[tblLabel] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblLabelUser] CHECK CONSTRAINT [FK_tblLabelMembership_tblLabel]
GO
/****** Object:  ForeignKey [FK_tblLabelUser_tblUserData]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblLabelUser]  WITH CHECK ADD  CONSTRAINT [FK_tblLabelUser_tblUserData] FOREIGN KEY([UserId])
REFERENCES [dbo].[tblUserData] ([UserId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblLabelUser] CHECK CONSTRAINT [FK_tblLabelUser_tblUserData]
GO
/****** Object:  ForeignKey [UserApplication]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [UserApplication] FOREIGN KEY([ApplicationId])
REFERENCES [dbo].[Applications] ([ApplicationId])
GO
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [UserApplication]
GO
/****** Object:  ForeignKey [MembershipApplication]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[Memberships]  WITH CHECK ADD  CONSTRAINT [MembershipApplication] FOREIGN KEY([ApplicationId])
REFERENCES [dbo].[Applications] ([ApplicationId])
GO
ALTER TABLE [dbo].[Memberships] CHECK CONSTRAINT [MembershipApplication]
GO
/****** Object:  ForeignKey [MembershipUser]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[Memberships]  WITH CHECK ADD  CONSTRAINT [MembershipUser] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[Memberships] CHECK CONSTRAINT [MembershipUser]
GO
/****** Object:  ForeignKey [UsersInRoleRole]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[UsersInRoles]  WITH CHECK ADD  CONSTRAINT [UsersInRoleRole] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Roles] ([RoleId])
GO
ALTER TABLE [dbo].[UsersInRoles] CHECK CONSTRAINT [UsersInRoleRole]
GO
/****** Object:  ForeignKey [UsersInRoleUser]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[UsersInRoles]  WITH CHECK ADD  CONSTRAINT [UsersInRoleUser] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[UsersInRoles] CHECK CONSTRAINT [UsersInRoleUser]
GO
/****** Object:  ForeignKey [RoleApplication]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[Roles]  WITH CHECK ADD  CONSTRAINT [RoleApplication] FOREIGN KEY([ApplicationId])
REFERENCES [dbo].[Applications] ([ApplicationId])
GO
ALTER TABLE [dbo].[Roles] CHECK CONSTRAINT [RoleApplication]
GO
/****** Object:  ForeignKey [FK_QRTZ_TRIGGERS_QRTZ_JOB_DETAILS]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[QRTZ_TRIGGERS]  WITH CHECK ADD  CONSTRAINT [FK_QRTZ_TRIGGERS_QRTZ_JOB_DETAILS] FOREIGN KEY([SCHED_NAME], [JOB_NAME], [JOB_GROUP])
REFERENCES [dbo].[QRTZ_JOB_DETAILS] ([SCHED_NAME], [JOB_NAME], [JOB_GROUP])
GO
ALTER TABLE [dbo].[QRTZ_TRIGGERS] CHECK CONSTRAINT [FK_QRTZ_TRIGGERS_QRTZ_JOB_DETAILS]
GO
/****** Object:  ForeignKey [FK_tblCompletedWorkingDocumentPresentedFact_tblCompletedPresentedTypeDimension]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblCompletedWorkingDocumentPresentedFact]  WITH CHECK ADD  CONSTRAINT [FK_tblCompletedWorkingDocumentPresentedFact_tblCompletedPresentedTypeDimension] FOREIGN KEY([ReportingPresentedTypeId])
REFERENCES [dbo].[tblCompletedPresentedTypeDimension] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblCompletedWorkingDocumentPresentedFact] CHECK CONSTRAINT [FK_tblCompletedWorkingDocumentPresentedFact_tblCompletedPresentedTypeDimension]
GO
/****** Object:  ForeignKey [FK_tblCompletedWorkingDocumentPresentedFact_tblCompletedUserDimension]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblCompletedWorkingDocumentPresentedFact]  WITH CHECK ADD  CONSTRAINT [FK_tblCompletedWorkingDocumentPresentedFact_tblCompletedUserDimension] FOREIGN KEY([ReportingUserReviewerId])
REFERENCES [dbo].[tblCompletedUserDimension] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblCompletedWorkingDocumentPresentedFact] CHECK CONSTRAINT [FK_tblCompletedWorkingDocumentPresentedFact_tblCompletedUserDimension]
GO
/****** Object:  ForeignKey [FK_tblCompletedWorkingDocumentPresentedFact_tblCompletedUserDimension1]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblCompletedWorkingDocumentPresentedFact]  WITH CHECK ADD  CONSTRAINT [FK_tblCompletedWorkingDocumentPresentedFact_tblCompletedUserDimension1] FOREIGN KEY([ReportingUserRevieweeId])
REFERENCES [dbo].[tblCompletedUserDimension] ([Id])
GO
ALTER TABLE [dbo].[tblCompletedWorkingDocumentPresentedFact] CHECK CONSTRAINT [FK_tblCompletedWorkingDocumentPresentedFact_tblCompletedUserDimension1]
GO
/****** Object:  ForeignKey [FK_tblCompletedWorkingDocumentFact_tblCompletedUserDimension]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblCompletedWorkingDocumentFact]  WITH CHECK ADD  CONSTRAINT [FK_tblCompletedWorkingDocumentFact_tblCompletedUserDimension] FOREIGN KEY([ReportingUserReviewerId])
REFERENCES [dbo].[tblCompletedUserDimension] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblCompletedWorkingDocumentFact] CHECK CONSTRAINT [FK_tblCompletedWorkingDocumentFact_tblCompletedUserDimension]
GO
/****** Object:  ForeignKey [FK_tblCompletedWorkingDocumentFact_tblCompletedUserDimension1]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblCompletedWorkingDocumentFact]  WITH CHECK ADD  CONSTRAINT [FK_tblCompletedWorkingDocumentFact_tblCompletedUserDimension1] FOREIGN KEY([ReportingUserRevieweeId])
REFERENCES [dbo].[tblCompletedUserDimension] ([Id])
GO
ALTER TABLE [dbo].[tblCompletedWorkingDocumentFact] CHECK CONSTRAINT [FK_tblCompletedWorkingDocumentFact_tblCompletedUserDimension1]
GO
/****** Object:  ForeignKey [FK_tblCompletedWorkingDocumentFact_tblCompletedWorkingDocumentDimension]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblCompletedWorkingDocumentFact]  WITH CHECK ADD  CONSTRAINT [FK_tblCompletedWorkingDocumentFact_tblCompletedWorkingDocumentDimension] FOREIGN KEY([ReportingWorkingDocumentId])
REFERENCES [dbo].[tblCompletedWorkingDocumentDimension] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblCompletedWorkingDocumentFact] CHECK CONSTRAINT [FK_tblCompletedWorkingDocumentFact_tblCompletedWorkingDocumentDimension]
GO
/****** Object:  ForeignKey [FK_tblHierarchyRoleUserType_tblHierarchyRole]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblHierarchyRoleUserType]  WITH CHECK ADD  CONSTRAINT [FK_tblHierarchyRoleUserType_tblHierarchyRole] FOREIGN KEY([RoleId])
REFERENCES [dbo].[tblHierarchyRole] ([Id])
GO
ALTER TABLE [dbo].[tblHierarchyRoleUserType] CHECK CONSTRAINT [FK_tblHierarchyRoleUserType_tblHierarchyRole]
GO
/****** Object:  ForeignKey [FK_tblHierarchyRoleUserType_tblUserType]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblHierarchyRoleUserType]  WITH NOCHECK ADD  CONSTRAINT [FK_tblHierarchyRoleUserType_tblUserType] FOREIGN KEY([UserTypeId])
REFERENCES [dbo].[tblUserType] ([Id])
GO
ALTER TABLE [dbo].[tblHierarchyRoleUserType] CHECK CONSTRAINT [FK_tblHierarchyRoleUserType_tblUserType]
GO
/****** Object:  ForeignKey [FK_tblLabelResource_tblLabel]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblLabelResource]  WITH CHECK ADD  CONSTRAINT [FK_tblLabelResource_tblLabel] FOREIGN KEY([LabelId])
REFERENCES [dbo].[tblLabel] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblLabelResource] CHECK CONSTRAINT [FK_tblLabelResource_tblLabel]
GO
/****** Object:  ForeignKey [FK_tblLabelResource_tblResource]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblLabelResource]  WITH CHECK ADD  CONSTRAINT [FK_tblLabelResource_tblResource] FOREIGN KEY([ResourceId])
REFERENCES [dbo].[tblResource] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblLabelResource] CHECK CONSTRAINT [FK_tblLabelResource_tblResource]
GO
/****** Object:  ForeignKey [FK_tblMediaPreview_tblMedia]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblMediaPreview]  WITH NOCHECK ADD  CONSTRAINT [FK_tblMediaPreview_tblMedia] FOREIGN KEY([MediaId])
REFERENCES [dbo].[tblMedia] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblMediaPreview] CHECK CONSTRAINT [FK_tblMediaPreview_tblMedia]
GO
/****** Object:  ForeignKey [FK_tblMediaData_tblMedia]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblMediaData]  WITH CHECK ADD  CONSTRAINT [FK_tblMediaData_tblMedia] FOREIGN KEY([MediaId])
REFERENCES [dbo].[tblMedia] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblMediaData] CHECK CONSTRAINT [FK_tblMediaData_tblMedia]
GO
/****** Object:  ForeignKey [FK_tblResourceData_tblResource]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblResourceData]  WITH CHECK ADD  CONSTRAINT [FK_tblResourceData_tblResource] FOREIGN KEY([ResourceId])
REFERENCES [dbo].[tblResource] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblResourceData] CHECK CONSTRAINT [FK_tblResourceData_tblResource]
GO
/****** Object:  ForeignKey [FK_tblPublishingGroupActorLabel_tblLabel]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblPublishingGroupActorLabel]  WITH CHECK ADD  CONSTRAINT [FK_tblPublishingGroupActorLabel_tblLabel] FOREIGN KEY([LabelId])
REFERENCES [dbo].[tblLabel] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblPublishingGroupActorLabel] CHECK CONSTRAINT [FK_tblPublishingGroupActorLabel_tblLabel]
GO
/****** Object:  ForeignKey [FK_tblPublishingGroupActorLabel_tblPublishingGroup]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblPublishingGroupActorLabel]  WITH CHECK ADD  CONSTRAINT [FK_tblPublishingGroupActorLabel_tblPublishingGroup] FOREIGN KEY([PublishingGroupId])
REFERENCES [dbo].[tblPublishingGroup] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblPublishingGroupActorLabel] CHECK CONSTRAINT [FK_tblPublishingGroupActorLabel_tblPublishingGroup]
GO
/****** Object:  ForeignKey [FK_tblPublishingGroupActorLabel_tblPublishingGroupActorType]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblPublishingGroupActorLabel]  WITH CHECK ADD  CONSTRAINT [FK_tblPublishingGroupActorLabel_tblPublishingGroupActorType] FOREIGN KEY([PublishingTypeId])
REFERENCES [dbo].[tblPublishingGroupActorType] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblPublishingGroupActorLabel] CHECK CONSTRAINT [FK_tblPublishingGroupActorLabel_tblPublishingGroupActorType]
GO
/****** Object:  ForeignKey [FK_tblWorkingDocument_tblPublishingGroupResource]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblWorkingDocument]  WITH NOCHECK ADD  CONSTRAINT [FK_tblWorkingDocument_tblPublishingGroupResource] FOREIGN KEY([PublishingGroupResourceId])
REFERENCES [dbo].[tblPublishingGroupResource] ([Id])
GO
ALTER TABLE [dbo].[tblWorkingDocument] CHECK CONSTRAINT [FK_tblWorkingDocument_tblPublishingGroupResource]
GO
/****** Object:  ForeignKey [FK_tblWorkingDocument_tblUserAgent]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblWorkingDocument]  WITH NOCHECK ADD  CONSTRAINT [FK_tblWorkingDocument_tblUserAgent] FOREIGN KEY([UserAgentId])
REFERENCES [dbo].[tblUserAgent] ([Id])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[tblWorkingDocument] CHECK CONSTRAINT [FK_tblWorkingDocument_tblUserAgent]
GO
/****** Object:  ForeignKey [FK_tblWorkingDocument_tblUserData]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblWorkingDocument]  WITH NOCHECK ADD  CONSTRAINT [FK_tblWorkingDocument_tblUserData] FOREIGN KEY([ReviewerId])
REFERENCES [dbo].[tblUserData] ([UserId])
GO
ALTER TABLE [dbo].[tblWorkingDocument] CHECK CONSTRAINT [FK_tblWorkingDocument_tblUserData]
GO
/****** Object:  ForeignKey [FK_tblWorkingDocument_tblUserData1]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblWorkingDocument]  WITH NOCHECK ADD  CONSTRAINT [FK_tblWorkingDocument_tblUserData1] FOREIGN KEY([RevieweeId])
REFERENCES [dbo].[tblUserData] ([UserId])
GO
ALTER TABLE [dbo].[tblWorkingDocument] CHECK CONSTRAINT [FK_tblWorkingDocument_tblUserData1]
GO
/****** Object:  ForeignKey [FK_tblWorkingDocument_tblWorkingDocumentStatus]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblWorkingDocument]  WITH NOCHECK ADD  CONSTRAINT [FK_tblWorkingDocument_tblWorkingDocumentStatus] FOREIGN KEY([Status])
REFERENCES [dbo].[tblWorkingDocumentStatus] ([Id])
GO
ALTER TABLE [dbo].[tblWorkingDocument] CHECK CONSTRAINT [FK_tblWorkingDocument_tblWorkingDocumentStatus]
GO
/****** Object:  ForeignKey [FK_tblPublishingGroupResource_tblMedia]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblPublishingGroupResource]  WITH CHECK ADD  CONSTRAINT [FK_tblPublishingGroupResource_tblMedia] FOREIGN KEY([PictureId])
REFERENCES [dbo].[tblMedia] ([Id])
GO
ALTER TABLE [dbo].[tblPublishingGroupResource] CHECK CONSTRAINT [FK_tblPublishingGroupResource_tblMedia]
GO
/****** Object:  ForeignKey [FK_tblPublishingGroupResource_tblPublishingGroup]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblPublishingGroupResource]  WITH CHECK ADD  CONSTRAINT [FK_tblPublishingGroupResource_tblPublishingGroup] FOREIGN KEY([PublishingGroupId])
REFERENCES [dbo].[tblPublishingGroup] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblPublishingGroupResource] CHECK CONSTRAINT [FK_tblPublishingGroupResource_tblPublishingGroup]
GO
/****** Object:  ForeignKey [FK_tblPublishingGroupResource_tblResource]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblPublishingGroupResource]  WITH CHECK ADD  CONSTRAINT [FK_tblPublishingGroupResource_tblResource] FOREIGN KEY([ResourceId])
REFERENCES [dbo].[tblResource] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblPublishingGroupResource] CHECK CONSTRAINT [FK_tblPublishingGroupResource_tblResource]
GO
/****** Object:  ForeignKey [FK_tblPluginData_tblPlugin]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblPluginData]  WITH CHECK ADD  CONSTRAINT [FK_tblPluginData_tblPlugin] FOREIGN KEY([PluginId])
REFERENCES [dbo].[tblPlugin] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblPluginData] CHECK CONSTRAINT [FK_tblPluginData_tblPlugin]
GO
/****** Object:  ForeignKey [FK_tblUserContextData_tblCompletedWorkingDocumentFact]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblUserContextData]  WITH CHECK ADD  CONSTRAINT [FK_tblUserContextData_tblCompletedWorkingDocumentFact] FOREIGN KEY([CompletedWorkingDocumentId])
REFERENCES [dbo].[tblCompletedWorkingDocumentFact] ([WorkingDocumentId])
GO
ALTER TABLE [dbo].[tblUserContextData] CHECK CONSTRAINT [FK_tblUserContextData_tblCompletedWorkingDocumentFact]
GO
/****** Object:  ForeignKey [FK_tblUserContextData_tblUserData]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblUserContextData]  WITH NOCHECK ADD  CONSTRAINT [FK_tblUserContextData_tblUserData] FOREIGN KEY([UserId])
REFERENCES [dbo].[tblUserData] ([UserId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblUserContextData] CHECK CONSTRAINT [FK_tblUserContextData_tblUserData]
GO
/****** Object:  ForeignKey [FK_tblUserContextData_tblUserTypeContextPublishedResource]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblUserContextData]  WITH CHECK ADD  CONSTRAINT [FK_tblUserContextData_tblUserTypeContextPublishedResource] FOREIGN KEY([UserTypeContextPublishedResourceId])
REFERENCES [dbo].[tblUserTypeContextPublishedResource] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblUserContextData] CHECK CONSTRAINT [FK_tblUserContextData_tblUserTypeContextPublishedResource]
GO
/****** Object:  ForeignKey [FK_tblUserTypeContextPublishedResource_tblPublishingGroupResource]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblUserTypeContextPublishedResource]  WITH NOCHECK ADD  CONSTRAINT [FK_tblUserTypeContextPublishedResource_tblPublishingGroupResource] FOREIGN KEY([PublishingGroupResourceId])
REFERENCES [dbo].[tblPublishingGroupResource] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblUserTypeContextPublishedResource] CHECK CONSTRAINT [FK_tblUserTypeContextPublishedResource_tblPublishingGroupResource]
GO
/****** Object:  ForeignKey [FK_tblUserTypeContextPublishedResource_tblUserType]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblUserTypeContextPublishedResource]  WITH NOCHECK ADD  CONSTRAINT [FK_tblUserTypeContextPublishedResource_tblUserType] FOREIGN KEY([UserTypeId])
REFERENCES [dbo].[tblUserType] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblUserTypeContextPublishedResource] CHECK CONSTRAINT [FK_tblUserTypeContextPublishedResource_tblUserType]
GO
/****** Object:  ForeignKey [FK_tblPublishingGroupResourcePublicVariable_tblPublishingGroupResource]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblPublishingGroupResourcePublicVariable]  WITH NOCHECK ADD  CONSTRAINT [FK_tblPublishingGroupResourcePublicVariable_tblPublishingGroupResource] FOREIGN KEY([PublishingGroupResourceId])
REFERENCES [dbo].[tblPublishingGroupResource] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblPublishingGroupResourcePublicVariable] CHECK CONSTRAINT [FK_tblPublishingGroupResourcePublicVariable_tblPublishingGroupResource]
GO
/****** Object:  ForeignKey [FK_tblPublishingGroupResourceData_tblPublishingGroupResource]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblPublishingGroupResourceData]  WITH CHECK ADD  CONSTRAINT [FK_tblPublishingGroupResourceData_tblPublishingGroupResource] FOREIGN KEY([PublishingGroupResourceId])
REFERENCES [dbo].[tblPublishingGroupResource] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblPublishingGroupResourceData] CHECK CONSTRAINT [FK_tblPublishingGroupResourceData_tblPublishingGroupResource]
GO
/****** Object:  ForeignKey [FK_tblPublicPublishingGroupResource_tblPublicTicket]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblPublicPublishingGroupResource]  WITH CHECK ADD  CONSTRAINT [FK_tblPublicPublishingGroupResource_tblPublicTicket] FOREIGN KEY([PublicTicketId])
REFERENCES [dbo].[tblPublicTicket] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblPublicPublishingGroupResource] CHECK CONSTRAINT [FK_tblPublicPublishingGroupResource_tblPublicTicket]
GO
/****** Object:  ForeignKey [FK_tblPublicPublishingGroupResource_tblPublishingGroupResource]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblPublicPublishingGroupResource]  WITH NOCHECK ADD  CONSTRAINT [FK_tblPublicPublishingGroupResource_tblPublishingGroupResource] FOREIGN KEY([PublishingGroupResourceId])
REFERENCES [dbo].[tblPublishingGroupResource] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblPublicPublishingGroupResource] CHECK CONSTRAINT [FK_tblPublicPublishingGroupResource_tblPublishingGroupResource]
GO
/****** Object:  ForeignKey [FK_tblLabelPublishingGroupResource_tblLabel]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblLabelPublishingGroupResource]  WITH NOCHECK ADD  CONSTRAINT [FK_tblLabelPublishingGroupResource_tblLabel] FOREIGN KEY([LabelId])
REFERENCES [dbo].[tblLabel] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblLabelPublishingGroupResource] CHECK CONSTRAINT [FK_tblLabelPublishingGroupResource_tblLabel]
GO
/****** Object:  ForeignKey [FK_tblLabelPublishingGroupResource_tblPublishingGroupResource]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblLabelPublishingGroupResource]  WITH NOCHECK ADD  CONSTRAINT [FK_tblLabelPublishingGroupResource_tblPublishingGroupResource] FOREIGN KEY([PublishingGroupResourceId])
REFERENCES [dbo].[tblPublishingGroupResource] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblLabelPublishingGroupResource] CHECK CONSTRAINT [FK_tblLabelPublishingGroupResource_tblPublishingGroupResource]
GO
/****** Object:  ForeignKey [FK_tblCompletedPresentedUploadMediaFactValue_tblCompletedWorkingDocumentPresentedFact]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblCompletedPresentedUploadMediaFactValue]  WITH CHECK ADD  CONSTRAINT [FK_tblCompletedPresentedUploadMediaFactValue_tblCompletedWorkingDocumentPresentedFact] FOREIGN KEY([CompletedWorkingDocumentPresentedFactId])
REFERENCES [dbo].[tblCompletedWorkingDocumentPresentedFact] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblCompletedPresentedUploadMediaFactValue] CHECK CONSTRAINT [FK_tblCompletedPresentedUploadMediaFactValue_tblCompletedWorkingDocumentPresentedFact]
GO
/****** Object:  ForeignKey [FK_tblCompletedPresentedUploadMediaFactValue_tblMedia]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblCompletedPresentedUploadMediaFactValue]  WITH CHECK ADD  CONSTRAINT [FK_tblCompletedPresentedUploadMediaFactValue_tblMedia] FOREIGN KEY([MediaId])
REFERENCES [dbo].[tblMedia] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblCompletedPresentedUploadMediaFactValue] CHECK CONSTRAINT [FK_tblCompletedPresentedUploadMediaFactValue_tblMedia]
GO
/****** Object:  ForeignKey [FK_tblCompletedPresentedGeoLocationFactValue_tblCompletedWorkingDocumentPresentedFact]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblCompletedPresentedGeoLocationFactValue]  WITH CHECK ADD  CONSTRAINT [FK_tblCompletedPresentedGeoLocationFactValue_tblCompletedWorkingDocumentPresentedFact] FOREIGN KEY([CompletedWorkingDocumentPresentedFactId])
REFERENCES [dbo].[tblCompletedWorkingDocumentPresentedFact] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblCompletedPresentedGeoLocationFactValue] CHECK CONSTRAINT [FK_tblCompletedPresentedGeoLocationFactValue_tblCompletedWorkingDocumentPresentedFact]
GO
/****** Object:  ForeignKey [FK_tblCompletedPresentedFactValue_tblCompletedWorkingDocumentPresentedFact]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblCompletedPresentedFactValue]  WITH CHECK ADD  CONSTRAINT [FK_tblCompletedPresentedFactValue_tblCompletedWorkingDocumentPresentedFact] FOREIGN KEY([CompletedWorkingDocumentPresentedFactId])
REFERENCES [dbo].[tblCompletedWorkingDocumentPresentedFact] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblCompletedPresentedFactValue] CHECK CONSTRAINT [FK_tblCompletedPresentedFactValue_tblCompletedWorkingDocumentPresentedFact]
GO
/****** Object:  ForeignKey [FK_tblCompletedLabelWorkingDocumentPresentedDimension_tblCompletedLabelDimension]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblCompletedLabelWorkingDocumentPresentedDimension]  WITH CHECK ADD  CONSTRAINT [FK_tblCompletedLabelWorkingDocumentPresentedDimension_tblCompletedLabelDimension] FOREIGN KEY([CompletedLabelDimensionId])
REFERENCES [dbo].[tblCompletedLabelDimension] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblCompletedLabelWorkingDocumentPresentedDimension] CHECK CONSTRAINT [FK_tblCompletedLabelWorkingDocumentPresentedDimension_tblCompletedLabelDimension]
GO
/****** Object:  ForeignKey [FK_tblCompletedLabelWorkingDocumentPresentedDimension_tblCompletedWorkingDocumentPresentedFact]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblCompletedLabelWorkingDocumentPresentedDimension]  WITH CHECK ADD  CONSTRAINT [FK_tblCompletedLabelWorkingDocumentPresentedDimension_tblCompletedWorkingDocumentPresentedFact] FOREIGN KEY([CompletedWorkingDocumentPresentedFactId])
REFERENCES [dbo].[tblCompletedWorkingDocumentPresentedFact] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblCompletedLabelWorkingDocumentPresentedDimension] CHECK CONSTRAINT [FK_tblCompletedLabelWorkingDocumentPresentedDimension_tblCompletedWorkingDocumentPresentedFact]
GO
/****** Object:  ForeignKey [FK_tblCompletedLabelWorkingDocumentDimension_tblCompletedLabelDimension]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblCompletedLabelWorkingDocumentDimension]  WITH CHECK ADD  CONSTRAINT [FK_tblCompletedLabelWorkingDocumentDimension_tblCompletedLabelDimension] FOREIGN KEY([CompletedLabelDimensionId])
REFERENCES [dbo].[tblCompletedLabelDimension] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblCompletedLabelWorkingDocumentDimension] CHECK CONSTRAINT [FK_tblCompletedLabelWorkingDocumentDimension_tblCompletedLabelDimension]
GO
/****** Object:  ForeignKey [FK_tblCompletedLabelWorkingDocumentDimension_tblCompletedWorkingDocumentFact]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[tblCompletedLabelWorkingDocumentDimension]  WITH CHECK ADD  CONSTRAINT [FK_tblCompletedLabelWorkingDocumentDimension_tblCompletedWorkingDocumentFact] FOREIGN KEY([CompletedWorkingDocumentFactId])
REFERENCES [dbo].[tblCompletedWorkingDocumentFact] ([WorkingDocumentId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblCompletedLabelWorkingDocumentDimension] CHECK CONSTRAINT [FK_tblCompletedLabelWorkingDocumentDimension_tblCompletedWorkingDocumentFact]
GO
/****** Object:  ForeignKey [FK_QRTZ_SIMPROP_TRIGGERS_QRTZ_TRIGGERS]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[QRTZ_SIMPROP_TRIGGERS]  WITH CHECK ADD  CONSTRAINT [FK_QRTZ_SIMPROP_TRIGGERS_QRTZ_TRIGGERS] FOREIGN KEY([SCHED_NAME], [TRIGGER_NAME], [TRIGGER_GROUP])
REFERENCES [dbo].[QRTZ_TRIGGERS] ([SCHED_NAME], [TRIGGER_NAME], [TRIGGER_GROUP])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[QRTZ_SIMPROP_TRIGGERS] CHECK CONSTRAINT [FK_QRTZ_SIMPROP_TRIGGERS_QRTZ_TRIGGERS]
GO
/****** Object:  ForeignKey [FK_QRTZ_SIMPLE_TRIGGERS_QRTZ_TRIGGERS]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[QRTZ_SIMPLE_TRIGGERS]  WITH CHECK ADD  CONSTRAINT [FK_QRTZ_SIMPLE_TRIGGERS_QRTZ_TRIGGERS] FOREIGN KEY([SCHED_NAME], [TRIGGER_NAME], [TRIGGER_GROUP])
REFERENCES [dbo].[QRTZ_TRIGGERS] ([SCHED_NAME], [TRIGGER_NAME], [TRIGGER_GROUP])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[QRTZ_SIMPLE_TRIGGERS] CHECK CONSTRAINT [FK_QRTZ_SIMPLE_TRIGGERS_QRTZ_TRIGGERS]
GO
/****** Object:  ForeignKey [FK_QRTZ_CRON_TRIGGERS_QRTZ_TRIGGERS]    Script Date: 03/03/2013 11:18:45 ******/
ALTER TABLE [dbo].[QRTZ_CRON_TRIGGERS]  WITH CHECK ADD  CONSTRAINT [FK_QRTZ_CRON_TRIGGERS_QRTZ_TRIGGERS] FOREIGN KEY([SCHED_NAME], [TRIGGER_NAME], [TRIGGER_GROUP])
REFERENCES [dbo].[QRTZ_TRIGGERS] ([SCHED_NAME], [TRIGGER_NAME], [TRIGGER_GROUP])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[QRTZ_CRON_TRIGGERS] CHECK CONSTRAINT [FK_QRTZ_CRON_TRIGGERS_QRTZ_TRIGGERS]
GO
