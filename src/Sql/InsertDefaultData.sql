﻿BEGIN TRAN

IF NOT EXISTS (SELECT * FROM Applications WHERE ApplicationId = 'c2a59fc5-96fd-4881-abeb-9b753db84d3c') 
BEGIN
	PRINT 'Inserting Application'
	Insert into Applications (ApplicationName, ApplicationId) Values ('/', 'c2a59fc5-96fd-4881-abeb-9b753db84d3c')
END

PRINT 'Merging Roles'
MERGE INTO Roles t USING (VALUES 
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', 'c9cfae8a-37da-4561-b9d5-2017af0bb841', 'User'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', 'ccf45535-3c4d-44ad-81b8-6b27d882dce2', 'Navigation'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', 'b477fbb6-ab92-4b3c-a360-8d94e3a623a5', 'ResourceManager'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', 'c07e81b4-3423-48b1-b855-1cba151f52c1', 'PublishingManager'), 
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', '5fbb1f2d-b85c-43fd-878c-a372cb1c3bf7', 'CoreReports'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', 'F83F2539-0AA4-4E85-8330-8331B1A96FAB', 'OrganisationRoles'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', '6E26F79F-DDF0-4BAA-A3B1-5211CD9E35B4', 'OrganisationHierarchy'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', '102aebf1-c1db-4b8c-9f2f-bccec0e1cec9', 'LabelManager'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', '592C0E4F-E0A8-480D-AD97-5AD424AB630F', 'CompanyManager'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', 'ef72aa97-c769-4466-896a-f9ef71c20386', 'MembershipManager'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', 'FB0B9537-D759-4012-8EF1-E732B1A50800', 'MembershipImportManager'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', 'BE7F2A15-E48D-4A55-B5E2-86EA72387C51', 'MembershipTypeManager'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', '83A215E5-C48C-4625-AD1C-45B684B16532', 'MediaManager'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', '579B9CE1-2C9B-4DB2-9E02-D804513EA37C', 'Settings'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', '0D632D32-A698-4974-B1DE-302493437E52', 'TaskTypeManager'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', 'EE06715D-C462-449A-8312-678E0E2DA03C', 'RosterManager'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', '8A665395-467A-4E84-9F56-43BE21B7CAC0', 'Roles'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', '49FFA168-7C8A-4417-9450-7BAFDB9183F1', 'NewsFeed'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', '69D2D548-CA38-4A30-8EB2-2201BE22C18E', 'LanguageManager'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', '2B15FD1D-628B-4820-A9AF-61D8876145B1', 'ScheduleManager'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', 'CD776A53-5659-4616-B846-994F0C909296', 'ScheduleApprovalManager'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', '3A812317-CA34-49D6-AB8C-C86E3CD71572', 'CronManager'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', 'E917FA1A-C307-4BC3-A8C2-25211EEE8B6E', 'NotLimitedToCompany'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', '5E4566FE-7945-4877-9137-103F4927298A', 'NotLimitedToHierarchy'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', '2FA782E1-3150-4309-BE52-CD8116881591', 'EndOfDayManager'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', '11403898-AE62-4E8C-B451-731E1CEAE022', 'Developer'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', '2A3184CD-74D1-4F48-BB8A-EF663832929E', 'ViewReports'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', '628D31DD-71DF-42CC-88A1-59BD3B412AD1', 'DeviceManagement'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', '8EBEACD8-549D-454B-A3F6-3AFD840B0F2F', 'WorkLoading'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', 'ED3B9B36-C35E-4594-BD23-F0D4E8B9C706', 'TaskUtilityOperationsAssignTasksToOthers'),	
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', '49C54814-A5A1-4382-B308-2C7A2A4F353F', 'TaskUtilityOperationsAcquireTasksFromOthers'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', '9B6BEF9A-D9EE-444E-8E2C-D5E3651F1231', 'TaskUtilityOperationsCancel'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', '9F4E91C3-E177-4C14-B6C2-82218C688174', 'TaskUtilityOperationsFinishByManagement'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', 'E2E1E00A-B2DD-4A23-8C1C-72D08E094396', 'TaskUtilityOperationsDelete'),		
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', '16718F03-8525-45ED-B1BF-FD301577B56C', 'Report Tickets'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', '42F2EAF0-4510-4F5D-81EE-6F8842889F9B', 'UserBillingType'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', 'E671F91E-B00B-42A9-9FBF-18965FE0F7C5', 'InvoiceAdmin'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', '0BD27B6B-31CD-45C9-8A8D-798A348FAD35', 'TabletAdmin'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', 'C709E776-0B37-4522-936E-967B8CC2B783', 'AssetAdmin'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', '40CA2CD1-33F7-4326-9964-EDBF6B2BB2F9', 'AssetTypeAdmin'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', 'F12B3694-4BB3-4DFB-914D-C90D4ADFD1A5', 'TimesheetItemTypeManager'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', '1EB2F4CC-D54B-450A-A2E8-A33D8196D3F1', 'FacilityStructureAdmin'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', '6A6ADEE8-CBAF-409B-8E16-70DEA67CC4EF', 'ProductManager'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', 'B6FE0146-2D36-4D00-A4FB-DBDB65DDB8F2', 'CanSideStepHierarchy'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', 'A8ACE172-712C-4EAB-89E2-6337DD47C9E5', 'JsFunctionManager'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', '374BB00F-2463-4E58-82CE-5A8F16DED02C', 'TabletProfileAdmin'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', 'B4659744-B889-43D6-8663-DCB7A4BE3655', 'DeleteTenant'),
	) AS s (ApplicationId, RoleId, RoleName) ON t.ApplicationId = s.ApplicationId AND t.RoleId = s.RoleId
	WHEN MATCHED THEN UPDATE SET RoleName = s.RoleName
	WHEN NOT MATCHED THEN INSERT (ApplicationId, RoleId, RoleName) VALUES (s.ApplicationId, s.RoleId, s.RoleName);


SET IDENTITY_INSERT [dbo].[tblUserType] ON
IF NOT EXISTS (SELECT * FROM tblUserType WHERE Id = 1) 
BEGIN
	PRINT 'Adding User Type'
	Insert into tblUserType (Id, Name) Values (1, 'User')
END

IF NOT EXISTS (SELECT * FROM tblUserType WHERE Id = 2) 
BEGIN
	PRINT 'Adding Site Type'
	Insert into tblUserType (Id, Name) Values (2, 'Site')
END
SET IDENTITY_INSERT [dbo].[tblUserType] OFF

PRINT 'Merging Publishing Group Actor Types'
MERGE INTO tblPublishingGroupActorType t USING (VALUES 
		(1, 'Reviewer'),
		(2, 'Reviewee')
	) AS s (Id, Name) ON t.Id = s.Id 
	WHEN MATCHED THEN UPDATE SET Name = s.Name
	WHEN NOT MATCHED THEN INSERT (Id, Name) VALUES (s.Id, s.Name);

PRINT 'Merging WorkingDocument Statuses'
MERGE INTO tblWorkingDocumentStatus t USING (VALUES 
		(1, 'Started'), 
		(2, 'CompletedClient'), 
		(3, 'CompletedServer')
	) AS s (Id, [Text]) ON t.Id = s.Id 
	WHEN MATCHED THEN UPDATE SET [Text] = s.[Text]
	WHEN NOT MATCHED THEN INSERT (Id, [Text]) VALUES (s.Id, s.[Text]);


IF NOT EXISTS (SELECT * FROM Users WHERE UserName = 'sa') 
BEGIN
	PRINT 'Creating sa user'
	Insert into Users (ApplicationId, UserId, UserName, IsAnonymous, LastActivityDate) Values ('c2a59fc5-96fd-4881-abeb-9b753db84d3c', 'd32503e1-b4b8-4b4c-9903-a5656ad5a71f', 'sa', 0, '2012-02-21 12:00:59.653')
	Insert into Memberships (ApplicationId, UserId, Password, PasswordFormat, PasswordSalt, Email, IsApproved, IsLockedOut, CreateDate, LastLoginDate, LastPasswordChangedDate, LastLockoutDate, FailedPasswordAttemptCount, FailedPasswordAttemptWindowStart, FailedPasswordAnswerAttemptCount, FailedPasswordAnswerAttemptWindowsStart) Values ('c2a59fc5-96fd-4881-abeb-9b753db84d3c', 'd32503e1-b4b8-4b4c-9903-a5656ad5a71f', 'b!teme5uarez', 0, 'o1/opPSkSeX1EVFvB3PMrg==', 'neil.fillingham@virtualmgr.co.uk', 1, 0, getdate(), getdate(), getdate(), '1754-01-01 00:00:00.000', 0, '1754-01-01 00:00:00.000', 0, '1754-01-01 00:00:00.000')

	Insert into UsersInRoles (UserId, RoleId) Values ('d32503e1-b4b8-4b4c-9903-a5656ad5a71f', 'c07e81b4-3423-48b1-b855-1cba151f52c1')
	Insert into UsersInRoles (UserId, RoleId) Values ('d32503e1-b4b8-4b4c-9903-a5656ad5a71f', 'c9cfae8a-37da-4561-b9d5-2017af0bb841')
	Insert into UsersInRoles (UserId, RoleId) Values ('d32503e1-b4b8-4b4c-9903-a5656ad5a71f', 'b477fbb6-ab92-4b3c-a360-8d94e3a623a5')
	Insert into UsersInRoles (UserId, RoleId) Values ('d32503e1-b4b8-4b4c-9903-a5656ad5a71f', '5fbb1f2d-b85c-43fd-878c-a372cb1c3bf7')
	Insert into UsersInRoles (UserId, RoleId) Values ('d32503e1-b4b8-4b4c-9903-a5656ad5a71f', '102aebf1-c1db-4b8c-9f2f-bccec0e1cec9')
	Insert into UsersInRoles (UserId, RoleId) Values ('d32503e1-b4b8-4b4c-9903-a5656ad5a71f', 'ef72aa97-c769-4466-896a-f9ef71c20386')
	Insert into UsersInRoles (UserId, RoleId) Values ('d32503e1-b4b8-4b4c-9903-a5656ad5a71f', '579B9CE1-2C9B-4DB2-9E02-D804513EA37C')
	Insert into UsersInRoles (UserId, RoleId) Values ('d32503e1-b4b8-4b4c-9903-a5656ad5a71f', 'ccf45535-3c4d-44ad-81b8-6b27d882dce2')
	Insert into UsersInRoles (UserId, RoleId) Values ('d32503e1-b4b8-4b4c-9903-a5656ad5a71f', '42F2EAF0-4510-4F5D-81EE-6F8842889F9B')
	Insert into UsersInRoles (UserId, RoleId) Values ('d32503e1-b4b8-4b4c-9903-a5656ad5a71f', 'E671F91E-B00B-42A9-9FBF-18965FE0F7C5')
	Insert into UsersInRoles (UserId, RoleId) Values ('d32503e1-b4b8-4b4c-9903-a5656ad5a71f', '0BD27B6B-31CD-45C9-8A8D-798A348FAD35')
END

PRINT 'Updating Super User'
Update tblUserData set Name = 'Super User' where userid = 'd32503e1-b4b8-4b4c-9903-a5656ad5a71f'

SET IDENTITY_INSERT [dbo].[tblMimeType] ON
PRINT 'Merging MimeTypes'
MERGE INTO tblMimeType t USING (VALUES
	(1, N'jpg', N'image/jpeg', N'ImageResizer.Plugins.MediaReader.PassThroughPreviewGenerator, ConceptCave.www, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null'),
	(2, N'jpeg', N'image/jpeg', N'ImageResizer.Plugins.MediaReader.PassThroughPreviewGenerator, ConceptCave.www, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null'),
	(3, N'gif', N'image/gif', N'ImageResizer.Plugins.MediaReader.PassThroughPreviewGenerator, ConceptCave.www, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null'),
	(4, N'png', N'image/png', N'ImageResizer.Plugins.MediaReader.PassThroughPreviewGenerator, ConceptCave.www, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null'),
	(5, N'js', N'text/javascript', N'ImageResizer.Plugins.MediaReader.PassThroughPreviewGenerator, ConceptCave.www, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null'),
	(6, N'css', N'text/css', N'ImageResizer.Plugins.MediaReader.PassThroughPreviewGenerator, ConceptCave.www, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null'),
	(7, N'ico', N'image/x-icon', N'ImageResizer.Plugins.MediaReader.PassThroughPreviewGenerator, ConceptCave.www, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null'),
	(8, N'doc', N'application/msword', N'ImageResizer.Plugins.MediaReader.PassThroughPreviewGenerator, ConceptCave.www, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null'),
	(9, N'docx', N'application/vnd.openxmlformats-officedocument.wordprocessingml.document', N'ImageResizer.Plugins.MediaReader.PassThroughPreviewGenerator, ConceptCave.www, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null'),
	(10, N'pdf', N'application/pdf', N'ImageResizer.Plugins.MediaReader.PassThroughPreviewGenerator, ConceptCave.www, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null'),
	(11, N'eot', N'application/vmd.ms-fontobject', NULL),
	(12, N'ttf', N'application/octet-stream', NULL),
	(13, N'woff', N'application/font-woff', NULL),
	(14, N'otf', N'application/x-font-otf', NULL),
	(15, N'xlsx', N'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', NULL)
	) AS s (Id, Extension, MimeType, MediaPreviewGenerator) ON t.Id = s.Id
	WHEN MATCHED THEN UPDATE SET Extension = s.Extension, MimeType = s.MimeType, MediaPreviewGenerator = s.MediaPreviewGenerator
	WHEN NOT MATCHED THEN INSERT ([Id], [Extension], [MimeType], [MediaPreviewGenerator]) VALUES (s.Id, s.Extension, s.MimeType, s.MediaPreviewGenerator);

SET IDENTITY_INSERT [dbo].[tblMimeType] OFF

SET IDENTITY_INSERT [dbo].[tblGlobalSetting] ON

PRINT 'Merging Global Settings'
MERGE INTO tblGlobalSetting t USING (VALUES
	(2, N'Name', N'BWorkflow'),
	(3, N'ThemeMediaId', NULL),
	(4, N'SiteId', NULL),
	(5, N'ScheduledProcessEnabled', N'false'),
	(6, N'ScheduledProcessThreadCount', N'5'),
	(7, N'UtcLastModified', N'5/10/2012 10:31:32 AM'),
	(8, N'StoreLoginCoordinates', N'True'),
	(9, N'StoreLoginSite', N'False')
	) AS s ([Id], [Name], [Value]) ON t.Name = s.Name
	WHEN NOT MATCHED THEN INSERT ([Id], [Name], [Value]) VALUES (s.Id, s.Name, s.Value);

PRINT 'Merging English Language'
MERGE INTO tblLanguage t USING (VALUES
	(N'English', N'English', N'en'))
	AS s ([LanguageName], [NativeName], [CultureName]) ON t.CultureName = s.CultureName
	WHEN NOT MATCHED THEN INSERT ([LanguageName], [NativeName], [CultureName]) VALUES (s.LanguageName, s.NativeName, s.CultureName);

SET IDENTITY_INSERT [dbo].[tblGlobalSetting] OFF

PRINT 'Merging Country States'
MERGE INTO [gce].tblCountryState t USING (VALUES 
		('1', 'Victoria', 'VIC', 'AUS Eastern Standard Time'),
		('2', 'New South Wales', 'NSW', 'AUS Eastern Standard Time'),
		('3', 'Tasmania', 'TAS', 'AUS Eastern Standard Time'),
		('4', 'Australian Capital Territory', 'ACT', 'AUS Eastern Standard Time'),
		('5', 'Queensland', 'QLD', 'E. Australia Standard Time'),
		('6', 'Northern Territory', 'NT', 'AUS Central Standard Time'),
		('7', 'South Australia', 'SA', 'Cen. Australia Standard Time'),
		('8', 'Western Australia', 'WA', 'W. Australia Standard Time')
	) AS s (Id, Name, Abbreviation, TimeZone) ON t.Id = s.Id
	WHEN MATCHED THEN UPDATE SET Name = s.Name, Abbreviation = s.Abbreviation, TimeZone = s.TimeZone
	WHEN NOT MATCHED THEN INSERT (Id, Name, Abbreviation, TimeZone) VALUES (s.Id, s.Name, s.Abbreviation, s.TimeZone);

PRINT 'Merging MeasurementUnits'
MERGE INTO [wl].[tblMeasurementUnit] t USING (VALUES 
		(0, 'count', 'count', 1, 0),
		(1, 'm', 'metres', 1, 1),
		(2, 'ft', 'feet', 0.3048, 1),
		(3, 'yd', 'yards', 0.9144, 1),
		(4, 'm²', 'square metres', 1, 2),
		(5, 'ft²', 'square feet', 0.3048 * 0.3048, 2),
		(6, 'm³', 'cubic metres', 1, 3),
		(7, 'ft³', 'cubic feet', 0.3048 * 0.3048 * 0.3048, 3),
		(8, 'l', 'litres', 0.001, 3),
		(9, 'gallon', 'us gallon', 0.003785411784, 3)
	) AS s (Id, ShortName, LongName, PerMetric, Dimensions) ON t.Id = s.Id
	WHEN MATCHED THEN UPDATE SET ShortName = s.ShortName, LongName = s.LongName, PerMetric = s.PerMetric, Dimensions = s.Dimensions
	WHEN NOT MATCHED THEN INSERT (Id, ShortName, LongName, PerMetric, Dimensions) VALUES (s.Id, s.ShortName, s.LongName, s.PerMetric, s.Dimensions);
	
-- Insert default Pause Statuses
PRINT 'Merging Project Job Finished Statuses'
MERGE INTO [gce].[tblProjectJobFinishedStatus] t USING (VALUES 
		('9D4B5EB1-8B05-403D-8F7B-A586035CA6AA', 'Break', 0, 1),
		('673E3CD9-07E8-46CC-BF6A-6FF124379565', 'Lunch', 0, 1),
		('A7DF9443-BE33-4739-9FE7-6D06452B8AEE', 'Paged', 0, 1),
		('42469F85-FC2B-42A6-9A11-ABDA47D551B6', 'Other', 1, 1)
	) AS s (Id, [Text], RequiresExtraNotes, Stage) ON t.Id = s.Id
	WHEN MATCHED THEN UPDATE SET [Text] = s.[Text], RequiresExtraNotes = s.RequiresExtraNotes, Stage = s.Stage
	WHEN NOT MATCHED THEN INSERT (Id, [Text], RequiresExtraNotes, Stage) VALUES (s.Id, s.[Text], s.RequiresExtraNotes, s.Stage);

PRINT 'Merging Roster Project Job Task Events'
MERGE INTO [gce].[tblRosterProjectJobTaskEvent] t USING (VALUES 
		(1, 'Schedule Approval Automatic'),
		(2, 'End of Day Automatic')
	) AS s (Id, [Name]) ON t.Id = s.Id
	WHEN MATCHED THEN UPDATE SET [Id] = s.[Id], Name = s.Name
	WHEN NOT MATCHED THEN INSERT (Id, [Name]) VALUES (s.Id, s.[Name]);
	
SET IDENTITY_INSERT [gce].[tblProjectJobTaskRelationshipType] ON
PRINT 'Merging Project Job Task Relationship Types'
MERGE INTO [gce].[tblProjectJobTaskRelationshipType] t USING (VALUES 
		(1, 'Audit')
	) AS s (Id, [Name]) ON t.Id = s.Id
	WHEN MATCHED THEN UPDATE SET Name = s.Name
	WHEN NOT MATCHED THEN INSERT (Id, [Name]) VALUES (s.Id, s.[Name]);
SET IDENTITY_INSERT [gce].[tblProjectJobTaskRelationshipType] OFF
	
-- Pause Statuses are included in all current Task Types
PRINT 'Creating Pause Statuses for current Task Types'
DECLARE taskTypeCursor CURSOR FOR SELECT Id FROM [gce].tblProjectJobTaskType
OPEN taskTypeCursor
DECLARE @taskTypeID UNIQUEIDENTIFIER
FETCH NEXT FROM taskTypeCursor INTO @taskTypeID
WHILE @@FETCH_STATUS = 0
BEGIN
	INSERT INTO [gce].[tblProjectJobTaskTypeFinishedStatus] (ID, TaskTypeId, ProjectJobFinishedStatusId)
		SELECT NEWID(), @taskTypeID, t2.Id FROM [gce].[tblProjectJobFinishedStatus] t2
		WHERE t2.Stage = 1 AND NOT EXISTS (SELECT Id FROM [gce].[tblProjectJobTaskTypeFinishedStatus] t3 WHERE t3.TaskTypeId = @taskTypeID AND t3.ProjectJobFinishedStatusId = t2.Id)
	FETCH NEXT FROM taskTypeCursor INTO @taskTypeID
END
CLOSE taskTypeCursor
DEALLOCATE taskTypeCursor

COMMIT
--ROLLBACK
