UPDATE [tblWorkingDocumentStatus]
SET [Text] = 'CompletedClient'
WHERE [Id] = 2

INSERT INTO [tblWorkingDocumentStatus] ([Id], [Text])
VALUES (3, 'CompletedServer')

UPDATE [tblWorkingDocument]
SET [Status] = 3
WHERE [Status] = 2