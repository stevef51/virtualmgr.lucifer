﻿using eWAY.Rapid.Enums;
using eWAY.Rapid.Models;
using System;

namespace eWAY.Rapid
{
    public interface IRapidClientSync
    {
        CancelAuthorisationResponse CancelAuthorisation(CancelAuthorisationRequest cancelRequest);
        CapturePaymentResponse CapturePayment(CapturePaymentRequest captureRequest);
        CreateTransactionResponse Create(PaymentMethod paymentMethod, Transaction transaction);
        CreateCustomerResponse Create(PaymentMethod paymentMethod, Customer customer);
        QueryCustomerResponse QueryCustomer(long tokenCustomerId);
        QueryTransactionResponse QueryInvoiceNumber(string invoiceNumber);
        QueryTransactionResponse QueryInvoiceRef(string invoiceRef);
        QueryTransactionResponse QueryTransaction(TransactionFilter filter);
        QueryTransactionResponse QueryTransaction(int transactionId);
        QueryTransactionResponse QueryTransaction(long transactionId);
        QueryTransactionResponse QueryTransaction(string accessCode);
        RefundResponse Refund(Refund refund);
        SettlementSearchResponse SettlementSearch(SettlementSearchRequest settlementSearchRequest);
        CreateCustomerResponse UpdateCustomer(PaymentMethod paymentMethod, Customer customer);
    }
}
