﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using eWAY.Rapid.Enums;
using eWAY.Rapid.Models;

namespace eWAY.Rapid
{
    public class RapidClientSync : IRapidClientSync
    {
        private readonly IRapidClient _client;

        public RapidClientSync(IRapidClient client)
        {
            _client = client;
        }

        public CancelAuthorisationResponse CancelAuthorisation(CancelAuthorisationRequest cancelRequest)
        {
            return Task.Run(async () => await _client.CancelAuthorisationAsync(cancelRequest)).Result;
        }

        public CapturePaymentResponse CapturePayment(CapturePaymentRequest captureRequest)
        {
            return Task.Run(async () => await _client.CapturePaymentAsync(captureRequest)).Result;
        }

        public CreateTransactionResponse Create(PaymentMethod paymentMethod, Transaction transaction)
        {
            return Task.Run(async () => await _client.CreateAsync(paymentMethod, transaction)).Result;
        }

        public CreateCustomerResponse Create(PaymentMethod paymentMethod, Customer customer)
        {
            return Task.Run(async () => await _client.CreateAsync(paymentMethod, customer)).Result;
        }

        public QueryCustomerResponse QueryCustomer(long tokenCustomerId)
        {
            return Task.Run(async () => await _client.QueryCustomerAsync(tokenCustomerId)).Result;
        }

        public QueryTransactionResponse QueryInvoiceNumber(string invoiceNumber)
        {
            return Task.Run(async () => await _client.QueryInvoiceNumberAsync(invoiceNumber)).Result;
        }

        public QueryTransactionResponse QueryInvoiceRef(string invoiceRef)
        {
            return Task.Run(async () => await _client.QueryInvoiceRefAsync(invoiceRef)).Result;
        }

        public QueryTransactionResponse QueryTransaction(TransactionFilter filter)
        {
            return Task.Run(async () => await _client.QueryTransactionAsync(filter)).Result;
        }

        public QueryTransactionResponse QueryTransaction(int transactionId)
        {
            return Task.Run(async () => await _client.QueryTransactionAsync(transactionId)).Result;
        }

        public QueryTransactionResponse QueryTransaction(long transactionId)
        {
            return Task.Run(async () => await _client.QueryTransactionAsync(transactionId)).Result;
        }

        public QueryTransactionResponse QueryTransaction(string accessCode)
        {
            return Task.Run(async () => await _client.QueryTransactionAsync(accessCode)).Result;
        }

        public RefundResponse Refund(Refund refund)
        {
            return Task.Run(async () => await _client.RefundAsync(refund)).Result;
        }

        public SettlementSearchResponse SettlementSearch(SettlementSearchRequest settlementSearchRequest)
        {
            return Task.Run(async () => await _client.SettlementSearchAsync(settlementSearchRequest)).Result;
        }

        public CreateCustomerResponse UpdateCustomer(PaymentMethod paymentMethod, Customer customer)
        {
            return Task.Run(async () => await _client.UpdateCustomerAsync(paymentMethod, customer)).Result;
        }
    }
}
