namespace VirtualMgr.MassTransit
{
    public class MassTransitOptions
    {
        public enum TransportType
        {
            RabbitMQ = 1,
            AzureServiceBus = 2
        }

        public TransportType Transport { get; set; }
        public RabbitMQOptions RabbitMQ { get; set; }
        public AzureServiceBusOptions AzureServiceBus { get; set; }
    }
}