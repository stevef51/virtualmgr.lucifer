namespace VirtualMgr.MassTransit
{
    public class RabbitMQOptions
    {
        public string Server { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}