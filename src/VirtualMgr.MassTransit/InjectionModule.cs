using Microsoft.Extensions.DependencyInjection;
using MassTransit;
using MassTransit.AspNetCoreIntegration;
using System;
using Microsoft.Extensions.Configuration;
using MassTransit.Context;
using Microsoft.Extensions.Logging;
using MassTransit.RabbitMqTransport;
using MassTransit.ExtensionsDependencyInjectionIntegration;
using MassTransit.Azure.ServiceBus.Core;
using VirtualMgr.MultiTenant.Consumers;
using Microsoft.Extensions.Options;
using VirtualMgr.MassTransit.Contracts;

namespace VirtualMgr.MassTransit
{
    public class InjectionModule : ServiceCollection
    {
        public InjectionModule(
            IConfiguration configuration,
            string serviceName,
            Action<IReceiveEndpointConfigurator, IServiceProvider> configureTempEndpoint = null,
            Action<IBusFactoryConfigurator, IServiceProvider> configureBus = null)
        {
            this.Configure<MassTransitOptions>(configuration.GetSection("MassTransit"));

            this.AddMassTransit(serviceProvider =>
            {
                var massTransitOptions = serviceProvider.GetRequiredService<IOptions<MassTransitOptions>>().Value;

                Func<RabbitMQOptions, IBusControl> createRabbitMQBus = (opts) =>
                {
                    var bus = Bus.Factory.CreateUsingRabbitMq(cfg =>
                    {
                        cfg.Host(new Uri($"rabbitmq://{opts.Server}"), h =>
                        {
                            h.Username(opts.Username);
                            h.Password(opts.Password);
                        });

                        // Setup a "temp" endpoint for this service, the endpoint (exchange + queue) will auto delete when we disconnect
                        // and also messages sent to this endpoint are not persisted
                        cfg.ReceiveEndpoint($"temp-{serviceName}-{Guid.NewGuid()}", x =>
                        {
                            x.Durable = false;
                            x.AutoDelete = true;

                            configureTempEndpoint?.Invoke(x, serviceProvider);

                            // MultiTenant consumes this event to clear its internal cache of Tenants
                            x.Consumer<TenantConnectionsUpdatedConsumer>(serviceProvider);
                        });

                        configureBus?.Invoke(cfg, serviceProvider);
                    });

                    return bus;
                };

                Func<AzureServiceBusOptions, IBusControl> createAzureServiceBus = (opts) =>
                {
                    var bus = Bus.Factory.CreateUsingAzureServiceBus(cfg =>
                    {
                        var host = cfg.Host(new Uri($"sb://{opts.Namespace}.servicebus.windows.net"), h =>
                        {
                            h.SharedAccessSignature(s =>
                            {
                                s.KeyName = opts.KeyName;
                                s.SharedAccessKey = opts.SharedAccessKey;
                                s.TokenTimeToLive = opts.TokenTimeToLive;
                                s.TokenScope = Microsoft.Azure.ServiceBus.Primitives.TokenScope.Namespace;
                            });
                        });

                        // Setup a "temp" endpoint for this service, the endpoint (exchange + queue) will auto delete when we disconnect
                        // and also messages sent to this endpoint are not persisted
                        cfg.SubscriptionEndpoint<TenantConnectionsUpdated>($"{serviceName}-{DateTime.UtcNow.Ticks}-{new System.Random().Next(100000)}", x =>
                        {
                            x.AutoDeleteOnIdle = new TimeSpan(0, 5, 0);
                            // MultiTenant consumes this event to clear its internal cache of Tenants
                            x.Consumer<TenantConnectionsUpdatedConsumer>(serviceProvider);
                        });

                        configureBus?.Invoke(cfg, serviceProvider);
                    });

                    return bus;
                };

                switch (massTransitOptions.Transport)
                {
                    case MassTransitOptions.TransportType.RabbitMQ:
                        return createRabbitMQBus(massTransitOptions.RabbitMQ);

                    case MassTransitOptions.TransportType.AzureServiceBus:
                        return createAzureServiceBus(massTransitOptions.AzureServiceBus);

                    default:
                        throw new ApplicationException("MassTransit must be configured");
                }
            });
        }
    }
}