using System;

namespace VirtualMgr.MassTransit
{
    public class AzureServiceBusOptions
    {
        public string Namespace { get; set; }
        public string KeyName { get; set; }
        public string SharedAccessKey { get; set; }
        public TimeSpan TokenTimeToLive { get; set; }
    }
}