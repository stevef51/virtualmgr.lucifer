﻿//Extension methods to convert from entities to DTO's and vice versa
using System;
using System.Collections.Generic;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.Data.DTOConverters
{
	public static class <[CurrentEntityName]>Converter
	{
	
		public static <[CurrentEntityName]>Entity ToEntity(this <[CurrentEntityName]>DTO dto)
		{
			return dto.ToEntity(new <[CurrentEntityName]>Entity(), new Dictionary<object, object>());
		}

		public static <[CurrentEntityName]>Entity ToEntity(this <[CurrentEntityName]>DTO dto, <[CurrentEntityName]>Entity ent)
		{
			return dto.ToEntity(ent, new Dictionary<object, object>());
		}
		
		public static <[CurrentEntityName]>Entity ToEntity(this <[CurrentEntityName]>DTO dto, 
			IDictionary<object, object> cache)
		{
			return dto.ToEntity(new <[CurrentEntityName]>Entity(), cache);
		}
	
		public static <[CurrentEntityName]>DTO ToDTO(this <[CurrentEntityName]>Entity ent)
		{
			return ent.ToDTO(new Dictionary<object, object>());
		}

		public static <[CurrentEntityName]>Entity ToEntity(this <[CurrentEntityName]>DTO dto, <[CurrentEntityName]>Entity newEnt,
			IDictionary<object, object> cache)
		{
			if (dto == null) return null;
			if (cache.ContainsKey(dto)) return (<[CurrentEntityName]>Entity)cache[dto];
		
			newEnt.IsNew = dto.__IsNew;
			newEnt.IsDirty = true;
			
			<[Foreach EntityField CrLf]>
			<[If Not IsReadOnly]>
			<[If IsNullable]>
			if (dto.<[EntityFieldName]> == null) 
			{
				// Work around LLBLGen bug - set nullable field to a none null value before assigning null
				<[If IsStringField]>
				newEnt.SetNewFieldValue((int)<[CurrentEntityName]>FieldIndex.<[EntityFieldName]>, "");
				<[Else]>
				newEnt.SetNewFieldValue((int)<[CurrentEntityName]>FieldIndex.<[EntityFieldName]>, default(<[TypeOfField]>));
				<[EndIf]>
			}
			<[EndIf]>
			newEnt.<[EntityFieldName]> = dto.<[EntityFieldName]>;
			<[Else]>
			<[If IsPrimaryKey]>
			newEnt.<[EntityFieldName]> = dto.<[EntityFieldName]>;
			<[EndIf]>
			<[EndIf]>
			<[NextForeach]>
			
			cache.Add(dto, newEnt);
			
			<[Foreach RelatedEntity OneToOne]>
			newEnt.<[MappedFieldNameRelation]> = dto.<[MappedFieldNameRelation]>.ToEntity(cache);
			<[NextForeach]>
			
			<[Foreach RelatedEntity ManyToOne]>
			newEnt.<[MappedFieldNameRelation]> = dto.<[MappedFieldNameRelation]>.ToEntity(cache);
			<[NextForeach]>
			
			<[Foreach RelatedEntity OneToMany]>
			foreach (var related in dto.<[MappedFieldNameRelation]>)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.<[MappedFieldNameRelation]>.Contains(relatedEntity))
				{
					newEnt.<[MappedFieldNameRelation]>.Add(relatedEntity);
				}
			}
			<[NextForeach]>
		
			<[Foreach RelatedEntity ManyToMany]>
			foreach (var related in dto.<[MappedFieldNameRelation]>)
			{
				var relatedEntity = related.ToEntity(cache);
				if (!newEnt.<[MappedFieldNameRelation]>.Contains(relatedEntity))
				{
					newEnt.<[MappedFieldNameRelation]>.Add(relatedEntity);
				}
			}
			<[NextForeach]>
			
			cache[dto] = newEnt;
			return newEnt;
		}
		
		public static <[CurrentEntityName]>DTO ToDTO(this <[CurrentEntityName]>Entity ent,
			IDictionary<object, object> cache)
		{
			if (ent == null) return null;
			if (cache.ContainsKey(ent)) return (<[CurrentEntityName]>DTO)cache[ent];
			
			var newDTO = new <[CurrentEntityName]>DTO();
			newDTO.__IsNew = ent.IsNew;
			
			<[Foreach EntityField CrLf]>
			newDTO.<[EntityFieldName]> = ent.<[EntityFieldName]>;
			<[NextForeach]>
			
			
			cache.Add(ent, newDTO);
			
			<[Foreach RelatedEntity OneToOne]>
			newDTO.<[MappedFieldNameRelation]> = ent.<[MappedFieldNameRelation]>.ToDTO(cache);
			<[NextForeach]>
			
			<[Foreach RelatedEntity ManyToOne]>
			newDTO.<[MappedFieldNameRelation]> = ent.<[MappedFieldNameRelation]>.ToDTO(cache);
			<[NextForeach]>
			
			<[Foreach RelatedEntity OneToMany]>
			foreach (var related in ent.<[MappedFieldNameRelation]>)
			{
				newDTO.<[MappedFieldNameRelation]>.Add(related.ToDTO(cache));
			}
			<[NextForeach]>
		
			<[Foreach RelatedEntity ManyToMany]>
			foreach (var related in ent.<[MappedFieldNameRelation]>)
			{
				newDTO.<[MappedFieldNameRelation]>.Add(related.ToDTO(cache));
			}
			<[NextForeach]>
			
			cache[ent] = newDTO;
			return newDTO;
		}
	
	}
}