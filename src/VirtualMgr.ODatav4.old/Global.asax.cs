﻿using ConceptCave.Checklist.OData.Attributes;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Routing;
using VirtualMgr.Central;

namespace ConceptCave.Checklist.OData
{
    public class WebApiApplication : System.Web.HttpApplication
    {

        protected void Application_Start()
        {
            // Setup Multi-Tenancy
            Func<ConnectionStringSettings> fnConnectionStringSettings = () => new ConnectionStringSettings(MultiTenantManager.CurrentTenant.Name, MultiTenantManager.CurrentTenant.ConnectionString);

            MultiTenantManager.TenantMasterConnectionString = ConfigurationManager.ConnectionStrings["VirtualMgr.Central"].ConnectionString;
            //MultiTenantManager.TenantDataAccessAdapterType = typeof(ConceptCave.Data.DatabaseSpecific.DataAccessAdapter);
            MultiTenantManager.TenantContextProvider = new VirtualMgr.Central.MultiTenantContextProvider(() => System.Web.HttpContext.Current.Request.Url.Host.ToLower());

            GlobalConfiguration.Configure(WebApiConfig.Register);
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
            var context = HttpContext.Current;
            context.Response.SuppressFormsAuthenticationRedirect = true;
        }

        private HttpOriginMatcher _origins = new HttpOriginMatcher() { Origins = Consts.DefaultCorsAllowOrigin };

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            string origin = null;
            if (Context.Request.Headers.AllKeys.Any(h => h.ToLower() == "origin"))
                origin = (Context.Request.Headers.GetValues("origin").FirstOrDefault() ?? "").ToLower();
            if (_origins.MatchOrigin(origin))
            {
                Context.Response.Headers.Add("Access-Control-Allow-Origin", origin);
                Context.Response.Headers.Add("Access-Control-Allow-Headers", "Content-Type, bworkflow-handle-errors-generically, cache-control");
                Context.Response.Headers.Add("Access-Control-Allow-Methods", "*");
                Context.Response.Headers.Add("Access-Control-Allow-Credentials", "true");

                // If this is OPTIONS then return empty result 
                if (Context.Request.HttpMethod.ToLower() == "options")
                {
                    Context.Response.End();
                }
            }
        }
    }
}
