﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Filters;
using ConceptCave.Checklist.OData.Filters.Results;
using System.Web.Security;
using VirtualMgr.Central;

namespace ConceptCave.Checklist.OData.Filters
{
    public abstract class BasicAuthenticationAttribute : Attribute, IAuthenticationFilter
    {
        public string Realm { get; set; }

        public async Task AuthenticateAsync(HttpAuthenticationContext context, CancellationToken cancellationToken)
        {
            HttpRequestMessage request = context.Request;
            AuthenticationHeaderValue authorization = request.Headers.Authorization;


            IPrincipal principal = null;

            // attempt to use forms authentication cookie
            var cookie = GetAuthenticationCookie(request);

            if (cookie != null)
            {
                var ticket = FormsAuthentication.Decrypt(cookie.Value);

                if (ticket.Expired == false)
                {
                    principal = await AuthenticateAsync(ticket.Name, null, cancellationToken, false);
                    // Authentication was attempted and succeeded. Set Principal to the authenticated user.
                    context.Principal = principal;

                    return;
                }
            }

            if (authorization == null)
            {
                // No authentication was attempted (for this authentication method).
                // Do not set either Principal (which would indicate success) or ErrorResult (indicating an error).
                return;
            }

            if (authorization.Scheme == "Bearer")
            {
                var tm = new VirtualMgr.JwtAuth.TokenGenerator() { Secret = Encoding.UTF8.GetBytes(MultiTenantManager.CurrentTenant.BaseUrl.ToString() + "279B7487-7F3D-4E77-AE5E-FD6073508E32") };
                var token = authorization.Parameter;
                string username = null;
                if (tm.ValidateToken(token, out username))
                {
                    principal = await AuthenticateAsync(username, null, cancellationToken, false);
                    context.Principal = principal;
                    return;
                }
            }

            if (principal == null)
            { 
                if (authorization.Scheme != "Basic")
                {
                    // No authentication was attempted (for this authentication method).
                    // Do not set either Principal (which would indicate success) or ErrorResult (indicating an error).
                    return;
                }

                if (String.IsNullOrEmpty(authorization.Parameter))
                {
                    // Authentication was attempted but failed. Set ErrorResult to indicate an error.
                    context.ErrorResult = new AuthenticationFailureResult("Missing credentials", request);
                    return;
                }

                Tuple<string, string> userNameAndPasword = ExtractUserNameAndPassword(authorization.Parameter);

                if (userNameAndPasword == null)
                {
                    // Authentication was attempted but failed. Set ErrorResult to indicate an error.
                    context.ErrorResult = new AuthenticationFailureResult("Invalid credentials", request);
                    return;
                }

                string userName = userNameAndPasword.Item1;
                string password = userNameAndPasword.Item2;

                principal = await AuthenticateAsync(userName, password, cancellationToken);
            }
            if (principal == null)
            {
                // Authentication was attempted but failed. Set ErrorResult to indicate an error.
                context.ErrorResult = new AuthenticationFailureResult("Invalid username or password", request);
            }
            else
            {
                // Authentication was attempted and succeeded. Set Principal to the authenticated user.
                context.Principal = principal;
            }
        }

        protected abstract Task<IPrincipal> AuthenticateAsync(string userName, string password,
            CancellationToken cancellationToken, bool validateUsernamePassword = true);

        protected CookieState GetAuthenticationCookie(HttpRequestMessage request)
        {
            foreach (var c in request.Headers.GetCookies())
            {
                foreach (var cookie in c.Cookies)
                {
                    if (cookie.Name == FormsAuthentication.FormsCookieName)
                    {
                        return cookie;
                    }
                }
            }

            return null;
        }

        private static Tuple<string, string> ExtractUserNameAndPassword(string authorizationParameter)
        {
            byte[] credentialBytes;

            try
            {
                credentialBytes = Convert.FromBase64String(authorizationParameter);
            }
            catch (FormatException)
            {
                return null;
            }

            // The currently approved HTTP 1.1 specification says characters here are ISO-8859-1.
            // However, the current draft updated specification for HTTP 1.1 indicates this encoding is infrequently
            // used in practice and defines behavior only for ASCII.
            Encoding encoding = Encoding.ASCII;
            // Make a writable copy of the encoding to enable setting a decoder fallback.
            encoding = (Encoding)encoding.Clone();
            // Fail on invalid bytes rather than silently replacing and continuing.
            encoding.DecoderFallback = DecoderFallback.ExceptionFallback;
            string decodedCredentials;

            try
            {
                decodedCredentials = encoding.GetString(credentialBytes);
            }
            catch (DecoderFallbackException)
            {
                return null;
            }

            if (String.IsNullOrEmpty(decodedCredentials))
            {
                return null;
            }

            int colonIndex = decodedCredentials.IndexOf(':');

            if (colonIndex == -1)
            {
                return null;
            }

            string userName = decodedCredentials.Substring(0, colonIndex);
            string password = decodedCredentials.Substring(colonIndex + 1);
            return new Tuple<string, string>(userName, password);
        }

        public Task ChallengeAsync(HttpAuthenticationChallengeContext context, CancellationToken cancellationToken)
        {
            Challenge(context);
            return Task.FromResult(0);
        }

        private void Challenge(HttpAuthenticationChallengeContext context)
        {
            var cookie = GetAuthenticationCookie(context.Request);

            if (cookie != null)
            {
                var ticket = FormsAuthentication.Decrypt(cookie.Value);

                if (ticket.Expired == false)
                {
                    // no challenge
                    return;
                }
            }

            string parameter;

            if (String.IsNullOrEmpty(Realm))
            {
                parameter = null;
            }
            else
            {
                // A correct implementation should verify that Realm does not contain a quote character unless properly
                // escaped (precededed by a backslash that is not itself escaped).
                parameter = "realm=\"" + Realm + "\"";
            }

            context.ChallengeWith("Basic", parameter);
        }

        public virtual bool AllowMultiple
        {
            get { return false; }
        }
    }
}