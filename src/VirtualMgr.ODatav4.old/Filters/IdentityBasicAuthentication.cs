﻿using ConceptCave.Checklist.OData.Filters.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Filters;
using VirtualMgr.Membership;

namespace ConceptCave.Checklist.OData.Filters
{
    public class IdentityBasicAuthenticationAttribute : BasicAuthenticationAttribute
    {
        protected readonly MembershipWrapper _wrapper;
        public IdentityBasicAuthenticationAttribute(MembershipWrapper wrapper)
        {
            _wrapper = wrapper;
        }

        public IdentityBasicAuthenticationAttribute(string realm)
        {
            this.Realm = realm;
        }

        protected override async Task<IPrincipal> AuthenticateAsync(string userName, string password, CancellationToken cancellationToken, bool validateUsernamePassword = true)
        {
            cancellationToken.ThrowIfCancellationRequested(); // Unfortunately, UserManager doesn't support CancellationTokens.

            if(validateUsernamePassword == true)
            {
                if (_wrapper.ValidateUser(userName, password) == false)
                {
                    return null;
                }
            }

            GenericIdentity ident = new GenericIdentity(userName);
            GenericPrincipal princ = new GenericPrincipal(ident, System.Web.Security.Roles.GetRolesForUser(userName));

            return princ;
        }
    }
}