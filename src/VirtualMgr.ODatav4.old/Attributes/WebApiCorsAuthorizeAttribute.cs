﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Http;
using System.Web.Http.Filters;

namespace ConceptCave.Checklist.OData.Attributes
{
    public static class Consts
    {
        public const string DefaultCorsAllowOrigin = @"http://localhost:*,http://127.0.0.1:*";
        public const string SecureDomains = @"^https://.*\.getevs\.com$,^https://.*\.gethealthclean\.tech$,^https://.*\.getsmartcleaner\.org$,^https://.*\.getfoodsafe\.tech$";
    }

    public class WebApiCorsAuthorizeAttribute : AuthorizeAttribute
    {
        protected override bool IsAuthorized(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            // CORS requests send an options request down that doesn't contain
            // authentication information (cookies, headers etc). These requests
            // are done by the browser, with no control in script, so we can't
            // control this client side. Based on the HTTP Spec we can treat options
            // requests as authorized
            if (actionContext.Request.Method == System.Net.Http.HttpMethod.Options)
            {
                return true;
            }

            return base.IsAuthorized(actionContext);
        }
    }

    public class HttpOriginMatcher
    {
        private bool _anyOrigin = true;
        private string[] _origins = new string[0];
        public string Origins
        {
            get
            {
                return string.Join(",", _origins);
            }
            set
            {
                _anyOrigin = value == "*";
                _origins = value.ToLower().Split(',');
            }
        }

        public bool MatchOrigin(string origin)
        {
            if (origin == null)
                return false;

            if (_anyOrigin)
                return true;

            foreach (var testOrigin in _origins)
            {
                if (testOrigin.StartsWith("^"))
                {
                    Regex rex = new Regex(testOrigin);
                    if (rex.IsMatch(origin))
                        return true;
                }
                else
                {
                    if (testOrigin == origin)
                        return true;

                    var s = testOrigin.IndexOf('*');
                    if (s >= 0)
                    {
                        if (s > origin.Length)
                            continue;

                        if (origin.Substring(0, s) == testOrigin.Substring(0, s))
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }


    }
    public class WebApiCorsOptionsAllowAttribute : ActionFilterAttribute
    {
        private HttpOriginMatcher _origins = new HttpOriginMatcher();

        public string Origins
        {
            get
            {
                return _origins.Origins;
            }
            set
            {
                _origins.Origins = value;
            }
        }

        public override void OnActionExecuting(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            if (actionContext.Request.Method == System.Net.Http.HttpMethod.Options)
            {
                string origin = null;
                if (actionContext.Request.Headers.Contains("origin"))
                    origin = (actionContext.Request.Headers.GetValues("origin").FirstOrDefault() ?? "").ToLower();
                if (_origins.MatchOrigin(origin))
                {
                    if (actionContext.Response == null)
                        actionContext.Response = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.OK);

                    actionContext.Response.StatusCode = System.Net.HttpStatusCode.OK;
                    actionContext.Response.Headers.Add("Access-Control-Allow-Origin", origin);
                    actionContext.Response.Headers.Add("Access-Control-Allow-Headers", "Content-Type, bworkflow-handle-errors-generically");
                    actionContext.Response.Headers.Add("Access-Control-Allow-Methods", "*");
                    actionContext.Response.Headers.Add("Access-Control-Allow-Credentials", "true");

                    return;
                }
            }
            base.OnActionExecuting(actionContext);
        }

        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            base.OnActionExecuted(actionExecutedContext);

            if (actionExecutedContext.Request.Method != System.Net.Http.HttpMethod.Options)
            {
                string origin = null;
                if (actionExecutedContext.Request.Headers.Contains("origin"))
                    origin = (actionExecutedContext.Request.Headers.GetValues("origin").FirstOrDefault() ?? "").ToLower();
                if (_origins.MatchOrigin(origin))
                {
                    if (actionExecutedContext.Response == null)
                        actionExecutedContext.Response = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.OK);

                    actionExecutedContext.Response.Headers.Add("Access-Control-Allow-Origin", origin);
                    actionExecutedContext.Response.Headers.Add("Access-Control-Allow-Headers", "Content-Type, bworkflow-handle-errors-generically");
                    actionExecutedContext.Response.Headers.Add("Access-Control-Allow-Methods", "*");
                    actionExecutedContext.Response.Headers.Add("Access-Control-Allow-Credentials", "true");
                }
            }

        }
    }
}
