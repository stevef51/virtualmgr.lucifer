﻿using System;
using System.Configuration;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;
using VirtualMgr.Central;
using VirtualMgr.Membership;
using VirtualMgr.Membership.Direct.IdentityOverrides;

[assembly: OwinStartup(typeof(ConceptCave.Checklist.OData.Startup))]

namespace ConceptCave.Checklist.OData
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }

        public static ApplicationRoleManager CreateRoleManager(IdentityFactoryOptions<ApplicationRoleManager> options, IOwinContext context)
        {
            var result = new ApplicationRoleManager(context.Get<RoleStore<ApplicationRole>>());

            return result;
        }

        public static ApplicationUserManager CreateUserManager(IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context)
        {
            var result = new ApplicationUserManager(context.Get<UserStore<ApplicationUser, ApplicationRole, string, IdentityUserLogin, IdentityUserRole, IdentityUserClaim>>());

            return result;
        }

        public static RolesWrapper CreateRoleWrapper(IdentityFactoryOptions<RolesWrapper> options, IOwinContext context)
        {
            var result = new RolesWrapper(context.Get<ApplicationRoleManager>(), context.Get<ApplicationUserManager>());

            return result;
        }

        public static RoleStore<ApplicationRole> CreateRoleStore(IdentityFactoryOptions<RoleStore<ApplicationRole>> options, IOwinContext context)
        {
            var result = new RoleStore<ApplicationRole>(context.Get<ApplicationDbContext>());

            return result;
        }

        public static UserStore<ApplicationUser, ApplicationRole, string, IdentityUserLogin, IdentityUserRole, IdentityUserClaim> CreateUserStore(IdentityFactoryOptions<UserStore<ApplicationUser, ApplicationRole, string, IdentityUserLogin, IdentityUserRole, IdentityUserClaim>> options, IOwinContext context)
        {
            var result = new UserStore<ApplicationUser, ApplicationRole, string, IdentityUserLogin, IdentityUserRole, IdentityUserClaim>(context.Get<ApplicationDbContext>());

            return result;
        }

        public static ApplicationDbContext CreateDbContext(IdentityFactoryOptions<ApplicationDbContext> options, IOwinContext context)
        {
            Func<ConnectionStringSettings> fnConnectionStringSettings = () => new ConnectionStringSettings(MultiTenantManager.CurrentTenant.Name, MultiTenantManager.CurrentTenant.ConnectionString);

            var result = new ApplicationDbContext(new VirtualMgr.Membership.Direct.DatabaseConnectionManager(fnConnectionStringSettings));

            return result;
        }

        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureAuth(IAppBuilder app)
        {
            // Enable the application to use a cookie to store information for the signed in user
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Authenticate")
            });

            app.CreatePerOwinContext<ApplicationDbContext>(CreateDbContext);
            app.CreatePerOwinContext<UserStore<ApplicationUser, ApplicationRole, string, IdentityUserLogin, IdentityUserRole, IdentityUserClaim>>(CreateUserStore);
            app.CreatePerOwinContext<RoleStore<ApplicationRole>>(CreateRoleStore);
            app.CreatePerOwinContext<ApplicationUserManager>(CreateUserManager);
            app.CreatePerOwinContext<ApplicationRoleManager>(CreateRoleManager);
            app.CreatePerOwinContext<RolesWrapper>(CreateRoleWrapper);

            //app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            //// App.Secrets is application specific and holds values in CodePasteKeys.json
            //// Values are NOT included in repro – auto-created on first load
            //if (!string.IsNullOrEmpty(App.Secrets.GoogleClientId))
            //{
            //    app.UseGoogleAuthentication(
            //        clientId: App.Secrets.GoogleClientId,
            //        clientSecret: App.Secrets.GoogleClientSecret);
            //}

            //if (!string.IsNullOrEmpty(App.Secrets.TwitterConsumerKey))
            //{
            //    app.UseTwitterAuthentication(
            //        consumerKey: App.Secrets.TwitterConsumerKey,
            //        consumerSecret: App.Secrets.TwitterConsumerSecret);
            //}

            //if (!string.IsNullOrEmpty(App.Secrets.GitHubClientId))
            //{
            //    app.UseGitHubAuthentication(
            //        clientId: App.Secrets.GitHubClientId,
            //        clientSecret: App.Secrets.GitHubClientSecret);
            //}

            //AntiForgeryConfig.UniqueClaimTypeIdentifier = ClaimTypes.NameIdentifier;
        }
    }
}
