﻿using ConceptCave.Checklist.Reporting.Data;
using ConceptCave.Checklist.Reporting.Data.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.OData.Builder;
using System.Web.Http.OData.Extensions;
using System.Web.Http.Routing;

namespace ConceptCave.Checklist.OData
{

    public static class WebApiConfig
    {
        public static EntitySetConfiguration<T> DataScopeEntitySet<T>(this ODataModelBuilder builder, string name) where T : class
        {
            var eset = builder.EntitySet<T>(name);
            builder.Entity<T>().Collection.Action("DataScopeInfo");
            return eset;
        }

        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
            config.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;

            // the V3 ODATA stuff is in System.Web.Http.ODarta namespace,
            // we aren't going to support V4 at the moment as its not supported
            // by Power Query or Power Pivot.
            ODataModelBuilder builder = new ODataConventionModelBuilder();

            var mEnt = builder.DataScopeEntitySet<Membership>("Members");
            mEnt.EntityType.HasKey(m => m.UserId);

            var mResource = builder.DataScopeEntitySet<Resource>("Resource");
            mResource.EntityType.HasKey(m => m.Id);

            var mResourceLabel = builder.DataScopeEntitySet<ResourceLabel>("ResourceLabel");
            mResourceLabel.EntityType.HasKey(m => m.Row);

            var mMemberLabel = builder.DataScopeEntitySet<MembershipLabel>("MemberLabels");
            mMemberLabel.EntityType.HasKey(m => m.Row);

            var mContexts = builder.DataScopeEntitySet<MembershipContext>("MemberContexts");
            mContexts.EntityType.HasKey(m => m.Id);

            //            ent.EntityType.Ignore(u => u.ExpiryDate); // for v4 DateTime isn't supported
            var vEnt = builder.DataScopeEntitySet<Media>("Media");
            vEnt.EntityType.HasKey(v => v.Id);

            var mMedia = builder.DataScopeEntitySet<MembershipMedia>("MembersMedia");
            mMedia.EntityType.HasKey(m => m.MediaId);

            var mvEnt = builder.DataScopeEntitySet<MediaView>("MediaViews");
            mvEnt.EntityType.HasKey(v => v.Id);

            var mSum = builder.DataScopeEntitySet<CurrentMediaVersionSummary>("CurrentMediaVersionSummary");
            mSum.EntityType.HasKey(v => v.UserId);
            mSum.EntityType.HasKey(v => v.MediaId);

            var pSum = builder.DataScopeEntitySet<MediaPageView>("MediaPageViews");
            pSum.EntityType.HasKey(v => v.Id);

            var mSchedule = builder.DataScopeEntitySet<Schedule>("Schedules");
            mSchedule.EntityType.HasKey(t => t.Id);

            var mRosterRoles = builder.DataScopeEntitySet<RosterRole>("RosterRoles");
            mRosterRoles.EntityType.HasKey(t => t.RosterId);
            mRosterRoles.EntityType.HasKey(t => t.HierarchyBucketId);

            var mRosters = builder.DataScopeEntitySet<Roster>("Rosters");
            mRosters.EntityType.HasKey(t => t.Id);

            var mTeamHierarchys = builder.DataScopeEntitySet<TeamHierarchy>("TeamHierarchys");
            mTeamHierarchys.EntityType.HasKey(t => t.Id);

//            var mCalendar = builder.EntitySet<CalendarEvent>("Calendar");
//            mCalendar.EntityType.HasKey(t => t.Id);

            var mTask = builder.DataScopeEntitySet<Task>("Tasks");
            mTask.EntityType.HasKey(t => t.Id);

            var mHierarchyBucket = builder.DataScopeEntitySet<tblHierarchyBucket>("HierarchyBucket");
            mHierarchyBucket.EntityType.HasKey(t => t.Id);

            var mCompany = builder.DataScopeEntitySet<Company>("Companies");
            mCompany.EntityType.HasKey(t => t.Id);

            var mIncomplete = builder.DataScopeEntitySet<IncompleteTask>("IncompleteTasks");
            mIncomplete.EntityType.HasKey(t => t.Id);

            var mTaskWorklog = builder.DataScopeEntitySet<TaskWorklog>("TaskWorklogs");
            mTaskWorklog.EntityType.HasKey(t => t.Id);

            var mTaskMediaStream = builder.DataScopeEntitySet<TaskMediaStream>("TaskMediaStream");
            mTaskMediaStream.EntityType.HasKey(t => t.MediaId);

            var mTaskLabels = builder.DataScopeEntitySet<TaskLabel>("TaskLabels");
            mTaskLabels.EntityType.HasKey(t => t.Id);
            mTaskLabels.EntityType.HasKey(t => t.LabelId);

            var mTaskFinish = builder.DataScopeEntitySet<TaskFinishStatus>("TaskFinishStatus");
            mTaskFinish.EntityType.HasKey(t => t.Id);
            mTaskFinish.EntityType.HasKey(t => t.ProjectJobFinishedStatusId);

            var mTimesheetItemTypes = builder.DataScopeEntitySet<TimesheetItemType>("TimesheetItemTypes");
            mTimesheetItemTypes.EntityType.HasKey(t => t.Id);

            var mTimesheet = builder.DataScopeEntitySet<TimesheetItem>("TimesheetItems");
            mTimesheet.EntityType.HasKey(t => t.Id);

            var mTaskWorkLoadingActivity = builder.DataScopeEntitySet<TaskWorkLoadingActivity>("TaskWorkLoadingActivities");
            mTaskWorkLoadingActivity.EntityType.HasKey(t => t.TaskId);
            mTaskWorkLoadingActivity.EntityType.HasKey(t => t.SiteFeatureId);
            mTaskWorkLoadingActivity.EntityType.HasKey(t => t.WorkLoadingStandardId);

            var utSum = builder.DataScopeEntitySet<UserTaskSummary_Result>("CurrentUserTasksSummary");
            utSum.EntityType.HasKey(v => v.UserId);

            var utRosterSum = builder.DataScopeEntitySet<UserTaskRosterSummary_Result>("CurrentUserTasksRosterSummary");
            utRosterSum.EntityType.HasKey(v => v.UserId);
            utRosterSum.EntityType.HasKey(v => v.RosterId);

            var checklists = builder.DataScopeEntitySet<ConceptCave.Checklist.Reporting.Data.Checklist>("Checklists");
            checklists.EntityType.HasKey(v => v.WorkingDocumentId);

            var checklistlabels = builder.DataScopeEntitySet<ConceptCave.Checklist.Reporting.Data.ChecklistLabel>("ChecklistLabels");
            checklistlabels.EntityType.HasKey(v => v.Id);
            checklistlabels.EntityType.HasKey(v => v.LabelId);

            var checklistResults = builder.DataScopeEntitySet<ChecklistResult>("ChecklistResults");
            checklistResults.EntityType.HasKey(v => v.Id); 

            var checklistResultLabels = builder.EntitySet<ChecklistResultLabel>("ChecklistResultLabels");
            checklistResultLabels.EntityType.HasKey(v => v.Id);
            checklistResultLabels.EntityType.HasKey(v => v.LabelId);

            var checklistMediaStream = builder.DataScopeEntitySet<ChecklistMediaStream>("ChecklistMediaStream");
            checklistMediaStream.EntityType.HasKey(v => v.WorkingDocumentId);
            checklistMediaStream.EntityType.HasKey(v => v.PresentedId);

            var workingDocuments = builder.DataScopeEntitySet<WorkingDocument>("WorkingDocuments");
            workingDocuments.EntityType.HasKey(v => v.Id);

            var mCronJobs = builder.DataScopeEntitySet<tblQRTZHistory>("CRONJobs");
            mCronJobs.EntityType.HasKey(t => t.Id);

            var finishstatuses = builder.DataScopeEntitySet<FinishStatus>("FinishStatuses");
            finishstatuses.EntityType.HasKey(t => t.Id);

            var executionQueues = builder.DataScopeEntitySet<tblExecutionHandlerQueue>("ExecutionHandlerQueue");
            executionQueues.EntityType.HasKey(t => t.Id);

            var errorLogDetails = builder.DataScopeEntitySet<ErrorLogDetail>("ErrorLogDetails");
            errorLogDetails.EntityType.HasKey(t => t.Id);

            var errorLogSummaries = builder.DataScopeEntitySet<ErrorLogSummary>("ErrorLogSummaries");
            errorLogSummaries.EntityType.HasKey(t => t.Id);

            var assetTypes = builder.DataScopeEntitySet<AssetType>("AssetTypes");
            assetTypes.EntityType.HasKey(t => t.Id);

            var assets = builder.DataScopeEntitySet<Asset>("Assets");
            assets.EntityType.HasKey(t => t.Id);

            var beacons = builder.DataScopeEntitySet<Beacon>("Beacons");
            beacons.EntityType.HasKey(t => t.Id);

            var assetLocationHistory = builder.DataScopeEntitySet<AssetLocationHistory>("AssetLocationHistory");
            assetLocationHistory.EntityType.HasKey(t => t.Id);

            var assetLastKnownLocation = builder.DataScopeEntitySet<AssetLastKnownLocation>("AssetLastKnownLocation");
            assetLastKnownLocation.EntityType.HasKey(t => t.Id);

            var gpsLocation = builder.DataScopeEntitySet<GpsLocation>("GpsLocation");
            gpsLocation.EntityType.HasKey(t => t.Id);

            var tablet = builder.DataScopeEntitySet<Tablet>("Tablets");          
            tablet.EntityType.HasKey(t => t.UUID);

            var tabletLocationHistory = builder.DataScopeEntitySet<TabletLocationHistory>("TabletLocationHistory");
            tabletLocationHistory.EntityType.HasKey(t => t.Id);

            var facilityStructures = builder.DataScopeEntitySet<FacilityStructure>("FacilityStructures");
            facilityStructures.EntityType.HasKey(t => t.Id);

            var floorPlans = builder.DataScopeEntitySet<FloorPlan>("FloorPlans");
            floorPlans.EntityType.HasKey(t => t.Id);

            var leaveTypes = builder.DataScopeEntitySet<LeaveType>("LeaveTypes");
            leaveTypes.EntityType.HasKey(t => t.Id);

            var leaves = builder.DataScopeEntitySet<Leave>("Leave");
            leaves.EntityType.HasKey(t => t.Id);

            var tenants = builder.DataScopeEntitySet<Tenant>("Tenants");
            tenants.EntityType.HasKey(t => t.Id);

            var products = builder.DataScopeEntitySet<Product>("Products");
            products.EntityType.HasKey(t => t.Id);

            var orders = builder.DataScopeEntitySet<Order>("Orders");
            orders.EntityType.HasKey(t => t.Id);

            var orderItems = builder.DataScopeEntitySet<OrderItem>("OrderItems");
            orderItems.EntityType.HasKey(t => t.Id);

            var taskType = builder.DataScopeEntitySet<TaskType>("TaskTypes");
            taskType.EntityType.HasKey(t => t.Id);

            var taskTypeFinishedStatus = builder.DataScopeEntitySet<TaskTypeFinishedStatus>("TaskTypeFinishedStatus");
            taskTypeFinishedStatus.EntityType.HasKey(t => t.Id);

            var userTypeTaskType = builder.DataScopeEntitySet<UserTypeTaskType>("UserTypeTaskTypes");
            userTypeTaskType.EntityType.HasKey(t => t.RelationshipType);
            userTypeTaskType.EntityType.HasKey(t => t.TaskTypeId);
            userTypeTaskType.EntityType.HasKey(t => t.UserTypeId);

            var esProbes = builder.DataScopeEntitySet<ESProbe>("ESProbes");
            esProbes.EntityType.HasKey(t => t.Id);

            var esSensors = builder.DataScopeEntitySet<ESSensor>("ESSensors");
            esSensors.EntityType.HasKey(t => t.Id);

            var esSensorReadings = builder.DataScopeEntitySet<ESSensorReading>("ESSensorReadings");
            esSensorReadings.EntityType.HasKey(t => t.Id);

            var qrCodes = builder.DataScopeEntitySet<QRCode>("QRCodes");
            qrCodes.EntityType.HasKey(t => t.Id);

            var userLogs = builder.DataScopeEntitySet<UserLog>("UserLogs");
            userLogs.EntityType.HasKey(t => t.Id);

            var dbObjectSizes = builder.DataScopeEntitySet<Controllers.DBObjectSize>("DBObjectSizes");
            dbObjectSizes.EntityType.HasKey(t => t.Name);
            dbObjectSizes.EntityType.HasKey(t => t.Type);

            var dbSizes = builder.DataScopeEntitySet<Controllers.DBSize>("DBSizes");
            dbSizes.EntityType.HasKey(t => t.DatabaseBytes);
            dbSizes.EntityType.HasKey(t => t.DataFileBytes);
            dbSizes.EntityType.HasKey(t => t.LogFileBytes);

            var periodicExecutionHistory = builder.DataScopeEntitySet<PeriodicExecutionHistory>("PeriodicExecutionHistory");
            periodicExecutionHistory.EntityType.HasKey(t => t.Id);


            var pendingPayments = builder.DataScopeEntitySet<PendingPayment>("PendingPayments");
            pendingPayments.EntityType.HasKey(t => t.Id);

            var paymentResponses = builder.DataScopeEntitySet<PaymentResponse>("PaymentResponses");
            paymentResponses.EntityType.HasKey(t => t.Id);

            var orderMedias = builder.DataScopeEntitySet<OrderMedia>("OrderMedias");
            orderMedias.EntityType.HasKey(t => t.Id);

            var globalSettings = builder.DataScopeEntitySet<GlobalSetting>("GlobalSettings");
            globalSettings.EntityType.HasKey(t => t.Id);

            var newsFeed = builder.DataScopeEntitySet<NewsFeed>("NewsFeeds");
            newsFeed.EntityType.HasKey(t => t.Id);

            config.Routes.MapODataServiceRoute("odata", "odata-v3", builder.GetEdmModel());

            var formatters = System.Web.Http.OData.Formatter.ODataMediaTypeFormatters.Create();
            config.Formatters.InsertRange(0, formatters);

//            config.Filters.Add(new ConceptCave.Checklist.OData.Filters.IdentityBasicAuthenticationAttribute());

            //config.Routes.MapHttpRoute(
            //    name: "DefaultApi",
            //    routeTemplate: "api/{controller}/{id}",
            //    defaults: new { id = RouteParameter.Optional }
            //);
        }

    }
}
