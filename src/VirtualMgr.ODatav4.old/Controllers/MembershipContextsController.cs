﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using ConceptCave.Checklist.OData.Filters;
using System.Threading;
using System.Web.Http.OData.Query;
using System.Web.Http.OData.Extensions;
using ConceptCave.Checklist.Reporting.Data;
using ConceptCave.Checklist.OData.DataScopes;
using ConceptCave.Checklist.OData.Attributes;

namespace ConceptCave.Checklist.OData.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using ConceptCave.Checklist.OData.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<tblUserData>("UserData");
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    [IdentityBasicAuthentication("odata")]
    [WebApiCorsAuthorize]
    public class MemberContextsController : ODataControllerBase
    {
        protected override object GetDataScope(Dictionary<string, DataScopeField> metaData)
        {
            return new MembershipDataScope().PopulateMetaData(metaData);
        }

        // GET: odata/UserData
        [EnableQuery]
        public IQueryable<MembershipContext> GetMemberContexts(string datascope = null)
        {
            MembershipDataScope scope = MembershipDataScope.FromDataScopeString(datascope);

            IQueryable<MembershipContext> result = db.MembershipContextsEnforceSecurityWithParams(scope.ExcludeOutsideHierarchies, scope.IncludeCurrentUser);

            if (scope.ExcludeExpired)
            {
                result = result.Where(r => !r.ExpiryDate.HasValue || r.ExpiryDate.Value > DateTime.UtcNow);
            }

            return result;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
