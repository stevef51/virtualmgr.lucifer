﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using ConceptCave.Checklist.OData.Filters;
using System.Threading;
using System.Web.Http.OData.Query;
using ConceptCave.Checklist.Reporting.Data;
using ConceptCave.Checklist.OData.Attributes;
using VirtualMgr.Central;
using ConceptCave.Checklist.Reporting.Data.DTO;
using System.Data.SqlClient;

namespace ConceptCave.Checklist.OData.Controllers
{
    public class DBSize
    {
        public long DatabaseBytes { get; set; }
        public long DataFileBytes { get; set; }
        public long LogFileBytes { get; set; }
    }

    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using ConceptCave.Checklist.OData.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<tblUserData>("UserData");
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
//    [WebApiCorsAuthorize]
    [WebApiCorsOptionsAllow(Origins = Consts.DefaultCorsAllowOrigin + "," + Consts.SecureDomains)]
    public class DBSizesController : ODataControllerBase
    {
        [EnableQuery]
        public IHttpActionResult GetDBSizes()
        {
            var result = new List<DBSize>();

            using (var cn = new SqlConnection(MultiTenantManager.CurrentTenant.ConnectionString))
            {
                cn.Open();
                var cmd = new SqlCommand(@"
SELECT 
    SUM(reserved_page_count) * CAST(8 * 1024 AS BIGINT) AS DatabaseBytes,
    (SELECT SUM(df.size) * CAST(8 * 1024 AS BIGINT) FROM sys.database_files df WHERE df.type_desc = 'ROWS') AS DataFileBytes,
    (SELECT SUM(df.size) * CAST(8 * 1024 AS BIGINT) FROM sys.database_files df WHERE df.type_desc = 'LOG') AS LogFileBytes
FROM sys.dm_db_partition_stats
", cn);
                var reader = cmd.ExecuteReader();
                while(reader.Read())
                {
                    result.Add(new DBSize()
                    {
                        DatabaseBytes = reader.GetInt64(reader.GetOrdinal("DatabaseBytes")),
                        DataFileBytes = reader.GetInt64(reader.GetOrdinal("DataFileBytes")),
                        LogFileBytes = reader.GetInt64(reader.GetOrdinal("LogFileBytes")),
                    });                    
                }
            }

            return Ok(result);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
