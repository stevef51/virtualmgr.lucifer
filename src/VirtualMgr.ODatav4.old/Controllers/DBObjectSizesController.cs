﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using ConceptCave.Checklist.OData.Filters;
using System.Threading;
using System.Web.Http.OData.Query;
using ConceptCave.Checklist.Reporting.Data;
using ConceptCave.Checklist.OData.Attributes;
using VirtualMgr.Central;
using ConceptCave.Checklist.Reporting.Data.DTO;
using System.Data.SqlClient;

namespace ConceptCave.Checklist.OData.Controllers
{
    public class DBObjectSize
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public long Bytes { get; set; }
    }

    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using ConceptCave.Checklist.OData.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<tblUserData>("UserData");
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    //[WebApiCorsAuthorize]
    public class DBObjectSizesController : ODataControllerBase
    {
        [EnableQuery]
        public IHttpActionResult GetDBObjectSizes()
        {
            var result = new List<DBObjectSize>();

            using (var cn = new SqlConnection(MultiTenantManager.CurrentTenant.ConnectionString))
            {
                cn.Open();
                var cmd = new SqlCommand(@"
SELECT
      sys.objects.name, sys.objects.type_desc, SUM(reserved_page_count) * CAST(8 * 1024 AS BIGINT) AS Bytes
FROM    
      sys.dm_db_partition_stats, sys.objects
WHERE    
      sys.dm_db_partition_stats.object_id = sys.objects.object_id

GROUP BY sys.objects.name, sys.objects.type_desc
", cn);
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    result.Add(new DBObjectSize()
                    {
                        Name = reader.GetString(reader.GetOrdinal("name")),
                        Type = reader.GetString(reader.GetOrdinal("type_desc")),
                        Bytes = reader.GetInt64(reader.GetOrdinal("Bytes")),
                    });
                }
            }

            return Ok(result);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
