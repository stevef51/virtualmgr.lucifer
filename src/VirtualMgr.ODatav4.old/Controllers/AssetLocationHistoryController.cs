﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using ConceptCave.Checklist.OData.Filters;
using System.Threading;
using System.Web.Http.OData.Query;
using ConceptCave.Checklist.Reporting.Data;
using ConceptCave.Checklist.OData.Attributes;
using System.Device.Location;
using ConceptCave.Checklist.OData.DataScopes;

namespace ConceptCave.Checklist.OData.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using ConceptCave.Checklist.OData.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<tblUserData>("UserData");
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    [IdentityBasicAuthentication("odata")]
    [WebApiCorsAuthorize]
    public class AssetLocationHistoryController : ODataControllerBase
    {
        public class AssetLocationDistanceHelper
        {
            public double Distance { get; set; }
            public AssetLocationHistory History { get; set; }
        }

        protected override object GetDataScope(Dictionary<string, DataScopeField> metaData)
        {
            return new AssetLocationHistoryDataScope().PopulateMetaData(metaData);
        }

        protected IHttpActionResult GetAssetLocationHistory()
        {
            var result = db.AssetLocationHistories;
            return Ok(result);
        }

        [EnableQuery(MaxExpansionDepth = 5)]
        public IHttpActionResult GetAssetLocationHistory(ODataQueryOptions<AssetLocationHistory> options, string datascope = null)
        {
            AssetLocationHistoryDataScope scope = AssetLocationHistoryDataScope.FromDataScopeString(datascope);

            if(scope.Count <= 0)
            {
                return GetAssetLocationHistory();
            }

            ODataQuerySettings settings = new ODataQuerySettings();

            var result = options.Filter.ApplyTo(db.AssetLocationHistories, settings);

            GeoCoordinate location = new GeoCoordinate(scope.Lat, scope.Lng);

            List<AssetLocationDistanceHelper> helpers = new List<AssetLocationDistanceHelper>();

            foreach(AssetLocationHistory history in result)
            {
                if (history.GpsLocation == null)
                {
                    // we can't do much if we don't have a location
                    continue;
                }

                AssetLocationDistanceHelper helper = new AssetLocationDistanceHelper() { History = history };

                helper.Distance = location.GetDistanceTo(new GeoCoordinate((double)history.GpsLocation.Latitude, (double)history.GpsLocation.Longitude));

                helpers.Add(helper);
            }

            return Ok((from h in helpers orderby h.Distance ascending select h.History).Take(scope.Count));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
