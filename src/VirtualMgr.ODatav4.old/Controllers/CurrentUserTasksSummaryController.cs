﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using ConceptCave.Checklist.OData.Filters;
using System.Threading;
using System.Web.Http.OData.Query;
using ConceptCave.Checklist.Reporting.Data;
using ConceptCave.Checklist.OData.Attributes;

namespace ConceptCave.Checklist.OData.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using ConceptCave.Checklist.OData.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<tblUserData>("UserData");
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    [IdentityBasicAuthentication("odata")]
    [WebApiCorsAuthorize]
    public class CurrentUserTasksSummaryController : ODataControllerBase
    {
        // GET: odata/UserData
        public IQueryable<UserTaskSummary_Result> GetCurrentUserTasksSummary(ODataQueryOptions<UserTaskSummary_Result> options)
        {
            DateTime startDate = DateTime.Now.Date.ToUniversalTime();
            DateTime endDate = DateTime.Now.Date.AddDays(1).ToUniversalTime();
            
            // we are now going to see if we can work out a start and end time that is better than what
            // we've set above
            var userId = db.CurrentUser.UserId;
            var hierarchies = (from h in db.tblHierarchyBuckets where h.UserId == userId select h);

            tblHierarchyBucket hierarchy = null;

            if(hierarchies.Count() > 0)
            {
                // try to select a sensible roster by tracking through the hierarchy
                var primaries = (from h in hierarchies where h.IsPrimary == true select h);
                if(primaries.Count() > 0)
                {
                    hierarchy = primaries.First();
                }
                else {
                    hierarchy = hierarchies.First();
                }

                var rosters = (from r in db.tblRosters where r.HierarchyId == hierarchy.HierarchyId select r);

                if(rosters.Count() > 0)
                {
                    var roster = rosters.First();
                    var tz = TimeZoneInfo.FindSystemTimeZoneById(db.CurrentUser.TimeZone);
                    var localTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, tz);

                    // so let's lay the timeline out as follows
                    // 8AM Mon ----------------- Midnight ---------------- 8AM Tue
                    // Where 8AM Mon is the start and 8AM Tue is the start of the roster the next day
                    // So if localTime is > start then its on that day
                    // if localTime is < start then its on the previous day
                    // Note, we aren't taking the roster end time into account here intentionally as
                    // processes may occur outside the operational times for the roster, for example
                    // the automated end of day will want to run after the days work has been completed.
                    // So I'm assuming a day as the timeframe rather than the roster's time span.

                    // lets find out where things fall, to do this we get the roster start time on the day of local time,
                    // we can then just compare the dates simply.
                    var compareTime = new DateTime(localTime.Year, localTime.Month, localTime.Day, roster.StartTime.Hour, roster.StartTime.Minute, roster.StartTime.Second, DateTimeKind.Unspecified);

                    if (localTime < compareTime)
                    {
                        // so its before the roster starts, therefore we need to go back a day
                        localTime = localTime.AddHours(-24);
                    }

                    // so the start of the roster is
                    localTime = new DateTime(localTime.Year, localTime.Month, localTime.Day, roster.StartTime.Hour, roster.StartTime.Minute, roster.StartTime.Second, DateTimeKind.Unspecified);

                    startDate = TimeZoneInfo.ConvertTimeToUtc(localTime, tz);
                    endDate = startDate.AddDays(1);
                }
            }

            IQueryable<UserTaskSummary_Result> result = db.UserTaskSummaryEnforceSecurity(startDate, endDate);

            var applied = options.ApplyTo(result) as IQueryable<UserTaskSummary_Result>;

            return applied;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
