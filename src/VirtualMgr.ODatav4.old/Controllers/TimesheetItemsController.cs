﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using ConceptCave.Checklist.OData.Filters;
using System.Threading;
using System.Web.Http.OData.Query;
using ConceptCave.Checklist.Reporting.Data;
using ConceptCave.Checklist.OData.DataScopes;
using ConceptCave.Checklist.OData.Attributes;
using ConceptCave.Checklist.Reporting.Data.DTO;
using ConceptCave.Checklist.Reporting.Data.Repositories;

namespace ConceptCave.Checklist.OData.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using ConceptCave.Checklist.OData.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<tblUserData>("UserData");
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    [IdentityBasicAuthentication("odata")]
    [WebApiCorsAuthorize]
    public class TimesheetItemsController : ODataControllerBase
    {
        protected override object GetDataScope(Dictionary<string, DataScopeField> metaData)
        {
            return new QueryUserDataScope().PopulateMetaData(metaData);
        }

        public IQueryable<TimesheetItem> GetTimesheetItems(ODataQueryOptions<TimesheetItem> options)
        {
            return GetTimesheetItems(options, null, null, null, null, null, TimesheetGrouping.None.ToString(), null);
        }

        public IQueryable<TimesheetItem> GetTimesheetItems(ODataQueryOptions<TimesheetItem> options, DateTime StartDate, DateTime EndDate)
        {
            return GetTimesheetItems(options, StartDate, EndDate, null, null, null, TimesheetGrouping.None.ToString(), null);
        }

        public IQueryable<TimesheetItem> GetTimesheetItems(ODataQueryOptions<TimesheetItem> options, DateTime StartDate, DateTime EndDate, string GroupBySite)
        {
            return GetTimesheetItems(options, StartDate, EndDate, null, null, null, GroupBySite, null);
        }

        public IQueryable<TimesheetItem> GetTimesheetItems(ODataQueryOptions<TimesheetItem> options, DateTime StartDate, DateTime EndDate, Guid OwnerId, string GroupBySite)
        {
            return GetTimesheetItems(options, StartDate, EndDate, OwnerId, null, null, GroupBySite, null);
        }

        public IQueryable<TimesheetItem> GetTimesheetItems(ODataQueryOptions<TimesheetItem> options, DateTime StartDate, DateTime EndDate, Guid SiteId)
        {
            return GetTimesheetItems(options, StartDate, EndDate, null, SiteId, null, TimesheetGrouping.None.ToString(), null);
        }

        public IQueryable<TimesheetItem> GetTimesheetItems(ODataQueryOptions<TimesheetItem> options, DateTime? StartDate, DateTime? EndDate, Guid? ownerId, Guid? siteId, string notesPrompt, string groupBySite, string datascope)
        {
            var scope = QueryUserDataScope.FromDataScopeString(datascope);
            IQueryable<TaskWorklog> result = db.TaskWorklogsEnforceSecurityAndScope(scope.QueryScope);

            // this should be injected
            TaskRepository taskRepo = new TaskRepository();

            DateTime sd = DateTime.Now.Date.ToUniversalTime();
            DateTime ed = DateTime.Now.Date.AddDays(1).ToUniversalTime();

            if(StartDate.HasValue)
                sd = StartDate.Value;

            if(EndDate.HasValue)
                ed = EndDate.Value;

            var sheets = taskRepo.Timesheet(sd, ed, ownerId, siteId, notesPrompt, groupBySite, false, result);

            var applied = options.ApplyTo(sheets) as IQueryable<TimesheetItem>;

            return applied;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
