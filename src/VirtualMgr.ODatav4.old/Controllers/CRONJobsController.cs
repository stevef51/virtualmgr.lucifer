﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using ConceptCave.Checklist.OData.Filters;
using System.Web.Http.OData.Query;
using ConceptCave.Checklist.Reporting.Data;
using ConceptCave.Checklist.OData.Attributes;

namespace ConceptCave.Checklist.OData.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using ConceptCave.Checklist.OData.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<tblUserData>("UserData");
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    [IdentityBasicAuthentication("odata")]
    [WebApiCorsAuthorize]
    public class CRONJobsController : ODataControllerBase
    {
        // GET: odata/UserData
        [EnableQuery]
        public IQueryable<tblQRTZHistory> GetCRONJobs(ODataQueryOptions<tblQRTZHistory> options)
        {
            var result = db.tblQRTZHistories;

            var applied = options.ApplyTo(result) as IQueryable<tblQRTZHistory>;

            return applied;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
