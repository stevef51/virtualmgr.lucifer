﻿using ConceptCave.Checklist.OData.Attributes;
using ConceptCave.Checklist.OData.DataScopes;
using ConceptCave.Checklist.OData.Filters;
using ConceptCave.Checklist.Reporting.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.OData;
using System.Web.Mvc;


namespace ConceptCave.Checklist.OData.Controllers
{
    [IdentityBasicAuthentication("odata")]
    [WebApiCorsAuthorize]
    public class GlobalSettingsController : ODataControllerBase
    {
        [EnableQuery]
        public IQueryable<GlobalSetting> Get()        
        {
            return db.GlobalSettings;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}