﻿using ConceptCave.Checklist.Reporting.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ConceptCave.Checklist.OData.Controllers
{
    public class HealthCheckController : ODataControllerBase
    {
        [Route("Health/Check")]
        [HttpGet]
        public string Check()
        {
            var healthRepo = new HealthRepository();

            if(healthRepo.CheckDatabaseConnection())
            {
                return "UP";
            }

            return "DOWN";
        }
    }
}
