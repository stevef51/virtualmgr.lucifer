﻿using ConceptCave.Checklist.Reporting.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Http.OData;
using System.Web.Http;
using ConceptCave.Checklist.OData.DataScopes;
using ConceptCave.Checklist.OData.Attributes;

namespace ConceptCave.Checklist.OData.Controllers
{
    public class ODataControllerBase : ODataController
    {
        private SecurityAndTimezoneAwareDataModel _db;
        protected SecurityAndTimezoneAwareDataModel db
        {
            get
            {
                if (_db == null)
                {
                    _db = new SecurityAndTimezoneAwareDataModel();
                    _db.LoadCurrentUser();
                }
                return _db;
            }            
        }

        [HttpPost]
        public virtual object DataScopeInfo()
        {
            Dictionary<string, DataScopeField> metaData = new Dictionary<string, DataScopeField>();
            var dataScope = GetDataScope(metaData);
            return new { DataScope = dataScope, MetaData = metaData };
        }

        protected virtual object GetDataScope(Dictionary<string, DataScopeField> metaData)
        {
            return null;
        }
    }
}