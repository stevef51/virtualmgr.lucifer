﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using ConceptCave.Checklist.OData.Filters;
using System.Threading;
using System.Web.Http.OData.Query;
using ConceptCave.Checklist.Reporting.Data;
using Microsoft.Data.OData.Query.SemanticAst;
using ConceptCave.Scheduling;
using ConceptCave.Checklist.OData.Attributes;
using ConceptCave.Checklist.Reporting.Data.DTO;

namespace ConceptCave.Checklist.OData.Controllers
{
    // This class currently isn't exposed over ODATA as EVS-350 needs to be completd first
    // when you want it available uncomment the code in the webapiconfig that adds it as a feed.


    public enum CalendarPeriod
    {
        Daily,
        Weekly,
        Monthly
    }
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using ConceptCave.Checklist.OData.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<tblUserData>("UserData");
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    [IdentityBasicAuthentication("odata")]
    [WebApiCorsAuthorize]
    public class CalendarController : ODataControllerBase
    {
        public IQueryable<CalendarEvent> GetCalendar(ODataQueryOptions<CalendarEvent> options)
        {
            return GetCalendar(DateTime.Now.Date.ToUniversalTime(), CalendarPeriod.Daily, options);
        }
        public IQueryable<CalendarEvent> GetCalendar(DateTime date, CalendarPeriod period, ODataQueryOptions<CalendarEvent> options)
        {
            return GetCalendar(date, period, null, null, options);
        }

        public IQueryable<CalendarEvent> GetCalendar(DateTime date, CalendarPeriod period, Guid ownerId, ODataQueryOptions<CalendarEvent> options)
        {
            return GetCalendar(date, period, ownerId, null, options);
        }

        public IQueryable<CalendarEvent> GetCalendar(DateTime date, CalendarPeriod period, int rosterId, ODataQueryOptions<CalendarEvent> options)
        {
            return GetCalendar(date, period, null, rosterId, options);
        }

        public IQueryable<CalendarEvent> GetCalendar(DateTime date, CalendarPeriod period, Guid? ownerid, int? rosterId, ODataQueryOptions<CalendarEvent> options)
        {
            var schedules = db.SchedulesEnforceSecurity;

            if(ownerid.HasValue == true)
            {
                schedules = schedules.Where(s => s.OwnerId == ownerid.Value);
            }

            if(rosterId.HasValue == true)
            {
                schedules = schedules.Where(s => s.RosterId == rosterId.Value);
            }

            var tz = TimeZoneInfo.FindSystemTimeZoneById(db.CurrentUser.TimeZone);

            var enforcer = new ConceptCave.Scheduling.ExclusionRuleEnforcer();

            enforcer.Rules.Add(new ConceptCave.Scheduling.ExclusionRules.ExpiredOrNotStartedExclusionRule());
            enforcer.Rules.Add(new ConceptCave.Scheduling.ExclusionRules.DailyScheduleExclusionRule());
            enforcer.Rules.Add(new ConceptCave.Scheduling.ExclusionRules.CronScheduleExclusionRule(tz));

            List<IScheduledTask> helpers = new List<IScheduledTask>();
            foreach(var s in schedules)
            {
                helpers.Add(new ScheduleExclusionRuleHelper(s));
            }

            List<CalendarEvent> result = new List<CalendarEvent>();

            foreach(var d in GetDates(date, period))
            {
                // since the enforcer changes the collection handed into it, we take a copy
                // of the original and use that as it'll get used on each day
                var copy = helpers.ConvertAll<IScheduledTask>(i => i);

                enforcer.Enforce(copy, d, false);

                copy.ForEach(c => result.Add(new CalendarEvent(((ScheduleExclusionRuleHelper)c).Entity, d)));
            }

            return options.ApplyTo(result.AsQueryable()) as IQueryable<CalendarEvent>;
        }

        protected List<DateTime> GetDates(DateTime date, CalendarPeriod period)
        {
            if(period == CalendarPeriod.Daily)
            {
                return GetDailyDates(date);
            }
            else if(period == CalendarPeriod.Weekly)
            {
                return GetWeeklyDates(date);
            }
            else
            {
                return GetMonthlyDates(date);
            }
        }

        protected List<DateTime> GetDailyDates(DateTime date)
        {
            var result = new List<DateTime>();

            result.Add(date);

            return result;
        }

        protected List<DateTime> GetWeeklyDates(DateTime date)
        {
            int diff = date.DayOfWeek - DayOfWeek.Sunday;
            if(diff < 0)
            {
                diff += 7;
            }

            DateTime start = date.AddDays(-1 * diff);

            var result = new List<DateTime>();

            for(int i =0; i<7;i++)
            {
                result.Add(start.AddDays(i));
            }

            return result;
        }

        protected List<DateTime> GetMonthlyDates(DateTime date)
        {
            DateTime start = date.AddDays(-date.Day + 1);
            DateTime end = start.AddMonths(1);

            var result = new List<DateTime>();

            for(int d=0; d < (int)end.Subtract(start).TotalDays; d++)
            {
                result.Add(start.AddDays(d));
            }

            return result;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
