﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using ConceptCave.Checklist.OData.Filters;
using System.Threading;
using System.Web.Http.OData.Query;
using ConceptCave.Checklist.Reporting.Data;
using ConceptCave.Checklist.OData.Attributes;
using ConceptCave.Checklist.OData.DataScopes;

namespace ConceptCave.Checklist.OData.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using ConceptCave.Checklist.OData.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<tblUserData>("UserData");
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    [IdentityBasicAuthentication("odata")]
    [WebApiCorsAuthorize]
    public class CurrentUserTasksRosterSummaryController : ODataControllerBase
    {
        protected override object GetDataScope(Dictionary<string, DataScopeField> metaData)
        {
            return new QueryUserDataScope().PopulateMetaData(metaData);
        }

        // GET: odata/UserData
        public IQueryable<UserTaskRosterSummary_Result> GetCurrentUserTasksRosterSummary(ODataQueryOptions<UserTaskRosterSummary_Result> options, string datascope)
        {
            var scope = QueryUserDataScope.FromDataScopeString(datascope);
            DateTime startDate = DateTime.Now.Date.ToUniversalTime();
            DateTime endDate = DateTime.Now.Date.AddDays(1).ToUniversalTime();

            List<UserTaskRosterSummary_Result> result = new List<UserTaskRosterSummary_Result>();
            foreach(var hb in db.CurrentUserHierarchyBuckets.Include(hb => hb.tblHierarchy.tblRosters))
            {
                foreach (var roster in hb.tblHierarchy.tblRosters)
                {
                    var tz = TimeZoneInfo.FindSystemTimeZoneById(db.CurrentUser.TimeZone);
                    var localTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, tz);

                    // so let's lay the timeline out as follows
                    // 8AM Mon ----------------- Midnight ---------------- 8AM Tue
                    // Where 8AM Mon is the start and 8AM Tue is the start of the roster the next day
                    // So if localTime is > start then its on that day
                    // if localTime is < start then its on the previous day
                    // Note, we aren't taking the roster end time into account here intentionally as
                    // processes may occur outside the operational times for the roster, for example
                    // the automated end of day will want to run after the days work has been completed.
                    // So I'm assuming a day as the timeframe rather than the roster's time span.

                    // lets find out where things fall, to do this we get the roster start time on the day of local time,
                    // we can then just compare the dates simply.
                    var compareTime = new DateTime(localTime.Year, localTime.Month, localTime.Day, roster.StartTime.Hour, roster.StartTime.Minute, roster.StartTime.Second, DateTimeKind.Unspecified);

                    if (localTime < compareTime)
                    {
                        // so its before the roster starts, therefore we need to go back a day
                        localTime = localTime.AddHours(-24);
                    }

                    // so the start of the roster is
                    localTime = new DateTime(localTime.Year, localTime.Month, localTime.Day, roster.StartTime.Hour, roster.StartTime.Minute, roster.StartTime.Second, DateTimeKind.Unspecified);

                    startDate = TimeZoneInfo.ConvertTimeToUtc(localTime, tz);
                    endDate = startDate.AddDays(1);

                    var hiearchyResult = db.UserTaskRosterSummaryEnforceSecurity(startDate, endDate, hb.HierarchyId, hb.LeftIndex, hb.RightIndex, scope.QueryScope);
                    result.AddRange(hiearchyResult);
                }
            }

            var applied = options.ApplyTo(result.AsQueryable()) as IQueryable<UserTaskRosterSummary_Result>;

            return applied;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
