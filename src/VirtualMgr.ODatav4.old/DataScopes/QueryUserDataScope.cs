﻿using ConceptCave.Checklist.Reporting.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json.Converters;

namespace ConceptCave.Checklist.OData.DataScopes
{

    public class QueryUserDataScope : DataScope
    {
        public QueryScope QueryScope { get; set; }
        public QueryUserDataScope()
        {
            QueryScope = Reporting.Data.QueryScope.Normal;
        }

        public override DataScope PopulateMetaData(Dictionary<string, DataScopeField> metaData)
        {
            metaData.Add("QueryScope", new DataScopeField<QueryScope>("Scope") { group = "User", showHelp = true, description = "How or whether current user is included" });
            return this;
        }
        public static QueryUserDataScope FromDataScopeString(string datascope)
        {
            QueryUserDataScope scope = new QueryUserDataScope();
            if (string.IsNullOrEmpty(datascope) == false)
            {
                try
                {
                    scope = Newtonsoft.Json.JsonConvert.DeserializeObject<QueryUserDataScope>(datascope);
                }
                catch
                {
                    QueryScope queryScope = scope.QueryScope;
                    Enum.TryParse<QueryScope>(datascope, out queryScope);
                    scope.QueryScope = queryScope;
                }
            }
            return scope;
        }
    }

}