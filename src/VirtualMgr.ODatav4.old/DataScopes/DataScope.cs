﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConceptCave.Checklist.OData.DataScopes
{
    // The DataScopeField is modelled around the jQuery Property Grid to allow a javascript object to be easily edited
    // This object describes each of the fields on the javascript object for a nice UI
    public class DataScopeField
    {
        public enum FieldType
        {
            boolean,
            number,
            color,
            options,
            label,
            @string
        }
        public bool browsable { get; set; }
        public string group { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        public object options { get; set; }
        public string description { get; set; }
        public bool showHelp { get; set; }

        public DataScopeField(string name, FieldType type)
        {
            browsable = true;
            this.name = name;
            this.type = type.ToString();
        }
    }

    public class DataScopeBoolField : DataScopeField
    {
        public DataScopeBoolField(string name) : base(name, FieldType.boolean)
        {

        }
    }

    public class DataScopeStringField : DataScopeField
    {
        public DataScopeStringField(string name)
            : base(name, FieldType.@string)
        {

        }
    }

    public class DataScopeNumberField : DataScopeField
    {
        public DataScopeNumberField(string name) : base(name, FieldType.number)
        {

        }
    }


    public class DataScopeField<TEnum> : DataScopeField
    {
        public class Option
        {
            public string text { get; set; }
            public string value { get; set; }
        }
        public DataScopeField(string name)
            : base(name, FieldType.options)
        {
            var enumType = typeof(TEnum);
            var names = Enum.GetNames(enumType);
            var values = Enum.GetValues(enumType);
            List<Option> list = new List<Option>();
            foreach (TEnum e in values)
                list.Add(new Option() { text = e.ToString(), value = ((int)(object)e).ToString() });
            options = list;
        }
    }

    public class DataScope
    {
        public virtual DataScope PopulateMetaData(Dictionary<string, DataScopeField> metaData)
        {
            return this;
        }
    }
}