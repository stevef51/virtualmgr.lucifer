﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json.Converters;
using ConceptCave.Checklist.Reporting.Data;

namespace ConceptCave.Checklist.OData.DataScopes
{
    public class MembershipDataScope : ExpiredDataScope
    {
        public bool ExcludeOutsideHierarchies { get; set; }
        public bool IncludeCurrentUser { get; set; }

        public bool AttemptHierarchySideStep { get; set; }

        public override DataScope PopulateMetaData(Dictionary<string, DataScopeField> metaData)
        {
            base.PopulateMetaData(metaData);

            metaData.Add("ExcludeOutsideHierarchies", new DataScopeBoolField("Exclude Members Outside Hierarchies") { group = "Filter", showHelp = true, description = "Exclude from results members who are not in a hierarchy" });
            metaData.Add("IncludeCurrentUser", new DataScopeBoolField("Include Current User") { group = "User", showHelp = true, description = "Whether current user is included" });

            metaData.Add("AttemptHierarchySideStep", new DataScopeBoolField("Attempt Hierarchy Side Step") { group = "User", showHelp = true, description = "Wether the request should side step the org hierarchy. This will only be obeyed if the user is in the CanSideStepHierarchy otherwise a normal result set will be returned." });
            return this;
        }

        public static MembershipDataScope FromDataScopeString(string datascope)
        {
            MembershipDataScope scope = new MembershipDataScope();
            if (string.IsNullOrEmpty(datascope) == false)
            {
                try
                {
                    scope = Newtonsoft.Json.JsonConvert.DeserializeObject<MembershipDataScope>(datascope);
                }
                catch
                {
                }
            }
            return scope;
        }
    }
}