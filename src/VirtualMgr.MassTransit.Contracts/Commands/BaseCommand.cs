using System;

namespace VirtualMgr.MassTransit.Contracts
{
    public interface BaseCommand
    {
        Guid CorrelationId { get; }
        Guid ConversationId { get; }
        Guid InitiatorId { get; }
        Guid MessageId { get; }
    }
}