﻿using System;

namespace VirtualMgr.MassTransit.Contracts
{
    public interface TenantConnectionsUpdated : BaseEvent
    {
        string[] Added { get; }
        string[] Removed { get; }
        string[] Changed { get; }
    }
}
