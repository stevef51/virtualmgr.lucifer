namespace VirtualMgr.MultiTenant
{
    public static class AppTenantExtensions
    {
        public static System.TimeZoneInfo TimeZoneInfo(this AppTenant self) => System.TimeZoneInfo.FindSystemTimeZoneById(self.TimeZone);
    }
}