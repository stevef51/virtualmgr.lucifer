﻿using System;

namespace VirtualMgr.MultiTenant
{
    public class AppTenantProfile
    {
        public int Id { get; set; }
        public string Name { get; private set; }
        public bool IsEmailEnabled { get; private set; }
        public string EmailWhitelist { get; private set; }
        public bool CanClone { get; private set; }
        public bool CanDelete { get; private set; }
        public bool Hidden { get; private set; }
        public string CreateAlertMessage { get; private set; }
        public bool IsTenantMaster { get; private set; }
        public bool IsDevelopment { get; private set; }
        public AppTenantProfile(
            int id,
            string name,
            bool isEmailEnabled,
            string emailWhitelist,
            bool canClone,
            bool canDelete,
            bool hidden,
            string createAlertMessage,
            bool isTenantMaster,
            bool isDevelopment
            )
        {
            Id = id;
            Name = name;
            IsEmailEnabled = isEmailEnabled;
            EmailWhitelist = emailWhitelist;
            CanClone = canClone;
            CanDelete = canDelete;
            Hidden = hidden;
            CreateAlertMessage = createAlertMessage;
            IsTenantMaster = isTenantMaster;
            IsDevelopment = isDevelopment;
        }
    }
}
