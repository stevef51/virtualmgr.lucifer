using MongoDB.Bson.Serialization.Attributes;

namespace VirtualMgr.MultiTenant
{
    [BsonIgnoreExtraElements]
    public class SqlTenantMaster
    {
        [BsonElement("name")]
        public string Name { get; set; }
        [BsonElement("connectionString")]
        public string ConnectionString { get; set; }
    }
}