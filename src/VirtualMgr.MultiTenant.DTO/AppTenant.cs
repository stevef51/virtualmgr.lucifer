﻿using System;

namespace VirtualMgr.MultiTenant
{
    public class AppTenant
    {
        public int Id { get; private set; }
        public bool ValidTenant { get; private set; }
        public string Name { get; private set; }
        public string PrimaryHostname { get; private set; }
        public string Folder { get; private set; }
        public string ConnectionString { get; private set; }
        public string TimeZone { get; private set; }
        public string Currency { get; private set; }
        public string EmailFromAddress { get; private set; }

        public AppTenantProfile ActiveProfile { get; private set; }
        public AppTenantProfile TargetProfile { get; private set; }

        public SqlTenantMaster TenantMaster { get; private set; }
        public AppTenant(
            SqlTenantMaster tenantMaster,
            int id,
            string name,
            string primaryHostname,
            string folder,
            string connectionString,
            string timeZone,
            string currency,
            string emailFromAddress,
            AppTenantProfile activeProfile,
            AppTenantProfile targetProfile)
        {
            this.ValidTenant = true;
            this.TenantMaster = tenantMaster;
            this.Id = id;
            this.Name = name;
            this.PrimaryHostname = primaryHostname.ToLower();
            this.Folder = folder;
            this.ConnectionString = connectionString;
            this.TimeZone = timeZone;
            this.Currency = currency;
            this.EmailFromAddress = emailFromAddress;
            this.ActiveProfile = activeProfile;
            this.TargetProfile = targetProfile;
        }

        public AppTenant(string primaryHostname)
        {
            this.ValidTenant = false;
            this.PrimaryHostname = primaryHostname.ToLower();
        }
    }
}
