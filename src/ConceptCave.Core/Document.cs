﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Core.Coding;

namespace ConceptCave.Core
{
    public class Document : Node, IDocument
    {
        protected CommandManager _commandManager;

        public Document()
        {
            _commandManager = new CommandManager(this);
        }

        public ICommandManager CommandManager
        {
            get { return _commandManager; }
        }

        public override void Encode(IEncoder encoder)
        {
            base.Encode(encoder);
        }

        public override void Decode(IDecoder decoder)
        {
            base.Decode(decoder);
        }
    }
}
