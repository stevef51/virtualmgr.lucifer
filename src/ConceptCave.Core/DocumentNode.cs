﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConceptCave.Core
{
    public abstract class DocumentNode : Node, IHasDocument
    {
        protected Document _document;

        public DocumentNode()
        {
        }

        public DocumentNode(Document document)
        {
            _document = document;
        }

        public override void Encode(Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("Document", _document);
        }

        public override void Decode(Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            decoder.Decode("Document", out _document);
        }

        #region IHasDocument Members

        public IDocument Document
        {
            get { return _document; }
        }

        #endregion
    }
}
