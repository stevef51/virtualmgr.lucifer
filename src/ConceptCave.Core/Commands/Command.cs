﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConceptCave.Core
{
    [Flags]
    public enum CommandCan
    {
        DoNothing       = 0,
        BeDone          = 1,
        BeUndone        = 2,
        BeRedone        = 4
    }

    public enum CommandDoneState
    {
        NotDone,
        Done,
        Undone,
        Redone
    }

    public abstract class Command : Node, ICommand
    {
        private CommandCan _commandCan = CommandCan.BeDone | CommandCan.BeUndone | CommandCan.BeRedone;
        private CommandDoneState _commandDoneState;
        public CommandDoneState CommandDoneState { get { return _commandDoneState; } }

        public Command()
        {
        }

        protected void SetCan(CommandCan commandCan)
        {
            _commandCan = commandCan;
        }

        public virtual IDocument Document { get; set; }

        protected abstract void DoIt();
        protected abstract void UndoIt();
        protected virtual void RedoIt()
        {
            DoIt();
        }

        public virtual void Do()
        {
            if (Document == null)
            {
                throw new InvalidOperationException("The document must be set before the command can be execute");
            }

            DoIt();

            _commandDoneState = CommandDoneState.Done;
        }
        
        public virtual void Redo()
        {
            if (Document == null)
            {
                throw new InvalidOperationException("The document must be set before the command can be redone");
            }

            RedoIt();

            _commandDoneState = CommandDoneState.Redone;
        }

        public virtual void Undo()
        {
            if (Document == null)
            {
                throw new InvalidOperationException("The document must be set before the command can be undone");
            }

            UndoIt();

            _commandDoneState = Core.CommandDoneState.Undone;
        }

        public virtual bool CanDo
        {
            get { return _commandDoneState == CommandDoneState.NotDone && ((_commandCan & CommandCan.BeDone) != 0); }
        }

        public virtual bool CanUndo
        {
            get { return (_commandDoneState == CommandDoneState.Done || _commandDoneState == CommandDoneState.Redone) && ((_commandCan & CommandCan.BeUndone) != 0); }
        }

        public virtual bool CanRedo
        {
            get { return _commandDoneState == CommandDoneState.Undone && ((_commandCan & CommandCan.BeRedone) != 0); }
        }
    }
}
