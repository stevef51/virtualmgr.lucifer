﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using ConceptCave.Core.Coding;

namespace ConceptCave.Core
{
    public class CommandManagerException : ApplicationException
    {
        private ICommand _command;
        public ICommand Command { get { return _command; } }

        public CommandManagerException(string message)
            : this(message, null)
        {
        }

        public CommandManagerException(string message, ICommand command)
            : base(message)
        {
            _command = command;
        }
    }

    public class CommandManager : Node, ICommandManager
    {
        /// <summary>
        /// Stack of commands that have been Done to the attached Document
        /// </summary>
        protected Stack<ICommand> _doneCommands = new Stack<ICommand>();

        /// <summary>
        /// Stack of commands that have been Undone to the attached Document
        /// </summary> 
        protected Stack<ICommand> _undoneCommands = new Stack<ICommand>();

        /// <summary>
        /// The Document this Command Manager operates against
        /// </summary>
        protected IDocument _document;

        public CommandManager(IDocument document)
        {
            _document = document;
        }

        /// <summary>
        /// Copy of the Done Commands
        /// </summary>
        public ICommand[] DoneCommands { get { return _doneCommands.ToArray(); } }

        /// <summary>
        /// Copy of the Undone Commands
        /// </summary>
        public ICommand[] UndoneCommands { get { return _undoneCommands.ToArray(); } }

        public IDocument Document
        {
            get { return _document; }
        }

        public void Do(ICommand command)
        {
            if (!command.CanDo)
                throw new CommandManagerException(string.Format("Cannot Do Command {0}", command), command);

            command.Document = _document;

            command.Do();

            _undoneCommands.Clear();

            if (command.CanUndo)
                _doneCommands.Push(command);
            else
                // Since this command cannot be Undone then we clear all done commands since it makes no sense to allow previously done commands to be 
                // Undone that will destroy the documents historical state
                _doneCommands.Clear();      
        }

        public void Undo()
        {
            if (_doneCommands.Count == 0)
                throw new CommandManagerException("Nothing to Undo");

            ICommand commandToUndo = _doneCommands.Pop();
            commandToUndo.Undo();

            if (commandToUndo.CanRedo)
                _undoneCommands.Push(commandToUndo);
            // no else since a none Redoable command would be unusual firstly and it wont destroy the document historical state
        }

        public void Redo()
        {
            if (_undoneCommands.Count == 0)
                throw new CommandManagerException("Nothing to Redo");

            ICommand commandToRedo = _undoneCommands.Pop();
            commandToRedo.Redo();

            if (commandToRedo.CanUndo)
                _doneCommands.Push(commandToRedo);
            else
                // Since this command cannot be Undone then we clear all done commands since it makes no sense to allow previously done commands to be 
                // Undone that will destroy the documents historical state
                _doneCommands.Clear();
        }

        public override void Encode(IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("DoneCommands", _doneCommands);
            encoder.Encode("UndoneCommands", _undoneCommands);
        }

        public override void Decode(IDecoder decoder)
        {
            base.Decode(decoder);

            _doneCommands = decoder.Decode<Stack<ICommand>>("DoneCommands", null);
            _undoneCommands = decoder.Decode<Stack<ICommand>>("UndoneCommands", null);
        }
    }
}
