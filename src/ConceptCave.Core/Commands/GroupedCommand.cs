﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConceptCave.Core.Commands
{
    public class GroupedCommand : Command
    {
        public readonly List<ICommand> Commands = new List<ICommand>();

        public override IDocument Document
        {
            get
            {
                return base.Document;
            }
            set
            {
                base.Document = value;
                Commands.ForEach(c => c.Document = value);
            }
        }

        public GroupedCommand()
        {
        }

        public GroupedCommand(IEnumerable<ICommand> commandsToDo)
        {
            Commands.AddRange(commandsToDo);
        }

        protected override void DoIt()
        {
            Commands.ForEach(c => c.Do());
        }

        protected override void UndoIt()
        {
            Commands.Reverse<ICommand>().ToList().ForEach(c => c.Undo());
        }

        protected override void RedoIt()
        {
            Commands.ForEach(c => c.Redo());
        }
    }
}
