﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConceptCave.Core
{
    public interface ICommand : IUniqueNode
    {
        IDocument Document { get; set; }

        void Do();
        void Undo();
        void Redo();

        bool CanDo { get; }
        bool CanUndo { get; }
        bool CanRedo { get; }
    }

}
