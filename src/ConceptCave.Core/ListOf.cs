﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConceptCave.Core
{
    public class ListOf<TItem> : Node, IListOf<TItem>
    {
        private List<TItem> _items = new List<TItem>();

        #region IListOf<TItem> Members

        public List<TItem> Items
        {
            get { return _items; }
        }

        #endregion

        public override void Decode(Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            _items = decoder.Decode<List<TItem>>("Items", null);
        }

        public override void Encode(Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("Items", _items);
        }
    }
}
