﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConceptCave.Core
{
    public interface IHasDocument
    {
        /// <summary>
        /// The document this object belongs to
        /// </summary>
        IDocument Document { get; }
    }
}
