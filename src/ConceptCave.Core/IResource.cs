﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConceptCave.Core
{
    public interface IResource : IUniqueNode
    {
        string Name { get; set; }
    }
}
