﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConceptCave.Core.Coding
{
    public interface IDecoder : ICoder
    {
        T Decode<T>(string name, T defaultValue);
        bool Decode<T>(string name, out T value);
    }
}
