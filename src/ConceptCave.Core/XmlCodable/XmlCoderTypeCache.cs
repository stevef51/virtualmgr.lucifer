﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.ComponentModel;
using System.Collections;

namespace ConceptCave.Core.XmlCodable
{
    /// <summary>
    /// Caches Assembly Qualified Type Names against a short name (preferably Type.Name) for use in XmlEncoder/XmlDecoder
    /// </summary>
    public class XmlCoderTypeCache
    {
        public static class Null
        {
        }

        /// <summary>
        /// The Global Type Cache registers primitives which do not store the Assembly Qualified Type Name in XmlElement since they are fixed
        /// </summary>
        public readonly static XmlCoderTypeCache GlobalTypeCache;
        
        /// <summary>
        /// Root element of the Type Cache
        /// </summary>
        private XmlElement _element;

        /// <summary>
        /// A Parent Type Cache that can be used if Type is not found in this cache (normally this is the GlobalTypeCache)
        /// </summary>
        private XmlCoderTypeCache _parent = null;
        
        /// <summary>
        /// Cache of Types to their short name
        /// </summary>
        private Dictionary<Type, string>        _typeToName         = new Dictionary<Type, string>();
        
        /// <summary>
        /// Reverse cache of names to their Types
        /// </summary>
        private Dictionary<string, Type>        _nameToType         = new Dictionary<string, Type>();
        
        /// <summary>
        /// Cache of short names to the XmlElement containing their Assembly Qualified Type Name
        /// </summary>
        private Dictionary<string, XmlElement>  _nameToElement      = new Dictionary<string, XmlElement>();

        static XmlCoderTypeCache()
        {
            GlobalTypeCache = new XmlCoderTypeCache();
            GlobalTypeCache.Add(typeof(Null));
            GlobalTypeCache.Add(typeof(Boolean));
            GlobalTypeCache.Add(typeof(SByte));
            GlobalTypeCache.Add(typeof(Byte));
            GlobalTypeCache.Add(typeof(Int16));
            GlobalTypeCache.Add(typeof(UInt16));
            GlobalTypeCache.Add(typeof(Int32));
            GlobalTypeCache.Add(typeof(UInt32));
            GlobalTypeCache.Add(typeof(Int64));
            GlobalTypeCache.Add(typeof(UInt64));
            GlobalTypeCache.Add(typeof(Single));
            GlobalTypeCache.Add(typeof(Double));
            GlobalTypeCache.Add(typeof(Decimal));
            GlobalTypeCache.Add(typeof(String));
            GlobalTypeCache.Add(typeof(DateTime));
            GlobalTypeCache.Add(typeof(Char));
            GlobalTypeCache.Add(typeof(Guid));
            GlobalTypeCache.Add(typeof(TimeSpan));
        }

        /// <summary>
        /// Used only for GlobalTypeCache
        /// </summary>
        private XmlCoderTypeCache()
        {
        }

        /// <summary>
        /// Create a Type Cache and use 'element' as the root of the cache
        /// </summary>
        /// <param name="element"></param>
        public XmlCoderTypeCache(XmlElement typeCacheElement)
            : this(typeCacheElement, GlobalTypeCache)
        {
        }

        /// <summary>
        /// Create a Type Cache and use 'element' as the root of the cache and a specific cache parent
        /// </summary>
        /// <param name="element"></param>
        /// <param name="parent"></param>
        public XmlCoderTypeCache(XmlElement typeCacheElement, XmlCoderTypeCache parent)
        {
            foreach (XmlNode node in typeCacheElement.ChildNodes)
            {
                XmlElement element = node as XmlElement;
                if (element == null)
                    continue;

                string name = element.Name;
                string typeName = element.InnerText;
                Type type = Type.GetType(typeName);

                this.Add(type, name);
            }

            _element = typeCacheElement;
            _parent = parent;
        }

        /// <summary>
        /// The cache root XmlElement
        /// </summary>
        public XmlElement Element
        {
            get { return _element; }
        }

        /// <summary>
        /// Readonly lookup of a Type in the cache
        /// </summary>
        /// <param name="type">Type to lookup</param>
        /// <returns>Short name for type (safe to be used as an XmlElement name) or null</returns>
        public string this[Type type]
        {
            get
            {
                string name = null;
                if (!_typeToName.TryGetValue(type, out name) && _parent != null)
                    name = _parent[type];

                return name;
            }
        }


        public Type this[string name]
        {
            get
            {
                Type type = null;
                if (!_nameToType.TryGetValue(name, out type) && _parent != null)
                    return _parent[name];

                return type;
            }
        }
        /// <summary>
        /// Add a Type with specified name (will throw if one already exists)
        /// </summary>
        /// <param name="type">Type to add</param>
        /// <param name="name">Preferred name for type</param>
        /// <returns>Name of type (safe to be used as an XmlElement name)</returns>
        public string Add(Type type, string name)
        {
            _typeToName[type] = name;
            _nameToType[name] = type;

            if (_element != null)
            {
                XmlElement element = _element.OwnerDocument.CreateElement(name);
                _element.AppendChild(element);
                _nameToElement[name] = element;
                element.InnerText = type.AssemblyQualifiedName;
            }
            return name;
        }

        /// <summary>
        /// Add a Type to the cache (will use Type.Name if possible or an alternative if that already exists in cache)
        /// </summary>
        /// <param name="type">Type to add</param>
        /// <returns>Name of type (safe to be used as an XmlElement name)</returns>
        public string Add(Type type)
        {
            string name = this[type];
            if (name == null)
            {
                int alternativeNo = 1;

                name = XmlTypeSafeName(type);
                while (_nameToElement.ContainsKey(name))
                    name = type.Name + alternativeNo++.ToString();

                Add(type, name);
            }
            return name;
        }

        private string XmlTypeSafeName(Type type)
        {
            string name = type.Name;

            if (type.IsArray)
                name = "ArrayOf_" + XmlTypeSafeName(type.GetElementType());

            else if (type.IsGenericType)
            {
                Type genericTypeDefinition = type.GetGenericTypeDefinition();

                if (genericTypeDefinition == typeof(Stack<>))
                    name = "StackOf_" + XmlTypeSafeName(type.GetGenericArguments()[0]);

                else if (genericTypeDefinition == typeof(Queue<>))
                    name = "QueueOf_" + XmlTypeSafeName(type.GetGenericArguments()[0]);

                else if (genericTypeDefinition == typeof(List<>))
                    name = "ListOf_" + XmlTypeSafeName(type.GetGenericArguments()[0]);

                else if (genericTypeDefinition == typeof(Dictionary<,>))
                    name = "DictionaryOf_" + XmlTypeSafeName(type.GetGenericArguments()[0]) + "_" + XmlTypeSafeName(type.GetGenericArguments()[1]);

                else
                {
                    Type[] genericTypes = type.GetGenericArguments();
                    foreach (Type genericType in genericTypes)
                        name += "_" + XmlTypeSafeName(genericType);
                }
            }

            return name.Replace("`", "");
        }

        public XmlElement CreateValueElement(object value, XmlElement parentElement)
        {
            Type valueType = null;
            if (value != null)
                valueType = value.GetType();
            else
                valueType = typeof(XmlCoderTypeCache.Null);

            string typeName = this.Add(valueType);

            XmlElement valueElement = parentElement.OwnerDocument.CreateElement(typeName);
            parentElement.AppendChild(valueElement);

            return valueElement;
        }
    }
}
