﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.ComponentModel;
using ConceptCave.Core.Coding;

namespace ConceptCave.Core.XmlCodable
{
    public class XmlDocumentDecoder : XmlDocumentCoder, IDecoder
    {
        public XmlDocumentDecoder(XmlElement xmlCodedElement)
        {
            _element = xmlCodedElement.SelectSingleNode(RootValueElement) as XmlElement;
            XmlCoderTypeCache typeCache = new XmlCoderTypeCache(xmlCodedElement.SelectSingleNode(TypeCacheElement) as XmlElement);
            XmlCoderObjectCache objectCache = new XmlCoderObjectCache(xmlCodedElement.SelectSingleNode(ObjectCacheElement) as XmlElement, typeCache);
            _coderContext = new XmlCoderContext(typeCache, objectCache);
        }

        public XmlDocumentDecoder(XmlElement element, XmlCoderContext coderContext)
        {
            _element = element;
            _coderContext = coderContext;
        }

        public static XmlDocumentDecoder CreateForRootElement(XmlDocument doc)
        {
            XmlElement element = doc.DocumentElement.SelectSingleNode(RootValueElement).FirstChild as XmlElement;
            XmlCoderTypeCache typeCache = new XmlCoderTypeCache(doc.DocumentElement.SelectSingleNode(TypeCacheElement) as XmlElement);
            XmlCoderObjectCache objectCache = new XmlCoderObjectCache(doc.DocumentElement.SelectSingleNode(ObjectCacheElement) as XmlElement, typeCache);
            return new XmlDocumentDecoder(element, new XmlCoderContext(typeCache, objectCache));
        }

        public T Decode<T>(string name, T defaultValue)
        {
            T result = defaultValue;
            Decode(name, out result);
            return result;
        }

        public bool Decode<T>(string name, out T value)
        {
            value = default(T);

            XmlElement valueElement = _element;
            if (name != null)
                valueElement = _element.SelectSingleNode("*[@" + NameAttribute + "='" + name + "']") as XmlElement;

            if (valueElement == null)
                return false;

            Type type = TypeCache[valueElement.Name];
            if (type == typeof(XmlCoderTypeCache.Null))
                return true;

            if (typeof(IUniqueNode).IsAssignableFrom(type) && ObjectCache != null && valueElement.HasAttribute(RefIdAttribute))
            {
                Guid id = new Guid(valueElement.GetAttribute(RefIdAttribute));
                value = (T)ObjectCache.GetObject(id);
                return true;
            }
            if (type != null)
            {
                value = (T)XmlElementConverter.DecodeFromXmlElement(valueElement, type, _coderContext);
                return true;
            }

            return false;
        }
    }
}
