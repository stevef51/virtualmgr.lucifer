﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.ComponentModel;
using System.Collections;
using ConceptCave.Core.Coding;

namespace ConceptCave.Core.XmlCodable
{
    public class XmlDocumentEncoder : XmlDocumentCoder, IEncoder
    {
        public XmlDocumentEncoder()
        {
            XmlDocument doc = new XmlDocument();
            XmlElement rootElement = doc.CreateElement(DocumentRootElement);
            doc.AppendChild(rootElement);

            XmlCoderTypeCache typeCache = new XmlCoderTypeCache(doc.CreateElement(TypeCacheElement));
            rootElement.AppendChild(typeCache.Element);

            XmlCoderObjectCache objectCache = new XmlCoderObjectCache(doc.CreateElement(ObjectCacheElement), typeCache);
            rootElement.AppendChild(objectCache.Element);

            _coderContext = new XmlCoderContext(typeCache, objectCache);

            _element = doc.CreateElement(RootValueElement);
            rootElement.AppendChild(_element);
        }

        public XmlDocumentEncoder(XmlElement element, XmlCoderContext coderContext)
        {
            _element = element;
            if (coderContext != null)
                _coderContext = coderContext;
            else
            {
                XmlCoderTypeCache typeCache = new XmlCoderTypeCache(element.OwnerDocument.CreateElement(TypeCacheElement));
                element.AppendChild(typeCache.Element);

                XmlCoderObjectCache objectCache = new XmlCoderObjectCache(element.OwnerDocument.CreateElement(ObjectCacheElement), typeCache);
                element.AppendChild(objectCache.Element);

                _coderContext = new XmlCoderContext(typeCache, objectCache);
            }
        }

        public void Encode<T>(string name, T value)
        {
            XmlElement valueElement = TypeCache.CreateValueElement(value, _element);

            if (name != null)
                valueElement.SetAttribute(NameAttribute, name);

            IUniqueNode uniqueNode = value as IUniqueNode;
            if (uniqueNode != null && ObjectCache != null && !GetContext<bool>("ForFreezing"))
            {
                ObjectCache.AddObject(uniqueNode);
                valueElement.SetAttribute(RefIdAttribute, uniqueNode.Id.ToString());
            }
            else if (value != null)
                XmlElementConverter.EncodeInXmlElement(valueElement, value, _coderContext);
        }
    }
}
