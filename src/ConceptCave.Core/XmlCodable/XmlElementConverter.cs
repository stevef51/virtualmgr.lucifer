﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Xml;
using ConceptCave.Core;
using System.Reflection;
using ConceptCave.Core.Coding;

namespace ConceptCave.Core.XmlCodable
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple=false)]
    public class XmlElementConverterAttribute : SelfRegisteringAttribute
    {
        public Type ConverterType;

        public XmlElementConverterAttribute(Type converterType)
        {
            ConverterType = converterType;
        }

        public override void RegisterAttributesOnType(Type type)
        {
            XmlElementConverter.RegisterConverterFactory(new XmlTypeConverterFactory() { TypeToConvert = type, Converter = Activator.CreateInstance(ConverterType) as IXmlElementConverter });
        }
    }

    public interface IXmlElementConverterFactory
    {
        bool CanConvertType(Type typeToConvert);
        IXmlElementConverter CreateConverter();
    }

    public interface IXmlElementConverter
    {
        void EncodeInXmlElement(XmlElement element, object value, XmlCoderContext context);
        object DecodeFromXmlElement(XmlElement element, Type typeToConvert, XmlCoderContext context);
    }

    public class XmlElementConverter
    {
        private static List<IXmlElementConverterFactory> _converterFactoryTypes = new List<IXmlElementConverterFactory>();

        static XmlElementConverter()
        {
            SelfRegisteringAttribute.RegisterFromAppDomain(AppDomain.CurrentDomain);
            RegisterConverterFactory(new XmlElementStringConvertibleConverter());
            RegisterConverterFactory(new XmlElementUniqueNodeConverter());
            RegisterConverterFactory(new XmlElementCodableConverter());
            RegisterConverterFactory(new XmlElementDictionaryConverter());
            RegisterConverterFactory(new XmlElementDictionaryEntryConverter());
            RegisterConverterFactory(new XmlElementKeyValuePairConverter());
            RegisterConverterFactory(new XmlElementPrimitiveArrayConverter());
            RegisterConverterFactory(new XmlElementArrayConverter());
            RegisterConverterFactory(new XmlElementPrimitiveCollectionConverter());
            RegisterConverterFactory(new XmlElementCollectionConverter());
        }

        public static void RegisterConverterFactory(IXmlElementConverterFactory converterFactory)
        {
            _converterFactoryTypes.Add(converterFactory);
        }

        public static void EncodeInXmlElement(XmlElement element, object value, XmlCoderContext context)
        {
            Type typeToConvert = value.GetType();
            var converter = (from c in _converterFactoryTypes where c.CanConvertType(typeToConvert) select c).First();

            if (converter == null)
            {
                throw new InvalidOperationException(string.Format("No XmlElementConverter defined for {0}", typeToConvert.FullName));
            }

            converter.CreateConverter().EncodeInXmlElement(element, value, context);
        }

        public static object DecodeFromXmlElement(XmlElement element, Type typeToConvert, XmlCoderContext context)
        {
            var converter = (from c in _converterFactoryTypes where c.CanConvertType(typeToConvert) select c).First();

            if (converter == null)
            {
                throw new InvalidOperationException(string.Format("No XmlElementConverter defined for {0}", typeToConvert.FullName));
            }

            return converter.CreateConverter().DecodeFromXmlElement(element, typeToConvert, context);
        }

        public static bool CanConvertType(Type typeToConvert)
        {
            return (from c in _converterFactoryTypes where c.CanConvertType(typeToConvert) select c).First() != null;
        }
    }

    /// <summary>
    /// Generic class which can convert any TypeConveter'able class to an XmlElement using its InvariantString representation (primarily primitive types)
    /// </summary>
    public class XmlElementStringConvertibleConverter : IXmlElementConverter, IXmlElementConverterFactory
    {
        public void EncodeInXmlElement(XmlElement element, object value, XmlCoderContext context)
        {
            TypeConverter converter = TypeDescriptor.GetConverter(value.GetType());
            element.InnerText = converter.ConvertToInvariantString(value);
        }

        public object DecodeFromXmlElement(XmlElement element, Type typeToConvert, XmlCoderContext context)
        {
            TypeConverter converter = TypeDescriptor.GetConverter(typeToConvert);
            return converter.ConvertFromInvariantString(element.InnerText);
        }

        public bool CanConvertType(Type typeToConvert)
        {
            TypeConverter converter = TypeDescriptor.GetConverter(typeToConvert);
            return converter != null && converter.CanConvertTo(typeof(String)) && converter.CanConvertFrom(typeof(String));
        }

        public IXmlElementConverter CreateConverter()
        {
            return this;
        }
    }

    public class XmlElementCodableConverter : IXmlElementConverter, IXmlElementConverterFactory
    {
        public void EncodeInXmlElement(XmlElement element, object value, XmlCoderContext context)
        {
            ICodable codable = value as ICodable;
            if (codable != null)
                codable.Encode(new XmlDocumentEncoder(element, context));
        }

        public object DecodeFromXmlElement(XmlElement element, Type typeToConvert, XmlCoderContext context)
        {
            ICodable codable = Activator.CreateInstance(typeToConvert) as ICodable;

            if (codable != null)
            {
                codable.Decode(new XmlDocumentDecoder(element, context));
            }

            return codable;
        }

        public bool CanConvertType(Type typeToConvert)
        {
            return typeof(ICodable).IsAssignableFrom(typeToConvert);
        }

        public IXmlElementConverter CreateConverter()
        {
            return this;
        }
    }


    public class XmlElementUniqueNodeConverter : IXmlElementConverter, IXmlElementConverterFactory
    {
        public void EncodeInXmlElement(XmlElement element, object value, XmlCoderContext context)
        {
            IUniqueNode uniqueNode = value as IUniqueNode;
            if (uniqueNode != null)
            {
                element.SetAttribute(XmlDocumentCoder.IdAttribute, uniqueNode.Id.ToString());
                uniqueNode.Encode(new XmlDocumentEncoder(element, context));
            }
        }

        public object DecodeFromXmlElement(XmlElement element, Type typeToConvert, XmlCoderContext context)
        {
            IUniqueNode uniqueNode = Activator.CreateInstance(typeToConvert) as IUniqueNode;

            if (uniqueNode != null)
            {
                // Must set the objects Id
                uniqueNode.UseDecodedId(new Guid(element.GetAttribute(XmlDocumentCoder.IdAttribute)));

                context.UniqueNodeCreated(uniqueNode);

                // Now we can decode remainder of the object
                uniqueNode.Decode(new XmlDocumentDecoder(element, context));
            }

            return uniqueNode;
        }

        public bool CanConvertType(Type typeToConvert)
        {
            return typeof(IUniqueNode).IsAssignableFrom(typeToConvert);
        }

        public IXmlElementConverter CreateConverter()
        {
            return this;
        }
    }

    /// <summary>
    /// Factory Class to map a Type to its XmlElementConverter type
    /// </summary>
    public class XmlTypeConverterFactory : IXmlElementConverterFactory
    {
        public Type TypeToConvert;
        public IXmlElementConverter Converter;

        public bool CanConvertType(Type typeToConvert)
        {
            return typeToConvert == TypeToConvert;
        }

        public IXmlElementConverter CreateConverter()
        {
            return Converter;
        }
    }

    public class XmlElementArrayConverter : IXmlElementConverterFactory, IXmlElementConverter
    {
        public bool CanConvertType(Type typeToConvert)
        {
            return typeToConvert.IsArray && XmlElementConverter.CanConvertType(typeToConvert.GetElementType());
        }

        public IXmlElementConverter CreateConverter()
        {
            return this;
        }

        public void EncodeInXmlElement(XmlElement element, object value, XmlCoderContext context)
        {
            Array array = value as Array;
            foreach (object item in array)
            {
                XmlDocumentEncoder encoder = new XmlDocumentEncoder(element, context);
                encoder.Encode(null, item);
            }
        }

        public object DecodeFromXmlElement(XmlElement element, Type typeToConvert, XmlCoderContext context)
        {
            ArrayList list = new ArrayList();
            foreach (XmlElement childElement in element.ChildNodes)
            {
                if (childElement.NodeType != XmlNodeType.Element)
                    continue;

                object item = null;
                XmlDocumentDecoder decoder = new XmlDocumentDecoder(childElement, context);
                decoder.Decode(null, out item);

                list.Add(item);
            }

            return list.ToArray(typeToConvert.GetElementType());
        }
    }

    public class XmlElementPrimitiveArrayConverter : IXmlElementConverterFactory, IXmlElementConverter
    {
        public bool CanConvertType(Type typeToConvert)
        {
            return typeToConvert.IsArray && typeToConvert.GetElementType().IsPrimitive;
        }

        public IXmlElementConverter CreateConverter()
        {
            return this;
        }

        public void EncodeInXmlElement(XmlElement element, object value, XmlCoderContext context)
        {
            TypeConverter typeConverter = TypeDescriptor.GetConverter(value.GetType().GetElementType());
            StringBuilder sb = new StringBuilder();
            Array array = value as Array;
            foreach (object item in array)
            {
                if (sb.Length > 0)
                    sb.Append(",");
                sb.Append(typeConverter.ConvertToInvariantString(item));
            }
            element.InnerText = sb.ToString();
        }

        public object DecodeFromXmlElement(XmlElement element, Type typeToConvert, XmlCoderContext context)
        {
            TypeConverter typeConverter = TypeDescriptor.GetConverter(typeToConvert.GetElementType());
            string[] parts = element.InnerText.Split(',');
            ArrayList list = new ArrayList();
            foreach (string part in parts)
            {
                list.Add(typeConverter.ConvertFromInvariantString(part));
            }

            return list.ToArray(typeToConvert.GetElementType());
        }
    }

    public class XmlElementPrimitiveCollectionConverter : IXmlElementConverterFactory, IXmlElementConverter
    {
        public bool CanConvertType(Type typeToConvert)
        {
            return typeof(ICollection).IsAssignableFrom(typeToConvert) && typeToConvert.IsGenericType && typeToConvert.GetGenericArguments()[0].IsPrimitive;
        }

        public IXmlElementConverter CreateConverter()
        {
            return this;
        }

        public void EncodeInXmlElement(XmlElement element, object value, XmlCoderContext context)
        {
            Type valueType = value.GetType();
            TypeConverter typeConverter = TypeDescriptor.GetConverter(valueType.GetGenericArguments()[0]);
            StringBuilder sb = new StringBuilder();
            ICollection list = value as ICollection;
            
            foreach (object item in list)
            {
                if (sb.Length > 0)
                    sb.Append(",");
                sb.Append(typeConverter.ConvertToInvariantString(item));
            }
            element.InnerText = sb.ToString();
        }

        public object DecodeFromXmlElement(XmlElement element, Type typeToConvert, XmlCoderContext context)
        {
            TypeConverter typeConverter = TypeDescriptor.GetConverter(typeToConvert.GetGenericArguments()[0]);
            string[] parts = element.InnerText.Split(',');

            Type specificListType = typeof(List<>).MakeGenericType(typeToConvert.GetGenericArguments());
            IList list = (IList)Activator.CreateInstance(specificListType);
            bool requiresReversing = typeToConvert == typeof(Stack) || typeToConvert.IsGenericType && typeToConvert.GetGenericTypeDefinition() == typeof(Stack<>);
            foreach (string part in parts)
            {
                if (requiresReversing)
                    list.Insert(0, typeConverter.ConvertFromInvariantString(part));
                else
                    list.Add(typeConverter.ConvertFromInvariantString(part));
            }

            ICollection result = (ICollection)Activator.CreateInstance(typeToConvert, list);
            return result;
        }
    }

    public class XmlElementCollectionConverter : IXmlElementConverterFactory, IXmlElementConverter
    {
        public bool CanConvertType(Type typeToConvert)
        {
            return typeof(ICollection).IsAssignableFrom(typeToConvert);
        }

        public IXmlElementConverter CreateConverter()
        {
            return this;
        }

        public void EncodeInXmlElement(XmlElement element, object value, XmlCoderContext context)
        {
            ICollection list = value as ICollection;
            foreach (object item in list)
            {
                XmlDocumentEncoder encoder = new XmlDocumentEncoder(element, context);
                encoder.Encode(null, item);
            }
        }

        public object DecodeFromXmlElement(XmlElement element, Type typeToConvert, XmlCoderContext context)
        {
            Type specificListType = typeof(List<>).MakeGenericType(typeToConvert.GetGenericArguments());
            IList list = (IList)Activator.CreateInstance(specificListType);
            bool requiresReversing = typeToConvert == typeof(Stack) || typeToConvert.IsGenericType && typeToConvert.GetGenericTypeDefinition() == typeof(Stack<>);
            foreach (XmlElement itemElement in element.ChildNodes)
            {
                XmlDocumentDecoder decoder = new XmlDocumentDecoder(itemElement, context);
                object item = null;
                if (decoder.Decode(null, out item))
                {
                    if (requiresReversing)
                        list.Insert(0, item);
                    else
                        list.Add(item);
                }
            }

            ICollection result = (ICollection)Activator.CreateInstance(typeToConvert, list);
            return result;
        }
    }

    public class XmlElementDictionaryConverter : IXmlElementConverterFactory, IXmlElementConverter
    {
        public bool CanConvertType(Type typeToConvert)
        {
            return typeof(IDictionary).IsAssignableFrom(typeToConvert);
        }

        public IXmlElementConverter CreateConverter()
        {
            return this;
        }

        public void EncodeInXmlElement(XmlElement element, object value, XmlCoderContext context)
        {
            IDictionary dictionary = value as IDictionary;
            foreach (DictionaryEntry entry in dictionary)
            {
                XmlDocumentEncoder encoder = new XmlDocumentEncoder(element, context);
                encoder.Encode(null, entry);
            }
        }

        public object DecodeFromXmlElement(XmlElement element, Type typeToConvert, XmlCoderContext context)
        {
            IDictionary dictionary = (IDictionary)Activator.CreateInstance(typeToConvert);

            foreach (XmlNode entryNode in element.ChildNodes)
            {
                if (!(entryNode is XmlElement))
                    continue;

                XmlDocumentDecoder decoder = new XmlDocumentDecoder(entryNode as XmlElement, context);                
                DictionaryEntry entry;
                if (decoder.Decode<DictionaryEntry>(null, out entry))
                    dictionary.Add(entry.Key, entry.Value);
            }

            return dictionary;
        }
    }

    public class XmlElementDictionaryEntryConverter : IXmlElementConverterFactory, IXmlElementConverter
    {
        public bool CanConvertType(Type typeToConvert)
        {
            return typeof(DictionaryEntry).IsAssignableFrom(typeToConvert);
        }

        public IXmlElementConverter CreateConverter()
        {
            return this;
        }

        public void EncodeInXmlElement(XmlElement element, object value, XmlCoderContext context)
        {
            DictionaryEntry entry = (DictionaryEntry)value;
            XmlDocumentEncoder encoder = new XmlDocumentEncoder(element, context);
            encoder.Encode("Key", entry.Key);
            encoder.Encode("Value", entry.Value);
        }

        public object DecodeFromXmlElement(XmlElement element, Type typeToConvert, XmlCoderContext context)
        {
            DictionaryEntry entry = (DictionaryEntry)Activator.CreateInstance(typeToConvert);
            XmlDocumentDecoder decoder = new XmlDocumentDecoder(element, context);
            entry.Key = decoder.Decode<object>("Key", null);
            entry.Value = decoder.Decode<object>("Value", null);
            return entry;
        }
    }

    public class XmlElementKeyValuePairConverter : IXmlElementConverterFactory, IXmlElementConverter
    {
        public bool CanConvertType(Type typeToConvert)
        {
            return typeToConvert.IsGenericType && typeToConvert.GetGenericTypeDefinition() == typeof(KeyValuePair<,>);
        }

        public IXmlElementConverter CreateConverter()
        {
            return this;
        }

        public void EncodeInXmlElement(XmlElement element, object value, XmlCoderContext context)
        {
            Type valueType = value.GetType();

            PropertyInfo piKey = valueType.GetProperty("Key");
            PropertyInfo piValue = valueType.GetProperty("Value");

            object keyValue = piKey.GetValue(value, new object[0]);
            object valueValue = piValue.GetValue(value, new object[0]);

            XmlDocumentEncoder encoder = new XmlDocumentEncoder(element, context);
            encoder.Encode("Key", keyValue);
            encoder.Encode("Value", valueValue);
        }

        public object DecodeFromXmlElement(XmlElement element, Type typeToConvert, XmlCoderContext context)
        {
            object entry = Activator.CreateInstance(typeToConvert);

            PropertyInfo piKey = typeToConvert.GetProperty("Key");
            PropertyInfo piValue = typeToConvert.GetProperty("Value");

            XmlDocumentDecoder decoder = new XmlDocumentDecoder(element, context);

            piKey.SetValue(entry, decoder.Decode<object>("Key", null), new object[0]);
            piValue.SetValue(entry, decoder.Decode<object>("Value", null), new object[0]);

            return entry;
        }
    }
}
