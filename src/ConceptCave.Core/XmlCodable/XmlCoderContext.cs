﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Core.Coding;

namespace ConceptCave.Core.XmlCodable
{
    public class XmlCoderContext
    {
        private XmlCoderTypeCache _typeCache;
        private XmlCoderObjectCache _objectCache;

        public event Action<IUniqueNode> OnUniqueNodeCreated;

        public XmlCoderTypeCache TypeCache { get { return _typeCache; } }
        public XmlCoderObjectCache ObjectCache { get { return _objectCache; } }

        public XmlCoderContext(XmlCoderTypeCache typeCache, XmlCoderObjectCache objectCache)
        {
            _typeCache = typeCache;
            _objectCache = objectCache;
        }

        public void UniqueNodeCreated(IUniqueNode uniqueNode)
        {
            if (OnUniqueNodeCreated != null)
                OnUniqueNodeCreated(uniqueNode);
        }
    }
}
