﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using ConceptCave.Core.Coding;

namespace ConceptCave.Core.XmlCodable
{
    public abstract class XmlDocumentCoder : ContextContainer, ICoder
    {
        public const string DocumentRootElement = "DocRoot";
        public const string TypeCacheElement = "TypeCache";
        public const string ObjectCacheElement = "ObjectCache";
        public const string RootValueElement = "RootValue";

        public const string NameAttribute = "Name";
        public const string RefIdAttribute = "RefId";
        public const string IdAttribute = "Id";

        protected XmlElement _element;
        protected XmlCoderContext _coderContext;

        public XmlDocument Document
        {
            get
            {
                return _element.OwnerDocument;
            }
        }

        public XmlCoderContext CoderContext
        {
            get { return _coderContext; }
        }

        public XmlCoderTypeCache TypeCache
        {
            get 
            {
                if (_coderContext == null)
                    return null;
                return _coderContext.TypeCache; 
            }
        }

        public XmlCoderObjectCache ObjectCache
        {
            get 
            {
                if (_coderContext == null)
                    return null;
                return _coderContext.ObjectCache; 
            }
        }
    }
}
