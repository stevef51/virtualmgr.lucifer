﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConceptCave.Core
{
    public class ContextContainer : IContextContainer
    {
        private Dictionary<Type, Dictionary<string, object>> _contexts = new Dictionary<Type, Dictionary<string, object>>();

        #region IContextContainer Members

        public bool HasContext<T>(string name)
        {
            Dictionary<string, object> typeDict = null;
            if (!_contexts.TryGetValue(typeof(T), out typeDict))
                return false;
            
            return typeDict.ContainsKey(name);
        }

        public T GetContext<T>(string name)
        {
            object result = default(T);
            Dictionary<string, object> typeDict = null;
            if (_contexts.TryGetValue(typeof(T), out typeDict))
                typeDict.TryGetValue(name, out result);
            return (T)result;
        }

        public void SetContext<T>(string name, T context)
        {
            Dictionary<string, object> typeDict = null;
            if (!_contexts.TryGetValue(typeof(T), out typeDict))
            {
                typeDict = new Dictionary<string, object>();
                _contexts[typeof(T)] = typeDict;
            }

            typeDict[name] = context;
        }

        #endregion
    }
}
