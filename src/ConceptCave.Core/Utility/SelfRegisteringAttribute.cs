﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace ConceptCave.Core
{
    public abstract class SelfRegisteringAttribute : Attribute
    {
        private static List<Type> _selfRegisteringTypes;

        public static void FindSelfRegisteringTypesInAssembly(Assembly assembly, List<Type> selfRegisteringTypes)
        {
            foreach (Type type in assembly.GetTypes())
            {
                if (typeof(SelfRegisteringAttribute).IsAssignableFrom(type))
                    selfRegisteringTypes.Add(type);
            }
        }

        public static void RegisterFromAppDomain(AppDomain appDomain)
        {
            if (_selfRegisteringTypes == null)
            {
                lock (typeof(SelfRegisteringAttribute))
                {
                    if (_selfRegisteringTypes == null)
                    {
                        _selfRegisteringTypes = new List<Type>();
                        foreach (Assembly assembly in appDomain.GetAssemblies())
                            FindSelfRegisteringTypesInAssembly(assembly, _selfRegisteringTypes);

                        foreach (Assembly assembly in appDomain.GetAssemblies())
                            RegisterFromAssembly(assembly, _selfRegisteringTypes);
                    }
                }
            }
        }

        public static void RegisterFromAssembly(Assembly assembly, List<Type> selfRegisteringTypes)
        {
            foreach (Type type in assembly.GetTypes())
            {
                foreach (Type selfRegisteringType in selfRegisteringTypes)
                {
                    SelfRegisteringAttribute[] attributes = type.GetCustomAttributes(selfRegisteringType, true) as SelfRegisteringAttribute[];
                    if (attributes != null)
                    {
                        foreach (SelfRegisteringAttribute sra in attributes)
                            sra.RegisterAttributesOnType(type);
                    }
                }
            }
        }

        public abstract void RegisterAttributesOnType(Type type);
    }
}
