﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConceptCave.Core
{
    public interface IContextContainer
    {
        bool HasContext<T>(string name);
        T GetContext<T>(string name);
        void SetContext<T>(string name, T context);
    }
}
