﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Core.Coding;
using System.Xml;
using ConceptCave.Core.XmlCodable;

namespace ConceptCave.Core
{
    public class FrozenState
    {
        public readonly XmlDocument State;
        
        public FrozenState(XmlDocument state)
        {
            State = state;
        }
    }

    public static class FrozenStateExtensions
    {
        public static FrozenState Freeze(this ICodable codable)
        {
            XmlDocumentEncoder encoder = new XmlDocumentEncoder();
            encoder.SetContext<bool>("ForFreezing", true);
            encoder.Encode(null, codable);
            return new FrozenState(encoder.Document);
        }

        public static void Unfreeze(this ICodable codable, FrozenState state)
        {
            XmlDocumentDecoder decoder = XmlDocumentDecoder.CreateForRootElement(state.State);
            decoder.SetContext<bool>("ForFreezing", true);
            codable.Decode(decoder);
        }
    }
}
