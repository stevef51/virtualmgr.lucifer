﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace System.Web.Mvc
{
    public static class UrlExtensionsHelper
    {
        /// <summary>
        /// Takes a relative or absolute url and returns the fully-qualified url path.
        /// </summary>
        /// <param name="text">The url to make fully-qualified. Ex: Home/About</param>
        /// <returns>The absolute url plus protocol, server, & port. Ex: http://localhost:1234/Home/About</returns>
        public static string ToFullyQualifiedUrl(this UrlHelper url, string text)
        {

            //### the VirtualPathUtility doesn"t handle querystrings, so we break the original url up
            var oldUrl = text;
            var oldUrlArray = (oldUrl.Contains("?") ? oldUrl.Split('?') : new[] { oldUrl, "" });

            //### we"ll use the Request.Url object to recreate the current server request"s base url
            //### requestUri.AbsoluteUri = "http://domain.com:1234/Home/Index?page=123"
            //### requestUri.LocalPath = "/Home/Index"
            //### requestUri.Query = "?page=123"
            //### subtract LocalPath and Query from AbsoluteUri and you get "http://domain.com:1234", which is urlBase
            var requestUri = GetRequestUri();
            var urlBase = requestUri.GetLeftPart(UriPartial.Authority);
 //           var localPathAndQuery = requestUri.LocalPath + requestUri.Query;
 //           var urlBase = requestUri.AbsoluteUri.Substring(0, requestUri.AbsoluteUri.Length - localPathAndQuery.Length);

            //### convert the request url into an absolute path, then reappend the querystring, if one was specified
            var newUrl = VirtualPathUtility.ToAbsolute(oldUrlArray[0]);
            if (!string.IsNullOrEmpty(oldUrlArray[1]))
                newUrl += "?" + oldUrlArray[1];

            //### combine the old url base (protocol + server + port) with the new local path
            return urlBase + newUrl;
        }

        public static Uri GetRequestUri()
        {
            return HttpContext.Current.Request.Url;
        }
    }
}