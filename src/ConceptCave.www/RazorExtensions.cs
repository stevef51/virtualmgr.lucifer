﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Aspose.Words;
using ConceptCave.Configuration;
using ConceptCave.www.Squisher;
using SquishIt.Framework;
using SquishIt.Mvc;
using VirtualMgr.Central;
using VirtualMgr.Membership;
using Microsoft.AspNet.Identity.Owin;

namespace System.Web.Mvc
{
    public static class RazorExtensions
    {
        public static string ThemeJSBundleUrl(this UrlHelper url)
        {
#if DEBUG
            return Bundle.JavaScript().ForceDebug().RenderCachedAssetTag("jsbundle_debug");
#else
            return Bundle.JavaScript().ForceRelease().RenderCachedAssetTag("jsbundle");
#endif
        }

        public static string ThemeCSSBundleUrl(this UrlHelper url)
        {
#if DEBUG
            try
            {
                return Bundle.Css().ForceRelease().RenderCachedAssetTag(MultiTenantManager.MakeTenantId("cssbundle"));
            }
            catch (KeyNotFoundException knfe)
            {
                // Probably new Tenant, see if we can render their content and try once more ..
                MakeBundles.MakeCSSBundle();
                return Bundle.Css().ForceRelease().RenderCachedAssetTag(MultiTenantManager.MakeTenantId("cssbundle"));
            }


            //try
            //{
            //    return Bundle.Css().ForceDebug().RenderCachedAssetTag(MultiTenantManager.MakeTenantId("cssbundle_debug"));
            //}
            //catch (KeyNotFoundException knfe)
            //{
            //    // Probably new Tenant, see if we can render their content and try once more ..
            //    MakeBundles.MakeCSSBundle();
            //    return Bundle.Css().ForceDebug().RenderCachedAssetTag(MultiTenantManager.MakeTenantId("cssbundle_debug"));
            //}
#else
            try
            {
                return Bundle.Css().MvcRenderCachedAssetTag(MultiTenantManager.MakeTenantId("cssbundle")).ToHtmlString();
//                return Bundle.Css().ForceRelease().RenderCachedAssetTag(MultiTenantManager.MakeTenantId("cssbundle"));
            }
            catch(KeyNotFoundException knfe)
            {
                // Probably new Tenant, see if we can render their content and try once more ..
                MakeBundles.MakeCSSBundle();
                return Bundle.Css().ForceRelease().RenderCachedAssetTag(MultiTenantManager.MakeTenantId("cssbundle"));
            }
#endif
        }

        //This number gets pretty big for an int
        //would be nice if totalmilliseconds was a decimal instead of a double, but ah well
        public static double ToJSTimestamp(this DateTime time)
        {
            return Math.Round(time.Subtract(new DateTime(1970, 1, 1, 0, 0, 0)).TotalMilliseconds);
        }
   
        public static string NgContent(this UrlHelper Url, string virtualPath)
        {
            //we need to do something about managing resources for mobile in here
            return Url.Content(virtualPath);
        }
    }
}