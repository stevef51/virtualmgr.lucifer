﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace ConceptCave.www.API.Support
{
    public abstract class ApiException : Exception
    {
        public HttpStatusCode StatusCode { get; set; }

        protected ApiException() : base("An error occured during the fufilment of your request")
        {
        }

        protected ApiException(string message) : base(message)
        {
            
        }

        protected ApiException(string message, Exception innerException) : base(message, innerException)
        {
            
        }
    }
}