﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace ConceptCave.www.API.Support
{
    public class NotFoundException : ApiException
    {
        public NotFoundException()
            : base("The requested resource does not exist")
        {
            StatusCode = HttpStatusCode.NotFound;
        }

        public NotFoundException(string message, Exception innerException = null) : base(message, innerException)
        {
            StatusCode = HttpStatusCode.NotFound;
        }
    }
}