﻿using ConceptCave.Checklist.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Dispatcher;
using System.Web.Security;
using VirtualMgr.Central;
using VirtualMgr.Membership;

namespace ConceptCave.www.API.Support
{
    public class BasicAuthMessageHandler : DelegatingHandler
    {
        private readonly Func<IMembership> _membershipResolver;
        private readonly Func<ITokenValidator> _fnTokenValidator;

        protected class Credentials
        {
            public string Username { get; set; }
            public string Password { get; set; }
        }

        //Adapted from http://www.piotrwalat.net/basic-http-authentication-in-asp-net-web-api-using-message-handlers/
        private const string BasicAuthResponseHeader = "WWW-Authenticate";
        private const string BasicAuthResponseHeaderValue = "Basic";

        public IMembership Membership
        {
            get
            {
                return _membershipResolver();
            }
        }

        public BasicAuthMessageHandler(HttpConfiguration config, Func<IMembership> membershipResolver, Func<ITokenValidator> fnTokenValidator)
        {
            _membershipResolver = membershipResolver;
            _fnTokenValidator = fnTokenValidator;

            //Avoids a 500 error; makes sure that after we're done, the next thing we do is dispatch to a controller.
            //Could instead chain this to something else.
            InnerHandler = new HttpControllerDispatcher(config);
        }

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            AuthenticationHeaderValue authorization = request.Headers.Authorization;
            if (authorization != null)
            {
                // Hijack Basic auth to do Bearer auth first ..
                if (authorization.Scheme == "Bearer")
                {
                    var tokenValidator = _fnTokenValidator();
                    var token = authorization.Parameter;
                    string username = null;
                    if (tokenValidator.ValidateToken(token, out username))
                    {
                        //all is good in the world; set the pricnipal
                        var ident = new GenericIdentity(username, "Basic");
                        Thread.CurrentPrincipal = new RolePrincipal(ident);
                        HttpContext.Current.User = Thread.CurrentPrincipal;
                    }
                }
                else if (!String.IsNullOrEmpty(authorization.Parameter))
                {
                    Credentials parsedCredentials = ParseAuthorizationHeader(authorization.Parameter);
                    if (parsedCredentials != null)
                    {
                        //Use ASP.NET membership to auth them
                        //Don't explicitly knock them back now, because there might be a valid forms auth cookie attached to this request too
                        //Invalid user/opass on basic header should be ignored if such token is present
                        if (Membership.ValidateUser(parsedCredentials.Username, parsedCredentials.Password))
                        {
                            //all is good in the world; set the pricnipal
                            var ident = new GenericIdentity(parsedCredentials.Username, "Basic");
                            Thread.CurrentPrincipal = new RolePrincipal(ident);
                            HttpContext.Current.User = Thread.CurrentPrincipal;
                        }
                    }
                }
            }
            return base.SendAsync(request, cancellationToken)
                .ContinueWith(task =>
                    {
                        var response = task.Result;
                        //If they were 403 forbidden, send a header indicating that basic auth will work fine.
                        if (response.StatusCode == HttpStatusCode.Unauthorized && !response.Headers.Contains(BasicAuthResponseHeader))
                        {
                            response.Headers.Add(BasicAuthResponseHeader, BasicAuthResponseHeaderValue);
                        }
                        return response;
                    });
        }

        protected Credentials ParseAuthorizationHeader(string header)
        {
            string[] creds = Encoding.ASCII.GetString(Convert.FromBase64String(header)).Split(':');
            if (creds.Length != 2 || string.IsNullOrEmpty(creds[0]) || string.IsNullOrEmpty(creds[1]))
                return null;
            return new Credentials() { Username = creds[0], Password = creds[1] };
        }
    }
}