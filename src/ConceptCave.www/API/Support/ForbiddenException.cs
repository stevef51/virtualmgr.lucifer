﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace ConceptCave.www.API.Support
{
    public class ForbiddenException : ApiException
    {
        public ForbiddenException() : base("Access to this content is forbidden for this user")
        {
            StatusCode = HttpStatusCode.Forbidden;
        }

        public ForbiddenException(string message) : base(message)
        {
            StatusCode = HttpStatusCode.Forbidden;
        }
    }
}