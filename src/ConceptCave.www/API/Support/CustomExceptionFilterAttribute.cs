﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Filters;
using System.Net.Http;
using System.Net;
using ConceptCave.Checklist.Lingo;
using NLog;
using System.IO;

namespace ConceptCave.www.API.Support
{
    public class CustomExceptionFilterAttribute : ExceptionFilterAttribute
    {
        private static readonly Logger log = LogManager.GetCurrentClassLogger();

        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            string message;
            var exception = actionExecutedContext.Exception;
            HttpStatusCode status;

 //           Elmah.ErrorSignal.FromCurrentContext().Raise(exception);

            LogEventInfo logEvent = new LogEventInfo(LogLevel.Error, "API", exception.Message);
            logEvent.Properties["Line"] = "NA";
            logEvent.Properties["WorkingDocument"] = "NA";
            logEvent.Properties["Phase"] = "NA";
            logEvent.Properties["Username"] = "Unknown";

            if (System.Threading.Thread.CurrentPrincipal != null)
            {
                logEvent.Properties["Username"] = System.Threading.Thread.CurrentPrincipal.Identity.Name;
            }
            logEvent.Properties["UserVariables"] = "NA";
            logEvent.Exception = exception;

            if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.Request != null)
            {
                logEvent.Properties["RequestRawUrl"] = System.Web.HttpContext.Current.Request.RawUrl;
                logEvent.Properties["RequestMethod"] = System.Web.HttpContext.Current.Request.HttpMethod;
                try
                {
                    System.Web.HttpContext.Current.Request.InputStream.Position = 0;
                    var reader = new StreamReader(System.Web.HttpContext.Current.Request.InputStream);
                    logEvent.Properties["RequestBody"] = reader.ReadToEnd();
                }
                catch(Exception e)
                { }
            }

            if (exception is ApiException)
            {
                log.Log(logEvent);
                var tException = exception as ApiException;
                status = tException.StatusCode;
                message = tException.Message;
                actionExecutedContext.Response = actionExecutedContext.Request.CreateErrorResponse(
                    status, message);
                
            }
            else if (exception is LingoException)
            {
                var tException = exception as LingoException;
                var exceptionModel = new LingoExceptionReturnModel(tException);

                if (tException.Ast != null)
                {
                    logEvent.Properties["Line"] = tException.Ast.Location.Line;
                }
                logEvent.Properties["WorkingDocument"] = exceptionModel.WorkingDocumentId.ToString();
                logEvent.Properties["Phase"] = exceptionModel.Phase;

                if (exceptionModel.UserVariableValues != null)
                {
                    System.Text.StringBuilder userVars = new System.Text.StringBuilder();
                    exceptionModel.UserVariableValues.ToList().ForEach(v => userVars.AppendFormat("{0} = {1}<br/>", v.Name, v.Value));
                    logEvent.Properties["UserVariables"] = userVars.ToString();
                }

                log.Log(logEvent);
                actionExecutedContext.Response =
                    actionExecutedContext.Request.CreateResponse(HttpStatusCode.InternalServerError,
                        exceptionModel);

            }
            else
            {
                log.Log(logEvent);
                status = HttpStatusCode.InternalServerError;
                actionExecutedContext.Response = actionExecutedContext.Request.CreateResponse(
                    status, new LingoExceptionReturnModel(exception));
            }
        }
    }
}