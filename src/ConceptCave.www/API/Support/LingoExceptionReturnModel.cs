﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConceptCave.Checklist;
using ConceptCave.Checklist.Lingo;
using DocumentFormat.OpenXml.Bibliography;
using Irony;

namespace ConceptCave.www.API.Support
{
    public class LingoExceptionReturnModel
    {
        public enum ErrorPhase
        {
            Runtime, Compiletime, StringExpansion, System
        }

        public ErrorPhase Phase { get; protected set; }
        public string State {get { return "Error"; }}
        public bool CanPrevious { get; protected set; }
        public bool HavePresented { get; protected set; }
        public Guid WorkingDocumentId { get; protected set; }
        public IList<LingoExceptionVariable> AllVariableValues { get; protected set; }
        public IList<LingoExceptionVariable> UserVariableValues { get; protected set; }
        public IList<LingoExceptionVariable> ProgramVariableValues { get; protected set; }
        public IList<SourceCodeExcerpt> SourceExcerpts { get; protected set; } 
        
        public IList<InnerExceptionData> InnerExceptions { get; protected set; }

        public LingoExceptionReturnModel(Exception ex)
        {
            Phase = ErrorPhase.System;
            SourceExcerpts = new List<SourceCodeExcerpt>();
            SourceExcerpts.Add(new SourceCodeExcerpt("This error has not occurred in the context of Lingo execution", "An error has occurred while processing your request"));
            InnerExceptions = new List<InnerExceptionData>();
            SetInnerExceptions(ex);
        }

        public LingoExceptionReturnModel(LingoException ex)
        {
            //what phase?
            Phase = ex.Program == null ? ErrorPhase.Compiletime : ErrorPhase.Runtime;
            if (ex is ExpandStringException)
                Phase = ErrorPhase.StringExpansion;

            InnerExceptions = new List<InnerExceptionData>();
            switch (Phase)
            {
                case ErrorPhase.Compiletime:
                    SourceExcerpts = ex.ParserMessages.Select(m => new SourceCodeExcerpt(m, ex)).ToList();
                    CanPrevious = false;
                    HavePresented = false;
                    WorkingDocumentId = Guid.Empty;
                    break;
                case ErrorPhase.Runtime:
                    SourceExcerpts = new List<SourceCodeExcerpt>();
                    SourceExcerpts.Add(new SourceCodeExcerpt(ex));
                    HavePresented = ex.Program.WorkingDocument.Presented.Any();
                    SetVariables(ex);
                    SetInnerExceptions(ex);

                    CanPrevious = ex.Program.WorkingDocument.CanPrevious();
                    break;
                case ErrorPhase.StringExpansion:
                    var expex = ex as ExpandStringException;
                    HavePresented = ex.Program.WorkingDocument.Presented.Count() > 1; //one of these is the one that failed
                    CanPrevious = ex.Program.WorkingDocument.CanPrevious();
                    SourceExcerpts = new List<SourceCodeExcerpt>();
                    SourceExcerpts.Add(new SourceCodeExcerpt(expex.Code, 
                        string.Format("{0} ({1}, named {2})", expex.Message, expex.QuestionType, expex.QuestionName)));
                    SetVariables(ex);
                    SetInnerExceptions(ex);
                    break;
            }
        }

        private string StringifyVariable(object value)
        {
            if (value == null)
            {
                return "<null>";
            }
            else if (value is IEnumerable<object>) //covariant
            {
                var seq = (IEnumerable<object>) value;
                var stringSeq = seq.Select(StringifyVariable);
                return "[" + string.Join(",", stringSeq) + "]";
            }
            else
            {
                return value.ToString();
            }
        }

        private void SetVariables(LingoException ex)
        {
            WorkingDocumentId = ex.Program.WorkingDocument.Id;
            AllVariableValues =
                        ex.Program.WorkingDocument.Variables.Select(
                            v =>
                                new LingoExceptionVariable(v.Name ?? "", StringifyVariable(v.ObjectValue))).ToList();
            UserVariableValues =
                AllVariableValues.Where(v => !string.IsNullOrEmpty(v.Name) && !v.Name.StartsWith(".")).ToList();
            ProgramVariableValues = AllVariableValues.Where(v => v.Name.StartsWith(".")).ToList();
        }

        private void SetInnerExceptions(Exception ex)
        {
            if (ex == null) return;
            InnerExceptions.Add(new InnerExceptionData(ex));
            SetInnerExceptions(ex.InnerException);
        }
    }

    public class SourceCodeExcerpt
    {
        protected const int ContextSize = 2;
        public int ErrorLine { get; protected set; }
        public int StartLine { get; protected set; }
        public int EndLine { get; protected set; }
        public IList<string> SourceCodeLines { get; protected set; }
        public string Message { get; protected set; }

        public SourceCodeExcerpt(LogMessage log, LingoException ex) : this(ex, log.Message, log.Location.Line)
        {
        }

        public SourceCodeExcerpt(LingoException ex) : this(ex, ex.Message, ex.Ast.Location.Line)
        {
        }

        public SourceCodeExcerpt(string line, string message)
        {
            ErrorLine = 1;
            StartLine = 1;
            EndLine = 1;
            SourceCodeLines = new string[]{line};
            Message = message;
        }

        protected SourceCodeExcerpt(LingoException ex, string message, int errorLine)
        {
            ErrorLine = errorLine + 1; //irony reports zero-based lines in both ex.Ast and log.Location
            StartLine = Math.Max(ErrorLine - ContextSize, 1);
            var totalProgramLines = ex.ProgramDefinition.SourceCode.Split('\n').Count();
            EndLine = Math.Min(ErrorLine + ContextSize, totalProgramLines);

            //This line skips the initial lines then takes 5 lines of context
            SourceCodeLines = ex.ProgramDefinition.SourceCode.Split('\n').Skip(StartLine - 1).Take(2 * ContextSize + 1).ToList();
            Message = message;
        }
    }

    public class LingoExceptionVariable
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public LingoExceptionVariable() {}

        public LingoExceptionVariable(string name, string value)
        {
            Name = name;
            Value = value;
        }
    }

    public class InnerExceptionData
    {
        public string Type { get; set; }
        public string Message { get; set; }
        public string StackTrace { get; set; }

        public InnerExceptionData(Exception ex)
        {
            Type = ex.GetType().FullName;
            Message = ex.Message;
            StackTrace = ex.StackTrace;
        }
    }
}