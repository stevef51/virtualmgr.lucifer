﻿using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.www.Attributes;
using ConceptCave.www.Models;
using System.Net.Http;
using System.Web.Http;

namespace ConceptCave.www.API.v1
{
    [WebApiCorsOptionsAllow]
    public class PeriodicExecutionController : ApiController
    {
        private PeriodicExecutionLogic _logic;

        public PeriodicExecutionController(PeriodicExecutionLogic logic)
        {
            _logic = logic;
        }

        [HttpGet]
        [HttpPost]
        [HttpOptions]
        public HttpResponseMessage Execute(string filter = null)
        {
            var result = new PeriodicExecutionLogic.ExecutionResults();
            if (!_logic.PerformExecute(filter, result, new ContinuousProgressJSONResponse()))
            {
                return Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, result);
            }

            return Request.CreateResponse(System.Net.HttpStatusCode.OK, result);
        }
    }
}