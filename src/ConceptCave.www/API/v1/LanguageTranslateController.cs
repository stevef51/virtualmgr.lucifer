﻿using ConceptCave.BusinessLogic.Models;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.SimpleExtensions;
using ConceptCave.www.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Http;

namespace ConceptCave.www.API.v1
{
    [WebApiCorsOptionsAllow]
    public class LanguageTranslateController : ApiController
    {
        protected ILanguageRepository _languageRepo;
        protected ILanguageTranslationRepository _languageTranslationRepo;

        public LanguageTranslateController(ILanguageRepository languageRepo, ILanguageTranslationRepository languageTranslationRepo)
        {
            _languageRepo = languageRepo;
            _languageTranslationRepo = languageTranslationRepo;
        }

        [HttpGet]
        [HttpOptions]
        public IDictionary<string, string> NGTranslations(string cultureName)
        {
            var all = _languageTranslationRepo.GetAllTranslations(cultureName);
            var ng = all.ToDictionary(t => t.TranslationId, t => t.NativeText);
            return ng;
        }

        [HttpPost]
        [HttpOptions]
        public void MissingTranslation(string translationId, string nativeText)
        {
            // The post could be from any culture, but we are only interested in missing English entries as these are the master records to which other languages translate from
            var dto = _languageTranslationRepo.GetTranslation("en", translationId);
            if (dto == null)
            {
                dto = new LanguageTranslationDTO();
                dto.__IsNew = true;
                dto.CultureName = "en";
                dto.TranslationId = translationId;
            }
            dto.NativeText = nativeText;

            _languageTranslationRepo.SaveTranslation(dto, false, false);
        }

        [HttpGet]
        [HttpOptions]
        public IEnumerable<LanguageModel> AvailableLanguages()
        {
            return _languageRepo.AllLanguages().Select(l => new LanguageModel() { culturename = l.CultureName, languagename = l.LanguageName, nativename = l.NativeName });
        }
    }
}