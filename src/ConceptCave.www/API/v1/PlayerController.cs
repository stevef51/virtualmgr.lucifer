﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Transactions;
using System.Web;
using System.Web.Http;
using System.Web.Security;
using System.Xml;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.RunTime;
using ConceptCave.Core.XmlCodable;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Repository;
using ConceptCave.Repository.LingoQuery.Datastores;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.www.API.Support;
using ConceptCave.BusinessLogic;
using ConceptCave.BusinessLogic.Models;
using Microsoft.CSharp.RuntimeBinder;
using Newtonsoft.Json.Linq;
using ConceptCave.Checklist.Facilities;
using ConceptCave.BusinessLogic.Facilities;
using ConceptCave.Checklist;
using ConceptCave.BusinessLogic.Configuration;
using System.IO;
using ConceptCave.www.Attributes;
using Newtonsoft.Json;
using VirtualMgr.Membership;
using VirtualMgr.Membership.Interfaces;

namespace ConceptCave.www.API.v1
{
    [WebApiCorsAuthorize(Roles = "User")]
    [WebApiCorsOptionsAllow]
    public class PlayerController : ApiController, IWorkingDocumentFactory, IExecutionMethodHandlerContext
    {
        private readonly IUserAgentRepository _userAgentRepo;
        private readonly IWorkingDocumentRepository _wdRepo;
        private readonly IWorkingDocumentStateManager _wdStateManager;
        private readonly IMembershipRepository _memberRepo;
        private readonly IGlobalSettingsRepository _settingRepo;
        private readonly IMediaManager _mediaManager;
        private readonly HttpContext _httpCtx;
        private readonly IReflectionFactory _reflectionFactory;
        private readonly IRepositorySectionManager _repositorySectionManager;
        private readonly IErrorLogRepository _errorLogRepo;
        private readonly IExecutionHandlerQueueRepository _executionQueueRepo;
        private readonly IUrlParser _urlParser;
        private readonly IMembership _membership;
        private readonly ICurrentContextProvider _membershipContextProvider;

        public PlayerController(IUserAgentRepository userAgentRepo, 
            IWorkingDocumentRepository wdRepo,
            IWorkingDocumentStateManager wdStateManager, 
            HttpContext httpCtx, 
            IReflectionFactory reflectionFactory, 
            IRepositorySectionManager repositorySectionManager, 
            IMediaManager mediaManager, 
            IGlobalSettingsRepository settingRepo,
            IErrorLogRepository errorLogRepo,
            IExecutionHandlerQueueRepository executionQueueRepo,
            IUrlParser urlParser,
            IMembership membership,
            ICurrentContextProvider membershipContextProvider)
        {
            _userAgentRepo = userAgentRepo;
            _wdRepo = wdRepo;
            _wdStateManager = wdStateManager;
            _settingRepo = settingRepo;
            //_memberRepo = memberRepo;
            _mediaManager = mediaManager;
            _httpCtx = httpCtx;

            _reflectionFactory = reflectionFactory;
            _repositorySectionManager = repositorySectionManager;
            _errorLogRepo = errorLogRepo;
            _executionQueueRepo = executionQueueRepo;
            _urlParser = urlParser;
            _membership = membership;
            _membershipContextProvider = membershipContextProvider;
        }


        public class BeginModel
        {
            public int groupId { get; set; }
            public int resourceId { get; set; }
            public Guid? revieweeId { get; set; }
            public string args { get; set; }
            public int? publishingGroupResourceId { get; set; }
            public string jsonArgs { get; set; }
        }

        public PlayerModel CreateStartAndSave(int groupId, int resourceId, string args)
        {
            Guid reviewerId = _membershipContextProvider.InternalUserId;

            return CreateStartAndSave(groupId, resourceId, reviewerId, reviewerId, args);
        }

        public PlayerModel CreateStartAndSave(int groupId, int resourceId, Guid reviewerId, Guid? revieweeId, string args, int? publishingGroupResourceId = null)
        {
            Dictionary<string, object> arguments = null;
            if (!string.IsNullOrEmpty(args))
            {
                try
                {
                    // 0 0
                    // 1 3
                    // 2 2
                    // 3 1
                    var argsb64 = args;
                    if ((argsb64.Length & 3) != 0)
                    {
                        argsb64 = argsb64.PadRight((args.Length + 4) & ~3, '=');
                    }
                    var b64 = Convert.FromBase64String(argsb64);
                    var ms = new MemoryStream(b64);
                    using (var sr = new StreamReader(ms, System.Text.Encoding.UTF8))
                    {
                        var b64s = sr.ReadToEnd();
                        arguments = JsonConvert.DeserializeObject<Dictionary<string, object>>(b64s);
                    }
                }
                catch
                {
                    arguments = HrefFacility.DecodeArguments(args);
                }
            }
            WorkingDocumentDTO documentEntity = null;
            IWorkingDocument documentObject = null;

            using (var scope = new TransactionScope(TransactionScopeOption.Required, TimeSpan.FromMinutes(10)))
            {
                var agent = _userAgentRepo.GetOrCreate(_httpCtx.Request.UserAgent.ToString());
                try
                {
                    WorkingDocumentStateManager.WorkingDocumentContainer docContainer;
                    if (publishingGroupResourceId.HasValue)
                        docContainer = _wdStateManager.BeginWorkingDocument(publishingGroupResourceId.Value, reviewerId, revieweeId, null, arguments);
                    else
                        docContainer = _wdStateManager.BeginWorkingDocument(groupId, resourceId, reviewerId, revieweeId, null, arguments);

                    documentEntity = docContainer.DocumentEntity;
                    documentObject = docContainer.DocumentObject;
                }
                catch (ArgumentException e)
                {
                    throw new NotFoundException(
                        String.Format("The checklist specified by groupId {0} and resourceId {1} was not found", groupId,
                                      resourceId), e);
                }


                documentEntity.RevieweeId = revieweeId;
                documentEntity.ClientIp = _httpCtx.Request.UserHostAddress;
                documentEntity.UserAgentId = agent.Id;
                documentEntity.DateCreated = DateTime.UtcNow;
                documentEntity = _wdStateManager.SaveWorkingDocument(documentObject, documentEntity);

                //now advance it to the first presentable
                InjectServerOnlyObjects(documentObject);
                var returnModel = _wdStateManager.AdvanceWorkingDocument(documentObject, documentEntity);

                //save WD after this
                _wdStateManager.SaveWorkingDocument(documentObject, documentEntity);
                scope.Complete();
                return returnModel;
            }
        }

        public IWorkingDocument CreateFrom(WorkingDocumentDTO dto)
        {
            var wd = _wdStateManager.WorkingDocumentFromString(dto.WorkingDocumentData.Data);
            //now advance the WD
            InjectServerOnlyObjects(wd);

            return wd;
        }

        //This method actually sets up the checklist to be started, and calls next through 
        [HttpPost]
        [HttpOptions]
        public PlayerModel Begin([FromBody] BeginModel beginModel)
        {
            return CreateStartAndSave(
                beginModel.groupId,
                beginModel.resourceId,
                _membershipContextProvider.InternalUserId,
                beginModel.revieweeId,
                beginModel.args,
                beginModel.publishingGroupResourceId);
        }

        [HttpPost]
        [HttpOptions]
        public PlayerModel Save(AnswerModel answerModel)
        {
            //load up WD
            var documentEntity = LoadWorkingDocument(answerModel.WorkingDocumentId);
            var wd = _wdStateManager.WorkingDocumentFromString(documentEntity.WorkingDocumentData.Data);
            //now advance the WD
            InjectServerOnlyObjects(wd);

            using (var scope = new TransactionScope(TransactionScopeOption.Required, TimeSpan.FromMinutes(10)))
            {
                var returnModel = _wdStateManager.SaveState(wd, documentEntity, answerModel.Answers);

                //save the WD
                _wdStateManager.SaveWorkingDocument(wd, documentEntity);
                scope.Complete();
                return returnModel;
            }
        }

        [HttpPost]
        [HttpOptions]
        public PlayerModel Next(AnswerModel answerModel)
        {
            //load up WD
            var documentEntity = LoadWorkingDocument(answerModel.WorkingDocumentId);
            var wd = _wdStateManager.WorkingDocumentFromString(documentEntity.WorkingDocumentData.Data);
            //now advance the WD
            InjectServerOnlyObjects(wd);
            
            using (var scope = new TransactionScope(TransactionScopeOption.Required, TimeSpan.FromMinutes(10)))
            {
                // Fix for EVS-699 "Mandatory" flag in checklist stops AutoFinish 
                // Pass the document DTO by reference since AdvanceWorkingDocument *can* (not always) actually save the DTO
                // and return a new instance, we need this new instance since we save it below, if we dont then we save stale data
                // which causes state issues like EVS-699
                var returnModel = _wdStateManager.AdvanceWorkingDocument(wd, ref documentEntity, answerModel.Answers);

                //save the WD
                _wdStateManager.SaveWorkingDocument(wd, documentEntity);
                scope.Complete();
                return returnModel;
            }
        }

        [HttpPost]
        [HttpOptions]
        public PlayerModel Previous(AnswerModel answerModel)
        {
            var documentEntity = LoadWorkingDocument(answerModel.WorkingDocumentId);
            var wd = _wdStateManager.WorkingDocumentFromString(documentEntity.WorkingDocumentData.Data);
            InjectServerOnlyObjects(wd);

            using (var scope = new TransactionScope(TransactionScopeOption.Required, TimeSpan.FromMinutes(10)))
            {
                dynamic returnModel = _wdStateManager.ReverseWorkingDocument(wd, documentEntity);
                _wdStateManager.SaveWorkingDocument(wd, documentEntity);
                scope.Complete();
                return returnModel;
            }
        }

        [HttpPost]
        [HttpOptions]
        public bool Cancel(Guid id)
        {
            using (var scope = new TransactionScope(TransactionScopeOption.Required, TimeSpan.FromMinutes(10)))
            {
                var result = _wdStateManager.CancelWorkingDocument(id);
                scope.Complete();
                return result;
            }
        }

        [HttpGet]
        [HttpOptions]
        public PlayerModel Continue(Guid id)
        {
            var wddto = LoadWorkingDocument(id);
            var wdobj = _wdStateManager.WorkingDocumentFromString(wddto.WorkingDocumentData.Data);
            InjectServerOnlyObjects(wdobj);
            using (var scope = new TransactionScope(TransactionScopeOption.Required, TimeSpan.FromMinutes(10)))
            {
                var pm = _wdStateManager.ContinueWorkingDocument(wdobj, wddto);
                scope.Complete();
                return pm;
            }
        }

        [HttpGet]
        [HttpOptions]
        public DashboardModel Dashboard()
        {
            //who is this user, and what are their dashboards?
            var uid = _membershipContextProvider.InternalUserId;
            var dbList = _wdStateManager.PlayDashboards(uid, InjectServerOnlyObjects);
            var pm = new DashboardModel()
            {
                Dashboards = dbList,
                GeneratedAtUtc = DateTime.UtcNow,
                IsStale = false
            };
            return pm;
        }

        [HttpPost]
        [HttpOptions]
        public JToken Execute(ExecuteModel executeModel)
        {
            string type = _repositorySectionManager.ExecutionHandlerTypeForName(executeModel.Handler);
            
            IExecutionMethodHandler handler = (IExecutionMethodHandler)_reflectionFactory.InstantiateObject(Type.GetType(type));

            if (handler is IServerSideQueuedExecutionHandler)
            {
                // it does support server side queueing. So what we are trying to do here
                // is build in some resilliance to server side issues and let the client go ahead with
                // what its wanting to do
                var queueHandler = (IServerSideQueuedExecutionHandler)handler;

                if(queueHandler.MethodSupportsQueue(executeModel.Method, executeModel.Parameters) == true)
                {
                    string primaryEntityId = queueHandler.ExractPrimaryEntityId(executeModel.Method, executeModel.Parameters);

                    JObject queuedResult = new JObject();
                    queuedResult["queued"] = true;

                    if (string.IsNullOrEmpty(primaryEntityId) == false)
                    {
                        // ok so queuing is supported by the handler and method and we have an id

                        // the id here is that we queue as a last resort, however we want to play actions in the same order
                        // as they occurr against what ever the primary entity is. So if there are items not processed already
                        // queued up, then we add to that queue to guarantee sequentiality. If there aren't then we try and
                        // do the execution now and only attempt to queue if that fails. If the reason for the failure is some
                        // db problen, then that'll likely cause the queue operation to fail and the client will receive an exception
                        // but we've done our best to save things and the client should be queueing offline stuff anyway, so it
                        // will try again at some point.
                        if(_executionQueueRepo.GetUnProcessedCountForPrimaryEntity(executeModel.Handler, primaryEntityId) > 0)
                        {
                            // ther are already queued items for this primary entity that haven't been processed yet.
                            // to maintain the sequence of actions, we just queue this request and move on
                            _executionQueueRepo.Queue(executeModel.Handler, executeModel.Method, primaryEntityId, executeModel.Parameters.ToString(), "Previous actions for entity unprocessed");

                            return queuedResult;
                        }
                        else
                        {
                            try
                            {
                                // so nothng queued, so lets see if we can execute, if we can't we'll queue this request
                                return handler.Execute(executeModel.Method, executeModel.Parameters, this);
                            }
                            catch(Exception e)
                            {
                                _executionQueueRepo.Queue(executeModel.Handler, executeModel.Method, primaryEntityId, executeModel.Parameters.ToString(), e.Message);

                                return queuedResult;
                            }
                        }
                    }
                }
            }

            // doesn't support server side queueing, so just execute and be done with it
            return handler.Execute(executeModel.Method, executeModel.Parameters, this);
        }

        [HttpPost]
        [HttpOptions]
        public void LogError(JToken request)
        {
            var agent = _userAgentRepo.GetOrCreate(_httpCtx.Request.UserAgent.ToString());

            Func<JToken, string, string> extract = (jtoken, key) =>
                {
                    var value = jtoken[key];
                    if (value == null)
                        return null;
                    return value.ToString();
                };

            var error = request["error"];
            var errorType = error["type"];
            var type = extract(errorType, "type") ?? "<Unknown Type>";
            var source = extract(errorType, "source") ?? "<Unknown Source>";
            var message = extract(errorType, "message") ?? "<Unknown Message>";
            var stackTrace = extract(errorType, "stack") ?? "<Unknown Stack Trace>";

            _errorLogRepo.LogError(new ErrorLogTypeDTO()
            {
                Type = type,
                Source = source,
                Message = message,
                StackTrace = stackTrace
            }, new ErrorLogItemDTO()
            {
                UserId = _membershipContextProvider.InternalUserId,
                UserAgentId = agent.Id,
                Data = extract(error, "data")
            });
        }

        //This method gets the workign dcoument from the DB
        //but alsoc hecks for appropriate permissions: can only load if your'e the reivewer
        private WorkingDocumentDTO LoadWorkingDocument(Guid id)
        {
            var entity = _wdRepo.GetById(id, WorkingDocumentLoadInstructions.Data);
            if (entity == null)
                throw new NotFoundException(string.Format("No working document with Id {0} was found", id));
            return entity;
        }

        private void InjectServerOnlyObjects(IWorkingDocument doc)
        {
            doc.Program.Set<ILingoDatastore>("dbquestions", new QuestionDatastore());
            doc.Program.Set<ILingoDatastore>("dbchecklists", new ChecklistDatastore());
            doc.Program.Set<ILingoDatastore>("dbscoredchecklists",new ScoredChecklistDatastore());
            doc.Program.Set<ILingoDatastore>("dbthischecklist", new CurrentDocumentDatastore());
            doc.Program.Set<ILingoDatastore>("dbmemberships", new MembershipDatastore());
            doc.Program.Set<ILingoDatastore>("dbpublishedresources", new PublishedChecklistDatastore());
            doc.Program.Set<ILingoDatastore>("dbmembershipcontexts", new MembershipContextDatastore());
            doc.Program.Set<IPlayerStringRenderer>(new ConceptCave.www.Services.PlayerStringRenderer(_mediaManager, _settingRepo, _urlParser));
        }

        public object CurrentUserId
        {
            get 
            {
                return _membershipContextProvider.InternalUserId;
            }
        }
    }
}