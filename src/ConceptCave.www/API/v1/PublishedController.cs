﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using ConceptCave.Repository;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.www.API.Support;
using ConceptCave.BusinessLogic.Models;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.www.Attributes;

namespace ConceptCave.www.API.v1
{
    [Authorize(Roles = "User")]
    [WebApiCorsOptionsAllow]
    public class PublishedController : ApiController
    {
        private readonly IPublishingGroupResourceRepository _publishingGroupResourceRepository;
        private readonly IPublishingGroupRepository _publishingGroupRepository;

        public PublishedController(IPublishingGroupResourceRepository publishingGroupResourceRepository,
                                    IPublishingGroupRepository publishingGroupRepository)
        {
            _publishingGroupResourceRepository = publishingGroupResourceRepository;
            _publishingGroupRepository = publishingGroupRepository;
        }

        [HttpGet]
        public IList<EnumerateChecklistsResponseItem> Checklists()
        {
            var checklists = _publishingGroupResourceRepository.GetChecklistsUserCanDo(HttpContext.Current.User.Identity.Name);
            return checklists.Select(c => new EnumerateChecklistsResponseItem(c)).ToList();
        }

        [HttpGet]
        public EnumerateChecklistsResponseItem GetById(int id)
        {
            var result = _publishingGroupResourceRepository.GetById(id, PublishingGroupResourceLoadInstruction.Resource);
            if (result == null)
            {
                throw new NotFoundException();
            }
            else
            {
                return new EnumerateChecklistsResponseItem(result);
            }
        }

        [HttpGet]
        public DetailChecklistResponseItem Checklists(int id)
        {
            var clist = _publishingGroupResourceRepository.GetById(id, PublishingGroupResourceLoadInstruction.Resource);
            if (clist == null)
            {
                throw new NotFoundException();
            }
            else if (_publishingGroupRepository.UserIsAllowedGroup(HttpContext.Current.User.Identity.Name,
                clist.PublishingGroupId, PublishingGroupActorType.Reviewer))
            {
                var reviewees = _publishingGroupRepository.GetUsersForGroup(clist.PublishingGroupId,
                                                                            PublishingGroupActorType.Reviewee);
                var reviewers = _publishingGroupRepository.GetUsersForGroup(clist.PublishingGroupId,
                                                                            PublishingGroupActorType.Reviewer);
                return new DetailChecklistResponseItem(clist, reviewers, reviewees);
            }
            else
            {
                throw new ForbiddenException();
            }
        }

        [HttpGet]
        public IList<EnumerateGroupsResponseItem> Groups()
        {
            var groupEnts = _publishingGroupRepository.GetGroupsUserCanDo(HttpContext.Current.User.Identity.Name,
                                                                          PublishingGroupActorType.Reviewer);
            var result = from e in groupEnts
                         select new EnumerateGroupsResponseItem(e);
            return result.ToList();
        }

        [HttpGet]
        public DetailGroupResponseItem Groups(int id)
        {
            var group = _publishingGroupRepository.GetById(id, PublishingGroupLoadInstruction.Resources |
                PublishingGroupLoadInstruction.Actors | PublishingGroupLoadInstruction.Labels);
            if (group == null)
            {
                throw new NotFoundException();
            }
            else if (_publishingGroupRepository.UserIsAllowedGroup(HttpContext.Current.User.Identity.Name, group.Id, PublishingGroupActorType.Reviewer))
            {
                var reviewees = _publishingGroupRepository.GetUsersForGroup(id, PublishingGroupActorType.Reviewee);
                return new DetailGroupResponseItem(group, group.PublishingGroupResources, reviewees);
            }
            else
            {
                throw new ForbiddenException();
            }
        }
    }
}