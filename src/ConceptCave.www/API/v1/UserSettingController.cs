﻿using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.www.Attributes;
using ConceptCave.www.Squisher;
using Newtonsoft.Json.Linq;
using SquishIt.Framework;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Security;
using VirtualMgr.Central;

namespace ConceptCave.www.API.v1
{
    [WebApiCorsAuthorize(Roles = "User")]
    [WebApiCorsOptionsAllow]
    public class UserSettingController : ApiController
    {
        private readonly IGlobalSettingsRepository _globalSettingsRepo;

        public UserSettingController(IGlobalSettingsRepository globalSettingsRepo)
        {
            _globalSettingsRepo = globalSettingsRepo;
        }

        [HttpGet]
        [HttpOptions]
        public string Get(string name)
        {
            var setting = _globalSettingsRepo.GetSetting<string>("User:" + name);
            return setting;
        }

        public class SetUserSettingRequest
        {
            public string name { get; set; }
            public string value { get; set; }
        }
        [HttpPost]
        [HttpOptions]
        public void Put(SetUserSettingRequest request)
        {
            _globalSettingsRepo.SetSetting("User:" + request.name, request.value);
        }

    }
}