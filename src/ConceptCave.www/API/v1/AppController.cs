﻿using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.www.App_Start;
using ConceptCave.www.Areas.Admin.Models;
using ConceptCave.www.Attributes;
using ConceptCave.www.Squisher;
using Newtonsoft.Json.Linq;
using Ninject;
using NLog;
using SquishIt.Framework;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Security;
using VirtualMgr.Central;
using VirtualMgr.Central.Interfaces;
using VirtualMgr.Membership;

namespace ConceptCave.www.API.v1
{
    [WebApiCorsOptionsAllow]
    public class AppController : ApiController
    {
        private const string FORCED_APP_UPDATE = "Forced App Update";

        private static readonly Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        private readonly IMembershipRepository _membershipRepo;
        private readonly IVersionsRepository _versionsRepo;
        private readonly ITabletRepository _tabletRepo;
        private readonly IGlobalSettingsRepository _globalSettingsRepo;
        private readonly IMultiTenantRepository _multiTenantRepo;
        private readonly IMembership _membership;

        private readonly IKernel _kernel;

        public AppController(IVersionsRepository versionsRepo, ITabletRepository tabletRepo, IMembershipRepository membershipRepo, IGlobalSettingsRepository globalSettingsRepo, IMultiTenantRepository multiTenantRepo, IMembership membership, Func<IKernel> fnKernel)
        {
            _versionsRepo = versionsRepo;
            _tabletRepo = tabletRepo;
            _membershipRepo = membershipRepo;
            _globalSettingsRepo = globalSettingsRepo;
            _multiTenantRepo = multiTenantRepo;
            _kernel = fnKernel();
            _membership = membership;
        }

        // GET api/<controller>
        [HttpGet]
        [HttpOptions]
        [WebApiCorsOptionsAllow]
        public string VersionHash()
        {
            var hash = "";
            
            // Not exactly a Hash but the Bundles use a Hash of their content in their URL ..
            hash += Bundle.JavaScript().ForceRelease().RenderCachedAssetTag("jsbundle") + Environment.NewLine;

            try
            {
                hash += Bundle.Css().ForceRelease().RenderCachedAssetTag(MultiTenantManager.MakeTenantId("cssbundle")) + Environment.NewLine;
            }
            catch(KeyNotFoundException knfe)
            {
                // Probably new Tenant, see if we can render their content and try once more ..
                MakeBundles.MakeCSSBundle();
                hash += Bundle.Css().ForceRelease().RenderCachedAssetTag(MultiTenantManager.MakeTenantId("cssbundle")) + Environment.NewLine;
            }
            var appVersion = _versionsRepo.GetVersion(FORCED_APP_UPDATE, RepositoryInterfaces.Enums.VersionLoadInstructions.None);
            if (appVersion != null)
                hash += string.Format("{0}={1}", appVersion.Id, appVersion.Version) + Environment.NewLine;

            // By including Tenant Master version it allows us to Force update all tenants through the Tenant Master
            appVersion = _versionsRepo.GetTenantMasterVersion(FORCED_APP_UPDATE, RepositoryInterfaces.Enums.VersionLoadInstructions.None);
            if (appVersion != null)
                hash += string.Format("Tenant Master {0}={1}", appVersion.Id, appVersion.Version) + Environment.NewLine;

            return hash;
        }

        [Authorize]
        [HttpPost]
        public void ForceAppUpdate()
        {
            var appVersion = _versionsRepo.GetVersion(FORCED_APP_UPDATE, RepositoryInterfaces.Enums.VersionLoadInstructions.None);
            if (appVersion == null)
            {
                appVersion = new DTO.DTOClasses.VersionDTO();
                appVersion.__IsNew = true;
                appVersion.Id = FORCED_APP_UPDATE;
            }
            appVersion.Version = DateTime.UtcNow.ToString("o");
            _versionsRepo.Save(appVersion);

            ConceptCave.www.Squisher.MakeBundles.RemoveCssBundle();
            Bundle.Css().ClearCache();
            Bundle.JavaScript().ClearCache();

            ConceptCave.www.Squisher.MakeBundles.MakeCSSBundle();

            HttpRuntime.Cache.Insert("ForcedAppUpdate", Guid.NewGuid());
        }

        public class NameValuePair
        {
            public string Name { get; set; }
            public string Value { get; set; }

            public NameValuePair()
            {
            }
        }

        [HttpGet]
        public IEnumerable<NameValuePair> GetVersions()
        {
            return from v in _versionsRepo.GetAll(RepositoryInterfaces.Enums.VersionLoadInstructions.None) select new NameValuePair() { Name = v.Id, Value = v.Version };
        }

        [HttpGet]
        public IEnumerable<NameValuePair> GetTenantInfo()
        {
            var result = new List<NameValuePair>();
            try
            {
                DateTime? webappStartTimeUtc = HttpContext.Current.Cache.Get("WebAppStartTime") as DateTime?;
                DateTime? webappStartTimeLocal = webappStartTimeUtc?.ToLocalTime();

                result.Add(new NameValuePair() { Name = "WebApp Start Time UTC", Value = webappStartTimeUtc?.ToString("yyyy-MM-dd HH:mm:ss") });
                result.Add(new NameValuePair() { Name = "WebApp Start Time", Value = webappStartTimeLocal?.ToString("yyyy-MM-dd HH:mm:ss") });
            }
            catch
            {

            }
            result.Add(new NameValuePair() { Name = "WebApp", Value = (ConfigurationManager.AppSettings["WEBSITE_SITE_NAME"] ?? "").ToLower() });
            result.Add(new NameValuePair() { Name = "Hostname", Value = MultiTenantManager.CurrentTenant.HostName });
            
            Action<string> addEnvironmentVariable = (name) => result.Add(new NameValuePair() { Name = name, Value = Environment.GetEnvironmentVariable(name) });

            addEnvironmentVariable("COMPUTERNAME");
            addEnvironmentVariable("REGION_NAME");
            
            var connectionString = new SqlConnectionStringBuilder(MultiTenantManager.CurrentTenant.ConnectionString);
            result.Add(new NameValuePair() { Name = "Primary Server", Value = connectionString.DataSource });
            result.Add(new NameValuePair() { Name = "Failover Server", Value = connectionString.FailoverPartner });
            result.Add(new NameValuePair() { Name = "InitialCatalog", Value = connectionString.InitialCatalog });
            using (var cn = new SqlConnection(connectionString.ConnectionString))
            {
                try
                {
                    cn.Open();
                    result.Add(new NameValuePair() { Name = "Data Source", Value = cn.DataSource });
                }
                catch(Exception ex)
                {
                    result.Add(new NameValuePair() { Name = "Data Source", Value = ex.Message });
                }
                finally
                {
                    if (cn.State == System.Data.ConnectionState.Open)
                    {
                        cn.Close();
                    }
                }
            }
            return result;
        }

        public class UpdateTabletRequest
        {
            public string TabletUUID { get; set; }
            public string Description { get; set; }
        }

        [HttpPost]
        [HttpOptions]
        [WebApiCorsAuthorize(Roles = "User")]
        [WebApiCorsOptionsAllow]
        public bool UpdateTablet(UpdateTabletRequest request)
        {
            var tabletUuid = _tabletRepo.GetTabletUuid(request.TabletUUID, TabletUuidLoadInstructions.None);
            if (tabletUuid == null)
            {
                tabletUuid = new TabletUuidDTO()
                {
                    __IsNew = true,
                    Uuid = request.TabletUUID,
                    LastContactUtc = DateTime.UtcNow
                };
            }
            tabletUuid.Description = request.Description;
            _tabletRepo.SaveTabletUuid(tabletUuid, false, false);
            return true;
        }

        public class GetProfileForTabletRequest
        {
            public string TabletUUID { get; set; }
        }

        public class GetProfileForTabletResponse
        {
            public bool requireupdatetablet { get; set; }
            public int? autologoutlongsecs { get; set; }
            public int? autologoutshortsecs { get; set; }
            public bool allownumberpadlogin { get; set; }
            public Guid? siteid { get; set; }
            public bool enablefma { get; set; }
            public bool enableenvironmentalsensing { get; set; }
            public string settingspassword { get; set; }
            public string logoutpassword { get; set; }
            public ConfirmLogoutType confirmlogout { get; set; }
            public int loginphotoevery { get; set; }
        }
        [HttpPost]
        [HttpOptions]
        [WebApiCorsAuthorize(Roles = "User")]
        [WebApiCorsOptionsAllow]

//        [WebApiCorsOptionsAllow(Origins = Consts.DefaultCorsAllowOrigin)]
        public GetProfileForTabletResponse GetProfileForTablet(GetProfileForTabletRequest request)
        {
            var userId = (Guid)_membership.GetUser().ProviderUserKey;
            var user = _membershipRepo.GetById(userId, MembershipLoadInstructions.TabletProfile);
            
            bool requireUpdateTablet = false;
            var tabletUuid = _tabletRepo.GetTabletUuid(request.TabletUUID, TabletUuidLoadInstructions.Tablet | TabletUuidLoadInstructions.AndTabletProfile);
            if (tabletUuid == null || string.IsNullOrEmpty(tabletUuid.Description) || tabletUuid.Tablet == null || tabletUuid.Tablet.TabletProfile == null)
                requireUpdateTablet = true;

            if (tabletUuid != null)
            {
                tabletUuid.LastContactUtc = DateTime.UtcNow;
                _tabletRepo.SaveTabletUuid(tabletUuid, false, false);
            }

            var tablet = tabletUuid != null ? tabletUuid.Tablet : null;
            var tabletProfile = tablet != null ? tablet.TabletProfile : null;
            var overrideProfile = user.TabletProfile ?? tabletProfile;

            return new GetProfileForTabletResponse()
            {
                requireupdatetablet = requireUpdateTablet,
                // The site the Tablet is at, no override
                siteid = tablet != null ? tablet.SiteId : null,
                // Allow user override for Auto logout stuff
                autologoutlongsecs = overrideProfile != null ? overrideProfile.AutoLogoutLongSecs : null,
                autologoutshortsecs = overrideProfile != null ? overrideProfile.AutoLogoutShortSecs : null,
                // Allow Numberpad login is specific to the Tablet Profile
                allownumberpadlogin = tabletProfile != null ? tabletProfile.AllowNumberPadLogin : false,
                confirmlogout = tabletProfile != null ? (ConfirmLogoutType)tabletProfile.ConfirmLogout : ConfirmLogoutType.None,
                enablefma = tabletProfile != null ? tabletProfile.EnableFma : false,
                enableenvironmentalsensing = tabletProfile != null ? tabletProfile.EnableEnvironmentalSensing : false,
                settingspassword = tabletProfile != null ? tabletProfile.SettingsPassword : string.Empty,
                logoutpassword = tabletProfile != null ? tabletProfile.LogoutPassword : string.Empty,
                loginphotoevery = tabletProfile?.LoginPhotoEvery ?? 0
            };
        }

        [HttpGet]
        [HttpOptions]
        [WebApiCorsOptionsAllow]
        public string WebAppVersion()
        {
            return typeof(ConceptCave.www.MvcApplication).Assembly.GetName().Version.ToString();
        }

        [HttpGet]
        [HttpOptions]
        [WebApiCorsOptionsAllow]
        public IList<TimeZoneInfo> TimeZones()
        {
            return TimeZoneInfo.GetSystemTimeZones();
        }


        [HttpGet]
        [HttpOptions]
        [WebApiCorsAuthorizeSecureDomains]
        public object[] GetTenants()
        {
            return (from t in MultiTenantManager.Tenants
                    select new
                    {
                        id = t.Id,
                        name = t.Name
                    }).ToArray();
        }

        [HttpGet]
        [HttpOptions]
        [WebApiCorsAuthorizeSecureDomains]
        public async Task<Areas.Admin.Controllers.MultiTenantAdminController.TenantModel> GetTenant(int id)
        {
            var controller = _kernel.Get<Areas.Admin.Controllers.MultiTenantAdminController>();
            var dto = _multiTenantRepo.GetTenantById(id);
            if (dto == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            return await controller.MakeModelAsync(dto);
        }

        [HttpPost]
        [WebApiCorsOptionsAllow(Consts.SecureDomains)]
        [Authorize]
        public string TenantDbConnectionString()
        {
            return MultiTenantManager.CurrentTenant.ConnectionString;
        }
    }
}