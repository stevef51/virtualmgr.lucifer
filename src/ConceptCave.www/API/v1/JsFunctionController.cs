﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Transactions;
using System.Web;
using System.Web.Http;
using System.Web.Security;
using System.Xml;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Repository;
using ConceptCave.Repository.LingoQuery.Datastores;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using Microsoft.CSharp.RuntimeBinder;
using Newtonsoft.Json.Linq;
using System.IO;
using ConceptCave.www.Attributes;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.NodeServices;
using ConceptCave.Checklist.Interfaces;

namespace ConceptCave.www.API.v1
{
    [WebApiCorsAuthorize(Roles = "User")]
    [WebApiCorsOptionsAllow]
    public class JsFunctionController : ApiController
    {
        private readonly IJSFunctionRepository _jsFunctionRepo;
        private readonly IJsFunctionService _jsFuncService;

        public JsFunctionController(IJSFunctionRepository jsFunctionRepo, IJsFunctionService jsFuncService)
        {
            _jsFunctionRepo = jsFunctionRepo;
            _jsFuncService = jsFuncService;
        }

        public object GetScript()
        {
            return new
            {
                functions = (from jsf in _jsFunctionRepo.GetAll()
                             select new
                             {
                                 id = jsf.Id,
                                 name = jsf.Name,
                                 type = ((JSFunctionType)jsf.Type).ToString(),
                                 script = jsf.Script
                             }).ToArray(),
                utcnow = DateTime.UtcNow.ToString("o")
            };
        }

        [HttpGet]
        public string Execute(string name, string args)
        {
            return _jsFuncService.Execute<dynamic>(name, args).result.ToString();
        }
    }
}