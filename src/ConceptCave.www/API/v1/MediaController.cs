﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Transactions;
using System.Web;
using System.Web.Http;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Repository;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.RepositoryInterfaces.ViewDefs;
using ConceptCave.www.Attributes;
using Newtonsoft.Json.Linq;
namespace ConceptCave.www.API.v1
{
    [WebApiCorsOptionsAllow]
    public class MediaController : ApiController
    {
        private readonly IMediaRepository _mediaRepo;
        private readonly IMembershipRepository _membershipRepo;

        public MediaController(IMediaRepository mediaRepo,
            IMembershipRepository membershipRepo)
        {
            _mediaRepo = mediaRepo;
            _membershipRepo = membershipRepo;
        }

        [Authorize(Roles = "User")]
        [HttpGet]
        public HttpResponseMessage GetMediaItem(Guid id)
        {
            var ent = _mediaRepo.GetById(id, RepositoryInterfaces.Enums.MediaLoadInstruction.Data);
            var stream = new MemoryStream(ent.MediaData.Data);
            var resp = new HttpResponseMessage(System.Net.HttpStatusCode.OK);
            resp.Content = new StreamContent(stream);
            resp.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue(ent.Type);

            return resp;
        }

        [HttpPost]
        [HttpOptions]
        [WebApiCorsOptionsAllow]
        public async Task<bool> UploadLoginPhoto()
        {
            var request = this.Request;
            var loginKey = HttpLoginKey.GetLoginKey();
            var uid = _membershipRepo.GetByUsername(this.User.Identity.Name, MembershipLoadInstructions.None).UserId;

            var base64 = await request.Content.ReadAsStringAsync();
            var data = Convert.FromBase64String(base64);

            using (TransactionScope scope = new TransactionScope())
            {
                var loginLog = _membershipRepo.GetLogin(loginKey);
                if (loginLog != null)
                {
                    var mediaDTO = new MediaDTO()
                    {
                        __IsNew = true,
                        CreatedBy = uid,
                        DateCreated = DateTime.UtcNow,
                        Description = "Login photo",
                        Filename = "loginphoto.jpg",
                        Id = Guid.NewGuid(),
                        Type = "image/jpg",
                        UseVersioning = false
                    };
                    var mediaDataDTO = new MediaDataDTO()
                    {
                        __IsNew = true,
                        MediaId = mediaDTO.Id,
                        Data = data,
                    };
                    mediaDTO.MediaData = mediaDataDTO;
                    mediaDataDTO.Medium = mediaDTO;

                    _mediaRepo.Save(mediaDTO, false, true);

                    loginLog.LoginMediaId = mediaDTO.Id;

                    _membershipRepo.SaveLogin(loginLog);
                }
                scope.Complete();
            }

            return true;
        }

    }
}