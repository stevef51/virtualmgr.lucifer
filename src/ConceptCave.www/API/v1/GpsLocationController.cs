﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;
using System.Web.UI;
using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.BusinessLogic.Models;
using ConceptCave.www.Attributes;

namespace ConceptCave.www.API.v1
{
    [WebApiCorsOptionsAllow]
    public class GpsLocationController : ApiController
    {
        private readonly IGpsLocationRepository _gpsLocationRepo;

        public GpsLocationController(IGpsLocationRepository gpsLocationRepo)
        {
            _gpsLocationRepo = gpsLocationRepo;
        }

        [HttpGet]
        public GpsLocationModel Get(int id)
        {
            var record = _gpsLocationRepo.GetById(id, RepositoryInterfaces.Enums.GpsLocationLoadInstructions.None);
            if (record == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);
            return GpsLocationModel.FromDTO(record);
        }

        [HttpPost] 
        public GpsLocationModel Save(GpsLocationModel model)
        {
            var dto = _gpsLocationRepo.GetById(model.id, RepositoryInterfaces.Enums.GpsLocationLoadInstructions.None);
            if (dto == null)
            {
                dto = new GpsLocationDTO()
                {
                    __IsNew = true
                };
            }

            dto.Latitude = model.pos.lat;
            dto.Longitude = model.pos.lng;
            dto.Altitude = model.pos.alt;
            dto.TimestampUtc = DateTime.UtcNow;
            dto.LocationType = (int)model.locationtype;
            dto.AccuracyMetres = model.accuracy;
            dto.AltitudeAccuracyMetres = model.altitudeaccuracy;
            dto.Floor = model.floor;

            dto = _gpsLocationRepo.Save(dto, true, true);

            return GpsLocationModel.FromDTO(dto);
        }
    }
}