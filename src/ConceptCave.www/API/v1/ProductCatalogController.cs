﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Transactions;
using System.Web;
using System.Web.Http;
using System.Web.Security;
using System.Xml;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Repository;
using ConceptCave.Repository.LingoQuery.Datastores;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using Microsoft.CSharp.RuntimeBinder;
using Newtonsoft.Json.Linq;
using System.IO;
using ConceptCave.www.Attributes;
using System.Text;
using ConceptCave.BusinessLogic;

namespace ConceptCave.www.API.v1
{
    [WebApiCorsAuthorize(Roles = "User")]
    [WebApiCorsOptionsAllow]
    public class ProductCatalogController : ApiController
    {
        // Nasty having to use this but in order to keep exact same JS render logic as the QuestionOrder we really need to
        private readonly RenderConverter _renderConverter;

        public ProductCatalogController(RenderConverter renderConverter)
        {
            _renderConverter = renderConverter;
        }

        [HttpGet]
        public JToken CatalogsForFacilityStructure(int id)
        {
            return _renderConverter.CatalogsForFacilityStructure(id);
        }
    }
}