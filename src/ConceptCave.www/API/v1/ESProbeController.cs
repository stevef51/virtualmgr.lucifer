﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Transactions;
using System.Web;
using System.Web.Http;
using System.Web.Security;
using System.Xml;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Repository;
using ConceptCave.Repository.LingoQuery.Datastores;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using Microsoft.CSharp.RuntimeBinder;
using Newtonsoft.Json.Linq;
using System.IO;
using ConceptCave.www.Attributes;
using System.Text;


namespace ConceptCave.www.API.v1
{
    public class ESSensorModel
    {
        public int id { get; set; }
        public int probeid { get; set; }
        public int sensorindex { get; set; }
        public int sensortype { get; set; }
        public string compliance { get; set; }
        public static ESSensorModel FromDto(ESSensorDTO dto)
        {
            return new ESSensorModel()
            {
                id = dto.Id,
                probeid = dto.ProbeId,
                sensorindex = dto.SensorIndex,
                sensortype = dto.SensorType,
                compliance = dto.Compliance
            };
        }
    }

    public class ESProbeModel
    {
        public int id { get; set; }
        public bool archived { get; set; }
        public string name { get; set; }
        public string key { get; set; }
        public string model { get; set; }
        public string serialnumber { get; set; }
        public int? assetid { get; set; }
        public List<ESSensorModel> sensors { get; set; }


        public static ESProbeModel FromDTO(ESProbeDTO dto)
        {
            return new ESProbeModel()
            {
                id = dto.Id,
                name = dto.Name,
                key = dto.Key,
                archived = dto.Archived,
                model = dto.Model,
                serialnumber = dto.SerialNumber,
                assetid = dto.AssetId,
                sensors = dto.Sensors != null ? (from s in dto.Sensors select ESSensorModel.FromDto(s)).ToList() : null
            };
        }
    }

    [WebApiCorsAuthorize(Roles = RoleRepository.COREROLE_ASSETADMIN)]
    [WebApiCorsOptionsAllow]
    public class ESProbeController : ApiController
    {
        private readonly IESProbeRepository _esProbeRepo;
        private readonly IESSensorRepository _esSensorRepo;

        public ESProbeController(IESProbeRepository probeRepo, IESSensorRepository sensorRepo)
        {
            _esProbeRepo = probeRepo;
            _esSensorRepo = sensorRepo;
        }

        [HttpGet]
        public ESProbeModel ESProbe(int id)
        {
            var dto = _esProbeRepo.GetById(id, ESProbeLoadInstructions.Sensors);
            if (dto == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            return ESProbeModel.FromDTO(dto);
        }

        [HttpDelete]
        public bool ESProbeDelete(int id)
        {
            var dto = _esProbeRepo.GetById(id);
            if (dto == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            var archived = false;
            _esProbeRepo.Delete(id, true, out archived);
            return archived;
        }

        [HttpPost]
        public ESProbeModel ESProbeSave([FromBody] ESProbeModel record)
        {
            var dto = _esProbeRepo.GetById(record.id, ESProbeLoadInstructions.Sensors);
            if (dto == null)
            {
                dto = new ESProbeDTO()
                {
                    __IsNew = true
                };
            }

            dto.Archived = record.archived;
            dto.Name = record.name;
            dto.AssetId = record.assetid;
            // Key, Model, Serial are readonly

            foreach(var sensor in record.sensors)
            {
                var sensorDto = dto.Sensors.FirstOrDefault(d => d.Id == sensor.id);
                if (sensorDto != null)
                {
                    sensorDto.Compliance = sensor.compliance;
                }
            }
            _esProbeRepo.Save(dto, false, true);

            return ESProbeModel.FromDTO(dto);
        }

        [HttpGet]
        public object Search(string search, int? pageNumber, int? itemsPerPage, bool includeArchived = false, int? searchFlags = null, int? includeFlags = null)
        {
            int totalItems = 0;
            var items = _esProbeRepo.Search("%" + search + "%", pageNumber, itemsPerPage, includeArchived, ref totalItems, searchFlags, includeFlags);

            return new { count = totalItems, items = items.Select(i => ESProbeModel.FromDTO(i)).ToList() };
        }

       
    }
}