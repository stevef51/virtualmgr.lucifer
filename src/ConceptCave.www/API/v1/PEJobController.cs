﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Transactions;
using System.Web;
using System.Web.Http;
using System.Web.Security;
using System.Xml;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Repository;
using ConceptCave.Repository.LingoQuery.Datastores;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using Microsoft.CSharp.RuntimeBinder;
using Newtonsoft.Json.Linq;
using System.IO;
using ConceptCave.www.Attributes;
using System.Text;

namespace ConceptCave.www.API.v1
{
    public class PEJobModel
    {
        public int id { get; set; }
        public bool archived { get; set; }
        public string name { get; set; }
        public string cronexpression { get; set; }
        public bool enabled { get; set; }
        public JObject jobdata { get; set; }
        public string jobtype { get; set; }
        public DateTime? nextrunutc { get; set; }

        public static PEJobModel FromDTO(PEJobDTO dto)
        {
            return new PEJobModel()
            {
                id = dto.Id,
                name = dto.Name,
                cronexpression = dto.Cronexpression,
                archived = dto.Archived,
                enabled = dto.Enabled,
                jobdata = !string.IsNullOrEmpty(dto.JobData) ? JObject.Parse(dto.JobData) : null,
                jobtype = dto.JobType,
                nextrunutc = dto.NextRunUtc
            };
        }
    }

    [WebApiCorsAuthorize(Roles = RoleRepository.COREROLE_CRON)]
    [WebApiCorsOptionsAllow]
    public class PEJobController : ApiController
    {
        private readonly IPEJobRepository _peJobRepo;

        public PEJobController(IPEJobRepository peJobRepo)
        {
            _peJobRepo = peJobRepo;
        }

        [HttpGet]
        public PEJobModel PEJob(int id)
        {
            var dto = _peJobRepo.GetById(id);
            if (dto == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            return PEJobModel.FromDTO(dto);
        }

        [HttpDelete]
        public bool PEJobDelete(int id)
        {
            var dto = _peJobRepo.GetById(id);
            if (dto == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            var archived = false;
            _peJobRepo.Delete(id, true, out archived);
            return archived;
        }

        [HttpPost]
        public PEJobModel PEJobSave([FromBody] PEJobModel record)
        {
            var dto = _peJobRepo.GetById(record.id);
            if (dto == null)
            {
                dto = new PEJobDTO()
                {
                    __IsNew = true,
                    JobType = record.jobtype
                };
            }

            dto.Archived = record.archived;
            dto.Name = record.name;
            dto.JobType = record.jobtype;
            dto.Cronexpression = record.cronexpression;
            dto.Enabled = record.enabled;
            dto.JobData = record.jobdata?.ToString();

            _peJobRepo.Save(dto, true);

            return PEJobModel.FromDTO(dto);
        }

        [HttpGet]
        public object Search(string search, int? pageNumber, int? itemsPerPage, bool includeArchived = false)
        {
            int totalItems = 0;
            var items = _peJobRepo.Search("%" + search + "%", pageNumber, itemsPerPage, includeArchived, ref totalItems);

            return new { count = totalItems, items = items.Select(i => PEJobModel.FromDTO(i)).ToList() };
        }


    }
}