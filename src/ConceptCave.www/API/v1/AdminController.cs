﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ConceptCave.BusinessLogic.Json;
using ConceptCave.Repository;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.www.Attributes;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ConceptCave.www.API.v1
{
    [Authorize(Roles = RoleRepository.COREROLE_ADMIN)]
    [WebApiCorsOptionsAllow]
    public class AdminController : ApiController
    {
        protected ILabelRepository _labelRepo;
        protected IHierarchyRepository _hierarchyRepo;
        private readonly VirtualMgr.Membership.IRoles _roles;

        public AdminController(ILabelRepository labelRepo, IHierarchyRepository hierarchyRepo, VirtualMgr.Membership.IRoles roles)
        {
            _labelRepo = labelRepo;
            _hierarchyRepo = hierarchyRepo;
            _roles = roles;
        }

        protected JsonSerializer GetSerializer()
        {
            var serialiser = new JsonSerializer();

            serialiser.Formatting = Formatting.Indented;
            serialiser.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            LowercaseContractResolver resolver = new LowercaseContractResolver();
            serialiser.ContractResolver = resolver;

            return serialiser;
        }

        [HttpGet]
        public JObject Labels(RepositoryInterfaces.Enums.LabelFor type)
        {
            var result = _labelRepo.GetAll(type);

            JObject r = new JObject();
            r["result"] = JArray.FromObject(result, GetSerializer());

            return r;
        }

        [HttpGet]
        public JObject Roles()
        {
            var result = _roles.GetAllRoles();

            JObject r = new JObject();
            r["result"] = JArray.FromObject(result, GetSerializer());

            return r;
        }

        [HttpGet]
        public JObject Hierarchies()
        {
            var result = _hierarchyRepo.GetAllHierarchies();

            JObject r = new JObject();
            r["result"] = JArray.FromObject(result, GetSerializer());

            return r;
        }
    }
}