﻿using ConceptCave.BusinessLogic.Facilities;
using ConceptCave.Checklist.Facilities;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.www.API.Support;
using ConceptCave.www.Attributes;
using eWAY.Rapid;
using eWAY.Rapid.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ConceptCave.www.API.v1
{
    [WebApiCorsOptionsAllow]
    public class OrderController : ApiController
    {
        private PendingPaymentFacility _facility;

        public OrderController(PendingPaymentFacility facility)
        {
            _facility = facility;
        }

        public class ApplyCouponCodeRequest
        {
            public string couponCode { get; set; }
            public Guid orderId { get; set; }
        }

        [HttpPost]
        public object ApplyCouponCode(ApplyCouponCodeRequest request)
        {
            return _facility.ApplyCouponCode(request.couponCode, request.orderId, true);
        }

        public class RemoveCouponCodeRequest
        {
            public Guid couponId { get; set; }
            public Guid orderId { get; set; }
        }

        [HttpPost]
        public object RemoveCouponCode(RemoveCouponCodeRequest request)
        {
            return _facility.RemoveCouponCode(request.couponId, request.orderId, true);
        }

        public class OrderCouponsRequest
        {
            public Guid orderId { get; set; }
        }

        [HttpPost]
        public object OrderCoupons(OrderCouponsRequest request)
        {
            return _facility.GetOrderCoupons(request.orderId);
        }

        public class CancelOrderRequest
        {
            public Guid orderId { get; set; }
        }
        [HttpPost]
        public object CancelOrder(CancelOrderRequest request)
        {
            return _facility.CancelOrder(request.orderId);
        }
    }
}