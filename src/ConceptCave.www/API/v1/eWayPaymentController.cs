﻿using ConceptCave.BusinessLogic.Facilities;
using ConceptCave.Checklist.Facilities;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.www.API.Support;
using ConceptCave.www.Attributes;
using eWAY.Rapid;
using eWAY.Rapid.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ConceptCave.www.API.v1
{
    [WebApiCorsOptionsAllow]
    public class eWayPaymentController : ApiController
    {
        private Func<bool, eWAY.Rapid.IRapidClient> _fnRapidClient;
        private eWayPaymentFacility _facility;

        public eWayPaymentController(Func<bool, eWAY.Rapid.IRapidClient> fnRapidClient, eWayPaymentFacility facility)
        {
            _fnRapidClient = fnRapidClient;
            _facility = facility;
        }

        public class QueryTransactionRequest
        {
            public Guid pendingPaymentId { get; set; }
            public bool? ignoreIfProcessing { get; set; }
        }

        [HttpPost]
        public eWayPaymentFacility.QueryTransactionResponse QueryTransaction(QueryTransactionRequest request)
        {
            return _facility.QueryTransaction(request.pendingPaymentId, request.ignoreIfProcessing ?? false);
        }

        public class PreparePaymentRequest
        {
            public Guid pendingPaymentId { get; set; }
        }

        [HttpPost]
        public eWAY.Rapid.Models.CreateTransactionResponse PreparePayment(PreparePaymentRequest request)
        {
            return _facility.CreateTransaction(request.pendingPaymentId);
        }

        [HttpPost]
        public string[] GetErrorDescriptions(string[] errorCodes, string language = "EN")
        {
            return (from errorCode in errorCodes select eWAY.Rapid.RapidClientFactory.UserDisplayMessage(errorCode, language)).ToArray();
        }

        [HttpPost]
        public object CheckProcessingPayments()
        {
            return _facility.CheckProcessingPayments();
        }
    }
}