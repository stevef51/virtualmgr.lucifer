﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Security;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.www.Attributes;
using Newtonsoft.Json;
using VirtualMgr.Central;
using VirtualMgr.Central.Interfaces;
using VirtualMgr.JsReport;

namespace ConceptCave.www.API.v1
{
    [WebApiCorsOptionsAllow]
    public class ReportsController : ApiController
    {
        protected IReportManagementRepository _reportRepo;
        protected IMembershipRepository _membershipRepo;
        protected IGlobalSettingsRepository _globalSettingRepo;
        protected readonly IServerUrls _serverUrls;
        private readonly ITokenGenerator _tokenGenerator;

        public ReportsController(
            IReportManagementRepository reportRepo,
            IMembershipRepository memberRepo,
            IGlobalSettingsRepository globalSettingRepo,
            IServerUrls serverUrls,
            ITokenGenerator tokenGenerator)
        {
            _reportRepo = reportRepo;
            _membershipRepo = memberRepo;
            _globalSettingRepo = globalSettingRepo;
            _serverUrls = serverUrls;
            _tokenGenerator = tokenGenerator;
        }

        [HttpPost]
        [HttpGet]
        public async Task<HttpResponseMessage> ExecuteReport()
        {
            var reportGenerator = new JsReportGenerator(_serverUrls.JsReportServerUrl, _globalSettingRepo.GetSetting<string>("JsReportUserName"), _globalSettingRepo.GetSetting<string>("JsReportPassword"));
            var reportRequest = new JsReportRequest();
            reportRequest.AddParameterValue("tenantHostName", MultiTenantManager.CurrentTenant.HostName);

            NameValueCollection queryParams = HttpUtility.ParseQueryString(Request.RequestUri.Query);
            var requestBody = await Request.Content.ReadAsStringAsync();
            Dictionary<string, object> bodyParams = new Dictionary<string, object>();
            if (!string.IsNullOrEmpty(requestBody))
            {
                bodyParams = JsonConvert.DeserializeObject<Dictionary<string, object>>(requestBody);
            }

            Func<string, object> getParam = name =>
            {
                var r = queryParams[name];
                if (r != null)
                {
                    return r;
                }
                if (bodyParams.ContainsKey(name))
                {
                    return bodyParams[name];
                }
                return null;
            };

            ReportDTO reportDto = null;
            object shortId = getParam("shortId");
            if (shortId == null)
            {
                var reportId = getParam("reportId");
                if (reportId != null)
                {
                    reportDto = _reportRepo.GetById(int.Parse(reportId.ToString()), ReportLoadInstructions.Data);
                }
                else
                {
                    var reportName = getParam("reportName");
                    if (reportName != null)
                    {
                        reportDto = _reportRepo.GetByName(reportName.ToString(), ReportLoadInstructions.Data).FirstOrDefault();
                    }
                }
            }

            if (shortId == null && reportDto != null)
            {
                if (reportDto.Type != (int)ReportType.JsReport)
                {
                    throw new ApplicationException("Can only run JsReport templates");
                }

                shortId = Encoding.UTF8.GetString(reportDto.ReportData.Data);

                // Insert default parameters from ReportData
                if (!string.IsNullOrWhiteSpace(reportDto.Configuration))
                {
                    reportRequest.ParseTemplateParameters(reportDto.Configuration);
                }
            }

            if (shortId == null)
            {
                throw new ApplicationException("Must specify valid shortId, reportId or reportName");
            }

            reportRequest.ShortId = shortId.ToString();
            reportRequest.Recipe = getParam("recipe")?.ToString();
            reportRequest.Token = _tokenGenerator.GenerateToken(Membership.GetUser().UserName);
            reportRequest.Preview = bool.Parse(getParam("preview")?.ToString() ?? "false");

            var knownParams = new string[] { "shortid", "reportname", "reportid", "recipe" };
            foreach (var param in queryParams.Keys.Cast<string>())
            {
                if (knownParams.Contains(param.ToLower()))
                {
                    continue;
                }
                reportRequest.AddParameterValue(param, queryParams[param]);
            }
            foreach (var param in bodyParams.Keys.Cast<string>())
            {
                if (knownParams.Contains(param.ToLower()))
                {
                    continue;
                }
                reportRequest.AddParameterValue(param, bodyParams[param]);
            }

            var reportResponse = await reportGenerator.GenerateAsync(reportRequest);
            var resp = new HttpResponseMessage(reportResponse.StatusCode);
            if (reportResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                resp.Content = new StreamContent(reportResponse.Content);
                resp.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue(reportResponse.ContentType);
                if (!reportRequest.Preview)
                {
                    resp.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
                    if (reportDto != null)
                    {
                        var extension = new MimeSharp.Mime().Extension(reportResponse.ContentType).FirstOrDefault() ?? "";
                        resp.Content.Headers.ContentDisposition.FileName = reportDto.Name + "." + extension;
                    }
                }
            }
            else
            {
                resp.ReasonPhrase = reportResponse.Message;
            }
            return resp;
        }
    }
}