﻿using ConceptCave.www.Attributes;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Web.Http;
using System.Web.Security;
using VirtualMgr.Central;
using VirtualMgr.JwtAuth;

namespace ConceptCave.www.API.v1
{
    [Authorize]
    [WebApiCorsOptionsAllow]
    public class AuthController : ApiController
    {
        private readonly TokenGenerator _generator;

        public AuthController(TokenGenerator generator)
        {
            _generator = generator;
        }

        // POST api/<controller>
        public object Token()
        {
            var username = Membership.GetUser().UserName;
            return new {
                token = _generator.GenerateToken(username, 10)
            };
        }


    }
}