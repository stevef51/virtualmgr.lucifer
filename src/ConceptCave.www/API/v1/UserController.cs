﻿using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.www.Attributes;
using ConceptCave.www.Squisher;
using Newtonsoft.Json.Linq;
using SquishIt.Framework;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Security;
using VirtualMgr.Central;

namespace ConceptCave.www.API.v1
{
    [WebApiCorsAuthorize(Roles = "User")]
    [WebApiCorsOptionsAllow]
    public class UserController : ApiController
    {
        private readonly IMembershipRepository _membershipRepo;

        public UserController(IMembershipRepository membershipRepo)
        {
            _membershipRepo = membershipRepo;
        }

        public class ConvertTimeRequest
        {
            public List<DateTime> datetimes { get; set; }
            public string sourceTimezone { get; set; }
            public string destinationTimezone { get; set; }
        }

        public class ConvertTimeResponse
        {
            public List<DateTime> datetimes { get; set; }
            public string sourceTimezone { get; set; }
            public string destinationTimezone { get; set; }
        }

        [HttpPost]
        [HttpOptions]
        public ConvertTimeResponse ConvertDateTimes(ConvertTimeRequest request)
        {
            var sourceTimezoneInfo = TimeZoneInfo.FindSystemTimeZoneById(request.sourceTimezone ?? "UTC");
            if (String.IsNullOrEmpty(request.destinationTimezone))
            {
                var memberDto = _membershipRepo.GetByUsername(HttpContext.Current.User.Identity.Name, MembershipLoadInstructions.None);
                request.destinationTimezone = memberDto.TimeZone;
            }
            var destinationTimezoneInfo = TimeZoneInfo.FindSystemTimeZoneById(request.destinationTimezone);
            return new ConvertTimeResponse()
            {
                sourceTimezone = sourceTimezoneInfo.Id,
                destinationTimezone = destinationTimezoneInfo.Id,
                datetimes = (from dt in request.datetimes select TimeZoneInfo.ConvertTime(dt, sourceTimezoneInfo, destinationTimezoneInfo)).ToList()
            };
        }

        public class DefaultLanguageRequest
        {
            public string code { get; set; }
        }

        [HttpPost]
        [HttpOptions]
        public string SetDefaultLanguage(DefaultLanguageRequest request)
        {
            var dto = _membershipRepo.GetByUsername(HttpContext.Current.User.Identity.Name);
            if (dto.DefaultLanguageCode != request.code)
            {
                dto.DefaultLanguageCode = request.code;
                dto = _membershipRepo.Save(dto, true, false);
            }
            return dto.DefaultLanguageCode;
        }

        [HttpGet]
        [HttpOptions]
        public string GetDefaultLanguage()
        {
            var dto = _membershipRepo.GetByUsername(HttpContext.Current.User.Identity.Name);
            return dto.DefaultLanguageCode;
        }
    }
}