﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.RepositoryInterfaces.ViewDefs;
using Newtonsoft.Json.Linq;
using ConceptCave.www.Attributes;

namespace ConceptCave.www.API.v1
{
    [WebApiCorsAuthorize(Roles = "User")]
    [WebApiCorsOptionsAllow]
    public class QHelpersController : ApiController
    {
        private IMembershipRepository _membershipRepo;

        public QHelpersController(IMembershipRepository membershipRepo)
        {
            _membershipRepo = membershipRepo;
        }

        [HttpGet]
        [HttpOptions]
        public IList<UserListResponse> AllUsers()
        {
            //just get all (Id, Name) pairs and return them
            return _membershipRepo.GetAllUsers().ToList();
        }

        [HttpGet]
        [HttpOptions]
        public IList<UserListResponse> AllUsersFromLabels([FromUri] Guid[] labels)
        {
            if (labels == null || !labels.Any())
                return _membershipRepo.GetAllUsers().ToList();
            else
                return _membershipRepo.GetAllUsersByLabel(labels).ToList();
        }
        
        [HttpGet]
        [HttpOptions]
        public IList<UserListResponse> AllRealUsers()
        {
            return _membershipRepo.GetAllRealUsers().ToList();
        }
    }
}