﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;
using System.Web.UI;
using System.Xml;
using ConceptCave.Configuration;
using ConceptCave.www.Squisher;
//using ICSharpCode.SharpZipLib.Core;
//using ICSharpCode.SharpZipLib.Zip;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SquishIt.Framework;
using WebAPI.OutputCache;
using ConceptCave.www.Attributes;
using VirtualMgr.Central;
using ConceptCave.SimpleExtensions;

namespace ConceptCave.www.API.v1
{
    [WebApiCorsOptionsAllow]
    //No authorize attribute: this shit is free
    public class AssetsController : ApiController
    {
        private const int OneYear = 60*60*24*365;

        [HttpGet]
        public object Echo(string request)
        {
            // Check tenant is valid
            if (MultiTenantManager.TenantContextProvider.GetCurrentTenant(false) != null)
            {
                return new { Echo = request };
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }

        [HttpGet]
        //[CacheOutput(ClientTimeSpan = OneYear, ServerTimeSpan = OneYear, MustRevalidate = true)]
        public HttpResponseMessage AssetBundle(string id = "")
        {
            var oMemStream = new MemoryStream();
            var platform = id;
            //var zip = ZipFile.Create(oMemStream);
/*            var zipStream = new ZipOutputStream(oMemStream);
            zipStream.UseZip64 = UseZip64.Off;
            var bundleExtension = (string.IsNullOrEmpty(platform) || platform.ToLower() == "browser") 
                                    ? "" 
                                    : "_" + platform;

            //First thing to add: bundles
            var bundleStringContent = Bundle.JavaScript().ForceRelease().RenderCached("jsbundle" + bundleExtension);
            AddToZip(zipStream, "Scripts/bundle.js", bundleStringContent);
            bundleStringContent = Bundle.Css().ForceRelease().RenderCached(MultiTenantManager.MakeTenantId("cssbundle" + bundleExtension));
            AddToZip(zipStream, "Content/bundle.css", bundleStringContent);

            //now copy over the whole content directory
            Action<VirtualDirectory, string> AddDirToZip = null;
            AddDirToZip = (directory, nameBase) =>
            {
                foreach (var file in directory.Files.Cast<VirtualFile>())
                {
                    using (var stream = file.Open())
                    {
                        AddToZip(zipStream, nameBase.TrimEnd('/') + "/" + file.Name, stream);
                    }
                }
                foreach (var dir in directory.Directories.Cast<VirtualDirectory>())
                {
                    var newBase = nameBase.TrimEnd('/') + "/" + dir.Name;
                    AddDirToZip(dir, newBase);
                }
            };
            var cdir = HostingEnvironment.VirtualPathProvider.GetDirectory("~/Content/");
            AddDirToZip(cdir, "Content");
            //also add script directory
            var scriptdir = HostingEnvironment.VirtualPathProvider.GetDirectory("~/Scripts/");
            AddDirToZip(scriptdir, "Scripts");
            
            //Flatten template directory
            const string templateNameBase = "Templates/";
            Action<VirtualDirectory> FlattenDirIntoZip = null;
            FlattenDirIntoZip = (directory) =>
            {
                foreach (var file in directory.Files.Cast<VirtualFile>())
                {
                    using (var stream = file.Open())
                    {
                        AddToZip(zipStream, templateNameBase.TrimEnd('/') + "/" + file.Name, stream);
                    }
                }
                foreach (var dir in directory.Directories.Cast<VirtualDirectory>())
                {
                    FlattenDirIntoZip(dir);
                }
            };
            var templatedir = HostingEnvironment.VirtualPathProvider.GetDirectory("~/AngularTemplates/");
            FlattenDirIntoZip(templatedir);

            //Manifest of scripts
            var cfg = SquisherSection.Current;

            IList<string> platformStyleList = null;
            IList<string> platformScriptList = null;
            IList<string> platformTemplateList = null;
            if (string.IsNullOrEmpty(platform) || platform.ToLower() == "browser")
            {
                platformScriptList = cfg.BrowserScriptNames;
                platformStyleList = cfg.BrowserStyleNames;
                platformTemplateList = cfg.BrowserTemplateNames;
            }
            else if (platform.ToLower() == "ios")
            {
                platformScriptList = cfg.IOSScriptNames;
                platformStyleList = cfg.IOSStyleNames;
                platformTemplateList = cfg.IOSTemplateNames;
            }

            using (var ms = new MemoryStream())
            {
                var rootObj = new JObject();
                var scriptArr = new JArray();
                var styleArr = new JArray();
                var templateArr = new JArray();

                foreach (var script in platformScriptList)
                    scriptArr.Add(script);
                foreach (var style in platformStyleList)
                    styleArr.Add(style);
                foreach (var template in platformTemplateList)
                    templateArr.Add(template);

                rootObj.Add("scripts", scriptArr);
                rootObj.Add("styles", styleArr);
                rootObj.Add("templates", templateArr);

                AddToZip(zipStream, "conceptcave.squisher.json", 
                    JsonConvert.SerializeObject(rootObj));
            }

            zipStream.IsStreamOwner = false;
            zipStream.Finish();
            zipStream.Close();
            */
            var result = new HttpResponseMessage(HttpStatusCode.OK);
            //oMemStream.Position = 0;
            //result.Content = new StreamContent(oMemStream);
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/zip");
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                FileName = "bundle.zip"
            };
            return result;
        }

/*        private void AddJSToZip(ZipOutputStream zip, bool seperateFiles)
        {
            // The following dynamically created JS 'bundle.js' is ran by VMPlayer at dashboard bootup, this file
            // used to contain all JS concatenated together, however this proved cumbersome to debug and sometimes Safari
            // debugger would just not show the extremely large JS file at all, so it is now sent as individual files
            // and bundle.js is responsible for loading them in the same order they were concatenated before.
            // They are loaded sequentially as asking the browser to load them all at once does not gaurantee run order
            // and can generate dependancy errors
            if (seperateFiles)
            {
                string js = "";
                js += "(function() {\n";
                js += "  var head = document.getElementsByTagName('head')[0];\n";
                js += "  var allLoaded = function() {\n";
                js += "    var body = document.querySelector('body');\n";
                js += "    angular.bootstrap(body, ['player']);\n";
                js += "  }\n";
                js += "  var runJs = function(file, callback) {\n";
                js += "    var js = document.createElement('script');\n";
                js += "    js.type = 'text/javascript';\n";
                js += "    js.src = 'http://' + window.location.host + '/' + file;\n";
                js += "    js.onload = callback;\n";            // onload we callback which will load next JS file
                js += "    head.appendChild(js);\n";
                js += "  }\n";
                js += "  var files = [];\n";
                js += "  var preloadFiles = function() {\n";
                js += "    files.forEach(function(file) {\n";
                js += "      if (file.loaded) {\n";
                js += "        files.forEach(function(otherFile) {\n";
                js += "          otherFile.dependsOn.remove(file.file.toLowerCase());\n";
                js += "        });\n";
                js += "      }\n";
                js += "    });\n";
                js += "  }\n";
                js += "  var loadMore = function(loadedFile) {\n";
                js += "    if (loadedFile) loadedFile.loaded = 2;\n";
                js += "    var loadedCnt = 0;\n";
                js += "    files.forEach(function(file) {\n";
                js += "      if (file.loaded == 2) loadedCnt++\n";
                js += "      if (loadedFile) file.dependsOn.remove(loadedFile.file.toLowerCase());\n";
                js += "      if (!file.loaded && file.dependsOn.length == 0) {\n";
                js += "        file.loaded = 1;\n";
                js += "        console.log('loading ' + file.file);\n";
                js += "        runJs('bundlejs/' + file.file, function() {\n";
                js += "          console.log('loaded  ' + file.file);\n";
                js += "          loadMore(file);\n";
                js += "        });\n";
                js += "      }\n";
                js += "    });\n";
                js += "    console.log(loadedCnt + ' of ' + files.length + ' loaded');\n";
                js += "    if (loadedCnt == files.length) {\n";
                js += "      console.log('Bootstrapping App..');\n";
                js += "      allLoaded();\n";
                js += "    }\n";
                js += "  }\n";

                foreach (var jsFile in SquisherSection.Current.IOSScriptFiles)
                {
                    var vpath = "~/Scripts/" + jsFile.Filename;

                    using (var contentStream = HostingEnvironment.VirtualPathProvider.GetFile(vpath).Open())
                    {
                        AddToZip(zip, "bundlejs/" + jsFile.Filename, contentStream);
                    }

                    js += string.Format("  files.push({{file: '{0}', loaded: {1}, dependsOn: [{2}]}});\n", jsFile.Filename, jsFile.Preloaded ? "2" : "0", string.Join(",", jsFile.DependsOn.Select(f => "'" + f.ToLower() + "'")));
                }

                js += "\n  preloadFiles();\n  loadMore();\n";
                js += "})();\n";

                AddToZip(zip, "bundle.js", js);
            }
            else
            {
                Dictionary<string, string> dependants = new Dictionary<string, string>();
                StringBuilder js = new StringBuilder();
                while (dependants.Count != SquisherSection.Current.IOSScriptFiles.Count())
                {
                    foreach (var jsFile in SquisherSection.Current.IOSScriptFiles)
                    {
                        if (dependants.ContainsKey(jsFile.Filename.ToLower()))
                        {
                            continue;
                        }    

                        if (jsFile.Preloaded)
                        {
                            dependants.Add(jsFile.Filename.ToLower(), jsFile.Filename);
                            continue;
                        }

                        int dependantsReady = 0;
                        foreach(var dependancy in jsFile.DependsOn)
                        {
                            if (dependants.ContainsKey(dependancy.ToLower()))
                                dependantsReady++;
                        }

                        if (dependantsReady == jsFile.DependsOn.Length)
                        {
                            js.AppendFormat(";\n// {0} depends on {1}\n", jsFile.Filename, string.Join(", ", jsFile.DependsOn));

                            dependants.Add(jsFile.Filename.ToLower(), jsFile.Filename);
                            var vpath = "~/Scripts/" + jsFile.Filename;

                            using (var contentStream = HostingEnvironment.VirtualPathProvider.GetFile(vpath).Open())
                            {
                                using (StreamReader sr = new StreamReader(contentStream))
                                {
                                    js.Append(sr.ReadToEnd() + ";\n");
                                }
                            }
                        }
                    }
                }

                js.Append("\n\n    var body = document.querySelector('body');\n");
                js.Append("    angular.bootstrap(body, ['player']);\n");

                AddToZip(zip, "bundle.js", js.ToString());
            }
        }
*/
        [HttpGet]
        public HttpResponseMessage AppBundle(bool appendSHA1 = false, bool seperateFiles = false)
        {
            var oMemStream = new MemoryStream();
            //var zip = ZipFile.Create(oMemStream);
/*            var zipStream = new ZipOutputStream(oMemStream);
            zipStream.UseZip64 = UseZip64.Off;

            AddJSToZip(zipStream, seperateFiles);
            
            var bundleStringContent = Bundle.Css().ForceRelease().RenderCached(MultiTenantManager.MakeTenantId("cssbundle_ios"));
            AddToZip(zipStream, "bundle.css", bundleStringContent);
            AddToZip(zipStream, "bundle.html", TemplateBundleString().ToString());

            //now copy over the whole content directory
            Action<VirtualDirectory, string> AddDirToZip = null;
            AddDirToZip = (directory, nameBase) =>
            {
                foreach (var file in directory.Files.Cast<VirtualFile>())
                {
                    using (var stream = file.Open())
                    {
                        AddToZip(zipStream, (nameBase.TrimEnd('/') + "/").TrimStart('/') + file.Name, stream);
                    }
                }
                foreach (var dir in directory.Directories.Cast<VirtualDirectory>())
                {
                    var newBase = (nameBase.TrimEnd('/') + "/").TrimStart('/') + dir.Name;
                    AddDirToZip(dir, newBase);
                }
            };
            AddDirToZip(HostingEnvironment.VirtualPathProvider.GetDirectory("~/VMPlayer/"), "");
            AddDirToZip(HostingEnvironment.VirtualPathProvider.GetDirectory("~/Content/images"), "images");
            AddDirToZip(HostingEnvironment.VirtualPathProvider.GetDirectory("~/Content/font"), "font");

            zipStream.IsStreamOwner = false;
            zipStream.Finish();
            zipStream.Close();
            */
            var result = new HttpResponseMessage(HttpStatusCode.OK);
            //oMemStream.SetLength(oMemStream.Length / 2);
            var sha1 = oMemStream.ToArray().SHA1Hash();
            if (appendSHA1) 
            {
                oMemStream.Write(sha1, 0, sha1.Length);
            }

            oMemStream.Position = 0;
            //result.Content = new StreamContent(oMemStream);
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/zip");
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                FileName = sha1.Hexify() + ".zip"
            };
            return result;
        }

/*        private void AddToZip(ZipOutputStream zip, string name, string content)
        {
            var bytes = Encoding.UTF8.GetBytes(content);
            AddToZip(zip, name, bytes);
        }

        private void AddToZip(ZipOutputStream zip, string name, byte[] content)
        {
            var entry = new ZipEntry(name);
            entry.DateTime = DateTime.UtcNow;
            entry.CompressionMethod = CompressionMethod.Deflated;
            
            
            zip.PutNextEntry(entry);
            zip.Write(content, 0, content.Length);
            zip.CloseEntry();
        }

        private void AddToZip(ZipOutputStream zip, string name, Stream content)
        {
            var ms = new MemoryStream();
            content.CopyTo(ms);
            AddToZip(zip, name, ms.ToArray());
        }
*/
        [HttpGet]
        [CacheOutput(ClientTimeSpan = OneYear, ServerTimeSpan = OneYear, MustRevalidate = true)]
        public HttpResponseMessage JsBundle(string id = "jsbundle")
        {
            HttpContext.Current.Response.AddCacheItemDependency("ForcedAppUpdate");
            var result = new HttpResponseMessage(HttpStatusCode.OK);

            var squishCache = Bundle.JavaScript().ForceRelease().RenderCached(id);
            result.Content = new StringContent(squishCache);
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/javascript");
            return result;
        }

        [HttpGet]
        [CacheOutput(ClientTimeSpan = OneYear, ServerTimeSpan = OneYear, MustRevalidate = true)]
        public HttpResponseMessage CssBundle(string id = "cssbundle")
        {
            HttpContext.Current.Response.AddCacheItemDependency("ForcedAppUpdate");
            var result = new HttpResponseMessage(HttpStatusCode.OK);

            var squishCache = Bundle.Css().ForceRelease().RenderCached(MultiTenantManager.MakeTenantId(id));
            result.Content = new StringContent(squishCache);
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/css");
            return result;
        }

        [HttpGet]
        [CacheOutput(ClientTimeSpan = OneYear, ServerTimeSpan = OneYear, MustRevalidate = true)]
        public HttpResponseMessage TemplateBundle(string id = "templatebundle")
        {
            HttpContext.Current.Response.AddCacheItemDependency("ForcedAppUpdate");

            var result = new HttpResponseMessage(HttpStatusCode.OK);

            StringBuilder builder = TemplateBundleString();

            result.Content = new StringContent(builder.ToString());
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");

            return result;
        }

        private static StringBuilder TemplateBundleString()
        {
            StringBuilder builder = new StringBuilder();

            foreach (var fname in SquisherSection.Current.BrowserTemplateNames)
            {
                var basedir = HttpContext.Current.Server.MapPath("~/AngularTemplates/");
                var fullFileName = Directory.EnumerateFiles(basedir, fname, SearchOption.AllDirectories).Single();
                using (var fstream = new FileStream(fullFileName, FileMode.Open, FileAccess.Read))
                {

                    using (StreamReader sr = new StreamReader(fstream, Encoding.UTF8))
                    {
                        var part = string.Format("<script type=\"text/ng-template\" id=\"{0}\">{1}</script>", fname, sr.ReadToEnd());
                        builder.Append(part);
                    }
                }
            }
            return builder;
        }
        
        [HttpGet]
        public IList<string> ContentFiles()
        {
            var pth = HostingEnvironment.VirtualPathProvider.GetDirectory("~/Content/images/");
            return pth.Files.OfType<VirtualFile>().Select(f => f.Name).ToList();
        }
    }
}