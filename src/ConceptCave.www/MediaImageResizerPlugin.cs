﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using ConceptCave.BusinessLogic;
using ConceptCave.BusinessLogic.Configuration;
using ConceptCave.Checklist;
using ConceptCave.Repository.Facilities;
using ConceptCave.RepositoryInterfaces.Enums;
using ImageResizer.Configuration.Issues;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Repository;
using System.IO;
using ConceptCave.Checklist.RunTime;
using ConceptCave.Checklist.Interfaces;
using Aspose.Words.Saving;
using ConceptCave.Checklist.Facilities;
using ConceptCave.Checklist.Editor;
using ConceptCave.Repository.Facilities.WordHelpers;
using ConceptCave.RepositoryInterfaces.Repositories;

namespace ImageResizer.Plugins.MediaReader
{
    public class MediaReaderPlugin : IPlugin, IIssueProvider, IVirtualImageProvider, IMultiInstancePlugin
    {
        public static string MediaImagePath = "MediaImage";
        public static string MediaPreviewImagePath = "MediaPreviewImage";
        public static string WorkingDocumentMediaImagePath = "WorkingDocumentMediaImage";
        public static string MembershipWorkingDocumentMediaImagePath = "MembershipWorkingDocumentMediaImage";
        public static string DocumentPreviewMediaImagePath = "DocumentPreviewMediaImage";
        public static string MembershipMediaImagePath = "MembershipImage";
        public static string TaskTypeMediaImagePath = "TaskTypeImage";

        private bool? _useCaching = true;
        private string _httpCachingType = "private";
        private int? _cacheForSeconds = 300;

        private readonly IMediaManager _mediaMgr;
        private readonly IMembershipRepository _membershipRepo;

        public MediaReaderPlugin()
        {
            //Need to grab a DI container ourselves because there is no way to punch one in here
            var sectionMgr = (IRepositorySectionManager)ConfigurationManager.GetSection("conceptcave.repository.resources");
            var resolver = sectionMgr.ReflectionFactory;
            _mediaMgr = resolver.InstantiateObject<IMediaManager>();
            _membershipRepo = resolver.InstantiateObject<IMembershipRepository>();
        }

        public bool UseCaching
        {
            get
            {
                if(_useCaching.HasValue == false)
                {
                    _useCaching = GlobalSettingsRepository.GetSetting<bool>("ImageResizer.UseImageCaching");
                }

                return _useCaching.Value;
           }
        }

        public string HttpCachingType
        {
            get
            {
                if (_httpCachingType == null)
                {
                    _httpCachingType = GlobalSettingsRepository.GetSetting<string>("ImageResizer.CacheType") ?? "private";
                }
                return _httpCachingType;
            }
        }

        public int CacheForSeconds
        {
            get
            {
                if (!_cacheForSeconds.HasValue)
                {
                    _cacheForSeconds = GlobalSettingsRepository.GetSetting<int>("ImageResizer.CacheForSeconds");
                }
                return _cacheForSeconds.Value;
            }
        }

        public string MediaPath
        {
            get
            {
                return ImageResizer.Util.PathUtils.ResolveAppRelative(string.Format("~/{0}", MediaImagePath));
            }
        }

        public string MediaPreviewPath
        {
            get
            {
                return ImageResizer.Util.PathUtils.ResolveAppRelative(string.Format("~/{0}", MediaPreviewImagePath));
            }
        }

        public string WorkingDocumentMediaPath
        {
            get
            {
                return ImageResizer.Util.PathUtils.ResolveAppRelative(string.Format("~/{0}", WorkingDocumentMediaImagePath));
            }
        }

        public string MembershipWorkingDocumentMediaPath
        {
            get
            {
                return ImageResizer.Util.PathUtils.ResolveAppRelative(string.Format("~/{0}", MembershipWorkingDocumentMediaImagePath));
            }
        }

        public string DocumentPreviewMediaPath
        {
            get
            {
                return ImageResizer.Util.PathUtils.ResolveAppRelative(string.Format("~/{0}", DocumentPreviewMediaImagePath));
            }
        }

        public string MembershipPath
        {
            get
            {
                return ImageResizer.Util.PathUtils.ResolveAppRelative(string.Format("~/{0}", MembershipMediaImagePath));
            }
        }

        public string TaskTypePath
        {
            get
            {
                return ImageResizer.Util.PathUtils.ResolveAppRelative(string.Format("~/{0}", TaskTypeMediaImagePath));
            }
        }

        public bool IsInPath(string sourcePath, string testForPath)
        {
            return sourcePath.StartsWith(testForPath, true, null);
        }

        public Guid ParseIdFromPath(string path)
        {
            path = path.TrimStart(new char[] { '/', '\\' });

            // ok, so we know we are expecting a GUID which should now be at the start of the path, so grab the relevant number of chars
            if (path.Length < 36)
            {
                return Guid.Empty;
            }

            string id = path.Substring(0, 36);

            Guid result = Guid.Empty;
            Guid.TryParse(id, out result);

            return result;
        }

        private bool DoesFileExist(string virtualPath, System.Collections.Specialized.NameValueCollection queryString)
        {
            String requestPath = ImageResizer.Util.PathUtils.ResolveAppRelative(virtualPath);

            if (IsInPath(requestPath, MediaPreviewPath))
            {
                requestPath = requestPath.Substring(MediaPreviewPath.Length);

                Guid previewId = ParseIdFromPath(requestPath);

                MediaEntity preview = MediaRepository.GetById(previewId, MediaLoadInstruction.None);

                if (preview == null || preview.PreviewCount.HasValue == false)
                {
                    return false;
                }

                int page = int.Parse(queryString["pageindex"]);

                return preview.PreviewCount >= page;
            }
            else if (IsInPath(requestPath, MediaPath))
            {
                requestPath = requestPath.Substring(MediaPath.Length);

                Guid id = ParseIdFromPath(requestPath);

                MediaEntity media = MediaRepository.GetById(id, MediaLoadInstruction.None);
                if (media == null)
                {
                    return false;
                }

                MimeTypeEntity mime = null;
                if (string.IsNullOrEmpty(media.Type))
                {
                    mime = MimeTypeCache.GetByExtension(media.Extension);
                    return mime != null && string.IsNullOrEmpty(mime.MediaPreviewGenerator) == false;
                }
                else
                    return true;
            }
            else if (IsInPath(requestPath, WorkingDocumentMediaPath))
            {
                requestPath = requestPath.Substring(WorkingDocumentMediaPath.Length);

                Guid rootId = ParseIdFromPath(requestPath); // id of the working document
                Guid presentedId = Guid.Parse(queryString["presentedid"]); // id if the presented in the working document

                WorkingDocumentEntity entity = WorkingDocumentRepository.GetById(rootId, WorkingDocumentLoadInstructions.Data);

                WorkingDocument document = (WorkingDocument)WorkingDocument.CreateFromString(entity.WorkingDocumentData.Data);

                return DoesPresentedMediaExist(document, presentedId, queryString);
            }
            else if (IsInPath(requestPath, MembershipWorkingDocumentMediaPath))
            {
                requestPath = requestPath.Substring(MembershipWorkingDocumentMediaPath.Length);

                Guid rootId = ParseIdFromPath(requestPath); // id of the user
                string contextName = queryString["usertypecontextname"]; // id of the UserTypeContext
                Guid presentedId = Guid.Parse(queryString["presentedid"]); // id if the presented in the working document

                var entity = _membershipRepo.GetUserContextData(rootId, contextName);
                UserTypeContextPublishedResourceEntity typeContext = MembershipTypeContextPublishedResource.GetById(entity.UserTypeContextPublishedResourceId, UserTypeContextLoadInstructions.PublishingGroupResource | UserTypeContextLoadInstructions.PublishingGroupResourceData);

                IDesignDocument doc = null; // MembershipRepository.InsertContextAnswers(typeContext.PublishingGroupResource.PublishingGroupResourceData.Data, entity.Data);

                WorkingDocument document = WorkingDocumentRepository.CreateWorkingDocumentFromDesignDocument((DesignDocument)doc);

                return DoesPresentedMediaExist(document, presentedId, queryString);
            }
            else if (IsInPath(requestPath, DocumentPreviewMediaPath))
            {
                requestPath = requestPath.Substring(DocumentPreviewMediaPath.Length);

                Guid rootId = ParseIdFromPath(requestPath); // id of the working document
                Guid mediaId = Guid.Parse(queryString["mediaid"]); // id of the media entity that will get merged with the working document

                WorkingDocumentEntity entity = WorkingDocumentRepository.GetById(rootId, WorkingDocumentLoadInstructions.Data);
                MediaEntity media = MediaRepository.GetById(mediaId, MediaLoadInstruction.None);

                return media != null && entity != null;
            }
            else if (IsInPath(requestPath, MembershipPath))
            {
                requestPath = requestPath.Substring(MembershipPath.Length);

                Guid id = ParseIdFromPath(requestPath); // id of the media entity that will get merged with the working document

                var user = _membershipRepo.GetById(id, MembershipLoadInstructions.None);

                if (user != null && user.MediaId.HasValue == true)
                {
                    return true;
                }

                // see if we can fall back to a default no user image
                var imageId = GlobalSettingsRepository.GetSetting<string>(GlobalSettingsConstants.SystemSetting_MembershipNoMediaImageId);
                if(string.IsNullOrEmpty(imageId) == true)
                {
                    return false;
                }

                return true;
            }
            else if (IsInPath(requestPath, TaskTypePath))
            {
                requestPath = requestPath.Substring(TaskTypePath.Length);

                Guid id = ParseIdFromPath(requestPath); // id of the media entity that will get merged with the working document

                ConceptCave.Repository.Injected.OTaskTypeRepository tr = new ConceptCave.Repository.Injected.OTaskTypeRepository();

                var tt = tr.GetById(id, ProjectJobTaskTypeLoadInstructions.None);

                if (tt != null && tt.MediaId.HasValue == true)
                {
                    return true;
                }

                return false;
            }

            return false;
        }

        protected bool DoesPresentedMediaExist(WorkingDocument document, Guid presentedId, System.Collections.Specialized.NameValueCollection queryString)
        {
            object answer = null;
            var presented = (from p in document.PresentedAsDepthFirstEnumerable() where p.Id == presentedId && p is IPresentedQuestion select p).Cast<IPresentedQuestion>();

            if (presented.Count() == 0)
            {
                // see if we were really given the questionid rather than the presentedid, which occurrs when viewing context stuff
                // as there is a different presented each time
                var question = (from p in document.AsDepthFirstEnumerable() where p is IQuestion && ((IQuestion)p).Id == presentedId select p).Cast<IQuestion>();

                if (question.Count() == 0)
                {
                    return false;
                }

                answer = (IUploadMediaAnswer)question.First().DefaultAnswer;
            }
            else
            {
                answer = presented.First().GetAnswer();
            }

            if (answer is IUploadMediaAnswer)
            {
                IUploadMediaAnswer mediaAnswer = (IUploadMediaAnswer)answer;
                Guid itemId = Guid.Parse(queryString["itemid"]);

                var item = (from a in mediaAnswer.Items.Items where a.Id == itemId select a);

                return item.Count() > 0;
            }

            return false;
        }

        public IVirtualFile RetrieveFile(string virtualPath, System.Collections.Specialized.NameValueCollection queryString)
        {
            String requestPath = ImageResizer.Util.PathUtils.ResolveAppRelative(virtualPath);

            if (IsInPath(requestPath, MediaPreviewPath))
            {
                requestPath = requestPath.Substring(MediaPreviewPath.Length);

                Guid previewId = ParseIdFromPath(requestPath);

                int page = int.Parse(queryString["pageindex"]);

                var preview = MediaRepository.GetMediaPreviewById(previewId, page);

                if (preview == null)
                {
                    return null;
                }

                var gen = new PassThroughPreviewGenerator();

                return gen.GetMediaFile(virtualPath, queryString, preview.Data);
            }
            else if (IsInPath(requestPath, MediaPath))
            {
                requestPath = requestPath.Substring(MediaPath.Length);

                Guid id = ParseIdFromPath(requestPath);

                MediaEntity media = MediaRepository.GetById(id, MediaLoadInstruction.Data);
                MimeTypeEntity mime = MimeTypeCache.GetByExtension(media.Extension);

                if (string.IsNullOrEmpty(mime.MediaPreviewGenerator))
                {
                    return null;
                }

                IImagePreviewGenerator generator = GetImagePreviewGenerator(mime);

                return generator.GetMediaFile(virtualPath, queryString, media.MediaData.Data);
            }
            else if (IsInPath(requestPath, WorkingDocumentMediaPath))
            {
                requestPath = requestPath.Substring(WorkingDocumentMediaPath.Length);

                Guid rootId = ParseIdFromPath(requestPath); // id of the working document
                Guid presentedId = Guid.Parse(queryString["presentedid"]); // id if the presented in the working document

                WorkingDocumentEntity entity = WorkingDocumentRepository.GetById(rootId, WorkingDocumentLoadInstructions.Data);

                WorkingDocument document = (WorkingDocument)WorkingDocument.CreateFromString(entity.WorkingDocumentData.Data);

                return RetrieveFileFromPresentedMedia(document, presentedId, virtualPath, queryString);
            }
            else if (IsInPath(requestPath, MembershipWorkingDocumentMediaPath))
            {
                requestPath = requestPath.Substring(MembershipWorkingDocumentMediaPath.Length);

                Guid rootId = ParseIdFromPath(requestPath); // id of the user
                string contextName = queryString["usertypecontextname"]; // id of the UserTypeContext
                Guid presentedId = Guid.Parse(queryString["presentedid"]); // id if the presented in the working document

                var entity = _membershipRepo.GetUserContextData(rootId, contextName);
                UserTypeContextPublishedResourceEntity typeContext = MembershipTypeContextPublishedResource.GetById(entity.UserTypeContextPublishedResourceId, UserTypeContextLoadInstructions.PublishingGroupResource | UserTypeContextLoadInstructions.PublishingGroupResourceData);

                IDesignDocument doc = null; // MembershipRepository.InsertContextAnswers(typeContext.PublishingGroupResource.PublishingGroupResourceData.Data, entity.Data);

                WorkingDocument document = WorkingDocumentRepository.CreateWorkingDocumentFromDesignDocument((DesignDocument)doc);

                return RetrieveFileFromPresentedMedia(document, presentedId, virtualPath, queryString);
            }
            else if (IsInPath(requestPath, DocumentPreviewMediaPath))
            {
                // ok this path indicates that we want to see a preview of the merged result between a working document and a word document media element
                requestPath = requestPath.Substring(DocumentPreviewMediaPath.Length);
                Guid rootId = ParseIdFromPath(requestPath); // id of the working document
                Guid mediaId = Guid.Parse(queryString["mediaid"]); // id of the media entity that will get merged with the working document

                WorkingDocumentEntity entity = WorkingDocumentRepository.GetById(rootId, WorkingDocumentLoadInstructions.Data);
                MediaEntity media = MediaRepository.GetById(mediaId, MediaLoadInstruction.Data);

                MimeTypeEntity mime = MimeTypeCache.GetByExtension(media.Extension);

                if (string.IsNullOrEmpty(mime.MediaPreviewGenerator))
                {
                    return null;
                }

                using(MemoryStream ms = new MemoryStream(media.MediaData.Data))
                {
                    Aspose.Words.Document doc = new Aspose.Words.Document(ms);

                    WorkingDocument wd = (WorkingDocument)WorkingDocument.CreateFromString(entity.WorkingDocumentData.Data);

                    var fac = new AsposeWordDocument(doc, _mediaMgr);
                    ConceptCave.Core.IContextContainer container = new ConceptCave.Core.ContextContainer();
                    container.Set<ILingoProgram>(wd.Program);
                    fac.Merge(container);

                    IImagePreviewGenerator generator = new WordDocumentImagePreviewGenerator();

                    return generator.GetMediaFile(virtualPath, queryString, doc);
                }
            }
            else if (IsInPath(requestPath, MembershipPath))
            {
                requestPath = requestPath.Substring(MembershipPath.Length);

                Guid id = ParseIdFromPath(requestPath);
                var user = _membershipRepo.GetById(id, MembershipLoadInstructions.Media | MembershipLoadInstructions.MediaData);

                if(user.MediaId.HasValue == true)
                {
                    MimeTypeEntity mime = MimeTypeCache.GetByExtension(user.Medium.Extension);

                    if (string.IsNullOrEmpty(mime.MediaPreviewGenerator))
                    {
                        return null;
                    }

                    IImagePreviewGenerator generator = GetImagePreviewGenerator(mime);

                    return generator.GetMediaFile(virtualPath, queryString, user.Medium.MediaData.Data);
                }
                else
                {
                    // see if we can fall back to a default no user image
                    var imageId = GlobalSettingsRepository.GetSetting<string>(GlobalSettingsConstants.SystemSetting_MembershipNoMediaImageId);
                    MediaEntity media = MediaRepository.GetById(Guid.Parse(imageId), MediaLoadInstruction.Data);

                    if(media == null)
                    {
                        // perhaps its a media folder id we have
                        media = MediaRepository.GetByFileId(Guid.Parse(imageId), MediaLoadInstruction.Data);
                    }

                    MimeTypeEntity mime = MimeTypeCache.GetByExtension(media.Extension);

                    if (string.IsNullOrEmpty(mime.MediaPreviewGenerator))
                    {
                        return null;
                    }

                    IImagePreviewGenerator generator = GetImagePreviewGenerator(mime);

                    return generator.GetMediaFile(virtualPath, queryString, media.MediaData.Data);
                }
            }
            else if (IsInPath(requestPath, TaskTypePath))
            {
                requestPath = requestPath.Substring(TaskTypePath.Length);

                Guid id = ParseIdFromPath(requestPath);

                ConceptCave.Repository.Injected.OTaskTypeRepository tr = new ConceptCave.Repository.Injected.OTaskTypeRepository();

                var tt = tr.GetById(id, ProjectJobTaskTypeLoadInstructions.Media | ProjectJobTaskTypeLoadInstructions.MediaData);

                if (tt.MediaId.HasValue == true)
                {
                    MimeTypeEntity mime = MimeTypeCache.GetByExtension(System.IO.Path.GetExtension(tt.Medium.Filename));

                    if (string.IsNullOrEmpty(mime.MediaPreviewGenerator))
                    {
                        return null;
                    }

                    IImagePreviewGenerator generator = GetImagePreviewGenerator(mime);

                    return generator.GetMediaFile(virtualPath, queryString, tt.Medium.MediaData.Data);
                }
            }

            return null;
        }

        private static IImagePreviewGenerator GetImagePreviewGenerator(MimeTypeEntity mime)
        {
            IImagePreviewGenerator generator = (IImagePreviewGenerator)System.Type.GetType(mime.MediaPreviewGenerator).GetConstructor(System.Type.EmptyTypes).Invoke(null);
            return generator;
        }

        public IVirtualFile RetrieveFileFromPresentedMedia(WorkingDocument document, Guid presentedId, string virtualPath, System.Collections.Specialized.NameValueCollection queryString)
        {
            object answer = null;

            var presented = (from p in document.PresentedAsDepthFirstEnumerable() where p.Id == presentedId && p is IPresentedQuestion select p).Cast<IPresentedQuestion>();

            if (presented.Count() == 0)
            {
                // see if we were really given the questionid rather than the presentedid, which occurrs when viewing context stuff
                // as there is a different presented each time
                var question = (from p in document.AsDepthFirstEnumerable() where p is IQuestion && ((IQuestion)p).Id == presentedId select p).Cast<IQuestion>();

                if (question.Count() == 0)
                {
                    return null;
                }

                answer = (IUploadMediaAnswer)question.First().DefaultAnswer;
            }
            else
            {
                answer = presented.First().GetAnswer();
            }

            if (answer is IUploadMediaAnswer)
            {
                IUploadMediaAnswer mediaAnswer = (IUploadMediaAnswer)answer;
                Guid itemId = Guid.Parse(queryString["itemid"]);

                var item = (from a in mediaAnswer.Items.Items where a.Id == itemId select a);

                if (item.Count() == 0)
                {
                    return null;
                }

                return null; // new MediaPluginFile(virtualPath, item.First().Data);
            }

            return null;
        }

        #region IPlugin Members

        public IPlugin Install(Configuration.Config c)
        {
            c.Plugins.add_plugin(this);

            if(UseCaching == true)
            {
                c.Pipeline.PreHandleImage += delegate(IHttpModule sender, HttpContext context, Caching.IResponseArgs e)
                {
                    if(context.Request.QueryString["nocache"] != null)
                    {
                        return;
                    }

                    if(this.HttpCachingType.ToLower() == "public")
                    {
                        e.ResponseHeaders.CacheControl = HttpCacheability.Public;
                    }
                    else
                    {
                        e.ResponseHeaders.CacheControl = HttpCacheability.Private;
                    }

                    e.ResponseHeaders.Expires = DateTime.UtcNow.AddSeconds(this.CacheForSeconds);
                };
            }

            c.Pipeline.RewriteDefaults += delegate(IHttpModule sender, HttpContext context, Configuration.IUrlEventArgs e)
            {
                //Only work with database images
                //Non-images will be served as-is
                //Cache all file types, whether they are processed or not.
                string testPath = c.Pipeline.PreRewritePath;
                if (IsInPath(testPath, MediaPath) || 
                    IsInPath(testPath, MediaPreviewPath) || 
                    IsInPath(WorkingDocumentMediaPath, testPath) || 
                    IsInPath(testPath, DocumentPreviewMediaPath))
                    e.QueryString["cache"] = ServerCacheMode.Always.ToString();
            };

            c.Pipeline.PostRewrite += delegate(IHttpModule sender, HttpContext context, Configuration.IUrlEventArgs e)
            {
                //Only work with database images
                //If the data is untrusted, always re-encode each file.
                string testPath = c.Pipeline.PreRewritePath;
                if (IsInPath(testPath, MediaPath) || 
                    IsInPath(testPath, MediaPreviewPath) || 
                    IsInPath(WorkingDocumentMediaPath, testPath) || 
                    IsInPath(testPath, DocumentPreviewMediaPath))
                    e.QueryString["process"] = ImageResizer.ProcessWhen.Always.ToString();
            };

            //HostingEnvironment.RegisterVirtualPathProvider(this);

            return this;
        }

        public bool Uninstall(Configuration.Config c)
        {
            c.Plugins.remove_plugin(c);

            return true;
        }

        #endregion

        #region IIssueProvider Members

        public IEnumerable<IIssue> GetIssues()
        {
            return new List<IIssue>();
        }

        #endregion

        #region IVirtualImageProvider Members

        public bool FileExists(string virtualPath, System.Collections.Specialized.NameValueCollection queryString)
        {
            return DoesFileExist(virtualPath, queryString);
        }

        public IVirtualFile GetFile(string virtualPath, System.Collections.Specialized.NameValueCollection queryString)
        {
            IVirtualFile file = RetrieveFile(virtualPath, queryString);
            return file;
        }

        #endregion
    }

    public class MediaPluginFile : IVirtualFile
    {
        protected byte[] Data { get; set; }
        public string VirtualPath { get; set; }

        public MediaPluginFile(string virtualPath, byte[] data)
        {
            VirtualPath = virtualPath;
            Data = data;
        }

        public System.IO.Stream Open()
        {
            MemoryStream result = new MemoryStream(Data);

            return result;
        }
    }

    public interface IImagePreviewGenerator
    {
        MediaPluginFile GetMediaFile(string virtualPath, System.Collections.Specialized.NameValueCollection queryString, object data);
    }

    public class PassThroughPreviewGenerator : IImagePreviewGenerator
    {
        public MediaPluginFile GetMediaFile(string virtualPath, System.Collections.Specialized.NameValueCollection queryString, object data)
        {
            return new MediaPluginFile(virtualPath, (byte[])data);
        }
    }

    public class WordDocumentImagePreviewGenerator : IImagePreviewGenerator
    {
        public MediaPluginFile GetMediaFile(string virtualPath, System.Collections.Specialized.NameValueCollection queryString, object data)
        {
            if (data is byte[])
            {
                return GetMediaFileFromByteArray(virtualPath, queryString, (byte[])data);
            }

            if (data is Aspose.Words.Document)
            {
                return GetMediaFileFromDocument(virtualPath, queryString, (Aspose.Words.Document)data);
            }

            return null;
        }

        protected MediaPluginFile GetMediaFileFromDocument(string virtualPath, System.Collections.Specialized.NameValueCollection queryString, Aspose.Words.Document doc)
        {
            ImageSaveOptions options = BuildImageSaveOptions(queryString);

            using (MemoryStream docStream = new MemoryStream())
            {
                doc.Save(docStream, options);

                docStream.Seek(0, SeekOrigin.Begin);

                return new MediaPluginFile(virtualPath, docStream.ToArray());
            }
        }

        protected MediaPluginFile GetMediaFileFromByteArray(string virtualPath, System.Collections.Specialized.NameValueCollection queryString, byte[] data)
        {
            using (MemoryStream ms = new MemoryStream(data))
            {
                Aspose.Words.Document doc = new Aspose.Words.Document(ms);

                ImageSaveOptions options = BuildImageSaveOptions(queryString);

                using (MemoryStream docStream = new MemoryStream())
                {
                    doc.Save(docStream, options);

                    docStream.Seek(0, SeekOrigin.Begin);

                    return new MediaPluginFile(virtualPath, docStream.ToArray());
                }
            }
        }

        protected ImageSaveOptions BuildImageSaveOptions(System.Collections.Specialized.NameValueCollection queryString)
        {
            ImageSaveOptions options = new ImageSaveOptions(Aspose.Words.SaveFormat.Png);

            if (queryString["pageindex"] != null)
            {
                options.PageIndex = int.Parse(queryString["pageindex"]);
            }

            if (queryString["pagecount"] != null)
            {
                options.PageCount = int.Parse(queryString["pagecount"]);
            }

            if (queryString["scale"] != null)
            {
                options.Scale = float.Parse(queryString["scale"]);
            }

            if (queryString["usehighquality"] != null)
            {
                options.UseHighQualityRendering = bool.Parse(queryString["usehighquality"]);
            }

            if (queryString["useantialiasing"] != null)
            {
                options.UseAntiAliasing = bool.Parse(queryString["useantialiasing"]);
            }

            if (queryString["resolution"] != null)
            {
                var resolution = float.Parse(queryString["resolution"]);

                if (resolution > 200)
                {
                    options.Resolution = 200;
                }
            }

            return options;
        }
    }
}