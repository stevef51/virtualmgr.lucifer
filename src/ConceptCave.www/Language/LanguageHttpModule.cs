﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Threading;
using System.Web;

namespace ConceptCave.www.HttpModules
{
    public class LanguageHttpModule : IHttpModule
    {

        public void Init(HttpApplication httpApp)
        {
            httpApp.BeginRequest += OnBeginRequest;
        }

        public void OnBeginRequest(Object sender, EventArgs e)
        {
            var httpApp = (HttpApplication)sender;
            var cookie = httpApp.Request.Cookies.Get("culture");
            if (cookie == null)
                return;

            string cultureName = cookie.Value;
            try
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(cultureName);
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(cultureName);
            }
            catch
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("en");
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("en");
            }
        }

        public void Dispose()
        {

        }
    }
}