﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Web;
using System.ServiceModel.Channels;
using System.ServiceModel;
using System.Threading;
using Newtonsoft.Json;
using ConceptCave.Checklist.Interfaces;
using System.Xml;
using System.Xml.Linq;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json.Linq;

namespace ConceptCave.www.Language
{
    public class AzureTranslateServiceV3 : ILanguageTranslateService
    {
        private AzureAuthToken _authentication;
        const string TranslateApiUrl = "https://api.cognitive.microsofttranslator.com/translate?api-version=3.0";
        private string ApiKey;

        public AzureTranslateServiceV3(string apiKey)
        {
            ApiKey = apiKey;
            _authentication = new AzureAuthToken(ApiKey);
        }

        private async Task<string> TryTranslate(string fromCultureName, string fromText, string toCultureName)
        {
            return (await TryTranslate(fromCultureName, new string[] { fromText }, toCultureName)).FirstOrDefault();
        }

        public async Task<string> TranslateAsync(string fromCultureName, string fromText, string toCultureName)
        {
            try
            {
                return await TryTranslate(fromCultureName, fromText, toCultureName);
            }
            catch
            {
                _authentication = new AzureAuthToken(ApiKey);
                return await TryTranslate(fromCultureName, fromText, toCultureName);
            }
        }


        public class AzureAuthToken
        {
            /// URL of the token service
            private static readonly Uri ServiceUrl = new Uri("https://api.cognitive.microsoft.com/sts/v1.0/issueToken");

            /// Name of header used to pass the subscription key to the token service
            private const string OcpApimSubscriptionKeyHeader = "Ocp-Apim-Subscription-Key";

            /// After obtaining a valid token, this class will cache it for this duration.
            /// Use a duration of 5 minutes, which is less than the actual token lifetime of 10 minutes.
            private static readonly TimeSpan TokenCacheDuration = new TimeSpan(0, 5, 0);

            /// Cache the value of the last valid token obtained from the token service.
            private string _storedTokenValue = string.Empty;

            /// When the last valid token was obtained.
            private DateTime _storedTokenTime = DateTime.MinValue;

            /// Gets the subscription key.
            public string SubscriptionKey { get; }

            /// Gets the HTTP status code for the most recent request to the token service.
            public HttpStatusCode RequestStatusCode { get; private set; }

            /// <summary>
            /// Creates a client to obtain an access token.
            /// </summary>
            /// <param name="key">Subscription key to use to get an authentication token.</param>
            public AzureAuthToken(string key)
            {
                if (string.IsNullOrEmpty(key))
                {
                    throw new ArgumentNullException(nameof(key), "A subscription key is required");
                }

                this.SubscriptionKey = key;
                this.RequestStatusCode = HttpStatusCode.InternalServerError;
            }

            /// <summary>
            /// Gets a token for the specified subscription.
            /// </summary>
            /// <returns>The encoded JWT token prefixed with the string "Bearer ".</returns>
            /// <remarks>
            /// This method uses a cache to limit the number of request to the token service.
            /// A fresh token can be re-used during its lifetime of 10 minutes. After a successful
            /// request to the token service, this method caches the access token. Subsequent 
            /// invocations of the method return the cached token for the next 5 minutes. After
            /// 5 minutes, a new token is fetched from the token service and the cache is updated.
            /// </remarks>
            public async Task<string> GetAccessTokenAsync()
            {
                if (string.IsNullOrWhiteSpace(this.SubscriptionKey))
                {
                    return string.Empty;
                }

                // Re-use the cached token if there is one.
                if ((DateTime.Now - _storedTokenTime) < TokenCacheDuration)
                {
                    return _storedTokenValue;
                }

                using (var client = new HttpClient())
                using (var request = new HttpRequestMessage())
                {
                    request.Method = HttpMethod.Post;
                    request.RequestUri = ServiceUrl;
                    request.Content = new StringContent(string.Empty);
                    request.Headers.TryAddWithoutValidation(OcpApimSubscriptionKeyHeader, this.SubscriptionKey);
                    client.Timeout = TimeSpan.FromSeconds(2);
                    var response = await client.SendAsync(request);
                    this.RequestStatusCode = response.StatusCode;
                    response.EnsureSuccessStatusCode();
                    var token = await response.Content.ReadAsStringAsync();
                    _storedTokenTime = DateTime.Now;
                    _storedTokenValue = "Bearer " + token;
                    return _storedTokenValue;
                }
            }

            /// <summary>
            /// Gets a token for the specified subscription. Synchronous version.
            /// Use of async version preferred
            /// </summary>
            /// <returns>The encoded JWT token prefixed with the string "Bearer ".</returns>
            /// <remarks>
            /// This method uses a cache to limit the number of request to the token service.
            /// A fresh token can be re-used during its lifetime of 10 minutes. After a successful
            /// request to the token service, this method caches the access token. Subsequent 
            /// invocations of the method return the cached token for the next 5 minutes. After
            /// 5 minutes, a new token is fetched from the token service and the cache is updated.
            /// </remarks>
            public string GetAccessToken()
            {
                // Re-use the cached token if there is one.
                if ((DateTime.Now - _storedTokenTime) < TokenCacheDuration)
                {
                    return _storedTokenValue;
                }

                string accessToken = null;
                var task = Task.Run(async () =>
                {
                    accessToken = await this.GetAccessTokenAsync();
                });

                while (!task.IsCompleted)
                {
                    System.Threading.Thread.Yield();
                }
                if (task.IsFaulted)
                {
                    throw task.Exception;
                }
                if (task.IsCanceled)
                {
                    throw new Exception("Timeout obtaining access token.");
                }
                return accessToken;
            }

        }

        private class Input
        {
            public string Text { get; set; }
        }

        private class Translation
        {
            public string text { get; set; }
            public string to { get; set; }
        }

        private class Output
        {
            public List<Translation> translations { get; set; }
        }
        private async Task<string[]> TryTranslate(string fromCultureName, IEnumerable<string> fromText, string toCultureName)
        {
            string authToken = await _authentication.GetAccessTokenAsync();

            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create($"{TranslateApiUrl}&from={fromCultureName}&to={toCultureName}&textType=html");
            webRequest.Method = "POST";
            webRequest.ContentType = "application/json";
            webRequest.Headers.Add("Authorization", authToken);

            List<Input> body = new List<Input>();
            foreach(var text in fromText)
            {
                body.Add(new Input() { Text = text });
            }

            var requestStream = await webRequest.GetRequestStreamAsync();
            var writer = new StreamWriter(requestStream);

            var bodyStr = JsonConvert.SerializeObject(body);
            writer.Write(bodyStr);
            writer.Flush();

            var response = await webRequest.GetResponseAsync();
            try
            {
                using (Stream stream = response.GetResponseStream())
                {
                    using (StreamReader rdr = new StreamReader(stream, System.Text.Encoding.UTF8))
                    {
                        // Deserialize the response
                        string strResponse = await rdr.ReadToEndAsync();
                        var result = JsonConvert.DeserializeObject<List<Output>>(strResponse);

                        return (from r in result select r.translations[0].text).ToArray();
                    }
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (response != null)
                {
                    response.Close();
                    response = null;
                }
            }
        }

        public async Task<string[]> TranslateAsync(string fromCultureName, string[] fromText, string toCultureName)
        {
            List<string> result = new List<string>();

            int batchSize = 0;
            List<string> batch = new List<string>();
            foreach (var s in fromText)
            {
                if (batchSize + s.Length < 5000 && batch.Count() < 100)
                {
                    batch.Add(s);
                    batchSize += s.Length;
                }
                else
                {
                    result.AddRange(await TranslateBatch(fromCultureName, batch, toCultureName));
                    batch.Clear();
                    batch.Add(s);
                    batchSize = s.Length;
                }
            }
            if (batch.Any()) { 
                result.AddRange(await TranslateBatch(fromCultureName, batch, toCultureName));
            }
            return result.ToArray();
        }

        private async Task<string[]> TranslateBatch(string fromCultureName, IEnumerable<string> fromText, string toCultureName)
        {
            try
            {
                return await TryTranslate(fromCultureName, fromText, toCultureName);
            }
            catch
            {
                _authentication = new AzureAuthToken(ApiKey);
                return await TryTranslate(fromCultureName, fromText, toCultureName);
            }
        }
    }
}
