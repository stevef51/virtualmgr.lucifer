﻿using ConceptCave.Repository.Injected;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

    public static class L
    {
        public static string T(string translationId)
        {
            string cultureName = System.Threading.Thread.CurrentThread.CurrentUICulture.Name;
            var t = OLanguageTranslationRepository.Get(cultureName, translationId);
            if (t == null)
            {
                OLanguageTranslationRepository.Set(cultureName, translationId, translationId);
                t = OLanguageTranslationRepository.Get("en", translationId);
                if (t == null)
                    OLanguageTranslationRepository.Set("en", translationId, translationId);
                return translationId;
            }
            return t.NativeText;
        }
    }