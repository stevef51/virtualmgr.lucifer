﻿using ConceptCave.Data.EntityClasses;
using ConceptCave.Repository.Injected;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConceptCave.www.Language
{
    public static class Helper
    {
        public static string LanguageName
        {
            get 
            {
                return System.Threading.Thread.CurrentThread.CurrentUICulture.DisplayName;
            }
        }

        public static bool IsCurrent(LanguageEntity language)
        {
            return string.Compare(System.Threading.Thread.CurrentThread.CurrentUICulture.Name, language.CultureName, true) == 0;
        }
    }
}

