﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VirtualMgr.Membership.Interfaces;
using Microsoft.AspNet.Identity;

namespace ConceptCave.www
{
    public class MembershipDirectCurrentContextProvider : ICurrentContextProvider
    {
        public static string InternalUserIdClaimType = "ConceptCave.www.MembershipDirectCurrentContextProvider.InternalUserId";
        public object ProviderUserKey
        {
            get
            {
                return HttpContext.Current.GetOwinContext().Authentication.User.Identity.GetUserId<string>();
            }
        }

        public Guid InternalUserId
        {
            get
            {
                return Guid.Parse(HttpContext.Current.GetOwinContext().Authentication.User.FindFirst(InternalUserIdClaimType).Value);
            }
        }
    }
}