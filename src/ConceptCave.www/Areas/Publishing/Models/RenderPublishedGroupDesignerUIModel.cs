﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConceptCave.Data.EntityClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.www.Models;
using ConceptCave.Repository;
using System.Web.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ConceptCave.www.Areas.Publishing.Models
{
    public class RenderPublishedGroupDesignerUIModel
    {
        public PublishingGroupEntity Entity { get; set; }

        public SelectMultipleUsersModel Reviewers { get; set; }
        public SelectMultipleUsersModel Reviewees { get; set; }
        public List<RenderPublishedGroupChecklistItem> Checklists { get; set; }

        public SelectMultipleLabelsModel ReviewersLabels { get; set; }
        public SelectMultipleLabelsModel RevieweesLabels { get; set; }

        public RenderPublishedGroupDesignerUIModel()
        {
            Reviewers = new SelectMultipleUsersModel();
            Reviewees = new SelectMultipleUsersModel();
            Checklists = new List<RenderPublishedGroupChecklistItem>(); 
        }

        public RenderPublishedGroupDesignerUIModel(PublishingGroupEntity entity, IEnumerable<LabelEntity> availableLables)
        {
            Entity = entity;
            Reviewers = new SelectMultipleUsersModel();
            Reviewees = new SelectMultipleUsersModel();
            Checklists = new List<RenderPublishedGroupChecklistItem>();

            foreach (PublishingGroupActorEntity actor in (from p in Entity.PublishingGroupActors where p.PublishingGroupActorType.Name == PublishingGroupActorType.Reviewer.ToString() select p))
            {
                Reviewers.Items.Add(new SelectMultipleUsersModelItem(actor));
            }

            foreach (PublishingGroupActorEntity actor in (from p in Entity.PublishingGroupActors where p.PublishingGroupActorType.Name == PublishingGroupActorType.Reviewee.ToString() select p))
            {
                Reviewees.Items.Add(new SelectMultipleUsersModelItem(actor));
            }

            foreach (PublishingGroupResourceEntity resource in entity.PublishingGroupResources)
            {
                RenderPublishedGroupChecklistItem item = new RenderPublishedGroupChecklistItem() 
                {
                    Id = resource.Id, 
                    ResourceId = resource.Resource.Id,
                    ResourceNodeId = resource.Resource.NodeId,
                    Name = resource.Name,
                    IsPublic = resource.PublicPublishingGroupResources.Count > 0
                };

                if (item.IsPublic)
                {
                    item.PublicTicketId = resource.PublicPublishingGroupResources[0].PublicTicketId;
                    item.AnonymousUserId = resource.PublicPublishingGroupResources[0].UserId;
                }

                Checklists.Add(item);
            }

            ReviewersLabels = new SelectMultipleLabelsModel(availableLables, (from s in entity.PublishingGroupActorLabels where s.PublishingGroupActorType.Name == PublishingGroupActorType.Reviewer.ToString() select s.Label));
            RevieweesLabels = new SelectMultipleLabelsModel(availableLables, (from s in entity.PublishingGroupActorLabels where s.PublishingGroupActorType.Name == PublishingGroupActorType.Reviewee.ToString() select s.Label));
        }
    }

    public class RenderPublishedGroupChecklistItem
    {
        public RenderPublishedGroupChecklistItem()
        {
            PopulateReportingTables = true;
        }

        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }
        
        [HiddenInput(DisplayValue = false)]
        public int ResourceId { get; set; }
        
        [HiddenInput(DisplayValue = false)]
        public Guid ResourceNodeId { get; set; }
        
        [HiddenInput(DisplayValue = false)]
        public string Name { get; set; }

        [DisplayName("Make checklist available to anonymous users")]
        public bool IsPublic { get; set; }

        [UIHint("SingleUser")]
        public Guid? AnonymousUserId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public Guid? PublicTicketId { get; set; }

        [DisplayName("Delete if not finished after (seconds)")]
        public int? CleanUpIfNotFinishedAfterSeconds { get; set; }

        [DisplayName("Delete when finished after (seconds)")]
        public int? CleanUpIfFinishedAfterSeconds { get; set; }

        [DisplayName("Populate reporting data")]
        public bool PopulateReportingTables { get; set; }

        [UIHint("SingleMedia")]
        public Guid? Picture { get; set; }

        [DataType(DataType.MultilineText)]
        public string Description { get; set; }
    }
}