﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Data.EntityClasses;

namespace ConceptCave.www.Areas.Publishing.Models
{
    public class PublishingManagementModel
    {
        public IList<PublishingGroupEntity> Items { get; set; }
    }
}