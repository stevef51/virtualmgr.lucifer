﻿using System.Web.Mvc;

namespace ConceptCave.www.Areas.Publishing
{
    public class PublishingAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Publishing";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Publishing_default",
                "Publishing/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
