﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Repository;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.www.Areas.Publishing.Models;
using ConceptCave.www.Areas.Designer.Controllers;
using ConceptCave.www.Models;
using ConceptCave.Checklist;
using System.Transactions;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.Data.DTOConverters;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.www.Areas.Publishing.Controllers
{
    public enum PublishingManagementControllerActionType
    {
        Reviewer,
        Reviewee,
        Checklist
    }

     [Authorize(Roles = RoleRepository.COREROLE_PUBlISHING)]
    public class PublishingManagementController : ControllerBase
    {
        private readonly IPublishingGroupRepository _publishingGroupRepo;
        private readonly IPublishingGroupResourceRepository _publishingGroupResourceRepo;

        public PublishingManagementController(IPublishingGroupRepository publishingGroupRepo, IPublishingGroupResourceRepository publishingGroupResourceRepo)
        {
            _publishingGroupRepo = publishingGroupRepo;
            _publishingGroupResourceRepo = publishingGroupResourceRepo;
        }

        //
        // GET: /Publishing/PublishingManagement/

        public ActionResult Index()
        {
            var items = _publishingGroupRepo.GetAll().Select(r => r.ToEntity()).ToList();

            PublishingManagementModel model = new PublishingManagementModel() { Items = items };

            return View(model);
        }

        public ActionResult Save(int id, string name, bool allowuseasusercontext, bool allowuseasdashboard, bool allowuseastaskworkflow, bool allowuseasassetcontext)
        {
            var publish = _publishingGroupRepo.GetById(id, PublishingGroupLoadInstruction.None);

            publish.Name = name;
            publish.AllowUseAsUserContext = allowuseasusercontext;
            publish.AllowUseAsDashboard = allowuseasdashboard;
            publish.AllowUseAsTaskWorkflow = allowuseastaskworkflow;
            publish.AllowUseAsAssetContext = allowuseasassetcontext;

            _publishingGroupRepo.Save(publish, true, false);

            ViewBag.ActionTaken = ActionTaken.Save;

            return View("RenderPublishedGroupDesignerUI", new RenderPublishedGroupDesignerUIModel(publish.ToEntity(), LabelRepository.GetAll(LabelFor.Users)));
        }

        public ActionResult Add()
        {
            var item = new PublishingGroupDTO();

            item.Name = "New Publishing Group";

            _publishingGroupRepo.Save(item, true, false);

            ViewBag.ActionTaken = ActionTaken.New;

            return PartialView("RenderPublishedGroupDesignerUI", new RenderPublishedGroupDesignerUIModel(item.ToEntity(), LabelRepository.GetAll(LabelFor.Users)));
        }

        public ActionResult Remove(int id)
        {
            var publishEntity = _publishingGroupRepo.GetById(id, PublishingGroupLoadInstruction.None);

            if (publishEntity == null)
            {
                throw new ArgumentException("No publishing group with that id exists");
            }

            _publishingGroupRepo.Remove(id);

            ViewBag.ActionTaken = ActionTaken.Remove;

            // hand in the question so the view can get at the information that was deleted
            return PartialView("RenderPublishedGroupDesignerUI", new RenderPublishedGroupDesignerUIModel() { Entity = publishEntity.ToEntity() });
        }

        public ActionResult RemoveActor(int publishedGroupId, Guid membershipId, PublishingGroupActorType actortype)
        {
            PublishingGroupActorRepository.Remove(publishedGroupId, membershipId, actortype);

            ViewBag.ActionTaken = ActionTaken.Remove;
            ViewBag.SubActionTaken = actortype == PublishingGroupActorType.Reviewer ? PublishingManagementControllerActionType.Reviewer : PublishingManagementControllerActionType.Reviewee;

            return PartialView("RenderSelectMultipleUsersListItem", null);
        }

        public ActionResult AddActor(int publishedGroupId, Guid membershipId, PublishingGroupActorType actortype)
        {
            PublishingGroupActorRepository.Add(publishedGroupId, membershipId, actortype);

            PublishingGroupActorEntity actor = PublishingGroupActorRepository.GetById(publishedGroupId, membershipId, actortype, PublishingGroupActorLoadInstruction.Membership);

            ViewBag.ActionTaken = ActionTaken.New;
            ViewBag.SubActionTaken = actortype == PublishingGroupActorType.Reviewer ? PublishingManagementControllerActionType.Reviewer : PublishingManagementControllerActionType.Reviewee;

            SelectMultipleUsersModelItem model = new SelectMultipleUsersModelItem() { Id = actor.UserData.UserId, Name = actor.UserData.Name };

            return PartialView("RenderSelectMultipleUsersListItem", model);
        }

        public ActionResult AddChecklist(int publishedGroupId, Guid checklistId)
        {
            var groupResource = _publishingGroupResourceRepo.Add(publishedGroupId, checklistId);

            groupResource = _publishingGroupResourceRepo.GetById(publishedGroupId, groupResource.ResourceId, PublishingGroupResourceLoadInstruction.Resource);

            ViewBag.ActionTaken = ActionTaken.New;
            ViewBag.SubActionTaken = PublishingManagementControllerActionType.Checklist;

            RenderPublishedGroupChecklistItem model = new RenderPublishedGroupChecklistItem() { Id = groupResource.Id, ResourceId = groupResource.Resource.Id, ResourceNodeId = groupResource.Resource.NodeId, Name = groupResource.Resource.Name };

            return PartialView("RenderPublisedGroupDesignerUIItem", model);
        }

        public ActionResult RemoveChecklist(int publishedGroupId, Guid checklistId)
        {
            _publishingGroupResourceRepo.Remove(publishedGroupId, checklistId);

            ViewBag.ActionTaken = ActionTaken.Remove;
            ViewBag.SubActionTaken = PublishingManagementControllerActionType.Checklist;

            return PartialView("RenderSelectMultipleChecklistsListItem", null);
        }

        public ActionResult RefreshChecklist(int publishedGroupId, Guid checklistId)
        {
            var groupResource = _publishingGroupResourceRepo.RefreshFromResource(publishedGroupId, checklistId);
            groupResource = _publishingGroupResourceRepo.GetById(publishedGroupId, groupResource.ResourceId, PublishingGroupResourceLoadInstruction.Resource);

            ViewBag.ActionTaken = ActionTaken.Save;
            ViewBag.SubActionTaken = PublishingManagementControllerActionType.Checklist;

            SelectMultipleChecklistsModelItem model = new SelectMultipleChecklistsModelItem() { Id = checklistId, Name = groupResource.Resource.Name };

            return PartialView("RenderSelectMultipleChecklistsListItem", model);
        }

        public ActionResult EditChecklist(int publishedGroupId, Guid checklistId)
        {
            var groupResource = _publishingGroupResourceRepo.GetById(publishedGroupId, checklistId, PublishingGroupResourceLoadInstruction.PublicTickets | PublishingGroupResourceLoadInstruction.Resource);

            RenderPublishedGroupChecklistItem model = new RenderPublishedGroupChecklistItem() { Id = groupResource.Id, ResourceId = groupResource.Resource.Id, ResourceNodeId = groupResource.Resource.NodeId, Name = groupResource.Resource.Name, IsPublic = groupResource.PublicPublishingGroupResources.Count > 0 };

            model.CleanUpIfFinishedAfterSeconds = groupResource.IfFinishedCleanUpAfter;
            model.CleanUpIfNotFinishedAfterSeconds = groupResource.IfNotFinishedCleanUpAfter;
            model.PopulateReportingTables = groupResource.PopulateReportingData;

            //Entries for saving media + descriptions
            model.Description = groupResource.Description;
            model.Picture = groupResource.PictureId;

            if (model.IsPublic)
            {
                model.PublicTicketId = groupResource.PublicPublishingGroupResources[0].PublicTicketId;
                model.AnonymousUserId = groupResource.PublicPublishingGroupResources[0].UserId;
            }

            return PartialView("EditPublishedGroupDesignerUIItem", model);
        }

        public ActionResult SaveChecklist(RenderPublishedGroupChecklistItem model)
        {
            var groupResource = _publishingGroupResourceRepo.GetById(model.Id, PublishingGroupResourceLoadInstruction.PublicTickets | PublishingGroupResourceLoadInstruction.Resource);

            groupResource.PopulateReportingData = model.PopulateReportingTables;

            groupResource.IfFinishedCleanUpAfter = model.CleanUpIfFinishedAfterSeconds;
            groupResource.IfNotFinishedCleanUpAfter = model.CleanUpIfNotFinishedAfterSeconds;

            //Entries in the entity for saving media + descriptions
            groupResource.PictureId = model.Picture;
            groupResource.Description = model.Description;

            if (model.IsPublic == false)
            {
                _publishingGroupResourceRepo.RemovePublicTicket(groupResource.Id);

                RenderPublishedGroupChecklistItem result = new RenderPublishedGroupChecklistItem() { Id = groupResource.Id, ResourceId = groupResource.Resource.Id, ResourceNodeId = groupResource.Resource.NodeId, Name = groupResource.Resource.Name };

                _publishingGroupResourceRepo.Save(groupResource, true, true);

                return PartialView("RenderPublisedGroupDesignerUIItem", result);
            }

            if (groupResource.PublicPublishingGroupResources.Count > 0)
            {
                groupResource.PublicPublishingGroupResources[0].UserId = model.AnonymousUserId.Value;

                _publishingGroupResourceRepo.Save(groupResource, true, true);

                return PartialView("RenderPublisedGroupDesignerUIItem", model);
            }

            // need to create a ticket and map to that
            PublicTicketEntity ticket = new PublicTicketEntity();
            ticket.Id = CombFactory.NewComb();
            ticket.Status = (int)PublicTicketStatus.Enabled;

            ticket.PublicPublishingGroupResources.Add(new PublicPublishingGroupResourceEntity() { PublishingGroupResourceId = groupResource.Id, PublicTicketId = ticket.Id, UserId = model.AnonymousUserId.Value });

            using (TransactionScope scope = new TransactionScope())
            {
                _publishingGroupResourceRepo.Save(groupResource, false, false);
                PublicTicketRepository.Save(ticket, false, true);

                scope.Complete();
            }

            groupResource = _publishingGroupResourceRepo.GetById(model.Id, PublishingGroupResourceLoadInstruction.PublicTickets | PublishingGroupResourceLoadInstruction.Resource);

            RenderPublishedGroupChecklistItem r = new RenderPublishedGroupChecklistItem() { Id = groupResource.Id, ResourceId = groupResource.Resource.Id, ResourceNodeId = groupResource.Resource.NodeId, Name = groupResource.Resource.Name, IsPublic = true };

            if (r.IsPublic)
            {
                r.PublicTicketId = groupResource.PublicPublishingGroupResources[0].PublicTicketId;
                r.AnonymousUserId = groupResource.PublicPublishingGroupResources[0].UserId;
            }

            return PartialView("RenderPublisedGroupDesignerUIItem", r);
        }

        public ActionResult AddLabel(int publishedGroupId, Guid labelId, PublishingGroupActorType actortype)
        {
            _publishingGroupResourceRepo.AddLabel(publishedGroupId, labelId, actortype);

            return PartialView("SelectMultipleLabels", new SelectMultipleLabelsModel());
        }

        public ActionResult RemoveLabel(int publishedGroupId, Guid labelId, PublishingGroupActorType actortype)
        {
            _publishingGroupResourceRepo.RemoveLabel(publishedGroupId, labelId, actortype);

            return PartialView("SelectMultipleLabels", new SelectMultipleLabelsModel());
        }
    }
}
