﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.Data.DTOConverters;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Repository;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.www.Areas.Publishing.Models;

namespace ConceptCave.www.Areas.Publishing.Controllers
{
    public class GetPublishedGroupDesignerUIController : ControllerBase
    {
        private IPublishingGroupRepository _publishingGroupRepo;

        public GetPublishedGroupDesignerUIController(IPublishingGroupRepository publishingGroupRepo)
        {
            _publishingGroupRepo = publishingGroupRepo;
        }
        //
        // GET: /Publishing/GetPublishedGRoupDesignerUI/

        public ActionResult Index(int id)
        {
            var publish = _publishingGroupRepo.GetById(id, PublishingGroupLoadInstruction.Actors | PublishingGroupLoadInstruction.Resources | PublishingGroupLoadInstruction.ResourceTickets | PublishingGroupLoadInstruction.Labels).ToEntity();
            RenderPublishedGroupDesignerUIModel model = new RenderPublishedGroupDesignerUIModel(publish, LabelRepository.GetAll(LabelFor.Users));

            UrlHelper helper = new UrlHelper(HttpContext.Request.RequestContext);

            model.Reviewers.SearchUserUrl = UrlHelper.GenerateContentUrl("~/SearchUser/Index", HttpContext);
            model.Reviewees.SearchUserUrl = model.Reviewers.SearchUserUrl;
            
            return View(model);
        }

    }
}
