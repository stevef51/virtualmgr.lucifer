﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.Data.DTOConverters;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Repository;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;

namespace ConceptCave.www.Areas.Publishing.Controllers
{
    public class GetPublishedGroupListItemController : ControllerBase
    {
        private IPublishingGroupRepository _publishingGroupRepo;

        public GetPublishedGroupListItemController(IPublishingGroupRepository publishingGroupRepo)
        {
            _publishingGroupRepo = publishingGroupRepo;
        }
        //
        // GET: /Publishing/GetPublishedGroupListItem/

        public ActionResult Index(int id)
        {
            PublishingGroupEntity publish = _publishingGroupRepo.GetById(id, PublishingGroupLoadInstruction.None).ToEntity();

            return View(publish);
        }

    }
}
