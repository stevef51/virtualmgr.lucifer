﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace ConceptCave.www.Areas.Designer.Models
{
    public class EditEwayPaymentTemplateModelBinder : DefaultModelBinder
    {
        protected override object CreateModel(ControllerContext controllerContext, ModelBindingContext bindingContext, Type modelType)
        {
            EditEwayPaymentTemplateModel model = new EditEwayPaymentTemplateModel();

            model.AlternateHandler = bindingContext.ValueProvider.GetValue("AlternateHandler").AttemptedValue;
            model.AlternateHandlerMethod = bindingContext.ValueProvider.GetValue("AlternateHandlerMethod").AttemptedValue;


            model.FacilityId = Guid.Parse(bindingContext.ValueProvider.GetValue("FacilityId").AttemptedValue);
            model.Id = Guid.Parse(bindingContext.ValueProvider.GetValue("Id").AttemptedValue);
            model.Name = bindingContext.ValueProvider.GetValue("Name").AttemptedValue;
            model.JSON = bindingContext.ValueProvider.GetValue("JSON").AttemptedValue;

            return model;
        }
    }

    public class EditEwayPaymentTemplateModel : ModelFromAlternateHandler
    {
        [HiddenInput(DisplayValue = false)]
        public Guid Id { get; set; }

        [HiddenInput(DisplayValue = false)]
        public Guid FacilityId { get; set; }

        public string Name { get; set; }
        [UIHint("Json")]
        public string JSON { get; set; }

        public EditEwayPaymentTemplateModel()
        {
        }

        public EditEwayPaymentTemplateModel(ConceptCave.Checklist.Facilities.eWayPaymentFacility.TransactionTemplate template, Guid facilityId)
        {
            Id = template.Id;
            FacilityId = facilityId;
            Name = template.Name;
            JSON = template.JSON;
        }
    }
}