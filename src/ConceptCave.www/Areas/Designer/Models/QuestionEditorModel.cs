﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConceptCave.BusinessLogic.Configuration;
using ConceptCave.Checklist.Core;
using ConceptCave.Repository;

namespace ConceptCave.www.Areas.Designer.Models
{
    public class ResourcesEditorModel
    {
        public List<ResourceEditorModel> Resources { get; set; }

        public IEnumerable<IRepositorySectionManagerItem> AvailableResourcesTypes { get; set; }
    }
}