﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConceptCave.Checklist.Editor;
using ConceptCave.Data.EntityClasses;

namespace ConceptCave.www.Areas.Designer.Models
{
    public class RenderDesignDocumentUIModel
    {
        public Guid ItemId { get; set; }
        public Guid OldParentId { get; set; }
        public Guid NewParentId { get; set; }
    }
}