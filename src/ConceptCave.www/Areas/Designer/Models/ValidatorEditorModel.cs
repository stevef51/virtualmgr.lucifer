﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConceptCave.Checklist.Validators;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.www.Models;
using ConceptCave.Repository.ProcessingRules;
using ConceptCave.Checklist.Editor;
using ConceptCave.Checklist.ProcessingRules;

namespace ConceptCave.www.Areas.Designer.Models
{
    public class ValidatorEditorModel
    {
        public GetResourceDesignerUIModel ResourceModel { get; set; }
        public IProcessingRule Validator { get; set; }
    }

    public class ValidatorJSONDataConverter : IJSONDataModelItemConverter
    {
        public const string NamePropertyName = "name";

        public virtual void Convert(JSONDataModelItem item, Core.IUniqueNode node)
        {
            IProcessingRule rule = (IProcessingRule)node;

            var names = (from p in item.Properties where p.Name == NamePropertyName select p);

            if(names.Count() > 0)
            {
                rule.Name = names.First().Value;
            }
        }
    }

    public class MandatoryValidatorJSONDataConverter : ValidatorJSONDataConverter
    {
        public const string ErrorMessagePropertyName = "errormessage";

        public override void Convert(JSONDataModelItem item, Core.IUniqueNode node)
        {
            base.Convert(item, node);

            MandatoryValidator validator = (MandatoryValidator)node;

            var names = (from p in item.Properties where p.Name == ErrorMessagePropertyName select p);

            if (names.Count() > 0)
            {
                validator.ErrorMessage = names.First().Value;
            }
        }
    }

    public class NumberRangeValidatorJSONDataConverter : ValidatorJSONDataConverter
    {
        public const string MinimumPropertyName = "min";
        public const string MaximumPropertyName = "max";

        public override void Convert(JSONDataModelItem item, Core.IUniqueNode node)
        {
            base.Convert(item, node);

            NumberRangeValidator validator = (NumberRangeValidator)node;

            var mins = (from p in item.Properties where p.Name == MinimumPropertyName select p);
            if (mins.Count() > 0)
            {
                decimal min = 0;

                if (decimal.TryParse(mins.First().Value, out min) == true)
                {
                    validator.MinimumValue = min;
                }
            }

            var maxs = (from p in item.Properties where p.Name == MaximumPropertyName select p);
            if (maxs.Count() > 0)
            {
                decimal max = 0;

                if (decimal.TryParse(maxs.First().Value, out max) == true)
                {
                    validator.MaximumValue = max;
                }
            }
        }
    }

    public class MinMaxLengthValidatorJSONDataConverter : ValidatorJSONDataConverter
    {
        public const string MinimumPropertyName = "min";
        public const string MaximumPropertyName = "max";

        public override void Convert(JSONDataModelItem item, Core.IUniqueNode node)
        {
            base.Convert(item, node);

            MinMaxLengthValidator validator = (MinMaxLengthValidator)node;

            var mins = (from p in item.Properties where p.Name == MinimumPropertyName select p);
            if (mins.Count() > 0)
            {
                int min = 0;

                if (int.TryParse(mins.First().Value, out min) == true)
                {
                    validator.MinLength = min;
                }
            }

            var maxs = (from p in item.Properties where p.Name == MaximumPropertyName select p);
            if (maxs.Count() > 0)
            {
                int max = 0;

                if (int.TryParse(maxs.First().Value, out max) == true)
                {
                    validator.MaxLength = max;
                }
            }
        }
    }
                 
    public class AllowedValuesValidatorJSONDataConverter : ValidatorJSONDataConverter
    {
        public const string AllowedValuesPropertyName = "allowedvalues";
        public const string ErrorMessagePropertyName = "errormessage";

        public override void Convert(JSONDataModelItem item, Core.IUniqueNode node)
        {
            base.Convert(item, node);

            AllowedValuesValidator validator = (AllowedValuesValidator)node;

            var names = (from p in item.Properties where p.Name == ErrorMessagePropertyName select p);

            if (names.Count() > 0)
            {
                validator.ErrorMessage = names.First().Value;
            }

            validator.AllowedValues.Clear();

            names = (from p in item.Properties where p.Name == AllowedValuesPropertyName select p);

            if(names.Count() > 0)
            {
                foreach(var part in names.First().Value.Split(','))
                {
                    string p = part.Trim(' ');

                    if(string.IsNullOrEmpty(p))
                    {
                        continue;
                    }

                    validator.AllowedValues.Add(p);
                }
            }
        }
    }

    public class RegularExpressionValidatorJSONDataConverter : ValidatorJSONDataConverter
    {
        public const string ValidationExpressionPropertyName = "validationexpression";
        public const string ErrorMessagePropertyName = "errormessage";

        public override void Convert(JSONDataModelItem item, Core.IUniqueNode node)
        {
            base.Convert(item, node);

            RegularExpressionValidator validator = (RegularExpressionValidator)node;

            var names = (from p in item.Properties where p.Name == ValidationExpressionPropertyName select p);

            if (names.Count() > 0)
            {
                validator.ValidationExpression = names.First().Value;
            }

            names = (from p in item.Properties where p.Name == ErrorMessagePropertyName select p);

            if (names.Count() > 0)
            {
                validator.ErrorMessage = names.First().Value;
            }
        }
    }

    public class CategoryValidatorJSONDataConverter : ValidatorJSONDataConverter
    {
        public const string CategoryPropertyName = "category";
        public const string MandatoryCountPropertyName = "mandatorycount";
        public const string ErrorMessagePropertyName = "errormessage";

        public override void Convert(JSONDataModelItem item, Core.IUniqueNode node)
        {
            base.Convert(item, node);

            CategoryValidator validator = (CategoryValidator)node;

            var names = (from p in item.Properties where p.Name == CategoryPropertyName select p);

            if (names.Count() > 0)
            {
                validator.Category = names.First().Value;
            }

            names = (from p in item.Properties where p.Name == MandatoryCountPropertyName select p);

            if (names.Count() > 0)
            {
                int val = 1;
                if (int.TryParse(names.First().Value, out val))
                {
                    validator.MandatoryCount = val;
                }
            }

            names = (from p in item.Properties where p.Name == ErrorMessagePropertyName select p);

            if (names.Count() > 0)
            {
                validator.ErrorMessage = names.First().Value;
            }
        }
    }

    public class GeoCodeDistanceRuleJSONDataConverter : ValidatorJSONDataConverter
    {
        public const string UseSpecifiedLocationPropertyName = "UseSpecifiedLocation";
        public const string SpecifiedLatitudePropertyName = "SpecifiedLatitude";
        public const string SpecifiedLongitudePropertyName = "SpecifiedLongitude";
        public const string RadiusPropertyName = "Radius";
        public const string EnforceMandatoryPropertyName = "Mandatory";

        public override void Convert(JSONDataModelItem item, Core.IUniqueNode node)
        {
            base.Convert(item, node);

            GeoCodeDistanceRule validator = (GeoCodeDistanceRule)node;

            var useSpecs = (from p in item.Properties where p.Name == UseSpecifiedLocationPropertyName select p);
            if (useSpecs.Count() > 0)
            {
                validator.UseSpecifiedLocation = useSpecs.First().Value.Equals("on", StringComparison.InvariantCultureIgnoreCase) == true;
            }

            var lats = (from p in item.Properties where p.Name == SpecifiedLatitudePropertyName select p);
            var longs = (from p in item.Properties where p.Name == SpecifiedLongitudePropertyName select p);
            if (lats.Count() > 0 && longs.Count() > 0)
            {
                decimal lat = 0;
                decimal lng = 0;

                if (decimal.TryParse(lats.First().Value, out lat) == true && decimal.TryParse(longs.First().Value, out lng) == true)
                {
                    validator.SpecifiedLocation = new LatLng(lat, lng, 0);
                }
            }

            var rads = (from p in item.Properties where p.Name == RadiusPropertyName select p);
            if (rads.Count() > 0)
            {
                decimal rad = 0;

                if (decimal.TryParse(rads.First().Value, out rad) == true)
                {
                    validator.Radius = rad;
                }
            }

            var mands = (from p in item.Properties where p.Name == EnforceMandatoryPropertyName select p);
            if (mands.Count() > 0)
            {
                validator.EnforceMandatory = mands.First().Value.Equals("on", StringComparison.InvariantCultureIgnoreCase) == true;
            }
        }
    }

    public class PublicVariableJSONDataConverter : ValidatorJSONDataConverter
    {
        public const string NamePropertyName = "name";

        public override void Convert(JSONDataModelItem item, Core.IUniqueNode node)
        {
            base.Convert(item, node);

            PublicVariable validator = (PublicVariable)node;

            var names = (from p in item.Properties where p.Name == NamePropertyName select p);
            if (names.Count() > 0)
            {
                decimal min = 0;

                validator.Name = names.First().Value;
            }
        }
    }

    public class CategoryRuleJSONDataConverter : ValidatorJSONDataConverter
    {
        public const string TextPropertyName = "text";

        public override void Convert(JSONDataModelItem item, Core.IUniqueNode node)
        {
            base.Convert(item, node);

            CategoryRule validator = (CategoryRule)node;

            var names = (from p in item.Properties where p.Name == TextPropertyName select p);
            if (names.Count() > 0)
            {
                validator.Text = names.First().Value;
            }
        }
    }

    public class TableFormatterRuleJSONDataConverter : ValidatorJSONDataConverter
    {
        public const string VisiblePropertyName = "visible";

        public override void Convert(JSONDataModelItem item, Core.IUniqueNode node)
        {
            base.Convert(item, node);

            TableFormatterRule rule = (TableFormatterRule)node;

            var names = (from p in item.Properties where p.Name == VisiblePropertyName select p);
            if (names.Count() > 0)
            {
                rule.Visible = bool.Parse(names.First().Value);
            }

        }
    }
}