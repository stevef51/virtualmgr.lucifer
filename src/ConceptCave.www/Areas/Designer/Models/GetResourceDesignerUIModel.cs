﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConceptCave.BusinessLogic.Configuration;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Repository;
using ConceptCave.Core;
using System.ComponentModel.DataAnnotations;
using ConceptCave.www.Models;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Data.EntityClasses;

namespace ConceptCave.www.Areas.Designer.Models
{
    public class GetResourceDesignerUIModel : ModelFromAlternateHandler
    {
        public IRepositorySectionManagerItem DesignerType { get; set; }
        public IResource Resource { get; set; }

        public IEnumerable<LanguageEntity> Languages { get; set; }

        public bool IsRoot { get; set; }
        public Guid RootId { get; set; }

        public bool IsNameMandatory { get; set; }

        public SelectMultipleLabelsModel Labels { get; set; }

        public IEnumerable<LabelEntity> SelectedLabels { get; set; }

        /// <summary>
        /// Just a utility property to get at the Name of the Resource but to have some control over
        /// the field name that gets used in the Form in the front end
        /// </summary>
        public string Name
        {
            get
            {
                return Resource.Name;
            }
            set
            {
                Resource.Name = value;
            }
        }

        public GetResourceDesignerUIModel()
        {
            ContextFields = new List<ControllerWithAlternateHandlerContextField>();
        }
    }
}