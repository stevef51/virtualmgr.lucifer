﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.Repository.Facilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace ConceptCave.www.Areas.Designer.Models
{
    public class EditMessageFacilityTemplateModel : ModelFromAlternateHandler
    {
        [HiddenInput(DisplayValue = false)]
        public Guid? Id { get; set; }

        [HiddenInput(DisplayValue = false)]
        public Guid FacilityId { get; set; }

        public Dictionary<Guid, string> Users { get; set; }
        public List<Guid> Labels { get; set; }

        [UIHint("SingleUser")]
        [DisplayName("Sender")]
        public Guid? Sender { get; set; }

        public string Name { get; set; }

        [DisplayName("Requires Acknowledgement")]
        public bool RequiresAcknowledgement { get; set; }

        public readonly MessagingFacility.MessagingTemplate InnerTemplate;

        public EditMessageFacilityTemplateModel()
        {
            Users = new Dictionary<Guid, string>();
            Labels = new List<Guid>();
        }

        public EditMessageFacilityTemplateModel(MessagingFacility.MessagingTemplate template, Guid facilityId)
            : this() //this assigns uses and labels
        {
            throw new NotImplementedException("I broke messagingpretty bad");
            /*Id = template.Id;
            Name = template.Name;
            FacilityId = facilityId;
            RequiresAcknowledgement = template.RequiresAcknowledgement;
            if (template.Sender != null)
                Sender = template.Sender.Id;
            else
                Sender = null;
            InnerTemplate = template;

            foreach (var u in template.RecipientUsers)
                Users.Add(u.Id, u.Name);
            foreach (var l in template.RecipientLabels)
                Labels.Add(l.Id);*/
        }
    }
}