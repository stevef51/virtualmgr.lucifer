﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConceptCave.BusinessLogic.Configuration;
using ConceptCave.BusinessLogic.Questions;
using ConceptCave.Checklist.Core;
using System.Web.Mvc;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.www.Areas.Designer.Controllers;
using ConceptCave.Data.EntityClasses;
using System.ComponentModel.DataAnnotations;
using ConceptCave.Repository;
using ConceptCave.Core;
using ConceptCave.Checklist.Facilities;
using ConceptCave.Checklist.Editor;
using ConceptCave.Repository.Questions;
using ConceptCave.Checklist.Editor.Reporting;

namespace ConceptCave.www.Areas.Designer.Models
{
    public class ResourceManagerModel
    {
        public List<ResourceEditorModel> Resources { get; set; }
        public IEnumerable<IRepositorySectionManagerItem> AvailableResourceTypes { get; set; }
    }

    public class ResourceEditorModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string AlternateHandler {get;set;}
        public string AlternateHandlerMethod { get; set; }
        public IRepositorySectionManagerItem DesignerType { get; set; }
        public List<ControllerWithAlternateHandlerContextField> ContextFields { get; set; }

        public IEnumerable<LabelResourceEntity> LabelResources { get; set; }


        public virtual void PopulateFromValueProvider(IValueProvider provider, IRepositorySectionManagerItem designerType)
        {
            Id = new Guid(provider.GetValue(ResourceManagementController.QuestionIdFieldName).AttemptedValue);
            DesignerType = designerType;
            Name = provider.GetValue(ResourceManagementController.ResourceNameFieldName).AttemptedValue;

            var alternateHandler = provider.GetValue(ResourceManagementController.AlternameHandlerFieldName);
            if (alternateHandler != null)
            {
                AlternateHandler = alternateHandler.AttemptedValue;
            }

            var alternateHandlerMethod = provider.GetValue(ResourceManagementController.AlternameHandlerMethodFieldName);
            if (alternateHandlerMethod != null)
            {
                AlternateHandlerMethod = alternateHandlerMethod.AttemptedValue;
            }

            ContextFields = new List<ControllerWithAlternateHandlerContextField>();
            var contextstring = provider.GetValue(ResourceManagementController.ContextFieldName);
            if (contextstring != null)
            {
                ContextFields = ControllerWithAlternateHandler.ContextListFromString(contextstring.AttemptedValue);
            }


        }

        public virtual void FromResource(IResource resource, IRepositorySectionManagerItem designerType)
        {
            Id = resource.Id;
            Name = resource.Name;
            DesignerType = designerType;
        }

        public virtual void SetResourceValues(IResource resource)
        {
            resource.Name = Name;
        }

        public virtual void FromResourceEntity(ResourceEntity resourceEntity, IRepositorySectionManagerItem designerType)
        {
            Id = resourceEntity.NodeId;
            DesignerType = designerType;
            Name = resourceEntity.Name;

            LabelResources = resourceEntity.LabelResources;
        }

        protected virtual bool ParseCheckboxField(IValueProvider provider, string FieldName, bool defaultValue = false)
        {
            var value = provider.GetValue(FieldName);
            if (value == null)
                return defaultValue;

            return bool.Parse(value.AttemptedValue.Split(',')[0]);
        }

        protected virtual int? ParseIntField(IValueProvider provider, string FieldName, int? defaultValue = null)
        {
            var value = provider.GetValue(FieldName);
            if (value == null)
                return defaultValue;

            int result = 0;
            if (int.TryParse(value.AttemptedValue, out result))
                return result;

            return defaultValue;
        }

        protected virtual decimal? ParseDecimalField(IValueProvider provider, string FieldName, decimal? defaultValue = null)
        {
            var value = provider.GetValue(FieldName);
            if (value == null)
                return defaultValue;

            decimal result = 0;
            if (decimal.TryParse(value.AttemptedValue, out result))
                return result;

            return defaultValue;
        }

        public static IDictionary<string, string> ParseStringDictionary(IValueProvider provider, string fieldName)
        {
            var dict = new Dictionary<string, string>();
            int i = 0;
            do
            {
                var key = provider.GetValue(fieldName + "[" + i.ToString() + "].Key");
                var value = provider.GetValue(fieldName + "[" + i.ToString() + "].Value");
                if (key != null && value != null)
                    dict[key.AttemptedValue] = value.AttemptedValue;
                else
                    break;
                i++;
            } while (true);
            return dict;
        }

    }

    public class QuestionResourceEditorModel : ResourceEditorModel
    {
        public bool HidePrompt { get; set; }
        public bool AllowNotesOnAnswer { get; set; }
        public decimal PossibleScore { get; set; }
        public bool Visible { get; set; }
        public string HtmlCustomStyling { get; set; }
        public string HtmlCustomClass { get; set; }
        public ISet<Guid> Labels { get; set; }
        public IDictionary<string, string> LocalisablePrompt { get; set; }
        public IDictionary<string, string> LocalisableNote { get; set; }
        public IDictionary<string, string> LocalisableHelpContent { get; set; }

        public HtmlPresentationHintLabelPlacement HelpPosition { get; set; }

        public override void PopulateFromValueProvider(IValueProvider provider, IRepositorySectionManagerItem designerType)
        {
            base.PopulateFromValueProvider(provider, designerType);

            HidePrompt = ParseCheckboxField(provider, "hideprompt");

            AllowNotesOnAnswer = ParseCheckboxField(provider, "allownotesonanswer");

            Visible = ParseCheckboxField(provider, "visible");

            HtmlCustomClass = provider.GetValue("htmlcustomclass").AttemptedValue;
            HtmlCustomStyling = provider.GetValue("htmlcustomstyling").AttemptedValue;

            Labels = new HashSet<Guid>();
            var labelprovider = provider.GetValue("labels");
            if (labelprovider != null)
            {
                var labelstrings = labelprovider.AttemptedValue.Split(',');
                foreach (var str in labelstrings)
                    Labels.Add(Guid.Parse(str));
            }

            HelpPosition = (HtmlPresentationHintLabelPlacement)Enum.Parse(typeof(HtmlPresentationHintLabelPlacement), provider.GetValue("helpposition").AttemptedValue);

            LocalisablePrompt = ParseStringDictionary(provider, "localisableprompt");
            LocalisableNote = ParseStringDictionary(provider, "localisablenote");
            LocalisableHelpContent = ParseStringDictionary(provider, "localisablehelpcontent");
        }

        public override void FromResource(IResource resource, IRepositorySectionManagerItem designerType)
        {
            base.FromResource(resource, designerType);

            IQuestion question = (IQuestion)resource;
            HidePrompt = question.HidePrompt;
            PossibleScore = question.PossibleScore;
            AllowNotesOnAnswer = question.AllowNotesOnAnswer;
            Visible = question.ClientModel.Visible;
            HtmlCustomClass = question.HtmlPresentation.CustomClass;
            HtmlCustomStyling = question.HtmlPresentation.CustomStyling;
            Labels = question.Labels;

            HelpPosition = question.HelpPosition;

            LocalisablePrompt = question.LocalisablePrompt.ToDictionary();
            LocalisableNote = question.LocalisableNote.ToDictionary();
            LocalisableHelpContent = question.LocalisableHelpContent.ToDictionary();
        }

        public override void SetResourceValues(IResource resource)
        {
            base.SetResourceValues(resource);

            IQuestion question = (IQuestion)resource;

            question.HidePrompt = HidePrompt;
            question.PossibleScore = PossibleScore;
            question.AllowNotesOnAnswer = AllowNotesOnAnswer;
            question.ClientModel.Visible = Visible;
            question.HtmlPresentation.CustomClass = HtmlCustomClass;
            question.HtmlPresentation.CustomStyling = HtmlCustomStyling;

            question.Labels.Clear();
            foreach (var lbl in Labels)
                question.Labels.Add(lbl);

            question.HelpPosition = HelpPosition;

            question.LocalisablePrompt.FromDictionary(LocalisablePrompt);
            question.LocalisableNote.FromDictionary(LocalisableNote);
            question.LocalisableHelpContent.FromDictionary(LocalisableHelpContent);
        }
    }

    public class TemplatedQuestionResourceEditorModel : QuestionResourceEditorModel
    {
        public int? TemplatedResource { get; set; }
        public Guid? TemplatedQuestion { get; set; }

        public override void FromResource(IResource resource, IRepositorySectionManagerItem designerType)
        {
            base.FromResource(resource, designerType);
            var tp = (ITemplatedPresentable)resource;
            TemplatedResource = tp.TemplatePublishedResourceId;
            TemplatedQuestion = tp.TemplateQuestionId;
        }

        public override void PopulateFromValueProvider(IValueProvider provider, IRepositorySectionManagerItem designerType)
        {
            base.PopulateFromValueProvider(provider, designerType);

            TemplatedResource = null;
            TemplatedQuestion = null;
            if (provider.GetValue("Resource.TemplatePublishedResourceId") != null &&
                provider.GetValue("Resource.TemplateQuestionId") != null)
            {
                var pubresourceidval = provider.GetValue("Resource.TemplatePublishedResourceId").AttemptedValue;
                var questionidval = provider.GetValue("Resource.TemplateQuestionId").AttemptedValue;
                if (!string.IsNullOrEmpty(pubresourceidval) && !string.IsNullOrEmpty(questionidval))
                {
                    try
                    {
                        TemplatedResource = int.Parse(pubresourceidval);
                        TemplatedQuestion = Guid.Parse(questionidval);
                    }
                    catch (FormatException)
                    {
                        TemplatedResource = null;
                        TemplatedQuestion = null;
                    }
                }
            }

        }

        public override void SetResourceValues(IResource resource)
        {
            base.SetResourceValues(resource);
            var tp = (ITemplatedPresentable)resource;
            tp.TemplatePublishedResourceId = TemplatedResource;
            tp.TemplateQuestionId = TemplatedQuestion;
        }
    }

    public class FreeTextQuestionEditorModel : QuestionResourceEditorModel 
    {
        public bool AllowMultiLine { get; set; }
        public string DefaultAnswer { get; set; }

        public override void PopulateFromValueProvider(IValueProvider provider, IRepositorySectionManagerItem designerType)
        {
            base.PopulateFromValueProvider(provider, designerType);

            AllowMultiLine = ParseCheckboxField(provider, "allowmultiline");
            DefaultAnswer = provider.GetValue("defaultanswer").AttemptedValue;
        }

        public override void FromResource(IResource resource, IRepositorySectionManagerItem designerType)
        {
            base.FromResource(resource, designerType);

            AllowMultiLine = ((IFreeTextQuestion)resource).AllowMultiLine;
            DefaultAnswer = (string)((IFreeTextQuestion)resource).DefaultAnswer;
        }

        public override void SetResourceValues(IResource resource)
        {
            base.SetResourceValues(resource);

            ((IFreeTextQuestion)resource).AllowMultiLine = AllowMultiLine;
            ((IFreeTextQuestion)resource).DefaultAnswer = DefaultAnswer;
        }
    }

    public class NumberQuestionEditorModel : QuestionResourceEditorModel 
    {
        public int DecimalPlaces { get; set; }
        public decimal? MaxValue { get; set; }
        public decimal? MinValue { get; set; }

        public bool ShowKeypad { get; set; }
        public bool UseDecimalKeypad { get; set; }

        public override void PopulateFromValueProvider(IValueProvider provider, IRepositorySectionManagerItem designerType)
        {
            base.PopulateFromValueProvider(provider, designerType);

            DecimalPlaces = int.Parse(provider.GetValue("decimalplaces").AttemptedValue);
            MaxValue = ParseDecimalField(provider, "maxvalue");
            MinValue = ParseDecimalField(provider, "minvalue");
            ShowKeypad = ParseCheckboxField(provider, "showkeypad");
            UseDecimalKeypad = ParseCheckboxField(provider, "usedecimalkeypad");
        }

        public override void FromResource(IResource resource, IRepositorySectionManagerItem designerType)
        {
            base.FromResource(resource, designerType);

            DecimalPlaces = ((NumberQuestion)resource).DecimalPlaces;
            MaxValue = ((NumberQuestion)resource).MaxValue;
            MinValue = ((NumberQuestion)resource).MinValue;
            ShowKeypad = ((NumberQuestion)resource).ShowKeypad;
            UseDecimalKeypad = ((NumberQuestion)resource).UseDecimalKeypad;
        }

        public override void SetResourceValues(IResource resource)
        {
            base.SetResourceValues(resource);

            ((NumberQuestion)resource).DecimalPlaces = DecimalPlaces;
            ((NumberQuestion)resource).MaxValue = MaxValue;
            ((NumberQuestion)resource).MinValue = MinValue;
            ((NumberQuestion)resource).ShowKeypad = ShowKeypad;
            ((NumberQuestion)resource).UseDecimalKeypad = UseDecimalKeypad;
        }
    }

    public class TemperatureProbeQuestionEditorModel : QuestionResourceEditorModel 
    {
        public bool DisplayAsFarenheit { get; set; }
        public int RefreshPeriod { get; set; }
        public decimal MinimumPassReading { get; set; }
        public decimal MaximumPassReading { get; set; }
        public int DisconnectProbeTimeout { get; set; }

        public string AllowedProbeProviders { get; set; }
        public string AllowedProbeSerialNumbers { get; set; }
        public int ProbeOutOfRangeTimeout { get; set; }
        public TemperatureReadingType TemperatureReading { get; set; }

        public override void PopulateFromValueProvider(IValueProvider provider, IRepositorySectionManagerItem designerType)
        {
            base.PopulateFromValueProvider(provider, designerType);

            DisplayAsFarenheit = ParseCheckboxField(provider, "displayasfarenheit");
            RefreshPeriod = int.Parse(provider.GetValue("refreshperiod").AttemptedValue);

            MinimumPassReading = decimal.Parse(provider.GetValue("minimumpassreading").AttemptedValue);
            MaximumPassReading = decimal.Parse(provider.GetValue("maximumpassreading").AttemptedValue);

            DisconnectProbeTimeout = int.Parse(provider.GetValue("disconnectprobetimeout").AttemptedValue);
            AllowedProbeProviders = provider.GetValue("allowedprobeproviders").AttemptedValue;
            AllowedProbeSerialNumbers = provider.GetValue("allowedprobeserialnumbers").AttemptedValue;
            ProbeOutOfRangeTimeout = int.Parse(provider.GetValue("probeoutofrangetimeout").AttemptedValue);
            TemperatureReading = (TemperatureReadingType)Enum.Parse(typeof(TemperatureReadingType), provider.GetValue("temperaturereading").AttemptedValue);

        }

        public override void FromResource(IResource resource, IRepositorySectionManagerItem designerType)
        {
            base.FromResource(resource, designerType);

            DisplayAsFarenheit = ((TemperatureProbeQuestion)resource).DisplayAsFarenheit;
            RefreshPeriod = ((TemperatureProbeQuestion)resource).RefreshPeriod;

            MinimumPassReading = ((TemperatureProbeQuestion)resource).MinimumPassReading;
            MaximumPassReading = ((TemperatureProbeQuestion)resource).MaximumPassReading;

            DisconnectProbeTimeout = ((TemperatureProbeQuestion)resource).DisconnectProbeTimeout;
            AllowedProbeProviders = ((TemperatureProbeQuestion)resource).AllowedProbeProviders;
            AllowedProbeSerialNumbers = ((TemperatureProbeQuestion)resource).AllowedProbeSerialNumbers;

            ProbeOutOfRangeTimeout = ((TemperatureProbeQuestion)resource).ProbeOutOfRangeTimeout;
            TemperatureReading = ((TemperatureProbeQuestion)resource).TemperatureReading;
        }

        public override void SetResourceValues(IResource resource)
        {
            base.SetResourceValues(resource);

            ((TemperatureProbeQuestion)resource).DisplayAsFarenheit = DisplayAsFarenheit;
            ((TemperatureProbeQuestion)resource).RefreshPeriod = RefreshPeriod;

            ((TemperatureProbeQuestion)resource).MinimumPassReading = MinimumPassReading;
            ((TemperatureProbeQuestion)resource).MaximumPassReading = MaximumPassReading;

            ((TemperatureProbeQuestion)resource).DisconnectProbeTimeout = DisconnectProbeTimeout;
            ((TemperatureProbeQuestion)resource).AllowedProbeProviders = AllowedProbeProviders;
            ((TemperatureProbeQuestion)resource).AllowedProbeSerialNumbers = AllowedProbeSerialNumbers;
            ((TemperatureProbeQuestion)resource).ProbeOutOfRangeTimeout = ProbeOutOfRangeTimeout;
            ((TemperatureProbeQuestion)resource).TemperatureReading = TemperatureReading;
        }
    }

    public class pHProbeQuestionEditorModel : QuestionResourceEditorModel
    {
        public int RefreshPeriod { get; set; }
        public decimal MinimumPassReading { get; set; }
        public decimal MaximumPassReading { get; set; }

        public override void PopulateFromValueProvider(IValueProvider provider, IRepositorySectionManagerItem designerType)
        {
            base.PopulateFromValueProvider(provider, designerType);

            RefreshPeriod = int.Parse(provider.GetValue("refreshperiod").AttemptedValue);

            MinimumPassReading = decimal.Parse(provider.GetValue("minimumpassreading").AttemptedValue);
            MaximumPassReading = decimal.Parse(provider.GetValue("maximumpassreading").AttemptedValue);
        }

        public override void FromResource(IResource resource, IRepositorySectionManagerItem designerType)
        {
            base.FromResource(resource, designerType);

            RefreshPeriod = ((pHProbeQuestion)resource).RefreshPeriod;

            MinimumPassReading = ((pHProbeQuestion)resource).MinimumPassReading;
            MaximumPassReading = ((pHProbeQuestion)resource).MaximumPassReading;
        }

        public override void SetResourceValues(IResource resource)
        {
            base.SetResourceValues(resource);

            ((pHProbeQuestion)resource).RefreshPeriod = RefreshPeriod;

            ((pHProbeQuestion)resource).MinimumPassReading = MinimumPassReading;
            ((pHProbeQuestion)resource).MaximumPassReading = MaximumPassReading;
        }
    }

    public class CheckboxQuestionEditorModel : QuestionResourceEditorModel 
    {
        public CheckboxQuestionNoAnswerBehaviour NoAnswerBehaviour { get; set; }

        public override void PopulateFromValueProvider(IValueProvider provider, IRepositorySectionManagerItem designerType)
        {
            base.PopulateFromValueProvider(provider, designerType);

            NoAnswerBehaviour = (CheckboxQuestionNoAnswerBehaviour)Enum.Parse(typeof(CheckboxQuestionNoAnswerBehaviour), provider.GetValue("noanswerbehaviour").AttemptedValue);
        }

        public override void FromResource(IResource resource, IRepositorySectionManagerItem designerType)
        {
            base.FromResource(resource, designerType);

            NoAnswerBehaviour = ((ICheckboxQuestion)resource).NoAnswerBehaviour;
        }

        public override void SetResourceValues(IResource resource)
        {
            base.SetResourceValues(resource);

            ((ICheckboxQuestion)resource).NoAnswerBehaviour = NoAnswerBehaviour;
        }
    }

    public class TableQuestionEditorModel : QuestionResourceEditorModel
    {
        public string DataSource { get; set; }
        public string UpdateDataSources { get; set; }

        public string ClearDataSources { get; set; }

        public bool UseDataSourceVisibleFields { get; set; }

        public string VisibleFields { get; set; }

        public bool ShowControls { get; set; }

        public bool HideWhenNoData { get; set; }

        public bool Stripes { get; set; }
        public bool Bordered { get; set; }
        public bool Hover { get; set; }
        public bool Condense { get; set; }

        public override void PopulateFromValueProvider(IValueProvider provider, IRepositorySectionManagerItem designerType)
        {
            base.PopulateFromValueProvider(provider, designerType);

            DataSource =  provider.GetValue("datasource").AttemptedValue;
            UpdateDataSources = provider.GetValue("updatedatasources").AttemptedValue;
            ClearDataSources = provider.GetValue("cleardatasources").AttemptedValue;

            HideWhenNoData = ParseCheckboxField(provider, "hidewhennodata");

            UseDataSourceVisibleFields = ParseCheckboxField(provider, "usedatasourcevisiblefields");

            ShowControls = ParseCheckboxField(provider, "showcontrols");
            VisibleFields = provider.GetValue("visiblefields").AttemptedValue;

            Stripes = ParseCheckboxField(provider, "stripes");
            Bordered = ParseCheckboxField(provider, "borderd");
            Hover = ParseCheckboxField(provider, "hover");
            Condense = ParseCheckboxField(provider, "condense");
        }

        public override void FromResource(IResource resource, IRepositorySectionManagerItem designerType)
        {
            base.FromResource(resource, designerType);

            DataSource = ((TableQuestion)resource).DataSource;
            UpdateDataSources = ((TableQuestion)resource).UpdateDataSources;
            ClearDataSources = ((TableQuestion)resource).ClearDataSources;

            HideWhenNoData = ((TableQuestion)resource).HideWhenNoData;

            UseDataSourceVisibleFields = ((TableQuestion)resource).UseDataSourceVisibleFields;
            VisibleFields = ((TableQuestion)resource).VisibleFields;

            ShowControls = ((TableQuestion)resource).ShowControls;

            Stripes = ((TableQuestion)resource).Stripes;
            Bordered = ((TableQuestion)resource).Bordered;
            Hover = ((TableQuestion)resource).Hover;
            Condense = ((TableQuestion)resource).Condense;
        }

        public override void SetResourceValues(IResource resource)
        {
            base.SetResourceValues(resource);

            ((TableQuestion)resource).DataSource = DataSource;
            ((TableQuestion)resource).UpdateDataSources = UpdateDataSources;
            ((TableQuestion)resource).ClearDataSources = ClearDataSources;

            ((TableQuestion)resource).HideWhenNoData = HideWhenNoData;

            ((TableQuestion)resource).UseDataSourceVisibleFields = UseDataSourceVisibleFields;
            ((TableQuestion)resource).VisibleFields = VisibleFields;

            ((TableQuestion)resource).ShowControls = ShowControls;

            ((TableQuestion)resource).Stripes = Stripes;
            ((TableQuestion)resource).Bordered = Bordered;
            ((TableQuestion)resource).Hover = Hover;
            ((TableQuestion)resource).Condense = Condense;
        }
    }

    public class RepeaterQuestionEditorModel : QuestionResourceEditorModel
    {
        public string DataSource { get; set; }
        public string UpdateDataSources { get; set; }
        public string ClearDataSources { get; set; }

        public bool HideWhenNoData { get; set; }
        public bool ShowControls { get; set; }
        public string RepeaterContent { get; set; }

        public override void PopulateFromValueProvider(IValueProvider provider, IRepositorySectionManagerItem designerType)
        {
            base.PopulateFromValueProvider(provider, designerType);

            DataSource = provider.GetValue("datasource").AttemptedValue;
            UpdateDataSources = provider.GetValue("updatedatasources").AttemptedValue;
            ClearDataSources = provider.GetValue("cleardatasources").AttemptedValue;

            HideWhenNoData = ParseCheckboxField(provider, "hidewhennodata");
            ShowControls = ParseCheckboxField(provider, "showcontrols");
            RepeaterContent = provider.GetValue("repeatcontent").AttemptedValue;
        }

        public override void FromResource(IResource resource, IRepositorySectionManagerItem designerType)
        {
            base.FromResource(resource, designerType);

            DataSource = ((RepeaterQuestion)resource).DataSource;
            UpdateDataSources = ((RepeaterQuestion)resource).UpdateDataSources;
            ClearDataSources = ((RepeaterQuestion)resource).ClearDataSources;

            HideWhenNoData = ((RepeaterQuestion)resource).HideWhenNoData;
            ShowControls = ((RepeaterQuestion)resource).ShowControls;
            RepeaterContent = ((RepeaterQuestion)resource).RepeatContent;
        }

        public override void SetResourceValues(IResource resource)
        {
            base.SetResourceValues(resource);

            ((RepeaterQuestion)resource).DataSource = DataSource;
            ((RepeaterQuestion)resource).UpdateDataSources = UpdateDataSources;
            ((RepeaterQuestion)resource).ClearDataSources = ClearDataSources;

            ((RepeaterQuestion)resource).HideWhenNoData = HideWhenNoData;
            ((RepeaterQuestion)resource).ShowControls = ShowControls;
            ((RepeaterQuestion)resource).RepeatContent = RepeaterContent;
        }
    }

    public class TimelineQuestionEditorModel : QuestionResourceEditorModel
    {
        public string DataSource { get; set; }
        public string UpdateDataSources { get; set; }
        public string ClearDataSources { get; set; }

        public string DateFormat { get; set; }

        public bool FillHeight { get; set; }

        public int? BottomPaddingWhenFillingHeight { get; set; }

        public override void PopulateFromValueProvider(IValueProvider provider, IRepositorySectionManagerItem designerType)
        {
            base.PopulateFromValueProvider(provider, designerType);

            DataSource = provider.GetValue("datasource").AttemptedValue;
            UpdateDataSources = provider.GetValue("updatedatasources").AttemptedValue;
            ClearDataSources = provider.GetValue("cleardatasources").AttemptedValue;
            DateFormat = provider.GetValue("dateformat").AttemptedValue;

            FillHeight = ParseCheckboxField(provider, "fillheight");
            BottomPaddingWhenFillingHeight = ParseIntField(provider, "fillpadding");

        }

        public override void FromResource(IResource resource, IRepositorySectionManagerItem designerType)
        {
            base.FromResource(resource, designerType);

            DataSource = ((TimelineQuestion)resource).DataSource;
            UpdateDataSources = ((TimelineQuestion)resource).UpdateDataSources;
            ClearDataSources = ((TimelineQuestion)resource).ClearDataSources;
            DateFormat = ((TimelineQuestion)resource).DateFormat;

            FillHeight = ((TimelineQuestion)resource).FillHeight;
            BottomPaddingWhenFillingHeight = ((TimelineQuestion)resource).BottomPaddingWhenFillingHeight;
        }

        public override void SetResourceValues(IResource resource)
        {
            base.SetResourceValues(resource);

            ((TimelineQuestion)resource).DataSource = DataSource;
            ((TimelineQuestion)resource).UpdateDataSources = UpdateDataSources;
            ((TimelineQuestion)resource).ClearDataSources = ClearDataSources;
            ((TimelineQuestion)resource).DateFormat = DateFormat;
            ((TimelineQuestion)resource).FillHeight = FillHeight;
            ((TimelineQuestion)resource).BottomPaddingWhenFillingHeight = BottomPaddingWhenFillingHeight;
        }
    }


    public class PieChartQuestionEditorModel : QuestionResourceEditorModel
    {
    }

    public class LineGraphQuestionEditorModel : QuestionResourceEditorModel
    {
        public string XAxisName { get; set; }
        public string YAxisName { get; set; }
        public AxisUnits XAxisUnits { get; set; }

        public string XAxisMin { get; set; }
        public string XAxisMax { get; set; }
        public string YAxisMin { get; set; }
        public string YAxisMax { get; set; }

        public override void PopulateFromValueProvider(IValueProvider provider, IRepositorySectionManagerItem designerType)
        {
            base.PopulateFromValueProvider(provider, designerType);
            XAxisName = provider.GetValue("XAxisName").AttemptedValue;
            YAxisName = provider.GetValue("YAxisName").AttemptedValue;
            var xAxisUnitsString = provider.GetValue("XAxisUnits").AttemptedValue;
            XAxisUnits = (AxisUnits)Enum.Parse(typeof(AxisUnits), xAxisUnitsString);
        }

        public override void FromResource(IResource resource, IRepositorySectionManagerItem designerType)
        {
            base.FromResource(resource, designerType);
            var q = (LineChartQuestion)resource;
            XAxisName = q.XAxisName;
            YAxisName = q.YAxisName;
            XAxisUnits = q.XAxisUnits;
        }

        public override void SetResourceValues(IResource resource)
        {
            base.SetResourceValues(resource);
            var q = (LineChartQuestion)resource;
            q.XAxisName = XAxisName;
            q.YAxisName = YAxisName;
            q.XAxisUnits = XAxisUnits;
        }
    }

    public class LabelQuestionEditorModel : QuestionResourceEditorModel
    {
        [UIHint("MultilineText")]
        public IDictionary<string,string> LocalisableText { get; set; }
        public bool PopulateReporting { get; set; }
        public bool Listen { get; set; }
        public string ListenTo { get; set; }
        public bool HideUntilFirstListen { get; set; }

        public override void PopulateFromValueProvider(IValueProvider provider, IRepositorySectionManagerItem designerType)
        {
            base.PopulateFromValueProvider(provider, designerType);

            LocalisableText = ParseStringDictionary(provider, "localisabletext");
            PopulateReporting = ParseCheckboxField(provider, "populatereporting");
            Listen = ParseCheckboxField(provider, "listen");
            ListenTo = provider.GetValue("listento").AttemptedValue;
            HideUntilFirstListen = ParseCheckboxField(provider, "hideuntilfirstlisten");
        }

        public override void FromResource(IResource resource, IRepositorySectionManagerItem designerType)
        {
            base.FromResource(resource, designerType);

            LocalisableText = ((ILabelQuestion)resource).LocalisableText.ToDictionary();
            PopulateReporting = ((ILabelQuestion)resource).PopulateReporting;
            Listen = ((LabelQuestion)resource).Listen;
            ListenTo = ((LabelQuestion)resource).ListenTo;
            HideUntilFirstListen = ((LabelQuestion)resource).HideUntilFirstListen;
        }

        public override void SetResourceValues(IResource resource)
        {
            base.SetResourceValues(resource);

            ((ILabelQuestion)resource).LocalisableText.FromDictionary(LocalisableText);
            ((ILabelQuestion)resource).PopulateReporting = PopulateReporting;
            ((LabelQuestion)resource).Listen = Listen;
            ((LabelQuestion)resource).ListenTo = ListenTo;
            ((LabelQuestion)resource).HideUntilFirstListen = HideUntilFirstListen;
        }
    }

    public class DateTimeQuestionEditorModel : QuestionResourceEditorModel
    {
        public bool AllowTimeEntry { get; set; }

        public string UpdateDataSources { get; set; }

        public override void PopulateFromValueProvider(IValueProvider provider, IRepositorySectionManagerItem designerType)
        {
            base.PopulateFromValueProvider(provider, designerType);

            AllowTimeEntry = ParseCheckboxField(provider, "allowtimeentry");

            UpdateDataSources = provider.GetValue("updatedatasources").AttemptedValue;
        }

        public override void FromResource(IResource resource, IRepositorySectionManagerItem designerType)
        {
            base.FromResource(resource, designerType);

            AllowTimeEntry = ((IDateTimeQuestion)resource).AllowTimeEntry;
            UpdateDataSources = ((IDateTimeQuestion)resource).UpdateDataSources;
        }

        public override void SetResourceValues(IResource resource)
        {
            base.SetResourceValues(resource);

            ((IDateTimeQuestion)resource).AllowTimeEntry = AllowTimeEntry;
            ((IDateTimeQuestion)resource).UpdateDataSources = UpdateDataSources;
        }
    }

    public class MultiChoiceQuestionEditorModel : TemplatedQuestionResourceEditorModel
    {
        public bool AllowMultiSelect { get; set; }
        public int NumberOfChoicesAllowed { get; set; }
        public int NumberOfAnswersMustPass { get; set; }
        public MultiChoiceQuestionFormat Format { get; set; }
        public bool RandomizeChoices { get; set; }

        public override void PopulateFromValueProvider(IValueProvider provider, IRepositorySectionManagerItem designerType)
        {
            base.PopulateFromValueProvider(provider, designerType);

            AllowMultiSelect = ParseCheckboxField(provider, "question.AllowMultiSelect");
            NumberOfChoicesAllowed = int.Parse(provider.GetValue("NumberOfChoicesAllowed").AttemptedValue);
            NumberOfAnswersMustPass = int.Parse(provider.GetValue("NumberOfAnswersMustPass").AttemptedValue);
            Format = (MultiChoiceQuestionFormat)Enum.Parse(typeof(ConceptCave.Checklist.Interfaces.MultiChoiceQuestionFormat), provider.GetValue("format").AttemptedValue);
            RandomizeChoices = ParseCheckboxField(provider, "question.RandomizeChoices");
        }

        public override void FromResource(IResource resource, IRepositorySectionManagerItem designerType)
        {
            base.FromResource(resource, designerType);

            AllowMultiSelect = ((IMultiChoiceQuestion)resource).AllowMultiSelect;
            NumberOfChoicesAllowed = ((IMultiChoiceQuestion)resource).NumberOfChoicesAllowed;
            NumberOfAnswersMustPass = ((IMultiChoiceQuestion)resource).NumberOfAnswersMustPass;
            Format = ((IMultiChoiceQuestion)resource).Format;
            RandomizeChoices = ((IMultiChoiceQuestion)resource).RandomizeChoices;
        }

        public override void SetResourceValues(IResource resource)
        {
            base.SetResourceValues(resource);

            IMultiChoiceQuestion multiChoice = (IMultiChoiceQuestion)resource;
            multiChoice.AllowMultiSelect = AllowMultiSelect;
            multiChoice.NumberOfChoicesAllowed = NumberOfChoicesAllowed;
            multiChoice.NumberOfAnswersMustPass = NumberOfAnswersMustPass;
            multiChoice.Format = Format;
            multiChoice.RandomizeChoices = RandomizeChoices;
        }
    }

    public class GeoLocationQuestionEditorModel : QuestionResourceEditorModel
    {
        public bool LookUpAddress { get; set; }

        public override void PopulateFromValueProvider(IValueProvider provider, IRepositorySectionManagerItem designerType)
        {
            base.PopulateFromValueProvider(provider, designerType);

            LookUpAddress = ParseCheckboxField(provider, "lookupaddress");
        }

        public override void FromResource(IResource resource, IRepositorySectionManagerItem designerType)
        {
            base.FromResource(resource, designerType);

            LookUpAddress = ((IGeoLocationQuestion)resource).LookUpAddress;
        }

        public override void SetResourceValues(IResource resource)
        {
            base.SetResourceValues(resource);

            ((IGeoLocationQuestion)resource).LookUpAddress = LookUpAddress;
        }
    }

    public class SelectUserQuestionEditorModel : QuestionResourceEditorModel
    {
        private IList<Guid> AllowedLabels { get; set; }
        public string DataSource { get; set; }
        public string UpdateDataSources { get; set; }
        public string ClearDataSources { get; set; }

        public SelectUserQuestionEditorModel()
        {
            AllowedLabels = new List<Guid>();
        }

        public override void PopulateFromValueProvider(IValueProvider provider, IRepositorySectionManagerItem designerType)
        {
            base.PopulateFromValueProvider(provider, designerType);

            var providerResult = provider.GetValue("AllowedLabelIds");
            if (providerResult != null)
            {
                var stringArr = (string[])providerResult.RawValue;
                AllowedLabels = stringArr.Select(s => Guid.Parse(s)).ToList();
            }

            DataSource = provider.GetValue("datasource").AttemptedValue;
            UpdateDataSources = provider.GetValue("updatedatasources").AttemptedValue;
            ClearDataSources = provider.GetValue("cleardatasources").AttemptedValue;
        }

        public override void FromResource(IResource resource, IRepositorySectionManagerItem designerType)
        {
            base.FromResource(resource, designerType);
            AllowedLabels = ((SelectUserQuestion)resource).AllowedLabelIds;

            DataSource = ((SelectUserQuestion)resource).DataSource;
            UpdateDataSources = ((SelectUserQuestion)resource).UpdateDataSources;
            ClearDataSources = ((SelectUserQuestion)resource).ClearDataSources;
        }

        public override void SetResourceValues(IResource resource)
        {
            base.SetResourceValues(resource);

            ((SelectUserQuestion)resource).DataSource = DataSource;
            ((SelectUserQuestion)resource).UpdateDataSources = UpdateDataSources;
            ((SelectUserQuestion)resource).ClearDataSources = ClearDataSources;

            ((SelectUserQuestion)resource).AllowedLabelIds.Clear();
            foreach (var id in AllowedLabels)
                ((SelectUserQuestion)resource).AllowedLabelIds.Add(id);
        }
    }
    public class SelectODataQuestionEditorModel : QuestionResourceEditorModel
    {
        public string DataSource { get; set; }
        public string UpdateDataSources { get; set; }
        public string ClearDataSources { get; set; }

        public SelectODataQuestionEditorModel()
        {
        }

        public override void PopulateFromValueProvider(IValueProvider provider, IRepositorySectionManagerItem designerType)
        {
            base.PopulateFromValueProvider(provider, designerType);

            DataSource = provider.GetValue("datasource").AttemptedValue;
            UpdateDataSources = provider.GetValue("updatedatasources").AttemptedValue;
            ClearDataSources = provider.GetValue("cleardatasources").AttemptedValue;
        }

        public override void FromResource(IResource resource, IRepositorySectionManagerItem designerType)
        {
            base.FromResource(resource, designerType);
            
            DataSource = ((SelectODataQuestion)resource).DataSource;
            UpdateDataSources = ((SelectODataQuestion)resource).UpdateDataSources;
            ClearDataSources = ((SelectODataQuestion)resource).ClearDataSources;
        }

        public override void SetResourceValues(IResource resource)
        {
            base.SetResourceValues(resource);

            ((SelectODataQuestion)resource).DataSource = DataSource;
            ((SelectODataQuestion)resource).UpdateDataSources = UpdateDataSources;
            ((SelectODataQuestion)resource).ClearDataSources = ClearDataSources;

        }
    }

    public class SelectMediaQuestionEditorModel : QuestionResourceEditorModel
    { }

    public class SelectFacilityStructureQuestionEditorModel : QuestionResourceEditorModel
    {
        public bool IncludeNextButton { get; set; }
        public int? StartFromParentId { get; set; }

        public override void PopulateFromValueProvider(IValueProvider provider, IRepositorySectionManagerItem designerType)
        {
            base.PopulateFromValueProvider(provider, designerType);

            IncludeNextButton = ParseCheckboxField(provider, "includenextbutton");

            var startfrom = provider.GetValue("startfromparentid");
            if (startfrom != null)
            {
                var sf = startfrom.AttemptedValue;

                if(string.IsNullOrEmpty(sf) == false)
                {
                    int sfAttempt = 0;
                    if(int.TryParse(sf, out sfAttempt) == true)
                    {
                        StartFromParentId = sfAttempt;
                    }
                }
            }
        }

        public override void FromResource(IResource resource, IRepositorySectionManagerItem designerType)
        {
            base.FromResource(resource, designerType);

            IncludeNextButton = ((SelectFacilityStructureQuestion)resource).IncludeNextButton;
            StartFromParentId = ((SelectFacilityStructureQuestion)resource).StartFromParentId;
        }

        public override void SetResourceValues(IResource resource)
        {
            base.SetResourceValues(resource);

            ((SelectFacilityStructureQuestion)resource).IncludeNextButton = IncludeNextButton;
            ((SelectFacilityStructureQuestion)resource).StartFromParentId = StartFromParentId;
        }
    }

    public class SelectCompanyQuestionEditorModel : QuestionResourceEditorModel
    { }

    public class MediaDirectoryQuestionEditorModel : QuestionResourceEditorModel
    {
        public bool ShowPageViewings { get; set; }

        public override void PopulateFromValueProvider(IValueProvider provider, IRepositorySectionManagerItem designerType)
        {
            base.PopulateFromValueProvider(provider, designerType);

            ShowPageViewings = ParseCheckboxField(provider, "showpageviewings");
        }

        public override void FromResource(IResource resource, IRepositorySectionManagerItem designerType)
        {
            base.FromResource(resource, designerType);

            ShowPageViewings = ((MediaDirectoryQuestion)resource).ShowPageViewings;
        }

        public override void SetResourceValues(IResource resource)
        {
            base.SetResourceValues(resource);

            ((MediaDirectoryQuestion)resource).ShowPageViewings = ShowPageViewings;
        }
    }

    public class MediaViewingSummaryQuestionEditorModel : QuestionResourceEditorModel
    {
        public bool Listen { get; set; }
        public string ListenTo { get; set; }

        public override void PopulateFromValueProvider(IValueProvider provider, IRepositorySectionManagerItem designerType)
        {
            base.PopulateFromValueProvider(provider, designerType);

            Listen = ParseCheckboxField(provider, "listen");
            ListenTo = provider.GetValue("listento").AttemptedValue;
        }

        public override void FromResource(IResource resource, IRepositorySectionManagerItem designerType)
        {
            base.FromResource(resource, designerType);

            Listen = ((MediaViewingSummaryQuestion)resource).Listen;
            ListenTo = ((MediaViewingSummaryQuestion)resource).ListenTo;
        }

        public override void SetResourceValues(IResource resource)
        {
            base.SetResourceValues(resource);

            ((MediaViewingSummaryQuestion)resource).Listen = Listen;
            ((MediaViewingSummaryQuestion)resource).ListenTo = ListenTo;
        }
    }

    public class PresentWordDocumentQuestionEditorModel : QuestionResourceEditorModel
    {
        public bool AllowDownload { get; set; }
        public bool MergeWithChecklist { get; set; }
        public WordDocumentPreviewType PreviewType { get; set; }
        public Guid MediaId { get; set; }

        public bool UseUseHighQualityRendering { get; set; }
        public bool UseAntiAliasing { get; set; }
        public float Resolution { get; set; }
        public bool TrackPageViewing { get; set; }

        public override void PopulateFromValueProvider(IValueProvider provider, IRepositorySectionManagerItem designerType)
        {
            base.PopulateFromValueProvider(provider, designerType);

            AllowDownload = ParseCheckboxField(provider, "allowdownload");
            MergeWithChecklist = ParseCheckboxField(provider, "mergewithchecklist");
            PreviewType = (WordDocumentPreviewType)Enum.Parse(typeof(WordDocumentPreviewType), provider.GetValue("previewtype").AttemptedValue);
            MediaId = Guid.Parse(provider.GetValue("mediaid").AttemptedValue);

            UseUseHighQualityRendering = ParseCheckboxField(provider, "usehighquality");
            UseAntiAliasing = ParseCheckboxField(provider, "useantialiased");
            Resolution = float.Parse(provider.GetValue("resolution").AttemptedValue);

            TrackPageViewing = ParseCheckboxField(provider, "trackpageviewing");
        }

        public override void FromResource(IResource resource, IRepositorySectionManagerItem designerType)
        {
            base.FromResource(resource, designerType);

            AllowDownload = ((PresentWordDocumentQuestion)resource).AllowDownload;
            MergeWithChecklist = ((PresentWordDocumentQuestion)resource).MergeWithChecklist;
            PreviewType = ((PresentWordDocumentQuestion)resource).PreviewType;
            MediaId = ((PresentWordDocumentQuestion)resource).MediaId;

            UseUseHighQualityRendering = ((PresentWordDocumentQuestion)resource).UseUseHighQualityRendering;
            UseAntiAliasing = ((PresentWordDocumentQuestion)resource).UseAntiAliasing;
            Resolution = ((PresentWordDocumentQuestion)resource).Resolution;

            TrackPageViewing = ((PresentWordDocumentQuestion)resource).TrackPageViewing;
        }

        public override void SetResourceValues(IResource resource)
        {
            base.SetResourceValues(resource);

            ((PresentWordDocumentQuestion)resource).AllowDownload = AllowDownload;
            ((PresentWordDocumentQuestion)resource).MergeWithChecklist = MergeWithChecklist;
            ((PresentWordDocumentQuestion)resource).PreviewType = PreviewType;
            ((PresentWordDocumentQuestion)resource).MediaId = MediaId;

            ((PresentWordDocumentQuestion)resource).UseUseHighQualityRendering = UseUseHighQualityRendering;
            ((PresentWordDocumentQuestion)resource).UseAntiAliasing = UseAntiAliasing;
            ((PresentWordDocumentQuestion)resource).Resolution = Resolution;

            ((PresentWordDocumentQuestion)resource).TrackPageViewing = TrackPageViewing;
        }
    }

    public class PresentMediaQuestionEditorModel : QuestionResourceEditorModel
    {
        public string Url { get; set; }
        public PresentMediaQuestionType MediaType { get; set; }
        public IDimension Width { get; set; }
        public IDimension Height { get; set; }

        public YouTubePresentMediaQuestionSettings YouTubeSettings { get; set; }

        public override void PopulateFromValueProvider(IValueProvider provider, IRepositorySectionManagerItem designerType)
        {
            base.PopulateFromValueProvider(provider, designerType);

            Url = provider.GetValue("url").AttemptedValue;
            MediaType = (PresentMediaQuestionType)Enum.Parse(typeof(PresentMediaQuestionType), provider.GetValue("mediatype").AttemptedValue);

            Width = new Dimension();
            Width.Value = decimal.Parse(provider.GetValue("width.Value").AttemptedValue);
            Width.DimensionType = (DimensionType)Enum.Parse(typeof(DimensionType), provider.GetValue("width.DimensionType").AttemptedValue);

            Height = new Dimension();
            Height.Value = decimal.Parse(provider.GetValue("height.Value").AttemptedValue);
            Height.DimensionType = (DimensionType)Enum.Parse(typeof(DimensionType), provider.GetValue("height.DimensionType").AttemptedValue);

            switch(MediaType)
            {
                case PresentMediaQuestionType.YouTube:
                    YouTubeSettings = new YouTubePresentMediaQuestionSettings();

                    YouTubeSettings.AllowFullScreen = ParseCheckboxField(provider, "youtubeallowfullscreen");
                    YouTubeSettings.ModestBranding = ParseCheckboxField(provider, "youtubemodestbranding");
                    YouTubeSettings.ShowRelatedVideos = ParseCheckboxField(provider, "youtubeshowrelated");
                    YouTubeSettings.ShowInfo = ParseCheckboxField(provider, "youtubeshowinfo");
                    YouTubeSettings.ControlsType = (YouTubePresentedMediaQuestionSettingsControls)Enum.Parse(typeof(YouTubePresentedMediaQuestionSettingsControls), provider.GetValue("youtubecontrols").AttemptedValue);

                    break;
            }
        }

        public override void FromResource(IResource resource, IRepositorySectionManagerItem designerType)
        {
            base.FromResource(resource, designerType);

            Url = ((IPresentMediaQuestion)resource).Url;
            MediaType = ((IPresentMediaQuestion)resource).MediaType;
            Width = ((IPresentMediaQuestion)resource).Width;
            Height = ((IPresentMediaQuestion)resource).Height;

            switch (MediaType)
            {
                case PresentMediaQuestionType.YouTube:
                    YouTubeSettings = (YouTubePresentMediaQuestionSettings)((IPresentMediaQuestion)resource).Settings;
                    break;
            }
        }

        public override void SetResourceValues(IResource resource)
        {
            base.SetResourceValues(resource);

            ((IPresentMediaQuestion)resource).Url = Url;
            ((IPresentMediaQuestion)resource).MediaType = MediaType;
            ((IPresentMediaQuestion)resource).Width = Width;
            ((IPresentMediaQuestion)resource).Height = Height;

            switch (MediaType)
            {
                case PresentMediaQuestionType.YouTube:
                    ((IPresentMediaQuestion)resource).Settings = YouTubeSettings;
                    break;
            }
        }
    }

    public class PresentationQuestionEditorModel : QuestionResourceEditorModel
    {
        public string ManifestJson { get; set; }
        public string Module { get; set; }

        public string NextButtonText { get; set; }
        public string BackButtonText { get; set; }

        public bool TrackViewing { get; set; }

        public override void PopulateFromValueProvider(IValueProvider provider, IRepositorySectionManagerItem designerType)
        {
            base.PopulateFromValueProvider(provider, designerType);

            ManifestJson = provider.GetValue("jsonContent").AttemptedValue;
            Module = provider.GetValue("module").AttemptedValue;
            TrackViewing = ParseCheckboxField(provider, "trackviewing");
            NextButtonText = provider.GetValue("nextbuttontext").AttemptedValue;
            BackButtonText = provider.GetValue("backbuttontext").AttemptedValue;
        }

        public override void FromResource(IResource resource, IRepositorySectionManagerItem designerType)
        {
            base.FromResource(resource, designerType);

            ManifestJson = ((PresentationQuestion)resource).ManifestJson;
            Module = ((PresentationQuestion)resource).Module;
            TrackViewing = ((PresentationQuestion)resource).TrackViewing;
            NextButtonText = ((PresentationQuestion)resource).NextButtonText;
            BackButtonText = ((PresentationQuestion)resource).BackButtonText;
        }

        public override void SetResourceValues(IResource resource)
        {
            base.SetResourceValues(resource);

            ((PresentationQuestion)resource).ManifestJson = ManifestJson;
            ((PresentationQuestion)resource).Module = Module;
            ((PresentationQuestion)resource).TrackViewing = TrackViewing;
            ((PresentationQuestion)resource).NextButtonText = NextButtonText;
            ((PresentationQuestion)resource).BackButtonText = BackButtonText;
        }
    }

    public class UploadMediaQuestionEditorModel : QuestionResourceEditorModel
    {
        public int MaxUploadCount { get; set; }
        public int? MaxWidth { get; set; }
        public int? MaxHeight { get; set; }
        public bool KeepAspect { get; set; }
        public bool AllowImage { get; set; }
        public bool AllowVideo { get; set; }
        public bool AllowAudio { get; set; }
        public bool AllowAnyMediaType { get; set; }
        public UploadMediaQuestionDestination Destination { get; set; }

        public string UploadPrompt { get; set; }

        public override void PopulateFromValueProvider(IValueProvider provider, IRepositorySectionManagerItem designerType)
        {
            base.PopulateFromValueProvider(provider, designerType);

            MaxUploadCount = int.Parse(provider.GetValue("maxuploadcount").AttemptedValue);
            UploadPrompt = provider.GetValue("uploadprompt").AttemptedValue;
            MaxWidth = ParseIntField(provider, "maxwidth");
            MaxHeight = ParseIntField(provider, "maxheight");
            KeepAspect = ParseCheckboxField(provider, "keepaspect", false);

            AllowImage = ParseCheckboxField(provider, "allowimage", false);
            AllowVideo = ParseCheckboxField(provider, "allowvideo", false);
            AllowAudio = ParseCheckboxField(provider, "allowaudio", false);
            AllowAnyMediaType = ParseCheckboxField(provider, "allowanymedia", false);

            Destination = (UploadMediaQuestionDestination)Enum.Parse(typeof(UploadMediaQuestionDestination), provider.GetValue("destination").AttemptedValue);

        }

        public override void FromResource(IResource resource, IRepositorySectionManagerItem designerType)
        {
            base.FromResource(resource, designerType);

            MaxUploadCount = ((IUploadMediaQuestion)resource).MaxUploadCount;
            UploadPrompt = ((IUploadMediaQuestion)resource).UploadPrompt;
            MaxWidth = ((IUploadMediaQuestion)resource).MaxWidth;
            MaxHeight = ((IUploadMediaQuestion)resource).MaxHeight;
            KeepAspect = ((IUploadMediaQuestion)resource).KeepAspect;
            AllowImage = ((IUploadMediaQuestion)resource).AllowImage;
            AllowVideo = ((IUploadMediaQuestion)resource).AllowVideo;
            AllowAudio = ((IUploadMediaQuestion)resource).AllowAudio;
            AllowAnyMediaType = ((IUploadMediaQuestion)resource).AllowAnyMediaType;
            Destination = ((IUploadMediaQuestion)resource).Destination;
        }

        public override void SetResourceValues(IResource resource)
        {
            base.SetResourceValues(resource);

            ((IUploadMediaQuestion)resource).MaxUploadCount = MaxUploadCount;
            ((IUploadMediaQuestion)resource).UploadPrompt = UploadPrompt;
            ((IUploadMediaQuestion)resource).MaxWidth = MaxWidth;
            ((IUploadMediaQuestion)resource).MaxHeight = MaxHeight;
            ((IUploadMediaQuestion)resource).KeepAspect = KeepAspect;
            ((IUploadMediaQuestion)resource).AllowImage = AllowImage;
            ((IUploadMediaQuestion)resource).AllowVideo = AllowVideo;
            ((IUploadMediaQuestion)resource).AllowAudio = AllowAudio;
            ((IUploadMediaQuestion)resource).AllowAnyMediaType = AllowAnyMediaType;
            ((IUploadMediaQuestion)resource).Destination = Destination;
        }
    }

    public class PayPalQuestionEditorModel : QuestionResourceEditorModel
    {
        public bool UseSandbox { get; set; }
        public string DevelopmentUrl { get; set; }
        public decimal Price { get; set; }
        public decimal? TaxRate { get; set; }
        public string MerchantId { get; set; }
        public PayPalCurrency Currency { get; set; }

        public override void PopulateFromValueProvider(IValueProvider provider, IRepositorySectionManagerItem designerType)
        {
            base.PopulateFromValueProvider(provider, designerType);

            UseSandbox = ParseCheckboxField(provider, "usesandbox");
            DevelopmentUrl = provider.GetValue("developmenturl").AttemptedValue;
            Price = decimal.Parse(provider.GetValue("price").AttemptedValue);
            if (string.IsNullOrEmpty(provider.GetValue("taxrate").AttemptedValue) == false)
            {
                TaxRate = decimal.Parse(provider.GetValue("taxrate").AttemptedValue);
            }
            MerchantId = provider.GetValue("merchantid").AttemptedValue;
            Currency = (PayPalCurrency)Enum.Parse(typeof(PayPalCurrency), provider.GetValue("currency").AttemptedValue);
        }

        public override void FromResource(IResource resource, IRepositorySectionManagerItem designerType)
        {
            base.FromResource(resource, designerType);

            UseSandbox = ((IPayPalQuestion)resource).UseSandbox;
            DevelopmentUrl = ((IPayPalQuestion)resource).DevelopmentUrl;
            Price = ((IPayPalQuestion)resource).Price;
            TaxRate = ((IPayPalQuestion)resource).TaxRate;
            MerchantId = ((IPayPalQuestion)resource).MerchantId;
            Currency = ((IPayPalQuestion)resource).Currency;
        }

        public override void SetResourceValues(IResource resource)
        {
            base.SetResourceValues(resource);

            ((IPayPalQuestion)resource).UseSandbox = UseSandbox;
            ((IPayPalQuestion)resource).DevelopmentUrl = DevelopmentUrl;
            ((IPayPalQuestion)resource).Price = Price;
            ((IPayPalQuestion)resource).TaxRate = TaxRate;
            ((IPayPalQuestion)resource).MerchantId = MerchantId;
            ((IPayPalQuestion)resource).Currency = Currency;
        }
    }

    public class ProgressIndicatorQuestionEditorModel : QuestionResourceEditorModel
    {
        public decimal MaxValue { get; set; }
        public decimal MinValue { get; set; }
        public decimal Value { get; set; }

        public override void PopulateFromValueProvider(IValueProvider provider, IRepositorySectionManagerItem designerType)
        {
            base.PopulateFromValueProvider(provider, designerType);

            MaxValue = decimal.Parse(provider.GetValue("maxvalue").AttemptedValue);
            MinValue = decimal.Parse(provider.GetValue("minvalue").AttemptedValue);
            Value = decimal.Parse(provider.GetValue("value").AttemptedValue);
        }

        public override void FromResource(IResource resource, IRepositorySectionManagerItem designerType)
        {
            base.FromResource(resource, designerType);

            MaxValue = ((IProgressIndicatorQuestion)resource).MaxValue;
            MinValue = ((IProgressIndicatorQuestion)resource).MinValue;
            Value = ((IProgressIndicatorQuestion)resource).Value;
        }

        public override void SetResourceValues(IResource resource)
        {
            base.SetResourceValues(resource);

            ((IProgressIndicatorQuestion)resource).MaxValue = MaxValue;
            ((IProgressIndicatorQuestion)resource).MinValue = MinValue;
            ((IProgressIndicatorQuestion)resource).Value = Value;
        }
    }

    //public class MessagingViewQuestionEditorModel : QuestionResourceEditorModel 
    //{
    //    public static IList<Guid> UsersToCC = new List<Guid>();

    //    public override void PopulateFromValueProvider(IValueProvider provider, IRepositorySectionManagerItem designerType)
    //    {
    //        base.PopulateFromValueProvider(provider, designerType);

    //        var providerResult = provider.GetValue("UsersToCC");
    //        if (providerResult != null)
    //        {
    //            var stringArr = (string[])providerResult.RawValue;
    //            UsersToCC = stringArr.Select(s => Guid.Parse(s)).ToList();
    //        }
    //    }

    //    public override void FromResource(IResource resource, IRepositorySectionManagerItem designerType)
    //    {
    //        base.FromResource(resource, designerType);
    //        UsersToCC = ((MessagingViewQuestion)resource).UsersToCC;
    //    }

    //    public override void SetResourceValues(IResource resource)
    //    {
    //        base.SetResourceValues(resource);
    //        ((MessagingViewQuestion)resource).UsersToCC = UsersToCC;
    //    }
    //}

    public class VariableDataQuestionEditorModel : QuestionResourceEditorModel
    { }

    public class SectionEditorModel : ResourceEditorModel
    {
        public static string PromptFieldName = "prompt";
        public static string AlignmentFieldName = "alignment";
        public static string ClearFieldName = "clear";
        public static string CustomClassFieldName = "class";
        public static string CustomStylingFieldName = "customstyle";
        public static string VisibleFieldName = "visible";
        public static string LayoutStylingFieldName = "layout";
        public static string PresentAsFinalFieldName = "presentasfinal";

        public IDictionary<string,string> LocalisablePrompt { get; set; }
        public bool Visible { get; set; }
        public HtmlPresentationHintAlignment Alignment { get; set; }
        public HtmlPresentationHintClear Clear { get; set; }
        public string CustomStyling { get; set; }
        public string CustomClass { get; set; }
        public HtmlSectionLayout Layout { get; set; }
        public bool AllowFilterByCategory { get; set; }
        public string DataSource { get; set; }
        public string NgController { get; set; }
        public bool PresentAsFinal { get; set; }
        public bool EnableDeepEdit { get; set; }

        public override void PopulateFromValueProvider(IValueProvider provider, IRepositorySectionManagerItem designerType)
        {
            base.PopulateFromValueProvider(provider, designerType);

            LocalisablePrompt = ParseStringDictionary(provider, "LocalisablePrompt");
            Alignment = (HtmlPresentationHintAlignment)Enum.Parse(typeof(HtmlPresentationHintAlignment), provider.GetValue(AlignmentFieldName).AttemptedValue);
            Clear = (HtmlPresentationHintClear)Enum.Parse(typeof(HtmlPresentationHintClear), provider.GetValue(ClearFieldName).AttemptedValue);
            CustomStyling = provider.GetValue(CustomStylingFieldName).AttemptedValue;
            CustomClass = provider.GetValue(CustomClassFieldName).AttemptedValue;
            Layout = (HtmlSectionLayout)Enum.Parse(typeof(HtmlSectionLayout), provider.GetValue(LayoutStylingFieldName).AttemptedValue);
            Visible = ParseCheckboxField(provider, VisibleFieldName);
            AllowFilterByCategory = ParseCheckboxField(provider, "allowfilterbycategory");
            DataSource = provider.GetValue("DataSource").AttemptedValue;
            PresentAsFinal = ParseCheckboxField(provider, PresentAsFinalFieldName);

            var controller = provider.GetValue("NgController");
            if(controller != null)
            {
                NgController = controller.AttemptedValue;
            }
            EnableDeepEdit = ParseCheckboxField(provider, "enabledeepedit");
        }

        public override void FromResource(IResource resource, IRepositorySectionManagerItem designerType)
        {
            base.FromResource(resource, designerType);

            LocalisablePrompt = ((ISection)resource).LocalisablePrompt.ToDictionary();
            Alignment = ((IHtmlPresentationHint)resource).Alignment;
            Clear = ((IHtmlPresentationHint)resource).Clear;
            CustomStyling = ((IHtmlPresentationHint)resource).CustomStyling;
            CustomClass = ((IHtmlPresentationHint)resource).CustomClass;
            Layout = ((IHtmlPresentationHint)resource).Layout;
            Visible = ((ISection)resource).ClientModel.Visible;
            AllowFilterByCategory = ((ISection)resource).AllowFilterByCategory;
            DataSource = ((ISection)resource).DataSource;
            PresentAsFinal = ((ISection)resource).PresentAsFinal;
            NgController = ((ISection)resource).NgController;
            EnableDeepEdit = ((ISection)resource).EnableDeepEdit;

        }

        public override void SetResourceValues(IResource resource)
        {
            base.SetResourceValues(resource);

            ISection sec = (ISection)resource;
            sec.LocalisablePrompt.FromDictionary(LocalisablePrompt);
            sec.HtmlPresentation.Alignment = Alignment;
            sec.HtmlPresentation.Clear = Clear;
            sec.HtmlPresentation.CustomStyling = CustomStyling;
            sec.HtmlPresentation.CustomClass = CustomClass;
            sec.HtmlPresentation.Layout = Layout;
            sec.ClientModel.Visible = Visible;
            sec.AllowFilterByCategory = AllowFilterByCategory;
            sec.DataSource = DataSource;
            ((Section)sec).PresentAsFinal = PresentAsFinal;
            sec.NgController = NgController;
            sec.EnableDeepEdit = EnableDeepEdit;
        }
    }

    public class EmailTriggerActionModel : ResourceEditorModel { }

    public class WordDocumentActionModel : ResourceEditorModel { }

    public class eWayPaymentActionModel : ResourceEditorModel
    {
        public bool UseSandbox { get; set; }

        public override void PopulateFromValueProvider(IValueProvider provider, IRepositorySectionManagerItem designerType)
        {
            base.PopulateFromValueProvider(provider, designerType);

            UseSandbox = ParseCheckboxField(provider, "useSandbox");
        }

        public override void FromResource(IResource resource, IRepositorySectionManagerItem designerType)
        {
            base.FromResource(resource, designerType);

            UseSandbox = ((eWayPaymentFacility)resource).UseSandbox;
        }

        public override void SetResourceValues(IResource resource)
        {
            base.SetResourceValues(resource);

            eWayPaymentFacility action = (eWayPaymentFacility)resource;
            action.UseSandbox = UseSandbox;
        }
    }

    public class PresentChecklistActionModel : ResourceEditorModel
    {
        public string PublishingGroupName { get; set; }
        public string ChecklistName { get; set; }
        public string ReviewerName { get; set; }
        public bool ReferenceParentWorkingDocument { get; set; }

        public override void PopulateFromValueProvider(IValueProvider provider, IRepositorySectionManagerItem designerType)
        {
            base.PopulateFromValueProvider(provider, designerType);

            PublishingGroupName = provider.GetValue("publishingGroupName").AttemptedValue;
            ChecklistName = provider.GetValue("checklistName").AttemptedValue;
            ReviewerName = provider.GetValue("reviewer").AttemptedValue;
            ReferenceParentWorkingDocument = ParseCheckboxField(provider, "referenceParent");
        }

        public override void FromResource(IResource resource, IRepositorySectionManagerItem designerType)
        {
            base.FromResource(resource, designerType);

            PublishingGroupName = ((ConceptCave.BusinessLogic.Facilities.PresentChecklistFacility)resource).PublishingGroupName;
            ChecklistName = ((ConceptCave.BusinessLogic.Facilities.PresentChecklistFacility)resource).ChecklistName;
            ReviewerName = ((ConceptCave.BusinessLogic.Facilities.PresentChecklistFacility)resource).ReviewerName;
            ReferenceParentWorkingDocument = ((ConceptCave.BusinessLogic.Facilities.PresentChecklistFacility)resource).ReferenceParentWorkingDocument;
        }

        public override void SetResourceValues(IResource resource)
        {
            base.SetResourceValues(resource);

            ConceptCave.BusinessLogic.Facilities.PresentChecklistFacility action = (ConceptCave.BusinessLogic.Facilities.PresentChecklistFacility)resource;
            action.PublishingGroupName = PublishingGroupName;
            action.ChecklistName = ChecklistName;
            action.ReviewerName = ReviewerName;
            action.ReferenceParentWorkingDocument = ReferenceParentWorkingDocument;
        }
    }

    public class SignatureQuestionEditorModel : QuestionResourceEditorModel
    {
        public bool AllowTypedEntry { get; set; }
        public IDimension Width { get; set; }
        public IDimension Height { get; set; }
        public bool TakePictureWhenSigning { get; set; }
        public bool ShowTakenPicture { get; set; }

        public override void PopulateFromValueProvider(IValueProvider provider, IRepositorySectionManagerItem designerType)
        {
            base.PopulateFromValueProvider(provider, designerType);

            AllowTypedEntry = ParseCheckboxField(provider, "allowtypedentry");

            Width = new Dimension();
            Width.Value = decimal.Parse(provider.GetValue("width.Value").AttemptedValue);
            Width.DimensionType = (DimensionType)Enum.Parse(typeof(DimensionType), provider.GetValue("width.DimensionType").AttemptedValue);

            Height = new Dimension();
            Height.Value = decimal.Parse(provider.GetValue("height.Value").AttemptedValue);
            Height.DimensionType = (DimensionType)Enum.Parse(typeof(DimensionType), provider.GetValue("height.DimensionType").AttemptedValue);

            TakePictureWhenSigning = ParseCheckboxField(provider, "takepicture");
            ShowTakenPicture = ParseCheckboxField(provider, "showpicture");
        }

        public override void FromResource(IResource resource, IRepositorySectionManagerItem designerType)
        {
            base.FromResource(resource, designerType);

            AllowTypedEntry = ((ISignatureQuestion)resource).AllowTypedEntry;
            Width = ((ISignatureQuestion)resource).Width;
            Height = ((ISignatureQuestion)resource).Height;

            TakePictureWhenSigning = ((ISignatureQuestion)resource).TakePictureWhenSigning;
            ShowTakenPicture = ((ISignatureQuestion)resource).ShowTakenPicture;
        }

        public override void SetResourceValues(IResource resource)
        {
            base.SetResourceValues(resource);

            ((ISignatureQuestion)resource).AllowTypedEntry = AllowTypedEntry;
            ((ISignatureQuestion)resource).Width = Width;
            ((ISignatureQuestion)resource).Height = Height;

            ((ISignatureQuestion)resource).TakePictureWhenSigning = TakePictureWhenSigning;
            ((ISignatureQuestion)resource).ShowTakenPicture = ShowTakenPicture;
        }
    }

    public class ListNewsFeedQuestionEditorModel : QuestionResourceEditorModel
    {
        public NewsFeedDisplayAs ViewAs { get; set; }
        public int ItemCount { get; set; }
        public string Channels { get; set; }

        public override void PopulateFromValueProvider(IValueProvider provider, IRepositorySectionManagerItem designerType)
        {
            base.PopulateFromValueProvider(provider, designerType);

            ViewAs = (NewsFeedDisplayAs)Enum.Parse(typeof(NewsFeedDisplayAs), provider.GetValue("viewas").AttemptedValue);
            ItemCount = int.Parse(provider.GetValue("itemcount").AttemptedValue);
            Channels = provider.GetValue("channels").AttemptedValue;
        }

        public override void FromResource(IResource resource, IRepositorySectionManagerItem designerType)
        {
            base.FromResource(resource, designerType);

            ViewAs = ((ListNewsFeedQuestion)resource).ViewAs;
            ItemCount = ((ListNewsFeedQuestion)resource).ItemCount;
            Channels = ((ListNewsFeedQuestion)resource).Channels;
        }

        public override void SetResourceValues(IResource resource)
        {
            base.SetResourceValues(resource);

            ((ListNewsFeedQuestion)resource).ViewAs = ViewAs;
            ((ListNewsFeedQuestion)resource).ItemCount = ItemCount;
            ((ListNewsFeedQuestion)resource).Channels = Channels;
        }
    }

    public class TaskListQuestionEditorModel : QuestionResourceEditorModel
    {
        public bool EnforceSingleClockin { get; set; }
        public TaskListSortType SortType { get; set; }
        public int SortThrottleSeconds { get; set; }
        public MeasurementType MeasurementType { get; set; }
        public ISet<Guid> FilterByLabelids { get; set; }

        public ISet<Guid> FilterByStatusIds { get; set; }

        public bool AllowTaskGrouping { get; set; }

        public string TaskCreationTemplatesJson { get; set; }

        public bool ShowGeneralNotesForIncompleteTasks { get; set; }

        public int RefreshPeriodSeconds { get;set;}
        public int StalePeriodSeconds { get; set; }
        public bool Listen { get; set; }


        public bool SupportOffline { get; set; }

        public string TasksAddedMessage { get; set; }
        public string TasksRemovedMessage { get; set; }

        public bool ShowTaskSites { get; set; }
        public bool ShowTaskDistances { get;set;}

        public bool ShowStartTimes { get; set; }

        public string MinimumTaskDurationMessage { get; set; }

        public bool AllowClaimTasksFromParent { get; set; }
        public bool AllowButtonStart { get; set; }
        public bool AllowQRCodeStart { get; set; }
        public bool AllowBeaconStart { get; set; }
        public bool AllowButtonFinish { get; set; }
        public bool AllowQRCodeFinish { get; set; }

        public bool PromptLateTasks { get; set; }

        public override void PopulateFromValueProvider(IValueProvider provider, IRepositorySectionManagerItem designerType)
        {
            base.PopulateFromValueProvider(provider, designerType);

            EnforceSingleClockin = ParseCheckboxField(provider, "singleclockin");
            SortType = (TaskListSortType)Enum.Parse(typeof(TaskListSortType), provider.GetValue("sorttype").AttemptedValue);
            SortThrottleSeconds = int.Parse(provider.GetValue("sortthrottleseconds").AttemptedValue);
            MeasurementType = (MeasurementType)Enum.Parse(typeof(MeasurementType), provider.GetValue("measurementtype").AttemptedValue);
            FilterByLabelids = new HashSet<Guid>();
            FilterByStatusIds = new HashSet<Guid>();

            var labelprovider = provider.GetValue("filterbylabelids");
            if (labelprovider != null)
            {
                var labelstrings = labelprovider.AttemptedValue.Split(',');
                foreach (var str in labelstrings)
                    FilterByLabelids.Add(Guid.Parse(str));
            }

            var statusProvider = provider.GetValue("filterbystatusids");
            if(statusProvider != null)
            {
                var statusstrings = statusProvider.AttemptedValue.Split(',');
                foreach (var str in statusstrings)
                    FilterByStatusIds.Add(Guid.Parse(str));
            }

            Listen = ParseCheckboxField(provider, "listen");

            SupportOffline = ParseCheckboxField(provider, "supportoffline");

            RefreshPeriodSeconds = int.Parse(provider.GetValue("refreshperiodseconds").AttemptedValue);
            StalePeriodSeconds = int.Parse(provider.GetValue("staleperiodseconds").AttemptedValue);
            AllowTaskGrouping = ParseCheckboxField(provider, "allowtaskgrouping");

            TaskCreationTemplatesJson = provider.GetValue("taskcreationtemplates").AttemptedValue;

            ShowGeneralNotesForIncompleteTasks = ParseCheckboxField(provider, "showgeneralnotesforincompletetasks");

            TasksAddedMessage = provider.GetValue("tasksaddedmessage").AttemptedValue;
            TasksRemovedMessage = provider.GetValue("tasksremovedmessage").AttemptedValue;

            ShowTaskSites = ParseCheckboxField(provider, "showtasksites");
            ShowTaskDistances = ParseCheckboxField(provider, "showtaskdistances");
            ShowStartTimes = ParseCheckboxField(provider, "showstarttimes");

            MinimumTaskDurationMessage = provider.GetValue("minimumtaskdurationmessage").AttemptedValue;
            AllowClaimTasksFromParent = ParseCheckboxField(provider, "includeparentstasks");

            AllowButtonStart = ParseCheckboxField(provider, "allowbuttonstart");
            AllowQRCodeStart = ParseCheckboxField(provider, "allowqrcodestart");
            AllowBeaconStart = ParseCheckboxField(provider, "allowbeaconstart");
            AllowButtonFinish = ParseCheckboxField(provider, "allowbuttonfinish");
            AllowQRCodeFinish = ParseCheckboxField(provider, "allowqrcodefinish");

            PromptLateTasks = ParseCheckboxField(provider, "promptlatetasks");
        }

        public override void FromResource(IResource resource, IRepositorySectionManagerItem designerType)
        {
            base.FromResource(resource, designerType);

            EnforceSingleClockin = ((TaskListQuestion)resource).EnforceSingleClockin;
            SortType  = ((TaskListQuestion)resource).SortType;
            SortThrottleSeconds = ((TaskListQuestion)resource).SortThrottleSeconds;
            MeasurementType = ((TaskListQuestion)resource).MeasurementType;
            FilterByLabelids = ((TaskListQuestion)resource).FilterByLabelIds;
            FilterByStatusIds = ((TaskListQuestion)resource).FilterByStatusIds;
            AllowTaskGrouping = ((TaskListQuestion)resource).AllowTaskGrouping;
            TaskCreationTemplatesJson = ((TaskListQuestion)resource).TaskCreationTemplatesToJson().ToString();
            ShowGeneralNotesForIncompleteTasks = ((TaskListQuestion)resource).ShowGeneralNotesForIncompleteTasks;
            Listen  = ((TaskListQuestion)resource).Listen;
            SupportOffline = ((TaskListQuestion)resource).SupportOffline;
            RefreshPeriodSeconds = ((TaskListQuestion)resource).RefreshPeriodSeconds;
            StalePeriodSeconds = ((TaskListQuestion)resource).StalePeriodSeconds;
            TasksAddedMessage = ((TaskListQuestion)resource).TasksAddedMessage;
            TasksRemovedMessage = ((TaskListQuestion)resource).TasksRemovedMessage;
            ShowTaskSites = ((TaskListQuestion)resource).ShowTaskSites;
            ShowTaskDistances = ((TaskListQuestion)resource).ShowTaskDistances;
            ShowStartTimes = ((TaskListQuestion)resource).ShowStartTimes;
            MinimumTaskDurationMessage = ((TaskListQuestion)resource).MinimumTaskDurationMessage;
            AllowClaimTasksFromParent = ((TaskListQuestion)resource).AllowClaimTasksFromParent;
            AllowButtonStart = ((TaskListQuestion)resource).AllowButtonStart;
            AllowQRCodeStart = ((TaskListQuestion)resource).AllowQRCodeStart;
            AllowBeaconStart = ((TaskListQuestion)resource).AllowBeaconStart;
            AllowButtonFinish = ((TaskListQuestion)resource).AllowButtonFinish;
            AllowQRCodeFinish = ((TaskListQuestion)resource).AllowQRCodeFinish;
            PromptLateTasks = ((TaskListQuestion)resource).PromptLateTasks;
        }

        public override void SetResourceValues(IResource resource)
        {
            base.SetResourceValues(resource);

            ((TaskListQuestion)resource).EnforceSingleClockin = EnforceSingleClockin;
            ((TaskListQuestion)resource).SortType = SortType;
            ((TaskListQuestion)resource).SortThrottleSeconds = SortThrottleSeconds;
            ((TaskListQuestion)resource).MeasurementType = MeasurementType;

            ((TaskListQuestion)resource).FilterByLabelIds.Clear();
            FilterByLabelids.ToList().ForEach(l => ((TaskListQuestion)resource).FilterByLabelIds.Add(l));

            ((TaskListQuestion)resource).FilterByStatusIds.Clear();
            FilterByStatusIds.ToList().ForEach(l => ((TaskListQuestion)resource).FilterByStatusIds.Add(l));

            ((TaskListQuestion)resource).AllowTaskGrouping = AllowTaskGrouping;
            ((TaskListQuestion)resource).TaskCreationTemplatesFromJson(TaskCreationTemplatesJson);

            ((TaskListQuestion)resource).ShowGeneralNotesForIncompleteTasks = ShowGeneralNotesForIncompleteTasks;

            ((TaskListQuestion)resource).Listen = Listen;
            ((TaskListQuestion)resource).SupportOffline = SupportOffline;
            ((TaskListQuestion)resource).RefreshPeriodSeconds = RefreshPeriodSeconds;
            ((TaskListQuestion)resource).StalePeriodSeconds = StalePeriodSeconds;
            ((TaskListQuestion)resource).TasksAddedMessage = TasksAddedMessage;
            ((TaskListQuestion)resource).TasksRemovedMessage = TasksRemovedMessage;

            ((TaskListQuestion)resource).ShowTaskSites = ShowTaskSites;
            ((TaskListQuestion)resource).ShowTaskDistances = ShowTaskDistances;
            ((TaskListQuestion)resource).ShowStartTimes = ShowStartTimes;

            ((TaskListQuestion)resource).MinimumTaskDurationMessage = MinimumTaskDurationMessage;
            ((TaskListQuestion)resource).AllowClaimTasksFromParent = AllowClaimTasksFromParent;

            ((TaskListQuestion)resource).AllowButtonStart = AllowButtonStart;
            ((TaskListQuestion)resource).AllowQRCodeStart = AllowQRCodeStart;
            ((TaskListQuestion)resource).AllowBeaconStart = AllowBeaconStart;
            ((TaskListQuestion)resource).AllowButtonFinish = AllowButtonFinish;
            ((TaskListQuestion)resource).AllowQRCodeFinish = AllowQRCodeFinish;
            ((TaskListQuestion)resource).PromptLateTasks = PromptLateTasks;

        }
    }

    public class ClaimHierarchyBucketTasksQuestionEditorModel : QuestionResourceEditorModel
    {
        public bool HideWhenTasksPresent { get; set; }

        public bool HideShowTaskList { get; set; }

        public bool UseCompactUI { get; set; }

        public bool AllowAssignmentToOtherUsers { get; set; }
        public string ListenTo { get; set; }
        public int MaximumUsersPerLetterBucket { get; set; }

        public override void PopulateFromValueProvider(IValueProvider provider, IRepositorySectionManagerItem designerType)
        {
            base.PopulateFromValueProvider(provider, designerType);

            HideWhenTasksPresent = ParseCheckboxField(provider, "hidewhentaskspresent");
            HideShowTaskList = ParseCheckboxField(provider, "hideshowtasklist");
            UseCompactUI = ParseCheckboxField(provider, "usecompactui");
            AllowAssignmentToOtherUsers = ParseCheckboxField(provider, "allowassignmenttootherusers");
            ListenTo = provider.GetValue("listento").AttemptedValue;
            MaximumUsersPerLetterBucket = ParseIntField(provider, nameof(MaximumUsersPerLetterBucket)) ?? ClaimHierarchyBucketTasksQuestion.DefaultMaximumUsersPerLetterBucket;
        }

        public override void FromResource(IResource resource, IRepositorySectionManagerItem designerType)
        {
            base.FromResource(resource, designerType);

            HideWhenTasksPresent = ((ClaimHierarchyBucketTasksQuestion)resource).HideWhenTasksPresent;
            HideShowTaskList = ((ClaimHierarchyBucketTasksQuestion)resource).HideShowTaskList;
            UseCompactUI = ((ClaimHierarchyBucketTasksQuestion)resource).UseCompactUI;
            AllowAssignmentToOtherUsers = ((ClaimHierarchyBucketTasksQuestion)resource).AllowAssignmentToOtherUsers;
            ListenTo = ((ClaimHierarchyBucketTasksQuestion)resource).ListenTo;
            MaximumUsersPerLetterBucket = ((ClaimHierarchyBucketTasksQuestion)resource).MaximumUsersPerLetterBucket;
        }

        public override void SetResourceValues(IResource resource)
        {
            base.SetResourceValues(resource);

            ((ClaimHierarchyBucketTasksQuestion)resource).HideWhenTasksPresent = HideWhenTasksPresent;
            ((ClaimHierarchyBucketTasksQuestion)resource).HideShowTaskList = HideShowTaskList;
            ((ClaimHierarchyBucketTasksQuestion)resource).UseCompactUI = UseCompactUI;
            ((ClaimHierarchyBucketTasksQuestion)resource).AllowAssignmentToOtherUsers = AllowAssignmentToOtherUsers;
            ((ClaimHierarchyBucketTasksQuestion)resource).ListenTo = ListenTo;
            ((ClaimHierarchyBucketTasksQuestion)resource).MaximumUsersPerLetterBucket = MaximumUsersPerLetterBucket;
        }
    }

    public class OrderQuestionEditorModel : QuestionResourceEditorModel
    {
        public string Catalogs { get; set; }

        public OrderingUI UI { get; set; }

        public bool AutoCreateOrderOnFinish { get; set; }
        public bool ShowGrandTotal { get; set; }
        public bool ShowSubTotals { get; set; }
        public bool ShowCatalogDescriptions { get; set; }

        public string ProductTitleText { get; set; }
        public string PriceTitleText { get; set; }
        public string MinStockTitleText { get; set; }
        public string StockTitleText { get; set; }
        public string QuantityTitleText { get; set; }
        public string LineItemTotalText { get; set; }

        public string LineItemNotesTitleText { get; set; }
        public string CatalogTotalText { get; set; }
        public string GrandTotalText { get; set; }

        public string LengthByWidthLengthText { get; set; }
        public string LengthByWidthWidthText { get; set; }
        public string LengthByWidthAreaText { get; set; }

        public string AddRemoveManualEntryPrompt { get; set; }
        public string AddRemoveManualEntryNotes { get; set; }

        public override void PopulateFromValueProvider(IValueProvider provider, IRepositorySectionManagerItem designerType)
        {
            base.PopulateFromValueProvider(provider, designerType);

            Catalogs = provider.GetValue("catalogs").AttemptedValue;
            ShowGrandTotal = ParseCheckboxField(provider, "showgrandtotal");
            ShowSubTotals = ParseCheckboxField(provider, "showsubtotals");
            ShowCatalogDescriptions = ParseCheckboxField(provider, "showcatalogdescriptions");

            AutoCreateOrderOnFinish = ParseCheckboxField(provider, "autocreateorder");

            ProductTitleText = provider.GetValue("producttitletext").AttemptedValue;
            PriceTitleText = provider.GetValue("pricetitletext").AttemptedValue;
            MinStockTitleText = provider.GetValue("minstocktitletext").AttemptedValue;
            StockTitleText = provider.GetValue("stocktitletext").AttemptedValue;
            QuantityTitleText = provider.GetValue("quantitytitletext").AttemptedValue;
            LineItemTotalText = provider.GetValue("lineitemtotaltitletext").AttemptedValue;
            CatalogTotalText = provider.GetValue("catalogtotaltitletext").AttemptedValue;
            GrandTotalText = provider.GetValue("grandtotaltitletext").AttemptedValue;
            LineItemNotesTitleText = provider.GetValue("lineitemnotestitletext").AttemptedValue;

            LengthByWidthLengthText = provider.GetValue("lengthbywidthlengthtext").AttemptedValue;
            LengthByWidthWidthText = provider.GetValue("lengthbywidthwidthtext").AttemptedValue;
            LengthByWidthAreaText = provider.GetValue("lengthbywidthareatext").AttemptedValue;

            AddRemoveManualEntryPrompt = provider.GetValue("addremovemanualentryprompt").AttemptedValue;
            AddRemoveManualEntryNotes = provider.GetValue("addremovemanualentrynotes").AttemptedValue;

            UI = (OrderingUI)Enum.Parse(typeof(OrderingUI), provider.GetValue("ui").AttemptedValue);

        }

        public override void FromResource(IResource resource, IRepositorySectionManagerItem designerType)
        {
            base.FromResource(resource, designerType);

            Catalogs = ((OrderQuestion)resource).Catalogs;
            ShowGrandTotal = ((OrderQuestion)resource).ShowGrandTotal;
            ShowSubTotals = ((OrderQuestion)resource).ShowSubTotals;
            ShowCatalogDescriptions = ((OrderQuestion)resource).ShowCatalogDescriptions;

            AutoCreateOrderOnFinish = ((OrderQuestion)resource).AutoCreateOrderOnFinish;

            ProductTitleText = ((OrderQuestion)resource).ProductTitleText;
            PriceTitleText = ((OrderQuestion)resource).PriceTitleText;
            MinStockTitleText = ((OrderQuestion)resource).MinStockTitleText;
            StockTitleText = ((OrderQuestion)resource).StockTitleText;
            QuantityTitleText = ((OrderQuestion)resource).QuantityTitleText;
            LineItemTotalText = ((OrderQuestion)resource).LineItemTotalText;
            CatalogTotalText = ((OrderQuestion)resource).CatalogTotalText;
            GrandTotalText = ((OrderQuestion)resource).GrandTotalText;
            LineItemNotesTitleText = ((OrderQuestion)resource).LineItemNotesTitleText;

            LengthByWidthLengthText = ((OrderQuestion)resource).LengthByWidthLengthText;
            LengthByWidthWidthText = ((OrderQuestion)resource).LengthByWidthWidthText;
            LengthByWidthAreaText = ((OrderQuestion)resource).LengthByWidthAreaText;

            AddRemoveManualEntryPrompt = ((OrderQuestion)resource).AddRemoveManualEntryPrompt;
            AddRemoveManualEntryNotes = ((OrderQuestion)resource).AddRemoveManualEntryNotes;

            UI = ((OrderQuestion)resource).UI;
        }

        public override void SetResourceValues(IResource resource)
        {
            base.SetResourceValues(resource);

            ((OrderQuestion)resource).Catalogs = Catalogs;
            ((OrderQuestion)resource).ShowGrandTotal = ShowGrandTotal;
            ((OrderQuestion)resource).ShowSubTotals = ShowSubTotals;
            ((OrderQuestion)resource).ShowCatalogDescriptions = ShowCatalogDescriptions;

            ((OrderQuestion)resource).AutoCreateOrderOnFinish = AutoCreateOrderOnFinish;

            ((OrderQuestion)resource).ProductTitleText = ProductTitleText;
            ((OrderQuestion)resource).PriceTitleText = PriceTitleText;
            ((OrderQuestion)resource).MinStockTitleText = MinStockTitleText;
            ((OrderQuestion)resource).StockTitleText = StockTitleText;
            ((OrderQuestion)resource).QuantityTitleText = QuantityTitleText;
            ((OrderQuestion)resource).LineItemTotalText = LineItemTotalText;
            ((OrderQuestion)resource).CatalogTotalText = CatalogTotalText;
            ((OrderQuestion)resource).GrandTotalText = GrandTotalText;
            ((OrderQuestion)resource).LineItemNotesTitleText = LineItemNotesTitleText;

            ((OrderQuestion)resource).LengthByWidthLengthText = LengthByWidthLengthText;
            ((OrderQuestion)resource).LengthByWidthWidthText = LengthByWidthWidthText;
            ((OrderQuestion)resource).LengthByWidthAreaText = LengthByWidthAreaText;

            ((OrderQuestion)resource).AddRemoveManualEntryPrompt = AddRemoveManualEntryPrompt;
            ((OrderQuestion)resource).AddRemoveManualEntryNotes = AddRemoveManualEntryNotes;

            ((OrderQuestion)resource).UI = UI;
        }
    }

    public class TaskBatchOperationsQuestionEditorModel : QuestionResourceEditorModel
    {
        public string AssignToDatasource { get; set; }
        public string AssignToParameterName { get; set; }
        public string UpdateDatasource { get; set; }

        public bool AllowReassign { get; set; }
        public bool AllowCancellation { get; set; }
        public bool AllowFinishByManagement { get; set; }
        public bool AllowFinishComplete { get; set; }
        public bool AquireTasksWhenFinishing { get; set; }
        public bool AllowDelete { get; set; }
        public bool AllowClearStartTime { get; set; }

        public bool RequireAuthentication { get; set; }
        public bool KioskMode { get; set; }

        public bool UseButtons { get; set; }

        public override void PopulateFromValueProvider(IValueProvider provider, IRepositorySectionManagerItem designerType)
        {
            base.PopulateFromValueProvider(provider, designerType);

            AssignToDatasource = provider.GetValue("assigntodatasource").AttemptedValue;
            AssignToParameterName = provider.GetValue("assigntoparametername").AttemptedValue;
            UpdateDatasource = provider.GetValue("updatedatasource").AttemptedValue;

            AllowReassign = ParseCheckboxField(provider, "allowreassign");
            AllowCancellation = ParseCheckboxField(provider, "allowcancellation");
            AllowFinishByManagement = ParseCheckboxField(provider, "allowfinishbymanagement");
            AllowFinishComplete = ParseCheckboxField(provider, "allowfinishcomplete");
            AquireTasksWhenFinishing = ParseCheckboxField(provider, "aquiretaskswhenfinishing");
            AllowDelete = ParseCheckboxField(provider, "allowdelete");
            AllowClearStartTime = ParseCheckboxField(provider, "allowclearstarttime");

            RequireAuthentication = ParseCheckboxField(provider, "requireauthentication");
            KioskMode = ParseCheckboxField(provider, "kioskmode");
            UseButtons = ParseCheckboxField(provider, "usebuttons");
        }

        public override void FromResource(IResource resource, IRepositorySectionManagerItem designerType)
        {
            base.FromResource(resource, designerType);

            AssignToDatasource = ((TaskBatchOperationsQuestion)resource).AssignToDatasource;
            AssignToParameterName = ((TaskBatchOperationsQuestion)resource).AssignToParameterName;
            UpdateDatasource = ((TaskBatchOperationsQuestion)resource).UpdateDatasource;

            AllowReassign = ((TaskBatchOperationsQuestion)resource).AllowReassign;
            AllowCancellation = ((TaskBatchOperationsQuestion)resource).AllowCancellation;
            AllowFinishByManagement = ((TaskBatchOperationsQuestion)resource).AllowFinishByManagement;
            AllowFinishComplete = ((TaskBatchOperationsQuestion)resource).AllowFinishComplete;
            AquireTasksWhenFinishing = ((TaskBatchOperationsQuestion)resource).AquireTasksWhenFinishing;
            AllowDelete = ((TaskBatchOperationsQuestion)resource).AllowDelete;
            AllowClearStartTime = ((TaskBatchOperationsQuestion)resource).AllowClearStartTime;

            RequireAuthentication = ((TaskBatchOperationsQuestion)resource).RequireAuthentication;
            KioskMode = ((TaskBatchOperationsQuestion)resource).KioskMode;

            UseButtons = ((TaskBatchOperationsQuestion)resource).UseButtons;
        }

        public override void SetResourceValues(IResource resource)
        {
            base.SetResourceValues(resource);

            ((TaskBatchOperationsQuestion)resource).AssignToDatasource = AssignToDatasource;
            ((TaskBatchOperationsQuestion)resource).AssignToParameterName = AssignToParameterName;
            ((TaskBatchOperationsQuestion)resource).UpdateDatasource = UpdateDatasource;

            ((TaskBatchOperationsQuestion)resource).AllowReassign = AllowReassign;
            ((TaskBatchOperationsQuestion)resource).AllowCancellation = AllowCancellation;
            ((TaskBatchOperationsQuestion)resource).AllowFinishByManagement = AllowFinishByManagement;
            ((TaskBatchOperationsQuestion)resource).AllowFinishComplete = AllowFinishComplete;
            ((TaskBatchOperationsQuestion)resource).AquireTasksWhenFinishing = AquireTasksWhenFinishing;
            ((TaskBatchOperationsQuestion)resource).AllowDelete = AllowDelete;
            ((TaskBatchOperationsQuestion)resource).AllowClearStartTime = AllowClearStartTime;

            ((TaskBatchOperationsQuestion)resource).RequireAuthentication = RequireAuthentication;
            ((TaskBatchOperationsQuestion)resource).KioskMode = KioskMode;

            ((TaskBatchOperationsQuestion)resource).UseButtons = UseButtons;
        }
    }

    public class LeaveManagementQuestionEditorModel : QuestionResourceEditorModel
    {
        public bool AllowReplacedByUser { get; set; }

        public override void PopulateFromValueProvider(IValueProvider provider, IRepositorySectionManagerItem designerType)
        {
            base.PopulateFromValueProvider(provider, designerType);

            AllowReplacedByUser = ParseCheckboxField(provider, "allowreplacedbyuser");
        }

        public override void FromResource(IResource resource, IRepositorySectionManagerItem designerType)
        {
            base.FromResource(resource, designerType);

            AllowReplacedByUser = ((LeaveManagementQuestion)resource).AllowReplacedByUser;
        }

        public override void SetResourceValues(IResource resource)
        {
            base.SetResourceValues(resource);

            ((LeaveManagementQuestion)resource).AllowReplacedByUser = AllowReplacedByUser;
        }
    }

    public class TimesheetManagementQuestionEditorModel : QuestionResourceEditorModel
    {
        public override void PopulateFromValueProvider(IValueProvider provider, IRepositorySectionManagerItem designerType)
        {
            base.PopulateFromValueProvider(provider, designerType);
        }

        public override void FromResource(IResource resource, IRepositorySectionManagerItem designerType)
        {
            base.FromResource(resource, designerType);
        }

        public override void SetResourceValues(IResource resource)
        {
            base.SetResourceValues(resource);
        }
    }

    public class TaskScheduleQuestionEditorModel : QuestionResourceEditorModel
    {
        public Guid? CompanyId { get; set; }
        public Guid? UserId { get; set; }
        public bool ShowJobs { get; set; }

        public override void PopulateFromValueProvider(IValueProvider provider, IRepositorySectionManagerItem designerType)
        {
            base.PopulateFromValueProvider(provider, designerType);

            ShowJobs = ParseCheckboxField(provider, "showjobs");
            if (string.IsNullOrEmpty(provider.GetValue("companyid").AttemptedValue) == false)
            {
                CompanyId = Guid.Parse(provider.GetValue("companyid").AttemptedValue);
            }
            if (string.IsNullOrEmpty(provider.GetValue("userid").AttemptedValue) == false)
            {
                UserId = Guid.Parse(provider.GetValue("userid").AttemptedValue);
            }
        }

        public override void FromResource(IResource resource, IRepositorySectionManagerItem designerType)
        {
            base.FromResource(resource, designerType);

            ShowJobs = ((TaskScheduleQuestion)resource).ShowJobs;
            CompanyId = ((TaskScheduleQuestion)resource).CompanyId;
            UserId = ((TaskScheduleQuestion)resource).UserId;
        }

        public override void SetResourceValues(IResource resource)
        {
            base.SetResourceValues(resource);

            ((TaskScheduleQuestion)resource).ShowJobs = ShowJobs;
            ((TaskScheduleQuestion)resource).CompanyId = CompanyId;
            ((TaskScheduleQuestion)resource).UserId = UserId;
        }
    }

    public class CompanySummaryQuestionEditorModel : QuestionResourceEditorModel
    {
        public bool ShowUserList { get; set; }
        public bool CanCreateUsers { get; set; }
        public List<int> AllowedUserTypes { get; set; }

        public override void PopulateFromValueProvider(IValueProvider provider, IRepositorySectionManagerItem designerType)
        {
            base.PopulateFromValueProvider(provider, designerType);


            ShowUserList = ParseCheckboxField(provider, "showuserlist");
            CanCreateUsers = ParseCheckboxField(provider, "cancreateusers");
            AllowedUserTypes = new List<int>();

            if (provider.GetValue("allowedusertypes") != null)
            {
                string types = provider.GetValue("allowedusertypes").AttemptedValue;
                var typesSplit = types.Split(',');
                foreach (var t in typesSplit)
                {
                    AllowedUserTypes.Add(int.Parse(t));
                }
            }
        }

        public override void FromResource(IResource resource, IRepositorySectionManagerItem designerType)
        {
            base.FromResource(resource, designerType);

            ShowUserList = ((CompanySummaryQuestion)resource).ShowUserList;
            CanCreateUsers = ((CompanySummaryQuestion)resource).CanCreateUsers;
            AllowedUserTypes = ((CompanySummaryQuestion)resource).AllowedUserTypes;
        }

        public override void SetResourceValues(IResource resource)
        {
            base.SetResourceValues(resource);

            ((CompanySummaryQuestion)resource).ShowUserList = ShowUserList;
            ((CompanySummaryQuestion)resource).CanCreateUsers = CanCreateUsers;
            ((CompanySummaryQuestion)resource).AllowedUserTypes = AllowedUserTypes;
        }
    }

    public class CompanyConstructorQuestionEditorModel : QuestionResourceEditorModel
    {
        public string TileUrl { get; set; }
        public string GeocodingUrl { get; set; }

        public bool CreateDefaultUser { get; set; }
        public List<int> AllowedUserTypes { get; set; }

        public override void PopulateFromValueProvider(IValueProvider provider, IRepositorySectionManagerItem designerType)
        {
            base.PopulateFromValueProvider(provider, designerType);

            CreateDefaultUser = ParseCheckboxField(provider, "createdefaultuser");
            AllowedUserTypes = new List<int>();

            TileUrl = provider.GetValue("tileurl").AttemptedValue;
            GeocodingUrl = provider.GetValue("geocodingurl").AttemptedValue;
            if (provider.GetValue("allowedusertypes") != null)
            {
                string types = provider.GetValue("allowedusertypes").AttemptedValue;
                var typesSplit = types.Split(',');
                foreach (var t in typesSplit)
                {
                    AllowedUserTypes.Add(int.Parse(t));
                }
            }
        }

        public override void FromResource(IResource resource, IRepositorySectionManagerItem designerType)
        {
            base.FromResource(resource, designerType);

            TileUrl = ((CompanyConstructorQuestion)resource).TileUrl;
            GeocodingUrl = ((CompanyConstructorQuestion)resource).GeocodingUrl;
            CreateDefaultUser = ((CompanyConstructorQuestion)resource).CreateDefaultUser;
            AllowedUserTypes = ((CompanyConstructorQuestion)resource).AllowedUserTypes;
        }

        public override void SetResourceValues(IResource resource)
        {
            base.SetResourceValues(resource);
            ((CompanyConstructorQuestion)resource).TileUrl = TileUrl;
            ((CompanyConstructorQuestion)resource).GeocodingUrl = GeocodingUrl;
            ((CompanyConstructorQuestion)resource).CreateDefaultUser = CreateDefaultUser;
            ((CompanyConstructorQuestion)resource).AllowedUserTypes = AllowedUserTypes;
        }
    }

    public class CompanyDirectoryQuestionEditorModel : QuestionResourceEditorModel
    {
        public string TileUrl { get; set; }
        public bool AllowRating { get; set; }
        public bool AllowEditing { get; set; }

        public override void PopulateFromValueProvider(IValueProvider provider, IRepositorySectionManagerItem designerType)
        {
            base.PopulateFromValueProvider(provider, designerType);

            TileUrl = provider.GetValue("tileurl").AttemptedValue;
            AllowRating = ParseCheckboxField(provider, "allowrating");
            AllowEditing = ParseCheckboxField(provider, "allowediting");
        }

        public override void FromResource(IResource resource, IRepositorySectionManagerItem designerType)
        {
            base.FromResource(resource, designerType);

            TileUrl = ((CompanyDirectoryQuestion)resource).TileUrl;
            AllowRating = ((CompanyDirectoryQuestion)resource).AllowRating;
            AllowEditing = ((CompanyDirectoryQuestion)resource).AllowEditing;
        }

        public override void SetResourceValues(IResource resource)
        {
            base.SetResourceValues(resource);
            ((CompanyDirectoryQuestion)resource).TileUrl = TileUrl;
            ((CompanyDirectoryQuestion)resource).AllowRating = AllowRating;
            ((CompanyDirectoryQuestion)resource).AllowEditing = AllowEditing;
        }
    }

    public class DataSourceQuestionEditorModel : QuestionResourceEditorModel
    {
        public bool ShowUserInterface { get; set; }
        public string FeedsJson { get; set; }

        public override void PopulateFromValueProvider(IValueProvider provider, IRepositorySectionManagerItem designerType)
        {
            base.PopulateFromValueProvider(provider, designerType);

            ShowUserInterface = ParseCheckboxField(provider, "showuserinterface");
            FeedsJson = provider.GetValue("feeds").AttemptedValue;
        }

        public override void FromResource(IResource resource, IRepositorySectionManagerItem designerType)
        {
            base.FromResource(resource, designerType);

            ShowUserInterface = ((DataSourceQuestion)resource).ShowUserInterface;
            FeedsJson = ((DataSourceQuestion)resource).FeedsToJson(null).ToString();
        }

        public override void SetResourceValues(IResource resource)
        {
            base.SetResourceValues(resource);
            ((DataSourceQuestion)resource).ShowUserInterface = ShowUserInterface;
            ((DataSourceQuestion)resource).FeedsFromJson(FeedsJson);
        }
    }

    public class MappingQuestionEditorModel : QuestionResourceEditorModel
    {
    }

    public class MergedHierarchicalDataSourceQuestionEditorModel : QuestionResourceEditorModel
    {
        public string DataSource { get; set; }

        public override void PopulateFromValueProvider(IValueProvider provider, IRepositorySectionManagerItem designerType)
        {
            base.PopulateFromValueProvider(provider, designerType);

            DataSource = provider.GetValue("datasource").AttemptedValue;
        }

        public override void FromResource(IResource resource, IRepositorySectionManagerItem designerType)
        {
            base.FromResource(resource, designerType);

            DataSource = ((MergedHierarchicalDataSourceQuestion)resource).DataSource;
        }

        public override void SetResourceValues(IResource resource)
        {
            base.SetResourceValues(resource);
            ((MergedHierarchicalDataSourceQuestion)resource).DataSource = DataSource;
        }
    }

    public class eWayPaymentQuestionEditorModel : QuestionResourceEditorModel
    {
    }

    public class PendingPaymentQuestionEditorModel : QuestionResourceEditorModel
    {
        public PendingPaymentQuestionMode Mode { get; set; }

        public override void PopulateFromValueProvider(IValueProvider provider, IRepositorySectionManagerItem designerType)
        {
            base.PopulateFromValueProvider(provider, designerType);

            Mode = (PendingPaymentQuestionMode)Enum.Parse(typeof(PendingPaymentQuestionMode), provider.GetValue("mode").AttemptedValue, true);
        }

        public override void FromResource(IResource resource, IRepositorySectionManagerItem designerType)
        {
            base.FromResource(resource, designerType);

            Mode = ((PendingPaymentQuestion)resource).Mode;
        }

        public override void SetResourceValues(IResource resource)
        {
            base.SetResourceValues(resource);
            ((PendingPaymentQuestion)resource).Mode = Mode;
        }
    }

    public class RecaptchaQuestionEditorModel : QuestionResourceEditorModel
    {
    }
}