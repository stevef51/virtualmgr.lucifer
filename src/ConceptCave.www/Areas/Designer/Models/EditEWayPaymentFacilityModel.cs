﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace ConceptCave.www.Areas.Designer.Models
{
    public class EditEwayPaymentFacilityModelBinder : DefaultModelBinder
    {
        protected override object CreateModel(ControllerContext controllerContext, ModelBindingContext bindingContext, Type modelType)
        {
            EditEwayPaymentFacilityModel model = new EditEwayPaymentFacilityModel();

            model.AlternateHandler = bindingContext.ValueProvider.GetValue("ah").AttemptedValue;
            model.AlternateHandlerMethod = bindingContext.ValueProvider.GetValue("ahm").AttemptedValue;


            model.Id = Guid.Parse(bindingContext.ValueProvider.GetValue("facilityId").AttemptedValue);

            var value = bindingContext.ValueProvider.GetValue("useSandbox");
            if (value != null)
                model.UseSandbox = bool.Parse(value.AttemptedValue.Split(',')[0]);
            
            return model;
        }
    }
        
    public class EditEwayPaymentFacilityModel : ModelFromAlternateHandler
    {
        [HiddenInput(DisplayValue = false)]
        public Guid Id { get; set; }

        public bool UseSandbox { get; set; }    

        public EditEwayPaymentFacilityModel()
        {
        }

        public EditEwayPaymentFacilityModel(ConceptCave.Checklist.Facilities.eWayPaymentFacility facility)
        {
            Id = facility.Id;
            UseSandbox = facility.UseSandbox;
        }
    }
}