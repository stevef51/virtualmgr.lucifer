﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConceptCave.www.Areas.Designer.Models
{
    public class SyntaxCheckModel
    {
        public List<SyntaxCheckMessage> Messages { get; set; }

        public SyntaxCheckModel(Irony.Parsing.ParseTree parseTree)
        {
            Messages = new List<SyntaxCheckMessage>();
            foreach (var e in parseTree.ParserMessages)
                Messages.Add(new SyntaxCheckMessage()
                {
                    Line = e.Location.Line,
                    Column = e.Location.Column > 0 ? e.Location.Column - 1 : 0,
                    Length = 3,
                    Level = e.Level,
                    Message = e.Message
                });
        }
    }

    public class SyntaxCheckMessage
    {
        public int Line { get; set; }
        public int Column { get; set; }
        public int Length { get; set; }
        public string Message { get; set; }
        public Irony.ErrorLevel Level { get; set; }
    }
}