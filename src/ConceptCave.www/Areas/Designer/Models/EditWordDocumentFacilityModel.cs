﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConceptCave.Data.EntityClasses;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace ConceptCave.www.Areas.Designer.Models
{
    public class EditWordDocumentFacilityModelBinder : DefaultModelBinder
    {
        protected override object CreateModel(ControllerContext controllerContext, ModelBindingContext bindingContext, Type modelType)
        {
            EditWordDocumentFacilityModel model = new EditWordDocumentFacilityModel();

            model.AlternateHandler = bindingContext.ValueProvider.GetValue("AlternateHandler").AttemptedValue;
            model.AlternateHandlerMethod = bindingContext.ValueProvider.GetValue("AlternateHandlerMethod").AttemptedValue;

            model.FacilityId = Guid.Parse(bindingContext.ValueProvider.GetValue("FacilityId").AttemptedValue);
            model.Id = Guid.Parse(bindingContext.ValueProvider.GetValue("Id").AttemptedValue);
            model.Name = bindingContext.ValueProvider.GetValue("Name").AttemptedValue;
            model.MediaId = Guid.Parse(bindingContext.ValueProvider.GetValue("MediaId").AttemptedValue);

            return model;
        }
    }

    public class EditWordDocumentFacilityModel : ModelFromAlternateHandler
    {
        [HiddenInput(DisplayValue = false)]
        public Guid Id { get; set; }

        [HiddenInput(DisplayValue = false)]
        public Guid FacilityId { get; set; }

        public string Name { get; set; }

        [UIHint("SingleMedia")]
        public Guid MediaId { get; set; }

        public EditWordDocumentFacilityModel() { }

        public EditWordDocumentFacilityModel(ConceptCave.Repository.Facilities.WordDocumentFacility.WordDocumentFacilityTemplate template, Guid facilityId)
        {
            Id = template.Id;
            FacilityId = facilityId;
            Name = template.Name;
            MediaId = template.MediaId;
        }
    }

    public class EditWordDocumentFacilityTemplateItemModel
    {
        public ConceptCave.Repository.Facilities.WordDocumentFacility.WordDocumentFacilityTemplate Template { get; set; }
        public MediaEntity Media { get; set; }
    }
}