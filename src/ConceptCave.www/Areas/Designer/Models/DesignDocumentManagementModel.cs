﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConceptCave.Checklist.Editor;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Repository;

namespace ConceptCave.www.Areas.Designer.Models
{
    public class DesignDocumentManagementModel
    {
        public List<DesignDocumentManagerModelItem> Items { get; set; }
    }

    public class DesignDocumentManagerModelItem : ResourceEditorModel
    {
    }
}