﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.Editor;
using System.Web.Mvc;
using System.Dynamic;

namespace ConceptCave.www.Areas.Designer.Models
{
    public class PresentMediaQuestionClientEventModel : ModelFromAlternateHandler
    {
        public IQuestion Question { get; set; }
        public PresentedMediaQuestionTrackEvent Event { get; set; }
        public bool IsEditing { get; set; }
    }

    public class PresentMediaQuestionClientEventSaveModelBinder : DefaultModelBinder
    {
        protected override object CreateModel(ControllerContext controllerContext, ModelBindingContext bindingContext, Type modelType)
        {
            PresentMediaQuestionClientEventSaveModel model = new PresentMediaQuestionClientEventSaveModel();

            model.QuestionId = Guid.Parse(bindingContext.ValueProvider.GetValue("questionid").AttemptedValue);
            model.EventId = Guid.Parse(bindingContext.ValueProvider.GetValue("eventid").AttemptedValue);
            model.ActionType = bindingContext.ValueProvider.GetValue("actiontype").AttemptedValue;
            model.Start = decimal.Parse(bindingContext.ValueProvider.GetValue("start").AttemptedValue);
            model.End = decimal.Parse(bindingContext.ValueProvider.GetValue("end").AttemptedValue);

            model.ActionData = new ExpandoObject();

            switch (model.ActionType)
            { 
                case "show":
                    model.ActionData.TargetName = bindingContext.ValueProvider.GetValue("targetname").AttemptedValue;
                    model.ActionData.EnterAnimation = (ClienModelAnimationType)Enum.Parse(typeof(ClienModelAnimationType), bindingContext.ValueProvider.GetValue("enteranimation").AttemptedValue);
                    model.ActionData.LeaveAnimation = (ClienModelAnimationType)Enum.Parse(typeof(ClienModelAnimationType), bindingContext.ValueProvider.GetValue("leaveanimation").AttemptedValue);
                    model.ActionData.MakeVisible = ParseCheckboxField(bindingContext.ValueProvider, "makevisible");
                    break;

                case "seek":
                    model.ActionData.SeekPosition = decimal.Parse(bindingContext.ValueProvider.GetValue("seekposition").AttemptedValue);
                    break;
            }

            return model;
        }

        protected virtual bool ParseCheckboxField(IValueProvider provider, string FieldName)
        {
            return bool.Parse(provider.GetValue(FieldName).AttemptedValue.Split(',')[0]);
        }
    }

    public class PresentMediaQuestionClientEventSaveModel
    {
        public Guid QuestionId { get; set; }
        public Guid EventId { get; set; }
        public string ActionType { get; set; }

        public decimal Start { get; set; }
        public decimal End { get; set; }

        public dynamic ActionData { get; set; }
    }
}