﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Repository.Injected;

namespace ConceptCave.www.Areas.Designer.Models
{
    /// <summary>
    /// Had to implement this as the nullable bool for PassFail was giving me grief being parsed back when a post is done
    /// </summary>
    public class EditMultiChoiceQuestionOptionModelBinder : DefaultModelBinder
    {
        protected override object CreateModel(ControllerContext controllerContext, ModelBindingContext bindingContext, Type modelType)
        {
            EditMultiChoiceQuestionOptionModel model = new EditMultiChoiceQuestionOptionModel() { Languages = OLanguageRepository.GetAll() };

            model.AlternateHandler = bindingContext.ValueProvider.GetValue("AlternateHandler").AttemptedValue;
            model.AlternateHandlerMethod = bindingContext.ValueProvider.GetValue("AlternateHandlerMethod").AttemptedValue;


            model.QuestionId = Guid.Parse(bindingContext.ValueProvider.GetValue("QuestionId").AttemptedValue);
            model.Id = Guid.Parse(bindingContext.ValueProvider.GetValue("Id").AttemptedValue);
            model.LocalisableDisplay = ResourceEditorModel.ParseStringDictionary(bindingContext.ValueProvider, "localisabledisplay");
            model.Answer = bindingContext.ValueProvider.GetValue("Answer").AttemptedValue;
            model.Score = decimal.Parse(bindingContext.ValueProvider.GetValue("Score").AttemptedValue);

            bool passFail = false;

            if (bool.TryParse(bindingContext.ValueProvider.GetValue("PassFail").AttemptedValue, out passFail))
            {
                model.PassFail = passFail;
            }

            return model;
        }
    }

    public class EditMultiChoiceQuestionOptionModel : ModelFromAlternateHandler
    {
        [HiddenInput(DisplayValue=false)]
        public Guid QuestionId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public Guid Id { get; set; }

        public IDictionary<string,string> LocalisableDisplay { get; set; }
        public string Answer { get; set; }
        public decimal Score { get; set; }
        
        public bool? PassFail { get; set; }

        public IMultiChoiceItem Resource { get; set; }
        public IEnumerable<LanguageEntity> Languages { get; set; }

        public void FromMultiChoiceOption(Guid questionId, IMultiChoiceItem item)
        {
            Resource = item;
            QuestionId = questionId;
            Id = item.Id;
            LocalisableDisplay = item.LocalisableDisplay.ToDictionary();
            Answer = item.Answer;
            Score = item.Score;
            PassFail = item.PassFail;
        }

        public void ToMultiChoiceOption(IMultiChoiceItem item)
        {
            item.LocalisableDisplay.FromDictionary(LocalisableDisplay);
            item.Answer = Answer;
            item.Score = Score;
            item.PassFail = PassFail;
        }
    }
}