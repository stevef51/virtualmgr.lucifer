﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace ConceptCave.www.Areas.Designer.Models
{
    public class EditEmailFacilityModelBinder : DefaultModelBinder
    {
        protected override object CreateModel(ControllerContext controllerContext, ModelBindingContext bindingContext, Type modelType)
        {
            EditEmailFacilityModel model = new EditEmailFacilityModel();

            model.AlternateHandler = bindingContext.ValueProvider.GetValue("AlternateHandler").AttemptedValue;
            model.AlternateHandlerMethod = bindingContext.ValueProvider.GetValue("AlternateHandlerMethod").AttemptedValue;


            model.FacilityId = Guid.Parse(bindingContext.ValueProvider.GetValue("FacilityId").AttemptedValue);
            model.Id = Guid.Parse(bindingContext.ValueProvider.GetValue("Id").AttemptedValue);
            model.Name = bindingContext.ValueProvider.GetValue("Name").AttemptedValue;
            model.Subject = bindingContext.ValueProvider.GetValue("Subject").AttemptedValue;
            model.To = bindingContext.ValueProvider.GetValue("To").AttemptedValue;
            model.Body = bindingContext.ValueProvider.GetValue("Body").AttemptedValue;

            return model;
        }
    }

    public class EditEmailFacilityModel : ModelFromAlternateHandler
    {
        [HiddenInput(DisplayValue = false)]
        public Guid Id { get; set; }

        [HiddenInput(DisplayValue = false)]
        public Guid FacilityId { get; set; }

        public string Name { get; set; }
        public string Subject { get; set; }
        public string To { get; set; }
        public string From { get; set; }

        [Display(Name="Send Immediately")]
        public bool SendImmediately { get; set; }

        [UIHint("Html")]
        public string Body { get; set; }

        public List<EditEmailFacilityAttachmentModel> Attachments { get; set; }

        public EditEmailFacilityModel()
        {
            Attachments = new List<EditEmailFacilityAttachmentModel>();
        }

        public EditEmailFacilityModel(ConceptCave.Checklist.Facilities.EmailFacility.EmailTemplate template, Guid facilityId)
        {
            Id = template.Id;
            FacilityId = facilityId;
            Name = template.Name;
            Subject = template.Subject;
            To = template.To;
            Body = template.Body;
            From = template.From;
            SendImmediately = template.SendImmediately;

            Attachments = new List<EditEmailFacilityAttachmentModel>();

            foreach (var a in template.Attachments)
            {
                var attach = new EditEmailFacilityAttachmentModel()
                {
                    Id = a.Id,
                    Name = a.Name
                };

                Attachments.Add(attach);
            }
        }
    }

    public class EditEmailFacilityAttachmentModel
    {
        [HiddenInput(DisplayValue = false)]
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}