﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Repository;
using ConceptCave.Checklist.Editor;
using ConceptCave.www.Areas.Designer.Models;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.Repository.Injected;

namespace ConceptCave.www.Areas.Designer.Controllers
{
    public class GetDesignDocumentDesignerUIController : ControllerBase
    {
        //
        // GET: /Designer/GetDesignDocumentDesignerUI/

        public ActionResult Index(Guid id)
        {
            ResourceEntity resourceEntity = ResourceRepository.GetByNodeId(id, ResourceLoadInstructions.Data | ResourceLoadInstructions.ResourceLabel | ResourceLoadInstructions.Label);

            DesignDocument document = (DesignDocument)ResourceRepository.ResourceFromEntity(resourceEntity);

            return View(new GetResourceDesignerUIModel() 
            { 
                Resource = document,
                Labels = new www.Models.SelectMultipleLabelsModel(LabelRepository.GetAll(LabelFor.Resources), (from l in resourceEntity.LabelResources select l.Label)),
                SelectedLabels = (from l in resourceEntity.LabelResources select l.Label),
                Languages = OLanguageRepository.GetAll()
            });
        }

    }
}
