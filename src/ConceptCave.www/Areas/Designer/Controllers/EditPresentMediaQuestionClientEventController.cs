﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.Editor;
using ConceptCave.www.Areas.Designer.Models;

namespace ConceptCave.www.Areas.Designer.Controllers
{
    public class EditPresentMediaQuestionClientEventController : ControllerWithAlternateHandler
    {
        private IAlternateHandler _currentAlternateHandler;

        protected IAlternateHandler CurrentAlternateHandler
        {
            get
            {
                if (_currentAlternateHandler == null)
                {
                    _currentAlternateHandler = CreateAlternateHandler();
                }

                return _currentAlternateHandler;
            }
        }

        protected IAlternateHandlerResult GetQuestionFromHandler(Guid questionId)
        {
            return CurrentAlternateHandler.GetResource(questionId);
        }

        protected void SaveQuestionToHandler(IAlternateHandlerResult result)
        {
            CurrentAlternateHandler.SaveResource(result);
        }

        [HttpPost]
        public ActionResult NewEvent(Guid questionId, string actionType)
        {
            IAlternateHandlerResult result = GetQuestionFromHandler(questionId);

            IPresentMediaQuestion question = (IPresentMediaQuestion)result.Resource;

            PresentedMediaQuestionTrackEvent ev = new PresentedMediaQuestionTrackEvent();

            switch (actionType)
            {
                case "show":
                    ev.Action = new ChangeVisibilityClientModelEventAction() { MakeVisible = true };
                    break;

                case "pause":
                    ev.Action = new PauseMediaClientModelEventAction();
                    break;

                case "seek":
                    ev.Action = new SeekToClientModelEventAction();
                    break;
            }

            question.ClientModel.Events.Add(ev);

            SaveQuestionToHandler(result);

            PresentMediaQuestionClientEventModel model = new PresentMediaQuestionClientEventModel() { AlternateHandler = CurrentAlternateHandlerName, Question = question, Event = ev };
            CurrentAlternateHandler.SetModelForHandler(model);

            return View("PresentMediaQuestionEventListItem", model);
        }

        [HttpPost]
        public ActionResult SaveEvent([ModelBinder(typeof(PresentMediaQuestionClientEventSaveModelBinder))] PresentMediaQuestionClientEventSaveModel model)
        {
            IAlternateHandlerResult result = GetQuestionFromHandler(model.QuestionId);
            IPresentMediaQuestion question = (IPresentMediaQuestion)result.Resource;

            var ev = (from e in question.ClientModel.Events where e.Id == model.EventId select e).Cast<PresentedMediaQuestionTrackEvent>().DefaultIfEmpty(null).First();

            ev.Start = model.Start;
            ev.End = model.End;

            if (ev.Action is ChangeVisibilityClientModelEventAction)
            {
                ChangeVisibilityClientModelEventAction action = (ChangeVisibilityClientModelEventAction)ev.Action;
                action.TargetName = model.ActionData.TargetName;
                action.EnterAnimation = model.ActionData.EnterAnimation;
                action.LeaveAnimation = model.ActionData.LeaveAnimation;
                action.MakeVisible = model.ActionData.MakeVisible;
            }
            else if (ev.Action is SeekToClientModelEventAction)
            {
                SeekToClientModelEventAction action = (SeekToClientModelEventAction)ev.Action;
                action.SeekPosition = model.ActionData.SeekPosition;
            }

            SaveQuestionToHandler(result);

            ViewBag.ActionTaken = ActionTaken.Save;

            PresentMediaQuestionClientEventModel r = new PresentMediaQuestionClientEventModel() { AlternateHandler = CurrentAlternateHandlerName, Question = question, Event = ev };
            CurrentAlternateHandler.SetModelForHandler(r);

            return View("PresentMediaQuestionEventListItem", r);
        }

        [HttpPost]
        public ActionResult EditEvent(Guid questionId, Guid eventId)
        {
            IAlternateHandlerResult result = GetQuestionFromHandler(questionId);
            IPresentMediaQuestion question = (IPresentMediaQuestion)result.Resource;

            var ev = (from e in question.ClientModel.Events where e.Id == eventId select e).DefaultIfEmpty(null).First();

            if (ev == null)
            {
                return View("PresentMediaQuestionEventListItem");
            }

            PresentMediaQuestionClientEventModel model = new PresentMediaQuestionClientEventModel() { AlternateHandler = CurrentAlternateHandlerName, Question = question, Event = (PresentedMediaQuestionTrackEvent)ev, IsEditing = true };
            CurrentAlternateHandler.SetModelForHandler(model);

            return View("PresentMediaQuestionEventListItem", model);
        }

        [HttpPost]
        public ActionResult RemoveEvent(Guid questionId, Guid eventId)
        {
            IAlternateHandlerResult result = GetQuestionFromHandler(questionId);
            IPresentMediaQuestion question = (IPresentMediaQuestion)result.Resource;

            var ev = (from e in question.ClientModel.Events where e.Id == eventId select e).DefaultIfEmpty(null).First();

            if (ev == null)
            {
                return View("PresentMediaQuestionEventListItem");
            }

            question.ClientModel.Events.Remove(ev);

            SaveQuestionToHandler(result);

            ViewBag.ActionTaken = ActionTaken.Remove;
            return View("PresentMediaQuestionEventListItem", new PresentMediaQuestionClientEventModel() { Question = question, Event = (PresentedMediaQuestionTrackEvent)ev });
        }
    }
}
