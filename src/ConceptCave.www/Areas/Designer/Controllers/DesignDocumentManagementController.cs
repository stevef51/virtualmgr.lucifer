﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.BusinessLogic.Configuration;
using ConceptCave.Data.DTOConverters;
using ConceptCave.Repository;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.Editor;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Core;
using ConceptCave.www.Areas.Designer.Models;
using ConceptCave.Data.HelperClasses;
using System.Dynamic;
using RepositorySectionManager = ConceptCave.Repository.RepositorySectionManager;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.Repository.DesignDocumentImport;
using ConceptCave.Repository.Injected;
using System.IO;
using ConceptCave.Core.XmlCodable;
using System.Xml;

namespace ConceptCave.www.Areas.Designer.Controllers
{
    public class DesignDocumentManagementController : ControllerWithAlternateHandler, IAlternateHandler
    {
        public class DesignDocumentManagementControllerAlternateHandlerResult : IAlternateHandlerResult
        {
            public ResourceEntity Entity { get; set; }
            public DesignDocument Document { get; set; }
            public IResource Resource { get; set; }
        }

        public static string DesignDocumentManagementControllerAlternameHandlerName = "AlternateHandler.DesignDocumentManagementController";
        public static string DesignDocumentManagementControllerAlternameHandlerMethodName = "SaveSection";
        public static string DesignDocumentManagementControllerAlternameHandlerActionMethodName = "SaveAction";
        public static string RootIdContextFieldName = "rootchecklistid";

        protected class LoadByIdResult
        {
            public ResourceEntity Entity { get; set; }
            public DesignDocument Document { get; set; }
        }

        protected LoadByIdResult LoadByNodeId(Guid id)
        {
            ResourceEntity resourceEntity = ResourceRepository.GetByNodeId(id, ResourceLoadInstructions.Data | ResourceLoadInstructions.ResourceLabel | ResourceLoadInstructions.Label);

            if (resourceEntity == null)
            {
                throw new ArgumentException("No checklist with that id exists");
            }

            DesignDocument document = (DesignDocument)ResourceRepository.ResourceFromEntity(resourceEntity);

            return new LoadByIdResult() { Entity = resourceEntity, Document = document };
        }

        public ActionResult Index()
        {
            EntityCollection<ResourceEntity> resources = ResourceRepository.GetLikeName("%", StandardResourceCategories.Checklists.ToString(), ResourceLoadInstructions.ResourceLabel | ResourceLoadInstructions.Label);

            List<DesignDocumentManagerModelItem> items = new List<DesignDocumentManagerModelItem>();

            foreach (var resource in resources)
            {
                IRepositorySectionManagerItem item = RepositorySectionManager.Current.ItemForResourceEntity(resource.ToDTO());

                DesignDocumentManagerModelItem q = (DesignDocumentManagerModelItem)item.CreateEditorModel();
                q.FromResourceEntity(resource, item);

                items.Add(q);
            }

            DesignDocumentManagementModel model = new DesignDocumentManagementModel() 
            {
                Items = items
            };

            return View(model);
        }

        [HttpPost]
        public ActionResult Add()
        {
            IRepositorySectionManagerItem item = RepositorySectionManager.Current.ItemsForCategory(StandardResourceCategories.Checklists.ToString()).First();

            DesignDocument document = (DesignDocument)item.CreateResource();

            document.Name = "New Checklist";

            ResourceEntity resourceEntity = ResourceRepository.New((IResource)document);

            ResourceRepository.Save((IResource)document, resourceEntity, true, true);

            ViewBag.ActionTaken = ActionTaken.New;

            return PartialView("RenderDesignDocumentUI", new GetResourceDesignerUIModel() 
            { 
                Resource = document, 
                Labels = new www.Models.SelectMultipleLabelsModel(LabelRepository.GetAll(LabelFor.Resources), null),
                Languages = OLanguageRepository.GetAll()
            });
        }

        [HttpPost]
        public ActionResult Remove(Guid id)
        {
            ResourceEntity resourceEntity = ResourceRepository.GetByNodeId(id, ResourceLoadInstructions.Data);

            if (resourceEntity == null)
            {
                throw new ArgumentException("No checklist with that id exists");
            }

            IResource resource = ResourceRepository.ResourceFromEntity(resourceEntity);

            ResourceRepository.Remove(id);

            ViewBag.ActionTaken = ActionTaken.Remove;

            // hand in the question so the view can get at the information that was deleted
            return PartialView("RenderDesignDocumentUI", new GetResourceDesignerUIModel() { Resource = resource, Languages = OLanguageRepository.GetAll() });
        }

        [HttpPost]
        public ActionResult Move(Guid id, Guid itemId, Guid originalParentId, Guid newParentId, int newIndex)
        {
            LoadByIdResult result = LoadByNodeId(id);

            Section originalParent = (Section)(from s in result.Document.AsDepthFirstEnumerable() where ((IUniqueNode)s).Id == originalParentId select s).First();
            Section newParent = (Section)(from s in result.Document.AsDepthFirstEnumerable() where ((IUniqueNode)s).Id == newParentId select s).First();

            IPresentable presentable = (from s in result.Document.AsDepthFirstEnumerable() where ((IUniqueNode)s).Id == itemId select s).First();

            originalParent.Children.Remove(presentable);
            newParent.Children.Insert(newIndex, presentable);

            ResourceRepository.Save(result.Document, result.Entity, true, true);

            IRepositorySectionManagerItem item = RepositorySectionManager.Current.ItemForObject(presentable);

            ViewBag.ActionTaken = ActionTaken.Move;

            var uiresult = new GetResourceDesignerUIModel()
            {
                DesignerType = item,
                Resource = (IResource)presentable,
                RootId = id,
                AlternateHandler = DesignDocumentManagementControllerAlternameHandlerName,
                AlternateHandlerMethod = DesignDocumentManagementControllerAlternameHandlerMethodName,
                Labels = new www.Models.SelectMultipleLabelsModel(LabelRepository.GetAll(LabelFor.Resources), (from l in result.Entity.LabelResources select l.Label)),
                SelectedLabels = (from l in result.Entity.LabelResources select l.Label),
                Languages = OLanguageRepository.GetAll()
            };

            uiresult.ContextFields.Add(new ControllerWithAlternateHandlerContextField() { Name = RootIdContextFieldName, Value = id.ToString() });

            return PartialView("RenderResourceDesignerUI", uiresult);
        }

        [HttpPost]
        public ActionResult MovePage(Guid id, Guid itemId, int newIndex)
        {
            LoadByIdResult result = LoadByNodeId(id);

            Section section = (Section)(from s in result.Document.Sections where ((IUniqueNode)s).Id == itemId select s).First();

            result.Document.Sections.Remove(section);
            result.Document.Sections.Insert(newIndex, section);

            ResourceRepository.Save(result.Document, result.Entity, true, true);

            IRepositorySectionManagerItem item = RepositorySectionManager.Current.ItemForObject(section);

            ViewBag.ActionTaken = ActionTaken.Move;

            var uiresult = new GetResourceDesignerUIModel()
            {
                DesignerType = item,
                Resource = (IResource)section,
                RootId = id,
                AlternateHandler = DesignDocumentManagementControllerAlternameHandlerName,
                AlternateHandlerMethod = DesignDocumentManagementControllerAlternameHandlerMethodName,
                Labels = new www.Models.SelectMultipleLabelsModel(LabelRepository.GetAll(LabelFor.Resources), (from l in result.Entity.LabelResources select l.Label)),
                SelectedLabels = (from l in result.Entity.LabelResources select l.Label),
                Languages = OLanguageRepository.GetAll()
            };

            uiresult.ContextFields.Add(new ControllerWithAlternateHandlerContextField() { Name = RootIdContextFieldName, Value = id.ToString() });

            return PartialView("RenderResourceDesignerUI", uiresult);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Save(Guid id, string name, string sourcecode, Guid[] labels, FinishMode finishmode, IDictionary<string,string> localisableDisplayName)
        {
            LoadByIdResult result = LoadByNodeId(id);

            result.Document.Name = name;
            result.Document.LocalisableDisplayName.FromDictionary(localisableDisplayName);
            result.Document.ProgramDefinition.SourceCode = sourcecode;
            result.Document.FinishMode = finishmode;

            // now any deleted labels
            if (labels == null)
            {
                labels = new Guid[] { };
            }
            var deletedLabels = (from m in result.Entity.LabelResources where labels.Contains(m.LabelId) == false select m.LabelId);
            // now any new labels
            var newLabels = (from m in labels where (from q in result.Entity.LabelResources select q.LabelId).Contains(m) == false select m);

            newLabels.ToList().ForEach(i =>
            {
                result.Entity.LabelResources.Add(new LabelResourceEntity() { LabelId = i, ResourceId = result.Entity.Id });
            });

            ResourceRepository.Save((IResource)result.Document, result.Entity, true, true);
            ResourceRepository.RemoveFromLabels(deletedLabels.ToArray(), result.Entity.Id);

            result = LoadByNodeId(id);

            ViewBag.ActionTaken = ActionTaken.Save;

            return PartialView("RenderDesignDocumentUI", new GetResourceDesignerUIModel() 
            { 
                Resource = result.Document, 
                Labels = new www.Models.SelectMultipleLabelsModel(LabelRepository.GetAll(LabelFor.Resources), (from l in result.Entity.LabelResources select l.Label)), 
                SelectedLabels = (from l in result.Entity.LabelResources select l.Label),
                Languages = OLanguageRepository.GetAll()
            });
        }

        [HttpPost]
        public ActionResult Import(Guid id, HttpPostedFileBase fileUpload)
        {
            LoadByIdResult result = LoadByNodeId(id);

            XlsxImporter importer = new XlsxImporter();
            var dd = (DesignDocument)importer.Import(fileUpload.InputStream, result.Document);

            ResourceRepository.Save(dd, result.Entity, true, true);

            ViewBag.ActionTaken = ActionTaken.Reload;

            return PartialView("~/Areas/Designer/Views/Shared/RenderDesignDocumentUI.cshtml", new GetResourceDesignerUIModel() { Resource = result.Document });
        }

        [HttpGet]
        public FileStreamResult Export(Guid id)
        {
            LoadByIdResult result = LoadByNodeId(id);

            XlsxExporter exporter = new XlsxExporter();

            var stream = new MemoryStream();

            exporter.Export(stream, result.Document);

            return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", result.Document.Name + ".xlsx");
        }

        [HttpPost]
        public ActionResult ImportXml(Guid id, HttpPostedFileBase fileUpload)
        {
            LoadByIdResult result = LoadByNodeId(id);

            XmlDocument doc = new XmlDocument();
            doc.Load(fileUpload.InputStream);
            XmlDocumentDecoder decoder = new XmlDocumentDecoder(doc.DocumentElement);

            DesignDocument dd = new DesignDocument();
            dd.Decode(decoder);
            
            // Set the imported doc to use the Id specified (allows for duplication)
            IUniqueNode un = dd as IUniqueNode;
            un.UseDecodedId(id);

            ResourceRepository.Save(dd, result.Entity, true, true);

            ViewBag.ActionTaken = ActionTaken.Reload;

            return PartialView("~/Areas/Designer/Views/Shared/RenderDesignDocumentUI.cshtml", new GetResourceDesignerUIModel() { Resource = result.Document });
        }

        [HttpGet]
        public FileStreamResult ExportXml(Guid id)
        {
            LoadByIdResult result = LoadByNodeId(id);

            XmlDocumentEncoder encoder = new XmlDocumentEncoder();
            result.Document.Encode(encoder);

            var stream = new MemoryStream();
            encoder.Document.Save(stream);
            stream.Position = 0;

            return File(stream, "application/xml", result.Document.Name + ".xml");
        }

        [HttpPost]
        public ActionResult GetSelectPages(Guid id)
        {
            LoadByIdResult result = LoadByNodeId(id);

            return PartialView("RenderSelectPage", result.Document.Sections);
        }

        [HttpPost]
        public ActionResult AddPage(Guid id)
        {
            LoadByIdResult result = LoadByNodeId(id);

            Section section = new Section() { Name = "New Page" };
            result.Document.Sections.Add(section);

            ResourceRepository.Save(result.Document, result.Entity, true, true);

            ViewBag.ActionTaken = ActionTaken.New;

            return PartialView("RenderPageListItem", section);
        }

        [HttpPost]
        public ActionResult RemovePage(Guid id, Guid pageId)
        {
            LoadByIdResult result = LoadByNodeId(id);

            var page = (from p in result.Document.Sections where p.Id == pageId select p).First();

            result.Document.Sections.Remove(page);

            ResourceRepository.Save(result.Document, result.Entity, true, true);

            ViewBag.ActionTaken = ActionTaken.Remove;

            IRepositorySectionManagerItem item = RepositorySectionManager.Current.ItemForObject(page);
            var resultModel = new GetResourceDesignerUIModel()
            {
                RootId = page.Id,
                DesignerType = item,
                Resource = (IResource)page,
                AlternateHandler = DesignDocumentManagementControllerAlternameHandlerName,
                AlternateHandlerMethod = DesignDocumentManagementControllerAlternameHandlerMethodName,
                Labels = new www.Models.SelectMultipleLabelsModel(LabelRepository.GetAll(LabelFor.Resources), (from l in result.Entity.LabelResources select l.Label)),
                SelectedLabels = (from l in result.Entity.LabelResources select l.Label),
                Languages = OLanguageRepository.GetAll()
            };

            resultModel.ContextFields.Add(new ControllerWithAlternateHandlerContextField() { Name = RootIdContextFieldName, Value = id.ToString() });

            return PartialView("RenderSectionUI", resultModel);
        }

        [HttpPost]
        public ActionResult GetPage(Guid id, Guid pageId)
        {
            LoadByIdResult result = LoadByNodeId(id);

            var page = (from p in result.Document.Sections where p.Id == pageId select p).First();

            IRepositorySectionManagerItem item = RepositorySectionManager.Current.ItemForObject(page);
            var resultModel = new GetResourceDesignerUIModel()
            {
                RootId = id,
                DesignerType = item,
                Resource = (IResource)page,
                AlternateHandler = DesignDocumentManagementControllerAlternameHandlerName,
                AlternateHandlerMethod = DesignDocumentManagementControllerAlternameHandlerMethodName,
                Labels = new www.Models.SelectMultipleLabelsModel(LabelRepository.GetAll(LabelFor.Resources), (from l in result.Entity.LabelResources select l.Label)),
                SelectedLabels = (from l in result.Entity.LabelResources select l.Label),
                IsRoot = true,
                Languages = OLanguageRepository.GetAll()
            };

            resultModel.ContextFields.Add(new ControllerWithAlternateHandlerContextField() { Name = RootIdContextFieldName, Value = id.ToString() });

            return PartialView("RenderSectionUI", resultModel);
        }

        [HttpPost]
        public ActionResult GetPageListItem(Guid id, Guid pageId)
        {
            LoadByIdResult result = LoadByNodeId(id);

            var page = (from p in result.Document.Sections where p.Id == pageId select p).First();

            return PartialView("RenderPageListItem", page);
        }

        [HttpPost]
        public ActionResult AddAction(Guid id, string key)
        {
            LoadByIdResult result = LoadByNodeId(id);

            IRepositorySectionManagerItem item = RepositorySectionManager.Current.ItemForFriendlyType(key);

            IResource resource = item.CreateResource();

            result.Document.Facilities.Add((IFacility)resource);

            ResourceRepository.Save(result.Document, result.Entity, true, true);

            return PartialView("RenderActionListItem", resource);
        }

        [HttpPost]
        public ActionResult RemoveAction(Guid id, Guid actionId)
        {
            LoadByIdResult result = LoadByNodeId(id);

            var action = (from p in result.Document.Facilities where p.Id == actionId select p).First();

            result.Document.Facilities.Remove(action);

            ResourceRepository.Save(result.Document, result.Entity, true, true);

            ViewBag.ActionTaken = ActionTaken.Remove;

            IRepositorySectionManagerItem item = RepositorySectionManager.Current.ItemForObject(action);
            var resultModel = new GetResourceDesignerUIModel()
            {
                RootId = actionId,
                DesignerType = item,
                Resource = (IResource)action,
                AlternateHandler = DesignDocumentManagementControllerAlternameHandlerName,
                AlternateHandlerMethod = DesignDocumentManagementControllerAlternameHandlerMethodName,
                Labels = new www.Models.SelectMultipleLabelsModel(LabelRepository.GetAll(LabelFor.Resources), (from l in result.Entity.LabelResources select l.Label)),
                SelectedLabels = (from l in result.Entity.LabelResources select l.Label),
                Languages = OLanguageRepository.GetAll()
            };

            resultModel.ContextFields.Add(new ControllerWithAlternateHandlerContextField() { Name = RootIdContextFieldName, Value = id.ToString() });

            return PartialView("RenderResourceDesignerUI", resultModel);
        }

        [HttpPost]
        public ActionResult GetAction(Guid id, Guid actionId)
        {
            LoadByIdResult result = LoadByNodeId(id);

            var action = (from p in result.Document.Facilities where p.Id == actionId select p).First();

            IRepositorySectionManagerItem item = RepositorySectionManager.Current.ItemForObject(action);
            var resultModel = new GetResourceDesignerUIModel()
            {
                RootId = actionId,
                DesignerType = item,
                Resource = (IResource)action,
                AlternateHandler = DesignDocumentManagementControllerAlternameHandlerName,
                AlternateHandlerMethod = DesignDocumentManagementControllerAlternameHandlerActionMethodName,
                Labels = new www.Models.SelectMultipleLabelsModel(LabelRepository.GetAll(LabelFor.Resources), (from l in result.Entity.LabelResources select l.Label)),
                SelectedLabels = (from l in result.Entity.LabelResources select l.Label),
                Languages = OLanguageRepository.GetAll()
            };

            resultModel.ContextFields.Add(new ControllerWithAlternateHandlerContextField() { Name = RootIdContextFieldName, Value = id.ToString() });

            return PartialView("RenderResourceDesignerUI", resultModel);
        }

        public ActionResult Handle(string method, params object[] models)
        {
            if (method == "SaveAction")
            {
                return SaveAction((ResourceEditorModel)models[0]);
            }
            
            return SaveSection((ResourceEditorModel)models[0]);
        }

        public ActionResult SaveSection(ResourceEditorModel model)
        {
            var id = Guid.Parse((from c in model.ContextFields where c.Name == RootIdContextFieldName select c.Value).First());

            LoadByIdResult result = LoadByNodeId(id);

            IResource section = (IResource)(from s in result.Document.AsDepthFirstEnumerable() where ((IUniqueNode)s).Id == model.Id select s).First();

            model.SetResourceValues(section);

            ResourceRepository.Save((IResource)result.Document, result.Entity, true, true);

            IRepositorySectionManagerItem item = RepositorySectionManager.Current.ItemForObject(section);

            ViewBag.ActionTaken = ActionTaken.Save;

            var resultModel = new GetResourceDesignerUIModel()
            {
                RootId = section.Id,
                DesignerType = item,
                Resource = section,
                AlternateHandler = DesignDocumentManagementControllerAlternameHandlerName,
                AlternateHandlerMethod = DesignDocumentManagementControllerAlternameHandlerMethodName,
                Labels = new www.Models.SelectMultipleLabelsModel(LabelRepository.GetAll(LabelFor.Resources), (from l in result.Entity.LabelResources select l.Label)),
                SelectedLabels = (from l in result.Entity.LabelResources select l.Label),
                Languages = OLanguageRepository.GetAll(),
                IsRoot = result.Document.Sections.Any(s => s.Id == model.Id)
            };

            resultModel.ContextFields.Add(new ControllerWithAlternateHandlerContextField() { Name = RootIdContextFieldName, Value = id.ToString() });

            return PartialView("RenderResourceDesignerUI", resultModel);
        }

        public ActionResult SaveAction(ResourceEditorModel model)
        {
            var id = Guid.Parse((from c in model.ContextFields where c.Name == RootIdContextFieldName select c.Value).First());

            LoadByIdResult result = LoadByNodeId(id);

            IResource action = (IResource)(from s in result.Document.Facilities where ((IUniqueNode)s).Id == model.Id select s).First();

            model.SetResourceValues(action);

            ResourceRepository.Save((IResource)result.Document, result.Entity, true, true);

            IRepositorySectionManagerItem item = RepositorySectionManager.Current.ItemForObject(action);

            ViewBag.ActionTaken = ActionTaken.Save;

            var resultModel = new GetResourceDesignerUIModel()
            {
                RootId = action.Id,
                DesignerType = item,
                Resource = action,
                AlternateHandler = DesignDocumentManagementControllerAlternameHandlerName,
                AlternateHandlerMethod = DesignDocumentManagementControllerAlternameHandlerActionMethodName,
                Labels = new www.Models.SelectMultipleLabelsModel(LabelRepository.GetAll(LabelFor.Resources), (from l in result.Entity.LabelResources select l.Label)),
                SelectedLabels = (from l in result.Entity.LabelResources select l.Label),
                Languages = OLanguageRepository.GetAll()
            };

            resultModel.ContextFields.Add(new ControllerWithAlternateHandlerContextField() { Name = RootIdContextFieldName, Value = id.ToString() });

            return PartialView("RenderResourceDesignerUI", resultModel);
        }

        public IAlternateHandlerResult GetResource(Guid id)
        {
            List<ControllerWithAlternateHandlerContextField> contexts = ContextList();
            string rootId = (from c in contexts where c.Name == RootIdContextFieldName select c.Value).First();

            if (string.IsNullOrEmpty(rootId))
            {
                throw new InvalidOperationException("The root id must be supplied");
            }

            ResourceEntity documentEntity = ResourceRepository.GetByNodeId(Guid.Parse(rootId), ResourceLoadInstructions.Data);
            DesignDocument document = (DesignDocument)ResourceRepository.ResourceFromEntity(documentEntity);

            IResource child = (IResource)(from s in document.AsDepthFirstEnumerable() where ((IUniqueNode)s).Id == id select s).DefaultIfEmpty(null).First();

            if (child == null)
            {
                child = (IResource)(from f in document.Facilities where f.Id == id select f).DefaultIfEmpty(null).First();
            }

            return new DesignDocumentManagementControllerAlternateHandlerResult() { Document = document, Resource = child, Entity = documentEntity };
        }

        public void SaveResource(IAlternateHandlerResult result)
        {
            DesignDocumentManagementControllerAlternateHandlerResult r = (DesignDocumentManagementControllerAlternateHandlerResult)result;
            ResourceRepository.Save(r.Document, r.Entity, true, true);
        }

        public void SetModelForHandler(IModelFromAlternateHandler model)
        {
            model.AlternateHandler = CurrentAlternateHandlerName;
            model.AlternateHandlerMethod = CurrentAlternateHandlerMethodName;
            model.ContextFields = ContextList();
        }


        public IDesignDocument GetDocument(IAlternateHandlerResult result)
        {
            return ((DesignDocumentManagementControllerAlternateHandlerResult)result).Document;
        }
    }
}
