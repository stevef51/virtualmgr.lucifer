﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.BusinessLogic.Configuration;
using ConceptCave.Repository;
using ConceptCave.Core;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Checklist.Editor;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.www.Areas.Designer.Models;
using RepositorySectionManager = ConceptCave.Repository.RepositorySectionManager;
using ConceptCave.Repository.Injected;
using ConceptCave.Checklist;
using ConceptCave.Core.XmlCodable;
using System.IO;
using System.Xml;

namespace ConceptCave.www.Areas.Designer.Controllers
{
    public class EditSectionResourceController : ControllerWithAlternateHandler, IAlternateHandler
    {
        public class EditSectionResourceControllerAlternateHandlerResult : IAlternateHandlerResult
        {
            public ResourceEntity Entity { get; set; }
            public Section Section { get; set; }
            public IResource Resource { get; set; }
        }

        public class EditSectionResourceControllerDefaultHandler : IAlternateHandler
        {
            public string AlternateHandler { get; set; }
            public string AlternateHandlerMethod { get; set; }
            public List<ControllerWithAlternateHandlerContextField> ContextFields { get; set; } 

            public ActionResult Handle(string method, params object[] models)
            {
                throw new NotImplementedException();
            }

            public IAlternateHandlerResult GetResource(Guid id)
            {
                ResourceEntity sectionEntity = ResourceRepository.GetByNodeId(id, ResourceLoadInstructions.Data);
                Section s = (Section)ResourceRepository.ResourceFromEntity(sectionEntity);

                return new EditSectionResourceControllerAlternateHandlerResult() { Entity = sectionEntity, Section = s, Resource = s };
            }

            public void SaveResource(IAlternateHandlerResult result)
            {
                EditSectionResourceControllerAlternateHandlerResult a = (EditSectionResourceControllerAlternateHandlerResult)result;
                ResourceRepository.Save(a.Section, a.Entity, false, true);
            }

            public void SetModelForHandler(IModelFromAlternateHandler model)
            {
                model.AlternateHandler = AlternateHandler;
                model.AlternateHandlerMethod = AlternateHandlerMethod;
                model.ContextFields = ContextFields;
            }


            public IDesignDocument GetDocument(IAlternateHandlerResult result)
            {
                return null;
            }
        }

        public static string EditSectionResourceControllerAlternameHandlerName = "AlternateHandler.EditSectionResourceController";
        public static string RootIdContextFieldName = "rootsectionid";
        public static string SavePresentedMethodName = "SavePresented";

        protected IAlternateHandler GetAlternateHandler()
        {
            IAlternateHandler alternate = null;
            if (CurrentAlternateHandlerName != EditSectionResourceControllerAlternameHandlerName)
            {
                // we need to defer handling of the loading of a section to someone else
                alternate = this.CreateAlternateHandler();
            }
            else
            {
                alternate = new EditSectionResourceControllerDefaultHandler()
                {
                    AlternateHandler = CurrentAlternateHandlerName,
                    AlternateHandlerMethod = CurrentAlternateHandlerMethodName,
                    ContextFields = ContextList()
                };
            }

            return alternate;
        }

        public ActionResult Add(string key, Guid sectionid, Guid parentId)
        {
            IAlternateHandler alternate = GetAlternateHandler();
            IAlternateHandlerResult alternateResult = alternate.GetResource(sectionid);

            Section section = (Section)alternateResult.Resource;

            IRepositorySectionManagerItem item = RepositorySectionManager.Current.ItemForFriendlyType(key);
            IResource resource = item.CreateResource();

            if (resource is IQuestion)
            {
                ((IQuestion)resource).Prompt = "New " + item.EditorNewPromptText;
            }
            else
            {
                resource.Name = "New " + item.EditorNewPromptText;
            }

            var parent = (from p in section.AsDepthFirstEnumerable() where ((IUniqueNode)p).Id == parentId select p).First();

            ((ISection)parent).Children.Add((IPresentable)resource);

            alternate.SaveResource(alternateResult);
            
            ViewBag.ActionTaken = ActionTaken.New;

            var result = new GetResourceDesignerUIModel()
            {
                DesignerType = item,
                Resource = resource,
                RootId = sectionid,
                Languages = OLanguageRepository.GetAll()
            };

            alternate.SetModelForHandler(result);
            result.ContextFields.Add(new ControllerWithAlternateHandlerContextField() { Name = RootIdContextFieldName, Value = sectionid.ToString() });

            return PartialView("RenderResourceDesignerUI", result);
        }

        public ActionResult Remove(Guid sectionid, Guid deleteid, Guid parentId)
        {
            IAlternateHandler alternate = GetAlternateHandler();
            IAlternateHandlerResult alternateResult = alternate.GetResource(sectionid);

            Section section = (Section)alternateResult.Resource;

            IResource child = (IResource)(from s in section.AsDepthFirstEnumerable() where ((IUniqueNode)s).Id == deleteid select s).First();

            Section parent = (Section)(from s in section.AsDepthFirstEnumerable() where ((IUniqueNode)s).Id == parentId select s).First();

            IRepositorySectionManagerItem item = RepositorySectionManager.Current.ItemForObject(child);

            parent.Children.Remove((IPresentable)child);

            alternate.SaveResource(alternateResult);

            ViewBag.ActionTaken = ActionTaken.Remove;

            var result = new GetResourceDesignerUIModel()
            {
                DesignerType = item,
                Resource = child,
                RootId = sectionid,
                Languages = OLanguageRepository.GetAll()
            };

            alternate.SetModelForHandler(result);
            result.ContextFields.Add(new ControllerWithAlternateHandlerContextField() { Name = RootIdContextFieldName, Value = sectionid.ToString() });

            return PartialView("RenderResourceDesignerUI", result);
        }

        public ActionResult ExportXml(Guid sectionid, Guid deleteid, Guid parentId)
        {
            IAlternateHandler alternate = GetAlternateHandler();
            IAlternateHandlerResult alternateResult = alternate.GetResource(sectionid);

            Section section = (Section)alternateResult.Resource;

            XmlDocumentEncoder encoder = new XmlDocumentEncoder();
            section.Encode(encoder);

            var stream = new MemoryStream();
            encoder.Document.Save(stream);
            stream.Position = 0;

            string name = "Section Export";
            if(string.IsNullOrEmpty(section.Prompt) == false)
            {
                name = section.Prompt;
            }
            else if(string.IsNullOrEmpty(section.Name) == false)
            {
                name = section.Name;
            }

            return File(stream, "application/xml", name + ".xml");
        }

        [HttpPost]
        public ActionResult ImportXml(Guid sectionid, HttpPostedFileBase fileUpload)
        {
            IAlternateHandler alternate = GetAlternateHandler();
            IAlternateHandlerResult alternateResult = alternate.GetResource(sectionid);

            // this is the section that needs to be written over content wise
            Section section = (Section)alternateResult.Resource;

            XmlDocument doc = new XmlDocument();
            doc.Load(fileUpload.InputStream);
            XmlDocumentDecoder decoder = new XmlDocumentDecoder(doc.DocumentElement);

            section.Decode(decoder);

            section.ChangeId(Guid.NewGuid());

            alternate.SaveResource(alternateResult);

            return Json("success");
        }

        public ActionResult Move(Guid sectionid, Guid itemId, Guid originalParentId, Guid newParentId, int newIndex)
        {
            IAlternateHandler alternate = GetAlternateHandler();
            IAlternateHandlerResult alternateResult = alternate.GetResource(sectionid);

            Section section = (Section)alternateResult.Resource;

            Section originalParent = (Section)(from s in section.AsDepthFirstEnumerable() where ((IUniqueNode)s).Id == originalParentId select s).First();

            var news = (from s in section.AsDepthFirstEnumerable() where ((IUniqueNode)s).Id == newParentId select s);

            Section newParent = null;

            if (news.Count() > 0)
            {
                newParent = (Section)news.First();
            }
            else
            {
                newParent = (Section)(from s in alternate.GetDocument(alternateResult).Sections where ((IUniqueNode)s).Id == newParentId select s).First();
            }

            IPresentable presentable = (from s in section.AsDepthFirstEnumerable() where ((IUniqueNode)s).Id == itemId select s).First();

            originalParent.Children.Remove(presentable);
            newParent.Children.Insert(newIndex, presentable);

            alternate.SaveResource(alternateResult);

            IRepositorySectionManagerItem item = RepositorySectionManager.Current.ItemForObject(presentable);

            ViewBag.ActionTaken = ActionTaken.Move;

            var result = new GetResourceDesignerUIModel()
            {
                DesignerType = item,
                Resource = (IResource)presentable,
                RootId = sectionid,
                Languages = OLanguageRepository.GetAll()
            };

            alternate.SetModelForHandler(result);
            result.ContextFields.Add(new ControllerWithAlternateHandlerContextField() { Name = RootIdContextFieldName, Value = sectionid.ToString() });

            return PartialView("RenderResourceDesignerUI", result);
        }

        public ActionResult Handle(string method, params object[] models)
        {
            return SavePresented((ResourceEditorModel)models[0]);    
        }

        public ActionResult SavePresented(ResourceEditorModel model)
        {
            var id = Guid.Parse((from c in model.ContextFields where c.Name == RootIdContextFieldName select c.Value).First());

            IAlternateHandler alternate = GetAlternateHandler();
            IAlternateHandlerResult alternateResult = alternate.GetResource(id);

            Section section = (Section)alternateResult.Resource;

            IResource child = (IResource)(from s in section.AsDepthFirstEnumerable() where ((IUniqueNode)s).Id == model.Id select s).First();

            model.SetResourceValues(child);

            alternate.SaveResource(alternateResult);

            IRepositorySectionManagerItem item = RepositorySectionManager.Current.ItemForObject(child);

            ViewBag.ActionTaken = ActionTaken.Save;

            var result = new GetResourceDesignerUIModel() 
            {
                DesignerType = item,
                Resource = child,
                Languages = OLanguageRepository.GetAll()
            };

            alternate.SetModelForHandler(result);
            result.ContextFields.Add(new ControllerWithAlternateHandlerContextField() { Name = RootIdContextFieldName, Value = id.ToString() });

            return PartialView("RenderResourceDesignerUI", result);
        }

        public IAlternateHandlerResult GetResource(Guid id)
        {
            List<ControllerWithAlternateHandlerContextField> contexts = ContextList();
            string rootId = (from c in contexts where c.Name == RootIdContextFieldName select c.Value).First();

            if (string.IsNullOrEmpty(rootId))
            {
                throw new InvalidOperationException("The root id must be supplied");
            }

            ResourceEntity sectionEntity = ResourceRepository.GetByNodeId(Guid.Parse(rootId), ResourceLoadInstructions.Data);
            Section section = (Section)ResourceRepository.ResourceFromEntity(sectionEntity);

            IResource child = (IResource)(from s in section.AsDepthFirstEnumerable() where ((IUniqueNode)s).Id == id select s).First();

            return new EditSectionResourceControllerAlternateHandlerResult() { Section = section, Resource = child, Entity = sectionEntity };
        }

        public void SaveResource(IAlternateHandlerResult result)
        {
            EditSectionResourceControllerAlternateHandlerResult r = (EditSectionResourceControllerAlternateHandlerResult)result;
            ResourceRepository.Save(r.Section, r.Entity, true, true);
        }

        public void SetModelForHandler(IModelFromAlternateHandler model)
        {
            model.AlternateHandler = CurrentAlternateHandlerName;
            model.AlternateHandlerMethod = CurrentAlternateHandlerMethodName;
            model.ContextFields = ContextList();
        }


        public IDesignDocument GetDocument(IAlternateHandlerResult result)
        {
            return null;
        }
    }
}
