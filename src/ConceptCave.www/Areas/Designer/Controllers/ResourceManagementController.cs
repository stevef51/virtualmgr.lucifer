﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.BusinessLogic.Configuration;
using ConceptCave.Checklist.Core;
using ConceptCave.Data.DTOConverters;
using ConceptCave.www.Areas.Designer.Models;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Repository;
using ConceptCave.Core;
using RepositorySectionManager = ConceptCave.Repository.RepositorySectionManager;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.Repository.Injected;

namespace ConceptCave.www.Areas.Designer.Controllers
{
    public enum ActionTaken
    {
        Save,
        New,
        Remove,
        Move,
        Error,
        Reload
    }


    [Authorize(Roles = RoleRepository.COREROLE_RESOURCES)]
    public class ResourceManagementController : ControllerWithAlternateHandler, IAlternateHandler
    {
        public class ResourceManagementControllerHandlerResult : IAlternateHandlerResult
        {
            public ResourceEntity Entity { get; set; }
            public IResource Resource { get; set; }
        }

        public static string QuestionModelTypeFieldName = "resourcetype";
        public static string QuestionIdFieldName = "resourceid";
        public static string ResourceNameFieldName = "Name";

        //
        // GET: /Designer/ResourceEditor/

        public ActionResult Index(string term, int? pageNumber, int? pageSize)
        {
            string search = string.Empty;

            if (string.IsNullOrEmpty(term) == false)
            {
                search = term;
            }

            int page = pageNumber.HasValue ? pageNumber.Value : 1;
            int pageS = pageSize.HasValue ? pageSize.Value : 10;

            EntityCollection<ResourceEntity> resources = ResourceRepository.GetLikeName("%" + search + "%", null, ResourceLoadInstructions.ResourceLabel | ResourceLoadInstructions.Label, page, pageS);

            List<ResourceEditorModel> questions = new List<ResourceEditorModel>();

            foreach (var resource in resources)
            {
                IRepositorySectionManagerItem item = RepositorySectionManager.Current.ItemForResourceEntity(resource.ToDTO());

                ResourceEditorModel q = (ResourceEditorModel)item.CreateEditorModel();
                q.FromResourceEntity(resource, item);

                questions.Add(q);
            }

            if (Request.IsAjaxRequest())
            {
                return PartialView("RenderResourceList", questions);
            }

            ResourceManagerModel model = new ResourceManagerModel() { Resources = questions, AvailableResourceTypes = RepositorySectionManager.Current.Items };

            return View(model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Save([ModelBinder(typeof(ResourceModelBinder))] ResourceEditorModel model)
        {
            // we could end up here by having a resource that is part of another resource being saved (so for example a free text question that is presented by a section).
            // the AlternateHandler defines who is responsible for handling the save action, if its defined, then defer handling off to them
            IAlternateHandler handler = CreateAlternateHandler();
            
            if (handler != null)
            {
                return handler.Handle(model.AlternateHandlerMethod, model);
            }

            return Handle("Save", model);
        }

        [HttpPost]
        public ActionResult Add(string key)
        {
            if (string.IsNullOrEmpty(key))
            {
                key = "Checklist Design";
            }

            IRepositorySectionManagerItem item = RepositorySectionManager.Current.ItemForFriendlyType(key);

            IResource resource = item.CreateResource();

            resource.Name = "New " + item.EditorNewPromptText;

            ResourceEntity resourceEntity = ResourceRepository.New(resource);

            ResourceRepository.Save(resource, resourceEntity, true, true);

            ViewBag.ActionTaken = ActionTaken.New;

            return PartialView("RenderResourceDesignerUI", new GetResourceDesignerUIModel() 
            { 
                DesignerType = item, 
                Resource = resource, 
                IsNameMandatory = true, 
                Labels = new www.Models.SelectMultipleLabelsModel(LabelRepository.GetAll(LabelFor.Resources), null),
                Languages = OLanguageRepository.GetAll()
            });
        }

        [HttpPost]
        public ActionResult Remove(Guid id)
        {
            ResourceEntity resourceEntity = ResourceRepository.GetByNodeId(id, ResourceLoadInstructions.Data);
            IRepositorySectionManagerItem item = RepositorySectionManager.Current.ItemForFriendlyType(resourceEntity.Type);

            if (resourceEntity == null)
            {
                throw new ArgumentException("No question with that id exists");
            }

            IResource resource = ResourceRepository.ResourceFromEntity(resourceEntity);

            ResourceRepository.Remove(id);

            ViewBag.ActionTaken = ActionTaken.Remove;

            // hand in the question so the view can get at the information that was deleted
            return PartialView("RenderResourceDesignerUI", new GetResourceDesignerUIModel() 
            { 
                DesignerType = item, 
                Resource = resource, 
                Labels = new www.Models.SelectMultipleLabelsModel(LabelRepository.GetAll(LabelFor.Resources), null),
                Languages = OLanguageRepository.GetAll()
            });
        }

        public ActionResult Handle(string method, params object[] models)
        {
            ResourceEditorModel model = (ResourceEditorModel)models[0];

            ResourceEntity resourceEntity = ResourceRepository.GetByNodeId(model.Id, ResourceLoadInstructions.Data);

            if (resourceEntity == null)
            {
                throw new ArgumentException("No resource with that id exists");
            }

            IResource resource = ResourceRepository.ResourceFromEntity(resourceEntity);

            model.SetResourceValues(resource);

            ResourceRepository.Save(resource, resourceEntity, true, true);

            ViewBag.ActionTaken = ActionTaken.Save;

            return PartialView("RenderResourceDesignerUI", new GetResourceDesignerUIModel() 
            { 
                DesignerType = model.DesignerType, 
                Resource = resource, 
                Languages = OLanguageRepository.GetAll()
            });
        }

        public IAlternateHandlerResult GetResource(Guid id)
        {
            ResourceEntity resourceEntity = ResourceRepository.GetByNodeId(id, ResourceLoadInstructions.Data);
            return new ResourceManagementControllerHandlerResult() { Resource = ResourceRepository.ResourceFromEntity(resourceEntity), Entity = resourceEntity };
        }

        public void SaveResource(IAlternateHandlerResult result)
        {
            ResourceManagementControllerHandlerResult r = (ResourceManagementControllerHandlerResult)result;
            ResourceRepository.Save(r.Resource, r.Entity, true, true);
        }


        public void SetModelForHandler(IModelFromAlternateHandler model)
        {
            // we don't do anything
        }


        public IDesignDocument GetDocument(IAlternateHandlerResult result)
        {
            return null;
        }
    }

    public class ResourceModelBinder : DefaultModelBinder
    {
        protected override object CreateModel(ControllerContext controllerContext, ModelBindingContext bindingContext, Type modelType)
        {
            var resourceModelType = bindingContext.ValueProvider.GetValue(ResourceManagementController.QuestionModelTypeFieldName);

            IRepositorySectionManagerItem item = RepositorySectionManager.Current.ItemForAssemblyQualifiedName(resourceModelType.AttemptedValue);

            ResourceEditorModel model = (ResourceEditorModel)item.CreateEditorModel();
            
            model.PopulateFromValueProvider(bindingContext.ValueProvider, item);

            return model;
        }
    }
}
