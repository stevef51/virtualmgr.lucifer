﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.BusinessLogic.Configuration;
using ConceptCave.Checklist.Editor;
using ConceptCave.Checklist.Validators;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.www.Areas.Designer.Models;
using ConceptCave.www.Models;
using RepositorySectionManager = ConceptCave.Repository.RepositorySectionManager;


namespace ConceptCave.www.Areas.Designer.Controllers
{
    public class ValidatorManagementController : ControllerWithAlternateHandler
    {
        [HttpPost]
        public ActionResult New(Guid questionId, string type)
        {
            IAlternateHandler handler = CreateAlternateHandler();

            IAlternateHandlerResult result = handler.GetResource(questionId);

            Question question = (Question)result.Resource;

            IRepositorySectionManagerItem item = RepositorySectionManager.Current.ItemForObject(question);

            IRepositorySectionManagerRuleItem validatorItem = item.ValidatorItemForType(type);

            IProcessingRule validator = validatorItem.CreateRule();

            question.Validators.Add(validator);

            handler.SaveResource(result);

            ViewBag.ActionTaken = ActionTaken.New;

            var r = new GetResourceDesignerUIModel()
            {
                DesignerType = item,
                Resource = question,
                RootId = questionId
            };

            handler.SetModelForHandler(r);

            return PartialView(validator.GetType().Name + "Editor", new ValidatorEditorModel() { ResourceModel = r, Validator = validator });
        }

        [HttpPost]
        public ActionResult Delete(Guid questionId, Guid id)
        {
            IAlternateHandler handler = CreateAlternateHandler();

            IAlternateHandlerResult result = handler.GetResource(questionId);

            Question question = (Question)result.Resource;

            IProcessingRule validator = (from v in question.Validators where v.Id == id select v).First();

            question.Validators.Remove(validator);

            handler.SaveResource(result);

            ViewBag.ActionTaken = ActionTaken.Remove;

            IRepositorySectionManagerItem item = RepositorySectionManager.Current.ItemForObject(question);

            var r = new GetResourceDesignerUIModel()
            {
                DesignerType = item,
                Resource = question,
                RootId = questionId
            };

            handler.SetModelForHandler(r);

            return PartialView(new ValidatorEditorModel() { ResourceModel = r, Validator = validator });
        }

        [HttpPost]
        public ActionResult Save([ModelBinder(typeof(JSONDataModelBinder))] JSONDataModel model)
        {
            IAlternateHandler handler = CreateAlternateHandler();

            IAlternateHandlerResult result = handler.GetResource(new Guid(model.RootId));

            Question question = (Question)result.Resource;

            IProcessingRule validator = (from v in question.Validators where v.Id == new Guid(model.Items[0].Id) select v).First();

            IRepositorySectionManagerItem item = RepositorySectionManager.Current.ItemForObject(question);

            IRepositorySectionManagerRuleItem validatorItem = item.ValidatorItemForObject(validator);

            IJSONDataModelItemConverter converter = (IJSONDataModelItemConverter)validatorItem.CreateEditorModel();

            converter.Convert(model.Items[0], validator);

            handler.SaveResource(result);

            ViewBag.ActionTaken = ActionTaken.Save;

            var r = new GetResourceDesignerUIModel()
            {
                DesignerType = item,
                Resource = question,
                RootId = new Guid(model.RootId)
            };

            handler.SetModelForHandler(r);

            return PartialView(validator.GetType().Name + "Editor", new ValidatorEditorModel() { ResourceModel = r, Validator = validator });
        }
    }
}
