﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.www.Areas.Designer.Models;
using ConceptCave.Repository;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Core;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.Repository.Injected;

namespace ConceptCave.www.Areas.Designer.Controllers
{
    public class GetResourceDesignerUIController : ControllerBase
    {
        //
        // GET: /Designer/GetQuestionDesignerUI/

        public ActionResult Index(Guid id)
        {
            ResourceEntity resourceEntity = ResourceRepository.GetByNodeId(id, ResourceLoadInstructions.Data | ResourceLoadInstructions.ResourceLabel | ResourceLoadInstructions.Label);

            IResource resource = ResourceRepository.ResourceFromEntity(resourceEntity);

            return View(new GetResourceDesignerUIModel() 
            { 
                Resource = resource, 
                DesignerType = RepositorySectionManager.Current.ItemForObject(resource), 
                IsRoot = true, 
                RootId = id,
                IsNameMandatory = true,
                Labels = new www.Models.SelectMultipleLabelsModel(LabelRepository.GetAll(LabelFor.Resources), (from l in resourceEntity.LabelResources select l.Label)),
                SelectedLabels = (from l in resourceEntity.LabelResources select l.Label),
                Languages = OLanguageRepository.GetAll()
            });
        }

    }
}
