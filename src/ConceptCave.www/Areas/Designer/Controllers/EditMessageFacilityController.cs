﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.Repository.Facilities;
using ConceptCave.www.Areas.Designer.Models;

namespace ConceptCave.www.Areas.Designer.Controllers
{
    public class EditMessageFacilityController : ControllerWithAlternateHandler
    {
        //I have sweet NFI what this does:
        private IAlternateHandler _currentAlternateHandler;

        protected IAlternateHandler CurrentAlternateHandler
        {
            get
            {
                if (_currentAlternateHandler == null)
                {
                    _currentAlternateHandler = CreateAlternateHandler();
                }

                return _currentAlternateHandler;
            }
        }

        protected IAlternateHandlerResult GetFacilityFromHandler(Guid questionId)
        {
            return CurrentAlternateHandler.GetResource(questionId);
        }

        protected void SaveFacilityToHandler(IAlternateHandlerResult result)
        {
            CurrentAlternateHandler.SaveResource(result);
        }

        //
        // GET: /Designer/EditMessageFacility/

        //Routed!
        public ActionResult Index(Guid facilityId, Guid? templateId)
        {
            if (templateId.HasValue)
                return Index(facilityId, templateId.Value);
            else
                return Index(facilityId);
        }

        //Not actually routed
        private ActionResult Index(Guid facilityId)
        {
            IAlternateHandlerResult result = GetFacilityFromHandler(facilityId);
            var facility = (MessagingFacility)result.Resource;

            var templateName = "New Template";
            if (facility.Templates.Any(t => t.Name.ToLower() == templateName.ToLower()))
            {
                //There is already "New Template", better make "New Template 2"
                int tnumber = 2;
                while (facility.Templates.Any(t => t.Name.ToLower() == templateName.ToLower() + tnumber))
                    tnumber++;
                templateName = "New Template" + tnumber;
            }

            var model = new EditMessageFacilityTemplateModel();
            model.Name = templateName;
            model.FacilityId = facilityId;
            CurrentAlternateHandler.SetModelForHandler(model);
            return View(model);
        }

        //Not actually routed
        private ActionResult Index(Guid facilityId, Guid templateId)
        {
            IAlternateHandlerResult result = GetFacilityFromHandler(facilityId);
            var facility = (MessagingFacility)result.Resource;
            MessagingFacility.MessagingTemplate template;

            try {
                template = facility.Templates.First(t => t.Id == templateId);
            }
            catch (InvalidOperationException ex) {
                throw new HttpException("Template with this Id not found", ex);
            }

            var model = new EditMessageFacilityTemplateModel(template, facilityId);
            CurrentAlternateHandler.SetModelForHandler(model);
            return View(model);
        }

        [HttpPost]
        public ActionResult Save(Guid facilityId, EditMessageFacilityTemplateModel newTemplate)
        {
            IAlternateHandlerResult result = GetFacilityFromHandler(facilityId);
            var facility = (MessagingFacility)result.Resource;
            
            //Is this template edit or new?
            MessagingFacility.MessagingTemplate template;
            if (newTemplate.Id == null) //new
            {
                template = new MessagingFacility.MessagingTemplate();
                if (facility.Templates.Any(t => t.Name.ToLower() == newTemplate.Name.ToLower()))
                    throw new HttpException("Already have a template with this name");
                facility.Templates.Add(template);
            }
            else //edit
            {
                template = facility.Templates.First(t => t.Id == newTemplate.Id);
                if (template == null)
                    throw new HttpException("Unknown template to edit"); //this is bad
                if (facility.Templates.Any(t => t.Name.ToLower() == newTemplate.Name.ToLower() &&
                    t.Id != newTemplate.Id))
                    throw new HttpException("Already have a template with this name");
            }
            template.Name = newTemplate.Name;
            template.SetSenderId(newTemplate.Sender);
            template.SetRecipientLabelIds(newTemplate.Labels);
            template.SetRecipientUserIds(newTemplate.Users.Keys);
            template.RequiresAcknowledgement = newTemplate.RequiresAcknowledgement;
            SaveFacilityToHandler(result);

            return View("SendMessageTemplateItem", template);
        }

        [HttpPost]
        public ActionResult Delete(Guid facilityId, Guid templateId)
        {
            IAlternateHandlerResult result = GetFacilityFromHandler(facilityId);
            var facility = (MessagingFacility)result.Resource;
            var deltemplate = facility.Templates.First(t => t.Id == templateId);
            facility.Templates.Remove(deltemplate);
            SaveFacilityToHandler(result);
            return Content("Success");
        }


    }
}
