﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.Checklist.Lingo;
using ConceptCave.www.Areas.Designer.Models;


namespace ConceptCave.www.Areas.Designer.Controllers
{
    public class SourcecodeController : ControllerBase
    {
        //
        // GET: /Designer/Sourcecode/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SyntaxCheck(string code)
        {
            LingoProgramDefinition program = new LingoProgramDefinition();
            program.SourceCode = code;
            Irony.Parsing.ParseTree parseTree = program.SyntaxCheck();

            return this.Json(new SyntaxCheckModel(parseTree));
        }

    }
}
