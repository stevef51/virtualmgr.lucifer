﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.BusinessLogic.Configuration;
using ConceptCave.Data.DTOConverters;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Repository;
using ConceptCave.www.Areas.Designer.Models;
using RepositorySectionManager = ConceptCave.Repository.RepositorySectionManager;

namespace ConceptCave.www.Areas.Designer.Controllers
{
    public class GetDesignDocumentListItemController : ControllerBase
    {
        //
        // GET: /Designer/GetDesignDocumentListItem/

        public ActionResult Index(Guid id)
        {
            ResourceEntity resourceEntity = ResourceRepository.GetByNodeId(id, ResourceLoadInstructions.None);

            IRepositorySectionManagerItem item = RepositorySectionManager.Current.ItemForResourceEntity(resourceEntity.ToDTO());

            DesignDocumentManagerModelItem q = (DesignDocumentManagerModelItem)item.CreateEditorModel();
            q.FromResourceEntity(resourceEntity, item);

            return View(q);
        }

    }
}
