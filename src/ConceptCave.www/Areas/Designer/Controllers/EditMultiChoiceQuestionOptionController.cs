﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.www.Areas.Designer.Models;
using ConceptCave.Checklist.Core;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Repository;
using ConceptCave.Core;
using ConceptCave.Checklist.Editor;
using ConceptCave.Repository.Injected;

namespace ConceptCave.www.Areas.Designer.Controllers
{
    public class EditMultiChoiceQuestionOptionController : ControllerWithAlternateHandler
    {
        private IAlternateHandler _currentAlternateHandler;

        protected IAlternateHandler CurrentAlternateHandler {
            get
            {
                if (_currentAlternateHandler == null)
                {
                    _currentAlternateHandler = CreateAlternateHandler();
                }

                return _currentAlternateHandler;
            }
       }

        protected IAlternateHandlerResult GetQuestionFromHandler(Guid questionId)
        {
            return CurrentAlternateHandler.GetResource(questionId);
        }

        protected void SaveQuestionToHandler(IAlternateHandlerResult result)
        {
            CurrentAlternateHandler.SaveResource(result);
        }

        public ActionResult Index(Guid questionId, Guid id)
        {
            IAlternateHandlerResult result = GetQuestionFromHandler(questionId);

            IMultiChoiceQuestion question = (IMultiChoiceQuestion)result.Resource;

            var option = (from o in question.List.Items where o.Id == id select o).First();

            EditMultiChoiceQuestionOptionModel model = new EditMultiChoiceQuestionOptionModel() { Languages = OLanguageRepository.GetAll() };
            model.FromMultiChoiceOption(questionId, option);
            CurrentAlternateHandler.SetModelForHandler(model);

            return View(model);
        }

        public ActionResult GetListItem(Guid questionId, Guid id)
        {
            IAlternateHandlerResult result = GetQuestionFromHandler(questionId);

            IMultiChoiceQuestion question = (IMultiChoiceQuestion)result.Resource;

            var option = (from o in question.List.Items where o.Id == id select o).First();

            return View("MultiChoiceQuestionOptionListItem", option);
        }

        [HttpPost]
        public ActionResult New(Guid questionId)
        {
            IAlternateHandlerResult result = GetQuestionFromHandler(questionId);

            IMultiChoiceQuestion question = (IMultiChoiceQuestion)result.Resource;

            MultiChoiceQuestion.Item item = new MultiChoiceQuestion.Item() { Answer = "New option" };
            question.List.Items.Add(item);

            SaveQuestionToHandler(result);

            EditMultiChoiceQuestionOptionModel model = new EditMultiChoiceQuestionOptionModel() { AlternateHandler = CurrentAlternateHandlerName, Languages = OLanguageRepository.GetAll() };
            model.FromMultiChoiceOption(questionId, item);
            CurrentAlternateHandler.SetModelForHandler(model);

            return View("Index", model);
        }

        [HttpPost]
        public ActionResult Save([ModelBinder(typeof(EditMultiChoiceQuestionOptionModelBinder))] EditMultiChoiceQuestionOptionModel model)
        {
            IAlternateHandlerResult result = GetQuestionFromHandler(model.QuestionId);

            IMultiChoiceQuestion question = (IMultiChoiceQuestion)result.Resource;

            var option = (from o in question.List.Items where o.Id == model.Id select o).First();

            model.ToMultiChoiceOption(option);
            model.Resource = option;

            SaveQuestionToHandler(result);

            ViewBag.ActionTaken = ActionTaken.Save;

            return View("Index", model);
        }

        [HttpPost]
        public ActionResult Remove(Guid questionId, Guid id)
        {
            IAlternateHandlerResult result = GetQuestionFromHandler(questionId);

            IMultiChoiceQuestion question = (IMultiChoiceQuestion)result.Resource;

            var option = (from o in question.List.Items where o.Id == id select o).First();

            question.List.Items.Remove(option);

            SaveQuestionToHandler(result);

            ViewBag.ActionTaken = ActionTaken.Remove;

            return View("MultiChoiceQuestionOptionListItem", option);
        }
    }
}
