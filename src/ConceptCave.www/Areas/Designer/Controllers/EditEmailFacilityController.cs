﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.BusinessLogic.Facilities;
using ConceptCave.Checklist.Facilities;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.www.Areas.Designer.Models;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Repository;

namespace ConceptCave.www.Areas.Designer.Controllers
{
    public class EditEmailFacilityController : ControllerWithAlternateHandler
    {
        public EditEmailFacilityController(IMediaManager mediaMgr, IEmailConfiguration emailConfiguration, ISendGridConfiguration sendGridConfiguration)
        {
            _mediaMgr = mediaMgr;
            _emailConfiguration = emailConfiguration;
            _sendGridConfiguration = sendGridConfiguration;
        }

        private IAlternateHandler _currentAlternateHandler;
        private readonly IMediaManager _mediaMgr;
        private IEmailConfiguration _emailConfiguration;
        private ISendGridConfiguration _sendGridConfiguration;

        protected IAlternateHandler CurrentAlternateHandler
        {
            get
            {
                if (_currentAlternateHandler == null)
                {
                    _currentAlternateHandler = CreateAlternateHandler();
                }

                return _currentAlternateHandler;
            }
        }

        protected IAlternateHandlerResult GetFacilityFromHandler(Guid questionId)
        {
            return CurrentAlternateHandler.GetResource(questionId);
        }

        protected void SaveFacilityToHandler(IAlternateHandlerResult result)
        {
            CurrentAlternateHandler.SaveResource(result);
        }

        public ActionResult Index(Guid facilityId, Guid templateId)
        {
            IAlternateHandlerResult result = GetFacilityFromHandler(facilityId);

            EmailFacility fac = (EmailFacility)result.Resource;

            var template = (from t in fac.Templates where t.Id == templateId select t).DefaultIfEmpty(null).First();

            if (template == null)
            {
                throw new HttpException(404, "Template with that id could not be found on the facility");
            }

            EditEmailFacilityModel model = new EditEmailFacilityModel(template, facilityId) { AlternateHandler = CurrentAlternateHandlerName };

            CurrentAlternateHandler.SetModelForHandler(model);

            return View("Index", model);
        }

        [HttpPost]
        public ActionResult New(Guid facilityId)
        {
            IAlternateHandlerResult result = GetFacilityFromHandler(facilityId);

            EmailFacility fac = (EmailFacility)result.Resource;

            if ((from t in fac.Templates where t.Name.ToLower() == "new template" select t).Count() > 0)
            {
                throw new HttpException(403, "There is already a template with the name 'new template' in this facility");
            }

            var template = new ConceptCave.Checklist.Facilities.EmailFacility.EmailTemplate(_emailConfiguration, _sendGridConfiguration)
            {
                Name = "New Template",
            };

            fac.Templates.Add(template);

            SaveFacilityToHandler(result);

            EditEmailFacilityModel model = new EditEmailFacilityModel(template, facilityId) { AlternateHandler = CurrentAlternateHandlerName };

            CurrentAlternateHandler.SetModelForHandler(model);

            return View("Index", model);
        }

        public ActionResult GetTemplateItem(Guid facilityId, Guid templateId)
        {
            IAlternateHandlerResult result = GetFacilityFromHandler(facilityId);

            EmailFacility fac = (EmailFacility)result.Resource;

            var template = (from t in fac.Templates where t.Id == templateId select t).DefaultIfEmpty(null).First();

            if (template == null)
            {
                throw new HttpException(404, "Template with that id could not be found on the facility");
            }

            return View("SendEmailActionTemplateItem", template);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Save([ModelBinder(typeof(EditEmailFacilityModelBinder))] EditEmailFacilityModel model)
        {
            IAlternateHandlerResult result = GetFacilityFromHandler(model.FacilityId);

            EmailFacility fac = (EmailFacility)result.Resource;

            var template = (from t in fac.Templates where t.Id == model.Id select t).DefaultIfEmpty(null).First();

            if (template == null)
            {
                throw new HttpException(404, "Template with that id could not be found on the facility");
            }

            template.Name = model.Name;
            template.Subject = model.Subject;
            template.To = model.To;
            template.Body = model.Body;
            template.From = model.From;
            template.SendImmediately = model.SendImmediately;

            SaveFacilityToHandler(result);

            ViewBag.ActionTaken = ActionTaken.Save;

            return View("Index", model);
        }

        [HttpPost]
        public ActionResult Remove(Guid facilityId, Guid templateId)
        {
            IAlternateHandlerResult result = GetFacilityFromHandler(facilityId);

            EmailFacility fac = (EmailFacility)result.Resource;

            var template = (from t in fac.Templates where t.Id == templateId select t).DefaultIfEmpty(null).First();

            if (template == null)
            {
                throw new HttpException(404, "Template with that id could not be found on the facility");
            }

            fac.Templates.Remove(template);

            SaveFacilityToHandler(result);

            ViewBag.ActionTaken = ActionTaken.Remove;

            return View("SendEmailActionTemplateItem", template);
        }

        [HttpPost]
        public ActionResult AddAttachment(Guid facilityId, Guid templateId, Guid mediaId)
        {
            IAlternateHandlerResult result = GetFacilityFromHandler(facilityId);

            EmailFacility fac = (EmailFacility)result.Resource;

            var template = (from t in fac.Templates where t.Id == templateId select t).DefaultIfEmpty(null).First();

            if (template == null)
            {
                throw new HttpException(404, "Template with that id could not be found on the facility");
            }

            MediaEntity media = MediaRepository.GetById(mediaId, MediaLoadInstruction.None);

            MediaFacility.LibraryMediaItem item = new MediaFacility.LibraryMediaItem(_mediaMgr) { MediaId = mediaId, Name = media.Name, MediaType = media.Type };
            template.Attachments.Add(item);

            SaveFacilityToHandler(result);
            
            EditEmailFacilityModel model = new EditEmailFacilityModel(template, facilityId) { AlternateHandler = CurrentAlternateHandlerName };

            return View("SendEmailActionTemplateAttachmentList", model);
        }

        [HttpPost]
        public ActionResult RemoveAttachment(Guid facilityId, Guid templateId, Guid attachmentId)
        {
            IAlternateHandlerResult result = GetFacilityFromHandler(facilityId);

            EmailFacility fac = (EmailFacility)result.Resource;

            var template = (from t in fac.Templates where t.Id == templateId select t).DefaultIfEmpty(null).First();

            if (template == null)
            {
                throw new HttpException(404, "Template with that id could not be found on the facility");
            }

            var attach = (from a in template.Attachments where a.Id == attachmentId select a).DefaultIfEmpty(null).First();

            if (attach == null)
            {
                throw new HttpException(404, "Attachment with that id could not be found on the template");
            }

            template.Attachments.Remove(attach);

            SaveFacilityToHandler(result);

            EditEmailFacilityModel model = new EditEmailFacilityModel(template, facilityId) { AlternateHandler = CurrentAlternateHandlerName };

            return View("SendEmailActionTemplateAttachmentList", model);
        }
    }
}
