﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.Checklist.Facilities;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Repository.Facilities;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.www.Areas.Designer.Models;
using ConceptCave.Repository;

namespace ConceptCave.www.Areas.Designer.Controllers
{
    public class EditWordDocumentFacilityController : ControllerWithAlternateHandler
    {
        public EditWordDocumentFacilityController(IMediaManager mediaMgr)
        {
            _mediaMgr = mediaMgr;
        }

        private IAlternateHandler _currentAlternateHandler;
        private readonly IMediaManager _mediaMgr;

        protected IAlternateHandler CurrentAlternateHandler
        {
            get
            {
                if (_currentAlternateHandler == null)
                {
                    _currentAlternateHandler = CreateAlternateHandler();
                }

                return _currentAlternateHandler;
            }
        }

        protected IAlternateHandlerResult GetFacilityFromHandler(Guid questionId)
        {
            return CurrentAlternateHandler.GetResource(questionId);
        }

        protected void SaveFacilityToHandler(IAlternateHandlerResult result)
        {
            CurrentAlternateHandler.SaveResource(result);
        }

        public ActionResult Index(Guid facilityId, Guid templateId)
        {
            IAlternateHandlerResult result = GetFacilityFromHandler(facilityId);

            WordDocumentFacility fac = (WordDocumentFacility)result.Resource;

            var template = (from t in fac.Templates where t.Id == templateId select t).DefaultIfEmpty(null).First();

            if (template == null)
            {
                throw new HttpException(404, "Template with that id could not be found on the facility");
            }

            EditWordDocumentFacilityModel model = new EditWordDocumentFacilityModel(template, facilityId) { AlternateHandler = CurrentAlternateHandlerName };

            CurrentAlternateHandler.SetModelForHandler(model);

            return View("Index", model);
        }

        [HttpPost]
        public ActionResult New(Guid facilityId)
        {
            IAlternateHandlerResult result = GetFacilityFromHandler(facilityId);

            WordDocumentFacility fac = (WordDocumentFacility)result.Resource;

            if ((from t in fac.Templates where t.Name.ToLower() == "new template" select t).Count() > 0)
            {
                throw new HttpException(403, "There is already a template with the name 'new template' in this facility");
            }

            var template = new ConceptCave.Repository.Facilities.WordDocumentFacility.WordDocumentFacilityTemplate(_mediaMgr)
            {
                Name = "New Template",
            };

            fac.Templates.Add(template);

            SaveFacilityToHandler(result);

            EditWordDocumentFacilityModel model = new EditWordDocumentFacilityModel(template, facilityId) { AlternateHandler = CurrentAlternateHandlerName };

            CurrentAlternateHandler.SetModelForHandler(model);

            return View("Index", model);
        }

        public ActionResult GetTemplateItem(Guid facilityId, Guid templateId)
        {
            IAlternateHandlerResult result = GetFacilityFromHandler(facilityId);

            WordDocumentFacility fac = (WordDocumentFacility)result.Resource;

            var template = (from t in fac.Templates where t.Id == templateId select t).DefaultIfEmpty(null).First();

            if (template == null)
            {
                throw new HttpException(404, "Template with that id could not be found on the facility");
            }

            var media = MediaRepository.GetById(template.MediaId, MediaLoadInstruction.None);

            EditWordDocumentFacilityTemplateItemModel model = new EditWordDocumentFacilityTemplateItemModel()
            {
                Template = template,
                Media = media
            };

            return View("WordDocumentFacilityTemplateItem", model);
        }

        [HttpPost]
        public ActionResult Save([ModelBinder(typeof(EditWordDocumentFacilityModelBinder))] EditWordDocumentFacilityModel model)
        {
            IAlternateHandlerResult result = GetFacilityFromHandler(model.FacilityId);

            WordDocumentFacility fac = (WordDocumentFacility)result.Resource;

            var template = (from t in fac.Templates where t.Id == model.Id select t).DefaultIfEmpty(null).First();

            if (template == null)
            {
                throw new HttpException(404, "Template with that id could not be found on the facility");
            }

            template.Name = model.Name;
            template.MediaId = model.MediaId;

            SaveFacilityToHandler(result);

            ViewBag.ActionTaken = ActionTaken.Save;

            return View("Index", model);
        }

        [HttpPost]
        public ActionResult Remove(Guid facilityId, Guid templateId)
        {
            IAlternateHandlerResult result = GetFacilityFromHandler(facilityId);

            WordDocumentFacility fac = (WordDocumentFacility)result.Resource;

            var template = (from t in fac.Templates where t.Id == templateId select t).DefaultIfEmpty(null).First();

            if (template == null)
            {
                throw new HttpException(404, "Template with that id could not be found on the facility");
            }

            fac.Templates.Remove(template);

            SaveFacilityToHandler(result);

            ViewBag.ActionTaken = ActionTaken.Remove;

            EditWordDocumentFacilityTemplateItemModel model = new EditWordDocumentFacilityTemplateItemModel()
            {
                Template = template
            };

            return View("WordDocumentFacilityTemplateItem", model);
        }

    }
}
