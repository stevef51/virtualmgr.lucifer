﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.BusinessLogic.Facilities;
using ConceptCave.Checklist.Facilities;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.www.Areas.Designer.Models;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Repository;

namespace ConceptCave.www.Areas.Designer.Controllers
{
    public class EditEWayPaymentFacilityController : ControllerWithAlternateHandler
    {
        public EditEWayPaymentFacilityController()
        {
        }

        private IAlternateHandler _currentAlternateHandler;

        protected IAlternateHandler CurrentAlternateHandler
        {
            get
            {
                if (_currentAlternateHandler == null)
                {
                    _currentAlternateHandler = CreateAlternateHandler();
                }

                return _currentAlternateHandler;
            }
        }

        protected IAlternateHandlerResult GetFacilityFromHandler(Guid questionId)
        {
            return CurrentAlternateHandler.GetResource(questionId);
        }

        protected void SaveFacilityToHandler(IAlternateHandlerResult result)
        {
            CurrentAlternateHandler.SaveResource(result);
        }

        public ActionResult Index(Guid facilityId, Guid templateId)
        {
            IAlternateHandlerResult result = GetFacilityFromHandler(facilityId);

            eWayPaymentFacility fac = (eWayPaymentFacility)result.Resource;

            var template = (from t in fac.Templates where t.Id == templateId select t).DefaultIfEmpty(null).First();

            if (template == null)
            {
                throw new HttpException(404, "Template with that id could not be found on the facility");
            }

            var model = new EditEwayPaymentTemplateModel(template, facilityId) { AlternateHandler = CurrentAlternateHandlerName };

            CurrentAlternateHandler.SetModelForHandler(model);

            return View("Index", model);
        }

        [HttpPost]
        public ActionResult New(Guid facilityId)
        {
            IAlternateHandlerResult result = GetFacilityFromHandler(facilityId);

            eWayPaymentFacility fac = (eWayPaymentFacility)result.Resource;

            if ((from t in fac.Templates where t.Name.ToLower() == "new template" select t).Count() > 0)
            {
                throw new HttpException(403, "There is already a template with the name 'new template' in this facility");
            }

            var template = new ConceptCave.Checklist.Facilities.eWayPaymentFacility.TransactionTemplate()
            {
                Name = "New Template",
            };

            fac.Templates.Add(template);

            SaveFacilityToHandler(result);

            var model = new EditEwayPaymentTemplateModel(template, facilityId) { AlternateHandler = CurrentAlternateHandlerName };

            CurrentAlternateHandler.SetModelForHandler(model);

            return View("Index", model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Save([ModelBinder(typeof(EditEwayPaymentFacilityModelBinder))] EditEwayPaymentFacilityModel model)
        {
            IAlternateHandlerResult result = GetFacilityFromHandler(model.Id);

            var fac = (eWayPaymentFacility)result.Resource;

            fac.UseSandbox = model.UseSandbox;

            SaveFacilityToHandler(result);

            ViewBag.ActionTaken = ActionTaken.Save;

            return View("Facility", model);
        }

        public ActionResult GetTemplateItem(Guid facilityId, Guid templateId)
        {
            IAlternateHandlerResult result = GetFacilityFromHandler(facilityId);

            var fac = (eWayPaymentFacility)result.Resource;

            var template = (from t in fac.Templates where t.Id == templateId select t).DefaultIfEmpty(null).First();

            if (template == null)
            {
                throw new HttpException(404, "Template with that id could not be found on the facility");
            }

            return View("eWayPaymentActionTemplateItem", template);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SaveTemplate([ModelBinder(typeof(EditEwayPaymentTemplateModelBinder))] EditEwayPaymentTemplateModel model)
        {
            IAlternateHandlerResult result = GetFacilityFromHandler(model.FacilityId);

            var fac = (eWayPaymentFacility)result.Resource;

            var template = (from t in fac.Templates where t.Id == model.Id select t).DefaultIfEmpty(null).First();

            if (template == null)
            {
                throw new HttpException(404, "Template with that id could not be found on the facility");
            }

            template.Name = model.Name;
            template.JSON = model.JSON;

            SaveFacilityToHandler(result);

            ViewBag.ActionTaken = ActionTaken.Save;

            return View("Index", model);
        }

        [HttpPost]
        public ActionResult RemoveTemplate(Guid facilityId, Guid templateId)
        {
            IAlternateHandlerResult result = GetFacilityFromHandler(facilityId);

            var fac = (eWayPaymentFacility)result.Resource;

            var template = (from t in fac.Templates where t.Id == templateId select t).DefaultIfEmpty(null).First();

            if (template == null)
            {
                throw new HttpException(404, "Template with that id could not be found on the facility");
            }

            fac.Templates.Remove(template);

            SaveFacilityToHandler(result);

            ViewBag.ActionTaken = ActionTaken.Remove;

            return View("eWayPaymentActionTemplateItem", template);
        }

    }
}
