using ConceptCave.BusinessLogic.Models;
using ConceptCave.DTO.DTOClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConceptCave.www.Areas.Admin.Models
{
    public class BeaconModel
    {
        public string id { get; set; }
        public string name { get; set; }
        public DateTime? lastcontactutc { get; set; }
        public DateTime? lastbuttonpressedutc { get; set; }
        public int? facilitystructureid { get; set; }
        public int? assetid { get; set; }
        public long? staticlocationid { get; set; }
        public decimal? staticheightfromfloor { get; set; }

        public GpsLocationModel staticlocation { get; set; }

        public static BeaconModel FromDto(BeaconDTO dto)
        {
            return new BeaconModel()
            {
                id = dto.Id,
                name = dto.Name,
                lastcontactutc = dto.LastContactUtc,
                lastbuttonpressedutc = dto.LastButtonPressedUtc,
                assetid = dto.AssetId,
                facilitystructureid = dto.FacilityStructureId,
                staticlocationid = dto.StaticLocationId,
                staticlocation = dto.StaticLocation != null ? GpsLocationModel.FromDTO(dto.StaticLocation) : null,
                staticheightfromfloor = dto.StaticHeightFromFloor
            };
        }
    }
}