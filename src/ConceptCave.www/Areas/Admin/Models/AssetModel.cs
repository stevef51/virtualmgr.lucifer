using ConceptCave.BusinessLogic;
using ConceptCave.BusinessLogic.Context;
using ConceptCave.BusinessLogic.LingoDataObjects;
using ConceptCave.BusinessLogic.Models;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.DTO.DTOClasses;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ConceptCave.www.Areas.Admin.Models
{
    public class AssetScheduleModel
    {
        public int id { get; set; }
        public int assetid { get; set; }
        public AssetTypeCalendarModel calendar { get; set; }
        public int assettypecalendarid { get; set; }
        public DateTime? lastrunutc { get; set; }
        public DateTime? nextrunutc { get; set; }
        public TimeSpan? overridestarttimelocal { get; set; }

        public static AssetScheduleModel FromDto(AssetScheduleDTO dto)
        {
            return new AssetScheduleModel()
            {
                id = dto.Id,
                assetid = dto.AssetId,
                calendar = dto.AssetTypeCalendar != null ? AssetTypeCalendarModel.FromDto(dto.AssetTypeCalendar) : null,
                assettypecalendarid = dto.AssetTypeCalendarId,
                lastrunutc = dto.LastRunUtc,
                nextrunutc = dto.NextRunUtc,
                overridestarttimelocal = dto.OverrideStartTimeLocal
            };
        }
    }

    public class AssetModelContext
    {
        public string Checklist { get; set; }

        public AnswerModel Data { get; set; }
    }

    public class AssetModel
    {
        public int id { get; set; }
        public bool archived { get; set; }
        public string name { get; set; }
        public int assettypeid { get; set; }
        public AssetTypeModel assettype { get; set; }
        public string beaconid { get; set; }
        public BeaconModel beacon { get; set; }
        public DateTime datecreated { get; set; }
        public DateTime? datedecommissioned { get; set; }
        public int? facilitystructureid { get; set; }

        public Guid? companyid { get; set; }

        public List<AssetModelContext> contextanswers { get; set; }

        public List<PlayerModel> contexts { get; set; }
        public List<AssetScheduleModel> schedules { get; set; }

        public static PlayerModel ConstructPlayerModel(IWorkingDocument wdObject)
        {
            var returnModel = new PlayerModel()
            {
                WorkingDocumentId = Guid.Empty,
                ChecklistName = wdObject.Name,
                IsValid = true,
                State = wdObject.RunMode,
                CanPrevious = false,
                RootStepCount = wdObject.Sections.Count
            };

            return returnModel;
        }

        public static AssetModel FromDto(AssetDTO dto, Guid userId, IContextManagerFactory ctxFac = null, RenderConverter rConverter = null)
        {
            var result = new AssetModel()
            {
                id = dto.Id,
                archived = dto.Archived,
                name = dto.Name,
                assettypeid = dto.AssetTypeId,
                assettype = dto.AssetType != null ? AssetTypeModel.FromDto(dto.AssetType, null) : null,
                beaconid = dto.Beacon != null ? dto.Beacon.Id : null,
                beacon = dto.Beacon != null ? BeaconModel.FromDto(dto.Beacon) : null,
                datecreated = dto.DateCreated,
                facilitystructureid = dto.FacilityStructureId,
                companyid = dto.CompanyId,
                datedecommissioned = dto.DateDecomissioned,
                contexts = new List<PlayerModel>(),
                schedules = dto.AssetSchedules.Select(s => AssetScheduleModel.FromDto(s)).ToList()
            };

            if(ctxFac != null)
            {
                var wds = GetAssetContextWorkingDocuments(dto, userId, ctxFac, rConverter);
                foreach(var getter in wds)
                {
                    var doc = getter.GetWorkingDocument(null);
                    var presented = doc.GetPresented();

                    PlayerModel pc = ConstructPlayerModel(doc);
                    pc.Presented = rConverter.MakePresentedRenderable(presented, null, doc);

                    result.contexts.Add(pc);
                }
            }

            return result;
        }

        public static IList<ContextWorkingDocumentObjectGetter> GetAssetContextWorkingDocuments(AssetDTO dto, Guid userId, IContextManagerFactory ctxFac = null, RenderConverter rConverter = null)
        {
            var contextManager = ctxFac.GetInstance(dto, ContextType.Asset, userId);

            var result = new List<ContextWorkingDocumentObjectGetter>();

            foreach (var typeContext in dto.AssetType.AssetTypeContextPublishedResources)
            {
                var contextGetter = contextManager.LoadContextDocument(typeContext.PublishingGroupResourceId);
                result.Add(contextGetter);
            }

            return result;
        }
    }

    /// <summary>
    /// The default MVC model binder was having grief with JObject properties being present in the PlayerModel
    /// so we have to do some heavy lifting ourselves.
    /// </summary>
    public class AssetModelTransformationBinder : IModelBinder
    {
        protected class AssetModelTransformBinderHelper
        {
            public AssetModel record { get; set; }   
        }

        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            // can't find any other way of doing this online (sigh)
            controllerContext.HttpContext.Request.InputStream.Seek(0, System.IO.SeekOrigin.Begin);
            StreamReader reader = new StreamReader(controllerContext.HttpContext.Request.InputStream);
            string json = reader.ReadToEnd();

            AssetModelTransformBinderHelper result = JsonConvert.DeserializeObject<AssetModelTransformBinderHelper>(json);
            return result.record;
        }
    }
}