﻿using ConceptCave.DTO.DTOClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConceptCave.www.Areas.Admin.Models
{
    public class TabletUuidModel
    {
        public bool __isnew { get; set; }
        public string uuid { get; set; }
        public string description { get; set; }
        public int? tabletid { get; set; }
        public DateTime? lastcontactutc { get; set; }

        public static TabletUuidModel FromDto(TabletUuidDTO dto)
        {
            return new TabletUuidModel()
            {
                __isnew = dto.__IsNew,
                uuid = dto.Uuid,
                description = dto.Description,
                tabletid = dto.TabletId,
                lastcontactutc = dto.LastContactUtc
            };
        }
    }
}