﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConceptCave.Data.EntityClasses;
using ConceptCave.www.Models;

namespace ConceptCave.www.Areas.Admin.Models
{
    public class RenderMediaDesignerUIModel
    {
        public MediaEntity Entity { get; set; }

        public SelectMultipleLabelsModel Labels { get; set; }
    }
}