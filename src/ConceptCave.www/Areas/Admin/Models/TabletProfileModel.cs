﻿using ConceptCave.DTO.DTOClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConceptCave.www.Areas.Admin.Models
{
    public enum ConfirmLogoutType
    {
        None = 0,
        SimpleConfirm = 1
    }

    public class TabletProfileModel
    {
        public int id { get; set; }
        public bool archived { get; set; }
        public string name { get; set; }
        public int? autologoutshortsecs { get; set; }
        public int? autologoutlongsecs { get; set; }
        public bool allownumberpadlogin { get; set; }
        public ConfirmLogoutType confirmlogout { get; set; }
        public bool enablefma { get; set; }
        public bool enableenvironmentalsensing { get; set; }
        public string settingspassword { get; set; }
        public string logoutpassword { get; set; }
        public bool enableloginphoto { get; set; }
        public int loginphotoevery { get; set; }

        public static TabletProfileModel FromDto(TabletProfileDTO dto)
        {
            return new TabletProfileModel()
            {
                id = dto.Id,
                archived = dto.Archived,
                name = dto.Name,
                allownumberpadlogin = dto.AllowNumberPadLogin,
                autologoutshortsecs = dto.AutoLogoutShortSecs,
                autologoutlongsecs = dto.AutoLogoutLongSecs,
                confirmlogout = (ConfirmLogoutType)dto.ConfirmLogout,
                enablefma = dto.EnableFma,
                settingspassword = dto.SettingsPassword,
                logoutpassword = dto.LogoutPassword,
                enableenvironmentalsensing = dto.EnableEnvironmentalSensing,
                loginphotoevery = Math.Max(dto.LoginPhotoEvery, 1),
                enableloginphoto = dto.LoginPhotoEvery > 0
            };
        }
    }
}