﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConceptCave.Data.EntityClasses;

namespace ConceptCave.www.Areas.Admin.Models
{
    public class LabelManagementModel
    {
        public List<LabelEntity> Items { get; set; }
    }
}