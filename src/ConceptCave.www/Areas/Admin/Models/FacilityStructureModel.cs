using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConceptCave.www.Areas.Admin.Models
{
    public class FacilityStructureBreadcrumbModel
    {
        public int id { get; set; }
        public string name { get; set; }
        public FacilityStructureType structuretype { get; set; }
    }

    public class FacilityStructureModel
    {
        public int id { get; set; }
        public bool archived { get; set; }
        public string name { get; set; }
        public FacilityStructureType structuretype { get; set; }
        public AddressModel address { get; set; }
        public string description { get; set; }
        public int sortorder { get; set; }
        public int? level { get; set; }
        public decimal? floorHeight { get; set; }
        public int? parentid { get; set; }
        public FacilityStructureModel parent { get; set; }
        public IList<FacilityStructureModel> children { get; set; }
        public Guid? siteid { get; set; }
        public FloorPlanModel floorplan { get; set; }

        public Guid? mediaid { get; set; }
        public int? hierarchyid { get; set; }

        public IList<FacilityStructureBreadcrumbModel> breadcrumbs { get; set; }

        public IList<FacilityStructureProductCatalogModel> Catalogs { get; set; }

        public static IList<FacilityStructureBreadcrumbModel> MakeBreadCrumbs(FacilityStructureDTO dto)
        {
            List<FacilityStructureBreadcrumbModel> crumbs = new List<FacilityStructureBreadcrumbModel>();
            while(dto.Parent != null)
            {
                dto = dto.Parent;
                crumbs.Add(new FacilityStructureBreadcrumbModel()
                    {
                        id = dto.Id,
                        name = dto.Name,
                        structuretype = (FacilityStructureType)dto.StructureType
                    });
            }
            return crumbs;
        }

        public static IList<FacilityStructureProductCatalogModel> MakeCatalogs(FacilityStructureDTO dto)
        {
            List<FacilityStructureProductCatalogModel> result = new List<FacilityStructureProductCatalogModel>();

            foreach(var c in dto.ProductCatalogs)
            {
                result.Add(FacilityStructureProductCatalogModel.FromDto(c, c.FacilityStructureId != dto.Id));
            }

            return result;
        }

        public static FacilityStructureModel FromDto(FacilityStructureDTO dto, bool includeParent, bool includeChildren, bool makeBreadCrumbs = false)
        {
            return new FacilityStructureModel()
            {
                id = dto.Id,
                archived = dto.Archived,
                name = dto.Name,
                structuretype = (FacilityStructureType)dto.StructureType,
                address = dto.Address != null ? AddressModel.FromDto(dto.Address) : null,
                description = dto.Description,
                sortorder = dto.SortOrder,
                level = dto.Level,
                floorHeight = dto.FloorHeight,
                parentid = dto.ParentId,
                parent = includeParent && dto.Parent != null ? FacilityStructureModel.FromDto(dto.Parent, true, false) : null,
                breadcrumbs = makeBreadCrumbs && dto.Parent != null ? MakeBreadCrumbs(dto) : null,
                children = includeChildren && dto.Children != null ? new List<FacilityStructureModel>(from c in dto.Children select FacilityStructureModel.FromDto(c, false, false)) : null,
                siteid = dto.SiteId,
                mediaid = dto.MediaId,
                floorplan = dto.FloorPlan != null ? FloorPlanModel.FromDto(dto.FloorPlan) : null,
                Catalogs = MakeCatalogs(dto),
                hierarchyid = dto.HierarchyId
            };
        }
    }

    public class FacilityStructureProductCatalogModel
    {
        public bool __IsNew { get; set; }

        public int Id { get; set; }

        public int FacilityStructureId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public bool StockCountWhenOrdering { get; set; }

        public bool ShowPricingWhenOrdering { get; set; }

        public bool ShowLineItemNotesWhenOrdering { get; set; }

        /// <summary>
        /// Indicates if the catalog is inherited from a parent level
        /// </summary>
        public bool Inherited { get; set; }

        /// <summary>
        /// Indicates what rule the catalog is following, can be one of Inherited, Excluded, Included.
        /// While the Excluded property can be used to find out if the catalog should be exlcuded or not, this property tells you
        /// how this answer was arrived at (ie an inclusion could be because this catalog was included, or it could be because it's inheriting an inclusion from above)
        /// </summary>
        public string InheritedRule { get; set; }

        /// <summary>
        /// Indicates if an inherited catalog is excluded from the current level (and down) by an exclusion/inclusion rule. Since inclusion is the deffault, we look for when its excluded
        /// </summary>
        public bool Excluded { get; set; }

        public IList<FacilityStructureProductCatalogItemModel> Items { get; set; }

        public static FacilityStructureProductCatalogModel FromDto(ProductCatalogDTO dto, bool inherited)
        {
            var result = new FacilityStructureProductCatalogModel()
            {
                __IsNew = false,
                Id = dto.Id,
                FacilityStructureId = dto.FacilityStructureId,
                Name = dto.Name,
                Description = dto.Description,
                StockCountWhenOrdering = dto.StockCountWhenOrdering,
                ShowPricingWhenOrdering = dto.ShowPricingWhenOrdering,
                ShowLineItemNotesWhenOrdering = dto.ShowLineItemNotesWhenOrdering,
                Items = new List<FacilityStructureProductCatalogItemModel>(),
                Inherited = inherited,
                Excluded = inherited == true ? dto.Excluded : false,
                InheritedRule = inherited == true ? dto.InheritedRule : ""
            };

            foreach(var item in dto.ProductCatalogItems)
            {
                result.Items.Add(FacilityStructureProductCatalogItemModel.FromDto(item));
            }

            return result;
        }
    }

    public class FacilityStructureProductCatalogItemModel
    {
        public bool __IsNew { get; set; }

        public int ProductCatalogId { get; set; }

        public int ProductId { get; set; }

        public string Name { get; set; }

        public decimal? MinStockLevel { get; set; }

        public decimal? Price { get; set; }

        public static FacilityStructureProductCatalogItemModel FromDto(ProductCatalogItemDTO dto)
        {
            var result = new FacilityStructureProductCatalogItemModel()
            {
                __IsNew = false,
                ProductCatalogId = dto.ProductCatalogId,
                ProductId = dto.ProductId,
                MinStockLevel = dto.MinStockLevel,
                Name = dto.Product.Name,
                Price = dto.Price
            };

            return result;
        }
    }

    public class ProductModel
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public static ProductModel FromDto(ProductDTO dto)
        {
            return new ProductModel() { Id = dto.Id, Name = dto.Name };
        }
    }

    public class FacilityStructureStandardsTest
    {
        public string Type { get; set; }
        public string Compliance { get; set; }
        public string[] Name { get; set; }
    }
}