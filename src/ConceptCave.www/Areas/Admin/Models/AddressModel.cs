using ConceptCave.DTO.DTOClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConceptCave.www.Areas.Admin.Models
{
    public class AddressModel
    {
        public int id { get; set; }
        public string line1 { get; set; }
        public string line2 { get; set; }
        public string line3 { get; set; }
        public string line4 { get; set; }
        public string locality { get; set; }
        public string region { get; set; }
        public string zipcode { get; set; }
        public string countrycode { get; set; }

        public static AddressModel FromDto(AddressDTO dto)
        {
            return new AddressModel()
            {
                id = dto.Id,
                line1 = dto.Line1,
                line2 = dto.Line2,
                line3 = dto.Line3,
                line4 = dto.Line4,
                locality = dto.Locality,
                region = dto.Region,
                zipcode = dto.ZipCode,
                countrycode = dto.CountryCode
            };
        }
    }
}