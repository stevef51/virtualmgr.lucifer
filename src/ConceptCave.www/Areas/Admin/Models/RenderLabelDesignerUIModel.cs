﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConceptCave.Data.EntityClasses;
using System.Drawing;

namespace ConceptCave.www.Areas.Admin.Models
{
    public class RenderLabelDesignerUIModel
    {
        public LabelEntity Entity { get; set; }

        public Color ForeColor
        {
            get
            {
                return Color.FromArgb(Entity.Forecolor);
            }
        }

        public Color BackColor
        {
            get
            {
                return Color.FromArgb(Entity.Backcolor);
            }
        }

        public string ForeColorForHtml
        {
            get
            {
                return System.Drawing.ColorTranslator.ToHtml(ForeColor);
            }
        }

        public string BackColorForHtml
        {
            get
            {
                return System.Drawing.ColorTranslator.ToHtml(BackColor);
            }
        }
    }
}