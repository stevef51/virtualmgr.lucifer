﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConceptCave.Data.EntityClasses;
using ConceptCave.RepositoryInterfaces.Importing;

namespace ConceptCave.www.Areas.Admin.Models
{
    public class MembershipImportManagementModel
    {
        public List<UserTypeEntity> UserTypes { get; set; }
        public bool TestOnly { get; set; }
        public bool UseSingleTransaction { get; set; }

        public MembershipImportManagementModel()
        {
            TestOnly = true;
        }
    }

    public class MembershipImportResultModel
    {
        public ImportParserResult Result { get; set; }
        public bool IsTestOnly { get; set; }
    }
}