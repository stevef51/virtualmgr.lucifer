﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;
using Newtonsoft.Json;

namespace ConceptCave.www.Areas.Admin.Models
{
    public class MediaManagementModel
    {
        public List<MediaEntity> Items { get; set; }
    }


    public class MediaFolder
    {
        public MediaFolder()
        {
            Children = new List<MediaFolder>();

            Data = new MediaFolderData();
        }

        public MediaFolder(MediaFolderFileDTO source)
            : this()
        {
            Label = source.Medium.Name;
            Data.Id = source.Id;
            Data.MediaId = source.MediaId;
            Data.ParentId = null;
            Data.IsFile = true;
        }

        public MediaFolder(MediaFolderDTO source) : this()
        {
            Label = source.Text;
            UseVersioning = source.UseVersioning;
            Data.Id = source.Id;
            Data.ParentId = source.ParentId;

            foreach (var child in source.MediaFolders)
            {
                var c = new MediaFolder(child);
                Children.Add(c);
            }

            foreach (var file in source.MediaFolderFiles)
            {
                var f = new MediaFolder(file);
                Children.Add(f);
            }
        }

        [JsonProperty("label")]
        public string Label { get; set; }

        [JsonProperty("children")]
        public List<MediaFolder> Children { get; set; }

        [JsonProperty("data")]
        public MediaFolderData Data { get; set; }

        [JsonProperty("useversioning")]
        public bool UseVersioning { get; set; }
    }

    public class MediaFolderData
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        public Guid? MediaId { get; set; }

        [JsonProperty("parentid")]
        public Guid? ParentId { get; set; }

        [JsonProperty("isfile")]
        public bool IsFile { get; set; }
    }
}