﻿using ConceptCave.DTO.DTOClasses;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ConceptCave.www.Areas.Admin.Models
{
    public class AssetTypeCalendarTaskModel
    {
        public bool __isnew { get; set; }
        public int id { get; set; }
        public Guid tasktypeid { get; set; }
        public string tasktypename { get; set; }
        public int hierarchyroleid { get; set; }
        public string hierarchyrolename { get; set; }
        public Guid tasksiteid { get; set; }
        public int? tasklengthindays { get; set; }
        public int? tasklevel { get; set; }
        public string taskname { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string taskdescription { get; set; }

        public List<Guid> labels { get; set; }
        public string starttimelocal { get; set; }

        public static AssetTypeCalendarTaskModel FromDto(AssetTypeCalendarTaskDTO dto)
        {
            return new AssetTypeCalendarTaskModel
            {
                id = dto.Id,
                tasktypename = dto.ProjectJobTaskType?.Name,
                tasktypeid = dto.TaskTypeId,
                hierarchyroleid = dto.HierarchyRoleId,
                hierarchyrolename = dto.HierarchyRole?.Name,
                tasksiteid = dto.TaskSiteId,
                tasklengthindays = dto.TaskLengthInDays,
                tasklevel = dto.TaskLevel,
                taskname = dto.TaskName,
                taskdescription = dto.TaskDescription,
                labels = (from l in dto.AssetTypeCalendarTaskLabels select l.LabelId).ToList(),
                starttimelocal = dto.StartTimeLocal?.ToString()
            };
        }
    }

    public class AssetTypeCalendarModel
    {
        public bool __isnew { get; set; }
        public int id { get; set; }
        public int assettypeid { get; set; }
        public string name { get; set; }
        public string scheduleexpression { get; set; }
        public IList<AssetTypeCalendarTaskModel> tasks { get; set; }
        public string timezone { get; set; }

        public static AssetTypeCalendarModel FromDto(AssetTypeCalendarDTO dto)
        {
            return new AssetTypeCalendarModel
            {
                id = dto.Id,
                assettypeid = dto.AssetTypeId,
                name = dto.Name,
                scheduleexpression = dto.ScheduleExpression,
                tasks = dto.AssetTypeCalendarTasks != null ? (from t in dto.AssetTypeCalendarTasks select AssetTypeCalendarTaskModel.FromDto(t)).ToList() : null,
                timezone = dto.TimeZone
            };
        }
    }

    public class AssetTypeModel
    {
        public int id { get; set; }
        public bool archived { get; set; }
        public string name { get; set; }

        public int forecolor { get; set; }
        public int backcolor { get; set; }

        public bool cantrack { get; set; }

        public bool __isnew { get; set; }

        public IList<AssetTypeContextGroup> Contexts { get; set; }
        public List<AssetTypeCalendarModel> calendars { get; set; }
        public static AssetTypeModel FromDto(AssetTypeDTO dto, IList<PublishingGroupDTO> contextGroups)
        {
            var model = new AssetTypeModel()
            {
                id = dto.Id,
                archived = dto.Archived,
                name = dto.Name,
                cantrack = dto.CanTrack,
                forecolor = dto.Forecolor,
                backcolor = dto.Backcolor,
                Contexts = new List<AssetTypeContextGroup>(),
                calendars = dto.AssetTypeCalendars != null ? (from c in dto.AssetTypeCalendars select AssetTypeCalendarModel.FromDto(c)).ToList() : null
            };

            if(contextGroups != null)
            {
                foreach (var group in contextGroups)
                {
                    var g = new AssetTypeContextGroup()
                    {
                        Name = group.Name,
                        Resources = new List<AssetTypeContextGroupResource>()
                    };

                    foreach (var resource in group.PublishingGroupResources)
                    {
                        var r = new AssetTypeContextGroupResource()
                        {
                            Id = resource.Id,
                            Name = resource.Name,
                        };

                        if ((from p in dto.AssetTypeContextPublishedResources where p.PublishingGroupResourceId == resource.Id select p).Count() > 0)
                        {
                            r.Selected = true;
                        }

                        g.Resources.Add(r);
                    }

                    model.Contexts.Add(g);
                }
            }

            return model;
        }
    }

    public class AssetTypeContextGroup
    {
        public string Name { get; set; }

        public IList<AssetTypeContextGroupResource> Resources { get; set; }
    }

    public class AssetTypeContextGroupResource
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Selected { get; set; }
    }

}