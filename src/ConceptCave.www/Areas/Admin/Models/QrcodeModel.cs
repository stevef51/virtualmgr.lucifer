using ConceptCave.BusinessLogic;
using ConceptCave.BusinessLogic.Context;
using ConceptCave.BusinessLogic.LingoDataObjects;
using ConceptCave.BusinessLogic.Models;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.DTO.DTOClasses;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ConceptCave.www.Areas.Admin.Models
{
    public class QrcodeModel
    {
        public int id { get; set; }
        public string qrcode { get; set; }
        public Guid? userid { get; set; }
        public string username { get; set; }
        public Guid? tasktypeid { get; set; }
        public string tasktypename { get; set; }
        public int? assetid { get; set; }
        public string assetname { get; set; }
        public bool selected { get; set; }
        public static QrcodeModel FromDto(QrcodeDTO dto)
        {
            return new QrcodeModel()
            {
                id = dto.Id,
                qrcode = dto.Qrcode,
                userid = dto.UserId,
                username = dto.UserData?.Name,
                tasktypeid = dto.TaskTypeId,
                tasktypename = dto.ProjectJobTaskType?.Name,
                assetid = dto.AssetId,
                assetname = dto.Asset?.Name,
                selected = dto.Selected
            };
        }
    }
}