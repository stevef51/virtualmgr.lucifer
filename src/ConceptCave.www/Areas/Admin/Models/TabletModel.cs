﻿using ConceptCave.DTO.DTOClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConceptCave.www.Areas.Admin.Models
{
    public class TabletModel
    {
        public int id { get; set; }
        public bool archived { get; set; }
        public string name { get; set; }
        public int? tabletprofileid { get; set; }
        public Guid? siteid { get; set; }
        public string uuid { get; set; }
        public TabletUuidModel tabletuuid { get; set; }
        public DateTime? lastcontactutc { get; set; }

        public static TabletModel FromDto(TabletDTO dto)
        {
            return new TabletModel()
            {
                id = dto.Id,
                archived = dto.Archived,
                name = dto.Name,
                siteid = dto.SiteId,
                uuid = dto.Uuid,
                tabletprofileid = dto.TabletProfileId,
            };
        }
    }
}