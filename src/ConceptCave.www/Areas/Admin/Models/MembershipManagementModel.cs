﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.HelperClasses;

namespace ConceptCave.www.Areas.Admin.Models
{
    public class MembershipManagementModel
    {
        public List<UserDataEntity> Items { get; set; }
    }
}