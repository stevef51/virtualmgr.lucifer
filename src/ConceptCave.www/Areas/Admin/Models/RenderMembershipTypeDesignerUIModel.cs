﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.HelperClasses;
using ConceptCave.www.Models;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.www.Areas.Admin.Models
{
    public class RenderMembershipTypeDesignerUIModel
    {
        public RenderMembershipTypeDesignerUIModel(UserTypeEntity entity, SelectMultipleLabelsModel labels, EntityCollection<AspNetRoleEntity> roles, IEnumerable<UserBillingTypeDTO> userBillingTypes)
        {
            Entity = entity;
            UserBillingTypeId = entity.UserBillingTypeId;
            Labels = labels;
            Roles = roles;
            UserBillingTypes = userBillingTypes;
        }

        public UserTypeEntity Entity { get; set; }

        public SelectMultipleLabelsModel Labels { get; set; }
        public EntityCollection<AspNetRoleEntity> Roles { get; set; }

        public SelectMultiplePublishedChecklists SelectMultiplePublishedModel { get; set; }
        public MembershipTypePublishedChecklists SelectDashboardsModel { get; set; }

        public int? UserBillingTypeId { get; set; }
        public IEnumerable<UserBillingTypeDTO> UserBillingTypes { get; set; }
    }

    public class MembershipTypePublishedChecklists
    {
        public List<MembershipTypePublishedChecklistsGroup> Groups { get; set; }
        public UserTypeDashboardEntity[] SelectedPublishedChecklists { get; set; }
        public string FieldName { get; set; }

        public MembershipTypePublishedChecklists()
        {
            Groups = new List<MembershipTypePublishedChecklistsGroup>();
            SelectedPublishedChecklists = new UserTypeDashboardEntity[] { };
        }

        public MembershipTypePublishedChecklists(List<PublishingGroupEntity> items)
            : this()
        {
            foreach (var p in items)
            {
                Groups.Add(new MembershipTypePublishedChecklistsGroup(p));
            }
        }

        public MembershipTypePublishedChecklists(IList<PublishingGroupDTO> items)
            : this()
        {
            foreach (var p in items)
            {
                Groups.Add(new MembershipTypePublishedChecklistsGroup(p));
            }
        }
    }

    public class MembershipTypePublishedChecklistsGroup
    {
        public string Name { get; set; }

        public List<MembershipTypePublishedChecklistsItem> Items { get; set; }

        public MembershipTypePublishedChecklistsGroup()
        {
            Items = new List<MembershipTypePublishedChecklistsItem>();
        }

        public MembershipTypePublishedChecklistsGroup(PublishingGroupEntity entity)
            : this()
        {
            Name = entity.Name;

            foreach (var r in entity.PublishingGroupResources)
            {
                Items.Add(new MembershipTypePublishedChecklistsItem(r));
            }
        }

        public MembershipTypePublishedChecklistsGroup(PublishingGroupDTO entity)
            : this()
        {
            Name = entity.Name;

            foreach (var r in entity.PublishingGroupResources)
            {
                Items.Add(new MembershipTypePublishedChecklistsItem(r));
            }
        }
    }

    public class MembershipTypePublishedChecklistsItem
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public MembershipTypePublishedChecklistsItem() { }

        public MembershipTypePublishedChecklistsItem(PublishingGroupResourceEntity entity)
        {
            Id = entity.Id;
            Name = entity.Name;
        }

        public MembershipTypePublishedChecklistsItem(PublishingGroupResourceDTO entity)
        {
            Id = entity.Id;
            Name = entity.Name;
        }
    }

}