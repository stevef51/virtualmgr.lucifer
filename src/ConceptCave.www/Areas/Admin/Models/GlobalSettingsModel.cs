﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConceptCave.Data.EntityClasses;

namespace ConceptCave.www.Areas.Admin.Models
{
    public class GlobalSettingsModel
    {
        public string Name { get; set; } //actually a setting called name
        public Guid? ThemeMediaId { get; set; }
        public Guid? FavIconId { get; set; }
        public bool StoreLoginCoordinates { get; set; }
        public bool StoreLoginSite { get; set; }
        public bool SchedulesEnabled { get; set; }
        public int ScheduleProcessCount { get; set; }

        public IEnumerable<GlobalSettingEntity> CustomSettings { get; set; }

        public IEnumerable<PluginEntity> Plugins { get; set; }
    }
}