﻿using ConceptCave.DTO.DTOClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConceptCave.www.Areas.Admin.Models
{
    public class TeamHierarchyModel
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public static TeamHierarchyModel FromDto(HierarchyDTO h)
        {
            TeamHierarchyModel result = new TeamHierarchyModel() { Id = h.Id, Name = h.Name };

            return result;
        }
    }

    public class TeamBucketModel
    {
        public int Id { get; set; }
        public int? Pid { get; set; }
        public string Name { get; set; }
        public Guid? UserId { get; set; }
        public string UsersName { get; set; }
        public int RoleId { get; set; }
        public string RoleName { get; set; }

        public static IList<TeamBucketModel> FromDto(IList<HierarchyBucketDTO> buckets)
        {
            List<TeamBucketModel> result = new List<TeamBucketModel>();

            foreach(var bucket in buckets)
            {
                var b = new TeamBucketModel()
                {
                    Id = bucket.Id,
                    Pid = bucket.ParentId,
                    Name = bucket.Name,
                    UserId = bucket.UserId,
                    UsersName = bucket.UserId.HasValue ? bucket.UserData.Name : "",
                    RoleId = bucket.RoleId,
                    RoleName = bucket.HierarchyRole.Name
                };

                result.Add(b);
            }

            return result;
        }
    }
}