﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConceptCave.BusinessLogic.LingoDataObjects;
using ConceptCave.BusinessLogic.Context;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.HelperClasses;
using ConceptCave.www.Models;
using ConceptCave.Checklist.RunTime;
using ConceptCave.Repository;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.www.Areas.Player.Models;
using ConceptCave.Repository.Facilities;
using ConceptCave.Checklist.Editor;
using ConceptCave.DTO.DTOClasses;
using Newtonsoft.Json.Converters;

namespace ConceptCave.www.Areas.Admin.Models
{
    public class RenderMembershipDesignerUIModel
    {
        public RenderMembershipDesignerUIModel(IContextManagerFactory ctxFac, IList<CompanyDTO> companies, IList<TabletProfileDTO> tabletProfiles, IList<LanguageDTO> languages)
        {
            _ctxFac = ctxFac;

            CompanyDTO blank = new CompanyDTO();
            blank.Id = Guid.Empty;
            blank.Name = "";

            companies.Insert(0, blank);

            Companies = companies;
            TabletProfiles = tabletProfiles;
            Languages = languages;
        }

        protected UserDataDTO _entity;
        protected IList<UserTypeContextPublishedResourceEntity> _typeContexts;
        public IList<CompanyDTO> Companies { get; protected set; }
        public IList<TabletProfileDTO> TabletProfiles { get; protected set; }
        public IList<LanguageDTO> Languages { get; protected set; }

        private readonly IContextManagerFactory _ctxFac;

        public UserDataDTO Entity
        {
            get
            {
                return _entity;
            }
            set
            {
                _entity = value;
                Populate();
            }
        }


        public IList<UserTypeContextPublishedResourceEntity> TypeContexts
        {
            get
            {
                return _typeContexts;
            }
            set
            {
                _typeContexts = value;
                Populate();
            }
        }


        private void Populate()
        {
            if (TypeContexts == null || Entity == null)
            {
                return;
            }

            PlayerModels = new List<PlayerModel>();
            var contextManager = _ctxFac.GetInstance(_entity.UserId, ContextType.Membership, _entity.UserId);

            foreach (var typeContext in TypeContexts)
            {
                var contextGetter = contextManager.LoadContextDocument(typeContext.PublishingGroupResourceId);
                var doc = contextGetter.GetWorkingDocument(null);
                var presented = doc.GetPresented();

                PlayModelContext pc = new PlayModelContext() { Area = PlayModelContextArea.MembershipDesigner };
                pc.Data.UserTypeContextPublishedResourceId = typeContext.PublishingGroupResourceId;

                PlayerModel player = new PlayerModel(presented, null, _entity.UserId.ToString(), doc.Program) { ChecklistName = doc.DesignDocument.Name, Context = pc, ReviewerId = Entity.UserId, RevieweeId = Entity.UserId };

                PlayerModels.Add(player);
            }
        }

        public int MembershipTypeId
        {
            get
            {
                return Entity.UserTypeId;
            }
            set
            {
                Entity.UserTypeId = value;
            }
        }

        public Guid? CompanyId
        {
            get
            {
                return Entity.CompanyId;
            }
            set
            {
                Entity.CompanyId = value;
            }
        }

        public int? DefaultTabletProfileId
        {
            get { return Entity.DefaultTabletProfileId; }
            set { Entity.DefaultTabletProfileId = value; }
        }

        public string DefaultLanguageCode
        {
            get { return Entity.DefaultLanguageCode; }
            set { Entity.DefaultLanguageCode = value; }
        }

        public List<PlayerModel> PlayerModels { get; set; }
        public EntityCollection<UserTypeEntity> Types { get; set; }
        public EntityCollection<AspNetRoleEntity> Roles { get; set; }

        public SelectMultipleLabelsModel Labels { get; set; }
    }
}