﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.www.Areas.Admin.Models
{
    public class CompanyManagementModel
    {
        public IList<CompanyDTO> Items { get; set; }
        public Guid? CompanyId { get; set; }
    }

    public class CompanyManagementDetailModel
    {
        public CompanyDTO Item { get; set; }

        public int? CountryStateId
        {
            get
            {
                return Item.CountryStateId;
            }
        }

        public IList<CountryStateDTO> States { get; set; }

        public CompanyManagementDetailModel(IList<CountryStateDTO> states)
        {
            CountryStateDTO blank = new CountryStateDTO();

            blank.Id = -1;
            blank.Name = "";
            blank.Abbreviation = "";

            states.Insert(0, blank);

            States = states;
        }
    }
}