﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.Repository;
using ConceptCave.Repository.ImportMapping;

namespace ConceptCave.www.Areas.Admin.Models
{
    public class FieldMappingModel
    {
        public int UserTypeId { get; set; }
        public List<FieldMapping> SourceMappings { get; set; }

        public List<FieldMapping> DestinationFields { get; set; }

        public List<FieldTransform> Transforms { get; set; }
    }

    public class FieldTransformationModel
    {
        public int UserTypeId { get; set; }

        public List<FieldTransformation> Transformations { get; set; }
    }

    public class FieldTransformation
    {
        public string SourceName { get; set; }
        public string Destination { get; set; }
    }

    public class FieldTransformationBinder : DefaultModelBinder
    {
        protected override object CreateModel(ControllerContext controllerContext, ModelBindingContext bindingContext, Type modelType)
        {
            int userTypeId = int.Parse(bindingContext.ValueProvider.GetValue("usertypeid").AttemptedValue);

            var mappings = ImportFieldMappingRepository.GetFieldMappings(userTypeId);

            var result = new FieldTransformationModel() { UserTypeId = userTypeId };
            result.Transformations = new List<FieldTransformation>();

            foreach (var map in mappings)
            {
                string fieldName = "source_" + TagBuilder.CreateSanitizedId(map.Name);

                if (string.IsNullOrEmpty(bindingContext.ValueProvider.GetValue(fieldName).AttemptedValue) == true)
                {
                    continue;
                }

                FieldTransformation t = new FieldTransformation() { SourceName = map.Name, Destination = bindingContext.ValueProvider.GetValue(fieldName).AttemptedValue };
                // not sure why, but the email field is coming through with a comma at the end. I've checked what's getting posted back and there isn't a comma in that
                // so not sure what's going on.
                t.Destination = t.Destination.TrimEnd(',');
                result.Transformations.Add(t);
            }

            return result;
        }
    }
}