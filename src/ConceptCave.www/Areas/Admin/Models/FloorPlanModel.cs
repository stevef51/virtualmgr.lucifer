﻿using ConceptCave.BusinessLogic.Models;
using ConceptCave.DTO.DTOClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConceptCave.www.Areas.Admin.Models
{
    public class FloorPlanModel
    {
        public bool __isnew { get; set; }
        public int id { get; set; }
        public string imageurl { get; set; }
        public LatLngModel topleft { get; set; }
        public LatLngModel topright { get; set; }
        public LatLngModel bottomleft { get; set; }
        public string indooratlasfloorplanid { get; set; }
        public static FloorPlanModel FromDto(FloorPlanDTO dto)
        {
            return new FloorPlanModel()
            {
                __isnew = dto.__IsNew,
                id = dto.Id,
                imageurl = dto.ImageUrl,
                topleft = new LatLngModel(dto.TopLeftLatitude, dto.TopLeftLongitude),
                topright = new LatLngModel(dto.TopRightLatitude, dto.TopRightLongitude),
                bottomleft = new LatLngModel(dto.BottomLeftLatitude, dto.BottomLeftLongitude),
                indooratlasfloorplanid = dto.IndoorAtlasFloorPlanId
            };
        }
    }
}