﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Repository;
using ConceptCave.Repository.ImportMapping;
using ConceptCave.RepositoryInterfaces.Importing;
using ConceptCave.www.Areas.Admin.Models;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.BusinessLogic.Context;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.BusinessLogic;

namespace ConceptCave.www.Areas.Admin.Controllers
{
    [Authorize(Roles = RoleRepository.COREROLE_MEMBERSHIPIMPORTER)]
    public class MembershipImportManagementController : ControllerBase
    {
        protected Func<MembershipImportParser> _fnMembershipImportParser;

        public MembershipImportManagementController(Func<MembershipImportParser> fnMembershipImportParser)
        {
            _fnMembershipImportParser = fnMembershipImportParser;
        }

        public ActionResult Index()
        {
            var userTypes = MembershipTypeRepository.GetAll();

            return View(new MembershipImportManagementModel() { UserTypes = userTypes.ToList() });
        }

        public ActionResult GetFieldMappings(int userTypeId)
        {
            var mappings = ImportFieldMappingRepository.GetFieldMappings(userTypeId);

            var parser = _fnMembershipImportParser();
            var destinationMappings = parser.Parse(userTypeId, new List<FieldMapping>());

            var transforms = ImportFieldMappingRepository.GetFieldTransforms(userTypeId);

            return PartialView("RenderImportTemplateFieldMappings", new FieldMappingModel() { SourceMappings = mappings, DestinationFields = destinationMappings, Transforms = transforms, UserTypeId = userTypeId });
        }

        public JsonResult SaveFieldMappings([ModelBinder(typeof(FieldTransformationBinder))] FieldTransformationModel model)
        {
            var result = new Dictionary<string, object>();
            result["Success"] = false;
            result["Message"] = "";

            var mappings = ImportFieldMappingRepository.GetFieldMappings(model.UserTypeId);

            var parser = _fnMembershipImportParser();
            var destinationMappings = parser.Parse(model.UserTypeId, new List<FieldMapping>());

            List<FieldTransform> transforms = new List<FieldTransform>();

            foreach (var t in model.Transformations)
            {
                var map = (from m in mappings where m.Name.Equals(t.SourceName) select m).DefaultIfEmpty(null).First();
                var dMap = (from m in destinationMappings where m.Data.Equals(t.Destination) select m).DefaultIfEmpty(null).First();

                if (map == null || dMap == null)
                {
                    continue;
                }

                FieldTransform trans = new FieldTransform() { Source = map, Destination = dMap };
                transforms.Add(trans);
            }

            ImportFieldMappingRepository.SaveFieldTransforms(model.UserTypeId, transforms);

            result["Success"] = true;

            return Json(result);
        }

        public JsonResult UploadTemplate(HttpPostedFileBase fileUpload, int userTypeId)
        {
            var result = new Dictionary<string, object>();
            result["Success"] = false;
            result["Message"] = "";

            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    var templateId = GlobalSettingsConstants.SystemSetting_MembershipImportTemplateId + "_" + userTypeId.ToString();

                    var TemplateMediaId = GlobalSettingsRepository.GetSetting<Guid?>(templateId);

                    if (TemplateMediaId.HasValue)
                    {
                        MediaRepository.Delete(TemplateMediaId.Value);
                    }

                    MediaEntity media = MediaRepository.CreateMediaFromPostedFile(new HttpPostedFileAdapter(fileUpload), "Membership Import Template", "Membership import template for this b-workflow installation");

                    GlobalSettingsRepository.SetSetting(templateId, media.Id);

                    var mappings = ImportFieldMappingRepository.GetFieldMappings(userTypeId);

                    using(var stream = new MemoryStream(media.MediaData.Data))
                    {
                        var parser = new ExcelImportParser();
                        mappings = parser.Parse(stream, mappings);
                    }

                    ImportFieldMappingRepository.SaveFieldMappings(userTypeId, mappings);

                    scope.Complete();
                }

                result["Success"] = true;
            }
            catch (Exception e)
            {
                result["Message"] = e.Message;
            }

            return Json(result);
        }

        public ActionResult UploadImport(HttpPostedFileBase fileUpload, int userTypeId, bool testOnly)
        {
            var mappings = ImportFieldMappingRepository.GetFieldMappings(userTypeId);

            byte[] data = new byte[fileUpload.ContentLength];
            fileUpload.InputStream.Read(data, 0, fileUpload.ContentLength);

            ImportParserResult result = null;

            using (var stream = new MemoryStream(data))
            {
                var parser = new ExcelImportParser();
                var verification = parser.Verify(stream, mappings);

                if (verification.Success == false)
                {
                    return PartialView("~/Areas/Admin/Views/Shared/RenderImportResult.cshtml", new MembershipImportResultModel() { Result = verification, IsTestOnly = testOnly });
                }

                // need to go back to the start so that it can be fully imported
                stream.Seek(0, SeekOrigin.Begin);

                var transforms = ImportFieldMappingRepository.GetFieldTransforms(userTypeId);

                var mParser = _fnMembershipImportParser();
                var writer = mParser.GetWriter(userTypeId);

                int timeout = GlobalSettingsRepository.GetSetting<int>(GlobalSettingsConstants.SystemSetting_MembershipImportTransactionTimeout);
                if (timeout == 0)
                {
                    timeout = 1;
                }

                bool useIndividualTransactions = GlobalSettingsRepository.GetSetting<bool>(GlobalSettingsConstants.SystemSetting_MembershipImportIndividualTransactions);

                Action performImport = () => {
                    Action<int> progressCallback = counter =>
                    {
                        Response.Write(".");
                        Response.Flush();
                    };

                    result = parser.Import(stream, transforms, writer, testOnly, progressCallback);
                };

                if (useIndividualTransactions == false)
                {
                    using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(0, timeout, 0)))
                    {
                        performImport();

                        if (result.Success == true)
                        {
                            scope.Complete();
                        }
                    }
                }
                else
                {
                    performImport();
                }
            }

            Response.Write("endprogress");

            return PartialView("~/Areas/Admin/Views/Shared/RenderImportResult.cshtml", new MembershipImportResultModel() { Result = result, IsTestOnly = testOnly });
        }
    }
}
