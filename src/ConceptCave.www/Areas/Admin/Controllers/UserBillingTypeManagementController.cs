﻿using ConceptCave.DTO.DTOClasses;
using ConceptCave.Repository;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;

namespace ConceptCave.www.Areas.Admin.Controllers
{
    [Authorize(Roles = RoleRepository.COREROLE_USERBILLINGTYPE)]
    public class UserBillingTypeManagementController : ControllerBase
    {
        public class UserBillingTypeModel
        {
            public bool __isnew { get; set; }
            public int id { get; set; }
            public string name { get; set; }
            public decimal amountpermonth { get; set; }

            public UserBillingTypeDTO ToDTO(UserBillingTypeDTO dto = null)
            {
                if (dto == null)
                {
                    dto = new UserBillingTypeDTO();
                }
                dto.__IsNew = __isnew;
                dto.Id = id;
                dto.Name = name;
                dto.AmountPerMonth = amountpermonth;
                return dto;
            }

            public static UserBillingTypeModel ToModel(UserBillingTypeDTO dto)
            {
                var model = new UserBillingTypeModel();
                model.__isnew = dto.__IsNew;
                model.id = dto.Id;
                model.name = dto.Name;
                model.amountpermonth = dto.AmountPerMonth;
                return model;
            }
        }

        private readonly IUserBillingTypeRepository _userBillingTypeRepo;

        public UserBillingTypeManagementController(IUserBillingTypeRepository userBillingTypeRepo)
        {
            _userBillingTypeRepo = userBillingTypeRepo;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult UserBillingType(int id)
        {
            return ReturnJson(UserBillingTypeModel.ToModel(_userBillingTypeRepo.GetById(id, UserBillingTypeLoadInstructions.None)));
        }

        [HttpGet]
        public ActionResult UserBillingTypes()
        {
            return ReturnJson(from dto in _userBillingTypeRepo.GetAll(UserBillingTypeLoadInstructions.None) select UserBillingTypeModel.ToModel(dto));
        }

        [HttpGet]
        public ActionResult UserBillingTypeSearch(string search, int? pageNumber, int? itemsPerPage, bool includeArchived = false)
        {
            int totalItems = 0;
            var items = _userBillingTypeRepo.Search("%" + search + "%", pageNumber, itemsPerPage, includeArchived, ref totalItems, UserBillingTypeLoadInstructions.None);

            return ReturnJson(new { count = totalItems, items = items });
        }

        [HttpPost]
        public ActionResult UserBillingTypeSave(UserBillingTypeModel record)
        {
            UserBillingTypeDTO dto = record.ToDTO();
            using (TransactionScope scope = new TransactionScope())
            {
                dto = _userBillingTypeRepo.Save(dto, true);

                scope.Complete();
            }

            return ReturnJson(UserBillingTypeModel.ToModel(dto));
        }
    }
}