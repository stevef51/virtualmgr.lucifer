﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Repository;
using ConceptCave.www.Areas.Admin.Models;
using ConceptCave.www.Areas.Designer.Controllers;
using System.Drawing;
using ConceptCave.RepositoryInterfaces.Enums;

namespace ConceptCave.www.Areas.Admin.Controllers
{
    [Authorize(Roles = RoleRepository.COREROLE_LABELS)]
    public class LabelManagementController : ControllerBase
    {
        //
        // GET: /Admin/LabelManagement/

        public ActionResult Index()
        {
            EntityCollection<LabelEntity> labels = LabelRepository.GetAll(LabelFor.None);

            return View(new LabelManagementModel() { Items = labels.ToList() });
        }

        public ActionResult Remove(Guid id)
        {
            LabelEntity item = LabelRepository.GetById(id);

            if (item == null)
            {
                throw new ArgumentException("No label with that id exists");
            }

            LabelRepository.Remove(id);

            ViewBag.ActionTaken = ActionTaken.Remove;

            // hand in the question so the view can get at the information that was deleted
            return PartialView("RenderLabelDesignerUI", new RenderLabelDesignerUIModel() { Entity = item });
        }

        public ActionResult Add()
        {
            LabelEntity item = new LabelEntity();

            item.Name = "New Label";
            item.Forecolor = Color.White.ToArgb();
            item.Backcolor = Color.Black.ToArgb();

            LabelRepository.Save(item, true, false);

            ViewBag.ActionTaken = ActionTaken.New;

            return PartialView("RenderLabelDesignerUI", new RenderLabelDesignerUIModel() { Entity = item });
        }

        public ActionResult Save(Guid id, string name, string foreColor, string backColor, bool formedia, bool forresources, bool forquestions, bool fortasks, bool foruser)
        {
            LabelEntity item = LabelRepository.GetById(id);

            item.Name = name;
            item.Forecolor = System.Drawing.ColorTranslator.FromHtml(foreColor).ToArgb();
            item.Backcolor = System.Drawing.ColorTranslator.FromHtml(backColor).ToArgb();
            item.ForUsers = foruser;
            item.ForMedia = formedia;
            item.ForResources = forresources;
            item.ForQuestions = forquestions;
            item.ForTasks = fortasks;

            LabelRepository.Save(item, true, false);

            ViewBag.ActionTaken = ActionTaken.Save;

            return PartialView("RenderLabelDesignerUI", new RenderLabelDesignerUIModel() { Entity = item });
        }
    }
}
