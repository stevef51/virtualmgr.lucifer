﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using ConceptCave.Repository;

namespace ConceptCave.www.Areas.Admin.Controllers
{
    [Authorize(Roles = RoleRepository.COREROLE_ROLES)]
    public class RolesManagementController : ControllerBase
    {
        private readonly VirtualMgr.Membership.IRoles _roles;

        public RolesManagementController(VirtualMgr.Membership.IRoles roles)
        {
            _roles = roles;
        }

        protected string[] CoreRoles
        {
            get
            {
                return RoleRepository.ALLROLES.Split(',');
            }
        }

        [HttpGet]
        public ActionResult Roles()
        {
            var result = _roles.GetAllRoles();

            return ReturnJson(result.Except(CoreRoles));
        }

        [HttpPost]
        public ActionResult RoleSave(string oldValue, string newValue)
        {
            if(oldValue == newValue)
            {
                return ReturnJson(true);
            }

            if (_roles.RoleExists(newValue) == true)
            {
                return ReturnJson(false);
            }

            if (string.IsNullOrEmpty(oldValue) == true)
            {
                _roles.CreateRole(newValue);

                return ReturnJson(true);
            }

            var role = RoleRepository.GetByName(oldValue);

            if (role == null)
            {
                _roles.CreateRole(newValue);

                return ReturnJson(true);
            }

            role.Name = newValue;
            RoleRepository.Save(role);

            return ReturnJson(true);
        }

        public ActionResult Index()
        {
            return View();
        }
    }
}
