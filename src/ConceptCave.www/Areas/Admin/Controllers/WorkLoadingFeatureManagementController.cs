﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Repository;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.www.Areas.Admin.Models;
using ConceptCave.www.Areas.Designer.Controllers;
using ConceptCave.Checklist;
using System.Transactions;

namespace ConceptCave.www.Areas.Admin.Controllers
{
    [Authorize(Roles = RoleRepository.COREROLE_WORKLOADING)]
    public class WorkLoadingFeatureManagementController : ControllerBase
    {
        public class WorkLoadingFeatureModel
        {
            public bool __IsNew { get; set; }
            public bool Archived { get; set; }
            public Guid Id { get; set; }
            public string Name { get; set; }
            public int Dimensions { get; set; }
        }

        protected IWorkLoadingFeatureRepository FeatureRepo { get; set; }

        public WorkLoadingFeatureManagementController(IWorkLoadingFeatureRepository featureRepo)
        {
            FeatureRepo = featureRepo;
        }

        public ActionResult Index()
        {
            return View();
        }

        protected WorkLoadingFeatureDTO ToDTO(WorkLoadingFeatureModel model, WorkLoadingFeatureDTO dto = null)
        {
            if (dto == null)
                dto = new WorkLoadingFeatureDTO();
            dto.__IsNew = model.__IsNew;
            dto.Archived = model.Archived;

            if (model.__IsNew == true)
            {
                dto.Id = CombFactory.NewComb();
            }

            dto.Name = model.Name;
            dto.Dimensions = model.Dimensions;

            return dto;
        }

        protected WorkLoadingFeatureModel ToModel(WorkLoadingFeatureDTO dto, WorkLoadingFeatureModel model = null)
        {
            if (model == null)
                model = new WorkLoadingFeatureModel();
            model.__IsNew = dto.__IsNew;
            model.Archived = dto.Archived;
            model.Id = dto.Id;
            model.Name = dto.Name;
            model.Dimensions = dto.Dimensions;
            return model;
        }

        [HttpGet]
        public ActionResult Feature(Guid id)
        {
            return ReturnJson(ToModel(FeatureRepo.GetById(id, WorkLoadingFeatureLoadInstructions.None)));
        }

        [HttpGet]
        public ActionResult Features()
        {
            return ReturnJson(from b in FeatureRepo.GetAll(WorkLoadingFeatureLoadInstructions.None) select ToModel(b));
        }

        [HttpGet]
        public ActionResult Search(string search, int? pageNumber, int? itemsPerPage, bool includeArchived = false)
        {
            int totalItems = 0;
            var items = FeatureRepo.Search("%" + search + "%", pageNumber, itemsPerPage, includeArchived, ref totalItems, WorkLoadingFeatureLoadInstructions.None);

            return ReturnJson(new { count = totalItems, items = items });
        }

        [HttpPost]
        public ActionResult FeatureSave(WorkLoadingFeatureModel record)
        {
            WorkLoadingFeatureDTO dto = record.__IsNew ? new WorkLoadingFeatureDTO() : FeatureRepo.GetById(record.Id, WorkLoadingFeatureLoadInstructions.None);

            ToDTO(record, dto);

            WorkLoadingFeatureDTO result;
            using (TransactionScope scope = new TransactionScope())
            {
                result = FeatureRepo.Save(dto, true, true);

                scope.Complete();
            }

            result = FeatureRepo.GetById(dto.Id, WorkLoadingFeatureLoadInstructions.None);

            return ReturnJson(ToModel(result));
        }

        [HttpDelete]
        public ActionResult FeatureDelete(Guid id)
        {
            bool archived = false;
            FeatureRepo.Delete(id, true, out archived);
            if (archived)
                return ReturnJson(true);
            else
                return ReturnJson(new { archived = true });
        }
    }
}
