﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Repository;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.www.Areas.Admin.Models;
using ConceptCave.www.Areas.Designer.Controllers;

namespace ConceptCave.www.Areas.Admin.Controllers
{
    [Authorize(Roles = RoleRepository.COREROLE_COMPANIES)]
    public class CompanyManagementController : ControllerBase
    {
        protected ICompanyRepository CompanyRepo { get; set; }
        protected ICountryStateRepository CountryStateRepo { get; set; }

        public CompanyManagementController(ICompanyRepository companyRepo, ICountryStateRepository countrystateRepo)
        {
            CompanyRepo = companyRepo;
            CountryStateRepo = countrystateRepo;
        }

        public ActionResult Index(string term, int? pageNumber, int? pageSize, Guid? companyId)
        {
            string search = string.Empty;

            if (string.IsNullOrEmpty(term) == false)
            {
                search = term;
            }

            int page = pageNumber.HasValue ? pageNumber.Value : 1;
            int pageS = pageSize.HasValue ? pageSize.Value : 10;

            var companies = CompanyRepo.Search("%" + search + "%", page, pageS, CompanyLoadInstructions.None);

            if (Request.IsAjaxRequest())
            {
                return PartialView("RenderCompanyList", companies);
            }

            return View(new CompanyManagementModel() { Items = companies, CompanyId = companyId });
        }

        /// <summary>
        /// End point for the search company editor template
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public JsonResult Search(string query)
        {
            var companies = CompanyRepo.Search("%" + query + "%", 0, 10, CompanyLoadInstructions.None);

            return Json(companies);
        }

        public ActionResult CompanyDesigner(Guid id)
        {
            var company = CompanyRepo.GetById(id, CompanyLoadInstructions.None);
            var states = CountryStateRepo.GetAll();

            return View(new CompanyManagementDetailModel(states) { Item = company });
        }

        public ActionResult CompanyListItem(Guid id)
        {
            var project = CompanyRepo.GetById(id, CompanyLoadInstructions.None);

            return PartialView("RenderCompanyListItem", project);
        }

        public ActionResult Add()
        {
            CompanyDTO item = CompanyRepo.New("New Company");
            var states = CountryStateRepo.GetAll();

            ViewBag.ActionTaken = ActionTaken.New;

            return PartialView("CompanyDesigner", new CompanyManagementDetailModel(states) { Item = item });
        }

        public ActionResult Save(Guid id, string name, string street, string town, int countryStateId, string postcode)
        {
            var company = CompanyRepo.GetById(id, CompanyLoadInstructions.None);

            if (company == null)
            {
                return View("CompanyDesigner", null);
            }

            company.Name = name;
            company.Street = street;
            company.Town = town;
            if (countryStateId == -1)
            {
                company.CountryStateId = null;
            }
            else
            {
                company.CountryStateId = countryStateId;
            }

            company.Postcode = postcode;

            CompanyRepo.Save(company, false, false);

            var states = CountryStateRepo.GetAll();

            ViewBag.ActionTaken = ActionTaken.Save;

            return View("CompanyDesigner", new CompanyManagementDetailModel(states) { Item = company });
        }
    }
}
