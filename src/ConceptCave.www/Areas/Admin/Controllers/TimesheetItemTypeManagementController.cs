﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.Repository;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.DTO.DTOClasses;
using System.Transactions;
using ConceptCave.Checklist;

namespace ConceptCave.www.Areas.Admin.Controllers
{
    [Authorize(Roles = RoleRepository.COREROLE_TIMESHEETITEMTYPE)]
    public class TimesheetItemTypeManagementController : ControllerBase
    {
        public class TimesheetItemTypeModel
        {
            public bool __IsNew { get; set; }
            public int Id { get; set; }
            public string Name { get;set;}

            public bool IsWeekDayPay { get; set; }
            public bool IsSaturdayPay { get; set; }
            public bool IsSundayPay { get; set; }
            public bool IsLeave { get; set; }
        }

        protected ITimesheetItemTypeRepository _timesheetItemTypeRepo;

        public TimesheetItemTypeManagementController(ITimesheetItemTypeRepository timesheetItemTypeRepo)
        {
            _timesheetItemTypeRepo = timesheetItemTypeRepo;
        }

        public ActionResult Index()
        {
            return View();
        }

        protected List<TimesheetItemTypeModel> ConvertToModel(IEnumerable<TimesheetItemTypeDTO> items)
        {
            List<TimesheetItemTypeModel> result = new List<TimesheetItemTypeModel>();

            foreach(var item in items)
            {
                var r = ConvertToModel(item);

                result.Add(r);
            }

            return result;
        }

        protected TimesheetItemTypeModel ConvertToModel(TimesheetItemTypeDTO item)
        {
            var r = new TimesheetItemTypeModel()
            {
                Id = item.Id,
                Name = item.Name,
                IsWeekDayPay = item.IsWeekDayPay,
                IsSaturdayPay = item.IsSaturdayPay,
                IsSundayPay = item.IsSundayPay,
                IsLeave = item.IsLeave
            };

            return r;
        }

        protected void PopulateDTO(TimesheetItemTypeDTO dto, TimesheetItemTypeModel model)
        {
            dto.Name = model.Name;
            dto.IsWeekDayPay = model.IsWeekDayPay;
            dto.IsSaturdayPay = model.IsSaturdayPay;
            dto.IsSundayPay = model.IsSundayPay;
            dto.IsLeave = model.IsLeave;
        }

        [HttpGet]
        public ActionResult TimesheetItemTypes()
        {
            var result = _timesheetItemTypeRepo.GetAll();

            return ReturnJson(ConvertToModel(result));
        }

        [HttpGet]
        public ActionResult TimesheetItemType(int id)
        {
            var result = _timesheetItemTypeRepo.GetById(id);

            return ReturnJson(ConvertToModel(result));
        }

        [HttpPost]
        public ActionResult TimesheetItemTypeSave(TimesheetItemTypeModel type)
        {
            TimesheetItemTypeDTO t = null;

            if (type.__IsNew == true)
            {
                t = new TimesheetItemTypeDTO()
                {
                    __IsNew = true
                };
            }
            else
            {
                t = _timesheetItemTypeRepo.GetById(type.Id);
            }

            PopulateDTO(t, type);

            TimesheetItemTypeDTO result = null;

            using (TransactionScope scope = new TransactionScope())
            {
                result = _timesheetItemTypeRepo.Save(t, true, true);

                scope.Complete();
            }

            result = _timesheetItemTypeRepo.GetById(result.Id);

            return ReturnJson(ConvertToModel(result));
        }
    }
}
