﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Repository;
using ConceptCave.www.Areas.Admin.Models;

namespace ConceptCave.www.Areas.Admin.Controllers
{
    public class GetLabelDesignerUIController : ControllerBase
    {
        //
        // GET: /Admin/GetLabelDesignerUI/

        public ActionResult Index(Guid id)
        {
            LabelEntity entity = LabelRepository.GetById(id);

            return View(new RenderLabelDesignerUIModel() { Entity = entity });
        }

    }
}
