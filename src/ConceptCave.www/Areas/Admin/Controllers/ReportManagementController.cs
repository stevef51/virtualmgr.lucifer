﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using ConceptCave.Repository;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Checklist;
using System.Transactions;
using ConceptCave.RepositoryInterfaces.Importing;
using ConceptCave.PowerBi;
using Newtonsoft.Json.Linq;
using VirtualMgr.Central.Interfaces;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.www.Areas.Admin.Controllers
{
    [Authorize(Roles = RoleRepository.COREROLE_REPORTS)]
    public class ReportManagementController : ControllerBase
    {
        public class ReportHelper
        {
            public int Id { get; set; }
            public bool __IsNew { get; set; }
            public List<ReportRoleHelper> ReportRoles { get; set; }

            public string Name { get; set; }

            public string ShortId { get; set; }

            public string Description { get; set; }

            public string Configuration { get; set; }

            public int Type { get; set; }

            public ReportHelper() {
                ReportRoles = new List<ReportRoleHelper>();
            }

            public ReportHelper(ReportDTO report) : this()
            {
                Id = report.Id;
                Name = report.Name;
                Description = report.Description;
                Configuration = report.Configuration;
                __IsNew = report.__IsNew;
                Type = report.Type;
                ShortId = report.ReportData != null ? Encoding.UTF8.GetString(report.ReportData.Data) : null; 

                report.ReportRoles.ForEach(r => ReportRoles.Add(new ReportRoleHelper(r)));
            }
		}
        public class ReportInformationHelper
        {
            public string Message { get; set; }

            public string PowerBiId { get; set; }
            public string PowerBiName { get; set; }
            public string PowerBiEmbedUrl { get; set; }
            public string PowerBiWebUrl { get; set; }
        }
        

        public class ReportRoleHelper
        {
            public int ReportId { get; set; }
            public string RoleId { get; set; }

            public string Name { get; set; }


            public ReportRoleHelper() { }

            public ReportRoleHelper(ReportRoleDTO role)
            {
                ReportId = role.ReportId;
                RoleId = role.RoleId;
                Name = role.AspNetRole.Name;
            }
        }

        public class ReportServerUrlHelper
        {
            public string Url { get; set; }
            public string JsReportServerUrl { get; set; }
        }

        protected IReportManagementRepository _reportRepo;
        protected IGlobalSettingsRepository _globalSettingsRepo;
        protected IPowerBiManager _powerBi;
        protected readonly IServerUrls _serverUrls;

        public ReportManagementController(IReportManagementRepository reportRepo, 
            IGlobalSettingsRepository globalSettingsRepo,
            IPowerBiManager powerBi,
            IServerUrls serverUrls)
        {
            _reportRepo = reportRepo;
            _globalSettingsRepo = globalSettingsRepo;
            _powerBi = powerBi;
            _serverUrls = serverUrls;
        }

        [HttpGet]
        public ActionResult Reports(int? reportType = null)
        {
            var reports = _reportRepo.GetAll(reportType, ReportLoadInstructions.Roles | ReportLoadInstructions.Data);

            List<ReportHelper> result = new List<ReportHelper>();

            reports.ForEach(r => result.Add(new ReportHelper(r)));

            return ReturnJson(result);
        }
        [HttpGet]
        public ActionResult ReportInformation(int id)
        {
            var report = _reportRepo.GetById(id, ReportLoadInstructions.Data);

            if(report.Type == (int)ReportType.Telerik)
            {
                return ReturnJson(new ReportInformationHelper() { Message = "Telerik report no more information available" });
            }
            else if (report.Type == (int)ReportType.PowerBI)
            {
                JObject obj = GetPowerBiReportData(report.ReportData.Data);

                return ReturnJson(obj);
            }
            else if (report.Type == (int)ReportType.JsReport)
            {
                return ReturnJson(new ReportInformationHelper() { Message = "JsReport no more information available" });
            }
            else
            {
                return ReturnJson(new ReportInformationHelper() { Message = "No more information available" });
            }
        }

        public JObject GetPowerBiReportData(byte[] data)
        {
            var s = GetString(data);
            JObject obj = JObject.Parse(s);

            return obj;
        }

        [HttpGet]
        public ActionResult ReportServerUrl()
        {
            return ReturnJson(new ReportServerUrlHelper() { Url = _serverUrls.ReportingUrl, JsReportServerUrl = _serverUrls.JsReportServerUrl });
        }

        [HttpPost]
        public ActionResult ReportSave(ReportHelper report, string[] roles)
        {
            if (roles == null)
            {
                roles = new string[] { };
            }

            IEnumerable<string> newRoles = null;
            IEnumerable<string> deletedRoles = null;
            ReportDTO current = null;

            if(report.__IsNew == false)
            {
                current = _reportRepo.GetById(report.Id, ReportLoadInstructions.Roles | ReportLoadInstructions.Data);

                current.Name = report.Name;
                current.Description = report.Description;
                current.Configuration = report.Configuration;

                if (report.Type == (int)ReportType.JsReport)
                {
                    if (current.ReportData == null)
                    {
                        current.ReportData = new ReportDataDTO()
                        {
                            ReportId = current.Id,
                             __IsNew = true
                        };
                    }
                    current.ReportData.Data = Encoding.ASCII.GetBytes(report.ShortId);
                }
                var roleIds = (from m in current.ReportRoles select m.AspNetRole).ToList();
                deletedRoles = (from r in roleIds where roles.Contains(r.Name) == false select r.Id);
                newRoles = (from r in roles where (from t in roleIds select t.Name).Contains(r) == false select r);

                newRoles.ToList().ForEach(i =>
                {
                    var role = RoleRepository.GetByName(i); // this needs to be added to the injected version

                    current.ReportRoles.Add(new ReportRoleDTO() { __IsNew = true, RoleId = role.Id, ReportId = report.Id });
                });

                if (report.ReportRoles.Count > 0)
                {
                    deletedRoles.ForEach(d =>
                    {
                        current.ReportRoles.Remove((from l in current.ReportRoles where l.AspNetRole.Id == d select l).First());
                    });
                }
            }
            else
            {
                // we if its new, we just have to create what we need to create
                current = new ReportDTO()
                {
                    __IsNew = true,
                    Name = report.Name,
                    Description = report.Description,
                    Configuration = report.Configuration,
                    Type = report.Type,
                    ReportRoles = new List<ReportRoleDTO>(),
                   
                };
                
                roles.ForEach(r => current.ReportRoles.Add(new ReportRoleDTO()
                {
                    __IsNew = true,
                    RoleId = RoleRepository.GetByName(r).Id
                }));

                // we may need to create a workspace up on Azure for this instance. Let's have a look to see if we need to shall we?
                if(report.Type == (int)ReportType.PowerBI)
                {
                    var workspaceId = _globalSettingsRepo.GetSetting<string>(GlobalSettingsConstants.SystemSetting_AzureWorkspaceId);

                    if(string.IsNullOrEmpty(workspaceId) == true)
                    {
                        workspaceId = _powerBi.CreateWorkspace();

                        _globalSettingsRepo.SetSetting(GlobalSettingsConstants.SystemSetting_AzureWorkspaceId, workspaceId);
                    }
                } else if (report.Type == (int)ReportType.JsReport)
                {
                    current.ReportData = new ReportDataDTO()
                    {
                        ReportId = current.Id,
                        __IsNew = true,
                        Data = Encoding.ASCII.GetBytes(report.ShortId)
                    };
                }
            }

            ReportDTO result = null;

            using (TransactionScope scope = new TransactionScope())
            {
                result = _reportRepo.Save(current, true, true);
                
                if (deletedRoles != null)
                {
                    _reportRepo.RemoveReportFromRoles(report.Id, deletedRoles.ToArray());
                }

                scope.Complete();
            }
            result = _reportRepo.GetById(result.Id, ReportLoadInstructions.Roles | ReportLoadInstructions.Data);
            return ReturnJson(new ReportHelper(result));
        }

        [HttpDelete]
        public ActionResult ReportDelete(int id)
        {
            var report = _reportRepo.GetById(id, ReportLoadInstructions.None);

            if(report.Type == (int)ReportType.PowerBI)
            {
                var workspaceId = _globalSettingsRepo.GetSetting<string>(GlobalSettingsConstants.SystemSetting_AzureWorkspaceId);
                report = _reportRepo.GetById(id, ReportLoadInstructions.Data);

                var s = GetString(report.ReportData.Data);
                JObject obj = JObject.Parse(s);

                _powerBi.DeletePbix(workspaceId, obj["dataset"]["id"].ToString());
            }
            _reportRepo.Remove(id);

            return ReturnJson(true);
        }


        protected List<ImportParserResult> ImportTelerikReport(HttpPostedFileBase file, int id, ReportDTO report)
        {
            try
            {
                byte[] data = new byte[file.ContentLength];
                file.InputStream.Read(data, 0, file.ContentLength);

                if (report.ReportData == null)
                {
                    report.ReportData = new ReportDataDTO()
                    {
                        __IsNew = true
                    };
                }

                report.ReportData.Data = data;

                _reportRepo.Save(report, false, true);
            }
            catch (Exception e)
            {
                ImportParserResult r = new ImportParserResult();
                r.Errors.Add(e.Message);

                List<ImportParserResult> rs = new List<ImportParserResult>();
                rs.Add(r);

                return rs;
            }

            ImportParserResult result = new ImportParserResult() { Count = 1, Data = id };
            List<ImportParserResult> results = new List<ImportParserResult>();
            results.Add(result);

            return results;
        }

        protected List<ImportParserResult> ImportPowerBiReport(HttpPostedFileBase file, int id, ReportDTO report)
        {
            try
            {
                if (report.ReportData == null)
                {
                    report.ReportData = new ReportDataDTO()
                    {
                        __IsNew = true
                    };
                }

                var workspaceId = _globalSettingsRepo.GetSetting<string>(GlobalSettingsConstants.SystemSetting_AzureWorkspaceId);
                var importResult = _powerBi.ImportPbix(workspaceId, file.FileName, file.InputStream);

                var str = importResult.ToJson();
                byte[] bytes = new byte[str.Length * sizeof(char)];
                System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);

                report.ReportData.Data = bytes;

                _reportRepo.Save(report, false, true);
            }
            catch (Exception e)
            {
                ImportParserResult r = new ImportParserResult();
                r.Errors.Add(e.Message);

                List<ImportParserResult> rs = new List<ImportParserResult>();
                rs.Add(r);

                return rs;
            }

            ImportParserResult result = new ImportParserResult() { Count = 1, Data = id };
            List<ImportParserResult> results = new List<ImportParserResult>();
            results.Add(result);

            return results;
        }
        [HttpPost]
        public ActionResult ReportUpload(HttpPostedFileBase file, int id)
        {
            var report = _reportRepo.GetById(id, ReportLoadInstructions.Data);

            if(report.Type == (int)ReportType.Telerik)
            {
                return ReturnJson(ImportTelerikReport(file, id, report));
            }
            else
            {
                return ReturnJson(ImportPowerBiReport(file, id, report));
            }
        }

        [HttpGet]
        public async Task ReportDownload(int id)
        {
            var report = _reportRepo.GetById(id, ReportLoadInstructions.Data);

            Response.ContentType = "application/octetstream";
            Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", report.Name));
            await Response.OutputStream.WriteAsync(report.ReportData.Data, 0, report.ReportData.Data.Length);
        }

        [HttpPost]
        public ActionResult SetConnectionData(string connectionString, string username, string password, int id)
        {
            var report = _reportRepo.GetById(id, ReportLoadInstructions.Data);

            JObject obj = GetPowerBiReportData(report.ReportData.Data);

            var workspaceId = _globalSettingsRepo.GetSetting<string>(GlobalSettingsConstants.SystemSetting_AzureWorkspaceId);

            _powerBi.SetConnectionInformation(workspaceId, obj["dataset"]["id"].ToString(), connectionString, username, password);

            return ReturnJson(true);
        }

        [HttpGet]
        public ActionResult GetPowerBiReports()
        {
            var workspaceId = _globalSettingsRepo.GetSetting<string>(GlobalSettingsConstants.SystemSetting_AzureWorkspaceId);

            var reports = _powerBi.GetReports(workspaceId);

            return ReturnJson(reports);
        }
        public ActionResult Index()
        {
            return View();
        }
    }
}
