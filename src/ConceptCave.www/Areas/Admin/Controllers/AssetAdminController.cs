﻿using ConceptCave.BusinessLogic;
using ConceptCave.BusinessLogic.Context;
using ConceptCave.BusinessLogic.Invoices;
using ConceptCave.BusinessLogic.Models;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Repository;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.www.Areas.Admin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using VirtualMgr.Central;

namespace ConceptCave.www.Areas.Admin.Controllers
{
    [Authorize(Roles = RoleRepository.COREROLE_ASSETADMIN)]
    public class AssetAdminController : ControllerBaseEx
    {
        private readonly IAssetRepository _assetRepo;
        private readonly IAssetTypeRepository _assetTypeRepo;
        private readonly IAssetTrackerRepository _assetTrackerRepo;
        private readonly IContextManagerFactory _ctxFac;
        private readonly RenderConverter _rConverter;
        private readonly AnswerConverter _aConverter;

        public AssetAdminController(IAssetRepository assetRepo, 
            IAssetTypeRepository assetTypeRepo, 
            IAssetTrackerRepository assetTrackerRepo,
            IContextManagerFactory ctxFac,
            RenderConverter rConverter,
            AnswerConverter aConverter,
            VirtualMgr.Membership.IMembership membership,
            IMembershipRepository memberRepo) : base(membership, memberRepo)
        {
            _assetRepo = assetRepo;
            _assetTypeRepo = assetTypeRepo;
            _assetTrackerRepo = assetTrackerRepo;
            _ctxFac = ctxFac;
            _rConverter = rConverter;
            _aConverter = aConverter;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Search(string search, int? pageNumber, int? itemsPerPage, bool includeArchived = false)
        {
            int totalItems = 0;
            var items = _assetRepo.Search("%" + search + "%", pageNumber, itemsPerPage, includeArchived, ref totalItems);

            return ReturnJson(new { count = totalItems, items = from i in items select AssetModel.FromDto(i, (Guid)this.CurrentUserId) });
        }

        [HttpGet]
        public ActionResult Asset(int id)
        {           
            return ReturnJsonRetainCase(LoadAssetModel(id));
        }

        protected AssetDTO LoadAsset(int id)
        {
            return _assetRepo.GetById(id, AssetLoadInstructions.AssetType |
                AssetLoadInstructions.AssetTypeContextPublishedResource |
                AssetLoadInstructions.Beacon |
                AssetLoadInstructions.ContextData |
                AssetLoadInstructions.PublishingGroupResource |
                AssetLoadInstructions.PublishingGroupResourceData |
                AssetLoadInstructions.Schedule | AssetLoadInstructions.ScheduleCalendar);
        }

        protected AssetModel LoadAssetModel(int id)
        {
            return AssetModel.FromDto(LoadAsset(id), (Guid)this.CurrentUserId, _ctxFac, _rConverter);
        }

        [HttpDelete]
        public ActionResult AssetDelete(int id)
        {
            bool archived = false;
            _assetRepo.Delete(id, true, out archived);
            if (!archived)
                return ReturnJson(true);
            else
                return ReturnJson(new { archived = true });
        }

        [HttpPost]
        public ActionResult AssetSave([ModelBinder(typeof(AssetModelTransformationBinder))] AssetModel record)
        {
            var dto = _assetRepo.GetById(record.id, AssetLoadInstructions.Schedule);
            AssetDTO model = null;

            if (dto == null)
            {
                dto = new AssetDTO()
                {
                    __IsNew = true,
                    DateCreated = DateTime.UtcNow
                };
            }
            else
            {
                model = LoadAsset(record.id);
            }

            dto.Archived = false;           // Saving will always Unarchive a record
            dto.Name = record.name;
            dto.AssetTypeId = record.assettypeid;
            dto.FacilityStructureId = record.facilitystructureid;
            dto.CompanyId = record.companyid;

            // Note the user cannot add or delete Schedules, they can edit NextRunUtc and OverrideStartTimeLocal
            if(record.schedules != null)
            {
                foreach (var scheduleModel in record.schedules)
                {
                    var scheduleDto = dto.AssetSchedules.FirstOrDefault(s => s.Id == scheduleModel.id);
                    if (scheduleDto != null)
                    {
                        scheduleDto.NextRunUtc = scheduleModel.nextrunutc;
                        scheduleDto.OverrideStartTimeLocal = scheduleModel.overridestarttimelocal;
                    }
                }
            }

            var savedDto = dto;
            using (TransactionScope scope = new TransactionScope())
            {
                savedDto = _assetRepo.SaveAsset(dto, true, true);

                if(model != null)
                {
                    // we load the existing dto from the db, if there is context stuff to do, as we are going to have to hydrate the wds with the answers
                    // and save to the db etc.
                    var wds = AssetModel.GetAssetContextWorkingDocuments(model, (Guid)this.CurrentUserId, _ctxFac, _rConverter);
                    foreach(var getter in wds)
                    {
                        var doc = getter.GetWorkingDocument(null);

                        var answers = (from a in record.contextanswers where a.Checklist == doc.Name select a.Data);

                        if(answers.Count() == 0)
                        {
                            // huh how does this occur
                            throw new ArgumentException("Answers couldn't be found for " + doc.Name);
                        }

                        _aConverter.FillInAnswers(answers.First().Answers, doc, ContextType.Asset);

                        getter.Save();
                    }
                }

                scope.Complete();
            }

            dto = _assetRepo.GetById(savedDto.Id, AssetLoadInstructions.AssetType |
                AssetLoadInstructions.AssetTypeContextPublishedResource |
                AssetLoadInstructions.Beacon |
                AssetLoadInstructions.ContextData |
                AssetLoadInstructions.PublishingGroupResource |
                AssetLoadInstructions.PublishingGroupResourceData |
                AssetLoadInstructions.Schedule | AssetLoadInstructions.ScheduleCalendar);

            return ReturnJsonRetainCase(AssetModel.FromDto(dto, (Guid)this.CurrentUserId, _ctxFac, _rConverter));
        }

        [HttpPost]
        public ActionResult AssignBeacon(int assetId, string beaconId)
        {
            BeaconDTO dto = null;
            if (beaconId == null)           // Unassign
                dto = _assetTrackerRepo.GetBeaconByAssetId(assetId, AssetTrackerBeaconLoadInstructions.None);
            else
                dto = _assetTrackerRepo.GetBeacon(beaconId, AssetTrackerBeaconLoadInstructions.None);

            if (dto == null)
                return HttpNotFound("Beacon not found");

            if (beaconId == null)           // Unassign
                dto.AssetId = null;
            else
                dto.AssetId = assetId;

            dto = _assetTrackerRepo.Save(dto, true, false);

            return Asset(assetId);
        }

        [HttpGet]
        public ActionResult FindUnassignedBeacons()
        {
            return ReturnJson(from r in _assetTrackerRepo.FindUnassignedBeacons() select BeaconModel.FromDto(r));
        }

    }
}