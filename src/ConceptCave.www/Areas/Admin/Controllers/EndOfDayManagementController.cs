﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Repository;
using ConceptCave.RepositoryInterfaces.Repositories;
using System.Globalization;
using ConceptCave.www.Areas.Admin.Models;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.Checklist.Interfaces;
using System.Transactions;
using ConceptCave.Checklist;
using ConceptCave.BusinessLogic.ScheduleManagement;
using ConceptCave.RepositoryInterfaces.ScheduleManagement;
using VirtualMgr.Central;

namespace ConceptCave.www.Areas.Admin.Controllers
{
     [Authorize(Roles = RoleRepository.COREROLE_ENDOFDAY)]
    public class EndOfDayManagementController : ControllerBaseEx
    {
        private IRosterRepository _rosterRepo;
        private IHierarchyRepository _hierarchyRepo;
        private IScheduledTaskRepository _scheduleRepo;
        private ITaskRepository _taskRepo;
        private IMembershipRepository _memberRepo;
        private ITaskChangeRequestRepository _changeRequestRepo;
        private ITaskWorkLogRepository _logRepo;
        private readonly IDateTimeProvider _dateTimeProvider;

        public EndOfDayManagementController(IRosterRepository rosterRepo, 
            IHierarchyRepository hierarchyRepo,
            IScheduledTaskRepository scheduleRepo,
            ITaskRepository taskRepo,
            IMembershipRepository memberRepo,
            ITaskChangeRequestRepository changeRequestRepo,
            ITaskWorkLogRepository logRepo,
            IDateTimeProvider dateTimeProvider,
            VirtualMgr.Membership.IMembership membership) : base(membership, memberRepo)
        {
            _rosterRepo = rosterRepo;
            _hierarchyRepo = hierarchyRepo;
            _scheduleRepo = scheduleRepo;
            _taskRepo = taskRepo;
            _memberRepo = memberRepo;
            _changeRequestRepo = changeRequestRepo;
            _logRepo = logRepo;
            _dateTimeProvider = dateTimeProvider;
        }

        protected TimeZoneInfo GetTimezone()
        {
            Guid userId = (Guid)this.CurrentUserId;

            var mem = _memberRepo.GetById(userId, MembershipLoadInstructions.None);

            return TimeZoneInfo.FindSystemTimeZoneById(mem.TimeZone);
        }

        [HttpGet]
        public ActionResult Rosters()
        {
            Guid userId = (Guid)this.CurrentUserId;
            var buckets = _hierarchyRepo.GetBucketsForUser(userId, null, RepositoryInterfaces.Enums.HierarchyBucketLoadInstructions.None);

            var hs = (from b in buckets select b.HierarchyId).Distinct();

            var result = _rosterRepo.GetForHierarchies(hs.ToArray());

            return ReturnJson(result);
        }

        [HttpGet]
        public ActionResult Buckets(int rosterId)
        {
            Guid userId = (Guid)this.CurrentUserId;

            EndOfDayManagement manager = new EndOfDayManagement(_rosterRepo, _hierarchyRepo, _scheduleRepo, _taskRepo, _memberRepo, _changeRequestRepo, _logRepo, _dateTimeProvider);

            var r = _rosterRepo.GetById(rosterId, RosterLoadInstructions.None);
            var tz = TimeZoneInfo.FindSystemTimeZoneById(r.Timezone);
            var parsedDate = manager.GetStartOfCurrentShiftInTimezone(rosterId, tz);

            var result = manager.Buckets(rosterId, userId, parsedDate, tz);

            return ReturnJson(result);
        }

        public ActionResult Approve(List<EndOfDayBucketJson> buckets)
        {
            // the data is divided in 2, the stuff in the primary bucket is either to be continued
            // the next shift by the user or marked as finished by management.
            // the stuff in the secondary bucket needs to be moved to another roster
            Guid userId = (Guid)this.CurrentUserId;

            EndOfDayManagement manager = new EndOfDayManagement(_rosterRepo, _hierarchyRepo, _scheduleRepo, _taskRepo, _memberRepo, _changeRequestRepo, _logRepo, _dateTimeProvider);
            manager.Approve(buckets, userId);

            return ReturnJson(true);
        }

        public ActionResult Index()
        {
            return View();
        }

    }
}
