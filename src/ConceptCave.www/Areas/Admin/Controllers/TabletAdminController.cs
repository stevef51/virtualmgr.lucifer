﻿using ConceptCave.BusinessLogic.Invoices;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Repository;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.www.Areas.Admin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using VirtualMgr.Central;

namespace ConceptCave.www.Areas.Admin.Controllers
{
    [Authorize(Roles = RoleRepository.COREROLE_TABLETADMIN)]
    public class TabletAdminController : ControllerBase
    {
        private readonly ITabletRepository _tabletRepo;
        private readonly ITabletProfileRepository _tabletProfileRepo;

        public TabletAdminController(ITabletRepository tabletRepo, ITabletProfileRepository tabletProfileRepo)
        {
            _tabletRepo = tabletRepo;
            _tabletProfileRepo = tabletProfileRepo;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult TabletSearch(string search, int? pageNumber, int? itemsPerPage, bool includeArchived = false)
        {
            int totalItems = 0;
            var items = _tabletRepo.Search("%" + search + "%", pageNumber, itemsPerPage, includeArchived, ref totalItems);

            return ReturnJson(new { count = totalItems, items = from i in items select TabletModel.FromDto(i) });
        }

        [HttpGet]
        public ActionResult Tablet(int id)
        {
            return ReturnJson(_tabletRepo.GetTabletById(id, RepositoryInterfaces.Enums.TabletLoadInstructions.TabletUUID));
        }

        [HttpDelete]
        public ActionResult TabletDelete(int id)
        {
            bool archived = false;
            _tabletRepo.Delete(id, true, out archived);
            if (!archived)
                return ReturnJson(true);
            else
                return ReturnJson(new { archived = true });
        }


        [HttpPost]
        public ActionResult TabletSave(TabletModel record)
        {
            var dto = _tabletRepo.GetTabletById(record.id, RepositoryInterfaces.Enums.TabletLoadInstructions.None);
            if (dto == null)
            {
                dto = new TabletDTO()
                {
                    __IsNew = true
                };
            }
            dto.Archived = false;           // Saving will always Unarchive a record
            dto.Name = record.name;
            dto.SiteId = record.siteid;
            dto.TabletProfileId = record.tabletprofileid;

            dto = _tabletRepo.SaveTablet(dto, true, true);

            return ReturnJson(TabletModel.FromDto(dto));
        }

        [HttpPost]
        public ActionResult AssignTabletUuid(int tabletId, string uuid)
        {
            var dto = _tabletRepo.GetTabletById(tabletId, RepositoryInterfaces.Enums.TabletLoadInstructions.None);
            if (dto == null)
                return HttpNotFound("Tablet not found");

            if (dto.Uuid != null)
            {
                var uuidDto = _tabletRepo.GetTabletUuid(dto.Uuid, RepositoryInterfaces.Enums.TabletUuidLoadInstructions.None);
                if (uuidDto != null)
                {
                    uuidDto.TabletId = null;
                    _tabletRepo.SaveTabletUuid(uuidDto, false, false);
                }
            }

            dto.Uuid = uuid;
            dto = _tabletRepo.SaveTablet(dto, true, false);

            if (uuid != null)
            {
                var newUuidDto = _tabletRepo.GetTabletUuid(uuid, RepositoryInterfaces.Enums.TabletUuidLoadInstructions.None);
                if (newUuidDto != null)
                {
                    newUuidDto.TabletId = tabletId;
                    _tabletRepo.SaveTabletUuid(newUuidDto, false, false);
                }
            }

            return Tablet(tabletId);
        }

        [HttpGet]
        public ActionResult FindUnassignedTabletUuids()
        {
            return ReturnJson(from r in _tabletRepo.FindUnassignedTabletUuids() select TabletUuidModel.FromDto(r));
        }

        [HttpGet]
        public ActionResult GetSetup()
        {
            return ReturnJson(new
            {
                tabletprofiles = _tabletProfileRepo.GetAll()
            });
        }
    }
}