﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Repository;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.www.Areas.Admin.Models;
using ConceptCave.www.Areas.Designer.Controllers;
using ConceptCave.Checklist;
using System.Transactions;

namespace ConceptCave.www.Areas.Admin.Controllers
{
    [Authorize(Roles = RoleRepository.COREROLE_WORKLOADING)]
    public class SiteFeatureManagementController : ControllerBase
    {
        public class SiteModel
        {
            public Guid SiteId { get; set; }
            public string UserName { get; set; }
            public IList<SiteAreaModel> SiteAreas { get; set; }
            public IList<SiteFeatureModel> SiteFeatures { get; set; }
        }

        public class SiteAreaModel
        {
            public bool __IsNew { get; set; }
            public Guid Id { get; set; }
            public Guid SiteId { get; set; }
            public string Name { get; set; }
        }
        public class SiteFeatureModel
        {
            public bool __IsNew { get; set; }
            public Guid Id { get; set; }
            public Guid SiteId { get; set; }
            public Guid SiteAreaId { get; set; }
            public Guid WorkLoadingFeatureId { get; set; }
            public decimal Measure { get; set; }
            public int UnitId { get; set; }
            public string Name { get; set; }
        }

        protected ISiteFeatureRepository SiteFeatureRepo { get; set; }
        protected IMembershipRepository MembershipRepo { get; set; }
        public SiteFeatureManagementController(ISiteFeatureRepository siteFeatureRepo, IMembershipRepository membershipRepo)
        {
            SiteFeatureRepo = siteFeatureRepo;
            MembershipRepo = membershipRepo;
        }

        public ActionResult Index()
        {
            return View();
        }

        private static SiteAreaDTO ToDTO(SiteAreaDTO dto, SiteAreaModel sam)
        {
            dto.__IsNew = sam.__IsNew;

            if (sam.__IsNew == true)
            {
                dto.Id = CombFactory.NewComb();
            }
            else
            {
                dto.Id = sam.Id;
            }
            dto.Name = sam.Name;
            dto.SiteId = sam.SiteId;
            return dto;
        }

        private static SiteFeatureDTO ToDTO(SiteFeatureDTO dto, SiteFeatureModel sfm)
        {
            dto.__IsNew = sfm.__IsNew;

            if (sfm.__IsNew == true)
            {
                dto.Id = CombFactory.NewComb();
            }
            else
            {
                dto.Id = sfm.Id;
            }
            dto.Measure = sfm.Measure;
            dto.Name = sfm.Name;
            dto.SiteId = sfm.SiteId;
            dto.SiteAreaId = sfm.SiteAreaId;
            dto.UnitId = sfm.UnitId;
            dto.WorkLoadingFeatureId = sfm.WorkLoadingFeatureId;
            return dto;
        }

 
        protected SiteModel ToModel(UserDataDTO siteDto, IList<SiteAreaDTO> areaDtos, IList<SiteFeatureDTO> featureDtos, SiteModel model = null)
        {
            if (model == null)
                model = new SiteModel();

            model.SiteId = siteDto.UserId;
            model.UserName = siteDto.Name;
            if (areaDtos != null)
            {
                if (model.SiteAreas == null)
                    model.SiteAreas = new List<SiteAreaModel>();

                foreach (var areaDto in areaDtos)
                {
                    var sam = ToModel(areaDto);
                    model.SiteAreas.Add(sam);
                }
            }
            if (featureDtos != null)
            {
                if (model.SiteFeatures == null)
                    model.SiteFeatures = new List<SiteFeatureModel>();

                foreach (var dto in featureDtos)
                {
                    var sfm = ToModel(dto);
                    model.SiteFeatures.Add(sfm);
                }
            }
            return model;
        }

        protected static SiteAreaModel ToModel(SiteAreaDTO siteAreaDto)
        {
            var model = new SiteAreaModel();

            model.__IsNew = siteAreaDto.__IsNew;
            model.Id = siteAreaDto.Id;
            model.Name = siteAreaDto.Name;
            model.SiteId = siteAreaDto.SiteId;
            return model;
        }

        private static SiteFeatureModel ToModel(SiteFeatureDTO dto)
        {
            var sfm = new SiteFeatureModel();
            sfm.__IsNew = dto.__IsNew;
            sfm.Id = dto.Id;
            sfm.SiteId = dto.SiteId;
            sfm.SiteAreaId = dto.SiteAreaId;
            sfm.Measure = dto.Measure;
            sfm.Name = dto.Name;
            sfm.UnitId = dto.UnitId;
            sfm.WorkLoadingFeatureId = dto.WorkLoadingFeatureId;
            return sfm;
        }


        [HttpGet]
        public ActionResult Site(Guid id)
        {
            var site = MembershipRepo.GetById(id, MembershipLoadInstructions.AspNetMembership);
            var areas = SiteFeatureRepo.GetAreasBySiteId(id, SiteAreaLoadInstructions.None);
            var features = SiteFeatureRepo.GetFeaturesBySiteId(id, SiteFeatureLoadInstructions.None);
            return ReturnJson(ToModel(site, areas, features));
        }

        [HttpGet]
        public ActionResult SiteSearch(string search, int? pageNumber, int? itemsPerPage)
        {
            int totalItems = 0;
            var items = from s in SiteFeatureRepo.SearchSites(search, pageNumber, itemsPerPage, ref totalItems) select ToModel(s, null, null);
            return ReturnJson(new { count = totalItems, items = items });
        }

        [HttpPost]
        public ActionResult SiteFeatureSave(SiteFeatureModel record)
        {
            SiteFeatureDTO dto = record.__IsNew ? new SiteFeatureDTO() : SiteFeatureRepo.GetFeatureById(record.Id, SiteFeatureLoadInstructions.None);

            ToDTO(dto, record);

            SiteFeatureDTO result;
            using (TransactionScope scope = new TransactionScope())
            {
                result = SiteFeatureRepo.Save(dto, true, true);

                scope.Complete();
            }

            result = SiteFeatureRepo.GetFeatureById(dto.Id, SiteFeatureLoadInstructions.None);

            return ReturnJson(ToModel(result));
        }
        [HttpPost]
        public ActionResult SiteAreaSave(SiteAreaModel record)
        {
            SiteAreaDTO dto = record.__IsNew ? new SiteAreaDTO() : SiteFeatureRepo.GetAreaById(record.Id, SiteAreaLoadInstructions.None);

            ToDTO(dto, record);

            SiteAreaDTO result;
            using (TransactionScope scope = new TransactionScope())
            {
                result = SiteFeatureRepo.Save(dto, true, true);

                scope.Complete();
            }

            result = SiteFeatureRepo.GetAreaById(dto.Id, SiteAreaLoadInstructions.None);

            return ReturnJson(ToModel(result));
        }

        [HttpDelete]
        public ActionResult SiteFeature(Guid id)
        {
            SiteFeatureRepo.DeleteSiteFeature(id);

            return ReturnJson(true);
        }

        [HttpDelete]
        public ActionResult SiteArea(Guid id)
        {
            SiteFeatureRepo.DeleteSiteArea(id);

            return ReturnJson(true);
        }
    }
}
