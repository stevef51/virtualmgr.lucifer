﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Repository;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.www.Areas.Admin.Models;
using ConceptCave.www.Areas.Designer.Controllers;
using ConceptCave.www.Models;
using System.Transactions;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.Repository.ImportMapping;
using System.IO;
using ConceptCave.RepositoryInterfaces.Importing;
using ConceptCave.Data.DTOConverters;

namespace ConceptCave.www.Areas.Admin.Controllers
{
    [Authorize(Roles = RoleRepository.COREROLE_MEMBERSHIPTYPE)]
    public class MembershipTypeManagementController : ControllerBaseEx
    {
        private readonly IUserBillingTypeRepository _userBillingTypeRepo;
        private readonly IFacilityStructureRepository _facRepo;
        private readonly IMembershipRepository _membershipRepo;
        private readonly IMembershipTypeRepository _typeRepo;
        private readonly ICompanyRepository _companyRepo;
        private readonly ITabletProfileRepository _tabletRepo;
        private readonly ILabelRepository _labelRepo;
        private readonly IPublishingGroupRepository _publishingGroupRepo;
        private readonly VirtualMgr.Membership.IRoles _roles;

        public MembershipTypeManagementController(IUserBillingTypeRepository userBillingTypeRepo, 
            IFacilityStructureRepository facRepo,
            IMembershipRepository membershipRepo,
            IMembershipTypeRepository typeRepo,
            ICompanyRepository companyRepo,
            ITabletProfileRepository tabletRepo,
            ILabelRepository labelRepo,
            IPublishingGroupRepository publishingGroupRepo,
            VirtualMgr.Membership.IMembership membership,
            VirtualMgr.Membership.IRoles roles) : base(membership, membershipRepo)
        {
            _userBillingTypeRepo = userBillingTypeRepo;
            _facRepo = facRepo;
            _membershipRepo = membershipRepo;
            _typeRepo = typeRepo;
            _companyRepo = companyRepo;
            _tabletRepo = tabletRepo;
            _labelRepo = labelRepo;
            _publishingGroupRepo = publishingGroupRepo;
            _roles = roles;
        }

        //
        // GET: /Admin/MembershipType/

        public ActionResult Index()
        {
            EntityCollection<UserTypeEntity> labels = MembershipTypeRepository.GetAll();

            return View(new MembershipTypeManagementModel() { Items = labels.ToList() });
        }

        public ActionResult GetDesigner(int id)
        {
            UserTypeEntity entity = MembershipTypeRepository.GetById(id,
                MembershipTypeLoadInstructions.UserTypeContext | 
                MembershipTypeLoadInstructions.Dashboards | 
                MembershipTypeLoadInstructions.DefaultLabels | 
                MembershipTypeLoadInstructions.Label | 
                MembershipTypeLoadInstructions.DefaultRoles | 
                MembershipTypeLoadInstructions.Role);

            var published = _publishingGroupRepo.GetByAllowedForUserContext(PublishingGroupLoadInstruction.Resources).ToList();

            SelectMultiplePublishedChecklists pubModel = new SelectMultiplePublishedChecklists(published);
            pubModel.SelectedPublishedChecklists = (from e in entity.UserTypeContextPublishedResources select e.PublishingGroupResourceId).ToArray();

            var dashboardPublishedEntities =
                _publishingGroupRepo.GetByAllowedForDashboard(PublishingGroupLoadInstruction.Resources);
            var dashboardListModel = new MembershipTypePublishedChecklists(dashboardPublishedEntities.ToList());
            dashboardListModel.SelectedPublishedChecklists = entity.UserTypeDashboards.ToArray();
            dashboardListModel.FieldName = "selectedDashboards";

            return PartialView("RenderMembershipTypeDesignerUI",
                new RenderMembershipTypeDesignerUIModel(entity, new www.Models.SelectMultipleLabelsModel(LabelRepository.GetAll(LabelFor.Users), (from m in entity.UserTypeDefaultLabels select m.Label)), RoleRepository.GetAll(), _userBillingTypeRepo.GetAll())
                {
                    Entity = entity, 
                    SelectMultiplePublishedModel = pubModel,
                    SelectDashboardsModel = dashboardListModel
                });
        }

        public ActionResult GetListItem(int id)
        {
            UserTypeEntity item = MembershipTypeRepository.GetById(id, MembershipTypeLoadInstructions.None);

            return PartialView("RenderMembershipTypeListItem", item);
        }

        public ActionResult Add()
        {
            UserTypeEntity item = new UserTypeEntity();

            item.Name = "New Membership Type";

            MembershipTypeRepository.Save(item, true, false);

            ViewBag.ActionTaken = ActionTaken.New;

            var published = _publishingGroupRepo.GetByAllowedForUserContext(PublishingGroupLoadInstruction.Resources).ToList();

            SelectMultiplePublishedChecklists pubModel = new SelectMultiplePublishedChecklists(published);

            var dashboardPublishedEntities =
                _publishingGroupRepo.GetByAllowedForDashboard(PublishingGroupLoadInstruction.Resources);
            var dashboardListModel = new MembershipTypePublishedChecklists(dashboardPublishedEntities.ToList());
            dashboardListModel.FieldName = "selectedDashboards";

            return PartialView("RenderMembershipTypeDesignerUI",
                new RenderMembershipTypeDesignerUIModel(item, new www.Models.SelectMultipleLabelsModel(LabelRepository.GetAll(LabelFor.Users), (from m in item.UserTypeDefaultLabels select m.Label)), RoleRepository.GetAll(), _userBillingTypeRepo.GetAll())
                {
                    SelectMultiplePublishedModel = pubModel,
                    SelectDashboardsModel = dashboardListModel
                });
        }

        public JsonResult MakeUsernamesEqualPasswords(int id)
        {
            var users = _membershipRepo.GetByUserType(id, MembershipLoadInstructions.None);

            VirtualMgr.Membership.IMembershipUser u = null;

            try
            {
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(0,0,10,0,0)))
                {
                    foreach(var user in users)
                    {
                        u = _membership.GetUser(user.UserId, false);
                        u.ChangePassword(u.ResetPassword(), u.UserName);
                    }

                    scope.Complete();
                }

                return Json(true);
            }
            catch(Exception e)
            {
                if(u != null)
                {
                    return Json(string.Format("Error for user {0} was {1}", u.UserName, e.Message));
                }
                else
                {
                    return Json(e.Message);
                }
            }
        }

        public JsonResult ApplyRoles(int id)
        {
            var type = MembershipTypeRepository.GetById(id, MembershipTypeLoadInstructions.DefaultRoles | MembershipTypeLoadInstructions.Role);
            var users = _membershipRepo.GetByUserType(id, MembershipLoadInstructions.None);

            var typeRoles = (from r in type.UserTypeDefaultRoles select r.AspNetRole.Name).ToArray();

            VirtualMgr.Membership.IMembershipUser u = null;
            try
            {
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(0,0,10,0,0)))
                {
                    foreach (var user in users)
                    {
                        u = _membership.GetUser(user.UserId, false);

                        var userRoles = _roles.GetRolesForUser(u.UserName);
                        if(userRoles.Length > 0)
                        {
                            _roles.RemoveUserFromRoles(u.UserName, userRoles);
                        }

                        _roles.AddUserToRoles(u.UserName, typeRoles);
                    }

                    scope.Complete();
                }

                return Json(true);
            }
            catch (Exception e)
            {
                if (u != null)
                {
                    return Json(string.Format("Error for user {0} was {1}", u.UserName, e.Message));
                }
                else
                {
                    return Json(e.Message);
                }
            }
        }

        public ActionResult Save(int id, string name, string defaultdashboard, bool hasGeoCordinates, bool isasite, bool canLogin,
            int[] selectedPublishedChecklists, int[] selectedDashboards, string[] selectedDashboardsTitle, string[] selectedDashboardsToolTip, int[] selectedDashboardsOrder, string[] selectedDashboardsConfig, Guid[] labels, string[] roleIds, int? userBillingTypeId)
        {
            UserTypeEntity item = MembershipTypeRepository.GetById(id,
                 MembershipTypeLoadInstructions.UserTypeContext | MembershipTypeLoadInstructions.Dashboards | MembershipTypeLoadInstructions.DefaultLabels | MembershipTypeLoadInstructions.Label | MembershipTypeLoadInstructions.DefaultRoles | MembershipTypeLoadInstructions.Role);

            item.Name = name;
            item.HasGeoCordinates = hasGeoCordinates;
            item.DefaultDashboard = defaultdashboard;
            item.IsAsite = isasite;
            item.CanLogin = canLogin;
            item.UserBillingTypeId = userBillingTypeId;

            // first any deleted roles
            if (roleIds == null)
            {
                roleIds = new string[] { };
            }

            var deletedRoles = (from m in item.UserTypeDefaultRoles where roleIds.Contains(m.RoleId) == false select m.RoleId);
            // now any new roles
            var newRoles = (from m in roleIds where (from q in item.UserTypeDefaultRoles select q.RoleId).Contains(m) == false select m);

            newRoles.ToList().ForEach(i =>
            {
                item.UserTypeDefaultRoles.Add(new UserTypeDefaultRoleEntity() { RoleId = i, UserTypeId = id });
            });

            // now any deleted labels
            if (labels == null)
            {
                labels = new Guid[] { };
            }
            var deletedLabels = (from m in item.UserTypeDefaultLabels where labels.Contains(m.LabelId) == false select m.LabelId);
            // now any new labels
            var newLabels = (from m in labels where (from q in item.UserTypeDefaultLabels select q.LabelId).Contains(m) == false select m);

            newLabels.ToList().ForEach(i =>
            {
                item.UserTypeDefaultLabels.Add(new UserTypeDefaultLabelEntity() { LabelId = i, UserTypeId = id });
            });

            // first any deleted roles
            if (selectedPublishedChecklists == null)
            {
                selectedPublishedChecklists = new int[] { };
            }

            var deletedContexts = (from m in item.UserTypeContextPublishedResources where selectedPublishedChecklists.Contains(m.PublishingGroupResourceId) == false select m.PublishingGroupResourceId);

            // now any new contexts
            var newContexts = (from m in selectedPublishedChecklists where (from q in item.UserTypeContextPublishedResources select q.PublishingGroupResourceId).Contains(m) == false select m);

            //and the same for dashboards
            selectedDashboards = selectedDashboards ?? new int[] {};
            var deletedDashboards =
                item.UserTypeDashboards.Where(db => !selectedDashboards.Contains(db.PublishingGroupResourceId))
                    .Select(i => i.PublishingGroupResourceId);
            var newDashboards =
                selectedDashboards.Where(dbid => !item.UserTypeDashboards.Any(p => p.PublishingGroupResourceId == dbid));

            using (TransactionScope scope = new TransactionScope())
            {
                MembershipTypeRepository.Save(item, true, true);

                MembershipTypeRepository.RemoveLabelsFromUserType(deletedLabels.ToArray(), id);
                MembershipTypeRepository.RemoveRolesFromUserType(deletedRoles.ToArray(), id);

                var newContextEntities = MembershipTypeRepository.AddContextsToUserType(newContexts.ToArray(), id);
                MembershipTypeRepository.RemoveContextsFromUserType(deletedContexts.ToArray(), id);
                MembershipTypeRepository.AddDashboardsToUserType(newDashboards.ToArray(), id);
                MembershipTypeRepository.RemoveDashboardsFromUserType(deletedDashboards.ToArray(), id);
//                MembershipRepository.CreateAndMergeUserContextData(newContextEntities, null);

                item = MembershipTypeRepository.GetById(id, MembershipTypeLoadInstructions.UserTypeContext | MembershipTypeLoadInstructions.Dashboards);

                int i = 0;
                foreach(var did in selectedDashboards)
                {
                    var title = selectedDashboardsTitle[i];
                    var tooltip = selectedDashboardsToolTip[i];
                    var order = selectedDashboardsOrder[i];
                    var config = selectedDashboardsConfig[i];

                    var dItem = (from d in item.UserTypeDashboards where d.PublishingGroupResourceId == did select d).DefaultIfEmpty(null).First();

                    if(dItem == null)
                    {
                        // how?
                        continue;
                    }

                    dItem.Title = title;
                    dItem.Tooltip = tooltip;
                    dItem.SortOrder = order;
                    dItem.Config = config;
                    i++;
                }

                MembershipTypeRepository.Save(item, false, true);

                scope.Complete();
            }

            item = MembershipTypeRepository.GetById(id,
                MembershipTypeLoadInstructions.UserTypeContext |
                MembershipTypeLoadInstructions.Dashboards |
                MembershipTypeLoadInstructions.DefaultLabels |
                MembershipTypeLoadInstructions.Label |
                MembershipTypeLoadInstructions.DefaultRoles |
                MembershipTypeLoadInstructions.Role);

            ViewBag.ActionTaken = ActionTaken.Save;

            var published = _publishingGroupRepo.GetByAllowedForUserContext(PublishingGroupLoadInstruction.Resources);

            SelectMultiplePublishedChecklists pubModel = new SelectMultiplePublishedChecklists(published.ToList());
            pubModel.SelectedPublishedChecklists = (from e in item.UserTypeContextPublishedResources select e.PublishingGroupResourceId).ToArray();

            var dashboardPublishedEntities =
                _publishingGroupRepo.GetByAllowedForDashboard(PublishingGroupLoadInstruction.Resources);
            var dashboardListModel = new MembershipTypePublishedChecklists(dashboardPublishedEntities.ToList());
            dashboardListModel.SelectedPublishedChecklists = item.UserTypeDashboards.ToArray();
            dashboardListModel.FieldName = "selectedDashboards";

            return PartialView("RenderMembershipTypeDesignerUI",
                new RenderMembershipTypeDesignerUIModel(item, new www.Models.SelectMultipleLabelsModel(LabelRepository.GetAll(LabelFor.Users), (from m in item.UserTypeDefaultLabels select m.Label)), RoleRepository.GetAll(), _userBillingTypeRepo.GetAll())
                {
                    SelectMultiplePublishedModel = pubModel,
                    SelectDashboardsModel = dashboardListModel
                });
        }

        protected List<FieldTransform> CreateTransforms()
        {
            return new SimpleImportExportTransformFactory().Create();
        }

        [HttpGet]
        public FileResult Export(int id)
        {
            var parser = new SimpleMembershipExportParser(_facRepo, _typeRepo, _companyRepo, _tabletRepo) { IncludeTitles = true, IncludeArchived = true };
            var writer = new ExcelWriter();
            var transforms = CreateTransforms();

            var users = _membershipRepo.GetByUserType(id, SimpleMembershipImportWriter.GetLoadInstructions()).ToList();

            Action<int> progressCallback = counter =>
            {
                // not doing anything at the moment
            };

            // I know calling Import seems backwards, think of it as importing the data into Excel
            parser.Import(users, transforms, writer, false, progressCallback);

            Response.AddHeader("Content-Disposition", "inline; filename=MemberExport.xlsx");

            byte[] data;
            using (MemoryStream stream = new MemoryStream())
            {
                writer.Book.SaveAs(stream);

                data = stream.ToArray();
            }

            return this.File(data, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        }

        [HttpPost]
        public ActionResult Import(HttpPostedFileBase fileUpload, int id)
        {
            SimpleMembershipImportWriter writer = new SimpleMembershipImportWriter(_membershipRepo, _companyRepo, _labelRepo, _facRepo, _typeRepo, _tabletRepo);

            byte[] data = new byte[fileUpload.ContentLength];
            fileUpload.InputStream.Read(data, 0, fileUpload.ContentLength);

            ImportParserResult result = null;

            Action<int> progressCallback = counter =>
            {
                // not doing anything at the moment
            };

            using (var stream = new MemoryStream(data))
            {
                ExcelImportParser parser = new ExcelImportParser();
                var transform = CreateTransforms();

                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(0, 10, 0)))
                {
                    result = parser.Import(stream, transform, writer, false, progressCallback);

                    try
                    {
                        if (result.Success)
                        {
                            scope.Complete();
                        }
                    }
                    catch (Exception e)
                    {
                        result.Errors.Add(string.Format("A problem was encountered committing the data to the database, the error message given was {0}", e.Message));
                    }
                }
            }

            return PartialView("~/Areas/Admin/Views/Shared/RenderImportResult.cshtml", new MembershipImportResultModel() { Result = result, IsTestOnly = false });
        }
    }
}
