﻿using System.Web.Mvc;
using ConceptCave.Repository;

namespace ConceptCave.www.Areas.Admin.Controllers
{
    [Authorize(Roles = RoleRepository.COREROLE_CRON)]
    public class PEJobManagementController : ControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}
