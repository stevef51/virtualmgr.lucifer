﻿using System.Web.Mvc;
using ConceptCave.Repository;

namespace ConceptCave.www.Areas.Admin.Controllers
{
    [Authorize(Roles = RoleRepository.COREROLE_ASSETADMIN)]
    public class ESProbeManagementController : ControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}
