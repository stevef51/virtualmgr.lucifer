﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Repository;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.www.Areas.Admin.Models;
using ConceptCave.www.Areas.Designer.Controllers;
using ConceptCave.Checklist;
using System.Transactions;

namespace ConceptCave.www.Areas.Admin.Controllers
{
    public class MeasurementUnitManagementController : ControllerBase
    {
        public class MeasurementUnitModel
        {
            public bool __IsNew { get; set; }
            public int Id { get; set; }
            public string ShortName { get; set; }
            public string LongName { get; set; }
            public MeasurementUnitDimension Dimensions { get; set; }
            public decimal PerMetric { get; set; }
        }

        protected IMeasurementUnitRepository Repo { get; set; }

        public MeasurementUnitManagementController(IMeasurementUnitRepository repo)
        {
            Repo = repo;
        }

        public ActionResult Index()
        {
            return View();
        }

        protected MeasurementUnitDTO ToDTO(MeasurementUnitDTO dto, MeasurementUnitModel model)
        {
            dto.__IsNew = model.__IsNew;

            dto.Id = model.Id;
            dto.ShortName = model.ShortName;
            dto.LongName = model.LongName;
            dto.Dimensions = (int)model.Dimensions;
            dto.PerMetric = model.PerMetric;

            return dto;
        }

        protected MeasurementUnitModel ToModel(MeasurementUnitModel model, MeasurementUnitDTO dto)
        {
            model.__IsNew = dto.__IsNew;
            model.Id = dto.Id;
            model.ShortName = dto.ShortName;
            model.LongName = dto.LongName;
            model.Dimensions = (MeasurementUnitDimension)dto.Dimensions;
            model.PerMetric = dto.PerMetric;
            return model;
        }

        [HttpGet]
        public ActionResult Unit(int id)
        {
            return ReturnJson(ToModel(new MeasurementUnitModel(), Repo.GetById(id)));
        }

        
        [HttpGet]
        public ActionResult GetUnits()
        {
            return ReturnJson(from b in Repo.GetAll() select ToModel(new MeasurementUnitModel(), b));
        }

        public class DimensionModel
        {
            public int Id { get; set; }
            public string Name { get; set; }
        }

        private static DimensionModel[] _dimensions = new DimensionModel[]
        {
            new DimensionModel() { Id = 0, Name = "Count" },
            new DimensionModel() { Id = 1, Name = "Length" },
            new DimensionModel() { Id = 2, Name = "Area" },
            new DimensionModel() { Id = 3, Name = "Volume" },
        };

        [HttpGet]
        public ActionResult GetDimensions()
        {
            return ReturnJson(_dimensions);
        }
    }
}
