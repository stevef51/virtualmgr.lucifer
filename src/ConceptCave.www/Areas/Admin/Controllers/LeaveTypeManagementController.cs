﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.Repository;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.DTO.DTOClasses;
using System.Transactions;
using ConceptCave.Checklist;

namespace ConceptCave.www.Areas.Admin.Controllers
{
    [Authorize(Roles = RoleRepository.COREROLE_SCHEDULING)]
    public class LeaveTypeManagementController : ControllerBase
    {
        public class LeaveTypeModel
        {
            public bool __IsNew { get; set; }
            public int Id { get; set; }
            public string Name { get;set;}
        }

        protected IHierarchyBucketOverrideTypeRepository _typeRepo;

        public LeaveTypeManagementController(IHierarchyBucketOverrideTypeRepository typeRepo)
        {
            _typeRepo = typeRepo;
        }

        public ActionResult Index()
        {
            return View();
        }

        protected List<LeaveTypeModel> ConvertToModel(IEnumerable<HierarchyBucketOverrideTypeDTO> items)
        {
            List<LeaveTypeModel> result = new List<LeaveTypeModel>();

            foreach(var item in items)
            {
                var r = ConvertToModel(item);

                result.Add(r);
            }

            return result;
        }

        protected LeaveTypeModel ConvertToModel(HierarchyBucketOverrideTypeDTO item)
        {
            var r = new LeaveTypeModel()
            {
                Id = item.Id,
                Name = item.Name
            };

            return r;
        }

        protected void PopulateDTO(HierarchyBucketOverrideTypeDTO dto, LeaveTypeModel model)
        {
            dto.Name = model.Name;
        }

        [HttpGet]
        public ActionResult LeaveTypes()
        {
            var result = _typeRepo.GetAll();

            return ReturnJson(ConvertToModel(result));
        }

        [HttpGet]
        public ActionResult LeaveType(int id)
        {
            var result = _typeRepo.GetById(id);

            return ReturnJson(ConvertToModel(result));
        }

        [HttpPost]
        public ActionResult LeaveTypeSave(LeaveTypeModel type)
        {
            HierarchyBucketOverrideTypeDTO t = null;

            if (type.__IsNew == true)
            {
                t = new HierarchyBucketOverrideTypeDTO()
                {
                    __IsNew = true
                };
            }
            else
            {
                t = _typeRepo.GetById(type.Id);
            }

            PopulateDTO(t, type);

            HierarchyBucketOverrideTypeDTO result = null;

            using (TransactionScope scope = new TransactionScope())
            {
                result = _typeRepo.Save(t, true, true);

                scope.Complete();
            }

            result = _typeRepo.GetById(result.Id);

            return ReturnJson(ConvertToModel(result));
        }
    }
}
