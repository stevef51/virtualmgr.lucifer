﻿using ConceptCave.BusinessLogic.Invoices;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Repository;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.www.Areas.Admin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using VirtualMgr.Central;

namespace ConceptCave.www.Areas.Admin.Controllers
{
    [Authorize(Roles = RoleRepository.COREROLE_TABLETPROFILEADMIN)]
    public class TabletProfileAdminController : ControllerBase
    {
        private readonly ITabletProfileRepository _tabletProfileRepo;

        public TabletProfileAdminController(ITabletProfileRepository tabletProfileRepo)
        {
            _tabletProfileRepo = tabletProfileRepo;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult TabletProfileSearch(string search, int? pageNumber, int? itemsPerPage, bool includeArchived = false)
        {
            int totalItems = 0;
            var items = _tabletProfileRepo.Search("%" + search + "%", pageNumber, itemsPerPage, includeArchived, ref totalItems);

            return ReturnJson(new { count = totalItems, items = from i in items select TabletProfileModel.FromDto(i) });
        }

        [HttpGet]
        public ActionResult TabletProfile(int id)
        {
            return ReturnJson(TabletProfileModel.FromDto(_tabletProfileRepo.GetTabletProfileById(id, RepositoryInterfaces.Enums.TabletProfileLoadInstructions.None)));
        }

        [HttpDelete]
        public ActionResult TabletProfileDelete(int id)
        {
            bool archived = false;
            _tabletProfileRepo.Delete(id, true, out archived);
            if (!archived)
                return ReturnJson(true);
            else
                return ReturnJson(new { archived = true });
        }


        [HttpPost]
        public ActionResult TabletProfileSave(TabletProfileModel record)
        {
            var dto = _tabletProfileRepo.GetTabletProfileById(record.id, RepositoryInterfaces.Enums.TabletProfileLoadInstructions.None);
            if (dto == null)
            {
                dto = new TabletProfileDTO()
                {
                    __IsNew = true
                };
            }
            dto.Archived = false;           // Saving will always Unarchive a record
            dto.Name = record.name;
            dto.AllowNumberPadLogin = record.allownumberpadlogin;
            dto.AutoLogoutLongSecs = record.autologoutlongsecs;
            dto.AutoLogoutShortSecs = record.autologoutshortsecs;
            dto.ConfirmLogout = (int)record.confirmlogout;
            dto.EnableFma = record.enablefma;
            dto.LogoutPassword = record.logoutpassword;
            dto.SettingsPassword = record.settingspassword;
            dto.EnableEnvironmentalSensing = record.enableenvironmentalsensing;
            dto.LoginPhotoEvery = record.enableloginphoto ? Math.Max(record.loginphotoevery, 1) : 0;

            dto = _tabletProfileRepo.SaveTabletProfile(dto, true, true);

            return ReturnJson(TabletProfileModel.FromDto(dto));
        }

    }
}