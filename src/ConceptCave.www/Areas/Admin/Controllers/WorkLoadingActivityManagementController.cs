﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Repository;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.www.Areas.Admin.Models;
using ConceptCave.www.Areas.Designer.Controllers;
using ConceptCave.Checklist;
using System.Transactions;

namespace ConceptCave.www.Areas.Admin.Controllers
{
    [Authorize(Roles = RoleRepository.COREROLE_WORKLOADING)]
    public class WorkLoadingActivityManagementController : ControllerBase
    {
        public class WorkLoadingActivityModel
        {
            public bool __IsNew { get; set; }
            public bool Archived { get; set; }
            public Guid Id { get; set; }
            public string Name { get; set; }
        }

        protected IWorkLoadingActivityRepository ActivityRepo { get; set; }

        public WorkLoadingActivityManagementController(IWorkLoadingActivityRepository activityRepo)
        {
            ActivityRepo = activityRepo;
        }

        public ActionResult Index()
        {
            return View();
        }

        protected WorkLoadingActivityDTO ToDTO(WorkLoadingActivityModel model, WorkLoadingActivityDTO dto = null)
        {
            if (dto == null)
                dto = new WorkLoadingActivityDTO();

            dto.__IsNew = model.__IsNew;
            dto.Archived = model.Archived;

            if (model.__IsNew == true)
            {
                dto.Id = CombFactory.NewComb();
            }

            dto.Name = model.Name;

            return dto;
        }

        protected WorkLoadingActivityModel ToModel(WorkLoadingActivityDTO dto, WorkLoadingActivityModel model = null)
        {
            if (model == null)
                model = new WorkLoadingActivityModel();
            model.__IsNew = dto.__IsNew;
            model.Archived = dto.Archived;
            model.Id = dto.Id;
            model.Name = dto.Name;
            return model;
        }

        [HttpGet]
        public ActionResult Activity(Guid id)
        {
            return ReturnJson(ToModel(ActivityRepo.GetById(id, WorkLoadingActivityLoadInstructions.None)));
        }

        [HttpGet]
        public ActionResult Activities()
        {
            return ReturnJson(from b in ActivityRepo.GetAllActivities(WorkLoadingActivityLoadInstructions.None) select ToModel(b));
        }

        [HttpGet]
        public ActionResult Search(string search, int? pageNumber, int? itemsPerPage, bool includeArchived = false)
        {
            int totalItems = 0;
            var items = from b in ActivityRepo.Search("%" + search + "%", pageNumber, itemsPerPage, includeArchived, ref totalItems, WorkLoadingActivityLoadInstructions.None) select ToModel(b);
            return ReturnJson(new { count = totalItems, items = items });
        }

        [HttpPost]
        public ActionResult ActivitySave(WorkLoadingActivityModel record)
        {
            WorkLoadingActivityDTO dto = record.__IsNew ? new WorkLoadingActivityDTO() : ActivityRepo.GetById(record.Id, WorkLoadingActivityLoadInstructions.None);

            ToDTO(record, dto);

            WorkLoadingActivityDTO result;
            using (TransactionScope scope = new TransactionScope())
            {
                result = ActivityRepo.Save(dto, true, true);

                scope.Complete();
            }

            result = ActivityRepo.GetById(dto.Id, WorkLoadingActivityLoadInstructions.None);

            return ReturnJson(ToModel(result));
        }

        [HttpDelete]
        public ActionResult ActivityDelete(Guid id)
        {
            bool archived = false;
            ActivityRepo.Delete(id, true, out archived);
            if (!archived)
                return ReturnJson(true);
            else
                return ReturnJson(new { archived = true });
        }
    }
}
