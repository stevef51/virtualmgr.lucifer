﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.Repository;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.DTO.DTOClasses;
using System.Transactions;
using ConceptCave.Checklist;

namespace ConceptCave.www.Areas.Admin.Controllers
{
    [Authorize(Roles = RoleRepository.COREROLE_TASKTYPE)]
    public class FinishedStatusManagementController : ControllerBase
    {
        public enum FinishedStatusStage
        {
            Finished = 0,
            Paused = 1
        }

        public class FinishedStatusModel
        {
            public bool __IsNew { get; set; }
            public Guid Id { get; set; }
            public string Text { get;set;}
            public bool RequiresExtraNotes { get; set; }
            public int Stage { get; set; }
        }

        protected IFinishedStatusRepository _statusController;
        public FinishedStatusManagementController(IFinishedStatusRepository statusController)
        {
            _statusController = statusController;
        }

        public ActionResult Index()
        {
            return View();
        }

        protected List<FinishedStatusModel> ConvertToModel(IEnumerable<ProjectJobFinishedStatusDTO> items)
        {
            List<FinishedStatusModel> result = new List<FinishedStatusModel>();

            foreach(var item in items)
            {
                var r = ConvertToModel(item);

                result.Add(r);
            }

            return result;
        }

        protected FinishedStatusModel ConvertToModel(ProjectJobFinishedStatusDTO item)
        {
            var r = new FinishedStatusModel()
            {
                Id = item.Id,
                Text = item.Text,
                RequiresExtraNotes = item.RequiresExtraNotes,
                Stage = item.Stage
            };

            return r;
        }

        protected void PopulateDTO(ProjectJobFinishedStatusDTO dto, FinishedStatusModel model)
        {
            dto.Text = model.Text;
            dto.RequiresExtraNotes = model.RequiresExtraNotes;
            dto.Stage = model.Stage;
        }

        [HttpGet]
        public ActionResult Statuses()
        {
            var result = _statusController.GetAll();

            return ReturnJson(ConvertToModel(result));
        }

        [HttpGet]
        public ActionResult Status(Guid id)
        {
            var result = _statusController.GetById(id);

            return ReturnJson(ConvertToModel(result));
        }

        [HttpPost]
        public ActionResult StatusSave(FinishedStatusModel status)
        {
            ProjectJobFinishedStatusDTO t = null;

            if (status.__IsNew == true)
            {
                t = new ProjectJobFinishedStatusDTO()
                {
                    __IsNew = true,
                    Id = CombFactory.NewComb()
                };
            }
            else
            {
                t = _statusController.GetById(status.Id);
            }

            PopulateDTO(t, status);

            ProjectJobFinishedStatusDTO result = null;

            using (TransactionScope scope = new TransactionScope())
            {
                result = _statusController.Save(t, true, true);

                scope.Complete();
            }

            result = _statusController.GetById(t.Id);

            return ReturnJson(ConvertToModel(result));
        }
    }
}
