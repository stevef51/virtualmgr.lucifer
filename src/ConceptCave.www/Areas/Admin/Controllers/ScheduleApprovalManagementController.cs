﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Globalization;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Repository;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using Newtonsoft.Json.Linq;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.www.Areas.Admin.Models;
using ConceptCave.Checklist;
using ConceptCave.BusinessLogic.ScheduleManagement;
using ConceptCave.RepositoryInterfaces.ScheduleManagement;
using ConceptCave.RepositoryInterfaces.Workloading;
using VirtualMgr.Central;

namespace ConceptCave.www.Areas.Admin.Controllers
{
    [Authorize(Roles = RoleRepository.COREROLE_SCHEDULINGAPPROVAL)]
    public class ScheduleApprovalManagementController : ControllerBaseEx
    {
        private IRosterRepository _rosterRepo;
        private IHierarchyRepository _hierarchyRepo;
        private IScheduledTaskRepository _scheduleRepo;
        private ITaskRepository _taskRepo;
        private ITaskChangeRequestRepository _changeRequestRepo;
        private IWorkloadingActivityFactory _activityFactory;
        private IWorkloadingDurationCalculator _durationCalculator;
        private IGlobalSettingsRepository _settingsRepo;
        private readonly IDateTimeProvider _dateTimeProvider;

        public ScheduleApprovalManagementController(IRosterRepository rosterRepo, 
            IHierarchyRepository hierarchyRepo, 
            IScheduledTaskRepository scheduleRepo, 
            ITaskRepository taskRepo,
            IMembershipRepository memberRepo,
            ITaskChangeRequestRepository changeRequestRepo,
            IWorkloadingActivityFactory activityFactory,
            IWorkloadingDurationCalculator durationCalculator,
            IGlobalSettingsRepository settingsRepo,
            IDateTimeProvider dateTimeProvider,
            VirtualMgr.Membership.IMembership membership) : base(membership, memberRepo)
        {
            _rosterRepo = rosterRepo;
            _hierarchyRepo = hierarchyRepo;
            _scheduleRepo = scheduleRepo;
            _taskRepo = taskRepo;
            _changeRequestRepo = changeRequestRepo;
            _activityFactory = activityFactory;
            _durationCalculator = durationCalculator;
            _settingsRepo = settingsRepo;
            _dateTimeProvider = dateTimeProvider;
        }

        protected TimeZoneInfo GetTimezone()
        {
            Guid userId = (Guid)this.CurrentUserId;

            var mem = _memberRepo.GetById(userId, MembershipLoadInstructions.None);

            return TimeZoneInfo.FindSystemTimeZoneById(mem.TimeZone);
        }

        [HttpGet]
        public ActionResult WorkloadingModule()
        {
            var hasModule = _settingsRepo.GetSetting<bool>(GlobalSettingsConstants.Module_Workloading);

            return ReturnJson(hasModule);
        }

        [HttpGet]
        public ActionResult Rosters()
        {
            IList<RosterDTO> result = null;
            if(IsInRole(RoleRepository.COREROLE_NOTLIMITEDTOHIERARCHY))
            {
                result = _rosterRepo.GetAll();
            }
            else
            {
                // we want the set of rosters that are mapped to hierarchies that the current user is in
                Guid userId = (Guid)this.CurrentUserId;
                var buckets = _hierarchyRepo.GetBucketsForUser(userId, null, RepositoryInterfaces.Enums.HierarchyBucketLoadInstructions.None);

                var hs = (from b in buckets select b.HierarchyId).Distinct();

                result = _rosterRepo.GetForHierarchies(hs.ToArray());
            }

            return ReturnJson(result);
        }

        [HttpGet]
        public ActionResult Buckets(int rosterId, bool showworkloading)
        {
            Guid userId = (Guid)this.CurrentUserId;

            ScheduleApprovalManagement manager = new ScheduleApprovalManagement(_rosterRepo, _hierarchyRepo, _scheduleRepo, _taskRepo, _memberRepo, _changeRequestRepo, _activityFactory, _durationCalculator, _dateTimeProvider);

            var roster = _rosterRepo.GetById(rosterId, RosterLoadInstructions.None);

            var tz = TimeZoneInfo.FindSystemTimeZoneById(roster.Timezone);

            var parsedDate = manager.GetStartOfCurrentShiftInTimezone(rosterId, tz);

            var scheduleApprovalResult = manager.Buckets(rosterId, parsedDate, tz, userId, showworkloading);

            return ReturnJson(scheduleApprovalResult);
        }

        [HttpPost]
        public ActionResult Slot(int id, int roster, bool showworkloading)
        {
            ScheduleApprovalManagement manager = new ScheduleApprovalManagement(_rosterRepo, _hierarchyRepo, _scheduleRepo, _taskRepo, _memberRepo, _changeRequestRepo, _activityFactory, _durationCalculator, _dateTimeProvider);

            var r = _rosterRepo.GetById(roster, RosterLoadInstructions.None);
            var tz = TimeZoneInfo.FindSystemTimeZoneById(r.Timezone);
            var parsedDate = manager.GetStartOfCurrentShiftInTimezone(roster, tz);

            SlotJson result = manager.LoadSlot(id, parsedDate, tz, showworkloading);

            return ReturnJson(result);
        }

        [HttpPost]
        public ActionResult ApproveUnchanged(int roster, int[] slots)
        {
            ScheduleApprovalManagement manager = new ScheduleApprovalManagement(_rosterRepo, _hierarchyRepo, _scheduleRepo, _taskRepo, _memberRepo, _changeRequestRepo, _activityFactory, _durationCalculator, _dateTimeProvider);

            var r = _rosterRepo.GetById(roster, RosterLoadInstructions.None);
            var tz = TimeZoneInfo.FindSystemTimeZoneById(r.Timezone);

            var parsedDate = manager.GetStartOfCurrentShiftInTimezone(roster, tz);

            var b = manager.ApproveUnchanged(parsedDate, tz, roster, slots, (Guid)this.CurrentUserId, false);
            manager.ReplicateTasksForLabels(b);

            return ReturnJson(true);
        }

        [HttpPost]
        public ActionResult ApproveSlot(int roster, SlotJson slot)
        {
            ScheduleApprovalManagement manager = new ScheduleApprovalManagement(_rosterRepo, _hierarchyRepo, _scheduleRepo, _taskRepo, _memberRepo, _changeRequestRepo, _activityFactory, _durationCalculator, _dateTimeProvider);
            
            var r = _rosterRepo.GetById(roster, RosterLoadInstructions.None);
            var tz = TimeZoneInfo.FindSystemTimeZoneById(r.Timezone);

            var parsedDate = manager.GetStartOfCurrentShiftInTimezone(roster, tz);

            BucketJson bucket = new BucketJson();
            bucket.RosterId = roster;
            bucket.Date = parsedDate;

            bucket.Slots.Add(slot);

            manager.ApprovePrimaryBuckets(new BucketJson[] { bucket }, tz, (Guid)this.CurrentUserId);
            manager.ReplicateTasksForLabels(bucket);

            return ReturnJson(true);
        }
        
        [HttpPost]
        public ActionResult RejectOtherRosters(int roster, BucketJson bucket)
        {
            ScheduleApprovalManagement manager = new ScheduleApprovalManagement(_rosterRepo, _hierarchyRepo, _scheduleRepo, _taskRepo, _memberRepo, _changeRequestRepo, _activityFactory, _durationCalculator, _dateTimeProvider);
            
            manager.RejectCancelledSecondaryTasks(bucket.Slots, (Guid)this.CurrentUserId);

            return ReturnJson(true);
        }

        public ActionResult Index()
        {
            return View();
        }
    }
}
