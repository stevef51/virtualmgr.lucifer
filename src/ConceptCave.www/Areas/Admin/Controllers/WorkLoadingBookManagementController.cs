﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Repository;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.www.Areas.Admin.Models;
using ConceptCave.www.Areas.Designer.Controllers;
using ConceptCave.Checklist;
using System.Transactions;
using System.Text;
using Newtonsoft.Json;

namespace ConceptCave.www.Areas.Admin.Controllers
{
    [Authorize(Roles = RoleRepository.COREROLE_WORKLOADING)]
    public class WorkLoadingBookManagementController : ControllerBase
    {
        public class StandardModel
        {
            public bool __IsNew { get; set; }
            public Guid Id { get; set; }
            public Guid BookId { get; set; }
            public Guid FeatureId { get; set; }
            public Guid ActivityId { get; set; }
            public decimal Measure { get; set; }
            public int UnitId { get; set; }
            public decimal Seconds { get; set; }
        }

        public class BookModel
        {
            public bool __IsNew { get; set; }
            public bool Archived { get; set; }
            public Guid Id { get; set; }
            public string Name { get; set; }

            public IList<StandardModel> Standards { get; set; }
        }

        public class ActivityModel
        {
            public Guid Id { get; set; }
            public string Name { get; set; }
        }

        public class FeatureModel
        {
            public Guid Id { get; set; }
            public string Name { get; set; }
            public int Dimensions { get; set; }
        }

        public enum ImportBehaviour
        {
            CreateNew,      // Ignore BookId, always create a new one
            Overwrite,      // Delete existing Book, import new
            MergeReplace,   // Merge conflicts will replace
            MergeIgnore,    // Merge conflicts will be ignored
        }

        public class BookExportModel
        {
            public BookModel Book { get; set; }
            public IList<ActivityModel> Activities { get; set; }
            public IList<FeatureModel> Features { get; set; }
        }

        protected IWorkLoadingBookRepository BookRepo { get; set; }
        protected IWorkLoadingActivityRepository ActivityRepo { get; set; }
        protected IWorkLoadingFeatureRepository FeatureRepo { get; set; }

        public WorkLoadingBookManagementController(
            IWorkLoadingBookRepository bookRepo, 
            IWorkLoadingActivityRepository activityRepo,
            IWorkLoadingFeatureRepository featureRepo)
        {
            BookRepo = bookRepo;
            ActivityRepo = activityRepo;
            FeatureRepo = featureRepo;
        }

        public ActionResult Index()
        {
            return View();
        }

        protected StandardModel ToModel(WorkLoadingStandardDTO dto, StandardModel model = null)
        {
            if (model == null)
                model = new StandardModel();
            model.Id = dto.Id;
            model.ActivityId = dto.ActivityId;
            model.BookId = dto.BookId;
            model.FeatureId = dto.FeatureId;
            model.Measure = dto.Measure;
            model.UnitId = dto.UnitId;
            model.Seconds = dto.Seconds;
            return model;
        }

        protected WorkLoadingStandardDTO ToDTO(StandardModel model, WorkLoadingStandardDTO dto = null)
        {
            if (dto == null)
                dto = new WorkLoadingStandardDTO();

            if (model.__IsNew)
                dto.Id = CombFactory.NewComb();

            dto.__IsNew = model.__IsNew;
            dto.BookId = model.BookId;
            dto.ActivityId = model.ActivityId;
            dto.FeatureId = model.FeatureId;
            dto.Measure = model.Measure;
            dto.UnitId = model.UnitId;
            dto.Seconds = model.Seconds;

            return dto;
        }

        protected WorkLoadingBookDTO ToDTO(BookModel model, WorkLoadingBookDTO dto = null)
        {
            if (dto == null)
                dto = new WorkLoadingBookDTO();

            if (model.__IsNew == true)
            {
                dto.Id = CombFactory.NewComb();
            }
            dto.__IsNew = model.__IsNew;
            dto.Archived = model.Archived;
            dto.Name = model.Name;

            return dto;
        }

        protected BookModel ToModel(WorkLoadingBookDTO dto, BookModel model = null)
        {
            if (model == null)
                model = new BookModel();
            model.__IsNew = dto.__IsNew;
            model.Archived = dto.Archived;
            model.Id = dto.Id;
            model.Name = dto.Name;

            if (dto.WorkLoadingStandards != null)
            {
                model.Standards = (from s in dto.WorkLoadingStandards select ToModel(s)).ToList();
            }
            return model;
        }

        [HttpGet]
        public ActionResult Book(Guid id)
        {
            return ReturnJson(ToModel(BookRepo.GetById(id, WorkLoadingBookLoadInstructions.Standard)));
        }

        [HttpGet]
        public ActionResult Search(string search, int? pageNumber, int? itemsPerPage, bool includeArchived = false)
        {
            int totalItems = 0;
            var items = from b in BookRepo.Search(search, pageNumber, itemsPerPage, includeArchived, ref totalItems, WorkLoadingBookLoadInstructions.None) select ToModel(b);
            return ReturnJson(new { count = totalItems, items = items });
        }

        [HttpPost]
        public ActionResult BookSave(BookModel record)
        {
            WorkLoadingBookDTO dto = record.__IsNew ? new WorkLoadingBookDTO() : BookRepo.GetById(record.Id, WorkLoadingBookLoadInstructions.None);

            ToDTO(record, dto);

            WorkLoadingBookDTO result;
            using (TransactionScope scope = new TransactionScope())
            {
                result = BookRepo.Save(dto, true, true);

                scope.Complete();
            }

            result = BookRepo.GetById(dto.Id, WorkLoadingBookLoadInstructions.None);

            return ReturnJson(ToModel(result));
        }

        [HttpPost]
        public ActionResult StandardSave(StandardModel record)
        {
            WorkLoadingStandardDTO dto = record.__IsNew ? new WorkLoadingStandardDTO() : BookRepo.GetStandardById(record.Id, WorkLoadingStandardLoadInstructions.None);

            ToDTO(record, dto);

            WorkLoadingStandardDTO result;
            using (TransactionScope scope = new TransactionScope())
            {
                result = BookRepo.SaveStandard(dto, true, true);

                scope.Complete();
            }

            result = BookRepo.GetStandardById(dto.Id, WorkLoadingStandardLoadInstructions.None);

            return ReturnJson(ToModel(result));
        }

        [HttpDelete]
        public ActionResult Standard(Guid id)
        {
            bool archived = false;

            BookRepo.DeleteStandard(id, true, out archived);
            if (!archived)
                return ReturnJson(true);
            else
                return ReturnJson(new { archived = true });
        }

        protected ActivityModel ToModel(WorkLoadingActivityDTO dto)
        {
            ActivityModel model = new ActivityModel();
            model.Id = dto.Id;
            model.Name = dto.Name;
            return model;
        }

        protected FeatureModel ToModel(WorkLoadingFeatureDTO dto)
        {
            FeatureModel model = new FeatureModel();
            model.Id = dto.Id;
            model.Name = dto.Name;
            model.Dimensions = dto.Dimensions;
            return model;
        }

        [HttpGet]
        public ActionResult BookExport(Guid id, string mimetype = "text/json")
        {
            var book = BookRepo.GetById(id, WorkLoadingBookLoadInstructions.Standard | WorkLoadingBookLoadInstructions.StandardActivities | WorkLoadingBookLoadInstructions.StandardFeatures);
            var uniqueActivities = (from wls in book.WorkLoadingStandards select wls.WorkLoadingActivity).DistinctBy(wla => wla.Id);
            var uniqueFeatures = (from wls in book.WorkLoadingStandards select wls.WorkLoadingFeature).DistinctBy(wlf => wlf.Id);

            var json = JsonText(new BookExportModel()
            {
                Book = ToModel(book),
                Activities = (from a in uniqueActivities select ToModel(a)).ToList(),
                Features = (from f in uniqueFeatures select ToModel(f)).ToList()
            });
            var jsonBytes = Encoding.UTF8.GetBytes(json);

            return File(jsonBytes, mimetype, book.Name);
        }

        [HttpPost]
        public ActionResult BookImport(HttpPostedFileBase file, ImportBehaviour bookBehaviour = ImportBehaviour.MergeReplace, ImportBehaviour activityBehaviour = ImportBehaviour.MergeReplace, ImportBehaviour featureBehaviour = ImportBehaviour.MergeReplace)
        {
            try
            {
                BookExportModel model = FromJson<BookExportModel>(file.InputStream);
                using (TransactionScope scope = new TransactionScope())
                {
                    Dictionary<Guid, Guid> activityIds = new Dictionary<Guid, Guid>();
                    foreach (var importActivity in model.Activities)
                    {
                        WorkLoadingActivityDTO activity = null;
                        if (activityBehaviour != ImportBehaviour.CreateNew)
                            activity = ActivityRepo.GetById(importActivity.Id, WorkLoadingActivityLoadInstructions.None);

                        if (activity == null)
                        {
                            activity = new WorkLoadingActivityDTO();
                            activity.__IsNew = true;
                            if (activityBehaviour == ImportBehaviour.CreateNew)
                                activity.Id = CombFactory.NewComb();
                            else
                                activity.Id = importActivity.Id;
                        }
                        activityIds[importActivity.Id] = activity.Id;

                        if (activityBehaviour != ImportBehaviour.MergeIgnore || activity.__IsNew)
                        {
                            activity.Name = importActivity.Name;
                        }

                        ActivityRepo.Save(activity, true, false);
                    }

                    Dictionary<Guid, Guid> featureIds = new Dictionary<Guid, Guid>();
                    foreach (var importFeature in model.Features)
                    {
                        WorkLoadingFeatureDTO feature = null;
                        if (featureBehaviour != ImportBehaviour.CreateNew)
                            feature = FeatureRepo.GetById(importFeature.Id, WorkLoadingFeatureLoadInstructions.None);

                        if (feature == null)
                        {
                            feature = new WorkLoadingFeatureDTO();
                            feature.__IsNew = true;
                            if (featureBehaviour == ImportBehaviour.CreateNew)
                                feature.Id = CombFactory.NewComb();
                            else
                                feature.Id = importFeature.Id;
                        }
                        featureIds[importFeature.Id] = feature.Id;

                        if (featureBehaviour != ImportBehaviour.MergeIgnore || feature.__IsNew)
                        {
                            feature.Name = importFeature.Name;
                        }

                        FeatureRepo.Save(feature, true, false);
                    }

                    WorkLoadingBookDTO book = null;
                    if (bookBehaviour != ImportBehaviour.CreateNew)
                        book = BookRepo.GetById(model.Book.Id, WorkLoadingBookLoadInstructions.Standard);

                    if (book == null)
                    {
                        book = new WorkLoadingBookDTO();
                        book.__IsNew = true;
                        if (bookBehaviour == ImportBehaviour.CreateNew)
                            book.Id = CombFactory.NewComb();
                        else
                            book.Id = model.Book.Id;
                    }

                    if (bookBehaviour != ImportBehaviour.MergeIgnore || book.__IsNew)
                        book.Name = model.Book.Name;

                    BookRepo.Save(book, true, false);

                    // If Overwrite then we delete existing Books standards completely
                    // and will write new ones
                    List<Guid> deleteStandards = new List<Guid>();
                    if (bookBehaviour == ImportBehaviour.Overwrite)
                        deleteStandards.AddRange(from s in book.WorkLoadingStandards select s.Id);

                    foreach (var importStandard in model.Book.Standards)
                    {
                        WorkLoadingStandardDTO standard = null;
                        if (bookBehaviour != ImportBehaviour.CreateNew)
                            standard = BookRepo.GetStandardById(importStandard.Id, WorkLoadingStandardLoadInstructions.None);

                        if (standard == null)
                        {
                            standard = new WorkLoadingStandardDTO();
                            standard.__IsNew = true;
                            if (bookBehaviour == ImportBehaviour.CreateNew)
                                standard.Id = CombFactory.NewComb();
                            else
                                standard.Id = importStandard.Id;
                        }

                        standard.BookId = book.Id;

                        if (bookBehaviour != ImportBehaviour.MergeIgnore || standard.__IsNew)
                        {
                            standard.Seconds = importStandard.Seconds;
                            standard.Measure = importStandard.Measure;
                            standard.UnitId = importStandard.UnitId;
                        }

                        standard.ActivityId = activityIds[importStandard.ActivityId];
                        standard.FeatureId = featureIds[importStandard.FeatureId];

                        BookRepo.SaveStandard(standard, true, false);

                        deleteStandards.Remove(standard.Id);
                    }

                    // Delete any standards that are no longer referenced (if Overwrite)
                    foreach (var deleteId in deleteStandards)
                    {
                        bool dummy;
                        BookRepo.DeleteStandard(deleteId, true, out dummy);
                    }

                    scope.Complete();
                }

                return ReturnJson(true);
            }
            catch(Exception ex)
            {
                Response.StatusCode = 500;
                return ReturnJson(ex.Message);
            }
        }

        [HttpDelete]
        public ActionResult BookDelete(Guid id)
        {
            try
            {
                BookRepo.DeleteBook(id);
                return ReturnJson(true);
            }
            catch (Exception ex)
            {
                var entity = BookRepo.GetById(id, WorkLoadingBookLoadInstructions.None);
                if (entity != null && !entity.Archived)
                {
                    entity.Archived = true;
                    BookRepo.Save(entity, false, false);
                }
                return ReturnJson(new { archived = true });
            }
        }
    }
}
