﻿using ConceptCave.Repository;
using Microsoft.Rest;
using Microsoft.SqlServer.Dac;
using Ninject;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using VirtualMgr.Central.DTO.DTOClasses;
using VirtualMgr.Central.Interfaces;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using ConceptCave.www.Models;
using VirtualMgr.Central;
using Microsoft.Azure.Management.Sql.Fluent;
using Microsoft.Azure.Management.Sql.Fluent.Models;
using Microsoft.Azure.Management.Fluent;
using Microsoft.Azure.Management.AppService.Fluent;
using Microsoft.Azure.Management.AppService.Fluent.Models;
using ConceptCave.RepositoryInterfaces;
using System.Threading;
using System.Configuration;
using Microsoft.Rest.Azure;
using ConceptCave.www.Attributes;

namespace ConceptCave.www.Areas.Admin.Controllers
{
    public class ResourceGroupItem
    {
        public string ResourceGroupName { get; set; }
        public string ResourceName { get; set; }
        private bool _isValid;
        public bool IsValid()
        {
            return _isValid;
        }

        public ResourceGroupItem(string combined)
        {
            if (!string.IsNullOrWhiteSpace(combined))
            {
                var split = combined.Split(':');
                if (split.Length >= 2)
                {
                    ResourceGroupName = split[0];
                    ResourceName = split[1];
                    _isValid = true;
                }
            }
        }

        public override string ToString()
        {
            return _isValid ? (ResourceGroupName + ":" + ResourceName) : "";
        }
    }

    public class MultiTenantAdminConfig
    {
        public ResourceGroupItem DNSZone { get; set; }
    }

    [Authorize(Roles = RoleRepository.COREROLE_MULTI_TENANT_ADMIN)]
    public class MultiTenantAdminController : ControllerBase
    {
        [Flags]
        public enum TenantStatus
        {
            Online = 0,
            Disabled = 1,
            DatabaseNotConfigured = 2,
            DatabaseInvalid = 4,
            HostnameNotBound = 8,
            HostnameIncorrectlyBound = 16,
        }

        public class PricingTier
        {
            public Guid id { get; set; }
            public string name { get; set; }
            public bool iselastic { get; set; }
        }

        private static readonly PricingTier _BasicPricingTier = new PricingTier() { name = "Basic", id = Guid.Parse("dd6d99bb-f193-4ec1-86f2-43d3bccbc49c") };
        private static readonly PricingTier _S0PricingTier = new PricingTier() { name = "S0", id = Guid.Parse("f1173c43-91bd-4aaa-973c-54e79e15235b") };
        private static readonly PricingTier _S1PricingTier = new PricingTier() { name = "S1", id = Guid.Parse("1b1ebd4d-d903-4baa-97f9-4ea675f5e928") };
        private static readonly PricingTier _S2PricingTier = new PricingTier() { name = "S2 ", id = Guid.Parse("455330e1-00cd-488b-b5fa-177c226f28b7") };
        private static readonly PricingTier _S3PricingTier = new PricingTier() { name = "S3", id = Guid.Parse("789681b8-ca10-4eb0-bdf2-e0b050601b40") };
        private static readonly PricingTier _ElasticPoolPricingTier = new PricingTier() { name = "SQL Elastic Pool", id = Guid.Parse("d1737d22-a8ea-4de7-9bd0-33395d2a7419"), iselastic = true };
        private static List<PricingTier> _pricingTiers = new List<PricingTier>(new[] { _BasicPricingTier, _S0PricingTier, _S1PricingTier, _S2PricingTier, _S3PricingTier, _ElasticPoolPricingTier });

        public class TenantDatabaseModel
        {
            public string sqlservername { get; set; }
            public string database { get; set; }
            public bool onazure { get; set; }
            public PricingTier pricingtier { get; set; }
            public string elasticdbpool { get; set; }
        }

        public class AzureDatabaseOperation
        {
            public string name { get; set; }
            public string error { get; set; }
            public int percentcomplete { get; set; }
            public DateTime starttime { get; set; }
            public DateTime endtime { get; set; }
            public string state { get; set; }
            public string type { get; set; }
        }

        public class UserPermissions
        {
            public bool candelete { get; set; }
        }

        public class TenantProfileModel
        {
            public int id { get; set; }
            public string name { get; set; }
            public bool canclone { get; set; }
            public bool candelete { get; set; }
            public bool candisable { get; set; }
            public bool canupdateschema { get; set; }
            public bool emailenabled { get; set; }
            public string emailwhitelist { get; set; }
            public string createalertmessage { get; set; }
            public string databaseappend { get; set; }
            public string description { get; set; }
            public string domainappend { get; set; }
            public bool hidden { get; set; }
            public static TenantProfileModel FromDto(TenantProfileDTO dto)
            {
                return new TenantProfileModel()
                {
                    id = dto.Id,
                    name = dto.Name,
                    canclone = dto.CanClone,
                    candelete = dto.CanDelete,
                    candisable = dto.CanDisable,
                    canupdateschema = dto.CanUpdateSchema,
                    emailenabled = dto.EmailEnabled,
                    emailwhitelist = dto.EmailWhitelist,
                    createalertmessage = dto.CreateAlertMessage,
                    databaseappend = dto.DatabaseAppend,
                    description = dto.Description,
                    domainappend = dto.DomainAppend,
                    hidden = dto.Hidden
                };
            }
        }

        public class TenantHostModel
        {
            public int id { get; set; }
            public string name { get; set; }
            public string sqlservername { get; set; }
            public string trafficmanagername { get; set; }
            public string location { get; set; }
            public string webappname { get; set; }
            public string[] elasticdbpools { get; set; }
            public static TenantHostModel FromDto(TenantHostDTO dto)
            {
                return new TenantHostModel()
                {
                    id = dto.Id,
                    name = dto.Name,
                    sqlservername = dto.SqlServerName,
                    trafficmanagername = dto.TrafficManagerName,
                    location = dto.Location,
                    webappname = dto.WebAppName
                };
            }
        }
        public class TenantModel
        {
            public bool __isnew { get; set; }
            public int id { get; set; }
            public string name { get; set; }
            public string hostname { get; set; }

            public TenantDatabaseModel database { get; set; }
            public string[] elasticdbpools { get; set; }
            public bool enabled { get; set; }
            public bool canenable { get; set; }
            public TenantStatus tenantstatus { get; set; }
            public List<string> tenantstatustext { get; set; }
            public TenantProfileModel profile { get; set; }
            public TenantProfileModel targetprofile { get; set; }
            public UserPermissions userpermissions { get; set; }
            public TenantHostModel host { get; set; }
            public string timezone { get; set; }
            public string invoiceidprefix { get; set; }
            public string billingcompanyname { get; set; }
            public string billingaddress { get; set; }
            public string currency { get; set; }

            public string databaseschemaversion { get; set; }
            public string databasescriptversion { get; set; }

            public string tenantstate { get; set; }

            public string emailfromaddress { get; set; }

            public bool enabledatawarehousepush { get; set; }

            public TenantDTO CopyToDTO(TenantDTO dto)
            {
                dto.Enabled = enabled;
                dto.__IsNew = __isnew;
                dto.HostName = hostname;
                dto.Id = id;
                dto.Name = name;
                dto.InvoiceIdPrefix = invoiceidprefix;
                dto.TimeZone = timezone;
                dto.BillingCompanyName = billingcompanyname;
                dto.BillingAddress = billingaddress;
                dto.Currency = currency;
                dto.EmailFromAddress = emailfromaddress;
                dto.EnableDatawareHousePush = enabledatawarehousepush;

                // Unhook the existing TenantProfile to allow the id to be set explicityly below
                if (dto.TenantProfile != null && dto.TenantProfile.Tenants != null)
                {
                    dto.TenantProfile.Tenants.Remove(dto);
                    dto.TenantProfile = null;
                }
                // and set the new id 
                dto.TenantProfileId = profile.id;
                return dto;
            }
        }

        private readonly Func<IAzure> _fnAzure;
        private readonly IMultiTenantRepository _multiTenantRepo;
        private readonly Func<Stream> _fnConceptCaveSqlDacpacStream;
        private readonly Func<IEnumerable<IUpdateScript>> _fnAfterSchemaUpdateScripts;
        private readonly Func<IEnumerable<IUpdateScript>> _fnBeforeSchemaUpdateScripts;
        private readonly MultiTenantAdminConfig _config;
        private readonly UserPermissions _userPermissions;

        public MultiTenantAdminController(
            IMultiTenantRepository multiTenantRepo,
            [Named("ConceptCave.sql.DacPac")] Func<Stream> fnConceptCaveSqlDacpacStream,
            [Named("AfterSchemaUpdateScripts")] Func<IEnumerable<IUpdateScript>> fnAfterSchemaUpdateScripts,
            [Named("BeforeSchemaUpdateScripts")] Func<IEnumerable<IUpdateScript>> fnBeforeSchemaUpdateScripts,
            MultiTenantAdminConfig config,
            Func<IAzure> fnAzure)
        {
            _multiTenantRepo = multiTenantRepo;
            _fnConceptCaveSqlDacpacStream = fnConceptCaveSqlDacpacStream;
            _fnAfterSchemaUpdateScripts = fnAfterSchemaUpdateScripts;
            _fnBeforeSchemaUpdateScripts = fnBeforeSchemaUpdateScripts;
            _config = config;

            IAzure cachedAzure = null;
            _fnAzure = () =>
            {
                if (cachedAzure == null)
                    cachedAzure = fnAzure();
                return cachedAzure;
            };

            _userPermissions = new UserPermissions()
            {
                candelete = Thread.CurrentPrincipal.IsInRole(RoleRepository.COREROLE_DELETE_TENANT)
            };
        }

        public async Task<ActionResult> EnableTenant(int tenantId, bool enable)
        {
            var dto = _multiTenantRepo.GetTenantById(tenantId);
            if (dto == null)
                return HttpNotFound("Tenant not found");

            dto.Enabled = enable;

            _multiTenantRepo.SaveTenant(dto);

            return ReturnJson(await MakeModelAsync(dto));
        }

        private string GetServiceObjectiveName(Guid serviceObjectiveId)
        {
            var pt = _pricingTiers.FirstOrDefault(t => t.id == serviceObjectiveId);
            if (pt != null)
            {
                return pt.name;
            }

            // Not sure this is completely accurate ..
            return "Elastic Db";
        }

        public async Task<TenantModel> MakeModelAsync(TenantDTO dto)
        {
            var csb = new SqlConnectionStringBuilder(dto.ConnectionString);
            var tenantContext = MultiTenantManager.GetTenant(dto.Id);
            var tdb = tenantContext != null ? tenantContext.GetTenantDatabase() : null;
            var model = new TenantModel()
            {
                __isnew = dto.__IsNew,
                id = dto.Id,
                name = dto.Name,
                hostname = dto.HostName,
                enabled = dto.Enabled,
                host = TenantHostModel.FromDto( dto.TenantHost),
                profile = TenantProfileModel.FromDto(dto.TenantProfile),
                targetprofile = TenantProfileModel.FromDto(dto.TargetProfile),
                userpermissions = _userPermissions,
                invoiceidprefix = dto.InvoiceIdPrefix,
                timezone = dto.TimeZone,
                billingaddress = dto.BillingAddress,
                billingcompanyname = dto.BillingCompanyName,
                currency = dto.Currency,
                databaseschemaversion = tdb != null ? tdb.DatabaseSchemaVersion : "",
                databasescriptversion = tdb != null ? tdb.DatabaseScriptVersion : "",
                tenantstate = (tenantContext != null ? tenantContext.TenantState : TenantState.Unknown).ToString(),
                emailfromaddress = dto.EmailFromAddress,
                enabledatawarehousepush = dto.EnableDatawareHousePush
            };

            string databaseInvalidText = "";
            model.canenable = !dto.Enabled;
            model.tenantstatus = dto.Enabled ? 0 : TenantStatus.Disabled;

            string hostnameIncorrectlyBoundText = null;
            if (string.IsNullOrEmpty(dto.HostName))
            {
                model.canenable = false;
                model.tenantstatus |= TenantStatus.HostnameNotBound;
            } 
            else
            {
                hostnameIncorrectlyBoundText = CheckHostnameBinding(dto.TenantHost, model.hostname, dto.TenantHost.WebAppName);
                if (hostnameIncorrectlyBoundText != null)
                    model.tenantstatus |= TenantStatus.HostnameIncorrectlyBound;
                else if (dto.TenantHost.TrafficManagerName != null && dto.TenantHost.BackupWebAppName != null)
                {
                    hostnameIncorrectlyBoundText = CheckHostnameBinding(dto.TenantHost, model.hostname, dto.TenantHost.BackupWebAppName);
                    if (hostnameIncorrectlyBoundText != null)
                        model.tenantstatus |= TenantStatus.HostnameIncorrectlyBound;
                }
            }

            if (string.IsNullOrEmpty(dto.ConnectionString))
            {
                model.canenable = false;
                model.tenantstatus |= TenantStatus.DatabaseNotConfigured;
            }
            else
            {
                using (SqlConnection cn = new SqlConnection(dto.ConnectionString))
                {
                    try
                    {
                        model.database = new TenantDatabaseModel()
                        {
                            sqlservername = csb.DataSource,
                            database = csb.InitialCatalog,
                            onazure = false
                        };
                        await cn.OpenAsync();
                    }
                    catch (Exception ex)
                    {
                        model.canenable = false;
                        model.tenantstatus |= TenantStatus.DatabaseInvalid;
                        databaseInvalidText = "Database Invalid - " + ex.Message;
                    }
                }
            }

            // If the Db is on Azure we can get PricingTier info ..
            await IfAzureDbThenAsync(dto.ConnectionString, async (smc, sqlServer, sqlDb) =>
            {
                model.database = new TenantDatabaseModel()
                {
                    sqlservername = sqlServer.Name,
                    database = sqlDb.Name,
                    pricingtier = _pricingTiers.FirstOrDefault(pt => pt.id == sqlDb.CurrentServiceObjectiveId),
                    elasticdbpool = sqlDb.ElasticPoolName,
                    onazure = true
                };
                model.elasticdbpools = GetElasticPools(sqlServer.Name);
            });

            List<string> statustext = new List<string>();
            if (model.tenantstatus == TenantStatus.Online)
                statustext.Add("Online");
            if ((model.tenantstatus & TenantStatus.Disabled) != 0)
                statustext.Add("Disabled");
            if ((model.tenantstatus & TenantStatus.DatabaseNotConfigured) != 0)
                statustext.Add("Database Not Configured");
            if ((model.tenantstatus & TenantStatus.DatabaseInvalid) != 0)
                statustext.Add(databaseInvalidText);
            if ((model.tenantstatus & TenantStatus.HostnameNotBound) != 0)
                statustext.Add("Hostname Not Bound");
            if ((model.tenantstatus & TenantStatus.HostnameIncorrectlyBound) != 0)
                statustext.Add(hostnameIncorrectlyBoundText);

            model.tenantstatustext = statustext;

            return model;
        }

        private async Task<bool> GetAzureDbThenAsync(string sqlServerName, string databaseName, Func<IAzure, ISqlServer, ISqlDatabase, Task> fnAzureDb, Func<string, bool> fnNoServer = null, Func<ISqlServer, string, bool> fnNoDb = null)
        {
            try
            {
                var azure = _fnAzure();

                // Find the SqlServer ..
                var servers = azure.SqlServers.List();
                var server = (from s in servers where s.Name == sqlServerName || s.Inner.FullyQualifiedDomainName == sqlServerName select s).FirstOrDefault();

                if (server != null)
                {
                    // Server exists, find the Sql Db on it ..
                    var database = server.Databases.Get(databaseName);
                    if (database != null)
                    {
                        await fnAzureDb(azure, server, database);
                        return true;
                    }
                    else if (fnNoDb != null)
                        return fnNoDb(server, databaseName);
                }
                else if (fnNoServer != null)
                    return fnNoServer(sqlServerName);
            }
            catch
            {

            }
            return false;
        }

        private async Task<bool> IfAzureDbThenAsync(string connectionString, Func<IAzure, ISqlServer, ISqlDatabase, Task> fnAzureDb, Func<string, bool> fnNoServer = null, Func<ISqlServer, string, bool> fnNoDb = null)
        {
            try
            {
                var azure = _fnAzure();

                SqlConnectionStringBuilder csb = new SqlConnectionStringBuilder(connectionString);

                var pattern = new Regex(@"(.*:)?(?<name>[^,]*)(,\d*)?");
                var m = pattern.Match(csb.DataSource);
                var sqlServerName = m.Groups["name"].Value;

                var databaseName = csb.InitialCatalog;
                if (databaseName == null)
                    databaseName = csb["Database"].ToString();

                return await GetAzureDbThenAsync(sqlServerName, databaseName, fnAzureDb, fnNoServer);
            }
            catch
            {

            }
            return false;
        }

        [HttpGet]
        public ActionResult GetDump(string method)
        {
            try
            {
                var azure = _fnAzure();
                object o = null;
                switch (method)
                {
                    case "AppServicePlans.List":
                        o = from s in azure.AppServices.AppServicePlans.List() select s.Inner;
                        break;

                    case "AppServiceDomains.List":
                        o = from s in azure.AppServices.AppServiceDomains.List() select s.Inner;
                        break;

                    case "AppServiceWebApps.List":
                        o = from s in azure.AppServices.WebApps.List() select s.Inner;
                        break;

                    case "SqlServer.Get":
                        o = azure.SqlServers.GetByResourceGroup("getevs", "getevs-sql").Inner;
                        break;

                    case "SqlDatabases.List":
                        o = from s in azure.SqlServers.GetByResourceGroup("getevs", "getevs-sql").Databases.List() select s.Inner;
                        break;

                    case "SqlElasticPools.List":
                        o = from s in azure.SqlServers.GetByResourceGroup("getevs", "getevs-sql").ElasticPools.List() select s.Inner;
                        break;

                    case "SqlServers.List":
                        o = from s in azure.SqlServers.List() select s.Inner;
                        break;

                    case "WebApps.List":
                        o = from s in azure.WebApps.List() select s.Inner;
                        break;

                    case "ResourceGroups.List":
                        o = from s in azure.ResourceGroups.List() select s.Inner;
                        break;

                    case "DnsZones.List":
                        o = from s in azure.DnsZones.List() select s.Inner;
                        break;
                }
                return ReturnJson(o);
            }
            catch(Exception ex)
            {
                return ReturnJson(ex);
            }
        }

        [HttpGet]
        public string[] GetElasticPools(string sqlServerName)
        {
            var azure = _fnAzure();

            return (from s in azure.SqlServers.List() where s.Name == sqlServerName
                   from ep in s.ElasticPools.List()
                   select ep.Name).ToArray();
        }

        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Returns the name of the tenant currently having it's schema updated
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult CurrentlyUpdatingTenantSchema()
        {
            var current = (from t in MultiTenantManager.Tenants where t.TenantState == TenantState.UpdatingSchema select t);

            if(current.Count() == 0)
            {
                return ReturnJson(new { name = "" });
            }

            return ReturnJson(new { name = current.First().Name });
        }

        [HttpGet]
        public ActionResult TenantSearch(string search, int? pageNumber, int? itemsPerPage, bool includeArchived = false)
        {
            int totalItems = 0;
            var items = (from b in _multiTenantRepo.Search("%" + search + "%", pageNumber, itemsPerPage, includeArchived, ref totalItems) select b).ToList();
            
            var list = new List<TenantModel>();
            foreach (var t in items)
            {
                var tenant = MultiTenantManager.GetTenant(t.Id);
                list.Add(new TenantModel()
                {
                    id = t.Id,
                    name = t.Name,
                    profile = TenantProfileModel.FromDto( t.TenantProfile),
                    host = TenantHostModel.FromDto(t.TenantHost),
                    tenantstate = (tenant != null ? tenant.TenantState : TenantState.Unknown).ToString()
                });
            }
            return ReturnJson(new { count = totalItems, items = list });
        }

        [HttpGet]
        public async Task<ActionResult> TenantAsync(int id)
        {
            var dto = _multiTenantRepo.GetTenantById(id);
            if (dto == null)
                return new HttpNotFoundResult();

            var model = await MakeModelAsync(dto);

            return ReturnJson(model);
        }

        [HttpPost]
        public async Task<ActionResult> TenantSave(TenantModel record)
        {
            var progress = new ContinuousProgressJSONResponse(Response);

            var tenant = _multiTenantRepo.GetTenantById(record.id);
            if (tenant == null)
            {
                tenant = new TenantDTO();
            }

            record.CopyToDTO(tenant);

            await SetDatabasePricingTierAsync(tenant, record.database.pricingtier, record.database.elasticdbpool, progress.NewCategory("Azure Pricing Tier"));

            var savedTenant = _multiTenantRepo.SaveTenant(tenant);

            progress.Finished();

            return await TenantAsync(savedTenant.Id);
        }

        private async Task SetDatabasePricingTierAsync(TenantDTO tenant, PricingTier pricingTier, string elasticDbPool, IContinuousProgressCategory progress)
        {
            if (progress != null)
            {
                progress.Working("Checking Pricing Tier...");
            }

            await IfAzureDbThenAsync(tenant.ConnectionString, async (smc, sqlServer, sqlDb) =>
            {
                Microsoft.Azure.Management.Sql.Fluent.SqlDatabase.Update.IUpdate update = null;

                // If Pricing Tier changed (and new one is a valid tier)
                string sqlChange = "";
                string sqlArg = "";
                if (pricingTier != null && !pricingTier.iselastic)
                {
                    if (sqlDb.CurrentServiceObjectiveId != pricingTier.id)
                    {
                        update = sqlDb.Update().WithServiceObjective(pricingTier.name);
                        sqlChange = "Pricing Tier";
                        sqlArg = pricingTier.name;
                    }
                }
                else if (!string.IsNullOrEmpty(elasticDbPool))
                {
                    if (sqlDb.ElasticPoolName != elasticDbPool)
                    {
                        update = sqlDb.Update().WithExistingElasticPool(elasticDbPool);
                        sqlChange = "Elastic Db pool";
                        sqlArg = elasticDbPool;
                    }
                }

                if (update != null)
                {
                    if (progress != null)
                    {
                        progress.Working("Requesting {0} to {1}...", sqlChange, sqlArg);
                    }
                    await update.ApplyAsync();

                    if (progress != null)
                        progress.Complete("Requested {0} to {1}", sqlChange, sqlArg);

                    for (int i = 0 ; i < 100 ; i++)
                    {
                        if (progress != null)
                        {
                            progress.Working("Waiting for {0} changes", sqlChange);
                        }

                        await sqlDb.RefreshAsync();
                        if (sqlDb.CurrentServiceObjectiveId == sqlDb.RequestedServiceObjectiveId)
                        {
                            if (progress != null)
                            {
                                progress.Complete("{0} changed to {1}", sqlChange, sqlArg);
                            }
                            return;
                        }

                        if (progress != null)
                        {
                            progress.Working("Waiting for {0} changes...", sqlChange);

                            await Task.Delay(5000);
                        }
                    }
                    if (progress != null)
                    {
                        progress.Error("Timed out waiting for {0} changes, please check again later", sqlChange);
                    }
                }
                else
                {
                    if (progress != null)
                    {
                        progress.Complete("No changes required");
                    }
                }
            });

            await WaitForDatabaseReadyAsync(tenant, progress);
        }

        [HttpGet]
        public ActionResult GetSetup()
        {
            return ReturnJson(new
            {
                hosts = _multiTenantRepo.GetTenantHosts().Select(h => new TenantHostModel()
                {
                    id = h.Id,
                    name = h.Name,
                    sqlservername = h.SqlServerName,
                    location = h.Location,
                    trafficmanagername = h.TrafficManagerName,
                    webappname = h.WebAppName,
                    elasticdbpools = GetElasticPools(h.SqlServerName)
                }),
                profiles = _multiTenantRepo.GetTenantProfiles(),
                azurepricingtiers = _pricingTiers,
                currencies = _multiTenantRepo.GetCurrencies(),
                mastersite = new
                {
                    rootdomain = _config.DNSZone.ResourceName,
                    databaseNameFormat = _config.DNSZone.ResourceGroupName + "-{0}-db"
                },
                sourcedatabaseschemaversion = GetSourceDatabaseSchemaVersion(),
                sourcedatabasescriptversion = GetSourceDatabaseScriptVersion()
            });
        }

        public string GetSourceDatabaseSchemaVersion()
        {
            using(var dacpacStream = _fnConceptCaveSqlDacpacStream())
            {
                DacPackage package = DacPackage.Load(dacpacStream);
                return package.Version.ToString();  
            }
        }

        public string GetSourceDatabaseScriptVersion()
        {
            int hashcode = 0;
            var scripts = _fnAfterSchemaUpdateScripts().Concat(_fnBeforeSchemaUpdateScripts());
            foreach(var script in scripts)
            {
                hashcode ^= script.ScriptText.GetHashCode();
            }
            return hashcode.ToString("X");
        }

        [HttpGet]
        public async Task<ActionResult> GetAzureSqlServerDatabases(string resourceGroup, string serverName)
        {
            var azure = _fnAzure();

            var server = await azure.SqlServers.GetByResourceGroupAsync(resourceGroup, serverName);

            if (server == null)
                return HttpNotFound("Server not found");

            return ReturnJson(from database in server.Databases.List() select database.Inner);
        }

        [HttpPost]
        public ActionResult CheckTenantDatabaseSchema(int tenantId)
        {
            var tenant = MultiTenantManager.GetTenant(tenantId);
            var tdb = tenant.GetTenantDatabase();

            dynamic result = new ExpandoObject();
            result.canapplyschemaupdate = false;
            result.canapplyscriptsupdate = false;
            result.noschemachanges = false;
            result.noscriptchanges = false;
            using (var dacpacStream = _fnConceptCaveSqlDacpacStream())
            {
                IEnumerable<string> updateScripts = null;
                try
                {
                    var scripts = tdb.CheckDatabaseScripts(_fnBeforeSchemaUpdateScripts()).ToList();
                    updateScripts = scripts.Select(s => s.ScriptName);
                    result.scripts = updateScripts;
                    result.canapplyscriptsupdate = scripts.Count > 0;
                    result.noscriptchanges = scripts.Count == 0;
                }
                catch (Exception ex)
                {
                    result.error = ex.Message;
                }
                try
                {
                    var report = tdb.CheckDatabaseSchema(dacpacStream, true);
                    result.report = report;
                    result.canapplyschemaupdate = report.Operations != null ? report.Operations.Length > 0 : false;
                    result.noschemachanges = report.Operations != null ? report.Operations.Length == 0 : true;
                }
                catch (Exception ex)
                {
                    return ReturnJson(new { error = ex.Message });
                }
                try
                {
                    var scripts = tdb.CheckDatabaseScripts(_fnAfterSchemaUpdateScripts()).ToList();
                    result.scripts = updateScripts.Concat(scripts.Select(s => s.ScriptName));
                    result.canapplyscriptsupdate |= scripts.Count > 0;
                    result.noscriptchanges |= scripts.Count == 0;
                }
                catch (Exception ex)
                {
                    result.error = ex.Message;
                }
                result.canapplyupdate = result.canapplyschemaupdate || result.canapplyscriptsupdate;
                result.nochanges = result.noschemachanges && result.noscriptchanges;
                return ReturnJson(result);
            }
        }

        [HttpPost]
        public ActionResult UpdateTenantDatabaseSchema(int tenantId)
        {
            ActionResult result = null;
            try
            {
                UpdateTenantDatabaseSchema(tenantId, null);
                result = ReturnJson(true);
            }
            catch (Exception ex)
            {
                result = ReturnJson(ex.Message);
            }
            return result;
        }

        private void UpdateTenantDatabaseSchema(int tenantId, Action<string> fnProgress)
        {
            var tenant = MultiTenantManager.GetTenant(tenantId);
            var runningOnAzure = ConfigurationManager.AppSettings["WEBSITE_SITE_NAME"] != null;

            if (!tenant.Profile.IsDevelopment && (tenant.IsAzureDatabase && !runningOnAzure))
            {
                throw new ApplicationException("Cannot update Azure database outside Azure");
            }
            else
            {
                if (fnProgress == null)
                {
                    fnProgress = msg => { };
                }
                using (var dacpacStream = _fnConceptCaveSqlDacpacStream())
                {
                    var tdb = tenant.GetTenantDatabase();
                    tdb.UpdateDatabaseScripts(_fnBeforeSchemaUpdateScripts(), fnProgress);
                    tdb.UpdateDatabaseSchema(dacpacStream, true, fnProgress);
                    tdb.UpdateDatabaseScripts(_fnAfterSchemaUpdateScripts(), fnProgress);
                }
            }
        }

        [HttpPost]
        public async Task<ActionResult> CheckCanCreateDatabase(string server, string database)
        {
            var azure = _fnAzure();
            var sqlServer = (from s in await azure.SqlServers.ListAsync() where s.Name == server select s).FirstOrDefault();
            if (sqlServer == null)
                return ReturnJson(new { exists = false, reason = "Server not found" });

            var existing = sqlServer.Databases.Get(database);
            if (existing == null)
                return ReturnJson(new { exists = false, reason = "Database not found" });

            return ReturnJson(new { exists = true });
        }

        [HttpPost]
        public ActionResult CheckCanCreateSubdomain(string subdomain)
        {
            var tenant = MultiTenantManager.Tenants.Where(t => t.HostName.ToLower() == (subdomain + "." + _config.DNSZone.ResourceName).ToLower()).FirstOrDefault();
            if (tenant != null)
            {
                return ReturnJson(new { available = false, reason = "Not available" });
            }

            return ReturnJson(new { available = true });
        }

        [HttpPost]
        public async Task<ActionResult> CreateTenant(string name, int tenantHostId, int tenantProfileId, int tenantTargetProfileId, string database, PricingTier pricingtier, string elasticDbPool, string subdomain, string timezone, TenantDatabaseModel cloneFrom)
        {
            var progress = new ContinuousProgressJSONResponse(Response);

            var tenant = new TenantDTO()
            {
                __IsNew = true,
                Name = name,
                Enabled = true,
                TimeZone = timezone
            };

            var tenantHost = _multiTenantRepo.GetTenantHosts().FirstOrDefault(s => s.Id == tenantHostId);
            if (tenantHost == null)
                throw new HttpOperationException("Tenant Host not found");

            var tenantProfile = _multiTenantRepo.GetTenantProfiles().FirstOrDefault(s => s.Id == tenantProfileId);
            if (tenantProfile == null)
                throw new HttpOperationException("Tenant Profile not found");

            var targetProfile = _multiTenantRepo.GetTenantProfiles().FirstOrDefault(s => s.Id == tenantTargetProfileId);
            if (tenantProfile == null)
                throw new HttpOperationException("Tenant Profile not found");

            tenant.HostName = string.Format("{0}.{1}", subdomain, _config.DNSZone.ResourceName);
            tenant.TenantHost = tenantHost;
            tenant.TenantProfile = tenantProfile;
            tenant.TargetProfile = targetProfile;

            // Build the connection string
            var connectionBuilder = new SqlConnectionStringBuilder(tenant.TenantHost.ConnectionStringTemplate);
            connectionBuilder.InitialCatalog = database;
            tenant.ConnectionString = connectionBuilder.ConnectionString;

            // Save upfront, this way if something errors we have a tenant record which we can probe and fix ..
            tenant = _multiTenantRepo.SaveTenant(tenant);

            // Database and Subdomain are mutually exclusive
            // However Pricing Tier depends on Database ..
            Func<Task> databaseTask = async () => 
            {
                if (await CreateTenantDatabaseAsync(subdomain, tenant, database, cloneFrom, pricingtier, elasticDbPool, progress.NewCategory("Database")))
                {
                    var schemaProgress = progress.NewCategory("Updating database schema");

                    try
                    {
                        UpdateTenantDatabaseSchema(tenant.Id, msg => schemaProgress.Working(msg));
                        schemaProgress.Complete("Finished");
                    }
                    catch(Exception ex)
                    {
                        schemaProgress.Error(ex.Message);
                    }
                }
            };

            // Do Database first, the subdomaintask will trigger a WebApp restart
            await databaseTask();

            var savedTenant = _multiTenantRepo.SaveTenant(tenant);

            progress.Finished();

            return await TenantAsync(savedTenant.Id);
        }

        private async Task<bool> CreateTenantDatabaseAsync(string subdomain, TenantDTO tenant, string database, TenantDatabaseModel cloneFrom, PricingTier pricingtier, string elasticDbPool, IContinuousProgressCategory progress)
        {
            try
            {
                progress.Working("Contacting Azure servers...");
                
                if (!await GetAzureDbThenAsync(cloneFrom.sqlservername, cloneFrom.database, async (azure, sourceServer, sourceDatabase) =>
                {
                    var destServer = (from s in await azure.SqlServers.ListAsync() where s.Name == tenant.TenantHost.SqlServerName select s).FirstOrDefault();

                    if (pricingtier.iselastic)
                    {
                        progress.Working("Creating database {0} in pool {1}...", database, elasticDbPool);
                        await destServer.Databases
                            .Define(database)
                            .WithExistingElasticPool(elasticDbPool)
                            .WithSourceDatabase(sourceDatabase)
                            .WithMode(CreateMode.Copy)
                            .CreateAsync();

                        progress.Complete("Created database {0} in pool {1}", database, elasticDbPool);
                        await WaitForDatabaseReadyAsync(tenant, progress);
                    }
                    else
                    {
                        await destServer.Databases
                            .Define(database)
                            .WithSourceDatabase(sourceDatabase)
                            .WithMode(CreateMode.Copy)
                            .CreateAsync();
                        progress.Complete("Created database {0}", database);
                        await WaitForDatabaseReadyAsync(tenant, progress);
                        await SetDatabasePricingTierAsync(tenant, pricingtier, elasticDbPool, progress.NewCategory("Azure Pricing Tier"));
                    }
                }))
                {
                    throw new HttpOperationException("Source Database is not an Azure Database");
                }

                return true;
            }
            catch(Exception ex)
            {
                progress.Error(ex.Message);
                return false;
            }
        }

        private async Task WaitForDatabaseReadyAsync(TenantDTO tenant, IContinuousProgressCategory progress)
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(tenant.ConnectionString);
            for (int i = 0; i < 100; i++)
            {
                if (progress != null)
                {
                    progress.Working("Waiting for database");
                }

                using(var cn = new SqlConnection(tenant.ConnectionString))
                {
                    try
                    {
                        await cn.OpenAsync();

                        for (int j = 0; j < 100; j++)
                        {
                            if (progress != null)
                            {
                                progress.Working("Waiting for database ready");
                            }
                            using (var isWriteableCmd = new SqlCommand("SELECT DATABASEPROPERTYEX(DB_NAME(), 'Updateability')", cn))
                            {
                                isWriteableCmd.CommandType = System.Data.CommandType.Text;
                                var isWriteable = await isWriteableCmd.ExecuteScalarAsync();
                                if (isWriteable.ToString().ToUpper() == "READ_WRITE")
                                {
                                    if (progress != null)
                                    {
                                        progress.Complete("{0} is online and ready", builder.InitialCatalog);
                                    }
                                    return;
                                }
                                else
                                {
                                    if (progress != null)
                                    {
                                        progress.Working("Waiting for database ready...");
                                    }
                                    await Task.Delay(5000);
                                }
                            }
                        }
                    }
                    catch
                    {
                    }
                }

                if (progress != null)
                {
                    progress.Working("Waiting for database...");
                }
                await Task.Delay(5000);
            }

            if (progress != null)
            {
                progress.Error("Time out connecting - the database should appear online shortly");
            }
        }

        [HttpGet]
        public async Task<ActionResult> GetCerts(string rg)
        {
            var azure = _fnAzure();
            var certs = await azure.AppServices.AppServiceCertificates.ListByResourceGroupAsync(rg);
            return ReturnJson(from c in certs select c.Inner);
        }

        private string CheckHostnameBinding(TenantHostDTO tenantHost, string hostname, string webAppName)
        {
            if (!hostname.EndsWith(_config.DNSZone.ResourceName))
                return string.Format("'{0}' is not a subdomain of '{1}'", hostname, _config.DNSZone.ResourceName);

            return null;
        }

        [HttpDelete]
        public async Task<ActionResult> TenantDelete(int tenantId)
        {
            var progress = new ContinuousProgressJSONResponse(Response);

            var tenant = _multiTenantRepo.GetTenantById(tenantId);
            if (tenant == null)
            {
                return HttpNotFound("Tenant not found");
            }

            Func<Task> dbTask = async () => await DeleteAzureDatabase(tenant, progress.NewCategory("Database"));

            await Task.WhenAll(
                dbTask());

            var tenantRecordProgress = progress.NewCategory("Tenant record");
            bool deleted = false;
            if (progress.ErrorCount() == 0)
            {
                if (_multiTenantRepo.DeleteTenant(tenant.Id))
                {
                    deleted = true;
                    tenantRecordProgress.Complete("Deleted");
                }
                else
                {
                    tenantRecordProgress.Error("Tenant record not deleted");
                }
            }
            else
            {
                tenantRecordProgress.Warning("Not deleted due to errors");
            }
            progress.Finished();

            return ReturnJson(deleted);
        }

        private async Task DeleteAzureDatabase(TenantDTO tenant, IContinuousProgressCategory progress)
        {
            if (tenant.ConnectionString == null)
            {
                progress.Complete("No database configured");
            }
            else
            {
                progress.Working("Deleting database...");

                if (!await IfAzureDbThenAsync(tenant.ConnectionString, async (azure, sqlServer, sqlDb) =>
                {
                    try
                    {
                        sqlDb.Delete();
                        progress.Complete("Deleted");
                    }
                    catch (Exception ex)
                    {
                        progress.Error(ex.Message);
                    }
                }, sqlServerName => 
                {
                    progress.Warning("Azure SQLServer {0} does not exist", sqlServerName);
                    return true;
                }, (sqlServer, sqlDatabaseName) =>
                {
                    progress.Warning("Azure Database {0} does not exist on {1}", sqlDatabaseName, sqlServer.Name);
                    return true;
                }))
                {
                    progress.Warning("Not an Azure database");
                }
            }
        }
    }

}
