﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.BusinessLogic.LingoDataObjects;
using ConceptCave.BusinessLogic.Context;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Repository;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.www.Areas.Admin.Models;
using ConceptCave.www.Areas.Designer.Controllers;
using System.Web.Security;
using ConceptCave.Configuration;
using System.Transactions;
using ConceptCave.www.Areas.Player.Models;
using ConceptCave.Checklist.RunTime;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Repository.Facilities;
using ConceptCave.Checklist.Editor;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Importing;
using VirtualMgr.Central;
using ConceptCave.Data.DTOConverters;

namespace ConceptCave.www.Areas.Admin.Controllers
{
    [Authorize(Roles = RoleRepository.COREROLE_MEMBERSHIP)]
    public class MembershipManagementController : ControllerBaseEx
    {
        private readonly IContextManagerFactory _ctxFac;
        private readonly ICompanyRepository _companyRepo;
        private readonly IMediaRepository _mediaRepo;
        private readonly ITabletProfileRepository _tabletProfileRepo;
        private readonly IMembershipRepository _membershipRepo;
        private readonly ILanguageRepository _languageRepository;

        public class MembershipSaveHelper
        {
            public Guid id {get;set;}
            public bool? patchContext { get; set; }
            public string username {get;set;}
            public string email {get;set;}
            public string mobile {get;set;}
            public string name {get;set;}
            public string timezone {get;set;}
            public int membershipTypeId {get;set;}
            public string commencementDate {get;set;}
            public string expiryDate {get;set;}
            public decimal? latitude {get;set;}
            public decimal? longitude {get;set;}
            public string[] membershipRoleId {get;set;}
            public Guid[] labels {get;set;}
            public Guid? companyId {get;set;}
            [AllowHtml]
            public string answerModel {get;set;}
            public int? defaultTabletProfileId { get; set; }
            public string defaultLanguageCode { get; set; }
        }

        public MembershipManagementController(IContextManagerFactory ctxFac, 
            ICompanyRepository companyRepo, 
            IMediaRepository mediaRepo, 
            ITabletProfileRepository tabletProfileRepo,
            VirtualMgr.Membership.IMembership membership, 
            IMembershipRepository membershipRepo, 
            ILanguageRepository languageRepo) : base(membership, membershipRepo)
        {
            _ctxFac = ctxFac; 
            _companyRepo = companyRepo;
            _mediaRepo = mediaRepo;
            _tabletProfileRepo = tabletProfileRepo;
            _membershipRepo = membershipRepo;
            _languageRepository = languageRepo;
        }

        //
        // GET: /Admin/MembershipManagement/

        public ActionResult Index(string term, bool? expired, int? pageNumber, int? pageSize)
        {
            string search = string.Empty;

            if (string.IsNullOrEmpty(term) == false)
            {
                search = term;
            }

            int page = pageNumber.HasValue ? pageNumber.Value : 1;
            int pageS = pageSize.HasValue ? pageSize.Value : 10;

            bool includeExpired = false;
            if(expired.HasValue == true)
            {
                includeExpired = expired.Value;
            }

            var members = _membershipRepo.Search("%" + search + "%", includeExpired, page, pageS, MembershipLoadInstructions.MembershipLabel | MembershipLoadInstructions.Label).Select(r => r.ToEntity()).ToList();

            if (Request.IsAjaxRequest())
            {
                return PartialView("RenderMembershipList", members);
            }

            return View(new MembershipManagementModel() { Items = members.ToList() });
        }

        public ActionResult Add(string username, string password)
        {
            try
            {
                var user = _membershipRepo.Create(username, password, MultiTenantManager.CurrentTenant.TimeZoneInfo.Id);

                var item = _membershipRepo.GetById(user.UserId, MembershipLoadInstructions.MembershipType | MembershipLoadInstructions.MembershipRole | MembershipLoadInstructions.MembershipLabel | MembershipLoadInstructions.AspNetMembership | MembershipLoadInstructions.ContextData);
                EntityCollection<UserTypeEntity> types = MembershipTypeRepository.GetAll();
                EntityCollection<AspNetRoleEntity> roles = RoleRepository.GetAll();

                var companies = _companyRepo.GetAll(CompanyLoadInstructions.None);
                var tabletProfiles = _tabletProfileRepo.GetAll();

                ViewBag.ActionTaken = ActionTaken.New;
                return PartialView("RenderMembershipDesignerUI", new RenderMembershipDesignerUIModel(_ctxFac, companies, tabletProfiles, _languageRepository.AllLanguages()) { Entity = item, Types = types, Roles = roles, Labels = new www.Models.SelectMultipleLabelsModel(LabelRepository.GetAll(LabelFor.Users), null) });
            }
            catch (MembershipCreateUserException e)
            {
                ViewBag.ActionTaken = ActionTaken.Error;
                ViewBag.Error = e.StatusCode.ToString();
                var companies = _companyRepo.GetAll(CompanyLoadInstructions.None);
                var tabletProfiles = _tabletProfileRepo.GetAll();

                return PartialView("RenderMembershipDesignerUI", new RenderMembershipDesignerUIModel(_ctxFac, companies, tabletProfiles, _languageRepository.AllLanguages()));
            }
        }

        public ActionResult Remove(Guid id)
        {
            var entity = _membershipRepo.GetById(id, MembershipLoadInstructions.MembershipType | MembershipLoadInstructions.MembershipRole | MembershipLoadInstructions.MembershipLabel | MembershipLoadInstructions.Label | MembershipLoadInstructions.AspNetMembership);

            var companies = _companyRepo.GetAll(CompanyLoadInstructions.None);
            var tabletProfiles = _tabletProfileRepo.GetAll();
            if (id == (Guid)this.CurrentUserId)
            {
                ViewBag.ActionTaken = ActionTaken.Error;
                ViewBag.Error = "You can not delete yourself";


                return PartialView("RenderMembershipDesignerUI", new RenderMembershipDesignerUIModel(_ctxFac, companies, tabletProfiles, _languageRepository.AllLanguages()) { Entity = entity });
            }

            ViewBag.ActionTaken = ActionTaken.Remove;

            _membershipRepo.Remove(id);

            return PartialView("RenderMembershipDesignerUI", new RenderMembershipDesignerUIModel(_ctxFac, companies, tabletProfiles, _languageRepository.AllLanguages()) { Entity = entity });
        }

        public ActionResult Save(MembershipSaveHelper data)
        {
            var entity = _membershipRepo.GetById(data.id, MembershipLoadInstructions.MembershipRole | MembershipLoadInstructions.MembershipLabel | MembershipLoadInstructions.AspNetMembership | MembershipLoadInstructions.ContextData);
            IEnumerable<string> deletedRoles = null, newRoles = null;
            IEnumerable<Guid> deletedLabels = null, newLabels = null;
            if (!data.patchContext.HasValue || data.patchContext == false)
            {
                entity.AspNetUser.UserName = data.username;
                entity.AspNetUser.Email = data.email;
                entity.Mobile = data.mobile;
                entity.Name = data.name;
                entity.UserTypeId = data.membershipTypeId;
                entity.TimeZone = data.timezone;
                if (data.companyId == Guid.Empty)
                {
                    entity.CompanyId = null;
                }
                else
                {
                    entity.CompanyId = data.companyId;
                }

                if (data.latitude.HasValue == true)
                {
                    entity.Latitude = data.latitude.Value;
                    entity.Longitude = data.longitude.Value;
                }

                if (string.IsNullOrEmpty(data.commencementDate) == false)
                {
                    DateTime commenced = DateTime.MinValue;
                    if (DateTime.TryParseExact(data.commencementDate, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.AssumeUniversal, out commenced) == true)
                    {
                        entity.CommencementDate = commenced;
                    }
                }
                else
                {
                    entity.CommencementDate = null;
                }

                entity.DefaultTabletProfileId = data.defaultTabletProfileId;
                entity.DefaultLanguageCode = data.defaultLanguageCode;

                if (string.IsNullOrEmpty(data.expiryDate) == false)
                {
                    DateTime expires = DateTime.MinValue;
                    if (DateTime.TryParseExact(data.expiryDate, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.AssumeUniversal, out expires) == true)
                    {
                        entity.ExpiryDate = expires;
                    }
                }
                else
                {
                    entity.ExpiryDate = null;
                }

                // first any deleted roles
                if (data.membershipRoleId == null)
                {
                    data.membershipRoleId = new string[] { };
                }

                deletedRoles = (from m in entity.AspNetUser.AspNetUserRoles where data.membershipRoleId.Contains(m.RoleId) == false select m.RoleId);
                // now any new roles
                newRoles = (from m in data.membershipRoleId where (from q in entity.AspNetUser.AspNetUserRoles select q.RoleId).Contains(m) == false select m);

                newRoles.ToList().ForEach(i =>
                {
                    entity.AspNetUser.AspNetUserRoles.Add(new AspNetUserRoleDTO() { RoleId = i, UserId = entity.AspNetUserId, __IsNew = true });
                });

                // now any deleted labels
                if (data.labels == null)
                {
                    data.labels = new Guid[] { };
                }
                deletedLabels = (from m in entity.LabelUsers where data.labels.Contains(m.LabelId) == false select m.LabelId);
                // now any new labels
                newLabels = (from m in data.labels where (from q in entity.LabelUsers select q.LabelId).Contains(m) == false select m);

                newLabels.ToList().ForEach(i =>
                {
                    entity.LabelUsers.Add(new LabelUserDTO() { LabelId = i, UserId = data.id, __IsNew = true });
                });
            }
            // load up each of the contexts into a working document and get the AnswerModel to populate them with answers
            var workings = new List<IWorkingDocument>();
            var ctxGetters = new List<ContextWorkingDocumentObjectGetter>();
            if (string.IsNullOrEmpty(data.answerModel) == false)
            {
                //  now the context side of things
                var model = System.Web.Helpers.Json.Decode<AnswerModel>(data.answerModel); // get the serialized context stuff from the JSON string

                var contextManager = _ctxFac.GetInstance(data.id, ContextType.Membership, data.id);
                // loop through the various types of context we have against the user type
                foreach (var typeContextName in contextManager.ContextNames())
                {
                    var contextGetter = contextManager.LoadContextDocument(typeContextName);
                    workings.Add(contextGetter.GetWorkingDocument(null));
                    ctxGetters.Add(contextGetter);
                }

                model.PopulateAnswersFromQuestions(workings.Cast<WorkingDocument>().ToList());
            }

            // Errr, this is annoying entity framework doesn't play nice with transaction scope's :(
            // so adding roles to a user by changing the user type doesn't work when a transaction scope
            // is involved.
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                foreach (var getter in ctxGetters)
                    getter.Save();

                _membershipRepo.Save(entity, true, true);

                if (deletedRoles != null)
                {
                	_membershipRepo.RemoveFromRoles(deletedRoles.ToArray(), data.id);
                }
                if (deletedLabels != null)
                {
                	_membershipRepo.RemoveFromLabels(deletedLabels.ToArray(), data.id);
                }

                scope.Complete();

                ViewBag.ActionTaken = ActionTaken.Save;
            }

            entity = _membershipRepo.GetById(data.id, MembershipLoadInstructions.MembershipType | MembershipLoadInstructions.MembershipRole | MembershipLoadInstructions.MembershipLabel | MembershipLoadInstructions.Label | MembershipLoadInstructions.AspNetMembership | MembershipLoadInstructions.ContextData);
            EntityCollection<UserTypeContextPublishedResourceEntity> tcs = MembershipTypeRepository.GetByUserType(entity.UserTypeId, UserTypeContextLoadInstructions.PublishingGroupResource | UserTypeContextLoadInstructions.PublishingGroupResourceData);

            var companies = _companyRepo.GetAll(CompanyLoadInstructions.None);
            var tabletProfiles = _tabletProfileRepo.GetAll();

            return PartialView("RenderMembershipDesignerUI", new RenderMembershipDesignerUIModel(_ctxFac, companies, tabletProfiles, _languageRepository.AllLanguages()) { Entity = entity, TypeContexts = tcs, Types = MembershipTypeRepository.GetAll(), Roles = RoleRepository.GetAll(), Labels = new www.Models.SelectMultipleLabelsModel(LabelRepository.GetAll(LabelFor.Users), (from m in entity.LabelUsers select m.Label.ToEntity())) });
        }

        public JsonResult ChangePassword(Guid id, string password)
        {
            var member = _membership.GetUser(id);
            bool result = false;
            var jsonResult = new Dictionary<string, object>();
            jsonResult["Message"] = "";

            try
            {
                // EVS-415 Change Password Checklist To Support "Unlocking" Locked Users
                // When ever password is changed successfully we unlock the user
                if (member.IsLockedOut)
                    member.UnlockUser();

                result = member.ChangePassword(string.Empty, password);
            }
            catch (Exception e)
            {
                jsonResult["Message"] = e.Message;
            }

            jsonResult["Success"] = result;

            return Json(jsonResult);
        }

        [HttpPost]
        public ActionResult MediaUpload(HttpPostedFileBase file, string name, string description, Guid id)
        {
            MediaDTO media = null;

            var member = _membershipRepo.GetById(id, MembershipLoadInstructions.Media | MembershipLoadInstructions.MediaData);

            try
            {
                byte[] data = new byte[file.ContentLength];
                file.InputStream.Read(data, 0, file.ContentLength);

                if (member.MediaId.HasValue == false)
                {
                    using (TransactionScope scope = new TransactionScope())
                    {
                        media = _mediaRepo.CreateMediaFromPostedFile(data, name, description, file.FileName, file.ContentType, false, (Guid)this.CurrentUserId);
                        member.MediaId = media.Id;
                        _membershipRepo.Save(member, true, false);

                        scope.Complete();
                    }
                }
                else
                {
                    media = _mediaRepo.UpdateMediaFromPostedFile(data, member.MediaId.Value, string.IsNullOrEmpty(name) == false ? name : string.Format("Image for {0} member", member.Name), string.IsNullOrEmpty(description) == false ? description : string.Format("Image for {0} member", member.Name), file.FileName, file.ContentType, false, (Guid)this.CurrentUserId);
                }
            }
            catch (Exception e)
            {
                ImportParserResult r = new ImportParserResult();
                r.Errors.Add(e.Message);

                List<ImportParserResult> rs = new List<ImportParserResult>();
                rs.Add(r);

                return ReturnJson(rs);
            }

            ImportParserResult result = new ImportParserResult() { Count = 1, Data = member.MediaId };
            List<ImportParserResult> results = new List<ImportParserResult>();
            results.Add(result);

            return ReturnJson(results);
        }
    }
}
