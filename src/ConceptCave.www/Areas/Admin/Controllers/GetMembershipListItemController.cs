﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.Data.DTOConverters;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Repository;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;

namespace ConceptCave.www.Areas.Admin.Controllers
{
    public class GetMembershipListItemController : ControllerBase
    {
        private readonly IMembershipRepository _membershipRepo;

        public GetMembershipListItemController(IMembershipRepository membershipRepo)
        {
            _membershipRepo = membershipRepo;
        }
        //
        // GET: /Admin/GetMembershipListItem/

        public ActionResult Index(Guid id)
        {
            var entity = _membershipRepo.GetById(id, MembershipLoadInstructions.MembershipType | MembershipLoadInstructions.MembershipLabel | MembershipLoadInstructions.Label).ToEntity();

            return View(entity);
        }

    }
}
