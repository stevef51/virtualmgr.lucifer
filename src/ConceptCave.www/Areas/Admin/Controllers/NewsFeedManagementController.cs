﻿using ConceptCave.Checklist;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Repository;
using ConceptCave.RepositoryInterfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ConceptCave.www.Areas.Admin.Controllers
{
    [Authorize(Roles = RoleRepository.COREROLE_NEWSFEED)]
    public class NewsFeedManagementController : ControllerBaseEx
    {
        protected INewsFeedRepository _newsFeedRepo;

        public NewsFeedManagementController(INewsFeedRepository newsFeedRepo,
            VirtualMgr.Membership.IMembership membership,
            IMembershipRepository memberRepo) : base(membership, memberRepo)
        {
            _newsFeedRepo = newsFeedRepo;
        }

        [HttpGet]
        public ActionResult Channels()
        {
            var channels = _newsFeedRepo.AvailableChannels();

            return ReturnJson(channels);
        }

        [HttpGet]
        public ActionResult ChannelItems(string channel)
        {
            var items = _newsFeedRepo.Get(channel, null, null, null, 20);

            return ReturnJson(items);
        }

        [HttpPost]
        public ActionResult ChannelItem(NewsFeedDTO item)
        {
            if (item.__IsNew == true)
            {
                item.Id = CombFactory.NewComb();
                item.DateCreated = DateTime.UtcNow;
                item.CreatedBy = (Guid)this.CurrentUserId;
            }

            _newsFeedRepo.Save(item, true, false);

            return ReturnJson(item);
        }

        public ActionResult Index()
        {
            return View();
        }
    }
}
