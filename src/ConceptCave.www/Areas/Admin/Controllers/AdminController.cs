﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.Repository;

namespace ConceptCave.www.Areas.Admin.Controllers
{
    // This file defines stub controllers that map to nodes in the site map file
    // that have child menu's but no end point themselves. They define the set of
    // roles, one of which needs to be present for the menu item to be visible.

    [Authorize(Roles = RoleRepository.COREROLE_ADMIN)]
    public class AdminController : ControllerBase
    {
    }

    [Authorize(Roles = RoleRepository.COREROLE_ADMIN_USERSANDROLES)]
    public class AdminUsersAndRolesController : ControllerBase
    {
    }

    [Authorize(Roles = RoleRepository.COREROLE_ADMIN_LISTS)]
    public class AdminListsController : ControllerBase
    {
    }

    [Authorize(Roles = RoleRepository.COREROLE_ADMIN_MEDIAANDNEWS)]
    public class AdminMediaAndNewsController : ControllerBase
    {
    }

    [Authorize(Roles = RoleRepository.COREROLE_ADMIN_ROSTERSANDSCHEDULES)]
    public class AdminRostersAndSchedulesController : ControllerBase
    {
    }

    [Authorize(Roles = RoleRepository.COREROLE_ADMIN_SYSTEM)]
    public class AdminSystemController : ControllerBase
    {
    }
}
