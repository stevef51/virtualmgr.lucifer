﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.Repository;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.DTO.DTOClasses;
using System.Transactions;
using ConceptCave.Checklist;
using Newtonsoft.Json.Linq;

namespace ConceptCave.www.Areas.Admin.Controllers
{
    [Authorize(Roles = RoleRepository.COREROLE_STANDARDS)]
    public class StandardsManagementController : ControllerBase
    {
        public class StandardModel
        {
            public bool __IsNew { get; set; }

            // used to track the type between client and server, in case the user edits the type
            public string Id { get; set; }

            public string Type { get; set; }
            public decimal Value { get; set; }
            public decimal? Value2 { get; set; }
            public string Comparison { get; set; }
        }

        protected IGlobalSettingsRepository _globalRepo;

        public StandardsManagementController(IGlobalSettingsRepository globalRepo)
        {
            _globalRepo = globalRepo;
        }

        public ActionResult Index()
        {
            return View();
        }

        protected List<StandardModel> ConvertToModel(string jsonString)
        {
            if (string.IsNullOrEmpty(jsonString))
            {
                return new List<StandardModel>();
            }

            JObject standards = JObject.Parse(jsonString);
            JArray c = (JArray)standards["compliance"];

            List<StandardModel> result = new List<StandardModel>();

            foreach(var s in c)
            {
                var standard = ConvertToModel((JObject)s);

                result.Add(standard);
            }

            return result;
        }

        protected StandardModel ConvertToModel(JObject item)
        {
            var r = new StandardModel()
            {
                Type = item["type"].ToString(),
                Id = item["type"].ToString(),
                Value = decimal.Parse(item["value"].ToString()),
                Comparison = item["comparison"].ToString()
            };

            if(item["value2"] != null)
            {
                r.Value2 = decimal.Parse(item["value2"].ToString());
            }

            return r;
        }

        protected string ToJson(IList<StandardModel> standards)
        {
            JObject root = new JObject();
            JArray compliance = new JArray();
            root.Add("compliance", compliance);

            foreach(var standard in standards)
            {
                JObject s = new JObject();
                s["type"] = standard.Type;
                s["value"] = standard.Value;
                if(standard.Value2.HasValue)
                {
                    s["value2"] = standard.Value2;
                }
                s["comparison"] = standard.Comparison;

                compliance.Add(s);
            }

            return root.ToString();
        }

        [HttpGet]
        public ActionResult Standards()
        {
            var result = _globalRepo.GetSetting<string>("HACCP");

            return ReturnJson(ConvertToModel(result));
        }

        [HttpPost]
        public ActionResult StandardSave(StandardModel standard)
        {
            var jsonString = _globalRepo.GetSetting<string>("HACCP");
            var standards = ConvertToModel(jsonString);

            var toChange = (from s in standards where s.Type == standard.Id select s).DefaultIfEmpty().First();

            if(toChange == null)
            {
                toChange = new StandardModel();
                standards.Add(toChange);
            }

            toChange.Type = standard.Type;
            toChange.Value = standard.Value;
            toChange.Value2 = standard.Value2;
            toChange.Comparison = standard.Comparison;

            string result = ToJson(standards);

            _globalRepo.SetSetting("HACCP", result);

            return ReturnJson(ConvertToModel(result));
        }
    }
}
