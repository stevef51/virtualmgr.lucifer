﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Repository;

namespace ConceptCave.www.Areas.Admin.Controllers
{
    public class GetLabelListItemController : ControllerBase
    {
        //
        // GET: /Admin/GetLabelListItem/

        public ActionResult Index(Guid id)
        {
            LabelEntity item = LabelRepository.GetById(id);

            return View(item);
        }

    }
}
