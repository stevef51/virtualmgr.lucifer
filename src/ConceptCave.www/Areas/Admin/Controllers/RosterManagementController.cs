﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Repository;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.BusinessLogic.Configuration;
using System.Transactions;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.RepositoryInterfaces.TaskEvents;

namespace ConceptCave.www.Areas.Admin.Controllers
{
     [Authorize(Roles = RoleRepository.COREROLE_ROSTERING)]
    public class RosterManagementController : ControllerBase
    {
         public class RostersHelper
         {
             public bool AllowAutomation { get; set; }
             public IList<RosterDTO> Rosters { get; set; }

             public IList<EventHelper> Events { get; set; }

             public IList<TaskTypeHelper> TaskTypes { get; set; }

             public IList<RosterConditionActionHelper> RosterConditionActions { get; set; }

             public IList<TimeZoneInfo> Timezones { get; set; }
         }

         public class EventHelper
         {
             public int Id { get; set; }
             public string Name { get; set; }

             public bool Timed { get; set; }

             public EventHelper() { }

             public EventHelper(RosterProjectJobTaskEventDTO dto)
             {
                 fromDTO(dto);
             }

             public void fromDTO(RosterProjectJobTaskEventDTO dto)
             {
                 Id = dto.Id;
                 Name = dto.Name;
                 Timed = dto.Timed;
             }
         }

         public class TaskTypeHelper
         {
            public Guid Id { get; set; }
            public string Name { get; set; }

            public TaskTypeHelper() { }

            public TaskTypeHelper(ProjectJobTaskTypeDTO dto)
            {
                fromDTO(dto);
            }

            public void fromDTO(ProjectJobTaskTypeDTO dto)
            {
                Id = dto.Id;
                Name = dto.Name;
            }
         }

         public class RosterConditionActionHelper
         {
             public string Id { get; set; }
             public string Name { get;set;}
             public string Directive { get; set; }

             public RosterConditionActionHelper()
             {
             }

             public RosterConditionActionHelper(RosterConditionActionItem item)
             {
                 Id = item.Id;
                 Name = item.Name;
                 Directive = item.Directive;
             }
         }

         private IRosterRepository _rosterRepo;
         private ITaskTypeRepository _taskTypeRepo;
         private IRepositorySectionManager _repositorySectionManager;
         private IRosterTaskEventsManager _rosterTaskEventsManager;

         public RosterManagementController(IRosterRepository rosterRepo, 
             ITaskTypeRepository taskTypeRepo,
             IRepositorySectionManager repositorySectionManager,
             IRosterTaskEventsManager rosterTaskEventsManager)
         {
             _rosterRepo = rosterRepo;
             _taskTypeRepo = taskTypeRepo;
             _repositorySectionManager = repositorySectionManager;
             _rosterTaskEventsManager = rosterTaskEventsManager;
         }

         protected IList<EventHelper> ToEvents(IList<RosterProjectJobTaskEventDTO> events)
         {
             return events.Select(e => new EventHelper(e)).ToList();
         }

         protected IList<TaskTypeHelper> ToTaskTypes(IList<ProjectJobTaskTypeDTO> types)
         {
             return types.Select(t => new TaskTypeHelper(t)).ToList();
         }

         protected IList<RosterConditionActionHelper> ToActions(IList<RosterConditionActionItem> actions)
         {
             return actions.Select(a => new RosterConditionActionHelper(a)).ToList();
         }

         [HttpGet]
         public ActionResult Rosters()
         {
             var result = _rosterRepo.GetAll(RosterLoadInstructions.Conditions | RosterLoadInstructions.ConditionActions).OrderBy(r => r.SortOrder).ThenBy(r => r.Name).ToList();

             return ReturnJson(new RostersHelper() 
             { 
                 AllowAutomation = false, 
                 Rosters = result,
                 Events = ToEvents(_rosterRepo.GetAllTaskEventTypes()),
                 TaskTypes = ToTaskTypes(_taskTypeRepo.GetAll(ProjectJobTaskTypeLoadInstructions.None)),
                 RosterConditionActions = ToActions(_repositorySectionManager.RosterConditionActions),
                 Timezones = TimeZoneInfo.GetSystemTimeZones()
             });
         }

         [HttpPost]
         public ActionResult RosterSave(RosterDTO roster)
         {
            RosterDTO result = null;
            
            using(TransactionScope scope = new TransactionScope())
            {
                if(roster.__IsNew == false)
                {
                    var r = _rosterRepo.GetById(roster.Id, RosterLoadInstructions.Conditions | RosterLoadInstructions.ConditionActions);

                    // we need to work out what's been deleted (if anything) from the conditions
                    var deletedConditions = (from c in r.RosterEventConditions where 
                                       (from a in roster.RosterEventConditions select a.Id).Contains(c.Id) == false select c.Id);

                    // also the deleted condition actions we need to delete them
                    var deletedActions = (from c in r.RosterEventConditions 
                                          from a in c.RosterEventConditionActions 
                                          where (from cc in roster.RosterEventConditions 
                                                 from aa in cc.RosterEventConditionActions select aa.Id).Contains(a.Id) == false select a.Id);

                    _rosterRepo.RemoveRosterEventConditionActions(deletedActions.ToArray());
                    _rosterRepo.RemoveRosterEventConditions(deletedConditions.ToArray());
                }

                // set the next time the conditions are meant to run (in UTC)
                TimeZoneInfo tz = TimeZoneHelpers.FindSystemTimeZoneById(roster.Timezone);
                DateTime localNow = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, tz);
                _rosterTaskEventsManager.SetNextUTCRunTime(localNow, tz, roster.RosterEventConditions);

                result = _rosterRepo.Save(roster, true, true);

                scope.Complete();
            }

             return ReturnJson(result);
         }

         [HttpPost]
         public ActionResult RosterClearScheduleApprovalRun(int roster)
         {
             var result = _rosterRepo.GetById(roster);

             result.AutomaticScheduleApprovalTimeLastRun = null;

             result = _rosterRepo.Save(result, false, false);

             result = _rosterRepo.GetById(roster, RosterLoadInstructions.ConditionActions | RosterLoadInstructions.Conditions);

             return ReturnJson(result);
         }

         [HttpPost]
         public ActionResult RosterClearEndOfDayRun(int roster)
         {
             var result = _rosterRepo.GetById(roster);

             result.AutomaticEndOfDayTimeLastRun = null;

             result = _rosterRepo.Save(result, false, false);

             result = _rosterRepo.GetById(roster, RosterLoadInstructions.ConditionActions | RosterLoadInstructions.Conditions);

             return ReturnJson(result);
         }

        public ActionResult Index()
        {
            return View();
        }

    }
}
