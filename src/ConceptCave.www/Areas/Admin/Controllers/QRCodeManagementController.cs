﻿using ConceptCave.BusinessLogic;
using ConceptCave.BusinessLogic.Context;
using ConceptCave.BusinessLogic.Invoices;
using ConceptCave.BusinessLogic.Models;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Repository;
using ConceptCave.Repository.ImportMapping;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Importing;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.www.Areas.Admin.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using VirtualMgr.Central;

namespace ConceptCave.www.Areas.Admin.Controllers
{
    [Authorize(Roles = RoleRepository.COREROLE_QRCODEADMIN)]
    public class QRCodeManagementController : ControllerBase
    {
        private readonly IQRCodeRepository _qrcodeRepo;
        private readonly Func<QRCodeExportParser> _fnQrcodeExportParser;
        private readonly Func<QRCodeImportWriter> _fnQrcodeImportWriter;

        public QRCodeManagementController(IQRCodeRepository qrcodeRepo, Func<QRCodeExportParser> fnQrcodeExportParser, Func<QRCodeImportWriter> fnQrcodeImportWriter)
        {
            _qrcodeRepo = qrcodeRepo;
            _fnQrcodeExportParser = fnQrcodeExportParser;
            _fnQrcodeImportWriter = fnQrcodeImportWriter;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Search(string search, int? pageNumber, int? itemsPerPage, bool includeArchived = false, int? searchFlags = null, int? includeFlags = null, bool? showSelected = null)
        {
            int totalItems = 0;
            var items = _qrcodeRepo.Search("%" + search + "%", pageNumber, itemsPerPage, includeArchived, ref totalItems, searchFlags, includeFlags, showSelected, QRCodeLoadInstructions.User | QRCodeLoadInstructions.TaskType | QRCodeLoadInstructions.Asset);

            return ReturnJson(new { count = totalItems, items = from i in items select QrcodeModel.FromDto(i) });
        }

        [HttpGet]
        public ActionResult Qrcode(int id)
        {           
            return ReturnJson(LoadQrcode(id));
        }

        protected QrcodeModel LoadQrcode(int id)
        {
            return QrcodeModel.FromDto(_qrcodeRepo.GetById(id, QRCodeLoadInstructions.Asset | QRCodeLoadInstructions.TaskType | QRCodeLoadInstructions.User));
        }
       
        [HttpDelete]
        public ActionResult QrcodeDelete(int id)
        {
            bool archived = false;
            _qrcodeRepo.Delete(id, true, out archived);
            if (!archived)
                return ReturnJson(true);
            else
                return ReturnJson(new { archived = true });
        }

        [HttpPost]
        public ActionResult SetSelection(int id, bool selected)
        {
            var dto = _qrcodeRepo.GetById(id, QRCodeLoadInstructions.None);
            if (dto != null)
            {
                if (dto.Selected != selected)
                {
                    dto.Selected = selected;

                    dto = _qrcodeRepo.Save(dto, true, false);
                }

                return ReturnJson(dto.Selected);
            }

            return ReturnJson(null);
        }

        [HttpPost]
        public ActionResult QrcodeSave(QrcodeModel record)
        {
            var dto = _qrcodeRepo.GetById(record.id, QRCodeLoadInstructions.None);

            if (dto == null)
            {
                dto = new QrcodeDTO()
                {
                    __IsNew = true,
                };
            }

            dto.Qrcode = record.qrcode;
            dto.TaskTypeId = record.tasktypeid;
            dto.UserId = record.userid;
            dto.AssetId = record.assetid;

            var savedDto = dto;
            using (TransactionScope scope = new TransactionScope())
            {
                savedDto = _qrcodeRepo.Save(dto, true, true);

                scope.Complete();
            }

            dto = _qrcodeRepo.GetById(savedDto.Id, QRCodeLoadInstructions.Asset | QRCodeLoadInstructions.TaskType | QRCodeLoadInstructions.User);

            return ReturnJson(QrcodeModel.FromDto(dto));
        }

        [HttpGet]
        public FileResult Export(bool onlySelected = false)
        {
            var parser = _fnQrcodeExportParser();
            parser.IncludeTitles = true;
            var writer = new ExcelWriter();
            var transforms = QRCodeImportExportTransformFactory.Create();

            int total = -1;
            var items = _qrcodeRepo.Search("%", null, null, false, ref total, null, null, onlySelected ? (bool?)true : null, QRCodeLoadInstructions.Asset | QRCodeLoadInstructions.TaskType | QRCodeLoadInstructions.User);

            Action<int> progressCallback = counter =>
            {
                // not doing anything at the moment
            };

            // I know calling Import seems backwards, think of it as importing the data into Excel
            parser.Import(items, transforms, writer, false, progressCallback);

            Response.AddHeader("Content-Disposition", "inline; filename=QRCodeExport.xlsx");

            byte[] data;
            using (MemoryStream stream = new MemoryStream())
            {
                writer.Book.SaveAs(stream);

                data = stream.ToArray();
            }

            return this.File(data, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        }

        [HttpPost]
        public ActionResult Import(HttpPostedFileBase file)
        {
            QRCodeImportWriter writer = _fnQrcodeImportWriter(); 

            byte[] data = new byte[file.ContentLength];
            file.InputStream.Read(data, 0, file.ContentLength);

            ImportParserResult result = null;

            Action<int> progressCallback = counter =>
            {
                // not doing anything at the moment
            };

            using (var stream = new MemoryStream(data))
            {
                ExcelImportParser parser = new ExcelImportParser();
                var transform = QRCodeImportExportTransformFactory.Create();

                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(0, 10, 0)))
                {
                    result = parser.Import(stream, transform, writer, false, progressCallback);

                    try
                    {
                        if (result.Success)
                        {
                            scope.Complete();
                        }
                    }
                    catch (Exception e)
                    {
                        result.Errors.Add(string.Format("A problem was encountered committing the data to the database, the error message given was {0}", e.Message));
                    }
                }
            }

            return ReturnJson(result);
        }


    }
}