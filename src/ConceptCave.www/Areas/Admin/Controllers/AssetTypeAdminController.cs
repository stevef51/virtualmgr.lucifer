﻿using ConceptCave.BusinessLogic.Invoices;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Repository;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.www.Areas.Admin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using VirtualMgr.Central;

namespace ConceptCave.www.Areas.Admin.Controllers
{
    [Authorize(Roles = RoleRepository.COREROLE_ASSETTYPEADMIN)]
    public class AssetTypeAdminController : ControllerBase
    {
        private readonly IAssetTypeRepository _assetTypeRepo;
        private readonly IPublishingGroupRepository _pubRepo;
        private readonly IAssetTypeCalendarRepository _assetTypeCalendarRepo;
        private readonly IAssetTypeCalendarTaskRepository _assetTypeCalenderTaskRepo;

        public AssetTypeAdminController(IAssetTypeRepository assetTypeRepo, IPublishingGroupRepository pubRepo, IAssetTypeCalendarRepository assetTypeCalendarRepo, IAssetTypeCalendarTaskRepository assetTypeCalendarTaskRepo)
        {
            _assetTypeRepo = assetTypeRepo;
            _pubRepo = pubRepo;
            _assetTypeCalendarRepo = assetTypeCalendarRepo;
            _assetTypeCalenderTaskRepo = assetTypeCalendarTaskRepo;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Search(string search, int? pageNumber, int? itemsPerPage, bool includeArchived = false)
        {
            int totalItems = 0;
            var items = _assetTypeRepo.Search("%" + search + "%", pageNumber, itemsPerPage, includeArchived, ref totalItems);

            return ReturnJson(new { count = totalItems, items = from i in items select AssetTypeModel.FromDto(i, null) });
        }

        [HttpGet]
        public ActionResult AssetType(int id)
        {
            var groups = _pubRepo.GetByAllowedForAssetContext(PublishingGroupLoadInstruction.Resources).ToList();

            return ReturnJson(AssetTypeModel.FromDto(_assetTypeRepo.GetById(id, AssetTypeLoadInstructions.AssetTypeContext | AssetTypeLoadInstructions.Calendar | AssetTypeLoadInstructions.CalendarTask), groups));
        }

        [HttpDelete]
        public ActionResult AssetTypeDelete(int id)
        {
            bool archived = false;
            _assetTypeRepo.Delete(id, true, out archived);
            if (!archived)
                return ReturnJson(true);
            else
                return ReturnJson(new { archived = true });
        }


        [HttpPost]
        public ActionResult AssetTypeSave(AssetTypeModel record)
        {
            var dto = _assetTypeRepo.GetById(record.id, AssetTypeLoadInstructions.AssetTypeContext);
            if (dto == null)
            {
                dto = new AssetTypeDTO()
                {
                    __IsNew = true
                };
            }

            dto.Archived = false;           // Saving will always Unarchive a record
            dto.Name = record.name;
            dto.Backcolor = record.backcolor;
            dto.Forecolor = record.forecolor;
            dto.CanTrack = record.cantrack;

            using (TransactionScope scope = new TransactionScope())
            {
                dto = _assetTypeRepo.SaveAssetType(dto, true, true);

                if (record.Contexts != null)
                {
                    var selectedResources = (from c in record.Contexts from r in c.Resources where r.Selected == true select r.Id).ToList();

                    // deleted contexts
                    var deletedContexts = (from m in dto.AssetTypeContextPublishedResources where selectedResources.Contains(m.PublishingGroupResourceId) == false select m.PublishingGroupResourceId);

                    // now any new contexts
                    var newContexts = (from m in selectedResources where (from q in dto.AssetTypeContextPublishedResources select q.PublishingGroupResourceId).Contains(m) == false select m);

                    _assetTypeRepo.RemoveContextsFromAssetType(deletedContexts.ToArray(), dto.Id);
                    _assetTypeRepo.AddContextsToAssetType(newContexts.ToArray(), dto.Id);
                }

                scope.Complete();
            }

            dto = _assetTypeRepo.GetById(dto.Id, AssetTypeLoadInstructions.AssetTypeContext | AssetTypeLoadInstructions.Calendar | AssetTypeLoadInstructions.CalendarTask);
            var groups = _pubRepo.GetByAllowedForAssetContext(PublishingGroupLoadInstruction.Resources).ToList();

            return ReturnJson(AssetTypeModel.FromDto(dto, groups));
        }

        [HttpPost]
        public ActionResult AssetTypeCalendarSave(AssetTypeCalendarModel record)
        {
            AssetTypeCalendarDTO dto = record.__isnew ? new AssetTypeCalendarDTO() { __IsNew = true } : _assetTypeCalendarRepo.GetById(record.id, AssetTypeCalendarLoadInstructions.CalendarTask);
            if (dto == null)
            {
                dto = new AssetTypeCalendarDTO()
                {
                    __IsNew = true
                };
            }

            dto.Archived = false;           // Saving will always Unarchive a record
            dto.Name = record.name;
            dto.AssetTypeId = record.assettypeid;
            dto.ScheduleExpression = record.scheduleexpression;
            dto.TimeZone = record.timezone;
        
            // Remove deleted Tasks
            List<AssetTypeCalendarTaskDTO> removeDtos = new List<AssetTypeCalendarTaskDTO>();
            List<AssetTypeCalendarTaskLabelDTO> removeLabelDtos = new List<AssetTypeCalendarTaskLabelDTO>();
            foreach (var taskDto in dto.AssetTypeCalendarTasks)
            {
                var taskModel = record.tasks?.FirstOrDefault(t => t.id == taskDto.Id);
                if (taskModel == null)
                {
                    // mark to be deleted
                    removeDtos.Add(taskDto);
                }
                else
                {
                    // edit the dto from the model and remove it from the list
                    taskDto.Archived = false;
                    taskDto.ProjectJobTaskType = null;      // Let the .TaskTypeId define the column value
                    taskDto.TaskTypeId = taskModel.tasktypeid;
                    taskDto.HierarchyRole = null;           // Let the .HierarchyRoleId define the column value
                    taskDto.HierarchyRoleId = taskModel.hierarchyroleid;
                    taskDto.TaskSiteId = taskModel.tasksiteid;
                    taskDto.TaskLengthInDays = taskModel.tasklengthindays;
                    taskDto.TaskLevel = taskModel.tasklevel;
                    taskDto.TaskName = taskModel.taskname;
                    taskDto.TaskDescription = taskModel.taskdescription;
                    if (taskModel.starttimelocal != null)
                    {
                        taskDto.StartTimeLocal = TimeSpan.Parse(taskModel.starttimelocal);
                    }
                    else
                    {
                        taskDto.StartTimeLocal = null;
                    }

                    if (taskModel.labels != null)
                    {
                        removeLabelDtos.AddRange(taskDto.AssetTypeCalendarTaskLabels.Where(l => !taskModel.labels.Contains(l.LabelId)));
                        var newLabels = taskModel.labels.Where(id => !taskDto.AssetTypeCalendarTaskLabels.Any(label => label.LabelId == id))
                            .Select(id => new AssetTypeCalendarTaskLabelDTO()
                            {
                                __IsNew = true,
                                LabelId = id,
                                AssetTypeCalendarTask = taskDto
                            });

                        taskDto.AssetTypeCalendarTaskLabels = newLabels.Concat(taskDto.AssetTypeCalendarTaskLabels.Where(l => taskModel.labels.Contains(l.LabelId))).ToList();
                    }
                    else
                    {
                        removeLabelDtos.AddRange(taskDto.AssetTypeCalendarTaskLabels);
                    }
                    record.tasks.Remove(taskModel);
                }
            }

            // Any remaining incoming model tasks are new ..
            if (record.tasks != null)
            {
                foreach (var taskModel in record.tasks)
                {
                    var taskDto = new AssetTypeCalendarTaskDTO()
                    {
                        __IsNew = true,
                        HierarchyRoleId = taskModel.hierarchyroleid,
                        TaskTypeId = taskModel.tasktypeid,
                        TaskSiteId = taskModel.tasksiteid,
                        TaskLengthInDays = taskModel.tasklengthindays,
                        TaskLevel = taskModel.tasklevel,
                        TaskName = taskModel.taskname,
                        TaskDescription = taskModel.taskdescription,
                        StartTimeLocal = null
                    };
                    if (taskModel.starttimelocal != null)
                    {
                        taskDto.StartTimeLocal = TimeSpan.Parse(taskModel.starttimelocal);
                    }
                    if (taskModel.labels != null)
                    {
                        taskDto.AssetTypeCalendarTaskLabels = taskModel.labels.Select(id => new AssetTypeCalendarTaskLabelDTO()
                        {
                            __IsNew = true,
                            LabelId = id,
                            AssetTypeCalendarTask = taskDto
                        }).ToList();
                    }
                    dto.AssetTypeCalendarTasks.Add(taskDto);
                }
            }

            var savedDto = dto;
            using (TransactionScope scope = new TransactionScope())
            {
                savedDto = _assetTypeCalendarRepo.Save(dto, true, true);

                foreach (var taskDto in removeDtos)
                {
                    dto.AssetTypeCalendarTasks.Remove(taskDto);
                    bool archived = false;
                    _assetTypeCalenderTaskRepo.Delete(taskDto.Id, true, out archived);
                }

                foreach(var taskLabelDto in removeLabelDtos)
                {
                    _assetTypeCalenderTaskRepo.DeleteLabel(taskLabelDto.AssetTypeCalendarTaskId, taskLabelDto.LabelId);
                }

/*                foreach(var taskDto in dto.AssetTypeCalendarTasks)
                {
                    taskDto.AssetTypeCalendar = savedDto;
                    _assetTypeCalenderTaskRepo.Save(taskDto, true, true);
                }
*/
                scope.Complete();
            }

            dto = _assetTypeCalendarRepo.GetById(savedDto.Id, AssetTypeCalendarLoadInstructions.CalendarTask);

            return ReturnJson(AssetTypeCalendarModel.FromDto(dto));
        }

        [HttpDelete]
        public ActionResult AssetTypeCalendarDelete(int id)
        {
            bool archived = false;

            var dto = _assetTypeCalendarRepo.GetById(id, AssetTypeCalendarLoadInstructions.CalendarTask);
            if (dto != null)
            {
                // Have to delete all Calendar Tasks first
                using (TransactionScope scope = new TransactionScope())
                {
                    foreach (var taskDto in dto.AssetTypeCalendarTasks)
                    {
                        bool taskArchived = false;
                        _assetTypeCalenderTaskRepo.Delete(taskDto.Id, true, out taskArchived);
                    }
                    _assetTypeCalendarRepo.Delete(id, true, out archived);

                    scope.Complete();
                }
            }
            if (!archived)
                return ReturnJson(true);
            else
                return ReturnJson(new { archived = true });
        }
    }
}