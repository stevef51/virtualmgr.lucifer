﻿using ConceptCave.BusinessLogic.Context;
using ConceptCave.BusinessLogic.Invoices;
using ConceptCave.BusinessLogic.Models;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Repository;
using ConceptCave.Repository.ImportMapping;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Importing;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.www.Areas.Admin.Models;
using ConceptCave.www.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using VirtualMgr.Central;

namespace ConceptCave.www.Areas.Admin.Controllers
{
    [Authorize(Roles = RoleRepository.COREROLE_FACILITYSTRUCTUREADMIN)]
    public class FacilityStructureAdminController : ControllerBaseEx
    {
        private readonly IFacilityStructureRepository _facilityStructureRepo;
        private readonly IAssetTrackerRepository _assetTrackerRepo;
        private readonly IGpsLocationRepository _gpsLocationRepo;
        private readonly IProductRepository _productRepo;
        private readonly IProductCatalogRepository _catalogRepo;
        private readonly IProductCatalogItemRepository _itemRepo;
        private readonly IMediaRepository _mediaRepo;
        private readonly Func<ProductCatalogExportParser> _createFacilityStructureExportParser;
        private readonly Func<ProductCatalogImportWriter> _createFacilityStructureImportWriter;
        private readonly IHierarchyRepository _hierarchyRepo;
        private readonly IGlobalSettingsRepository _globalRepo;
        private readonly IContextManagerFactory _ctxFac;

        public FacilityStructureAdminController(IFacilityStructureRepository facilityStructureRepo, 
            IAssetTrackerRepository assetTrackerRepo, 
            IGpsLocationRepository gpsLocationRepo,
            IProductRepository productRepo,
            IProductCatalogRepository catalogRepo,
            IProductCatalogItemRepository itemRepo,
            IMediaRepository mediaRepo,
            Func<ProductCatalogExportParser> createFacilityStructureExportParser,
            Func<ProductCatalogImportWriter> createFacilityStructureImportWriter,
            IHierarchyRepository hierarchyRepo,
            IGlobalSettingsRepository globalRepo,
            IContextManagerFactory ctxFac,
            VirtualMgr.Membership.IMembership membership,
            IMembershipRepository memberRepo) : base(membership, memberRepo)
        {
            _facilityStructureRepo = facilityStructureRepo;
            _assetTrackerRepo = assetTrackerRepo;
            _gpsLocationRepo = gpsLocationRepo;
            _catalogRepo = catalogRepo;
            _itemRepo = itemRepo;
            _mediaRepo = mediaRepo;
            _productRepo = productRepo;
            _createFacilityStructureExportParser = createFacilityStructureExportParser;
            _createFacilityStructureImportWriter = createFacilityStructureImportWriter;
            _hierarchyRepo = hierarchyRepo;
            _globalRepo = globalRepo;
            _ctxFac = ctxFac;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Search(string search, int? pageNumber, int? itemsPerPage, bool includeArchived = false, int? parentId = null, int? searchFlags = null)
        {
            int totalItems = 0;
            var items = _facilityStructureRepo.Search(parentId, "%" + search + "%", pageNumber, itemsPerPage, includeArchived, ref totalItems, searchFlags);

            return ReturnJson(new { count = totalItems, items = from i in items select FacilityStructureModel.FromDto(i, false, false, true) });
        }

        [HttpGet]
        public ActionResult FacilityStructure(int id)
        {
            var dto = _facilityStructureRepo.GetById(id, FacilityStructureLoadInstructions.Children | 
                FacilityStructureLoadInstructions.ParentDeep | 
                FacilityStructureLoadInstructions.UserData | 
                FacilityStructureLoadInstructions.FloorPlan);

            var catalogs = new List<ProductCatalogDTO>();
            var rules = new List<ProductCatalogFacilityStructureRuleDTO>();
            _catalogRepo.GetCatalogsForFacilityStructure(id, catalogs, rules);

            catalogs.ForEach(c => dto.ProductCatalogs.Add(c));

            var model = FacilityStructureModel.FromDto(dto, true, true, true);
            return ReturnJson(model);
        }

        [HttpDelete]
        public ActionResult FacilityStructureDelete(int id)
        {
            bool archived = false;
            _facilityStructureRepo.Delete(id, true, out archived);
            if (!archived)
                return ReturnJson(true);
            else
                return ReturnJson(new { archived = true });
        }

        [HttpPost]
        public ActionResult FacilityStructureSave(FacilityStructureModel record)
        {
            var dto = _facilityStructureRepo.GetById(record.id, FacilityStructureLoadInstructions.FloorPlan);
            if (dto == null)
            {
                dto = new FacilityStructureDTO()
                {
                    __IsNew = true
                };
            }

            dto.Archived = false;           // Saving will always Unarchive a record
            dto.StructureType = (int)record.structuretype;
            dto.Name = record.name;
            dto.Description = record.description;
            dto.SortOrder = record.sortorder;
            dto.Level = record.level;
            dto.FloorHeight = record.floorHeight;
            dto.ParentId = record.parentid;
            dto.SiteId = record.siteid;
            dto.MediaId = record.mediaid;
            dto.HierarchyId = record.hierarchyid;

            if (record.floorplan != null && record.floorplan.imageurl != null)  // Must have an ImageUrl to define a floorplan
            {
                if (dto.FloorPlan == null)
                {
                    dto.FloorPlan = new FloorPlanDTO()
                    {
                        __IsNew = true
                    };
                }
                dto.FloorPlan.ImageUrl = record.floorplan.imageurl;
                dto.FloorPlan.TopLeftLatitude = record.floorplan.topleft.lat;
                dto.FloorPlan.TopLeftLongitude = record.floorplan.topleft.lng;
                dto.FloorPlan.TopRightLatitude = record.floorplan.topright.lat;
                dto.FloorPlan.TopRightLongitude = record.floorplan.topright.lng;
                dto.FloorPlan.BottomLeftLatitude = record.floorplan.bottomleft.lat;
                dto.FloorPlan.BottomLeftLongitude = record.floorplan.bottomleft.lng;
                dto.FloorPlan.IndoorAtlasFloorPlanId = record.floorplan.indooratlasfloorplanid;
            }
            dto = _facilityStructureRepo.Save(dto, true, true);

            return ReturnJson(FacilityStructureModel.FromDto(dto, true, true));
        }

        [HttpGet]
        public ActionResult Hierarchies()
        {
            return ReturnJson(from h in _hierarchyRepo.GetAllHierarchies() select new { id = h.Id, name = h.Name });
        }

        [HttpGet]
        public ActionResult SearchSites(string query)
        {
            var items = _facilityStructureRepo.SearchSites(query, 20);

            return ReturnJson(from i in items
                              select new SearchUserModel()
                                  {
                                      Id = i.UserId,
                                      Name = i.Name,
                                  }
                );
        }

        [HttpGet]
        public ActionResult GetSiteBeacons(int facilityStructureId, bool showUnassignedBeacons)
        {
            IEnumerable<BeaconDTO> result = null;
            var beacons = _assetTrackerRepo.GetFacilityStructureBeacons(facilityStructureId, AssetTrackerBeaconLoadInstructions.StaticLocation);
            if (showUnassignedBeacons)
            {
                var unassigned = _assetTrackerRepo.FindUnassignedBeacons();
                result = beacons.Concat(unassigned);
            }
            else
                result = beacons;
            return ReturnJson(from b in result select BeaconModel.FromDto(b));
        }

        [HttpPost]
        public ActionResult SaveBeacon(BeaconModel beacon)
        {
            BeaconDTO dto = null;
            dto = _assetTrackerRepo.GetBeacon(beacon.id, AssetTrackerBeaconLoadInstructions.StaticLocation | AssetTrackerBeaconLoadInstructions.FacilityStructure);
            if (dto == null)
                return HttpNotFound("Beacon not found");

            // Assign/Unassign to FacilityStructure
            dto.Name = beacon.name;

            GpsLocationDTO deleteLocationDto = null;

            // Update location
            if (beacon.staticlocation != null)
            {
                // If it has StaticLocation then it should also have FacilityStructure and FloorHeight
                dto.FacilityStructureId = beacon.facilitystructureid;
                dto.StaticHeightFromFloor = beacon.staticheightfromfloor;

                // Calculate the Altitude based on the calculated Floor altitude + Beacons height from the floor
                FacilityStructureDTO findDto = dto.FacilityStructure;
                FacilityStructureDTO beaconFloorDto = null;
                while(findDto != null && findDto.ParentId.HasValue && (FacilityStructureType)findDto.StructureType != FacilityStructureType.Building)
                {
                    // If this is a Floor then load the parent building and all its floors
                    if ((FacilityStructureType)findDto.StructureType == FacilityStructureType.Floor)
                    {
                        beaconFloorDto = findDto;
                        findDto = _facilityStructureRepo.GetById(findDto.ParentId.Value, FacilityStructureLoadInstructions.Children);
                    }
                    else // else must be a Site or Zone, just get the Parent and continue searching up
                    {
                        findDto = _facilityStructureRepo.GetById(findDto.ParentId.Value);
                    }
                }

                if (findDto != null && beaconFloorDto != null && beaconFloorDto.Level.HasValue)
                {
                    // Ok, we have all the Floors of the building we can calculate the Beacons altitude with respect to lowest floor
                    var altitude = 0M;
                    foreach (var floorDto in from f in findDto.Children where f.FloorHeight.HasValue && f.Level.HasValue && f.Level < beaconFloorDto.Level orderby f.Level ascending select f) 
                    {
                        if (floorDto.Level < 0)
                            altitude -= floorDto.FloorHeight.Value;
                        else
                            altitude += floorDto.FloorHeight.Value;
                    }
                    beacon.staticlocation.pos.alt = altitude + beacon.staticheightfromfloor;                    
                }
                // Note, we will create a new GpsLocation record if the Beacon has been moved, this is done so that the GpsLocation Id can be used
                // to know when a beacon has been moved - it will have a new StaticLocationId
                var locationDto = dto.StaticLocation;
                if (locationDto == null || locationDto.Latitude != beacon.staticlocation.pos.lat || locationDto.Longitude != beacon.staticlocation.pos.lng || locationDto.Altitude != beacon.staticlocation.pos.alt)
                {
                    deleteLocationDto = locationDto;
                    locationDto = new GpsLocationDTO()
                    {
                        __IsNew = true
                    };
                }
                locationDto.TimestampUtc = DateTime.UtcNow;
                locationDto.Latitude = beacon.staticlocation.pos.lat;
                locationDto.Longitude = beacon.staticlocation.pos.lng;
                locationDto.Altitude = beacon.staticlocation.pos.alt;
                locationDto.AccuracyMetres = beacon.staticlocation.accuracy;
                locationDto.AltitudeAccuracyMetres = beacon.staticlocation.altitudeaccuracy;
                locationDto.LocationType = (int)GpsLocationType.StaticBeacon;
                locationDto.FacilityStructureFloorId = beaconFloorDto?.Id;
                locationDto.Floor = beaconFloorDto?.Level;
                dto.StaticLocation = locationDto;
            }
            else
            {
                dto.StaticLocation = null;
                dto.StaticLocationId = null;
                dto.FacilityStructure = null;
                dto.FacilityStructureId = null;
                dto.StaticHeightFromFloor = null;
            }
            dto = _assetTrackerRepo.Save(dto, true, true);

            // Delete the old location if required
            if (deleteLocationDto != null)
            {
                bool archived;
                _gpsLocationRepo.Delete(deleteLocationDto.Id, true, out archived);
            }


            return ReturnJson(BeaconModel.FromDto(dto));
        }

        [HttpGet]
        public ActionResult Products()
        {
            int count = 0;
            var result = _productRepo.Search("%%", false, null, null, false, ref count);

            List<ProductModel> r = new List<ProductModel>();

            foreach(var p in result)
            {
                r.Add(ProductModel.FromDto(p));
            }

            return ReturnJson(r);
        }

        [HttpPost]
        public ActionResult SaveCatalogInheritenceRule(FacilityStructureProductCatalogModel catalog, int facilityStructureId)
        {
            ProductCatalogFacilityStructureRuleDTO rule = null;

            if (catalog.Inherited == true)
            {
                // only inherited catalogs can be overridden
                rule = _catalogRepo.GetCatalogStructureRule(facilityStructureId, catalog.Id);

                // ok, a few options here based on what the rule has been set to and what we have in the db
                switch (catalog.InheritedRule)
                {
                    case "Inherited":
                        if (rule != null)
                        {
                            // can't inherit if there is a rule, so delete what's there
                            _catalogRepo.Delete(facilityStructureId, catalog.Id);
                        }
                        break;
                    case "Excluded":
                        if (rule == null)
                        {
                            // can't exclude if there isn't anything at this level
                            rule = new ProductCatalogFacilityStructureRuleDTO()
                            {
                                __IsNew = true,
                                FacilityStructureId = facilityStructureId,
                                ProductCatalogId = catalog.Id
                            };
                        }
                        rule.Exclude = true;
                        _catalogRepo.Save(rule, false);
                        break;
                    case "Included":
                        if (rule == null)
                        {
                            // can't exclude if there isn't anything at this level
                            rule = new ProductCatalogFacilityStructureRuleDTO()
                            {
                                __IsNew = true,
                                FacilityStructureId = facilityStructureId,
                                ProductCatalogId = catalog.Id
                            };
                        }
                        rule.Exclude = false;
                        _catalogRepo.Save(rule, false);
                        break;
                }
            }

            return ReturnJson(true);
        }

        [HttpPost]
        public ActionResult SaveCatalog(FacilityStructureProductCatalogModel catalog)
        {
            if(catalog.Inherited)
            {
                throw new InvalidOperationException("An inherited catalog cannot be saved");
            }

            ProductCatalogDTO c;
            if(catalog.__IsNew == false)
            {
                c = _catalogRepo.GetById(catalog.Id);
            }
            else
            {
                c = new ProductCatalogDTO() { __IsNew = true, FacilityStructureId = catalog.FacilityStructureId };
            }

            c.Name = catalog.Name;
            c.Description = catalog.Description;
            c.StockCountWhenOrdering = catalog.StockCountWhenOrdering;
            c.ShowPricingWhenOrdering = catalog.ShowPricingWhenOrdering;
            c.ShowLineItemNotesWhenOrdering = catalog.ShowLineItemNotesWhenOrdering;

            c = _catalogRepo.Save(c, true);

            return ReturnJson(FacilityStructureProductCatalogModel.FromDto(c, false));
        }

        [HttpPost]
        public ActionResult SaveCatalogItem(FacilityStructureProductCatalogItemModel item)
        {
            ProductCatalogItemDTO i;
            if(item.__IsNew == false)
            {
                i = _itemRepo.GetById(item.ProductId, item.ProductCatalogId, ProductCatalogItemLoadInstructions.Product);
            }
            else
            {
                i = new ProductCatalogItemDTO() { __IsNew = true, ProductId = item.ProductId, ProductCatalogId = item.ProductCatalogId };
            }

            i.MinStockLevel = item.MinStockLevel;
            i.Price = item.Price;

            i = _itemRepo.Save(i, true);

            i = _itemRepo.GetById(i.ProductId, i.ProductCatalogId, ProductCatalogItemLoadInstructions.Product);

            return ReturnJson(FacilityStructureProductCatalogItemModel.FromDto(i));
        }

        [HttpDelete]
        public ActionResult DeleteCatalogItem(int productId, int catalogId)
        {
            _itemRepo.Remove(productId, catalogId);

            return ReturnJson(true);
        }

        [HttpDelete]
        public ActionResult DeleteCatalog(int catalogId)
        {
            bool archived = false;
            _itemRepo.DeleteFromCatalog(catalogId);
            _catalogRepo.Delete(catalogId, true, out archived);

            return ReturnJson(true);
        }

        [HttpPost]
        public ActionResult ImportProductCatalogs(HttpPostedFileBase file)
        {
            ProductCatalogImportWriter writer = _createFacilityStructureImportWriter();

            byte[] data = new byte[file.ContentLength];
            file.InputStream.Read(data, 0, file.ContentLength);

            ImportParserResult result = null;

            Action<int> progressCallback = counter =>
            {
                // not doing anything at the moment
            };

            using (var stream = new MemoryStream(data))
            {
                ExcelImportParser parser = new ExcelImportParser();
                var transform = ProductCatalogImportExportTransformFactory.Create();

                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(0, 10, 0)))
                {
                    result = parser.Import(stream, transform, writer, false, progressCallback);

                    try
                    {
                        if (result.Success)
                        {
                            scope.Complete();
                        }
                    }
                    catch (Exception e)
                    {
                        result.Errors.Add(string.Format("A problem was encountered committing the data to the database, the error message given was {0}", e.Message));
                    }
                }
            }

            return ReturnJson(result);
        }

        [HttpGet]
        public FileResult ExportProductCatalogs()
        {
            var parser = _createFacilityStructureExportParser();
            parser.IncludeTitles = true;
            var writer = new ExcelWriter();
            var transforms = ProductCatalogImportExportTransformFactory.Create();

            int total = -1;
            var items = _facilityStructureRepo.Search(null, "%", null, null, false, ref total, null);

            Action<int> progressCallback = counter =>
            {
                // not doing anything at the moment
            };

            // I know calling Import seems backwards, think of it as importing the data into Excel
            parser.Import(items, transforms, writer, false, progressCallback);

            Response.AddHeader("Content-Disposition", "inline; filename=ProductCatalogExport.xlsx");

            byte[] data;
            using (MemoryStream stream = new MemoryStream())
            {
                writer.Book.SaveAs(stream);

                data = stream.ToArray();
            }

            return this.File(data, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        }

        [HttpPost]
        public ActionResult MediaUpload(HttpPostedFileBase file, string name, string description, int id)
        {
            MediaDTO media = null;

            var fac = _facilityStructureRepo.GetById(id);

            try
            {
                byte[] data = new byte[file.ContentLength];
                file.InputStream.Read(data, 0, file.ContentLength);

                if (fac.MediaId.HasValue == false)
                {
                    using (TransactionScope scope = new TransactionScope())
                    {
                        media = _mediaRepo.CreateMediaFromPostedFile(data, name, description, file.FileName, file.ContentType, false, (Guid)this.CurrentUserId);
                        fac.MediaId = media.Id;
                        _facilityStructureRepo.Save(fac, true, false);

                        scope.Complete();
                    }
                }
                else
                {
                    media = _mediaRepo.UpdateMediaFromPostedFile(data, fac.MediaId.Value, string.IsNullOrEmpty(name) == false ? name : string.Format("Image for {0} task type", fac.Name), string.IsNullOrEmpty(description) == false ? description : string.Format("Image for {0} task type", fac.Name), file.FileName, file.ContentType, false, (Guid)this.CurrentUserId);
                }
            }
            catch (Exception e)
            {
                ImportParserResult r = new ImportParserResult();
                r.Errors.Add(e.Message);

                List<ImportParserResult> rs = new List<ImportParserResult>();
                rs.Add(r);

                return ReturnJson(rs);
            }

            ImportParserResult result = new ImportParserResult() { Count = 1, Data = fac.MediaId };
            List<ImportParserResult> results = new List<ImportParserResult>();
            results.Add(result);

            return ReturnJson(results);
        }

        [HttpGet]
        public ActionResult Standards()
        {
            var jsonString = _globalRepo.GetSetting<string>("HACCP");

            return Content(jsonString);
        }

        [HttpGet]
        public ActionResult StandardTests(Guid id)
        {
            var haccpContext = _globalRepo.GetSetting<string>("HACCP.Context");
            if(string.IsNullOrEmpty(haccpContext) == true)
            {
                return ReturnJson(false);
            }

            // we expect this to be checklist.question, so for example configuration.haccp
            var parts = haccpContext.Split('.');
            var checklist = parts[0];
            var question = parts[1];

            string jsonString = null;

            var contextManager = _ctxFac.GetInstance(id, ContextType.Membership, id);
            // loop through the various types of context we have against the user type
            foreach (var typeContextName in contextManager.ContextNames())
            {
                if(typeContextName != checklist)
                {
                    continue;
                }

                var contextGetter = contextManager.LoadContextDocument(typeContextName);
                var wd = contextGetter.GetWorkingDocument(null);

                var node = (from p in wd.PresentedAsDepthFirstEnumerable() where p is IPresentedQuestion && ((IPresentedQuestion)p).Question.Name == question select p);

                if(node.Count() == 0)
                {
                    continue;
                }

                jsonString = ((IPresentedQuestion)node.First()).GetAnswer().AnswerAsString;
            }

            return Content(jsonString);
        }

        [HttpPost]
        public ActionResult StandardTests(List<FacilityStructureStandardsTest> items, Guid id)
        {
            JObject result = new JObject();
            JArray jItems = new JArray();
            result["items"] = jItems;

            foreach(var test in items)
            {
                JObject c = new JObject();
                c["type"] = test.Type;
                c["compliance"] = test.Compliance;
                c["name"] = new JArray();

                foreach(var n in test.Name)
                {
                    ((JArray)c["name"]).Add(n);
                }

                jItems.Add(c);
            }

            var jsonString = result.ToString();

            var haccpContext = _globalRepo.GetSetting<string>("HACCP.Context");
            if (string.IsNullOrEmpty(haccpContext) == true)
            {
                return ReturnJson(false);
            }

            // we expect this to be checklist.question, so for example configuration.haccp
            var parts = haccpContext.Split('.');
            var checklist = parts[0];
            var question = parts[1];

            var contextManager = _ctxFac.GetInstance(id, ContextType.Membership, id);
            // loop through the various types of context we have against the user type
            foreach (var typeContextName in contextManager.ContextNames())
            {
                if (typeContextName != checklist)
                {
                    continue;
                }

                var contextGetter = contextManager.LoadContextDocument(typeContextName);
                var wd = contextGetter.GetWorkingDocument(null);

                var node = (from p in wd.PresentedAsDepthFirstEnumerable() where p is IPresentedQuestion && ((IPresentedQuestion)p).Question.Name == question select p);

                if (node.Count() == 0)
                {
                    continue;
                }

                ((IFreeTextAnswer)((IPresentedQuestion)node.First()).GetAnswer()).SetValue(jsonString);

                contextGetter.Save();

                break;
            }

            return ReturnJson(true);
         }
    }
}