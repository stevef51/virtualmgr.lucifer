﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.Repository;

namespace ConceptCave.www.Areas.Admin.Controllers
{
    public class UtilsController : ControllerBase
    {
        //
        // GET: /Admin/Utils/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult DeleteDemoData()
        {
            return View();
        }

        public ActionResult ConfirmDeleteDemoData()
        {
            WorkingDocumentRepository.DeleteDemoData();

            ViewBag.Success = true;

            return View("DeleteDemoData");
        }
    }
}
