﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.Checklist;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Repository;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using System.Transactions;
using ConceptCave.RepositoryInterfaces.Workloading;
using ConceptCave.BusinessLogic.Workloading;
using ConceptCave.Repository.ImportMapping;
using ConceptCave.RepositoryInterfaces.Importing;
using System.IO;
using VirtualMgr.Central;
using System.Threading.Tasks;

namespace ConceptCave.www.Areas.Admin.Controllers
{
    [Authorize(Roles = RoleRepository.COREROLE_SCHEDULING)]
    public class ScheduleManagementController : ControllerBase
    {
        public class ScheduledTaskModel
        {
            public ProjectJobScheduledTaskDTO Task { get; set; }
            public ScheduledTaskWorkloadingStandardModel[] Workloading {get;set;}
        }

        public class ScheduledTaskWorkloadingStandardError
        {
            public bool Error { get { return true; } }
            public string Reason { get; set; }
        }

        public class ScheduledTaskWorkloadingStandardModel
        {
            public Guid Id { get; set; }
            public Guid SiteFeatureId { get; set; }
            public string Book { get; set; }
            public string Area { get; set; }
            public string Feature { get; set; }
            public string SiteFeatureName { get; set; }
            public string Activity { get; set; }

            public bool Selected { get; set; }

            public string Text { get; set; }

            public int CalculatedDuration { get; set; }

            public int? EstimatedDuration { get; set; }

            public int Frequency { get; set; }

            public bool OverrideDays { get; set; }
            public bool? ExecuteSunday { get; set; }
            public bool? ExecuteMonday { get; set; }
            public bool? ExecuteTuesday { get; set; }
            public bool? ExecuteWednesday { get; set; }
            public bool? ExecuteThursday { get; set; }
            public bool? ExecuteFriday { get; set; }
            public bool? ExecuteSaturday { get; set; }
        }

        private IRosterRepository _rosterRepo;
        private IHierarchyRepository _hierarchyRepo;
        private IScheduledTaskRepository _scheduleRepo;
        private ITaskTypeRepository _typeRepo;
        private IWorkLoadingBookRepository _bookRepo;
        private IWorkloadingDurationCalculator _workloadingCalculator;
        private IWorkloadingActivityFactory _activityFactory;
        private IGlobalSettingsRepository _settingsRepo;
        private IMembershipRepository _memberRepo;
        protected IWorkLoadingFeatureRepository _featureRepo;
        protected ISiteFeatureRepository _siteFeatureRepo;
        protected IWorkLoadingActivityRepository _activityRepo;
        protected ILabelRepository _labelRepo;

        public ScheduleManagementController(IRosterRepository rosterRepo, 
            IHierarchyRepository hierarchyRepo, 
            IScheduledTaskRepository scheduleRepo,
            ITaskTypeRepository typeRepo,
            IWorkLoadingBookRepository bookRepo,
            IWorkloadingDurationCalculator workloadingCalculator,
            IWorkloadingActivityFactory activityFactory,
            IGlobalSettingsRepository settingsRepo,
            IMembershipRepository memberRepo,
            IWorkLoadingFeatureRepository featureRepo,
            ISiteFeatureRepository siteFeatureRepo,
            IWorkLoadingActivityRepository activityRepo,
            ILabelRepository labelRepo)
        {
            _rosterRepo = rosterRepo;
            _hierarchyRepo = hierarchyRepo;
            _scheduleRepo = scheduleRepo;
            _typeRepo = typeRepo;
            _bookRepo = bookRepo;
            _workloadingCalculator = workloadingCalculator;
            _activityFactory = activityFactory;
            _settingsRepo = settingsRepo;
            _memberRepo = memberRepo;
            _featureRepo = featureRepo;
            _siteFeatureRepo = siteFeatureRepo;
            _activityRepo = activityRepo;
            _labelRepo = labelRepo;
        }

        [HttpGet]
        public ActionResult WorkloadingModule()
        {
            var hasModule = _settingsRepo.GetSetting<bool>(GlobalSettingsConstants.Module_Workloading);

            return ReturnJson(hasModule);
        }

        [HttpGet]
        public ActionResult Rosters()
        {
            var result = _rosterRepo.GetAll();

            return ReturnJson(result);
        }

        [HttpGet]
        public ActionResult Types()
        {
            var result = _typeRepo.GetAll(ProjectJobTaskTypeLoadInstructions.None);

            return ReturnJson(TaskTypeManagementController.ConvertToModel(result));
        }

        [HttpGet]
        public ActionResult Roles(int rosterId)
        {
            var roster = _rosterRepo.GetById(rosterId);

            if(roster == null)
            {
                throw new ArgumentException(string.Format("{0} is not a valid roster", rosterId));
            }

            var buckets = _hierarchyRepo.GetBucketsForHierarchy(roster.HierarchyId, RepositoryInterfaces.Enums.HierarchyBucketLoadInstructions.BucketRole | HierarchyBucketLoadInstructions.User);

            var roles = (from b in buckets group b by b.RoleId into g select new { id = g.Key, Name = g.First().HierarchyRole.Name, Users = (from a in g where a.UserId.HasValue == true select a.UserData.Name).ToArray() });

            return ReturnJson(roles);
        }

        [HttpGet]
        public ActionResult Schedules(int rosterId, int roleId)
        {
            var roster = _rosterRepo.GetById(rosterId);

            if (roster == null)
            {
                throw new ArgumentException(string.Format("{0} is not a valid roster", rosterId));
            }

            var buckets = _hierarchyRepo.GetBucketsForHierarchy(roster.HierarchyId, roleId, RepositoryInterfaces.Enums.HierarchyBucketLoadInstructions.BucketRole | RepositoryInterfaces.Enums.HierarchyBucketLoadInstructions.User);

            // a list of users that this role applies to in this roster
            var users = from b in buckets select b.UserData == null ? string.Format("{0} unassigned", b.HierarchyRole.Name) : b.UserData.Name;

            // now the set of scheduled tasks for this role in this roster
            var tasks = _scheduleRepo.GetForRoster(rosterId, roleId, RepositoryInterfaces.Enums.ProjectJobScheduledTaskLoadInstructions.CalendarRule | 
                ProjectJobScheduledTaskLoadInstructions.ScheduleLabels | 
                ProjectJobScheduledTaskLoadInstructions.AuditSampling |
                ProjectJobScheduledTaskLoadInstructions.AllTranslated);

            dynamic result = new { Users = users, Tasks = tasks };

            return ReturnJson(result);
        }

        [HttpGet]
        public ActionResult Workloading(Guid scheduledTaskId)
        {
            var task = _scheduleRepo.GetById(scheduledTaskId, ProjectJobScheduledTaskLoadInstructions.Users |
                ProjectJobScheduledTaskLoadInstructions.UsersWorkloadingFeatures |
                ProjectJobScheduledTaskLoadInstructions.WorkloadingActivities |
                ProjectJobScheduledTaskLoadInstructions.Users |
                ProjectJobScheduledTaskLoadInstructions.UsersWorkloadingFeatures |
                ProjectJobScheduledTaskLoadInstructions.UsersWorkloadingFeaturesAreas |
                ProjectJobScheduledTaskLoadInstructions.UsersWorkloadingFeaturesUnits);

            if(task.SiteId.HasValue == false)
            {
                return ReturnJson(new ScheduledTaskWorkloadingStandardError() { Reason = "Site must be supplied" });
            }

            var standards = _bookRepo.GetStandardsForFeatures((from f in task.UserData_.SiteFeatures select f.WorkLoadingFeatureId).ToArray(), WorkLoadingStandardLoadInstructions.Books |
                WorkLoadingStandardLoadInstructions.Features |
                WorkLoadingStandardLoadInstructions.Activities |
                WorkLoadingStandardLoadInstructions.Units);

            List<ScheduledTaskWorkloadingStandardModel> result = new List<ScheduledTaskWorkloadingStandardModel>();

            foreach (var s in standards)
            {
                var wlActivities = _activityFactory.NewForSite(s, task.UserData_);
                _workloadingCalculator.EstimateDuration(wlActivities);

                foreach(var workloadingActivity in wlActivities)
                {
                    var siteFeature = ((SiteFeatureDTO)workloadingActivity.FeatureMeasurement.Data);
                    var activites = (from a in task.ProjectJobScheduledTaskWorkloadingActivities where a.WorkloadingStandardId == s.Id && 
                                         a.SiteFeatureId == siteFeature.Id select a);

                    ProjectJobScheduledTaskWorkloadingActivityDTO activity = null;

                    if (activites.Count() > 0)
                    {
                        activity = activites.First();
                    }

                    ScheduledTaskWorkloadingStandardModel model = new ScheduledTaskWorkloadingStandardModel()
                    {
                        Id = s.Id,
                        SiteFeatureId = siteFeature.Id,
                        Book = s.WorkLoadingBook.Name,
                        Area = siteFeature.SiteArea.Name,
                        Feature = s.WorkLoadingFeature.Name,
                        SiteFeatureName = siteFeature.Name,
                        Activity = s.WorkLoadingActivity.Name,
                        Selected = activity == null ? false : true,
                        Text = activity == null ? s.WorkLoadingActivity.Name : activity.Text,
                        CalculatedDuration = (int)workloadingActivity.EstimatedDuration,
                        EstimatedDuration = activity == null ? null : (int?)activity.EstimatedDuration,
                        Frequency = activity == null ? 1 : activity.Frequency,
                        ExecuteSunday = activity == null ? null : activity.ExecuteSunday,
                        ExecuteMonday = activity == null ? null : activity.ExecuteMonday,
                        ExecuteTuesday = activity == null ? null : activity.ExecuteTuesday,
                        ExecuteWednesday = activity == null ? null : activity.ExecuteWednesday,
                        ExecuteThursday = activity == null ? null : activity.ExecuteThursday,
                        ExecuteFriday = activity == null ? null : activity.ExecuteFriday,
                        ExecuteSaturday = activity == null ? null : activity.ExecuteSaturday,
                        OverrideDays = activity == null ? false : activity.ExecuteSunday.HasValue ||
                            activity.ExecuteMonday.HasValue ||
                            activity.ExecuteTuesday.HasValue ||
                            activity.ExecuteWednesday.HasValue ||
                            activity.ExecuteThursday.HasValue ||
                            activity.ExecuteFriday.HasValue ||
                            activity.ExecuteSaturday.HasValue
                    };

                    result.Add(model);
                }
            }

            return ReturnJson(result.ToArray());
        }

        [HttpPost]
        public ActionResult Task(ScheduledTaskModel model)
        {
            var task = model.Task;

            if (task.__IsNew == true)
            {
                task.Id = CombFactory.NewComb();
                task.CalendarRule.DateCreated = DateTime.Now.ToUniversalTime();
            }

            var timezone = MultiTenantManager.CurrentTenant.TimeZoneInfo;
            // ok the startdate and enddate on the schedule have been specified by the user in their timezone (when they select
            // a monday they mean monday their time). The JS sends the dates down without times and coded as UTC. So we need to
            // interpret them as local times and then convert to UTC. We are going to use the default timezone for the server
            // as otherwise the times become dependant upon which user did things.
            var localTime = DateTime.SpecifyKind(task.CalendarRule.StartDate.ToUniversalTime(), DateTimeKind.Unspecified);

            task.CalendarRule.StartDate = TimeZoneInfo.ConvertTimeToUtc(localTime, timezone);
            if(task.CalendarRule.EndDate.HasValue == true)
            {
                localTime = DateTime.SpecifyKind(task.CalendarRule.EndDate.Value.ToUniversalTime(), DateTimeKind.Unspecified);
                task.CalendarRule.EndDate = TimeZoneInfo.ConvertTimeToUtc(localTime, timezone);
            }

            foreach (var translation in task.Translated)
            {
                if (translation.__IsNew)
                    translation.Id = task.Id;
                translation.Name = translation.Name ?? "";
            }

            ProjectJobScheduledTaskDTO t = null;

            task.ProjectJobScheduledTaskWorkloadingActivities.Clear();

            if(model.Workloading != null)
            {
                // the task that we have been sent doesn't have the workloading on it
                // so we need to add this as we've tried to make things simpler for the UI
                // by wrapping up the workloading in a different model
                foreach(var loading in model.Workloading)
                {
                    if(loading.Selected == false)
                    {
                        continue;
                    }

                    task.ProjectJobScheduledTaskWorkloadingActivities.Add(new ProjectJobScheduledTaskWorkloadingActivityDTO() 
                    { 
                        __IsNew = true,
                        ProjectJobScheduledTaskId = task.Id,
                        SiteFeatureId = loading.SiteFeatureId,
                        WorkloadingStandardId = loading.Id,
                        Text = loading.Text,
                        Frequency = loading.Frequency,
                        ExecuteSunday = loading.OverrideDays == false ? null : loading.ExecuteSunday.HasValue == false ? false : loading.ExecuteSunday,
                        ExecuteMonday = loading.OverrideDays == false ? null : loading.ExecuteMonday.HasValue == false ? false : loading.ExecuteMonday,
                        ExecuteTuesday = loading.OverrideDays == false ? null : loading.ExecuteTuesday.HasValue == false ? false : loading.ExecuteTuesday,
                        ExecuteWednesday = loading.OverrideDays == false ? null : loading.ExecuteWednesday.HasValue == false ? false : loading.ExecuteWednesday,
                        ExecuteThursday = loading.OverrideDays == false ? null : loading.ExecuteThursday.HasValue == false ? false : loading.ExecuteThursday,
                        ExecuteFriday = loading.OverrideDays == false ? null : loading.ExecuteFriday.HasValue == false ? false : loading.ExecuteFriday,
                        ExecuteSaturday = loading.OverrideDays == false ? null : loading.ExecuteSaturday.HasValue == false ? false : loading.ExecuteSaturday,
                        EstimatedDuration = loading.EstimatedDuration
                    });
                }
            }
            
            using(TransactionScope scope = new TransactionScope())
            {
                _scheduleRepo.RemoveLabels(task.Id);
                _scheduleRepo.RemoveWorkloadingActivities(task.Id);
                t = _scheduleRepo.Save(task, true, true);

                scope.Complete();
            }

            return ReturnJson(t);
        }

        [HttpDelete]
        public ActionResult Task(Guid id)
        {
            _scheduleRepo.Delete(id);

            return ReturnJson(true);
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Move(Guid[] id, int toRoster, int toRole)
        {
            var tasks = _scheduleRepo.GetById(id, ProjectJobScheduledTaskLoadInstructions.None);

            using(TransactionScope scope = new TransactionScope())
            {
                foreach (var task in tasks)
                {
                    task.RosterId = toRoster;
                    task.RoleId = toRole;

                    _scheduleRepo.Save(task, false, false);
                }

                scope.Complete();
            }

            return ReturnJson(true);
        }

        [HttpPost]
        public ActionResult Copy(Guid[] id, int toRoster, int toRole)
        {
            var tasks = _scheduleRepo.GetById(id, RepositoryInterfaces.Enums.ProjectJobScheduledTaskLoadInstructions.CalendarRule |
                ProjectJobScheduledTaskLoadInstructions.ScheduleLabels |
                ProjectJobScheduledTaskLoadInstructions.AllTranslated |
                ProjectJobScheduledTaskLoadInstructions.WorkloadingActivities);

            using(TransactionScope scope = new TransactionScope())
            {
                foreach(var task in tasks)
                {
                    task.RosterId = toRoster;
                    task.RoleId = toRole;

                    var newId = CombFactory.NewComb();
                    task.Id = newId;
                    task.__IsNew = true;

                    task.CalendarRuleId = 0;
                    task.CalendarRule.Id = 0;
                    task.CalendarRule.__IsNew = true;

                    foreach (var l in task.ProjectJobScheduledTaskLabels)
                    {
                        l.ProjectJobScheduledTaskId = newId;
                        l.__IsNew = true;
                    }

                    foreach (var t in task.Translated)
                    {
                        t.Id = newId;
                        t.__IsNew = true;
                    }

                    foreach (var l in task.ProjectJobScheduledTaskWorkloadingActivities)
                    {
                        l.ProjectJobScheduledTaskId = newId;
                        l.__IsNew = true;
                    }

                    _scheduleRepo.Save(task, false, true);
                }

                scope.Complete();
            }

            return ReturnJson(true);
        }

        [HttpPost]
        public ActionResult ChangeProperties(Guid[] id, string siteId, int? level, CalendarRuleDTO calendarrule, string lengthindays, TimeSpan? starttime, int? starttimewindowseconds, int? estimateddurationseconds)
        {
            var tasks = _scheduleRepo.GetById(id, RepositoryInterfaces.Enums.ProjectJobScheduledTaskLoadInstructions.CalendarRule);

            var timezone = MultiTenantManager.CurrentTenant.TimeZoneInfo;

            using (TransactionScope scope = new TransactionScope())
            {
                foreach (var task in tasks)
                {
                    if(string.IsNullOrEmpty(siteId) == false)
                    {
                        task.SiteId = Guid.Parse(siteId);
                    }

                    if(level.HasValue)
                    {
                        task.Level = level.Value;
                    }

                    if(starttime.HasValue == true)
                    {
                        task.StartTime = starttime.Value;

                        if (starttimewindowseconds.HasValue == true)
                        {
                            task.StartTimeWindowSeconds = starttimewindowseconds.Value;
                        }
                    }

                    if(lengthindays != null)
                    {
                        // ok the startdate and enddate on the schedule have been specified by the user in their timezone (when they select
                        // a monday they mean monday their time). The JS sends the dates down without times and coded as UTC. So we need to
                        // interpret them as local times and then convert to UTC. We are going to use the default timezone for the server
                        // as otherwise the times become dependant upon which user did things.
                        var localTime = DateTime.SpecifyKind(calendarrule.StartDate.ToUniversalTime(), DateTimeKind.Unspecified);

                        task.CalendarRule.StartDate = TimeZoneInfo.ConvertTimeToUtc(localTime, timezone);
                        if (task.CalendarRule.EndDate.HasValue == true)
                        {
                            localTime = DateTime.SpecifyKind(calendarrule.EndDate.Value.ToUniversalTime(), DateTimeKind.Unspecified);
                            task.CalendarRule.EndDate = TimeZoneInfo.ConvertTimeToUtc(localTime, timezone);
                        }
                        task.CalendarRule.ExecuteSunday = calendarrule.ExecuteSunday;
                        task.CalendarRule.ExecuteMonday = calendarrule.ExecuteMonday;
                        task.CalendarRule.ExecuteTuesday = calendarrule.ExecuteTuesday;
                        task.CalendarRule.ExecuteWednesday = calendarrule.ExecuteWednesday;
                        task.CalendarRule.ExecuteThursday = calendarrule.ExecuteThursday;
                        task.CalendarRule.ExecuteFriday = calendarrule.ExecuteFriday;
                        task.CalendarRule.ExecuteSaturday = calendarrule.ExecuteSaturday;

                        if(task.CalendarRule.ExecuteSunday == false &&
                            task.CalendarRule.ExecuteMonday == false &&
                            task.CalendarRule.ExecuteTuesday == false &&
                            task.CalendarRule.ExecuteWednesday == false &&
                            task.CalendarRule.ExecuteThursday == false &&
                            task.CalendarRule.ExecuteFriday == false &&
                            task.CalendarRule.ExecuteSaturday == false
                            )
                        {
                            // we can't have them not executing on any day, so give them a day
                            task.CalendarRule.ExecuteSunday = true;
                        }

                        task.CalendarRule.Frequency = calendarrule.Frequency;
                        task.LengthInDays = lengthindays;

                        if(estimateddurationseconds.HasValue)
                        {
                            task.EstimatedDurationSeconds = estimateddurationseconds.Value;
                        }
                    }

                    _scheduleRepo.Save(task, false, true);
                }

                scope.Complete();
            }

            return ReturnJson(true);
        }

        protected List<FieldTransform> CreateTransforms()
        {
            return new ScheduleImportExportTransformFactory().Create();
        }

        [HttpGet]
        public FileResult Export(int rosterId, int roleId)
        {
            var parser = new ScheduleExportParser(_typeRepo, _memberRepo, _activityRepo, _featureRepo, _bookRepo, _siteFeatureRepo, _rosterRepo, _hierarchyRepo, _labelRepo, rosterId) { IncludeTitles = true };
            var writer = new ExcelWriter();
            var transforms = CreateTransforms();

            var schedules = _scheduleRepo.GetForRoster(rosterId, roleId, parser.ScheduledTaskLoadInstructions);

            Action<int> progressCallback = counter =>
            {
                // not doing anything at the moment
            };

            // I know calling Import seems backwards, think of it as importing the data into Excel
            parser.Import(schedules, transforms, writer, false, progressCallback);

            Response.AddHeader("Content-Disposition", "inline; filename=ScheduleExport.xlsx");

            byte[] data;
            using(MemoryStream stream = new MemoryStream())
            {
                writer.Book.SaveAs(stream);

                data = stream.ToArray();
            }

            return this.File(data, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        }

        [HttpPost]
        public ActionResult Import(HttpPostedFileBase file)
        {
            ScheduleImportWriter writer = new ScheduleImportWriter(_scheduleRepo,
                _rosterRepo,
                _memberRepo,
                _hierarchyRepo,
                _typeRepo,
                _featureRepo,
                _siteFeatureRepo,
                _activityRepo,
                _bookRepo,
                _labelRepo);

            byte[] data = new byte[file.ContentLength];
            file.InputStream.Read(data, 0, file.ContentLength);

            ImportParserResult result = null;

            Action<int> progressCallback = counter =>
            {
                // not doing anything at the moment
            };

            using (var stream = new MemoryStream(data))
            {
                ExcelImportParser parser = new ExcelImportParser();
                var transform = CreateTransforms();

                using(TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(0, 10, 0)))
                {
                    result = parser.Import(stream, transform, writer, false, progressCallback);

                    try
                    {
                        if (result.Success)
                        {
                            scope.Complete();
                        }
                    }
                    catch(Exception e)
                    {
                        result.Errors.Add(string.Format("A problem was encountered committing the data to the database, the error message given was {0}", e.Message));
                    }
                }
            }

            return ReturnJson(result);
        }

        [HttpGet]
        public ActionResult Times(int rosterId, int roleId, int day)
        {
            var days = _scheduleRepo.GetDaySchedule(rosterId, roleId, day);

            ProjectJobScheduleDayDTO d = null;

            if(days.Count == 0)
            {
                d = new ProjectJobScheduleDayDTO()
                {
                    __IsNew = true,
                    RosterId = rosterId,
                    RoleId = roleId,
                    Day = day
                };
            }
            else
            {
                d = days.First();
            }

            return ReturnJson(d);
        }

        [HttpPost]
        public ActionResult Times(ProjectJobScheduleDayDTO day)
        {
            var dto = _scheduleRepo.Save(day, true, false);

            return ReturnJson(dto);
        }
    }



    //public class ScheduledTaskModelBinder : DefaultModelBinder
    //{
    //    protected override object CreateModel(ControllerContext controllerContext, ModelBindingContext bindingContext, Type modelType)
    //    {
    //        var result = (ConceptCave.www.Areas.Admin.Controllers.ScheduleManagementController.ScheduledTaskModel)base.CreateModel(controllerContext, bindingContext, modelType);

    //        var ts = bindingContext.ValueProvider.GetValue("model.task.starttime").AttemptedValue;

    //        DateTime source = DateTime.Parse(ts);

    //        result.Task.StartTime = source.Subtract(source.Date);

    //        return result;
    //    }
    //}
}
