﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Repository;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.www.Areas.Admin.Models;
using ConceptCave.www.Areas.Designer.Controllers;
using System.Transactions;
using ConceptCave.RepositoryInterfaces.Repositories;
using Newtonsoft.Json;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Checklist;
using ConceptCave.RepositoryInterfaces.Importing;
using System.Net.Http;
using ConceptCave.RepositoryInterfaces.PolicyManagement;
using System.Threading.Tasks;
using System.Net.Http.Headers;
using System.Text;

namespace ConceptCave.www.Areas.Admin.Controllers
{
    [Authorize(Roles = RoleRepository.COREROLE_MEDIA)]
    public class MediaManagementController : ControllerBaseEx
    {
        protected IMediaRepository _mediaRepo;
        protected IMediaPolicyManager _policyManager;
        protected IMediaExporterFactoryAsync _mediaExporterFactory;
        protected IMediaImporterFactoryAsync _mediaImporterFactory;

        public MediaManagementController(
            IMediaRepository mediaRepo, 
            IMediaPolicyManager policyManager, 
            IMediaExporterFactoryAsync mediaExporterFactory,
            IMediaImporterFactoryAsync mediaImporterFactory,
            VirtualMgr.Membership.IMembership membership,
            IMembershipRepository memberRepo) : base(membership, memberRepo)
        {
            _mediaRepo = mediaRepo;
            _policyManager = policyManager;
            _mediaExporterFactory = mediaExporterFactory;
            _mediaImporterFactory = mediaImporterFactory;
        }

        [HttpGet]
        public ActionResult Folders(bool notfiled, bool? includeFiles)
        {
            bool files = true;
            if(includeFiles.HasValue == true)
            {
                files = includeFiles.Value;
            }

            MediaFolderLoadInstruction instructions = MediaFolderLoadInstruction.None;
            if(files == true)
            {
                instructions = MediaFolderLoadInstruction.FolderFile | MediaFolderLoadInstruction.FolderFileMedia;
            }

            var folders = _mediaRepo.GetFolders(notfiled, null, instructions, true);

            List<MediaFolder> result = new List<MediaFolder>();
            foreach (var folder in folders)
            {
                var mediaFolder = new MediaFolder(folder);
                result.Add(mediaFolder);
            }

            return ReturnJson(result);
        }

        [HttpGet]
        public ActionResult Folder(Guid id)
        {
            var folder = _mediaRepo.GetFolderById(id, MediaFolderLoadInstruction.MediaFolderPolicies | MediaFolderLoadInstruction.Roles | MediaFolderLoadInstruction.AllTranslated, true);

            return ReturnJson(folder);
        }

        [HttpPost]
        public ActionResult FolderSave(MediaFolderDTO folder, string[] roles)
        {
            if (roles == null)
            {
                roles = new string[] { };
            }

            if (folder.__IsNew == true)
            {
                folder.Id = CombFactory.NewComb();
            }

            foreach(var translation in folder.Translated)
            {
                if (translation.__IsNew)
                    translation.Id = folder.Id;
                translation.Text = translation.Text ?? "";
            }

            // ok we need to work out what roles have been added and which have been removed.

            var roleIds = (from m in folder.MediaFolderRoles select m.AspNetRole).ToList();
            var deletedRoles = (from r in roleIds where roles.Contains(r.Name) == false select r.Id);
            var newRoles = (from r in roles where (from t in roleIds select t.Name).Contains(r) == false select r);

            newRoles.ToList().ForEach(i =>
            {
                var role = RoleRepository.GetByName(i); // this needs to be added to the injected version

                folder.MediaFolderRoles.Add(new MediaFolderRoleDTO() { __IsNew = true, RoleId = role.Id, MediaFolderId = folder.Id });
            });

            deletedRoles.ForEach(d =>
            {
                folder.MediaFolderRoles.Remove((from l in folder.MediaFolderRoles where l.AspNetRole.Id == d select l).First());
            });

            MediaFolderDTO mediaFolder = null;

            using (TransactionScope scope = new TransactionScope())
            {
                mediaFolder = _mediaRepo.SaveFolder(folder, true, true);

                _mediaRepo.RemoveFolderFromRoles(folder.Id, deletedRoles.ToArray());

                mediaFolder = _mediaRepo.GetFolderById(mediaFolder.Id, MediaFolderLoadInstruction.MediaFolderPolicies | MediaFolderLoadInstruction.Roles, true);

                scope.Complete();
            }


            return ReturnJson(new MediaFolder(mediaFolder));
        }

        [HttpPost]
        public ActionResult FolderAdd(Guid? parentId)
        {
            var folder = new MediaFolderDTO()
            {
                __IsNew = true,
                Id = CombFactory.NewComb(),
                Text = "New Folder"
            };

            if (parentId.HasValue == true && parentId.Value != Guid.Empty)
            {
                folder.ParentId = parentId.Value;
            }

            folder = _mediaRepo.SaveFolder(folder, true, false);

            return ReturnJson(new MediaFolder(folder));
        }

        [HttpDelete]
        public ActionResult FolderDelete(Guid id)
        {
            if (id == Guid.Empty)
            {
                // its the virtual none filed folder
                return ReturnJson(false);
            }

            _mediaRepo.DeleteFolder(id);

            return ReturnJson(true);
        }

        [HttpPost]
        public ActionResult FolderCheckPolicies(Guid id)
        {
            var folder = _mediaRepo.GetFolderById(id, MediaFolderLoadInstruction.FolderFile | MediaFolderLoadInstruction.FolderFileMedia | MediaFolderLoadInstruction.MediaFolderPolicies);

            _policyManager.Run(folder);

            return ReturnJson(true);
        }

        [HttpGet]
        public ActionResult Media(Guid id)
        {
            var media = _mediaRepo.GetById(id, MediaLoadInstruction.MediaLabel | MediaLoadInstruction.Label | MediaLoadInstruction.MediaVersions | MediaLoadInstruction.MediaPolicies | MediaLoadInstruction.Roles | MediaLoadInstruction.AllTranslated);

            if (media == null)
            {
                // ok its a media folder file
                var mediaFile = _mediaRepo.GetFolderFileById(id, MediaFolderFileLoadInstruction.Media | MediaFolderFileLoadInstruction.MediaVersions | MediaFolderFileLoadInstruction.MediaLabels | MediaFolderFileLoadInstruction.MediaPolicies | MediaFolderFileLoadInstruction.MediaRoles | MediaFolderFileLoadInstruction.AllTranslated);

                media = mediaFile.Medium;
                media.MediaFolderFiles.Clear();

                return ReturnJson(new { media = media, mediafile = mediaFile });
            }

            return ReturnJson(new { media = media });
        }

        [HttpDelete]
        public ActionResult MediaDelete(Guid id)
        {
            _mediaRepo.Delete(id);

            return ReturnJson(true);
        }

        [HttpPost]
        public ActionResult MediaSave(MediaDTO media, Guid[] labels, string[] roles, MediaFolderFileDTO mediaFile)
        {
            if (labels == null)
            {
                labels = new Guid[] {};
            }

            if (roles == null)
            {
                roles = new string[] { };
            }

            foreach (var translation in media.Translated)
            {
                if (translation.__IsNew)
                    translation.Id = media.Id;
                translation.Name = translation.Name ?? "";
                translation.Filename = translation.Filename ?? "";
                translation.Description = translation.Description ?? "";
            }

            var current = _mediaRepo.GetById(media.Id, MediaLoadInstruction.MediaLabel | MediaLoadInstruction.Label | MediaLoadInstruction.Roles);

            var labelIds = from m in current.LabelMedias select m.LabelId;
            var deletedLabels = (from m in current.LabelMedias where labels.Contains(m.LabelId) == false select m.LabelId);
            var newLabels = (from m in labels where (from q in current.LabelMedias select q.LabelId).Contains(m) == false select m);

            newLabels.ToList().ForEach(i =>
            {
                media.LabelMedias.Add(new LabelMediaDTO() { __IsNew = true, LabelId = i, MediaId = media.Id });
            });

            deletedLabels.ForEach(d =>
            {
                media.LabelMedias.Remove((from l in media.LabelMedias where l.LabelId == d select l).First());
            });


            // ok we need to work out what roles have been added and which have been removed.

            var roleIds = (from m in current.MediaRoles select m.AspNetRole).ToList();
            var deletedRoles = (from r in roleIds where roles.Contains(r.Name) == false select r.Id);
            var newRoles = (from r in roles where (from t in roleIds select t.Name).Contains(r) == false select r);

            newRoles.ToList().ForEach(i =>
            {
                var role = RoleRepository.GetByName(i); // this needs to be added to the injected version

                media.MediaRoles.Add(new MediaRoleDTO() { __IsNew = true, RoleId = role.Id, MediaId = media.Id });
            });

            deletedRoles.ForEach(d =>
            {
                media.MediaRoles.Remove((from l in media.MediaRoles where l.AspNetRole.Id == d select l).First());
            });

            using (TransactionScope scope = new TransactionScope())
            {
                _mediaRepo.RemoveFromLabels(deletedLabels.ToArray(), media.Id);
                _mediaRepo.RemoveFromRoles(deletedRoles.ToArray(), media.Id);

                if (mediaFile != null && mediaFile.Id != Guid.Empty)
                    _mediaRepo.SaveFolderFile(mediaFile, true, true);

                var result = _mediaRepo.Save(media, true, true);

                scope.Complete();
            }

            return ReturnJson(new MediaFolder());
        }

        public ActionResult MediaUpload(HttpPostedFileBase file, string name, string description, Guid? id, Guid? parentId, bool? useVersioning)
        {
            MediaDTO media = null;

            try
            {
                byte[] data = new byte[file.ContentLength];
                file.InputStream.Read(data, 0, file.ContentLength);
                
                if (id.HasValue == false)
                {
                    using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(0, 10, 0)))
                    {
                        media = _mediaRepo.CreateMediaFromPostedFile(data, name, description, file.FileName, file.ContentType, useVersioning.Value, (Guid)this.CurrentUserId);

                        if (parentId.HasValue == true)
                        {
                            var mediaFile = new MediaFolderFileDTO() { __IsNew = true, Id = CombFactory.NewComb(), MediaFolderId = parentId.Value, MediaId = media.Id };
                            _mediaRepo.SaveFolderFile(mediaFile, false, false);
                        }

                        scope.Complete();
                    }
                }
                else
                {
                    var m = _mediaRepo.GetById(id.Value, MediaLoadInstruction.None);
                    media = _mediaRepo.UpdateMediaFromPostedFile(data, id.Value, string.IsNullOrEmpty(name) == false ? name : m.Name, string.IsNullOrEmpty(description) == false ? description : m.Description, file.FileName, file.ContentType, m.UseVersioning, (Guid)this.CurrentUserId);
                }
            }
            catch (Exception e)
            {
                ImportParserResult r = new ImportParserResult();
                r.Errors.Add(e.Message);

                List<ImportParserResult> rs = new List<ImportParserResult>();
                rs.Add(r);

                return ReturnJson(rs);
            }


            media = _mediaRepo.GetById(media.Id, MediaLoadInstruction.MediaLabel | MediaLoadInstruction.Label);

            ImportParserResult result = new ImportParserResult() { Count = 1 };
            List<ImportParserResult> results = new List<ImportParserResult>();
            results.Add(result);

            return ReturnJson(results);
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task ExportFolder(Guid id)
        {
            using (var exporter = await _mediaExporterFactory.CreateAsync(_mediaRepo, Response.OutputStream))
            {
                var folderName = await exporter.ExportFolderAsync(id);
                Response.ContentType = "application/octetstream";
                Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}.tar.gz", folderName));
            }
        }

        [HttpPost]
        public async Task<ActionResult> ImportFolder(HttpPostedFileBase file, Guid? id, bool? testOnly)
        {
            MediaImportInstructions instructions = new MediaImportInstructions()
            {
                TestOnly = testOnly.HasValue ? testOnly.Value : false,
                UserId = (Guid)CurrentUserId
            };
            using(var importer = await _mediaImporterFactory.CreateAsync(_mediaRepo, file.InputStream, instructions))
            {
                return ReturnJson(await importer.ImportFolderAsync(id));
            }
        }
    }
}
