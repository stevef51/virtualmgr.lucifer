﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.Repository;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.DTO.DTOClasses;
using System.Transactions;
using ConceptCave.Checklist;
using ConceptCave.Repository.ImportMapping;
using ConceptCave.RepositoryInterfaces.Importing;
using System.IO;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

using VirtualMgr.Membership;

namespace ConceptCave.www.Areas.Admin.Controllers
{
    [Authorize(Roles = RoleRepository.COREROLE_STOCK_PRODUCT)]
    public class ProductManagementController : ControllerBaseEx
    {
        public class ProductModel
        {
            public bool __IsNew { get; set; }
            public int Id { get; set; }
            public string Name { get;set;}
            public string Description {get;set;}
            public string Code {get;set;}
            public bool IsCoupon { get; set; }
            [JsonConverter(typeof(JsonUTCDateTimeConverter))]
            public DateTime? ValidFrom { get; set; }
            [JsonConverter(typeof(JsonUTCDateTimeConverter))]
            public DateTime? ValidTo { get; set; }
            public Guid? MediaId { get; set; }
            public decimal? Price { get; set; }
            public int ProductTypeId { get; set; }
            public int? PriceJsFunctionId { get; set; }

            public IList<ChildProductModel> ChildProducts { get; set; }
            public string Extra { get; set; }
        }

        public class ChildProductModel
        {
            public int ParentId { get; set; }
            public int ChildId { get; set; }
            public string ChildName { get; set; }
            public string Name { get; set; }
        }

        public class SimpleProductModel
        {
            public int Id { get; set; }
            public string Name { get; set; }

            public static SimpleProductModel FromDto(ProductDTO dto)
            {
                return new SimpleProductModel() { Id = dto.Id, Name = dto.Name };
            }
        }

        protected IProductRepository _productRepo;
        protected IMediaRepository _mediaRepo;
        protected IJSFunctionRepository _jsFunctionRepo;

        public ProductManagementController(IProductRepository productRepo, 
            IMediaRepository mediaRepo, 
            IJSFunctionRepository jsFunctionRepo, 
            IMembership membership,
            IMembershipRepository memberRepo) : base(membership, memberRepo)
        {
            _productRepo = productRepo;
            _mediaRepo = mediaRepo;
            _jsFunctionRepo = jsFunctionRepo;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Types()
        {
            return ReturnJson(new { types = _productRepo.GetAllProductTypes() });
        }

        [HttpGet]
        public ActionResult PriceJsFunctions()
        {
            return ReturnJson(new {
                lineitemprice = _jsFunctionRepo.GetAll(JSFunctionType.LineItemPrice).Select(jsf => new { id = jsf.Id, name = jsf.Name }),
                coupon = _jsFunctionRepo.GetAll(JSFunctionType.Coupon).Select(jsf => new { id = jsf.Id, name = jsf.Name })
            });
        }

        [HttpGet]
        public ActionResult Search(string search, int? pageNumber, int? itemsPerPage, bool includeArchived = false)
        {
            int totalItems = 0;
            var items = _productRepo.Search("%" + search + "%", null, pageNumber, itemsPerPage, includeArchived, ref totalItems);

            return ReturnJson(new { count = totalItems, items = from i in items select ConvertToModel(i) });
        }

        [HttpGet]
        public ActionResult Products()
        {
            int count = 0;
            var result = _productRepo.Search("%%", null, null, null, false, ref count);

            List<SimpleProductModel> r = new List<SimpleProductModel>();

            foreach (var p in result)
            {
                r.Add(SimpleProductModel.FromDto(p));
            }

            return ReturnJson(r);
        }

        protected List<ProductModel> ConvertToModel(IEnumerable<ProductDTO> items)
        {
            List<ProductModel> result = new List<ProductModel>();

            foreach(var item in items)
            {
                var r = ConvertToModel(item);

                result.Add(r);
            }

            return result;
        }

        protected ProductModel ConvertToModel(ProductDTO item)
        {
            var r = new ProductModel()
            {
                Id = item.Id,
                Name = item.Name,
                Description = item.Description,
                Code = item.Code,
                IsCoupon = item.IsCoupon,
                ValidFrom = item.ValidFrom,
                ValidTo = item.ValidTo,
                MediaId = item.MediaId,
                Price = item.Price,
                ProductTypeId = item.ProductTypeId,
                PriceJsFunctionId = item.PriceJsFunctionId,
                ChildProducts = new List<ChildProductModel>(),
                Extra = item.Extra
            };

            if(item.ParentProductHierarchies.Count > 0)
            {
                var children = _productRepo.GetById((from p in item.ParentProductHierarchies select p.ChildProductId).ToArray());

                foreach (var ch in item.ParentProductHierarchies)
                {
                    var child = (from k in children where k.Id == ch.ChildProductId select k).First();

                    var c = new ChildProductModel()
                    {
                        ParentId = ch.ParentProductId,
                        ChildId = ch.ChildProductId,
                        ChildName = child.Name,
                        Name = ch.Name
                    };

                    r.ChildProducts.Add(c);
                }
            }

            return r;
        }

        protected void PopulateDTO(ProductDTO dto, ProductModel model)
        {
            dto.Name = model.Name;
            dto.Description = model.Description;
            dto.Code = model.Code;
            dto.IsCoupon = model.IsCoupon;
            dto.ValidFrom = model.ValidFrom;
            dto.ValidTo = model.ValidTo;
            dto.MediaId = model.MediaId;
            dto.Price = model.Price;
            dto.ProductTypeId = model.IsCoupon ? 1 : model.ProductTypeId;
            dto.PriceJsFunctionId = model.PriceJsFunctionId;
            dto.Extra = model.Extra?.ToString();

            dto.ParentProductHierarchies.Clear();

            if (model.ChildProducts != null)
            {
                foreach (var ch in model.ChildProducts)
                {
                    var c = new ProductHierarchyDTO()
                    {
                        __IsNew = true,
                        ParentProduct = dto,
                        ChildProductId = ch.ChildId,
                        Name = ch.Name
                    };

                    dto.ParentProductHierarchies.Add(c);
                }
            }
        }

        [HttpGet]
        public ActionResult Product(int id)
        {
            var result = _productRepo.GetById(id, RepositoryInterfaces.Enums.ProductLoadInstructions.ParentProductHierarchy);

            return ReturnJson(ConvertToModel(result));
        }

        [HttpPost]
        public ActionResult ProductSave(ProductModel record)
        {
            ProductDTO p = null;

            if (record.__IsNew == true)
            {
                p = new ProductDTO()
                {
                    __IsNew = true,
                    ParentProductHierarchies = new List<ProductHierarchyDTO>(),
                    ChildProductHierarchies = new List<ProductHierarchyDTO>()
                };
            }
            else
            {
                p = _productRepo.GetById(record.Id);
            }

            PopulateDTO(p, record);

            ProductDTO result = null;

            using (TransactionScope scope = new TransactionScope())
            {
                _productRepo.RemoveProductHierarchies(record.Id);

                result = _productRepo.Save(p, true, true);

                scope.Complete();
            }

            result = _productRepo.GetById(result.Id, RepositoryInterfaces.Enums.ProductLoadInstructions.ParentProductHierarchy);

            return ReturnJson(ConvertToModel(result));
        }

        protected List<FieldTransform> CreateTransforms()
        {
            return new ProductImportExportTransformFactory().Create();
        }

        [HttpPost]
        public ActionResult Import(HttpPostedFileBase file)
        {
            ProductImportWriter writer = new ProductImportWriter(_productRepo);

            byte[] data = new byte[file.ContentLength];
            file.InputStream.Read(data, 0, file.ContentLength);

            ImportParserResult result = null;

            Action<int> progressCallback = counter =>
            {
                // not doing anything at the moment
            };

            using (var stream = new MemoryStream(data))
            {
                ExcelImportParser parser = new ExcelImportParser();
                var transform = CreateTransforms();

                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(0, 10, 0)))
                {
                    result = parser.Import(stream, transform, writer, false, progressCallback);

                    try
                    {
                        if (result.Success)
                        {
                            scope.Complete();
                        }
                    }
                    catch (Exception e)
                    {
                        result.Errors.Add(string.Format("A problem was encountered committing the data to the database, the error message given was {0}", e.Message));
                    }
                }
            }

            return ReturnJson(result);
        }

        [HttpGet]
        public FileResult Export()
        {
            var parser = new ProductExportParser(_productRepo) { IncludeTitles = true };
            var writer = new ExcelWriter();
            var transforms = CreateTransforms();

            int total = -1;
            var products = _productRepo.Search("%", null, null, null, false, ref total);

            Action<int> progressCallback = counter =>
            {
                // not doing anything at the moment
            };

            // I know calling Import seems backwards, think of it as importing the data into Excel
            parser.Import(products, transforms, writer, false, progressCallback);

            Response.AddHeader("Content-Disposition", "inline; filename=ProductExport.xlsx");

            byte[] data;
            using (MemoryStream stream = new MemoryStream())
            {
                writer.Book.SaveAs(stream);

                data = stream.ToArray();
            }

            return this.File(data, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        }

        [HttpPost]
        public ActionResult MediaUpload(HttpPostedFileBase file, string name, string description, int id)
        {
            MediaDTO media = null;

            var product = _productRepo.GetById(id);

            try
            {
                byte[] data = new byte[file.ContentLength];
                file.InputStream.Read(data, 0, file.ContentLength);

                if (product.MediaId.HasValue == false)
                {
                    using (TransactionScope scope = new TransactionScope())
                    {
                        media = _mediaRepo.CreateMediaFromPostedFile(data, name, description, file.FileName, file.ContentType, false, (Guid)this.CurrentUserId);
                        product.MediaId = media.Id;
                        _productRepo.Save(product, true);

                        scope.Complete();
                    }
                }
                else
                {
                    media = _mediaRepo.UpdateMediaFromPostedFile(data, product.MediaId.Value, string.IsNullOrEmpty(name) == false ? name : string.Format("Image for {0} task type", product.Name), string.IsNullOrEmpty(description) == false ? description : string.Format("Image for {0} task type", product.Name), file.FileName, file.ContentType, false, (Guid)this.CurrentUserId);
                }
            }
            catch (Exception e)
            {
                ImportParserResult r = new ImportParserResult();
                r.Errors.Add(e.Message);

                List<ImportParserResult> rs = new List<ImportParserResult>();
                rs.Add(r);

                return ReturnJson(rs);
            }

            ImportParserResult result = new ImportParserResult() { Count = 1, Data = product.MediaId };
            List<ImportParserResult> results = new List<ImportParserResult>();
            results.Add(result);

            return ReturnJson(results);
        }
    }
}
