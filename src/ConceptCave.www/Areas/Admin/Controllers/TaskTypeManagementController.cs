﻿using ConceptCave.Checklist;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Repository;
using ConceptCave.RepositoryInterfaces.Importing;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Dynamic;
using ConceptCave.www.Areas.Admin.Models;

namespace ConceptCave.www.Areas.Admin.Controllers
{
    [Authorize(Roles = RoleRepository.COREROLE_TASKTYPE)]
    public class TaskTypeManagementController : ControllerBaseEx
    {
        public class UserTypeRelationshipModel
        {
            public int UserTypeId { get; set; }
            public string Name { get; set; }
            public bool CanPerform { get; set; }

        }
        public class TaskTypeModel
        {
            public bool __IsNew { get; set; }

            public Guid Id { get; set; }
            public string Name { get; set; }
            public string ProductCatalogs { get; set; }
            public int UserInterfaceType { get; set; }
            public string DefaultLengthInDays { get; set; }
            public int PhotoSupport { get; set; }
            public List<TaskTypePublishedResourceModel> PublishedResources { get; set; }
            public List<TaskTypeStatusModel> Statuses { get; set; }
            public List<TaskTypeFinishedStatusModel> FinishedStatuses { get; set; }
            public List<TaskTypeMediaFolderModel> Documentation { get; set; }

            public List<TaskTypeRelationshipModel> Relationships { get; set; }
            public List<TaskTypeFacilityOverrideModel> FacilityOverrides { get; set; }
            public Guid? MediaId { get; set; }
            public Guid? DefaultStatusToWhenCreated { get; set; }
            public Guid? SetToStatusWhenFinished { get; set; }
            public bool AutoStart1stChecklist { get; set; }
            public bool AutoFinishTaskOnLastChecklist { get; set; }
            public decimal? EstimatedDurationSeconds { get; set; }
            public string MinimumDuration { get; set; }
            public List<UserTypeRelationshipModel> RelatedUserTypes { get; set; }

            public bool ExcludeFromTimesheets { get; set; }

            public bool ExcludeFromAutoTimesheetGrouping { get; set; }
            public bool AutoFinishAfterStart { get; set; }
            public TaskTypeModel()
            {
                PublishedResources = new List<TaskTypePublishedResourceModel>();
                Documentation = new List<TaskTypeMediaFolderModel>();
                FinishedStatuses = new List<TaskTypeFinishedStatusModel>();
                Statuses = new List<TaskTypeStatusModel>();
                Relationships = new List<TaskTypeRelationshipModel>();
                FacilityOverrides = new List<TaskTypeFacilityOverrideModel>();
                RelatedUserTypes = new List<UserTypeRelationshipModel>();
            }
        }

        public class TaskTypePublishedResourceModel
        {
            public bool __IsNew { get; set; }
            public Guid TaskTypeId { get; set; }
            public int PublishedResourceId { get; set; }
            public string Name { get; set; }
            public bool IsMandatory { get; set; }
            public bool AllowMultiple { get; set; }
            public bool PreventFinishing { get; set; }
            public int EstimatedPageCount { get; set; }
        }

        public class TaskTypeFinishedStatusModel
        {
            public Guid Id { get; set; }
            
            public string Text { get;set;}

            public bool Selected { get; set; }
            public int Stage { get; set; }
        }

        public class TaskTypeStatusModel
        {
            public Guid Id { get; set; }
            
            public string Text { get;set;}

            public int SortOrder { get; set; }

            public bool Selected { get; set; }

            public int ReferenceNumberMode { get; set; }
        }

        public class TaskTypeMediaFolderModel
        {
            public bool __IsNew { get; set; }
            public Guid TaskTypeId { get; set; }
            public Guid MediaFolderId { get; set; }
            public string MediaName { get; set; }
            public string Text { get; set; }
        }

        public class TaskTypeRelationshipModel
        {
            public Guid TaskTypeId { get; set; }

            public string Name { get; set; }
            public bool Selected { get; set; }
        }

        public class TaskTypeFacilityOverrideModel
        {
            public Guid TaskTypeId { get; set; }
            public int FacilityStructureId { get; set; }
            public FacilityStructureModel FacilityStructure { get; set; }
            public int? EstimatedDurationSeconds { get; set; }
            public string MinimumDuration { get; set; }
        }

        private ITaskTypeRepository _typeRepo;
        private IPublishingGroupResourceRepository _prRepo;
        private IMediaRepository _mediaRepo;
        private IFinishedStatusRepository _finishedStatusRepo;
        private IStatusRepository _statusRepo;
        private IMembershipTypeRepository _membershipTypeRepo;

        public TaskTypeManagementController(ITaskTypeRepository typeRepo, 
            IPublishingGroupResourceRepository prRepo, 
            IMediaRepository mediaRepo,
            IFinishedStatusRepository finishedStatusRepo,
            IStatusRepository statusRepo,
            IMembershipTypeRepository membershipTypeRepo,
            VirtualMgr.Membership.IMembership membership,
            IMembershipRepository memberRepo) : base(membership, memberRepo)
        {
            _typeRepo = typeRepo;
            _prRepo = prRepo;
            _mediaRepo = mediaRepo;
            _finishedStatusRepo = finishedStatusRepo;
            _statusRepo = statusRepo;
            _membershipTypeRepo = membershipTypeRepo;
        }

        public ActionResult Index()
        {
            return View();
        }

        public static List<TaskTypeModel> ConvertToModel(IEnumerable<ProjectJobTaskTypeDTO> items)
        {
            var result = new List<TaskTypeModel>();

            items.ForEach(t =>
            {
                var m = ConvertToModel(t, null, null, null, null);

                result.Add(m);
            });

            return result;
        }

        public static TaskTypeModel ConvertToModel(ProjectJobTaskTypeDTO t, 
            IEnumerable<ProjectJobFinishedStatusDTO> finishedStatuses,
            IEnumerable<ProjectJobStatusDTO> statuses,
            IEnumerable<ProjectJobTaskTypeDTO> types,
            IEnumerable<UserTypeDTO> userTypes)
        {
            var m = new TaskTypeModel()
            {
                Id = t.Id,
                Name = t.Name,
                ProductCatalogs = t.ProductCatalogs,
                UserInterfaceType = t.UserInterfaceType,
                DefaultLengthInDays = t.DefaultLengthInDays,
                MediaId = t.MediaId,
                AutoStart1stChecklist = t.AutoStart1stChecklist,
                AutoFinishTaskOnLastChecklist = t.AutoFinishAfterLastChecklist,
                PhotoSupport = t.PhotoSupport,
                EstimatedDurationSeconds = t.EstimatedDurationSeconds,
                MinimumDuration = t.MinimumDuration,
                ExcludeFromTimesheets = t.ExcludeFromTimesheets,
                AutoFinishAfterStart = t.AutoFinishAfterStart
            };

            if(types != null)
            {
                foreach (var r in types)
                {
                    var n = new TaskTypeRelationshipModel()
                    {
                        TaskTypeId = r.Id,
                        Name = r.Name,
                        Selected = (from a in t.ProjectJobTaskTypeSourceRelationships where a.ProjectJobTaskTypeDestinationId.Equals(r.Id) == true select a).Count() > 0
                    };

                    m.Relationships.Add(n);
                }
            }

            foreach(var fo in t.ProjectJobTaskTypeFacilityOverrides)
            {
                m.FacilityOverrides.Add(new TaskTypeFacilityOverrideModel()
                {
                    TaskTypeId = fo.TaskTypeId,
                    FacilityStructureId = fo.FacilityStructureId,
                    EstimatedDurationSeconds = fo.EstimatedDurationSeconds,
                    MinimumDuration = fo.MinimumDuration,
                    FacilityStructure = fo.FacilityStructure != null ? FacilityStructureModel.FromDto(fo.FacilityStructure, false, false, true) : null
                });
            }

            foreach (var p in t.ProjectJobTaskTypePublishingGroupResources)
            {
                var r = new TaskTypePublishedResourceModel()
                {
                    TaskTypeId = t.Id,
                    PublishedResourceId = p.PublishingGroupResourceId,
                    Name = p.PublishingGroupResource.Name,
                    IsMandatory = p.Mandatory,
                    AllowMultiple = p.AllowMultiple,
                    PreventFinishing = p.PreventFinishing,
                    EstimatedPageCount = p.PublishingGroupResource.EstimatedPageCount
                };

                if(r.EstimatedPageCount == 1)
                {
                    r.PreventFinishing = true;
                }

                m.PublishedResources.Add(r);
            }

            if(finishedStatuses != null)
            {
                foreach (var f in finishedStatuses)
                {
                    var s = new TaskTypeFinishedStatusModel()
                    {
                        Id = f.Id,
                        Text = f.Text,
                        Selected = (from a in t.ProjectJobTaskTypeFinishedStatuses where a.ProjectJobFinishedStatusId == f.Id select a).Count() > 0,
                        Stage = f.Stage
                    };

                    m.FinishedStatuses.Add(s);
                }
            }

            if (statuses != null)
            {
                foreach (var f in statuses)
                {
                    var st = (from a in t.ProjectJobTaskTypeStatuses where a.StatusId == f.Id select a);

                    var s = new TaskTypeStatusModel()
                    {
                        Id = f.Id,
                        Text = f.Text,
                        SortOrder = 0,
                        Selected = st.Count() > 0
                    };

                    if (st.Count() > 0)
                    {
                        s.ReferenceNumberMode = st.First().ReferenceNumberMode;
                        s.SortOrder = st.First().SortOrder;

                        if (st.First().DefaultToWhenCreated == true)
                        {
                            m.DefaultStatusToWhenCreated = st.First().StatusId;
                        }

                        if (st.First().SetToWhenFinished == true)
                        {
                            m.SetToStatusWhenFinished = st.First().StatusId;
                        }
                    }

                    m.Statuses.Add(s);
                }
            }

            foreach(var mf in t.ProjectJobTaskTypeMediaFolders)
            {
                var f = new TaskTypeMediaFolderModel()
                {
                    TaskTypeId = t.Id,
                    MediaFolderId = mf.MediaFolderId,
                    MediaName = mf.MediaFolder.Text,
                    Text = mf.Text
                };

                m.Documentation.Add(f);
            }

            if (userTypes != null)
            {
                foreach (var userType in userTypes)
                {
                    var utttr = t.UserTypeTaskTypeRelationships.FirstOrDefault(r => r.UserTypeId == userType.Id && r.RelationshipType == (int)UserTypeTaskTypeRelationship.CanPerform);
                    m.RelatedUserTypes.Add(new UserTypeRelationshipModel()
                    {
                        CanPerform = utttr != null,
                        UserTypeId = userType.Id,
                        Name = userType.Name
                    });
                }
            }

            return m;
        }

        protected dynamic PopulateDTO(ProjectJobTaskTypeDTO dto, TaskTypeModel model)
        {
            dto.__IsNew = model.__IsNew;

            if (model.__IsNew == true)
            {
                dto.Id = CombFactory.NewComb();
            }

            dto.Name = model.Name;
            dto.ProductCatalogs = model.ProductCatalogs;
            dto.UserInterfaceType = model.UserInterfaceType;
            dto.DefaultLengthInDays = model.DefaultLengthInDays;
            dto.AutoFinishAfterStart = model.AutoFinishAfterStart;
            if (dto.AutoFinishAfterStart)
            {
                // When Auto Finish After Start is checked there can be no other workflow (ie no checklists) - make sure this persists
                dto.AutoStart1stChecklist = false;
                dto.AutoFinishAfterLastChecklist = false;
                model.PublishedResources.Clear();                   
            }
            else
            {
                dto.AutoStart1stChecklist = model.AutoStart1stChecklist;
                dto.AutoFinishAfterLastChecklist = model.AutoFinishTaskOnLastChecklist;
            }
            dto.PhotoSupport = model.PhotoSupport;
            dto.EstimatedDurationSeconds = model.EstimatedDurationSeconds;
            dto.MinimumDuration = model.MinimumDuration;
            dto.ExcludeFromTimesheets = model.ExcludeFromTimesheets;

            // added relationships
            foreach(var r in (from m in model.Relationships where m.Selected == true select m))
            {
                var existing = (from f in dto.ProjectJobTaskTypeSourceRelationships where f.ProjectJobTaskTypeDestinationId == r.TaskTypeId select f);
                if (existing.Count() != 0)
                {
                    continue;
                }

                ProjectJobTaskTypeRelationshipDTO rel = new ProjectJobTaskTypeRelationshipDTO()
                {
                    __IsNew = true,
                    ProjectJobTaskTypeSourceId = dto.Id,
                    ProjectJobTaskTypeDestinationId = r.TaskTypeId
                };

                dto.ProjectJobTaskTypeSourceRelationships.Add(rel);
            }

            // Add/Update Facility Overrides
            foreach (var mFo in model.FacilityOverrides)
            {
                var existing = dto.ProjectJobTaskTypeFacilityOverrides.FirstOrDefault(dtoFo => dtoFo.FacilityStructureId == mFo.FacilityStructureId);
                if (existing == null)
                {
                    dto.ProjectJobTaskTypeFacilityOverrides.Add(new ProjectJobTaskTypeFacilityOverrideDTO()
                    {
                        __IsNew = true,
                        TaskTypeId = mFo.TaskTypeId,
                        FacilityStructureId = mFo.FacilityStructureId,
                        EstimatedDurationSeconds = mFo.EstimatedDurationSeconds,
                        MinimumDuration = mFo.MinimumDuration
                    });
                } 
                else
                {
                    existing.EstimatedDurationSeconds = mFo.EstimatedDurationSeconds;
                    existing.MinimumDuration = mFo.MinimumDuration;
                }
            }

            // first resources that have been added
            foreach(var p in (from m in model.PublishedResources where m.__IsNew == true select m))
            {
                ProjectJobTaskTypePublishingGroupResourceDTO mapping = new ProjectJobTaskTypePublishingGroupResourceDTO() {
                    __IsNew = true,
                    TaskTypeId = p.TaskTypeId,
                    PublishingGroupResourceId = p.PublishedResourceId,
                    Mandatory = p.IsMandatory,
                    AllowMultiple = p.AllowMultiple,
                    PreventFinishing = p.PreventFinishing
                };
                    
                dto.ProjectJobTaskTypePublishingGroupResources.Add(mapping);
            }

            // now just editing the ones that already exist
            foreach (var p in (from m in model.PublishedResources where m.__IsNew == false select m))
            {
                var existing = (from m in dto.ProjectJobTaskTypePublishingGroupResources where m.PublishingGroupResourceId == p.PublishedResourceId select m);
                if (existing.Count() == 0)
                {
                    // hmm
                    continue;
                }

                existing.First().Mandatory = p.IsMandatory;
                existing.First().AllowMultiple = p.AllowMultiple;
                existing.First().PreventFinishing = p.PreventFinishing;

                existing.First().PublishingGroupResource = null;
            }

            // now the documentation that has been added
            foreach(var d in (from m in model.Documentation where m.__IsNew == true select m))
            {
                ProjectJobTaskTypeMediaFolderDTO folder = new ProjectJobTaskTypeMediaFolderDTO()
                {
                    __IsNew = true,
                    TaskTypeId = d.TaskTypeId,
                    MediaFolderId = d.MediaFolderId,
                    Text = string.IsNullOrEmpty(d.Text) == true ? "" : d.Text
                };

                dto.ProjectJobTaskTypeMediaFolders.Add(folder);
            }

            // now the documentation that already exists
            foreach(var d in (from m in model.Documentation where m.__IsNew == false select m))
            {
                var existing = (from m in dto.ProjectJobTaskTypeMediaFolders where m.TaskTypeId == d.TaskTypeId && m.MediaFolderId == d.MediaFolderId select m);
                if(existing.Count() == 0)
                {
                    // not sure how this can happen
                    continue;
                }

                existing.First().MediaFolderId = d.MediaFolderId;
                existing.First().Text = string.IsNullOrEmpty(d.Text) == true ? "" : d.Text;
            }

            // now the finish statuses that have been added
            foreach(var s in (from f in model.FinishedStatuses where f.Selected == true select f))
            {
                var existing = (from f in dto.ProjectJobTaskTypeFinishedStatuses where f.ProjectJobFinishedStatusId == s.Id select f);
                if(existing.Count() != 0)
                {
                    continue;
                }

                var m = new ProjectJobTaskTypeFinishedStatusDTO()
                {
                    __IsNew = true,
                    Id = CombFactory.NewComb(),
                    ProjectJobFinishedStatusId = s.Id,
                    TaskTypeId = dto.Id
                };

                dto.ProjectJobTaskTypeFinishedStatuses.Add(m);
            }

            // now the statuses that have been added
            foreach (var s in (from f in model.Statuses where f.Selected == true select f))
            {
                var existing = (from f in dto.ProjectJobTaskTypeStatuses where f.StatusId == s.Id select f);
                if (existing.Count() != 0)
                {
                    existing.First().SortOrder = s.SortOrder;
                    existing.First().ReferenceNumberMode = s.ReferenceNumberMode;
                    existing.First().DefaultToWhenCreated = false; // these are set later 
                    existing.First().SetToWhenFinished = false;
                    continue;
                }

                var m = new ProjectJobTaskTypeStatusDTO()
                {
                    __IsNew = true,
                    StatusId = s.Id,
                    TaskTypeId = dto.Id,
                    SortOrder = s.SortOrder,
                    ReferenceNumberMode = s.ReferenceNumberMode,
                    DefaultToWhenCreated = false,
                    SetToWhenFinished = false
                };

                dto.ProjectJobTaskTypeStatuses.Add(m);
            }

            if(model.DefaultStatusToWhenCreated.HasValue == true)
            {
                var defaultTo = from f in dto.ProjectJobTaskTypeStatuses where f.StatusId == model.DefaultStatusToWhenCreated.Value select f;

                if(defaultTo.Count() > 0)
                {
                    defaultTo.First().DefaultToWhenCreated = true;
                }
            }

            if (model.SetToStatusWhenFinished.HasValue == true)
            {
                var setTo = from f in dto.ProjectJobTaskTypeStatuses where f.StatusId == model.SetToStatusWhenFinished.Value select f;

                if (setTo.Count() > 0)
                {
                    setTo.First().SetToWhenFinished = true;
                }
            }

            foreach(var rmt in model.RelatedUserTypes.Where(rmt => rmt.CanPerform && !dto.UserTypeTaskTypeRelationships.Any(mttr => mttr.UserTypeId == rmt.UserTypeId)))
            {
                dto.UserTypeTaskTypeRelationships.Add(new UserTypeTaskTypeRelationshipDTO()
                {
                    __IsNew = true,
                    UserTypeId = rmt.UserTypeId,
                    RelationshipType = (int)UserTypeTaskTypeRelationship.CanPerform,
                    ProjectJobTaskType = dto
                });
            }

            dynamic result = new ExpandoObject();

            result.DeletedResources = (from m in dto.ProjectJobTaskTypePublishingGroupResources where (from o in model.PublishedResources select o.PublishedResourceId).Contains(m.PublishingGroupResourceId) == false select m).ToList();
            result.DeletedDocumentation = (from m in dto.ProjectJobTaskTypeMediaFolders where (from o in model.Documentation select o.MediaFolderId.ToString() + o.TaskTypeId.ToString()).Contains(m.MediaFolderId.ToString() + m.TaskTypeId.ToString()) == false select m).ToList();
            result.DeletedFinishedStatuses = (from m in dto.ProjectJobTaskTypeFinishedStatuses where (from o in model.FinishedStatuses where o.Selected == false select o.Id).Contains(m.ProjectJobFinishedStatusId) == true select m).ToList();
            result.DeletedStatuses = (from m in dto.ProjectJobTaskTypeStatuses where (from o in model.Statuses where o.Selected == false select o.Id).Contains(m.StatusId) == true select m).ToList();
            result.DeletedRelationships = (from m in dto.ProjectJobTaskTypeSourceRelationships where (from o in model.Relationships where o.Selected == false select o.TaskTypeId).Contains(m.ProjectJobTaskTypeDestinationId) == true select m).ToList();
            result.DeletedFacilityOverrides = (from dtoFo in dto.ProjectJobTaskTypeFacilityOverrides where !model.FacilityOverrides.Any(mFo => mFo.FacilityStructureId == dtoFo.FacilityStructureId) select dtoFo).ToList();
            result.DeletedUserTypeRelationships = (from mttr in dto.UserTypeTaskTypeRelationships where !model.RelatedUserTypes.Any(rmt => rmt.UserTypeId == mttr.UserTypeId && rmt.CanPerform == true) select mttr).ToList();
            
            // we are going to remove anything that is going to be deleted from our object model
            foreach(var r in result.DeletedResources)
            {
                dto.ProjectJobTaskTypePublishingGroupResources.Remove((ProjectJobTaskTypePublishingGroupResourceDTO)r);
            }

            foreach (var r in result.DeletedDocumentation)
            {
                dto.ProjectJobTaskTypeMediaFolders.Remove((ProjectJobTaskTypeMediaFolderDTO)r);
            }

            foreach (var r in result.DeletedFinishedStatuses)
            {
                dto.ProjectJobTaskTypeFinishedStatuses.Remove((ProjectJobTaskTypeFinishedStatusDTO)r);
            }

            foreach (var r in result.DeletedStatuses)
            {
                dto.ProjectJobTaskTypeStatuses.Remove((ProjectJobTaskTypeStatusDTO)r);
            }

            foreach (var r in result.DeletedRelationships)
            {
                dto.ProjectJobTaskTypeSourceRelationships.Remove((ProjectJobTaskTypeRelationshipDTO)r);
            }

            foreach (var r in result.DeletedFacilityOverrides)
            {
                dto.ProjectJobTaskTypeFacilityOverrides.Remove((ProjectJobTaskTypeFacilityOverrideDTO)r);
            }

            foreach(var r in result.DeletedUserTypeRelationships)
            {
                dto.UserTypeTaskTypeRelationships.Remove((UserTypeTaskTypeRelationshipDTO)r);
            }
            // return the set of mappings that have been deleted (present in the DTO, but not in the model)
            return result; 
        }

        [HttpGet]
        public ActionResult Types()
        {
            var result = _typeRepo.GetAll(ProjectJobTaskTypeLoadInstructions.None);

            return ReturnJson(ConvertToModel(result));
        }

        [HttpGet]
        public ActionResult Type(Guid id)
        {
            var result = _typeRepo.GetById(id, ProjectJobTaskTypeLoadInstructions.PublishedResources | 
                ProjectJobTaskTypeLoadInstructions.Documentation | 
                ProjectJobTaskTypeLoadInstructions.DocumentationFolders |
                ProjectJobTaskTypeLoadInstructions.FinishedStatusses |
                ProjectJobTaskTypeLoadInstructions.Statusses |
                ProjectJobTaskTypeLoadInstructions.Relationships |
                ProjectJobTaskTypeLoadInstructions.FacilityOverrides | 
                ProjectJobTaskTypeLoadInstructions.FacilityOverrideStructures |
                ProjectJobTaskTypeLoadInstructions.RelatedUserTypes);

            var finishedStatuses = _finishedStatusRepo.GetAll();
            var statuses = _statusRepo.GetAll();
            var types = _typeRepo.GetAll(ProjectJobTaskTypeLoadInstructions.None);
            var sites = _membershipTypeRepo.GetAll();
            var userTypes = _membershipTypeRepo.GetAll();

            return ReturnJson(ConvertToModel(result, finishedStatuses, statuses, types, userTypes));
        }

        [HttpPost]
        public ActionResult TaskTypeSave(TaskTypeModel type)
        {
            ProjectJobTaskTypeDTO t = null;

            if (type.__IsNew == true)
            {
                t = new ProjectJobTaskTypeDTO()
                {
                    __IsNew = true
                };
            }
            else
            {
                t = _typeRepo.GetById(type.Id, 
                    ProjectJobTaskTypeLoadInstructions.PublishedResources | 
                    ProjectJobTaskTypeLoadInstructions.Documentation | 
                    ProjectJobTaskTypeLoadInstructions.DocumentationFolders |
                    ProjectJobTaskTypeLoadInstructions.FinishedStatusses |
                    ProjectJobTaskTypeLoadInstructions.Statusses |
                    ProjectJobTaskTypeLoadInstructions.Relationships |
                    ProjectJobTaskTypeLoadInstructions.FacilityOverrides |
                    ProjectJobTaskTypeLoadInstructions.RelatedUserTypes);
            }

            var toDelete = PopulateDTO(t, type);

            ProjectJobTaskTypeDTO result = null;

            using (TransactionScope scope = new TransactionScope())
            {
                result = _typeRepo.Save(t, true, true);

                List<ProjectJobTaskTypePublishingGroupResourceDTO> deletedResources = (List<ProjectJobTaskTypePublishingGroupResourceDTO>)toDelete.DeletedResources;
                List<ProjectJobTaskTypeMediaFolderDTO> deletedFolders = (List<ProjectJobTaskTypeMediaFolderDTO>)toDelete.DeletedDocumentation;
                List<ProjectJobTaskTypeFinishedStatusDTO> deletedFinishedStatuses = (List<ProjectJobTaskTypeFinishedStatusDTO>)toDelete.DeletedFinishedStatuses;
                List<ProjectJobTaskTypeStatusDTO> deletedStatuses = (List<ProjectJobTaskTypeStatusDTO>)toDelete.DeletedStatuses;
                List<ProjectJobTaskTypeRelationshipDTO> deletedRelationships = (List<ProjectJobTaskTypeRelationshipDTO>)toDelete.DeletedRelationships;
                List<ProjectJobTaskTypeFacilityOverrideDTO> deletedFacilityOverrides = (List<ProjectJobTaskTypeFacilityOverrideDTO>)toDelete.DeletedFacilityOverrides;
                var deletedUserTypeRelationships = (List<UserTypeTaskTypeRelationshipDTO>)toDelete.DeletedUserTypeRelationships;

                _typeRepo.RemoveTaskTypeResources(type.Id, (from d in deletedResources select d.PublishingGroupResourceId).ToArray());
                _typeRepo.RemoveTaskTypeDocumentation(type.Id, (from d in deletedFolders select d.MediaFolderId).ToArray());
                _typeRepo.RemoveTaskTypeFinishedStatuses(type.Id, (from d in deletedFinishedStatuses select d.ProjectJobFinishedStatusId).ToArray());
                _typeRepo.RemoveTaskTypeStatuses(type.Id, (from d in deletedStatuses select d.StatusId).ToArray());
                _typeRepo.RemoveTaskTypeRelationships(type.Id, (from d in deletedRelationships select d.ProjectJobTaskTypeDestinationId).ToArray());
                _typeRepo.RemoveTaskTypeFacilityOverrides(type.Id, (from d in deletedFacilityOverrides select d.FacilityStructureId).ToArray());
                _typeRepo.RemoveUserTypeRelationships(type.Id, (from d in deletedUserTypeRelationships select d.UserTypeId).ToArray(), UserTypeTaskTypeRelationship.CanPerform);

                scope.Complete();
            }

            result = _typeRepo.GetById(type.Id, ProjectJobTaskTypeLoadInstructions.PublishedResources |
                    ProjectJobTaskTypeLoadInstructions.Documentation |
                    ProjectJobTaskTypeLoadInstructions.DocumentationFolders |
                    ProjectJobTaskTypeLoadInstructions.FinishedStatusses |
                    ProjectJobTaskTypeLoadInstructions.Statusses |
                    ProjectJobTaskTypeLoadInstructions.FacilityOverrides | 
                    ProjectJobTaskTypeLoadInstructions.FacilityOverrideStructures |
                    ProjectJobTaskTypeLoadInstructions.RelatedUserTypes);

            var finishedStatuses = _finishedStatusRepo.GetAll();
            var statuses = _statusRepo.GetAll();
            var types = _typeRepo.GetAll(ProjectJobTaskTypeLoadInstructions.None);
            var userTypes = _membershipTypeRepo.GetAll();

            return ReturnJson(ConvertToModel(result, finishedStatuses, statuses, types, userTypes));
        }

        [HttpGet]
        public ActionResult SearchPublishedResources(string query)
        {
            var result = _prRepo.GetLikeName("%" + query + "%", PublishingGroupResourceLoadInstruction.Group, 1, 5);

            return ReturnJson(result);
        }

        [HttpPost]
        public ActionResult MediaUpload(HttpPostedFileBase file, string name, string description, Guid id)
        {
            MediaDTO media = null;

            var taskType = _typeRepo.GetById(id, ProjectJobTaskTypeLoadInstructions.None);

            try
            {
                byte[] data = new byte[file.ContentLength];
                file.InputStream.Read(data, 0, file.ContentLength);

                if(taskType.MediaId.HasValue == false)
                {
                    using(TransactionScope scope = new TransactionScope())
                    {
                        media = _mediaRepo.CreateMediaFromPostedFile(data, name, description, file.FileName, file.ContentType, false, (Guid)this.CurrentUserId);
                        taskType.MediaId = media.Id;
                        _typeRepo.Save(taskType, true, false);

                        scope.Complete();
                    }
                }
                else
                {
                    media = _mediaRepo.UpdateMediaFromPostedFile(data, taskType.MediaId.Value, string.IsNullOrEmpty(name) == false ? name : string.Format("Image for {0} task type", taskType.Name), string.IsNullOrEmpty(description) == false ? description : string.Format("Image for {0} task type", taskType.Name), file.FileName, file.ContentType, false, (Guid)this.CurrentUserId);
                }
            }
            catch (Exception e)
            {
                ImportParserResult r = new ImportParserResult();
                r.Errors.Add(e.Message);

                List<ImportParserResult> rs = new List<ImportParserResult>();
                rs.Add(r);

                return ReturnJson(rs);
            }

            ImportParserResult result = new ImportParserResult() { Count = 1, Data = taskType.MediaId };
            List<ImportParserResult> results = new List<ImportParserResult>();
            results.Add(result);

            return ReturnJson(results);
        }
    }
}
