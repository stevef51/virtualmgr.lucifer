﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.Repository;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.DTO.DTOClasses;
using System.Transactions;
using ConceptCave.Checklist;

namespace ConceptCave.www.Areas.Admin.Controllers
{
    [Authorize(Roles = RoleRepository.COREROLE_TASKTYPE)]
    public class StatusManagementController : ControllerBase
    {
        public class StatusModel
        {
            public bool __IsNew { get; set; }
            public Guid Id { get; set; }
            public string Text { get;set;}
            public string Description { get; set; }
        }

        protected IStatusRepository _statusController;

        public StatusManagementController(IStatusRepository statusController)
        {
            _statusController = statusController;
        }

        public ActionResult Index()
        {
            return View();
        }

        protected List<StatusModel> ConvertToModel(IEnumerable<ProjectJobStatusDTO> items)
        {
            List<StatusModel> result = new List<StatusModel>();

            foreach(var item in items)
            {
                var r = ConvertToModel(item);

                result.Add(r);
            }

            return result;
        }

        protected StatusModel ConvertToModel(ProjectJobStatusDTO item)
        {
            var r = new StatusModel()
            {
                Id = item.Id,
                Text = item.Text,
                Description = item.Description
            };

            return r;
        }

        protected void PopulateDTO(ProjectJobStatusDTO dto, StatusModel model)
        {
            dto.Text = model.Text;
            dto.Description = model.Description;
        }

        [HttpGet]
        public ActionResult Statuses()
        {
            var result = _statusController.GetAll();

            return ReturnJson(ConvertToModel(result));
        }

        [HttpGet]
        public ActionResult Status(Guid id)
        {
            var result = _statusController.GetById(id);

            return ReturnJson(ConvertToModel(result));
        }

        [HttpPost]
        public ActionResult StatusSave(StatusModel status)
        {
            ProjectJobStatusDTO t = null;

            if (status.__IsNew == true)
            {
                t = new ProjectJobStatusDTO()
                {
                    __IsNew = true,
                    Id = CombFactory.NewComb()
                };
            }
            else
            {
                t = _statusController.GetById(status.Id);
            }

            PopulateDTO(t, status);

            ProjectJobStatusDTO result = null;

            using (TransactionScope scope = new TransactionScope())
            {
                result = _statusController.Save(t, true, true);

                scope.Complete();
            }

            result = _statusController.GetById(t.Id);

            return ReturnJson(ConvertToModel(result));
        }
    }
}
