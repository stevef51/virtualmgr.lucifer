﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.Repository;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.DTO.DTOClasses;
using System.Transactions;
using ConceptCave.Checklist;
using ConceptCave.Repository.ImportMapping;
using ConceptCave.RepositoryInterfaces.Importing;
using System.IO;
using ConceptCave.Checklist.Interfaces;

namespace ConceptCave.www.Areas.Admin.Controllers
{
    [Authorize(Roles = RoleRepository.COREROLE_JSFUNCTION)]
    public class JSFunctionManagementController : ControllerBase
    {
        public class JSFunctionModel
        {
            public bool __IsNew { get; set; }
            public bool Archived { get; set; }
            public int Id { get; set; }
            public int Type { get; set; }
            public string Name { get;set;}
            public string Script {get;set;}
        }

        protected IJSFunctionRepository _JsFunctionRepo;
        private readonly IJsFunctionService _jsFunctionService;

        public JSFunctionManagementController(IJSFunctionRepository jsFunctionRepo, IJsFunctionService jsFunctionService)
        {
            _JsFunctionRepo = jsFunctionRepo;
            _jsFunctionService = jsFunctionService;
        }

        public ActionResult Index()
        {
            return View();
        }
        protected List<JSFunctionModel> ConvertToModel(IEnumerable<JsfunctionDTO> items)
        {
            List<JSFunctionModel> result = new List<JSFunctionModel>();

            foreach(var item in items)
            {
                var r = ConvertToModel(item);

                result.Add(r);
            }

            return result;
        }

        protected JSFunctionModel ConvertToModel(JsfunctionDTO item)
        {
            var r = new JSFunctionModel()
            {
                Id = item.Id,
                Archived = item.Archived,
                Name = item.Name,
                Type = item.Type,
                Script = item.Script
            };

            return r;
        }

        protected void PopulateDTO(JsfunctionDTO dto, JSFunctionModel model)
        {
            dto.Name = model.Name;
            dto.Type = model.Type;
            dto.Script = model.Script;
        }

        [HttpGet]
        public ActionResult JsFunction(int id)
        {
            var result = _JsFunctionRepo.GetById(id);

            return ReturnJson(ConvertToModel(result));
        }

        [HttpGet]
        public ActionResult Search(string search, int? pageNumber, int? itemsPerPage, bool includeArchived = false)
        {
            int totalItems = 0;
            var items = _JsFunctionRepo.Search("%" + search + "%", pageNumber, itemsPerPage, includeArchived, ref totalItems);

            return ReturnJson(new { count = totalItems, items = from i in items select ConvertToModel(i) });
        }


        [HttpPost]
        public ActionResult JsFunctionSave(JSFunctionModel record)
        {
            JsfunctionDTO p = null;

            if (record.__IsNew == true)
            {
                p = new JsfunctionDTO()
                {
                    __IsNew = true
                };
            }
            else
            {
                p = _JsFunctionRepo.GetById(record.Id);
            }

            // Save will unarchive
            p.Archived = false;
            PopulateDTO(p, record);

            JsfunctionDTO result = null;

            using (TransactionScope scope = new TransactionScope())
            {
                result = _JsFunctionRepo.Save(p, true);

                scope.Complete();
            }

            result = _JsFunctionRepo.GetById(result.Id);

            _jsFunctionService.Reload();

            return ReturnJson(ConvertToModel(result));
        }

        [HttpDelete]
        public ActionResult JsFunctionDelete(int id)
        {
            bool archived = false;
            _JsFunctionRepo.Delete(id, true, out archived);
            if (!archived)
                return ReturnJson(true);
            else
                return ReturnJson(new { archived = true });
        }


        /*        protected List<FieldTransform> CreateTransforms()
                {
                    return new ProductImportExportTransformFactory().Create();
                }

                [HttpPost]
                public ActionResult Import(HttpPostedFileBase file)
                {
                    ProductImportWriter writer = new ProductImportWriter(_productRepo);

                    byte[] data = new byte[file.ContentLength];
                    file.InputStream.Read(data, 0, file.ContentLength);

                    ImportParserResult result = null;

                    Action<int> progressCallback = counter =>
                    {
                        // not doing anything at the moment
                    };

                    using (var stream = new MemoryStream(data))
                    {
                        ExcelImportParser parser = new ExcelImportParser();
                        var transform = CreateTransforms();

                        using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(0, 10, 0)))
                        {
                            result = parser.Import(stream, transform, writer, false, progressCallback);

                            try
                            {
                                if (result.Success)
                                {
                                    scope.Complete();
                                }
                            }
                            catch (Exception e)
                            {
                                result.Errors.Add(string.Format("A problem was encountered committing the data to the database, the error message given was {0}", e.Message));
                            }
                        }
                    }

                    return ReturnJson(result);
                }

                [HttpGet]
                public FileResult Export()
                {
                    var parser = new ProductExportParser(_productRepo) { IncludeTitles = true };
                    var writer = new ExcelWriter();
                    var transforms = CreateTransforms();

                    int total = -1;
                    var products = _productRepo.Search("%", null, null, false, ref total);

                    Action<int> progressCallback = counter =>
                    {
                        // not doing anything at the moment
                    };

                    // I know calling Import seems backwards, think of it as importing the data into Excel
                    parser.Import(products, transforms, writer, false, progressCallback);

                    Response.AddHeader("Content-Disposition", "inline; filename=ProductExport.xlsx");

                    byte[] data;
                    using (MemoryStream stream = new MemoryStream())
                    {
                        writer.Book.SaveAs(stream);

                        data = stream.ToArray();
                    }

                    return this.File(data, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                }
        */
    }
}
