﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.Repository;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.DTO.DTOClasses;
using System.Transactions;
using ConceptCave.Checklist;
using ConceptCave.BusinessLogic.Configuration;
using ConceptCave.BusinessLogic;
using Newtonsoft.Json.Linq;

namespace ConceptCave.www.Areas.Admin.Controllers
{
    [Authorize(Roles = RoleRepository.COREROLE_SETTINGS)]
    public class ExecutionHandlerQueueManagementController : ControllerBase
    {
        IExecutionHandlerQueueRepository _executionQueueRepo;
        IRepositorySectionManager _repositorySectionManager;
        IReflectionFactory _reflectionFactory;

        public ExecutionHandlerQueueManagementController(IExecutionHandlerQueueRepository executionQueueRepo,
            IRepositorySectionManager repositorySectionManager,
            IReflectionFactory reflectionFactory)
        {
            _executionQueueRepo = executionQueueRepo;
            _repositorySectionManager = repositorySectionManager;
            _reflectionFactory = reflectionFactory;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult OpenHandlers()
        {
            var result = _executionQueueRepo.GetOpenHandlers();

            return ReturnJson(result);
        }

        [HttpGet]
        public ActionResult QueueItems(string handler, string primaryEntityId)
        {
            var result = _executionQueueRepo.GetOpenQueueItems(handler, primaryEntityId);

            return ReturnJson(result);
        }

        [HttpPost]
        public ActionResult Replay(Guid id)
        {
            var item = _executionQueueRepo.GetById(id);

            try
            {
                string type = _repositorySectionManager.ExecutionHandlerTypeForName(item.Handler);

                IExecutionMethodHandler handler = (IExecutionMethodHandler)_reflectionFactory.InstantiateObject(Type.GetType(type));

                JToken result = null;
                using(TransactionScope scope = new TransactionScope())
                {
                    result = handler.Execute(item.Method, JToken.Parse(item.Data), null);

                    item.Processed = true;

                    _executionQueueRepo.Save(item, false, false);

                    scope.Complete();
                }
                return ReturnJson(result);
            }
            catch(Exception e)
            {
                JObject errorResult = new JObject();
                errorResult["IsException"] = true;
                errorResult["Message"] = e.Message;
                errorResult["StackTrace"] = e.StackTrace;

                return ReturnJson(errorResult);
            }
        }
    }
}
