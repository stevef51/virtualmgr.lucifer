﻿using ConceptCave.BusinessLogic;
using ConceptCave.BusinessLogic.Context;
using ConceptCave.BusinessLogic.Invoices;
using ConceptCave.BusinessLogic.Models;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Repository;
using ConceptCave.Repository.ImportMapping;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Importing;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.www.Areas.Admin.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using VirtualMgr.Central;

namespace ConceptCave.www.Areas.Admin.Controllers
{
    [Authorize(Roles = RoleRepository.COREROLE_ASSETADMIN)]
    public class BeaconManagementController : ControllerBase
    {
        private readonly IAssetTrackerRepository _beaconRepo;

        public BeaconManagementController(IAssetTrackerRepository beaconRepo)
        {
            _beaconRepo = beaconRepo;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Search(string search, int? pageNumber, int? itemsPerPage, bool includeArchived = false, int? searchFlags = null, int? includeFlags = null)
        {
            int totalItems = 0;
            var items = _beaconRepo.Search("%" + search + "%", pageNumber, itemsPerPage, includeArchived, ref totalItems, searchFlags, includeFlags, AssetTrackerBeaconLoadInstructions.User | AssetTrackerBeaconLoadInstructions.TaskType);

            return ReturnJson(new { count = totalItems, items = from i in items select BeaconModel.FromDto(i) });
        }

        [HttpGet]
        public ActionResult Beacon(string id)
        {           
            return ReturnJson(LoadBeacon(id));
        }

        protected BeaconModel LoadBeacon(string id)
        {
            return BeaconModel.FromDto(_beaconRepo.GetBeacon(id, AssetTrackerBeaconLoadInstructions.TaskType | AssetTrackerBeaconLoadInstructions.User));
        }
       

        [HttpPost]
        public ActionResult BeaconSave(BeaconModel record)
        {
            var dto = _beaconRepo.GetBeacon(record.id, AssetTrackerBeaconLoadInstructions.None);

            if (dto == null)
            {
                dto = new BeaconDTO()
                {
                    __IsNew = true,
                };
            }

            dto.Id = record.id;
            dto.Name = record.name;
            dto.TaskTypeId = record.tasktypeid;
            dto.UserId = record.userid;
            dto.AssetId = record.assetid;

            var savedDto = dto;
            using (TransactionScope scope = new TransactionScope())
            {
                savedDto = _beaconRepo.Save(dto, true, true);

                scope.Complete();
            }

            dto = _beaconRepo.GetBeacon(savedDto.Id, AssetTrackerBeaconLoadInstructions.TaskType | AssetTrackerBeaconLoadInstructions.User);

            return ReturnJson(BeaconModel.FromDto(dto));
        }
    }
}