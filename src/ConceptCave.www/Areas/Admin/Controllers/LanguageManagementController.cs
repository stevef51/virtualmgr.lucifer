﻿using ConceptCave.Checklist.Interfaces;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Repository;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.SimpleExtensions;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ConceptCave.www.Areas.Admin.Controllers
{
    [Authorize(Roles = RoleRepository.COREROLE_LANGUAGE)]
    public class LanguageManagementController : ControllerBase
    {
        private class TranslationIdComparer : IEqualityComparer<LanguageTranslationDTO>
        {
            public bool Equals(LanguageTranslationDTO x, LanguageTranslationDTO y)
            {
                return x.TranslationId == y.TranslationId;
            }

            public int GetHashCode(LanguageTranslationDTO obj)
            {
                return obj.TranslationId.GetHashCode();
            }
        }


        protected ILanguageRepository _languageRepo;
        protected ILanguageTranslationRepository _languageTranslationRepo;
        protected ILanguageTranslateService _translateService;

        public LanguageManagementController(ILanguageRepository languageRepo, ILanguageTranslationRepository languageTranslationRepo, ILanguageTranslateService translateService)
        {
            _languageRepo = languageRepo;
            _languageTranslationRepo = languageTranslationRepo;
            _translateService = translateService;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Languages()
        {
            var languages = _languageRepo.AllLanguages();
            return ReturnJson(languages);
        }

        [HttpPost]
        public ActionResult SaveLanguage(LanguageDTO language)
        {
            try
            {
                var validCulture = CultureInfo.GetCultureInfo(language.CultureName);
                _languageRepo.Save(language, true, false);
                return ReturnJson(new { result = true });
            }
            catch (CultureNotFoundException cnfe)
            {
                return ReturnJson(new { result = false, error = "not-found" });
            }
            catch
            {
                return ReturnJson(new { result = false, error = "error" } );
            }
        }

        [HttpDelete]
        public ActionResult DeleteLanguage(string cultureName)
        {
            _languageRepo.Delete(cultureName);
            return ReturnJson(true);
        }

        public class EnglishToNativeTranslation
        {
            public string CultureName { get; set; }
            public string TranslationId { get; set; }
            public string EnglishText { get; set; }
            public string NativeText { get; set; }
        }

        [HttpGet]
        public ActionResult GetTranslations(string cultureName)
        {
            // Have to get both the native and English translations and then merge them based on translationId
            // All records must have an EnglishText to show the text to translate from
            var native = _languageTranslationRepo.GetAllTranslations(cultureName).ToDictionary(i => i.TranslationId);
            var english = _languageTranslationRepo.GetAllTranslations("en").ToDictionary(i => i.TranslationId);
            var result = new Dictionary<string, EnglishToNativeTranslation>();
            foreach(var nvp in native)
            {
                result.Add(nvp.Value.TranslationId, new EnglishToNativeTranslation() 
                {
                    CultureName = nvp.Value.CultureName,
                    TranslationId = nvp.Value.TranslationId,
                    NativeText = nvp.Value.NativeText
                });
            }
            // Merge in English, missing native records will be identifiable because the record will have a 'en' culturename
            foreach(var nvp in english)
            {
                EnglishToNativeTranslation etnt = null;
                if (!result.TryGetValue(nvp.Value.TranslationId, out etnt))
                {
                    etnt = new EnglishToNativeTranslation()
                    {
                        CultureName = nvp.Value.CultureName,            // This will be 'en'
                        TranslationId = nvp.Value.TranslationId,
                        NativeText = nvp.Value.NativeText
                    };
                    result.Add(nvp.Value.TranslationId, etnt);
                }
                etnt.EnglishText = nvp.Value.NativeText;
            }
            
            return ReturnJson(result.Values.ToList());
        }

        [HttpPost]
        public ActionResult SaveTranslation(string cultureName, string translationId, string nativeText)
        {
            var dto = _languageTranslationRepo.GetTranslation(cultureName, translationId);
            if (dto == null)
            {
                dto = new LanguageTranslationDTO();
                dto.__IsNew = true;
            }
            dto.CultureName = cultureName;
            dto.TranslationId = translationId;
            dto.NativeText = nativeText;
            
            _languageTranslationRepo.SaveTranslation(dto, false, false);
            return ReturnJson(true);
        }

        [HttpDelete]
        public ActionResult DeleteTranslation(string cultureName, string translationId)
        {
            _languageTranslationRepo.DeleteTranslation(cultureName, translationId);
            return ReturnJson(true);
        }

        private class TranslationWithArgs
        {
            public string EnglishText { get; set; }
            public Dictionary<int, string> Args { get; set; }
            public LanguageTranslationDTO DTO { get; set; }
        }

        [HttpPost]
        public async Task<ActionResult> AutoTranslate(string cultureName)
        {
            var native = _languageTranslationRepo.GetAllTranslations(cultureName);
            var english = _languageTranslationRepo.GetAllTranslations("en");
            var todo = english.Except(native, new TranslationIdComparer());
            
            var arrayEnglish = new List<TranslationWithArgs>();

            foreach(var t in todo)
            {
                try
                {
                    // Since some text will contain {{ angular.model.stuff }} and also < html-element > - assumes the Translation engine supports HTML
                    // we dont want the translation to try and convert 'angular.model.stuff' which must remain constant (although its position in the 
                    // translation is important, so we replace {{ angular.model.stuff }} with <_arg_n/> where n starts 0 upto number of arguments, this relies on the engine supporting HTML
                    // and so they can be resubstitued after the translation
                    // ie "Hello <b>{{ name }}</b>, how old are you?" => "Hello <b><_arg_0/></b>, how old are you?"
                    var args = new Dictionary<int, string>();

                    // Handle any {{..}}
                    var translateText = t.NativeText.Argumentize("{{", "}}", (i, s) =>
                    {
                        var index = args.Count;
                        args[index] = "{{" + s + "}}";
                        return "<_arg_" + index.ToString() + "/>";
                    });

                    arrayEnglish.Add(new TranslationWithArgs() { EnglishText = translateText, Args = args, DTO = t });
                }
                catch
                {

                }
            }

            var arrayNative = await _translateService.TranslateAsync("en", arrayEnglish.Select(a => a.EnglishText).ToArray(), cultureName);
            var translated = new List<LanguageTranslationDTO>();
            for (int i = 0; i < arrayNative.Length; i++)
            {
                var dto = arrayEnglish[i].DTO;

                // Put the {{ }} and < > back ..
                // ie "Hello <b><_arg_0/></b>, how old are you?" => "Hello <b>{{ name }}</b>, how old are you?"
                
                // This regex supports both <_arg_0/> and <_arg_0> </_arg_0>
                Regex rex = new Regex(@"<_arg_(?<n>\d*)(\/>|>(\s*<\/_arg_(\k<n>)>))");
                var matches = rex.Match(arrayNative[i]);
                dto.NativeText = rex.Replace(arrayNative[i], m => 
                    {
                        int index = int.Parse(m.Groups["n"].Value);
                        return arrayEnglish[i].Args[index];
                    });

                dto.CultureName = cultureName;
                dto.__IsNew = true;
                translated.Add(dto);
            }

            _languageTranslationRepo.SaveTranslations(translated);
            return ReturnJson(translated.Count());
        }
    }
}