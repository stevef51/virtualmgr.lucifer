﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.BusinessLogic.Context;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Repository;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.www.Areas.Admin.Models;
using ConceptCave.Data.HelperClasses;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.Data.DTOConverters;

namespace ConceptCave.www.Areas.Admin.Controllers
{
    public class GetMembershipDesignerUIController : ControllerBase
    {
        private readonly IContextManagerFactory _ctxFac;
        private readonly ICompanyRepository _companyRepo;
        private readonly ITabletProfileRepository _tabletProfileRepo;
        private readonly IMembershipRepository _membershipRepo;
        private readonly ILanguageRepository _languageRepo;

        public GetMembershipDesignerUIController(IContextManagerFactory ctxFac, ICompanyRepository companyRepo, ITabletProfileRepository tabletProfileRepo, IMembershipRepository membershipRepo, ILanguageRepository languageRepo)
        {
            _ctxFac = ctxFac;
            _companyRepo = companyRepo;
            _tabletProfileRepo = tabletProfileRepo;
            _membershipRepo = membershipRepo;
            _languageRepo = languageRepo;
        }

        //
        // GET: /Admin/GetMembershipDesignerUI/

        public ActionResult Index(Guid id)
        {
            var entity = _membershipRepo.GetById(id, MembershipLoadInstructions.MembershipType | 
                MembershipLoadInstructions.MembershipRole | 
                MembershipLoadInstructions.MembershipLabel | 
                MembershipLoadInstructions.Label | 
                MembershipLoadInstructions.AspNetMembership | 
                MembershipLoadInstructions.ContextData | 
                MembershipLoadInstructions.RelatedMedia);

            var typeContexts =
                MembershipTypeRepository.GetByUserType(entity.UserTypeId, UserTypeContextLoadInstructions.PublishingGroupResource |
                UserTypeContextLoadInstructions.PublishingGroupResourceData);

            EntityCollection<UserTypeEntity> types = MembershipTypeRepository.GetAll();
            EntityCollection<AspNetRoleEntity> roles = RoleRepository.GetAll();

            var companies = _companyRepo.GetAll(CompanyLoadInstructions.None);
            var tabletProfiles = _tabletProfileRepo.GetAll();

            return View(new RenderMembershipDesignerUIModel(_ctxFac, companies, tabletProfiles, _languageRepo.AllLanguages()) { Entity = entity, TypeContexts = typeContexts, Types = types, Roles = roles, Labels = new www.Models.SelectMultipleLabelsModel(LabelRepository.GetAll(LabelFor.Users), (from m in entity.LabelUsers select m.Label.ToEntity())) });
        }

    }
}
