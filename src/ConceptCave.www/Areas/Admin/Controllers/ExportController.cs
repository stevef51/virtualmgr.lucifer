﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Repository;
using ConceptCave.Data.DatabaseSpecific;
using ConceptCave.Data.Linq;

namespace ConceptCave.www.Areas.Admin.Controllers
{
    [Authorize(Roles = RoleRepository.COREROLE_REPORTS)]
    public class ExportController : ControllerBase
    {
        //
        // GET: /Admin/Export/

        public ActionResult Index(DateTime? startDate, DateTime? endDate)
        {
            DateTime start = DateTime.Now.Date;
            DateTime end = start.AddDays(1);

            if (startDate.HasValue)
            {
                start = startDate.Value;
            }

            if (endDate.HasValue)
            {
                end = endDate.Value;
            }

            EntityCollection<WorkingDocumentEntity> results = null; //WorkingDocumentRepository.GetByDate(start, end, WorkingDocumentLoadInstructions.CompletedData, WorkingDocumentStatus.Completed);

            HttpResponseBase response = HttpContext.Response;
            response.ContentType = "text/csv";

            var fileName = "file.csv";

            response.Headers.Add("Content-Disposition",
                String.Format("attachment; filename={0}", fileName));

            return View(results);
        }
    }
}
