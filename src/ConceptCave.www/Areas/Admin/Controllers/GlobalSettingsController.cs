﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Transactions;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Repository;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.www.Areas.Designer.Controllers;
using ConceptCave.www.Areas.Admin.Models;
using ConceptCave.Configuration;
using ConceptCave.Data.HelperClasses;
using System.Dynamic;

namespace ConceptCave.www.Areas.Admin.Controllers
{
    [Authorize(Roles = RoleRepository.COREROLE_SETTINGS)]
    public class GlobalSettingsController : Controller
    {
        public static Action<GlobalSettingEntity> OnSettingChanged;
        public static Action<GlobalSettingEntity> OnSettingDeleted;

        //
        // GET: /Admin/GlobalSettings/

        public ActionResult Index()
        {
            return View(LoadSettings());
        }

        private static GlobalSettingsModel LoadSettings()
        {
            EntityCollection<PluginEntity> plugins = PluginRepository.GetAllPlugins(PluginLoadInstructions.None);

            var customSettings = GlobalSettingsRepository.GetCustomSettings();

            return new GlobalSettingsModel()
            {
                StoreLoginSite = GlobalSettingsRepository.GetSetting<bool>(GlobalSettingsConstants.SystemSetting_StoreLoginSite),
                StoreLoginCoordinates = GlobalSettingsRepository.GetSetting<bool>(GlobalSettingsConstants.SystemSetting_StoreLoginCoordinates),
                SchedulesEnabled = GlobalSettingsRepository.GetSetting<bool>(GlobalSettingsConstants.SystemSetting_ScheduledProcessEnabled),
                ScheduleProcessCount = GlobalSettingsRepository.GetSetting<int>(GlobalSettingsConstants.SystemSetting_ScheduledProcessThreadCount),
                CustomSettings = customSettings,
                ThemeMediaId = GlobalSettingsRepository.GetSetting<Guid?>(GlobalSettingsConstants.SystemSetting_ThemeMediaId),
                FavIconId = GlobalSettingsRepository.GetSetting<Guid?>(GlobalSettingsConstants.SystemSetting_FavIconId),
                Plugins = plugins
            };
        }

        public ActionResult Save(GlobalSettingsModel savemodel)
        {
            if (savemodel != null)
            {
                GlobalSettingsRepository.SetSetting(GlobalSettingsConstants.SystemSetting_StoreLoginSite, savemodel.StoreLoginSite);
                GlobalSettingsRepository.SetSetting(GlobalSettingsConstants.SystemSetting_StoreLoginCoordinates, savemodel.StoreLoginCoordinates);
                GlobalSettingsRepository.SetSetting(GlobalSettingsConstants.SystemSetting_ScheduledProcessEnabled, savemodel.SchedulesEnabled);
            }

            return View("Index", LoadSettings());
        }

        public ActionResult UploadTheme(HttpPostedFileBase fileUpload)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                var ThemeMediaId = GlobalSettingsRepository.GetSetting<Guid?>("ThemeMediaId");
                if (ThemeMediaId.HasValue)
                {
                    MediaRepository.Delete(ThemeMediaId.Value);
                }

                MediaEntity media = MediaRepository.CreateMediaFromPostedFile(new HttpPostedFileAdapter(fileUpload), "Theme", "Theme for this b-workflow installation");

                GlobalSettingsRepository.SetSetting("ThemeMediaId", media.Id);

                ButterflyEnvironment.Current.SetTheme(media.MediaData.Data);

                ConceptCave.www.Squisher.MakeBundles.RemoveCssBundle();
                ConceptCave.www.Squisher.MakeBundles.MakeCSSBundle();

                scope.Complete();
            }

            ViewBag.ActionTaken = ActionTaken.Save;

            return View("~/Areas/Admin/Views/GlobalSettings/Index.cshtml", LoadSettings());
        }

        public ActionResult UploadFavIcon(HttpPostedFileBase fileUpload)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                var FavIconId = GlobalSettingsRepository.GetSetting<Guid?>(GlobalSettingsConstants.SystemSetting_FavIconId);
                if (FavIconId.HasValue)
                {
                    MediaRepository.Delete(FavIconId.Value);
                }

                MediaEntity media = MediaRepository.CreateMediaFromPostedFile(new HttpPostedFileAdapter(fileUpload), "FavIcon", "FavIcon for this b-workflow installation");

                GlobalSettingsRepository.SetSetting(GlobalSettingsConstants.SystemSetting_FavIconId, media.Id);

                scope.Complete();
            }

            ViewBag.ActionTaken = ActionTaken.Save;

            return View("~/Areas/Admin/Views/GlobalSettings/Index.cshtml", LoadSettings());
        }

        [ValidateInput(false)]
        public JsonResult SaveCustomItem(int id, string name, string value)
        {
            GlobalSettingEntity entity = GlobalSettingsRepository.GetSettingEntity(name);

            var result = new Dictionary<string, object>();
            result["Success"] = false;
            result["Message"] = "";

            if (id == -1 && entity != null)
            {
                // since -1 indicates a new entry, then if we get anything back from the call above we can't move forward;
                result["Message"] = string.Format("An entry with {0} already exists, the new entry cannot be created", name);

                return Json(result);
            }
            else if (entity != null && id != entity.Id)
            {
                result["Message"] = string.Format("An entry with {0} already exists, the name of this entry cannot be changed. None of your changes have been applied.", name);

                return Json(result);
            }

            if (entity == null)
            {
                entity = new GlobalSettingEntity();
            }

            // ok all good if we've reached here
            entity.Name = name;
            entity.Value = value;

            GlobalSettingsRepository.Save(entity, true, false);

            if (OnSettingChanged != null)
                OnSettingChanged(entity);

            result["Success"] = true;
            result["Id"] = entity.Id;

            return Json(result);
        }

        public JsonResult RemoveCustomItem(int id)
        {
            GlobalSettingEntity entity = GlobalSettingsRepository.GetSettingEntity(id);

            GlobalSettingsRepository.Delete(id);

            if (OnSettingDeleted != null && entity != null)
                OnSettingDeleted(entity);

            return Json(new { Success = true });
        }
    }
}
