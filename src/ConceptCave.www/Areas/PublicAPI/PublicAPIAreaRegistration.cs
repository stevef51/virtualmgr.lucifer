﻿using System.Web.Mvc;

namespace ConceptCave.www.Areas.PublicAPI
{
    public class PublicAPIAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "PublicAPI";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "PublicAPI_default",
                "PublicAPI/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
