﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Text;
using System.IO;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Repository;
using ConceptCave.Checklist.RunTime;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.RepositoryInterfaces.Enums;

namespace ConceptCave.www.Areas.PublicAPI.Controllers
{
    public class PayPalController : ControllerBase
    {
        //
        // GET: /PublicAPI/PayPal/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult IPN()
        {
            // the rootid and presentable id are stored in the custom field in the format GUID;GUID
            string ids = Request["custom"];
            if (string.IsNullOrEmpty(ids) == true)
            {
                return View(); // not sure quite what to do here, shouldn't occurr, but review to make sure this makes sense
            }

            string[] parts = ids.Split(';');
            if (parts.Length != 2)
            {
                return View();
            }

            Guid rootId = Guid.Empty;
            if (Guid.TryParse(parts[0], out rootId) == false)
            {
                return View();
            }

            Guid presentableId = Guid.Empty;
            if (Guid.TryParse(parts[1], out presentableId) == false)
            {
                return View();
            }

            WorkingDocumentEntity entity = WorkingDocumentRepository.GetById(rootId, WorkingDocumentLoadInstructions.Data);
            WorkingDocument document = (WorkingDocument)WorkingDocument.CreateFromString(entity.WorkingDocumentData.Data);

            var presenteds = (from p in document.PresentedAsDepthFirstEnumerable() where p.Id == presentableId select p);

            if (presenteds.Count() == 0)
            {
                return View();
            }

            IPresentedQuestion presented = (IPresentedQuestion)presenteds.First();

            var formVals = new Dictionary<string, string>();
            formVals.Add("cmd", "_notify-validate");

            string response = GetPayPalResponse(formVals, ((IPayPalQuestion)presented.Question).UseSandbox);

            if (response.StartsWith("VERIFIED"))
            {
                PayPalAnswer answer = (PayPalAnswer)presented.GetAnswer();

                Request.InputStream.Seek(0, SeekOrigin.Begin);
                byte[] param = Request.BinaryRead(Request.ContentLength);
                string strRequest = Encoding.ASCII.GetString(param);

                answer.AddNotification(strRequest);
            
                WorkingDocumentRepository.Save(document, entity, true, true);
            }
            
            return View();
        }


        /// <summary>
        /// Utility method for handling PayPal Responses
        /// </summary>
        private string GetPayPalResponse(Dictionary<string, string> formVals, bool useSandbox)
        {
            string paypalUrl = useSandbox ? "https://www.sandbox.paypal.com/cgi-bin/webscr"
                : "https://www.paypal.com/cgi-bin/webscr";


            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(paypalUrl);

            // Set values for the request back
            req.Method = "POST";
            req.ContentType = "application/x-www-form-urlencoded";

            byte[] param = Request.BinaryRead(Request.ContentLength);
            string strRequest = Encoding.ASCII.GetString(param);

            StringBuilder sb = new StringBuilder();
            sb.Append(strRequest);

            foreach (string key in formVals.Keys)
            {
                sb.AppendFormat("&{0}={1}", key, formVals[key]);
            }
            strRequest += sb.ToString();
            req.ContentLength = strRequest.Length;

            //for proxy
            //WebProxy proxy = new WebProxy(new Uri("http://urlort#");
            //req.Proxy = proxy;
            //Send the request to PayPal and get the response
            string response = "";
            using (StreamWriter streamOut = new StreamWriter(req.GetRequestStream(), System.Text.Encoding.ASCII))
            {

                streamOut.Write(strRequest);
                streamOut.Close();
                using (StreamReader streamIn = new StreamReader(req.GetResponse().GetResponseStream()))
                {
                    response = streamIn.ReadToEnd();
                }
            }

            return response;
        }
    }
}
