﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.www.Areas.MySettings.Models;
using ConceptCave.Configuration;
using System.Web.Security;
using VirtualMgr.Membership;

namespace ConceptCave.www.Areas.MySettings.Controllers
{
    public class ConfigureController : ControllerBase
    {
        private readonly IMembership _membership;

        public ConfigureController(IMembership membership)
        {
            _membership = membership;
        }
        //
        // GET: /MySettings/Configure/

        public ActionResult Index()
        {
            return View(new ConfigureModel());
        }

        public JsonResult ChnagePassword(string newPassword1, string newPassword2)
        {
            var jsonResult = new Dictionary<string, object>();
            jsonResult["Success"] = false;
            jsonResult["Message"] = "";

            if (newPassword1 != newPassword2)
            {
                jsonResult["Message"] = "The passwords you have entered do not match.";
                return Json(jsonResult);
            }

            bool result = false;
            var member = _membership.GetUser();
            try
            {
                // EVS-415 Change Password Checklist To Support "Unlocking" Locked Users
                // When ever password is changed successfully we unlock the user
                if (member.IsLockedOut)
                    member.UnlockUser();

                result = member.ChangePassword(member.ResetPassword(), newPassword1);
            }
            catch (Exception e)
            {
                jsonResult["Message"] = string.Format("There was a problem changing your password. The minimum length for a password is {0}, please make sure the password you have entered is at least {0} characters in length.", _membership.MinRequiredPasswordLength);
            }

            jsonResult["Success"] = result;

            return Json(jsonResult);
        }
    }
}
