﻿using System.Web.Mvc;

namespace ConceptCave.www.Areas.MySettings
{
    public class MySettingsAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "MySettings";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "MySettings_default",
                "MySettings/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
