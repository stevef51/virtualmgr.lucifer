﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConceptCave.Checklist.RunTime;
using ConceptCave.Checklist.Interfaces;

namespace ConceptCave.www.Areas.Reader.Models
{
    public class WorkingDocumentModel
    {
        public string Name { get; set; }

        public IWorkingDocument Document { get; set; }

        public bool isAjax { get; set; }

        public List<WorkingDocumentModelReferenceMaterial> ReferenceMaterial { get; set; }

        public WorkingDocumentModel()
        {
            ReferenceMaterial = new List<WorkingDocumentModelReferenceMaterial>();
        }
    }

    public class WorkingDocumentModelReferenceMaterial
    {
        public string Name { get; set; }
        public string Url { get; set; }
    }
}