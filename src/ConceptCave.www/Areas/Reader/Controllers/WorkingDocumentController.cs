﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.Checklist.RunTime;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Repository;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.www.Areas.Reader.Models;

namespace ConceptCave.www.Areas.Reader.Controllers
{
    public class WorkingDocumentController : ControllerBase
    {
        //
        // GET: /Reader/WorkingDocument/

        public ActionResult Index(string id, bool? history, bool? isAjax)
        {
            bool ajax = false;

            if (isAjax.HasValue)
            {
                ajax = isAjax.Value;
            }
            WorkingDocumentEntity entity = null;

            Guid guid = Guid.Parse(id);
            entity = WorkingDocumentRepository.GetById(Guid.Parse(id), WorkingDocumentLoadInstructions.Data | WorkingDocumentLoadInstructions.References);

            IWorkingDocument document = WorkingDocument.CreateFromString(entity.WorkingDocumentData.Data);

            WorkingDocumentModel model = new WorkingDocumentModel() { Name = entity.Name, Document = document, isAjax = ajax };

            foreach (WorkingDocumentReferenceEntity reference in entity.WorkingDocumentReferences)
            {
                WorkingDocumentModelReferenceMaterial mat = new WorkingDocumentModelReferenceMaterial() { Name = reference.Name, Url = Url.Content(reference.Url) };
                model.ReferenceMaterial.Add(mat);
            }
            
            return View(model);
        }
    }
}
