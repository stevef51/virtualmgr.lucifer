﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Repository;
using ConceptCave.www.Areas.Organisation.Models;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.www.Areas.Organisation.Controllers
{
    [Authorize(Roles = RoleRepository.COREROLE_ORGANISATION_HIERARCHY)]
    public class HierarchyController : ControllerBase
    {
        protected IHierarchyRepository _hierarchyRepo;
        protected ITimesheetItemTypeRepository _timesheetTypeRepo;

        public HierarchyController(IHierarchyRepository hierarchyRepo, ITimesheetItemTypeRepository timesheetTypeRepo) 
        {
            _hierarchyRepo = hierarchyRepo;
            _timesheetTypeRepo = timesheetTypeRepo;
        }

        public ActionResult Roles()
        {
            return ReturnJson(from r in _hierarchyRepo.GetRoles() orderby r.Name select new { id = r.Id, name = r.Name, isperson = r.IsPerson });
        }

        public ActionResult Index(int? id)
        {
            //The hierarchy editor needs a list of all users and all roles
            ViewBag.Roles = HierarchyManager.GetAllRoles();

            ViewBag.TimesheetItemTypes = _timesheetTypeRepo.GetAll();

            IList<HierarchyDTO> hs = _hierarchyRepo.GetAllHierarchies();

            if(hs.Count == 0)
            {
                HierarchyDTO h = new HierarchyDTO();
                h.Name = "New Hierarchy";
                h.__IsNew = true;

                h = _hierarchyRepo.Save(h, true, false);

                hs = _hierarchyRepo.GetAllHierarchies();
            }

            ViewBag.Hierarchies = hs;

            int selectedId = 0;

            if (id.HasValue == false)
            {
                ViewBag.SelectedHierarchy = hs.First().Name;
                selectedId = hs.First().Id;
            }
            else
            {
                ViewBag.SelectedHierarchy = (from h in hs where h.Id == id.Value select h).First().Name;
                selectedId = (from h in hs where h.Id == id.Value select h).First().Id;
            }

            ViewBag.SelectedHierarchyId = selectedId;

            //fetch the current hierarchy wholesale out of the database
            var hroot = HierarchyManager.LoadInMemory(selectedId);
            //Convert this into something more easily JSON'able
            HierarchyViewModel viewModel = null;
            if (hroot != null)
            {
                viewModel = new HierarchyViewModel() { hierarchyId = selectedId };
                Action<HierarchyBucketEntity, HierarchyViewModel> adder = null;
                //recursively traverse inmemory tree to make viewmodel
                adder = (entity, vm) => {
                    //add self to vm
                    vm.uniqueId = entity.Id;
                    vm.id = entity.RoleId;
                    vm.name = string.IsNullOrEmpty(entity.Name) == false ? entity.Name : entity.HierarchyRole.Name;
                    if (entity.UserId.HasValue)
                    {
                        vm.user = new HierarchyUserViewModel();
                        vm.user.id = entity.UserData.UserId;
                        vm.user.name = entity.UserData.Name;
                    }
                    vm.isPrimary = entity.IsPrimary;

                    vm.defaultWeekdayTimesheetItemTypeId = entity.DefaultWeekDayTimesheetItemTypeId;
                    vm.defaultSaturdayTimesheetItemTypeId = entity.DefaultSaturdayTimesheetItemTypeId;
                    vm.defaultSundayTimesheetItemTypeId = entity.DefaultSundayTimesheetItemTypeId;

                    //now do children
                    vm.children = new List<HierarchyViewModel>();
                    foreach (var c in entity.Children)
                    {
                        var newvm = new HierarchyViewModel() { hierarchyId = selectedId };
                        adder(c, newvm);
                        vm.children.Add(newvm);
                    }
                };
                adder(hroot, viewModel);
            }
            
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult NewHierarchy(string name)
        {
            HierarchyDTO h = new HierarchyDTO();
            h.Name = name;
            h.__IsNew = true;

            h = _hierarchyRepo.Save(h, true, false);

            return RedirectToAction("Index", new { id = h.Id });
        }

        [HttpPost]
        public ActionResult SaveHierarchy(int id, string name)
        {
            HierarchyDTO h = _hierarchyRepo.GetHierarchy(id);

            h.Name = name;

            _hierarchyRepo.Save(h, false, false);

            return RedirectToAction("Index", new { id = h.Id });
        }

        [HttpPost]
        public ActionResult Save(HierarchyViewModel vm)
        {
            //turn viewmodel into entity
            HierarchyBucketEntity newRoot = new HierarchyBucketEntity();

            if (vm.__IsNew == false)
            {
                newRoot = HierarchyManager.GetById(vm.uniqueId);
            }

            Action<HierarchyBucketEntity, HierarchyViewModel> adder = null;
            adder = (entity, vmnode) => {
                entity.RoleId = vmnode.id;
                entity.UserId = vmnode.user != null ? (Guid?)vmnode.user.id : null;
                entity.Name = vmnode.name;
                entity.IsNew = vmnode.__IsNew;
                entity.HierarchyId = vmnode.hierarchyId;

                entity.DefaultWeekDayTimesheetItemTypeId = vmnode.defaultWeekdayTimesheetItemTypeId;
                entity.DefaultSaturdayTimesheetItemTypeId = vmnode.defaultSaturdayTimesheetItemTypeId;
                entity.DefaultSundayTimesheetItemTypeId = vmnode.defaultSundayTimesheetItemTypeId;

                if (entity.IsNew == false)
                {
                    entity.Id = vmnode.uniqueId;
                }

                if (vmnode.children != null)
                {
                    foreach (var c in vmnode.children)
                    {
                        var newE = new HierarchyBucketEntity();

                        if (c.__IsNew == false)
                        {
                            newE = HierarchyManager.GetById(c.uniqueId);
                        }

                        newE.Parent = entity; //Set parent reference before handing off to fill out other details
                        adder(newE, c);
                    }
                }
                entity.IsPrimary = vmnode.isPrimary;
            };
            
            newRoot.Parent = null;

            adder(newRoot, vm);

            //Now label the numbers
            HierarchyManager.SetTreeNumbering(newRoot);
            //and save
            HierarchyManager.DumpAndSave(newRoot);
            
            return Content("Success");
        }

    }
}
