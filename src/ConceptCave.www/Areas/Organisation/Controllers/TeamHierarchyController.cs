﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Repository;
using ConceptCave.www.Areas.Organisation.Models;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.www.Areas.Admin.Models;
using System.Transactions;
using ConceptCave.Repository.ImportMapping;
using System.IO;
using ConceptCave.RepositoryInterfaces.Importing;

namespace ConceptCave.www.Areas.Organisation.Controllers
{
    [Authorize(Roles = RoleRepository.COREROLE_ORGANISATION_HIERARCHY)]
    public class TeamHierarchyController : ControllerBase
    {
        protected IHierarchyRepository _hierarchyRepo;
        protected IMembershipRepository _memberRepo;
        protected ITimesheetItemTypeRepository _timesheetTypeRepo;

        public TeamHierarchyController(IHierarchyRepository hierarchyRepo, IMembershipRepository memberRepo, ITimesheetItemTypeRepository timesheetTypeRepo) 
        {
            _hierarchyRepo = hierarchyRepo;
            _memberRepo = memberRepo;
            _timesheetTypeRepo = timesheetTypeRepo;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult TeamHierarchy(int id)
        {
            return ReturnJson(LoadTeamHierarchyModel(id));
        }

        [HttpGet]
        public ActionResult Chart(int id)
        {
            return ReturnJson(GetBucketModel(id));
        }

        [HttpGet]
        public ActionResult TimesheetItemTypes()
        {
            return ReturnJson(TimesheetItemTypeModel.FromDto(_timesheetTypeRepo.GetAll()));
        }

        protected IList<TeamBucketModel> GetBucketModel(int id)
        {
            var buckets = _hierarchyRepo.GetBucketsForHierarchy(id, RepositoryInterfaces.Enums.HierarchyBucketLoadInstructions.BucketRole | 
                RepositoryInterfaces.Enums.HierarchyBucketLoadInstructions.User | 
                RepositoryInterfaces.Enums.HierarchyBucketLoadInstructions.BucketLabel);

            return TeamBucketModel.FromDto(buckets);
        }

        [HttpPost]
        public ActionResult TeamHierarchySave(TeamHierarchyModel record)
        {
            HierarchyDTO h = _hierarchyRepo.GetHierarchy(record.Id);

            if(h == null)
            {
                h = new HierarchyDTO()
                {
                    __IsNew = true
                };
            }

            h.Name = record.Name;

            if(record.Buckets == null)
            {
                record.Buckets = new List<TeamBucketModel>();
            }

            Dictionary<int, HierarchyBucketDTO> bucketCache = new Dictionary<int, HierarchyBucketDTO>();
            List<HierarchyBucketLabelDTO> newLbls = new List<HierarchyBucketLabelDTO>();
            List<HierarchyBucketLabelDTO> deleteLbls = new List<HierarchyBucketLabelDTO>();

            // we do 2 passes through the buckets, the first is to build up all the DTO's, the second will populate children
            // we do this in 2 passes just in case the order of the list sometimes gives children before parents.
            foreach(var bucket in record.Buckets)
            {
                if(bucket.Labels == null)
                {
                    bucket.Labels = new Guid[] { };
                }
                // we use a -ve id for new buckets on the client so that we can have child buckets refer to
                // their parents in the JS object model
                HierarchyBucketDTO b;
                if(bucket.Id < 0)
                {
                    b = new HierarchyBucketDTO()
                    {
                        Id = bucket.Id < 0 ? 0 : bucket.Id,
                        __IsNew = bucket.Id < 0,
                        HierarchyId = record.Id
                    };
                }
                else
                {
                    b = _hierarchyRepo.GetById(bucket.Id, RepositoryInterfaces.Enums.HierarchyBucketLoadInstructions.BucketLabel);
                }

                b.RoleId = bucket.RoleId;
                b.Name = bucket.Name;
                b.UserId = bucket.UserId;
                b.IsPrimary = bucket.IsPrimary;
                b.TasksCanBeClaimed = bucket.TasksCanBeClaimed;
                b.DefaultWeekDayTimesheetItemTypeId = bucket.DefaultWeekdayTimesheetItemTypeId;
                b.DefaultSaturdayTimesheetItemTypeId = bucket.DefaultSaturdayTimesheetItemTypeId;
                b.DefaultSundayTimesheetItemTypeId = bucket.DefaultSundayTimesheetItemTypeId;
                b.AssignTasksThroughLabels = bucket.AssignTasksThroughLabels;
                b.OpenSecurityThroughLabels = bucket.OpenSecurityThroughLabels;
                b.SortOrder = bucket.SortOrder;

                var labelIds = (from l in b.HierarchyBucketLabels select l.LabelId).ToList();
                var deletedLabels = (from l in b.HierarchyBucketLabels where bucket.Labels.Contains(l.LabelId) == false select l);
                var newLabels = (from l in bucket.Labels where (from t in labelIds select t).Contains(l) == false select l);

                deletedLabels.ToList().ForEach(l => deleteLbls.Add(l));
                newLabels.ToList().ForEach(l => newLbls.Add(new HierarchyBucketLabelDTO() { __IsNew = true, HierarchyBucketId = b.Id, LabelId = l }));

                // add to our cache using the client side id as parent references in the model will be using this
                bucketCache.Add(bucket.Id, b);
            }

            // 2nd pass to hook up DTO children
            HierarchyBucketDTO root = null;
            foreach(var bucket in record.Buckets)
            {
                var b = bucketCache[bucket.Id];
                if (bucket.Pid.HasValue == false)
                {
                    root = b;
                    continue;
                }

                var p = bucketCache[bucket.Pid.Value];

                p.Children.Add(b);
            }

            using (TransactionScope scope = new TransactionScope())
            {
                if(root != null)
                {
                    _hierarchyRepo.SetTreeNumbering(root);
                    _hierarchyRepo.DumpAndSave(root);

                    _hierarchyRepo.RemoveBucketLabels(deleteLbls.ToArray());
                    _hierarchyRepo.AddBucketLabels(newLbls.ToArray());
                }

                _hierarchyRepo.Save(h, true, false);
                scope.Complete();
            }

            var result = TeamHierarchyModel.FromDto(h);
            result.Buckets = GetBucketModel(result.Id);

            return (ReturnJson(result));
        }

        protected TeamHierarchyModel LoadTeamHierarchyModel(int id)
        {
            var th = _hierarchyRepo.GetHierarchy(id);

            return TeamHierarchyModel.FromDto(th);
        }

        [HttpGet]
        public ActionResult Search(string search, int? pageNumber, int? itemsPerPage)
        {
            int totalItems = 0;
            int page = pageNumber.HasValue ? pageNumber.Value : 1;
            int count = itemsPerPage.HasValue ? itemsPerPage.Value : 10;
            var items = _hierarchyRepo.Search("%" + search + "%", page, count, ref totalItems);

            return ReturnJson(new { count = totalItems, items = from i in items select TeamHierarchyModel.FromDto(i) });
        }

        protected List<FieldTransform> CreateTransforms()
        {
            return new HierarchyImportExportTransformFactory().Create();
        }

        [HttpGet]
        public FileResult Export(int hierarchyId)
        {
            var parser = new HierarchyExportParser(_hierarchyRepo, _memberRepo) { IncludeTitles = true };
            var writer = new ExcelWriter();
            var transforms = CreateTransforms();

            var buckets = _hierarchyRepo.GetBucketsForHierarchy(hierarchyId, RepositoryInterfaces.Enums.HierarchyBucketLoadInstructions.None);

            Action<int> progressCallback = counter =>
            {
                // not doing anything at the moment
            };

            // I know calling Import seems backwards, think of it as importing the data into Excel
            parser.Import(buckets, transforms, writer, false, progressCallback);

            Response.AddHeader("Content-Disposition", "inline; filename=HierarchyExport.xlsx");

            byte[] data;
            using (MemoryStream stream = new MemoryStream())
            {
                writer.Book.SaveAs(stream);

                data = stream.ToArray();
            }

            return this.File(data, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        }

        [HttpPost]
        public ActionResult Import(HttpPostedFileBase file)
        {
            HierarchyImportWriter writer = new HierarchyImportWriter(_hierarchyRepo, _memberRepo);

            byte[] data = new byte[file.ContentLength];
            file.InputStream.Read(data, 0, file.ContentLength);

            ImportParserResult result = null;

            Action<int> progressCallback = counter =>
            {
                // not doing anything at the moment
            };

            using (var stream = new MemoryStream(data))
            {
                ExcelImportParser parser = new ExcelImportParser();
                var transform = CreateTransforms();

                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(0, 10, 0)))
                {
                    result = parser.Import(stream, transform, writer, false, progressCallback);

                    try
                    {
                        if (result.Success)
                        {
                            scope.Complete();
                        }
                    }
                    catch (Exception e)
                    {
                        result.Errors.Add(string.Format("A problem was encountered committing the data to the database, the error message given was {0}", e.Message));
                    }
                }
            }

            return ReturnJson(result);
        }
    }
}
