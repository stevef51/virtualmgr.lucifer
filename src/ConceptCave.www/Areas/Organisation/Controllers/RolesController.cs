﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.HelperClasses;
using ConceptCave.www;
using ConceptCave.Repository;
using ConceptCave.www.Areas.Organisation.Models;
using ConceptCave.www.Areas.Designer.Controllers;

namespace ConceptCave.www.Areas.Organisation.Controllers
{
    [Authorize(Roles = RoleRepository.COREROLE_ORGANISATION_ROLES)]
    public class RolesController : ControllerBase
    {
        //
        // GET: /Organisation/Roles/

        public ActionResult Index()
        {
            var items = HierarchyManager.GetAllRoles();
            return View(items);
        }

        public ActionResult GetRoleDesignerUI(int id)
        {
            ViewBag.UserTypes = MembershipTypeRepository.GetAll();
            var role = HierarchyManager.GetRoleById(id);
            var model = new RoleViewModel()
            {
                Name = role.Name,
                Id = role.Id,
                IsPerson = role.IsPerson,
                UserTypes = role.HierarchyRoleUserTypes.Select(r => r.UserTypeId).ToList()
            };
            return View("RenderRoleDesignerUI", model);
        }

        public ActionResult GetRoleListItem(int id)
        {
            var role = HierarchyManager.GetRoleById(id);
            return View("RenderRoleListItem", role);
        }

        public ActionResult Save(RoleViewModel model)
        {
            var entity = new HierarchyRoleEntity(model.Id);
            entity.IsNew = false;
            entity.Name = model.Name;
            entity.IsPerson = model.IsPerson;
            HierarchyManager.SaveRole(entity);
            //save new set of user types too
            HierarchyManager.SetUserTypesForRole(model.Id, model.UserTypes);

            ViewBag.ActionTaken = ActionTaken.Save;
            return GetRoleDesignerUI(model.Id);
        }

        public ActionResult Add()
        {
            var entity = new HierarchyRoleEntity();
            entity.Name = "New Role";
            entity.IsPerson = true;
            HierarchyManager.SaveRole(entity);
            ViewBag.ActionTaken = ActionTaken.New;
            return GetRoleDesignerUI(entity.Id);
        }

        public ActionResult Remove(int id)
        {
            var ent = HierarchyManager.GetRoleById(id);
            var model = new RoleViewModel()
            {
                Id = ent.Id,
                Name = ent.Name,
                IsPerson = ent.IsPerson,
                UserTypes = ent.HierarchyRoleUserTypes.Select(r => r.UserTypeId).ToList()
            };
            HierarchyManager.DeleteRole(id);
            ViewBag.ActionTaken = ActionTaken.Remove;
            ViewBag.UserTypes = MembershipTypeRepository.GetAll();
            return View("RenderRoleDesignerUI", model);
        }
    }
}
