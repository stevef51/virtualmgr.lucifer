﻿using ConceptCave.DTO.DTOClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConceptCave.www.Areas.Organisation.Models
{
    //Lower case identifiers to match js convention once serialised to JSON
    public class TeamHierarchyModel
    {
        public bool __IsNew { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }

        public IList<TeamBucketModel> Buckets { get; set; }

        public static TeamHierarchyModel FromDto(HierarchyDTO dto)
        {
            return new TeamHierarchyModel() { __IsNew = dto.__IsNew, Id = dto.Id, Name = dto.Name };
        }
    }

    public class TimesheetItemTypeModel
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public static IList<TimesheetItemTypeModel> FromDto(IList<TimesheetItemTypeDTO> items)
        {
            List<TimesheetItemTypeModel> result = new List<TimesheetItemTypeModel>();

            items.ToList().ForEach(i => result.Add(new TimesheetItemTypeModel() { Id = i.Id, Name = i.Name }));

            return result;
        }
    }

    public class TeamBucketModel
    {
        public int Id { get; set; }
        public int? Pid { get; set; }

        public string Name { get; set; }

        public Guid? UserId { get; set; }

        public string UsersName { get; set; }

        public int RoleId { get; set; }

        public string RoleName { get; set; }

        public bool? IsPrimary { get; set; }

        public bool TasksCanBeClaimed { get; set; }
        public int? DefaultWeekdayTimesheetItemTypeId { get; set; }
        public int? DefaultSaturdayTimesheetItemTypeId { get; set; }
        public int? DefaultSundayTimesheetItemTypeId { get; set; }

        public Guid[] Labels { get; set; }

        public bool AssignTasksThroughLabels { get; set; }
        public bool OpenSecurityThroughLabels { get; set; }

        public int SortOrder { get; set; }

        public static TeamBucketModel FromDto(HierarchyBucketDTO dto)
        {
            var result = new TeamBucketModel()
            {
                Id = dto.Id,
                Pid = dto.ParentId,
                Name = dto.Name,
                UserId = dto.UserId,
                UsersName = dto.UserId.HasValue ? dto.UserData.Name : "",
                RoleId = dto.RoleId,
                RoleName = dto.HierarchyRole.Name,
                IsPrimary = dto.IsPrimary,
                TasksCanBeClaimed = dto.TasksCanBeClaimed,
                DefaultWeekdayTimesheetItemTypeId = dto.DefaultWeekDayTimesheetItemTypeId,
                DefaultSaturdayTimesheetItemTypeId = dto.DefaultSaturdayTimesheetItemTypeId,
                DefaultSundayTimesheetItemTypeId = dto.DefaultSundayTimesheetItemTypeId,
                Labels = new Guid[] { },
                AssignTasksThroughLabels = dto.AssignTasksThroughLabels,
                OpenSecurityThroughLabels = dto.OpenSecurityThroughLabels,
                SortOrder = dto.SortOrder
            };

            List<Guid> ls = new List<Guid>();

            foreach(var l in dto.HierarchyBucketLabels)
            {
                ls.Add(l.LabelId);
            }
            result.Labels = ls.ToArray();

            return result;
        }

        public static IList<TeamBucketModel> FromDto(IList<HierarchyBucketDTO> dtos)
        {
            List<TeamBucketModel> result = new List<TeamBucketModel>();
            dtos.ToList().ForEach(d => result.Add(FromDto(d)));

            return result;
        }
    }
}