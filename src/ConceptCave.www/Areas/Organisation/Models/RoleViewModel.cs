﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConceptCave.www.Areas.Organisation.Models
{
    public class RoleViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsPerson { get; set; }
        public List<int> UserTypes { get; set; }

        public RoleViewModel()
        {
            UserTypes = new List<int>();
        }
    }
}