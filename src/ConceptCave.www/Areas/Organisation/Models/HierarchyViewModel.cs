﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConceptCave.www.Areas.Organisation.Models
{
    //Lower case identifiers to match js convention once serialised to JSON
    public class HierarchyViewModel
    {
        public int uniqueId { get; set; }
        public bool __IsNew { get; set; }
        public int hierarchyId { get; set; }
        public int id { get; set; }
        public string name { get; set; }

        public int? defaultWeekdayTimesheetItemTypeId { get; set; }
        public int? defaultSaturdayTimesheetItemTypeId { get; set; }
        public int? defaultSundayTimesheetItemTypeId { get; set; }

        public HierarchyUserViewModel user { get; set; }
        public IList<HierarchyViewModel> children { get; set; }
        public bool? isPrimary { get; set; }
    }

    public class HierarchyUserViewModel
    {
        public Guid id { get; set; }
        public string name { get; set; }
    }
}