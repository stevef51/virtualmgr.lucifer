﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.BusinessLogic.LingoDataObjects;
using ConceptCave.BusinessLogic.Context;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.www.Areas.Designer.Controllers;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Repository;
using ConceptCave.Checklist.RunTime;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.www.Areas.Player.Models;
using ConceptCave.Repository.Facilities;
using ConceptCave.Checklist.Editor;
using System.Transactions;
using System.Text.RegularExpressions;
using System.IO;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.www.Areas.Player.Controllers
{
    public class UploadMediaQuestionController : ControllerBase
    {
        private readonly IMediaManager _mediaMgr;
        private readonly IContextManagerFactory _ctxFac;
        private readonly IMembershipRepository _membershipRepo;

        public UploadMediaQuestionController(IMediaManager mediaMgr, IContextManagerFactory ctxFac, IMembershipRepository membershipRepo)
        {
            _mediaMgr = mediaMgr;
            _ctxFac = ctxFac;
            _membershipRepo = membershipRepo;
        }

        public class CanvasDataUrlPostedFile : HttpPostedFileBase
        {
		    protected string _filename;
            protected string _contentType;
            protected Stream _stream;



            public CanvasDataUrlPostedFile(string filename, string data)
            {
                _filename = filename;
                _stream = CreateStream(data);

                string extension = System.IO.Path.GetExtension(_filename);
                var type = MimeTypeRepository.GetByExtension(extension);

                if (type != null)
                {
                    _contentType = type.MimeType;
                }
            }

            protected Stream CreateStream(string data)
            {
                var base64Data = Regex.Match(data, @"data:image/(?<type>.+?),(?<data>.+)").Groups["data"].Value;
                var binData = Convert.FromBase64String(base64Data);

                return new System.IO.MemoryStream(binData);
            }

            public override string FileName
            {
                get
                {
                    return _filename;
                }
            }

            public override int ContentLength
            {
                get
                {
                    return (int)_stream.Length;
                }
            }

            public override string ContentType
            {
                get
                {
                    return _contentType;
                }
            }

            public override System.IO.Stream InputStream
            {
                get
                {
                    return _stream;
                }
            }
        }

        public ActionResult GetItems(Guid rootId, Guid presentedId)
        {
            WorkingDocumentEntity entity = WorkingDocumentRepository.GetById(rootId, WorkingDocumentLoadInstructions.Data);

            WorkingDocument document = (WorkingDocument)WorkingDocument.CreateFromString(entity.WorkingDocumentData.Data);

            var presented = (from p in document.PresentedAsDepthFirstEnumerable() where p.Id == presentedId && p is IPresentedQuestion select p).Cast<IPresentedQuestion>();

            if (presented.Count() == 0)
            {
                // need to do something here
                throw new ArgumentException("Could not find presentable");
            }

            IUploadMediaAnswer mediaAnswer = (IUploadMediaAnswer)presented.First().GetAnswer();

            return PartialView("~/Views/Play/DisplayUploadMediaItems.cshtml", new UploadMediaAnswerModel() { Answer = (UploadMediaAnswer)mediaAnswer, RootId = rootId });
        }

        public ActionResult UploadMedia(HttpPostedFileBase fileUpload, string name, string description, string fileUploadScaledName, string fileUploadScaledData, Guid rootId, Guid presentedId, Guid questionId, PlayModelContextArea context, int? userTypeContextPublishedResourceId)
        {
            HttpPostedFileBase upload = fileUpload;
            if (string.IsNullOrEmpty(fileUploadScaledName) == false)
            {
                upload = new CanvasDataUrlPostedFile(fileUploadScaledName, fileUploadScaledData);
            }

            if (context == PlayModelContextArea.Normal)
            {
                return UploadMediaToWorkingDocument(upload, name, description, rootId, presentedId);
            }
            else
            {
                return UploadMediaToUserContext(upload, name, description, rootId, questionId, userTypeContextPublishedResourceId);
            }
        }

        private ActionResult UploadMediaToUserContext(HttpPostedFileBase fileUpload, string name, string description, Guid rootId, Guid questionId, int? userTypeContextPublishedResourceId)
        {
            WorkingDocument document = null;
            UserContextDataDTO userContext = null;
            PlayModelContext pc = null;

            // ok rootid is the user that the context is stored for and UserTypeContextPublishedResourceId is the type of context, we can use these
            // to load up what we need.

            // we have a little work to do, first up get the requirments for building a working document we can work with
            var typeContext = MembershipTypeContextPublishedResource.GetById(userTypeContextPublishedResourceId.Value, UserTypeContextLoadInstructions.PublishingGroupResource | UserTypeContextLoadInstructions.PublishingGroupResourceData | UserTypeContextLoadInstructions.Resource);
            userContext = _membershipRepo.GetUserContextData(rootId, userTypeContextPublishedResourceId.Value);

            Guid workingDocumentId = Guid.NewGuid();

            if (userContext != null)
            {
                workingDocumentId = userContext.CompletedWorkingDocumentId;
            }

            var contextManager = _ctxFac.GetInstance(rootId, ContextType.Membership, rootId);
            var contextGetter = contextManager.LoadContextDocument(userTypeContextPublishedResourceId.Value);

            if (userContext == null)
            {
                workingDocumentId = contextGetter.GetWorkingDocument(null).Id;
                userContext = new UserContextDataDTO() { __IsNew = true, UserId = rootId, UserTypeContextPublishedResourceId = userTypeContextPublishedResourceId.Value, CompletedWorkingDocumentId = workingDocumentId };
            }

            //set the answer on the upload media presentable
            var presented = contextGetter.GetPresentedQuestion(null, questionId);

            if (presented == null)
            {
                // need to do something here
                throw new ArgumentException("Could not find presentable");
            }

            var mediaAnswer = (IUploadMediaAnswer)presented.GetAnswer();

            using (TransactionScope scope = new TransactionScope())
            {
                var mediaId = _mediaMgr.SaveStream(fileUpload.InputStream, fileUpload.FileName, null, null,
                    contextGetter.GetWorkingDocument(null));

                UploadMediaAnswerItem answer = new UploadMediaAnswerItem(_mediaMgr)
                {
                    Filename = fileUpload.FileName,
                    ContentType = fileUpload.ContentType,
                    Name = name,
                    Description = description,
                    MediaId = mediaId
                };

                mediaAnswer.Items.Items.Add(answer);

                contextGetter.Save(); //commit user context data to reporting tables

                _membershipRepo.Save(userContext, true, true);

                scope.Complete();
            }

            pc = new PlayModelContext() { Area = PlayModelContextArea.MembershipDesigner };
            pc.Data.UserTypeContextPublishedResourceId = userTypeContextPublishedResourceId;

            ViewBag.ActionTaken = ActionTaken.Save;

            return PartialView("~/Views/Play/DisplayUploadMediaItems.cshtml", new UploadMediaAnswerModel() { Answer = (UploadMediaAnswer)mediaAnswer, RootId = rootId, Context = pc });
        }

        private ActionResult UploadMediaToWorkingDocument(HttpPostedFileBase fileUpload, string name, string description, Guid rootId, Guid presentedId)
        {
            WorkingDocument document = null;
            WorkingDocumentEntity entity = null;
            PlayModelContext pc = new PlayModelContext() { Area = PlayModelContextArea.Normal };

            entity = WorkingDocumentRepository.GetById(rootId, WorkingDocumentLoadInstructions.Data);

            document = (WorkingDocument) WorkingDocument.CreateFromString(entity.WorkingDocumentData.Data);

            var presented =
                (from p in document.PresentedAsDepthFirstEnumerable()
                    where p.Id == presentedId && p is IPresentedQuestion
                    select p).Cast<IPresentedQuestion>().ToList();

            if (!presented.Any())
            {
                // need to do something here
                throw new ArgumentException("Could not find presentable");
            }

            var mediaId = _mediaMgr.SaveStream(fileUpload.InputStream, fileUpload.FileName, fileUpload.ContentType, null, document);

            UploadMediaAnswerItem answer = new UploadMediaAnswerItem(_mediaMgr)
            {
                Filename = fileUpload.FileName,
                ContentType = fileUpload.ContentType,
                Name = name,
                Description = description,
                MediaId = mediaId
            };

            IUploadMediaAnswer mediaAnswer = (IUploadMediaAnswer)presented.First().GetAnswer();
            mediaAnswer.Items.Items.Add(answer);

            WorkingDocumentRepository.Save(document, entity, true, true);

            ViewBag.ActionTaken = ActionTaken.Save;

            return PartialView("~/Views/Play/DisplayUploadMediaItems.cshtml", new UploadMediaAnswerModel() { Answer = (UploadMediaAnswer)mediaAnswer, RootId = rootId, Context = pc, UserTypeContextName = document.DesignDocument.Name });
        }

        public ActionResult DeleteMedia(Guid rootId, Guid presentedId, Guid questionId, Guid mediaId, PlayModelContextArea context, int? userTypeContextPublishedResourceId)
        {
            WorkingDocument document = null;
            WorkingDocumentEntity entity = null;
            UserContextDataDTO userContext = null;
            PlayModelContext pc = null;

            if (context == PlayModelContextArea.Normal)
            {
                entity = WorkingDocumentRepository.GetById(rootId, WorkingDocumentLoadInstructions.Data);

                document = (WorkingDocument) WorkingDocument.CreateFromString(entity.WorkingDocumentData.Data);

                var presented =
                    (from p in document.PresentedAsDepthFirstEnumerable()
                        where p.Id == presentedId && p is IPresentedQuestion
                        select p).Cast<IPresentedQuestion>().ToList();

                if (!presented.Any())
                {
                    // need to do something here
                    throw new ArgumentException("Could not find presentable");
                }

                IUploadMediaAnswer mediaAnswer = (IUploadMediaAnswer)presented.First().GetAnswer();

                var item = (from a in mediaAnswer.Items.Items where a.MediaId == mediaId select a).DefaultIfEmpty(null).First();
                if (item == null)
                {
                    return PartialView("~/Views/Play/DisplayUploadMediaItems.cshtml", new UploadMediaAnswerModel() { Answer = (UploadMediaAnswer)mediaAnswer, RootId = rootId });
                }

                mediaAnswer.Items.Items.Remove(item);

                pc = new PlayModelContext() { Area = PlayModelContextArea.Normal };

                using (TransactionScope scope = new TransactionScope())
                {
                    MediaRepository.Delete(item.MediaId);
                    WorkingDocumentRepository.Save(document, entity, true, true);

                    scope.Complete();
                }

                ViewBag.ActionTaken = ActionTaken.Save;

                return PartialView("~/Views/Play/DisplayUploadMediaItems.cshtml", new UploadMediaAnswerModel() { Answer = (UploadMediaAnswer)mediaAnswer, RootId = rootId, Context = pc });
            }
            else
            {

                var contextManager = _ctxFac.GetInstance(rootId, ContextType.Membership, rootId);
                var contextGetter = contextManager.LoadContextDocument(userTypeContextPublishedResourceId.Value);
                var pquestion = contextGetter.GetPresentedQuestion(null, questionId);
                userContext = _membershipRepo.GetUserContextData(rootId, userTypeContextPublishedResourceId.Value);

                IUploadMediaAnswer mediaAnswer = (IUploadMediaAnswer)pquestion.GetAnswer();

                var item = (from a in mediaAnswer.Items.Items where a.MediaId == mediaId select a).DefaultIfEmpty(null).First();
                if (item == null || userContext == null)
                {
                    return PartialView("~/Views/Play/DisplayUploadMediaItems.cshtml", new UploadMediaAnswerModel() { Answer = (UploadMediaAnswer)mediaAnswer, RootId = rootId });
                }

                mediaAnswer.Items.Items.Remove(item);

                using (TransactionScope scope = new TransactionScope())
                {
                    contextGetter.Save();
                    MediaRepository.Delete(item.MediaId);
                    _membershipRepo.Save(userContext, true, true);

                    scope.Complete();
                }

                pc = new PlayModelContext() { Area = PlayModelContextArea.MembershipDesigner };
                pc.Data.UserTypeContextPublishedResourceId = userTypeContextPublishedResourceId;

                ViewBag.ActionTaken = ActionTaken.Save;

                return PartialView("~/Views/Play/DisplayUploadMediaItems.cshtml", new UploadMediaAnswerModel() { Answer = (UploadMediaAnswer)mediaAnswer, RootId = rootId, Context = pc });
            }
        }

        public ActionResult IOSContinue(string id)
        {
            return View("~/Views/Play/DisplayUploadMediaItems.cshtml", new UploadMediaAnswerModel() { IsIOSContinue = true, IOSModalId = id });
        }
    }
}
