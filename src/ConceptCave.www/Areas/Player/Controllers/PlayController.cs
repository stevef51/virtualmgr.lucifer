﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.BusinessLogic.LingoDataObjects;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Repository;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.www.Areas.Player.Models;
using ConceptCave.Checklist.RunTime;
using ConceptCave.Data.HelperClasses;
using System.Transactions;
using ConceptCave.Repository.Facilities;
using System.Dynamic;
using ConceptCave.www.Services;
using ConceptCave.Checklist.Facilities;
using ConceptCave.Checklist.Commands.Runtime;

namespace ConceptCave.www.Areas.Player.Controllers
{
    public class PlayController : ControllerBase
    {
        public ActionResult Index()
        {
            //very simple, return the angular app
            return View();
        }

    }
}
