﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Helpers;
using ConceptCave.BusinessLogic.Configuration;
using ConceptCave.BusinessLogic.Questions;
using ConceptCave.Core;
using ConceptCave.Checklist.RunTime;
using ConceptCave.Checklist.Interfaces;
using System.Globalization;
using ConceptCave.Checklist.Editor;
using ConceptCave.Repository;
using ConceptCave.Repository.Questions;
using ConceptCave.www.Models;
using RepositorySectionManager = ConceptCave.Repository.RepositorySectionManager;
using VirtualMgr.Central;

namespace ConceptCave.www.Areas.Player.Models
{
    public class AnswerModelBinder : DefaultModelBinder
    {
        protected override object CreateModel(ControllerContext controllerContext, ModelBindingContext bindingContext, Type modelType)
        {
            var answermodelString = bindingContext.ValueProvider.GetValue("answermodel").AttemptedValue;

            AnswerModel model = Json.Decode<AnswerModel>(answermodelString);


            return model;
        }
    }

    public interface IAnswerModelAction
    {
        JSONDataModelItem Action(JSONDataModelItem item, IUniqueNode node);
    }

    public class AnswerModel : JSONDataModel
    {
        public bool CanPrevious { get; set; }
        public bool CanNext { get; set; }

        public void PopulateAnswersFromQuestions(List<WorkingDocument> documents)
        {
            Items.ForEach(i =>
            {
                Guid id = Guid.Parse(i.Id);

                IEnumerable<IPresented> node = null;
                foreach (var doc in documents)
                {
                    node = (from p in doc.PresentedAsDepthFirstEnumerable() where p is IPresentedQuestion && ((IPresentedQuestion)p).Question.Id == id select p);

                    if (node.Count() > 0)
                    {
                        break;
                    }
                }

                ConvertAnswerModelItemToPresentable(i, node);
            });
        }

        public void PopulateAnswers(List<WorkingDocument> documents)
        {
            Items.ForEach(i =>
                {
                    Guid id = Guid.Parse(i.Id);

                    IEnumerable<IPresented> node = null;
                    foreach (var doc in documents)
                    {
                        node = (from p in doc.PresentedAsDepthFirstEnumerable() where p.Id == id select p);

                        if (node.Count() > 0)
                        {
                            break;
                        }
                    }

                    ConvertAnswerModelItemToPresentable(i, node);
                });
        }

        public void PopulateAnswers(WorkingDocument document)
        {
            Items.ForEach(i =>
            {
                Guid id = Guid.Parse(i.Id);
                var node = (from p in document.PresentedAsDepthFirstEnumerable() where p.Id == id select p);

                ConvertAnswerModelItemToPresentable(i, node);
            });
        }

        private static void ConvertAnswerModelItemToPresentable(JSONDataModelItem jsonItem, IEnumerable<IPresented> node)
        {
            if (node == null || node.Count() == 0)
            {
                // not sure why this would happen, we can't find the presentable in the document
                return;
            }

            IRepositorySectionManagerItem item = RepositorySectionManager.Current.ItemForObject(node.First().Presentable);

            if (item.HasPlayerModel == false)
            {
                return;
            }

            IJSONDataModelItemConverter converter = (IJSONDataModelItemConverter)item.CreatePlayerModel();

            converter.Convert(jsonItem, node.First());
        }

        public AnswerModel Action(WorkingDocument document)
        {
            AnswerModel result = new AnswerModel() { RootId = document.Id.ToString() };

            Items.ForEach(i =>
            {
                Guid id = Guid.Parse(i.Id);
                var node = (from p in document.PresentedAsDepthFirstEnumerable() where p.Id == id select p);

                if (node.Count() == 0)
                {
                    // not sure why this would happen, we can't find the presentable in the document
                    return;
                }

                IRepositorySectionManagerItem item = RepositorySectionManager.Current.ItemForObject(node.First().Presentable);

                if (item.HasPlayerModel == false)
                {
                    return;
                }

                object model = item.CreatePlayerModel();

                if ((model is IAnswerModelAction) == false)
                {
                    // this shouldn't happen, but if it does get out gracefully
                    return;
                }

                JSONDataModelItem resultItem = ((IAnswerModelAction) model).Action(i, node.First());

                if (item != null)
                {
                    result.Items.Add(resultItem);
                }
            });

            return result;
        }
    }

    public class FreeTextQuestionAnswerModelItemConverter : IJSONDataModelItemConverter
    {
        public static string AnswerPropertyName = "answer";

        public void Convert(JSONDataModelItem item, IUniqueNode node)
        {
            PresentedQuestion presented = (PresentedQuestion)node;

            var value = (from p in item.Properties where p.Name == AnswerPropertyName select p);

            if (value.Count() == 0)
            {
                // that's strange, lets just get out of here
                return;
            }

            FreeTextAnswer answer = (FreeTextAnswer)presented.GetAnswer();
            answer.AnswerText = value.First().Value;
        }
    }

    public class NumberQuestionAnswerModelItemConverter : IJSONDataModelItemConverter
    {
        public static string AnswerPropertyName = "answer";

        public void Convert(JSONDataModelItem item, IUniqueNode node)
        {
            PresentedQuestion presented = (PresentedQuestion)node;

            var value = (from p in item.Properties where p.Name == AnswerPropertyName select p);

            if (value.Count() == 0)
            {
                // that's strange, lets just get out of here
                return;
            }

            NumberAnswer answer = (NumberAnswer)presented.GetAnswer();

            decimal v = 0;
            if (decimal.TryParse(value.First().Value, out v) == true)
            {
                answer.AnswerNumber = v;
            }
            else
            {
                answer.AnswerValue = null;
            }
        }
    }

    public class CheckboxQuestionAnswerModelItemConverter : IJSONDataModelItemConverter
    {
        public static string AnswerPropertyName = "answer";

        public void Convert(JSONDataModelItem item, IUniqueNode node)
        {
            PresentedQuestion presented = (PresentedQuestion)node;

            var value = (from p in item.Properties where p.Name == AnswerPropertyName select p);

            if (value.Count() == 0)
            {
                // that's strange, lets just get out of here
                return;
            }

            CheckboxAnswer answer = (CheckboxAnswer)presented.GetAnswer();

            bool v = false;
            if (bool.TryParse(value.First().Value, out v) == true)
            {
                answer.AnswerBool = v;
            }
            else
            {
                answer.AnswerBool = null;
            }
        }
    }

    public class MultiChoiceQuestionAnswerModelItemConverter : IJSONDataModelItemConverter
    {
        public static string AnswerPropertyName = "answer";

        public void Convert(JSONDataModelItem item, IUniqueNode node)
        {
            PresentedQuestion presented = (PresentedQuestion)node;

            var value = (from p in item.Properties where p.Name == AnswerPropertyName select p);

            if (value.Count() == 0)
            {
                // that's strange, lets just get out of here
                return;
            }

            MultiChoiceAnswer answer = (MultiChoiceAnswer)presented.GetAnswer();


            string val = value.First().Value;

            if(string.IsNullOrEmpty(val))
            {
                return;
            }

            ListOf<IMultiChoiceItem> items = new ListOf<IMultiChoiceItem>();

            var q = (IMultiChoiceQuestion)presented.Question;

            foreach (string index in val.Split(','))
            {
                var id = Guid.Parse(index);

                items.Add((from a in q.List.Items where a.Id == id select a).First());
            }

            answer.ChosenItems = items;
        }
    }

    public class DateTimeQuestionAnswerModelItemConverter : IJSONDataModelItemConverter
    {
        public static string AnswerPropertyName = "answer";

        public void Convert(JSONDataModelItem item, IUniqueNode node)
        {
            PresentedQuestion presented = (PresentedQuestion)node;

            var value = (from p in item.Properties where p.Name == AnswerPropertyName select p);

            if (value.Count() == 0)
            {
                // that's strange, lets just get out of here
                return;
            }

            DateTimeAnswer answer = (DateTimeAnswer)presented.GetAnswer();

            string dateValue = value.First().Value;
            if (string.IsNullOrEmpty(dateValue))
            {
                answer.AnswerDateTime = null;
                return;
            }

            TimeZoneInfo zoneInfo = MultiTenantManager.CurrentTenant.TimeZoneInfo;

            answer.AnswerDateTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.Parse(dateValue, null, DateTimeStyles.AdjustToUniversal), zoneInfo);
        }
    }

    public class PresentMediaQuestionAnswerModelItemConverter : IJSONDataModelItemConverter
    {
        public static string AnswerPropertyName = "answer";

        public void Convert(JSONDataModelItem item, IUniqueNode node)
        {
            PresentedQuestion presented = (PresentedQuestion)node;

            var value = (from p in item.Properties where p.Name == AnswerPropertyName select p);

            if (value.Count() == 0)
            {
                // that's strange, lets just get out of here
                return;
            }

            PresentMediaAnswer answer = (PresentMediaAnswer)presented.GetAnswer();

            string position = value.First().Value;
            if (string.IsNullOrEmpty(position))
            {
                answer.Position = null;
                return;
            }

            answer.Position = Decimal.Parse(position);
        }
    }

    public class GeoLocationQuestionAnswerModelItemConverter : IJSONDataModelItemConverter
    {
        public static string LattitudeAnswerPropertyName = "lattitude";
        public static string LongitudeAnswerPropertyName = "longitude";
        public static string AccuracyAnswerPropertyName = "accuracy";
        public static string ErrorAnswerPropertyName = "error";

        public void  Convert(JSONDataModelItem item, IUniqueNode node)
        {
            PresentedQuestion presented = (PresentedQuestion)node;

            var lat = (from p in item.Properties where p.Name == LattitudeAnswerPropertyName select p);

            if (lat.Count() == 0)
            {
                ConvertError(item, node);
                return;
            }

            var lon = (from p in item.Properties where p.Name == LongitudeAnswerPropertyName select p);
            var acc = (from p in item.Properties where p.Name == AccuracyAnswerPropertyName select p);

            decimal lattitude = 0;
            decimal longitude = 0;
            decimal accuracy = 0;

            if (decimal.TryParse(lat.First().Value, out lattitude) == false)
            {
                ConvertError(item, node);
                return;
            }

            if (decimal.TryParse(lon.First().Value, out longitude) == false)
            {
                ConvertError(item, node);
                return;
            }

            if (decimal.TryParse(acc.First().Value, out accuracy) == false)
            {
                ConvertError(item, node);
                return;
            }

            GeoLocationAnswer answer = (GeoLocationAnswer)presented.GetAnswer();

            answer.AnswerValue = new LatLng(lattitude, longitude, accuracy);
        }

        protected void ConvertError(JSONDataModelItem item, IUniqueNode node)
        {
            PresentedQuestion presented = (PresentedQuestion)node;

            var err = (from p in item.Properties where p.Name == ErrorAnswerPropertyName select p);

            GeoLocationAnswer answer = (GeoLocationAnswer)presented.GetAnswer();

            if (err.Count() == 0)
            {
                answer.AnswerValue = new LatLng("Geocode was not gathered");
                return;
            }

            answer.AnswerValue = new LatLng(err.First().Value);
        }
    }

    public class PayPalQuestionAnswerModelItemConverter : IAnswerModelAction
    {
        public static string AnswerPropertyName = "answer";
        public static string ResultPropertyName = "reason";
        public static string ReasonPropertyName = "result";

        public JSONDataModelItem Action(JSONDataModelItem item, IUniqueNode node)
        {
            PresentedQuestion presented = (PresentedQuestion)node;

            var value = (from p in item.Properties where p.Name == AnswerPropertyName select p);

            if (value.Count() == 0)
            {
                // that's strange, lets just get out of here
                return CreateErrorResult(presented, "Could not find presentable");
            }

            PayPalAnswer answer = (PayPalAnswer)presented.GetAnswer();
            string statusValue = value.First().Value;
            if (string.IsNullOrEmpty(statusValue))
            {
                return CreateErrorResult(presented, "Status value cannot be null");
            }

            PayPalAnswerStatus status = PayPalAnswerStatus.Created;
            if (Enum.TryParse<PayPalAnswerStatus>(statusValue, out status) == false)
            {
                return CreateErrorResult(presented, "Status value was invalid");
            }

            answer.AnswerValue = status;

            JSONDataModelItem result = new JSONDataModelItem() { Id = presented.Id.ToString() };
            result.Properties.Add(new JSONDataModelItemProperty() { Name = ResultPropertyName, Value = "true" });

            return result;
        }

        private static JSONDataModelItem CreateErrorResult(PresentedQuestion presented, string reason)
        {
            JSONDataModelItem result = new JSONDataModelItem() { Id = presented.Id.ToString() };
            result.Properties.Add(new JSONDataModelItemProperty() { Name = ResultPropertyName, Value = "false" });
            result.Properties.Add(new JSONDataModelItemProperty() { Name = ReasonPropertyName, Value = reason });

            return result;
        }

        public void Convert(JSONDataModelItem item, IUniqueNode node)
        {
            PresentedQuestion presented = (PresentedQuestion)node;

            var value = (from p in item.Properties where p.Name == AnswerPropertyName select p);

            if (value.Count() == 0)
            {
                // that's strange, lets just get out of here
                return;
            }

            DateTimeAnswer answer = (DateTimeAnswer)presented.GetAnswer();

            string dateValue = value.First().Value;
            if (string.IsNullOrEmpty(dateValue))
            {
                return;
            }

            answer.AnswerDateTime = System.Xml.XmlConvert.ToDateTime(dateValue);
        }
    }

    public class SelectUserQuestionAnswerModelItemConverter : IJSONDataModelItemConverter
    {
        public static string AnswerPropertyName = "answer";

        public void Convert(JSONDataModelItem item, IUniqueNode node)
        {
            PresentedQuestion presented = (PresentedQuestion)node;

            var value = (from p in item.Properties where p.Name == AnswerPropertyName select p);

            if (value.Count() == 0)
            {
                // that's strange, lets just get out of here
                return;
            }

            SelectUserAnswer answer = (SelectUserAnswer)presented.GetAnswer();
            string val = value.First().Value;

            if (string.IsNullOrEmpty(val))
            {
                answer.AnswerValue = null;
                return;
            }

            answer.AnswerValue = Guid.Parse(value.First().Value);
        }
    }

    public class SelectODataQuestionAnswerModelItemConverter : IJSONDataModelItemConverter
    {
        public static string AnswerPropertyName = "answer";

        public void Convert(JSONDataModelItem item, IUniqueNode node)
        {
            PresentedQuestion presented = (PresentedQuestion)node;

            var value = (from p in item.Properties where p.Name == AnswerPropertyName select p);

            if (value.Count() == 0)
            {
                // that's strange, lets just get out of here
                return;
            }

            var answer = (SelectODataAnswer)presented.GetAnswer();
            string val = value.First().Value;

            if (string.IsNullOrEmpty(val))
            {
                answer.AnswerValue = null;
                return;
            }

            answer.AnswerValue = value.First().Value;
        }
    }

    public class SelectMediaQuestionAnswerModelItemConverter : IJSONDataModelItemConverter
    {
        public static string AnswerPropertyName = "answer";

        public void Convert(JSONDataModelItem item, IUniqueNode node)
        {
            PresentedQuestion presented = (PresentedQuestion)node;

            var value = (from p in item.Properties where p.Name == AnswerPropertyName select p);

            if (value.Count() == 0)
            {
                // that's strange, lets just get out of here
                return;
            }

            SelectMediaAnswer answer = (SelectMediaAnswer)presented.GetAnswer();
            string val = value.First().Value;

            if (string.IsNullOrEmpty(val))
            {
                answer.AnswerValue = null;
                return;
            }

            answer.AnswerValue = Guid.Parse(value.First().Value);
        }
    }

    public class SignatureQuestionAnswerModelItemConverter : IJSONDataModelItemConverter
    {
        public static string AnswerPropertyName = "answer";

        public void Convert(JSONDataModelItem item, IUniqueNode node)
        {
            PresentedQuestion presented = (PresentedQuestion)node;

            var value = (from p in item.Properties where p.Name == AnswerPropertyName select p);

            if (value.Count() == 0)
            {
                // that's strange, lets just get out of here
                return;
            }

            SignatureAnswer answer = (SignatureAnswer)presented.GetAnswer();

            string a = value.First().Value;

            if (a == "[]")
            {
                a = null;
            }

            answer.AnswerAsJsonString = a;
        }
    }
}