﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.ComponentModel;

namespace ConceptCave.www.Areas.Player.Models
{
    public class NotesModel
    {
        public NotesModel()
        {
            Attachments = new List<NotesAttachmentModel>();
        }

        [HiddenInput(DisplayValue = false)] 
        public Guid RootId { get; set; }

        [HiddenInput(DisplayValue = false)] 
        public Guid PresentedId { get; set; }

        [DataType(DataType.MultilineText)]
        [DisplayName("Notes")]
        public string Text { get; set; }

        public List<NotesAttachmentModel> Attachments { get; set; }
    }

    public class NotesAttachmentModel
    {
        public string Name { get; set; }

        public Guid Id { get; set; }
    }
}