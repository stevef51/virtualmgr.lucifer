﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.RunTime;

namespace ConceptCave.www.Areas.Player.Models
{
    public class UploadMediaAnswerModel
    {
        public UploadMediaAnswer Answer { get; set; }
        public Guid RootId { get; set; }
        public PlayModelContext Context { get; set; }
        public string UserTypeContextName { get; set; }

        public bool IsIOSContinue { get; set; }
        public string IOSModalId { get; set; }
    }
}