﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.HelperClasses;

namespace ConceptCave.www.Areas.Player.Models
{
    public class SelectExistingModel
    {
        public UserDataEntity Reviewee { get; set; }
        public EntityCollection<WorkingDocumentEntity> Documents { get; set; }

        public int GroupId { get; set; }
        public int ResourceId { get; set; }
        public Guid? RevieweeId { get; set; }
    }
}