﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace ConceptCave.www.Areas.Player.Models
{
    /*
     * THis is the model that is used in the (post) send action of the MessagingQuestionControler
     * It is created from form data and contains things like who it's going to and the text
     * */
    public class SendMessageQuestionModel
    {
        public Guid Sender { get; set; }
        public IEnumerable<Guid> RecipientUsers { get; set; }
        public IEnumerable<Guid> RecipientLabels { get; set; }
        [DataType(DataType.MultilineText)]
        public string Text { get; set; }
        public bool RequiresAcknowledgement { get; set; }
        public Guid? InReplyTo { get; set; }
    }

    /*
     * This is the viewmodel for the single question view
     * It contains details about a message to be rendered
     * */
    public class SingleMessageViewModel
    {
        public string Sender { get; set; }
        public string Text { get; set; }
        public string AvatarUrl { get; set; }
        public Guid MessageInstanceId { get; set; }
        public Guid MessageId { get; set; }
        public DateTime SentAtUtc { get; set; }
        public bool IsUnread { get; set; }
    }

    public class ReplyViewModel
    {
        public SendMessageQuestionModel Reply;
        public SingleMessageViewModel Message;
    }
}