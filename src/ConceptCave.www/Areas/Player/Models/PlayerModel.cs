﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConceptCave.BusinessLogic.Configuration;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Repository;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.HelperClasses;
using System.IO;
using System.Dynamic;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.www.Models;
using RepositorySectionManager = ConceptCave.Repository.RepositorySectionManager;

namespace ConceptCave.www.Areas.Player.Models
{
    public enum PlayModelContextArea
    {
        Normal,
        MembershipDesigner
    }

    public class PlayModelContext
    {
        public PlayModelContextArea Area { get; set; }

        public dynamic Data { get; set; }

        public PlayModelContext()
        {
            Data = new ExpandoObject();
        }
    }

    public class PlayerModel
    {
        public PlayerModelItem Item { get; set; }

        public AnswerModel AnswerModel { get; set; }

        public string ChecklistName { get; set; }

        public RunMode RunMode { get; set; }

        public PlayModelContext Context { get; set; }

        public bool IsAjax { get; set; }

        public List<PlayerModelReferenceMaterial> ReferenceMaterials { get; set; }

        public ILingoProgram Program { get; set; }

        public Guid ReviewerId { get; set; }
        public Guid? RevieweeId { get; set; }

        public PlayerModel() 
        {
            Context = new PlayModelContext() { Area = PlayModelContextArea.Normal };
        }

        public PlayerModel(IPresented presented, IValidatorResult validationResult, string rootId, ILingoProgram program) : this()
        {
            Item = new PlayerModelItem(this, presented, validationResult, true);
            AnswerModel = new AnswerModel() { RootId = rootId, CanPrevious = program.WorkingDocument.CanPrevious(), CanNext = true };
            Program = program;
        }
    }

    public class PlayerModelItem
    {
        public PlayerModel PlayerModel { get; set; }

        public IPresented Presented { get; set; }

        public string PartialView { get; set; }

        public IAnswer Answer { get; set; }

        public bool ShowNoneAttachedValidationErrors { get; set; }

        public IEnumerable<IInvalidReason> InvalidReasons { get; set; }

        public List<PlayerModelItem> Children { get; set; }

        public Dictionary<string, object> Data { get; protected set; }

        public PlayerModelItem()
        {
            Children = new List<PlayerModelItem>();
            Data = new Dictionary<string, object>();
        }

        public PlayerModelItem(PlayerModel playerModel, IPresented presented, IValidatorResult validationResult, bool showNoneAttachedValidationErrors = false) : this()
        {
            PlayerModel = playerModel;

            Presented = presented;

            ShowNoneAttachedValidationErrors = showNoneAttachedValidationErrors;

            if (Presented != null)
            {
                IRepositorySectionManagerItem item = RepositorySectionManager.Current.ItemForObject(Presented.Presentable);

                PartialView = item.PlayerPartialView;

                InvalidReasons = new List<IInvalidReason>();

                if (presented is IPresentedQuestion)
                {
                    Answer = ((IPresentedQuestion)presented).GetAnswer();

                    if (Answer != null && validationResult != null)
                    {
                        InvalidReasons = (from v in validationResult.InvalidReasons where v.Answer == Answer select v);
                    }
                }
                else if (presented is IPresentedSection && showNoneAttachedValidationErrors == true)
                {
                    if (validationResult != null)
                    {
                        InvalidReasons = (from v in validationResult.InvalidReasons where v.Answer == null select v);
                    }
                }

                if (Presented is IPresentedSection)
                {
                    IPresentedSection presentedSection = (IPresentedSection)Presented;

                    presentedSection.Children.ForEach(c =>
                    {
                        Children.Add(new PlayerModelItem(playerModel, c, validationResult));
                    });
                }

                if (item.HasPlayerDataProvider == true)
                {
                    IPlayerModelItemDataProvider provider = (IPlayerModelItemDataProvider)item.CreatePlayerDataProvider();
                    provider.Execute(this);
                }
            }
        }
    }

    public class PlayerModelReferenceMaterial
    {
        public string Name { get; set; }
        public string Url { get; set; }
    }

    public interface IPlayerModelItemDataProvider
    {
        void Execute(PlayerModelItem item);
    }

    public class PresentWordDocumentQuestionDataProvider : IPlayerModelItemDataProvider
    {
        public void Execute(PlayerModelItem item)
        {
            if ((item.Presented.Presentable is ConceptCave.Checklist.Editor.PresentWordDocumentQuestion) == false)
            {
                return;
            }

            ConceptCave.Checklist.Editor.PresentWordDocumentQuestion question = (ConceptCave.Checklist.Editor.PresentWordDocumentQuestion)item.Presented.Presentable;

            if (question.PreviewType == WordDocumentPreviewType.None)
            {
                return;
            }

            MediaEntity media = MediaRepository.GetById(question.MediaId, MediaLoadInstruction.Data);

            using (MemoryStream ms = new MemoryStream(media.MediaData.Data))
            {
                Aspose.Words.Document doc = new Aspose.Words.Document(ms);

                item.Data.Add("Document", doc);
            }
        }
    }
}