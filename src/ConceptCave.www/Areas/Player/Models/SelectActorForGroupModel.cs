﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConceptCave.www.Areas.Player.Models
{
    public class SelectActorForGroupModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}