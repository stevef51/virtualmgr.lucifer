﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConceptCave.Checklist.Interfaces;

namespace ConceptCave.www.Areas.Player.Models
{
    public class FinishedModel
    {
        public string ChecklistName { get; set; }
        public bool Finished { get; set; }
    }
}