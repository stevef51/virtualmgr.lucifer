﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Xml;

namespace ConceptCave.www.Squisher
{
    public class SquisherSection : IConfigurationSectionHandler
    {
        public class FileMetaData
        {
            public string Filename { get; set; }
            public bool Preloaded { get; set; }
            public string[] DependsOn { get; set; }
            public FileMetaData(string filename, string dependsOn)
            {
                Filename = filename;
                var dependsOnList = new List<string>();
                if (dependsOn != null)
                {
                    foreach(var d in dependsOn.Split(','))
                    {
                        dependsOnList.Add(d.Trim());
                    }
                }
                DependsOn = dependsOnList.ToArray();
            }
        }

        private delegate string FileNameFunc(XmlElement el);

        private static object _lock = new object();
        private static SquisherSection _current = null;
        public static SquisherSection Current
        {
            get
            {
                if (_current == null)
                {
                    lock (_lock)
                    {
                        if (_current == null)
                        {
                            _current = (SquisherSection)ConfigurationManager.GetSection("conceptcave.squisher");
                        }
                    }
                }
                return _current;
            }
        }

        private readonly IList<string> _browserScriptNames = new List<string>();
        private readonly IList<string> _browserStyleNames = new List<string>();
        private readonly IList<string> _iosScriptNames = new List<string>();
        private readonly IList<FileMetaData> _iosScriptFiles = new List<FileMetaData>();
        private readonly IList<string> _iosStyleNames = new List<string>(); 
        private readonly IList<string> _browserTemplateNames = new List<string>();
        private readonly IList<string> _iosTemplateNames = new List<string>(); 

        public IList<string> BrowserScriptNames {get{return new ReadOnlyCollection<string>(_browserScriptNames);}}
        public IList<string> BrowserStyleNames { get { return new ReadOnlyCollection<string>(_browserStyleNames); } }
        public IList<string> IOSScriptNames { get { return new ReadOnlyCollection<string>(_iosScriptNames); } }
        public IList<FileMetaData> IOSScriptFiles { get { return new ReadOnlyCollection<FileMetaData>(_iosScriptFiles); } }
        public IList<string> IOSStyleNames { get { return new ReadOnlyCollection<string>(_iosStyleNames); } }
        public IList<string> BrowserTemplateNames { get {return new ReadOnlyCollection<string>(_browserTemplateNames);} }
        public IList<string> IOSTemplateNames { get { return new ReadOnlyCollection<string>(_iosTemplateNames); } } 
        public XmlElement ConfigNode { get; private set; }

        public object Create(object parent, object configContext, XmlNode section)
        {
            ConfigNode = (XmlElement) section;

            //Two subnodes: scripts and styles
            var scriptEl = section.ChildNodes.OfType<XmlElement>().Single(el => el.Name.ToLower() == "scripts");
            var styleEl = section.ChildNodes.OfType<XmlElement>().Single(el => el.Name.ToLower() == "styles");
            var templateEl = section.ChildNodes.OfType<XmlElement>().Single(el => el.Name.ToLower() == "templates");

            FileNameFunc browserDelegate = el => el.Attributes["file"].Value;
            FileNameFunc iosDelegate = el => (el.Attributes["ios-skip"] == null ? ((el.Attributes["ios-file"] ?? el.Attributes["file"]).Value) : null);

            FillList(scriptEl, _browserScriptNames, browserDelegate);
            FillList(scriptEl, _iosScriptNames, iosDelegate);
            FillList(scriptEl, _iosScriptFiles, iosDelegate);
            FillList(styleEl, _browserStyleNames, browserDelegate);
            FillList(styleEl, _iosStyleNames, iosDelegate);
            FillList(templateEl, _browserTemplateNames, browserDelegate);
            FillList(templateEl, _iosTemplateNames, iosDelegate);
            return this;
        }

        private void FillList(XmlElement el, IList<string> fillTarget, FileNameFunc nameF)
        {
            foreach (
                var objname in
                    el.ChildNodes.OfType<XmlElement>()
                        .Where(e => e.Name.ToLower() == "add")
                        .Select(e => nameF(e)))
            {
                if (objname != null)
                {
                    fillTarget.Add(objname);
                }
            }
        }
        private void FillList(XmlElement el, IList<FileMetaData> fillTarget, FileNameFunc nameF)
        {
            foreach (
                var childEl in
                    el.ChildNodes.OfType<XmlElement>()
                        .Where(e => e.Name.ToLower() == "add"))
            {
                var objname = nameF(childEl);
                if (objname != null)
                {
                    var fmd = new FileMetaData(objname, childEl.Attributes["depends-on"] != null ? childEl.Attributes["depends-on"].Value : null);
                    if (childEl.Attributes["preloaded"] != null)
                    {
                        fmd.Preloaded = bool.Parse(childEl.Attributes["preloaded"].Value);
                    }
                    fillTarget.Add(fmd);
                }
            }
        }
    }
}