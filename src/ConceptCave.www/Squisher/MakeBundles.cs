﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Hosting;
using ConceptCave.Configuration;
using SquishIt.Framework;
using SquishIt.Framework.Base;
using SquishIt.Framework.JavaScript;
using SquishIt.Framework.Minifiers.JavaScript;
using VirtualMgr.Central;
using ConceptCave.www.ThemeVirtualProvider;

namespace ConceptCave.www.Squisher
{
    public static class MakeBundles
    {

        public static void MakeJSBundle()
        {
            var squishConfig = SquisherSection.Current;
            var jsBundleDebug = Bundle.JavaScript();
            var jsBundleRelease = Bundle.JavaScript();
            var jsBundleIOS = Bundle.JavaScript();

            
            //This is a bit complicated because we need to make two bundles
            //This is cos squishit doesn't use virtual paths to find files to add to the bundles
            //Thus, it will miss theme stylesheets/scripts if we don't add them via AddString instead
            //But we need to use squishit to make the debug bundle to save us having to write out all the script tags ourselves
            //This works fine because the virtual path gets properly resolved in this instance.
            AddVDirToBundle(jsBundleDebug, "~/Scripts/", squishConfig.BrowserScriptNames, false, "", "Scripts");
            AddVDirToBundle(jsBundleRelease, "~/Scripts/", squishConfig.BrowserScriptNames, false, "", "Scripts");
            AddVDirToBundle(jsBundleIOS, "~/Scripts/", squishConfig.IOSScriptNames, false, "", "Scripts");

            jsBundleDebug.ForceDebug().AsCached("jsbundle_debug", "~/API/v1/Assets/JsBundle/jsbundle_debug");
            jsBundleRelease.ForceRelease().AsCached("jsbundle", "~/Scripts/bundle.js.ashx");
            jsBundleIOS.ForceRelease().AsCached("jsbundle_ios", "~/API/v1/Assets/JsBundle/jsbundle_ios");
        }

        public static void MakeCSSBundle()
        {
            var squishConfig = SquisherSection.Current;
            var cssBundleDebug = Bundle.Css();
            var cssBundleRelease = Bundle.Css();
            var cssBundleIOS = Bundle.Css();

//            Bundle.ConfigureDefaults().UseNoCssMinification();

            var tenantContent = "~/Content/";

            AddVDirToBundle(cssBundleDebug, tenantContent, squishConfig.BrowserStyleNames, false, "", "Content");
            AddVDirToBundle(cssBundleRelease, tenantContent, squishConfig.BrowserStyleNames, false, "", "Content");
            AddVDirToBundle(cssBundleIOS, tenantContent, squishConfig.IOSStyleNames, false, "", "Content");

            cssBundleDebug.ForceDebug().AsCached(MultiTenantManager.MakeTenantId("cssbundle_debug"), "~/API/v1/Assets/CssBundle/cssbundle_debug");
            cssBundleRelease.ForceRelease().AsCached(MultiTenantManager.MakeTenantId("cssbundle"), "~/Content/bundle.css.ashx");
            cssBundleIOS.ForceRelease().AsCached(MultiTenantManager.MakeTenantId("cssbundle_ios"), "~/API/v1/Assets/CssBundle/cssbundle_ios");
        }

        public static void RemoveCssBundle()
        {
            HttpRuntime.Cache.Remove("squishit_css" + MultiTenantManager.MakeTenantId("cssbundle_debug"));
            HttpRuntime.Cache.Remove("squishit_css" + MultiTenantManager.MakeTenantId("cssbundle"));
            HttpRuntime.Cache.Remove("squishit_css" + MultiTenantManager.MakeTenantId("cssbundle_ios"));
        }

        private static void AddVDirToBundle<T>(BundleBase<T> bundle, string baseDir, IEnumerable<string> files, bool content, string extension, string arbitraryFolder) 
            where T : BundleBase<T>
        {
            foreach (var file in files)
            {
                var vpath = baseDir.TrimEnd('/') + '/' + file;
                if (content)
                {
                    using(var contentStream = HostingEnvironment.VirtualPathProvider.GetFile(vpath).Open())
                    {
                        var sr = new StreamReader(contentStream, Encoding.UTF8);
                        bundle.AddString(sr.ReadToEnd(), extension);//, arbitraryFolder);
                    }
                }
                else
                {
                    bundle.Add(vpath);
                }
            }
        }
    }
}