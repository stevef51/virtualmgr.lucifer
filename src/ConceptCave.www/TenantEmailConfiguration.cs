﻿using ConceptCave.Checklist.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using VirtualMgr.Central;

namespace ConceptCave.www
{
    public class TenantEmailConfiguration : IEmailConfiguration
    {
        public bool Enabled
        {
            get { return MultiTenantManager.CurrentTenant.Profile.EmailEnabled; }
        }

        public string FilterAddress(string address)
        {
            address = address.Replace(";", ",").Trim(',', ' ');

            var whiteListAddresses = MultiTenantManager.CurrentTenant.Profile.EmailWhitelist != null ? MultiTenantManager.CurrentTenant.Profile.EmailWhitelist.Split(',') : null;
            if (whiteListAddresses == null || whiteListAddresses.Length == 0)
                return address;

            var addresses = address.Split(',');
            var filtered = new List<string>();
            foreach(var a in addresses)
            {
                var saneAddress = a.ToLower().Trim();
                var ok = false;
                foreach (var wla in whiteListAddresses)
                {
                    var saneWla = wla.ToLower().Trim();
                    var rex = Regex.Escape( saneWla ).Replace( @"\*", ".*" ).Replace( @"\?", "." ).Replace(@"#", @"\d");
                    var reg = new Regex(rex);

                    if (reg.IsMatch(saneAddress))
                    {
                        ok = true;
                        break;
                    }
                }
                if (ok)
                    filtered.Add(a);
            }
            return string.Join(",", filtered);
        }


        public string SubjectAppend
        {
            get 
            { 
                if (!string.IsNullOrEmpty(MultiTenantManager.CurrentTenant.Profile.EmailWhitelist))
                {
                    return string.Format(" ({0} {1})", MultiTenantManager.CurrentTenant.HostName, MultiTenantManager.CurrentTenant.Profile.Name);
                }
                return null;
            }
        }

        public System.Net.Mail.MailAddress From
        {
            get 
            {
                var address = MultiTenantManager.CurrentTenant.EmailFromAddress;
                if (string.IsNullOrEmpty(address))
                {
                    return new System.Net.Mail.MailAddress("noreply@" + MultiTenantManager.CurrentTenant.HostName);
                }
                else
                {
                    return new System.Net.Mail.MailAddress(address);
                }
            }
        }
    }
}