﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Xml;

namespace ConceptCave.www
{
    public class DynamicAreaSection : IConfigurationSectionHandler
    {
        public object Create(object parent, object configContext, System.Xml.XmlNode section)
        {
            XmlNodeList nodes = section.SelectNodes("//add");

            DynamicAreaManger manager = new DynamicAreaManger();

            foreach (XmlElement element in nodes)
            {
                DynamicAreaItem item = new DynamicAreaItem(element);
                manager.Items.Add(item);
            }

            return manager;
        }
    }

    public class DynamicAreaManger
    {
        public List<DynamicAreaItem> Items { get; set; }

        public DynamicAreaManger()
        {
            Items = new List<DynamicAreaItem>();
        }

        public static DynamicAreaManger Current
        {
            get
            {
                return (DynamicAreaManger)System.Configuration.ConfigurationManager.GetSection("conceptcave.dynamicareas");
            }
        }

    }

    public class DynamicAreaItem
    {
        public string ControllerNamespace { get; set; }

        public DynamicAreaItem(XmlElement element)
        {
            ControllerNamespace = element.GetAttribute("controllerNamespace");
        }
    }
}