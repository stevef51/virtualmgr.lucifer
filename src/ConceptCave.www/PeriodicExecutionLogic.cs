﻿using ConceptCave.BusinessLogic.Models;
using ConceptCave.BusinessLogic.PeriodicExecution;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Repository.PeriodicExecution;
using ConceptCave.RepositoryInterfaces;
using ConceptCave.RepositoryInterfaces.PolicyManagement;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.SimpleExtensions;
using ConceptCave.www.App_Start;
using ConceptCave.www.Attributes;
using ConceptCave.www.Models;
using Newtonsoft.Json.Linq;
using Ninject;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Web.Http;
using VirtualMgr.Central;
using VirtualMgr.PeriodicExecution;

namespace ConceptCave.www.API.v1
{
    [WebApiCorsOptionsAllow]
    public class PeriodicExecutionLogic
    {
        public class ExecutionResults
        {
            public HandlerResult[] Results { get; set; }
            public TimeSpan TotalExecutionTime { get; set; }
        }

        public class HandlerResult
        {
            public string Name { get; set; }
            public IPeriodicExecutionHandlerResult Result { get; set; }
            public string Cron { get; set; }
            public DateTime? NextExecutionTimeUtc { get; set; }
            public TimeSpan ExecutionTime { get; set; }
        }

        private readonly IGlobalSettingsRepository _globalRepo;
        private readonly IPeriodicExecutionHistoryRepository _historyRepo;
        private readonly Func<Type, IPeriodicExecutionHandler> _fnCreateHandler;

        public PeriodicExecutionLogic(IGlobalSettingsRepository globalRepo, IPeriodicExecutionHistoryRepository historyRepo, Func<Type, IPeriodicExecutionHandler> fnCreateHandler)
        {
            _globalRepo = globalRepo;
            _historyRepo = historyRepo;
            _fnCreateHandler = fnCreateHandler;
        }

        public bool ForceFinished()
        {
            var historyDTO = _historyRepo.GetUnfinished();
            if (historyDTO != null)
            {
                historyDTO.FinishUtc = DateTime.UtcNow;
                historyDTO.ForceFinished = true;
                _historyRepo.Save(historyDTO, false, false);
            }
            return historyDTO != null;
        }

        public bool PerformExecute(string filter, ExecutionResults result, IContinuousProgress continuousProgress)
        {
            var historyDTO = _historyRepo.GetUnfinished();
            if (historyDTO != null)
            {
                // We are currently running, cannot run again ..
                result.Results = new HandlerResult[0];
                return true;
            }
            try
            {
                historyDTO = new PeriodicExecutionHistoryDTO() { __IsNew = true };
                historyDTO.StartUtc = DateTime.UtcNow;
                historyDTO = _historyRepo.Save(historyDTO, true, false);
                Stopwatch tsw = new Stopwatch();
                tsw.Start();

                var handlers = GetHandlers(filter);

                string configString = _globalRepo.GetSetting<string>("PeriodicExecution.Configuration");

                JObject config = new JObject();
                if (string.IsNullOrEmpty(configString) == false)
                {
                    config = JObject.Parse(configString);
                }

                List<HandlerResult> hs = new List<HandlerResult>();

                DateTime runTime = DateTime.UtcNow;
                bool ok = true;

                foreach (var handler in handlers)
                {
                    var r = new HandlerResult();
                    r.Name = handler.Name;

                    JObject handlerConfig = (JObject)config[r.Name];

                    if (handlerConfig != null)
                    {
                        if (handlerConfig["CRON"] != null)
                        {
                            r.Cron = handlerConfig["CRON"].ToString();
                            r.NextExecutionTimeUtc = runTime.AddSeconds(-1);
                        }

                        if (handlerConfig["NextExecutionTimeUTC"] != null)
                        {
                            DateTime date;
                            if (DateTime.TryParse(handlerConfig["NextExecutionTimeUTC"].ToString(), out date))
                            {
                                r.NextExecutionTimeUtc = date;
                            }
                        }
                    }

                    hs.Add(r);

                    if (string.IsNullOrEmpty(r.Cron) == false && r.NextExecutionTimeUtc > runTime)
                    {
                        // it's not due to run yet
                        continue;
                    }

                    Stopwatch sw = new Stopwatch();
                    sw.Start();
                    r.Result = handler.Execute(null, continuousProgress.NewCategory(r.Name));
                    sw.Stop();

                    r.ExecutionTime = sw.Elapsed;

                    if (string.IsNullOrEmpty(r.Cron) == false)
                    {
                        var expression = new Quartz.CronExpression(r.Cron);

                        var currentTz = MultiTenantManager.CurrentTenant.TimeZoneInfo;

                        expression.TimeZone = currentTz;

                        // GetTimeAfter expects a UTC time and returns a UTC time, it uses the above TimeZone only when
                        // the Cron expression requires a fixed Hour like "Every day at 4pm" (0 0 16 1/1 * ? *)
                        // for Cron expressions like "Every 10 mins" (0 0/10 * 1/1 * ? *) the TimeZone is not used
                        var nextRunTime = expression.GetTimeAfter(new DateTimeOffset(runTime));

                        r.NextExecutionTimeUtc = TimeZoneInfo.ConvertTimeToUtc(nextRunTime.Value.DateTime, currentTz);
                        handlerConfig["NextExecutionTimeUTC"] = r.NextExecutionTimeUtc;
                    }

                    if (r.Result.Success == false)
                    {
                        ok = false;
                    }
                    else if (r.Result.Issues != null && r.Result.Issues.Any())
                    {
                        // This is temporary, Issues are not Errors but at the moment we dont have another way to report them
                        ok = false;
                    }
                }

                _globalRepo.SetSetting("PeriodicExecution.Configuration", config.ToString());


                result.Results = hs.ToArray();
                tsw.Stop();
                result.TotalExecutionTime = tsw.Elapsed;

                return ok;
            }
            finally
            {
                historyDTO.FinishUtc = DateTime.UtcNow;
                _historyRepo.Save(historyDTO, false, false);
            }
        }

        public IEnumerable<IPeriodicExecutionHandler> GetHandlers(string filter)
        {
            List<IPeriodicExecutionHandler> result = new List<IPeriodicExecutionHandler>();

            result.Add(_fnCreateHandler(typeof(CustomJobPeriodicHandler)));

            if (MultiTenantManager.CurrentTenant.Profile.IsTenantMaster)
            {
                result.Add(_fnCreateHandler(typeof(TenantMasterPopulateTenantsDimensionPeriodicHandler)));
            }
            else
            {
                result.Add(_fnCreateHandler(typeof(StartOfDayPeriodicHandler)));
                result.Add(_fnCreateHandler(typeof(EndOfDayPeriodicHandler)));
                result.Add(_fnCreateHandler(typeof(RosterTimedEventsPeriodicHandler)));
                result.Add(_fnCreateHandler(typeof(CompleteWorkingDocumentPeriodicHandler)));
                result.Add(_fnCreateHandler(typeof(IncompleteWorkingDocumentPeriodicHandler)));
                result.Add(_fnCreateHandler(typeof(MediaPolicyPeriodicHandler)));
                result.Add(_fnCreateHandler(typeof(RolesCheckerPeriodicHandler)));

                result.Add(_fnCreateHandler(typeof(TenantPopulateDatesDimensionPeriodicHandler)));
                result.Add(_fnCreateHandler(typeof(TenantPopulateDateTimesDimensionPeriodicHandler)));
                result.Add(_fnCreateHandler(typeof(TenantPopulateCompaniesDimensionPeriodicHandler)));
                result.Add(_fnCreateHandler(typeof(TenantPopulateFacilitiesDimensionPeriodicHandler)));
                result.Add(_fnCreateHandler(typeof(TenantPopulateFacilityBuildingsDimensionPeriodicHandler)));
                result.Add(_fnCreateHandler(typeof(TenantPopulateBuildingFloorsDimensionPeriodicHandler)));
                result.Add(_fnCreateHandler(typeof(TenantPopulateFloorZonesDimensionPeriodicHandler)));
                result.Add(_fnCreateHandler(typeof(TenantPopulateZoneSitesDimensionPeriodicHandler)));
                result.Add(_fnCreateHandler(typeof(TenantPopulateSitesDimensionPeriodicHandler)));
                result.Add(_fnCreateHandler(typeof(TenantPopulateUsersDimensionPeriodicHandler)));
                result.Add(_fnCreateHandler(typeof(TenantPopulateRostersDimensionPeriodicHandler)));
                result.Add(_fnCreateHandler(typeof(TenantPopulateTaskCustomStatusDimensionPeriodicHandler)));
                result.Add(_fnCreateHandler(typeof(TenantPopulateTaskStatusDimensionPeriodicHandler)));
                result.Add(_fnCreateHandler(typeof(TenantPopulateTaskTypesDimensionPeriodicHandler)));
                result.Add(_fnCreateHandler(typeof(TenantPopulateTaskFactsPeriodicHandler)));

                if (MultiTenantManager.CurrentTenant.EnableDatawareHousePush)
                {
                    result.Add(_fnCreateHandler(typeof(TenantMasterPopulateDatesDimensionPeriodicHandler)));
                    result.Add(_fnCreateHandler(typeof(TenantMasterPopulateDateTimesDimensionPeriodicHandler)));
                    result.Add(_fnCreateHandler(typeof(TenantMasterPopulateCompaniesDimensionPeriodicHandler)));
                    result.Add(_fnCreateHandler(typeof(TenantMasterPopulateFacilitiesDimensionPeriodicHandler)));
                    result.Add(_fnCreateHandler(typeof(TenantMasterPopulateFacilityBuildingsDimensionPeriodicHandler)));
                    result.Add(_fnCreateHandler(typeof(TenantMasterPopulateBuildingFloorsDimensionPeriodicHandler)));
                    result.Add(_fnCreateHandler(typeof(TenantMasterPopulateFloorZonesDimensionPeriodicHandler)));
                    result.Add(_fnCreateHandler(typeof(TenantMasterPopulateZoneSitesDimensionPeriodicHandler)));
                    result.Add(_fnCreateHandler(typeof(TenantMasterPopulateSitesDimensionPeriodicHandler)));
                    result.Add(_fnCreateHandler(typeof(TenantMasterPopulateUsersDimensionPeriodicHandler)));
                    result.Add(_fnCreateHandler(typeof(TenantMasterPopulateRostersDimensionPeriodicHandler)));
                    result.Add(_fnCreateHandler(typeof(TenantMasterPopulateTaskCustomStatusDimensionPeriodicHandler)));
                    result.Add(_fnCreateHandler(typeof(TenantMasterPopulateTaskStatusDimensionPeriodicHandler)));
                    result.Add(_fnCreateHandler(typeof(TenantMasterPopulateTaskTypesDimensionPeriodicHandler)));
                    result.Add(_fnCreateHandler(typeof(TenantMasterPopulateTaskFactsPeriodicHandler)));
                }
                result.Add(_fnCreateHandler(typeof(AssetTypeCalendarPeriodicHandler)));
            }
            if (filter == null)
                return result;
            else
            {
                var filterArray = filter.Split(',');
                var regexArray = from f in filterArray select new Regex(f);

                return from r in result from f in regexArray where f.IsMatch(r.Name) select r;
            }
        }
    }
}