﻿$(function () {

    //turn any .wd-delete-button's into things that pop open a dlog, then delete
    $(document).on('click', '.wd-delete-button', function () {
        //wd id is stored in data-wdid
        var wdid = $(this).data('wdid');
        var rowtodel = $(this).data('eltodel');

        var dlog = $('<div class="modal hide fade"></div>');
        var dheader = $('<div class="modal-header"></div>');
        $(dheader).append(
            $('<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>'));
        $(dheader).append($('<h3>Delete Document</h3>'));
        var dbody = $('<div class="modal-body"></div>');
        $(dbody).append($('<p>Are you sure you want to delete this working document?</p>'));
        var dfooter = $('<div class="modal-footer"></div>');
        $(dfooter).append($('<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>'));
        var delbtn = $('<button class="btn btn-danger wd-really-delete-button">Delete</button>');
        $(dfooter).append(delbtn);

        $(dlog).append(dheader).append(dbody).append(dfooter);
        $(dlog).modal();

        var thiso = this;

        $(delbtn).click(function () {
            $.ajax({
                url: window.razordata.deleteurl,
                type: 'POST',
                data: {
                    workingDocumentId: wdid
                },
                success: function () {
                    $(dlog).modal('hide');
                    $(dlog).remove();
                    if (typeof (rowtodel) !== 'undefined')
                        $('#' + rowtodel).remove();
                    $(thiso).trigger('wddeleted');
                }
            });
        });
    });


});