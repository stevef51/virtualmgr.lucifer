﻿$(function () {

    //turn any .wd-delete-button's into things that pop open a dlog, then delete
    $(document).on('click', '.request-in-modal-button', function () {
        //URL that needs to get loaded is stored in either data-url or href (if its an a tag)
        var url = "";
        if ($(this).prop("tagName") == "A") {
            url = $(this).attr('href');
        }
        else {
            url = $(this).data('url');
        }

        var title = $(this).data('modal-title');
        if (title == undefined) {
            title = "Modal";
        }

        var dlog = $('<div class="modal hide fade"></div>');

        var style = $(this).data('modal-style');
        if (style) {
            dlog.css
        }

        var dheader = $('<div class="modal-header"></div>');
        $(dheader).append(
            $('<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>'));
        $(dheader).append($('<h3>' + title + '</h3>'));

        var dbody = $('<div class="modal-body" style="overflow-x:hidden;"></div>');

        var dfooter = $('<div class="modal-footer"></div>');
        $(dfooter).append($('<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>'));

        $(dlog).append(dheader).append(dbody).append(dfooter);
        $(dlog).on('hidden', function () {
            $(dlog).remove();
        });
        $(dlog).modal();

        $.ajax({
                url: url,
                type: 'GET',
                success: function(data) {
                    $(dlog).find('.modal-body').html(data);
            }
        });

        return false;
    });
});