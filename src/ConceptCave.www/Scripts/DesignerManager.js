﻿var CurrentDesignerManager = null;

function CreateDesignerManager(options) {
    CurrentDesignerManager = new DesignerManager();

    CurrentDesignerManager.rootUrl = options.rootUrl;
    CurrentDesignerManager.editorElement = options.editorElement;
    CurrentDesignerManager.createListItemCallBack = options.createListItemCallBack;
    CurrentDesignerManager.loadDesignerUrl = options.loadDesignerUrl;
    CurrentDesignerManager.loadListItemUrl = options.loadListItemUrl;

    if (options.listItemPrefix != undefined) {
        CurrentDesignerManager.listItemPrefix = options.listItemPrefix;
    }
}

function DesignerManager() {
    this.rootUrl = null;

    this.loadDesignerUrl = null;
    this.loadListItemUrl = null;

    this.listItemPrefix = '';

    this.createListItemCallBack = null;

    this.editorElement = null;
}

DesignerManager.prototype.fullUrl = function (url) {
    return this.rootUrl + url;
}

DesignerManager.prototype.angularise = function (jqElm, data) {
    if (angular.isUndefined(jqElm)) {
        return;
    }

    if (angular.isDefined(data) == false)
    {
        angular.element(document).injector().invoke(["$compile", function ($compile) {
            var scope = angular.element(jqElm).scope();
            $compile(jqElm)(scope);
        }]);
    }
    else {
        angular.element(document).injector().invoke(["$compile", function ($compile) {
            var scope = angular.element(jqElm).scope();
            var linkfn = $compile(data);
            var content = linkfn(scope);

            jqElm.html('');
            jqElm.append(content);

            scope.$apply();
        }]);
    }
}

DesignerManager.prototype.loadDesigner = function (id) {
    var self = this;

    $.ajax(this.fullUrl(this.loadDesignerUrl + id)).done(function (data) {
        self.angularise(self.editorElement, data);
    });
}

DesignerManager.prototype.loadListItem = function (id) {
    var self = this;
    var elementId = this.listItemPrefix + id;

    var element = $("#" + elementId);

    if (element.length == 0) // if we can't find it, lets call back against who created us to create a new one in the list for us as we've not seen it
    {
        this.createListItemCallBack(elementId, id);
        element = $("#" + elementId);
    }

    // load the element and angularise the result
    $.ajax(this.fullUrl(this.loadListItemUrl + id)).done(function (data) {
        self.angularise(element, data);
    });
}

DesignerManager.prototype.save = function (form) {
    var self = this;
    var options = {
        target: this.editorElement,
        success: function (responseText) {
            self.angularise($(self.editorElement), responseText);
        }
    }

    $(form).ajaxSubmit(options);
}

DesignerManager.prototype.newItem = function (form) {
    var self = this;
    var options = {
        target: this.editorElement,
        success: function (responseText) {
            self.angularise($(self.editorElement), responseText);
        }
    }


    $(form).ajaxSubmit(options);
}

DesignerManager.prototype.removeItem = function (form) {
    var self = this;
    var options = {
        target: this.editorElement,
        success: function (responseText) {
            self.angularise($(self.editorElement), responseText);
        }
    }


    $(form).ajaxSubmit(options);
}

DesignerManager.prototype.removeListItem = function (id) {
    var elementId = this.listItemPrefix + id;
    $("#" + elementId).remove();
}

// submitForm(form, $("angularise me"))
// Submit the form, take the HTML response and html() the "angularise me" selector, then angularise the result
//
// submitForm(form, function callback(responseText, fnAngularise) { ... fnAngularise($("angularise me")) }
// Submit the form, pass HTML response to callback, callback can call fnAngularise if required
DesignerManager.prototype.submitForm = function (form, jqHtmlElement, successCallBack) {
    var self = this;
    var options = {};
    var f = form;
    if (angular.isFunction(jqHtmlElement)) {
        successCallBack = jqHtmlElement;
        options.success = function (responseText) {
            // if the callback returns a JQuery element then angularise it
            successCallBack(responseText, self.angularise);
        }
    } else {
        options.success = function (responseText) {
            var jq = $(jqHtmlElement);
 //           jq.html(responseText);
            self.angularise(jq, responseText);
            if (angular.isFunction(successCallBack)) {
                successCallBack(responseText);
            }
        }
    }

    $(form).ajaxSubmit(options);
}