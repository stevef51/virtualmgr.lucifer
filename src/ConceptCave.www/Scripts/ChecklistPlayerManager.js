﻿(function ($) {
    var methods = {
        init: function (options) {
            var formFields = {
                nextModelField: options.nextModelField
            };
            $(this).data('fields', formFields);
            $(this).data('playerElement', options.playerElement);
            $(this).data('listeners', []);

            return this;
        },
        getPresentedElement: function(name) {
            return $(this).data('playerElement').find("[data-presentable-name='" + name + "']");
        },
        getItem: function (id) {
            var item = null;
            $.each($(this).data('model').Items, function (index, value) {
                if (value.Id == id) {
                    item = value;
                    return false;
                }
            });

            return item;
        },
        addItem: function (item) {
            $(this).data('model').Items.push(item);
        },
        addListener: function (listener) {
            if ($(this).data('locked') != undefined) {
                return;
            }
            $(this).data('listeners').push(listener);
        },
        setModel: function (model) {
            $(this).removeData('locked');

            $(this).data('model', model);

            $(this).trigger('modelchanged', model);
        },
        getModel: function () {
            return $(this).data('model');
        },
        action: function (data) {
            $(this).trigger('action', { callback: data.callback, data: JSON.stringify(data.data, null, 2) });
        },
        next: function () {
            var args = { cancel: false };

            $.each($(this).data('listeners'), function (index, value) {
                if (value.beforeNext == undefined) {
                    return;
                }

                value.beforeNext(args);
            });

            if (args.cancel == true) {
                return false;
            }

            $.each($(this).data('listeners'), function (index, value) {
                if (value.cleanup == undefined) {
                    return;
                }

                value.cleanup();
            });

            $(this).data('listeners').length = 0;
            $(this).data('locked', 'true');

            $($(this).data('fields').nextModelField).val(JSON.stringify($(this).data('model'), null, 2));

            return true;
        },
        back: function () {
            var args = { cancel: false };

            $.each($(this).data('listeners'), function (index, value) {
                if (value.beforeBack == undefined) {
                    return;
                }

                value.beforeBack(args);
            });

            if (args.cancel == true) {
                return false;
            }

            $(this).data('listeners').length = 0;
            $(this).data('locked', 'true');

            return true;
        },
        finishing: function () {
            $(this).trigger("finishing");
        },
        finished: function () {
            $(this).trigger("finished");
        }
    };

    $.fn.checklistplayer = function (method) {
        var args = arguments[0] || {}; // It's your object of arguments

        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.tooltip');
        }
    }
})(jQuery);