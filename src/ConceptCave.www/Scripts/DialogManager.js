﻿var CurrentDialogManager = null;

function CreateDialogManager(modalDialogElement, modalDialogContentElement, saveButtonElement, width) {
    CurrentDialogManager = new DialogManager
    CurrentDialogManager.modalDialog = modalDialogElement;
    CurrentDialogManager.modalDialogContent = modalDialogContentElement;
    CurrentDialogManager.saveButton = saveButtonElement;

    CurrentDialogManager.modalDialogId = $(modalDialogElement).attr('id');

    $(CurrentDialogManager.modalDialog).dialog({
        autoOpen: false,
        show: "slide",
        hide: "drop",
        modal: true,
        width: width == undefined ? 300 : width
    });
};

function DialogManager() {
    this.modalDialog = null;

    this.modalDialogId = null;

    this.modalDialogContent = null;
    this.saveButton = null;

    this.Context = null;
};