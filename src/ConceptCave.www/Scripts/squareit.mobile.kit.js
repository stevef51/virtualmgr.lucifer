﻿    window.SquareIT = new function ()
    {
        var self = this;

        this.WebViewProxy = new function ()
        {
            var _methodQueue = [];
            var _completionCallbacks = {};
            var _execIFrame;

            var DispatchMethod = function(methodName, args) {
                this.methodName = methodName;
                this.args = args;
                this.id = methodName + "@" + new Date().toString() + "#" + Math.random().toString();
                return this;
            };

            var createExecIframe = function() {
                var iframe = window.document.createElement("iframe");
                iframe.style.display = 'none';
                window.document.head.appendChild(iframe);
                return iframe;
            };

            this.QueueMethod = function(kit, method, args, completionFn, exceptionFn) {
                var dispatchMethod = new DispatchMethod(method, args || {});

                _completionCallbacks[dispatchMethod.id] = { completionFn: completionFn, exceptionFn: exceptionFn };

                var request = "squareit://" + kit + "/" + method + "?" + escape(JSON.stringify(dispatchMethod));

                _execIFrame = _execIFrame || createExecIframe();
                _execIFrame.src = request;
            };

            this.CompletionCallback = function(id, argsJSON) {
                var callback = _completionCallbacks[id];

                if (!callback || callback.completionFn === undefined)
                    return -1;

                delete _completionCallbacks[id];

                var args = JSON.parse(argsJSON);

                callback.completionFn(args);

                return 1;
            };

            this.ExceptionCallback = function(id, argsJSON) {
                var callback = _completionCallbacks[id];

                if (!callback || callback.exceptionFn === undefined)
                    return -1;

                delete _completionCallbacks[id];

                var args = JSON.parse(argsJSON);

                callback.exceptionFn(args);

                return 1;
            };

            this.ReceiveMethods = function() {
                var methodsAsString = JSON.stringify(_methodQueue);
                _methodQueue = [];
                return methodsAsString;
            };
        }();

        // List of functions to be called after SqIT.Initialise has been called and returned
        self._afterInitialiseFns = [];

        // Register an AfterInitialise function.  Client code using SquareIT should call this via
        // $(function() { 
        //    SquareIT.AfterInitialise(function() { 
        //        client blah blah code 
        //    });
        // });
        //
        // This is to make sure that all appropriate WebKit methods have been registered from the Initialise method
        self.AfterInitialise = function(fn) {
            self._afterInitialiseFns.push(fn);
        };

        // v1.0 functions were hard-coded
        self.UploadPhoto = function(args, completionFn, exceptionFn) {
            SquareIT.WebViewProxy.QueueMethod('BWorkflow', 'UploadPhoto', args, completionFn, exceptionFn);
        };

        self.UploadMedia = function(args, completionFn, exceptionFn) {
            // Note, UploadMedia did not exist in v1.0 so map it to UploadPhoto
            SquareIT.WebViewProxy.QueueMethod('BWorkflow', 'UploadPhoto', args, completionFn, exceptionFn);
        };

        self.GetGPSLocation = function(args, completionFn, exceptionFn) {
            SquareIT.WebViewProxy.QueueMethod('BWorkflow', 'GetGPSLocation', args, completionFn, exceptionFn);
        };
    }();
    
(function () {
    // Override some standard methods ..
    navigator.geolocation.getCurrentPosition = function (successFn, failureFn) {
        SquareIT.GetGPSLocation({}, function (result) {
            if (result.authorized == 'Authorized') {
                if (successFn !== undefined)
                    successFn(result);
            } else if (failureFn !== undefined)
                failureFn({ message: "GPS access denied, please allow GPS access for this application" });
        });
    };
    
    //Pre and Post call overrides on a per-kit method basis
    var preCallFuncs = {};
    var postCallFuncs = {};
    
    //JSON->data transform for camera upload
    postCallFuncs.BWorkflow = {};
    postCallFuncs.BWorkflow.UploadMedia = function (result, completionFn) {
        if (result.type == 'image') {
            //.data is an image object
            result.data = new Image();
            
            var mimeType = '';
            if (result.fname.toLowerCase().indexOf('.png', result.fname.length - 4) !== -1)
                mimeType = 'image/png';
            else if (result.fname.toLowerCase().indexOf('.jpg', result.fname.length - 4) !== -1 ||
                result.fname.toLowerCase().indexOf('.jpeg', result.fname.length - 5) !== -1)
                mimeType = 'image/jpeg';

            result.data.src = 'data:' + mimeType + ';base64,' + result.datab64;
        } else {
            //.data is a byte string
            result.data = window.atob(result.datab64);
        }
        completionFn(result);
    };

    SquareIT.WebViewProxy.QueueMethod('SqIT', 'Initialise', {}, function (result) {
        SquareIT.version = parseFloat(result.version);

        // SquareIT Kit v1.1 onwards support dynamic kit methods .. 
        if ($.isArray(result.kits)) {
            result.kits.forEach(function (kit) {
                var kitName = kit.name;

                SquareIT[kitName] = {};

                kit.methods.forEach(function (method) {
                    var fn = function (args, completionFn) {
                        //work out what completion function to call.
                        //If a post-work function was given, call that with both the result and the real callback
                        var cf = (postCallFuncs[kitName] && postCallFuncs[kitName][method]) ?
                            function(r) { postCallFuncs[kitName][method](r, completionFn); } :
                            completionFn;
                        SquareIT.WebViewProxy.QueueMethod(kitName, method, args, cf);
                    };

                    // Add a kit namespaced version
                    SquareIT[kitName][method] = fn;
                });
            });
        }

        window.SquareIT.Loaded = true;
        $(document).trigger('SquareITLoaded');

        // Now call all AfterInitialise client functions
        SquareIT._afterInitialiseFns.forEach(function (fn) { fn(); });
    });
})();

window.SqIT = SquareIT;
window.SquareIT.app = true;