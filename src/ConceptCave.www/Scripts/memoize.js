﻿//http://talideon.com/weblog/2005/07/javascript-memoization.cfm
//Automemoizer for JS
//Used for cutting back on calls to server

Function.prototype.memoize = function() {
    var pad = {};
    var self = this;
    var obj = arguments.length > 0 ? arguments[i] : null;

    var memoizedFn = function() {
        // Copy the arguments object into an array: allows it to be used as
        // a cache key.
        var args = [];
        for (var i = 0; i < arguments.length; i++) {
            args[i] = arguments[i];
        }

        // Evaluate the memoized function if it hasn't been evaluated with
        // these arguments before.
        if (!(args in pad)) {
            pad[args] = self.apply(obj, arguments);
        }

        return pad[args];
    };

    memoizedFn.unmemoize = function() {
        return self;
    };

    return memoizedFn;
};

Function.prototype.unmemoize = function() {
    alert("Attempt to unmemoize an unmemoized function.");
    return null;
};