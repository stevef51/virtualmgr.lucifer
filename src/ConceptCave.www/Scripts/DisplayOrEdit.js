﻿(function ($) {
    $.fn.displayoredit = function (method) {
        var args = arguments[0] || {}; // It's your object of arguments
        var displayElement = $(args.displayElement);
        var editElement = $(args.editElement);
        var deleteElement = $(args.deleteElement);
        var editCallback = args.editCallback;
        var saveCallback = args.saveCallback;
        var cancelCallback = args.cancelCallback;
        var deleteCallback = args.deleteCallback;
        var displayGrabHandle = true;
        var context = null;

        var displayMenuElement = displayElement;
        if (args.displayMenuElement != undefined) {
            displayMenuElement = $(args.displayMenuElement);
        }

        var replaceDisplayContent = false;
        if (args.replaceDisplayContent != undefined) {
            replaceDisplayContent = args.replaceDisplayContent;
        }

        var editMenuElement = editElement;
        if (args.editMenuElement != undefined) {
            editMenuElement = $(args.editMenuElement);
        }

        var deleteMenuElement = deleteElement;
        if (args.deleteMenuElement != undefined) {
            deleteMenuElement = $(args.deleteMenuElement);
        }

        if (args.displayGrabHandle != undefined) {
            displayGrabHandle = args.displayGrabHandle;
        }

        if (args.context != undefined) {
            context = args.context;
        }

        var methods = {
            displayHoverIn: function () {
                var element = displayMenuElement.find("> .displayoredit_displaymenu");
                element.css('visibility', 'visible');
                displayElement.addClass('displayoredit_displaymenu_hover');
            },
            displayHoverOut: function () {
                var element = displayMenuElement.find("> .displayoredit_displaymenu");
                element.css('visibility', 'hidden');
                displayElement.removeClass('displayoredit_displaymenu_hover');
            },
            displayEditClick: function () {
                displayElement.slideToggle('fast');
                editElement.slideToggle('fast');
                editElement.addClass('displayoredit_highlight');

                var element = editElement.find(" .displayoredit_editmenu");
                element.css('visibility', 'visible');

                if (editCallback != undefined) {
                    editCallback(context);
                }
            },
            editHoverIn: function () {
                //var element = editElement.find(" .displayoredit_editmenu");
                //element.css('visibility', 'visible');
            },
            editHoverOut: function () {
                //var element = editElement.find(" .displayoredit_editmenu");
                //element.css('visibility', 'hidden');
            },
            editSaveClick: function () {
                editElement.slideToggle('fast');
                displayElement.slideToggle('fast');
                editElement.removeClass('displayoredit_editmenu_editing');
                saveCallback(context);
            },
            editCancelClick: function () {
                editElement.slideToggle('fast');
                displayElement.slideToggle('fast');
                editElement.removeClass('displayoredit_editmenu_editing');

                if (cancelCallback != undefined) {
                    cancelCallback();
                }
                else {
                    editElement.find("form").first().resetForm();
                }
            },
            editDeleteClick: function () {
                if (confirm('Are you sure you want to delete this item, this action cannot be undone?') == false) {
                    return;
                }

                editElement.slideToggle('fast');
                displayElement.slideToggle('fast', function () {
                    editElement.removeClass('displayoredit_editmenu_editing');

                    if (deleteCallback != undefined) {
                        deleteCallback(context);
                    }
                });
            }
        };

        // a the top of the display element will present a little toolbar allowing them to select to edit the item
        var displayMenuHtml = '<div style="visibility:hidden;" class="displayoredit_displaymenu">';
        if (displayGrabHandle == true) {
            displayMenuHtml += '<div class="handle" style="float:right;display: inline-block"><i class="icon-move"></i></div>'
        }
        displayMenuHtml += '<button style="float:right" type="button" class="btn"><i class="icon-edit"></i>&nbsp;Edit</button></div>';

        if (replaceDisplayContent == true) {
            displayMenuElement.html(displayMenuHtml);
        }
        else {
            displayMenuElement.prepend(displayMenuHtml);
        }
        displayMenuElement.find('> .displayoredit_displaymenu button').click(methods.displayEditClick);
        $(displayElement).hover(methods.displayHoverIn, methods.displayHoverOut);

        if (replaceDisplayContent == true) {
            editMenuElement.html('<div style="visibility:hidden;" class="displayoredit_editmenu"><div class="form-actions"><button class="btn btn-primary displayoredit_editmenu_save" type="button"><i class="icon-ok icon-white"></i>&nbsp;Save</button><button class="btn displayoredit_editmenu_cancel" type="button"><i class="icon-remove"></i>&nbsp;Cancel</button><button class="btn btn-danger displayoredit_editmenu_delete" type="button"><i class="icon-trash icon-white"></i>&nbsp;Delete</button></div></div>');
        }
        else {
            editMenuElement.append('<div style="visibility:hidden;" class="displayoredit_editmenu"><div class="form-actions"><button class="btn btn-primary displayoredit_editmenu_save" type="button"><i class="icon-ok icon-white"></i>&nbsp;Save</button><button class="btn displayoredit_editmenu_cancel" type="button"><i class="icon-remove"></i>&nbsp;Cancel</button><button class="btn btn-danger displayoredit_editmenu_delete" type="button"><i class="icon-trash icon-white"></i>&nbsp;Delete</button></div></div>');
        }
        editMenuElement.find('> .displayoredit_editmenu button.displayoredit_editmenu_save').button({ icons: { primary: 'ui-icon-disk' }, text: false });
        editMenuElement.find('> .displayoredit_editmenu button.displayoredit_editmenu_cancel').button({ icons: { primary: 'ui-icon-cancel' }, text: false });
        editMenuElement.find('> .displayoredit_editmenu button.displayoredit_editmenu_delete').button({ icons: { primary: 'ui-icon-trash' }, text: false });

        editMenuElement.find('> .displayoredit_editmenu button.displayoredit_editmenu_save').click(methods.editSaveClick);
        editMenuElement.find('> .displayoredit_editmenu button.displayoredit_editmenu_cancel').click(methods.editCancelClick);
        editMenuElement.find('> .displayoredit_editmenu button.displayoredit_editmenu_delete').click(methods.editDeleteClick);

        $(editElement).hover(methods.editHoverIn, methods.editHoverOut);

        return this; // maintain jquery chainability
    }
})(jQuery);