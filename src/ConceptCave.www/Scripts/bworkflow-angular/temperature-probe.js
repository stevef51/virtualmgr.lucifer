﻿angular.module('temperatureModule', ['vmngUtils', 'bootstrapCompat'])

.factory('iOSExternalAccessory', ['ngTimeout', '$log', 'actionQueue', function (ngTimeout, $log, actionQueue) {
    var plugin = cordova.plugins.iOSExternalAccessory;
    if (!plugin) {
        return angular.undefined;
    }

    // Comment the following out for Release builds
/*
    var _logId = 0;
    plugin.setLog(function (name, args) {
        var id = _logId++;
        $log.info(id.toString() + ' CALLING ' + name + ' ( ' + JSON.stringify(args) + ')');

        return {
            success: function (result) {
                $log.info(id.toString() + ' SUCCESS ' + name + ' = ' + JSON.stringify(result));
            },
            error: function (msg) {
                $log.info(id.toString() + ' ERROR   ' + name + ' : ' + msg);
            }
        };
    });
*/

    plugin.getVersion(function (info) {
        $log.info('iOSExternalAccessory version ' + info.version);
    });

    var _sessions = {};

    var makeSubscribers = function (list, key) {
        return function () {
            var args = Array.prototype.slice.call(arguments);
            list.forEach(function (subscriber) {
                if (subscriber[key]) {
                    subscriber[key].apply(null, args);
                }
            });
        };
    };

    var AccessorySession = function (session) {
        var self = this;
        self.id = session.sessionId;

        var rawDataSubscribers = [];

        self.write = function (data, success, error) {
            plugin.AccessorySession_write(self.id, data, ngTimeout(success), ngTimeout(error));
        };

        // The native plugin only supports 1 subscriber per session, we implement multiple subscribers here ..
        self.subscribeRawData = function (success, error) {
            if (rawDataSubscribers.length == 0) {
                plugin.AccessorySession_subscribeRawData(self.id, ngTimeout(makeSubscribers(rawDataSubscribers, 'success')), ngTimeout(rawDataSubscribers, 'error'));
            }

            var subscription = {
                success: success,
                error: error
            };

            rawDataSubscribers.push(subscription);

            // Calling result will unsubscribe
            return function () {
                rawDataSubscribers.remove(function (i) {
                    return i === subscription;
                });

                if (rawDataSubscribers.length == 0) {
                    plugin.AccessorySession_unsubscribeRawData(self.id);
                }
            }
        };

        self.disconnect = function (success, error) {
            self.q(function (finish) {
                if (--self.connectCount == 0) {
                    plugin.AccessorySession_disconnect(self.id, function () {
                        // Possible someone else has reconnected, check first
                        var check = _sessions[self.id];
                        if (check && check.connectCount == 0) {
                            delete _sessions[self.id];
                        }
                        ngTimeout(success, finish)();
                    }, ngTimeout(error, finish));
                } else {
                    ngTimeout(success, finish)();
                }
            });
        };

        self.connectCount = 1;

        // We queue Connects & Disconnects to stop them overlapping
        self.q = actionQueue();
    };

    var svc = {
        listDevices: function (success, error) {
            plugin.listDevices(ngTimeout(success), ngTimeout(error));
        },

        listSessions: function (success, error) {
            plugin.listSessions(ngTimeout(function (sessions) {
                var result = [];
                var newSessions = {};
                if (sessions) {
                    sessions.forEach(function (session) {
                        var existing = _sessions[session.sessionId];
                        if (!existing) {
                            existing = new AccessorySession(session);
                        }
                        result.push(existing);
                        newSessions[session.sessionId] = existing;
                    });
                }

                _sessions = newSessions;

                success(result);
            }), ngTimeout(error));
        },

        connect: function (device, protocol, success, error) {
            var key = device.id + ':' + protocol;
            var existing = _sessions[key];
            var subscriber = {
                success: success,
                error: error
            };
            // Could not exist OR could be processing a Disconnect
            if (!existing || existing.connectCount == 0) {
                var subscribers = [subscriber];
                _sessions[key] = {
                    connecting: true,
                    subscribers: subscribers
                };

                // Make sure we Queue the Connect (fix race-condition on Session Disconnecting)
                var q = (existing && existing.q) || actionQueue();

                q(function (finish) {
                    plugin.connect(device.id, protocol, ngTimeout(function (session) {
                        if (key != session.sessionId) {
                            $log.error('*** Session Id format has changed ***');
                        }
                        existing = existing || new AccessorySession(session);
                        existing.connectCount = subscribers.length;
                        _sessions[session.sessionId] = existing;

                        makeSubscribers(subscribers, 'success')(existing);
                    }, finish), ngTimeout(function () {
                        // Connect failed, remove session
                        delete _sessions[key];
                    }, makeSubscribers(subscribers, 'error'), finish));
                });
            } else if (existing.connecting) {
                // Someone else is trying to connect, we have to wait for it to finish, add our success,error callbacks
                existing.subscribers.push(subscriber);
            } else {
                existing.connectCount++;
                if (success) {
                    success(existing);
                }
            }
        },

        subscribeDeviceChanges: function (success, error) {
            plugin.subscribeDeviceChanges(ngTimeout(success), ngTimeout(error));
        },

        unsubscribeDeviceChanges: function (success, error) {
            plugin.unsubscribeDeviceChanges(ngTimeout(success), ngTimeout(error));
        }
    };

    return svc;
}])

.factory('ETIProtocol', ['$log', 'binUtils', function ($log, binUtils) {

    var ComputeChecksum = function (bytes, numberOfBytesToCheckSum) {
        var crc = 0;
        for (var i = 0; i < numberOfBytesToCheckSum; i++) {
            crc = CalculateCRC(bytes[i], crc);
        }
        var ncrc = crc;
        ncrc = ~ncrc;
        ncrc = ncrc & 0x0000FFFF;
        return ncrc;
    };

    var CalculateCRC = function (p, crc) {
        var tempCRC = crc;
        var word = p;
        for (var i = 0; i < 8; i++) {
            var crcin = (((tempCRC ^ word) & 1) << 15);
            word = word >> 1;
            tempCRC = tempCRC >> 1;
            if (crcin !== 0) {
                tempCRC = tempCRC ^ (0xA001);
            }
        }
        crc = tempCRC;
        return crc;
    };

    var bytesToString = function (bytes, start, end) {
        var str = '';
        for (var i = start ; i < end ; i++) {
            if (bytes[i] === 0) {
                break;
            }
            str += String.fromCharCode(bytes[i]);
        }
        return str;
    };

    var readInt16 = binUtils.littleEndian.readInt16;
    var readUInt16 = binUtils.littleEndian.readUInt16;
    var readInt32 = binUtils.littleEndian.readInt32;

    var ChargingState = {
        0: 'Not charging',
        32: 'Pre-charge',
        64: 'Fast charge',
        96: 'Top off',
        128: 'Trickle',
        160: 'Error - Battery temperature maximum',
        192: 'Error - Battery voltage exceeded maximum',
        224: 'Error - Pre-charge timeout',
    };

    var batteryPercent = function (volts) {
        var p;
        if (volts >= 12.0) {
            p = 25.0 + (volts - 12.0) * 75.0;
            if (p > 100.0) {
                p = 100.0;
            }
        } else if (volts >= 11.0) {
            p = (volts - 11.0) * 25.0;
            if (p > 25.0) {
                p = 25.0;
            }
        } else {
            p = 0.0;
        }
        return p / 100.0;
    }

    var _messageTypeHandlers = {
        1: function (packet) {
            var probe1raw = readInt32(packet, 0x36);
            var batteryVoltsRaw = readUInt16(packet, 0x5e) & 8191;
            var batteryVolts = batteryVoltsRaw / 100;
            var chargingState = packet[0x5f] & 224;
            var batteryTemperature = readInt32(packet, 0x60);
            var user = readInt32(packet, 0x7a);
            return {
                type: 'reading',
                version: packet[1],
                serialNumber: bytesToString(packet, 0x4, 0xD),
                probe1Name: bytesToString(packet, 0xE, 0x21),
                probe1Reading: (probe1raw / 100000) - 300.0,
                batteryVolts: batteryVolts,
                batteryPercent: batteryPercent(batteryVolts),
                batteryTemperature: (batteryTemperature / 100000) - 300.0,
                chargingState: ChargingState[chargingState],
                user: user
            };
        },

        3: function (packet) {
            return {
                type: 'button'
            };
        },

        5: function (packet) {
            return {
                type: 'shutdown'
            };
        }
    };

    var populateChecksum = function (bytes) {
        var crc = ComputeChecksum(bytes, 126);
        bytes[126] = crc & 0xFF;
        bytes[127] = (crc >> 8) & 0xFF;
    };

    // Some Requests are statically defined    
    var _shutdownRequest = new Uint8Array(128);
    _shutdownRequest.set([0x05], 0);
    populateChecksum(_shutdownRequest);

    var logError = function (error) {
        $log.error(error);
    };

    var svc = {
        pollRequest: function (user) {
            var request = new Uint8Array(128);
            request.set([0x01, 0x01, 0xFF, 0xFF], 0);
            // Set user data 
            if (angular.isDefined(user)) {
                request.set([(user >> 0) & 0xFF, (user >> 8) & 0xFF, (user >> 16) & 0xFF, (user >> 24) & 0xFF], 0x7a);
            }
            populateChecksum(request);
            return request;
        },

        shutdownRequest: function () {
            return _shutdownRequest;
        },

        receiveBytes: function (bytes, fnMessage) {
            if (bytes.length == 128) {
                var calculatedCrc = ComputeChecksum(bytes, 126);
                var expectedCrc = bytes[126] | (bytes[127] << 8);

                if (calculatedCrc == expectedCrc) {
                    var handler = _messageTypeHandlers[bytes[0]];
                    if (angular.isFunction(handler)) {
                        var msg = handler(bytes);
                        fnMessage(msg);
                    } else {
                        $log.warn("Received unknown ETI message " + bytes[0].toString());
                    }

                }
            }
        }
    };

    return svc;
}])

.factory('BaseBlueThermSerial', ['ngTimeout', 'ConnectableDevice', 'actionQueue', '$q', function (ngTimeout, ConnectableDevice, actionQueue, $q) {
    return function (api, impl) {
        var _listChangeFns = [];
        var _session = null;
        var _unsubscribeRawData = null;

        var q = actionQueue();

        var svc = {
            probes: null,

            activeProbe: function () {
                return _session ? _session._probe : null;
            },

            connect: function (probe, success, error) {
                q(function (finish) {
                    var cn = function () {
                        _session = null;

                        if (probe) {
                            probe.connecting();
                            return api.connect(probe, ngTimeout(function (session) {
                                probe.connected();
                                _session = session;
                                _session._probe = probe;

                                ngTimeout(success, finish)(probe);
                            }), ngTimeout(probe.error, error, finish));
                        } else {
                            ngTimeout(success, finish)(probe);
                        }
                    };

                    if (_session) {
                        if (!probe || _session._probe.id != probe.id) {
                            svc.disconnect(cn, cn);
                            finish();
                        } else {
                            ngTimeout(success, finish)(probe);
                        }
                    } else {
                        cn();
                    }
                });
            },

            disconnect: function (success, error) {
                q(function (finish) {
                    if (_unsubscribeRawData) {
                        _unsubscribeRawData();
                        _unsubscribeRawData = null;
                    }

                    if (_session) {
                        var session = _session;
                        _session = null;

                        session._probe.disconnecting();
                        session.disconnect(
                            ngTimeout(session._probe.disconnected, success, finish),
                            ngTimeout(session._probe.error, error, finish));
                    } else {
                        ngTimeout(success, finish)();
                    }
                });
            },

            subscribeRawData: function (success, error) {
                if (_unsubscribeRawData) {
                    _unsubscribeRawData();
                    _unsubscribeRawData = null;
                }

                if (_session) {
                    return _unsubscribeRawData = _session.subscribeRawData(ngTimeout(success), ngTimeout(error));
                }
            },

            write: function (data, success, error) {
                if (_session) {
                    return _session.write(data, ngTimeout(success), ngTimeout(error));
                }
            },

            raiseChangeFns: function () {
                _listChangeFns.forEach(function (fn) {
                    fn(svc.probes);
                });
            },

            list: function (success, error) {
                return api.listDevices(ngTimeout(function (devices) {
                    var before = JSON.stringify(svc.probes);

                    svc.probes = svc.probes || [];
                    var probes = [];
                    var sessionOk = false;
                    devices.forEach(function (dev) {
                        var probe = impl.filterDevice(dev);
                        if (probe) {
                            var existing = svc.probes.find(function (f) {
                                return f.id == probe.id;
                            });
                            if (existing) {
                                existing.other = probe.other;
                                probes.push(existing);

                                if (_session && _session._probe === existing) {
                                    sessionOk = true;
                                }
                            } else {
                                var connectableProbe = ConnectableDevice({}, probe.id, probe.name, probe.other);
                                connectableProbe._serialNumberPromise = $q.defer();
                                connectableProbe.getSerialNumber = function () {
                                    return this._serialNumberPromise.promise;
                                }
                                probes.push(connectableProbe);
                            }
                        }
                    });

                    // If our session has disappeared then it is likely it is Out of Range
                    // or the Battery is flat since the probe should tell us when it is shutting down
                    if (_session && !sessionOk) {
                        var sessionProbe = _session._probe;
                        svc.disconnect(null, function () {
                            sessionProbe.error('Out of range or battery flat (Press Button on probe to turn it on)');
                        });
                    }

                    svc.probes.splice(0, svc.probes.length);
                    svc.probes.push.apply(svc.probes, probes);

                    // Sort so we can compare list before and after ..
                    svc.probes.sort(function (a, b) {
                        return a.id.toString().localeCompare(b.id.toString());
                    });

                    if (success) {
                        success(svc.probes);
                    }

                    if (impl.probes) {
                        impl.probes(svc.probes);
                    }

                    var after = JSON.stringify(svc.probes);
                    if (before != after) {
                        svc.raiseChangeFns();
                    }
                }), ngTimeout(error));
            },

            subscribeListChange: function (change) {
                var remove = function () {
                    _listChangeFns.remove(function (i) { return i === change });
                };

                // Remove it first incase its already in there
                remove();

                var update = function () {
                    change(svc.probes);
                };

                _listChangeFns.push(change);

                // Give it fresh list
                if (!svc.probes) {
                    // Note we know we have no probes, if list() returns 1+ probes then
                    // callers 'change' will get called through raiseChangeFns :)
                    svc.list();		// hence no 'success'
                } else {
                    update();
                }

                return remove;
            },

            scanAllProbeButtons: function (success) {
                if (impl.scanAllProbeButtons) {
                    return impl.scanAllProbeButtons(success);
                }
            }
        };
        return svc;
    }
}])

.factory('BLEBlueThermSerial', ['BaseBlueThermSerial', '$interval', 'ConnectableDevice', '$timeout', 'ngTimeout', function (base, $interval, ConnectableDevice, $timeout, ngTimeout) {
    var impl = {
        filterDevice: function (dev) {
            if (dev.class == 1796 && dev.name.indexOf('BlueTherm') >= 0) {
                return {
                    id: dev.address,
                    name: dev.name,
                    other: dev
                };
            } else {
                return null;
            }
        },

        probes: function (probes) {
            // Extract SerialNumber from the probes name (eg "12345 BlueTherm")
            probes.forEach(function (probe) {
                if (!probe.serialNumber) {
                    var match = /^(\d*)\s*(.*)/.exec(probe.name);
                    if (match.length >= 1) {
                        probe.serialNumber = match[1];
                        if (match.length >= 2) {
                            probe.name = match[2];
                        }

                        if (probe._serialNumberPromise) {
                            probe._serialNumberPromise.resolve(probe.serialNumber);
                        }
                    }
                }
            });
        }
    };

    var plugin = window.bluetoothSerial;
    var api = angular.extend({}, plugin, {
        connect: function (probe, success, error) {
            var reconnect = 0;
            var cn = function () {
                plugin.connect(probe.id, function () {
                    var session = {
                        disconnect: plugin.disconnect,
                        subscribeRawData: plugin.subscribeRawData,
                        write: plugin.write
                    };

                    success(session);
                }, function (msg) {
                    if (msg == 'Device connection was lost' && reconnect++ < 3) {
                        $timeout(cn, 1000);
                    } else if (error) {
                        error(msg);
                    }
                });
            };

            cn();
        },

        listDevices: plugin.list
    });

    var svc = base(api, impl);

    $interval(function () {
        if (!svc.activeProbe()) {
            svc.list();
        }
    }, 5000);

    return svc;
}])

.factory('iOSBlueThermSerial', ['iOSExternalAccessory', 'BaseBlueThermSerial', '$log', 'ETIProtocol', function (plugin, base, $log, ETIProtocol) {
    var BLUETHERM_PROTOCOL = 'uk.co.etiltd.bluetherm1';

    // iOS External Accessory should be reporting the SerialNumber of the devices 
    // however for some reason (Apple or more likely BlueTherm) are not reporting the SerialNumber through
    // the External Accessory API and so we cannot seperate physical probes - so we will poll each
    // probe for its SerialNumber when we see them
    var findSerialNumber = function (probe) {
        if (!probe.serialNumber) {
            plugin.connect(probe, BLUETHERM_PROTOCOL, function (session) {
                var unsubscribe = session.subscribeRawData(function (rawArray) {
                    var bytes = new Uint8Array(rawArray);

                    ETIProtocol.receiveBytes(bytes, function (msg) {
                        switch (msg.type) {
                            case 'reading':
                                // Possible we get here multiple times
                                if (!probe.serialNumber) {
                                    probe.serialNumber = msg.serialNumber;
                                    probe.name = msg.serialNumber + ' ' + probe.name;   // Simulate what Android gets with Name

                                    if (probe._serialNumberPromise) {
                                        probe._serialNumberPromise.resolve(probe.serialNumber);
                                    }
                                    svc.raiseChangeFns();
                                }
                                unsubscribe();
                                session.disconnect();
                                break;
                        }
                    });
                });

                session.write(ETIProtocol.pollRequest());
            });
        }
    }

    var impl = {
        filterDevice: function (dev) {
            if (dev.protocolStrings.indexOf(BLUETHERM_PROTOCOL) != -1) {
                return {
                    id: dev.id,
                    name: dev.name,
                    other: dev
                };
            } else {
                return null;
            }
        },

        probes: function (probes) {
            probes.forEach(findSerialNumber);
        },

        // This function will connect to all probes and listen for the first button press from one of them
        // and then will disconnect from all and success the probe
        // It returns a function which when called will cancel the scanAllProbeButtons
        scanAllProbeButtons: function (success) {
            var tidyUps = [];

            var cancel = function () {
                if (tidyUps) {
                    tidyUps.forEach(function (fn) {
                        fn();
                    });
                    tidyUps = null;
                }
            };

            if (svc.probes) {
                svc.probes.forEach(function (probe) {
                    plugin.connect(probe, BLUETHERM_PROTOCOL, function (session) {
                        tidyUps.push(session.subscribeRawData(function (rawArray) {
                            var bytes = new Uint8Array(rawArray);

                            ETIProtocol.receiveBytes(bytes, function (msg) {
                                switch (msg.type) {
                                    case 'button':
                                        cancel();

                                        if (success) {
                                            success(probe);
                                        }
                                        break;
                                }
                            });
                        }));
                        tidyUps.push(session.disconnect);
                    });
                });
            }
            return cancel;
        }
    };

    var api = angular.extend({}, plugin, {
        connect: function (probe, success, error) {
            plugin.connect(probe.other, BLUETHERM_PROTOCOL, success, error);
        }
    });

    var svc = base(api, impl);

    // Disconnect any sessions
    plugin.listSessions(function (sessions) {
        sessions.forEach(function (session) {
            session.disconnect();
        });
    });

    // Watch for connect/disconnect
    plugin.subscribeDeviceChanges(function (args) {
        $log.debug('deviceChange: ' + JSON.stringify(args));
        // Since underlying id of Accessory changes on each Connect we have no way of identifying a unique
        // physical device (SerialNumber is not reported for the BlueTherm devices)
        // so we simply re-request full device list - we will query for SerialNumbers for each new probe seen

        svc.list();
    });

    return svc;
}])

.factory('blueThermSerial', ['$injector', function ($injector) {
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.iOSExternalAccessory) {
        return $injector.get('iOSBlueThermSerial');
    } else if (window.bluetoothSerial) {
        return $injector.get('BLEBlueThermSerial');
    } else {
        return {
            notsupported: true
        }
    }
}])

    .factory('temperatureProbe', ['ETIProtocol', 'blueThermSerial', '$timeout', '$q', '$log', 'ConnectableDeviceStates', function (ETIProtocol, blueThermSerial, $timeout, $q, $log, ConnectableDeviceStates) {
    if (blueThermSerial.notsupported) {
        return {
            supported: false,
            notsupported: true
        };
    }

    var _currentTemperature = {
        value: undefined
    };
    var _battery = {
        volts: undefined,
        temperature: undefined,
        charging: undefined
    };
    var _buttonCallbacks = [];
    var _deferreds = [];
    var _userSequence = 0;
    var _interval = 0;
    var _timeoutPoll = null;
    var _autoDisconnectTimeout = null;
    var _polling = false;
    var _backupPoll = null;

    var logError = function (error) {
        return $q(function () {
            $log.error(error);
        });
    };

    var scheduleNextPoll = function () {
        if (_interval > 0 && !_timeoutPoll) {
            _timeoutPoll = $timeout(function () {
                _timeoutPoll = null;
                svc.pollProbe();    // Poll it, on completion we will schedule next poll
            }, _interval);
        }
    }

    var cancelPolling = function () {
        _polling = false;
        _interval = 0;
        if (_timeoutPoll) {
            $timeout.cancel(_timeoutPoll);
            _timeoutPoll = null;
        }
    }

    function Driver(device) {
        this.device = device;
    }

    Driver.prototype.connect = function () {
        var deferred = $q.defer();
        svc.activeProbe(this.device, function () {
            deferred.resolve()
        }, function (error) {
            deferred.reject(error);
        })
        return deferred.promise;
    }

    Driver.prototype.disconnect = function () {
        if (svc.activeProbe() !== this.device) {
            return $q.when();
        }
        var deferred = $q.defer();
        svc.activeProbe(null, function () {
            deferred.resolve()
        }, function (error) {
            deferred.reject(error);
        })
        return deferred.promise;
    }

    Driver.prototype.measure = function () {
        return svc.pollProbe().then(function () {
            return $q.when();       // Measure dot not resolve any value - have to query the probe for new reading
        });
    }

    Driver.prototype.configure = function (options) {
        if (angular.isDefined(options)) {
            if (options.measurementMilliseconds) {
                svc.startMonitor(options.measurementMilliseconds);
            }
            return $q.when();
        } else {
            return {
                measurementMilliseconds: _polling ? _interval : 0
            };
        }
    }

    Driver.prototype.getSerialNumber = function () {
        return this.device.getSerialNumber();
    }

    var managerProbe;

    var findOrCreateManagerProbe = function (device) {
        var probe = svc.manager.findProbe(device.id);
        if (!probe) {
            probe = svc.manager.addProbe(svc, device.id, device.name, new Driver(device));
        }

        switch (device.status) {
            case ConnectableDeviceStates.Named.Connected:
                if (!probe.isConnected()) {
                    probe.connected();
                }
                break;
            case ConnectableDeviceStates.Named.Disconnected:
                if (!probe.isDisconnected()) {
                    probe.disconnected();
                }
                break;
            case ConnectableDeviceStates.Named.Connecting:
                if (!probe.isConnecting()) {
                    probe.connecting();
                }
                break;
            case ConnectableDeviceStates.Named.Disconnecting:
                if (!probe.isDisconnecting()) {
                    probe.disconnecting();
                }
                break;
            case ConnectableDeviceStates.Named.Error:
                probe.error(device.statusText);
                break;
        }

        return probe;
    }

    var svc = {
        id: 'ETIBlueThermSerial',

        currentTemperature: _currentTemperature,
        battery: _battery,

        subscribeProbeList: function (fnChange) {
            return blueThermSerial.subscribeListChange(fnChange);
        },

        activeProbe: function (probe, success, error) {
            if (angular.isUndefined(probe)) {
                return blueThermSerial.activeProbe();
            } else {
                if (probe) {
                    blueThermSerial.connect(probe, function (probe) {
                        managerProbe = findOrCreateManagerProbe(probe);

                        if (managerProbe) {
                            managerProbe.connected();
                        }

                        blueThermSerial.subscribeRawData(function (rawArray) {
                            var bytes = new Uint8Array(rawArray);

                            ETIProtocol.receiveBytes(bytes, function (msg) {

                                switch (msg.type) {
                                    case 'reading':
                                        if (_backupPoll) {
                                            $timeout.cancel(_backupPoll);
                                            _backupPoll = null;
                                        }
                                        $log.debug('reading ' + msg.probe1Reading);
                                        _polling = false;

                                        var rawTemp = msg.probe1Reading;

                                        _battery.volts = msg.batteryVolts;
                                        _battery.temperature = msg.batteryTemperature;
                                        _battery.charging = msg.chargingState;
                                        _battery.percent = msg.batteryPercent;

                                        if (managerProbe) {
                                            managerProbe.updated({
                                                temperature: {
                                                    rawValue: rawTemp
                                                },
                                                battery: {
                                                    percent: msg.batteryPercent,
                                                    volts: msg.batteryVolts,
                                                    charging: msg.chargingState,
                                                    temperature: msg.batteryTemperature
                                                }
                                            });
                                        }

                                        _deferreds.forEach(function (d) {
                                            d.resolve(msg);
                                        });
                                        _deferreds = [];
                                        scheduleNextPoll();
                                        break;

                                    case 'button':
                                        $log.debug('Button pressed');
                                        angular.forEach(_buttonCallbacks, function (cb) {
                                            cb();
                                        });
                                        if (managerProbe) {
                                            managerProbe.buttonPressed();
                                        }
                                        break;

                                    case 'shutdown':
                                        $log.debug('Shutdown received');
                                        blueThermSerial.disconnect();
                                        if (managerProbe && angular.isFunction(managerProbe.remove)) {
                                            managerProbe.remove();
                                        }
                                        break;
                                }
                            }, logError);
                        }, logError);

                        if (success) {
                            success(probe);
                        }

                        _polling = false;
                        scheduleNextPoll();
                    }, function (msg) {
                        logError(msg);
                        if (error) {
                            error(msg);
                        }
                    });
                } else {
                    cancelPolling();
                    blueThermSerial.disconnect(success, error);
                    if (managerProbe) {
                        managerProbe.disconnected();
                        managerProbe = null;
                    }
                }
            }
        },

        startMonitor: function (interval) {
            cancelPolling();
            _interval = interval;
            scheduleNextPoll();

            // Make sure we do not Auto Disconnect now we have been told to start monitoring
            if (_autoDisconnectTimeout) {
                $timeout.cancel(_autoDisconnectTimeout);
                _autoDisconnectTimeout = null;
            }
        },

        pollProbe: function () {
            if (blueThermSerial.activeProbe()) {
                var deferred = $q.defer();
                var seq = _userSequence++;
                _deferreds.push(deferred);
                if (!_polling) {
                    $log.debug('pollProbe');
                    _polling = true;
                    blueThermSerial.write(ETIProtocol.pollRequest(seq), null, logError);

                    // Set a backup timer incase our poll is missed we can retry ..
                    _backupPoll = $timeout(function () {
                        $log.debug('backupPoll FIRED');
                        _polling = false;
                        scheduleNextPoll();
                    }, _interval + 1000);
                } else {
                    $log.debug('pollProbe DUPLICATE');
                }
                return deferred.promise;
            }
        },

        shutdown: function (disconnectTime) {
            cancelPolling();

            if (disconnectTime == 0) {
                blueThermSerial.disconnect();
                _autoDisconnectTimeout = null;

            } else if (disconnectTime > 0) {
                // We will auto disconnect in X seconds
                _autoDisconnectTimeout = $timeout(function () {
                    blueThermSerial.disconnect();
                    _autoDisconnectTimeout = null;
                }, disconnectTime * 1000);
            }
        },

        onButton: function (callback) {
            _buttonCallbacks.push(callback);

            return function () {
                var idx = _buttonCallbacks.indexOf(callback);
                if (idx >= 0)
                    _buttonCallbacks = _buttonCallbacks.slice(idx, idx);
            };
        },

        connectOnButton: function (success) {
            return blueThermSerial.scanAllProbeButtons(function (probe) {
                svc.activeProbe(probe, success);
            });
        },

        initialise: function (manager) {
            svc.manager = manager;
            manager.registerProvider(svc);
        },

        startScan: function (timeout) {
            var deferred = $q.defer();
            var self = this;

            // scan straight away
            blueThermSerial.list(function (devices) {
                devices.forEach(findOrCreateManagerProbe);

                // then scan at end of timeout aswell 
                $timeout(function () {
                    blueThermSerial.list(function (devices) {
                        devices.forEach(findOrCreateManagerProbe);

                        deferred.resolve();
                    })
                }, timeout);
            });

            return deferred.promise;
        },

        stopScan: function () {
            // Nothing to do, we kind of scan all the time
            return $q.when();
        }
    };

    // Disconnect any devices upfront
    blueThermSerial.disconnect();

    return svc;
}])

    .run(['temperature-probe-manager', 'temperatureProbe', function (manager, provider) {
        if (!provider.notsupported) {
            provider.initialise(manager);
        }
    }])