﻿'use strict';

(function () {
    var module = angular.module('vmgr-jsreport', []);

/*
    To clarify naming conventions within this module :-

    JsReportServer          - The JsReport Server instance which hosts reporting
    ShortId                 - The JsReport shortId which uniquely identifies a Template within the JsReport Server instance
    Template                - The JsReport object describing how a report is generated (the HTML + NodeJs)
    Report                  - A generic VirtualMgr Report, one of Telerik, PowerBI or JsReport
    Parameters              - An object defining the parameters of a JsReport Template
    Options                 - An object defining all JsReport parameters and other options (available Recipes)
    ParamValues             - An object specifying the actual chosen values of the parameters
    ReportContext           - An object containing a Parameters and able to generate a ReportRequest 
*/

    module.factory('jsReportService', ['$http', '$window', function ($http, $window) {
        var portalUrl = $window.razordata.siteprefix.replace(/\/$/g, '');
        var _jsReportServerUrl = null;
        var tenantHostName = $window.location.host;

        var http = function (request) {
            return $http(request)
                .catch(function(err) {
                    if (request.url.startsWith('https:')) {
                        var devRequest = angular.extend({}, request, { url: request.url.replace(/^https:/, 'http:') });
                        return $http(devRequest);
                    }
                    throw err;
                });
        }
        var reportServerHttpRaw = function (request) {
            return http(angular.extend({
                method: 'GET',
                noHeaderModify: true,
                withCredentials: false,
                headers: {
                    authorization: 'Basic YWRtaW46MGxlcnVsM3o='
                }
            }, request)).then(function(response) {
                return response.data;
            });
        }
        var reportServerHttp = function (request) {
            return reportServerHttpRaw(request).then(function (data) {
                return data.value;
            });
        };
        var svc = {
            jsReportServerUrl: function () {
                if (_jsReportServerUrl) {
                    return Promise.resolve(_jsReportServerUrl);
                } else {
                    return http({ url: `${portalUrl}/Reports/ReportServerUrl`, method: 'GET' }).then(function(response) {
                        return _jsReportServerUrl = response.data.jsreportserverurl;
                    });
                }
            },
            getJsReportServerTags: function () {
                // Template tags are used to restrict available reports to tenants by a regex matching the tenantHostName, the regex is either 
                // 1. "exact match" of the tag 'name', eg 'exhibitors.sanctuarycoveboatshow.com.au'
                // 2. "explicit" regular expression of the tag 'description' - the description must start with '^' which matches the start 
                // 3. "simple *.xxx" a simplified regular expresion of the tag 'description' - allows '*.getfoodsafe.tech' for example rather than the more complex but equivalent regex '^.*\.getfoodsafe\.tech$'
                return svc.jsReportServerUrl().then(function(jsReportServerUrl) {
                    return reportServerHttp({ url: `${jsReportServerUrl}/odata/tags`, method: 'GET' }).then(function(tags) {
                        return tags.filter(function(tag) {
                            var expr = null;
                            if (tag.description && tag.description.startsWith('^')) {
                                expr = tag.description;
                            } else {
                                expr = '^' + (tag.description || tag.name) + '$';
                                expr = expr.replace('.', '\.').replace('*', '.*');
                            }
                            var rexp = new RegExp(expr);
                            return rexp.test(tenantHostName);
                        });
                    });
                });
            },
            getJsReportServerTemplates: function () {
                return svc.jsReportServerUrl().then(function(jsReportServerUrl) {
                    return reportServerHttp({ url: `${jsReportServerUrl}/odata/templates?$orderby=name`, method: 'GET' })
                });
            },
            getJsReportServerTenantTemplates: function () {
                return Promise.all([svc.getJsReportServerTags(), svc.getJsReportServerTemplates()]).then(function (results) {
                    var [tags, templates] = results;
                    return templates.filter(function (template) {
                        return template.tags && template.tags.find(function (templateTag) {
                            return tags.find(function (tag) {
                                return tag.shortid === templateTag.shortid;
                            });
                        });
                    });
                });
            },
            getTenantJsReport: function (reportId) {
                return svc.getTenantJsReports().then(function(reports) {
                    return reports.find(function (r) {
                        return r.id === reportId;
                    });
                });
            },
            getTenantReportParameters: function (report, name, folderId) {
                return Promise.all([Promise.resolve(report), svc.getJsReportParameters(name, folderId)]).then(function(results) {
                    var [tenant, options] = results;
                    if (tenant.configuration) {
                        if (tenant.configuration.parameters) {
                            options.parameters = tenant.configuration.parameters;
                        } else if (tenant.configuration['merge-parameters']) {
                            tenant.configuration['merge-parameters'].forEach(function(mp) {
                                var tp = options.parameters.find(function (p) {
                                    return p.id === mp.id;
                                });
                                if (tp) {
                                    Object.assign(tp, mp);
                                } else {
                                    options.parameters.push(mp);
                                }
                            });
                        }
                        if (tenant.configuration.recipes) {
                            options.recipes = tenant.configuration.recipes;
                        }
                    }
                    return options;
                })
            },
            getJsReportParameters: function (name, folderId) {
                return svc.jsReportServerUrl().then(function(jsReportServerUrl) {
                    return reportServerHttp({ url: `${jsReportServerUrl}/odata/data?$filter=name eq '${name}.parameters' and folder/shortid eq '${folderId}'`, method: 'GET' })
                        .then(function(response) {
                            var options = response.length ? JSON.parse(response[0].dataJson) : {};
                            return options;
                        });

                })
            },
            getReportContextByShortId: function (shortId) {
                return svc.jsReportServerUrl().then(function(jsReportServerUrl) {
                    return reportServerHttp({ url: `${jsReportServerUrl}/odata/templates?$filter=shortid eq '${shortId}'`, method: 'GET' })
                        .then(function(response) {
                            var template = response.length ? response[0] : null;
                            if (template != null) {
                                return svc.getJsReportParameters(template.name, template.folder.shortid).then(function(options) {
                                    return { template, options, reportName: template.name };
                                });
                            } else {
                                throw new Error('JsReport Template not found');
                            }
                        });
                });
            },
            getReportContextByReportId: function (reportId) {
                return svc.getTenantJsReport(reportId).then(function(report) {
                    return svc.jsReportServerUrl().then(function(jsReportServerUrl) {
                        return reportServerHttp({ url: `${jsReportServerUrl}/odata/templates?$filter=shortid eq '${report.shortid}'`, method: 'GET' })
                            .then(function(response) {
                                var template = response.length ? response[0] : null;
                                if (template != null) {
                                    return svc.getTenantReportParameters(report, template.name, template.folder.shortid).then(function(options) {
                                        return { template, options, reportName: report.name || template.name };
                                    });
                                } else {
                                    throw new Error('JsReport Template not found');
                                }
                            });
                    });
                });
            },
            getTenantJsReports: function () {
                return http({ url: `${portalUrl}/Reports/Reports`, method: 'GET' })
                    .then(function(response) {
                        return response.data.filter(function (report) {
                            return report.type == 2;
                        }).map(function(report) {
                            if (report.configuration) {
                                report.configuration = JSON.parse(report.configuration);
                            }
                            return report;
                        });
                    });
            },
            getTenantTemplates: function () {
                return Promise.all([svc.getTenantJsReports(), svc.getJsReportServerTenantTemplates()]).then(function(results) {
                    var [tenantReports, templates] = results;
                    return templates.filter(function (template) {
                        return tenantReports.find(function (tenantReport) {
                            return template.shortid === tenantReport.shortid;
                        });
                    });
                });
            },
            requestToken: function (tenant) {
                return http({ url: `https://${tenant}/portal/API/v1/Auth/Token`, method: 'POST' }).then(function(response) {
                    return response.data.token;
                });
            },
            generateReport: function (reportContext, recipe, responseType, preview) {
                var { template, parameters } = reportContext;
                return svc.requestToken(parameters.tenantHostName.value).then(function(token) {
/* Disabled this code since WindowsToIana timezone has been implemented in vmgr-jsreport
                    // Any datetimes need to be converted to userlocal ..
                    var utcDatetimeIds = Object.keys(parameters).filter(function(id) {
                        var p = parameters[id];
                        return (p.type === 'datetime' || p.type === 'date') && !p.timezone;
                    })

                    if (utcDatetimeIds.length) {
                        var convertResult = await svc.convertUtcDatetimesToUserLocal(utcDatetimeIds.map(function(id) { return parameters[id].value; }));
                        var i = 0;
                        for (var id in utcDatetimeIds) {
                            var p = parameter[id];
                            p.utcDatetime = p.value;
                            p.localDatetime = convertResult.datetimes[i];
                            p.localTimeZone = convertResult.destinationTimezone;
                            i++;
                        }
                    }
*/
                    var requestBody = {
                        template: {
                            shortid: template.shortid,
                            recipe
                        },
                        options: {
                            parameters,
                            token,
                            preview
                        }
                    };


                    return svc.jsReportServerUrl().then(function(jsReportServerUrl) {
                        return reportServerHttpRaw({ url: `${jsReportServerUrl}/api/report`, method: 'POST', data: requestBody, responseType: responseType || 'blob' });
                    });
                });
            },
            convertUtcDateTimesToUserLocal: function (dateTimes) {
                return http({ url: `https:${tenant}/portal/API/v1/User/ConvertDateTimes`, method: 'POST', data: { DateTimes: dateTimes }}).then(function(response) {
                    return response.data;
                });
            }
        }
        return svc;
    }]);

    module.directive('jsreportTemplateSelector', ['jsReportService', '$window',
        function (jsReportSvc, $window) {
            return {
                templateUrl: $window.razordata.siteprefix + 'angulartemplates/Reports/jsreport_template_selector.html',
                restrict: 'E',
                require: 'ngModel',
                scope: {
                    shortId: '=ngModel'
                },
                link: function (scope) {
                    jsReportSvc.getJsReportServerTenantTemplates().then(function(templates) {
                        return scope.templates = templates;
                    });
                }
            };
        }
    ]);

    module.controller('jsreportContextController', [
        '$scope', 'jsReportService', function ($scope, jsReportSvc) {
            $scope.reportContext = null;
            $scope.shortId = null;
            $scope.templateParameters = {};
            $scope.reportOutput = {};

            $scope.$watch('shortId', function(newValue) {
                if (newValue) {
                    jsReportSvc.getReportContextByShortId(newValue)
                        .then(function(reportContext) {
                            $scope.reportContext = reportContext;

                        });
                } else {
                    $scope.reportTemplate = null;
                }
            });
        }
    ]);

    module.directive('jsreportSelector', ['jsReportService', '$window',
        function (jsReportSvc, $window) {
            return {
                templateUrl: $window.razordata.siteprefix + 'angulartemplates/Reports/jsreport_selector.html',
                restrict: 'E',
                scope: {
                    shortId: '='
                },
                link: function (scope) {
                    jsReportSvc.getTenantTemplates().then(function(templates) {
                        scope.templates = templates;
                    });
                }
            };
        }
    ]);

    module.directive('jsreportParameters', ['$interpolate', '$parse', '$window', '$http', 'jsReportService',
        function ($interpolate, $parse, $window, $http, svc) {
            return {
                templateUrl: $window.razordata.siteprefix + 'angulartemplates/Reports/jsreport_parameters.html',
                scope: {
                    reportId: '=',
                    shortId: '=',
                    reportContext: '='
                },
                controller: function ($scope) {
                    // These 'hardcoded' parameters are always sent in every JsReport request
                    var defaultParameters = {
                        tenantHostName: {
                            value: $window.location.host
                        }
                    };

                    // The defaultScope allows access to handy functions from angular {{ templates }}
                    var defaultScope = {
                        moment: moment,
                        scope: $scope
                    };

                    // This Proxy handler maps to parameter objects, fallback to defaultParameters then defaultScope
                    var parameterHandler = {
                        get(target, id) {
                            var parameter = $scope.parameters.find(function (p) { return p.id === id; });
                            if (!angular.isDefined(parameter)) {
                                if (id in defaultParameters) {
                                    return defaultParameters[id];
                                } else {
                                    return defaultScope[id];
                                }
                            }
                            return parameter;
                        }
                    };

                    var dynamicProperties = [];

                    var interpolateProperty = function (obj, property, value, fnResolved, fnConvert) {
                        fnConvert = fnConvert || angular.identity;
                        var interpolated = $interpolate(value, true, null, true);
                        if (!interpolated) {
                            obj[property] = value;
                            return;
                        }

                        dynamicProperties.push(function(parameters) {
                            var result = fnConvert(interpolated(parameters));

                            if (obj[property] != result) {
                                obj[property] = result;
                                if (fnResolved) {
                                    fnResolved(result);
                                }
                            }
                        });
                    }

                    var parseProperty = function (obj, property, value, fnResolved, fnConvert) {
                        fnConvert = fnConvert || angular.identity;
                        var parsed = angular.isString(value) ? $parse(value) : function () {
                            return value;
                        };

                        dynamicProperties.push(function(parameters) {
                            var result = fnConvert(parsed(parameters));

                            if (obj[property] != result) {
                                obj[property] = result;
                                if (fnResolved) {
                                    fnResolved(result);
                                }
                            }
                        });
                    }

                    var customProperties = [
                        {
                            expr: /^\@items$/,                        // Can be a fixed array, comma seperated string or { sourceUrl, value, text } source definition - note we assign to parameter.$$items which means it will not get sent in request
                            fn: function (parameter) {
                                if (angular.isArray(parameter['@items'])) {     // Assume { value, text } array
                                    parameter.$$items = parameter['@items'];
                                } else if (angular.isObject(parameter['@items'])) {
                                    var interpolateValue = $interpolate(parameter['@items'].value, true, null, false) || angular.identity;
                                    var interpolateText = $interpolate(parameter['@items'].text, true, null, false) || angular.identity;
                                    interpolateProperty(parameter['@items'], 'sourceUrl', parameter['@items'].sourceUrl, function(sourceUrl) {
                                        if (!sourceUrl) {
                                            parameter.value = undefined;
                                            parameter.text = undefined;
                                        } else {
                                            var get = function (url) {
                                                return $http.get(url).then(function(response) {
                                                    parameter.$$items = response.data.value.map(function(item) {
                                                        return {
                                                            value: interpolateValue(item),
                                                            text: interpolateText(item)
                                                        }
                                                    });
                                                    // reset the parameter if the new items list does not contain its current value
                                                    if (!parameter.$$items.find(function (i) { return i.value == parameter.value; })) {
                                                        parameter.value = undefined;
                                                        parameter.text = undefined;
                                                    }
                                                })
                                            }
                                            // Handle developer machines not running https ..
                                            get(sourceUrl).catch(function(err) {
                                                return get(sourceUrl.replace(/^https:/, 'http:'));
                                            });
                                        }
                                    });
                                } else if (angular.isString(parameter['@items'])) {
                                    parameter.$$items = parameter['@items'].split(',').map(function(s) {
                                        return {
                                            value: s.trim(),
                                            text: s.trim()
                                        }
                                    });
                                }

                                // Set parameter.text specifically when parameter.value changes
                                $scope.$watch(`parameterValues.${parameter.id}.value`, function(newValue) {
                                    if (newValue && angular.isArray(parameter.$$items)) {
                                        var item = parameter.$$items.find(function (item) { return item.value === newValue; });
                                        if (item) {
                                            parameter.text = item.text;
                                        }
                                    } else {
                                        parameter.value = undefined;
                                        parameter.text = undefined;
                                    }
                                });
                            },
                        }, {
                            expr: /^\#\w*$/,                        // any word starting with # means use angular $parse
                            fn: function (parameter, property) {
                                return parseProperty(parameter, property.replace(/^#/, ''), parameter[property]);
                            }
                        }, {
                            expr: /^\w*$/,                         // any word means use angular $interpolate (if its a string)
                            fn: function (parameter, property) {
                                return interpolateProperty(parameter, property, parameter[property]);
                            }
                        }
                    ];

                    var prepareValue = function (parameter) {
                        if (parameter.type === 'date' || parameter.type === 'datetime') {
                            return moment(parameter.value).utc();
                        }
                        return parameter.value;
                    }

                    var tryBuildRequest = function (changedId) {
                        dynamicProperties.forEach(function(fn) {
                            fn($scope.parameterValues);
                        })

                        // Determine whether we are valid and also create the POJO parameter values
                        var valid = true;
                        var parameters = Object.assign({}, defaultParameters);
                        if ($scope.parameters) {
                            $scope.parameters.forEach(function(parameter) {
                                if (angular.isDefined(parameter.id)) {
                                    parameters[parameter.id] = {
                                        id: parameter.id
                                    };
/*                                    for (var property in parameter) {
                                        if (!property.startsWith('$$')) {                                               // dont send any properties starting with $$ (angular stuff + $$items)
                                            parameters[parameter.id][property] = angular.copy(parameter[property]);
                                        }
                                    }
*/

                                    if (angular.isDefined(parameter.value)) {
                                        parameters[parameter.id].value = prepareValue(parameter);
                                    } else if (parameter.required) {
                                        valid = false;
                                    }
                                }
                            });
                        }
                        $scope.reportContext.valid = valid;
                        if (valid) {
                            $scope.reportContext.parameters = parameters;
                        }
                    }

                    var createParameters = function (parameters) {
                        $scope.parameters = JSON.parse(JSON.stringify(parameters));        // clone
                        $scope.parameterValues = new Proxy({}, parameterHandler);
                        dynamicProperties = [];
                        $scope.parameters.forEach(function(parameter) {
                            for (var property in parameter) {
                                for (var i in customProperties) {
                                    if (customProperties[i].expr.test(property)) {
                                        customProperties[i].fn(parameter, property);
                                        break;
                                    }
                                }
                            }
                            var id = parameter.id;
                            if (id) {
                                $scope.$watch(`parameterValues.${id}.value`, function () { return tryBuildRequest(id); });
                            }
                        });
                        tryBuildRequest();
                    }

                    $scope.$watch('reportContext', function(newValue) {
                        if (newValue) {
                            createParameters(newValue.options.parameters);
                        }
                    });

                    $scope.$watch('shortId', function(newValue) {
                        if (newValue) {
                            svc.getReportContextByShortId(newValue).then(function(reportContext) {
                                $scope.reportContext = reportContext;
                            });
                        }
                    });
                    $scope.$watch('reportId', function(newValue) {
                        if (newValue) {
                            svc.getReportContextByReportId(newValue).then(function(reportContext) {
                                $scope.reportContext = reportContext;
                            });
                        }
                    });
                },
                link: function (scope) {
                    scope.dateOnRender = function (parameter, date) {
                        var m = moment(date);

                        if (angular.isDefined(parameter.minDate)) {
                            if (m.isBefore(parameter.minDate)) {
                                return 'disabled';
                            }
                        }
                        if (angular.isDefined(parameter.maxDate)) {
                            if (m.isAfter(parameter.maxDate)) {
                                return 'disabled';
                            }
                        }
                        return '';
                    }
                }
            };
        }
    ]);

    module.directive('jsreportViewer', ['$sce', 'jsReportService', '$window',
        function ($sce, jsReportSvc, $window) {
            return {
                templateUrl: $window.razordata.siteprefix + 'angulartemplates/Reports/jsreport_viewer.html',
                scope: {
                    reportContext: '=',
                    downloadText: '=',
                    previewText: '=',
                    recipe: '=',
                    reportOutput: '='
                },
                controller: function ($scope) {
                    $scope.trustAsHtml = function (string) {
                        return $sce.trustAsHtml(string);
                    };
                },
                link: function (scope) {
                    scope.downloadText = scope.downloadText || 'Download';
                    scope.previewText = scope.previewText || 'Preview';
                    scope.ui = {
                        recipe: scope.recipe || 'html'
                    };

                    scope.$watch('reportContext', function(newValue) {
                        if (newValue && newValue.options && newValue.options.recipes) {
                            scope.ui.recipe = newValue.options.recipes[0][1];
                        }
                    });

                    scope.generateReport = function (preview) {
                        delete scope.htmlContent;
                        scope.generating = true;
                        jsReportSvc.generateReport(scope.reportContext, scope.ui.recipe, 'blob', preview).then(function(data) {
                            scope.generating = false;
                            if (data instanceof Blob) {
                                if (angular.isDefined(scope.reportOutput) && preview) {
                                    scope.reportOutput.url = $sce.trustAsResourceUrl(URL.createObjectURL(data));
                                } else if (preview) {
                                    $window.open(URL.createObjectURL(data), '_blank');
                                } else {
                                    // Force a download of the Blob using FileSaver.js ..
                                    $window.saveAs(data, scope.reportContext.reportName);
                                }
                            } else if (angular.isDefined(scope.reportOutput)) {
                                scope.reportOutput.html = $sce.trustAsHtml(data);
                            }
                        }).catch(function(err) {
                            scope.generating = false;
                            scope.error = err.message;
                        });
                    }
                }
            }
        }
    ]);
}) ();
