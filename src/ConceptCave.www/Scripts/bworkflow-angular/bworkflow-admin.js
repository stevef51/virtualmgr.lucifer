﻿var managementapp = angular.module('managementapp', ['ngResource',
    'player',
    'angularBootstrapNavTree',
    'angularFileUpload',
    'bworkflowAdminApi',
    'bootstrapCompat',
    'angular.filter',
    'dndLists',
    'siyfion.sfTypeahead',
    'angularUtils.directives.dirPagination',
    'languageTranslation',
    'oc.lazyLoad',
    'monospaced.qrcode',
    'vmgr-jsreport'
]);

managementapp.directive('elastic', [
    '$timeout',
    function ($timeout) {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function (scope, element, attr, ctrl) {
                var resize = function () {
                    return element[0].style.height = "" + element[0].scrollHeight + "px";
                };
                element.on("blur keyup change", resize);
                ctrl.$viewChangeListeners.push(resize);
                $timeout(resize, 0);
            }
        };
    }
]);

// MEDIA MANAGEMENT STUFF
managementapp.controller('mediamanagementCtrl', [
    '$scope', '$http', '$q', 'FileUploader', '$filter', '$rootScope', '$timeout', function ($scope, $http, $q, FileUploader, $filter, $rootScope, $timeout) {
        $scope.rooturl = window.razordata.siteprefix + 'Admin/MediaManagement/';
        $scope.shownotfiled = false;

        // This object is injected into our ABN directive, ABN will populate it with tree control functions we can call
        $scope.treeControl = {};

        $scope.getFolders = function () {
            $http.get($scope.rooturl + "Folders?notfiled=" + $scope.shownotfiled)
                .success(function (data) {

                    angular.forEach(data, function (node, key) {
                        $scope.setFolderIcons(node);
                    });

                    $scope.folders = data;

                    // And expand them all
                    $timeout(function () {
                        if ($scope.treeControl.expand_all)
                            $scope.treeControl.expand_all();
                    });
                });
        };

        // On language change, reload the folder list
        $rootScope.$on('$translateChangeSuccess', $scope.getFolders);

        $scope.setFolderIcons = function (node) {
            if (node.data.isfile == true) {
                return;
            }

            if (node.children.length > 0) {
                angular.forEach(node.children, function (child, key) {
                    $scope.setFolderIcons(child);
                });

                return;
            }

            node.iconLeaf = "icon-folder-open";
        }

        $scope.toggleNotFiled = function () {
            //$scope.shownotfiled = !$scope.shownotfiled;

            $scope.getFolders();
        }

        $scope.viewDetails = function (branch) {
            if (branch.data.isfile == false) {
                // not a massive amount to do
                $scope.editing = {
                    type: 'folder',
                    item: branch,
                };
            }
            else {
                // we need to load up the media details
                $scope.editing = { type: 'file', item: branch };
            }
        };

        $scope.newfolder = function () {
            var parameters = { parentid: null };

            $http.post($scope.rooturl + "FolderAdd", parameters)
                .success(function (data) {
                    data.iconLeaf = "icon-folder-open";
                    $scope.folders.push(data);
                });
        };

        $scope.$on('mediamanagement.reloadfolders', function (event) {
            $scope.getFolders();
        });

        $scope.$on('mediamanagement.deletefile', function (event, args) {
            $scope.getFolders();
        });

        var uploader = $scope.uploader = new FileUploader({
            scope: $scope
        });

        $scope.$on('mediamanagement.uploadfile', function (event, args) {
            $scope.results = null;
            $scope.uploadFailed = null;

            uploader.clearQueue();
            uploader.url = $scope.rooturl + "MediaUpload";

            $scope.uploadargs = args[0];

            uploader.onBeforeUploadItem = function (item) {
                Array.prototype.push.apply(item.formData, [$scope.uploadargs]);
            };

            uploader.onSuccessItem = function (fileItem, response, status, headers) {
                $scope.results = response;

                var fails = $filter('filter')(response, { success: false }, true);

                $scope.uploadFailed = fails.length > 0;

                if ($scope.uploadFailed == false && $scope.uploadargs.isnew == true) {
                    $scope.getFolders();
                }
            };

            $scope.uploadingfile = true;
        });
        $scope.uploadingfile = false;

        $scope.$on('mediamanagement.importfolder', function (event, args) {
            $scope.importfolderresult = null;
            $scope.uploadFailed = null;

            uploader.clearQueue();
            uploader.url = $scope.rooturl + "ImportFolder";

            $scope.uploadargs = args[0];

            // Default to Test Only, user should Untick to make persistent Import
            uploader.onAfterAddingFile = function (item) {
                item.testOnly = true;
            }

            uploader.onBeforeUploadItem = function (item) {
                Array.prototype.push.apply(item.formData, [$scope.uploadargs]);
                item.formData[0].testOnly = item.testOnly;
            };

            uploader.onSuccessItem = function (fileItem, response, status, headers) {
                fileItem.importfolderresult = response;

                $scope.getFolders();
            };

            $scope.importingfolder = true;
        });
        $scope.importingfolder = false;

        $scope.getFolders();
    }
]);

managementapp.directive('mediaFolderEditor', ['$http', '$timeout', 'languageTranslate',
    function ($http, $timeout, languageTranslate) {
        return {
            templateUrl: window.razordata.siteprefix + 'angulartemplates/admin/media/media_folder_editor.html',
            require: "ngModel",
            restrict: 'E',
            scope: {
                original: '=ngModel'
            },
            link: function (scope, element, attrs, ngModel) {
                scope.languageTranslate = languageTranslate;

                scope.save = function () {
                    if (scope.item.mediafolderpolicy == null) {
                        scope.item.mediafolderpolicy = {
                            __isnew: true,
                            mediafolderid: scope.item.id
                        };
                    }

                    scope.item.mediafolderpolicy.data = angular.toJson(scope.policies);

                    var parameters = { folder: scope.item, roles: scope.roles };

                    $http.post(scope.rooturl + "FolderSave", parameters)
                        .success(function (data) {
                            scope.original.label = scope.item.text;

                            scope.original = data;
                        });
                };

                scope.newfolder = function () {
                    var parameters = { parentid: scope.original.data.id };

                    $http.post(scope.rooturl + "FolderAdd", parameters)
                        .success(function (data) {
                            data.iconLeaf = "icon-folder-open";
                            scope.original.children.push(data);
                        });
                };

                scope.deletefolder = function () {
                    if (confirm("Deleting this folder is a permanent action and will delete all child folders and move any undeleted media to the not filed section. Are you sure you want to do this?") == true) {
                        var parameters = { id: scope.original.data.id };

                        $http({ url: scope.rooturl + "FolderDelete", method: "DELETE", params: parameters })
                            .success(function (data) {
                                // allot could have changed, so reload the entire folder list
                                scope.$emit('mediamanagement.reloadfolders');
                            });
                    }
                };

                scope.uploadfile = function () {
                    scope.$emit('mediamanagement.uploadfile', [{ id: null, parentid: scope.original.data.id, name: 'New Media', description: '', useversioning: scope.original.useversioning, isnew: true }]);
                };

                scope.addolderthanpolicy = function () {
                    var policy = { type: 'currentversion-older', action: { type: 'email' } };

                    scope.policies.push(policy);
                };

                scope.addnotreadforpolicy = function () {
                    var policy = { type: 'currentversion-notreadfor', action: { type: 'email' } };

                    scope.policies.push(policy);
                };

                scope.buildpolicies = function () {
                    scope.policies = [];

                    if (scope.item.mediafolderpolicy == null) {
                        return;
                    }

                    scope.policies = angular.fromJson(scope.item.mediafolderpolicy.data);
                };

                scope.buildroles = function () {
                    scope.roles = [];

                    angular.forEach(scope.item.mediafolderroles, function (value, index) {
                        scope.roles.push(value.aspnetrole.rolename);
                    });
                };

                scope.checkpolicies = function () {
                    if (confirm('This operation may result in people receiving emails or other actions being taken by the system. Are you sure you want to do this?') == false) {
                        return;
                    }

                    var parameters = { id: scope.original.data.id };
                    $http.post(scope.rooturl + "FolderCheckPolicies", parameters)
                        .success(function (data) {

                        });
                };

                scope.$watch("original", function () {
                    var parameters = { id: scope.original.data.id };
                    $http({ url: scope.rooturl + "Folder", method: 'GET', params: parameters })
                        .success(function (data) {
                            scope.item = data;

                            scope.buildpolicies();

                            scope.buildroles();
                        });

                });

                scope.exportUrl = function () {
                    return scope.rooturl + 'ExportFolder?id=' + scope.original.data.id;
                };

                scope.import = function () {
                    scope.$emit('mediamanagement.importfolder', [{ id: scope.original.data.id }]);
                }

                scope.rooturl = window.razordata.siteprefix + 'Admin/MediaManagement/';
            }
        };
    }]);

managementapp.directive('mediaFileEditor', ['$http', '$timeout', 'bworkflowApi',
    function ($http, $timeout, bworkflowApi) {
        return {
            templateUrl: window.razordata.siteprefix + 'angulartemplates/admin/media/media_file_editor.html',
            require: "ngModel",
            restrict: 'E',
            scope: {
                original: '=ngModel'
            },
            link: function (scope, element, attrs, ngModel) {
                scope.save = function () {
                    if (scope.item.mediapolicy == null) {
                        scope.item.mediapolicy = {
                            __isnew: true,
                            mediaid: scope.item.id
                        };
                    }

                    scope.item.mediapolicy.data = angular.toJson(scope.policies);

                    if (scope.selectedlabels == null) {
                        scope.selectedlabels = [];
                    }

                    var parameters = { media: scope.item, labels: scope.selectedlabels, roles: scope.roles, mediafile: scope.mediafile };

                    $http.post(scope.rooturl + "MediaSave", parameters)
                        .success(function (data) {
                            scope.original.label = scope.item.name;

                            scope.loadMedia();
                        });
                };

                scope.uploadfile = function () {
                    scope.$emit('mediamanagement.uploadfile', [{ id: scope.item.id, userversioning: scope.item.userversioning }]);
                };

                scope.deletefile = function () {
                    if (confirm('Deleting this media is a permanent action. Are you sure you want to do this?') == false) {
                        return;
                    }

                    var parameters = { id: scope.item.id };

                    $http({ url: scope.rooturl + "MediaDelete", method: "DELETE", params: parameters })
                        .success(function (data) {
                            scope.$emit('mediamanagement.deletefile', [{ item: scope.original }]);
                        });
                };

                scope.buildroles = function () {
                    scope.roles = [];

                    angular.forEach(scope.item.mediaroles, function (value, index) {
                        scope.roles.push(value.aspnetrole.rolename);
                    });
                };

                scope.loadMedia = function () {
                    scope.mediaurl = window.razordata.siteprefix + "Media?id=" + scope.original.data.id;

                    var parameters = { id: scope.original.data.id };
                    $http({ url: scope.rooturl + "Media", method: 'GET', params: parameters })
                        .success(function (data) {
                            scope.item = data.media;
                            scope.mediafile = data.mediafile;

                            scope.buildpolicies();

                            scope.selectedlabels = [];
                            angular.forEach(scope.item.labelmedias, function (value, index) {
                                scope.selectedlabels.push(value.labelid);
                            });

                            scope.buildroles();

                            if (scope.item.previewcount) {
                                scope.pageNumber = 1;
                                scope.calculatePageUrl();
                            }
                        });
                };

                scope.$watch("original", function () {
                    scope.loadMedia();
                });

                scope.formatDate = function (value) {
                    return moment(moment.utc(value).toDate()).format('Do MMM YYYY HH:mm:ss');
                };

                scope.versionurl = function (version) {
                    return window.razordata.siteprefix + "Media/Version?id=" + version.id;
                };

                scope.backwards = function ($event) {
                    if (scope.pageNumber == 1) {
                        return;
                    }

                    scope.pageNumber = scope.pageNumber - 1;
                    scope.calculatePageUrl();
                };

                scope.forwards = function ($event) {
                    if (scope.pageNumber >= scope.item.previewcount) {
                        return;
                    }

                    scope.pageNumber = scope.pageNumber + 1;
                    scope.calculatePageUrl();
                };

                scope.calculatePageUrl = function () {
                    scope.viewurl = bworkflowApi.getfullurl("~/MediaPreviewImage/" + scope.item.id + ".png?scale=1&pageindex=" + (scope.pageNumber - 1) + "&version=" + scope.item.version);
                };

                scope.addolderthanpolicy = function () {
                    var policy = { type: 'currentversion-older', action: { type: 'email' } };

                    scope.policies.push(policy);
                };

                scope.addnotreadforpolicy = function () {
                    var policy = { type: 'currentversion-notreadfor', action: { type: 'email' } };

                    scope.policies.push(policy);
                };

                scope.buildpolicies = function () {
                    scope.policies = [];

                    if (scope.item.mediapolicy == null) {
                        return;
                    }

                    scope.policies = angular.fromJson(scope.item.mediapolicy.data);
                };

                scope.rooturl = window.razordata.siteprefix + 'Admin/MediaManagement/';
            }
        };
    }]);

managementapp.directive('policiesList', [
    function () {
        return {
            templateUrl: window.razordata.siteprefix + 'angulartemplates/admin/policies/policies_list.html',
            scope: {
                policies: '=ngModel'
            },
            restrict: 'E',
            link: function (scope, elt, attrs, ngModel) {
                scope.deletepolicy = function (policy) {
                    var index = $.inArray(policy, scope.policies);

                    if (index == -1) {
                        return;
                    }

                    scope.policies.splice(index, 1);
                };

                scope.editpolicy = function (policy, index) {
                    if (scope.currentlyediting != null) {
                        scope.closepolicy(scope.currentlyediting);
                    }

                    scope.currentlyediting = { policy: policy, index: index };

                    scope.$broadcast('policy.edit', policy);
                };

                scope.closepolicy = function (policy) {
                    scope.$broadcast('policy.edit.close', scope.currentlyediting);

                    scope.currentlyediting = null;
                };

                scope.$on('policy.editemail', function (event, data) {
                    scope.email = data;
                    scope.showemail = true;
                });

                scope.currentlyediting = null;
                scope.showemail = false;
            }
        };
    }
]);

managementapp.directive('policyCurrentmediaversionOlder', [
    function () {
        return {
            templateUrl: window.razordata.siteprefix + 'angulartemplates/admin/policies/policy_currentmediaversion_older.html',
            scope: {
                policy: '=ngModel'
            },
            restrict: 'E',
            link: function (scope, elt, attrs, ngModel) {
                scope.periods = ['day', 'week', 'month', 'year'];

                if (angular.isDefined(scope.policy.age) == false) {
                    scope.policy.age = 1;
                }

                if (angular.isDefined(scope.policy.period) == false) {
                    scope.policy.period = 'year';
                }

                if (angular.isDefined(scope.policy.action) == false) {
                    scope.policy.action = {};
                    scope.policy.action.type = 'email';
                }

                if (angular.isDefined(scope.policy.action.labels) == false) {
                    scope.policy.action.labels = [];
                }

                if (angular.isDefined(scope.policy.action.email) == false) {
                    scope.policy.action.email = { subject: null, body: null };
                }

                scope.showemaileditor = function () {
                    scope.$emit('policy.editemail', scope.policy.action.email);
                };

                scope.$on('policy.edit', function (event, args) {
                    if (args != scope.policy) {
                        return;
                    }

                    scope.editing = true;
                });

                scope.$on('policy.edit.close', function (event, args) {
                    scope.editing = false;
                });

                scope.editing = false;
            }
        };
    }
]);

managementapp.directive('policyCurrentmediaversionNotreadfor', [
    function () {
        return {
            templateUrl: window.razordata.siteprefix + 'angulartemplates/admin/policies/policy_currentmediaversion_notreadfor.html',
            scope: {
                policy: '=ngModel'
            },
            restrict: 'E',
            link: function (scope, elt, attrs, ngModel) {

                scope.periods = ['day', 'week', 'month', 'year'];

                if (angular.isDefined(scope.policy.age) == false) {
                    scope.policy.age = 1;
                }

                if (angular.isDefined(scope.policy.period) == false) {
                    scope.policy.period = 'year';
                }

                if (angular.isDefined(scope.policy.labels) == false) {
                    scope.policy.labels = [];
                }

                if (angular.isDefined(scope.policy.action) == false) {
                    scope.policy.action = {};
                    scope.policy.action.type = 'email';
                }

                if (angular.isDefined(scope.policy.action.email) == false) {
                    scope.policy.action.email = { subject: null, body: null };
                }

                scope.showemaileditor = function () {
                    scope.$emit('policy.editemail', scope.policy.action.email);
                };

                scope.$on('policy.edit', function (event, args) {
                    if (args != scope.policy) {
                        return;
                    }

                    scope.editing = true;
                });

                scope.$on('policy.edit.close', function (event, args) {
                    scope.editing = false;
                });


                scope.editing = false;
            }
        };
    }
]);

// ROLES MANAGEMENT STUFF

managementapp.controller('rolesmanagementCtrl', [
    '$scope', '$http', '$q', '$filter', function ($scope, $http, $q, $filter) {

        $scope.rooturl = window.razordata.siteprefix + 'Admin/RolesManagement/';

        $scope.getAllRoles = function () {
            $http.get($scope.rooturl + "Roles")
                .success(function (data) {
                    $scope.roles = data;
                });
        };

        $scope.newRole = function () {
            $scope.original = null;
            $scope.roles.push('New Role');
        };

        $scope.$on('role.edit', function (event, args) {
            $scope.original = args;
            $scope.editing = args;
        });

        $scope.$on('role.save', function (event, args) {
            if ($scope.original == args) {
                return;
            }

            var parameters = { oldValue: $scope.original, newValue: args };

            $http.post($scope.rooturl + "RoleSave", parameters)
                .success(function (data) {
                    if (data == false) {
                        return;
                    }

                    var index = $.inArray($scope.original, $scope.roles);
                    $scope.roles[index] = args;
                });
        });

        $scope.getAllRoles();
    }
]);

managementapp.directive('rolesSidebar', [
    function () {
        return {
            templateUrl: window.razordata.siteprefix + 'angulartemplates/admin/roles/roles_sidebar.html',
            restrict: 'E',
            require: 'ngModel',
            scope: {
                roles: '=ngModel'
            },
            link: function (scope, elt, attrs, ngModel) {
                scope.editRole = function (role) {
                    scope.$emit('role.edit', role);
                };
            }
        };
    }
]);

managementapp.directive('roleEditor', [
    function () {
        return {
            templateUrl: window.razordata.siteprefix + 'angulartemplates/admin/roles/role_editor.html',
            restrict: 'E',
            require: 'ngModel',
            scope: {
                role: '=ngModel'
            },
            link: function (scope, elt, attrs, ngModel) {
                scope.save = function () {
                    scope.$emit('role.save', scope.role);
                }
            }
        };
    }
]);

managementapp.directive('roleSelector', ['bworkflowAdminApi',
    function (bworkflowAdminApi) {
        return {
            templateUrl: window.razordata.siteprefix + 'angulartemplates/admin/roles/role_selector.html',
            restrict: 'E',
            require: "ngModel",
            scope: {
                selected: '=ngModel'
            },
            link: function (scope, elt, attrs, ngModel) {
                scope.changeSelection = function (role) {
                    var index = $.inArray(role, scope.selected);

                    if (index == -1) {
                        scope.selected.push(role);
                    }
                    else {
                        scope.selected.splice(index, 1);
                    }
                };

                scope.isSelected = function (role) {
                    return $.inArray(role, scope.selected) != -1;
                };

                bworkflowAdminApi.getRoles().then(function (roles) {
                    scope.roles = roles;
                }, function (error) {

                });
            }
        };
    }
]);

// REPORT MANAGEMENT STUFF
managementapp.factory('reportmanagementService', ['$http', '$q', '$timeout', function ($http, $q, $timeout) {
    var _url = window.razordata.siteprefix + 'Admin/ReportManagement/';

    var svc = {
        reportServerUrl: null,
        getReportServerUrl: function () {
            var deferred = $q.defer();

            if (svc.reportServerUrl == null) {
                $http.get(_url + "ReportServerUrl")
                    .success(function (data) {
                        svc.reportServerUrl = data.url;

                        deferred.resolve(svc.reportServerUrl);
                    });
            }
            else {
                // we already have it, so wrap things in a promise and return and resolve
                $timeout(function () {
                    deferred.resolve(svc.reportServerUrl);
                });
            }

            return deferred.promise;
        },
        getReportClientId: function () {
            var deferred = $q.defer();

            svc.getReportServerUrl().then(function (reportUrl) {
                var parameters = '{"timeStamp":' + Date.now() + '}:';

                $http({
                    method: 'POST',
                    url: reportUrl + '/api/reports/clients',
                    data: parameters,
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                }).success(function (clientData) {
                    deferred.resolve(clientData);
                });
            });

            return deferred.promise;
        },
        getReportParameters: function (reportId) {
            var deferred = $q.defer();

            svc.getReportServerUrl().then(function (reportUrl) {
                svc.getReportClientId().then(function (clientData) {
                    var parameters = { parameterValues: {}, report: reportId };

                    $http({
                        method: 'POST',
                        url: reportUrl + '/api/reports/clients/' + clientData.clientId + '/parameters',
                        data: parameters
                    }).success(function (params) {
                        deferred.resolve(params);
                    });
                });
            });

            return deferred.promise;
        }
    }
    return svc;
}]);

managementapp.controller('reportmanagementCtrl', [
    '$scope', '$http', '$q', '$filter', 'FileUploader', function ($scope, $http, $q, $filter, FileUploader) {

        $scope.rooturl = window.razordata.siteprefix + 'Admin/ReportManagement/';
        $scope.reportServerUrl = null;
        $scope.editing = null;
        $scope.uploadingfile = false;

        $scope.reporttypes = [{ text: 'Telerik Report', type: 0 }, { text: 'Power BI Report', type: 1 }, { text: 'JsReport', type: 2 }];

        $scope.reportinformation = null;
        $scope.powerbireports = null;

        $scope.clearState = function () {
            $scope.powerbireports = null;
        };

        $scope.getAllReports = function () {
            $http.get($scope.rooturl + "Reports")
                .success(function (data) {
                    $scope.reports = data;

                    angular.forEach($scope.reports, function (report, index) {
                        $scope.calculateReportTypeName(report);
                    });
                });
        };

        $scope.getReportInfo = function (report) {
            if (report.type == $scope.reporttypes[0].type || report.type == $scope.reporttypes[2].type) {
                $scope.reportinformation = null;
                return;
            }

            $http.get($scope.rooturl + "ReportInformation?id=" + report.id)
                .success(function (data) {
                    $scope.reportinformation = data;
                });
        };

        $scope.calculateReportTypeName = function (report) {
            report.typename = $filter('filter')($scope.reporttypes, { type: report.type }, true)[0].text;
        };

        $scope.newReport = function (type) {
            
            var report = {
                __isnew: true,
                name: 'New Report',
                description: null,
                reportroles: [],
                type: type,
                shortid: null
            };

            $scope.calculateReportTypeName(report);

            $scope.clearState();

            $scope.editing = report;
        };

        $scope.deleteReport = function (report) {
            if (report == null) {
                return;
            }

            $http.delete($scope.rooturl + "ReportDelete?id=" + report.id)
                .success(function (data) {
                    $scope.editing = null;

                    var index = $.inArray(report, $scope.reports);

                    if (index != -1) {
                        $scope.reports.splice(index, 1);
                    }
                });
        };

        $scope.listPowerBiReports = function () {
            $http.get($scope.rooturl + "GetPowerBiReports")
                .success(function (data) {
                    $scope.powerbireports = data;
                });
        };

        $scope.$on('report.edit', function (event, args) {

            $scope.editing = args;

            $scope.clearState();

            $scope.getReportInfo($scope.editing);
        });

        $scope.$on('report.save', function (event, args) {
            var parameters = { report: args.report, roles: args.roles };

            $http.post($scope.rooturl + "ReportSave", parameters)
                .success(function (data) {
                    if (data.success == false) {
                        if (data.message == "ReportAlreadyExists") {
                            alert("Report already exists. Please choose another report");
                        }
                        return false;
                    }
                    
                    if (args.report.__isnew == true) {
                        $scope.reports.push(args.report);
                    }

                    angular.copy(data, args.report);
                    $scope.calculateReportTypeName(args.report);
                });
        });

        $scope.$on('report.setauthentication', function (event, args) {
            $http.post($scope.rooturl + "SetConnectionData", args)
                .success(function (data) {
                    alert('done');
                });
        });

        $scope.getAllReports();
    }
]);

managementapp.directive('reportSidebar', [
    function () {
        return {
            templateUrl: window.razordata.siteprefix + 'angulartemplates/admin/report/report_sidebar.html',
            restrict: 'E',
            require: 'ngModel',
            scope: {
                reports: '=ngModel'
            },
            link: function (scope, elt, attrs, ngModel) {
                scope.editReport = function (report) {
                    scope.$emit('report.edit', report);
                };
            }
        };
    }
]);

managementapp.directive('reportEditor', ['FileUploader', '$filter', 'reportmanagementService', '$timeout',
    function (FileUploader, $filter, reportmanagementService, $timeout) {
        return {
            templateUrl: window.razordata.siteprefix + 'angulartemplates/admin/report/report_editor.html',
            restrict: 'E',
            require: 'ngModel',
            scope: {
                report: '=ngModel',
                reportinformation: '='
            },
            controller: function ($scope) {
                $scope.uploadingfile = false;

                $scope.uploadFile = function () {
                    $scope.results = null;
                    $scope.uploadFailed = null;

                    var uploader = $scope.uploader = new FileUploader({
                        scope: $scope,
                        url: "ReportManagement/ReportUpload"
                    });

                    $scope.uploadargs = { id: $scope.report.id };

                    uploader.onBeforeUploadItem = function (item) {
                        Array.prototype.push.apply(item.formData, [$scope.uploadargs]);
                    };

                    uploader.onSuccessItem = function (fileItem, response, status, headers) {
                        $scope.results = response;

                        var fails = $filter('filter')(response, { success: false }, true);

                        $scope.uploadFailed = fails.length > 0;
                    };

                    $scope.uploadingfile = true;
                };
            },
            link: function (scope, element, attrs, ngModel) {
                function tryParse() {
                    try {
                        var js = eval('(' + scope.report.configuration + ')');
                        delete scope.parseErrors;
                    } catch (e) {
                        scope.parseErrors = e.message;
                    }
                }

                scope.roles = [];
                scope.showauthentication = false;
                scope.reportParams = null;

                scope.save = function () {
                    scope.$emit('report.save', { report: scope.report, roles: scope.roles });
                };

                scope.buildroles = function () {
                    scope.roles = [];

                    angular.forEach(scope.report.reportroles, function (value, index) {
                        scope.roles.push(value.name);
                    });
                };

                scope.showSetAuthentication = function () {
                    scope.showauthentication = true;
                    scope.authentication = {
                        connection: '',
                        username: '',
                        password: '',
                        id: scope.report.id
                    };
                };

                scope.setAuthentication = function (authentication) {
                    scope.$emit('report.setauthentication', authentication);
                };

                scope.$watch('report', function (newValue, oldVale) {
                    scope.buildroles();
                    if (scope.report.type == 0) {
                        reportmanagementService.getReportParameters(scope.report.id).then(function (params) {
                            scope.reportParams = params;
                        });
                    }

                    var txtArea = $('#jsonConfiguration', element);
                    if (txtArea.length && !scope.codeMirrorEditor) {
                        scope.codeMirrorEditor = CodeMirror.fromTextArea(txtArea[0], {
                            lineNumbers: true,
                            theme: "neat",
                            mode: 'application/ld+json',
                            onChange: function (instance) {
                                var newValue = instance.getValue();
                                if (newValue !== scope.report.configuration) {
                                    scope.$evalAsync(function () {
                                        scope.report.configuration = newValue;
                                        tryParse();
                                    });
                                }
                            }
                        });
                    }
                    scope.codeMirrorEditor.setValue(scope.report.configuration || '{}');

                });

                scope.$on('tab_selected', function() {
                    if (scope.codeMirrorEditor) {
                        scope.codeMirrorEditor.refresh();
                    }
                });
            }
        };
    }
]);

// REPORT LIBRAR STUFF (this is the controllers/directives that allow the user to
// use the telerik reporting
managementapp.controller('reportLibraryCtrl', [
    '$scope', '$http', '$q', '$filter', '$timeout', function ($scope, $http, $q, $filter, $timeout) {
        $scope.rooturl = window.razordata.siteprefix + 'Reports/';

        $scope.reports = [];
        $scope.viewing = null;

        $scope.getReports = function () {
            $http.get($scope.rooturl + "Reports")
                .success(function (data) {
                    
                    $scope.reports = data;
                });
        }

        $scope.viewReportWithAccessKey = function (report) {
            $http.get($scope.rooturl + "AccessKey?id=" + report.id)
                .success(function (data) {
                    report.powerbi = data;

                    $scope.viewing = report;
                });
        }

        $scope.viewReport = function (report) {
            if (report.type == 1) {
                $scope.viewReportWithAccessKey(report);
                return;
            }
            
            $scope.viewing = report;
        }

        $scope.fullscreen = function () {
            $scope.$broadcast('power-bi.fullscreen');
        }

        $scope.getReports();
    }
]);

// CRON STUFF
managementapp.controller('cronmanagementCtrl', [
    '$scope', '$http', '$q', '$filter', function ($scope, $http, $q, $filter) {

        $scope.rooturl = window.razordata.siteprefix + 'Admin/CronManagement/';

        $scope.jobTypes = [
            'CompletedWorkingDocumentCleanUp',
            'IncompleteWorkingDocumentCleanUp',
            'MediaPolicyManagement',
            'RosterScheduleApproval',
            'RosterEndOfDayProcessing',
            'RosterTimedEvents',
            'Custom'];

        $scope.shownewcronjob = false;

        $scope.editmode = 'job';

        $scope.server = { state: 'Unknown' };

        $scope.getServerState = function () {
            $http.get($scope.rooturl + "Server")
                .success(function (data) {
                    $scope.server = data;
                });
        };

        $scope.getAllCrons = function () {
            $http.get($scope.rooturl + "Groups")
                .success(function (data) {
                    $scope.groups = data;
                });
        };

        $scope.$on('cron.edit', function (event, args) {
            $scope.editJob(args.group.name, args.job.name);
        });

        $scope.$on('cron.edit.server', function (event) {
            $scope.editmode = 'server';

            $scope.getServerState();
        });

        $scope.$on('cron.delete', function (event, args) {
            $http.delete($scope.rooturl + "Job?group=" + args.group + "&job=" + args.job)
                .success(function (data) {
                    $scope.getAllCrons();
                });
        });

        $scope.$on('cron.trigger.add', function (event, args) {
            var params = { group: $scope.editing.group, job: $scope.editing.name, trigger: args };

            $http.post($scope.rooturl + "Trigger", params)
                .success(function (data) {
                    $scope.editJob($scope.editing.group, $scope.editing.name);
                });
        });

        $scope.$on('cron.trigger.delete', function (event, args) {
            $http.delete($scope.rooturl + "Trigger?group=" + args.group + "&trigger=" + args.name)
                .success(function (data) {
                    var index = $.inArray(args, $scope.editing.triggers);

                    if (index != -1) {
                        $scope.editing.triggers.splice(index, 1);
                    }
                });
        });

        $scope.$on('cron.trigger.pause', function (event, args) {
            var params = { group: args.group, trigger: args.name };

            $http.post($scope.rooturl + "PauseTrigger", params)
                .success(function (data) {
                    angular.copy(data, args);
                });
        });

        $scope.$on('cron.trigger.resume', function (event, args) {
            var params = { group: args.group, trigger: args.name };

            $http.post($scope.rooturl + "ResumeTrigger", params)
                .success(function (data) {
                    angular.copy(data, args);
                });
        });

        $scope.$on('cron.trigger.fire', function (event, args) {
            var params = { group: args.group, trigger: args.name };

            $http.post($scope.rooturl + "FireTrigger", params)
                .success(function (data) {
                    alert('Trigger fired');
                });
        });

        $scope.editJob = function (group, job) {
            $scope.editmode = 'job';
            $http.get($scope.rooturl + "Job?group=" + group + "&job=" + job)
                .success(function (data) {
                    $scope.editing = data;

                    angular.forEach(data.triggers, function (trigger) {
                        if (trigger.type != 'Simple') {
                            return;
                        }

                        trigger.friendlyInterval = moment.duration(trigger.interval).humanize();
                    });
                });
        }

        $scope.showNewJob = function () {
            $scope.newjob = {
                __isnew: true,
                name: null,
                description: null,
                type: 'Custom'
            };

            $scope.shownewcronjob = true;
        };

        $scope.confirmNewJob = function () {
            var parameters = { job: $scope.newjob };

            $http.post($scope.rooturl + "Job", parameters)
                .success(function (data) {
                    $scope.getAllCrons();
                    $scope.shownewcronjob = false;
                });
        };

        $scope.shutdownServer = function () {
            $http.post($scope.rooturl + "ServerStandby")
                .success(function (data) {
                    scope.server = data;
                });
        };

        $scope.startServer = function () {
            $http.post($scope.rooturl + "ServerStart")
                .success(function (data) {
                    scope.server = data;
                });
        };

        $scope.getAllCrons();
    }
]);

managementapp.directive('cronSidebar', [
    function () {
        return {
            templateUrl: window.razordata.siteprefix + 'angulartemplates/admin/cron/cron_sidebar.html',
            restrict: 'E',
            require: 'ngModel',
            scope: {
                groups: '=ngModel'
            },
            link: function (scope, elt, attrs, ngModel) {
                scope.editJob = function (job, group) {
                    scope.$emit('cron.edit', { job: job, group: group });
                };

                scope.editServer = function () {
                    scope.$emit('cron.edit.server');
                }
            }
        };
    }
]);

managementapp.directive('cronEditor', [
    function () {
        return {
            templateUrl: window.razordata.siteprefix + 'angulartemplates/admin/cron/cron_editor.html',
            restrict: 'E',
            require: 'ngModel',
            scope: {
                job: '=ngModel'
            },
            link: function (scope, elt, attrs, ngModel) {
                scope.triggerTypes = ['Simple', 'Cron'];

                scope.shownewtrigger = false;

                scope.save = function () {
                    scope.$emit('cron.save', scope.job);
                };

                scope.delete = function () {
                    if (confirm('Are you sure you want to delete this scheduled job') == false) {
                        return;
                    }

                    scope.$emit('cron.delete', { group: scope.job.group, job: scope.job.name });
                };

                scope.showAddTrigger = function () {
                    scope.newtrigger = {
                        __isnew: true,
                        type: 'Simple',
                        description: null,
                        starttime: null,
                        endtime: null,
                        interval: '1.0:0:0',
                        intervalDay: 1,
                        intervalHour: 0,
                        intervalMin: 0,
                        intervalSec: 0,
                        cronexpression: null,
                        datamap: {
                            publishedresourceid: null,
                            reviewerid: null,
                            revieweeid: null,
                            items: []
                        }
                    };

                    scope.shownewtrigger = true;
                };

                scope.confirmNewTrigger = function () {
                    if (scope.newtrigger.type == 'Simple') {
                        scope.newtrigger.interval = scope.newtrigger.intervalDay + '.' + scope.newtrigger.intervalHour + ':' + scope.newtrigger.intervalMin + ':' + scope.newtrigger.intervalSec;
                    }

                    scope.newtrigger.datamap.reviewerid = scope.newtrigger.reviewer.Id;

                    if (angular.isDefined(scope.newtrigger.reviewee) == true && scope.newtrigger.reviewee != null) {
                        scope.newtrigger.datamap.revieweeid = scope.newtrigger.reviewee.Id;
                    }

                    scope.$emit('cron.trigger.add', scope.newtrigger);

                    scope.shownewtrigger = false;
                };

                scope.deleteTrigger = function (trigger) {
                    if (confirm("Are you sure you want to delete this trigger") == false) {
                        return;
                    }

                    scope.$emit('cron.trigger.delete', trigger);
                };

                scope.pauseTrigger = function (trigger) {
                    if (confirm("Are you sure you want to pause this trigger") == false) {
                        return;
                    }

                    scope.$emit('cron.trigger.pause', trigger);
                };

                scope.resumeTrigger = function (trigger) {
                    if (confirm("Are you sure you want to resume this trigger") == false) {
                        return;
                    }

                    scope.$emit('cron.trigger.resume', trigger);
                };

                scope.fireTrigger = function (trigger) {
                    if (confirm("Are you sure you want to fire this trigger now") == false) {
                        return;
                    }

                    scope.$emit('cron.trigger.fire', trigger);
                };
            },
            controller: function ($scope) {
                $scope.watchcontainer = {};
                $scope.watchcontainer.currentWorkflowObject = {};

                $scope.workflowSuggestion = new Bloodhound({
                    datumTokenizer: function (d) { return Bloodhound.tokenizers.whitespace(d.name); },
                    queryTokenizer: Bloodhound.tokenizers.whitespace,
                    remote: window.razordata.siteprefix + 'Admin/TaskTypeManagement/SearchPublishedResources?query=%QUERY'
                });

                $scope.workflowSuggestion.initialize();

                $scope.workflowDataset = {
                    displayKey: 'name',
                    source: $scope.workflowSuggestion.ttAdapter(),
                    templates: {
                        suggestion: Handlebars.compile('<strong>{{name}}</strong><br/><i>{{publishinggroup.name}}</i>')
                    }
                };

                $scope.$watch('watchcontainer.currentWorkflowObject', function (newWorkflow) {
                    if (!newWorkflow || angular.isDefined(newWorkflow.id) == false) {
                        return;
                    }

                    // we need to make sure we don't already have this checklist mapped into us
                    $scope.newtrigger.datamap.publishedresourceid = newWorkflow.id;
                });
            }
        };
    }
]);

// NEWS FEED STUFF
managementapp.controller('newsfeedmanagementCtrl', [
    '$scope', '$http', '$q', 'FileUploader', '$filter', function ($scope, $http, $q, FileUploader, $filter) {

        $scope.rooturl = window.razordata.siteprefix + 'Admin/NewsFeedManagement/';

        $scope.viewNewsItems = function (channel) {
            var parameters = { channel: channel };
            $http({ url: $scope.rooturl + "ChannelItems", method: 'GET', params: parameters })
                .success(function (data) {
                    $scope.items = data;
                });
        };

        $scope.getChannels = function () {
            $http.get($scope.rooturl + "Channels")
                .success(function (data) {
                    $scope.channels = data;
                });
        };

        $scope.editItem = function (item) {
            item.validto = moment(item.validto).format('DD-MM-YYYY');
            $scope.showeditor = true;
            $scope.editing = item;
        };

        $scope.newItem = function () {
            $scope.editing = {
                __isnew: true,
                channel: $scope.currentChannel
            };

            $scope.showeditor = true;
        };

        $scope.saveItem = function (item) {
            if ($scope.newsfeeditem.$valid == false) {
                return;
            }

            item.validto = moment(item.validto, 'DD-MM-YYYY').format();

            var parameters = { item: item };
            $http.post($scope.rooturl + "ChannelItem", parameters)
                .success(function (data) {
                    angular.copy(data, item);

                    $scope.getChannels();
                    $scope.viewNewsItems($scope.currentChannel);

                    $scope.showeditor = false;
                });
        };

        $scope.toLocalDate = function (value) {
            return moment(moment.utc(value).toDate()).format('LLL');
        };

        $scope.$on('newsfeed.select.channel', function (event, data) {
            $scope.currentChannel = data;
            $scope.viewNewsItems(data);
        });

        $scope.currentChannel = null;

        $scope.showeditor = false;
        $scope.getChannels();
    }
]);

managementapp.directive('channelSidebar', [
    function () {
        return {
            templateUrl: window.razordata.siteprefix + 'angulartemplates/admin/newsfeed/channel_sidebar.html',
            restrict: 'E',
            require: 'ngModel',
            scope: {
                channels: '=ngModel'
            },
            link: function (scope, elt, attrs, ngModel) {
                scope.selectChannel = function (channel) {
                    scope.$emit('newsfeed.select.channel', channel);
                };


            }
        };
    }
]);

// ROSTER MANAGEMENT STUFF

managementapp.controller('rostermanagementCtrl', [
    '$scope', '$http', '$q', '$filter', function ($scope, $http, $q, $filter) {

        $scope.rooturl = window.razordata.siteprefix + 'Admin/RosterManagement/';
        $scope.allowautomation = false;

        $scope.getAllRosters = function () {
            $http.get($scope.rooturl + "Rosters")
                .success(function (data) {
                    $scope.rosters = data.rosters;
                    $scope.allowautomation = data.allowautomation;
                    $scope.events = data.events;
                    $scope.tasktypes = data.tasktypes;
                    $scope.rosterconditionactions = data.rosterconditionactions;
                    $scope.timezones = data.timezones;
                });
        };

        $scope.newRoster = function () {
            $scope.original = null;
            $scope.rosters.push({
                __isnew: true,
                id: 0,
                starttime: '1970-01-01T08:00:00',
                endtime: '1970-01-01T17:30:00',
                name: 'New Roster',
                includelunchintimesheetdurations: true
            });
        };

        $scope.$on('roster.edit', function (event, args) {
            $scope.original = args;
            $scope.editing = angular.copy(args);
        });

        $scope.$on('roster.save', function (event, args) {
            var parameters = { roster: $scope.editing };

            $http.post($scope.rooturl + "RosterSave", parameters)
                .success(function (data) {
                    $scope.editing = data;
                    angular.copy(data, $scope.original);
                });
        });

        $scope.$on('roster.clearscheduleapprovalrun', function (event, args) {
            var parameters = { roster: $scope.editing.id };

            $http.post($scope.rooturl + "RosterClearScheduleApprovalRun", parameters)
                .success(function (data) {
                    $scope.editing = data;
                    angular.copy(data, $scope.original);
                });
        });

        $scope.$on('roster.clearendofdayrun', function (event, args) {
            var parameters = { roster: $scope.editing.id };

            $http.post($scope.rooturl + "RosterClearEndOfDayRun", parameters)
                .success(function (data) {
                    $scope.editing = data;
                    angular.copy(data, $scope.original);
                });
        });

        $scope.getAllRosters();
    }
]);

managementapp.directive('rosterSidebar', [
    function () {
        return {
            templateUrl: window.razordata.siteprefix + 'angulartemplates/admin/roster/roster_sidebar.html',
            restrict: 'E',
            require: 'ngModel',
            scope: {
                rosters: '=ngModel'
            },
            link: function (scope, elt, attrs, ngModel) {
                scope.editRoster = function (roster) {
                    scope.$emit('roster.edit', roster);
                };
            }
        };
    }
]);

managementapp.directive('rosterEditor', ['bworkflowAdminApi', '$filter',
    function (bworkflowAdminApi, $filter) {
        return {
            templateUrl: window.razordata.siteprefix + 'angulartemplates/admin/roster/roster_editor.html',
            restrict: 'E',
            require: 'ngModel',
            scope: {
                roster: '=ngModel',
                allowautomation: '=',
                eventtypes: '=',
                tasktypes: '=',
                rosterconditionactions: '=',
                timezones: '='
            },
            link: function (scope, elt, attrs, ngModel) {
                scope.endofdaystatuses = [
                    { id: 0, name: 'Finished by management' },
                    { id: 1, name: 'Resume next shift for user' }
                ];

                scope.scheduleapprovalstatuses = [
                    { id: 0, name: 'Unapproved' },
                    { id: 1, name: 'Approved' }
                ];

                scope.roundingmethods = [
                    { id: 0, name: 'No Rounding' },
                    { id: 1, name: 'Round Down' },
                    { id: 2, name: 'Round Up' },
                    { id: 3, name: 'Round To Nearest' },
                    { id: 4, name: 'Round To Shortest' },
                    { id: 5, name: 'Round To Longest' }
                ];

                scope.selectionmethods = [
                    { id: 0, name: 'Use Actual Times' },
                    { id: 1, name: 'Use Scheduled Times' },
                    { id: 2, name: 'Minimise Times' },
                    { id: 3, name: 'Maximise Times' }
                ];

                scope.editingcondition = false;
                scope.condition = null;

                scope.$watch('roster', function (newValue, oldVale) {
                    scope.roster.__automaticscheduleapprovaltime = null;
                    scope.roster.__automaticendofdaytime = null;
                    scope.roster.__starttime = moment(scope.roster.starttime, 'YYYY-MM-DDThh:mm:ss').toDate();
                    scope.roster.__endtime = moment(scope.roster.endtime, 'YYYY-MM-DDThh:mm:ss').toDate();

                    if (scope.roster.automaticscheduleapprovaltime != null) {
                        scope.roster.__automaticscheduleapprovaltime = moment(scope.roster.automaticscheduleapprovaltime, 'YYYY-MM-DDThh:mm:ss').toDate();
                    }

                    if (scope.roster.automaticendofdaytime != null) {
                        scope.roster.__automaticendofdaytime = moment(scope.roster.automaticendofdaytime, 'YYYY-MM-DDThh:mm:ss').toDate();
                    }

                    angular.forEach(scope.roster.rostereventconditions, function (condition) {
                        if (condition.data != null) {
                            condition.__data = JSON.parse(condition.data);
                            condition.__data.__time = moment(condition.__data.time, 'HH:mm:ss').toDate();
                        }
                        else {
                            condition.__data = {};
                        }

                        angular.forEach(condition.rostereventconditionactions, function (action) {
                            if (action.data != null) {
                                action.__data = JSON.parse(action.data);
                            }
                            else {
                                action.__data = {};
                            }
                        });
                    });
                });

                scope.save = function () {
                    if (scope.roster.__automaticscheduleapprovaltime == '') {
                        scope.roster.automaticscheduleapprovaltime = null;
                    }
                    else {
                        scope.roster.automaticscheduleapprovaltime = moment(scope.roster.__automaticscheduleapprovaltime).format('YYYY-MM-DDTHH:mm:ss') + '.000Z';;
                    }

                    if (scope.roster.__automaticendofdaytime == '') {
                        scope.roster.automaticendofdaytime = null;
                    }
                    else {
                        scope.roster.automaticendofdaytime = moment(scope.roster.__automaticendofdaytime).format('YYYY-MM-DDTHH:mm:ss') + '.000Z';;
                    }

                    scope.roster.starttime = moment(scope.roster.__starttime).format('YYYY-MM-DDTHH:mm:ss') + '.000Z';
                    scope.roster.endtime = moment(scope.roster.__endtime).format('YYYY-MM-DDTHH:mm:ss') + '.000Z';

                    angular.forEach(scope.roster.rostereventconditions, function (condition) {
                        condition.__data.time = null;
                        if (scope.isConditionTimed(condition) && condition.__data.__time != null && condition.__data.__time != '') {
                            condition.__data.time = moment(condition.__data.__time).format('HH:mm:ss');
                        }

                        condition.data = JSON.stringify(condition.__data);

                        angular.forEach(condition.rostereventconditionactions, function (action) {
                            action.data = JSON.stringify(action.__data);
                        });
                    });

                    scope.$emit('roster.save', scope.roster);
                }

                scope.isConditionTimed = function (condition) {
                    var et = $filter('filter')(scope.eventtypes, { id: condition.rosterprojectjobtaskevents }, true);

                    if (et.length == 0) {
                        return null;
                    }

                    return et[0].timed;
                }

                scope.clearScheduleApprovalRun = function () {
                    if (confirm('This will result in the schedule approval being run for this roster the next time the relevant scheduled task runs. Are you sure you want to do this?') == false) {
                        return;
                    }

                    scope.$emit('roster.clearscheduleapprovalrun', scope.roster);
                };

                scope.clearEndOfDayRun = function () {
                    if (confirm('This will result in the end of day process being run for this roster the next time the relevant scheduled task runs. Are you sure you want to do this?') == false) {
                        return;
                    }

                    scope.$emit('roster.clearendofdayrun', scope.roster);
                };

                scope.getEventType = function (id) {
                    var t = $filter('filter')(scope.eventtypes, { id: id }, true);

                    if (t.length == 0) {
                        return null;
                    }

                    return t[0].name;
                };

                scope.getConditionText = function (data) {
                    if (data == null) {
                        return "";
                    }

                    var result = "All Tasks";

                    if (data.levels != null || data.tasktypes.length > 0) {
                        result = "";
                        if (data.levels != null) {
                            result = "Tasks with level " + data.levels;

                            if (data.tasktypes.length > 0) {
                                result = result + " and ";
                            }
                        }

                        if (data.tasktypes.length > 0) {
                            result = result + "task type must be one of ";
                            angular.forEach(data.tasktypes, function (type) {
                                var t = $filter('filter')(scope.tasktypes, { id: type.id }, true);

                                result = result + t[0].name + " ";
                            });
                        }
                    }

                    if (data.time != null) {
                        result = result + " @ " + moment(data.__time).format("hh:mm a");
                    }

                    return result;
                };

                scope.addCondition = function () {
                    var condition = {
                        __isnew: true,
                        __isadded: false,
                        rosterid: scope.roster.id,
                        rosterprojectjobtaskeventid: null,
                        condition: 'taskfilter',
                        data: null,
                        __data: {
                            levels: null,
                            tasktypes: [],
                            __time: null,
                            time: null
                        },
                        rostereventconditionactions: []
                    };

                    scope.editCondition(condition);
                };

                scope.editCondition = function (condition) {
                    scope.editingcondition = true;
                    scope.condition = condition;
                    var types = angular.copy(scope.tasktypes);

                    angular.forEach(scope.condition.__data.tasktypes, function (type) {
                        var t = $filter('filter')(types, { id: type.id }, true);

                        if (t.length > 0) {
                            t[0].selected = true;
                        }
                    });

                    scope.condition.tasktypes = types;
                };

                scope.removeCondition = function (c) {
                    if (confirm('Are you sure you want to remove this event handler') == false) {
                        return;
                    }

                    var index = $.inArray(c, scope.roster.rostereventconditions);

                    if (index != -1) {
                        scope.roster.rostereventconditions.splice(index, 1);
                    }
                };

                scope.saveCondition = function () {
                    if (scope.conditionEditor.$valid == false) {
                        return;
                    }

                    if (scope.condition.__data.levels == "") {
                        scope.condition.__data.levels = null;
                    }

                    scope.condition.__data.tasktypes = [];
                    angular.forEach(scope.condition.tasktypes, function (type) {
                        if (type.selected) {
                            scope.condition.__data.tasktypes.push({ id: type.id });
                        }
                    });

                    if (scope.condition.__isadded == false) {
                        scope.roster.rostereventconditions.push(scope.condition);

                        scope.condition.__isadded = true;
                    }

                    scope.editingcondition = false;
                    scope.condition = null;
                };

                scope.getConditionActionText = function (action) {
                    var a = $filter('filter')(scope.rosterconditionactions, { id: action.action }, true);

                    return a[0].name;
                };

                scope.addConditionAction = function (type) {
                    var action = {
                        __isnew: true,
                        action: type.id,
                        data: null,
                        __data: {}
                    };

                    scope.condition.rostereventconditionactions.push(action);
                };

                scope.removeConditionAction = function (action) {
                    if (confirm('Are you sure you want to remove this action') == false) {
                        return;
                    }

                    var index = $.inArray(action, scope.condition.rostereventconditionactions);

                    if (index != -1) {
                        scope.condition.rostereventconditionactions.splice(index, 1);
                    }
                };

                bworkflowAdminApi.getHierarchies().then(function (hierarchies) {
                    scope.hierarchies = hierarchies;
                }, function (error) {

                });
            }
        };
    }
]);

// this directive is used to wrap up the construction of the directive used to display the config
// UI for the action.
managementapp.directive('rosterConditionActionWrapper', ['$compile', '$filter',
    function ($compile, $filter) {
        return {
            restrict: 'E',
            require: 'ngModel',
            scope: {
                action: '=ngModel',
                rosterconditionactions: '='
            },
            link: function (scope, elt, attrs, ngModel) {
                var a = $filter('filter')(scope.rosterconditionactions, { id: scope.action.action }, true);

                var content = '<' + a[0].directive + ' ng-model="action"></' + a[0].directive + '>';

                var dirElement = $compile(content)(scope);
                $(elt).append(dirElement);
            }
        }
    }]
);

managementapp.directive('rosterConditionActionRunchecklist', ['bworkflowAdminApi', '$filter',
    function (bworkflowAdminApi, $filter) {
        return {
            templateUrl: window.razordata.siteprefix + 'angulartemplates/admin/roster/roster_condition_action_runchecklist.html',
            restrict: 'E',
            require: 'ngModel',
            scope: {
                action: '=ngModel',
            },
            link: function (scope, elt, attrs, ngModel) {
                if (angular.isDefined(scope.action.__data.publishedresourceid) == false) {
                    scope.action.__data.publishedresourceid = null;
                }
            }
        }
    }]
);

managementapp.directive('rosterConditionActionAudit', ['bworkflowAdminApi', '$filter',
    function (bworkflowAdminApi, $filter) {
        return {
            templateUrl: window.razordata.siteprefix + 'angulartemplates/admin/roster/roster_condition_action_audit.html',
            restrict: 'E',
            require: 'ngModel',
            scope: {
                action: '=ngModel',
            },
            link: function (scope, elt, attrs, ngModel) {
                if (angular.isDefined(scope.action.__data.enforceevensampling) == false) {
                    scope.action.__data.enforceevensampling = false;
                }

                if (angular.isDefined(scope.action.__data.evensamplinggroup) == false) {
                    scope.action.__data.evensamplinggroup = 'default';
                }

                // hard coding this for the moment, we should supply a selector so that the
                // user can select what type of relationship should be created between the tasks
                scope.action.__data.relationshiptypeid = 1;
            }
        }
    }]
);

managementapp.directive('rosterConditionActionSendcommunications', ['bworkflowAdminApi', '$filter',
    function (bworkflowAdminApi, $filter) {
        return {
            templateUrl: window.razordata.siteprefix + 'angulartemplates/admin/roster/roster_condition_action_sendcommunications.html',
            restrict: 'E',
            require: 'ngModel',
            scope: {
                action: '=ngModel',
            },
            link: function (scope, elt, attrs, ngModel) {
                scope.destinationTypes = [
                    { name: 'Supervisor', type: 0, custom: false },
                    { name: 'Owner', type: 1, custom: false },
                    { name: 'Custom', type: 2, custom: true }];

                scope.sendRules = [
                    { name: 'all the time', type: 0 },
                    { name: 'Not Started', type: 1 },
                    { name: 'Finished by System', type: 2 }
                ];

                if (angular.isDefined(scope.action.__data.emailsubject) == false) {
                    scope.action.__data.emailsubject = '';
                }

                if (angular.isDefined(scope.action.__data.emailbody) == false) {
                    scope.action.__data.emailbody = '';
                }

                if (angular.isDefined(scope.action.__data.smsbody) == false) {
                    scope.action.__data.smsbody = '';
                }

                if (angular.isDefined(scope.action.__data.customemail) == false) {
                    scope.action.__data.customemail = '';
                }

                if (angular.isDefined(scope.action.__data.custommobile) == false) {
                    scope.action.__data.custommobile = '';
                }

                if (angular.isDefined(scope.action.__data.destination) == false) {
                    scope.action.__data.destination = '';
                }

                if (angular.isDefined(scope.action.__data.sendrule) == false) {
                    scope.action.__data.sendrule = '';
                }
            }
        }
    }]
);

// SCHEDULE MANAGEMENT STUFF
managementapp.factory('cronBuilderService', [function () {
    var svc = {
        types: [
            'Monthly',
            'Yearly',
            'Every X Days'
        ],
        months: [
            { name: 'January', id: 1 },
            { name: 'February', id: 2 },
            { name: 'March', id: 3 },
            { name: 'April', id: 4 },
            { name: 'May', id: 5 },
            { name: 'June', id: 6 },
            { name: 'July', id: 7 },
            { name: 'August', id: 8 },
            { name: 'September', id: 9 },
            { name: 'October', id: 10 },
            { name: 'November', id: 11 },
            { name: 'December', id: 12 },
        ],
        daysofweek: [
            { name: 'Sun', id: 0 },
            { name: 'Mon', id: 1 },
            { name: 'Tue', id: 2 },
            { name: 'Wed', id: 3 },
            { name: 'Thr', id: 4 },
            { name: 'Fri', id: 5 },
            { name: 'Sat', id: 6 },
        ]
    }
    return svc;
}]);

managementapp.controller('schedulemanagementCtrl', [
    '$scope',
    '$http',
    '$q',
    '$filter',
    'bworkflowAdminApi',
    'languageTranslate',
    'FileUploader',
    'cronBuilderService',
    function ($scope, $http, $q, $filter, bworkflowAdminApi, languageTranslate, FileUploader, cronBuilderService) {

        $scope.rooturl = window.razordata.siteprefix + 'Admin/ScheduleManagement/';

        $scope.languagetranslate = languageTranslate;

        $scope.rosterid = -1;
        $scope.movetorosterid = -1;

        $scope.changepropertychangecalendarrule = false;
        $scope.changepropertycalendarrule = null;
        $scope.changepropertystarttime = { value: null };
        $scope.changepropertystarttimewindowminutes = { value: null };
        $scope.changepropertyestimateddurationminutes = { value: null };
        $scope.changepropertychangelevel = false;
        $scope.changepropertylevel = null;
        $scope.changepropertychangesite = false;
        $scope.changepropertysite = null;
        $scope.changepropertylengthindays = 1;

        $scope.roleid = -1;
        $scope.level = 0;
        $scope.selecting = false;
        $scope.alltasks = null; // all tasks returned from the server
        $scope.types = [];


        $scope.editingScheduledTask = false;
        $scope.isinaction = false;
        $scope.currentScheduledTask = null;
        $scope.workloadingenabled = false;
        $scope.currentworkloadingstandards = null;

        $scope.showbatchchangeproperties = false;

        $scope.showImportScheduleDialog = false;

        $scope.showsettimes = false;
        $scope.showoverriderole = false;
        $scope.times = null;
        $scope.overrideRole = null;

        $scope.uploader = new FileUploader();
        $scope.uploader.url = window.razordata.siteprefix + 'Admin/ScheduleManagement/Import';
        $scope.uploader.alias = 'file';

        $scope.uploader.onSuccessItem = function (fileItem, response, status, headers) {
            $scope.importresults = response;

            $scope.importfailed = response.errors.length > 0;
            if (!$scope.importfailed) {
                // reload the schedule now
                $scope.load($scope.rosterid, $scope.roleid);
            }
        };


        $scope.changeTaskType = function (taskTypeId) {
            // User changed the Task Type, we must copy across new Task Type properties
            var type = $scope.types.find(function (t) {
                return t.id == taskTypeId;
            });
        }

        $scope.exportUrl = function () {
            return window.razordata.siteprefix + "Admin/ScheduleManagement/Export?rosterId=" + $scope.rosterid + "&roleId=" + $scope.roleid;
        };

        $scope.showImportSchedule = function () {
            $scope.uploader.clearQueue();
            $scope.showImportScheduleDialog = true;
        };

        $scope.closeImportSchedule = function () {
            $scope.showImportScheduleDialog = true;
        };

        $scope.uploadSchedule = function () {
            $scope.uploader.uploadAll();
        }

        $scope.getWorkloadingEnabled = function () {
            $http.get($scope.rooturl + "WorkloadingModule")
                .success(function (data) {
                    $scope.workloadingenabled = data == 'true';
                });
        };

        $scope.getAllRosters = function () {
            $http.get($scope.rooturl + "Rosters")
                .success(function (data) {
                    $scope.rosters = data;
                });
        };

        $scope.getAllTypes = function () {
            $http.get(window.razordata.siteprefix + 'Admin/ScheduleManagement/Types')
                .success(function (data) {
                    $scope.types = data;
                });
        };

        $scope.$watch('rosterid', function (newValue, oldValue) {
            if (newValue == -1) {
                return;
            }

            var parameters = { rosterid: newValue };

            $http.get($scope.rooturl + "Roles", { params: parameters })
                .success(function (data) {
                    angular.forEach(data, function (d) {
                        var users = "";
                        angular.forEach(d.users, function (u) {
                            users += u + ", ";
                        });

                        users = users.slice(0, -2);

                        if (d.users.length > 0) {
                            d.roleAndName = d.name + " (" + users + ")";
                        }
                        else {
                            d.roleAndName = d.name;
                        }

                    });

                    $scope.roles = data;
                });
        });

        $scope.$watch('roleid', function (newValue, oldValue) {
            if (newValue == -1) {
                return;
            }

            $scope.load($scope.rosterid, newValue);
        });

        function decorateServerTask(item) {
            $scope.parseCronExpression(item);
            $scope.doTranslate(item);
            $scope.appendTypeName(item);

            item.startsAt = { value: null };
            if (item.starttime != null) {
                item.startsAt.value = moment(item.starttime, 'HH:mm:ss').toDate();
            }
            item.starttimewindowminutes = { value: null };
            if (item.starttimewindowseconds != null) {
                item.starttimewindowminutes.value = item.starttimewindowseconds / 60;
            }
            item.estimateddurationminutes = { value: null };
            if (item.estimateddurationseconds != null) {
                item.estimateddurationminutes.value = item.estimateddurationseconds / 60;
            }
        }

        $scope.load = function (rosterid, roleid) {
            var parameters = { rosterid: rosterid, roleid: roleid };

            $http.get($scope.rooturl + "Schedules", { params: parameters })
                .success(function (data) {
                    $scope.users = data.users;
                    $scope.alltasks = data.tasks;

                    angular.forEach($scope.alltasks, function (item) {
                        decorateServerTask(item);
                    });
                });
        };

        $scope.$watch('languagetranslate.currentLanguage', function (newValue, oldValue) {
            angular.forEach($scope.alltasks, function (item) { $scope.doTranslate(item); });
        });

        $scope.$on('schedule.save', function (event, args) {
            if (args.__isnew == true && args.calendarrule.frequency == 'Cron' && angular.isDefined(args.calendarrule.cronbuilder) == false) {
                $scope.parseCronExpression(args);
                $scope.buildCronExpression(args);
            }

            $scope.saveScheduledTask(args);
        });

        $scope.saveScheduledTask = function (task) {
            if (task.__isnew == true) {
                task.rosterid = $scope.rosterid;
                task.roleid = $scope.roleid;
            }

            // to prevent things getting converted around by timezones we convert out times
            // to strings. We send the date back saying its UTC, which it isn't the user has selected
            // in the timezone relevant to them/server, however the server side code can handle the interpretation
            // of the timezone side of things.
            task.calendarrule.startdate = moment(task.calendarrule.startdate).format('YYYY-MM-DD') + 'T00:00:00.000Z';

            if (task.calendarrule.enddate != null && task.calendarrule.enddate != '') {
                task.calendarrule.enddate = moment(task.calendarrule.enddate).format('YYYY-MM-DD') + 'T00:00:00.000Z';
            }

            if (task.startsAt && task.startsAt.value) {
                task.starttime = moment(task.startsAt.value).format('HH:mm:ss');
            }

            task.starttimewindowseconds = null;
            if (task.starttimewindowminutes && task.starttimewindowminutes.value != null) {
                task.starttimewindowseconds = task.starttimewindowminutes.value * 60;
            }

            task.estimateddurationseconds = null;
            if (task.estimateddurationminutes && task.estimateddurationminutes.value != null) {
                task.estimateddurationseconds = task.estimateddurationminutes.value * 60;
            }

            var saving = task;

            var parameters = { model: { task: task, workloading: $scope.currentworkloadingstandards } };

            $http.post($scope.rooturl + "Task", parameters)
                .success(function (data) {
                    var index = $.inArray(saving, $scope.alltasks);
                    var task = $scope.alltasks[index];

                    angular.copy(data, saving);

                    decorateServerTask(saving);
                });
        };

        $scope.appendTypeName = function (item) {
            var type = $scope.types.find(function (t) {
                return t.id == item.tasktypeid;
            });

            item.tasktypename = type.name;
        };

        $scope.doTranslate = function (item) {
            item.translatedName = item.name;
            item.translatedDescription = item.description;
            var lt = languageTranslate;

            var translation = item.translated.find(function (m) {
                return m.culturename == lt.currentLanguage.culturename;
            });

            if (translation) {
                item.translatedName = translation.name;
                item.translatedDescription = translation.description;
            }
        };

        $scope.buildCronExpression = function (task) {
            if (task.calendarrule.frequency == 'Cron') {
                if (task.calendarrule.cronbuilder.type == 'Monthly') {
                    task.calendarrule.cronexpression = "0 0 0 " + task.calendarrule.cronbuilder.day + " 1/" + task.calendarrule.cronbuilder.months + " ? *";
                }
                else if (task.calendarrule.cronbuilder.type == 'Yearly') {
                    // its yearly
                    task.calendarrule.cronexpression = "0 0 0 " + task.calendarrule.cronbuilder.day + ' ' + task.calendarrule.cronbuilder.month + " ? *";
                }
                else {
                    // its every X Days
                    task.calendarrule.cronexpression = "0 0 0 1/" + task.calendarrule.cronbuilder.day + ' * ? *';
                }
            }
        };

        $scope.parseCronExpression = function (task) {
            // we need to convert the stuff that's meant to represent a date, to a date object
            task.calendarrule.startdate = moment.utc(task.calendarrule.startdate).toDate();

            if (task.calendarrule.enddate != null) {
                task.calendarrule.enddate = moment.utc(task.calendarrule.enddate).toDate();
            }

            if (task.calendarrule.frequency != 'Cron') {
                return;
            }

            task.calendarrule.cronbuilder = {
                day: 1,
                months: 1,
                type: 'Monthly'
            };

            if (task.calendarrule.cronexpression != null && task.calendarrule.cronexpression != '') {
                var parts = task.calendarrule.cronexpression.split(' ');

                if (parts.length == 7) {
                    var d = parseInt(parts[3]);
                    var m = parts[4].split('/');

                    if (m.length == 2) {
                        task.calendarrule.cronbuilder = {
                            day: parseInt(d),
                            months: parseInt(m[1]),
                            type: 'Monthly',
                        };

                        task.calendarrule.cronbuilder.friendly = 'Repeats every ' + task.calendarrule.cronbuilder.months + ' months on day ' + task.calendarrule.cronbuilder.day;
                    }
                    else if (parts[4] == '*') {
                        task.calendarrule.cronbuilder = {
                            day: parseInt(parts[3].split('/')[1]),
                            month: null,
                            type: 'Every X Days'
                        };

                        task.calendarrule.cronbuilder.friendly = 'Repeat every ' + task.calendarrule.cronbuilder.day + ' days';
                    }
                    else {
                        task.calendarrule.cronbuilder = {
                            day: parseInt(d),
                            month: parseInt(m[0]),
                            type: 'Yearly'
                        };

                        var sm = $filter('filter')(cronBuilderService.months, { id: task.calendarrule.cronbuilder.month }, true);

                        task.calendarrule.cronbuilder.friendly = 'Repeat each year on ' + sm[0].name + ' ' + task.calendarrule.cronbuilder.day;
                    }
                }
            }
        };

        $scope.loadScheduleTaskWorkloading = function (task) {
            var parameters = { scheduledTaskId: task.id };

            task.workloadingstandards = null;

            $http.get($scope.rooturl + "Workloading", { params: parameters })
                .success(function (data) {
                    if (data.error == true) {
                        $scope.currentworkloadingstandards = null;
                        return;
                    }

                    $scope.currentworkloadingstandards = data;
                });
        }

        $scope.$watch('editingScheduledTask', function (newvalue, oldvalue) {
            if (newvalue == false) {
                $scope.cancelBatchAction();
            }
        });

        $scope.$on('schedule.edit', function (event, data) {
            $scope.editingScheduledTask = true;
            $scope.isinaction = false;

            $scope.searchedsite = { Id: data.siteid };
            $scope.originalScheduledTask = data;
            $scope.currentScheduledTask = angular.copy(data);
            $scope.currentworkloadingstandards = null;
            $scope.loadScheduleTaskWorkloading($scope.currentScheduledTask);

            $scope.currentScheduledTask.selectedLabels = [];
            angular.forEach($scope.currentScheduledTask.projectjobscheduledtasklabels, function (value, key) {
                $scope.currentScheduledTask.selectedLabels.push(value.labelid);
            });
        });

        $scope.$on('schedule.times', function (event, data) {
            var dayofweek = ($filter('filter')($scope.daysofweek, { name: data.name }, true));

            var parameters = {
                day: dayofweek[0].id,
                roleid: $scope.roleid,
                rosterid: $scope.rosterid
            };

            var type = data.type;

            $http.get($scope.rooturl + "Times", { params: parameters })
                .success(function (data) {
                    if (type == "times") {
                        $scope.times = data;
                        $scope.showsettimes = true;
                    }
                    else {
                        $scope.overrideRole = data;
                        $scope.showoverriderole = true;
                    }
                });
        });

        $scope.completeSetTimesOrOverrideRole = function () {
            var params = {};

            if ($scope.showsettimes == true) {
                params = { day: $scope.times };
            }
            else {
                params = { day: $scope.overrideRole };
            }

            $http.post($scope.rooturl + "Times", params)
                .success(function (response) {
                    $scope.showsettimes = false;
                    $scope.showoverriderole = false;
                }).error(function (response, status, headers, config) {

                });
        };

        $scope.$watch('searchedsite', function (newValue, oldValue) {
            if ($scope.currentScheduledTask == null) {
                return;
            }

            if (newValue == null) {
                $scope.currentScheduledTask.siteid = null;
                return;
            }

            if (angular.isDefined(newValue.Id) == false) {
                return;
            }

            $scope.currentScheduledTask.siteid = newValue.Id;

            // since the site has changed, the work loading we have loaded is not valid
            // for the moment, the user can't change the site and edit work loading in the same hit
            // they need to save and re-open
            $scope.currentworkloadingstandards = null;
        });

        $scope.commitScheduledTask = function () {
            $scope.currentScheduledTask.projectjobscheduledtasklabels.length = 0;
            angular.forEach($scope.currentScheduledTask.selectedLabels, function (value, key) {
                $scope.currentScheduledTask.projectjobscheduledtasklabels.push({ labelid: value, projectjobscheduledtaskid: $scope.currentScheduledTask.id, __isnew: true });
            });

            $scope.buildCronExpression($scope.currentScheduledTask);

            angular.copy($scope.currentScheduledTask, $scope.originalScheduledTask);

            $scope.saveScheduledTask($scope.originalScheduledTask);

            $scope.$broadcast('schedule.saved', $scope.originalScheduledTask);

            $scope.editingScheduledTask = false;
        };

        $scope.confirmDelete = function () {
            if (confirm('This operation cannot be undone, are you sure you want to delete this scheduled task?') == false) {
                return;
            }

            $scope.delete($scope.originalScheduledTask);
        };

        $scope.delete = function (task) {
            $http({ url: $scope.rooturl + "Task", method: "DELETE", params: { id: task.id } })
                .success(function (data) {
                    var index = $.inArray(task, $scope.alltasks);

                    if (index != -1) {
                        $scope.alltasks.splice(index, 1);

                        $scope.editingScheduledTask = false;
                    }
                });
        };

        $scope.addAuditSampling = function () {
            var a = {
                __isnew: true,
                group: 'default',
                samplecount: 0
            };

            $scope.currentScheduledTask.audittaskconditionactions.push(a);
        };

        $scope.selectAll = function (select) {
            angular.forEach($scope.alltasks, function (task) {
                task.selected = select;
            });
        };

        $scope.startBatchAction = function (type) {
            $scope.selecting = true;

            if (type == 'move') {
                $scope.showMoveToOtherRole();
            }
            else if (type == 'copy') {
                $scope.showCopyToOtherRole();
            }
            else if (type == 'changeproperties') {
                $scope.showChangeProperties();
            }
        };

        $scope.showCompleteBatchAction = function (type) {
            if (type == 'move' || type == 'copy') {
                $scope.showbatchmovecopy = true;
            }
            else if (type == 'changeproperties') {
                $scope.showbatchchangeproperties = true;
            }
        };

        $scope.completeBatchAction = function () {
            selecteds = [];

            angular.forEach($scope.alltasks, function (task) {
                if (task.selected) {
                    selecteds.push(task.id);
                }
            });

            if ($scope.isinaction == 'move') {
                $scope.moveTask(selecteds, $scope.movetorosterid, $scope.movetoroleid);
            }
            else if ($scope.isinaction == 'copy') {
                $scope.copyTask(selecteds, $scope.movetorosterid, $scope.movetoroleid);
            }
            else if ($scope.isinaction == 'changeproperties') {
                $scope.changeProperties(selecteds);
            }

            $scope.showbatchmovecopy = false;
            $scope.showbatchchangeproperties = false;
            $scope.cancelBatchAction();
        };

        $scope.cancelBatchAction = function () {
            $scope.selecting = false;
            $scope.backOutOfAction();
        };

        $scope.$watch('movetorosterid', function (newValue, oldValue) {
            if (newValue == -1) {
                return;
            }

            var parameters = { rosterid: newValue };

            $http.get($scope.rooturl + "Roles", { params: parameters })
                .success(function (data) {
                    $scope.movetoroles = data;
                });
        });

        $scope.backOutOfAction = function () {
            $scope.isinaction = false;
        };

        $scope.showMoveToOtherRole = function () {
            $scope.isinaction = 'move';
            $scope.movetorosterid = $scope.rosterid;
            $scope.movetoroleid = null;
        };

        $scope.showCopyToOtherRole = function () {
            $scope.isinaction = 'copy';
            $scope.movetorosterid = $scope.rosterid;
            $scope.movetoroleid = null;
        };

        $scope.showChangeProperties = function () {
            $scope.isinaction = 'changeproperties';

            $scope.changepropertychangecalendarrule = false;
            $scope.changepropertychangelevel = false;
            $scope.changepropertychangesite = false;
            $scope.changepropertylevel = null;
            $scope.changepropertysite = null;
            $scope.changepropertycalendarrule = $scope.newcalendarrule();
            $scope.changepropertystarttime = { value: null };
            $scope.changepropertystarttimewindowminutes = { value: null };
            $scope.changepropertyestimateddurationminutes = { value: null };
            $scope.changepropertylengthindays = 1;
        };

        $scope.newcalendarrule = function () {
            var cr = {
                __isnew: true,
                executesunday: false,
                executemonday: false,
                executetuesday: false,
                executewednesday: false,
                executethursday: false,
                executefriday: false,
                executesaturday: false,
                frequency: 'Daily',
                period: 1,
                startdate: moment().startOf('day').toDate()
            };

            return cr;
        };

        $scope.takeAction = function () {
            if ($scope.isinaction == 'move') {
                $scope.moveTask([$scope.originalScheduledTask.id], $scope.movetorosterid, $scope.movetoroleid);
            }
            else if ($scope.isinaction == 'copy') {
                $scope.copyTask([$scope.originalScheduledTask.id], $scope.movetorosterid, $scope.movetoroleid);
            }
        };

        $scope.moveTask = function (ids, toroster, torole) {
            $http({ url: $scope.rooturl + "Move", method: "POST", params: { id: ids, toRoster: toroster, toRole: torole } })
                .success(function (data) {
                    toRemove = [];
                    angular.forEach($scope.alltasks, function (task) {
                        for (var i = 0; i < ids.length; i++) {
                            if (ids[i] == task.id) {
                                toRemove.push(task);
                            }
                        }
                    })

                    for (var i = 0; i < toRemove.length; i++) {
                        var index = $.inArray(toRemove[i], $scope.alltasks);

                        if (index != -1) {
                            $scope.alltasks.splice(index, 1);

                            $scope.editingScheduledTask = false;
                        }
                    }
                });
        };

        $scope.copyTask = function (ids, toroster, torole) {
            $http({ url: $scope.rooturl + "Copy", method: "POST", params: { id: ids, toRoster: toroster, toRole: torole } })
                .success(function (data) {
                    $scope.isinaction = false;
                });
        };

        $scope.changeProperties = function (ids) {
            var params = {
                id: ids,
                siteid: null,
                level: null,
                calendarrule: null,
                lengthindays: null
            };

            if ($scope.changepropertychangesite) {
                params.siteid = $scope.changepropertysite.Id;
            }

            if ($scope.changepropertychangelevel) {
                params.level = $scope.changepropertylevel;
            }

            if ($scope.changepropertychangecalendarrule) {
                params.calendarrule = $scope.changepropertycalendarrule;
                params.lengthindays = $scope.changepropertylengthindays;

                params.starttime = moment($scope.changepropertystarttime.value).format('HH:mm:ss');
                params.starttimewindowseconds = null;
                if ($scope.changepropertystarttimewindowminutes.value != null) {
                    params.starttimewindowseconds = $scope.changepropertystarttimewindowminutes.value * 60;
                }

                params.estimateddurationseconds = null;
                if ($scope.changepropertyestimateddurationminutes.value != null) {
                    params.estimateddurationseconds = $scope.changepropertyestimateddurationminutes.value * 60;
                }
            }

            $http({
                url: $scope.rooturl + "ChangeProperties",
                method: "POST",
                data: params
            })
                .success(function (data) {
                    $scope.isinaction = false;
                    $scope.load($scope.rosterid, $scope.roleid);
                });
        };

        $scope.getAllRosters();
        $scope.getAllTypes();
        $scope.getWorkloadingEnabled();
    }
]);

managementapp.directive('scheduleCalendarruleEditor', ['bworkflowAdminApi',
    '$timeout',
    '$filter',
    'languageTranslate',
    '$http',
    'cronBuilderService',
    function (bworkflowAdminApi, $timeout, $filter, languageTranslate, $http, cronBuilderService) {
        return {
            templateUrl: window.razordata.siteprefix + 'angulartemplates/admin/schedule/schedule_calendarrule_editor.html',
            restrict: 'E',
            require: 'ngModel',
            scope: {
                calendarrule: '=ngModel',
                lengthindays: '=',
                starttime: '=',
                starttimewindowminutes: '=',
                estimateddurationminutes: '=',
                minimumduration: '='
            },
            link: function (scope, elt, attrs, ngModel) {

                scope.cronbuilders = cronBuilderService.types;
                scope.cronbuildermonths = cronBuilderService.months;
                scope.daysofweek = cronBuilderService.daysofweek;

                scope.anySelections = function () {
                    if (scope.calendarrule.frequency == 'Cron') {
                        return true;
                    }

                    return scope.calendarrule.executesunday ||
                        scope.calendarrule.executemonday ||
                        scope.calendarrule.executetuesday ||
                        scope.calendarrule.executewednesday ||
                        scope.calendarrule.executethursday ||
                        scope.calendarrule.executefriday ||
                        scope.calendarrule.executesaturday;
                };

                scope.$watch('calendarrule.executesunday', function (newvalue, oldvalue) {
                    if (newvalue == false && scope.anySelections() == false) {
                        scope.calendarrule.executesunday = true;
                    }
                });

                scope.$watch('calendarrule.executemonday', function (newvalue, oldvalue) {
                    if (newvalue == false && scope.anySelections() == false) {
                        scope.calendarrule.executemonday = true;
                    }
                });

                scope.$watch('calendarrule.executetuesday', function (newvalue, oldvalue) {
                    if (newvalue == false && scope.anySelections() == false) {
                        scope.calendarrule.executetuesday = true;
                    }
                });

                scope.$watch('calendarrule.executewednesday', function (newvalue, oldvalue) {
                    if (newvalue == false && scope.anySelections() == false) {
                        scope.calendarrule.executewednesday = true;
                    }
                });

                scope.$watch('calendarrule.executethursday', function (newvalue, oldvalue) {
                    if (newvalue == false && scope.anySelections() == false) {
                        scope.calendarrule.executethursday = true;
                    }
                });

                scope.$watch('calendarrule.executefriday', function (newvalue, oldvalue) {
                    if (newvalue == false && scope.anySelections() == false) {
                        scope.calendarrule.executefriday = true;
                    }
                });

                scope.$watch('calendarrule.executesaturday', function (newvalue, oldvalue) {
                    if (newvalue == false && scope.anySelections() == false) {
                        scope.calendarrule.executesaturday = true;
                    }
                });

            }
        }
    }
]);

managementapp.directive('scheduleSelectRosterAndRole', ['bworkflowAdminApi',
    '$timeout',
    '$filter',
    'languageTranslate',
    '$http',
    function (bworkflowAdminApi, $timeout, $filter, languageTranslate, $http) {
        return {
            templateUrl: window.razordata.siteprefix + 'angulartemplates/admin/schedule/schedule_select_rosterandrole.html',
            restrict: 'E',
            require: 'ngModel',
            scope: {
                rosterid: '=ngModel',
                roleid: '=',
                rosters: '='
            },
            link: function (scope, elt, attrs, ngModel) {
                scope.rooturl = window.razordata.siteprefix + 'Admin/ScheduleManagement/';

                scope.roles = [];

                scope.$watch('rosterid', function (newValue, oldValue) {
                    if (newValue == -1) {
                        return;
                    }

                    var parameters = { rosterid: newValue };

                    $http.get(scope.rooturl + "Roles", { params: parameters })
                        .success(function (data) {
                            scope.roles = data;
                        });
                });
            }
        }
    }
]);

managementapp.directive('scheduleSetTimes', ['bworkflowAdminApi',
    '$timeout',
    '$filter',
    'languageTranslate',
    '$http',
    function (bworkflowAdminApi, $timeout, $filter, languageTranslate, $http) {
        return {
            templateUrl: window.razordata.siteprefix + 'angulartemplates/admin/schedule/schedule_set_times.html',
            restrict: 'E',
            require: 'ngModel',
            scope: {
                day: '=ngModel',
            },
            link: function (scope, elt, attrs, ngModel) {
                scope.calculateFormats = function () {
                    scope.day.starttimestring = scope.day.moments.starttime.format('HH:mm');
                    scope.day.endtimestring = scope.day.moments.endtime.format('HH:mm');
                    scope.day.lunchstartstring = scope.day.moments.lunchstart.format('HH:mm');
                    scope.day.lunchendstring = scope.day.moments.lunchend.format('HH:mm');
                };

                scope.calculateTimes = function () {
                    scope.day.starttime = scope.day.moments.starttime.format('YYYY-MM-DDTHH:mm:ss');
                    scope.day.endtime = scope.day.moments.endtime.format('YYYY-MM-DDTHH:mm:ss');
                    scope.day.lunchstart = scope.day.moments.lunchstart.format('YYYY-MM-DDTHH:mm:ss');
                    scope.day.lunchend = scope.day.moments.lunchend.format('YYYY-MM-DDTHH:mm:ss');

                    //socpe.calculateFormats();
                };

                scope.$watch('day', function (newValue, oldValue) {
                    if (newValue == null) {
                        return;
                    }

                    scope.day.moments = {};

                    scope.day.moments.starttime = moment(scope.day.starttime);
                    scope.day.moments.endtime = moment(scope.day.endtime);
                    scope.day.moments.lunchstart = moment(scope.day.lunchstart);
                    scope.day.moments.lunchend = moment(scope.day.lunchend);

                    scope.calculateFormats();
                });
            }
        }
    }
]);

managementapp.directive('scheduleSetOverriderole', ['bworkflowAdminApi',
    '$timeout',
    '$filter',
    'languageTranslate',
    '$http',
    function (bworkflowAdminApi, $timeout, $filter, languageTranslate, $http) {
        return {
            templateUrl: window.razordata.siteprefix + 'angulartemplates/admin/schedule/schedule_set_overriderole.html',
            restrict: 'E',
            require: 'ngModel',
            scope: {
                day: '=ngModel',
                roles: '='
            },
            link: function (scope, elt, attrs, ngModel) {
                scope.$watch('day', function (newValue, oldValue) {
                    if (newValue == null) {
                        return;
                    }

                    newValue.useDifferentRole = false;

                    if (newValue.overrideroleid != null) {
                        newValue.useDifferentRole = true;
                    }
                });

                scope.$watch('day.useDifferentRole', function (newValue, oldValue) {
                    if (newValue == false) {
                        scope.day.overrideroleid = null;
                    }
                });
            }
        }
    }
]);

managementapp.directive('scheduleDay', ['bworkflowAdminApi', '$timeout', '$filter', 'languageTranslate',
    function (bworkflowAdminApi, $timeout, $filter, languageTranslate) {
        return {
            templateUrl: window.razordata.siteprefix + 'angulartemplates/admin/schedule/schedule_day.html',
            restrict: 'E',
            require: 'ngModel',
            scope: {
                tasks: '=ngModel',
                level: '&level',
                types: '=',
                selecting: '='
            },
            link: function (scope, elt, attrs, ngModel) {
                scope.day = attrs.day;

                if (scope.tasks == null) {
                    scope.tasks = [];
                }

                scope.filterTasks = function () {
                    scope.filteredTasks = $filter('orderBy')($filter('filter')(scope.tasks, scope.taskFilter, true), "sortorder");
                };

                scope.taskFilter = function (task) {
                    switch (scope.day) {
                        case "Sun":
                            return task.calendarrule.executesunday;
                            break;
                        case "Mon":
                            return task.calendarrule.executemonday;
                            break;
                        case "Tue":
                            return task.calendarrule.executetuesday;
                            break;
                        case "Wed":
                            return task.calendarrule.executewednesday;
                            break;
                        case "Thr":
                            return task.calendarrule.executethursday;
                            break;
                        case "Fri":
                            return task.calendarrule.executefriday;
                            break;
                        case "Sat":
                            return task.calendarrule.executesaturday;
                            break;
                    }
                };

                scope.$watch('selecting', function (newvalue, oldvalue) {
                    if (newvalue == false) {
                        angular.forEach(scope.filteredTasks, function (task) {
                            task.selected = undefined;
                        });
                    }
                });

                scope.$watchCollection('tasks', function (newValues, oldValue) {
                    scope.filterTasks();
                });

                scope.$on('schedule.saved', function (event, data) {
                    scope.filterTasks();
                });

                scope.editTask = function (task) {
                    if (scope.selecting == true) {
                        if (angular.isDefined(task.selected) == true) {
                            task.selected = !task.selected;
                        }
                        else {
                            task.selected = true;
                        }

                        return;
                    }

                    scope.$emit('schedule.edit', task);
                };

                scope.newcalendarrule = function () {
                    var cr = {
                        __isnew: true,
                        executesunday: false,
                        executemonday: false,
                        executetuesday: false,
                        executewednesday: false,
                        executethursday: false,
                        executefriday: false,
                        executesaturday: false,
                        frequency: 'Daily',
                        period: 1,
                        startdate: moment().startOf('day').toDate()
                    };

                    return cr;
                };

                scope.newTask = function (type) {
                    var cr = scope.newcalendarrule();

                    switch (scope.day) {
                        case "Sun":
                            cr.executesunday = true;
                            break;
                        case "Mon":
                            cr.executemonday = true;
                            break;
                        case "Tue":
                            cr.executetuesday = true;
                            break;
                        case "Wed":
                            cr.executewednesday = true;
                            break;
                        case "Thr":
                            cr.executethursday = true;
                            break;
                        case "Fri":
                            cr.executefriday = true;
                            break;
                        case "Sat":
                            cr.executesaturday = true;
                            break;
                    }

                    var task = {
                        __isnew: true,
                        tasktypeid: type.id,
                        name: '',
                        calendarrule: cr,
                        editingname: true,
                        level: scope.level(),
                        lengthindays: type.defaultlengthindays,
                        startsAt: { value: null },
                        starttimewindowseconds: null,
                        starttimewindowminutes: { value: null },
                        estimateddurationseconds: null,
                        estimateddurationminutes: { value: null },
                        translated: [{ __isnew: true, culturename: languageTranslate.currentLanguage.culturename }]
                    };

                    scope.editing = task;

                    scope.tasks.push(task);

                    $timeout(function () {
                        elt.find('textarea').focus();
                    });
                };

                scope.cancelNew = function () {
                    var index = $.inArray(scope.editing, scope.tasks);

                    if (index != -1) {
                        scope.tasks.splice(index, 1);
                    }

                    scope.editing = null;
                };

                scope.approveNew = function ($event) {
                    if (angular.isDefined($event)) {
                        $event.stopPropagation();
                    }

                    if (scope.editing.name == null || scope.editing.name == '') {
                        // you've got to supply a name
                        return;
                    }

                    // force in the name that's been enterd into the translation table (so the entity and the translation for the current culture match)
                    scope.editing.translated[0].name = scope.editing.name;

                    scope.editing.editingname = false;

                    scope.$emit('schedule.save', scope.editing);

                    scope.editing = null;
                };

                scope.blr = function ($event) {
                    if ($event.relatedTarget == null) {
                        scope.cancelNew();
                        return;
                    }

                    if ($($event.relatedTarget).hasClass('approveNew') == false) {
                        scope.cancelNew();
                        return;
                    }

                    if ($($event.relatedTarget).parent()[0] != $($event.target).parent()[0]) {
                        scope.cancelNew();
                        return;
                    }
                };

                scope.keyup = function ($event) {
                    if ($event.which === 13) {
                        // Enter pressed
                        scope.approveNew();
                    }
                    else if ($event.which === 27) {
                        // Escape pressed
                        scope.cancelNew();
                    }
                };

                scope.setTimes = function () {
                    scope.$emit('schedule.times', { name: scope.day, type: 'times' });
                };

                scope.setOverrideRole = function () {
                    scope.$emit('schedule.times', { name: scope.day, type: 'overriderole' });
                };

                scope.filterTasks();
            }
        };
    }
]);

managementapp.directive('scheduleCron', ['bworkflowAdminApi', '$timeout', '$filter', 'languageTranslate',
    function (bworkflowAdminApi, $timeout, $filter, languageTranslate) {
        return {
            templateUrl: window.razordata.siteprefix + 'angulartemplates/admin/schedule/schedule_cron.html',
            restrict: 'E',
            require: 'ngModel',
            scope: {
                tasks: '=ngModel',
                level: '&level',
                types: '='
            },
            link: function (scope, elt, attrs, ngModel) {
                if (scope.tasks == null) {
                    scope.tasks = [];
                }

                scope.filterTasks = function () {
                    scope.filteredTasks = $filter('orderBy')($filter('filter')(scope.tasks, scope.taskFilter, true), "sortorder");
                };

                scope.taskFilter = function (task) {
                    return task.calendarrule.frequency == 'Cron';
                };

                scope.$watchCollection('tasks', function (newValues, oldValue) {
                    scope.filterTasks();
                });

                scope.$on('schedule.saved', function (event, data) {
                    scope.filterTasks();
                });

                scope.editTask = function (task) {
                    scope.$emit('schedule.edit', task);
                };

                scope.newTask = function (type) {
                    var cr = {
                        __isnew: true,
                        executesunday: false,
                        executemonday: false,
                        executetuesday: false,
                        executewednesday: false,
                        executethursday: false,
                        executefriday: false,
                        executesaturday: false,
                        cronexpression: '',
                        frequency: 'Cron',
                    };

                    var task = {
                        __isnew: true,
                        tasktypeid: type.id,
                        name: '',
                        calendarrule: cr,
                        editingname: true,
                        level: scope.level(),
                        startsAt: { value: null },
                        starttimewindowseconds: null,
                        starttimewindowminutes: { value: null },
                        estimateddurationminutes: { value: null },
                        translated: [{ __isnew: true, culturename: languageTranslate.currentLanguage.culturename }]
                    };

                    scope.editing = task;

                    scope.tasks.push(task);

                    $timeout(function () {
                        elt.find('textarea').focus();
                    });
                };

                scope.cancelNew = function () {
                    var index = $.inArray(scope.editing, scope.tasks);

                    if (index != -1) {
                        scope.tasks.splice(index, 1);
                    }

                    scope.editing = null;
                };

                scope.approveNew = function ($event) {
                    if (angular.isDefined($event)) {
                        $event.stopPropagation();
                    }

                    if (scope.editing.name == null || scope.editing.name == '') {
                        // you've got to supply a name
                        return;
                    }

                    // force in the name that's been enterd into the translation table (so the entity and the translation for the current culture match)
                    scope.editing.translated[0].name = scope.editing.name;

                    scope.editing.editingname = false;

                    scope.$emit('schedule.save', scope.editing);

                    scope.editing = null;
                };

                scope.blr = function ($event) {
                    if ($event.relatedTarget == null) {
                        scope.cancelNew();
                        return;
                    }

                    if ($($event.relatedTarget).hasClass('approveNew') == false) {
                        scope.cancelNew();
                        return;
                    }

                    if ($($event.relatedTarget).parent()[0] != $($event.target).parent()[0]) {
                        scope.cancelNew();
                        return;
                    }
                };

                scope.keyup = function ($event) {
                    if ($event.which === 13) {
                        // Enter pressed
                        scope.approveNew();
                    }
                    else if ($event.which === 27) {
                        // Escape pressed
                        scope.cancelNew();
                    }
                };

                scope.filterTasks();
            }
        };
    }
]);

managementapp.directive('scheduleLevelSelector', ['bworkflowAdminApi', '$filter',
    function (bworkflowAdminApi, $filter) {
        return {
            templateUrl: window.razordata.siteprefix + 'angulartemplates/admin/schedule/schedule_level_selector.html',
            restrict: 'E',
            require: 'ngModel',
            scope: {
                tasks: '=ngModel'
            },
            link: function (scope, elt, attrs, ngModel) {
                scope.addingNewLevel = false;
                scope.newLevel = '';

                scope.buildLevels = function () {
                    if (scope.tasks == null) {
                        scope.levels = [];
                        return;
                    }

                    scope.levels = $filter('orderBy')($filter('unique')(scope.tasks, 'level'), "level");
                };

                scope.$watch('tasks', function (newValue, oldValue) {
                    scope.buildLevels();

                    if (scope.levels.length > 0) {
                        scope.selectLevel(scope.levels[0].level);
                    }
                });

                scope.addNewLevel = function () {
                    scope.addingNewLevel = true;
                };

                scope.commitNewLevel = function () {
                    scope.levels.push({ level: parseInt(scope.newLevel) });

                    scope.levels = $filter('orderBy')(scope.levels, 'level');

                    scope.addingNewLevel = false;
                };

                scope.selectLevel = function (level) {
                    scope.currentLevel = level;

                    scope.$emit('schedule.select.level', level);
                };
            }
        };
    }
]);

// SCHEDULE APPROVAL MANAGEMENT STUFF

managementapp.controller('scheduleapprovalmanagementCtrl', [
    '$scope', '$http', '$q', '$filter', '$timeout', 'languageTranslate', function ($scope, $http, $q, $filter, $timeout, languageTranslate) {

        $scope.rooturl = window.razordata.siteprefix + 'Admin/ScheduleApprovalManagement/';
        $scope.dashboardurl = window.razordata.siteprefix;
        $scope.rosterid = -1;
        $scope.roster = null;
        $scope.workloadingenabled = false;
        $scope.showworkloading = false;
        $scope.showreplaceuser = false;
        $scope.shownewtask = false;
        $scope.showconfirmsave = false;
        $scope.searcheduser = {};
        $scope.currentslot = null;
        $scope.types = [];
        $scope.slotsWithUnapproved = [];
        $scope.unnapprovedTotal = 0;

        $scope.buckets = null;
        $scope.issaving = false;

        $scope.activeSlots = [];

        $scope.languagetranslate = languageTranslate;

        $scope.getWorkloadingEnabled = function () {
            $http.get($scope.rooturl + "WorkloadingModule")
                .success(function (data) {
                    $scope.workloadingenabled = data == 'true';
                });
        };

        $scope.getAllRosters = function () {
            $http.get($scope.rooturl + "Rosters")
                .success(function (data) {
                    $scope.rosters = data;

                    $scope.calculateRosterDurations();
                });
        };

        $scope.calculateRosterDurations = function () {
            angular.forEach($scope.rosters, function (roster) {
                var start = moment(roster.starttime, 'YYYY-MM-DDThh:mm:ss');
                var end = moment(roster.endtime, 'YYYY-MM-DDThh:mm:ss');
                // err the starttime and endtime for the roster are coming through with different
                // date portions, so lets set them to something we know
                if (start.year() != 2000) {
                    start.subtract(start.year() - 2000, 'years');
                }

                if (end.year() != 2000) {
                    end.subtract(end.year() - 2000, 'years');
                }

                if (end.isBefore(start)) {
                    var temp = start;
                    start = end;
                    end = start;
                }

                roster.duration = end.diff(start, 'seconds');
            });
        };

        $scope.getAllTypes = function () {
            $http.get(window.razordata.siteprefix + "Admin/TaskTypeManagement/Types")
                .success(function (data) {
                    $scope.types = data;
                });
        };

        $scope.$watch('rosterid', function (newValue, oldValue) {
            if (newValue == -1) {
                return;
            }

            var r = $filter('filter')($scope.rosters, { id: newValue }, true);
            $scope.roster = r[0];

            $scope.loadRoster(newValue);
        });

        $scope.$watch('showworkloading', function (newValue, oldValue) {
            if (newValue == null || $scope.rosterid == -1) {
                return;
            }

            $scope.loadRoster($scope.rosterid);
        });

        $scope.loadRoster = function (rosterId) {
            var parameters = { rosterid: rosterId, showworkloading: $scope.showworkloading, date: moment().format('DD-MM-YYYY') };

            $http.get($scope.rooturl + "Buckets", { params: parameters })
                .success(function (data) {
                    $scope.buckets = data;
                    $scope.issaving = false;

                    angular.forEach($scope.activeSlots, function (slotid, index) {
                        for (var b = 0; b < $scope.buckets.length; b++) {
                            var bucket = $scope.buckets[b];
                            for (var s = 0; s < bucket.slots.length; s++) {
                                var slot = bucket.slots[s];
                                if (slot.id == slotid) {
                                    slot.__isactive = true;
                                }
                            }
                        }
                    });

                    $scope.doTranslations();

                    $scope.activeSlots.length = 0;
                });
        };

        $scope.$watch('languagetranslate.currentLanguage', function (newValue, oldValue) {
            $scope.doTranslations();
        });

        $scope.doTranslations = function () {
            if ($scope.buckets == null || $scope.buckets.primarybuckets == null) {
                return;
            }

            angular.forEach($scope.buckets.primarybuckets, function (bucket) {
                angular.forEach(bucket.slots, function (slot) {
                    $scope.doTranslateSlot(slot);
                });
            });

            angular.forEach($scope.buckets.secondarybucket.slots, function (slot) {
                angular.forEach(slot.unassignedtasks, function (task) {
                    $scope.doTranslate(task);
                });
            });
        };

        $scope.doTranslateSlot = function (slot) {
            angular.forEach(slot.assignedtasks, function (task) {
                $scope.doTranslate(task);
            });
            angular.forEach(slot.unassignedtasks, function (task) {
                $scope.doTranslate(task);
            });
            angular.forEach(slot.cancelledtasks, function (task) {
                $scope.doTranslate(task);
            });
        }

        $scope.doTranslate = function (item) {
            item.translatedName = item.name;
            item.translatedDescription = item.description;
            var lt = languageTranslate;

            var translation = item.translated.find(function (m) {
                return m.culturename == lt.currentLanguage.culturename;
            });
            if (translation) {
                item.translatedName = translation.name;
                item.translatedDescription = translation.description;
            }
        };

        $scope.buildApprovalUnchangedActions = function (bucket, actions) {
            angular.forEach(bucket.slots, function (slot) {
                if (slot.isheader == true) {
                    var slots = [];

                    slots.add(slot.id);

                    var action = {
                        loading: false,
                        complete: false,
                        loadAttempts: 0,
                        slots: slots,
                        rosterid: bucket.rosterid,
                        commit: function (data) {
                            data.loading = true;

                            $http.post($scope.rooturl + "ApproveUnchanged", { date: moment().format('DD-MM-YYYY'), roster: data.rosterid, slots: data.slots })
                                .success(function (response) {
                                    data.loading = false;
                                    data.complete = true;
                                    data.loadAttempts = data.loadAttempts + 1;

                                    $scope.doApproveNextItem(); // next item in the chain please
                                }).error(function (response, status, headers, config) {
                                    data.loading = false;
                                    data.complete = false;
                                    data.loadAttempts = data.loadAttempts + 1;

                                    $scope.doApproveNextItem(); // next item in the chain please
                                });
                        }
                    };

                    action.text = slot.name;

                    actions.push(action);
                }
            });
        };

        $scope.buildApprovalChangedActions = function (bucket, actions) {
            angular.forEach(bucket.slots, function (slot) {
                if (slot.isheader == true) {
                    return;
                }

                var action = {
                    loading: false,
                    complete: false,
                    loadAttempts: 0,
                    slot: slot,
                    rosterid: bucket.rosterid,
                    commit: function (data) {
                        data.loading = true;

                        $http.post($scope.rooturl + "ApproveSlot", { date: moment().format('DD-MM-YYYY'), roster: data.rosterid, slot: data.slot })
                            .success(function (response) {
                                data.loading = false;
                                data.complete = true;
                                data.loadAttempts = data.loadAttempts + 1;

                                $scope.doApproveNextItem(); // next item in the chain please
                            }).error(function (response, status, headers, config) {
                                data.loading = false;
                                data.complete = false;
                                data.loadAttempts = data.loadAttempts + 1;

                                $scope.doApproveNextItem(); // next item in the chain please
                            });
                    }
                };

                action.text = slot.name;

                actions.push(action);
            });
        };

        $scope.buildApprovalOtherRosters = function (bucket, actions) {
            if (bucket.slots.length == 0) {
                return;
            }

            var action = {
                loading: false,
                complete: false,
                bucket: [],
                rosterid: bucket.rosterid,
                commit: function (data) {
                    data.loading = true;

                    $http.post($scope.rooturl + "RejectOtherRosters", { date: moment().format('DD-MM-YYYY'), roster: data.rosterid, bucket: bucket })
                        .success(function (response) {
                            data.loading = false;
                            data.complete = true;

                            $scope.doApproveNextItem(); // next item in the chain please
                        }).error(function (response, status, headers, config) {
                            data.loading = false;
                            data.complete = false;

                            $scope.doApproveNextItem(); // next item in the chain please
                        });
                }
            };

            action.text = "Rejecting requests from other rosters";

            actions.push(action);
        };

        $scope.buildApprovalActions = function () {
            // we build up a list of actions to take when saving so that we can give progress back to the user.
            // The actions fall into the following types.
            // 1. Approval of anything that hasn't been changed (so isheader == true), these items get batched together and done in a single hit
            // 2. Approval of anything that has been approved (so isheader == false), these item get broken out individually and done 1 at a time
            // 3. Knock back of any requests from end of day procedures that haven't been accepted.
            // each of the above are done per bucket

            $scope.saveactions = [];

            angular.forEach($scope.buckets.primarybuckets, function (bucket) {
                // approval of unchanged first then
                $scope.buildApprovalUnchangedActions(bucket, $scope.saveactions);
                $scope.buildApprovalChangedActions(bucket, $scope.saveactions);
            });

            $scope.buildApprovalOtherRosters($scope.buckets.secondarybucket, $scope.saveactions);
        };

        $scope.doApprove = function () {
            // save the active slots, so when things are reloaded again we 
            // can keep things nice and neat for the user
            if ($scope.buckets) {
                $scope.activeSlots.length = 0;
                for (var b = 0; b < $scope.buckets.length; b++) {
                    var bucket = $scope.buckets[b];
                    for (var s = 0; s < bucket.slots.length; s++) {
                        var slot = bucket.slots[s];
                        if (slot.__isactive == true) {
                            $scope.activeSlots.push(slot.id);
                        }
                    }
                }
            }

            $scope.buildApprovalActions();
            $scope.issaving = true;
            $scope.savingItemIndex = -1;

            $scope.showconfirmsave = false;
            $scope.issavingcomplete = false;

            // give the UI an opportunity to present things to the user
            $timeout(function () {
                // we let the actions chain themselves together one at a time
                // by committing them one at a time. When an action finishes,
                // it should call the doApproveNextItem method so that the
                // next action can do its thing. We start the chain here
                $scope.doApproveNextItem();
            });
        };

        $scope.doApproveNextItem = function () {
            $scope.savingItemIndex = $scope.savingItemIndex + 1;

            if ($scope.savingItemIndex >= $scope.saveactions.length) {
                $scope.issavingcomplete = true;
                return;
            }

            var currentAction = $scope.saveactions[$scope.savingItemIndex];

            currentAction.commit(currentAction);
        };

        $scope.leaveApprovalProgressUI = function () {
            $scope.issaving = false;
            $scope.loadRoster($scope.rosterid);
        };

        $scope.approve = function () {
            $scope.buildUnapproved();

            if ($scope.unnapprovedTotal != 0 || $scope.unrosteredTotal != 0) {
                $scope.showconfirmsave = true;
            }
            else {
                $scope.doApprove();
            }
        };

        $scope.buildUnapproved = function () {
            $scope.unnapprovedTotal = 0;
            $scope.unrosteredTotal = 0;
            $scope.slotsWithUnapproved.length = 0;

            angular.forEach($scope.buckets.primarybuckets, function (bucket, index) {
                angular.forEach(bucket.slots, function (slot, index) {
                    if (slot.isheader == false && slot.unassignedtasks.length > 0) {
                        $scope.unnapprovedTotal = $scope.unnapprovedTotal + slot.unassignedtasks.length;
                        angular.forEach(slot.unassignedtasks, function (u, index) {
                            $scope.slotsWithUnapproved.push(u);
                        });
                    }
                    else if (slot.isheader == true) {
                        $scope.unnapprovedTotal = $scope.unnapprovedTotal + slot.unassignedtaskscount;
                    }
                });
            });

            angular.forEach($scope.buckets.secondarybucket.slots, function (slot) {
                if (slot.unassignedtasks.length > 0) {
                    $scope.unrosteredTotal = $scope.unrosteredTotal + slot.unassignedtasks.length;
                }
            });
        };

        $scope.filterForUnapproved = function () {
            angular.forEach($scope.buckets, function (bucket, index) {
                angular.forEach(bucket.slots, function (slot, index) {
                    slot.__isactive = false;
                });
            });

            angular.forEach($scope.slotsWithUnapproved, function (slot, index) {
                slot.__isactive = true;
            });

            $scope.showconfirmsave = false;
        };

        $scope.$on('dailytasks.replaceuser', function (evt, args) {
            $scope.currentslot = args;
            $scope.showreplaceuser = true;
        });

        $scope.confirmreplaceuser = function () {
            $scope.currentslot.overriddenuserid = $scope.currentslot.userid;
            $scope.currentslot.overriddenusername = $scope.currentslot.name;

            $scope.currentslot.userid = $scope.searcheduser.Id;
            $scope.currentslot.name = $scope.searcheduser.Name;

            $scope.showreplaceuser = false;
        };

        $scope.$on('dailytasks.newtask', function (evt, args) {
            $scope.currentslot = args;
            $scope.searchedsite = {};
            $scope.isediting = false;

            $scope.currenttask = {
                __isnew: true,
                name: null,
                description: null,
                translatedName: null,
                translatedDescription: null,
                translated: [],
                level: 0,
                tasktypeid: $scope.types[0].id,
                roleid: $scope.currentslot.roleid
            };

            $scope.shownewtask = true;
        });

        $scope.$on('dailytasks.edittask', function (evt, args) {
            if (args.slot.isnormal == false || $scope.rosterid != args.task.rosterid) {
                // its either a task in a roster column, or its a task from a roster column
                return;
            }

            $scope.currentslot = args.slot;
            $scope.searchedsite = {};
            $scope.isediting = true;

            $scope.currenttask = args.task;

            $scope.searchedsite = { Id: args.task.siteid };

            $scope.currenttask.startsAt = { value: null };
            if ($scope.currenttask.starttime != null) {
                $scope.currenttask.startsAt.value = moment($scope.currenttask.starttime, 'HH:mm:ss').toDate();
            }

            $scope.currenttask.starttimewindowminutes = { value: null };
            if ($scope.currenttask.starttimewindowseconds != null) {
                $scope.currenttask.starttimewindowminutes.value = $scope.currenttask.starttimewindowseconds / 60;
            }

            $scope.shownewtask = true;
        });

        $scope.confirmnewtask = function () {
            $scope.doTranslate($scope.currenttask);

            if ($scope.isediting == false) {
                $scope.currentslot.assignedtasks.push($scope.currenttask);
            }

            $scope.currenttask.name = $scope.currenttask.translatedName;
            $scope.currenttask.description = $scope.currenttask.translatedDescription;

            if ($scope.currenttask.startsAt.value != null) {
                $scope.currenttask.starttime = moment($scope.currenttask.startsAt.value).format('HH:mm:ss');
            }

            $scope.shownewtask = false;
        };

        $scope.$watch('searchedsite', function (newValue, oldValue) {
            if ($scope.currenttask == null) {
                return;
            }

            if (newValue == null) {
                $scope.currenttask.siteid = null;
                return;
            }

            if (angular.isDefined(newValue.Id) == false) {
                return;
            }

            $scope.currenttask.siteid = newValue.Id;
        });

        $scope.slotSelected = function (slot) {
            if (slot.assignstasksthroughlabels == true) {
                alert('This item assigns tasks through labels and cannot be managed through schedule approval');
                return;
            }

            if (slot.isheader == true) {
                // ok, its only the header information, we need to load this from the server
                $http.post($scope.rooturl + "Slot", { id: slot.id, roster: $scope.rosterid, showworkloading: $scope.showworkloading })
                    .success(function (data) {
                        // data is the detailed version of the slot
                        angular.copy(data, slot);

                        $scope.doTranslateSlot(slot);

                        // it couldn't have been active before, so we just set that
                        slot.__isactive = true;
                    });
            }
            else {
                if (slot.__isactive) {
                    slot.__isactive = !slot.__isactive
                }
                else {
                    slot.__isactive = true;
                }
            }
        };

        $scope.getAllRosters();
        $scope.getAllTypes();
        $scope.getWorkloadingEnabled();
    }
]);

managementapp.directive('approveDailyTasks', ['bworkflowAdminApi', '$filter', '$timeout',
    function (bworkflowAdminApi, $filter, $timeout) {
        return {
            templateUrl: window.razordata.siteprefix + 'angulartemplates/admin/scheduleapproval/approve_daily_tasks.html',
            restrict: 'E',
            require: 'ngModel',
            scope: {
                slot: '=ngModel',
                roster: '=',
                showworkloading: '='
            },
            link: function (scope, elt, attrs, ngModel) {
                scope.unassignedtasks = scope.slot.unassignedtasks;
                scope.assignedtasks = scope.slot.assignedtasks;
                scope.cancelledtasks = scope.slot.cancelledtasks;
                scope.showingcancelled = false;
                scope.showingreplacecleaner = false;
                scope.managedLists = [];

                scope.allowedDropTypes = ['Task', 'Scheduled', 'OtherRoster'];
                scope.allowedCancelledDropTypes = ['Task', 'Scheduled'];

                scope.addFootnotes = function (list) {
                    angular.forEach(list, function (item) {
                        item.fromroster = scope.slot.name;
                    });
                };

                scope.getTypeNotes = function (task) {
                    if (angular.isDefined(task.datecreated) == false) {
                        return '';
                    }

                    if (task.datecreated == task.comparedate) {
                        return 'today';
                    }

                    return moment(task.datecreated).format('DD-MM-YYYY');
                };

                scope.approveAll = function () {
                    angular.forEach(scope.unassignedtasks, function (item, key) {
                        scope.assignedtasks.push(item);
                    });

                    scope.unassignedtasks.length = 0;
                };

                scope.unApproveAll = function () {
                    angular.forEach(scope.assignedtasks, function (item, key) {
                        scope.unassignedtasks.push(item);
                    });

                    scope.assignedtasks.length = 0;
                };

                scope.cancelAll = function (source) {
                    var s = scope.unassignedtasks;

                    if (angular.isDefined(source) == true) {
                        if (source == 'assigned') {
                            s = scope.assignedtasks;
                        }
                    }

                    angular.forEach(s, function (item, key) {
                        scope.cancelledtasks.push(item);
                    });

                    s.length = 0;
                };

                scope.unCancelAll = function () {
                    angular.forEach(scope.cancelledtasks, function (item, key) {
                        scope.unassignedtasks.push(item);
                    });

                    scope.cancelledtasks.length = 0;
                };

                scope.showcancelled = function (state) {
                    scope.showingcancelled = state;
                };

                scope.showreplaceuser = function () {
                    scope.$emit('dailytasks.replaceuser', scope.slot);
                };

                scope.shownewtask = function () {
                    scope.$emit('dailytasks.newtask', scope.slot);
                };

                scope.showedittask = function (task) {
                    scope.$emit('dailytasks.edittask', { slot: scope.slot, task: task });
                };

                scope.findLongest = function (list) {
                    var max = -1;
                    angular.forEach(list, function (item, index) {
                        var h = $(item).height();

                        if (h > max) {
                            max = h;
                        }
                    });

                    return max;
                };

                scope.recaculateListDimensions = function () {
                    angular.forEach(scope.managedLists, function (item, index) {
                        $(item).css('min-height', '');
                    });

                    $timeout(function () {
                        var height = scope.findLongest(scope.managedLists);

                        angular.forEach(scope.managedLists, function (item, index) {
                            $(item).css('min-height', height + 'px');
                        });
                    });
                };

                scope.$watch('unassignedtasks.length', function (newValue, oldValue) {
                    if (newValue == null) {
                        return;
                    }

                    scope.recaculateListDimensions();
                });

                scope.$watch('assignedtasks.length', function (newValue, oldValue) {
                    if (newValue == null) {
                        return;
                    }

                    scope.recaculateListDimensions();
                });

                scope.$watch('cancelledtasks.length', function (newValue, oldValue) {
                    if (newValue == null) {
                        return;
                    }

                    scope.recaculateListDimensions();
                });

                if (scope.slot.isnormal == false) {
                    // we hold tasks from another roster, we don't allow normal tasks to be dragged onto us
                    scope.allowedDropTypes = ['OtherRoster'];
                    scope.allowedCancelledDropTypes = ['OtherRoster'];

                    scope.addFootnotes(scope.slot.unassignedtasks);
                    scope.addFootnotes(scope.slot.cancelledtasks);

                    scope.showingcancelled = true;
                }


                $timeout(function () {
                    angular.forEach($(elt).find('.manage-list-dimensions'), function (item, index) {
                        scope.managedLists.push(item);
                    });
                });
            }
        };
    }
]);

// END OF DAY MANGEMENT STUFF

managementapp.controller('endofdaymanagementCtrl', [
    '$scope', '$http', '$q', '$filter', '$timeout', 'languageTranslate', function ($scope, $http, $q, $filter, $timeout, languageTranslate) {

        $scope.rooturl = window.razordata.siteprefix + 'Admin/EndOfDayManagement/';
        $scope.dashboardurl = window.razordata.siteprefix;
        $scope.rosterid = -1;

        $scope.selectedSlot = null;

        $scope.languagetranslate = languageTranslate;

        $scope.managedLists = [];

        $scope.issaving = false;

        $scope.getAllRosters = function () {
            $http.get($scope.rooturl + "Rosters")
                .success(function (data) {
                    $scope.rosters = data;
                });
        };

        $scope.$watch('rosterid', function (newValue, oldValue) {
            if (newValue == -1) {
                return;
            }

            $scope.loadRoster(newValue);
        });

        $scope.loadRoster = function (rosterId) {
            var parameters = { rosterid: rosterId, date: moment().format('DD-MM-YYYY') };

            $http.get($scope.rooturl + "Buckets", { params: parameters })
                .success(function (data) {
                    $scope.buckets = data;

                    $scope.doTranslations();
                });
        };

        $scope.$watch('languagetranslate.currentLanguage', function (newValue, oldValue) {
            $scope.doTranslations();
        });

        $scope.doTranslations = function () {
            angular.forEach($scope.buckets, function (bucket) {
                angular.forEach(bucket.primarybucket.slots, function (slot) {
                    angular.forEach(slot.assignedtasks, function (task) {
                        $scope.doTranslate(task);
                    });
                    angular.forEach(slot.finishedbymanagementtasks, function (task) {
                        $scope.doTranslate(task);
                    });
                });

                angular.forEach(bucket.secondarybucket.slots, function (slot) {
                    angular.forEach(slot.assignedtasks, function (task) {
                        $scope.doTranslate(task);
                    });
                });
            });
        };

        $scope.doTranslate = function (item) {
            item.translatedName = item.name;
            item.translatedDescription = item.description;
            var lt = languageTranslate;

            var translation = item.translated.find(function (m) {
                return m.culturename == lt.currentLanguage.culturename;
            });
            if (translation) {
                item.translatedName = translation.name;
                item.translatedDescription = translation.description;
            }
        };

        $scope.slotSelected = function (slot) {
            $scope.selectedSlot = slot;
        }

        $scope.doApprove = function () {
            $scope.saveactions = [];

            var action = {
                loading: false,
                complete: false,
                buckets: $scope.buckets,
                commit: function (data) {
                    data.loading = true;

                    $http.post($scope.rooturl + "Approve", { buckets: data.buckets })
                        .success(function (response) {
                            data.loading = false;
                            data.complete = true;

                            $scope.approveNextItem();
                        }).error(function (response, status, headers, config) {
                            data.loading = false;
                            data.complete = false;

                            $scope.approveNextItem();
                        });
                }
            };

            action.text = 'Approving end of day changes';

            $scope.saveactions.push(action);

            $scope.issaving = true;
            $scope.savingItemIndex = -1;

            $scope.showconfirmsave = false;
            $scope.issavingcomplete = false;

            $scope.approveNextItem();
        };

        $scope.approveNextItem = function () {
            $scope.savingItemIndex = $scope.savingItemIndex + 1;

            if ($scope.savingItemIndex >= $scope.saveactions.length) {
                $scope.issavingcomplete = true;
                return;
            }

            var currentAction = $scope.saveactions[$scope.savingItemIndex];

            currentAction.commit(currentAction);
        };

        $scope.approve = function () {
            $scope.doApprove();
        };

        $scope.leaveProgressUI = function () {
            $scope.issaving = false;
            $scope.selectedSlot = null;
            $scope.loadRoster($scope.rosterid);
        };

        $scope.getAllRosters();
    }
]);

managementapp.directive('manageEndOfDay', ['bworkflowAdminApi', '$filter', '$timeout',
    function (bworkflowAdminApi, $filter, $timeout) {
        return {
            templateUrl: window.razordata.siteprefix + 'angulartemplates/admin/endofday/manage_end_of_day.html',
            restrict: 'E',
            require: 'ngModel',
            scope: {
                slot: '=ngModel',
                secondarybucket: '='
            },
            link: function (scope, elt, attrs, ngModel) {
                scope.managedLists = [];

                scope.finishAll = function () {
                    angular.forEach(scope.slot.assignedtasks, function (item, key) {
                        scope.slot.finishedbymanagementtasks.push(item);
                    });

                    scope.slot.assignedtasks.length = 0;
                };

                scope.continueNextAll = function () {
                    angular.forEach(scope.slot.finishedbymanagementtasks, function (item, key) {
                        scope.slot.assignedtasks.push(item);
                    });

                    scope.slot.finishedbymanagementtasks.length = 0;
                };

                scope.findLongest = function (list) {
                    var max = -1;
                    angular.forEach(list, function (item, index) {
                        var h = $(item).height();

                        if (h > max) {
                            max = h;
                        }
                    });

                    return max;
                };

                scope.recaculateListDimensions = function () {
                    angular.forEach(scope.managedLists, function (item, index) {
                        $(item).css('min-height', '');
                    });

                    $timeout(function () {
                        var height = scope.findLongest(scope.managedLists);

                        angular.forEach(scope.managedLists, function (item, index) {
                            $(item).css('min-height', height + 'px');
                        });
                    });
                };

                scope.$watch('slot.finishedbymanagementtasks.length', function (newValue, oldValue) {
                    if (newValue == null) {
                        return;
                    }

                    scope.recaculateListDimensions();
                });

                scope.$watch('slot.assignedtasks.length', function (newValue, oldValue) {
                    if (newValue == null) {
                        return;
                    }

                    scope.recaculateListDimensions();
                });

                $timeout(function () {
                    angular.forEach($(elt).find('.manage-list-dimensions'), function (item, index) {
                        scope.managedLists.push(item);
                    });
                });
            }
        };
    }
]);

// MEMBER MANAGEMENT STUFF
// Currently the good majority of the member management area is implemented through JQuery and the old approach
// the only thing that is managed in Angular currently is the upload of an image for the user
managementapp.controller('membermanagementCtrl', [
    '$scope', '$http', '$q', 'FileUploader', '$filter', '$rootScope', '$timeout', function ($scope, $http, $q, FileUploader, $filter, $rootScope, $timeout) {
    }
]);

managementapp.directive('memberManagementMedia', ['bworkflowAdminApi', 'FileUploader',
    function (bworkflowAdminApi, FileUploader) {
        return {
            templateUrl: window.razordata.siteprefix + 'angulartemplates/admin/member/member_management_media.html',
            restrict: 'E',
            scope: {
            },
            link: function (scope, elt, attrs, ngModel) {
                scope.mediaid = attrs.mediaid;

                if (scope.mediaid == 'null') {
                    scope.mediaid = null;
                }

                scope.userid = attrs.userid;
            },
            controller: function ($scope) {
                $scope.rooturl = window.razordata.siteprefix + 'Admin/MembershipManagement/';

                $scope.uploadFile = function () {
                    $scope.results = null;
                    $scope.uploadFailed = null;

                    var uploader = $scope.uploader = new FileUploader({
                        scope: $scope,
                        url: "MembershipManagement/MediaUpload"
                    });
                    $scope.uploadargs = { id: $scope.userid, mediaid: $scope.mediaid };

                    uploader.onBeforeUploadItem = function (item) {
                        Array.prototype.push.apply(item.formData, [$scope.uploadargs]);
                    };

                    uploader.onSuccessItem = function (fileItem, response, status, headers) {
                        $scope.nonce = Date.now() / 1000 | 0;
                    };

                    $scope.uploadingfile = true;
                };

                $scope.imageUrl = bworkflowAdminApi.getImageUrl();
                $scope.nonce = Date.now() / 1000 | 0;

                $scope.uploadingfile = false;
            }
        };
    }
]);

// ORG HIERARCHY STUFF: Similar to the membership stuff, the good majority of this is in JQuery + the old techniques.
managementapp.controller('hierarchymanagementCtrl', [
    '$scope', '$http', '$q', '$filter', '$rootScope', '$timeout', function ($scope, $http, $q, $filter, $rootScope, $timeout) {
        $scope.rooturl = window.razordata.siteprefix + 'Organisation/Hierarchy/';

        $scope.searchUserText = '';
        $scope.searchUserResults = [];

        $scope.searchUsers = function (text) {
            $http.get(window.razordata.siteprefix + "SearchUser?query=" + text)
                .success(function (data) {
                    $scope.searchUserResults = data;

                    $timeout(function () {
                        $(window).trigger('hierarchymanagement_searchusers_completed');
                    });
                });
        };
    }
]);


// GENERAL STUFF

managementapp.directive('fileUploader', [
    function () {
        return {
            templateUrl: window.razordata.siteprefix + 'angulartemplates/admin/file_uploader.html',
            restrict: 'E',
            link: function (scope, elt, attrs, ngModel) {
            }
        };
    }
]);

managementapp.directive('labelSelector', ['bworkflowAdminApi',
    function (bworkflowAdminApi) {
        return {
            templateUrl: window.razordata.siteprefix + 'angulartemplates/admin/labels/label_selector.html',
            restrict: 'E',
            require: "ngModel",
            scope: {
                selected: '=ngModel',
                type: '='
            },
            link: function (scope, elt, attrs, ngModel) {
                scope.argbToHex = function (argb) {
                    if (argb == null) {
                        return null;
                    }
                    return '#' + ('000000' + (argb & 0xFFFFFF).toString(16)).slice(-6);
                };

                scope.changeSelection = function (label) {
                    var index = $.inArray(label.id, scope.selected);

                    if (index == -1) {
                        scope.selected.push(label.id);
                    }
                    else {
                        scope.selected.splice(index, 1);
                    }
                };

                scope.isSelected = function (label) {
                    return $.inArray(label.id, scope.selected) != -1;
                };

                var t = scope.type;

                if (t == null) {
                    t = attrs.type;
                }

                bworkflowAdminApi.getLabels(t).then(function (labels) {
                    scope.labels = labels;
                }, function (error) {

                });
            }
        };
    }
]);

managementapp.directive('selectUser', ['bworkflowAdminApi', '$timeout', '$http',
    function (bworkflowAdminApi, $timeout, $http) {
        return {
            templateUrl: window.razordata.siteprefix + 'angulartemplates/admin/select_user.html',
            restrict: 'E',
            require: "ngModel",
            scope: {
                selected: '=ngModel',
                mandatory: '='
            },
            controller: function ($scope) {
                $scope.watchcontainer = { currentuser: {} };
                $scope.options = {
                    highlight: true
                };

                $scope.selectedname = { value: null };

                $scope.$watch('selected', function (newValue, oldValue) {
                    if (newValue == null) {
                        $scope.watchcontainer = { currentuser: {} };
                        return;
                    }

                    if (angular.isDefined(newValue.Id) == false || newValue.Id == null) {
                        $scope.watchcontainer = { currentuser: {} };
                        return;
                    }

                    if (angular.isDefined(newValue.Name) == true) {
                        $scope.selectedname = { value: newValue.Name };
                        return;
                    }

                    // ok so we have an Id but no name, so hit the server for that
                    $http.get(window.razordata.siteprefix + "SearchUser/Details?id=" + newValue.Id)
                        .success(function (data) {
                            $scope.selectedname = { value: data.Name };
                        });
                });

                $scope.userSuggestion = new Bloodhound({
                    datumTokenizer: function (d) { return Bloodhound.tokenizers.whitespace(d.Name); },
                    queryTokenizer: Bloodhound.tokenizers.whitespace,
                    remote: window.razordata.siteprefix + 'SearchUser?query=%QUERY'
                });

                $scope.userSuggestion.initialize();
                //$scope.userdataset = undefined;

                $scope.userdataset = {
                    displayKey: 'Name',
                    source: $scope.userSuggestion.ttAdapter()
                };

                //$timeout(function () {
                //    $scope.userdataset = {
                //        displayKey: 'name',
                //        source: $scope.userSuggestion.ttAdapter()
                //    };
                //});

                $scope.$watch('watchcontainer.currentuser', function (newUser) {
                    if (!newUser || angular.isDefined(newUser.Id) == false) {
                        return;
                    }

                    $scope.selected = newUser;
                });

                $scope.clearselection = function () {
                    $scope.selected = null;
                    $scope.watchcontainer = { currentuser: {} };
                };
            },
            link: function (scope, elt, attrs, ngModel) {

            }
        };
    }
]);

managementapp.directive('searchUser', ['typeaheadDirective', function (typeaheadDirective) {
    return typeaheadDirective({
        ajaxItemName: 'name',
        ajaxItemId: 'id',
        displayKey: 'name',
        handleBars: '<strong>{{name}}</strong>',
        selectObjectNotId: true,
        ajaxItemUrl: function (id) {
            return 'SearchUser/Details?id=' + id;
        },
        ajaxItemSearch: function (query, scope) {
            return 'SearchUser?query=' + query + '&withcount=true';
        },
        transform: function (data) {
            return data.items;
        }
    })
}]);

managementapp.directive('searchCompany', ['typeaheadDirective', function (typeaheadDirective) {
    return typeaheadDirective({
        ajaxItemName: 'name',
        displayKey: 'name',
        handleBars: '<strong>{{name}}</strong>',
        ajaxItemUrl: function (id) {
            return 'SearchCompany/Details?id=' + id;
        },
        ajaxItemSearch: function (query, scope) {
            return 'SearchCompany?query=' + query;
        },
        transform: function (data) {
            return data.items;
        }
    })
}]);

managementapp.directive('searchAsset', ['typeaheadDirective', function (typeaheadDirective) {
    return typeaheadDirective({
        ajaxItemName: 'name',
        displayKey: 'name',
        handleBars: '<strong>{{name}}</strong>',
        ajaxItemUrl: function (id) {
            return 'SearchAsset/Details?id=' + id;
        },
        ajaxItemSearch: function (query, scope) {
            return 'SearchAsset?query=' + query;
        },
        transform: function (data) {
            return data.items;
        }
    })
}]);

managementapp.directive('searchUser', ['typeaheadDirective', function (typeaheadDirective) {
    return typeaheadDirective({
        ajaxItemName: 'name',
        ajaxItemId: 'id',
        displayKey: 'name',
        handleBars: '<strong>{{name}}</strong>',
        selectObjectNotId: true,
        ajaxItemUrl: function (id) {
            return 'SearchUser/Details?id=' + id;
        },
        ajaxItemSearch: function (query, scope) {
            return 'SearchUser?query=' + query + '&withcount=true';
        },
        transform: function (data) {
            return data.items;
        }
    })
}]);

managementapp.directive('searchCompany', ['typeaheadDirective', function (typeaheadDirective) {
    return typeaheadDirective({
        ajaxItemName: 'name',
        displayKey: 'name',
        handleBars: '<strong>{{name}}</strong>',
        ajaxItemUrl: function (id) {
            return 'SearchCompany/Details?id=' + id;
        },
        ajaxItemSearch: function (query, scope) {
            return 'SearchCompany?query=' + query;
        },
        transform: function (data) {
            return data.items;
        }
    })
}]);

managementapp.directive('searchAsset', ['typeaheadDirective', function (typeaheadDirective) {
    return typeaheadDirective({
        ajaxItemName: 'name',
        displayKey: 'name',
        handleBars: '<strong>{{name}}</strong>',
        ajaxItemUrl: function (id) {
            return 'SearchAsset/Details?id=' + id;
        },
        ajaxItemSearch: function (query, scope) {
            return 'SearchAsset?query=' + query;
        },
        transform: function (data) {
            return data.items;
        }
    })
}]);

managementapp.directive('labelDisplay', ['bworkflowAdminApi',
    function (bworkflowAdminApi) {
        return {
            templateUrl: window.razordata.siteprefix + 'angulartemplates/admin/labels/label_display.html',
            restrict: 'E',
            require: "ngModel",
            scope: {
                selected: '=ngModel',
                type: '='
            },
            link: function (scope, elt, attrs, ngModel) {
                scope.argbToHex = function (argb) {
                    if (argb == null) {
                        return null;
                    }
                    return '#' + ('000000' + (argb & 0xFFFFFF).toString(16)).slice(-6);
                };

                scope.isSelected = function (label) {
                    return $.inArray(label.id, scope.selected) != -1;
                };

                scope.$watch("selected", function (oldValue, newValue) {
                    if (scope.allLabels == null) {
                        return;
                    }

                    scope.populateLabels(scope.allLabels);
                });

                scope.populateLabels = function (labels) {
                    scope.labels = [];

                    angular.forEach(labels, function (value, index) {
                        if (scope.isSelected(value) == false) {
                            return;
                        }

                        scope.labels.push(value);
                    });
                };



                var t = scope.type;

                if (t == null) {
                    t = attrs.type;
                }

                scope.allLabels = null;
                bworkflowAdminApi.getLabels(t).then(function (labels) {
                    scope.allLabels = labels;

                    scope.populateLabels(labels);
                }, function (error) {

                });
            }
        };
    }
]);

managementapp.factory('languageSvc', ['$http', '$q', '$translate', function ($http, $q, $translate) {
    var svc = {};

    svc.rooturl = window.razordata.siteprefix + 'Admin/LanguageManagement/';
    svc.getLanguages = function () {
        var d = $q.defer();
        $http.get(svc.rooturl + "Languages")
            .success(function (data) {
                d.resolve(data);
            });
        return d.promise;
    };

    svc.deleteLanguage = function (culturename) {
        var d = $q.defer();
        $http({ url: svc.rooturl + "DeleteLanguage", method: "DELETE", params: { cultureName: culturename } })
            .success(function (result) {
                d.resolve(result);
            });
        return d.promise;
    };

    svc.saveLanguage = function (language) {
        var d = $q.defer();
        $http.post(svc.rooturl + "SaveLanguage", { language: language })
            .success(function (result) {
                if (result.result == true)
                    d.resolve(true);
                else
                    d.reject(result.error);
            });
        return d.promise;
    };

    svc.newLanguage = function (name) {
        return { languagename: name, __isnew: true };
    };

    svc.autoTranslate = function (culturename) {
        var d = $q.defer();
        $http.post(svc.rooturl + "AutoTranslate", { culturename: culturename })
            .success(function (result) {
                d.resolve(result);
            });
        return d.promise;
    };

    svc.getTranslations = function (culturename) {
        var d = $q.defer();
        $http({ url: svc.rooturl + "GetTranslations", method: "GET", params: { cultureName: culturename } })
            .success(function (translations) {
                // translations will be a list of
                // { culturename, translationid, englishtext, nativetext }
                // for a language 'x', each defined translation will have culturename = 'x'
                // for missing translations, the culturename will = 'en' to indicate an english default
                angular.forEach(translations, function (t) {
                    t.originalnativetext = t.nativetext;
                    t.changed = function () {
                        return t.originalnativetext != t.nativetext;
                    };
                    t.isTranslated = function () {
                        return t.culturename == culturename;
                    };
                    t.save = function (force) {
                        if (!force && t.nativetext == t.originalnativetext)
                            return;

                        $http.post(svc.rooturl + "SaveTranslation", { culturename: culturename, translationid: t.translationid, nativetext: t.nativetext })
                            .success(function (result) {
                                t.originalnativetext = t.nativetext;
                                t.culturename = culturename;
                                $translate.refresh(culturename);
                            });
                    };
                    t.delete = function () {
                        $http({ url: svc.rooturl + "DeleteTranslation", method: "DELETE", params: { culturename: culturename, translationid: t.translationid } })
                            .success(function (result) {
                                if (culturename != "en") {
                                    t.nativetext = t.englishtext;
                                    t.originalnativetext = t.englishtext;
                                    t.culturename = "en";
                                } else {
                                    // English is the master language, deleting English translation will permanently delete it
                                    var index = translations.indexOf(t);
                                    translations.splice(index, 1);
                                }
                                $translate.refresh(culturename);
                            });
                    };
                });
                d.resolve(translations);
            });
        return d.promise;
    };

    return svc;
}]);

managementapp.controller('languageCtrl', [
    '$scope', '$http', '$q', '$filter', 'languageSvc', function ($scope, $http, $q, $filter, languageSvc) {
        $scope.languages = [];

        $scope.getLanguages = function () {
            languageSvc.getLanguages()
                .then(function (data) {
                    $scope.languages = data;
                });
        };

        $scope.newLanguage = function () {
            $scope.original = null;
            $scope.languages.push(languageSvc.newLanguage('New Language'));
        };

        $scope.deleteLanguage = function () {
            if (confirm('Deleting this language is a permanent action. Are you sure you want to do this?') == false) {
                return;
            }

            if ($scope.editing.__isnew)
                $scope.languages.pop();
            else {
                languageSvc.deleteLanguage($scope.editing.culturename).then(function () {
                    $scope.getLanguages();
                })
            }
        };

        $scope.$on('language.edit', function (event, language) {
            $scope.original = language;
            $scope.editing = angular.copy(language);
            $scope.editing.translations = [];

            languageSvc.getTranslations($scope.editing.culturename).then(function (data) {
                $scope.editing.translations = data;
            });
        });

        $scope.$on('language.save', function (event, language) {
            if ($scope.original == language)
                return;

            languageSvc.saveLanguage(language).then(function () {
                $scope.getLanguages();
            });;
        });

        $scope.getLanguages();
    }
]);

// TASK TYPE MANAGEMENT STUFF

managementapp.controller('tasktypemanagementCtrl', [
    '$scope', '$http', '$q', '$filter', '$timeout', function ($scope, $http, $q, $filter, $timeout) {

        $scope.rooturl = window.razordata.siteprefix + 'Admin/TaskTypeManagement/';

        $scope.getAllTypes = function () {
            $http.get($scope.rooturl + "Types")
                .success(function (data) {
                    $scope.types = data;
                });
        };

        $scope.newType = function () {
            $scope.original = null;

            $scope.editing = {
                __isnew: true,
                name: 'New Task Type',
                defaultlengthindays: 1,
                publishedresources: []
            };
        };

        $scope.$on('type.edit', function (event, args) {
            $scope.original = args;

            $http.get($scope.rooturl + "Type", { params: { id: args.id } })
                .success(function (data) {
                    $scope.editing = data;
                });
        });

        $scope.$on('type.save', function (event, args) {
            var parameters = { type: args };

            $http.post($scope.rooturl + "TaskTypeSave", parameters)
                .success(function (data) {
                    if (data == false) {
                        return;
                    }

                    if (args.__isnew == false) {
                        $scope.original.name = data.name;
                    }
                    else {
                        $scope.types.push({ id: data.id, name: data.name });
                    }
                });
        });


        $scope.getAllTypes();
    }
]);

managementapp.directive('tasktypeSidebar', [
    function () {
        return {
            templateUrl: window.razordata.siteprefix + 'angulartemplates/admin/tasktypes/tasktype_sidebar.html',
            restrict: 'E',
            require: 'ngModel',
            scope: {
                types: '=ngModel'
            },
            link: function (scope, elt, attrs, ngModel) {
                scope.editType = function (type) {
                    scope.$emit('type.edit', type);
                };
            }
        };
    }
]);

managementapp.directive('tasktypeEditor', ['$filter', 'FileUploader', 'bworkflowAdminApi', '$http', 'statusStages', 'facilityStructureSvc',
    function ($filter, FileUploader, bworkflowAdminApi, $http, statusStages, facilityStructureSvc) {
        return {
            templateUrl: window.razordata.siteprefix + 'angulartemplates/admin/tasktypes/tasktype_editor.html',
            restrict: 'E',
            require: 'ngModel',
            scope: {
                type: '=ngModel'
            },
            link: function (scope, elt, attrs, ngModel) {
                scope.structureTypes = facilityStructureSvc.types();

                scope.removeFacilityOverride = function (fo) {
                    scope.type.facilityoverrides.remove(fo);
                }

                scope.$watch('NewFacilityOverride', function (newValue) {
                    if (newValue) {
                        scope.type.facilityoverrides.push({
                            __isnew: true,
                            tasktypeid: scope.type.id,
                            facilitystructure: newValue,
                            facilitystructureid: newValue.id,
                            estimateddurationseconds: null
                        });

                        scope.NewFacilityOverride = null;
                    }
                });

                scope.statusStages = statusStages;
                scope.interfacetypes =
                    [
                        { id: 0, name: 'Clockin Clockout' },
                        { id: 1, name: 'Desktop' },
                        { id: 2, name: 'Save Finish' },
                        { id: 3, name: 'Product Ordering' },
                        { id: 4, name: 'Hidden' }
                    ];
                scope.referencenumbermodes =
                    [
                        { id: 0, name: 'Hidden' },
                        { id: 1, name: 'Show' },
                        { id: 2, name: 'Mandatory' }
                    ];
                scope.photosupports =
                    [
                        { id: 0, name: 'None' },
                        { id: 1, name: 'Optional' },
                        { id: 2, name: 'Mandatory' }
                    ];
                scope.addingdocumentation = false;
                scope.selectedBranch = null;

                scope.save = function () {
                    scope.$emit('type.save', scope.type);
                };

                scope.removeResource = function (resource) {
                    var index = $.inArray(resource, scope.type.publishedresources);

                    if (index == -1) {
                        return;
                    }

                    scope.type.publishedresources.splice(index, 1);
                };

                scope.getFolders = function () {
                    $http.get(window.razordata.siteprefix + 'Admin/MediaManagement/Folders?notfiled=false&includefiles=false')
                        .success(function (data) {

                            angular.forEach(data, function (node, key) {
                                scope.setFolderIcons(node);
                            });

                            scope.folders = data;
                        });
                };

                scope.setFolderIcons = function (node) {
                    if (node.data.isfile == true) {
                        return;
                    }

                    if (node.children.length > 0) {
                        angular.forEach(node.children, function (child, key) {
                            scope.setFolderIcons(child);
                        });

                        return;
                    }

                    node.iconLeaf = "icon-folder-open";
                }

                scope.showAddDocumentation = function () {
                    scope.addingdocumentation = true;
                };

                scope.documentationAlreadyMapped = function (folderid) {
                    var isMapped = false;

                    angular.forEach(scope.type.documentation, function (value, index) {
                        if (value.mediafolderid == folderid) {
                            isMapped = true;
                        }
                    });

                    return isMapped;
                };

                scope.addDocumentation = function () {
                    if (scope.documentationAlreadyMapped(scope.selectedBranch.data.id) == true) {
                        alert('The folder is already mapped to this type of task. A folder may only be mapped once to each task type.');
                        return;
                    }

                    var doc = {
                        __isnew: true,
                        tasktypeid: scope.type.id,
                        mediafolderid: scope.selectedBranch.data.id,
                        medianame: scope.selectedBranch.label
                    };

                    scope.type.documentation.push(doc);
                    scope.addingdocumentation = false;
                }

                scope.deleteDocumentation = function (doc) {
                    var index = $.inArray(doc, scope.type.documentation);

                    if (index == -1) {
                        return;
                    }

                    scope.type.documentation.splice(index, 1);
                };

                scope.selectBranch = function (branch) {
                    scope.selectedBranch = branch;
                };

                scope.getFolders();
            },
            controller: function ($scope) {
                $scope.watchcontainer = {};
                $scope.watchcontainer.currentWorkflowObject = {};

                $scope.workflowSuggestion = new Bloodhound({
                    datumTokenizer: function (d) { return Bloodhound.tokenizers.whitespace(d.name); },
                    queryTokenizer: Bloodhound.tokenizers.whitespace,
                    remote: window.razordata.siteprefix + 'Admin/TaskTypeManagement/SearchPublishedResources?query=%QUERY'
                });

                $scope.workflowSuggestion.initialize();

                $scope.workflowDataset = {
                    displayKey: 'name',
                    source: $scope.workflowSuggestion.ttAdapter(),
                    templates: {
                        suggestion: Handlebars.compile('<strong>{{name}}</strong><br/><i>{{publishinggroup.name}}</i>')
                    }
                };

                $scope.$watch('watchcontainer.currentWorkflowObject', function (newWorkflow) {
                    if (!newWorkflow || angular.isDefined(newWorkflow.id) == false) {
                        return;
                    }

                    // we need to make sure we don't already have this checklist mapped into us
                    var current = $filter('filter')($scope.type.publishedresources, { publishedresourceid: newWorkflow.id }, true);
                    if (current.length > 0) {
                        return;
                    }

                    var newMapping = {
                        __isnew: true,
                        tasktypeid: $scope.type.id,
                        publishedresourceid: newWorkflow.id,
                        name: newWorkflow.name,
                        isembedded: false,
                        estimatedpagecount: newWorkflow.estimatedpagecount
                    };

                    if (newMapping.estimatedpagecount == 1) {
                        newMapping.preventfinishing = true;
                    }

                    $scope.type.publishedresources.push(newMapping);
                });

                $scope.uploadFile = function () {
                    $scope.results = null;
                    $scope.uploadFailed = null;

                    var uploader = $scope.uploader = new FileUploader({
                        scope: $scope,
                        url: "TaskTypeManagement/MediaUpload"
                    });
                    $scope.uploadargs = { id: $scope.type.id };

                    uploader.onBeforeUploadItem = function (item) {
                        Array.prototype.push.apply(item.formData, [$scope.uploadargs]);
                    };

                    uploader.onSuccessItem = function (fileItem, response, status, headers) {
                        $scope.type.mediaid = response[0].data;
                        $scope.nonce = Date.now() / 1000 | 0;
                    };

                    $scope.uploadingfile = true;
                };

                $scope.imageUrl = bworkflowAdminApi.getImageUrl();
                $scope.nonce = Date.now() / 1000 | 0;

                $scope.uploadingfile = false;
            }
        };
    }
]);

// FINISHED STATUS STUFF

managementapp.controller('finishedstatusmanagementCtrl', [
    '$scope', '$http', '$q', '$filter', '$timeout', function ($scope, $http, $q, $filter, $timeout) {

        $scope.rooturl = window.razordata.siteprefix + 'Admin/FinishedStatusManagement/';

        $scope.getAllStatuses = function () {
            $http.get($scope.rooturl + "Statuses")
                .success(function (data) {
                    $scope.statuses = data;
                });
        };

        $scope.newStatus = function () {
            $scope.original = null;

            $scope.editing = {
                __isnew: true,
                text: 'New Finish Status',
                requiresextranotes: false
            };
        };

        $scope.$on('status.edit', function (event, args) {
            $scope.original = args;

            $http.get($scope.rooturl + "Status", { params: { id: args.id } })
                .success(function (data) {
                    $scope.editing = data;
                });
        });

        $scope.$on('status.save', function (event, args) {
            var parameters = { status: args };

            $http.post($scope.rooturl + "StatusSave", parameters)
                .success(function (data) {
                    if (data == false) {
                        return;
                    }

                    if (args.__isnew == false) {
                        $scope.original.text = data.text;
                    }
                    else {
                        $scope.statuses.push({ id: data.id, text: data.text });
                    }
                });
        });

        $scope.getAllStatuses();
    }
]);

managementapp.directive('finishedstatusSidebar', [
    function () {
        return {
            templateUrl: window.razordata.siteprefix + 'angulartemplates/admin/finishedstatus/finishedstatus_sidebar.html',
            restrict: 'E',
            require: 'ngModel',
            scope: {
                statuses: '=ngModel'
            },
            link: function (scope, elt, attrs, ngModel) {
                scope.editStatus = function (type) {
                    scope.$emit('status.edit', type);
                };
            }
        };
    }
]);

managementapp.constant('statusStages', [
    { id: 0, name: 'Finish' },
    { id: 1, name: 'Pause' }
]);

managementapp.directive('finishedstatusEditor', ['$filter', 'FileUploader', 'bworkflowAdminApi', '$http', 'statusStages',
    function ($filter, FileUploader, bworkflowAdminApi, $http, statusStages) {
        return {
            templateUrl: window.razordata.siteprefix + 'angulartemplates/admin/finishedstatus/finishedstatus_editor.html',
            restrict: 'E',
            require: 'ngModel',
            scope: {
                status: '=ngModel'
            },
            link: function (scope, elt, attrs, ngModel) {
                scope.statusStages = statusStages;

                scope.save = function () {
                    scope.$emit('status.save', scope.status);
                };
            }
        };
    }
]);

// STATUS STUFF

managementapp.controller('statusmanagementCtrl', [
    '$scope', '$http', '$q', '$filter', '$timeout', function ($scope, $http, $q, $filter, $timeout) {

        $scope.rooturl = window.razordata.siteprefix + 'Admin/StatusManagement/';

        $scope.getAllStatuses = function () {
            $http.get($scope.rooturl + "Statuses")
                .success(function (data) {
                    $scope.statuses = data;
                });
        };

        $scope.newStatus = function () {
            $scope.original = null;

            $scope.editing = {
                __isnew: true,
                text: 'New Status',
                description: null
            };
        };

        $scope.$on('status.edit', function (event, args) {
            $scope.original = args;

            $http.get($scope.rooturl + "Status", { params: { id: args.id } })
                .success(function (data) {
                    $scope.editing = data;
                });
        });

        $scope.$on('status.save', function (event, args) {
            var parameters = { status: args };

            $http.post($scope.rooturl + "StatusSave", parameters)
                .success(function (data) {
                    if (data == false) {
                        return;
                    }

                    if (args.__isnew == false) {
                        $scope.original.text = data.text;
                    }
                    else {
                        $scope.statuses.push({ id: data.id, text: data.text });
                    }
                });
        });

        $scope.getAllStatuses();
    }
]);

managementapp.directive('statusSidebar', [
    function () {
        return {
            templateUrl: window.razordata.siteprefix + 'angulartemplates/admin/status/status_sidebar.html',
            restrict: 'E',
            require: 'ngModel',
            scope: {
                statuses: '=ngModel'
            },
            link: function (scope, elt, attrs, ngModel) {
                scope.editStatus = function (type) {
                    scope.$emit('status.edit', type);
                };
            }
        };
    }
]);

managementapp.directive('statusEditor', ['$filter', 'FileUploader', 'bworkflowAdminApi', '$http',
    function ($filter, FileUploader, bworkflowAdminApi, $http) {
        return {
            templateUrl: window.razordata.siteprefix + 'angulartemplates/admin/status/status_editor.html',
            restrict: 'E',
            require: 'ngModel',
            scope: {
                status: '=ngModel'
            },
            link: function (scope, elt, attrs, ngModel) {
                scope.save = function () {
                    scope.$emit('status.save', scope.status);
                };
            }
        };
    }
]);

// LEAVE TYPE STUFF

managementapp.controller('leavetypemanagementCtrl', [
    '$scope', '$http', '$q', '$filter', '$timeout', function ($scope, $http, $q, $filter, $timeout) {

        $scope.rooturl = window.razordata.siteprefix + 'Admin/LeaveTypeManagement/';

        $scope.getAllLeaveTypes = function () {
            $http.get($scope.rooturl + "LeaveTypes")
                .success(function (data) {
                    $scope.types = data;
                });
        };

        $scope.newType = function () {
            $scope.original = null;

            $scope.editing = {
                __isnew: true,
                name: 'New Leave Type'
            };
        };

        $scope.$on('leavetype.edit', function (event, args) {
            $scope.original = args;

            $http.get($scope.rooturl + "LeaveType", { params: { id: args.id } })
                .success(function (data) {
                    $scope.editing = data;
                });
        });

        $scope.$on('leavetype.save', function (event, args) {
            var parameters = { type: args };

            $http.post($scope.rooturl + "LeaveTypeSave", parameters)
                .success(function (data) {
                    if (data == false) {
                        return;
                    }

                    if (args.__isnew == false) {
                        $scope.original.name = data.name;
                    }
                    else {
                        $scope.types.push({ id: data.id, name: data.name });
                    }
                });
        });

        $scope.getAllLeaveTypes();
    }
]);

managementapp.directive('leavetypeSidebar', [
    function () {
        return {
            templateUrl: window.razordata.siteprefix + 'angulartemplates/admin/leavetype/leavetype_sidebar.html',
            restrict: 'E',
            require: 'ngModel',
            scope: {
                types: '=ngModel'
            },
            link: function (scope, elt, attrs, ngModel) {
                scope.editType = function (type) {
                    scope.$emit('leavetype.edit', type);
                };
            }
        };
    }
]);

managementapp.directive('leavetypeEditor', ['$filter', 'FileUploader', 'bworkflowAdminApi', '$http',
    function ($filter, FileUploader, bworkflowAdminApi, $http) {
        return {
            templateUrl: window.razordata.siteprefix + 'angulartemplates/admin/leavetype/leavetype_editor.html',
            restrict: 'E',
            require: 'ngModel',
            scope: {
                type: '=ngModel'
            },
            link: function (scope, elt, attrs, ngModel) {
                scope.save = function () {
                    scope.$emit('leavetype.save', scope.type);
                };
            }
        };
    }
]);

// TIMESHEET ITEM TYPE STUFF

managementapp.controller('timesheetitemtypemanagementCtrl', [
    '$scope', '$http', '$q', '$filter', '$timeout', function ($scope, $http, $q, $filter, $timeout) {

        $scope.rooturl = window.razordata.siteprefix + 'Admin/TimesheetItemTypeManagement/';

        $scope.getAllLeaveTypes = function () {
            $http.get($scope.rooturl + "TimesheetItemTypes")
                .success(function (data) {
                    $scope.types = data;
                });
        };

        $scope.newType = function () {
            $scope.original = null;

            $scope.editing = {
                __isnew: true,
                name: 'New Timesheet Item Type'
            };
        };

        $scope.$on('timesheetitemtype.edit', function (event, args) {
            $scope.original = args;

            $http.get($scope.rooturl + "TimesheetItemType", { params: { id: args.id } })
                .success(function (data) {
                    $scope.editing = data;
                });
        });

        $scope.$on('timesheetitemtype.save', function (event, args) {
            var parameters = { type: args };

            $http.post($scope.rooturl + "TimesheetItemTypeSave", parameters)
                .success(function (data) {
                    if (data == false) {
                        return;
                    }

                    if (args.__isnew == false) {
                        $scope.original.name = data.name;
                    }
                    else {
                        $scope.types.push({ id: data.id, name: data.name });
                    }
                });
        });

        $scope.getAllLeaveTypes();
    }
]);

managementapp.directive('timesheetitemtypeSidebar', [
    function () {
        return {
            templateUrl: window.razordata.siteprefix + 'angulartemplates/admin/timesheetitemtype/timesheetitemtype_sidebar.html',
            restrict: 'E',
            require: 'ngModel',
            scope: {
                types: '=ngModel'
            },
            link: function (scope, elt, attrs, ngModel) {
                scope.editType = function (type) {
                    scope.$emit('timesheetitemtype.edit', type);
                };
            }
        };
    }
]);

managementapp.directive('timesheetitemtypeEditor', ['$filter', 'FileUploader', 'bworkflowAdminApi', '$http',
    function ($filter, FileUploader, bworkflowAdminApi, $http) {
        return {
            templateUrl: window.razordata.siteprefix + 'angulartemplates/admin/timesheetitemtype/timesheetitemtype_editor.html',
            restrict: 'E',
            require: 'ngModel',
            scope: {
                type: '=ngModel'
            },
            link: function (scope, elt, attrs, ngModel) {
                scope.save = function () {
                    scope.$emit('timesheetitemtype.save', scope.type);
                };
            }
        };
    }
]);


// AUTHENTICATION Stuff
managementapp.controller('authenticationmanagementCtrl', [
    '$scope', '$http', '$q', '$filter', '$timeout', function ($scope, $http, $q, $filter, $timeout) {
        $scope.rooturl = window.razordata.siteprefix + 'Authenticate/';

        $scope.users = [];
        $scope.labels = [];
        $scope.showShortcuts = true;

        $scope.usersFromLocalStorage = function () {
            var u = localStorage.getItem('devicemanagement.shortcut.users');

            if (u == null) {
                $scope.users = [];
                $scope.showShortcuts = false;
                return;
            }

            $scope.users = JSON.parse(u);

            if ($scope.users.length == 0) {
                $scope.users = [];
                $scope.showShortcuts = false;
                return;
            }

            // we now need to merge the set of explicitly added users (stored in the shortcut.users localstorage)
            // with those that are maintained through labels and updated when logins occur (stored in shortcut.userswithlabels localstorage)
            var lu = localStorage.getItem('devicemanagement.shortcut.userswithlabels');
            if (lu != null && lu != '') {
                var labelledUsers = JSON.parse(lu);

                if (labelledUsers.length > 0) {
                    var hash = {};
                    angular.forEach($scope.users, function (item, index) {
                        hash[item.Username] = item;
                    });

                    angular.forEach(labelledUsers, function (item, index) {
                        if (angular.isDefined(hash[item.username]) == false) {
                            // ahh, the server is lowercasing the JSON its sending back
                            // where as the explicitly added users isn't. Let's force lowercase
                            // properties
                            var u = {
                                Id: item.id,
                                Username: item.username,
                                Name: item.name
                            };

                            $scope.users.push(u);
                            hash[u.Username] = u;
                        }
                    });
                }
            }

            $scope.users = $filter('orderBy')($scope.users, "Name");

            if ($scope.users.length > 0) {
                $scope.showShortcuts = true;
            }
        };

        $scope.labelsFromLocalStorage = function () {
            var l = localStorage.getItem('devicemanagement.shortcut.labels');

            if (l == null) {
                $scope.labels = [];
                return;
            }

            $scope.labels = JSON.parse(l);
        };

        $scope.labelledUsersToLocalStorage = function (users) {
            var u = JSON.stringify(users);

            localStorage.setItem('devicemanagement.shortcut.userswithlabels', u);
        };

        $scope.toggleShowShortcuts = function () {
            $scope.showShortcuts = !$scope.showShortcuts;
        };

        $scope.select = function (user, evt) {
            $scope.selected = user;
            evt.preventDefault();
        };

        $scope.login = function () {
            var parameters = {
                username: $scope.selected.Username,
                password: $scope.selected.password,
                refreshshortcutloginlabels: $scope.labels
            };

            $scope.selected.state = 'validating';

            $http.post($scope.rooturl + "ValidateAuthentication", parameters)
                .success(function (data) {
                    if (data.success == true) {
                        $scope.selected.state = 'redirecting';
                        $scope.selected.success = true;

                        // in case labels are being used to store additional users for shortcut login,
                        // we need to refresh our store of these now
                        if (data.labelshortcutlogins != null && data.labelshortcutlogins.length > 0) {
                            $scope.labelledUsersToLocalStorage(data.labelshortcutlogins);
                        }
                        else {
                            $scope.labelledUsersToLocalStorage([]);
                        }

                        $scope.$broadcast('authenticatesubmit.login', { Username: $scope.selected.Username, Password: $scope.selected.password });
                    }
                    else {
                        $scope.selected.state = '';
                        $scope.selected.success = false;
                    }
                });
        }

        $scope.labelsFromLocalStorage();

        $scope.usersFromLocalStorage();
    }
]);

managementapp.directive('authenticateSubmit', ['$timeout',
    function ($timeout) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                scope.$on('authenticatesubmit.login', function (evt, args) {
                    element.find('#authenticate_username').val(args.Username);
                    element.find('#authenticate_password').val(args.Password);

                    element.find('form').submit();
                })
            }
        };
    }]);

// DEVICE Management stuff
managementapp.controller('devicemanagementCtrl', [
    '$scope', '$http', '$q', '$filter', '$timeout', function ($scope, $http, $q, $filter, $timeout) {

        $scope.rooturl = window.razordata.siteprefix + 'Admin/DeviceManagement/';
        $scope.searcheduser = {};
        $scope.users = [];
        $scope.labels = [];
        $scope.firstChange = true;
        $scope.localStorageUsersKey = 'devicemanagement.shortcut.users';
        $scope.localStorageLabelsKey = 'devicemanagement.shortcut.labels';

        $scope.toLocalStorage = function (key, items) {
            var u = JSON.stringify(items);

            localStorage.setItem(key, u);
        };

        $scope.fromLocalStorage = function (key) {
            var u = localStorage.getItem(key);

            if (u == null) {
                return [];
            }

            var result = [];
            try {
                result = JSON.parse(u);
            }
            catch (e) {
                result = [];
            }

            return result;
        };

        $scope.$watch('searcheduser', function (newValue, oldValue) {
            if (newValue == null) {
                return;
            }

            if (angular.isDefined(newValue.Username) == false) {
                return;
            }

            var duplicate = false;
            angular.forEach($scope.users, function (item, index) {
                if (item.Id == newValue.Id) {
                    duplicate = true;
                }
            });

            if (duplicate == true) {
                return;
            }

            $scope.users.push(newValue);

            $scope.toLocalStorage($scope.localStorageUsersKey, $scope.users);
        });

        $scope.$watch('labels.length', function (newValue, oldValue) {
            if ($scope.firstChange == true) {
                $scope.firstChange = false;
                return;
            }

            $scope.toLocalStorage($scope.localStorageLabelsKey, $scope.labels);
        });

        $scope.removeShortcutUser = function (user) {
            var index = $.inArray(user, $scope.users);

            if (index == -1) {
                return;
            }

            $scope.users.splice(index, 1);
            $scope.toLocalStorage($scope.localStorageUsersKey, $scope.users);
        };

        $scope.users = $scope.fromLocalStorage($scope.localStorageUsersKey);
        $scope.labels = $scope.fromLocalStorage($scope.localStorageLabelsKey);
    }
]);

// EXECUTION HANDLER QUEUE STUFF

managementapp.controller('executionhandlerqueuemanagementCtrl', [
    '$scope', '$http', '$q', '$filter', '$timeout', function ($scope, $http, $q, $filter, $timeout) {

        $scope.rooturl = window.razordata.siteprefix + 'Admin/ExecutionHandlerQueueManagement/';
        $scope.showexception = false;

        $scope.getOpenHandlers = function () {
            $http.get($scope.rooturl + "OpenHandlers")
                .success(function (data) {
                    $scope.openhandlers = data;
                });
        };

        $scope.loadQueueItems = function (handler, primaryEntityId) {
            $http.get($scope.rooturl + "QueueItems", { params: { handler: handler, primaryentityid: primaryEntityId } })
                .success(function (data) {
                    $scope.editing = data;

                    if (data.length == 0) {
                        var index = $.inArray($scope.original, $scope.openhandlers);

                        if (index == -1) {
                            return;
                        }

                        $scope.openhandlers.splice(index, 1);
                    }
                });
        }

        $scope.$on('openhandler.edit', function (event, args) {
            $scope.original = args;

            $scope.loadQueueItems(args.handler, args.primaryentityid);
        });

        $scope.$on('openhandler.replay', function (event, args) {
            $http.post($scope.rooturl + "Replay", { id: args.id })
                .success(function (data) {
                    if (angular.isDefined(data.IsException) == true) {
                        // it didn't go through on replay
                        $scope.showexception = true;
                        $scope.exception = data;
                    }
                    else {
                        // it succeeded, yeah
                        $scope.loadQueueItems($scope.original.handler, $scope.original.primaryentityid);
                    }
                });
        });

        $scope.getOpenHandlers();
    }
]);

managementapp.directive('executionhandlerqueueSidebar', [
    function () {
        return {
            templateUrl: window.razordata.siteprefix + 'angulartemplates/admin/executionhandlerqueue/executionhandlerqueue_sidebar.html',
            restrict: 'E',
            require: 'ngModel',
            scope: {
                openhandlers: '=ngModel'
            },
            link: function (scope, elt, attrs, ngModel) {
                scope.editHandler = function (handler) {
                    scope.$emit('openhandler.edit', handler);
                };
            }
        };
    }
]);

managementapp.directive('executionhandlerqueueEditor', ['$filter', 'FileUploader', 'bworkflowAdminApi', '$http',
    function ($filter, FileUploader, bworkflowAdminApi, $http) {
        return {
            templateUrl: window.razordata.siteprefix + 'angulartemplates/admin/executionhandlerqueue/executionhandlerqueue_editor.html',
            restrict: 'E',
            require: 'ngModel',
            scope: {
                items: '=ngModel'
            },
            link: function (scope, elt, attrs, ngModel) {
                scope.replay = function (handler) {
                    scope.$emit('openhandler.replay', handler);
                }
            }
        };
    }
]);

// ok this controller is used as a means to inject angular into the jquery dominated script code
// used by the resource designer client side script. Since allot of what is returned there is done
// through jquery ajax calls the angular controllers and directives aren't loaded unless there is
// a call to the $compile function. The ajax success functions need to do something like the following

// <div id="ELEMENT WHERE STUFF IS TO BE INJECTED" ng-controller="questionEditorInjectorCtrl"></div>
// var injector = angular.element("#ELEMENT WHERE STUFF IS TO BE INJECTED").scope();
// injector.inject($("#ELEMENT WHERE STUFF IS TO BE INJECTED"), responseText);

// the above then allows this controller to hook angular in and we can leverage angular for the question
// editors. I've done this work for the pages side of things (note if a question loads stuff via ajax, then
// that will specifically need to change its element.html() type code to this approach for angular to work. If
// there are any facilities that we want to use Angular for their UI, then this work will need to be done there
// generally too.
managementapp.controller('questionEditorInjectorCtrl', [
    '$scope', '$compile', function ($scope, $compile) {
        $scope.inject = function (element, content) {
            var template = element.html(content);
            var linkFn = $compile(template);
            var elt = linkFn($scope);

            $scope.$apply();
        };
    }
]);

// Checklist designer stuff

managementapp.directive('datasourceQuestionEditor', ['$filter', '$http',
    function ($filter, $http) {
        return {
            templateUrl: window.razordata.siteprefix + 'angulartemplates/admin/designer/datasource_question_editor.html',
            restrict: 'E',
            link: function (scope, elt, attrs, ngModel) {
                // load up our data from the relevant container
                scope.formelement = attrs.formelement;

                scope.feeds = JSON.parse($('#' + scope.formelement).val());

                scope.currentFeed = null;
                scope.editFeed = false;
                scope.localurl = attrs.localurl;

                scope.availableformats = ['date', 'number', 'currency', 'image', 'images', 'membership image', 'task type image', 'task finish status'];
                scope.parameterinstructions = ['none', 'toUtc'];
                scope.datascopeinfo = { DataScope: null, MetaData: {} };

                scope.editDataScope = function () {
                    if (scope.datascopeinfo && scope.datascopeinfo.DataScope != null) {
                        var datascope = angular.copy(scope.datascopeinfo.DataScope);  // #1
                        try {
                            if (scope.currentFeed.datascope != null) {
                                datascope = JSON.parse(scope.currentFeed.datascope);
                            }
                        } catch (e) {
                            // will use Feeds Default DataScope from above #1
                            if (angular.isDefined(scope.datascopeinfo.DataScope.QueryScope)) {
                                // The old datascope was specifically User QueryScope enum { Normal, IncludeCurrentUser, OnlyCurrentUser }
                                // we will try and parse that into new JSON format ..
                                datascope = angular.extend(datascope, {
                                    QueryScope: ["Normal", "IncludeCurrentUser", "OnlyCurrentUser"].indexOf(scope.currentFeed.datascope)
                                });
                                if (datascope.QueryScope == -1)
                                    datascope.QueryScope = 0;       // Default to Normal
                                datascope.QueryScope = datascope.QueryScope.toString();
                            }
                        }

                        $("#DataScopePropertyGrid").jqPropertyGrid(datascope, { meta: scope.datascopeinfo.MetaData });
                    }
                }

                scope.$watch("currentFeed.feed", function (newvalue, oldvalue) {
                    if (angular.isDefined(newvalue) == false || newvalue == null || newvalue == '') {
                        return;
                    }

                    var url = scope.getFeedUrl();

                    url = url + "/" + newvalue;

                    $http({ url: url, method: 'GET', params: { $top: 1 } })
                        .success(function (data) {
                            scope.feedFields = [];
                            if (data.value.length > 0) {
                                angular.forEach(data.value[0], function (value, key) {
                                    scope.feedFields.push(key);
                                });
                            }
                        });

                    scope.datascopeinfo = { DataScope: null, MetaData: {} };
                    $http({ url: url + '/DataScopeInfo', method: 'POST' })
                        .success(function (data) {
                            if (data) {
                                scope.datascopeinfo = data;

                                scope.editDataScope();
                            }
                        });


                });

                scope.getLocalEndPoints = function () {
                    var url = scope.getFeedUrl();

                    $http({ url: url, method: 'GET' })
                        .success(function (data) {
                            scope.localendpoints = data.value;
                        });
                };

                scope.getFeedUrl = function () {
                    var url = scope.localurl;

                    if (url == null || url == '') {
                        url = 'odata';
                    }

                    return url;
                };

                scope.add = function () {
                    scope.edit({
                        __isnew: true,
                        name: null,
                        uselocalfeed: true,
                        feed: null,
                        visiblefields: null,
                        orderbyfields: null,
                        keyfields: null,
                        transformaroundfields: null,
                        transformtocolumntitlesfields: null,
                        transformtocolumnvaluesfields: null,
                        fieldformats: [],
                        parameterdefinitions: [],
                        filter: null,
                        expandfields: null,
                        selectfields: null,
                        usepaging: true,
                        includecount: false,
                        itemsperpage: 10,
                        refreshperiodseconds: null,
                        datascope: scope.datascopeinfo.DataScope ? JSON.stringify(scope.datascopeinfo.DataScope) : ''
                    });
                };

                scope.edit = function (feed) {
                    scope.currentFeed = feed;
                    scope.editFeed = true;

                    scope.editDataScope();
                };

                scope.feedsBackToElement = function () {
                    $('#' + scope.formelement).val(JSON.stringify(scope.feeds));
                };

                scope.resetDataScope = function () {
                    var datascope = angular.copy(scope.datascopeinfo.DataScope);
                    $("#DataScopePropertyGrid").jqPropertyGrid(datascope, { meta: scope.datascopeinfo.MetaData });
                }

                scope.save = function (feed) {
                    if (scope.datascopeinfo.DataScope) {
                        scope.currentFeed.datascope = JSON.stringify($('#DataScopePropertyGrid').jqPropertyGrid('get'));
                    } else {
                        scope.currentFeed.datascope = '';
                    }
                    if (feed.__isnew == true) {
                        scope.feeds.push(feed);
                        feed.__isnew = false;
                    }

                    scope.feedsBackToElement();

                    scope.editFeed = false;
                };

                scope.close = function () {
                    scope.editFeed = false;
                };

                scope.confirmDelete = function () {
                    if (confirm('Are you sure you want to delete the feed?') == true) {
                        scope.delete(scope.currentFeed);
                        scope.editFeed = false;
                    }
                };

                scope.delete = function (feed) {
                    var index = $.inArray(feed, scope.feeds);

                    if (index == -1) {
                        return;
                    }

                    scope.feeds.splice(index, 1);

                    scope.feedsBackToElement();
                };

                scope.addFormat = function () {
                    var f = {
                        fieldname: '',
                        format: 'date',
                        data: ''
                    };

                    scope.currentFeed.fieldformats.push(f);
                };

                scope.deleteFormat = function (format) {
                    var index = $.inArray(format, scope.currentFeed.fieldformats);

                    if (index == -1) {
                        return;
                    }

                    scope.currentFeed.fieldformats.splice(index, 1);
                };

                scope.addParameter = function () {
                    var p = {
                        internalname: '',
                        externalnames: '',
                        prequeryinstruction: 'none',
                        defaultvalue: ''
                    };

                    scope.currentFeed.parameterdefinitions.push(p);
                };

                scope.deleteParameter = function (param) {
                    var index = $.inArray(param, scope.currentFeed.parameterdefinitions);

                    if (index == -1) {
                        return;
                    }

                    scope.currentFeed.parameterdefinitions.splice(index, 1);
                };

                scope.getLocalEndPoints();
            }
        };
    }
]);

managementapp.directive('tasklistQuestionEditor', ['$filter', '$http',
    function ($filter, $http) {
        return {
            templateUrl: window.razordata.siteprefix + 'angulartemplates/admin/designer/tasklist_question_editor.html',
            restrict: 'E',
            scope: {},
            link: function (scope, elt, attrs, ngModel) {
                // load up our data from the relevant container
                scope.formelement = attrs.formelement;

                scope.taskcreationtemplates = JSON.parse($('#' + scope.formelement).val());
                scope.availabletemplatetypes = ['Normal', 'Shortcut'];

                // currently this is getting called from the save method of the TaskListQuestionEditor.cshtml
                scope.templatesBackToElement = function () {
                    $('#' + scope.formelement).val(JSON.stringify(scope.taskcreationtemplates));
                };

                scope.$on('tasklist-question-editor.save', function () {
                    scope.templatesBackToElement();
                });

                scope.getAllTypes = function () {
                    $http.get(window.razordata.siteprefix + 'Admin/TaskTypeManagement/Types')
                        .success(function (data) {
                            scope.tasktypes = data;
                        });
                };

                scope.getAllRosters = function () {
                    $http.get(window.razordata.siteprefix + 'Admin/RosterManagement/Rosters')
                        .success(function (data) {
                            scope.rosters = data.rosters;
                        });
                };

                scope.add = function () {
                    scope.taskcreationtemplates.push({
                        __isnew: true,
                        tasktypeid: null,
                        text: '',
                        requiresbasicdetails: false,
                        requiressitedetails: false,
                        type: 'normal'
                    });
                };

                scope.delete = function (t) {
                    var index = $.inArray(t, scope.taskcreationtemplates);

                    if (index == -1) {
                        return;
                    }

                    scope.taskcreationtemplates.splice(index, 1);
                };

                scope.getAllTypes();
                scope.getAllRosters();
            }
        };
    }
]);

managementapp.directive('languageSidebar', [
    function () {
        return {
            templateUrl: window.razordata.siteprefix + 'angulartemplates/admin/language/language_sidebar.html',
            restrict: 'E',
            require: 'ngModel',
            scope: {
                languages: '=ngModel'
            },
            link: function (scope, elt, attrs, ngModel) {
                scope.editLanguage = function (language) {
                    scope.$emit('language.edit', language);
                };
            }
        };
    }
]);

managementapp.controller('razorFriendly', ['$scope', function ($scope) {

}]);

managementapp.directive('languageEditor', ['languageSvc',
    function (languageSvc) {
        return {
            templateUrl: window.razordata.siteprefix + 'angulartemplates/admin/language/language_editor.html',
            restrict: 'E',
            require: 'ngModel',
            scope: {
                language: '=ngModel'
            },
            link: function (scope, elt, attrs, ngModel) {
                scope.save = function () {
                    scope.$emit('language.save', scope.language);
                }
                scope.textFilter = '';
                scope.itemsPerPageList = [10, 25, 100];
                scope.itemsPerPage = scope.itemsPerPageList[0];

                scope.filters = [
                    {
                        name: 'All',
                        filterFn: function () {
                            return function (item) {
                                return true;
                            };
                        }
                    },
                    {
                        name: 'Not translated',
                        filterFn: function () {
                            return function (item) {
                                return item.culturename != scope.language.culturename;
                            };
                        }
                    },
                    {
                        name: 'Translated',
                        filterFn: function () {
                            return function (item) {
                                return item.culturename == scope.language.culturename;
                            };
                        }
                    },
                    {
                        name: 'Same as English',
                        filterFn: function () {
                            return function (item) {
                                return item.englishtext == item.originalnativetext && item.culturename == scope.language.culturename;
                            };
                        }
                    }
                ];
                scope.filter = scope.filters[0];
                scope.setFilter = function (f) {
                    scope.filter = f;
                };
                scope.autoTranslate = function () {
                    languageSvc.autoTranslate(scope.language.culturename).then(function (result) {
                        languageSvc.getTranslations(scope.language.culturename).then(function (data) {
                            scope.language.translations = data;
                        });
                    });
                };
            }
        };
    }
]);

// WORK LOADING STUFF

managementapp.factory('workloadingSvc', ['$http', '$q', 'measurementSvc', function ($http, $q, measurementSvc) {
    var svc = {};
    var rooturl = window.razordata.siteprefix;

    var _features;
    svc.features = function () {
        var deferred = $q.defer();
        if (!_features) {
            $http.get(rooturl + '/Admin/WorkLoadingFeatureManagement/Features')
                .success(function (data) {
                    _features = {};
                    angular.forEach(data, function (i) {
                        _features[i.id] = i;
                    });
                    deferred.resolve(angular.copy(_features));
                });
        } else {
            deferred.resolve(angular.copy(_features));
        }
        return deferred.promise;
    }

    svc.feature = function (id) {
        var deferred = $q.defer();
        svc.features().then(function (features) {
            // no need to clone since features() promises a clone on each call
            deferred.resolve(features[id]);
        });
        return deferred.promise;
    }

    var _activities;
    svc.activities = function () {
        var deferred = $q.defer();
        if (!_activities) {
            $http.get(rooturl + '/Admin/WorkLoadingActivityManagement/Activities')
                .success(function (data) {
                    _activities = {};
                    angular.forEach(data, function (i) {
                        _activities[i.id] = i;
                    });
                    deferred.resolve(angular.copy(_activities));
                });
        } else {
            deferred.resolve(angular.copy(_activities));
        }
        return deferred.promise;
    }

    svc.activity = function (id) {
        var deferred = $q.defer();
        svc.activities().then(function (activities) {
            // no need to clone since activities() promises a clone on each call
            deferred.resolve(activities[id]);
        });
        return deferred.promise;
    }

    // note a group is not a real DTO/Db record, it simply represents a collection
    // of standards grouped by their feature(id)
    svc.group = function (args) {
        var self = this;

        args = args || {};
        self.bookid = args.bookid;

        var _featureid;
        self.feature = null;
        Object.defineProperty(self, 'featureid', {
            get: function () {
                return _featureid;
            },
            set: function (value) {
                _featureid = value;
                self.feature = !value ? null : svc.feature(value).then(function (feature) {
                    self.feature = feature;
                });
            }
        });
        self.featureid = args.featureid;

        self.clone = function () {
            return new svc.group(self);
        }

        self.standards = args.standards || [];
    }

    svc.standard = function (args) {
        var self = this;

        args = args || {};
        self.__isnew = angular.isDefined(args.__isnew) ? args.__isnew : true;
        self.id = args.id;
        self.bookid = args.bookid;

        var _featureid;
        self.feature = null;
        Object.defineProperty(self, 'featureid', {
            get: function () {
                return _featureid;
            },
            set: function (value) {
                _featureid = value;
                self.feature = !value ? null : svc.feature(value).then(function (feature) {
                    self.feature = feature;
                    if (angular.isDefined(args.measurement)) {
                        self.measurement = args.measurement.clone();
                    } else {
                        self.measurement = new measurementSvc.measurement({
                            dimensions: self.feature.dimensions,
                            distance: args.measure,
                            unitid: args.unitid
                        });
                    }
                });
            }
        });
        self.featureid = args.featureid;

        var _activityid;
        self.activity = null;
        Object.defineProperty(self, 'activityid', {
            get: function () {
                return _activityid;
            },
            set: function (value) {
                _activityid = value;
                self.activity = !value ? null : svc.activity(value).then(function (activity) {
                    self.activity = activity;
                });
            }
        });
        self.activityid = args.activityid;

        self.seconds = args.seconds;

        self.toDto = function () {
            return {
                __isnew: self.__isnew,
                id: self.id,
                bookid: self.bookid,
                featureid: self.featureid,
                activityid: self.activityid,
                measure: self.measurement.distance.toString(),
                unitid: self.measurement.unitid,
                seconds: self.seconds.toString(),
            }
        }

        self.clone = function () {
            return new svc.standard(self);
        }
    }

    svc.sitefeature = function (args) {
        var self = this;

        args = args || {};
        self.__isnew = angular.isDefined(args.__isnew) ? args.__isnew : true;
        self.id = args.id;
        self.siteid = args.siteid;
        self.siteareaid = args.siteareaid;
        self.name = args.name;

        var _workloadingfeatureid;
        self.workloadingfeature = null;
        Object.defineProperty(self, 'workloadingfeatureid', {
            get: function () {
                return _workloadingfeatureid;
            },
            set: function (value) {
                _workloadingfeatureid = value;
                self.workloadingfeature = !value ? null : svc.feature(value).then(function (feature) {
                    self.workloadingfeature = feature;
                    self.name = self.name || feature.name;  // default to name of feature
                    if (angular.isDefined(args.measurement)) {
                        self.measurement = args.measurement.clone();
                    } else {
                        self.measurement = new measurementSvc.measurement({
                            dimensions: self.workloadingfeature.dimensions,
                            distance: args.measure,
                            unitid: args.unitid
                        });
                    }
                });
            }
        });
        self.workloadingfeatureid = args.workloadingfeatureid;

        self.toDto = function () {
            return {
                __isnew: self.__isnew,
                id: self.id,
                siteid: self.siteid,
                siteareaid: self.siteareaid,
                workloadingfeatureid: self.workloadingfeatureid,
                measure: self.measurement.distance.toString(),
                unitid: self.measurement.unitid,
                name: self.name
            }
        }

        self.clone = function () {
            return new svc.sitefeature(self);
        }
    }
    return svc;
}]);

managementapp.factory('measurementSvc', ['$http', '$q', function ($http, $q) {
    var svc = {};
    var rooturl = window.razordata.siteprefix + 'Admin/MeasurementUnitManagement/';

    var _dimensions;
    svc.dimensions = function () {
        var deferred = $q.defer();
        if (!_dimensions) {
            $http.get(rooturl + 'GetDimensions')
                .success(function (data) {
                    _dimensions = data;
                    deferred.resolve(data);
                });
        } else {
            deferred.resolve(_dimensions);
        }
        return deferred.promise;
    }

    var _units;
    svc.units = function (dimension) {
        var filterFn = angular.isDefined(dimension) ? function (units) {
            return units.filter(function (u) {
                return u.dimensions == dimension;
            })
        } : angular.identity;

        var deferred = $q.defer();
        if (!_units) {
            $http.get(rooturl + 'GetUnits')
                .success(function (data) {
                    _units = data;
                    _units.byId = {};
                    _units.byName = {};
                    angular.forEach(data, function (u) {
                        _units.byId[u.id] = u;
                        _units.byName[u.shortname] = u;
                    });

                    deferred.resolve(filterFn(_units));
                });
        } else {
            deferred.resolve(filterFn(_units));
        }
        return deferred.promise;
    }

    svc.unit = function (id) {
        var deferred = $q.defer();
        svc.units().then(function (units) {
            deferred.resolve(units.byId[id]);
        });
        return deferred.promise;
    }

    svc.measurement = function (args) {
        var self = this;

        var _unitid, _unit;
        Object.defineProperty(self, 'unitid', {
            get: function () {
                return _unitid;
            },
            set: function (value) {
                if (_unitid != value) {
                    _unitid = value;
                    svc.unit(value).then(function (unit) {
                        _unit = unit;
                    })
                }
            }
        });
        Object.defineProperty(self, 'unit', {
            get: function () {
                return _unit;
            },
            set: function (unit) {
                _unit = unit;
                _unitid = unit.id;
            }
        });

        self.distance = args.distance || 0;
        self.unitid = args.unitid;
        self.dimensions = angular.isDefined(args.dimensions) ? args.dimensions : 1;

        self.toUnit = function (toUnitId) {
            return new svc.measurement({
                distance: self.distance / _units.byId[toUnit].perMetre,
                unitid: toUnitId,
                dimensions: self.dimensions
            });
        }

        self.toMetres = function () {
            return self.toUnit(1);      // metres
        }

        self.toString = function () {
            return self.distance.toString() + ' ' + (angular.isDefined(self.unit) ? self.unit.shortname : '');
        }

        self.clone = function () {
            return new svc.measurement(self);
        }

        // retrieve the list of units that are available with our dimension
        svc.units(self.dimensions).then(function (units) {
            self.units = units;
            self.unitid = self.unitid || units[0].id;       // default to 1st unit
        });

    }

    return svc;
}]);

managementapp.directive('durationSeconds', function ($window) {
    return {
        restrict: 'E',
        templateUrl: window.razordata.siteprefix + 'angulartemplates/duration.html',
        replace: true,
        require: 'ngModel',
        scope: {
            seconds: '=ngModel'
        },
        link: function (scope, element, attrs) {

            scope.$watch('seconds', function (newValue) {
                var duration = moment.duration(newValue, 'seconds');
                scope.duration = {
                    hours: duration.hours(),
                    minutes: duration.minutes(),
                    seconds: duration.seconds()
                }
            });

            scope.$watchCollection('duration', function (newTime, oldTime) {
                scope.seconds = moment.duration(scope.duration).asSeconds();
            });
        }
    };
});

managementapp.directive('measurement', [
    function () {
        return {
            templateUrl: window.razordata.siteprefix + 'angulartemplates/measurement.html',
            restrict: 'E',
            require: 'ngModel',
            scope: {
                measurement: '=ngModel',
                editing: '='
            },
            link: function (scope, elt, attrs, ngModel) {
            }
        };
    }
]);

// Utility fn to throw an arg, can be used like
// options.mandatory = options.mandatory || _throw('Option is mandatory');
//
var _throwFn = function (arg) {
    throw arg;
}

managementapp.controller('workloadingbookmanagementCtrl', ['$scope', '$http', 'recordControllerFactory', 'workloadingSvc', 'FileUploader',
    function ($scope, $http, factory, workloadingSvc, FileUploader) {
        var ctrl = factory($scope, {
            name: 'Book',
            mvcControllerUrl: 'Admin/WorkLoadingBookManagement/',
            modelFromDto: function (dto) {
                var model = angular.copy(dto);
                // convert the flat array into 
                // { 
                //   feature1id: { feature, standard: [ activity1, ... ] },
                //   feature2id: { feature, standard: [ activity2, ... ] }, ...
                // }
                var groups = {};
                angular.forEach(model.standards, function (s) {
                    groups[s.featureid] = groups[s.featureid] ||
                        new workloadingSvc.group({ featureid: s.featureid, bookid: s.bookid });
                    groups[s.featureid].standards.push(new workloadingSvc.standard(s));
                })
                delete model.standards;
                model.groups = Object.values(groups);
                return model;
            },
            modelToDto: function (model) {
                // convert standards back to flat array
                var dto = angular.copy(model);
                dto.standards = [];

                angular.forEach(model.groups, function (group) {
                    angular.forEach(group.standards, function (standardDto) {
                        dto.standards.push(standard);
                    });
                });

                return dto;
            }
        });

        $scope.editingStandard = false;

        $scope.$on('standard.edit', function (event, data) {
            $scope.canDelete = !data.__isnew;
            $scope.editingStandard = true;
            $scope.originalStandard = data;
            $scope.currentStandard = data.clone();
        })
        /*
                $scope.activityFilter = function (data) {
                    // Remove Activities that are already set
                    return data.filter(function (activity) {
                        return !$scope.group.standards.find(function (standard) {
                            return standard.activityid == activity.id;
                        });
                    })
                }
                */

        $scope.commitStandard = function (model) {
            var parameters = { record: model.toDto() };
            $http.post($scope.rooturl + 'StandardSave', parameters)
                .success(function (data) {
                    if (data == false) {
                        return;
                    }

                    angular.copy($scope.currentStandard, $scope.originalStandard);

                    if (model.__isnew) {
                        model.id = $scope.originalStandard.id = data.id;
                        model.__isnew = $scope.originalStandard.__isnew = false;
                        var group = $scope.editing.groups.find(function (group) {
                            return group.featureid == model.featureid;
                        })
                        group.standards.push($scope.originalStandard);
                    }

                    $scope.editingStandard = false;
                });
        };

        $scope.deleteStandard = function (model) {
            $http.delete($scope.rooturl + 'Standard?id=' + model.id)
                .success(function (data) {
                    if (data == false) {
                        return;
                    }

                    var group = $scope.editing.groups.find(function (group) {
                        return group.featureid == model.featureid;
                    })
                    group.standards.remove(model);

                    $scope.editingStandard = false;
                });
        }

        $scope.confirmDeleteStandard = function () {
            if (confirm('This operation cannot be undone, are you sure you want to delete this standard ?') == false) {
                return;
            }

            $scope.deleteStandard($scope.originalStandard);
        };

        $scope.bookImportBehaviours = [
            { id: 0, text: 'Create New' },
            { id: 1, text: 'Overwrite' },
            { id: 2, text: 'Merge Replace' },
            { id: 3, text: 'Merge Ignore' }]

        $scope.otherImportBehaviours = [
            { id: 0, text: 'Create New' },
            { id: 2, text: 'Merge Replace' },
            { id: 3, text: 'Merge Ignore' }]

        $scope.showImportBookDialog = false;
        $scope.uploader = new FileUploader();
        $scope.uploader.url = window.razordata.siteprefix + 'Admin/WorkLoadingBookManagement/BookImport';
        $scope.uploader.alias = 'file';

        $scope.uploader.onAfterAddingFile = function (item) {
            item.id = UUID.generate();
            item.bookBehaviour = $scope.bookImportBehaviours[2];
            item.activityBehaviour = $scope.otherImportBehaviours[1];
            item.featureBehaviour = $scope.otherImportBehaviours[1];
            item.onError = function (response, status, headers) {
                item.errorText = response;

                $('#' + item.id).popover({ html: true, trigger: 'hover', content: item.errorText, placement: 'bottom' });
            }
        }

        $scope.uploader.onBeforeUploadItem = function (item) {
            item.formData = [{
                bookBehaviour: item.bookBehaviour.id,
                activityBehaviour: item.activityBehaviour.id,
                featureBehaviour: item.featureBehaviour.id
            }]
        }
        $scope.importBooks = function () {
            $scope.uploader.clearQueue();
            $scope.showImportBookDialog = true;
        }

        $scope.uploadBooks = function () {
            $scope.uploader.uploadAll();
        }
    }
]);

managementapp.directive('workloadingbookSidebar', ['recordAjaxSidebarFactory',
    function (factory) {
        return factory({
            templateUrl: 'angulartemplates/admin/workloading/workloadingbook_sidebar.html',
            ajaxUrl: function (page, search, itemsPerPage, includeArchived) {
                return 'Admin/WorkLoadingBookManagement/Search?pageNumber=' + page + '&search=' + search + '&itemsPerPage=' + itemsPerPage + '&includeArchived=' + includeArchived;
            },

        })
    }
]);

managementapp.directive('workloadingbookEditor', ['recordEditorFactory', 'measurementSvc', 'workloadingSvc',
    function (factory, measurementSvc, workloadingSvc) {
        return factory({
            templateUrl: 'angulartemplates/admin/workloading/workloadingbook_editor.html',
            link: function (scope, elt, attrs, ngModel) {
                scope.newGroup = function () {
                    var group = new workloadingSvc.group({
                        featureid: scope.newFeatureId,
                        bookid: scope.record.id
                    });

                    scope.record.groups.push(group);

                    scope.newFeatureId = null;
                }

                scope.filteredGroups = scope.record.groups;

                scope.exportLink = window.razordata.siteprefix + 'Admin/WorkLoadingBookManagement/BookExport?id=' + scope.record.id;
            }
        })
    }
]);

managementapp.directive('workloadingStandardGroupEditor', ['workloadingSvc', function (workloadingSvc) {
    return {
        templateUrl: window.razordata.siteprefix + 'angulartemplates/admin/workloading/workloadingstandardgroup_editor.html',
        requires: 'ngModel',
        restrict: 'E',
        scope: {
            group: '=ngModel'
        },
        link: function (scope, elm, attrs, ngModel) {
            scope.time = function (standard) {
                var now = moment();
                var then = moment(now).add(standard.seconds, 'seconds');
                return now.preciseDiff(then);
            };

            scope.addActivity = function () {
                var standard = new workloadingSvc.standard({
                    featureid: scope.group.featureid,
                    bookid: scope.group.bookid
                });
                standard.editing = true;

                scope.$emit('standard.edit', standard);
            }

            scope.editStandard = function (standard) {
                scope.$emit('standard.edit', standard);
            }
        }
    }
}])

managementapp.directive('workloadingStandardEditor', ['workloadingSvc', function (workloadingSvc) {
    return {
        templateUrl: window.razordata.siteprefix + 'angulartemplates/admin/workloading/workloadingstandard_editor.html',
        requires: 'ngModel',
        restrict: 'E',
        scope: {
            standard: '=ngModel',
            group: '='
        },
        link: function (scope, elm, attrs, ngModel) {
        }
    }
}])

managementapp.directive('workloadingStandardSelector', [
    function () {
        return {
            templateUrl: window.razordata.siteprefix + 'angulartemplates/admin/Workloading/workloadingstandard_selector.html',
            restrict: 'E',
            require: 'ngModel',
            scope: {
                standards: '=ngModel'
            },
            link: function (scope, elt, attrs, ngModel) {
                scope.$watch('standards', function (newValue, oldValue) {
                    if (newValue == null || angular.isDefined(newValue) == false) {
                        return;
                    }

                    scope.books = {};

                    // we build up a slightly transformed model here to make displaying
                    // things in our UI a little easier. Basically just build the flat
                    // list we have up into a hierarchy of objects
                    angular.forEach(scope.standards, function (item) {
                        // there are a few levels we build up, the objet graph looks like
                        // book
                        // -- area
                        // ---- feature
                        // ------ activity

                        if (angular.isDefined(scope.books[item.book]) == false) {
                            scope.books[item.book] = { book: item.book, areas: [], areasByName: {} };
                        }

                        var book = scope.books[item.book];

                        if (angular.isDefined(book.areasByName[item.area]) == false) {
                            var firstArea = { area: item.area, features: {} };
                            book.areasByName[item.area] = firstArea;
                            book.areas.push(firstArea);
                        }

                        var area = book.areasByName[item.area];

                        if (angular.isDefined(area.features[item.feature]) == false) {
                            area.features[item.feature] = {
                                feature: item.sitefeaturename == item.feature ? item.feature : item.sitefeaturename + ' - ' + item.feature,
                                activities: []
                            };
                        }

                        area.features[item.feature].activities.push(item);
                    });

                    if (scope.standards.length > 0) {
                        var book = scope.books[scope.standards[0].book]
                        scope.selectBook(book);
                        scope.selectArea(book.areas[0]);
                    }
                });

                scope.selectBook = function (book) {
                    scope.selectedBook = book;
                    scope.selectArea(book.areas[0]);
                };

                scope.selectArea = function (area) {
                    scope.selectedArea = area;
                }

                scope.editDuration = function (activity) {
                    activity.estimatedduration = activity.calculatedduration
                }
            }
        };
    }
]);

// WORK LOADING ACTIVITY

managementapp.controller('workloadingactivitymanagementCtrl', ['$scope', 'recordControllerFactory', function ($scope, factory) {
    return factory($scope, {
        name: 'Activity',
        mvcControllerUrl: 'Admin/WorkLoadingActivityManagement/',
    })
}]);

managementapp.directive('workloadingactivitySidebar', ['recordAjaxSidebarFactory', function (factory) {
    return factory({
        templateUrl: 'angulartemplates/admin/workloading/workloadingactivity_sidebar.html',
        ajaxUrl: function (page, search, itemsPerPage, includeArchived) {
            return 'Admin/WorkLoadingActivityManagement/Search?pageNumber=' + page + '&search=' + search + '&itemsPerPage=' + itemsPerPage + '&includeArchived=' + includeArchived;
        },
    });
}]);

managementapp.directive('workloadingactivityEditor', ['recordEditorFactory', function (factory) {
    return factory({
        templateUrl: 'angulartemplates/admin/workloading/workloadingactivity_editor.html'
    });
}]);

managementapp.controller('workloadingfeaturemanagementCtrl', ['$scope', 'recordControllerFactory', function ($scope, factory) {
    return factory($scope, {
        name: 'Feature',
        mvcControllerUrl: 'Admin/WorkLoadingFeatureManagement/',
    })
}]);

managementapp.directive('workloadingfeatureSidebar', ['recordAjaxSidebarFactory', function (factory) {
    return factory({
        templateUrl: 'angulartemplates/admin/workloading/workloadingfeature_sidebar.html',
        ajaxUrl: function (page, search, itemsPerPage, includeArchived) {
            return 'Admin/WorkLoadingFeatureManagement/Search?pageNumber=' + page + '&search=' + search + '&itemsPerPage=' + itemsPerPage + '&includeArchived=' + includeArchived;
        },

    });
}]);

managementapp.directive('workloadingfeatureEditor', ['recordEditorFactory', 'measurementSvc', function (factory, measurementSvc) {
    return factory({
        templateUrl: 'angulartemplates/admin/workloading/workloadingfeature_editor.html',
        link: function (scope, elt, attrs, ngModel) {
            measurementSvc.dimensions().then(function (data) {
                scope.dimensions = data;
            })
        }
    });
}]);

managementapp.factory('recordControllerFactory', [
    '$http', '$q', '$filter', '$timeout', function ($http, $q, $filter, $timeout) {

        return function ($scope, options) {
            options = options || {};
            options.name = options.name || 'Record';
            options.displayName = options.displayName || options.name;
            options.idField = options.idField || 'id';
            options.nameField = options.nameField || 'name';
            options.mvcControllerUrl = options.mvcControllerUrl || _throwFn("Must define mvcControllerUrl");
            options.actions = angular.extend({
                get: options.name,
                'delete': options.name + 'Delete',
                save: options.name + 'Save'
            }, options.actions || {})
            options.modelFromDto = options.modelFromDto || angular.identity;
            options.modelToDto = options.modelToDto || angular.identity;
            options.modelsFromDtos = options.modelsFromDtos || angular.identity;

            $scope.options = options;

            $scope.rooturl = window.razordata.siteprefix + options.mvcControllerUrl;

            $scope.newRecord = function () {
                $scope.editing = {
                    __isnew: true,
                    text: 'New ' + options.displayName,
                };
            };

            $scope.$on('newRecord', $scope.newRecord);

            $scope.editRecord = function (record) {
                $http.get($scope.rooturl + options.actions.get, { params: { id: record[options.idField] } })
                    .success(function (data) {
                        $scope.editing = options.modelFromDto(data);
                        if ($scope.recordLoaded) {
                            $scope.recordLoaded();
                        }
                    });
            }

            $scope.$on('record.edit', function (event, args) {
                $scope.editRecord(args);
            });

            $scope.saveRecord = function (args) {
                var record = options.modelToDto(args);
                var parameters = options.isAPI ? record : { record: record };

                // A save will always unarchive the record
                record.archived = false;

                $http.post($scope.rooturl + options.actions.save, parameters)
                    .success(function (data) {
                        if (data == false) {
                            return;
                        }

                        var model = options.modelFromDto(data);
                        angular.copy(model, args);
                        /*                    args.archived = false;
                                            if (args.__isnew == true) {
                                                args[options.idField] = data[options.idField];
                                                args.__isnew = false;
                                            }
                                            */
                        $scope.$broadcast('record.changed', args);
                    });
            }

            $scope.$on('record.save', function (event, args) {
                $scope.saveRecord(args);
            });

            $scope.$on('record.deleted', function (event, args) {
                if (args == $scope.editing[options.idField]) {
                    $scope.editing = null;
                }
            });

            $scope.delete = function (record) {
                $http.delete($scope.rooturl + options.actions.delete + "?id=" + record[options.idField])
                    .success(function (data) {
                        $scope.$broadcast('record.deleted', record[options.idField]);
                    })
            }

            $scope.$on('record.delete', function (event, args) {
                if (confirm("Are you sure you want to delete this " + options.displayName + " ?") == true) {
                    $scope.delete(args);
                }
            });

            $scope.$on('record.unarchive', function (event, args) {
                $scope.saveRecord(args);
            })
        }
    }
]);

managementapp.factory('recordSidebarFactory', [
    function () {
        return function (options) {
            options.templateUrl = options.templateUrl || _throwFn("Must define templateUrl");

            return {
                templateUrl: window.razordata.siteprefix + options.templateUrl,
                restrict: 'E',
                require: 'ngModel',
                scope: {
                    records: '=ngModel'
                },
                link: function (scope, elt, attrs, ngModel) {
                    scope.editRecord = function (record) {
                        scope.$emit('record.edit', record);
                    };

                    if (options.link)
                        options.link(scope, elt, attrs, ngModel);
                }
            };
        }
    }
]);

managementapp.factory('recordAjaxSidebarFactory', ['$http', '$injector',
    function ($http, $injector) {
        var _paginationid = 0;

        return function (options) {
            options.templateUrl = options.templateUrl || _throwFn("Must define templateUrl");
            options.ajaxUrl = options.ajaxUrl || _throwFn("Must define ajaxUrl");
            options.transformResult = options.transformResult || angular.identity;
            options.scope = angular.extend({
                itemsPerPage: '='
            }, options.scope || {});

            return {
                templateUrl: window.razordata.siteprefix + options.templateUrl,
                restrict: 'E',
                scope: options.scope,
                link: function (scope, elt, attrs, ngModel) {
                    scope.records = [];
                    scope.search = '';
                    scope.totalRecords = 0;
                    scope.pageNumber = 1;
                    scope.itemsPerPage = scope.itemsPerPage || 10;
                    scope.includeArchived = false;

                    scope.pageChanged = function (newPage) {
                        getResultsPage(newPage);
                    };

                    var getResultsPage = function (pageNumber) {
                        // this is just an example, in reality this stuff should be in a service
                        $http.get(window.razordata.siteprefix + options.ajaxUrl(pageNumber, scope.search, scope.itemsPerPage, scope.includeArchived, scope))
                            .then(function (result) {
                                scope.records = result.data.items.map(options.transformResult);
                                scope.totalRecords = result.data.count;
                            });
                    }

                    scope.refreshNow = function () {
                        getResultsPage(scope.pageNumber);
                    }

                    scope.refresh = scope.refreshNow.debounce(300);

                    scope.editRecord = function (record) {
                        scope.$emit('record.edit', record);
                    };

                    getResultsPage(1);

                    scope.$watch('search', scope.refresh);
                    scope.$watch('includeArchived', scope.refresh);
                    scope.$on('record.changed', scope.refresh);
                    scope.$on('record.deleted', scope.refresh);

                    if (options.link) {
                        options.link(scope, elt, attrs, ngModel);
                    }
                },
                controller: function ($scope) {
                    $scope.paginationid = "pagination_" + (_paginationid++).toString();

                    if (options.controller) {
                        $injector.invoke(options.controller, this, { $scope: $scope });
                    }
                }
            };
        }
    }
]);

managementapp.factory('recordEditorFactory', ['$filter', 'FileUploader', 'bworkflowAdminApi', '$http',
    function ($filter, FileUploader, bworkflowAdminApi, $http) {
        return function (options) {
            options.templateUrl = options.templateUrl || _throwFn("Must define templateUrl");

            return {
                templateUrl: window.razordata.siteprefix + options.templateUrl,
                restrict: 'E',
                require: 'ngModel',
                scope: {
                    record: '=ngModel'
                },
            controller: function ($scope) {
                if (options.controller) {
                    options.controller($scope);
                }
            },
                link: function (scope, elt, attrs, ngModel) {
                    scope.save = function () {
                        scope.$emit('record.save', scope.record);
                    };

                    scope.delete = function () {
                        scope.$emit('record.delete', scope.record);
                    }

                    scope.unarchive = function () {
                        scope.$emit('record.unarchive', scope.record);
                    }

                    if (options.link)
                        options.link(scope, elt, attrs, ngModel);
                }
            };
        }
    }
]);

managementapp.factory('typeaheadDirective', ['bworkflowAdminApi', '$timeout', '$http',
    function (bworkflowAdminApi, $timeout, $http) {
        return function (options) {
            options = options || {};
            options.selectObjectNotId = options.selectObjectNotId || false;
            options.templateUrl = options.templateUrl || 'angulartemplates/admin/select_single_item.html';
            options.ajaxItemName = options.ajaxItemName || 'Name';
            options.ajaxItemId = options.ajaxItemId || 'id';
            options.displayKey = options.displayKey || 'name';
            options.handleBars = options.handleBars || '<strong>{{name}}</strong>';
            options.ajaxItemUrl = options.ajaxItemUrl || 'ajaxItemUrl not defined';
            options.ajaxItemSearch = options.ajaxItemSearch || 'ajaxItemSearch not defined';

            // The directive class can perform a transformation on the search result
            options.transform = options.transform || angular.identity;

            // Additionally, the directive instance can perform a specific transformation
            options.scope = angular.extend({
                selected: '=ngModel',
                placeholder: '@',
                mandatory: '=',
                transformFn: '&'
            }, options.scope || {});

            return {
                templateUrl: window.razordata.siteprefix + options.templateUrl,
                restrict: 'E',
                require: "ngModel",
                scope: options.scope,
                controller: function ($scope) {
                    $scope.watchcontainer = { current: null };
                    $scope.options = {
                        highlight: true
                    };

                    var transformFn = options.transform;
                    if ($scope.transformFn()) {
                        transformFn = function (data) {
                            return options.transform($scope.transformFn()(data));
                        }
                    }

                    $scope.$watch('selected', function (newValue, oldValue) {
                        $scope.selectedname = "";

                        if (newValue == null || angular.isUndefined(newValue)) {
                            $scope.watchcontainer.current = null;
                            return;
                        }

                        if (!options.selectObjectNotId) {
                            // ok so we have an Id but no name, so hit the server for that
                            $http.get(window.razordata.siteprefix + options.ajaxItemUrl(newValue, $scope))
                                .success(function (data) {
                                    $scope.selectedname = data[options.ajaxItemName];
                                });
                        } else {
                            $scope.selectedname = newValue[options.ajaxItemName];
                        }
                    });

                    $scope.suggestion = new Bloodhound({
                        datumTokenizer: function (d) { return Bloodhound.tokenizers.whitespace(d[options.ajaxItemName]); },
                        queryTokenizer: Bloodhound.tokenizers.whitespace,
                        remote: {
                            url: window.razordata.siteprefix + options.ajaxItemSearch('%QUERY', $scope),
                            filter: transformFn
                        }
                    });

                    $scope.suggestion.initialize();
                    //$scope.userdataset = undefined;

                    $scope.dataset = {
                        displayKey: options.displayKey,
                        source: $scope.suggestion.ttAdapter(),
                        templates: {
                            suggestion: Handlebars.compile(options.handleBars)
                        }
                    };

                    //$timeout(function () {
                    //    $scope.userdataset = {
                    //        displayKey: 'name',
                    //        source: $scope.userSuggestion.ttAdapter()
                    //    };
                    //});

                    $scope.$watch('watchcontainer.current', function (newItem) {
                        if (!newItem || angular.isUndefined(newItem[options.ajaxItemId])) {
                            return;
                        }

                        if (!options.selectObjectNotId) {
                            $scope.selected = newItem[options.ajaxItemId];
                        } else {
                            $scope.selected = newItem;
                        }
                    });

                    $scope.clearselection = function () {
                        $scope.selected = null;
                        $scope.selectedname = "";
                    };
                },
                link: function (scope, elt, attrs, ngModel) {

                }
            };
        }
    }
]);

managementapp.directive('searchWorkLoadingFeature', ['typeaheadDirective', function (typeaheadDirective) {
    return typeaheadDirective({
        ajaxItemName: 'name',
        displayKey: 'name',
        handleBars: '<strong>{{name}}</strong>',
        ajaxItemUrl: function (id) {
            return 'Admin/WorkLoadingFeatureManagement/Feature/' + id;
        },
        ajaxItemSearch: function (query) {
            return 'Admin/WorkLoadingFeatureManagement/Search?search=' + query;
        },
        transform: function (data) {
            return data.items;
        }
    })
}])

managementapp.directive('searchWorkLoadingActivity', ['typeaheadDirective', function (typeaheadDirective) {
    return typeaheadDirective({
        ajaxItemName: 'name',
        displayKey: 'name',
        handleBars: '<strong>{{name}}</strong>',
        ajaxItemUrl: function (id) {
            return 'Admin/WorkLoadingActivityManagement/Activity/' + id;
        },
        ajaxItemSearch: function (query, scope) {
            return 'Admin/WorkLoadingActivityManagement/Search?search=' + query;
        },
        transform: function (data) {
            return data.items;
        }

    })
}])

managementapp.directive('selectSingleChecklist', ['typeaheadDirective', function (typeaheadDirective) {
    return typeaheadDirective({
        templateUrl: 'angulartemplates/admin/select_single_checklist.html',
        ajaxItemName: 'Name',
        displayKey: 'name',
        handleBars: '<strong>{{name}}</strong><br/><i>{{publishinggroup.name}}</i>',
        ajaxItemUrl: function (id) {
            return 'API/v1/Published/GetById?id=' + id;
        },
        ajaxItemSearch: function (query) {
            return 'SearchChecklist/SearchPublishedResources?search=' + query;
        }
    })
}])

managementapp.directive('selectSingleUser', ['typeaheadDirective', function (typeaheadDirective) {
    return typeaheadDirective({
        scope: {
            isASite: '='
        },
        templateUrl: 'angulartemplates/admin/select_single_item.html',
        ajaxItemName: 'Name',
        ajaxItemId: 'Id',
        displayKey: 'Name',
        handleBars: '<strong>{{Name}}</strong>',
        ajaxItemUrl: function (id) {
            return 'SearchUser/Details?id=' + id;
        },
        ajaxItemSearch: function (query, scope) {
            if (angular.isDefined(scope.isASite)) {
                return 'SearchUser?query=' + query + '&isASite=' + scope.isASite;
            } else {
                return 'SearchUser?query=' + query;
            }
        }
    })
}])

managementapp.controller('sitefeaturemanagementCtrl', ['$scope', 'recordControllerFactory', '$http', 'workloadingSvc',
    function ($scope, factory, $http, workloadingSvc) {
        var ctrl = factory($scope, {
            name: 'Site',
            idField: 'siteid',
            nameField: 'username',
            mvcControllerUrl: 'Admin/SiteFeatureManagement/',
            modelFromDto: function (dto) {
                var model = angular.copy(dto);
                delete model.sitefeatures;

                angular.forEach(model.siteareas, function (siteModel) {
                    siteModel.sitefeatures = [];
                })

                angular.forEach(dto.sitefeatures, function (featureDto) {
                    var area = model.siteareas.find(function (a) {
                        return a.id == featureDto.siteareaid;
                    })
                    area.sitefeatures.push(new workloadingSvc.sitefeature(featureDto));
                })
                return model;
            },
        })

        $scope.$on('sitefeature.edit', function (event, data) {
            $scope.canDeleteSiteFeature = !data.__isnew;
            $scope.editingSiteFeature = true;
            $scope.originalSiteFeature = data;
            $scope.currentSiteFeature = data.clone();
        })

        $scope.$on('sitearea.edit', function (event, data) {
            $scope.canDeleteSiteArea = !data.__isnew && data.sitefeatures.length == 0;
            $scope.editingSiteArea = true;
            $scope.originalSiteArea = data;
            $scope.currentSiteArea = angular.copy(data);
        })

        $scope.commitSiteFeature = function (model) {
            var parameters = { record: model.toDto() };
            $http.post($scope.rooturl + 'SiteFeatureSave', parameters)
                .success(function (data) {
                    if (data == false) {
                        return;
                    }

                    angular.copy($scope.currentSiteFeature, $scope.originalSiteFeature);

                    if (model.__isnew) {
                        model.__isnew = $scope.originalSiteFeature.__isnew = false;
                        model.id = $scope.originalSiteFeature.id = data.id;

                        var area = $scope.editing.siteareas.find(function (a) {
                            return a.id == model.siteareaid;
                        })
                        area.sitefeatures.push($scope.originalSiteFeature);
                    }

                    $scope.editingSiteFeature = false;
                });
        };

        $scope.deleteSiteFeature = function (model) {
            $http.delete($scope.rooturl + 'SiteFeature?id=' + model.id)
                .success(function (data) {
                    if (data == false) {
                        return;
                    }

                    var area = $scope.editing.siteareas.find(function (a) {
                        return a.id == model.siteareaid;
                    })
                    area.sitefeatures.remove(model);

                    $scope.editingSiteFeature = false;
                });
        }

        $scope.confirmDeleteSiteFeature = function () {
            if (confirm('This operation cannot be undone, are you sure you want to delete this site feature ?') == false) {
                return;
            }

            $scope.deleteSiteFeature($scope.originalSiteFeature);
        };

        $scope.commitSiteArea = function (model) {
            var parameters = { record: { id: model.id, __isnew: model.__isnew, siteid: model.siteid, name: model.name } };
            $http.post($scope.rooturl + 'SiteAreaSave', parameters)
                .success(function (data) {
                    if (data == false) {
                        return;
                    }

                    angular.copy($scope.currentSiteArea, $scope.originalSiteArea);

                    if (model.__isnew) {
                        model.__isnew = $scope.originalSiteArea.__isnew = false;
                        model.id = $scope.originalSiteArea.id = data.id;
                        $scope.editing.siteareas.push($scope.originalSiteArea);
                    }

                    $scope.editingSiteArea = false;
                });
        };

        $scope.deleteSiteArea = function (model) {
            $http.delete($scope.rooturl + 'SiteArea?id=' + model.id)
                .success(function (data) {
                    if (data == false) {
                        return;
                    }

                    $scope.editing.siteareas.remove(model);

                    $scope.editingSiteArea = false;
                });
        }

        $scope.confirmDeleteSiteArea = function () {
            if (confirm('This operation cannot be undone, are you sure you want to delete this site area ?') == false) {
                return;
            }

            $scope.deleteSiteArea($scope.originalSiteArea);
        };
        return ctrl;
    }]);

managementapp.directive('sitefeatureSidebar', ['recordAjaxSidebarFactory', function (factory) {
    return factory({
        templateUrl: 'angulartemplates/admin/workloading/sitefeature_sidebar.html',
        ajaxUrl: function (page, search, itemsPerPage, includeArchived) {
            return 'Admin/SiteFeatureManagement/SiteSearch?pageNumber=' + page + '&search=' + search + '&itemsPerPage=' + itemsPerPage;
        },
    });
}])

managementapp.directive('sitefeatureEditor', ['recordEditorFactory', 'workloadingSvc', function (factory, workloadingSvc) {
    return factory({
        templateUrl: 'angulartemplates/admin/workloading/sitefeature_editor.html',
        link: function (scope, elt, attrs, ngModel) {
            scope.textFilter = '';

            scope.newArea = function () {
                var siteArea = {
                    siteid: scope.record.siteid,
                    __isnew: true,
                    sitefeatures: []
                }

                scope.$emit('sitearea.edit', siteArea);
            }

            scope.newFeature = function (featureId, siteArea) {
                var siteFeature = new workloadingSvc.sitefeature({
                    siteid: scope.record.siteid,
                    siteareaid: siteArea.id,
                    workloadingfeatureid: featureId,
                });

                scope.$emit('sitefeature.edit', siteFeature);
            }

            scope.editSiteFeature = function (siteFeature) {
                scope.$emit('sitefeature.edit', siteFeature);
            }

            scope.editArea = function (area) {
                scope.$emit('sitearea.edit', area)
            }

            scope.areaFilter = function (item) {
                var search = scope.textFilter.toLowerCase();
                return (item.name.toLowerCase().indexOf(search) != -1) || item.sitefeatures.find(function (feature) {
                    return feature.name.toLowerCase().indexOf(search) != -1;
                })
            }
        }
    });
}])

managementapp.controller("adminVersionsController", ['$scope', '$http', function ($scope, $http) {
    var url = window.razordata.siteprefix;

    $scope.versions = [];
    $scope.tenantInfo = [];

    $scope.forceAppUpdate = function () {
        $http({ url: url + 'api/v1/app/forceAppUpdate', method: 'POST' })
            .success(function () {
                $scope.getVersions();
            });;
    }

    $scope.getVersions = function () {
        $http({ url: url + 'api/v1/app/getVersions', method: 'GET' })
            .success(function (data) {
                $scope.versions = data;
            })
    }

    $scope.getTenantInfo = function () {
        $http({ url: url + 'api/v1/app/getTenantInfo', method: 'GET' })
            .success(function (data) {
                $scope.tenantInfo = data;
            })
    }

    $scope.getVersions();
    $scope.getTenantInfo();
}])

managementapp.factory('httpChunked', ['$q', '$timeout', function ($q, $timeout) {
    return function (options, postData) {
        var deferred = $q.defer();

        var last_response_len = false;
        var currentJBlob = '';

        var url = options.url || options;
        var method = options.method || 'POST';

        var fnProgress = function (data) {
            var this_response, response = data;
            if (last_response_len === false) {
                this_response = response;
                last_response_len = response.length;
            }
            else {
                this_response = response.substring(last_response_len);
                last_response_len = response.length;
            }

            currentJBlob += this_response;
            var i = currentJBlob.indexOf('\0');
            while (i >= 0) {
                var jblob = currentJBlob.substring(0, i);
                currentJBlob = currentJBlob.substring(i + 1);
                i = currentJBlob.indexOf('\0');

                $timeout(function () {
                    deferred.notify(JSON.parse(jblob));
                }, 0);
            }

        }
        $.ajax(url, {
            method: method,
            data: JSON.stringify(postData),
            contentType: "application/json; charset=utf-8",
            traditional: true,
            processData: false,
            xhrFields: {
                onprogress: function (e) {
                    fnProgress(e.currentTarget.response);
                }
            }
        })
            .done(function (data) {
                // data is the full response including all the progress jblobs, currentJBlob should contain the last jblob which is
                // our result ..
                $timeout(function () {
                    fnProgress(data);
                    deferred.resolve(JSON.parse(currentJBlob));
                }, 0);
            })
            .fail(function (error) {
                $timeout(function () {
                    deferred.reject(error);
                }, 0);
            });
        return deferred.promise;
    }
}]);

managementapp.factory('tenantSvc', ['$http', '$q', 'httpChunked', '$interval', function ($http, $q, httpChunked, $interval) {
    var url = window.razordata.siteprefix + 'Admin/MultiTenantAdmin/';

    var _isMonitoringSchema;

    var schemaUpdateStatus = { updating: "" }

    var _setup = null;                // This refreshes on each call

    var svc = {
        sourceSystem: function () {
            this.domain = '';
            this.canCreate = function () {
                return angular.isDefined(this.tenants);
            }
            this.cannotCreate = function () {
                return this.error;
            }
            this.checking = false;
            this.refresh = function () {
                var self = this;
                var d = this.domain;
                if (!d.contains('://')) {
                    d = 'https://' + this.domain;
                }
                self.checking = true;
                delete self.error;
                delete self.tenants;
                return $http.get(d + window.razordata.apiprefix + 'App/GetTenants').then(function (response) {
                    self.tenants = response.data;
                    self.checking = false;
                }, function (error) {
                    self.checking = false;
                    self.error = error.statusText || 'Could not be found';
                });
            }
            this.getTenant = function (tenant) {
                var self = this;
                var d = this.domain;
                if (!d.contains('://')) {
                    d = 'https://' + this.domain;
                }
                self.checkingTenant = true;
                delete self.tenantError;
                return $http.get(d + window.razordata.apiprefix + 'App/GetTenant?id=' + tenant.id.toString()).then(function (response) {
                    var dto = response.data;

                    self.checkingTenant = false;
                    if ((dto.tenantstatus & 6) == 0) {          // Checking only for database issues
                        delete self.tenantError;
                        return dto;
                    } else {
                        self.tenantError = dto.tenantstatustext.join('<br/>');
                        return $q.reject(dto.statustext);
                    }
                }, function (error) {
                    self.checkingTenant = false;
                    self.tenantError = error.statusText || 'Could not be found';
                    return error;
                });
            }
        },

        azureDatabase: function () {
            this.database = null;
            this.pricingtierid = null;
            this.databaseExists = null;
            this.checking = false;

            this.canCreate = function () {
                return this.database && this.databaseExists == false;
            }

            this.isValid = function () {
                return this.canCreate() && this.pricingtier && (!this.pricingtier.iselastic || this.elasticdbpool);
            }

            this.cannotCreate = function () {
                return !this.database || this.databaseExists == true;
            }
        },

        azureSubdomain: function () {
            var pattern = /^[a-z]([a-z0-9]+-?)*[a-z0-9]$/;

            this.hostname = null;
            this.hostnameAvailable = null;
            this.checking = false;

            this.isValid = function () {
                if (!this.hostname)
                    return false;

                return pattern.test(this.hostname);
            }

            this.canCreate = function () {
                return this.hostnameAvailable == true;
            }

            this.cannotCreate = function () {
                return this.hostnameAvailable == false;
            }
        },

        getSetup: function (cb) {
            if (_setup == null) {
                _setup = {};
                $http.get(url + 'GetSetup')
                    .success(function (data) {
                        angular.extend(_setup, data);
                        if (cb) {
                            cb(_setup);
                        }
                    });
            } else if (cb) {
                cb(_setup);
            }
            return _setup;
        },

        checkCurrentlyUpdatingTenantSchema: function () {
            return $http.get(url + 'CurrentlyUpdatingTenantSchema').then(function (response) { return response.data; });
        },

        getSchemaUpdateStatus: function () {
            return schemaUpdateStatus;
        },

        startSchemaUpdateMonitor: function () {
            if (angular.isDefined(_isMonitoringSchema)) return;

            var recurse = 0;
            _isMonitoringSchema = $interval(function () {
                // Only poll every 5 seconds if nto already waiting for a result (or try again after 12*5 (1 minute))
                if (recurse % 12 === 0) {
                    svc.checkCurrentlyUpdatingTenantSchema().then(function (data) {
                        schemaUpdateStatus.updating = data.name;
                        recurse = 0;
                    });
                }
                recurse++;
            }, 5000);
        },

        stopSchemaUpdateMonitor: function () {
            if (angular.isDefined(_isMonitoringSchema)) {
                $interval.cancel(_isMonitoringSchema);
                _isMonitoringSchema = undefined;
            }
        },

        checkSchemaUpdate: function (tenant) {
            return $http.post(url + 'CheckTenantDatabaseSchema', {
                tenantid: tenant.id
            })
                .then(function (response) {
                    return response.data;
                });
        },

        applySchemaUpdate: function (tenant) {
            return $http.post(url + 'UpdateTenantDatabaseSchema', {
                tenantid: tenant.id
            })
                .then(function (response) {
                    return response.data;
                });
        },

        enableTenant: function (tenant, enable) {
            return $http.post(url + 'EnableTenant', {
                tenantid: tenant.id,
                enable: enable
            })
                .then(function (response) {
                    return response.data;
                });
        },

        createTenant: function (tenant) {
            var deferred = $q.defer();
            var result = { message: 'Please wait...' };
            deferred.notify(result);

            httpChunked(url + 'CreateTenant', {
                name: tenant.name,
                tenantHostId: tenant.host.id,
                tenantProfileId: tenant.profile.id,
                tenantTargetProfileId: tenant.targetprofile.id,
                database: tenant.database.database,
                pricingtier: tenant.database.pricingtier,
                elasticDbPool: tenant.database.elasticdbpool,
                subdomain: tenant.subdomain.hostname + (tenant.targetprofile.domainappend || ''),
                timezone: tenant.timezone,
                cloneFrom: tenant.cloneFrom.database
            }).then(function (result) {
                deferred.resolve(result);
            }, function (error) {
                deferred.reject(error);
            }, function (progress) {
                deferred.notify(progress);
            });
            return deferred.promise;
        },

        saveTenant: function (tenant) {
            var deferred = $q.defer();
            var result = { message: 'Please wait...' };
            deferred.notify(result);

            httpChunked(url + 'TenantSave', tenant)
                .then(function (result) {
                    deferred.resolve(result);
                }, function (error) {
                    deferred.reject(error);
                }, function (progress) {
                    deferred.notify(progress);
                });
            return deferred.promise;
        },

        deleteTenant: function (tenant) {
            var deferred = $q.defer();
            var result = { message: 'Please wait...' };
            deferred.notify(result);

            httpChunked({ method: 'DELETE', url: url + 'TenantDelete?tenantId=' + tenant.id })
                .then(function (result) {
                    deferred.resolve(result);
                }, function (error) {
                    deferred.reject(error);
                }, function (progress) {
                    deferred.notify(progress);
                });
            return deferred.promise;
        },

        checkCanCreateSubdomain: function (subdomain, profile) {
            if (subdomain.checking)
                return;

            subdomain.hostnameAvailable = null;
            subdomain.checking = true;
            $http.post(url + 'CheckCanCreateSubdomain', { subdomain: subdomain.hostname + (profile.domainappend || '') })
                .success(function (data) {
                    subdomain.checking = false;
                    subdomain.hostnameAvailable = data.available;
                })
        },

        checkCanCreateDatabase: function (host, database) {
            if (database.checking)
                return;

            database.databaseExists = null;
            database.checking = true;
            $http.post(url + 'CheckCanCreateDatabase', { server: host.sqlservername, database: database.database })
                .success(function (data) {
                    database.checking = false;
                    database.databaseExists = data.exists;
                })
        },
    }

    svc.getSetup();

    return svc;
}])

managementapp.controller("multiTenantAdminController", ['$scope', 'recordControllerFactory', 'tenantSvc', '$timeout',
    function ($scope, factory, tenantSvc, $timeout) {
        var ctrl = factory($scope, {
            name: 'Tenant',
            idField: 'id',
            nameField: 'name',
            mvcControllerUrl: 'Admin/MultiTenantAdmin/'
        })

        $scope.setup = tenantSvc.getSetup();

        $scope.$on('new.tenant', function (event, args) {
            $scope.$broadcast('record.changed', args);
            event.stopPropagation();
        });

        $scope.$on('destroy', function () {
            tenantSvc.stopSchemaUpdateMonitor();
        });

        tenantSvc.startSchemaUpdateMonitor();

        $scope.$on('startRemoteClone', function (event, args) {
            $scope.$broadcast('remoteClone', args);
            event.stopPropagation();
        });

        return ctrl;
    }]);

managementapp.directive('multiTenantSidebar', ['recordAjaxSidebarFactory', 'tenantSvc', function (factory, tenantSvc) {
    return factory({
        templateUrl: 'angulartemplates/admin/multiTenantAdmin/multi_tenant_sidebar.html',
        ajaxUrl: function (page, search, itemsPerPage, includeArchived) {
            return 'Admin/MultiTenantAdmin/TenantSearch?pageNumber=' + page + '&search=' + search + '&itemsPerPage=' + itemsPerPage;
        },
        link: function (scope) {
            scope.setup = tenantSvc.getSetup();

            scope.updateSchemaMonitor = tenantSvc.getSchemaUpdateStatus();

            scope.remoteClone = function () {
                scope.$emit('startRemoteClone');
            }
        }
    });
}])

managementapp.directive('tenantEditor', ['recordEditorFactory', 'tenantSvc', '$rootScope', '$timeout',
    function (factory, tenantSvc, $rootScope, $timeout) {
        return factory({
            templateUrl: 'angulartemplates/admin/multiTenantAdmin/tenant_editor.html',
            link: function (scope, elt, attrs, ngModel) {
                scope.recordCssStatus = function () {
                    return scope.record.tenantstatus == 0 ? "alert-success" : "alert-danger";
                }

                scope.enableTenant = function (enable) {
                    tenantSvc.enableTenant(scope.record, enable)
                        .then(function (result) {
                            angular.copy(result, scope.record);
                        });
                }

                scope.canCheckSchemaUpdate = function () {
                    var result =
                        scope.record.profile.canupdateschema &&
                        ((scope.record.tenantstatus & 6) == 0);  // No database errors and we can check
                    return result;
                }

                scope.canClone = function () {
                    return scope.record.profile.canclone && /*scope.record.database.onazure && */ (scope.record.tenantstatus & 6) == 0;  // No database errors and we can check
                }

                scope.checkSchemaUpdate = function () {
                    scope.showCheckSchema = true;
                    scope.state = 'checking';
                    scope.canApplySchemaUpdate = false;

                    tenantSvc.checkSchemaUpdate(scope.record)
                        .then(function (result) {
                            scope.checkSchemaResult = result;
                            scope.state = 'checkResult';
                        });
                }

                scope.applySchemaUpdate = function () {
                    scope.state = 'applying';
                    scope.canApplySchemaUpdate = false;
                    tenantSvc.applySchemaUpdate(scope.record)
                        .then(function (result) {
                            scope.applySchemaResult = result;
                            if (result == 'true') {
                                scope.state = 'applyOK';
                            } else {
                                scope.state = 'applyResult';
                            }
                        });
                }

                scope.setup = tenantSvc.getSetup();

                scope.$on('remoteClone', function () {
                    scope.createTenantResult = null;

                    scope.sourceSystem = new tenantSvc.sourceSystem();

                    scope.setup = tenantSvc.getSetup(function (setup) {
                        scope.tenant = {
                            name: '',
                            subdomain: new tenantSvc.azureSubdomain(),
                            database: new tenantSvc.azureDatabase()
                        };

                        // Default to Elastic
                        scope.tenant.database.pricingtier = setup.azurepricingtiers.find(function (t) {
                            return t.iselastic;
                        });

                        if (setup.hosts.length == 1) {
                            scope.tenant.host = setup.hosts[0];
                            if (scope.tenant.host.elasticdbpools) {
                                scope.tenant.database.elasticdbpool = scope.tenant.host.elasticdbpools[0];
                            }
                        }

                        if (setup.profiles.length == 1) {
                            scope.tenant.profile = setup.profiles[0];
                            scope.tenant.targetprofile = setup.profiles[0];
                        }
                    });
                    scope.showNewTenant = true;
                });

                scope.$watch('tenant.host', function (newValue, oldValue) {
                    if (scope.tenant && scope.tenant.host && scope.tenant.host.elasticdbpools) {
                        scope.tenant.database.elasticdbpool = scope.tenant.host.elasticdbpools[0];
                    }
                })

                scope.changedCloneFrom = function () {
                    if (scope.sourceSystem) {
                        delete scope.tenant.cloneFrom;
                        scope.sourceSystem.getTenant(scope.sourceSystem.tenant).then(function (tenant) {
                            scope.tenant.pricingtierid = tenant.database.pricingtierid;
                            scope.tenant.cloneFrom = {
                                name: tenant.name,
                                database: tenant.database
                            };
                        })
                    }
                }

                scope.clone = function () {
                    scope.createTenantResult = null;
                    delete scope.sourceSystem;

                    scope.setup = tenantSvc.getSetup(function (setup) {
                        scope.tenant = {
                            name: '',
                            subdomain: new tenantSvc.azureSubdomain(),
                            database: new tenantSvc.azureDatabase(),
                            pricingtierid: scope.record.database.pricingtierid,      // Initially use the source tenants tier
                            cloneFrom: {
                                name: scope.record.name,
                                database: scope.record.database
                            }
                        };

                        // Default to Elastic
                        scope.tenant.database.pricingtier = setup.azurepricingtiers.find(function (t) {
                            return t.iselastic;
                        });


                        if (setup.hosts.length == 1) {
                            scope.tenant.host = setup.hosts[0];
                            if (scope.tenant.host.elasticdbpools) {
                                scope.tenant.database.elasticdbpool = scope.tenant.host.elasticdbpools[0];
                            }
                        }

                        if (setup.profiles.length == 1) {
                            scope.tenant.profile = setup.profiles[0];
                            scope.tenant.targetprofile = setup.profiles[0];
                        }
                    });
                    scope.showNewTenant = true;
                }

                // Make a database name from tenant name ..
                scope.makeDatabaseName = function () {
                    var name = scope.tenant.name;
                    var n = name.replace(/[^a-z0-9]*/gi, '') + scope.tenant.targetprofile.databaseappend.toLowerCase();
                    return scope.setup.mastersite.databasenameformat.replace('{0}', n);
                }

                scope.makeSubdomainName = function (name) {
                    // Remove leading none a-z
                    var n = name.replace(/^[^a-z]/, '');
                    while (n != name) {
                        name = n;
                        n = name.replace(/^[^a-z]/, '');
                    }

                    // Remove remaining none a-z, 0-9 or -
                    n = n.replace(/[^a-z0-9\-]/gi, '');

                    // Remove any -- sequences
                    n = n.replace(/(--)+-*/g, '-');

                    // Must be 63 or less
                    if (n.length > 63) {
                        n = n.substring(0, 63);
                    }

                    return n;
                }

                var checkSourceSystemDomain = function () {
                    scope.sourceSystem.refresh();
                }.debounce(1000);

                scope.$watch('sourceSystem.domain', function (newValue, oldValue) {
                    if (newValue) {
                        checkSourceSystemDomain();
                    }
                })

                var checkDatabaseName = function () {
                    if (scope.tenant) {
                        tenantSvc.checkCanCreateDatabase(scope.tenant.host, scope.tenant.database);
                    }
                }.debounce(1000);

                scope.$watch('tenant.database.database', function (newValue, oldValue) {
                    if (newValue) {
                        checkDatabaseName();
                    }
                })

                var checkSubdomainName = function () {
                    if (scope.tenant && scope.tenant.subdomain.isValid()) {
                        tenantSvc.checkCanCreateSubdomain(scope.tenant.subdomain, scope.tenant.targetprofile);
                    }
                }.debounce(1000);

                scope.$watch('tenant.subdomain.hostname', function (newValue, oldValue) {
                    if (newValue) {
                        scope.tenant.subdomain.hostname = scope.makeSubdomainName(scope.tenant.subdomain.hostname);
                        checkSubdomainName();
                    }
                })

                var makeDatabaseName = function () {
                    if (scope.tenant && scope.tenant.name && scope.tenant.database && scope.tenant.targetprofile) {
                        scope.tenant.database.database = scope.makeDatabaseName();
                    } else if (scope.tenant && scope.tenant.database) {
                        scope.tenant.database.database = '';
                    }
                }

                scope.$watch('tenant.name', makeDatabaseName);
                scope.$watch('tenant.targetprofile', makeDatabaseName);
                scope.$watch('tenant.targetprofile', checkSubdomainName);

                scope.createTenant = function () {
                    if (scope.tenant.targetprofile.createalertmessage) {
                        if (!confirm(scope.tenant.targetprofile.createalertmessage)) {
                            return;
                        }
                    }

                    scope.createTenantResult = {};      // This will show the result panel immediately (user feedback)
                    tenantSvc.createTenant(scope.tenant)
                        .then(function (result) {
                            scope.$emit('new.tenant', result);
                        }, function (error) {
                        }, function (progress) {
                            scope.createTenantResult = progress;
                        })
                }

                scope.tenantCanCreate = function () {
                    return !scope.createTenantResult
                        && scope.tenant && scope.tenant.host && scope.tenant.targetprofile && scope.tenant.profile
                        && scope.tenant.database.isValid() && scope.tenant.database.canCreate()
                        && scope.tenant.subdomain.isValid() && scope.tenant.subdomain.canCreate();
                }

                scope.save = function () {
                    scope.showSaveProgress = true;
                    tenantSvc.saveTenant(scope.record)
                        .then(function (data) {
                            scope.showSaveProgress = false;
                            angular.copy(data, scope.record);
                            scope.$broadcast('record.changed', scope.record);
                        }, function (error) {
                        }, function (progress) {
                            scope.saveProgress = progress;
                        });
                }


                scope.deleteTenant = function () {

                    if (confirm("Are you sure you want to delete tenant " + scope.record.name + "?")) {
                        scope.showDeleteProgress = true;
                        scope.deleteProgress = null;

                        tenantSvc.deleteTenant(scope.record)
                            .then(function (result) {
                                if (result) {
                                    scope.showDeleteProgress = false;
                                    // Chance for Modalify to close dialog
                                    $timeout(function () {
                                        $rootScope.$broadcast('record.deleted', scope.record.id);
                                    });
                                }
                            }, function (error) {
                            }, function (progress) {
                                scope.deleteProgress = progress;
                            })
                    }
                }
            }
        })
    }
]);

managementapp.directive('continuousProgress', [function () {
    return {
        templateUrl: window.razordata.siteprefix + 'angulartemplates/continuous-progress.html',
        restrict: 'E',
        require: 'ngModel',
        scope: {
            model: '=ngModel'
        },
        link: function (scope, element, attr, ctrl) {
        }
    }
}]);

managementapp.controller('userbillingtypemanagementCtrl', ['$scope', 'recordControllerFactory', '$http',
    function ($scope, factory, $http) {
        var ctrl = factory($scope, {
            name: 'UserBillingType',
            displayName: 'User Billing Type',
            idField: 'id',
            nameField: 'name',
            mvcControllerUrl: 'Admin/UserBillingTypeManagement/'
        })
        return ctrl;
    }
]);

managementapp.directive('userbillingtypeSidebar', ['recordAjaxSidebarFactory', function (factory) {
    return factory({
        templateUrl: 'angulartemplates/admin/userbillingtype/userbillingtype_sidebar.html',
        ajaxUrl: function (page, search, itemsPerPage, includeArchived) {
            return 'Admin/UserBillingTypeManagement/UserBillingTypeSearch?pageNumber=' + page + '&search=' + search + '&itemsPerPage=' + itemsPerPage;
        },
    });
}])

managementapp.directive('userbillingtypeEditor', ['recordEditorFactory', function (factory) {
    return factory({
        templateUrl: 'angulartemplates/admin/userbillingtype/userbillingtype_editor.html',
        link: function (scope, elt, attrs, ngModel) {
            scope.textFilter = '';
        }
    });
}])


managementapp.factory('InvoiceDate', [function () {
    return function InvoiceDate(date) {
        if (date instanceof Date) {
            this.year = date.getFullYear(),
                this.month = date.getMonth() + 1,
                this.day = date.getDate()
        } else {
            this.year = date.year;
            this.month = date.month;
            this.day = date.day;
        }
        this.getDate = function () {
            return new Date(this.year, this.month - 1, this.day, 0, 0, 0, 0);
        }
    }
}]);

managementapp.factory('invoiceSvc', ['$http', 'InvoiceDate', function ($http, InvoiceDate) {
    var url = window.razordata.siteprefix + 'Admin/InvoiceAdmin/';
    return {
        generateInvoice: function (description, fromdate, todate) {
            return $http.post(url + 'GenerateInvoice', {
                description: description,
                fromDate: new InvoiceDate(fromdate),
                toDate: new InvoiceDate(todate)
            })
                .then(function (response) {
                    return response.data;
                });
        },

        getNextInvoiceParameters: function () {
            return $http.get(url + 'GetNextInvoiceParameters')
                .then(function (response) {
                    return response.data;
                });
        },

        deleteInvoice: function (invoice) {
            return $http.delete(url + 'DeleteInvoice?id=' + invoice.id);
        },

        checkInvoiceDates: function (fromdate, todate) {
            return $http.post(url + 'CheckInvoiceDates', {
                fromDate: new InvoiceDate(fromdate),
                toDate: new InvoiceDate(todate)
            })
                .then(function (response) {
                    return response.data;
                });
        },

        markAsPaid: function (invoice, paymentreference, paymentdate) {
            return $http.post(url + 'MarkAsPaid', { id: invoice.id, paymentreference: paymentreference, paymentdate: paymentdate })
                .then(function (response) {
                    return response.data;
                });
        },

        markAsUnpaid: function (invoice) {
            return $http.post(url + 'MarkAsUnpaid', { id: invoice.id })
                .then(function (response) {
                    return response.data;
                });
        }
    }
}])

managementapp.controller('invoiceadminCtrl', ['$scope', 'recordControllerFactory', '$http', 'invoiceSvc', 'InvoiceDate',
    function ($scope, factory, $http, invoiceSvc, InvoiceDate) {
        var ctrl = factory($scope, {
            name: 'Invoice',
            displayName: 'Invoice',
            idField: 'id',
            nameField: 'description',
            mvcControllerUrl: 'Admin/InvoiceAdmin/'
        })

        $scope.showGenerateInvoice = function () {
            $scope.showGenerateInvoiceDialog = true;
            invoiceSvc.getNextInvoiceParameters().then(function (data) {
                $scope.validInvoice = 'valid';

                data.fromdate = new InvoiceDate(data.fromdate).getDate();
                data.todate = new InvoiceDate(data.todate).getDate();

                $scope.nextInvoice = data;
            })
        }

        $scope.checkInvoiceDatesNow = function () {
            if (!$scope.nextInvoice) {
                return;
            }

            if (new Date($scope.nextInvoice.fromdate) > new Date($scope.nextInvoice.todate)) {
                $scope.invalidInvoiceReason = 'Invalid date range, From must come before To';
                return;
            }

            $scope.invalidInvoiceReason = null;

            $scope.validInvoice = 'checking';
            invoiceSvc.checkInvoiceDates($scope.nextInvoice.fromdate, $scope.nextInvoice.todate)
                .then(function (response) {
                    $scope.validInvoice = !response.fromclash && !response.toclash ? 'valid' : 'invalid';
                    $scope.checkDates = response;
                });
        }
        $scope.checkInvoiceDates = $scope.checkInvoiceDatesNow.debounce(1000);

        $scope.$watch('nextInvoice.fromdate', function (newValue, oldValue) {
            $scope.checkInvoiceDates();
        });

        $scope.$watch('nextInvoice.todate', function (newValue, oldValue) {
            $scope.checkInvoiceDates();
        });

        $scope.generateInvoice = function () {
            $scope.record = invoiceSvc.generateInvoice('test', $scope.nextInvoice.fromdate, $scope.nextInvoice.todate).then(function (data) {
                $scope.$broadcast('record.changed', data);
                $scope.showGenerateInvoiceDialog = false;
            });
        }
        return ctrl;
    }
]);

managementapp.directive('invoiceSidebar', ['recordAjaxSidebarFactory', function (factory) {
    return factory({
        templateUrl: 'angulartemplates/admin/invoiceadmin/invoice_sidebar.html',
        ajaxUrl: function (page, search, itemsPerPage, includeArchived) {
            return 'Admin/InvoiceAdmin/InvoiceSearch?pageNumber=' + page + '&search=' + search + '&itemsPerPage=' + itemsPerPage;
        },
    });
}])

managementapp.directive('invoiceEditor', ['recordEditorFactory', 'invoiceSvc', function (factory, invoiceSvc) {
    return factory({
        templateUrl: 'angulartemplates/admin/invoiceadmin/invoice_editor.html',
        link: function (scope, elt, attrs, ngModel) {
            scope.textFilter = '';

            scope.canDeleteInvoice = function (invoice) {
                return !invoice.paymentdateutc && !invoice.paymentreference && !invoice.issueddateutc;
            }

            scope.detailLevels = [{ level: 0, name: 'Overview', tab: '' }, { level: 1, name: 'Detail', tab: '&nbsp;&nbsp;' }];
            scope.detailLevel = 0;

            scope.detailFilter = function (value, index, array) {
                return value.detaillevel <= scope.detailLevel;
            }

            scope.setDetailLevel = function (level) {
                scope.detailLevel = level.level;
            }

            scope.showMarkAsPaid = function () {
                scope.showMarkAsPaidDialog = true;
                scope.paymentreference = '';
                scope.paymentdate = new Date();
            }

            scope.markAsPaid = function () {
                invoiceSvc.markAsPaid(scope.record, scope.paymentreference, scope.paymentdate).then(function () {
                    scope.showMarkAsPaidDialog = false;
                    scope.$emit('record.changed', scope.record);
                })
            }

            scope.showMarkAsUnpaid = function () {
                if (confirm("Are you sure you want to mark this invoice as UNPAID?")) {
                    invoiceSvc.markAsUnpaid(scope.record).then(function () {
                        scope.$emit('record.changed', scope.record);
                    })
                }
            }
        }
    });
}])

managementapp.factory('tabletSvc', ['$http', '$q', function ($http, $q) {
    var url = window.razordata.siteprefix + 'Admin/TabletAdmin/';

    var _setup = null;                // This refreshes on each call
    var _unassignedUuids = [];

    var svc = {
        getSetup: function (cb) {
            if (_setup == null) {
                _setup = {};
                $http.get(url + 'GetSetup')
                    .success(function (data) {
                        angular.extend(_setup, data);
                        if (cb) {
                            cb(_setup);
                        }
                    });
            } else if (cb) {
                cb(_setup);
            }
            return _setup;
        },

        findUnassignedTabletUuids: function () {
            $http.get(url + 'FindUnassignedTabletUuids')
                .success(function (data) {
                    data.forEach(function (d) {
                        d.description = JSON.parse(d.description);
                    });
                    _unassignedUuids.splice(0, _unassignedUuids.length);
                    _unassignedUuids.push.apply(_unassignedUuids, data);
                });
            return _unassignedUuids;
        },

        assignUuid: function (tablet, uuid) {
            return $http.post(url + 'AssignTabletUuid', { tabletId: tablet.id, uuid: uuid.uuid }).then(function (data) {
                return data.data;
            });
        },

        unassignUuid: function (tablet) {
            return $http.post(url + 'AssignTabletUuid', { tabletId: tablet.id, uuid: null }).then(function (data) {
                return data.data;
            });
        }
    }

    svc.getSetup();

    return svc;
}])

managementapp.controller("tabletAdminCtrl", ['$scope', 'recordControllerFactory', 'tabletSvc', '$timeout',
    function ($scope, factory, tabletSvc, $timeout) {
        var ctrl = factory($scope, {
            name: 'Tablet',
            idField: 'id',
            nameField: 'name',
            mvcControllerUrl: 'Admin/TabletAdmin/'
        })

        $scope.setup = tabletSvc.getSetup();

        return ctrl;
    }
]);

managementapp.directive('tabletSidebar', ['recordAjaxSidebarFactory', function (factory) {
    return factory({
        templateUrl: 'angulartemplates/admin/tabletadmin/tablet_sidebar.html',
        ajaxUrl: function (page, search, itemsPerPage, includeArchived) {
            return 'Admin/TabletAdmin/TabletSearch?pageNumber=' + page + '&search=' + search + '&itemsPerPage=' + itemsPerPage + '&includeArchived=' + includeArchived;
        },
    });
}])

managementapp.directive('tabletEditor', ['recordEditorFactory', 'tabletSvc', '$timeout', function (factory, tabletSvc, $timeout) {
    return factory({
        templateUrl: 'angulartemplates/admin/tabletadmin/tablet_editor.html',
        link: function (scope) {
            scope.setup = tabletSvc.getSetup();

            scope.$watch('record.tabletuuid.description', function (newValue) {
                if (!newValue) {
                    return;
                }
                $timeout(function () {
                    var jobj = JSON.parse(newValue);

                    var meta = angular.extend(JSON.parse(newValue), {
                        platform: { type: 'label', group: 'Details' },
                        version: { type: 'label', group: 'Details' },
                        cordova: { type: 'label', group: 'Details' },
                        model: { type: 'label', group: 'Details' },
                        manufacturer: { type: 'label', group: 'Details' },
                        uuid: { browsable: false },
                        available: { browsable: false }
                    });

                    $("#tabletDescription").jqPropertyGrid(jobj, meta);
                }, 0);
            })

            scope.selectUuid = function (uuid) {
                scope.selectedUuid = uuid;
            }

            scope.findUnassignedTabletUuids = function () {
                scope.unassignedUuids = tabletSvc.findUnassignedTabletUuids();
            }

            scope.assign = function (uuid) {
                tabletSvc.assignUuid(scope.record, uuid).then(function (data) {
                    angular.copy(data, scope.record);
                    scope.selectedUuid = null;
                    scope.findUnassignedTabletUuids();
                });
            }

            scope.unassign = function () {
                tabletSvc.unassignUuid(scope.record).then(function (data) {
                    angular.copy(data, scope.record);
                    scope.findUnassignedTabletUuids();
                });
            }

            scope.$on('record.edit', function (event, args) {
                scope.findUnassignedTabletUuids();
            });
        }
    });
}])

managementapp.factory('assetTypeSvc', ['$http', function ($http) {
    var url = window.razordata.siteprefix + 'Admin/AssetTypeAdmin/';

    var svc = {
        simpleIntervals: [
            { id: 0, name: 'days' },
            { id: 1, name: 'weeks' },
            { id: 2, name: 'months' }
        ],

        days: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],

        createCalendar: function (assetTypeId) {
            return {
                __isnew: true,
                assettypeid: assetTypeId,
                scheduleexpression: {
                    type: 'simple',
                    simple: {
                        every: 6,
                        interval: 2,
                    },
                    onlydays: [false, true, true, true, true, true, false]
                },
                tasks: []
            }
        },

        createCalendarTask: function () {
            return {
                __isnew: true,
                tasktypeid: null,
                hierarchyroleid: null,
                tasksiteid: null,
                tasklengthindays: null,
                tasklevel: null,
                labels: []
            }
        },

        saveCalendar: function (calendar) {
            return $http.post(url + 'AssetTypeCalendarSave', { record: svc.calendarModelToDto(calendar) }).then(function (data) {
                return svc.calendarModelFromDto(data.data);
            });
        },

        deleteCalendar: function (calendar) {
            return $http.delete(url + 'AssetTypeCalendarDelete?id=' + calendar.id);
        },

        calendarTaskModelFromDto: function (dto, model) {
            model = angular.extend(model || {}, dto);
            model.starttimelocal = dto.starttimelocal != null ? moment('1970-01-01T' + dto.starttimelocal).toDate() : null;
            return model;
        },

        calendarTaskModelToDto: function (dto, model) {
            dto = angular.extend(dto || {}, model);
            dto.starttimelocal = model.starttimelocal != null ? moment(model.starttimelocal).format('HH:mm:ss') : null;
            return dto;
        },

        calendarModelFromDto: function (dto, model) {
            model = angular.extend(model || {}, dto);
            model.scheduleexpression = JSON.parse(dto.scheduleexpression);
            angular.forEach(model.tasks, function (task) {
                svc.calendarTaskModelFromDto(task, task);
            });
            return model;
        },

        calendarModelToDto: function (model, dto) {
            dto = angular.extend(dto || {}, model);
            dto.scheduleexpression = JSON.stringify(model.scheduleexpression);
            angular.forEach(dto.tasks, function (task) {
                svc.calendarTaskModelToDto(task, task);
            });
            return dto;
        },

        assetTypeModelFromDto: function (dto, model) {
            model = angular.extend(model || {}, dto);
            angular.forEach(model.calendars, function (calendar) {
                svc.calendarModelFromDto(calendar, calendar);
            });
            return model;
        },

        assetTypeModelToDto: function (model, dto) {
            dto = angular.extend(dto || {}, model);
            angular.forEach(dto.calendars, function (dto) {
                svc.calendarModelToDto(dto, dto);
            });
            return dto;
        }
    }

    return svc;
}]);

managementapp.directive('scheduleExpression', ['assetTypeSvc', function (assetTypeSvc) {
    return {
        templateUrl: window.razordata.siteprefix + 'angulartemplates/admin/schedule-expression.html',
        restrict: 'E',
        require: 'ngModel',
        scope: {
            expression: '=ngModel',
            readonly: '='
        },
        link: function (scope, elt, attrs, ngModel) {
            scope.simpleIntervals = assetTypeSvc.simpleIntervals;
            scope.days = assetTypeSvc.days;
        }
    }
}])

managementapp.controller('assetTypeAdminCtrl', ['$scope', 'recordControllerFactory', 'assetTypeSvc', function ($scope, factory, assetTypeSvc) {
    return factory($scope, {
        name: 'AssetType',
        mvcControllerUrl: 'Admin/AssetTypeAdmin/',
        modelFromDto: assetTypeSvc.assetTypeModelFromDto,
        modelToDto: assetTypeSvc.assetTypeModelToDto
    })
}]);

managementapp.directive('assetTypeSidebar', ['recordAjaxSidebarFactory', function (factory) {
    return factory({
        templateUrl: 'angulartemplates/admin/assettypeadmin/assettype_sidebar.html',
        ajaxUrl: function (page, search, itemsPerPage, includeArchived) {
            return 'Admin/AssetTypeAdmin/Search?pageNumber=' + page + '&search=' + search + '&itemsPerPage=' + itemsPerPage + '&includeArchived=' + includeArchived;
        },
    });
}]);

managementapp.directive('assetTypeEditor', ['recordEditorFactory', 'assetTypeSvc', function (factory, assetTypeSvc) {
    return factory({
        templateUrl: 'angulartemplates/admin/assettypeadmin/assettype_editor.html',
        link: function (scope, element) {
            scope.editCalendar = function (calendar) {
                scope.originalCalendar = calendar;
                scope.editingCalendar = angular.copy(calendar);
                scope.showeditcalendar = true;
            }

            scope.saveCalendar = function () {
                assetTypeSvc.saveCalendar(scope.editingCalendar).then(function (calendar) {
                    if (angular.isDefined(scope.originalCalendar)) {
                        angular.extend(scope.originalCalendar, calendar);
                    } else {
                        // this is a new record, add it
                        scope.record.calendars.push(calendar);
                    }
                    scope.showeditcalendar = false;
                });
            }

            scope.deleteCalendar = function (calendar) {
                if (confirm("Are you sure you want to delete this calendar ?")) {
                    assetTypeSvc.deleteCalendar(calendar).then(function () {
                        scope.record.calendars.remove(calendar);
                    });
                }
            }

            scope.addCalendar = function () {
                delete scope.originalCalendar;
                scope.editingCalendar = assetTypeSvc.createCalendar(scope.record.id);
                scope.showeditcalendar = true;
            }

            scope.deleteTask = function (task) {
                if (confirm("Are you sure you want to delete this task ?")) {
                    scope.editingCalendar.tasks.remove(task);
                }
            }

            scope.addTask = function () {
                scope.editingCalendar.tasks.push(assetTypeSvc.createCalendarTask());
            }

            scope.taskTypeChanged = function (selected) {
                console.log(JSON.stringify(selected));
            }
        }
    });
}]);

managementapp.factory('assetAdminSvc', ['$http', '$q', function ($http, $q) {
    var url = window.razordata.siteprefix + 'Admin/AssetAdmin/';

    var _unassignedBeacons = [];

    var svc = {
        assetScheduleModelFromDto: function (dto, model) {
            model = angular.extend(model || {}, dto);
            model.nextrunutc = dto.nextrunutc != null ? moment.utc(dto.nextrunutc).local().toDate() : null;
            model.overridestarttimelocal = dto.overridestarttimelocal != null ? moment('1970-01-01T' + dto.overridestarttimelocal).toDate() : null;
            return model;
        },

        assetScheduleModelToDto: function (model, dto) {
            dto = angular.extend(dto || {}, model);
            dto.overridestarttimelocal = model.overridestarttimelocal != null ? moment(model.overridestarttimelocal).format('HH:mm:ss') : null;
            return dto;
        },

        assetModelFromDto: function (dto, model) {
            model = angular.extend(model || {}, dto);
            angular.forEach(model.schedules, function (model) {
                svc.assetScheduleModelFromDto(model, model);
            })
            return model;
        },

        assetModelToDto: function (model, dto) {
            dto = angular.extend(dto || {}, model);
            angular.forEach(model.schedules, function (dto) {
                svc.assetScheduleModelToDto(dto, dto);
            })
            return model;
        },

        findUnassignedBeacons: function () {
            $http.get(url + 'FindUnassignedBeacons')
                .success(function (data) {
                    _unassignedBeacons.splice(0, _unassignedBeacons.length);
                    _unassignedBeacons.push.apply(_unassignedBeacons, data);
                });
            return _unassignedBeacons;
        },

        assignBeacon: function (asset, beacon) {
            return $http.post(url + 'AssignBeacon', { assetId: asset.id, beaconId: beacon.id }).then(function (data) {
                return data.data;
            });
        },

        unassignBeacon: function (asset) {
            return $http.post(url + 'AssignBeacon', { assetId: asset.id, beaconId: null }).then(function (data) {
                return data.data;
            });
        }
    }

    return svc;
}])

managementapp.controller('assetAdminCtrl', ['$scope', 'recordControllerFactory', 'assetAdminSvc', function ($scope, factory, assetAdminSvc) {
    return factory($scope, {
        name: 'Asset',
        mvcControllerUrl: 'Admin/AssetAdmin/',
        modelFromDto: assetAdminSvc.assetModelFromDto,
        modelToDto: assetAdminSvc.assetModelToDto
    })
}]);

managementapp.directive('assetSidebar', ['recordAjaxSidebarFactory', function (factory) {
    return factory({
        templateUrl: 'angulartemplates/admin/assetadmin/asset_sidebar.html',
        ajaxUrl: function (page, search, itemsPerPage, includeArchived) {
            return 'Admin/AssetAdmin/Search?pageNumber=' + page + '&search=' + search + '&itemsPerPage=' + itemsPerPage + '&includeArchived=' + includeArchived;
        },
    });
}]);

managementapp.directive('assetEditor', ['recordEditorFactory', 'assetAdminSvc', function (factory, assetAdminSvc) {
    return factory({
        templateUrl: 'angulartemplates/admin/assetadmin/asset_editor.html',
        link: function (scope) {
            scope.today = moment().format('YYYY-MM-DD');

            scope.selectBeacon = function (beacon) {
                scope.selectedBeacon = beacon;
            };

            scope.findUnassignedBeacons = function () {
                scope.unassignedBeacons = assetAdminSvc.findUnassignedBeacons();
            };

            scope.assign = function (beacon) {
                assetAdminSvc.assignBeacon(scope.record, beacon).then(function (data) {
                    angular.copy(data, scope.record);
                    scope.selectedBeacon = null;
                    scope.findUnassignedBeacons();
                });
            };

            scope.unassign = function () {
                assetAdminSvc.unassignBeacon(scope.record).then(function (data) {
                    angular.copy(data, scope.record);
                    scope.findUnassignedBeacons();
                });
            };

            scope.doSave = function () {
                // ok, so we are going to move things around a little
                scope.record.contextanswers = [];

                var ev = {
                    type: 'next'
                };
                scope.$broadcast('populateAnswer', ev);

                angular.forEach(scope.record.contexts, function (context) {
                    scope.record.contextanswers.push({ checklist: context.ChecklistName, data: { answers: context.Answers } });
                });

                // we clear this out just to save on data going down to the server
                scope.record.contexts = [];

                scope.save();
            };

            scope.$on('record.edit', function (event, args) {
                scope.findUnassignedBeacons();
            });
        }
    });
}]);

managementapp.directive('searchAssetType', ['typeaheadDirective', function (typeaheadDirective) {
    return typeaheadDirective({
        ajaxItemName: 'name',
        displayKey: 'name',
        handleBars: '<strong>{{name}}</strong>',
        ajaxItemUrl: function (id) {
            return 'Admin/AssetTypeAdmin/AssetType/' + id;
        },
        ajaxItemSearch: function (query, scope) {
            return 'Admin/AssetTypeAdmin/Search?search=' + query;
        },
        transform: function (data) {
            return data.items;
        }

    })
}])

managementapp.directive('selectHierarchyRole', [
    '$http',
    function ($http) {
        return {
            templateUrl: window.razordata.siteprefix + 'angulartemplates/admin/select-hierarchy-role.html',
            restrict: 'E',
            require: 'ngModel',
            scope: {
                roleid: '=ngModel',
                role: '=?',
                onSelect: '&'
            },
            link: function (scope, elt, attrs, ngModel) {
                scope.rooturl = window.razordata.siteprefix + 'Organisation/Hierarchy/';

                scope.roles = [];

                $http.get(scope.rooturl + "Roles")
                    .success(function (data) {
                        scope.roles = data;
                    });

                scope.select = function (record) {
                    if (angular.isFunction(scope.onSelect)) {
                        scope.onSelect({ id: scope.roleid });
                    }
                }

                scope.$watch('roleid', function (newValue, oldValue) {
                    if (angular.isDefined(newValue) == false || newValue == null) {
                        return;
                    }

                    var role = null;
                    angular.forEach(scope.roles, function (r) {
                        if (r.id == scope.roleid) {
                            role = r;
                        }
                    });

                    scope.role = role;
                });
            }
        }
    }
]);

managementapp.directive('selectTaskType', [
    '$http',
    function ($http) {
        return {
            templateUrl: window.razordata.siteprefix + 'angulartemplates/admin/select-task-type.html',
            restrict: 'E',
            require: 'ngModel',
            scope: {
                tasktypeid: '=ngModel',
                onSelect: '&',
                mandatory: '='
            },
            link: function (scope, elt, attrs, ngModel) {
                scope.rooturl = window.razordata.siteprefix + 'Admin/ScheduleManagement/';

                scope.types = [];
                scope.selected = null;

                scope.$watch('tasktypeid', function (newValue) {
                    if (angular.isDefined(newValue)) {
                        if (scope.types.length) {
                            scope.selected = scope.types.find(function (i) { return i.id == newValue; });
                        }
                    } else {
                        scope.selected = null;
                    }
                });

                $http.get(scope.rooturl + "Types")
                    .success(function (data) {
                        scope.types = data;
                        if (angular.isDefined(scope.tasktypeid)) {
                            scope.selected = scope.types.find(function (i) { return i.id == scope.tasktypeid; });
                        }
                    });

                scope.select = function () {
                    scope.tasktypeid = scope.selected ? scope.selected.id : null;
                    scope.onSelect({ selected: scope.selected });
                }

                scope.clearselection = function () {
                    scope.selected = null;
                    scope.tasktypeid = null;
                    scope.onSelect({ selected: null });
                }
            }
        }
    }
]);


managementapp.factory('timezoneSvc', ['$http', '$q', function ($http, $q) {
    var url = window.razordata.siteprefix;
    var $timezones = null;

    return {
        getTimeZones: function () {
            if ($timezones == null) {
                $timezones = $q.defer();
                $http({ url: url + 'api/v1/app/timezones' }).then(function (response) {
                    $timezones.resolve(response.data);
                })
            }
            return $timezones.promise;
        }
    }
}])

managementapp.directive('selectTimezoneId', ['timezoneSvc', function (timezoneSvc) {
    return {
        templateUrl: window.razordata.siteprefix + 'angulartemplates/admin/select-timezone-id.html',
        restrict: 'E',
        require: 'ngModel',
        scope: {
            timezoneId: '=ngModel',
            onSelect: '&',
            allowNull: '='
        },
        link: function (scope, elt, attrs, ngModel) {
            scope.timezones = [];
            timezoneSvc.getTimeZones().then(function (timezones) {
                scope.timezones = timezones;
                if (scope.allowNull) {
                    scope.timezones.unshift({ Id: null, DisplayName: 'None' });
                }
            })

            scope.select = function () {
                scope.onSelect({ id: scope.timezoneId });
            }
        }
    }
}])

managementapp.directive('selectFacilityStructure', ['facilityStructureSvc', function (svc) {
    return {
        templateUrl: window.razordata.siteprefix + 'angulartemplates/admin/select-facility-structure.html',
        restrict: 'E',
        require: 'ngModel',
        scope: {
            structureId: '=ngModel',
            onSelect: '&'
        },
        link: function (scope, elt, attrs, ngModel) {
            scope.structureTypes = svc.types;

            scope.$watch('structureId', function (newValue, oldValue) {
                if (newValue) {
                    svc.load(newValue).then(function (data) {
                        scope.record = data;
                        scope.parents = svc.makeBreadCrumbs(data).reverse();
                    })
                } else {
                    scope.record = {};
                    scope.parents = [];
                }
            });

            scope.loadSiblings = function (parent) {
                if (parent.siblings) {
                    return;
                }

                svc.loadSiblings(parent.parent != null ? parent.parent.id : null).then(function (data) {
                    parent.siblings = data;
                });
            }

            scope.loadChildren = function (parent) {
                if (parent.children) {
                    return;
                }

                svc.loadSiblings(parent.id).then(function (data) {
                    parent.children = data;
                });
            }

            scope.select = function (record) {
                scope.structureId = record.id;

                if (angular.isFunction(scope.onSelect)) {
                    scope.onSelect({ id: record.id });
                }
            }
        }
    }
}])

managementapp.factory('facilityStructureSvc', ['$http', 'gpsLocationService', 'VmAdminClasses', function ($http, gpsLocationSvc, VmAdmin) {
    var _url = window.razordata.siteprefix + 'Admin/FacilityStructureAdmin/';

    var _types = [
        { name: 'Facility', structuretype: 0, defaults: { haschildren: true, isfacility: true } },
        { name: 'Building', structuretype: 1, defaults: { haschildren: true, isbuilding: true } },
        { name: 'Floor', structuretype: 2, defaults: function () { return { haschildren: true, level: 0, isfloor: true, hasbeacons: true, floorplan: new VmAdmin.FloorPlan({ imageurl: '', topleft: L.latLng(0, 0), topright: L.latLng(0, 0), bottomleft: L.latLng(0, 0) }) } } },
        { name: 'Zone', structuretype: 3, defaults: { haschildren: true, iszone: true, hasbeacons: true } },
        { name: 'Site', structuretype: 4, defaults: { haschildren: false, issite: true, hasbeacons: true } }
    ];

    var svc = {
        types: function () {
            return angular.copy(_types);
        },
        modelFromDto: function (dto) {
            var type = _types[dto.structuretype];
            var model = angular.extend({}, angular.isFunction(type.defaults) ? type.defaults() : type.defaults, dto);
            if (model.floorplan) {
                model.floorplan = new VmAdmin.FloorPlan(model.floorplan);
            }
            return model;
        },
        newStructureOfType: function (parent, type) {
            var newType = _types[type];
            var record = {
                __isnew: true,
                parentid: parent && parent.id,
                parent: parent,
                name: 'New ' + newType.name,
                structuretype: newType.structuretype
            };
            angular.extend(record, angular.isFunction(newType.defaults) ? newType.defaults() : newType.defaults);
            return record;
        },

        load: function (id) {
            return $http.get(_url + 'FacilityStructure?id=' + id).then(function (response) {
                return response.data;
            });
        },

        makeBreadCrumbs: function (child) {
            var array = [];
            var iterate = function (i) {
                if (i) {
                    array.push({ id: i.id, name: i.name, structuretype: i.structuretype, parent: i.parent });
                    if (i.parent) {
                        iterate(i.parent);
                    }
                }
            }
            iterate(child);
            return array;
        },

        getSiteBeacons: function (site, showUnassignedBeacons) {
            return $http.get(_url + 'GetSiteBeacons?facilityStructureId=' + site.id + '&showUnassignedBeacons=' + showUnassignedBeacons)
                .then(function (response) {
                    var beacons = [];
                    response.data.forEach(function (dto) {
                        beacons.push(new VmAdmin.Beacon(dto));
                    });
                    return beacons;
                });
        },

        assignBeacon: function (site, beacon) {
            beacon = VmAdmin.beacon(beacon);
            beacon.facilitystructureid = site.id;
            return $http.post(_url + 'SaveBeacon', { beacon: beacon })
                .then(function (response) {
                    beacon.load(response.data);
                    return beacon;
                });
        },

        unassignBeacon: function (beacon) {
            beacon = VmAdmin.beacon(beacon);
            // Unassign from facility and remove the beacons static location
            beacon.facilitystructureid = null;
            beacon.staticlocation = null;
            return $http.post(_url + 'SaveBeacon', { beacon: beacon })
                .then(function (response) {
                    beacon.load(response.data);
                    return beacon;
                });
        },

        saveBeacon: function (beacon) {
            beacon = VmAdmin.beacon(beacon);
            return $http.post(_url + 'SaveBeacon', { beacon: beacon })
                .then(function (response) {
                    beacon.load(response.data);
                    return beacon;
                });
        },

        loadSiblings: function (parentId) {
            if (parentId == null) {
                return $http.get(_url + 'Search?searchFlags=1').then(function (data) {
                    return data.data.items;
                });
            } else {
                return $http.get(_url + 'Search?parentId=' + parentId).then(function (data) {
                    return data.data.items;
                });
            }
        },

        getProducts: function () {
            return $http.get(_url + 'Products');
        },

        getHierarchies: function () {
            return $http.get(_url + 'Hierarchies');
        },

        saveCatalog: function (catalog) {
            // we are going to take a copy and strip off a few things. The save catalog isn't intended
            // to save sub objects etc, so not worth sending them down
            var c = angular.copy(catalog);

            c.items = [];

            return $http.post(_url + 'SaveCatalog', { catalog: c }).then(function (data) {
                // in case it's a new catalog set the id so the next save updates rather than inserts
                catalog.__isnew = false;
                catalog.id = data.data.id;
            });
        },

        saveCatalogInheritenceRule: function (catalog, facilitystructureid) {
            // we are going to take a copy and strip off a few things. The save catalog isn't intended
            // to save sub objects etc, so not worth sending them down
            var c = angular.copy(catalog);

            c.items = [];

            return $http.post(_url + 'SaveCatalogInheritenceRule', { catalog: c, facilitystructureid: facilitystructureid });
        },

        deleteCatalog: function (catalog) {
            return $http.delete(_url + 'DeleteCatalog?catalogId=' + catalog.id);
        },

        saveCatalogItem: function (item) {
            return $http.post(_url + 'SaveCatalogItem', { item: item });
        },

        removeCatalogItem: function (item) {
            return $http.delete(_url + 'DeleteCatalogItem?productId=' + item.productid + '&catalogid=' + item.productcatalogid);
        },

        getStandards: function () {
            return $http.get(_url + 'Standards');
        },

        getStandardsTests: function (siteid) {
            return $http.get(_url + 'StandardTests?id=' + siteid);
        },

        setStandardsTests: function (siteid, items) {
            return $http.post(_url + 'StandardTests', { items: items, id: siteid });
        }
    }

    return svc;
}])

managementapp.controller('facilityStructureAdminCtrl', ['$scope', 'recordControllerFactory', 'facilityStructureSvc', 'FileUploader', function ($scope, factory, svc, FileUploader) {

    var ctrl = factory($scope, {
        name: 'FacilityStructure',
        mvcControllerUrl: 'Admin/FacilityStructureAdmin/',
        modelFromDto: svc.modelFromDto
    });

    $scope.newRecord = function () {
        $scope.editing = svc.newStructureOfType(null, 0);
    }

    $scope.$on('newChild', function (event, args) {
        $scope.editing = svc.newStructureOfType(args, args != null ? args.structuretype + 1 : 0);

        $("a[href='#general']").trigger('click');           // Click the General tab
    })

}]);

managementapp.directive('facilityStructureSidebar', ['recordAjaxSidebarFactory', 'facilityStructureSvc', 'FileUploader', function (factory, svc, FileUploader) {
    return factory({
        templateUrl: 'angulartemplates/admin/facilitystructureadmin/facilitystructure_sidebar.html',
        ajaxUrl: function (page, search, itemsPerPage, includeArchived, scope) {
            var url = 'Admin/FacilityStructureAdmin/Search?pageNumber=' + page + '&search=' + search + '&itemsPerPage=' + itemsPerPage + '&includeArchived=' + includeArchived;

            // If a Parent is specified then SearchFlags (Structure Types) are fixed to the parents child type, so ignore the flags if Parent is specified
            if (!angular.isDefined(scope.parent)) {
                url += '&searchFlags=' + scope.searchFlags;
            } else {
                url += '&parentId=' + scope.parent.id;
            }
            return url;
        },
        scope: {
            parent: '='
        },
        link: function (scope) {
            scope.structureTypes = svc.types();
            scope.searchFlags = 0;

            scope.$watch('parent', function (newValue, oldValue) {
                scope.refresh();
                scope.newStructureType = scope.parent != null ? (scope.parent.haschildren ? scope.parent.structuretype + 1 : null) : 0;
            });

            scope.toggleStructureType = function (st) {
                st.include = !st.include;
                if (st.include) {
                    scope.searchFlags |= 1 << st.structuretype;
                } else {
                    scope.searchFlags &= ~(1 << st.structuretype);
                }
                scope.refresh();
            }

            scope.newChild = function () {
                scope.$emit('newChild', scope.parent);
            }

            // Turn them all on first
            scope.structureTypes.forEach(scope.toggleStructureType);

            scope.showImportDialog = false;

            scope.uploader = new FileUploader();
            scope.uploader.url = window.razordata.siteprefix + 'Admin/FacilityStructureAdmin/ImportProductCatalogs';
            scope.uploader.alias = 'file';

            scope.uploader.onSuccessItem = function (fileItem, response, status, headers) {
                scope.importresults = response;

                scope.importfailed = response.errors.length > 0;
            };

            scope.getExportUrl = function () {
                return window.razordata.siteprefix + 'Admin/FacilityStructureAdmin/ExportProductCatalogs';
            };

            scope.showImport = function () {
                scope.uploader.clearQueue();
                scope.showImportDialog = true;
            };

            scope.closeImport = function () {
                scope.showImportDialog = false;
            };

            scope.uploadAll = function () {
                scope.uploader.uploadAll();
            };
        }
    });
}]);

managementapp.factory('gpsLocationService', ['$http', 'VmAdminClasses', function ($http, VmAdmin) {
    var _url = window.razordata.siteprefix + 'Api/v1/GpsLocation/';

    var svc = {
        getLocation: function (location) {
            location = VmAdmin.gpsLocation(location);
            return $http.get(_url + 'Get?id=' + location.id)
                .then(function (response) {
                    location.load(response.data);
                    return location;
                });
        },
        saveLocation: function (location) {
            location = VmAdmin.gpsLocation(location);
            return $http.post(_url + 'Save', location)
                .then(function (response) {
                    location.load(response.data);
                    return location;
                })
        }
    }
    return svc;
}]);


managementapp.directive('facilityStructureEditor', [
    '$timeout',
    'map-tile-layer',
    'recordEditorFactory',
    'facilityStructureSvc',
    'L-destroyer',
    'VmAdminClasses',
    'gpsLocationService',
    'directiveApi',
    'bworkflowAdminApi',
    'FileUploader',
    function ($timeout, mapTileLayer, factory, svc, Ldestroyer, VmAdmin, gpsLocationSvc, directiveApi, bworkflowAdminApi, FileUploader) {
        return factory({
            templateUrl: 'angulartemplates/admin/facilitystructureadmin/facilitystructure_editor.html',
            link: function (scope) {
                var Ldestroy = Ldestroyer(scope);

                scope.structureTypes = svc.types();

                scope.hierarchies = [];
                svc.getHierarchies().then(function (data) {
                    scope.hierarchies = data.data;
                    scope.hierarchies.unshift({ id: null, name: '' });
                });

                scope.editParent = function (parent) {
                    scope.$emit('record.edit', parent);
                }

                scope.loadSiblings = function (sibling) {
                    if (sibling.siblings) {
                        return;
                    }

                    svc.loadSiblings(sibling.parent != null ? sibling.parent.id : null).then(function (siblings) {
                        sibling.siblings = siblings;
                    });
                }

                scope.loadRecordId = function (id) {
                    scope.$emit('record.edit', { id: id });
                }

                var _destroyLs = [];
                scope.$watch('record', function (newValue, oldValue) {
                    // New record, destroy all L objects
                    Ldestroy([_destroyLs]);
                    if (angular.isDefined(newValue)) {
                        scope.selectedId = newValue.id;

                        scope.showUnassignedBeacons = false;
                        if (newValue) {
                            // These objects are used to communicate with the leaflet-floorplan directive
                            scope.floorplanApi = directiveApi();
                            scope.editFloorplanApi = directiveApi();

                            if (newValue.hasbeacons) {
                                scope.getBeacons();
                            }

                            // Find the Inherited Floor Plan if available
                            scope.inheritedfloorplan = null;
                            var record = newValue;
                            while (record != null) {
                                if (record.floorplan) {
                                    scope.inheritedfloorplan = VmAdmin.floorPlan(record.floorplan);
                                    break;
                                }
                                record = record.parent;
                            }
                        }
                    }
                })

                scope.beacons = [];
                scope.getBeacons = function () {
                    svc.getSiteBeacons(scope.record, scope.showUnassignedBeacons).then(function (beacons) {
                        // Remove all beacon markers from map
                        scope.beacons.forEach(function (b) {
                            b.transient.marker = b.transient.marker && b.transient.marker.remove() && null;
                        });

                        scope.beacons = beacons;

                        beacons.forEach(function (b) {
                            if (b.facilitystructureid) {
                                _toggleShowBeacon(b);
                            }
                        });

                        _zoomToShownBeacons();
                    })
                }

                scope.toggleBeaconEdit = function () {
                    scope.showUnassignedBeacons = !scope.showUnassignedBeacons;

                    scope.getBeacons();
                }

                scope.assignUnassign = function (beacon) {
                    if (!beacon.facilitystructureid) {
                        // When Assigning a new beacon we take the center point of the Floorplan as its default location
                        beacon.staticlocation = VmAdmin.gpsLocation({
                            locationtype: VmAdmin.GpsLocation.type.StaticBeacon,
                            pos: scope.inheritedfloorplan.getCenter()
                        });

                        svc.assignBeacon(scope.record, beacon);
                    } else {
                        svc.unassignBeacon(beacon);
                    }
                }

                scope.updateBeaconPopup = function (beacon) {
                    if (!beacon.transient.marker) {
                        return;
                    }
                    beacon.transient.marker.setPopupContent(beacon.nameAndId());
                }

                scope.popBeacon = function (beacon) {
                    if (!beacon.transient.show) {
                        _toggleShowBeacon(beacon);
                    }

                    scope.floorplanApi.getMap().then(function (map) {
                        // Remove and Add back to make it appear ontop of other markers
                        beacon.transient.marker.remove().addTo(map);
                        beacon.transient.marker.openPopup();

                        map.flyTo(beacon.transient.marker.getLatLng());
                    });
                }

                var _toggleShowBeacon = function (beacon) {
                    scope.floorplanApi.getMap().then(function (map) {
                        beacon.transient.show = !beacon.transient.show;

                        if (!beacon.staticlocation) {
                            var centre = map.getCenter().clone();

                            beacon.staticlocation = new VmAdmin.GpsLocation({
                                locationtype: VmAdmin.GpsLocation.type.StaticBeacon,
                                pos: map.getCenter().clone()
                            });
                        }

                        if (beacon.transient.show) {
                            if (!beacon.transient.marker) {
                                _destroyLs.push(
                                    beacon.transient.marker = L.marker(beacon.staticlocation.pos, { draggable: true })
                                        .addTo(map)
                                        .bindPopup(beacon.nameAndId())
                                );

                                beacon.transient.marker.on('drag', function (ev) {
                                    $timeout(function () {
                                        var latLng = ev.target.getLatLng();
                                        beacon.staticlocation.pos.lat = latLng.lat;
                                        beacon.staticlocation.pos.lng = latLng.lng;
                                    });
                                });

                                // Ondragend we save the new position ..
                                beacon.transient.marker.on('dragend', function (ev) {
                                    $timeout(function () {
                                        var latLng = ev.target.getLatLng();
                                        beacon.staticlocation.pos.lat = latLng.lat;
                                        beacon.staticlocation.pos.lng = latLng.lng;

                                        svc.saveBeacon(beacon);
                                    });
                                });
                            } else {
                                beacon.transient.marker.addTo(map);
                                beacon.transient.marker.setLatLng(beacon.staticlocation.pos);
                            }
                            beacon.transient.marker.openPopup();
                        } else {
                            beacon.transient.marker.remove();
                        }
                    });
                }

                var _zoomToShownBeacons = function () {
                    scope.floorplanApi.getMap().then(function (map) {
                        // Zoom to all shown beacons
                        var shown = scope.beacons.filter(function (b) {
                            return b.transient.show;
                        });
                        if (shown.length) {
                            var bounds = L.latLngBounds(shown[0].staticlocation.pos);
                            shown.forEach(function (b) {
                                bounds.extend(b.staticlocation.pos);
                            });

                            map.flyToBounds(bounds, { padding: [100, 100] });
                        } else {
                            map.flyToBounds(scope.inheritedfloorplan.getLatLngBounds());
                        }
                    });
                }

                scope.toggleShowBeacon = function (beacon) {
                    _toggleShowBeacon(beacon);
                    _zoomToShownBeacons();
                }

                scope.undoBeaconMove = function (beacon) {
                    beacon.undoMove();
                    gpsLocationSvc.saveLocation(beacon.staticlocation);
                }

                scope.saveBeacon = function (beacon) {
                    if (JSON.stringify(beacon) != JSON.stringify(beacon.original)) {
                        svc.saveBeacon(beacon);
                    }
                }

                var makeMap = function () {
                    scope.editFloorplanApi.getMap().then(function (map) {

                        var fp = scope.record.floorplan;
                        Ldestroy(['topleftMarker', 'toprightMarker', 'bottomleftMarker']);
                        if (fp && fp.imageurl && fp.topleft && fp.topright && fp.bottomleft) {
                            var topleft = fp.topleft;
                            var topright = fp.topright;
                            var bottomleft = fp.bottomleft;

                            var markerMoved = function (ptName) {
                                return function (ev) {
                                    $timeout(function () {
                                        var pos = ev.target.getLatLng();
                                        fp[ptName].lat = pos.lat;
                                        fp[ptName].lng = pos.lng;
                                    });
                                };
                            }

                            scope.topleftMarker = L.marker(fp.topleft, { draggable: true }).addTo(map).on('drag dragend', markerMoved('topleft'));
                            scope.toprightMarker = L.marker(fp.topright, { draggable: true }).addTo(map).on('drag dragend', markerMoved('topright'));
                            scope.bottomleftMarker = L.marker(fp.bottomleft, { draggable: true }).addTo(map).on('drag dragend', markerMoved('bottomleft'));
                        }
                    });
                }

                $("body").on("shown.bs.tab", "#beaconlink", function () {
                    scope.showUnassignedBeacons = false;
                    scope.getBeacons();
                });

                scope.$watch('record.floorplan', function (newValue, oldValue) {
                    if (!newValue) {
                        Ldestroy(['topleftMarker', 'toprightMarker', 'bottomleftMarker']);
                        return;
                    }
                    makeMap();
                });

                scope.uploadFile = function () {
                    scope.results = null;
                    scope.uploadFailed = null;

                    var uploader = scope.uploader = new FileUploader({
                        scope: scope,
                        url: "FacilityStructureAdmin/MediaUpload"
                    });
                    scope.uploadargs = { id: scope.record.id };

                    uploader.onBeforeUploadItem = function (item) {
                        Array.prototype.push.apply(item.formData, [scope.uploadargs]);
                    };

                    uploader.onSuccessItem = function (fileItem, response, status, headers) {
                        scope.record.mediaid = response[0].data;
                        scope.nonce = Date.now() / 1000 | 0;
                    };

                    scope.uploadingfile = true;
                };

                scope.imageUrl = bworkflowAdminApi.getImageUrl();
                scope.nonce = Date.now() / 1000 | 0;

                scope.uploadingfile = false;
            }
        });
    }]);

managementapp.directive('selectFacilitySite', ['typeaheadDirective', function (typeaheadDirective) {
    return typeaheadDirective({
        templateUrl: 'angulartemplates/admin/select_single_item.html',
        ajaxItemName: 'Name',
        ajaxItemId: 'id',
        displayKey: 'name',
        handleBars: '<strong>{{name}}</strong>',
        ajaxItemUrl: function (id) {
            return 'SearchUser/Details?id=' + id;
        },
        ajaxItemSearch: function (query) {
            return 'Admin/FacilityStructureAdmin/SearchSites?query=' + query;
        }
    })
}]);

managementapp.directive('selectFacility', ['typeaheadDirective', function (typeaheadDirective) {
    return typeaheadDirective({
        templateUrl: 'angulartemplates/admin/select_single_item.html',
        ajaxItemName: 'Name',
        ajaxItemId: 'id',
        displayKey: 'name',
        handleBars: '<div><span><i class="icon-facility-{{ structuretype }}"></i>&nbsp;{{ name }}</div><div class="muted small">{{#each breadcrumbs}}<span><i class="icon-facility-{{ structuretype }}"></i>&nbsp;{{ name }}&nbsp;{{/each}}</div>',
        selectObjectNotId: true,
        transform: function (response) {
            response.items.forEach(function (i) {
                if (i.breadcrumbs) {
                    i.breadcrumbs = i.breadcrumbs.reverse();
                }
            });
            return response.items;
        },
        ajaxItemUrl: function (id) {
            return 'SearchUser/Details?id=' + id;
        },
        ajaxItemSearch: function (query) {
            return 'Admin/FacilityStructureAdmin/Search?itemsPerPage=20&search=' + query;
        }
    })
}]);

managementapp.directive('facilityStructureProductCatalogs', ['facilityStructureSvc',
    function (svc) {
        return {
            templateUrl: window.razordata.siteprefix + 'angulartemplates/admin/facilitystructureadmin/facilitystructure_productcatalogs.html',
            restrict: 'E',
            require: 'ngModel',
            scope: {
                structure: '=ngModel'
            },
            link: function (scope, elt, attrs, ngModel) {
                scope.catalogName = '';
                scope.minStockLevel = '';

                scope.products = null;
                scope.showDuplicateMessage = false;

                scope.rules = [{ type: 'Inherited' }, { type: 'Excluded' }, { type: 'Included' }];

                svc.getProducts().then(function (data) {
                    scope.products = data.data;
                });
                scope.product = {};

                scope.$watch('structure', function (newValue, oldValue) {
                    scope.catalogs = [];

                    if (angular.isDefined(newValue)) {
                        scope.catalogs = newValue.catalogs;
                    }
                });

                scope.newCatalog = function () {
                    var n = {
                        __isnew: true,
                        inherited: false,
                        inheritedrule: 'Inherited',
                        facilitystructureid: scope.structure.id,
                        items: [],
                        name: 'New Catalog'
                    };

                    svc.saveCatalog(n);

                    scope.catalogs.push(n);
                };

                scope.newItem = function (product, catalog) {
                    var ok = true;
                    angular.forEach(catalog.items, function (item) {
                        if (item.productid == product) {
                            ok = false;
                        }
                    });

                    scope.showDuplicateMessage = false;
                    if (ok == false) {
                        scope.showDuplicateMessage = true;
                        return;
                    }

                    var p = {
                        __isnew: true,
                        productid: product,
                        productcatalogid: catalog.id
                    };

                    svc.saveCatalogItem(p).then(function (data) {
                        catalog.items.push(data.data);

                        catalog.__showaddproduct = false;
                    });
                };

                scope.showAddProduct = function (catalog) {
                    showDuplicateMessage = false;
                    catalog.__showaddproduct = true;
                };

                scope.hideAddProduct = function (catalog) {
                    showDuplicateMessage = false;
                    catalog.__showaddproduct = false;
                };

                scope.removeCatalogItem = function (item, catalog) {
                    if (confirm('Are you sure you want to delete this item?') == false) {
                        return;
                    }

                    svc.removeCatalogItem(item).then(function () {
                        var index = $.inArray(item, catalog.items);

                        if (index == -1) {
                            return;
                        }

                        catalog.items.splice(index, 1);
                    });
                };

                scope.nameFocus = function (catalog) {
                    scope.catalogName = catalog.name;
                };

                scope.descriptionFocus = function (catalog) {
                    scope.catalogDescription = catalog.description;
                };

                scope.descriptionLostFocus = function (catalog) {
                    if (scope.catalogDescription != catalog.description) {
                        svc.saveCatalog(catalog);
                    }
                };

                scope.setOverriden = function (catalogName, isOverriden) {
                    var toRemove = null;
                    angular.forEach(scope.catalogs, function (c) {
                        if (c.name == catalogName && c.inherited == true) {
                            toRemove = c;
                        }
                    });

                    if (toRemove != null) {
                        toRemove.overriden = isOverriden;
                    }
                };

                scope.nameLostFocus = function (catalog) {
                    if (scope.catalogName != catalog.name) {
                        scope.setOverriden(scope.catalogName, false);

                        svc.saveCatalog(catalog);

                        scope.setOverriden(catalog.name, true);
                    }
                };

                scope.deleteCatalog = function (catalog) {
                    if (confirm("Are you sure you want to delete Product Catalog '" + catalog.name + "' ?")) {
                        svc.deleteCatalog(catalog).then(function () {
                            scope.setOverriden(catalog.name, false);

                            var index = $.inArray(catalog, scope.catalogs);

                            if (index == -1) {
                                return;
                            }

                            scope.catalogs.splice(index, 1);
                        });
                    }
                };

                scope.changed = function (catalog) {
                    svc.saveCatalog(catalog);
                };

                scope.changedRule = function (catalog) {
                    svc.saveCatalogInheritenceRule(catalog, scope.structure.id);
                };

                scope.itemFocus = function (item, property) {
                    scope[property] = item[property];
                };

                scope.itemLostFocus = function (item, property) {
                    if (scope[property] != item[property]) {
                        svc.saveCatalogItem(item);
                    }
                };
            }
        };
    }
]);

managementapp.directive('facilityStructureStandardsTests', ['facilityStructureSvc',
    function (svc) {
        return {
            templateUrl: window.razordata.siteprefix + 'angulartemplates/admin/facilitystructureadmin/facilitystructure_standardstests.html',
            restrict: 'E',
            require: 'ngModel',
            scope: {
                structure: '=ngModel'
            },
            link: function (scope, elt, attrs, ngModel) {
                scope.tests = {};
                scope.editing = null;
                scope.showedittest = false;
                scope.showmanageorder = false;
                scope.standards = null;

                svc.getStandards().then(function (response) {
                    scope.standards = response.data;
                });

                scope.$watch('structure', function (newValue, oldValue) {
                    if (newValue == null) {
                        return;
                    }

                    svc.getStandardsTests(newValue.siteid).then(function (response) {
                        scope.tests = response.data;
                    });
                });

                scope.$watch('showedittest', function (newValue, oldValue) {
                    if (newValue == null || scope.editing == null || newValue == true) {
                        return;
                    }

                    scope.editing.name = [];
                    angular.forEach(scope.editing.equipment, function (val) {
                        scope.editing.name.push(val.name);
                    });

                    scope.editing.equipment = undefined;

                    svc.setStandardsTests(scope.structure.siteid, scope.tests.items);
                });

                scope.$watch('showmanageorder', function (newValue, oldValue) {
                    if (newValue == null || newValue == true) {
                        return;
                    }

                    svc.setStandardsTests(scope.structure.siteid, scope.tests.items);
                });

                scope.editTest = function (test) {
                    scope.editing = test;

                    scope.editing.equipment = [];
                    angular.forEach(test.name, function (val) {
                        scope.editing.equipment.push({ name: val });
                    });

                    scope.showedittest = true;
                };

                scope.addTest = function () {
                    scope.editing = {};

                    scope.tests.items.push(scope.editing);

                    scope.editTest(scope.editing);
                };

                scope.removeTest = function (test) {
                    var index = scope.tests.items.indexOf(test);

                    scope.tests.items.splice(index, 1);
                };

                scope.manageTests = function () {
                    scope.showmanageorder = true;
                };

                scope.moveTo = function (arr, oldIndex, newIndex) {
                    arr.splice(newIndex, 0, arr.splice(oldIndex, 1)[0]);
                };

                scope.up = function (arr, item) {
                    var index = arr.indexOf(item);

                    if (index == -1) {
                        return;
                    }

                    if (index == 0) {
                        return;
                    }

                    scope.moveTo(arr, index - 1, index);
                };

                scope.down = function (arr, item) {
                    var index = arr.indexOf(item);

                    if (index == -1) {
                        return;
                    }

                    if (index == arr.length - 1) {
                        return;
                    }

                    scope.moveTo(arr, index + 1, index);
                };

                scope.addEquipment = function (test) {
                    test.equipment.push({ name: 'New Equipment ' + test.equipment.length });
                };

                scope.removeEquipment = function (test, equipment) {
                    var index = test.equipment.indexOf(equipment);

                    test.equipment.splice(index, 1);
                };
            }
        };
    }
]);

managementapp.factory('productManagementSvc', ['$http', '$q', '$timeout', function ($http, $q, $timeout) {
    var svc = {
        rooturl: window.razordata.siteprefix + 'Admin/ProductManagement/',
        types: null,
        priceJsFunctions: null,
        products: null,
        export: function () {
            $http.get(url + 'Export');
        },
        getTypes: function () {
            var d = $q.defer();

            if (svc.types == null) {
                $http.get(svc.rooturl + "Types")
                    .success(function (data) {
                        svc.types = data.types;
                        d.resolve(svc.types);
                    });
            }
            else {
                $timeout(function () {
                    d.resolve(svc.types);
                });
            }

            return d.promise;
        },

        getPriceJsFunctions: function () {
            var d = $q.defer();

            if (svc.priceJsFunctions == null) {
                $http.get(svc.rooturl + "PriceJsFunctions")
                    .success(function (data) {
                        svc.priceJsFunctions = data;
                        d.resolve(svc.priceJsFunctions);
                    });
            }
            else {
                $timeout(function () {
                    d.resolve(svc.priceJsFunctions);
                });
            }

            return d.promise;
        },

        getProducts: function () {
            var d = $q.defer();

            if (svc.products == null) {
                $http.get(svc.rooturl + "Products")
                    .success(function (data) {
                        svc.products = data;
                        d.resolve(svc.products);
                    });
            }
            else {
                $timeout(function () {
                    d.resolve(svc.products);
                });
            }

            return d.promise;
        },
    }

    return svc;
}])

managementapp.controller('productManagementCtrl', ['$scope', 'recordControllerFactory', 'productManagementSvc', 'FileUploader', function ($scope, factory, productManagementSvc, FileUploader) {
    var ctrl = factory($scope, {
        name: 'Product',
        mvcControllerUrl: 'Admin/ProductManagement/',
        modelFromDto: function (dto, model) {
            model = angular.extend(model || {}, dto);
            model.extra = dto.extra ? JSON.parse(dto.extra) : {};
            model.validfrom = model.validfrom ? moment(model.validfrom).toDate() : null;
            model.validto = model.validto ? moment(model.validto).toDate() : null;
            return model;
        },
        modelToDto: function (model, dto) {
            dto = angular.extend(dto || {}, model);
            dto.extra = model.extra ? JSON.stringify(model.extra) : '';
            dto.validfrom = dto.validfrom ? moment(dto.validfrom).startOf('day').toDate() : null;
            dto.validto = dto.validto ? moment(dto.validto).startOf('day').toDate() : null;
            return dto;
        }
    })

    $scope.showImportDialog = false;

    $scope.uploader = new FileUploader();
    $scope.uploader.url = window.razordata.siteprefix + 'Admin/ProductManagement/Import';
    $scope.uploader.alias = 'file';

    $scope.uploader.onSuccessItem = function (fileItem, response, status, headers) {
        $scope.importresults = response;

        $scope.importfailed = response.errors.length > 0;
    };

    $scope.getExportUrl = function () {
        return window.razordata.siteprefix + 'Admin/ProductManagement/export';
    };

    $scope.showImportProduct = function () {
        $scope.uploader.clearQueue();
        $scope.showImportDialog = true;
    };

    $scope.closeImportProduct = function () {
        $scope.showImportDialog = false;
    };

    $scope.uploadSchedule = function () {
        $scope.uploader.uploadAll();
    };

    return ctrl;
}]);

managementapp.directive('productSidebar', ['recordAjaxSidebarFactory', function (factory) {
    return factory({
        templateUrl: 'angulartemplates/admin/product/product_sidebar.html',
        ajaxUrl: function (page, search, itemsPerPage, includeArchived) {
            return 'Admin/ProductManagement/Search?pageNumber=' + page + '&search=' + search + '&itemsPerPage=' + itemsPerPage;
        },
    });
}])

managementapp.directive('productEditor', ['recordEditorFactory', 'bworkflowAdminApi', 'FileUploader', 'productManagementSvc', '$filter',
    function (factory, bworkflowAdminApi, FileUploader, productManagementSvc, $filter) {
        return factory({
            templateUrl: 'angulartemplates/admin/product/product_editor.html',
            link: function (scope) {
                scope.uploadFile = function () {
                    scope.results = null;
                    scope.uploadFailed = null;
                    scope.__showaddproduct = false;

                    var uploader = scope.uploader = new FileUploader({
                        scope: scope,
                        url: "ProductManagement/MediaUpload"
                    });
                    scope.uploadargs = { id: scope.record.id };

                    uploader.onBeforeUploadItem = function (item) {
                        Array.prototype.push.apply(item.formData, [scope.uploadargs]);
                    };

                    uploader.onSuccessItem = function (fileItem, response, status, headers) {
                        scope.record.mediaid = response[0].data;
                        scope.nonce = Date.now() / 1000 | 0;
                    };

                    scope.uploadingfile = true;
                };

                scope.imageUrl = bworkflowAdminApi.getImageUrl();
                scope.nonce = Date.now() / 1000 | 0;

                scope.uploadingfile = false;
                scope.types = [];

                scope.$watch('record', function (newValue, oldValue) {
                    if (newValue == null) {
                        return;
                    }

                    if (newValue.__isnew == true) {
                        newValue.childproducts = [];
                    }
                });

                productManagementSvc.getTypes().then(function (types) {
                    scope.types = types;
                });

                scope.priceJsFunctions = [];
                productManagementSvc.getPriceJsFunctions().then(function (types) {
                    scope.priceJsFunctions = types;
                });

                scope.rawProducts = [];
                scope.products = [];
                productManagementSvc.getProducts().then(function (products) {
                    scope.rawProducts = products;
                });

                scope.removeProduct = function (product, array) {
                    var index = array.indexOf(product);
                    if (index > -1) {
                        array.splice(index, 1);
                    }
                };

                scope.showAddProduct = function (parent) {
                    scope.products = angular.copy(scope.rawProducts);

                    // remove the parent product
                    var ps = $filter('filter')(scope.products, { id: parent.id }, true);
                    scope.removeProduct(ps[0], scope.products);

                    // don't show anything already in our list
                    angular.forEach(scope.record.childproducts, function (child) {
                        var ps = $filter('filter')(scope.products, { id: child.childid }, true);

                        if (ps.length > 0) {
                            scope.removeProduct(ps[0], scope.products);
                        }
                    });

                    scope.__showaddproduct = true;
                };

                scope.newItem = function (product) {
                    var ps = $filter('filter')(scope.products, { id: product }, true);

                    var ni = {
                        parentid: scope.record.id,
                        childid: ps[0].id,
                        childname: ps[0].name,
                        name: ps[0].name
                    };

                    scope.record.childproducts.push(ni);

                    scope.__showaddproduct = false;
                };

                scope.hideAddProduct = function () {
                    scope.__showaddproduct = false;
                };

                scope.removeChild = function (child, parent) {
                    scope.removeProduct(child, parent.childproducts);
                };

                scope.record.extra = scope.record.extra || {};
                scope.extra = JSON.stringify(scope.record.extra);

                scope.$watch('record.extra', function (newValue, oldValue) {
                    if (scope.record && scope.record.extra) {
                        scope.extra = JSON.stringify(scope.record.extra);
                    } else {
                        scope.extra = '';
                    }
                });

                scope.onExtraChange = function () {
                    try {
                        scope.extraError = null;
                        var extra = JSON.parse(scope.extra);

                        scope.record.extra = extra;
                    } catch (err) {
                        scope.extraError = err.message;
                    }
                };

            }
        });
    }]);

managementapp.directive('jsFunctionHelpLineitemPrice', [function () {
    return {
        templateUrl: window.razordata.siteprefix + 'angulartemplates/admin/jsfunction/help-lineitem-price.html',
    }
}]);

managementapp.directive('jsFunctionHelpCoupon', [function () {
    return {
        templateUrl: window.razordata.siteprefix + 'angulartemplates/admin/jsfunction/help-coupon.html',
    }
}]);

managementapp.factory('JsFunctionManagementSvc', ['$http', '$q', '$timeout', '$sce', function ($http, $q, $timeout, $sce) {
    var types = [{
        id: 1,
        name: 'LineItemPrice',
        testArgs: function () {
            return {
                properties: {
                    quantity: 1,
                    price: 1,
                    day: moment().date(),
                    month: moment().month() + 1,
                    year: moment().year(),
                    hour: moment().hour(),
                    minute: moment().minute(),
                    offset: moment().utcOffset()
                },
                meta: {
                    quantity: { group: 'Item', type: 'number' },
                    price: { group: 'Item', type: 'number' },
                    day: { group: 'Date', type: 'number', options: { min: 1, max: 31 } },
                    month: { group: 'Date', type: 'number', options: { min: 1, max: 12 } },
                    year: { group: 'Date', type: 'number' },
                    hour: { group: 'Time', type: 'number', options: { min: 0, max: 23 } },
                    minute: { group: 'Time', type: 'number', options: { min: 0, max: 59 } },
                    offset: { group: 'Date', type: 'number', options: { min: -60 * 12, max: 60 * 12, step: 30 } },
                },
                makeLocals: function (p) {
                    return {
                        item: {
                            quantity: p.quantity,
                            price: p.price,
                            surcharge: 0,
                            discount: 0
                        },
                        notes: [],
                        now: moment([p.year, p.month - 1, p.day, p.hour, p.minute]).utcOffset(p.offset)
                    }
                },
                result: function (result, locals) {
                    return {
                        result: {
                            price: result,
                            notes: locals.notes,
                            now: locals.now.format()
                        }, meta: {
                            price: { group: 'Result', type: 'label' },
                            notes: { group: 'Result', type: 'textarea', colspan2: true }
                        }

                    }
                }
            }
        },
        helpDirective: '<js-function-help-lineitem-price></js-function-help-lineitem-price>'
    }, {
        id: 2,
        name: 'Coupon',
        testArgs: function () {
            return {
                properties: {
                    day: moment().date(),
                    month: moment().month() + 1,
                    year: moment().year(),
                    hour: moment().hour(),
                    minute: moment().minute(),
                    offset: moment().utcOffset()
                },
                meta: {
                    day: { group: 'Date', type: 'number', options: { min: 1, max: 31 } },
                    month: { group: 'Date', type: 'number', options: { min: 1, max: 12 } },
                    year: { group: 'Date', type: 'number' },
                    hour: { group: 'Time', type: 'number', options: { min: 0, max: 23 } },
                    minute: { group: 'Time', type: 'number', options: { min: 0, max: 59 } },
                    offset: { group: 'Date', type: 'number', options: { min: -60 * 12, max: 60 * 12, step: 30 } }
                },
                makeLocals: function (p) {
                    return {
                        args: {
                            payments: [],
                            coupon: {},
                            order: {},
                            now: moment([p.year, p.month - 1, p.day, p.hour, p.minute]).utcOffset(p.offset)
                        },
                    }
                },
                result: function (result, locals) {
                    return {
                        result: {
                            args: result.args,
                            now: locals.now.format()
                        }, meta: {
                            price: { group: 'Result', type: 'label' },
                            notes: { group: 'Result', type: 'textarea', colspan2: true }
                        }
                    }
                }
            }
        },
        helpDirective: '<js-function-help-coupon></js-function-help-coupon>'
    }];

    var typesById = Object.create(null);
    types.forEach(function (type) {
        typesById[type.id] = type;
    });

    var svc = {
        rooturl: window.razordata.siteprefix + 'Admin/JsFunctionManagement/',
        export: function () {
            $http.get(url + 'Export');
        },
        types: types,
        typesById: typesById
    }

    return svc;
}])

managementapp.controller('JsFunctionManagementCtrl', ['$scope', 'recordControllerFactory', 'JsFunctionManagementSvc', 'FileUploader', function ($scope, factory, svc, FileUploader) {
    var ctrl = factory($scope, {
        name: 'JsFunction',
        mvcControllerUrl: 'Admin/JsFunctionManagement/'
    })

    $scope.showImportDialog = false;

    $scope.uploader = new FileUploader();
    $scope.uploader.url = window.razordata.siteprefix + 'Admin/JsFunctionManagement/Import';
    $scope.uploader.alias = 'file';

    $scope.uploader.onSuccessItem = function (fileItem, response, status, headers) {
        $scope.importresults = response;

        $scope.importfailed = response.errors.length > 0;
    };

    $scope.getExportUrl = function () {
        return window.razordata.siteprefix + 'Admin/JsFunctionManagement/export';
    };

    $scope.showImport = function () {
        $scope.uploader.clearQueue();
        $scope.showImportDialog = true;
    };

    $scope.closeImport = function () {
        $scope.showImportDialog = false;
    };

    $scope.uploadSchedule = function () {
        $scope.uploader.uploadAll();
    };

    return ctrl;
}]);

managementapp.directive('jsFunctionSidebar', ['recordAjaxSidebarFactory', function (factory) {
    return factory({
        templateUrl: 'angulartemplates/admin/jsfunction/jsfunction_sidebar.html',
        ajaxUrl: function (page, search, itemsPerPage, includeArchived) {
            return 'Admin/JsFunctionManagement/Search?pageNumber=' + page + '&search=' + search + '&itemsPerPage=' + itemsPerPage + '&includeArchived=' + includeArchived;
        },
    });
}])

managementapp.directive('jsFunctionEditor', ['recordEditorFactory', 'bworkflowAdminApi', 'FileUploader', 'JsFunctionManagementSvc', 'jsFunctionSvc', '$timeout', '$log', '$injector',
    function (factory, bworkflowAdminApi, FileUploader, svc, jsFunctionSvc, $timeout, $log, $injector) {
        return factory({
            templateUrl: 'angulartemplates/admin/jsfunction/jsfunction_editor.html',
            link: function (scope, element) {
                var customTypes = {
                    textarea: {
                        html: function (elemId, name, value, meta) {
                            var html = '<textarea id="' + elemId + '" rows=6 style="white-space: nowrap; overflow-x: auto; width:90%">';
                            if (value instanceof Array) {
                                html += value.join("\n");
                            }
                            html += '</textarea>';
                            return html;
                        },
                        makeValueFn: function (elemId, name, value, meta) {
                            return function () {
                                return $('#' + elemId).val().split('\n');
                            }
                        }
                    }
                };

                scope.nonce = Date.now() / 1000 | 0;

                scope.types = svc.types;
                scope.typesById = svc.typesById;

                function testIt() {
                    if (angular.isFunction(scope.fn) && angular.isDefined(scope.testArgs)) {
                        var jobj = $('#testArgs').jqPropertyGrid('get');

                        var locals = scope.testArgs.makeLocals(jobj);

                        try {
                            var result = $injector.invoke(scope.fn, $injector.get('jsFunction' + scope.type.name), locals);
                            var resultGrid = scope.testArgs.result(result, locals);
                            $('#testResult').jqPropertyGrid(resultGrid.result, {
                                meta: resultGrid.meta,
                                customTypes: customTypes
                            });
                        } catch (e) {
                            scope.scriptErrors = e;
                        }
                    }
                }

                function tryParse() {
                    try {
                        delete scope.fn;
                        scope.fn = eval('(' + scope.record.script + ')');
                        scope.scriptErrors = '';

                        testIt();
                    } catch (e) {
                        scope.scriptErrors = e;
                    }
                }

                var txtArea = $('#scripteditor', element);
                if (txtArea.length) {
                    scope.codeMirrorEditor = CodeMirror.fromTextArea(txtArea[0], {
                        lineNumbers: true,
                        theme: "neat",
                        mode: 'javascript',
                        onChange: function (instance) {
                            var newValue = instance.getValue();
                            if (newValue !== scope.record.script) {
                                scope.$evalAsync(function () {
                                    scope.record.script = newValue;
                                    tryParse();
                                });
                            }
                        }
                    });

                    scope.$watch('record', function (newValue, oldValue) {
                        scope.codeMirrorEditor.setValue(scope.record.script || '');
                        tryParse();
                    })

                    scope.$watch('record.type', function (newValue, oldValue) {
                        if (angular.isUndefined(newValue)) {
                            return;
                        }

                        scope.type = scope.typesById[newValue];
                        if (scope.type.testArgs) {
                            scope.testArgs = scope.type.testArgs();

                            $('#testArgs').jqPropertyGrid(scope.testArgs.properties, {
                                meta: scope.testArgs.meta,
                                customTypes: customTypes,
                                callback: function () {
                                    scope.$evalAsync(testIt);
                                }
                            });
                        }
                    });
                }
            }
        });
    }]);

managementapp.factory('tabletProfileSvc', ['$http', '$q', function ($http, $q) {
    var url = window.razordata.siteprefix + 'Admin/TabletProfileAdmin/';

    var _confirmlogouttypes = [{
        id: 0, name: 'None'
    }, {
        id: 1, name: 'Simple Confirm'
    }];

    var svc = {
        getConfirmLogoutTypes: function () {
            return $q.when(_confirmlogouttypes);
        }
    }

    return svc;
}])

managementapp.controller("tabletProfileAdminCtrl", ['$scope', 'recordControllerFactory', 'tabletProfileSvc', '$timeout',
    function ($scope, factory, tabletProfileSvc, $timeout) {
        var ctrl = factory($scope, {
            name: 'TabletProfile',
            idField: 'id',
            nameField: 'name',
            mvcControllerUrl: 'Admin/TabletProfileAdmin/'
        })

        tabletProfileSvc.getConfirmLogoutTypes().then(function (types) {
            $scope.confirmlogouttypes = types;
        });

        return ctrl;
    }
]);

managementapp.directive('tabletProfileSidebar', ['recordAjaxSidebarFactory', function (factory) {
    return factory({
        templateUrl: 'angulartemplates/admin/tabletprofileadmin/tablet_profile_sidebar.html',
        ajaxUrl: function (page, search, itemsPerPage, includeArchived) {
            return 'Admin/TabletProfileAdmin/TabletProfileSearch?pageNumber=' + page + '&search=' + search + '&itemsPerPage=' + itemsPerPage + '&includeArchived=' + includeArchived;
        },
    });
}])

managementapp.directive('tabletProfileEditor', ['recordEditorFactory', 'tabletProfileSvc', '$timeout', function (factory, tabletProfileSvc, $timeout) {
    return factory({
        templateUrl: 'angulartemplates/admin/tabletprofileadmin/tablet_profile_editor.html',
        link: function (scope) {
            tabletProfileSvc.getConfirmLogoutTypes().then(function (types) {
                scope.confirmlogouttypes = types;
            });
        }
    });
}])

// STANDARDS MANAGEMENT
managementapp.controller('standardsmanagementCtrl', [
    '$scope', '$http', '$q', '$filter', '$timeout', function ($scope, $http, $q, $filter, $timeout) {
        $scope.rooturl = window.razordata.siteprefix + 'Admin/StandardsManagement/';

        $scope.getAllStandards = function () {
            $http.get($scope.rooturl + "Standards")
                .success(function (data) {
                    $scope.standards = data;
                });
        };

        $scope.newStandard = function () {
            $scope.original = null;

            $scope.editing = {
                __isnew: true,
                type: 'New Standard',
                value: 0,
                value2: null,
                comparison: 'lt'
            };
        };

        $scope.$on('standard.edit', function (event, args) {
            $scope.original = args;

            $scope.editing = args;
        });

        $scope.$on('standard.save', function (event, args) {
            var parameters = { standard: args };

            $http.post($scope.rooturl + "StandardSave", parameters)
                .success(function (data) {
                    if (data == false) {
                        return;
                    }

                    scope.standards = data;
                });
        });

        $scope.getAllStandards();
    }
]);

managementapp.directive('standardsSidebar', [
    function () {
        return {
            templateUrl: window.razordata.siteprefix + 'angulartemplates/admin/standards/standards_sidebar.html',
            restrict: 'E',
            require: 'ngModel',
            scope: {
                standards: '=ngModel'
            },
            link: function (scope, elt, attrs, ngModel) {
                scope.editStandard = function (standard) {
                    scope.$emit('standard.edit', standard);
                };
            }
        };
    }
]);

managementapp.directive('standardsEditor', ['$filter', 'FileUploader', 'bworkflowAdminApi', '$http',
    function ($filter, FileUploader, bworkflowAdminApi, $http) {
        return {
            templateUrl: window.razordata.siteprefix + 'angulartemplates/admin/standards/standards_editor.html',
            restrict: 'E',
            require: 'ngModel',
            scope: {
                standard: '=ngModel'
            },
            link: function (scope, elt, attrs, ngModel) {
                scope.comparisons = [{ display: 'Less Than', value: 'lt' }, { display: 'Greater Than', value: 'gt' }, { display: 'Between', value: 'between' }];

                scope.save = function () {
                    scope.$emit('standard.save', scope.standard);
                };
            }
        };
    }
]);

// QRCode management
managementapp.factory('qrcodeAdminSvc', ['$http', function ($http) {
    var _attachmentTypes = [
        { id: 0, name: 'User/Site', class: 'usersite' },
        { id: 1, name: 'Task Type', class: 'tasktype' }
    ]

    var _rooturl = window.razordata.siteprefix + 'Admin/QRCodeManagement/';

    return {
        getAttachmentTypes: function () {
            return _attachmentTypes;
        },

        setSelection: function (record) {
            return $http.post(_rooturl + 'SetSelection', { id: record.id, selected: record.selected }).then(function (response) {
                record.selected = JSON.parse(response.data);
            });
        }
    }
}])

managementapp.controller("qrcodeAdminCtrl", ['$scope', 'recordControllerFactory', '$timeout',
    function ($scope, factory, $timeout) {
        var ctrl = factory($scope, {
            name: 'QRCode',
            idField: 'id',
            nameField: 'qrcode',
            mvcControllerUrl: 'Admin/QRCodeManagement/'
        })

        return ctrl;
    }
]);

managementapp.directive('qrcodeSidebar', ['recordAjaxSidebarFactory', 'qrcodeAdminSvc', 'FileUploader', function (factory, svc, FileUploader) {
    return factory({
        templateUrl: 'angulartemplates/admin/qrcodeadmin/qrcode_sidebar.html',
        ajaxUrl: function (page, search, itemsPerPage, includeArchived, scope) {
            var url = 'Admin/QRCodeManagement/Search?pageNumber=' + page + '&search=' + search + '&itemsPerPage=' + itemsPerPage + '&includeArchived=' + includeArchived;

            if (scope.searchFlags) {
                url += '&searchFlags=' + scope.searchFlags + '&includeFlags=' + scope.includeFlags;
            }

            if (scope.showSelected) {
                url += '&showSelected=' + (scope.showSelected == 1);
            }
            return url;
        },

        link: function (scope) {
            scope.attachmentTypes = svc.getAttachmentTypes();

            scope.searchFlags = 0;
            scope.includeFlags = 0;

            scope.toggleAttachmentType = function (t) {
                t.include = ((t.include || 0) + 1) % 3;
                var i = 1 << t.id;
                switch (t.include) {
                    case 0: // Dont care
                        scope.searchFlags &= ~i;
                        scope.includeFlags &= ~i;
                        break;

                    case 1: // Include
                        scope.searchFlags |= i;
                        scope.includeFlags |= i;
                        break;

                    case 2: // Exclude
                        scope.searchFlags |= i;
                        scope.includeFlags &= ~i;
                        break;
                }
                scope.refresh();
            }

            scope.toggleSelection = function (record) {
                record.selected = !record.selected;

                svc.setSelection(record);
            }

            scope.showSelected = 0;
            scope.toggleShowSelected = function () {
                scope.showSelected = (scope.showSelected + 1) % 3;
                scope.refresh();
            }

            scope.newRecord = function () {
                scope.$emit('newRecord');
            }

            scope.getExportUrl = function (selected) {
                return window.razordata.siteprefix + 'Admin/QRCodeManagement/Export?onlySelected=' + selected;
            }

            scope.showImportDialog = false;

            scope.uploader = new FileUploader();
            scope.uploader.url = window.razordata.siteprefix + 'Admin/QRCodeManagement/Import';
            scope.uploader.alias = 'file';

            scope.uploader.onSuccessItem = function (fileItem, response, status, headers) {
                scope.importresults = response;

                scope.importfailed = response.errors.length > 0;
            };

            scope.showImport = function () {
                scope.uploader.clearQueue();
                scope.showImportDialog = true;
            };

            scope.closeImport = function () {
                scope.showImportDialog = false;
            };

            scope.uploadAll = function () {
                scope.uploader.uploadAll();
            };

        }
    });
}])

managementapp.directive('qrcodeEditor', ['recordEditorFactory', '$timeout', function (factory, $timeout) {
    return factory({
        templateUrl: 'angulartemplates/admin/qrcodeadmin/qrcode_editor.html',
        link: function (scope) {
        }
    });
}])

// Beacon management
managementapp.factory('beaconAdminSvc', ['$http', function ($http) {
    var _attachmentTypes = [
        { id: 0, name: 'User/Site', class: 'usersite' },
        { id: 1, name: 'Task Type', class: 'tasktype' }
    ]

    var _rooturl = window.razordata.siteprefix + 'Admin/BeaconManagement/';

    return {
        getAttachmentTypes: function () {
            return _attachmentTypes;
        }
    }
}])

managementapp.controller("beaconAdminCtrl", ['$scope', 'recordControllerFactory', '$timeout',
    function ($scope, factory, $timeout) {
        var ctrl = factory($scope, {
            name: 'Beacon',
            idField: 'id',
            nameField: 'name',
            mvcControllerUrl: 'Admin/BeaconManagement/'
        })

        return ctrl;
    }
]);

managementapp.directive('beaconSidebar', ['recordAjaxSidebarFactory', 'beaconAdminSvc', 'FileUploader', function (factory, svc, FileUploader) {
    return factory({
        templateUrl: 'angulartemplates/admin/beaconadmin/beacon_sidebar.html',
        ajaxUrl: function (page, search, itemsPerPage, includeArchived, scope) {
            var url = 'Admin/BeaconManagement/Search?pageNumber=' + page + '&search=' + search + '&itemsPerPage=' + itemsPerPage + '&includeArchived=' + includeArchived;

            if (scope.searchFlags) {
                url += '&searchFlags=' + scope.searchFlags + '&includeFlags=' + scope.includeFlags;
            }
            return url;
        },

        link: function (scope) {
            scope.attachmentTypes = svc.getAttachmentTypes();

            scope.searchFlags = 0;
            scope.includeFlags = 0;

            scope.toggleAttachmentType = function (t) {
                t.include = ((t.include || 0) + 1) % 3;
                var i = 1 << t.id;
                switch (t.include) {
                    case 0: // Dont care
                        scope.searchFlags &= ~i;
                        scope.includeFlags &= ~i;
                        break;

                    case 1: // Include
                        scope.searchFlags |= i;
                        scope.includeFlags |= i;
                        break;

                    case 2: // Exclude
                        scope.searchFlags |= i;
                        scope.includeFlags &= ~i;
                        break;
                }
                scope.refresh();
            }

            scope.newRecord = function () {
                scope.$emit('newRecord');
            }

        }
    });
}])







managementapp.directive('beaconEditor', ['recordEditorFactory', '$timeout', function (factory, $timeout) {
    return factory({
        templateUrl: 'angulartemplates/admin/beaconadmin/beacon_editor.html',
        link: function (scope) {
        }
    });
}]);

managementapp.directive('peJobExecutePublishedChecklistEditor', [function () {
    return {
        templateUrl: window.razordata.siteprefix + 'angulartemplates/admin/pejob/pejob_execute_published_checklist_editor.html',
        restrict: 'E',
        link: function (scope, elt, attrs, ngModel) {
            scope.record.jobdata = scope.record.jobdata || {};
            scope.extra = JSON.stringify(scope.record.jobdata.extra);

            scope.$watch('record.jobdata.extra', function (newValue, oldValue) {
                if (scope.record && scope.record.jobdata && scope.record.jobdata.extra) {
                    scope.extra = JSON.stringify(scope.record.jobdata.extra);
                } else {
                    scope.extra = '';
                }
            });

            scope.onExtraChange = function () {
                try {
                    scope.extraError = null;
                    var extra = JSON.parse(scope.extra);

                    scope.record.jobdata.extra = extra;
                } catch (err) {
                    scope.extraError = err.message;
                }
            };
        }
    };
}]);

managementapp.directive('peJobEwayPostExecuteChecklistEditor', [function () {
    return {
        templateUrl: window.razordata.siteprefix + 'angulartemplates/admin/pejob/pejob_eway_post_execute_checklist_editor.html',
        restrict: 'E',
        link: function (scope, elt, attrs, ngModel) {
            scope.record.jobdata = scope.record.jobdata || {};
            scope.extra = JSON.stringify(scope.record.jobdata.extra);

            scope.$watch('record.jobdata.extra', function (newValue, oldValue) {
                if (scope.record && scope.record.jobdata && scope.record.jobdata.extra) {
                    scope.extra = JSON.stringify(scope.record.jobdata.extra);
                } else {
                    scope.extra = '';
                }
            });

            scope.onExtraChange = function () {
                try {
                    scope.extraError = null;
                    var extra = JSON.parse(scope.extra);

                    scope.record.jobdata.extra = extra;
                } catch (err) {
                    scope.extraError = err.message;
                }
            };
        }
    };
}]);

managementapp.factory('PEJobAdminSvc', ['$q', function ($q) {
    var _types = [{
        id: 'ExecutePublishedChecklist',
        name: 'Execute Published Checklist',
        tabs: [{
            name: 'Execute Published Checklist',
            editDirective: '<pe-job-execute-published-checklist-editor></pe-job-execute-published-checklist-editor>'
        }]
    }, {
        id: 'eWayProcessPendingPayment',
        name: 'eWay Process Pending Payment',
        tabs: [{
            name: 'Post execute checklist',
            editDirective: '<pe-job-eway-post-execute-checklist-editor></pe-job-eway-post-execute-checklist-editor>'
        }]
    }];

    var _typesById = Object.create(null);
    _types.forEach(function (type) {
        _typesById[type.id] = type;
    });

    return {
        getTypes: function () {
            return $q.when({
                types: _types,
                typesById: _typesById
            });
        }
    };
}])

managementapp.controller("PEJobAdminCtrl", ['$scope', 'recordControllerFactory', '$timeout',
    function ($scope, factory, $timeout) {
        var ctrl = factory($scope, {
            name: 'PEJob',
            idField: 'id',
            nameField: 'name',
            mvcControllerUrl: 'API/v1/PEJob/',
            isAPI: true,
            modelFromDto: function (dto, model) {
                model = angular.extend(model || {}, dto);
                model.nextrunutc = dto.nextrunutc ? moment.utc(dto.nextrunutc).local() : null;
                return model;
            },
            modelToDto: function (model, dto) {
                dto = angular.extend(dto || {}, model);
                dto.nextrunutc = null;              // We do not change NextRunUtc
                return dto;
            }
        })

        return ctrl;
    }
]);

managementapp.directive('peJobSidebar', ['recordAjaxSidebarFactory', function (factory) {
    return factory({
        templateUrl: 'angulartemplates/admin/pejob/pejob_sidebar.html',
        ajaxUrl: function (page, search, itemsPerPage, includeArchived) {
            return 'API/v1/PEJob/Search?pageNumber=' + page + '&search=' + search + '&itemsPerPage=' + itemsPerPage + '&includeArchived=' + includeArchived;
        },
    });
}])

managementapp.directive('peJobEditor', ['recordEditorFactory', 'bworkflowAdminApi', 'PEJobAdminSvc', '$timeout', '$log', '$injector',
    function (factory, bworkflowAdminApi, svc, $timeout, $log, $injector) {
        return factory({
            templateUrl: 'angulartemplates/admin/pejob/pejob_editor.html',
            link: function (scope, element) {
                svc.getTypes().then(function (r) {
                    scope.types = r.types;
                    scope.typesById = r.typesById;
                })
            }
        });
    }]);

managementapp.factory('esprobAdminSvc', ['$http', function ($http) {
    var _attachmentTypes = [
        { id: 0, name: 'Site', class: 'usersite' }
    ]



    return {
        getAttachmentTypes: function () {
            return _attachmentTypes;
        }
    }
}])

managementapp.factory('esprobAdminSvc', ['$http', function ($http) {
    var _attachmentTypes = [
        { id: 0, name: 'Site', class: 'usersite' }
    ]



    return {
        getAttachmentTypes: function () {
            return _attachmentTypes;
        }
    }
}])

managementapp.factory('ESProbeAdminSvc', ['$q', '$http', function ($q, $http) {
    var rooturl = window.razordata.siteprefix;

    var _sensorTypes = [{
        id: 1,
        name: 'Temperature',
        hasCompliance: true
    }, {
        id: 2,
        name: 'Humidity',
    }, {
        id: 3,
        name: 'Barometric Pressure',
    }, {
        id: 4,
        name: 'Dew point',
    }, {
        id: 5,
        name: 'Battery',
    }]

    var _sensorTypesById = Object.create(null);
    _sensorTypes.forEach(function (type) {
        _sensorTypesById[type.id] = type;
    });

    return {
        getSensorTypes: function () {
            return $q.when({
                types: _sensorTypes,
                typesById: _sensorTypesById
            });
        },
        getComplianceStandards: function () {
            return $http.get(rooturl + "Admin/StandardsManagement/Standards").then(function (response) {
                return response.data;
            })
        },
        getLastSensorReading: function (sensorId) {
            return $http.get(window.razordata.odataprefix + 'odata-v3/ESSensorReadings?$filter=SensorId eq ' + sensorId + '&$top=1&$orderby=SensorTimestampUtc desc').then(function (response) {
                var reading = {};
                if (response.data.value.length) {
                    reading = response.data.value[0];
                    reading.Value = Number(reading.Value);
                    reading.ServerTimestampUtc = moment.utc(reading.ServerTimestampUtc);
                    reading.SensorTimestampUtc = moment.utc(reading.SensorTimestampUtc);
                }
                return reading;
            })
        }
    };
}])

managementapp.controller("ESProbeAdminCtrl", ['$scope', 'recordControllerFactory', '$timeout',
    function ($scope, factory, $timeout) {
        var ctrl = factory($scope, {
            name: 'ESProbe',
            idField: 'id',
            nameField: 'name',
            mvcControllerUrl: 'API/v1/ESProbe/',
            isAPI: true,
            modelFromDto: function (dto, model) {
                model = angular.extend(model || {}, dto);
                return model;
            },
            modelToDto: function (model, dto) {
                dto = angular.extend(dto || {}, model);
                return dto;
            }
        })

        return ctrl;
    }
]);

managementapp.directive('esProbeSidebar', ['recordAjaxSidebarFactory', 'esprobAdminSvc',function (factory ,svc) {
    return factory({
        templateUrl: 'angulartemplates/admin/esprobeadmin/esprobe_sidebar.html',
        ajaxUrl: function (page, search, itemsPerPage, includeArchived,scope) {
            var url = 'API/v1/ESProbe/Search?pageNumber=' + page + '&search=' + search + '&itemsPerPage=' + itemsPerPage + '&includeArchived=' + includeArchived;
            if (scope.searchFlags) {
                url += '&searchFlags=' + scope.searchFlags + '&includeFlags=' + scope.includeFlags;
            }
            return url;
        },
        link: function (scope) {
            scope.attachmentTypes = svc.getAttachmentTypes();

            scope.searchFlags = 0;
            scope.includeFlags = 0;

            scope.toggleAttachmentType = function (t) {
                t.include = ((t.include || 0) + 1) % 3;
                var i = 1 << t.id;
                switch (t.include) {
                    case 0: // Dont care
                        scope.searchFlags &= ~i;
                        scope.includeFlags &= ~i;
                        break;

                    case 1: // Include
                        scope.searchFlags |= i;
                        scope.includeFlags |= i;
                        break;

                    case 2: // Exclude
                        scope.searchFlags |= i;
                        scope.includeFlags &= ~i;
                        break;
                }
                scope.refresh();
            }

            scope.newRecord = function () {
                scope.$emit('newRecord');
            }

        }

    });
}])

managementapp.directive('esProbeEditor', ['recordEditorFactory', 'bworkflowAdminApi', 'ESProbeAdminSvc', '$timeout', '$log', '$injector',
    function (factory, bworkflowAdminApi, svc, $timeout, $log, $injector) {
        return factory({
            templateUrl: 'angulartemplates/admin/esprobeadmin/esprobe_editor.html',
            link: function (scope, element) {
                svc.getSensorTypes().then(function (r) {
                    scope.sensorTypes = r.types;
                    scope.sensorTypesById = r.typesById;
                })

                svc.getComplianceStandards().then(function (standards) {
                    scope.standards = standards;
                })

                scope.$watch('record', function (newValue, oldValue) {
                    if (newValue) {
                        scope.lastReading = {};
                        angular.forEach(newValue.sensors, function (sensor) {
                            svc.getLastSensorReading(sensor.id).then(function (reading) {
                                scope.lastReading[sensor.id] = reading;
                            })
                        });
                    }
                })
            }
        });
    }]);

managementapp.factory('teamHierarchySvc', ['$http', '$q', '$timeout', function ($http, $q, $timeout) {
    var svc = {
        rooturl: window.razordata.siteprefix + 'Organisation/TeamHierarchy/',
        timesheetitemtypes: null,
        teamHierarchyModelFromDto: function (dto, model) {
            model = angular.extend(model || {}, dto);

            return model;
        },

        teamHierarchyModelToDto: function (model, dto) {
            dto = angular.extend(dto || {}, model);

            return model;
        },

        getChartData: function (hierarchyId) {
            var d = $q.defer();

            $http.get(svc.rooturl + "Chart?id=" + hierarchyId)
                .success(function (data) {
                    d.resolve(data);
                });

            return d.promise;
        },

        getTimeSheetItemTypes: function () {
            var d = $q.defer();

            if (svc.timesheetitemtypes != null) {
                $timeout(function () {
                    d.resolve(svc.timesheetitemtypes);
                });
                return d.promise;
            }

            $http.get(svc.rooturl + "TimesheetItemTypes").success(function (data) {
                svc.timesheetitemtypes = data;

                d.resolve(svc.timesheetitemtypes);
            });

            return d.promise;
        },

        getExportUrl: function (id) {
            return svc.rooturl + "Export?hierarchyId=" + id;
        }
    }

    return svc;
}])

managementapp.controller("teamHierarchyAdminCtrl", ['$scope', 'recordControllerFactory', '$timeout', 'teamHierarchySvc',
    function ($scope, factory, $timeout, teamHierarchySvc) {
        var ctrl = factory($scope, {
            name: 'TeamHierarchy',
            idField: 'id',
            nameField: 'name',
            mvcControllerUrl: 'Organisation/TeamHierarchy/',
            modelFromDto: teamHierarchySvc.teamHierarchyModelFromDto,
            modelToDto: teamHierarchySvc.teamHierarchyModelToDto
        })

        return ctrl;
    }
]);

managementapp.directive('teamHierarchySidebar', ['recordAjaxSidebarFactory', function (factory) {
    return factory({
        templateUrl: 'angulartemplates/admin/teamhierarchy/teamhierarchy_sidebar.html',
        ajaxUrl: function (page, search, itemsPerPage, includeArchived) {
            return 'Organisation/TeamHierarchy/Search?pageNumber=' + page + '&search=' + search + '&itemsPerPage=' + itemsPerPage;
        },
    });
}]);

managementapp.directive('teamHierarchyEditor', ['recordEditorFactory',
    '$timeout',
    'teamHierarchySvc',
    'FileUploader',
    '$filter', function (factory, $timeout, teamHierarchySvc, FileUploader, $filter) {
    return factory({
        templateUrl: 'angulartemplates/admin/teamhierarchy/teamhierarchy_editor.html',
        controller: function ($scope) {
            $scope.uploadingfile = false;
            $scope.uploadFile = function () {
                $scope.uploadresults = null;
                $scope.uploadFailed = null;

                var uploader = $scope.uploader = new FileUploader({
                    scope: $scope,
                    url: "TeamHierarchy/Import"
                });

                $scope.uploadargs = { id: $scope.record.id };

                uploader.onBeforeUploadItem = function (item) {
                    Array.prototype.push.apply(item.formData, [$scope.uploadargs]);
                };

                uploader.onSuccessItem = function (fileItem, response, status, headers) {
                    $scope.uploadresults = response;

                    $scope.uploadFailed = !$scope.uploadresults.success;
                    if ($scope.uploadFailed == false) {
                        var id = $scope.record.id;
                        $scope.record.id = null;
                        $timeout(function () {
                            $scope.record.id = id;
                        });
                    }
                };

                $scope.uploadingfile = true;
            };
        },
        link: function (scope) {
            scope.chartManager = {};
            scope.showaddeditbucket = { showing: false };
            scope.modaloperation = '';
            scope.bucketuser = {};
            scope.selected = { role: {}};
            scope.nextNewId = -1;
            scope.timesheetitemtypes = null;

            scope.doSave = function () {
                if (angular.isDefined(scope.chartManager) && scope.chartManager != null && angular.isDefined(scope.chartManager.getData)) {
                    scope.record.buckets = scope.chartManager.getData();
                }
                scope.save();
            };

            scope.$on('record.changed', function (evt, args) {
                scope.chart = args.buckets;
                scope.record.buckets = null;
            });

            scope.completeEditAdd = function (bucket, bucketuser) {
                    if (bucketuser != null && angular.isDefined(bucketuser.id) && bucketuser.id != null) {
                    bucket.userid = bucketuser.id;
                    bucket.usersname = bucketuser.name;
                }
                else {
                    bucket.userid = null;
                    bucket.usersname = '';
                }

                if (bucket.name === null || bucket.name === '') {
                    bucket.name = scope.selected.role.name;
                }
                bucket.rolename = scope.selected.role.name;

                if (scope.modaloperation === 'org-chart.add') {
                    scope.chartManager.addBucket(bucket);
                }
                else {
                    scope.chartManager.updateBucket(bucket);
                }

                scope.showaddeditbucket.showing = false;
            };

            scope.$watch('record.id', function (newValue, oldValue) {
                if (angular.isDefined(newValue) == false || newValue == null) {
                    return;
                }

                teamHierarchySvc.getChartData(newValue).then(function (data) {
                    scope.chart = data;
                });
            });

            scope.$on('org-chart.edit', function (evt, args) {
                scope.bucket = args.manager.getBucket(args.bucketId);

                scope.modaloperation = 'org-chart.edit';

                scope.bucketuser = { id: scope.bucket.userid, name: scope.bucket.usersname };
                scope.showaddeditbucket.showing = true;
            });

            scope.$on('org-chart.add', function (evt, args) {
                scope.bucket = {
                    id: scope.nextNewId,
                    pid: args.bucketId,
                    name: '',
                    userid: null,
                    usersname: '',
                    roleid: null,
                    rolename: '',
                    isprimary: null
                };

                scope.modaloperation = 'org-chart.add';
                scope.nextNewId = scope.nextNewId - 1;
                scope.showaddeditbucket.showing = true;
            });

            scope.$on('org-chart.remove', function (evt, args) {
                if (window.confirm('Are you sure you want to remove the node from the hierarchy?') == false) {
                    return;
                }

                args.manager.removeBucket(args.bucketId);
            });

            scope.getExportUrl = function () {
                return teamHierarchySvc.getExportUrl(scope.record.id);
            }

            scope.showImport = function () {
                scope.uploadFile();
            }

            teamHierarchySvc.getTimeSheetItemTypes().then(function (data) {
                scope.timesheetitemtypes = data;
            });
        }
    });
}]);

