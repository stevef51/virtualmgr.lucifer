var app = null;

try {
    // in the IOS app, the player application has already been defined
    // by the bootstrapping we do there, so we don't want to write over the top
    // of that, so we get that module
    app = angular.module('player');
} catch (e) {
    // ok so we must be running in a normal browser, so in that case we create the module
    app = angular.module('player', []);
}

// now no matter what, add our dependancies
app.requires.push('ngResource',
        'ui.router',
        'ui.router.util',
        'ui.router.router',
        'ui.router.state',
        'ngGeolocation',
        'bworkflowApi',
        'cachedExecutionHandlers',
        'animationDirectives',
        'transitionCache',
        'questions',
        'bworkflowFilters',
        'ngAnimate',
        'bootstrapCompat',
        'angular.filter',
        'leaflet-directive',
        'angular-loading-bar',
        'ngPinchZoom',
        'angularBootstrapNavTree',
        'pascalprecht.translate',
        'languageTranslation',
        'angular-svg-round-progress',
        'siyfion.sfTypeahead',
        'bworkflowAnimationFullScreen',
        'temperatureModule',
        'oc.lazyLoad',
        'bworkflowErrorHandling',
        'ngToast',
        'bc.AngularKeypad',
        'moment-picker',
        'chart.js',
        'temperature-probe-manager-module',
        'bluetherm-thermaq-module',
        'monospaced.qrcode',
        'bluemaestro-tempo-module',
        'vcRecaptcha'
);

app.config(['$stateProvider',
        '$urlRouterProvider',
        '$provide',
        '$httpProvider',
function ($stateProvider, $urlRouterProvider, $provide, $httpProvider, $windowProvider, autoLogoutSvc) {

    // we want any HTTP requests to send down our authentication cookies.
    $httpProvider.defaults.withCredentials = true;
    //URL parameters HAVE to be lower case
    //This is because Angular normalises the casing on attribute names, and we match attribute names
    //to URL parameters
    $stateProvider
        //Checklist selection
        .state('sel', {
            //abstract: true,
            url: '/select',
            templateUrl: 'select_master.html',
            abstract: true
        })
        .state('sel.groups', {
            url: '/groups',
            templateUrl: 'select_groups.html',
            controller: 'groupsCtrl',
            resolve: { groupListResult: groupsCtrlResolver },
        })
        .state('sel.checklists', {
            url: '/checklists/:gid',
            templateUrl: 'select_checklists.html',
            controller: 'checklistsCtrl',
            resolve: { groupDetailsResult: checklistsCtrlResolver }
        })
        .state('sel.reviewees', {
            url: '/reviewees/:gid/:cid',
            templateUrl: 'select_reviewees.html',
            controller: 'revieweesCtrl',
            resolve: { groupDetailsResult: checklistsCtrlResolver }
        })
        //Player
        .state('play', {
            abstract: true,
            url: '/player',
            templateUrl: 'play_master.html',
            controller: 'playCtrl'
        })
        .state('play.begin', {
            url: '/begin/?groupId&resourceId&revieweeId&args&publishingGroupResourceId',
            controller: 'beginCtrl',
            templateUrl: 'play_loading.html'
        })
        .state('play.dashboard', {
            url: '/dashboard',
            templateUrl: 'play_dashboard.html',
            controller: 'dashboardCtrl'
        })
        .state('play.present', {
            url: '/continue/:workingDocumentId/?nonce',
            controller: 'presentRootCtrl',
            templateUrl: 'present_root.html',
        })
        .state('play.present.presentable', {
            templateUrl: 'play_present.html',
            controller: 'presentCtrl'
        })
        .state('play.present.error', {
            templateUrl: 'play_error.html',
            controller: 'errorCtrl'
        })
        .state('play.present.finishing', {
            templateUrl: 'play_finishing.html',
            controller: 'finishingCtrl'
        })
        .state('play.present.finished', {
            templateUrl: 'play_finished_default.html',
            controller: 'presentCtrl'
        });

    //Force case-insensitive route matches
    //https://github.com/angular-ui/ui-router/issues/197
    $urlRouterProvider.rule(function ($i, $location) {
        var path = $location.path();
        var normalised = path.toLowerCase();
        if (path != normalised) return normalised;
    });
    //Without this, the otherwise clause makes us go to /select/groups when the player starts
    $urlRouterProvider.when(/\/player/, function () {
        return true;
    });
    //This does soemthing sane with random hash urls
    $urlRouterProvider.otherwise('/player/dashboard');
}]);

// EVS-847 Ability to update App without logging out/in
app.factory('appUpdateMonitor', ['$window', '$interval', '$injector', '$rootScope', function ($window, $interval, $injector, $rootScope) {
    var _vmVersioning;
    var _vmSettings;
    var _pausers = [];
    var _checking = false;
    var _pausedChecks = 0;

    if (window.razordata.environment == 'mobile') {
        _vmVersioning = $injector.get('vmVersioning');
        _vmSettings = $injector.get('vmSettings');
    }

    var svc = {
        addPauser: function (pauser) {
            // pauser = function() as bool : true to pause App Updates
            // pauser = {
            //   fn: function() as bool : true to pause App Updates
            //   scope: scope to watch $destroy and remove the pauser
            // }
            if (angular.isFunction(pauser)) {
                pauser = { fn: pauser };
            } else if (angular.isDefined(pauser) && angular.isDefined(pauser.$root) && pauser.$root === $rootScope) {
                // pauser is a scope object
                pauser = { scope: pauser };
            } // else pauser is a POJO

            // scope with fn means we pause whilst this scope is alive 
            if (angular.isDefined(pauser.scope) && !angular.isDefined(pauser.fn)) {
                pauser.fn = function () {
                    return true;
                }
            }

            _pausers.push(pauser);

            // Return a function which removes the Pauser
            var fnRemovePauser = function () {
                var i = _pausers.indexOf(pauser);
                if (i >= 0) {
                    _pausers.splice(i, 1);
                }
            }

            if (angular.isDefined(pauser.scope)) {
                pauser.scope.$on('$destroy', function () {
                    fnRemovePauser();

                    // Now a Pauser has been removed, see if we can check now ..
                    if (_pausedChecks) {
                        _check();
                    }
                })
            }

            return fnRemovePauser;
        }
    }

    var _check = function () {
        if (_checking) {
            return;
        }

        var pause = false;
        // See if any pausers want us to pause App update check
        _pausers.forEach(function (pauser) {
            pause |= pauser.fn() ? true : false;
        })
        if (pause) {
            _pausedChecks++;        // Keep track of missed checks, we can perform these when Pausers are removed (above)
            return;
        }

        // We can check ..
        _pausedChecks = 0;
        _checking = true;
        _vmVersioning.updateResourcesIfRequired().then(
            function (result) {
                _checking = false;
                if (result) {
                    // New App update installed, page refresh
                    $window.location.reload();
                } else {
                    _vmSettings.updateProfile();
                }
            }, function (error) {
                _checking = false;
            }
        );
    }

    // Note, we only schedule checks if we are running within VM Player App
    if (_vmVersioning) {
        $interval(_check, 5 * 60 * 1000);        // Every 5 minutes check for new version

        // Check now aswell
        _check();
    }

    return svc;
}]);


// EVS-558 Version Control Refresh for devices
app.factory('pageRefresh', ['$window', '$http', '$interval', 'RequestsErrorHandler', function ($window, $http, $interval, RequestsErrorHandler) {
    var versionHash = null;
    var url = window.razordata.siteprefix;
    var timer;

    var stopChecking = function () {
        if (angular.isDefined(timer)) {
            $interval.cancel(timer);
            timer = undefined;
        }
    };

    var check = function () {
        RequestsErrorHandler.specificallyHandled(function () {
            $http({ url: url + 'api/v1/app/versionhash', method: 'GET' })
            .then(function (response) {
                stopChecking();

                if (versionHash != null) {
                    if (versionHash != response.data) {
                        // Hash has changed, force a page refresh
                        $window.location.reload();
                    } 
                }
                versionHash = response.data;
            }, function (error) {
            });
        });
    }

    var svc = {
        beginChecking: function () {
            // If we are already checking then ignore this request
            if (angular.isDefined(timer)) {
                return;
            }

            // Check now, but also schedule a retry (this retry will be cancelled if it succeeds)
            check();
            timer = $interval(check, 15000);
        }
    }

    svc.beginChecking();

    return svc;
}]);

//This stuff drives the checklist selection
//This stateOrder determines the forward/backward sequences for the checklist select area
app.run(['$rootScope', 'pageRefresh', '$window', 'autoLogoutSvc', 'appUpdateMonitor', function ($rootScope, pageRefresh, $window, autoLogoutSvc, appUpdateMonitor) {
    $rootScope.stateOrder = ['sel.groups', 'sel.checklists', 'sel.reviewees', 'play.begin'];
    $rootScope.nonceStack = []; //for the ordering of checklist forward/back

    // fix for EVS-780, this handles keyboard presses and makes sure that the auto logout side
    // of things get's reset with each key press
    $window.addEventListener('keyup', function (e) {
        autoLogoutSvc.longRestart(false);
    }, true);

    $window.addEventListener('click', function (e) {
        autoLogoutSvc.shortRestart(false);
    }, true);

    // fix for EVS-781, this stops the hardware backbutton in android doing nasty things
    // to the users in the app (logging them out etc)
    $window.document.addEventListener('backbutton', function (e) {
        e.preventDefault();
    }, false);
}]);

app.controller('groupsCtrl', ['$scope', 'groupListResult', function ($scope, groupListResult) {
    $scope.groups = groupListResult;
}]);

var groupsCtrlResolver = ['bworkflowApi', function(bworkflowApi) {
    return bworkflowApi.getPublishedGroups();
}];


app.controller('checklistsCtrl', ['$scope', 'groupDetailsResult', function ($scope, groupDetailsResult) {
    $scope.groupData = groupDetailsResult;
    $scope.gid = groupDetailsResult.Id;
    $scope.checklists = groupDetailsResult.Checklists;
}]);


app.controller('revieweesCtrl', ['$scope', 'groupDetailsResult', '$stateParams', function($scope, groupDetailsResult, $stateParams) {
    $scope.gid = groupDetailsResult.Id;
    $scope.cid = $stateParams.cid;
    $scope.reviewees = groupDetailsResult.Reviewees;
}]);

var checklistsCtrlResolver = ['$q', 'bworkflowApi', '$stateParams', function ($q, bworkflowApi, $stateParams) {
    var promise = bworkflowApi.getPublishedGroup($stateParams.gid);
    return promise;
}];

//and this stuff drives the player

app.controller('playCtrl', ['$scope', '$rootScope', '$state', 'transitionCache', 'animationManager', '$timeout', '$compile',
    function ($scope, $rootScope, $state, transitionCache, animationManager, $timeout, $compile) {

    $scope.getFullServerPath = function (path) {
        var trimP = path;
        if (trimP[0] == '/')
            trimP = trimP.substring(1);
        var trimPrefix = window.razordata.apiprefix;
        if (trimPrefix[trimPrefix.length - 1] == '/')
            trimPrefix = trimPrefix.substring(0, trimPrefix.length - 1);
        return trimPrefix + '/' + path;
    };
    
    $scope.$on('doSlideModal', function (ev, template) {
        var smr = $('.slide-modal-wrapper');
        var includeEl = $('<div ng-include="\'' + template + '\'"></div>');
        var compiled = $compile(includeEl)(ev.targetScope);
        $(smr).append(compiled);
        $timeout(function () {
            $('.slide-modal-root').hide('slide', { direction: 'left' }, function () {
                $('.slide-modal-wrapper').show('slide', { direction: 'right' });
            });

        });

    });
    $scope.$on('unSlideModal', function (ev) {
        $('.slide-modal-wrapper').hide('slide', { direction: 'right' }, function () {
            $('.slide-modal-wrapper').empty();
            $('.slide-modal-root').show('slide', { direction: 'left' });
        });
    });

    $scope.$on('player_broadcast', function (event, args) {
        if (args.__handled)
        {
            return;
        }
        // this event gets raised by questions when they want to notify other questions that something has changed.
        // typically this occurrs in a dashboard. We need to broadcast this back down the scopes to let others know
        // of the change. The event object created by who ever created the event should be of the form

        // args.name = name of event to broadcast out
        // args.data = object containing arguments relevant to the event
        $scope.$broadcast(args.name, args.data);
        args.__handled = true;
    });

    $scope.$on('player_broadcast_ajax_error', function (event, args) {
        // this allows code to handle the ajax errors themselves and then
        // to also broadcast it out and have it handled by the normal code as well.
        // An example of this is the odata feeds, which add the noUI flag to the object
        // so its logged on the server, but nothing is shown to the user
        $scope.$broadcast('ajax.error', args);
    });

    $scope.transitionWithPlayerModel = function (r, slideDir, noncearg) {
        if (typeof slideDir === 'undefined')
            slideDir = 'right';

        var wdid = r.WorkingDocumentId;
        transitionCache.addItem(r, wdid);
        var nonce = noncearg || window.rstring(7);
        $rootScope.nonceStack.push(nonce);
        if (r.State == 'Error') {
            animationManager.setDirection(slideDir);
            $state.go('play.present.error', { workingDocumentId: wdid, nonce: nonce });
        }
        else if (r.State == 'Presenting') {
            $rootScope.bodyClass = 'player';
            animationManager.setDirection(slideDir);
            $state.go('play.present.presentable', { workingDocumentId: wdid, nonce: nonce });
        }
        else if (r.State == 'Finishing') {
            animationManager.setDirection(slideDir);
            $state.go('play.present.finishing', { workingDocumentId: wdid, nonce: nonce });
        }
        else if (r.State == 'Finished') {
            if (r.Presented) {
                animationManager.setDirection(slideDir);
                $state.go('play.present.presentable', { workingDocumentId: wdid, nonce: nonce });
            } else {
                animationManager.setDirection(slideDir);
                $state.go('play.present.finished', { workingDocumentId: wdid, nonce: nonce });
            }
        }
    };
}]);

//These two controllers drive checklist launch functionality and swap to present views

app.controller('beginCtrl', ['$stateParams', 'bworkflowApi', '$scope',
    function ($stateParams, bworkflowApi, $scope) {

        //our job, is to fire off an api request to beginCtrl, then transition to present
        bworkflowApi.beginChecklist($stateParams.groupId, $stateParams.resourceId, $stateParams.revieweeId, $stateParams.args, $stateParams.publishingGroupResourceId)
            .then(function (r) {
                $scope.transitionWithPlayerModel(r);
            }, function(ex) {
                $scope.transitionWithPlayerModel(ex.data); //error handle
        });
    }]);

//Drives the forward/next bar
app.factory('playerButtons', [function () {
    var subscribedCanFns = [];

    var svc = {
        nextText: 'Next',
        previousText: 'Back',
        canPrevious: false,
        canNext: false,
        areAjaxing: false,

        subscribeCan: function (fn, scope) {
            subscribedCanFns.push(fn);
            svc.update();
            var destroy = function () {
                var i = subscribedCanFns.indexOf(fn);
                if (i >= 0) {
                    subscribedCanFns.splice(i, 1);
                    svc.update();
                }
            };
            if (angular.isDefined(scope)) {
                scope.$on('$destroy', destroy);
            }
            return destroy;
        },

        update: function () {
            var can = {
                previous: true,
                next: true
            };

            angular.forEach(subscribedCanFns, function (fn) {
                fn(can);
            });

            svc.canPrevious = !!can.previous;
            svc.canNext = !!can.next;
        }
    };

    return svc;
}]);

app.factory('playerActions', [function () {
    return {
        doTransition: function () {
        }
    };
}]);

app.controller('presentRootCtrl', ['$scope', 'bworkflowApi', '$location', 'transitionCache', '$stateParams', '$rootScope', 'playerButtons', 'playerActions',
    function ($scope, bworkflowApi, $location, transitionCache, $stateParams, $rootScope, playerButtons, playerActions) {
        
        //If we land here directly (/continue), we won't have a player model
        //Fetch one and continue

        $scope.loading = false;
        if (!transitionCache.getItem($stateParams.workingDocumentId)) {
            $scope.loading = true;
            bworkflowApi.continueChecklist($stateParams.workingDocumentId)
                .then(function(pm) {
                    $scope.transitionWithPlayerModel(pm, 'none');
                }, function(ex) {
                    $scope.transitionWithPlayerModel(ex.data, 'none');
            });
        }

        //button controlls
        $scope.buttonStates = playerButtons;

        $scope.buttonActions = playerActions;

        $scope.models = {
            answerModel: {},
            playerModel: {}
        };

        $scope.$on('$stateChangeSuccess', function() {
            $scope.buttonStates.areAjaxing = false;
        });
    
        //This stuff effectively wires up browser back
        //to checklist back
        $scope.$on('$stateChangeStart', function (ev, toState, toParams, fromState, fromParams) {

            if (!$scope.buttonStates.areAjaxing) {
                //are we trying to hit forward or backwards?
                var prevNonce = $rootScope.nonceStack[$rootScope.nonceStack.length - 2];
                if (toParams.nonce == prevNonce && typeof prevNonce !== 'undefined') {
                    //we're going back
                    ev.preventDefault();
                    if ($scope.buttonStates.canPrevious) {
                        $rootScope.nonceStack.pop();
                        $rootScope.nonceStack.pop();
                        $scope.buttonActions.doTransition('prev', toParams.nonce);
                    }
                }
            }

        });
    }]);

app.controller('errorCtrl', [
    '$scope', 'transitionCache', 'bworkflowApi', '$state', '$q', '$stateParams',
    function ($scope, transitionCache, bworkflowApi, $state, $q, $stateParams) {
        $scope.models.playerModel = transitionCache.getAndDelete($stateParams.workingDocumentId);
        $scope.buttonStates.canPrevious = false;
        $scope.buttonStates.canNext = false;
        $scope.pm = $scope.models.playerModel;
    }
]);

app.controller('presentCtrl', ['$scope', '$stateParams', 'transitionCache', 'bworkflowApi', '$state', '$q', 'appUpdateMonitor',
    function ($scope, $stateParams, transitionCache, bworkflowApi, $state, $q, appUpdateMonitor) {
        appUpdateMonitor.addPauser($scope);      // Pause updates whilst our scope is alive

        $scope.models.playerModel = transitionCache.getAndDelete($stateParams.workingDocumentId);

        $scope.buttonStates.canPrevious = $scope.models.playerModel.State == 'Presenting' ? //allow back only if we're presenting and canPrevious()
                                            $scope.models.playerModel.CanPrevious : false;
        $scope.buttonStates.canNext = $scope.models.playerModel.State == 'Presenting'; //don't want to next() for finished presented

        if ($scope.models.playerModel.Presented) {
            //enumerate all the answerable nodes in the answerModel
            (function thisfunc(_, p) { //jquery each makes the actual value the second arg, so define the function this way
                if (p.IsAnswerable) {
                    $scope.models.answerModel[p.Id] = p.Answer || {};
                    p.Answer = $scope.models.answerModel[p.Id];
                }
                if (p.Children) {
                    $.each(p.Children, thisfunc);
                }
            })(undefined, $scope.models.playerModel.Presented);
        }

        $scope.$on('player.next', function (ev, args) {
            $scope.buttonActions.doTransition('next');
        });

        $scope.$on('player.back', function (ev, args) {
            $scope.buttonActions.doTransition('prev');
        });

        //Next/back button event handler- submit to server then transition to self
        //type is either 'next' or 'prev'
        $scope.buttonActions.doTransition = function (type, nonce) {
            // do any client side validation first
            if (type == 'next') {
                var data = { errors: [], stage: 'next' };
                $scope.$broadcast('validate', data);

                if (data.errors.length > 0) {
                    $scope.$broadcast('validationErrors', data);
                    return;
                }
            }

            $scope.buttonStates.areAjaxing = true; //turn on the AJAX loader spinny thing

            //start by broadcasting a next notification downwards
            //In the eventArgs will be a method that can be called to attach a promise to a list
            //We only actually do the server next method once all the promises have been resolved
            var promiseList = [];
            var attachPromise = function (promise) {
                promiseList.push(promise);
            };
            var ev = {
                attachPromise: attachPromise,
                type: type //either 'next' or 'prev'
            };
            //events are broadcast synchronously in angular:
            //https://groups.google.com/forum/#!msg/angular/yyH3FYAy5ZY/GVy0ckj29CIJ
            //Means that promiseList is populated after this line
            $scope.$broadcast('populateAnswer', ev);

            //only after the promises are resolved do we do anything else
            $q.all(promiseList)
                .then(function () {
                    if (type == 'next')
                        return bworkflowApi.nextChecklist($scope.models.playerModel.WorkingDocumentId,
                            $scope.models.answerModel);
                    else if (type == 'prev')
                        return bworkflowApi.prevChecklist($scope.models.playerModel.WorkingDocumentId);
                    else return undefined;
                })
                .then(function (r) {
                    $scope.buttonStates.areAjaxing = false; 
                    var slideDir = type == 'prev' ? 'left' : 'right';
                    $scope.transitionWithPlayerModel(r, slideDir, nonce);
                }, function(ex) {
                    $scope.buttonStates.areAjaxing = false; 
                    if (ex) {
                        var slideDir = type == 'prev' ? 'left' : 'right'; //error callback
                        $scope.transitionWithPlayerModel(ex.data, slideDir, nonce);
                    }
            });

        };
    }]);

app.factory('tabletProfile', ['persistantStorage', function (persistantStorage) {   
    return function (success) {
        return persistantStorage.onChange('TabletProfile', function (profile) {
            success(angular.isString(profile) ? JSON.parse(profile) : (profile || {}));
        });
    }
}]);

app.config(['ngToastProvider', function (ngToast) {
    ngToast.configure({
        verticalPosition: 'bottom',
        horizontalPosition: 'center',
        maxNumber: 3
    });
}]);

app.factory('autoLogoutSvc', ['tabletProfile', '$interval', '$rootScope', function (tabletProfile, $interval, $rootScope) {
    var _autoLogoutTimePromise = null;

    var svc = {
        autoLogoutTime: null,
        startAutoLogoutTimer: function (secs, override) {
            if (svc.autoLogoutTime == null || svc.autoLogoutTime < secs || override) {
                svc.autoLogoutTime = secs;
            }

            if (!_autoLogoutTimePromise) {
                _autoLogoutTimePromise = $interval(function () {
                    svc.autoLogoutTime = svc.autoLogoutTime - 1;

                    if (svc.autoLogoutTime <= 0) {
                        svc.logoutNow();
                    }
                }, 1000);
            }
        },
        logoutNow: function () {
            $rootScope.$emit('auto-logout.execute');
        },
        logoutShortSecs: 0,
        logoutLongSecs: 0,
        shortRestart: function (override) {
            if (svc.logoutShortSecs) {
                svc.startAutoLogoutTimer(svc.logoutShortSecs, override);
            }
        },
        longRestart: function (override) {
            if (svc.logoutLongSecs) {
                svc.startAutoLogoutTimer(svc.logoutLongSecs, override);
            }
        }
    };

    tabletProfile(function (profile) {
        svc.logoutLongSecs = profile.autologoutlongsecs;
        svc.logoutShortSecs = profile.autologoutshortsecs;

        svc.longRestart();
    });

    return svc;
}]);

app.directive('autoLogoutPanel', ['autoLogoutSvc', 'ngToast', '$sce', '$window', function (autoLogoutSvc, ngToast, $sce, $window) {
    return {
        templateUrl: 'auto-logout-panel.html',
        restrict: 'E',
        scope: {
        },
        controller: function ($scope) {
            $scope.svc = autoLogoutSvc;

            var _toast = null;
            $scope.$watch('svc.autoLogoutTime', function (newValue, oldValue) {
                if (newValue == null) {
                    return;
                }
                if (newValue == autoLogoutSvc.logoutShortSecs) {
                    $scope.popToast();
                } else if (newValue > autoLogoutSvc.logoutShortSecs) {
                    if (_toast) {
                        ngToast.dismiss(_toast);
                        _toast = null;
                    }
                }
            })

            $scope.userExtend = function () {
                autoLogoutSvc.longRestart(true);
            }

            $scope.logoutNow = function () {
                autoLogoutSvc.logoutNow();
            }

            $window.addEventListener('touchmove', function () {
                autoLogoutSvc.shortRestart(false);
            });

            $scope.popToast = function () {
                if (_toast) {
                    return;
                }

                _toast = ngToast.create({
                    content: $sce.trustAsHtml('<div class="ng-cloak" style="width: 50vw"><div class="row-fluid"><div class="span12">Log out in {{ svc.autoLogoutTime }}</div></div><div class="row-fluid"><div class="span6"><button class="btn btn-large" ng-click="userExtend()">Delay Logout</button></div><div class="span6"><button class="btn btn-large" ng-click="logoutNow()">Logout Now</button></div></div>'),
                    compileContent: $scope,
                    horizontalPosition: 'center',
                    dismissOnClick: false,
                    dismissOnTimeout: false
                });
            }
        }
    }
}]);

app.controller('dashboardCtrl', ['$scope', '$stateParams', '$state', 'bworkflowApi', 'transitionCache', '$compile', '$timeout', '$rootScope', 
    function ($scope, $stateParams, $state, bworkflowApi, transitionCache, $compile, $timeout, $rootScope) {
        $scope.loading = true;
        $scope.notLoading = false;

        $scope.introoptions = {};

        $scope.$on('player_broadcast', function (event, args) {
            if (args.__handled) {
                return;
            }
            // this event gets raised by questions when they want to notify other questions that something has changed.
            // typically this occurrs in a dashboard. We need to broadcast this back down the scopes to let others know
            // of the change. The event object created by who ever created the event should be of the form

            // args.name = name of event to broadcast out
            // args.data = object containing arguments relevant to the event
            $scope.$broadcast(args.name, args.data);
            args.__handled = true;
        });

        $scope.$on('player_broadcast_ajax_error', function (event, args) {
            // this allows code to handle the ajax errors themselves and then
            // to also broadcast it out and have it handled by the normal code as well.
            // An example of this is the odata feeds, which add the noUI flag to the object
            // so its logged on the server, but nothing is shown to the user
            $scope.$broadcast('ajax.error', args);
        });

        $scope.$on('tab_selected', function (event, args) {
            $scope.currenttab = args.tabcontent;
        });

        $scope.$on('help_start_current_tab', function (event, args) {
            if ($scope.currenttab != null) {
                $scope.currenttab.scope().$broadcast('help_start', {});
            }
            else {
                $scope.$broadcast('help_start', {});
            }
        });

        $scope.showDashboardTabs = function () {
            return $scope.computedDashboardModels.length > 1 && window.razordata.environment == 'desktop';
        };

        bworkflowApi.getDashboard()
            .then(function (dashboards) {
                $scope.dashboardModel = dashboards;
                $scope.answers = {};
                $scope.loading = false;
                $scope.notLoading = true;

                $rootScope.bodyClass = 'dashboard';
                
                //Make answer model like in player
                $scope.computedDashboardModels = $.map(dashboards.Dashboards, function(db) {
                    var am = {};
                    
                    //enumerate all the answerable nodes in the answerModel
                    (function thisfunc(_, p) { //jquery each makes the actual value the second arg, so define the function this way
                        if (p.IsAnswerable) {
                            am[p.Id] = p.Answer || {};
                            p.Answer = am[p.Id];
                        }
                        if (p.Children) {
                            $.each(p.Children, thisfunc);
                        }
                    })(undefined, db.Presented);

                    return {
                        PlayerModel: db,
                        AnswerModel: am
                    };
                });

                $scope.$emit('dashboards', { dashboards: $scope.computedDashboardModels });

                $rootScope.$emit('dashboard.show');
            });
    }]);

app.controller('finishingCtrl', ['$scope', '$stateParams', 'transitionCache', 'bworkflowApi',
    function ($scope, $stateParams, transitionCache, bworkflowApi) {
    $scope.models.playerModel = transitionCache.getAndDelete($stateParams.workingDocumentId);

    $scope.buttonStates.canPrevious = $scope.models.playerModel.CanPrevious;
    $scope.buttonStates.canNext = false;

    $scope.doFinish = function() {
        $scope.buttonStates.areAjaxing = true;
        bworkflowApi.nextChecklist($scope.models.playerModel.WorkingDocumentId, $scope.models.answerModel)
            .then(function(r) {
                $scope.transitionWithPlayerModel(r, 'right');
            }, function(ex) {
                $scope.transitionWithPlayerModel(ex.data, 'right');
        });
    };
    }]);

app.directive('helpIntercept', [function () {
    return {
        restrict: 'A',
        link: function (scope, elt, attrs) {
            // this will get called if we are in single dashboard mode, the help intercept will get it
            scope.$on('help_start', function (event, args) {
                // we need to call down to get the options we are going to show
                var steps = [];
                scope.$broadcast('help_add_steps', steps);

                var intro = introJs();

                intro.setOptions({steps: steps});

                intro.start();
            });
        }
    };
}]);
app.directive('helpLink', [function () {
    return {
        restrict: 'A',
        link: function (scope, elt, attrs) {
            elt.bind('click', function () {
                scope.$emit('help_start_current_tab', {});
            });
        }
    };
}]);

app.directive('playEmbedded', ['bworkflowApi', '$q', 'animationManager', '$timeout', 'appUpdateMonitor',
    function (bworkflowApi, $q, animationManager, $timeout, appUpdateMonitor) {
        return {
            templateUrl: 'play_embedded.html',
            restrict: 'E',
            require: 'ngModel',
            scope: {
                workingdocumentid: '=ngModel',
                exposeAnswerModel: '=',
                areAjaxing: '='
            },
            link: function (scope, elt, attrs, ngModel) {
                appUpdateMonitor.addPauser(scope);      // Pause updates whilst our scope is alive

                if (angular.isDefined(scope.exposeAnswerModel) == false) {
                    scope.exposeAnswerModel = false;
                }

                scope.models = [];
                scope.status = 'none';

                scope.show = function (r) {
                    switch (r.State) {
                        case 'Error':
                        case 0:
                            scope.showError(r);
                            break;
                        case 'Presenting':
                        case 1:
                            scope.showStep(r);
                            break;
                        case 'Finishing':
                        case 3:
                            scope.showFinishing(r);
                            break;
                        case 'Finished':
                            scope.showFinished(r);
                        case 4:
                            if (r.Presented) {
                                scope.showStep(r);
                            } else {
                                scope.showFinished(r);
                            }
                            break;
                    }
                };

                scope.showStep = function (r) {
                    // we need do a little work to get things into a format where the rest of the framework
                    // understands things. The following code is copied from a section of the code that KJ
                    // has put together.
                    var PlayerModel = r;
                    var AnswerModel = {};

                    if (r.Presented) {
                        //enumerate all the answerable nodes in the answerModel
                        (function thisfunc(_, p) { //jquery each makes the actual value the second arg, so define the function this way
                            if (p.IsAnswerable) {
                                AnswerModel[p.Id] = p.Answer || {};
                                p.Answer = AnswerModel[p.Id];
                                // the embedded player includes the question id, as it could be used for context information
                                // and the questionid in this case is the only thing that is fixed that allows us to re-hook up
                                // the answer model back to the checklist on the server
                                p.Answer.QuestionId = p.QuestionId;
                            }
                            if (p.Children) {
                                $.each(p.Children, thisfunc);
                            }
                        })(undefined, PlayerModel.Presented);

                        scope.$emit('embedded-player.showstep', { data: r });


                        scope.AnswerModel = AnswerModel;
                        scope.PlayerModel = PlayerModel;
                        var m = { PlayerModel: PlayerModel, AnswerModel: AnswerModel, show: true };

                        if (scope.exposeAnswerModel) {
                            scope.PlayerModel.Answers = AnswerModel;
                        }

                        scope.models.push(m);

                        scope.status = 'ready';
                    }
                };

                scope.showFinishing = function (r) {
                    var args = { show: true, data: r };

                    scope.$emit('embedded-player.finishing', args);

                    if (args.show == true) {
                        scope.status = 'finishing';
                    }
                };

                scope.showFinished = function (r) {
                    var args = { show: true, data: r };

                    scope.$emit('embedded-player.finished', args);

                    if (args.show == true) {
                        scope.status = 'finished';
                    }
                };

                scope.showError = function (r) {

                };

                scope.loadworkingdocument = function (id) {
                    scope.status = 'loading';

                    // ok we accept either the id of a working document, or a JSON representation of a PlayModel
                    // object that who ever we are embedded in has loaded and handed to us
                    if (angular.isDefined(id.WorkingDocumentId) == false) {
                        bworkflowApi.continueChecklist(id)
                            .then(function (r) {
                                scope.$emit('embedded-player.started', { data: r });

                                scope.show(r);
                            }, function (ex) {
                                scope.status = 'error';
                                // need to handle this
                            });
                    }
                    else {
                        // simple, we have what we need
                        scope.$emit('embedded-player.started', { data: id });
                        scope.show(id);
                    }
                }

                scope.$watch('workingdocumentid', function (newValue, oldValue) {
                    if (newValue == null) {
                        return;
                    }

                    scope.loadworkingdocument(newValue);
                });

                scope.collectAnswers = function (type) {
                    // this code has been copied and adapter from the main player

                    //start by broadcasting a next notification downwards
                    //In the eventArgs will be a method that can be called to attach a promise to a list
                    //We only actually do the server next method once all the promises have been resolved
                    var promiseList = [];
                    var attachPromise = function (promise) {
                        promiseList.push(promise);
                    };
                    var ev = {
                        attachPromise: attachPromise,
                        type: type //either 'next' or 'prev'
                    };
                    //events are broadcast synchronously in angular:
                    //https://groups.google.com/forum/#!msg/angular/yyH3FYAy5ZY/GVy0ckj29CIJ
                    //Means that promiseList is populated after this line
                    scope.$broadcast('populateAnswer', ev);

                    return promiseList;
                }

                scope.navigateChecklist = function (type, args) {
                    var promiseList = scope.collectAnswers(type);

                    var allowOffline = false;
                    if (args) {
                        if (angular.isDefined(args.allowOffline)) {
                            allowOffline = args.allowOffline;
                        }
                    }

                    //only after the promises are resolved do we do anything else
                    $q.all(promiseList)
                        .then(function () {
                            if (type == 'next') {
                                scope.areAjaxing = true;
                                var p = bworkflowApi.nextChecklist(scope.workingdocumentid,
                                    scope.AnswerModel, allowOffline);

                                if (angular.isDefined(args.afterNextCallback) == true) {
                                    args.afterNextCallback();
                                }

                                return p;
                            }
                            else if (type == 'prev')
                            {
                                scope.areAjaxing = true;
                                return bworkflowApi.prevChecklist(scope.workingdocumentid, allowOffline);
                            }
                            else return undefined;
                        })
                        .then(function (r) {
                            scope.areAjaxing = false;
                            scope.show(r);
                        }, function (ex) {

                        });
                };

                scope.saveChecklist = function () {
                    var promiseList = scope.collectAnswers('next');

                    return bworkflowApi.saveChecklist(scope.workingdocumentid, scope.AnswerModel);
                }

                scope.finish = function (args) {
                    scope.navigateChecklist('next', args);
                };

                scope.cleanModels = function () {
                    if (scope.models.length <= 2) {
                        return;
                    }

                    // the last 2 items in the models array are the most recent
                    // so could be animating, we don't touch these, but remove everthing
                    // else to avoid clutter
                    scope.models.splice(0, scope.models.length - 2);
                };

                scope.validate = function (args) {
                    var data = { errors: [], stage: 'next' };
                    scope.$broadcast('validate', data);

                    if (angular.isDefined(args.validationCallback) == true) {
                        args.validationCallback(data.errors.length == 0);
                    }

                    return data.errors.length == 0;
                };

                scope.$on('embedded-player.next', function (event, args) {
                    if (scope.validate(args) == false) {
                        return;
                    }

                    scope.action = 'next';

                    $timeout(function () {
                        if (scope.models.length > 0) {
                            scope.models[scope.models.length - 1].show = false;

                            scope.cleanModels();
                        }

                        scope.navigateChecklist('next', args);
                    });
                });

                scope.$on('embedded-player.back', function (event, args) {
                    scope.action = 'prev';

                    $timeout(function () {
                        if (scope.models.length > 0) {
                            scope.models[scope.models.length - 1].show = false;

                            scope.cleanModels();
                        }

                        scope.navigateChecklist('prev', args);
                    });
                });

                scope.$on('embedded-player.save', function (event, args) {
                    scope.saveChecklist();
                });
            }
        };
    }
]);

app.directive('errorManager', ['bworkflowApi', '$q', '$interval', 'RequestsErrorHandler', function (bworkflowApi, $q, $interval, RequestsErrorHandler) {
    return {
        templateUrl: 'error_manager.html',
        restrict: 'E',
        scope: {},
        link: function (scope, elt, attrs) {
            scope.errors = [];
            scope.notLoggedErrors = [];
            scope.isAttemptingToLog = false;
            scope.attemptCountDown = 1;
            scope.showFor = 10; // number of seconds to show error message for
            scope.showCountDown = 0;
            scope.currentError = null;
            scope.showingDetails = false;
            scope.isInScriptError = false;

            scope.$on('ajax.error', function (event, args) {
                // We are going to ignore "status 0" which basically means "no internet" or "connection refused"
                if (args.status == 0) {
                    return;
                }
                var log = {
                    type: {
                        type: 'ajax',
                        source: 'player.js',
                        message: args.status + ' ' + args.statusText,
                        stack: {
                            method: args.config.method,
                            url: args.config.url,
                            headers: args.config.headers,
                            stack: args.config.stack
                        }
                    },
                    data: {
                        request: args.config.params,
                        response: args.data
                    },
                    recorded: false,
                    recordAttempts: 0
                };
                scope.populateClientDetails(log.data);

                scope.errors.push(log);

                ////if (args.clientDetails.online == false)
                ////{
                ////    // the error is from things being offline, we won't worry about notifuying the user as other UI does that in this case
                ////    return;
                ////}

                if (angular.isDefined(args.noUI) == false || args.noUI == false)
                {
                    // no instructions to the contray, so show our error UI
                    scope.showCountDown = scope.showFor;
                    scope.currentError = log;
                }

                scope.notLoggedErrors.push(log);
            });

            scope.$on('script.error', function (event, args) {
                if (scope.isInScriptError == true)
                {
                    // prevent a stack overflow if something goes wrong in us
                    return;
                }

                scope.isInScriptError = true;

                var toLog =
                {
                    type: {
                        type: 'javascript',
                        source: args.cause,
                        message: args.exception.message,
                        stack: {
                            stack: args.exception.stack
                        }
                    },
                    data: {
                    },
                    recorded: false,
                    recordAttempts: 0
                };

                scope.populateClientDetails(toLog.data);

                scope.errors.push(toLog);

                scope.showCountDown = scope.showFor;
                scope.currentError = toLog;

                scope.notLoggedErrors.push(toLog);

                scope.isInScriptError = false;
            });

            scope.populateClientDetails = function (args) {
                var location = bworkflowApi.currentLocation();

                var clientInfo = {
                    location: location,
                    date: moment().format('DD-MM-YYYY hh:mm:ss A'),
                    online: false
                };

                args.clientDetails = clientInfo;
            };

            scope.dismissCurrentError = function () {
                scope.currentError = null;
            };

            scope.showDetails = function () {
                scope.showingDetails = true;
            };

            scope.closeDetails = function () {
                scope.showingDetails = false;
            };

            scope.attemptToLog = function () {
                // manage wether or not we are showing our message alert
                if (scope.showCountDown > 0)
                {
                    scope.showCountDown = scope.showCountDown - 1;
                }

                if (scope.notLoggedErrors.length == 0) {
                    return;
                }

                if (scope.isAttemptingToLog == true)
                {
                    return;
                }

                scope.attemptCountDown = scope.attemptCountDown - 1;
                if (scope.attemptCountDown < 0)
                {
                    scope.attemptCountDown = 0;
                }

                if (scope.attemptCountDown != 0)
                {
                    return;
                }

                scope.isAttemptingToLog = true;

                var obj = scope.notLoggedErrors[0];

                // we don't want errors attempting log things adding to us
                // so signal we will handle things
                RequestsErrorHandler.specificallyHandled(
                    function () {
                        $q.all({ logError: bworkflowApi.logError(obj) }).then(
                            function () {
                                obj.recorded = true; // flag it as recorded

                                // remove from the not logged array
                                var index = scope.notLoggedErrors.indexOf(obj);
                                if (index != -1)
                                    scope.notLoggedErrors.splice(index, 1);

                                scope.isAttemptingToLog = false;
                                scope.attemptCountDown = 1;
                            },
                            function () {
                                obj.recordAttempts = obj.recordAttempts + 1;
                                scope.attemptCountDown = 2 * obj.recordAttempts;

                                if (scope.attemptCountDown > 30)
                                {
                                    scope.attemptCountDown = 30; // cap the time so it doesn't get crazy
                                }

                                // I've removed offline.js from the solution as it was
                                // getting in the way for the offline implementation.
                                // If we need some sort of notification, we should be able
                                // to handle this ourselves from here
                                //if (scope.showCountDown == 0)
                                //{
                                //    // hmm, we've failed to send the report to the server
                                //    // and we aren't showing our UI, let's give offlinejs
                                //    // an opportunity to let the user know what is going on
                                //    Offline.check();
                                //}

                                scope.isAttemptingToLog = false;
                            }
                        );
                    }
                );
            };

            scope.startErrorLogQueue = function () {
                if (angular.isDefined(scope.stop)) return;

                scope.stop = $interval(function () {
                    scope.attemptToLog();
                }, 1000);
            };

            scope.stopErrorLogQueue = function () {
                if (angular.isDefined(scope.stop)) {
                    $interval.cancel(scope.stop);
                    scope.stop = undefined;
                }
            };

            scope.$on('$destroy', function () {
                // Make sure that the interval is destroyed too
                scope.stopErrorLogQueue();
            });

            bworkflowApi.watchGPSLocation();
            scope.startErrorLogQueue();
        }
    };
}]);

// simple filter to format a duration (in seconds) in a human readable way
app.filter('duration', [function () {
    return function (seconds, format) {
        var s = seconds;
        if (typeof seconds == 'string')
        {
            s = parseFloat(s);
        }

        var f = 'full';

        if (angular.isDefined(format))
        {
            f = format.toLowerCase();
        }

        var dur = moment.duration(s, "seconds");

        switch(f)
        {
            case 'ashours':
                return dur.asHours() + ' hrs';
                break;
            case 'asmins':
                return dur.asMinutes() + ' mins';
            case 'asminutes':
                break;
            case 'assecs':
                return dur.asSeconds() + ' secs';
            case 'asseconds':
                break;
            case 'full':
                return dur.hours() + ' hrs ' + dur.minutes() + ' mins ' + dur.seconds() + ' secs';
                break;
        }
    };
}]);

// simple filter to total the values of a property in an array. So for example
// if you have the array somearray = [{num:10},{num:20},{num:30}] and you did
// somearray | total:'num' you'd get the sum of the values in num (60).
app.filter('total', function () {
    return function (input, property) {
        var i = input instanceof Array ? input.length : 0;
        if (typeof property === 'undefined' || i === 0) {
            return i;
        } else if (isNaN(input[0][property])) {
            throw 'filter total can count only numeric values';
        } else {
            var total = 0;
            while (i--)
                total += input[i][property];
            return total;
        }
    };
});

app.factory('beaconListSvc', ['$injector', function ($injector) {

    function beaconAsset(asset) {
        if (!asset) {
            return asset;
        }
        if (angular.isDefined(asset.beacon)) {
            return asset;
        }

        var assetId = asset.alldata.Id;
        if (angular.isDefined(assetId)) {
            var beaconId = asset.alldata.BeaconId;
            if (angular.isDefined(beaconId)) {
                var beacon = svc.beacons.find(function (b) {
                    return b.beaconId == beaconId;
                });

                asset.hasBeacon = angular.isDefined(beacon);
                asset.beacon = function () {
                    return beacon;
                }
            }
        }
        return asset;
    }

    var svc = {
        beaconAsset: beaconAsset
    };

    if (window.razordata.environment == 'mobile') {
        var beaconSvc = $injector.get('beaconSvc');
        svc.beacons = beaconSvc.getBeacons();
    } else {
        svc.beacons = [];
        svc.notSupported = true;
    }

    return svc;
}]);

app.controller('beaconListCtrl', ['$scope', 'beaconListSvc', function ($scope, beaconListSvc) {
    $scope.beacons = beaconListSvc.beacons;
    $scope.notSupported = beaconListSvc.notSupported;
    $scope.beaconAsset = beaconListSvc.beaconAsset;
}])

app.factory('geolocation-registry', [function () {
    var _byname = {};
    var _byindex = [];

    return {
        byname: _byname,
        byindex: _byindex,

        add: function (id, type, name, position) {
            var source = _byname[id];
            if (!source) {
                source = {
                    id: id,
                    type: type,
                    name: name,
                    position: position
                }
                _byname[id] = source;
                _byindex.push(source);
            }
            return source;
        }
    }
}]);

app.directive('compile', ['$compile', function ($compile) {
    return function (scope, element, attrs) {
        scope.$watch(
            function (scope) {
                // watch the 'compile' expression for changes
                return scope.$eval(attrs.compile);
            },
            function (value) {
                // when the 'compile' expression changes
                // assign it into the current DOM
                element.html(value);

                // compile the new DOM and link it to the current scope.
                $compile(element.contents())(scope);
            }
        );
    };
}]);


app.factory('jsFunctionLineItemPrice', [function () {
    return {
        calculate: function (item) {
            return item.quantity * item.price * (1.0 - item.discount + item.surcharge);
        },

        // function to allow quantity based price ranges, eg
        // quantitySlide(110, [
        //    [[0, 9], 1.2],          // 0..9 items @ $1.2
        //    [[10, 49], 1.1],        // 10..49 items @ $1.1
        //    [[50, 99], 1.0],        // 50..99 items @ $1.0
        //    [[100], 0.95])          // 100.. items @ $0.95
        //
        // quantitySlide(110, [
        //    [[0, 9], [1.2, '0..9'],          // 0..9 items @ $1.2
        //    [[10, 49], [1.1, '10..49'],        // 10..49 items @ $1.1
        //    [[50, 99], [1.0, '50..99'],        // 50..99 items @ $1.0
        //    [[100], [0.95, '100+'],          // 100.. items @ $0.95
        //    notes);
        // 
        quantitySlide: function (quantity, range, notes) {
            if (angular.isObject(quantity) && quantity.hasOwnProperty('quantity')) {
                quantity = quantity.quantity;
            }

            var price = 0;
            var q = quantity;
            var lastHigh = 0;
            var highestRange;
            range.forEach(function (r) {
                var lowHigh = r[0];
                var p = angular.isNumber(r[1]) ? r[1] : r[1][0];

                if (q >= lowHigh[0]) {
                    highestRange = r;
                    if (lowHigh.length == 2 && q > lowHigh[1]) {
                        price += (lowHigh[1] - lastHigh) * p;
                    } else {
                        price += (q - lastHigh) * p;
                    }
                    price = Math.round(price * 100) / 100.0;
                }
                if (lowHigh.length >= 2) {
                    lastHigh = lowHigh[1];
                }
            });

            if (angular.isArray(notes) && angular.isDefined(highestRange) && angular.isArray(highestRange[1]) && highestRange[1].length >= 2) {
                notes.push(highestRange[1][1]);
            }

            return price;
        }
    };
}]);

app.factory('jsFunctionSvc', ['$http', 'sectionUtils', '$injector', function ($http, sectionUtils, $injector) {
    var svc = {
        getScript: function () {
            return $http({ url: window.razordata.siteprefix + 'api/v1/JsFunction/GetScript' }).then(function (response) {
                var fns = Object.create(null);
                fns.byId = Object.create(null);

                response.data.functions.forEach(function (d) {
                    var fn = eval('(' + d.script + ')');
                    var invokeableFn = function (locals) {
                        locals['functionId'] = d.id;
                        locals['js' + d.type] = fns[d.type];        // Allow a function to DI inject another by name of same type (eg LineItemPrice)
                        locals['jsFunctions'] = fns;                // Allow a function to DI inject all functions (allows utility functions to be defined)
                        return $injector.invoke(fn, $injector.get('jsFunction' + d.type), locals);
                    };

                    // Function is callable by fns.byId[3]
                    fns.byId[d.id] = invokeableFn;

                    // Function is also callable by fns.LineItemPrice["50% Off"]
                    fns[d.type] = fns[d.type] || Object.create(null);
                    fns[d.type][d.name] = invokeableFn;
                });
                return {
                    functions: fns,
                    utcnow: moment(response.data.utcnow).utc()
                };
            });
        }
    };
    return svc;
}]);





