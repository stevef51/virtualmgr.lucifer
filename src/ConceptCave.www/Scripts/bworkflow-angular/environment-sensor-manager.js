﻿angular.module('environment-sensor-manager-module', ['vmngUtils', 'bootstrapCompat'])

    .factory('environment-sensing-probe', ['$q', function ($q) {
        function ProbeSensor(probe, index, type) {
            this.probe = probe;
            this.index = index;
            this.type = type;
            this.id = probe.id + ':' + type.toString() + '.' + index.toString();
            this.changeCount = this.updateCount = 0;
            this.updateValueSubscribers = [];
        }

        // Sensors call updateValue to publish their latest readings
        // Some hardware sensors will broadcast their readings more frequently than they actually measure (eg BlueMaestro Tempo discs)
        // the act of hearing a sensor reading means the sensor is in range (for wireless sensors), when 'readingIsNew' is true
        // it also means the sensor's value is new, it may be the same reading as the last one but it has been freshly read
        ProbeSensor.prototype.updateValue = function (value, readingIsNew) {
            this.updateCount++;                         // Counts how often we hear from sensor
            this.updateTimestamp = moment.utc();              // Keep track of last time we 'heard' from the sensor
            this.lastValue = this.value;

            if (this.value != value) {
                this.value = value;
                this.changeCount++;                     // Track how often the value changes
            }

            if (readingIsNew) {
                this.readingTimestamp = moment.utc();          // Keep track of last time the sensor 'took a measurement'
            }

            angular.forEach(this.updateValueSubscribers, function (callback) {
                callback(this);
            })
        }

        ProbeSensor.prototype.subscribeUpdateValue = function (callback) {
            var self = this;
            this.updateValueSubscribers.push(callback);
            callback(this);
            return function () {
                var i = self.updateValueSubscribers.indexOf(callback);
                if (i >= 0) {
                    self.updateValueSubscribers.splice(i, 1);
                }
            }
        }

        function Probe(id, model, serialNumber) {
            this.id = id;
            this.model = model;
            this.serialNumber = serialNumber;
            this.sensors = [];
            this.sensorsByIndex = Object.create(null);
            this.sensorsByType = Object.create(null);
            this.displaySensors = [];
        }

        Probe.prototype.getSensor = function (sensorIndex, sensorType) {
            var sensor = this.sensorsByIndex[sensorIndex];
            if (angular.isUndefined(sensor)) {
                sensor = new ProbeSensor(this, sensorIndex, sensorType);
                this.sensors.push(sensor);
                this.sensorsByIndex[sensorIndex] = sensor;
                this.sensorsByType[sensorType] = sensor;
                switch (sensorType) {
                    case 0:
                        sensor.display = function () {
                            return sensor.value.toString();
                        }
                        break;
                    case 1:
                        sensor.display = function () {
                            return sensor.value.toString() + '\xB0' + 'C';
                        }
                        this.displaySensors.push(sensor);
                        break;
                    case 2:
                        sensor.display = function () {
                            return sensor.value.toString() + '%';
                        }
                        break;
                    case 3:
                        sensor.display = function () {
                            return sensor.value.toString() + 'hPa';
                        }
                        break;
                    case 4:
                        sensor.display = function () {
                            return sensor.value.toString() + '\xB0' + 'C';
                        }
                        break;
                    case 5:
                        sensor.display = function () {
                            return sensor.value.toString() + '%';
                        }
                        this.displaySensors.push(sensor);
                        break;
                }
            }
            return sensor;
        }

        Probe.prototype.sensorTypes = Object.freeze({
            ByName: {
                Unknown: 0,
                Temperature: 1,
                Humidity: 2,
                BarometricPressure: 3,
                DewPoint: 4,
                Battery: 5
            }, ById: {
                0: 'Unknown',
                1: 'Temperature',
                2: 'Humidity',
                3: 'Barometric pressure',
                4: 'Dew point',
                5: 'Battery'
            }
        });
        Probe.sensorTypes = Probe.prototype.sensorTypes;

        return Probe;
    }])

    .factory('environment-sensor-manager', ['$timeout', function ($timeout) {
        function EnvironmentSensorManager() {
            this.probes = [];
            this.probesById = Object.create(null);

            this.providers = [];
            this.providersById = Object.create(null);

            this.scanCount = 0;
        }

        EnvironmentSensorManager.prototype.getProbes = function () {
            return this.probes;
        }

        EnvironmentSensorManager.prototype.addProvider = function (provider) {
            this.providers.push(provider);
            this.providersById[provider.id] = provider;
        }

        EnvironmentSensorManager.prototype.startScanning = function (callback) {
            var self = this;
            var sensorChangesById = Object.create(null);
            angular.forEach(this.providers, function (provider) {
                provider.startScanning(function (probes) {
                    $timeout(function () {
                        var changedSensors = [];
                        angular.forEach(probes, function (probe) {
                            var existing = self.probesById[probe.id];
                            if (angular.isUndefined(existing)) {
                                self.probes.push(probe);
                                self.probesById[probe.id] = probe;
                            }
                            angular.forEach(probe.sensors, function (sensor) {
                                var lastChange = sensorChangesById[sensor.id];
                                if (angular.isUndefined(lastChange) || lastChange != sensor.changeCount) {
                                    sensorChangesById[sensor.id] = sensor.changeCount;
                                    changedSensors.push(sensor);
                                    self.scanCount++;
                                }
                            })
                        })

                        if (changedSensors.length && callback) {
                            callback(changedSensors);
                        }
                    });
                })
            })

            return function () {
                angular.forEach(self.providers, function (provider) {
                    provider.stopScanning();
                });
            }
        }

        return new EnvironmentSensorManager();
    }])

    .controller('environment-sensor-test', ['$scope', 'environment-sensor-manager', function ($scope, manager) {
        $scope.manager = manager;

        manager.startScanning();
    }])

