﻿/*

UUID: 86F4E91D-07AC-47CC-916B-69C8789635D3
data: current temperature, max temperature, temperature unit, battery level, bluetooth MAC address.
note:
1. data is sent in the form of string. such as "25.1,78.9,F,0,AA:BB:CC:DD:EE:FF".
2. temperature unit: 'F' & 'C'.
3. battery level: '0' - normal, '1' - low voltage.
4. machine sends these data right now after APP sends the string "RD".

UUID: 131F59B3-75DA-45BC-BAAC-BC0A698B6371
data: characristic 'C' & 'F'.
note:
1. APP sends 'C' or 'F' to change the temperature unit of the machine.

UUID: C7A7D705-F876-47F9-9EBC-EE6D0F28F18D
data: "ota"
note:
1. APP sends the string "ota" to put the machine into OTA mode.

UUID: 6699A41C-FC5B-4277-AA1B-9DBDB7347013
data: "CR"
note:
1. APP sends the string "CR" to clear the max temperature value.

UUID: A83FBD52-CBCF-46FC-AAB8-AA24F1B6D77F
data: "PD", "PO", "RD"
note:
1. APP sends the string "PD" to put the machine into shutdown mode(bluetooth is still working).
2. APP sends the string "PO" to put the machine into power on mode(display temperature data).
3. After the APP sends the string "RD", the machine immediately sends the status (power on or shutdown).

UUID: DE2F3D91-0673-4424-8625-2AD7217B393E
data: Interval time, in the form of a string, such as "10" means that data is sent once every 10 seconds and received by the machine in a range of 1 second to 9999 seconds.

*/

angular.module('bluetherm-dishtemp-blue-module', ['vmngUtils', 'bootstrapCompat'])

    .factory('bluetherm-dishtemp-blue-provider', ['$q', 'ngTimeout', '$log', 'actionQueue', '$timeout', 'binUtils', 'temperatureConvert', 'semver', function ($q, ngTimeout, $log, actionQueue, $timeout, binUtils, temperatureConvert, semver) {
        if (angular.isUndefined(window.cordova) ||
            angular.isUndefined(cordova.plugins) ||
            angular.isUndefined(cordova.plugins.VirtualManagerBLE) ||
            angular.isUndefined(cordova.plugins.VirtualManagerBLE.Client) ||
            angular.isUndefined(cordova.plugins.VirtualManagerBLE.getVersion)) {
            return {
                isSupported: function () {
                    return $q.resolve(false);
                },

                supported: false,
                notsupported: true
            };
        }

        var _plugin = cordova.plugins.VirtualManagerBLE;
        var _bts = new _plugin.Client('dishtemp-blue', { deleteExistingClient: true, keepPeripherals: true });
        var base64 = cordova.require('cordova/base64');

        // On start-up Stop Native code from scanning - it is possible that page refresh/logout etc could leave Native
        // code scanning in background when new page JS has not requested scan, Cordova will throw away unrecognised Native callbacks
        // but still leaves high CPU usage for no gain
        _bts.stopScanning();

        var PrimaryServiceUUID = '71712A7E-BC95-4E65-A522-EA125BA4AC47';
        var ReadingCharacteristicUUID = '86F4E91D-07AC-47CC-916B-69C8789635D3';
        var UnitsCharacteristicUUID = '131F59B3-75DA-45BC-BAAC-BC0A698B6371';
        var OTAModeCharacteristicUUID = 'C7A7D705-F876-47F9-9EBC-EE6D0F28F18D';
        var ClearValuesCharacteristicUUID = '6699A41C-FC5B-4277-AA1B-9DBDB7347013';
        var PowerCharacteristicUUID = 'A83FBD52-CBCF-46FC-AAB8-AA24F1B6D77F';
        var IntervalCharacteristicUUID = 'DE2F3D91-0673-4424-8625-2AD7217B393E';

        $log.info('bluetherm-dishtemp-blue-provider starting up ..');

        function DishTempProbe(p) {
            this.peripheral = p;
            this.id = p.id;
            this.serialNumber = binUtils.bytesToHex(p.advertisement.data, 2, 8);
            this.getSerialNumber = () => $q.resolve(this.serialNumber);
            this.canCalibrate = false;
        }

        DishTempProbe.prototype.processReadData = function (data) {
            var text = binUtils.UTF8String(data, 0, data.length);
            if (text) {
                var parts = text.split(',');
                if (parts.length == 5) {
                    var current = Number(parts[0]);
                    var max = Number(parts[1]);
                    var celsiusNotFahrenheit = parts[2] === 'C';
                    var batteryPercent = parts[3] === '1' ? 0.1 : 1.0;
                    var macAddress = parts[4];
                    var now = moment.utc();
                    if (this.probe) {
                        this.probe.updated({
                            temperature: {
                                rawValue: celsiusNotFahrenheit ? current : temperatureConvert.fahrenheit.toCelsius(current),
                                value: celsiusNotFahrenheit ? current : temperatureConvert.fahrenheit.toCelsius(current),
                                updateTimestamp: now,
                                readingTimestamp: now,
                            },
                            maxTemperature: {
                                rawValue: celsiusNotFahrenheit ? max : temperatureConvert.fahrenheit.toCelsius(max),
                                value: celsiusNotFahrenheit ? max : temperatureConvert.fahrenheit.toCelsius(max),
                                updateTimestamp: now,
                                readingTimestamp: now
                            },
                            battery: {
                                percent: batteryPercent
                            }
                        });
                    }
                }
            }
        }

        DishTempProbe.prototype.connectCancel = function () {
            return this.disconnect();
        }

        DishTempProbe.prototype.connect = function () {
            var self = this;
            var deferred = $q.defer();

            var resolve = function () {
                if (p.connected) {
                    deferred.resolve(true);
                    self.probe.connected();
                } else {
                    deferred.resolve(false);
                    self.probe.disconnected();
                }
            }
            var reject = function () {
                deferred.reject();
                if (p.connected) {
                    self.probe.connected();
                } else {
                    self.probe.disconnected();
                }
            }
            var p = self.peripheral;
            if (p.connected) {
                resolve();
            } else {

                self.characteristics = Object.create(null);
                p.connect(function (msg) {
                    // Connect & spurious Disconnect come through here
                    if (!p.connected) {
                        self.probe.remove();
                        resolve();
                    } else {
                        p.discoverServices([], function (services) {
                            var primaryService = p.services[PrimaryServiceUUID];
                            if (primaryService) {
                                primaryService.discoverCharacteristics([], function () {
                                    self.characteristics.reading = primaryService.characteristics[ReadingCharacteristicUUID];
                                    self.characteristics.units = primaryService.characteristics[UnitsCharacteristicUUID];
                                    self.characteristics.OtaMode = primaryService.characteristics[OTAModeCharacteristicUUID];
                                    self.characteristics.clearValues = primaryService.characteristics[ClearValuesCharacteristicUUID];
                                    self.characteristics.power = primaryService.characteristics[PowerCharacteristicUUID];
                                    self.characteristics.interval = primaryService.characteristics[IntervalCharacteristicUUID];

                                    // these devices emit their data on the 'reading' characteristic periodically, so hook a read event up now
                                    self.characteristics.reading.subscribeRead(self.processReadData.bind(self));

                                    resolve();
                                });
                            }
                        });
                    }
                }, reject);
                self.probe.connecting();
            }
            return deferred.promise;
        }

        DishTempProbe.prototype.disconnect = function () {
            var self = this;
            var deferred = $q.defer();
            this.peripheral.disconnect(function () {
                deferred.resolve();
                self.probe.disconnected();
            }, deferred.reject);
            return deferred.promise;
        }

        function checkCharacteristic(self, characteristic) {
            var deferred = $q.defer();
            if (!self.peripheral.connected) {
                deferred.reject('Not connected');
            } else {
                if (!self.characteristics[characteristic]) {
                    deferred.reject(characteristic + ' characteristic not discovered');
                }
            }
            deferred.resolve();
            return deferred.promise;
        }
        
        function writeCharacteristic(characteristic, transform) {
            transform = transform || angular.identity;
            return function (data) {
                var self = this;
                return checkCharacteristic(self, characteristic).then(() => {
                    var deferred = $q.defer();
                    var utf8Data = binUtils.UTF8Array(transform(data));
                    if (angular.isDefined(utf8Data)) {
                        self.characteristics[characteristic].write(utf8Data, deferred.resolve, deferred.reject);
                    } else {
                        deferred.resolve();
                    }
                    return deferred.promise;
                });
            }
        }

        DishTempProbe.prototype.measure = writeCharacteristic('reading', () => 'RD');
        DishTempProbe.prototype.setUnits = writeCharacteristic('units', celcius => !!celcius ? 'C' : 'F');
        DishTempProbe.prototype.configure = writeCharacteristic('interval', options => {
            if (options.measurementMilliseconds != null) {
                var seconds = Math.floor((Number(options.measurementMilliseconds) + 1000 - 1) / 1000);      // round up to nearest second
                if (!seconds || seconds > 9999) seconds = 9999;         // Never is not an option, 9999 closest
                if (seconds < 1) seconds = 1;
                return seconds.toString();
            }
        });
        DishTempProbe.prototype.clearMaxTemp = writeCharacteristic('clearValues', () => 'CR');
        DishTempProbe.prototype.powerOn = writeCharacteristic('power', () => 'PO');
        DishTempProbe.prototype.shutDown = writeCharacteristic('power', () => 'PD');

        var $poweredOn = $q.defer();
        _bts.subscribeStateChange(function (state) {
            $log.info('subscribeStateChange = ' + state);
            if (state == 'PoweredOn') {
                $poweredOn.resolve();
            }
        });

        function DishTempBluePlugin() {
            this.id = 'DishTempBlue';
            this.scanState = 'Idle';
        }

        DishTempBluePlugin.prototype.isSupported = function () {
            var deferred = $q.defer();
            _plugin.getVersion(function (details) {
                var thisSemver = semver(details.version);
                deferred.resolve(thisSemver.isValid && thisSemver.isCompatibleOrLater('1.5'));
            });
            return deferred.promise;
        }

        DishTempBluePlugin.prototype.initialise = function (manager) {
            this.manager = manager;
            this.manager.registerProvider(this);
        }

        DishTempBluePlugin.prototype.startup = function () {
            if (this.$disconnectTimeout) {
                $timeout.cancel(this.$disconnectTimeout);
                delete this.$disconnectTimeout;
            }
        }

        DishTempBluePlugin.prototype.shutdown = function (disconnectTimeout) {
            var self = this;
            self.$disconnectTimeout = $timeout(function () {
                var probes = self.manager.providerProbes(self);
                probes.forEach(function (probe) {
                    if (probe.isConnected()) {
                        probe.disconnect();
                    }
                });
            }, disconnectTimeout);
        }

        DishTempBluePlugin.prototype.startScan = function (timeout) {
            var deferred = $q.defer();
            var self = this;
            $poweredOn.promise.then(function () {
                // Remove all Disconnected probes that we have found in the past, we are going to rescan for them ..
                var removeIfNotScanned = self.manager.providerProbes(self);

                if (timeout) {
                    $timeout(function () {
                        removeIfNotScanned.forEach(function (stale) {
                            if (!stale.isConnected()) {
                                stale.remove();
                            }
                        });
                        _bts.stopScanning();
                        deferred.resolve();
                    }, timeout);
                } else {
                    deferred.resolve();
                }

                _bts.startScanning([], { allowDuplicate: true, groupTimeout: 0, groupSize: 0 }, function (peripherals) {
                    self.scanState = 'Scanning';

                    if (peripherals != null) {
                        var blacklistIds = [];
                        angular.forEach(peripherals, function (p) {
                            p.timestamp = moment();
                            var findProbe = self.manager.findProbe(p.id);
                            if (angular.isUndefined(findProbe)) {
                                if ((p.name === "TMW012BT") && p.advertisement && p.advertisement.connectable) {
                                    if (typeof p.advertisement.data === 'string') {
                                        p.advertisement.data = new Uint8Array(base64.toArrayBuffer(p.advertisement.data));
                                    }
                                    var driver = new DishTempProbe(p);
                                    driver.probe = self.manager.addProbe(self, driver.id, driver.serialNumber, driver);
                                } else {
                                    blacklistIds.push(p);
                                }
                            }
                            var i = removeIfNotScanned.findIndex(function (existing) { return existing.id == p.id; });
                            if (i >= 0) {
                                removeIfNotScanned.splice(i, 1);
                            }
                        });
                        if (blacklistIds.length && angular.isFunction(_bts.blacklist)) {
                            _bts.blacklist(blacklistIds);
                        }
                    }

                }, function (err) {
                    deferred.reject(err);
                });
            });
            return deferred.promise;
        },

        DishTempBluePlugin.prototype.stopScan = function () {
            var deferred = $q.defer();
            _bts.stopScanning(function () {
                self.scanState = 'Idle';
                deferred.resolve();
            }, function (msg) {
                self.scanState = 'Idle';
                deferred.reject(msg);
            });
            return deferred.promise;
        }

        return new DishTempBluePlugin();
    }])

    .run(['temperature-probe-manager', 'bluetherm-dishtemp-blue-provider', function (manager, provider) {
        provider.isSupported().then(function (supported) {
            if (supported) {
                provider.initialise(manager);
            }
        });
    }])

