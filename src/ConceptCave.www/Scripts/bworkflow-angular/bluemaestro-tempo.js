﻿angular.module('bluemaestro-tempo-module', ['vmngUtils', 'bootstrapCompat', 'environment-sensor-manager-module'])

    .factory('bluemaestro-tempo-ble-svc', ['$log', 'ngTimeout', 'actionQueue', 'GATT', 'binUtils', 'environment-sensing-probe', '$q', function ($log, ngTimeout, actionQueue, GATT, binUtils, ESProbe, $q) {
        var _bts = new cordova.plugins.VirtualManagerBLE.Client('bluemaestro-tempo', { deleteExistingClient: true });
        var base64 = cordova.require('cordova/base64');

        // On start-up Stop Native code from scanning - it is possible that page refresh/logout etc could leave Native
        // code scanning in background when new page JS has not requested scan, Cordova will throw away unrecognised Native callbacks
        // but still leaves high CPU usage for no gain
        _bts.stopScanning();

        var uartServiceUUIDString = "6E400001-B5A3-F393-E0A9-E50E24DCCA9E";
        var uartRXCharacteristicUUIDString = "6E400002-B5A3-F393-E0A9-E50E24DCCA9E";
        var uartTXCharacteristicUUIDString = "6E400003-B5A3-F393-E0A9-E50E24DCCA9E";

        var MANUF_ID_BLUE_MAESTRO = 0x0133;
        var MANUF_ID_BLUE_MAESTRO_array = [0x33, 0x01]; 

        $log.info('bluemaestro-tempo-svc starting up ..');

        var q = actionQueue();

        function BlueMaestroTempoDevice(provider, p) {
            this.provider = provider;
            this.peripheral = p;
            this.id = p.id;
            this.name = p.name;
        }

        BlueMaestroTempoDevice.prototype.getSerialNumber = function () {
            return $q.when(this.id);
        }

        BlueMaestroTempoDevice.prototype.extractReferenceDate = function (data, index) {
            var referenceDate = binUtils.bigEndian.readUInt32(data, index);

            var lowDate = 1700000000; //1 January 2017
            var highDate = 1900000000; //1 January 2019
            if ((referenceDate != 0) && ((referenceDate > lowDate) || (referenceDate < highDate))) {
                var minutes = Math.floor(referenceDate % 100);
                var hours = Math.floor((referenceDate / 100) % 100);
                var days = Math.floor((referenceDate / 10000) % 100);
                var months = Math.floor((referenceDate / 1000000) % 100);
                var years = Math.floor((referenceDate / 100000000) % 100);

                return moment().local().set({ year: 2000 + years, month: months - 1, date: days, hour: hours, minute: minutes, second: 0, millisecond: 0 });
            }
        }

        BlueMaestroTempoDevice.prototype.update = function (peripheral) {

            var data = peripheral.data;
            this.version = data[2];
            this.timestamp = moment();

            var reading;
            var endian = binUtils.bigEndian;

            var esProbe = this.ESProbe;
            if (angular.isUndefined(esProbe) && this.name) {
                var model = 'Unknown';
                var classId = null;
                switch (this.version) {
                    case 13:
                        model = 'TEMPO_DISC_13';
                        classId = endian.readUInt8(data, data.length - 5);
                        break;
                    case 113:
                        model = 'TEMPO_DISC_113';
                        classId = endian.readUInt8(data, data.length - 5);
                        break;
                    case 22:
                        model = 'TEMPO_DISC_22';
                        break;
                    case 23:
                        model = 'TEMPO_DISC_23';
                        classId = endian.readUInt8(data, data.length - 5);
                        break;
                    case 27:
                        model = 'PEBBLE v27';
                        classId = endian.readUInt8(data, data.length - 5);
                        break;
                    case 32:
                        model = 'TEMPO_DISC_32';
                        break;
                    case 42:
                        model = 'TEMPO_DISC_32';
                        break;
                    case 99:
                        model = 'PACIF-I V2';
                        break;
                    case 52:
                        model = 'TEMPO_DISC_52';
                        break;
                    case 62:
                        model = 'TEMPO_DISC_62';
                        break;

                    default:
                        // Could not parse 
                        return false;
                }

                // Not a serialnumber obviously, but BlueMaestro devices do not report any such unique serial
                // so we make best we can from their programmable 'name' and 'classid' (0..254)
                var serialNumber = this.name;
/* EVS-1268 TempoDiscs NewID every time iPad wakes
 * Seems that the ClassId field changes every now and again so we cant use it - seriously???
                if (classId != null) {
                    serialNumber += '.' + classId.toString();
                }
*/
                esProbe = new ESProbe(model + '#' + serialNumber, model, serialNumber);
                this.ESProbe = esProbe;
            }

            switch (this.version) {
                case 13:
                case 113:
                    reading = {
                        battery: data[3],
                        timerInterval: endian.readUInt16(data, 4),
                        intervalCounter: endian.readUInt16(data, 6),
                        temperature: endian.readInt16(data, 8) / 10.0,
                        mode: data[14],
                        breachCount: data[15],
                        highestTemperature          : endian.readInt16(data, data.length - 25) / 10,
                        lowestTemperature           : endian.readInt16(data, data.length - 21) / 10,
                        highestDayTemperature       : endian.readInt16(data, data.length - 17) / 10,
                        lowestDayTemperature        : endian.readInt16(data, data.length - 13) / 10,
                        averageDayTemperature       : endian.readInt16(data, data.length - 9) / 10,
                        referenceDate: this.extractReferenceDate(data, data.length - 4)
                    };
                    break;
                case 22:
                    reading = {
                        battery: data[3],
                        timerInterval: endian.readUInt16(data, 4),
                        intervalCounter: endian.readUInt16(data, 6),
                        temperature: endian.readInt16(data, 8) / 10.0,
                        humidity: endian.readInt16(data, 10) / 10.0,
                        dewPoint: endian.readInt16(data, 12) / 10.0,
                        mode: data[14],
                        breachCount: data[15],
                        highestTemperature      : endian.readInt16(data, data.length - 25) / 10,
                        highestHumidity         : endian.readInt16(data, data.length - 23) / 10,
                        lowestTemperature       : endian.readInt16(data, data.length - 21) / 10,
                        lowestHumidity          : endian.readInt16(data, data.length - 19) / 10,
                        highestDayTemperature   : endian.readInt16(data, data.length - 17) / 10,
                        highestDayHumidity      : endian.readInt16(data, data.length - 15) / 10,
                        highestDayDew           : endian.readInt16(data, data.length - 13) / 10,
                        lowestDayTemperature    : endian.readInt16(data, data.length - 11) / 10,
                        lowestDayHumidity       : endian.readInt16(data, data.length - 9) / 10,
                        lowestDayDew            : endian.readInt16(data, data.length - 7) / 10,
                        averageDayTemperature   : endian.readInt16(data, data.length - 5) / 10,
                        averageDayHumidity      : endian.readInt16(data, data.length - 3) / 10,
                        averageDayDew           : endian.readInt16(data, data.length - 1) / 10,
                    };
                    break;
                case 23:
                    reading = {
                        battery: data[3],
                        timerInterval: endian.readUInt16(data, 4),
                        intervalCounter: endian.readUInt16(data, 6),
                        temperature: endian.readInt16(data, 8) / 10.0,
                        humidity: endian.readInt16(data, 10) / 10.0,
                        dewPoint: endian.readInt16(data, 12) / 10.0,
                        mode: data[14],
                        breachCount: data[15],
                        highestTemperature          : endian.readInt16(data, data.length - 25) / 10,
                        highestHumidity             : endian.readInt16(data, data.length - 23) / 10,
                        lowestTemperature           : endian.readInt16(data, data.length - 21) / 10,
                        lowestHumidity              : endian.readInt16(data, data.length - 19) / 10,
                        highestDayTemperature       : endian.readInt16(data, data.length - 17) / 10,
                        highestDayHumidity          : endian.readInt16(data, data.length - 15) / 10,
                        lowestDayTemperature        : endian.readInt16(data, data.length - 13) / 10,
                        lowestDayHumidity           : endian.readInt16(data, data.length - 11) / 10,
                        averageDayTemperature       : endian.readInt16(data, data.length - 9) / 10,
                        averageDayHumidity          : endian.readInt16(data, data.length - 7) / 10,
                        referenceDate               : this.extractReferenceDate(data, data.length - 4)
                    };
                    reading.highestDayDew = reading.highestDayTemperature - ((100 - reading.highestDayHumidity) / 5.0);
                    reading.lowestDayDew = reading.lowestDayTemperature - ((100 - reading.lowestDayHumidity) / 5.0);
                    reading.averageDayDew = reading.averageDayTemperature - ((100 - reading.averageDayHumidity) / 5.0);
                    break;
                case 27:
                    reading = {
                        battery: data[3],
                        timerInterval: endian.readUInt16(data, 4),
                        intervalCounter: endian.readUInt16(data, 6),
                        temperature: endian.readInt16(data, 8) / 10.0,
                        humidity: endian.readInt16(data, 10) / 10.0,
                        pressure: endian.readInt16(data, 12) / 10.0,
                        mode: data[14],
                        breachCount: data[15],
                        highestDayPressure          : endian.readInt16(data, data.length - 25) / 10,
                        averageDayPressure          : endian.readInt16(data, data.length - 23) / 10,
                        lowestDayPressure           : endian.readInt16(data, data.length - 21) / 10,
                        altitude                    : endian.readInt16(data, data.length - 19) / 10,
                        highestDayTemperature       : endian.readInt16(data, data.length - 17) / 10,
                        highestDayHumidity          : endian.readInt16(data, data.length - 15) / 10,
                        lowestDayTemperature        : endian.readInt16(data, data.length - 13) / 10,
                        lowestDayHumidity           : endian.readInt16(data, data.length - 11) / 10,
                        averageDayTemperature       : endian.readInt16(data, data.length - 9) / 10,
                        averageDayHumidity          : endian.readInt16(data, data.length - 7) / 10,
                        referenceDate               : this.extractReferenceDate(data, data.length - 4)
                    };
                    reading.dewPoint = reading.temperature - ((100 - reading.humidity) / 5.0);
                    reading.highestDayDew = reading.highestDayTemperature - ((100 - reading.highestDayHumidity) / 5.0);
                    reading.lowestDayDew = reading.lowestDayTemperature - ((100 - reading.lowestDayHumidity) / 5.0);
                    reading.averageDayDew = reading.averageDayTemperature - ((100 - reading.averageDayHumidity) / 5.0);
                    break;
                case 32:
                    reading = {
                        battery: data[3],
                        timerInterval: endian.readUInt16(data, 4),
                        intervalCounter: endian.readUInt16(data, 6),

                        humSensitivityLevel: data[8],
                        pestSensitivityLevel: data[9],
                        force: data[10],
                        movementMeasurePeriod: endian.readUInt16(data, 11),
                        dateNumber: binUtils.bigEndian.readUInt32(data, 13),
                        buttonPressControl: data[17],
                        lastPestDetectRate: endian.readUInt16(data, 18),
                        lastHumDetectRate: endian.readUInt16(data, 20),
                        totalPestEventsDetects: endian.readUInt16(data, 22),
                        totalHumEventsDetects: endian.readUInt16(data, 24),
                        lastButtonDetected: endian.readUInt16(data, 26)
                    };
                    break;
                case 42:
                    reading = {
                        battery: data[3],
                        timerInterval: endian.readUInt16(data, 4),
                        intervalCounter: endian.readUInt16(data, 6),
                        referenceDate: this.extractReferenceDate(data, 8),
                        buttonPressControl: data[12],
                        lastButtonDetected: endian.readUInt16(data, 13),
                        totalButtonEvents: endian.readUInt16(data, 15)
                    };
                    break;
                case 99:
                    reading = {
                        battery: data[3],
                        timerInterval: endian.readUInt16(data, 4),
                        intervalCounter: endian.readUInt16(data, 6),
                        temperature: endian.readInt16(data, 8) / 10.0,
                    };
                    break;
                case 52:
                    reading = {
                        battery: data[3],
                        timerInterval: endian.readUInt16(data, 4),
                        intervalCounter: endian.readUInt16(data, 6),
                        logPointer: endian.readUInt16(data, 8),
                        openClose: data[10],
                        mode: data[11],
                        referenceDate: this.extractReferenceDate(data, 12),
                        openEventsCount: endian.readUInt16(data, 16),
                        lastOpenInterval: endian.readUInt16(data, 18),
                        totalEventsCount: endian.readUInt16(data, 20),
                    };
                    break;
                case 62:
                    reading = {
                        battery: data[3],
                        timerInterval: endian.readUInt16(data, 4),
                        intervalCounter: endian.readUInt16(data, 6),
                        logPointer: endian.readUInt16(data, 8),
                        lightLevel: binUtils.bigEndian.readInt32(data, 10),
                        lightStatus: data[14],
                        mode: data[15],
                        referenceDate: this.extractReferenceDate(data, 16)
                    };
                    break;

                default:
                    // Could not parse 
                    return false;
            }

            if (angular.isDefined(esProbe)) {
                if (angular.isDefined(reading.battery)) {
                    var sensor = esProbe.getSensor(0, esProbe.sensorTypes.ByName.Battery);
                    sensor.updateValue(reading.battery, true);
                }
                if (angular.isDefined(reading.temperature)) {
                    var sensor = esProbe.getSensor(1, esProbe.sensorTypes.ByName.Temperature);
                    sensor.updateValue(reading.temperature, true);
                }
                if (angular.isDefined(reading.humidity)) {
                    var sensor = esProbe.getSensor(2, esProbe.sensorTypes.ByName.Humidity);
                    sensor.updateValue(reading.humidity, true);
                }
                if (angular.isDefined(reading.pressure)) {
                    var sensor = esProbe.getSensor(3, esProbe.sensorTypes.ByName.BarometricPressure);
                    sensor.updateValue(reading.pressure, true);
                }
                if (angular.isDefined(reading.dewPoint)) {
                    var sensor = esProbe.getSensor(4, esProbe.sensorTypes.ByName.DewPoint);
                    sensor.updateValue(reading.dewPoint, true);
                }
            }
            this.reading = angular.extend(this.reading || {}, reading);
            this.reading.counter = (this.reading.counter || 0) + 1;
            this.battery = reading.battery;
            return true;
        }

        q(function (next) {
            _bts.subscribeStateChange(function (state) {
                $log.info('subscribeStateChange = ' + state);
                if (state == 'PoweredOn') {
                    next();
                }
            });
        });

        BlueMaestroTempoProvider = function () {
            this.id = 'BlueMaestroTempo';
            this.probes = [];
            this.probesById = Object.create(null);
        }

        BlueMaestroTempoProvider.prototype.initialise = function (manager, temperatureProbeManager) {
            this.manager = manager;
            this.temperatureProbeManager = temperatureProbeManager;
            manager.addProvider(this);
            temperatureProbeManager.registerProvider(this);
        }

        BlueMaestroTempoProvider.prototype.startScanning = function (success, error) {
            var self = this;
            q(function (next) {
                _bts.startScanning([], {
                    allowDuplicate: true,   // iOS specific
                    rescanTimeout: 30000,
                    groupTimeout: 500,
                    groupSize: 0,
                    scanMode: 2,                // Android - see SCAN_MODE_BALANCED
                    matchMode: 1,               // Android - see MATCH_MODE_AGGRESSIVE
                    callbackType: 1,            // Android - see CALLBACK_TYPE_ALL_MATCHES
                    numOfMatches: 1             // Android - see MATCH_NUM_MAX_ADVERTISEMENT
                }, function (peripherals) {
                    // Call success with any new or updated probe data
                    var probes = [];
                    var probesById = Object.create(null);
                    if (peripherals) {
                        var blacklistIds = [];
                        angular.forEach(peripherals, function (p) {
                            var probe = self.probesById[p.id];
                            if (probe && !probe.name && p.name) {
                                probe.name = p.name;
                                var n = probe.name.indexOf('\0');       // Trim any trailing NULLs
                                if (n >= 0) {
                                    probe.name = probe.name.substr(0, n);
                                }
                            }
                            if (p.advertisement) {
                                var pa = p.advertisement;
                                // For some reason Tempo discs report their 'name' with extra stuff on the end, localName is correct
/*                                if (probe && pa.localName && probe.name != pa.localName) {
                                    probe.name = pa.localName;
                                }
*/                                if (pa.data) {
                                    pa.data = new Uint8Array(base64.toArrayBuffer(pa.data));
                                } else if (pa[MANUF_ID_BLUE_MAESTRO]) {
                                    pa.data = MANUF_ID_BLUE_MAESTRO_array.concat(new Uint8Array(base64.toArrayBuffer(pa[MANUF_ID_BLUE_MAESTRO])));
                                }
                                if (pa.serviceData) {
                                    angular.forEach(pa.serviceData, function (b64, uuid) {
                                        pa.serviceData[uuid] = new Uint8Array(base64.toArrayBuffer(b64));
                                    });
                                }
                            }

                            if (!probe && pa.data && pa.data.length > 2) {
                                var manuf = binUtils.littleEndian.readUInt16(pa.data, 0);
                                if (manuf == MANUF_ID_BLUE_MAESTRO) {
                                    probe = new BlueMaestroTempoDevice(self, p);
                                    self.probes.push(probe);
                                    self.probesById[probe.id] = probe;
                                }
                            }

                            if (angular.isUndefined(probe)) {
                                blacklistIds.push(p);
                            } else {
                                if (probe.update(pa)) {
                                    if (angular.isUndefined(probesById[probe.id])) {
                                        // Only push each probe once (we may get multiple readings from 1 probe in a single scan)
                                        // Also, dont push if we dont have a ESProbe (most probably no name yet)
                                        if (angular.isDefined(probe.ESProbe)) {
                                            probes.push(probe.ESProbe);
                                        }
                                        probesById[probe.id] = probe;
                                    }
                                }
                            }
                        });
                        if (blacklistIds.length && angular.isFunction(_bts.blacklist)) {
                            _bts.blacklist(blacklistIds);
                        }
                    }

                    if (probes.length) {
                        success(probes);

                        self.invokeTemperatureManagerScans(probes);
                    }
                    next();
                }, error);
            });
        }

        function BlueMaestroTemperatureProbe(sensor) {
            var self = this;
            this.sensor = sensor;      
        }

        BlueMaestroTemperatureProbe.prototype.getSerialNumber = function () {
            return $q.when(this.sensor.probe.serialNumber);
        }
        // Temperature Manager provider API
        BlueMaestroTempoProvider.prototype.invokeTemperatureManagerScans = function (probes) {
            var self = this;
            angular.forEach(probes, function (probe) {
                var sensor = probe.sensorsByType[ESProbe.sensorTypes.ByName.Temperature];
                if (angular.isDefined(sensor)) {
                    var probe = self.temperatureProbeManager.findProbe(sensor.id);
                    if (!probe) {
                        var bmsensor = new BlueMaestroTemperatureProbe(sensor);
                        probe = self.temperatureProbeManager.addProbe(self, sensor.id, sensor.probe.serialNumber, bmsensor);
                        bmsensor.onremove = sensor.subscribeUpdateValue(function () {
                            var batterySensor = sensor.probe.sensorsByType[ESProbe.sensorTypes.ByName.Battery];
                            probe.updated({
                                temperature: {
                                    rawValue: bmsensor.sensor.value,
                                    updateTimestamp: bmsensor.sensor.updateTimestamp,
                                    readingTimestamp: bmsensor.sensor.readingTimestamp
                                },
                                battery: batterySensor ? {
                                    percent: batterySensor.value / 100.0
                                } : null
                            }, sensor.value != sensor.lastValue);
                        });
                    }
                }
            })
        }

        return new BlueMaestroTempoProvider();
    }])

    .factory('bluemaestro-tempo-svc', ['$injector', function ($injector) {
        if (window.cordova && window.cordova.plugins && window.cordova.plugins.VirtualManagerBLE && window.cordova.plugins.VirtualManagerBLE.Client) {
            return $injector.get('bluemaestro-tempo-ble-svc');
        } else {
            return {
                notsupported: true
            }
        }
    }])

    .run(['environment-sensor-manager', 'temperature-probe-manager', 'bluemaestro-tempo-svc', function (manager, temperatureProbeManager, provider) {
        if (!provider.notsupported) {
            provider.initialise(manager, temperatureProbeManager);
        }
    }])


