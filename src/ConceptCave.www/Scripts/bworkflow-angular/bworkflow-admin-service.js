﻿var bworkflowAdminApiModule = angular.module('bworkflowAdminApi', ['ngResource', 'ngCookies']);

bworkflowAdminApiModule.factory('bworkflowAdminApi', ['$q', '$resource', '$rootScope', function ($q, $resource, $rootScope) {
    //construct a new instance of the service

    //methods:
    var jsonPost = {
        method: 'POST',
        isArray: false
    };

    var jsonArrayPost = {
        method: 'POST',
        isArray: true
    };

    var jsonGet = {
        method: 'GET',
        isArray: false
    };

    var jsonArrayGet = {
        method: 'GET',
        isArray: true
    };

    var apiprefix = window.razordata.apiprefix;
    var allLabelResource = $resource(apiprefix + 'Admin/Labels?type=:type');
    var allRolesResource = $resource(apiprefix + 'Admin/Roles');
    var allHierarchiesResource = $resource(apiprefix + 'Admin/Hierarchies');


    var mkResolver = function (deferred) {
        return function (r) { deferred.resolve(r); };
    };

    var mkError = function (deferred) {
        return function (r) { deferred.reject(r); };
    };

    var bworkflowAdminApiService = {
        cachedLabels: {},
        cachedRoles: null,
        cachedHierarchies: null,

        getLabels: function (labelType) {
            var deferred = $q.defer();
            if (angular.isDefined(bworkflowAdminApiService.cachedLabels[labelType]) == true) {
                deferred.resolve(bworkflowAdminApiService.cachedLabels[labelType]);

                return deferred.promise;
            }

            // grab our own promise and use this to make the call, we then get a chance to cache the result
            // and can let the caller know of the result through the promise we give them
            var callDeferred = $q.defer();

            allLabelResource.get({ type: labelType }, mkResolver(callDeferred), mkError(callDeferred));
            callDeferred.promise.then(function (data) {
                bworkflowAdminApiService.cachedLabels[labelType] = data.result;

                deferred.resolve(bworkflowAdminApiService.cachedLabels[labelType]);
            });

            return deferred.promise;
        },

        getRoles: function () {
            var deferred = $q.defer();

            if (bworkflowAdminApiService.cachedRoles != null) {
                deferred.resolve(bworkflowAdminApiService.cachedRoles);

                return deferred.promise;
            }

            // grab our own promise and use this to make the call, we then get a chance to cache the result
            // and can let the caller know of the result through the promise we give them
            var callDeferred = $q.defer();

            allRolesResource.get({}, mkResolver(callDeferred), mkError(callDeferred));
            callDeferred.promise.then(function (data) {
                bworkflowAdminApiService.cachedRoles = data.result;

                deferred.resolve(bworkflowAdminApiService.cachedRoles);
            });

            return deferred.promise;
        },

        getHierarchies: function () {
            var deferred = $q.defer();

            if (bworkflowAdminApiService.cachedHierarchies != null) {
                deferred.resolve(bworkflowAdminApiService.cachedHierarchies);

                return deferred.promise;
            }

            // grab our own promise and use this to make the call, we then get a chance to cache the result
            // and can let the caller know of the result through the promise we give them
            var callDeferred = $q.defer();

            allHierarchiesResource.get({}, mkResolver(callDeferred), mkError(callDeferred));
            callDeferred.promise.then(function (data) {
                bworkflowAdminApiService.cachedHierarchies = data.result;

                deferred.resolve(bworkflowAdminApiService.cachedHierarchies);
            });

            return deferred.promise;
        },

        getfullurl: function (url) {
            return url.replace(/^~\//, rtrim(window.razordata.siteprefix) + '/');
        },

        getImageUrl: function () {
            return window.razordata.siteprefix + 'MediaImage';
        }
    };

    return bworkflowAdminApiService;
}]);

(function () {
    var filtersModule = angular.module('bworkflowFilters');

    //Sneak in the client filter here
    filtersModule.filter('serverUrl', [function () {
        return function (inputstr) {
            return inputstr.replace(/^~\//, rtrim(window.razordata.siteprefix) + '/');
        };
    }]);
})();