﻿angular.module('bworkflowAnimationFullScreen', [])
  .animation('.fullscreen-animate', ['$window', function ($window) {
      return {
          enter: function (element, done) {
              var duration = 0.5;
              TweenMax.from(element, duration, { opacity: 0, scaleX: 5, scaleY: 5, ease: Strong.easeOut, onComplete: function () { done(); } });
          },
          leave: function (element, done) {
            var duration = 0.5;
            TweenMax.to(element, duration, { opacity: 0, scaleX: 5, scaleY: 5, ease: Strong.easeIn, onComplete: function () { done(); } });
        }
      }
  }])
  .animation('.fullscreen-panel-animate', ['$window', function ($window) {
      return {
          enter: function (element, done) {
              // we are going to animate each of the children off with a slight delay between each
              TweenMax.staggerFrom($(element).children(), 2, { scale: 0.5, opacity: 0, delay: 0.8, ease: Elastic.easeOut, force3D: true, onComplete: function () { done(); } }, 0.2);
          },
          leave: function (element, done) {
              TweenMax.staggerTo($(element).children(), 0.5, { opacity: 0, y: -100, ease: Back.easeIn, onComplete: function () { done(); } }, 0.1);
          }
      }
  }])