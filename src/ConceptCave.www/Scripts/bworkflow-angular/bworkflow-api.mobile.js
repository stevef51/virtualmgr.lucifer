﻿var bworkflowApiModule = angular.module('bworkflowApi', ['ngResource']);

bworkflowApiModule.factory('bworkflowApi', ['$q', '$resource', '$rootScope', function ($q, $resource, $rootScope) {
    //construct a new instance of the service
    
    var mkResolver = function(deferred) {
        return function (r) {
            $rootScope.$apply(function () {
                deferred.resolve(r);
            });
        };
    };

    var bworkflowApiService = {

        getPublishedGroups: function () {
            var deferred = $q.defer();
            SquareIT.BWorkflowApi.getPublishedGroups({}, mkResolver(deferred));
            return deferred.promise;
        }, //We can memoize this because they don't do anything on the server

        getPublishedGroup: function (id) {
            var deferred = $q.defer();
            SquareIT.BWorkflowApi.getPublishedGroup({ id: id }, mkResolver(deferred));
            return deferred.promise;
        },
        
        beginChecklist: function(groupId, resourceId, revieweeId, args) {
            var deferred = $q.defer();
            SquareIT.BWorkflowApi.beginChecklist({
                groupId: groupId,
                resourceId: resourceId, 
                revieweeId: revieweeId,
                args: args}, mkResolver(deferred));
            return deferred.promise;
        },
        
        nextChecklist: function(workingDocumentId, answerModel) {
            var deferred = $q.defer();
            SquareIT.BWorkflowApi.nextChecklist({
                WorkingDocumentId: workingDocumentId,
                Answers: answerModel }, mkResolver(deferred));
            return deferred.promise;
        },
        
        prevChecklist: function(workingDocumentId) {
            var deferred = $q.defer();
            SquareIT.BWorkflowApi.prevChecklist({
                WorkingDocumentId: workingDocumentId,
                Answers: {}
            }, mkResolver(deferred));
            return deferred.promise;
        },

        allUsers: function() {
            var deferred = $q.defer();
            SquareIT.BWorkflowApi.allUsers({}, mkResolver(deferred));
            return deferred.promise;
        },
        
        continueChecklist: function(workingDocumentId) {
            var deferred = $q.defer();
            SquareIT.BWorkflowApi.continueChecklist({
                id: workingDocumentId
            }, mkResolver(deferred));
            return deferred.promise;
        },
        
        getDashboard: function() {
            var deferred = $q.defer();
            SquareIT.BWorkflowApi.getDashboard({}, mkResolver(deferred));
            return deferred.promise;
        },
        
        //Other kit methods that aren't really the API

        ShowUploadMediaDialog: function (options) {
            var deferred = $q.defer();
            SquareIT.BWorkflowApi.UploadMedia(options, mkResolver(deferred));
            return deferred.promise;
        },
        
        getGPSLocation : function() {
            var deferred = $q.defer();
            SquareIT.GetGPSLocation({},
                function(result) {
                    deferred.resolve(result);
                }, function(error) {
                    deferred.reject(error);
                });
            return deferred.promise;
        }
    };
    return bworkflowApiService;
}]);

(function () {
    var filtersModule = angular.module('bworkflowFilters');

    //Sneak in the client filter here
    filtersModule.filter('serverUrl', [function () {
        return function (inputstr) {
            return inputstr.replace(/^~\//, rtrim(window.razordata.serverprefix) + '/');
        };
    }]);
})();