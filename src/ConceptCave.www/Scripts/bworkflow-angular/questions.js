var questionsModule = angular.module('questions', ['bworkflowApi', 'angularMoment', 'instrument-works-pH-probe', 'temperature-probe-manager-module', 'bluetherm-dishtemp-blue-module']);

//Framework methods...

questionsModule.filter('unsafeHtml', function ($sce) {
    return $sce.trustAsHtml;
});

//This guy is a wrapper you can use in your question templates
//If you do <div questionWrapper="presented">...</div>, you get the
//prompt, notes, custom styling etc
questionsModule.directive('questionWrapper', ['$sce', function ($sce) {
    return {
        templateUrl: 'question_presentable_wrapper.html',
        transclude: true,
        replace: true,
        restrict: 'A',
        scope: {
            presented: '=questionWrapper', //bind to value of this attribute
        },
        link: function (scope, elt, attrs) {
            //Behaviour for the general question template
            //TODO: Link up notes event handler!
            if (typeof (attrs.renderPrompt) === 'undefined')
                scope.shouldRenderPrompt = true;
            else
                scope.shouldRenderPrompt = attrs.renderPrompt.toLowerCase() == 'true';

            scope.presented.promptAsHtml = $sce.trustAsHtml(scope.presented.Prompt);
            //scope.presented.promptAsHtml = function () { return $sce.trustAsHtml(scope.presented.Prompt); };

            //since angular won't parse the string replace in the style
            $(elt).attr('style', scope.presented.HtmlPresentation.CustomStyling);

            scope.$on('help_add_steps', function (event, args) {
                if (scope.presented.HelpContent != null && scope.presented.Name != null) {
                    var step = { element: 'div[data-presentable-name="' + scope.presented.Name + '"]', intro: scope.presented.HelpContent, position: scope.presented.HelpPosition.toLowerCase() };

                    args.push(step);
                }
            });
        }
    };
}]);

//This directive is used to select the appropriate directive for a presented
//then, it inserts said directive into the DOM causing it to evaluate
//Used by top-level template and section template
questionsModule.directive('renderQuestion', ['$compile', '$parse', function ($compile, $parse) {
    return {
        restrict: 'EA',
        scope: {
            presented: '=renderQuestion',
            pageScope: '='
        },
        link: function (scope, elt, attrs) {
            var dirName = scope.presented.Directive;

            var elString = sprintf('<div %s presented="presented" page-scope="pageScope"></div>', dirName);
            var dirElement = $compile(elString)(scope);
            $(elt).append(dirElement);
        }
    };
}]);

questionsModule.factory('sectionUtils', ['$rootScope', '$q', function ($rootScope, $q) {
    var _urls = {};
    var _deferreds = {};

    return {
        urls: _urls,

        addUrls: function (urls) {
            _urls = angular.extend(_urls, urls);

            $rootScope.$emit('sectionUtils.addUrls', _urls);

            angular.forEach(urls, function (value, key) {
                if (!angular.isDefined(_deferreds[key])) {
                    _deferreds[key] = $q.defer();
                }
                _deferreds[key].resolve(value);
            })
        },

        buildODataUrl: function (feed) {
            var url = _urls['odata'];

            if (url == null || url == '') {
                url = 'odata';
            }

            url = url + "/" + feed;

            return url;
        },

        getUrl: function (url) {
            if (!angular.isDefined(_deferreds[url])) {
                _deferreds[url] = $q.defer();
            }            
            return _deferreds[url].promise;
        }
    }
}]);

//Section renderer- works the same way as freetext below basically
questionsModule.directive('questionSection', ['bworkflowApi', 'sectionUtils', '$timeout', 'playerButtons', function (bworkflowApi, sectionUtils, $timeout, playerButtons) {
    var _count = 0;

    function scrollToFirstValidationError(pageScopeElement) {
        var firstValidationError = $('#firstValidationError');
        if (firstValidationError.length) {
            var errorTop = firstValidationError[0].offsetTop - pageScopeElement.offsetTop;
            window.scrollTo({
                top: errorTop,
                behavior: 'smooth'
            });
        }
    }

    return {
        scope: {
            presented: '=',
            pageScope: '='
        },
        templateUrl: 'question_section.html',
        restrict: 'EA',
        controller: function ($scope, $controller, $element) {
            // ngControllerCode will only be defined at the Page root (Page is a Section)
            // We only want 1 pageScope at the Page level
            var ngControllerCode = $scope.presented.ngcontroller;
            var fn = function () { };
            if (ngControllerCode) {
                fn = eval('(' + ngControllerCode + ')');
            }

            $scope.SectionController = function () {
                // If we have a pageScope already then this is not the Page root section
                if ($scope.pageScope) {
                    return this;
                }

                // Must be Page root section, create a pageScope, this gets passed down to all questions..
                $scope.pageScope = $scope.$new();
                $scope.pageScope.scope = {};            // Bin for all named Questions scope objects
                $scope.pageScope.playerButtons = playerButtons;

                // pageScope can override this function
                $scope.pageScope.onready = function () {
                };

                // this function should really remain as it is, although its put on pageScope "just in case its ever needed to be overriden"
                $scope.pageScope._onReady = function () {
                    console.log('pageScope ready');
                    $scope.pageScope.onready();

                    scrollToFirstValidationError($element[0]);
                }

                $scope.$on('validationErrors', function () {
                    scrollToFirstValidationError($element[0])
                });

                var ctrl = $controller(fn, { $scope: $scope.pageScope }).constructor;

                // Queue the onready call, this will occur once all Questions have been rendered into pageScope
                $timeout($scope.pageScope._onReady);
            };
        },
        link: function (scope, elt, attrs) {
            scope.allquestionscategory = { Category: 'All Questions', ErrorCount: 0 };
            scope.feeds = {};

            if (scope.presented.presentasfinal) {
                // let anyone who's interested know this is the last page as dictated by the dude/dudet that put the checklist together
                console.log('Final page detected');
                scope.$emit('section.finalpage', scope.presented);
            }

            if (scope.presented.urls) {
                sectionUtils.addUrls(scope.presented.urls);
            }

            scope.visibleFirstEnter = true;

            if (scope.presented.AllowFilterByCategory == true && scope.presented.ChildCategories.length > 0) {
                scope.selectedcategory = scope.presented.ChildCategories[0];
            }

            if (scope.presented.HtmlPresentation.Layout == 3) {
                // tabbed presentation
                angular.forEach(scope.presented.Children, function (child) {
                    if (child.PresentedType == 'Section') {
                        child.HidePrompt = true;
                        child.tabId = child.Id;
                    }
                });
            }
            else if (scope.presented.HtmlPresentation.Layout == 4 || scope.presented.HtmlPresentation.Layout == 5) {
                // modal, we set this to visible = false by default
                scope.presented.Visible = false;

                // we watch the visible property so that we can add/remove the noscroll
                // class from the body, which gets added/removed as the scroll bars
                // on the body interfer with the full screen view of a section
                scope.$watch('presented.Visible', function (newValue, oldValue) {
                    if (scope.visibleFirstEnter == true) {
                        scope.visibleFirstEnter = false;
                        return;
                    }

                    $('body').toggleClass('noscroll');
                });
            }

            scope.show = function () {
                if (scope.presented.HtmlPresentation.Layout == 4 || scope.presented.HtmlPresentation.Layout == 5) {
                    // Find our parent first
                    var parent = scope.$parent;
                    while (parent != null) {
                        if (angular.isDefined(parent.presented) && parent.presented !== scope.presented) {
                            // Find our sibling Sections and hide them
                            scope.hiddenSiblings = [];
                            parent.presented.Children.forEach(function (sibling) {
                                scope.hiddenSiblings.push({ sibling: sibling, visible: sibling.Visible });
                                sibling.Visible = sibling === scope.presented;
                            });
                            break;
                        }
                        parent = parent.$parent;
                    }

                }
                scope.presented.Visible = true;
            };

            scope.close = function () {
                scope.presented.Visible = false;
                if (angular.isDefined(scope.hiddenSiblings)) {
                    scope.hiddenSiblings.forEach(function (hs) {
                        hs.sibling.Visible = hs.visible;
                    });
                    delete scope.hiddenSiblings;
                }
            };

            if (scope.presented.Name !== null && scope.presented.Name !== '') {

                // Hook ourselves into pageScope so it can access us by name
                scope.pageScope[scope.presented.Name] = scope;

                scope.$on(scope.presented.Name, function (evt, args) {
                    if (args.action === 'show') {
                        scope.show();
                    }
                });
            }

            scope.setVisible = function (visible) {
                scope.presented.Visible = !!visible;
            };

            scope.selectcategory = function (category) {
                scope.selectedcategory = category;
            };

            scope.filterByCategory = function (c) {
                return function (child) {
                    if (angular.isDefined(scope.selectedcategory) == false) {
                        return true;
                    }

                    if (scope.selectedcategory === scope.allquestionscategory) {
                        return true;
                    }

                    if (!child.Categories) {
                        return true;
                    }

                    if (child.Categories.length === 0) {
                        return true;
                    }

                    return child.Categories.indexOf(scope.selectedcategory.Category) !== -1;
                };
            };

            if (scope.presented.datasource) {
                var split = scope.presented.datasource.split(',');

                // we store each of the feeds in a varibale based on their name
                angular.forEach(split, function (source) {
                    var promise = bworkflowApi.getDataFeed(source);

                    if (promise !== null) {
                        promise.then(function (feed) {
                            var key = feed.notifier.id.split('.').join('');

                            scope.feeds[key] = feed;

                            scope.$watch('feeds.' + key + '.notifier', function (newValue, oldValue) {
                                if (angular.isDefined(newValue) === false || newValue === null) {
                                    return;
                                }

                                var isVisible = false;
                                angular.forEach(scope.feeds, function (feed) {
                                    if (feed.data.length > 0) {
                                        isVisible = true;
                                    }
                                });

                                scope.presented.Visible = isVisible;
                            });
                        });
                    }
                });
            }

            // Hook all childscopes to our deepEdit mode
            function setDeepEditMode(s, deepEdit) {
                if (s === null || (s.hasOwnProperty('presented') && s.presented.enabledeepedit)) {
                    return;
                }
                s.deepEdit = deepEdit;
                var cs = s.$$childHead;
                while (cs !== null) {
                    setDeepEditMode(cs, deepEdit);
                    cs = cs.$$childHead;
                }
                if (s.$$nextSibling !== null) {
                    setDeepEditMode(s.$$nextSibling, deepEdit);
                }
            }

            if (scope.presented.enabledeepedit) {
                scope.deepEdit = {
                    readonly: true
                };

                scope.toggleDeepEditMode = function () {
                    scope.deepEdit.readonly = !scope.deepEdit.readonly;
                    playerButtons.update();
                };

                $timeout(function () {
                    setDeepEditMode(scope.$$childHead, scope.deepEdit);
                });
            }
        }
    };
}]);

questionsModule.directive('bwkStyle', ['$sce', function ($sce) {
    return {
        link: function (scope, elt, attrs) {
            var styleText = attrs.bwkStyle;
            $(elt).attr('style', styleText);
        }
    };
}]);

var questionDirectiveBase = {
    scope: {
        presented: '=',
        pageScope: '='
    },
    restrict: 'EA',
    link: function (scope, scopeDefault, bwkApi, languageTranslate) {
        var bworkflowApi = bwkApi;

        // questions get access to languageTranslate through their L property, L.code is the current selected language (English = 'en')
        // which makes for Dashboard embedded translations easy
        // {{ {'en': 'Hello', 'fr': 'Bonjour'}[L.code] }}
        scope.L = languageTranslate;

        if (scope.presented.Name && scope.pageScope) {
            // Access to Question scope can be explicitly through the 'question'
            scope.pageScope.scope[scope.presented.Name] = scope;

            if (!angular.isDefined(scopeDefault)) {
                scopeDefault = scope;
            }
            scope.pageScope[scope.presented.Name] = scopeDefault;
        }

        scope.validate = function (stage, data) {
            if (angular.isDefined(scope.getAnswerValue) == false || angular.isUndefined(bworkflowApi)) {
                return;
            }

            var val = scope.getAnswerValue();

            bworkflowApi.validate(scope, val, scope.answer, scope.presented.validators, stage, data);
        };

        scope.$on('validate', function (ev, data) {
            if (scope.presented.Visible) {
                scope.validate(data.stage, data);
            }
        });

        scope.$on('populateAnswer', function (ev) {
            // Record whether we actually displayed the question or not so server can filter out for validation if required
            if (angular.isDefined(scope.answer)) {
                if (!scope.presented.Visible) {
                    scope.answer.WasHidden = !scope.presented.Visible;
                } else {
                    delete scope.answer.WasHidden;
                }
            }
        });

        scope.setVisible = function (visible) {
            scope.presented.Visible = visible;
        };
    }
};

//And question directives here!

//Structure of a question directive:
//It should accept presented and answers attributes, linking to this questions
//presented response and the global answers object
//Select a template for the question; it should have two-way databinding on scope.answers[scope.presented.Id]
//anything fancy can happen in the link function
questionsModule.directive('questionFreeText', ['$timeout', 'bworkflowApi', 'languageTranslate', function ($timeout, bworkflowApi, languageTranslate) {
    return $.extend({}, questionDirectiveBase, {
        templateUrl: 'question_freetext.html',
        link: function (scope, elt, attrs) {
            questionDirectiveBase.link(scope, undefined, bworkflowApi, languageTranslate);

            scope.answer = scope.presented.Answer;

            //Set up multiline text input?
            if (scope.presented.AllowMultiline) {
                //need to wait for the template to render
                $timeout(function () {
                    $(elt).find('textarea').redactor(
                        {
                            buttons: ['html', '|', 'formatting', '|', 'bold', 'italic',
                                'deleted', '|', 'unorderedlist', 'orderedlist', 'outdent',
                                'indent', '|', 'table', 'link', '|', 'fontcolor', 'backcolor',
                                '|', 'alignleft', 'aligncenter', 'alignright', 'justify',
                                '|', 'horizontalrule']
                        });
                });
                //and two-way binding doesn't work for the textarea, so we need to manually populate the answer model
                scope.$on('populateAnswer', function (ev) {
                    scope.answer.Text = $(elt).find('textarea').val(); //Need to update answer model
                });
            }

            // this supports validation as each question type stores it's answer in a different way
            scope.getAnswerValue = function () {
                if (scope.presented.AllowMultiline) {
                    return $(elt).find('textarea').val(); // due to two-way binding not working for text area
                }
                else {
                    return scope.answer.Text;
                }
            };
        }
    });
}]);

questionsModule.directive('questionDateTime', ['bworkflowApi', 'languageTranslate', function (bworkflowApi, languageTranslate) {
    return $.extend({}, questionDirectiveBase, {
        templateUrl: 'question_datetime.html',
        link: function (scope, elt, attrs) {
            questionDirectiveBase.link(scope, undefined, bworkflowApi, languageTranslate);
            scope.answer = scope.presented.Answer;

            scope.updateSources = function (newDate) {
                if (scope.presented.updatedatasources != null && scope.presented.updatedatasources != '') {
                    var sources = scope.presented.updatedatasources.split(',');

                    angular.forEach(sources, function (source) {
                        var promise = bworkflowApi.getDataFeed(source);

                        if (promise != null) {
                            promise.then(function (toUpdate) {
                                var startname = scope.presented.Name;
                                var endname = startname;

                                if (startname == null || startname == '') {
                                    startname = 'StartDate';
                                    endname = 'EndDate';
                                }
                                else {
                                    // this will make it available as startnameStartdate where
                                    // startname is the name of the question, notice the - results
                                    // in the startdate being capitilized (Angular) rather than
                                    // it being accessed through the - if you get my drift.
                                    startname = startname + 'StartDate';
                                    endname = endname + 'EndDate';
                                }

                                // we do a range of dates to make things easy on the odata side
                                toUpdate.parameters[startname] = angular.copy(newDate);
                                toUpdate.parameters[endname] = angular.copy(newDate).addDays(1);

                                // get data and force a refresh
                                toUpdate.getData(true);
                            });
                        }
                    });
                }
            };

            // this supports validation as each question type stores it's answer in a different way
            scope.getAnswerValue = function () {
                return moment(scope.answer.DateTime).isValid() ? scope.answer.DateTime : null;
            };

            // Adding this in as a fix for EVS-1237
            scope.$on('populateAnswer', function (ev) {
                var m = moment(scope.answer.DateTime);

                if (m.isValid() == false) {
                    return;
                }

                if (scope.sendAsLocalTime) {
                    scope.answer.DateTime = m.format('YYYY-MM-DD') + 'T00:00:00.000';
                } else {
                    scope.answer.DateTime = m.format('YYYY-MM-DD') + 'T00:00:00.000Z';
                }
            });

            if (scope.presented.updatedatasources != null && scope.presented.updatedatasources != '') {
                scope.$watch('answer.DateTime', function (newValue, oldValue) {
                    if (newValue == null || moment(newValue).isValid() == false) {
                        return;
                    }

                    scope.updateSources(newValue);
                });
            }

            function isBetween(m, s, e) {
                if (!s || !e) {
                    return false;
                }
                s = moment(s);
                e = moment(e);
                if (!s.isValid() || !e.isValid()) {
                    return false;
                }
                return !m.isBefore(s) && !m.isAfter(e);
            }

            scope.dateOnRender = function (date) {
                var m = moment(date);

                if (angular.isDefined(scope.minDate)) {
                    if (m.isBefore(scope.minDate)) {
                        return 'disabled';
                    }
                }
                if (angular.isDefined(scope.maxDate)) {
                    if (m.isAfter(scope.maxDate)) {
                        return 'disabled';
                    }
                }

                var between;
                if (angular.isDefined(scope.pairedStartDate)) {
                    between = isBetween(m, scope.pairedStartDate.answer.DateTime, scope.answer.DateTime);
                } else if (angular.isDefined(scope.pairedEndDate)) {
                    between = isBetween(m, scope.answer.DateTime, scope.pairedEndDate.answer.DateTime);
                }
                return between ? 'active' : '';
            }

            scope.dateChanged = function () {
                var m = moment(scope.answer.DateTime);
                if (m.isValid()) {
                    if (angular.isDefined(scope.minDate) && scope.minDate.isValid()) {
                        if (m.isBefore(scope.minDate)) {
                            scope.answer.DateTime = scope.minDate.toDate();
                        }
                    }
                    if (angular.isDefined(scope.maxDate) && scope.maxDate.isValid()) {
                        if (m.isAfter(scope.maxDate)) {
                            scope.answer.DateTime = scope.maxDate.toDate();
                        }
                    }
                    if (angular.isDefined(scope.pairedStartDate)) {
                        // m is the EndDate
                        var s = moment(scope.pairedStartDate.answer.DateTime);
                        if (!s.isValid() || m.isBefore(s)) {
                            scope.pairedStartDate.answer.DateTime = m.toDate();
                        }
                    } else if (angular.isDefined(scope.pairedEndDate)) {
                        // m is the StartDate
                        var e = moment(scope.pairedEndDate.answer.DateTime);
                        if (!e.isValid() || m.isAfter(e)) {
                            scope.pairedEndDate.answer.DateTime = m.toDate();
                        }
                    }
                }
            };

            scope.setMinDate = function (dt) {
                scope.minDate = moment(dt);
                if (scope.minDate.isValid()) {
                    var m = moment(scope.answer.DateTime);
                    if (m.isValid()) {
                        if (m.isBefore(scope.minDate)) {
                            scope.answer.DateTime = scope.minDate.toDate();
                        }
                    }
                }
            };

            scope.setMaxDate = function (dt) {
                scope.maxDate = moment(dt);
                if (scope.maxDate.isValid()) {
                    var m = moment(scope.answer.DateTime);
                    if (m.isValid()) {
                        if (m.isAfter(scope.maxDate)) {
                            scope.answer.DateTime = scope.maxDate.toDate();
                        }
                    }
                }
            };

            scope.pairWithEndDate = function (endDateQuestion, minMaxDates) {
                scope.pairedEndDate = endDateQuestion;
                endDateQuestion.pairedStartDate = scope;

                if (angular.isArray(minMaxDates) && minMaxDates.length > 0) {
                    var min = moment(minMaxDates[0]);
                    if (min.isValid()) {
                        scope.setMinDate(min);
                        endDateQuestion.setMinDate(min);
                    }
                    if (minMaxDates.length > 1) {
                        var max = moment(minMaxDates[1]);
                        if (max.isValid()) {
                            scope.setMaxDate(max);
                            endDateQuestion.setMaxDate(max);
                        }
                    }
                }
            }
        }
    });
}]);

questionsModule.directive('questionNumber', ['bworkflowApi', 'languageTranslate', function (bworkflowApi, languageTranslate) {
    return $.extend({}, questionDirectiveBase, {
        templateUrl: 'question_number.html',
        link: function (scope, elt, attrs) {
            questionDirectiveBase.link(scope, undefined, bworkflowApi, languageTranslate);
            scope.answer = scope.presented.Answer;

            // bc-keypad expects a string, otherwise it will blank its model
            if (angular.isDefined(scope.answer) && angular.isDefined(scope.answer.Number) && scope.answer.Number != null) {
                scope.answer.Number = scope.answer.Number.toString();
            }

            // this supports validation as each question type stores it's answer in a different way
            scope.getAnswerValue = function () {
                return Number(scope.answer.Number);
            };
        }
    });
}]);

questionsModule.directive('questionTemperatureProbe', ['temperature-probe-manager', '$filter', 'persistantStorage', 'serverBasedUserSettings', 'bworkflowApi', '$log', 'cookieTimerSvc', 'languageTranslate', 'temperatureConvert', function (temperatureProbe, $filter, persistantStorage, serverBasedUserSettings, bworkflowApi, $log, cookieTimerSvc, languageTranslate, temperatureConvert) {
    var TEMPERATURE_PROBE_LAST_SERIAL_NUMBER = 'TemperatureProbe.LastSerialNumber';
    var temperatureReadingLookups = {
        'Realtime': 'temperature',
        'MaximumReached': 'maxTemperature'
    };

    return $.extend({}, questionDirectiveBase, {
        templateUrl: 'question_temperature_probe.html',
        link: function (scope, elt, attrs) {
            questionDirectiveBase.link(scope, undefined, bworkflowApi, languageTranslate);

            var _cookieTimer = cookieTimerSvc(scope);

            scope.answer = scope.presented.Answer;
            scope.showSelectProbe = false;
            scope.onDestroy = [];

            function getProbeReading(probe) {
                if (!probe) {
                    probe = temperatureProbe.probe;
                }
                if (probe) {
                    return probe.readings[temperatureReadingLookups[scope.presented.TemperatureReading] || 'temperature'];
                }
            }

            function getProbeReadingValue(probe) {
                var reading = getProbeReading(probe);
                if (reading != null) {
                    return reading.value;
                }
            }

            // this supports validation as each question type stores it's answer in a different way
            scope.getAnswerValue = function () {
                return scope.presented.Answer.Celcius;
            };

            if (angular.isDefined(temperatureProbe.supported) == true && temperatureProbe.supported == false) {
                scope.supported = false;
                return;
            }
            scope.supported = true;

            // Register ourselves as a temperature client 
            scope.onDestroy.push(temperatureProbe.addClient(scope));

            scope.calibrateProbe = function () {
                if (!temperatureProbe.probe || !temperatureProbe.probe.canCalibrate) {
                    return;
                }
                scope.calibrationSequence = temperatureProbe.probe.createCalibrationSequence();
                scope.showCalibrateProbe = true;
            }

            scope.closeCalibrateProbe = function () {
                scope.calibrationSequence = null;
                scope.showCalibrateProbe = false;
            }

            scope.saveCalibration = function () {
                var calibrationData = scope.calibrationSequence.stop();
                if (angular.isDefined(calibrationData)) {
                    var tempCalibrationKey = 'Temperature Calibration:' + temperatureProbe.probe.serialNumber;
                    serverBasedUserSettings.setUserSetting(tempCalibrationKey, calibrationData);
                    persistantStorage.setItem(tempCalibrationKey, calibrationData);
                }
            }

            scope.degrees = function (reading) {
                if (reading == null || angular.isUndefined(reading)) {
                    return '';
                }

                if (scope.presented.DisplayAsFarenheit) {
                    reading = temperatureConvert.celsius.toFahrenheit(reading);
                }
                var result = $filter('number')(reading, 2);
                result += '\xB0';       // &deg;
                result += scope.presented.DisplayAsFarenheit ? 'F' : 'C';

                return result;
            }

            var getReadingPassStatus = function (celcius) {
                if (celcius == null || angular.isUndefined(celcius)) {
                    return 3;
                }
                // min/max are always Celcius
                if (celcius > scope.presented.MaximumPassReading) {
                    return 2;
                } else if (celcius < scope.presented.MinimumPassReading) {
                    return 1;
                }
                return 0;
            }

            scope.setReading = function (celcius) {
                scope.reading = celcius;
                scope.readingStatus = getReadingPassStatus(celcius);
            };

            scope.record = function (celcius) {
                var reading = getProbeReading();
                scope.answer.Celcius = celcius || (reading && reading.value);
                scope.answer.ExtraValues = {
                    RawCelcius: reading && reading.rawValue,
                    ProbeSerialNumber: reading && reading.serialNumber 
                };
                scope.recordedStatus = getReadingPassStatus(scope.answer.Celcius);
            };

            scope.clearRecorded = function () {
                scope.answer.Celcius = null;
                scope.recordedStatus = null;
            }

            if (scope.presented.Answer && angular.isDefined(scope.presented.Answer.Celcius) && scope.presented.Answer.Celcius != null) {
                scope.record(scope.presented.Answer.Celcius);
            }

            scope.tp = temperatureProbe;
            scope.battery = temperatureProbe.battery;

            temperatureProbe.startMonitor(scope.presented.RefreshPeriod);

            scope.allowedProbeProviders = scope.presented.AllowedProbeProviders ? scope.presented.AllowedProbeProviders.split(',') : null;
            scope.allowedProbeSerialNumbers = scope.presented.AllowedProbeSerialNumbers ? scope.presented.AllowedProbeSerialNumbers.split(',') : null;

            // Check if there is a currently active probe and whether its allowed on this question ..
            if (temperatureProbe.probe && scope.allowedProbeSerialNumbers) {
                var allowedProbe = scope.allowedProbeSerialNumbers.find(function (serialNumber) {
                    return temperatureProbe.probe.serialNumber == serialNumber;
                });
               
                if (!allowedProbe) {
                    temperatureProbe.activeProbe(null);
                }
            }
            function resetOutOfRangeTimeout() {
                var probe = temperatureProbe.probe;
                if (probe && scope.presented.ProbeOutOfRangeTimeout) {
                    _cookieTimer.setTimer(probe, scope.presented.ProbeOutOfRangeTimeout * 1000, null, function () {
                        // probe has not reported - remove it, it is out of range
                        probe.remove();
                    })
                }
            }

            function onProbeConnect (probe) {
                if (probe) {
                    temperatureProbe.startMonitor(scope.presented.RefreshPeriod);
                    probe.getSerialNumber().then(function (serialNumber) {
                        persistantStorage.setItem(TEMPERATURE_PROBE_LAST_SERIAL_NUMBER, serialNumber);

                        // Get local calibration data first
                        var tempCalibrationKey = 'Temperature Calibration:' + serialNumber;
                        persistantStorage.getItem(tempCalibrationKey, function (data) {
                            if (data) {
                                temperatureProbe.setCalibration(data);
                            }
                        });

                        // Request latest from server also
                        serverBasedUserSettings.getUserSetting(tempCalibrationKey).then(function (data) {
                            data = data || {};      // If server has removed calibration data then blank it locally too
                            probe.setCalibration(data);
                            persistantStorage.setItem(tempCalibrationKey, data);
                        });
                    });

                    resetOutOfRangeTimeout();
                }
            };

            scope.isActiveProbe = function (probe) {
                return probe === temperatureProbe.probe;
            }

            scope.connect = function (probe) {
                // Reset calibration data by default
                temperatureProbe.setCalibration(null);

                temperatureProbe.activeProbe(probe);
            };

            scope.closeSelectProbe = function () {
                scope.showSelectProbe = false;
                if (angular.isFunction(temperatureProbe.stopScan)) {
                    temperatureProbe.stopScan();
                }
            }

            scope.selectProbe = function () {
                scope.showSelectProbe = true;
                if (angular.isFunction(temperatureProbe.startScan)) {
                    temperatureProbe.startScan();
                }
            }

            scope.$on('$destroy', function () {
                scope.onDestroy.forEach(function (fn) {
                    if (angular.isFunction(fn)) {
                        fn();
                    }
                });
                temperatureProbe.shutdown(scope.presented.DisconnectProbeTimeout);
            });

            function filterProbes(probes) {
                var filtered = [];
                angular.forEach(probes, function (probe) {
                    if (scope.allowedProbeProviders != null && (scope.allowedProbeProviders.indexOf(probe.provider.id) < 0)) {
                        return;
                    }
                    if (scope.allowedProbeSerialNumbers != null && (scope.allowedProbeSerialNumbers.indexOf(probe.serialNumber) < 0)) {
                        return;
                    }
                    if (scope.presented.ProbeOutOfRangeTimeout && moment.duration(moment.utc().diff(getProbeReading(probe).updateTimestamp)).asSeconds() > scope.presented.ProbeOutOfRangeTimeout) {
                        // last reading is too old, probe is out of range
                        return;
                    }
                    filtered.push(probe);
                })
                return filtered;
            }
            scope.probes = filterProbes(temperatureProbe.probes);

            scope.onDestroy.push(temperatureProbe.subscribe(function (event) {
                $log.info('event ' + event.name);
                switch (event.name) {
                    case 'updated':
                        scope.setReading(getProbeReadingValue());
                        resetOutOfRangeTimeout();
                        break;

                    case 'button':
                        if (scope.showSelectProbe) {
                            temperatureProbe.activeProbe(event.probe).then(scope.closeSelectProbe);

                        } else if (temperatureProbe.probe === event.probe) {
                            event.probe.measure().then(scope.record);
                        }
                        break;

                    case 'existing':
                    case 'added':
                        if (scope.showSelectProbe)          // if showing the dialog then user must manually select
                            return;
                        scope.probes = filterProbes(temperatureProbe.probes);
                        if (!temperatureProbe.activeProbe() && !temperatureProbe.connectingProbe) {
                            if (scope.allowedProbeSerialNumbers && scope.allowedProbeSerialNumbers.length == 1) {
                                // We are only allowed to connect to a specific probe, last serial does not matter
                                if (scope.probes.length == 1) {
                                    scope.connect(event.probe);
                                }
                            } else {
                                persistantStorage.getItem(TEMPERATURE_PROBE_LAST_SERIAL_NUMBER, function (lastProbeSerialNumber) {
                                    if (lastProbeSerialNumber) {
                                        event.probe.getSerialNumber().then(function (serialNumber) {
                                            if (serialNumber == lastProbeSerialNumber) {
                                                if (!temperatureProbe.activeProbe() && !temperatureProbe.connectingProbe) {      // Double check not connected
                                                    scope.connect(event.probe);
                                                }
                                            }
                                        })
                                    }
                                });
                            }
                        }
                        break;

                    case 'removed':
                        scope.probes = filterProbes(temperatureProbe.probes);
                        break;

                    case 'probes':
                        scope.probes = filterProbes(event.probes);
                        if (temperatureProbe.activeProbe()) {
                            return;
                        }
                        if (scope.showSelectProbe)
                            return;

                        if (scope.allowedProbeSerialNumbers && scope.allowedProbeSerialNumbers.length == 1) {
                            if (scope.probes.length == 1) {
                                scope.connect(scope.probes[0]);
                            }
                        } else {
                            persistantStorage.getItem(TEMPERATURE_PROBE_LAST_SERIAL_NUMBER, function (lastProbeSerialNumber) {
                                if (lastProbeSerialNumber) {
                                    var probe = scope.probes.find(function (probe) {
                                        return probe.serialNumber == lastProbeSerialNumber;
                                    });
                                    if (probe) {
                                        scope.connect(probe);
                                    }
                                } else if (!scope.showSelectProbe) {
                                    scope.selectProbe();
                                }
                            });
                        }
                        break;

                    case 'connected':
                        temperatureProbe.stopScan();
                        onProbeConnect(event.probe);
                        if (scope.showSelectProbe) {
                            scope.closeSelectProbe();
                        }
                        break;

                    case 'disconnected':
                        temperatureProbe.startScan();
                        break;
                }
            }));

            // Scan for probes at until we connect to a probe
            temperatureProbe.startScan();

        }
    })
}]);

questionsModule.directive('questionPhProbe', ['InstrumentWorksPHProbeSvc', '$filter', 'persistantStorage', 'serverBasedUserSettings', 'bworkflowApi', 'languageTranslate', function (pHProbeSvc, $filter, persistantStorage, serverBasedUserSettings, bworkflowApi, languageTranslate) {
    var PH_PROBE_LAST_SERIAL_NUMBER = 'pHProbe.LastSerialNumber';
    return $.extend({}, questionDirectiveBase, {
        templateUrl: 'question_ph_probe.html',
        link: function (scope, elt, attrs) {
            questionDirectiveBase.link(scope, undefined, bworkflowApi, languageTranslate);
            scope.answer = scope.presented.Answer;
            scope.probe = null;
            scope.showSelectProbe = false;
            scope.showCalibrateProbe = false;
            scope.onDestroy = [];

            // this supports validation as each question type stores it's answer in a different way
            scope.getAnswerValue = function () {
                return scope.answer.pH;
            };

            if (angular.isDefined(pHProbeSvc.supported) == true && pHProbeSvc.supported == false) {
                scope.supported = false;
                return;
            }
            scope.supported = true;


            scope.calibrateProbe = function () {
                scope.calibrationSequence = pHProbeSvc.createCalibrationSequence();
                scope.showCalibrateProbe = true;
            }

            scope.closeCalibrateProbe = function () {
                scope.calibrationSequence = null;
                scope.showCalibrateProbe = false;
            }

            scope.saveCalibration = function () {
                var calibrationData = scope.calibrationSequence.stop();
                if (angular.isDefined(calibrationData)) {
                    serverBasedUserSettings.setUserSetting('pH Calibration:' + scope.probe.serialNumber, calibrationData);
                }
            }

            scope.pH = function (reading) {
                if (reading == null || angular.isUndefined(reading)) {
                    return '';
                }

                var result = scope.calculateReading(reading);

                return result;
            }

            scope.calculateReading = function (reading) {
                return Math.round(reading * 100) / 100.0;
            }

            var getReadingPassStatus = function (pH) {
                if (pH == null || angular.isUndefined(pH)) {
                    return 3;
                }
                // min/max are always pH
                if (pH > scope.presented.MaximumPassReading) {
                    return 2;
                } else if (pH < scope.presented.MinimumPassReading) {
                    return 1;
                }
                return 0;
            }

            scope.setReading = function (reading) {
                scope.reading = scope.calculateReading(reading);
                scope.readingStatus = getReadingPassStatus(reading);
            };

            scope.record = function (reading) {
                scope.answer.pH = scope.calculateReading(reading || pHProbeSvc.current_pH.value);
                scope.recordedStatus = getReadingPassStatus(scope.answer.pH);
            };

            scope.clearRecorded = function () {
                scope.answer.pH = null;
                scope.recordedStatus = null;
            }

            if (scope.presented.Answer && angular.isDefined(scope.presented.Answer.pH) && scope.presented.Answer.pH != null) {
                scope.record(scope.presented.Answer.pH);
            }

            scope.pHProbeSvc = pHProbeSvc;
            scope.battery = pHProbeSvc.battery;

            scope.$watch('pHProbeSvc.current_pH.value', function (newValue, oldValue) {
                if (angular.isDefined(newValue) == false) {
                    return;
                }

                scope.setReading(newValue);
            });

            pHProbeSvc.startMonitor(scope.presented.RefreshPeriod);

            var onProbeConnect = function (probe) {
                scope.probe = probe;
                if (probe && probe.serialNumber) {
                    persistantStorage.setItem(PH_PROBE_LAST_SERIAL_NUMBER, probe.serialNumber);
                }
            };

            scope.probes = [];
            scope.isActiveProbe = function (probe) {
                return probe === pHProbeSvc.getActiveProbe();
            }

            scope.connect = function (probe) {
                if (scope.disposeOnButton) {
                    scope.disposeOnButton();
                    scope.disposeOnButton = null;
                }
                scope.probe = probe;

                serverBasedUserSettings.getUserSetting('pH Calibration:' + scope.probe.serialNumber).then(function (data) {
                    if (data) {
                        probe.setCalibration(data);
                    }
                })

                pHProbeSvc.setActiveProbe(probe, function (connected) {
                    if (connected) {
                        onProbeConnect(probe);
                        // Disconnect will leave showSelectProbe dialog open
                        if (probe) {
                            scope.showSelectProbe = false;
                        }
                    } else {
                        // We disconnected, if we are are not findingProbes then auto look for another
                        if (!scope.showSelectProbe) {
                            scope.findProbes(true);         // Auto connect to 1st
                        }
                    }
                });
            };

            scope.closeSelectProbe = function () {
                scope.showSelectProbe = false;
                if (scope.removeProbesListener) {
                    scope.removeProbesListener();
                    scope.onDestroy.remove(scope.removeProbesListener);
                    scope.removeProbesListener = null;
                }
            }

            scope.selectProbe = function () {
                scope.showSelectProbe = true;

                // Don't auto connect when showing the findProbes dialog (user has to select)
                scope.findProbes(false);
            }

            scope.$on('$destroy', function () {
                scope.onDestroy.forEach(function (fn) {
                    if (angular.isFunction(fn)) {
                        fn();
                    }
                });
                pHProbeSvc.stopMonitor();
            });

            scope.removeProbesListener = null;
            scope.findProbes = function (autoConnect) {
                if (scope.removeProbesListener) {
                    scope.removeProbesListener();
                    scope.onDestroy.remove(scope.removeProbesListener);
                }
                scope.removeProbesListener = pHProbeSvc.addProbesListener(function (probes) {
                    scope.probes = probes;
                    if (pHProbeSvc.getActiveProbe()) {
                        return;
                    }
                    if (autoConnect) {
                        if (probes.length == 1) {
                            scope.connect(probes[0]);
                        } else {
                            persistantStorage.getItem(PH_PROBE_LAST_SERIAL_NUMBER, function (lastProbeSerialNumber) {
                                if (lastProbeSerialNumber) {
                                    var probe = probes.find(function (probe) {
                                        return probe.serialNumber == lastProbeSerialNumber;
                                    });
                                    if (probe) {
                                        scope.connect(probe);
                                    }
                                } else {
                                    scope.showSelectProbe = true;
                                }
                            });
                        }
                    }
                });
                pHProbeSvc.findProbes();
                scope.onDestroy.push(scope.removeProbesListener);
            }

            // Find and connect to 1st available probe
            scope.findProbes(true);
        }
    })
}]);

questionsModule.directive('questionMultiChoice', ['bworkflowApi', 'languageTranslate', function (bworkflowApi, languageTranslate) {
    return $.extend({}, questionDirectiveBase, {
        templateUrl: 'question_multichoice.html',
        link: function (scope, elt, attrs) {
            questionDirectiveBase.link(scope, undefined, bworkflowApi, languageTranslate);
            if (scope.presented.DisplayFormat == 'List' || scope.presented.DisplayFormat == 'List_Horizontal') {
                scope.inputCtrlType = scope.presented.AllowMultiSelect ? 'checkbox' : 'radio';
            } else {
                scope.inputCtrlType = 'select';
            }
            scope.answer = scope.presented.Answer;

            // this supports validation as each question type stores it's answer in a different way
            scope.getAnswerValue = function () {
                return scope.answer.Selected;
            };

            scope.limitSelection = function (max) {
                scope.$watchCollection('answer.Selected', function () {
                    if (angular.isDefined(scope.answer) && angular.isDefined(scope.answer.Selected)) {
                        var count = Object.count(scope.answer.Selected, true);

                        angular.forEach(scope.presented.Choices, function (c) {
                            c.disabled = !scope.answer.Selected[c.Id] && count >= max;
                        });
                    }
                });
            };
        }
    });
}]);

questionsModule.directive('questionCheckbox', ['$sce', 'bworkflowApi', 'languageTranslate', function ($sce, bworkflowApi, languageTranslate) {
    return $.extend({}, questionDirectiveBase, {
        templateUrl: 'question_checkbox.html',
        link: function (scope, elt, attrs) {
            questionDirectiveBase.link(scope, undefined, bworkflowApi, languageTranslate);
            if (typeof (attrs.renderPrompt) === 'undefined')
                scope.shouldRenderPrompt = true;
            else
                scope.shouldRenderPrompt = attrs.renderPrompt.ToLower() == 'true';

            scope.presented.promptAsHtml = $sce.trustAsHtml(scope.presented.Prompt);

            //since angular won't parse the string replace in the style
            $(elt).attr('style', scope.presented.HtmlPresentation.CustomStyling);
        }
    });
}]);

questionsModule.directive('questionLabel', ['$interpolate', 'sharedScope', 'languageTranslate', function ($interpolate, sharedScope, languageTranslate) {
    return $.extend({}, questionDirectiveBase, {
        templateUrl: 'question_label.html',
        link: function (scope, elt, attrs) {
            questionDirectiveBase.link(scope, undefined, undefined, languageTranslate);
            scope.showHelp = function () {
                scope.$emit('help_start', {});
            };

            scope.raiseEvent = function (eventName, args) {
                // allows someone to raise an event to get other elements to do things,
                // for example clicking a button in the label refreshing a task list
                scope.$emit('player_broadcast', { name: eventName, data: args });
            };

            scope.openWindow = function (url, target) {
                window.open(url, target);

                return false;
            };

            if (scope.presented.Listen) {
                scope.sharedScope = sharedScope.shared;

                var rootObj = scope.presented.ListenTo;
                var i = rootObj.indexOf('.');
                if (i != -1)
                    rootObj = rootObj.first(i);

                scope[rootObj] = sharedScope.get(rootObj);

                scope.isVisible = function () {
                    return scope.$eval('sharedScope.' + scope.presented.ListenTo);
                }
            }
        }
    });
}]);

questionsModule.directive('questionSignature', ['bworkflowApi', 'languageTranslate', function (bworkflowApi, languageTranslate) {
    return $.extend({}, questionDirectiveBase, {
        templateUrl: 'question_signature.html',
        link: function (scope, elt, attrs) {
            questionDirectiveBase.link(scope, undefined, bworkflowApi, languageTranslate);
            scope.answer = scope.presented.Answer;

            // this supports validation as each question type stores it's answer in a different way
            scope.getAnswerValue = function () {
                return $(elt).find('input').val();
            };

            scope.$on('signatureReady', function (ev, elt) {
                var hidden = elt.find("input");
                var canvas = elt.find("canvas");
                var sigWrapper = elt.find(".sigWrapper");

                var dimensionAsCss = function (dim) {
                    var result = dim.Value.toString();

                    if (dim.Type == "Pixels") {
                        result = result + "px";
                    } else {
                        result = result + "%";
                    }

                    return result;
                };

                elt.attr("style", "width:" + dimensionAsCss(scope.presented.Width));
                sigWrapper.attr("style", "height:" + dimensionAsCss(scope.presented.Height));
                canvas.attr("width", scope.presented.Width.Value);
                canvas.attr("height", scope.presented.Height.Value);

                scope.signaturePad = elt.signaturePad({ output: hidden, validateFields: false, drawOnly: true, lineTop: scope.presented.Height.Value - 20 });

                if (scope.answer.Signature != null && scope.answer.Signature != "") {
                    scope.signaturePad.regenerate(scope.answer.Signature);
                }

                ev.stopPropagation();
            });

            scope.clearSignature = function () {
                scope.signaturePad.clearCanvas();
            }

            scope.$on('populateAnswer', function (ev) {
                scope.answer.Signature = $(elt).find('input').val(); //Need to update answer model
            });
        }
    });
}]);

// directive that is used by the signature question type to actually put in place the signature object and other script related stuff
questionsModule.directive('signaturepad', [function () {
    return {
        scope: {
            signaturedata: '=',
        },
        link: function (scope, elt, attrs) {
            // all we do is raise an event so that the parent scope can get notified that the HTML elements are in place
            // for the signature stuff to be put in place.
            scope.$emit('signatureReady', elt, scope.signaturedata);
        }
    }
}]);

questionsModule.directive('questionSelectUser', ['bworkflowApi', 'languageTranslate', function (bworkflowApi, languageTranslate) {
    return $.extend({}, questionDirectiveBase, {
        templateUrl: 'question_selectuser.html',
        link: function (scope, elt, attrs) {
            questionDirectiveBase.link(scope, undefined, bworkflowApi, languageTranslate);
            scope.answer = scope.presented.Answer;

            scope.currentSelection = null;

            // this supports validation as each question type stores it's answer in a different way
            scope.getAnswerValue = function () {
                return scope.answer.SelectedId;
            };

            scope.updateSelection = function (id, name) {
                scope.answer.SelectedId = id;
                scope.answer.SelectedName = name;
            };

            scope.options = [];
            bworkflowApi.labelUsers(scope.presented.AllowedLabels).then(function (r) {
                scope.options = r;
            });

            scope.clearSources = function () {
                if (scope.presented.cleardatasources != null && scope.presented.cleardatasources != '') {
                    var sources = scope.presented.cleardatasources.split(',');
                    var fields = null;
                    if (scope.currentSelection != null) {
                        fields = scope.currentSelection;
                    }

                    angular.forEach(sources, function (source) {
                        var promise = bworkflowApi.getDataFeed(source);

                        if (promise != null) {
                            promise.then(function (toUpdate) {
                                // copy across what was selected into the parameters for the feed
                                toUpdate.clearParameters(fields);
                            });
                        }
                    });
                }
            };

            scope.updateSources = function () {
                if (scope.presented.updatedatasources != null && scope.presented.updatedatasources != '') {
                    var sources = scope.presented.updatedatasources.split(',');

                    angular.forEach(sources, function (source) {
                        var promise = bworkflowApi.getDataFeed(source);

                        if (promise != null) {
                            promise.then(function (toUpdate) {
                                // we store what the current selection is so that we can clear only what we've set
                                // in the clear source method (if its called)
                                scope.currentSelection = angular.copy(scope.watchcontainer.currentUserObject.alldata);

                                angular.forEach(scope.watchcontainer.currentUserObject.alldata, function (value, key) {
                                    toUpdate.parameters[key] = value;
                                });

                                // get data and force a refresh
                                toUpdate.getData(true);
                            });
                        }
                    });
                }
            };

            scope.selectUser = function () {
                if (scope.watchcontainer == null || scope.watchcontainer.currentUserObject == null || scope.watchcontainer.currentUserObject.alldata == null || scope.watchcontainer.currentUserObject.alldata.UserId == null) {
                    return;
                }

                scope.answer.SelectedId = scope.watchcontainer.currentUserObject.alldata.UserId;
                scope.answer.SelectedName = scope.watchcontainer.currentUserObject.alldata.Name;

                scope.clearSources();
                scope.updateSources();
            };

            if (scope.presented.datasource != null) {
                scope.watchcontainer = {};
                scope.watchcontainer.currentUserObject = { id: '', name: '' };

                if (scope.presented.Answer.SelectedId != null) {
                    scope.watchcontainer.currentUserObject.id = scope.answer.SelectedId;
                    scope.watchcontainer.currentUserObject.name = scope.answer.SelectedName;
                }

                scope.$watch('watchcontainer.currentUserObject', function (newUser) {
                    if (!newUser || angular.isDefined(newUser.alldata) == false) {
                        return;
                    }

                    scope.answer.SelectedId = newUser.alldata.UserId;
                    scope.answer.SelectedName = newUser.alldata.Name;

                    scope.clearSources();
                    scope.updateSources();
                });
            }
        },
        controller: function ($scope) {
            if ($scope.presented.datasource == null) {
                return;
            }

            $scope.asyncResults = null;

            $scope.options = {
                highlight: true
            };

            $scope.userDataset = {
                displayKey: function (suggestion) {
                    if (angular.isDefined(suggestion.alldata) == false) {
                        return;
                    }

                    return suggestion.alldata.Name
                },
                source: function (query, syncResults, asyncResults) {
                    $scope.asyncResults = syncResults;

                    var fieldName = 'search';
                    if ($scope.presented.Name != null && $scope.presented.Name != '') {
                        fieldName = $scope.presented.Name + '-search';
                    }

                    $scope.feed.parameters[fieldName] = query;
                    $scope.feed.getData(true);
                },
                async: true,
                templates: {
                    suggestion: Handlebars.compile('{{alldata.Name}}')
                }
            };

            var promise = bworkflowApi.getDataFeed($scope.presented.datasource);

            if (promise != null) {
                promise.then(function (feed) {
                    feed.afterLoadHooks.push(function (feed) {
                        if ($scope.asyncResults != null) {
                            $scope.asyncResults(feed.data);
                        }
                    });

                    $scope.feed = feed;
                    $scope.feed.allowMultipleAjax = true;           // EVS-1404 fix - For searching the user could type quicker than we can search, allowing multiple Ajax means we will always catch the users last search query
                });
            }
        }
    });
}]);

questionsModule.directive('questionSelectOdata', ['bworkflowApi', 'languageTranslate', function (bworkflowApi, languageTranslate) {
    return $.extend({}, questionDirectiveBase, {
        templateUrl: 'question_selectodata.html',
        link: function (scope, elt, attrs) {
            questionDirectiveBase.link(scope, undefined, bworkflowApi, languageTranslate);
            scope.answer = scope.presented.Answer;

            scope.currentSelection = null;

            // this supports validation as each question type stores it's answer in a different way
            scope.getAnswerValue = function () {
                return scope.answer.Selected;
            };
           
            scope.options = [];

            scope.clearSources = function () {
                if (scope.presented.cleardatasources != null && scope.presented.cleardatasources != '') {
                    var sources = scope.presented.cleardatasources.split(',');
                    var fields = null;
                    if (scope.currentSelection != null) {
                        fields = scope.currentSelection;
                    }

                    angular.forEach(sources, function (source) {
                        var promise = bworkflowApi.getDataFeed(source);

                        if (promise != null) {
                            promise.then(function (toUpdate) {
                                // copy across what was selected into the parameters for the feed
                                toUpdate.clearParameters(fields);
                            });
                        }
                    });
                }
            };

            scope.updateSources = function () {
                if (scope.presented.updatedatasources != null && scope.presented.updatedatasources != '') {
                    var sources = scope.presented.updatedatasources.split(',');

                    angular.forEach(sources, function (source) {
                        var promise = bworkflowApi.getDataFeed(source);

                        if (promise != null) {
                            promise.then(function (toUpdate) {
                                // we store what the current selection is so that we can clear only what we've set
                                // in the clear source method (if its called)
                                scope.currentSelection = angular.copy(scope.watchcontainer.currentObject.alldata);

                                angular.forEach(scope.watchcontainer.currentObject.alldata, function (value, key) {
                                    toUpdate.parameters[key] = value;
                                });

                                // get data and force a refresh
                                toUpdate.getData(true);
                            });
                        }
                    });
                }
            };

            scope.watchcontainer = {};
            scope.watchcontainer.currentObject = { };

            if (scope.presented.Answer.Selected != null) {
                scope.watchcontainer.currentObject = scope.answer.Selected;
            }

            scope.$watch('watchcontainer.currentObject', function (newValue) {
                if (!newValue || angular.isDefined(newValue.alldata) == false) {
                    return;
                }

                scope.answer.Selected = newValue.alldata;

                scope.clearSources();
                scope.updateSources();
            });
        },
        controller: function ($scope) {
            if ($scope.presented.datasource == null) {
                return;
            }

            $scope.asyncResults = null;

            $scope.options = {
                highlight: true
            };

                $scope.odataDataset = {
                    displayKey: function (suggestion) {
                        if (angular.isDefined(suggestion.alldata) == false) {
                            return;
                        }

                        return suggestion.alldata.Name;
                    },
                    source: function (query, syncResults, asyncResults) {
                        $scope.asyncResults = syncResults;

                        var fieldName = 'search';
                        if ($scope.presented.Name != null && $scope.presented.Name != '') {
                            fieldName = $scope.presented.Name + '-search';
                        }

                        $scope.feed.parameters[fieldName] = query;
                        $scope.feed.getData(true);
                    },
                    async: true,
                    templates: {
                        suggestion: Handlebars.compile('{{alldata.Name}}')
                    }
                };

            bworkflowApi.getDataFeed($scope.presented.datasource).then(function(feed) {
                feed.afterLoadHooks.push(function (feed) {
                    if ($scope.asyncResults != null) {
                        $scope.asyncResults(feed.data);
                    }
                });

                $scope.feed = feed;
                $scope.feed.allowMultipleAjax = true;           // EVS-1404 fix - For searching the user could type quicker than we can search, allowing multiple Ajax means we will always catch the users last search query
            });
        }
    });
}]);


questionsModule.directive('questionSelectCompany', ['bworkflowApi', '$timeout', 'languageTranslate', function (bworkflowApi, $timeout, languageTranslate) {
    return $.extend({}, questionDirectiveBase, {
        templateUrl: 'question_select_company.html',
        link: function (scope, elt, attrs) {
            questionDirectiveBase.link(scope, undefined, bworkflowApi, languageTranslate);

            // this supports validation as each question type stores it's answer in a different way
            scope.getAnswerValue = function () {
                return scope.answer.SelectedId;
            };
        },
        controller: function ($scope) {
            $scope.answer = $scope.presented.Answer;

            $scope.options = {
                highlight: true
            };

            $scope.watchcontainer = {};
            $scope.watchcontainer.currentCompanyObject = {};

            if ($scope.answer.SelectedId != null) {
                $scope.watchcontainer.currentCompanyObject.id = $scope.answer.SelectedId;
                $scope.watchcontainer.currentCompanyObject.name = $scope.answer.SelectedName;
            }

            $scope.companySuggestion = new Bloodhound({
                datumTokenizer: function (d) { return Bloodhound.tokenizers.whitespace(d.name); },
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                remote: {
                    url: window.razordata.apiprefix + 'Player/Execute?query=%QUERY',
                    replace: function (url, query) {
                        $scope.query = query;
                        return url + "#" + query;
                    },
                    ajax: {
                        beforeSend: function (jqXhr, settings) {
                            settings.data = JSON.stringify({
                                Handler: 'CompanyDirectory', Method: 'Search', parameters: {
                                    text: $scope.query,
                                    page: 1,
                                    count: 5,
                                    userid: $scope.presented.userid
                                },
                            });

                            jqXhr.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
                        },
                        type: "POST"
                    },
                    filter: function (data) {
                        return data.companies;
                    }
                }
            });

            $scope.companySuggestion.initialize();

            $scope.companyDataset = {
                displayKey: 'name',
                source: $scope.companySuggestion.ttAdapter(),
                templates: {
                    suggestion: Handlebars.compile('<strong>{{name}}</strong><br/><small><p class="muted">{{address.street}}, {{address.town}}, {{address.state}}</p></small>')
                }
            };

            $scope.$watch('watchcontainer.currentCompanyObject', function (newCompany) {
                if (!newCompany || angular.isDefined(newCompany.id) == false) {
                    return;
                }

                $scope.answer.SelectedId = newCompany.id;
                $scope.answer.SelectedName = newCompany.name;
            });

            $scope.clearAnswer = function () {
                $scope.watchcontainer.currentCompanyObject = { id: null, name: null };
            };
        }
    })
}]);

questionsModule.directive('questionMediaDirectory', ['bworkflowApi', '$rootScope', '$timeout', 'sharedScope', 'languageTranslate', function (bworkflowApi, $rootScope, $timeout, sharedScope, languageTranslate) {
    return $.extend({}, questionDirectiveBase, {
        templateUrl: 'question_media_directory.html',
        link: function (scope, elt, attrs) {
            questionDirectiveBase.link(scope, undefined, bworkflowApi, languageTranslate);
            scope.answer = scope.presented.Answer;

            scope.userSelection = null;

            var parameters = { showpageviewings: scope.presented.showpageviewings, userid: scope.presented.userid };

            // This object is injected into our ABN directive, ABN will populate it with tree control functions we can call
            scope.treeControl = {};

            scope.getFolders = function () {
                bworkflowApi.execute('MediaDirectory', 'GetFolders', parameters)
                .then(function (data) {
                    angular.forEach(data.folders, function (node, key) {
                        scope.setFolderIcons(node);
                    });

                    scope.folders = data.folders;

                    // And expand them all
                    $timeout(function () {
                        if (scope.treeControl.expand_all)
                            scope.treeControl.expand_all();
                    });
                }, function (tasks) {

                });
            };

            // On language change, 
            $rootScope.$on('$translateChangeSuccess', scope.getFolders);

            scope.setFolderIcons = function (node) {
                if (node.data.isfile == true) {
                    if (node.data.viewstatus == "unread") {
                        node.notification = { type: 'success', text: 'new' };
                    }
                    else if (node.data.viewstatus == "read previous") {
                        node.notification = { type: 'warning', text: 'updated' };
                    }

                    return;
                }

                if (node.children.length > 0) {
                    angular.forEach(node.children, function (child, key) {
                        scope.setFolderIcons(child);
                    });

                    return;
                }

                node.iconLeaf = "icon-folder-open";
            }

            scope.selectitem = function (branch) {
                var event = {
                    selectedfolderid: null,
                    selectedmediaid: null,
                    selectedmediashowviachecklistid: null,
                    type: null
                }

                if (branch.data.isfile == true) {
                    scope.item = branch;

                    scope.answer.SelectedId = branch.data.id;

                    // Search up throught tree for a populated "ShowViaChecklistId" property
                    var findNode = branch;
                    while (findNode && !findNode.data.showviachecklistid) {
                        findNode = scope.treeControl.get_parent_branch(findNode);
                    }

                    if (scope.presented.Name != null) {
                        event.selectedmediaid = branch.data.id;
                        event.type = 'mediafile';
                        event.selectedmediashowviachecklistid = findNode != null ? findNode.data.showviachecklistid : 1;
                    }
                } else {
                    scope.item = null;

                    if (scope.presented.Name != null) {
                        event.type = 'mediafolder';
                        event.selectedfolderid = branch.data.id;
                    }
                }

                if (scope.presented.Name != null) {
                    sharedScope.set(scope.presented.Name, event);

                    scope.$emit('player_broadcast', { name: scope.presented.Name, data: event });
                }
            }

            scope.getFolders();
        }
    });
}]);

questionsModule.directive('questionMediaViewingSummary', ['bworkflowApi', '$timeout', '$sce', 'languageTranslate', function (bworkflowApi, $timeout, $sce, languageTranslate) {
    return $.extend({}, questionDirectiveBase, {
        templateUrl: 'question_media_viewing_summary.html',
        link: function (scope, elt, attrs) {
            questionDirectiveBase.link(scope, undefined, bworkflowApi, languageTranslate);
            scope.answer = scope.presented.Answer;
            scope.selectedfolderid = null;

            scope.loadFolderSummary = function (folderid) {
                scope.selectedfolderid = folderid;

                var parameters = { folderid: folderid };

                bworkflowApi.execute('MediaViewingSummary', 'GetFolderSummary', parameters)
                .then(function (data) {
                    scope.data = data;
                }, function (tasks) {

                });
            };

            scope.showUserDetails = function (summary) {
                if (angular.isDefined(summary.showdetails) == false || summary.showdetails == false) {
                    summary.showdetails = true;
                }
                else {
                    summary.showdetails = false;
                }

                if (angular.isDefined(summary.userdetail) == true) {
                    return;
                }

                var parameters = { folderid: scope.selectedfolderid, userid: summary.userid };

                bworkflowApi.execute('MediaViewingSummary', 'GetFolderUserDetail', parameters)
                .then(function (data) {
                    angular.forEach(data.views, function (view, key) {
                        if (view.accepted == false) {
                            view.acceptNoteAsHtml = $sce.trustAsHtml(view.acceptionnotes);
                        }
                    });

                    summary.userdetail = data;
                }, function (tasks) {

                });
            };

            scope.toDuration = function (seconds) {
                return moment.duration(seconds, "seconds").humanize();
            };

            if (scope.presented.listen == true) {
                scope.$on(scope.presented.listento, function (event, args) {
                    if (args.type == 'mediafolder') {
                        scope.loadFolderSummary(args.selectedfolderid);
                    }
                });
            }
        }
    });
}]);

//questionsModule.directive('questionMediaViewingSummaryGauge', ['$translate',
//    function ($translate) {
//        return {
//            restrict: 'A',
//            require: 'ngModel',
//            scope: {
//                summary: '=ngModel',
//                usercount: '='
//            },
//            link: function (scope, elt, attrs, ngModel) {
//                scope.gaugeOptions = {

//                    chart: {
//                        type: 'solidgauge',
//                        backgroundColor: 'rgba(255,255,255,0.65)'
//                    },

//                    title: null,

//                    pane: {
//                        center: ['50%', '85%'],
//                        size: '140%',
//                        startAngle: -90,
//                        endAngle: 90,
//                        background: {
//                            backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
//                            innerRadius: '60%',
//                            outerRadius: '100%',
//                            shape: 'arc'
//                        }
//                    },

//                    tooltip: {
//                        enabled: false
//                    },

//                    // the value axis
//                    yAxis: {
//                        lineWidth: 0,
//                        minorTickInterval: null,
//                        tickPixelInterval: 400,
//                        tickWidth: 0,
//                        title: {
//                            y: -70
//                        },
//                        labels: {
//                            y: 16
//                        }
//                    },

//                    plotOptions: {
//                        solidgauge: {
//                            dataLabels: {
//                                y: 5,
//                                borderWidth: 0,
//                                useHTML: true
//                            }
//                        }
//                    }
//                };

//                var chartOpts = Highcharts.merge(scope.gaugeOptions, {
//                    yAxis: {
//                        min: 0,
//                        max: scope.usercount,
//                    },

//                    credits: {
//                        enabled: false
//                    },

//                    series: [{
//                        name: $translate.instant('Speed'),
//                        data: [scope.summary.distinctreads],
//                        dataLabels: {
//                            format: $translate.instant('<div style="text-align:center"><span style="font-size:25px;color:' +
//                                ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
//                                   '<span style="font-size:12px;color:silver">users have read</span></div>')
//                        },
//                        tooltip: {
//                            valueSuffix: ''
//                        }
//                    }]

//                });

//                chartOpts.chart.renderTo = $(elt)[0];
//                //chartOpts.chart.width = 200;
//                var hc = new Highcharts.Chart(chartOpts);
//            }
//        };
//    }
//]);

questionsModule.directive('questionUploadMedia', ['bworkflowApi', '$timeout', '$state', '$filter', 'languageTranslate', function (bworkflowApi, $timeout, $state, $filter, languageTranslate) {
    return $.extend({}, questionDirectiveBase, {
        templateUrl: 'question_uploadmedia.html',
        link: function (scope, elt, attrs) {
            questionDirectiveBase.link(scope, undefined, bworkflowApi, languageTranslate);

            scope.answer = scope.presented.Answer;
            scope.state = 'list';

            scope.maxHeight = scope.presented.options.maxHeight;
            scope.maxWidth = scope.presented.options.maxWidth;

            // this supports validation as each question type stores it's answer in a different way
            scope.getAnswerValue = function () {
                return scope.d.mediaItems.length == 0 ? '' : scope.d.mediaItems.length;
            };

            var addPhotoFile = function (file, cb) {
                window.SquareIT.BWorkflow.HandleUploads({
                    maxWidth: scope.maxWidth,
                    maxHeight: scope.maxHeight,
                    allowImage: true,
                    files: [file]
                }, function (files) {
                    $timeout(function () {
                        scope.d.mediaItems = scope.d.mediaItems.concat(files);

                        if (cb) {
                            cb();
                        }
                    });
                });
            }

            scope.takePhoto = function () {
                if (window.cordova && navigator.camera) {
                    navigator.camera.getPicture(function (imageUri) {
                        window.resolveLocalFileSystemURL(imageUri, function (fileEntry) {
                            fileEntry.file(function (file) {
                                addPhotoFile(file, function () {
                                    navigator.camera.cleanup();
                                });
                            });
                        });
                    }, function (error) {
                        $timeout(function () {
                            alert('Error taking photo\n' + error);
                        });
                    }, {
                        // Some common settings are 20, 50, and 100
                        quality: 50,
                        destinationType: Camera.DestinationType.FILE_URI,
                        // In this app, dynamically set the picture source, Camera or photo gallery
                        sourceType: Camera.PictureSourceType.CAMERA,
                        encodingType: Camera.EncodingType.JPEG,
                        mediaType: Camera.MediaType.PICTURE,
                        allowEdit: false,
                        correctOrientation: false  //Corrects Android orientation quirks
                    });
                } else {
                    $(elt.find('.camera_input')[0]).trigger('click');
                }
            };

            scope.photoTaken = function (element) {
                if (element.files.length === 0) return;

                addPhotoFile(element.files[0]);
            };

            scope.deletePhoto = function (item) {
                if (confirm("Are you sure you want to remove this photo?") == true) {
                    var index = scope.d.mediaItems.indexOf(item);
                    if (index != -1)
                        scope.d.mediaItems.splice(index, 1);
                }
            };

            scope.getMediaLink = function (id) {
                return scope.getFullServerPath('/Media/GetMediaItem/' + id);
            };

            scope.d = {};
            scope.d.mediaItems = $.map(scope.presented.Answer.Items, function (mi) {
                var newMi = mi; //reference copy, but we're allowed to clobber scope.presented.Answer anyway

                //we have mediaIds, code up Image objects
                //also set type to 'image' if needed
                if (window.isImage(mi.fname)) {
                    newMi.type = 'image';
                } else {
                    newMi.type = 'file';
                }

                if (newMi.type == 'image') {
                    newMi.data = new Image();
                    newMi.data.src = $filter('mediaItem')(newMi.mediaId);
                }
                return newMi;
            });

            scope.$on('populateAnswer', function (ev) {
                //need to fill up scope.Answer
                //mediaItems is a good start
                scope.answer.Items = scope.d.mediaItems;
                //but we need to strip out Files and Images and replace them with base64 coded strings
                for (var i = 0; i < scope.answer.Items.length; i++) {
                    var item = scope.answer.Items[i];
                    if (item.mediaId) {
                        //If tghe object's mediaid is set, it (as in it's data) hasn't been modified at all
                        //(i.e. this means, hasn't been drawn on)
                        //as an optimisation, don't reupload, just allow the server to look up the media by ID
                        item.data = null;
                    }
                    else if (item.type == 'image') {
                        //pull something out of the data url
                        //starts with data:image/{png,jpg};base64,
                        //look for base64, and start trim before that
                        var b64idx = item.data.src.indexOf('base64,');
                        item.data = item.data.src.substring(b64idx + 7);
                    }
                    else if (item.type == 'file') {
                        //base64 code the binary string
                        item.data = window.btoa(item.data);
                    }
                }

            });

        }
    });
}]);

questionsModule.directive('imageEditor', [function () {
    return {
        scope: {
            image: '=',
            maxHeight: '='
        },
        restrict: 'EA',
        templateUrl: 'question_uploadmedia_canvas.html',
        link: function (scope, elt, attrs) {

            var canvasParent = $(elt).find('div.uploadmedia-canvascontainer');
            var editCanvasElt = $(elt).find('canvas.uploadmedia-editcanvas').get(0);

            var editCtx = $(editCanvasElt).get(0).getContext('2d');

            var sizeEditCanvas = function () {
                //first resize the actual canvas elt
                //what's our max width?
                var maxWidth = $(canvasParent).width();
                var maxHeight = scope.maxHeight;
                var aspectRatio = scope.image.width / scope.image.height;
                if (maxWidth / aspectRatio > maxHeight) {
                    //we would be too high, clamp height
                    var w = maxHeight * aspectRatio;
                    $(editCanvasElt).width(w);
                    $(editCanvasElt).height(maxHeight);
                } else {
                    //we would be too wide, clamp width
                    var h = maxWidth / aspectRatio;
                    $(editCanvasElt).width(maxWidth);
                    $(editCanvasElt).height(h);
                }
            };
            var loadImage = function () {
                editCanvasElt.width = scope.image.width;
                editCanvasElt.height = scope.image.height;
                editCtx.drawImage(scope.image, 0, 0, scope.image.width, scope.image.height);
            };

            var onImgChange = function () {
                if (typeof scope.image === 'undefined') return;
                loadImage();
                sizeEditCanvas();
            };

            var onWindowScale = function () {
                if (typeof scope.image === 'undefined') return;
                sizeEditCanvas();
            };

            $(window).on('resize', onWindowScale);
            scope.$watch('image', onImgChange);
            onImgChange();

        }
    };
}]);

questionsModule.controller('uploadMediaSlideCtrl', ['$scope', '$timeout', '$anchorScroll', '$location', function ($scope, $timeout, $anchorScroll, $location) {
    $scope.goBack = function () {
        $scope.$emit('unSlideModal');
    };

    //what kind of file upload mode will we support?
    if (!window.mobile && Modernizr.draganddrop && window.SquareIT.BWorkflow.HandleUploads) {

        //show drag and drop one
        $scope.fileSelectMode = 'draganddrop';

        $('.uploadmedia-drop-target').on('dragenter', function (ev) {
            ev.stopPropagation();
            ev.preventDefault();
            $(ev.target).addClass('uploadmedia-drop-target-hover');
        });

        $('.uploadmedia-drop-target').on('dragleave', function (ev) {
            $(ev.target).removeClass('uploadmedia-drop-target-hover');
        });

        $('.uploadmedia-drop-target').on('dragover', function (ev) {
            ev.stopPropagation();
            ev.preventDefault();
            ev.originalEvent.dataTransfer.dropEffect = 'copy';
        });

        $('.uploadmedia-drop-target').on('drop', function (ev) {
            ev.stopPropagation();
            ev.preventDefault();
            $(ev.target).removeClass('uploadmedia-drop-target-hover');

            var dt = ev.originalEvent.dataTransfer;
            var files = dt.files;

            window.SquareIT.BWorkflow.HandleUploads({
                files: files,
                maxWidth: $scope.maxWidth,
                maxHeight: $scope.maxHeight
            }, function (resizedFiles) {
                $timeout(function () {
                    $scope.d.mediaItems = $scope.d.mediaItems.concat(resizedFiles);
                });
            });
        });

        //need to disable pointer events on drop target's children so that drag/drop works as expected
        $timeout(function () {
            $('.uploadmedia-drop-target').children().css('pointer-events', 'none');
        });
    } else {

        //show input
        $scope.fileSelectMode = 'fileinput';

        //timeout to wait for the switch DOM to render
        /*$timeout(function () {
            $('.uploadmedia-fileinput-button').on('click', function(ev) {
                window.SquareIT.TakePhoto
                ({
                        maxWidth: $scope.maxWidth,
                        maxHeight: $scope.maxHeight
                }, function (files) {
                    $timeout(function() {
                        $scope.d.mediaItems = $scope.d.mediaItems.concat(files);
                    });
                });
            });
        });*/

    }

    //give the editor an anchor
    $scope.editorAnchorId = 'x' + window.rstring(8);

    $scope.isImage = function (mi) {
        return window.isImage(mi.fname);
    };

    $scope.doEdit = function (mi) {
        $scope.editing = true;
        console.log('editing');
        $scope.editTarget = mi;
        $location.hash($scope.editorAnchorId);
        $anchorScroll();
    };

    $scope.removeItem = function (mi) {
        var index = $scope.d.mediaItems.indexOf(mi);
        if (index != -1)
            $scope.d.mediaItems.splice(index, 1);
        if (mi == $scope.editTarget) {
            $scope.editTarget = undefined;
            $scope.editing = false;
        }
    };
}]);

questionsModule.directive('questionPresentWordDocument', ['bworkflowApi', 'languageTranslate', function (bworkflowApi, languageTranslate) {
    return $.extend({}, questionDirectiveBase, {
        templateUrl: 'question_present_word_document.html',
        link: function (scope, elt, attrs) {
            questionDirectiveBase.link(scope, undefined, bworkflowApi, languageTranslate);

            scope.answer = scope.presented.Answer;

            scope.backwards = function () {
                if (scope.pageNumber == 1) {
                    return;
                }

                scope.pageNumber = scope.pageNumber - 1;
                scope.answer.lastpageread = scope.pageNumber;
                scope.notifyPageView(scope.pageNumber);

                scope.calculatePageUrl();
            };

            scope.forwards = function () {
                if (scope.presented.pagecount != null && scope.pageNumber >= scope.presented.pagecount) {
                    return;
                }

                scope.pageNumber = scope.pageNumber + 1;
                scope.answer.lastpageread = scope.pageNumber;
                scope.notifyPageView(scope.pageNumber);

                if (scope.pageNumber == scope.presented.pagecount - 1) {
                    scope.answer.allpagesread = true;
                }

                scope.calculatePageUrl();
            };

            scope.calculatePageUrl = function () {
                if (scope.presented.mergewithchecklist == true || scope.presented.pagecount == null) {
                    scope.mediaurl = bworkflowApi.getfullurl("~/DocumentPreviewMediaImage/" + scope.presented.workingdocumentid + ".png?mediaid=" + scope.presented.mediaid + "&scale=1&pageindex=" + (scope.pageNumber - 1) + "&pagecount=1&resolution=" + scope.presented.resolution + "&usehighquality=" + scope.presented.usehighquality + "&useantialiasing=" + scope.presented.useantialiasing);
                }
                else {
                    scope.mediaurl = bworkflowApi.getfullurl("~/MediaPreviewImage/" + scope.presented.mediaid + ".png?scale=1&pageindex=" + (scope.pageNumber - 1) + "&version=" + scope.presented.version); // version included to make things cache friendly
                }
            };

            scope.notifyPageView = function (toPage) {
                if (scope.presented.trackviewing == false) {
                    return;
                }

                var parameters = {
                    userid: scope.presented.userid,
                    workingdocumentid: scope.presented.workingdocumentid,
                    mediaid: scope.presented.mediaid,
                    currentpageview: scope.currentpageview,
                    nextpage: toPage
                };

                bworkflowApi.execute('MediaViewing', 'Notify', parameters)
                .then(function (data) {
                    scope.currentpageview = data.currentpageview;
                }, function (tasks) {

                });
            };

            // if the user is on the last page, they'll hit the next button of the player, so
            // we need to track that final page read on movement in the player
            scope.$on('populateAnswer', function (ev) {
                scope.notifyPageView(null);
            });

            scope.answer = scope.presented.Answer;
            scope.currentpageview = null;
            scope.pageNumber = scope.answer.lastpageread - 1;

            if (scope.pageNumber < 0) {
                scope.pageNumber = 0;
            }

            scope.forwards();



        }
    });
}]);

//Geolocation question (geocode)
questionsModule.directive('questionGeocode', ['bworkflowApi', 'languageTranslate', function (bworkflowApi, languageTranslate) {
    return $.extend({}, questionDirectiveBase, {
        templateUrl: 'question_geocode.html',
        link: function (scope, elt, attrs) {
            questionDirectiveBase.link(scope, undefined, bworkflowApi, languageTranslate);

            scope.locateStatus = 'locating';
            scope.answer = scope.presented.Answer;

            //first thing to do is try and geolocate
            bworkflowApi.getGPSLocation()
                .then(function (gpsval) {
                    scope.locateStatus = 'success';
                    scope.coords = gpsval.coords;
                })['catch'](function (ex) { //Can't use .catch() because it's a reserved word and IE8 will barf
                    scope.locateStatus = 'failure';
                    scope.errorMessage = ex.message;
                });

            scope.$on('populateAnswer', function (ev) {
                if (scope.locateStatus == 'locating') {
                    scope.answer.Error = 'The checklist was advanced before a location was acquired';
                } else if (scope.locateStatus == 'failure') {
                    scope.answer.Error = scope.errorMessage;
                } else if (scope.locateStatus == 'success') {
                    scope.answer.Latitude = scope.coords.latitude;
                    scope.answer.Longitude = scope.coords.longitude;
                    scope.answer.Accuracy = scope.coords.accuracy;
                }
            });

        }
    });
}]);

questionsModule.directive('questionDatatable', ['$timeout', 'bworkflowApi', 'languageTranslate', function ($timeout, bworkflowApi, languageTranslate) {
    return $.extend({}, questionDirectiveBase, {
        templateUrl: 'question_datatable.html',
        link: function (scope, elt, attrs) {
            questionDirectiveBase.link(scope, undefined, bworkflowApi, languageTranslate);
            scope.feed = null;

            scope.displayContainer = { displayfields: [] };

            scope.selectedRow = null;

            scope.clearSources = function () {
                if (scope.presented.cleardatasources != null && scope.presented.cleardatasources != '') {
                    var sources = scope.presented.cleardatasources.split(',');

                    var fields = null;
                    if (scope.selectedRow != null) {
                        fields = scope.selectedRow.alldata;
                    }

                    angular.forEach(sources, function (source) {
                        var promise = bworkflowApi.getDataFeed(source);

                        if (promise != null) {
                            promise.then(function (toUpdate) {
                                // copy across what was selected into the parameters for the feed
                                toUpdate.clearParameters(fields);
                            });
                        }
                    });
                }
            };

            scope.rowclicked = function (row) {
                scope.clearSources();

                if (scope.presented.updatedatasources != null && scope.presented.updatedatasources != '') {
                    scope.selectedRow = row;

                    var sources = scope.presented.updatedatasources.split(',');

                    angular.forEach(sources, function (source) {
                        var promise = bworkflowApi.getDataFeed(source);

                        if (promise != null) {
                            promise.then(function (toUpdate) {
                                angular.forEach(row.alldata, function (value, key) {
                                    toUpdate.parameters[key] = value;
                                });

                                // get data and force a refresh
                                toUpdate.getData(true);
                            });
                        }
                    });
                }
            }

            if (scope.presented.datasource) {
                // we get our data from a data source object
                var promise = bworkflowApi.getDataFeed(scope.presented.datasource);

                if (promise != null) {
                    promise.then(function (feed) {
                        if (scope.presented.usedatasourcevisiblefields == true) {
                            scope.displayContainer = feed.template;
                        }
                        else {
                            scope.displayContainer = scope.presented;
                        }

                        scope.feed = feed;
                    });
                }
            }
        }
    });
}]);

questionsModule.directive('questionDatatableCell', ['$compile', function ($compile) {
    return {
        restrict: 'E',
        scope: {
            field: '=',
            row: '='
        },
        link: function (scope, element, attrs) {
            var val = null;
            if (scope.$parent.presented.usedatasourcevisiblefields == true) {
                val = scope.row.data[scope.field];
            }
            else {
                val = scope.row.alldata[scope.field];
            }

            var formatter = scope.$parent.feed.getFormat(scope.field);

            var content = scope.$parent.feed.applyFormat(val, formatter, false);

            var dirElement = $compile(content)(scope);
            $(element).append(dirElement);
        }
    };
}]);


questionsModule.directive('questionRepeater', ['$timeout',
    'bworkflowApi',
    '$compile',
    '$sce',
    'languageTranslate', function ($timeout, bworkflowApi, $compile, $sce, languageTranslate) {
        return $.extend({}, questionDirectiveBase, {
            link: function (scope, elt, attrs) {
                questionDirectiveBase.link(scope, undefined, bworkflowApi, languageTranslate);
                scope.feed = null;

                scope.data = [];

                scope.feeds = {};
                scope.selectedRow = null;

                scope.mediaImageUrl = window.razordata.siteprefix + 'MediaImage/';
                scope.membershipImageUrl = window.razordata.siteprefix + 'MembershipImage/'

                scope.date = function (dt) {
                    if (angular.isDefined(dt) == false) {
                        return moment();
                    }

                    return moment(dt);
                };

                scope.expandUrl = function (url) {
                    return bworkflowApi.getfullurl(url);
                };

                scope.raiseEvent = function (eventName, args) {
                    // allows someone to raise an event to get other elements to do things,
                    // for example clicking a button in the label refreshing a task list
                    scope.$emit('player_broadcast', { name: eventName, data: args });
                };

                scope.clearSources = function () {
                    if (scope.presented.cleardatasources != null && scope.presented.cleardatasources != '') {
                        var sources = scope.presented.cleardatasources.split(',');
                        var fields = null;

                        if (scope.selectedRow != null) {
                            fields = scope.selectedRow;
                        }

                        angular.forEach(sources, function (source) {
                            var promise = bworkflowApi.getDataFeed(source);

                            if (promise != null) {
                                promise.then(function (toUpdate) {
                                    // copy across what was selected into the parameters for the feed
                                    toUpdate.clearParameters(fields);
                                });
                            }
                        });
                    }
                };

                scope.update = function (data, datasources, refreshNow, mergeParams) {
                    var feeds = [];

                    // if datasources isn't defined then we work back through our properties
                    // to see if we can make things simple for the caller
                    if (angular.isDefined(datasources) == false || datasources == null) {
                        // nothing specified, so fall back to defaults
                        if (scope.presented.updatedatasources != null && scope.presented.updatedatasources != '') {
                            // we have update sources, so use them
                            feeds = scope.presented.updatedatasources.split(',');
                        }
                        else {
                            feeds = scope.presented.datasource.split(',');
                        }
                    }
                    else {
                        feeds = datasources;
                    }

                    var refresh = true;
                    if (angular.isDefined(refreshNow) == true) {
                        refresh = refreshNow;
                    }

                    angular.forEach(feeds, function (feed) {
                        var promise = bworkflowApi.getDataFeed(feed);

                        if (promise != null) {
                            promise.then(function (found) {
                                found.parameters = (mergeParams ? found.parameters : null) || {};

                                angular.forEach(data, function (value, key) {
                                    found.parameters[key] = value;
                                });

                                if (refresh == true) {
                                    found.getData(true);
                                }
                            });
                        }
                        else {
                            console.warn(feed + ' is not a valid datasource');
                            return;
                        }
                    });

                    scope.selectedRow = data;
                };

                scope.trustAsHtml = function (text) {
                    return $sce.trustAsHtml(text);
                };

                scope.showSection = function (sectionName) {
                    scope.$emit('player_broadcast', { name: sectionName, data: { action: 'show' } });
                };

                scope.$on('help_add_steps', function (event, args) {
                    if (scope.presented.HelpContent != null && scope.presented.Name != null) {
                        var step = { element: 'div[data-presentable-name="' + scope.presented.Name + '"]', intro: scope.presented.HelpContent, position: scope.presented.HelpPosition.toLowerCase() };

                        args.push(step);
                    }
                });

            scope.openWindow = function (url, target) {
                window.open(url, target);

                return false;
            };

                if (scope.presented.datasource) {
                    var split = scope.presented.datasource.split(',');

                    // we store each of the feeds in a varibale based on their name
                    angular.forEach(split, function (source) {
                        var promise = bworkflowApi.getDataFeed(source);

                        if (promise != null) {
                            promise.then(function (feed) {
                                scope.feeds[feed.template.name] = feed;
                            });
                        }
                    });

                    // we give the first feed special treatment, its the default feed
                    // and can be got at through some shortcut properties on us.
                    var promise = bworkflowApi.getDataFeed(split[0]);
                    if (promise != null) {
                        promise.then(function (feed) {
                            scope.feed = feed;

                            scope.data = scope.feed.data;
                        });
                    }
                }

                var content = '<div data-presentable-name="{{presented.Name}}">' + scope.presented.repeatcontent + '</div>';

                if (scope.presented.hidewhennodata == true) {
                    content = "<div ng-if='presented.hidewhennodata == false || (presented.hidewhennodata == true && data.length > 0)'>" + content + "</div>";
                }

                content = content + '<ul class="pager" ng-if="presented.showcontrols && feed.template.usepaging"><li><a href="#" ng-click="feed.previousPage()" ng-if="feed.page > 1">Previous</a></li><li><a href="#" ng-click="feed.nextPage()" ng-if="feed.data.length > 0">Next</a></li></ul>';

                var dirElement = $compile(content)(scope);
                $(elt).append(dirElement);
            }
        });
    }]);

questionsModule.directive('questionTimeline', ['$timeout', 'bworkflowApi', '$compile', 'languageTranslate', function ($timeout, bworkflowApi, $compile, languageTranslate) {
    return $.extend({}, questionDirectiveBase, {
        link: function (scope, elt, attrs) {
            questionDirectiveBase.link(scope, undefined, bworkflowApi, languageTranslate);
            scope.feeds = {};

            scope.taskItems = [];
            scope.taskWorklogItems = [];
            scope.taskChecklistItems = [];

            scope.groupItems = {};

            scope.items = [];

            scope.currentSelection = null;

            scope.buildItemsFromTasksFeed = function (feed) {
                scope.taskItems.length = 0;

                angular.forEach(feed.data, function (dataItem) {
                    if (dataItem.alldata.DateStarted == null) {
                        return;
                    }

                    var item = {
                        id: dataItem.alldata.Id,
                        data: dataItem,
                        date: moment(dataItem.alldata.DateStarted).toDate(),
                        type: 'Start Task',
                        badge:
                        {
                            type: "success",
                            icon: "icon-play"
                        },
                        body: "<strong>" + dataItem.alldata.Owner + "</strong> started task <strong>'" + dataItem.alldata.Name + "'</strong>"
                    };


                    if (dataItem.alldata.SiteName != null) {
                        item.body = item.body + " at site <strong>" + dataItem.alldata.SiteName + "</strong>";
                    }

                    scope.taskItems.push(item);

                    if (dataItem.alldata.DateCompleted == null) {
                        item.body = item.body + "<br/><i>This item is currently active</i>";
                        item.badge.type = "warning";
                        return;
                    }

                    item = {
                        id: dataItem.alldata.Id,
                        data: dataItem,
                        date: moment(dataItem.alldata.DateCompleted).toDate(),
                        type: 'Complete Task',
                        badge:
                        {
                            type: "danger",
                            icon: "icon-stop"
                        },
                        body: "<strong>" + dataItem.alldata.Owner + "</strong> completed task <strong>'" + dataItem.alldata.Name + "'</strong>"
                    };

                    if (dataItem.alldata.SiteName != null) {
                        item.body = item.body + " at site <strong>" + dataItem.alldata.SiteName + "</strong>";
                    }

                    item.body = item.body + " this took &cong; " + moment.duration(dataItem.alldata.ActualDuration * 1, 'seconds').humanize()

                    scope.taskItems.push(item);
                });
            };

            scope.buildItemsFromTaskWorklogsFeed = function (feed) {
                scope.taskWorklogItems.length = 0;

                angular.forEach(feed.data, function (dataItem) {
                    var item = {
                        id: dataItem.alldata.TaskId,
                        data: dataItem,
                        date: moment(dataItem.alldata.DateCreated).toDate(),
                        type: 'Clocked In',
                        badge:
                        {
                            type: ["warning", "small"],
                            icon: "icon-time"
                        },
                        body: '<i>This item is currently active</i>'
                    };

                    if (dataItem.alldata.DateCompleted != null) {
                        // its been clocked into and out of
                        item.body = undefined;
                        item.badge.type = ["success", "small"];
                    }

                    scope.taskWorklogItems.push(item);
                });
            };

            scope.filterStartingWorklogsOut = function () {
                // if we are hooked up to both a task feed and a work log feed
                // we get 2 items in the end result for the start of a task. To avoid
                // confusion, we filter out the clockin item

                angular.forEach(scope.taskItems, function (taskItem) {
                    var index = -1;

                    for (var i = 0; i < scope.taskWorklogItems.length; i++) {
                        if (taskItem.date.getTime() == scope.taskWorklogItems[i].date.getTime()) {
                            index = i;
                            break;
                        }
                    }

                    if (index != -1) {
                        scope.taskWorklogItems.splice(index, 1);
                    }
                });
            };

            scope.buildItemsFromChecklistsFeed = function (feed) {
                scope.taskChecklistItems.length = 0;

                angular.forEach(feed.data, function (dataItem) {
                    var startDate = moment(dataItem.alldata.DateStarted)
                    var item = {
                        id: dataItem.alldata.TaskId,
                        data: dataItem,
                        date: startDate.toDate(),
                        type: 'Checklist',
                        badge:
                        {
                            type: ["info", "small"],
                            icon: "icon-file"
                        },
                        body: "<strong>" + dataItem.alldata.ReviewerName + "</strong> completed checklist <strong>'" + dataItem.alldata.Name + "'</strong> relating to task <strong>'" + dataItem.alldata.TaskName + "'</strong> this took about " + moment.duration(moment(dataItem.alldata.DateCompleted).diff(startDate)).humanize()
                    };

                    scope.taskChecklistItems.push(item);
                });
            };

            scope.buildItemsFromFeed = function (feed) {
                // there are a number of feed types we work with, namely
                // Tasks, TaskWorklogs, Checklists
                scope.items.length = 0;

                switch (feed.template.feed) {
                    case "Tasks":
                        scope.buildItemsFromTasksFeed(feed);
                        break;
                    case "TaskWorklogs":
                        scope.buildItemsFromTaskWorklogsFeed(feed);
                        break;
                    case "Checklists":
                        scope.buildItemsFromChecklistsFeed(feed);
                        break;
                }

                scope.items.push.apply(scope.items, scope.taskItems);
                scope.filterStartingWorklogsOut();
                scope.items.push.apply(scope.items, scope.taskWorklogItems);
                scope.items.push.apply(scope.items, scope.taskChecklistItems);

                scope.buildGroupItems();
            };

            scope.buildGroupItems = function () {
                scope.groupItems = {};

                angular.forEach(scope.taskItems, function (taskItem) {
                    if (angular.isDefined(scope.groupItems[taskItem.id]) == false) {
                        var group = { id: taskItem.id };
                        scope.groupItems[taskItem.id] = group;
                        taskItem.group = group;
                    }
                    else {
                        taskItem.group = scope.groupItems[taskItem.id];
                    }
                });

                angular.forEach(scope.taskWorklogItems, function (workItem) {
                    workItem.group = scope.groupItems[workItem.id];
                });

                angular.forEach(scope.taskChecklistItems, function (cItem) {
                    cItem.group = scope.groupItems[cItem.id];
                });
            };

            if (scope.presented.datasource) {
                var split = scope.presented.datasource.split(',');

                // we store each of the feeds in a varibale based on their name
                angular.forEach(split, function (source) {
                    var promise = bworkflowApi.getDataFeed(source);

                    if (promise != null) {
                        promise.then(function (feed) {
                            var key = feed.notifier.id.split('.').join('');

                            scope.feeds[key] = feed;

                            scope.$watch('feeds.' + key + '.notifier', function (newValue, oldValue) {
                                if (angular.isDefined(newValue) == false || newValue == null) {
                                    return;
                                }

                                scope.buildItemsFromFeed(scope.feeds[newValue.id.split('.').join('')]);
                            });
                        });
                    }
                });
            }

            scope.clearSources = function () {
                if (scope.presented.cleardatasources != null && scope.presented.cleardatasources != '') {
                    var sources = scope.presented.cleardatasources.split(',');
                    var fields = scope.currentSelection;

                    if (fields != null) {
                        fields = scope.currentSelection;
                    }
                    angular.forEach(sources, function (source) {
                        var promise = bworkflowApi.getDataFeed(source);

                        if (promise != null) {
                            promise.then(function (toUpdate) {
                                // copy across what was selected into the parameters for the feed
                                toUpdate.clearParameters(fields);
                            });
                        }
                    });
                }
            };

            scope.updateSources = function (data) {
                if (scope.presented.updatedatasources != null && scope.presented.updatedatasources != '') {
                    var sources = scope.presented.updatedatasources.split(',');

                    angular.forEach(sources, function (source) {
                        var promise = bworkflowApi.getDataFeed(source);

                        if (promise != null) {
                            promise.then(function (toUpdate) {
                                angular.forEach(data.alldata, function (value, key) {
                                    toUpdate.parameters[key] = value;
                                });

                                // get data and force a refresh
                                toUpdate.getData(true);
                            });
                        }
                    });
                }
            };

            scope.$on('timeline.item.clicked', function (event, args) {
                if (scope.currentSelection != null) {
                    scope.currentSelection.ui_selected = undefined;
                }

                scope.clearSources();
                scope.updateSources(args.data);

                scope.currentSelection = args.data;

                scope.currentSelection.ui_selected = true;
            });

            var content = "<div class='question timeline-question'><timeline items='items' dateformat='presented.dateformat' ng-if='items.length > 0'></timeline>";

            if (scope.presented.fillheight == true) {
                content = "<div class='question timeline-question' fill-height ";

                if (scope.presented.fillheightbottompadding != null) {
                    content = content + "additional-padding='" + scope.presented.fillheightbottompadding + "'>";
                }

                content = content + "<timeline items='items' dateformat='presented.dateformat' ng-if='items.length > 0'></timeline></div>";
            }

            var dirElement = $compile(content)(scope);
            $(elt).append(dirElement);
        }
    });
}]);

var chartSeries = {
    link: function (scope, series, bworkflowApi, $interpolate) {

        scope.addSeries(series.name, []);

        var addSeriesColor = function (index) {
            // since we support multiple fields in a row being rendered as different series, our colour notation has to support
            // multiple colours too, we do this through a comma seperated string in a similar fashion to the multiple fields = multiple series
            var color = scope.defaultColors[scope.colors.length];

            if (angular.isDefined(series.linecolour) && series.linecolour != null && series.linecolour != '') {
                var splits = series.linecolour.split(',');

                if (splits.length < index) {
                    index = splits.length - 1;
                }

                color = splits[index];
            }

            scope.colors.push(color);
        };

        var addArraySeries = function (scope, series, bworkflowApi) {
            addSeriesColor(0);
            scope.addSeries(series.name, series.data);
        };

        var addFeedSeries = function (scope, series, bworkflowApi, $interpolate) {
            var promise = bworkflowApi.getDataFeed(series.datasource);

            if (promise != null) {
                promise.then(function (f) {
                    series.feed = f;

                    var plotData = function (feed) {
                        var groupedData = {};

                        var data = [];

                        // ok, this gets a bit complicated. The datasourcefield supports comma seperated so that multiple
                        // fields can be pulled in.
                        var fieldSplit = series.datasourcefield.split(',');

                        // set up series colours first
                        var i = 0;
                        angular.forEach(fieldSplit, function (field) {
                            addSeriesColor(i);
                            i++;
                        });

                        feed.data.forEach(function (dto) {
                            angular.forEach(fieldSplit, function (field) {
                                if (angular.isDefined(dto.alldata[field]) == false) {
                                    return;
                                }

                                if (dto.alldata[field] == null) {
                                    data.push(null);
                                }
                                else {
                                    var val = parseFloat(dto.alldata[field]);

                                    if (series.groupbyfield != null && series.groupbyfield != '') {
                                        if (series.fieldformat == null) {
                                            series.fieldformat = '';
                                        }

                                        var gf = $interpolate(series.fieldformat, true, null, true);

                                        if (angular.isDefined(gf) == false) {
                                            gf = series.groupbyfield;
                                        }
                                        else if (gf == null) {
                                            gf = series.groupbyfield;
                                        }
                                        else {
                                            var params = { value: dto.alldata[series.groupbyfield] };

                                            gf = gf(params);
                                        }

                                        // we seperate each field out into it's own group
                                        gf = field + '_' + gf;

                                        if (angular.isDefined(groupedData[gf]) == false) {
                                            groupedData[gf] = 0;
                                        }

                                        groupedData[gf] = groupedData[gf] + val;
                                    }
                                    else {
                                        data.push(val);
                                    }
                                }
                            });
                        });

                        if (series.groupbyfield != null) {
                            angular.forEach(groupedData, function (val) {
                                data.push(val);
                            });
                        }

                        scope.addSeries(series.name, data);
                    };

                    if (angular.isDefined(f.data) && f.data.length > 0) {
                        plotData(f);
                    }

                    series.feed.addAfterLoadHook(function (feed) {
                        plotData(feed);
                    });
                });
            }
        };

        if (series.type == "datasource") {
            addFeedSeries(scope, series, bworkflowApi, $interpolate);
        }
        else {
            addArraySeries(scope, series, bworkflowApi);
        }
    }
};

var chartAxis = {
    link: function (scope, series, bworkflowApi, $interpolate) {

        scope.labels = [];

        var addArrayAxis = function (scope, series, bworkflowApi) {
            angular.forEach(series.data, function (d) {
                scope.labels.push(d);
            });
        };

        var addFeedAxis = function (scope, series, bworkflowApi, $interpolate) {
            var promise = bworkflowApi.getDataFeed(series.datasource);

            if (promise != null) {
                promise.then(function (f) {
                    series.feed = f;

                    var addLabels = function (feed) {
                        var data = [];

                        feed.data.forEach(function (dto) {
                            if (angular.isDefined(dto.alldata[series.datasourcefield]) == false) {
                                return;
                            }

                            if (dto.alldata[series.datasourcefield] == null) {
                                data.push(null);
                            }
                            else {
                                var val = dto.alldata[series.datasourcefield];

                                if (series.fieldformat != null && series.fieldformat != '') {
                                    var result = $interpolate(series.fieldformat, true, null, true);

                                    if (angular.isDefined(result) == false) {
                                        // this means the embedded expressions haven't been fully resolved (so the variables aren't defined)
                                        // not much we can do
                                        data.push(val);
                                    }
                                    else if (result == null) {
                                        // this means there are no embedded expressions in the format, so we go with the raw val
                                        data.push(val);
                                    }
                                    else {
                                        // we may need to do some work on the parameters given to us (transpose them to internal names or pre query processing)
                                        var params = { value: val };

                                        data.push(result(params));
                                    }
                                    
                                }
                                else {
                                    data.push(val);
                                }
                            }
                        });

                        scope.labels = data;
                    };

                    if (angular.isDefined(f.data) && f.data.length > 0) {
                        addLabels(f);
                    }

                    series.feed.addAfterLoadHook(function (feed) {
                        addLabels(feed);
                    });
                });
            }
        };

        if (series.type == "datasource") {
            addFeedAxis(scope, series, bworkflowApi, $interpolate);
        }
        else {
            addArrayAxis(scope, series, bworkflowApi);
        }
    }
};

questionsModule.directive('questionLineChart', ['$timeout', 'bworkflowApi', '$interpolate', 'languageTranslate', function ($timeout, bworkflowApi, $interpolate, languageTranslate) {
    return $.extend({}, questionDirectiveBase, {
        templateUrl: 'question_linechart.html',
        link: function (scope, elt, attrs) {
            questionDirectiveBase.link(scope, undefined, bworkflowApi, languageTranslate);
            scope.seriesCache = {};
            scope.data = [];
            scope.labels = [];
            scope.chartType = 'chart-' + scope.presented.highchart.chart.type;

            // default colours, we pick these based on index and the series index if the series doesn't specify a color
            scope.defaultColors = ['#803690', '#00ADF9', '#DCDCDC', '#46BFBD', '#FDB45C', '#949FB1', '#4D5360'];

            // the colors actually used (drawn from defaultColors or the series color)
            scope.colors = [];

            scope.addSeries = function (name, data) {
                if (angular.isDefined(scope.seriesCache[name]) == false) {
                    scope.data.push([]);
                    scope.seriesCache[name] = { name: name, index: scope.data.length };
                }

                scope.data[scope.seriesCache[name].index - 1] = data;
            };

            if (angular.isDefined(scope.presented.highchart.xAxis.labels) && scope.presented.highchart.xAxis.labels != null) {
                chartAxis.link(scope, scope.presented.highchart.xAxis.labels, bworkflowApi, $interpolate);
            }

            angular.forEach(scope.presented.highchart.series, function (s) {
                chartSeries.link(scope, s, bworkflowApi, $interpolate);
            });
        }
    });
}]);

questionsModule.directive('questionPieChart', ['$timeout', 'languageTranslate', function ($timeout, languageTranslate) {
    return $.extend({}, questionDirectiveBase, {
        templateUrl: 'question_piechart.html',
        link: function (scope, elt, attrs) {
            questionDirectiveBase.link(scope, undefined, undefined, languageTranslate);
            $timeout(function () {
                var chartOpts = scope.presented.highchart;
                chartOpts.chart.renderTo = $(elt).find('.piechart')[0];
                chartOpts.chart.width = $(chartOpts.chart.renderTo).width();
                var hc = new Highcharts.Chart(chartOpts);
            });
        }
    });
}]);

questionsModule.directive('questionProgressindicator', ['$timeout', 'languageTranslate', function ($timeout, languageTranslate) {
    return $.extend({}, questionDirectiveBase, {
        templateUrl: 'question_progressindicator.html',
        link: function (scope, elt, attrs) {
            questionDirectiveBase.link(scope, undefined, undefined, languageTranslate);
        }
    });
}]);

questionsModule.directive('questionNewsFeedList', ['$timeout', 'languageTranslate', function ($timeout, languageTranslate) {
    return $.extend({}, questionDirectiveBase, {
        templateUrl: 'question_news_feed_list.html',
        link: function (scope, elt, attrs) {
            questionDirectiveBase.link(scope, undefined, undefined, languageTranslate);

        }
    });
}]);

questionsModule.factory('taskListService', [function () {
    var svc = {
        currentTask: null,
        setCurrentTask: function (task) {
            svc.currentTask = task;
        }
    }
    return svc;
}])

questionsModule.factory('quickStartTaskService', ['$http', 'sectionUtils', '$q', 'taskTypesService', function ($http, sectionUtils, $q, taskTypesSvc) {
    var svc = {
        getSiteTypeTaskTypes: function () {
            return sectionUtils.getUrl('odata').then(function (url) {
                return $http({ url: url + '/UserTypeTaskTypes', method: 'GET' }).then(function (response) {
                    // Index SiteTypes by their SiteType
                    return response.data.value.groupBy('UserTypeId');
                })
            });
        },

        getQRcodes: function () {
            return sectionUtils.getUrl('odata').then(function (url) {
                return $http({ url: url + '/QRCodes?$filter=IsASite eq true or TaskTypeId ne null&$expand=User,TaskType&$select=QRcode,User/UserId,User/Name,User/UserTypeId,TaskType/*', method: 'GET' }).then(function (response) {
                    return response.data.value.reduce(function (map, r) {
                        map[r.QRcode] = {
                            qrcode: r.QRcode,
                            site: r.User,
                            tasktype: r.TaskType ? {
                                defaulttoroster: null,
                                mediaid: r.TaskType.MediaId,
                                requiresbasicdetails: false,
                                requiressitedetails: false,
                                tasktypeid: r.TaskType.Id,
                                text: r.TaskType.Name,
                                type: 'Shortcut'
                            } : null
                        };
                        return map;
                    }, {});
                })
            });
        },

        getBeacons: function () {
            return sectionUtils.getUrl('odata').then(function (url) {
                return $http({ url: url + '/Beacons?$filter=IsASite eq true or TaskTypeId ne null&$expand=User,TaskType&$select=Id,User/UserId,User/Name,User/UserTypeId,TaskType/*', method: 'GET' }).then(function (response) {
                    return response.data.value.reduce(function (map, r) {
                        map[r.Id] = {
                            beaconId: r.Id,
                            site: r.User,
                            tasktype: r.TaskType ? {
                                defaulttoroster: null,
                                mediaid: r.TaskType.MediaId,
                                requiresbasicdetails: false,
                                requiressitedetails: false,
                                tasktypeid: r.TaskType.Id,
                                text: r.TaskType.Name,
                                type: 'Shortcut'
                            } : null
                        };
                        return map;
                    }, {});
                })
            });
        },

        getTaskTypes: function () {
            return taskTypesSvc.getTaskTypes().then(function (tasktypes) {
                return tasktypes.reduce(function (map, taskType) {
                    map[taskType.Id] = {
                        defaulttoroster: null,
                        mediaid: taskType.MediaId,
                        requiresbasicdetails: false,
                        requiressitedetails: false,
                        tasktypeid: taskType.Id,
                        text: taskType.Name,
                        type: 'Shortcut'
                    };
                    return map;
                }, {});
            });
        },

        prepare: function () {
            var sttt = svc.getSiteTypeTaskTypes();
            var tt = svc.getTaskTypes();
            var qrcodes = svc.getQRcodes();
            var beacons = svc.getBeacons();
            var deferred = $q.defer();
            $q.all([sttt, tt, qrcodes, beacons]).then(function (results) {
                deferred.resolve({
                    siteTypeTaskTypes: results[0],
                    taskTypes: results[1],
                    qrcodes: results[2],
                    beacons: results[3]
                });
            });
            return deferred.promise;
        }
    }
    return svc;
}])

questionsModule.factory('safeBeaconService', ['$injector', function ($injector) {
    if (window.device && window.device.platform) {
        return $injector.get('beaconSvc');
    } else {
        return {
            notsupported: true
        }
    }
}])

questionsModule.directive('questionTaskList', ['bworkflowApi',
    '$interval',
    '$sce',
    '$cookieStore',
    '$filter',
    '$timeout',
    '$q',
    'RequestsErrorHandler',
    'sharedScope',
    'pageRefresh',
    'appUpdateMonitor',
    'taskListService',
    'quickStartTaskService',
    'createNewTaskWorkflow',
    'navigator-notification',
    'persistantStorage',
    'ngToast',
    'safeBeaconService',
    'cookieTimerSvc',
    'languageTranslate',
    function (bworkflowApi, $interval, $sce, $cookieStore, $filter, $timeout, $q, RequestsErrorHandler, sharedScope, pageRefresh, appUpdateMonitor, taskListService, quickStartTaskService, createNewTaskWorkflow, navigatorNotification, persistantStorage, ngToast, beaconSvc, cookieTimerSvc, languageTranslate) {
        return $.extend({}, questionDirectiveBase, {
            templateUrl: 'question_tasklist.html',
            link: function (scope, elt, attrs) {
                var cookieTimer = cookieTimerSvc();

                questionDirectiveBase.link(scope, undefined, bworkflowApi, languageTranslate);

                // These are allocated once and we *modify* the array instead of reallocating a new one
                scope.chooseQSTBeaconSites = [];
                scope.chooseQSTQrCodeSites = [];
                scope.chooseQSTSites = null;            // This points to either of the above, the UI displays this list

                function pushQSTSite(qstSites, qst) {
                    if (!qstSites.find(function (q) { return (q.site && qst.site && q.site.UserId == qst.site.UserId); })) {
                        qstSites.push(qst);

                        if (!scope.chooseQSTSites) {
                            scope.chooseQSTSites = qstSites;
                        }
                    }
                }

                function clearQSTSite(qstSites, fn) {
                    if (qstSites) {
                        for (var i = qstSites.length - 1; i >= 0; i--) {
                            if (fn(qstSites[i])) {
                                qstSites.splice(i, 1);
                            }
                        }
                        if (qstSites.length == 0 && scope.chooseQSTSites === qstSites) {
                            delete scope.chooseQSTSites;
                        }
                    }
                }

                scope.showQSTSitesDialog = function (qstSites) {
                    scope.chooseQSTSites = qstSites;
                }

                scope.hideQSTSitesDialog = function () {
                    delete scope.chooseQSTSites;
                }

                function checkQSTBeaconExpiry(beacon) {
                    var expireTime = moment.utc().subtract(10, 'seconds');
                    if (!angular.isDefined(beacon.timestamp) || beacon.timestamp.isBefore(expireTime)) {
                        clearQSTSite(scope.chooseQSTBeaconSites, function (qst) {
                            return qst.beacon === beacon;
                        });
                    } else {
                        cookieTimer.setTimer(beacon.beaconId, 10 * 1000, beacon, checkQSTBeaconExpiry);
                    }
                }

                function updateQSTBeacons(beacons) {
                    var compareTime = moment.utc().subtract(10, 'seconds');
                    if (beacons) {
                        angular.forEach(beacons, function (beacon) {
                            if (beacon.range.id <= 1 && beacon.timestamp.isAfter(compareTime)) {          // Near or Immediate, heard within last 10 seconds
                                var qstBeacon = _qstSetup.beacons[beacon.beaconId];
                                if (angular.isDefined(qstBeacon)) {
                                    cookieTimer.setTimer(beacon.beaconId, 10 * 1000, beacon, checkQSTBeaconExpiry);

                                    // The beacon will be either a Site and/or TaskType QST beacon ..
                                    var site = qstBeacon.site;
                                    var tasktype = qstBeacon.tasktype;

                                    if (tasktype && site) {
                                        pushQSTSite(scope.chooseQSTBeaconSites, {
                                            beacon: beacon,
                                            site: site,
                                            tasktypes: [angular.extend({ siteid: site.UserId }, tasktype)]
                                        });
                                    }
                                    else if (tasktype) {
                                        pushQSTSite(scope.chooseQSTBeaconSites, {
                                            beacon: beacon,
                                            tasktypes: [angular.extend({}, tasktype)]
                                        });
                                    }
                                    else if (site) {
                                        var siteTypeTaskTypes = _qstSetup.siteTypeTaskTypes[site.UserTypeId];
                                        if (angular.isDefined(siteTypeTaskTypes)) {
                                            var validTaskTypes = siteTypeTaskTypes.map(function (sttt) {
                                                return angular.extend({
                                                    siteid: site.UserId
                                                }, _qstSetup.taskTypes[sttt.TaskTypeId]);
                                            });

                                            pushQSTSite(scope.chooseQSTBeaconSites, {
                                                beacon: beacon,
                                                site: site,
                                                tasktypes: validTaskTypes
                                            });
                                        }
                                    }
                                }
                            }
                        });
                    }

                    // Remove any expired beacons from the QST dialog ..
                    clearQSTSite(scope.chooseQSTBeaconSites, function (qst) {
                        return (qst.beacon && qst.beacon.timestamp.isBefore(compareTime));
                    });
                }

                var _qstSetup;
                quickStartTaskService.prepare().then(function (setup) {
                    _qstSetup = setup;
                    scope.enableQRCodeQST = Object.getOwnPropertyNames(setup.qrcodes).length > 0 && scope.presented.template.allowqrcodestart;
                    scope.enableBeaconQST = Object.getOwnPropertyNames(setup.beacons).length > 0 && scope.presented.template.allowbeaconstart;

                    if (scope.enableBeaconQST && !beaconSvc.notsupported) {
                        scope.$on('$destroy', beaconSvc.startScanning(function (args) {
                            updateQSTBeacons(args.alive);
                        }, { forceScan: false, anyChange: true }));
                    }
                })

                function createAndStartTaskTemplate(template) {
                    createNewTaskWorkflow.createAndClockIn(template, scope.presented.userid, false).then(function (data) {
                        scope.offline = false;
                        scope.$emit('question-task-list.created-new-task', data);
                    }, function (error) {
                        scope.offline = true;
                    });
                }

                function extractQRCode(text) {
                    if (!text) {
                        return null;
                    }
                    var str = text.toString();
                    try {
                        var jobj = JSON.parse(str);
                        if (angular.isObject(jobj)) {
                            if (angular.isUndefined(jobj.type) || jobj.type != 'Site' || !jobj.value) {
                                return null;
                            }

                            return jobj.value.toString();
                        } else {
                            // Not a JSON object, trust the text is valid 5 char QR Code (cant do a length check
                            // as no gaurantee they are all 5 chars !!)
                            return str;
                        }
                    } catch (e) {
                        return str;
                    }
                }

                scope.QRCodeScanned = function (text, preselectedTask) {
                    var qrcode = extractQRCode(text);
                    if (qrcode == null) {
                        alert('Invalid QRCode');
                        return;
                    }

                    var qrConfig = _qstSetup.qrcodes[qrcode];
                    if (angular.isUndefined(qrConfig)) {
                        alert('QRCode is not configured');
                        return;
                    }

                    // This is where we decide what type of QR we have scanned and dictates what workflow will follow ..
                    // #1. Site only QR -> Original QR workflow, find Task Types attached to the Site and start (allow user to choose if > 1) the selected Task Type
                    // #2. TaskType only QR -> New QR workflow, start the scanned TaskType (will search Tasklist first or create a new Spot Task)
                    // #3. TaskType + Site QR -> New QR workflow, start the scanned TaskType (will search Tasklist first for matching Site, or create a new Spot task at the scanned site)
                    if (qrConfig.site && !qrConfig.tasktype) {
                        // #1
                        scope.siteQRCodeScanned(text, preselectedTask);
                    } else if (qrConfig.site || qrConfig.tasktype) {
                        // #2, #3
                        if (preselectedTask) {
                            if (preselectedTask.site && qrConfig.site && preselectedTask.site.id != qrConfig.site.UserId) {
                                alert('This task is for ' + preselectedTask.site.name + ' not ' + qrConfig.site.Name);
                                return;
                            }
                            if (qrConfig.tasktype && preselectedTask.id != qrConfig.tasktype.tasktypeid) {
                                alert('This QRCode is for a task type of ' + qrConfig.tasktype.text);
                                return;
                            }
                            scope.clockin(preselectedTask);
                        } else {
                            var task = scope.allTasks.find(function (t) {
                                return (qrConfig.tasktype ? t.tasktypeid == qrConfig.tasktype.tasktypeid : true) && (qrConfig.site ? t.site && t.site.id == qrConfig.site.UserId : true);
                            });
                            if (task) {
                                task = scope.tasks.find(function (t) {
                                    return (qrConfig.tasktype ? t.tasktypeid == qrConfig.tasktype.tasktypeid : true) && (qrConfig.site ? t.site && t.site.id == qrConfig.site.UserId : true);
                                })

                                if (!task) {
                                    alert(qrConfig.tasktype.text + ' cannot be started yet due to level restriction');
                                    return;
                                }

                                // Start the existing task instead of creating a Spot task
                                scope.showTask(task).then(function (result) {
                                    if (result.action == 'gettask') {
                                        scope.clockin(result.task);
                                    }
                                })
                            } else {
                                // This is a spot task ..
                                createAndStartTaskTemplate(angular.extend({
                                    siteid: qrConfig.site ? qrConfig.site.UserId : null,
                                    sitename: qrConfig.site ? qrConfig.site.Name : null
                                }, qrConfig.tasktype));
                            }
                        }
                    }
                }

                scope.siteQRCodeScanned = function (text, preselectedTask) {
                    var qrcode = extractQRCode(text);
                    if (qrcode == null) {
                        alert('Invalid QRCode');
                        return;
                    }

                    var qrConfig = _qstSetup.qrcodes[qrcode];
                    if (angular.isUndefined(qrConfig) || !qrConfig.site) {
                        alert('QRCode is not configured for site');
                        return;
                    }
                    var site = qrConfig.site;
                    if (preselectedTask && preselectedTask.site && preselectedTask.site.id != site.UserId) {
                        alert('This task is for ' + preselectedTask.site.name + ' not ' + site.Name);
                        return;
                    }

                    var siteTypeTaskTypes = _qstSetup.siteTypeTaskTypes[site.UserTypeId];
                    if (angular.isUndefined(siteTypeTaskTypes)) {
                        alert(site.Name + ' has no assigned Task Types');
                        return;
                    }

                    var validTaskTypes = siteTypeTaskTypes.map(function (sttt) {
                        return angular.extend({
                            siteid: site.UserId
                        }, _qstSetup.taskTypes[sttt.TaskTypeId]);
                    });

                    if (preselectedTask) {
                        var template = validTaskTypes.find(function (tt) {
                            return tt.tasktypeid == preselectedTask.tasktypeid;
                        });

                        if (!template) {
                            alert(preselectedTask.name + ' is not valid at ' + site.Name);
                        } else {
                            scope.clockin(preselectedTask);
                        }
                    } else if (validTaskTypes.length > 1) {
                        scope.chooseQSTQrCodeSites.splice(0, scope.chooseQSTQrCodeSites.length);        
                        pushQSTSite(scope.chooseQSTQrCodeSites, {
                            qrcode: qrcode,
                            site: site,
                            tasktypes: validTaskTypes
                        });

                    } else if (validTaskTypes.length == 1) {
                        // Auto start the 1 and only task
                        scope.startQSTTask(site, validTaskTypes[0]);
                    }
                }

                scope.finishTaskQRCode = function (text, preselectedTask) {
                    var qrcode = extractQRCode(text);
                    if (qrcode == null) {
                        alert('Invalid QRCode');
                        return;
                    }

                    var qrConfig = _qstSetup.qrcodes[qrcode];
                    if (angular.isUndefined(qrConfig)) {
                        alert('QRCode is not configured');
                        return;
                    }

                    if (preselectedTask) {
                        if (qrConfig.site && preselectedTask.site && preselectedTask.site.id != qrConfig.site.UserId) {
                            alert('This task is for ' + preselectedTask.site.name + ' not ' + qrConfig.site.Name);
                            return;
                        }
                        if (qrConfig.tasktype && preselectedTask.tasktypeid != qrConfig.tasktype.tasktypeid) {
                            alert('This QRCode is for ' + qrConfig.tasktype.text + ' not ' + preselectedTask.name);
                            return;
                        }

                        scope.finishing(preselectedTask);
                    }
                }

                scope.startQSTTask = function (site, template) {
                    scope.hideQSTSitesDialog();

                    // See if we can find the Task in the All Task List first ..
                    var findFn;
                    if (angular.isDefined(site)) {
                        findFn = function (t) {
                            return t.tasktypeid == template.tasktypeid && (t.site && t.site.id == site.UserId);
                        }
                    } else {
                        findFn = function (t) {
                            return t.tasktypeid == template.tasktypeid && !t.site;
                        }
                    }

                    var task = scope.allTasks.find(findFn);
                    if (task) {
                        task = scope.tasks.find(findFn);

                        if (!task) {
                            alert(template.text + ' cannot be started yet due to level restriction');
                            return;
                        }

                        // Start the existing task instead of creating a Spot task
                        scope.showTask(task).then(function (result) {
                            if (result.action == 'gettask') {
                                scope.clockin(result.task);
                            }
                        })
                        return;
                    }

                    createAndStartTaskTemplate(template);
                }

                scope.mediaurl = bworkflowApi.getfullurl('~/MediaImage');
                scope.showing = 'clockings';
                scope.canBack = false;
                scope.currentresource = null;

                // 1000 m in km (or 5280 feet to 1 mile)
                scope.longDistanceUnitsToSmallMultiplier = scope.presented.template.measurementtype == 'Metric' ? 1000 : 5280;
                scope.longDistanceUnits = scope.presented.template.measurementtype == 'Metric' ? 'KM' : 'MI';
                scope.shortDistanceUnits = scope.presented.template.measurementtype == 'Metric' ? 'M' : 'FT';

                scope.labelFilterAll = { name: 'Show <br/> All', id: null, listText: 'Show All' };
                scope.labels = [];
                scope.labelFilter = scope.labelFilterAll;
                scope.filteredTasks = [];
                scope.clockedIntoTasks = [];
                scope.notClockedIntoTasks = [];

                scope.activityoptions = [{ value: null, text: 'To Do' }, { value: true, text: 'Complete' }, { value: false, text: 'Incomplete' }];

                scope.creatingTask = false;
                scope.creatingGroup = false;

                scope.currentFolder = null;
                scope.folderStack = [];

                scope.lastSiteClockinClockout = null;

                scope.refreshCountDown = 1;

                scope.imageUrl = bworkflowApi.getImageUrl();

                scope.isClockedIn = false;
                scope.clockedInTo = null;

                scope.isFinalPage = false;

                scope.getExecutionQueueLength = function () {
                    return bworkflowApi.executionCallQueue.length;
                };

                scope.hasOnlineTaskList = false;
                scope.isvisible = true;

                scope.inGotoChecklist = false;

                scope.getMaxLevel = function (tasks) {
                    var maxLevel = 0;
                    for (var i = 0; i < tasks.length; i++) {
                        var t = tasks[i];

                        if (t.level > maxLevel) {
                            maxLevel = t.level;
                        }
                    }

                    return maxLevel;
                };

                scope.filterByLevel = function (tasks, level) {
                    var result = [];

                    for (var i = 0; i < tasks.length; i++) {
                        var t = tasks[i];

                        if (t.status == 'active') {
                            scope.isClockedIn = true;
                            scope.clockedInTo = t;
                        }

                        if (t.level == level) {
                            result.push(t);
                        }
                    }

                    return result;
                };

                scope.getTasks = function (forceonline) {
                    var parameters = {
                        userid: scope.presented.userid,
                        filterbylabels: scope.presented.filterbylabels,
                        filterbystatusses: scope.presented.filterbystatusses,
                        includeparentstasks: scope.presented.template.includeparentstasks
                    };

                    // If force online then display a message whilst we retrieve the list
                    if (forceonline) {
                        scope.hasOnlineTaskList = false;
                    }

                    bworkflowApi.execute('TaskListManagement', 'GetTasks', parameters, undefined, forceonline)
                        .then(function (result) {
                            scope.allTasks = result.data.opentasks;
                            scope.tasks = scope.filterByLevel(result.data.opentasks, scope.getMaxLevel(result.data.opentasks));
                            scope.hasOnlineTaskList = true;

                            // we need to work out the intersection of what we want to see with what we've been sent
                            // this occurs because even though we filter tasks by the labels we want on the server,
                            // tasks can have multiple labels attached, we may want to show these somewhere so we get the
                            // data back for them, however for filtering we only want to filter on what's configured
                            // for the task list
                            var ls = [];
                            angular.forEach(result.data.labels, function (value, key) {
                                var ids = $filter('filter')(scope.presented.filterbylabels, value.id, true);

                                if (ids.length > 0) {
                                    ls.push(value);
                                }
                            });

                            scope.labels = ls;

                            if (result.added && result.added.length) {
                                scope.tasksAdded = true;
                            }

                            if (result.removed && result.removed.length) {
                                scope.tasksRemoved = true;
                            }

                            // _momentReceivedUtc will only exist when Online result
                            if (result._momentReceivedUtc) {
                                scope.validFromDate = moment(result._momentReceivedUtc);
                                if (scope.presented.template.staleperiodseconds) {
                                    scope.validToDate = moment(scope.validFromDate).add(scope.presented.template.staleperiodseconds, 'seconds');
                                }
                            }

                            if (result.data.lastworklog != null) {
                                console.debug("Server has provided a last clockin clockout, saving this for possible later use");
                                scope.lastSiteClockinClockout = result.data.lastworklog;
                            }

                            scope.filterTasks();

                            scope.calculateTaskDistances();

                            if (scope.presented.template.sorttype == 'StartTime') {
                                scope.calculateStartTimeSort();
                            }
                            else if (scope.presented.template.sorttype == 'SortOrder') {
                                scope.calculateSortOrderSort();
                            }

                            // we support raising an event when the tasks have been loaded so that other things
                            // on a dashboard can show information without having to hit the server again. Typical
                            // use case is showing the total number of tasks somewhere. Doing the player_broadcast makes
                            // this data available to anyone pointing to us through listento
                            if (scope.presented.Name != null) {
                                sharedScope.set(scope.presented.Name, { tasks: scope.tasks, type: 'tasksloaded' });

                                scope.$emit('player_broadcast', { name: scope.presented.Name, data: { tasks: scope.tasks, type: 'tasksloaded' } });
                            }
                        }, function (error) {

                        });
                };

                scope.saveChanges = function (changes) {
                    var parameters = { userid: scope.presented.userid, taskid: scope.editing.id, changes: changes };

                    bworkflowApi.execute('TaskListManagement', 'SaveChanges', parameters)
                        .then(function (data) {
                            angular.forEach(changes, function (value, key) {
                                scope.original[key] = value;
                            });

                            // we now broadcast an event to let other task lists know
                            // in case the task meets their filters so things need to be changed for them too
                            scope.$emit('player_broadcast', { name: 'question-task-list.listitemchanged', data: { task: angular.copy(scope.original), source: scope.$id } });
                        }, function (error) {

                        });
                };

                scope.gotoChecklist = function (resource) {
                    if (scope.inGotoChecklist == true) {
                        // FIX for EVS-1576
                        // we are already going to a checklist.
                        return;
                    }

                    scope.inGotoChecklist = true;
                    scope.currentresource = resource;

                    var parameters = { userid: scope.presented.userid, taskid: scope.editing.id, publishinggroupresourceid: resource.publishedresourceid };

                    RequestsErrorHandler.specificallyHandled(
                        function () {
                            $q.all({ execute: bworkflowApi.execute('TaskListManagement', 'GotoChecklist', parameters, 10000) }).then(
                                function (data) {
                                    if (data.execute.allow == true) {
                                        scope.showing = 'player';
                                        scope.isFinalPage = false;
                                        scope.workingdocumentid = data.execute.id;
                                        scope.buttonstates.areAjaxing = false;
                                    }

                                    scope.inGotoChecklist = false;

                                    scope.currentresource.online = true;
                                },
                                function (reason) {
                                    scope.inGotoChecklist = false;
                                    scope.currentresource.online = false;
                                }
                            )
                        }
                    );
                };

                scope.gotoFolder = function (folder) {
                    var parameters = { userid: scope.presented.userid, taskid: scope.editing.id, mediafolderid: angular.isDefined(folder.folderid) == true ? folder.folderid : folder.id };

                    RequestsErrorHandler.specificallyHandled(
                        function () {
                            $q.all({ execute: bworkflowApi.execute('TaskListManagement', 'GetFolder', parameters, 5000) }).then(
                                function (data) {
                                    scope.showing = 'folder';
                                    scope.currentFolder = data.execute;
                                    scope.folderStack.push(data.execute);

                                    folder.online = true;
                                },
                                function (reason) {
                                    folder.online = false;
                                }
                            )
                        }
                    );
                };

                scope.gotoParentFolder = function () {
                    if (scope.folderStack.length <= 1) {
                        scope.closeCurrent();
                    }

                    var index = $.inArray(scope.folderStack[scope.folderStack.length - 1], scope.folderStack);

                    if (index == -1) {
                        return;
                    }

                    scope.folderStack.splice(index, 1);

                    scope.currentFolder = scope.folderStack[scope.folderStack.length - 1];
                };

                scope.buttonstates = { areAjaxing: false };

                scope.$on('section.finalpage', function () {
                    scope.isFinalPage = true;
                });

                scope.next = function () {
                    var args = {
                        allowOffline: scope.isFinalPage,
                        afterNextCallback: function () {
                            // this only get's called if validation has passed
                            if (scope.isFinalPage) {
                                scope.embeddedPlayerFinished();
                            }
                        }
                    };

                    scope.$broadcast('embedded-player.next', args);
                };

                scope.back = function () {
                    scope.$broadcast('embedded-player.back', null);
                };

                scope.$on('embedded-player.started', function (evt, model) {
                    if (scope.resourceHasWorkingDocument(scope.currentresource, model.data.WorkingDocumentId) == false) {
                        scope.currentresource.workingdocuments.push(model.data.WorkingDocumentId);
                        scope.currentresource.workingdocumentcount = scope.currentresource.workingdocumentcount + 1;
                    }
                });

                scope.$on('embedded-player.finished', function (evt, model) {
                    model.show = false; // prevent the default finish step being shown
                    scope.embeddedPlayerFinished(model);
                });

                scope.embeddedPlayerFinished = function (model) {
                    if (scope.currentresource.ismandatory == true) {
                        scope.currentresource.allowfinish = true;
                    }

                    scope.currentresource.completedworkingdocumentcount = scope.currentresource.completedworkingdocumentcount + 1;

                    scope.calculateCanFinish(scope.editing);

                    var task = scope.editing;

                    var p = false;
                    if (angular.isDefined(model) && angular.isDefined(model.data)) {
                        p = model.data.Presented != null;
                    }

                    if (task.canfinish && task.tasktype.autofinishtaskonlastchecklist) {
                        task.complete = true;

                        // if it's not null, then that means the finish has occurred as a part of a finish with that has
                        // presented something, so let the user see it
                        scope.finish(task, '', true, undefined, p);
                    }


                    if (p == false) {
                        scope.showing = 'clockings';
                    }
                }

                scope.$on('embedded-player.finishing', function (evt, model) {
                    if (scope.currentresource.preventfinishing == true) {
                        model.show = false;

                        if (scope.currentresource.ismandatory == true) {
                            scope.currentresource.allowfinish = true;
                        }

                        scope.calculateCanFinish(scope.editing);

                        var task = scope.editing;
                        if (task.canfinish && task.tasktype.autofinishtaskonlastchecklist) {
                            task.complete = true;
                            scope.finish(task, '', true);
                        }

                        scope.showing = 'clockings';
                    }
                });

                scope.$on('embedded-player.showstep', function (evt, model) {
                    scope.multiPage = model.data.RootStepCount > 1;
                    scope.canBack = model.data.CanPrevious;
                });

                scope.resourceHasWorkingDocument = function (resource, id) {
                    var has = false;

                    angular.forEach(resource.workingdocuments, function (value, index) {
                        if (value == id) {
                            has = true;
                        }
                    });

                    return has;
                };

                scope.getLabelFilterName = function (filter) {
                    return $sce.trustAsHtml(filter.name);
                };

                scope.getTaskDescription = function (task) {
                    return $sce.trustAsHtml(task.description);
                };

                scope.showTask = function (task) {
                    var deferred = $q.defer();

                    if (scope.creatingGroup == true) {
                        task.selectedInGroup = !task.selectedInGroup;
                        deferred.resolve({
                            action: 'group',
                            task: task
                        });
                    } else {

                        task.loading = true;
                        var parameters = { userid: scope.presented.userid, taskid: task.id };
                        scope.original = task;

                        bworkflowApi.execute('TaskListManagement', 'GetTask', parameters)
                            .then(function (data) {
                                scope.showTaskDetails(data, false);

                                // with the intro of offline someone can go into finish incomplete
                                // hit back to list, then hit on the task again in the task list
                                // we need to resume where we were in this case, which means making
                                // sure the incompletetasks list is populated with it
                                if (data.status != 'finishing-incomplete') {
                                    scope.incompleteTasks = null;
                                }
                                else {
                                    scope.incompleteTasks = [data];
                                }

                                task.loading = false;

                                deferred.resolve({
                                    action: 'gettask',
                                    task: data
                                });
                            }, function (error) {
                                task.loading = false;
                                deferred.reject(error);
                            });
                    }
                    return deferred.promise;
                };

                scope.showTaskDetails = function (data, refreshTaskList) {
                    scope.calculateCanFinish(data);
                    scope.editing = data;
                    scope.creatingTask = false;

                    if (angular.isDefined(refreshTaskList) == true && refreshTaskList == true) {
                        scope.getTasks();
                    }
                };

                scope.$on('question-task-list.created-new-task', function (event, task) {
                    // For newly created spot tasks, these have not yet been added to the task list, so make sure the task is actually listed
                    var find = scope.tasks.find(function (t) { return t.id == task.id; });
                    if (angular.isUndefined(find)) {
                        scope.tasks.push(task);
                    }

                    // we don't have an original (item in the task list that's been clicked) in this scenario, so lets fake it                    
                    scope.original = {};
                    scope.original.status = task.status;

                    scope.calculateCanFinish(task);

                    scope.isClockedIn = true;
                    scope.clockedInTo = task;

                    if (task.tasktype.autofinishafterstart) {
                        task.complete = true;           // EVS-1385 - mark these tasks as Complete so they dont end up FinishedIncomplete
                        scope.finish(task, '');

                        var msg = 'Completed <b>' + task.name + '</b>';
                        if (task.site && task.site.name) {
                            msg += ' at <b>' + task.site.name + '</b>';
                        }
                        ngToast.create({
                            content: $sce.trustAsHtml(msg),
                            horizontalPosition: 'center'
                        });
                    } else {
                        scope.showTaskDetails(task, true);

                        // EVS-632 Auto start 1st checklist on task start
                        if (task.tasktype.autostart1stchecklist) {
                            if (task.publishedresources && task.publishedresources.length) {
                                scope.gotoChecklist(task.publishedresources[0]);
                            }
                        }
                    }

                    scope.filterTasks();
                });

                scope.$on('question-task-list.listitemchanged', function (event, data) {
                    if (data.source == scope.$id || scope.presented.template.listen == false) {
                        // we initiated the change event, so let's get out of here
                        return;
                    }

                    scope.manageListItemChanged(data.task);
                });

                scope.$on('question-task-list.refreshTasks', function (event, args) {
                    if (angular.isDefined(args.name)) {
                        if (args.name != scope.presented.Name) {
                            return;
                        }
                    }

                    scope.getTasks(args.forceonline);
                });

                scope.$on('question-task-list.visiblechange', function (event, args) {
                    if (angular.isDefined(args.name)) {
                        if (args.name != scope.presented.Name) {
                            return;
                        }
                    }

                    scope.isvisible = args.visible;
                });

                scope.removeTask = function (task) {
                    var index = scope.tasks.indexOf(task);
                    if (index != -1) {
                        scope.tasks.splice(index, 1);
                        scope.filterTasks();
                    }
                }

                scope.manageListItemChanged = function (task) {
                    if (task == null) {
                        return;
                    }

                    var ids = $filter('filter')(scope.tasks, task.id, true);
                    var isOfInterest = scope.isTaskOfInterest(task);

                    // so there are a few options here
                    // 1. we have the task and its of interest - we do nothing
                    // 2. we have the task and its not of interest - we remove it
                    // 3. we don't have the taks and its not of interest - we do nothing
                    // 4. we don't have the task and its of interest - we add it
                    // so 2 and 4 mean we have to do something

                    // 2 first
                    if (ids.length > 0 && isOfInterest == false) {
                        scope.removeTask(ids[0]);
                    }

                    // 4 now
                    if (ids.length == 0 && isOfInterest == true) {
                        scope.tasks.push(angular.copy(task));
                        scope.filterTasks();
                    }
                };

                scope.isTaskOfInterest = function (task) {
                    if (scope.presented.filterbystatusses.length == 0) {
                        // everything is of interest to us and we already have what we want
                        return null;
                    }

                    // currently the only thing we filter in or out on is the task status
                    var statuses = $filter('filter')(scope.presented.filterbystatusses, task.statusid, true);

                    if (statuses.length == 0) {
                        return false;
                    }

                    return true;
                };

                scope.showTaskById = function (id) {
                    var t = null;

                    angular.forEach(scope.tasks, function (value) {
                        if (value.id == id) {
                            t = value;
                        }
                    });

                    if (t != null) {
                        scope.showTask(t);
                    }
                };

                scope.calculateCanFinish = function (task) {
                    var canfinish = true;

                    angular.forEach(task.publishedresources, function (value, index) {
                        if (value.ismandatory == false) {
                            return;
                        }

                        // if we are preventing finishing, then mandatory denotes it must be started
                        if (value.allowfinish == false) {
                            canfinish = value.allowfinish;
                        }
                    });

                    if (canfinish == true && task.workinggroupid != null) {
                        angular.forEach(task.workinggroupdata, function (value) {
                            if (value.allowfinish == false) {
                                canfinish = false;
                            }
                        });
                    }

                    task.photorequired = (task.photosupport == 2 && task.photos.length == 0);
                    task.canfinish = canfinish;
                };

                scope.showList = function () {
                    scope.manageListItemChanged(scope.original);
                    scope.original = null;
                    scope.editing = null;

                    scope.folderStack.length = 0;
                };

                scope.closeCurrent = function () {
                    if (scope.showing == 'player') {
                        // if the task is ended, then let's just go back to the task list as it will have been
                        // ended by the checklist finishing.
                        if (scope.editing.status == 'finished') {
                            scope.showing = 'clockings';
                            scope.closeCurrent();
                            return;
                        }
                        // save what ever has been entered by the user on the current page
                        scope.$broadcast('embedded-player.save', null);
                        scope.calculateCanFinish(scope.editing);
                        scope.showing = 'clockings';
                    }
                    else if (scope.showing == 'folder') {
                        scope.showing = 'clockings';
                    }
                    else if (scope.showing == 'clockings') {
                        scope.showList();
                    }
                };

                scope.setTaskStatus = function (task, status) {
                    task.status = status;

                    if (task.workinggroupid != null) {
                        angular.forEach(scope.filteredTasks, function (t) {
                            if (t.workinggroupid == task.workinggroupid) {
                                t.status = status;
                            }
                        });
                    }
                }

                scope.clockin = function (task) {
                    var status = task.status;
                    scope.setTaskStatus(task, 'active');

                    scope.original.status = task.status;

                    scope.calculateCanFinish(task);

                    scope.toggleClockinClockout(task, null, status);

                    scope.isClockedIn = true;
                    scope.clockedInTo = task;

                    // EVS-632 Auto start 1st checklist on task start
                    if (task.tasktype.autostart1stchecklist) {
                        if (task.publishedresources && task.publishedresources.length) {
                            scope.gotoChecklist(task.publishedresources[0]);
                        }
                    }
                    scope.filterTasks();

                    if (task.tasktype.autofinishafterstart) {
                        scope.finish(task, '');
                    }
                };

                scope.cancelclockingout = function (task) {
                    scope.setTaskStatus(task, 'active');
                    scope.original.status = task.status;
                    scope.filterTasks();
                }

                scope.clockingout = function (task) {
                    scope.setTaskStatus(task, 'pausing');
                    scope.original.status = task.status;
                    scope.filterTasks();
                };

                scope.pause = function (task, pausestate) {
                    if (pausestate.requiresextranotes) {
                        task.requiresnotes = true;
                    } else {
                        scope.clockout(task, pausestate.text);
                    }
                }

                scope.clockout = function (task, pausingNotes) {
                    var status = task.status;
                    scope.setTaskStatus(task, 'paused');
                    scope.original.status = task.status;

                    task.requiresnotes = false;

                    scope.toggleClockinClockout(task, pausingNotes, status);

                    scope.isClockedIn = false;
                    scope.clockedInTo = null;
                    scope.filterTasks();
                };

                scope.clearTaskNotifications = function (task) {
                    persistantStorage.getItem('taskPromptTimes', function (taskPromptTimes) {
                        taskPromptTimes = taskPromptTimes || Object.create(null);

                        if (angular.isDefined(taskPromptTimes[task.id])) {
                            delete taskPromptTimes[task.id];
                            persistantStorage.setItem('taskPromptTimes', taskPromptTimes);
                        }
                    });
                }

                scope.toggleClockinClockout = function (task, notes, fallbacktostate) {
                    if (task.status == 'active') {
                        taskListService.setCurrentTask(task);
                    } else if (task.status == 'paused' || task.status == 'finished') {
                        taskListService.setCurrentTask(null);
                    }

                    if (task.site != null && task.site.latitude != null && task.site.longitude != null) {
                        console.debug("site has geo coords, storing for possible later use");
                        scope.lastSiteClockinClockout = { coords: { latitude: task.site.latitude, longitude: task.site.longitude } };
                    }
                    else {
                        console.debug("site has no geo coords");
                        scope.lastSiteClockinClockout = null;
                    }

                    var loc = bworkflowApi.currentLocation();

                    loc = angular.copy(loc);

                    var parameters = {
                        userid: scope.presented.userid,
                        taskid: task.id,
                        enforcesingleclockin: scope.presented.template.enforcesingleclockin,
                        location: loc,
                        notes: notes,
                        activities: task.activities,
                        attemptclaim: task.requiresclaiming
                    };

                    // No longer urgent or overdue ..
                    scope.clearTaskNotifications(task);

                    bworkflowApi.execute('TaskListManagement', 'ToggleClockinClockout', parameters).then(
                    function () { },
                    function (error) {
                        // something went wrong, we need to fall back to something
                        scope.setTaskStatus(task, fallbacktostate);
                        scope.original.status = fallbacktostate;
                        scope.filterTasks();
                    });

                    if (scope.presented.template.sorttype == 'LastClockin') {
                        scope.calculateTaskDistances();
                    }
                };

                scope.clockinAndOut = function (task, notes) {
                    var loc = angular.copy(bworkflowApi.currentLocation());

                    var parameters = {
                        userid: scope.presented.userid,
                        taskid: task.id,
                        enforcesingleclockin: scope.presented.template.enforcesingleclockin,
                        location: loc,
                        notes: notes
                    };

                    scope.setTaskStatus(task, 'paused');

                    return bworkflowApi.execute('TaskListManagement', 'ClockinAndClockout', parameters).then(
                        function () { },
                        function (error) {
                            // something went wrong, we need to fall back to something
                            scope.setTaskStatus(task, fallbacktostate);
                            scope.original.status = fallbacktostate;
                            scope.filterTasks();
                        });

                    scope.filterTasks();
                }

                scope.getTaskWorklog = function (task) {
                    var parameters = {
                        userid: scope.presented.userid,
                        taskid: task.id,
                    };

                    bworkflowApi.execute('TaskListManagement', 'GetTaskWorklog', parameters).then(function (data) {
                        task.worklog = data.log;
                    }, function (error) {
                    });
                }

                scope.finishing = function (task) {
                    // we need to work out if they are finising the task complete or incomplete
                    // if there aren't any activities on the task, then we have to ask the ESW
                    // to tell us if its complete or incomplete. If there are activities we look
                    // at their status to determine complete or incomplete.

                    if (task.activities.length == 0) {
                        // no activities, so we can't work it out on our own, so get the user to
                        // let us know.

                        scope.setTaskStatus(task, 'finishing');

                        // if we are dealing with a group of tasks we need to set a few things up on the 
                        // model
                        if (task.workinggroupid != null) {
                            task.complete = true;
                            angular.forEach(task.workinggroupdata, function (t) {
                                t.complete = true;
                            });
                        }

                        scope.original.status = task.status;
                        scope.incompleteTasks = null;
                    }
                    else {
                        // activities, so we can inspect their status and do the relevant thing based
                        // on their status.
                        var acts = $filter('filter')(task.activities, { completed: false }, true);

                        if (acts.length > 0) {
                            // there are incomplete activities
                            scope.finishingIncomplete(task);
                        }
                        else {
                            // all activities are marked as complete so just finish
                            if (confirm('Are you sure you want to finish this task') == false) {
                                return;
                            }

                            scope.setTaskStatus(task, 'finishing');
                            scope.finish(task, '');
                        }

                    }
                    scope.filterTasks();
                };

                scope.finishingIncomplete = function (task) {
                    task.previousStatus = task.status;
                    scope.setTaskStatus(task, 'finishing-incomplete');
                    scope.original.status = task.status;

                    scope.incompleteTasks = [];
                    scope.incompleteTasks.push(task);
                    scope.filterTasks();
                };

                scope.cancelfinishingincomplete = function (task) {
                    scope.setTaskStatus(task, task.previousStatus);
                    scope.original.status = task.status;

                    scope.incompleteTasks = null;
                    scope.filterTasks();
                };

                scope.findGroupFinishingDataMatch = function (taskid, items) {
                    var result = null;

                    angular.forEach(items, function (val) {
                        if (val.id == taskid) {
                            result = val;
                        }
                    });

                    return result;
                }

                scope.finishingGroup = function (task) {
                    var isFinishedComplete = task.complete;
                    scope.incompleteTasks = [];

                    if (task.complete == false) {
                        scope.incompleteTasks.push(task);
                    }

                    angular.forEach(task.workinggroupdata, function (val) {
                        if (val.complete == false) {
                            isFinishedComplete = false;
                            scope.incompleteTasks.push(val);
                        }
                    });

                    // everything marked as finished, so no more to do here
                    if (isFinishedComplete == true) {
                        scope.finish(task, '');
                        return;
                    }

                    scope.setTaskStatus(task, 'finishing-incomplete');
                    scope.original.status = task.status;

                    // ok the group side of things causes a little more work for us as
                    // we need to get the finish statuses for the other tasks in the group.
                    var parameters = { taskid: task.id, userid: scope.presented.userid };

                    var current = task;

                    bworkflowApi.execute('TaskListManagement', 'GetGroupFinishingDetails', parameters).then(function (data) {
                        // merge the finishing statuses returned for each task with the workinggroupdata objects we already have
                        angular.forEach(task.workinggroupdata, function (val) {
                            var gt = scope.findGroupFinishingDataMatch(val.id, data.groupdata);

                            if (gt == null) {
                                return;
                            }

                            val.finishedstatuses = gt.finishedstatuses;
                        });

                    }, function (error) {
                    });
                    scope.filterTasks();
                };

                scope.finish = function (task, notes, clockout, overrideCanContinue, preventShowList) {
                    if (angular.isDefined(overrideCanContinue) == false) {
                        overrideCanContinue = false;
                    }

                    if (scope.incompleteTasks != null && overrideCanContinue == false) {
                        var canContinue = true;

                        angular.forEach(scope.incompleteTasks, function (task) {
                            task.error = false;
                            var taskCanContinue = false;

                            angular.forEach(task.finishedstatuses, function (item) {
                                if (item.selected == true) {
                                    if (item.requiresextranotes == false || (item.requiresextranotes == true && item.notes != null && item.notes != '')) {
                                        taskCanContinue = true;
                                    }
                                }
                            });

                            if (taskCanContinue == false) {
                                task.error = true;
                                canContinue = false;
                            }
                        });

                        if (canContinue == false) {
                            return;
                        }
                    }

                    var complete = task.complete;

                    if (angular.isDefined(complete) == false) {
                        complete = task.status == 'finishing';
                    }

                    var status = task.status;
                    scope.setTaskStatus(task, 'finished');
                    scope.filterTasks();
                    scope.original.status = task.status;

                    var loc = angular.copy(bworkflowApi.currentLocation());

                    var parameters = {
                        userid: scope.presented.userid,
                        taskid: task.id,
                        enforcesingleclockin: scope.presented.template.enforcesingleclockin,
                        location: loc,
                        complete: complete,
                        completionnotes: notes,
                        finishedstatuses: task.finishedstatuses,
                        workinggroupdata: null,
                        clockout: angular.isDefined(clockout) ? clockout : false,
                        activities: task.activities
                    };

                    if (task.workinggroupid != null) {
                        parameters.workinggroupdata = task.workinggroupdata;
                    }

                    RequestsErrorHandler.specificallyHandled(
                        function () {
                            $q.all({ finish: bworkflowApi.execute('TaskListManagement', 'Finish', parameters, 5000) }).then(
                                function (httpData) {
                                    // EVS-453 - Trying to click into a task you've just finished throws a red error
                                    // Remove task from the client, if we wait for server response there is a window
                                    // for the user to click the task again when it has already disappeared from the 
                                    // offline cache and throw an undefined exception
                                    scope.removeTask(task);
                                    scope.isClockedIn = false;
                                    scope.clockedInTo = null;
                                    if (angular.isDefined(preventShowList) == false || preventShowList == false) {
                                        scope.showList();
                                    }
                                    scope.getTasks();
                                },
                                function (httpData) {
                                    // well that didn't work, fall back to the last state.
                                    // We've seen bug reports where scope.original appears to be null
                                    // I haven't been able to replicate this locally, but think something
                                    // might going wrong after showList and things are somehow falling through
                                    // to this code (I can't see how). So lets just check to make sure the
                                    // state we think we are in is the state we are in.
                                    if (scope.original != null) {
                                        scope.setTaskStatus(task, status);

                                        if (angular.isDefined(task.finishErrorCount) == false) {
                                            task.finishErrorCount = 0;
                                        }
                                        task.finishErrorCount = task.finishErrorCount + 1;

                                        scope.original.status = status;
                                    }

                                    httpData.noUI = true;
                                    scope.$emit('player_broadcast_ajax_error', httpData);
                                    scope.filterTasks();
                                }
                            );
                        }
                    );
                };

                scope.calculateStartTimeSort = function () {
                    if (angular.isDefined(scope.tasks) == false) {
                        return;
                    }
                    var future = moment().add(1, 'y').valueOf();

                    angular.forEach(scope.tasks, function (task, index) {
                        if (task.starttime == null) {
                            task.sortNumber = future;
                        }
                        else {
                            task.sortNumber = task.starttime.valueOf();
                        }
                    });
                };

                scope.calculateSortOrderSort = function () {
                    if (angular.isDefined(scope.tasks) == false) {
                        return;
                    }

                    angular.forEach(scope.tasks, function (task, index) {
                        if (task.sortorder == null) {
                            task.sortNumber = 100000;
                        }
                        else {
                            task.sortNumber = task.sortorder;
                        }
                    });
                };

                scope.calculateTaskDistances = function () {
                    if (angular.isDefined(scope.tasks) == false) {
                        return;
                    }

                    var currentLocation = null;

                    if (scope.presented.template.sorttype != 'LastClockin') {
                        console.debug("Geocode sorting being used, retrieving current location");
                        currentLocation = bworkflowApi.currentLocation();
                    }
                    else {
                        if (scope.lastSiteClockinClockout == null) {
                            console.debug("Last clockin clockout sorting being used, but no last site is available, falling back to geo code");
                            currentLocation = bworkflowApi.currentLocation();
                        }
                        else {
                            console.debug("Last clockin clockout sorting being used, using last location");
                            currentLocation = scope.lastSiteClockinClockout;
                        }
                    }

                    if (currentLocation == null || angular.isDefined(currentLocation.coords) == false || currentLocation.coords == null) {
                        currentLocation = null;
                    }

                    $timeout(function () {
                        angular.forEach(scope.tasks, function (task, index) {
                            if (currentLocation == null) {
                                task.distance = bworkflowApi.noResultDistance;
                                return;
                            }

                            if (task.site == null) {
                                task.distance = bworkflowApi.noResultDistance;
                                return;
                            }

                            task.distance = bworkflowApi.distanceBetween(currentLocation.coords.latitude, currentLocation.coords.longitude, task.site.latitude, task.site.longitude);

                            // we have the distance in km's, we might need to do more with this
                            if (scope.presented.template.measurementtype == "Imperial") {
                                // we have to convert to miles and feet
                                task.distance = task.distance * 0.621371192;
                            }

                            if (scope.presented.template.sorttype != 'StartTime' && scope.presented.template.sorttype != 'SortOrder') {
                                task.sortNumber = task.distance;
                            }
                        });
                    });
                };

                scope.filterByLabel = function (label) {
                    scope.labelFilter = label;

                    scope.filterTasks();
                };

                scope.filterTasks = function () {
                    if (scope.labelFilter == scope.labelFilterAll) {
                        scope.filteredTasks = scope.tasks.slice(0);
                    } else {

                        scope.filteredTasks.length = 0;

                        angular.forEach(scope.tasks, function (task, index) {
                            var labels = $filter('filter')(task.labels, { id: scope.labelFilter.id }, true);

                            if (labels.length > 0) {
                                scope.filteredTasks.push(task);
                            }
                        });
                    }

                    scope.clockedIntoTasks = [];
                    scope.notClockedIntoTasks = [];

                    // ok, we are going to include any task that the user is clocked
                    // into in the started list even if it's on another level, which
                    // stops the case where the user is clocked in, then a higher level task comes
                    // along and they can't clockin to the higher level one as they are already
                    // clocked into one, but can't see the one they are clocked into due to the higher
                    // level task taking the UI
                    angular.forEach(scope.allTasks, function (task) {
                        if (task.status == 'active') {
                            scope.clockedIntoTasks.push(task);
                        }
                    });

                    // and now we use the filtered tasks, which will have been filtered by level etc
                    // to populate the rest
                    angular.forEach(scope.filteredTasks, function (task) {
                        if (task.status == 'active') {
                            return; // it will have been added above
                        }

                        if (task.status == 'paused' || task.status == 'pausing') {
                            scope.clockedIntoTasks.push(task);
                        } else {
                            scope.notClockedIntoTasks.push(task);
                        }
                    })
                };

                scope.startGroupCreation = function () {
                    scope.creatingGroup = true;
                };

                scope.cancelGroupCreation = function () {
                    scope.creatingGroup = false;

                    angular.forEach(scope.filteredTasks, function (task) {
                        task.selectedInGroup = false;
                    });
                };

                scope.finishGroupCreation = function () {
                    scope.creatingGroup = false;

                    var tasks = [];
                    var tasksInGroup = [];
                    angular.forEach(scope.filteredTasks, function (task) {
                        if (task.selectedInGroup == true) {
                            tasks.push(task);
                            tasksInGroup.push(task.id);
                        }
                    });

                    var loc = angular.copy(bworkflowApi.currentLocation());

                    var parameters = {
                        userid: scope.presented.userid,
                        tasks: tasksInGroup,
                        enforcesingleclockin: false,
                        location: loc
                    };

                    bworkflowApi.execute('TaskListManagement', 'CreateGroup', parameters)
                    .then(function (data) {
                        var groupId = data.groupid;

                        angular.forEach(tasks, function (task) {
                            task.workinggroupid = groupId;
                        });
                    }, function (error) {
                    });
                };

                scope.deleteGroup = function (task) {
                    if (confirm('Are you sure you want to delete this group? Note the tasks will NOT BE deleted, and you will still be able to complete the tasks individually.') == false) {
                        return;
                    }

                    var parameters = {
                        userid: scope.presented.userid,
                        workinggroupid: task.workinggroupid
                    };

                    bworkflowApi.execute('TaskListManagement', 'DeleteGroup', parameters)
                    .then(function (data) {
                        scope.showTask(scope.original);
                        scope.getTasks();
                    }, function (error) {
                    });
                };


                scope.showCreateTask = function (template) {
                    scope.creatingTask = true;
                    scope.currentTemplate = angular.extend({}, template);
                };

                scope.closeCreateTask = function () {
                    scope.creatingTask = false;
                    scope.currentTemplate = null;
                }

                scope.startRefreshCountDown = function () {
                    if (angular.isDefined(scope.stop)) return;

                    var lastFireTime;
                    scope.stop = $interval(function () {
                        var forceRefresh = false;
                        if (angular.isDefined(scope.validToDate)) {
                            forceRefresh = moment().isAfter(scope.validToDate);
                        }

                        var actives = $filter('filter')(scope.tasks, { status: 'active' }, true);
                        angular.forEach(actives, function (act) {
                            if (act.minimumdurationleft == null || act.minimumdurationleft <= 0) {
                                return;
                            }

                            act.minimumdurationleft = act.minimumdurationleft - 1;
                        });

                        if (scope.editing != null && !forceRefresh) {
                            // countdown while editing
                            return;
                        }

                        if (scope.editing == null && !scope.creatingTask) {
                            // We are displaying the Task List so can run the Tasks Added/Removed message timers
                            if (scope.tasksAdded && !angular.isDefined(scope.tasksAddedExpiry)) {
                                scope.tasksAddedExpiry = moment().add(scope.presented.template.refreshperiodseconds, 'seconds');
                            }
                            if (angular.isDefined(scope.tasksAddedExpiry)) {
                                if (moment().isAfter(scope.tasksAddedExpiry)) {
                                    scope.tasksAdded = false;
                                    delete scope.tasksAddedExpiry;
                                }
                            }

                            if (scope.tasksRemoved && !angular.isDefined(scope.tasksRemovedExpiry)) {
                                scope.tasksRemovedExpiry = moment().add(scope.presented.template.refreshperiodseconds, 'seconds');
                            }
                            if (angular.isDefined(scope.tasksRemovedExpiry)) {
                                if (moment().isAfter(scope.tasksRemovedExpiry)) {
                                    scope.tasksRemoved = false;
                                    delete scope.tasksRemovedExpiry;
                                }
                            }

                            // we now check the late after stuff and update the flag on the task to manage that
                            var now = moment();

                            persistantStorage.getItem('taskPromptTimes', function (taskPromptTimes) {
                                var urgentTasks = [];
                                var overdueTasks = [];

                                taskPromptTimes = taskPromptTimes || Object.create(null);
                                angular.forEach(scope.tasks, function (task) {
                                    var promptTimes = taskPromptTimes[task.id] || Object.create(null);
                                    if (task.status == 'new' && task.notifyuser && angular.isUndefined(promptTimes.urgent)) {
                                        promptTimes.urgent = new Date;
                                        taskPromptTimes[task.id] = promptTimes;
                                        urgentTasks.push(task);
                                    }

                                    if (task.lateafter) {
                                        task.islate = now.isAfter(task.lateafter);

                                        if (task.status == 'new' && task.islate && scope.presented.template.promptlatetasks && angular.isUndefined(promptTimes.overdue)) {
                                            promptTimes.overdue = new Date;
                                            taskPromptTimes[task.id] = promptTimes;
                                            overdueTasks.push(task);
                                        }
                                    }
                                });

                                if (urgentTasks.length || overdueTasks.length) {
                                    persistantStorage.setItem('taskPromptTimes', taskPromptTimes);
                                    if (urgentTasks.length) {
                                        var msg = urgentTasks.map('name').join('\n');
                                        navigatorNotification.alert(msg, null, urgentTasks.length == 1 ? 'Urgent task' : 'Urgent tasks');
                                    }
                                    if (overdueTasks.length) {
                                        var msg = overdueTasks.map('name').join('\n');
                                        navigatorNotification.alert(msg, null, overdueTasks.length == 1 ? 'Overdue task' : 'Overdue tasks');
                                    }
                                }
                            })
                        }

                        if (angular.isDefined(scope.firstGetTasks) == false) {
                            scope.firstGetTasks = true;
                        }
                        else {
                            scope.firstGetTasks = false;
                        }

                        var forceOnline = scope.firstGetTasks || forceRefresh;
                        var shouldGetTasks = forceOnline;

                        // Check if a Task refresh is required at all
                        if (!shouldGetTasks && scope.presented.template.refreshperiodseconds) {
                            scope.refreshCountDown = scope.refreshCountDown - 1;

                            if (scope.refreshCountDown < 0) {
                                scope.refreshCountDown = 0;
                            }

                            if (scope.refreshCountDown == 0) {
                                shouldGetTasks = true;
                            }
                        }

                        if (shouldGetTasks) {
                            // reset the refresh countdown
                            scope.refreshCountDown = scope.presented.template.refreshperiodseconds;
                            if (forceOnline) {
                                // we cannot be editing a task if forced online
                                scope.editing = null;
                                scope.creatingTask = false;

                                // EVS-558 Force a check of App version update
                                delete scope.validToDate;

                                pageRefresh.beginChecking();
                            }
                            scope.getTasks(forceOnline);
                        }
                    }, 1000);

                };

                scope.$on('photo-manager.phototaken', function (event, args) {
                    var param = {
                        userid: scope.presented.userid,
                        taskid: scope.editing.id,
                        id: generateCombGuid(),
                        text: scope.editing.name + ' photo.jpg',
                        data: args.data
                    };

                    args.image.mediaid = param.id;

                    bworkflowApi.execute('TaskListManagement', 'SavePhoto', param)
                    .then(function (data) {
                        scope.editing.photos.push({ mediaid: param.id });

                        scope.calculateCanFinish(scope.editing);
                    }, function (error) {
                    });
                });

                scope.$on('photo-manager.photoremoved', function (event, args) {
                    var param = {
                        userid: scope.presented.userid,
                        taskid: scope.editing.id,
                        mediaid: args.photo.mediaid
                    };

                    bworkflowApi.execute('TaskListManagement', 'RemovePhoto', param)
                        .then(function (data) {
                            var i = scope.editing.photos.findIndex(function (p) { return p.mediaid == param.mediaid; });
                            if (i >= 0) {
                                scope.editing.photos.splice(i, 1);
                            }
                            scope.calculateCanFinish(scope.editing);
                    }, function (error) {
                    });
                });

                scope.$on('question-task-list-task-type-editor.changed', function (event, args) {
                    var param = {
                        userid: scope.presented.userid,
                        taskid: scope.editing.id,
                        tasktypeid: scope.editing.subtasktypeid
                    };

                    bworkflowApi.execute('TaskListManagement', 'ChangeTaskType', param)
                    .then(function (data) {
                    }, function (error) {
                    });
                });

                scope.stopRefreshCountDown = function () {
                    if (angular.isDefined(scope.stop)) {
                        $interval.cancel(scope.stop);
                        scope.stop = undefined;
                    }
                };

                scope.$on('$destroy', function () {
                    // Make sure that the interval is destroyed too
                    scope.stopRefreshCountDown();
                });

                // if its not already doing so, get it to do
                scope.positionWatch = {};
                scope.positionWatch = bworkflowApi.watchGPSLocation();

                // EVS-396 GeoSorting of the task list to be only once every 10sec
                // Use a combintation of SugarJS Delay and Throttle functions to give behaviour of
                // - Delay the GPS change by 10 second and then Throttle it by 10 second to make sure
                // 1. Do not recalculate more often than 10 seconds (eg)
                // 2. If during the delay another GPS occurs that it does not reset the 10second timer (like a plain Debounce would)
                var throttledCalculateTaskDistances = scope.calculateTaskDistances;
                if (scope.presented.template.sortthrottleseconds) {
                    throttledCalculateTaskDistances = throttledCalculateTaskDistances.throttle(scope.presented.template.sortthrottleseconds * 1000);
                }

                scope.$watch('positionWatch.position.coords', function (newValue, oldValue) {
                    if (scope.presented.template.sorttype == 'Geocode' || (scope.presented.template.sorttype == 'LastClockin' && scope.lastSiteClockinClockout == null)) {
                        throttledCalculateTaskDistances.delay(scope.presented.template.sortthrottleseconds * 1000);
                    }
                });

                bworkflowApi.supportOffline('TaskListManagement', scope.presented.template.supportoffline);

                scope.startRefreshCountDown();
            }
        });
    }]);

questionsModule.directive('questionTasklistFolder', ['appUpdateMonitor',
    function (appUpdateMonitor) {
        return {
            templateUrl: 'question_tasklist_folder_ui.html',
            restrict: 'E',
            link: function (scope, element, attrs) {
                appUpdateMonitor.addPauser(scope);      // Pause updates whilst our scope is alive
            }
        };
    }]);

questionsModule.factory('createNewTaskWorkflow', ['$q', 'bworkflowApi', 'RequestsErrorHandler', 'taskTypesService', function ($q, bworkflowApi, RequestsErrorHandler, taskTypesSvc) {
    var _taskTypes;
    taskTypesSvc.getTaskTypes().then(function (taskTypes) {
        _taskTypes = taskTypes;
    });

    return {
        createAndClockIn: function (template, userid, enforceSingleClockin) {
            var deferred = $q.defer();
            var loc = angular.copy(bworkflowApi.currentLocation());

            var parameters = {
                userid: userid,
                template: template,
                clockin: true,
                location: loc,
                enforcesingleclockin: enforceSingleClockin
            };

            RequestsErrorHandler.specificallyHandled(
                function () {
                    bworkflowApi.execute('TaskListManagement', 'NewTask', parameters, 5000).then(
                        function (data) {
                            deferred.resolve(data);
                        },
                        function (reason) {
                            deferred.reject(reason);
                        },
                        function (notify) {
                            deferred.notify(notify);
                        }
                    );
                }
            );

            return deferred.promise;
        }        
    }
}])

questionsModule.run(['bworkflowApi', '$rootScope', function (bworkflowApi, $rootScope) {
    $rootScope.$on('dashboard.show', function () {
        var cacheExecutor = bworkflowApi.registerODataCacheExecutor({
            key: 'sites.searchByName',
            feed: 'Members',
            query: {
                $filter: 'IsASite eq true',
                $orderby: 'Name',
                $select: 'UserId,Name',
                datascope: {
                    ExcludeOutsideHierarchies: false,
                    IncludeCurrentUser: true,
                    ExcludeExpired: true
                }
            },
            filter: function (data, params) {
                return data.filter(function (r) {
                    return r.Name.toLowerCase().indexOf(params.searchmemberssearch.toLowerCase()) >= 0;
                });
            },
            expireSeconds: 60 * 60
        });
        cacheExecutor.refresh();
    });
}]);

questionsModule.directive('questionTasklistNewtaskUi', ['$timeout',
    'bworkflowApi', '$animate', 'RequestsErrorHandler', '$q', 'sectionUtils', 'tabletProfile', 'appUpdateMonitor', 'createNewTaskWorkflow',
    function ($timeout, bworkflowApi, $animate, RequestsErrorHandler, $q, sectionUtils, tabletProfile, appUpdateMonitor, createNewTaskWorkflow) {
        return {
            templateUrl: 'question_tasklist_newtask_ui.html',
            restrict: 'E',
            require: 'ngModel',
            scope:
            {
                template: '=ngModel',
                presented: '='
            },
            link: function (scope, element, attrs) {
                appUpdateMonitor.addPauser(scope);      // Pause updates whilst our scope is alive

                scope.selection = {

                };

                scope.memberFeedTemplate = {
                    name: 'members',
                    feed: 'Members',
                    filter: "substringof('[[searchmemberssearch]]', Name) and IsASite eq true",
                    orderbyfields: 'Name',
                    idfields: ['UserId'],
                    selectfields: 'UserId,Name',
                    itemsperpage: 5,
                    datascope: {
                        ExcludeOutsideHierarchies: false,
                        IncludeCurrentUser: true,
                        ExcludeExpired: true
                    },
                    parameterdefinitions: [
                        {
                            internalname: 'searchmemberssearch'
                        }],
                    cache: {
                        key: 'sites.searchByName'
                    }
                };

                scope.memberFeed = bworkflowApi.createDataFeed(scope.memberFeedTemplate, scope);

                var fnUnsubscribeTabletProfile;
                fnUnsubscribeTabletProfile = tabletProfile(function (profile) {
                    if (profile.siteid) {
                        var defaultMemberFeedTemplate = {
                            name: 'members',
                            feed: 'Members',
                            filter: "UserId eq guid'" + profile.siteid + "'",
                            orderbyfields: 'Name',
                            idfields: ['UserId'],
                            itemsperpage: 5
                        };

                        var defaultMemberFeed = bworkflowApi.createDataFeed(defaultMemberFeedTemplate, scope);
                        defaultMemberFeed.afterLoadHooks.push(function (feed) {
                            if (feed.data.length) {
                                scope.selection.site = feed.data[0];
                            }
                        });
                        defaultMemberFeed.getData(true);
                    }

                    fnUnsubscribeTabletProfile();
                });

                scope.create = function () {
                    // Prevent double-click immediately
                    scope.isCreateDisabled = true;
                    scope.isCreatingOnline = false;

                    if (scope.selection.site) {
                        scope.template.siteid = scope.selection.site.alldata.UserId;
                    }

                    createNewTaskWorkflow.createAndClockIn(scope.template, scope.presented.userid, scope.presented.template.enforcesingleclockin).then(function (data) {
                        scope.isCreateDisabled = false;
                        scope.isCreatingOnline = false;
                        scope.offline = false;
                        scope.$emit('question-task-list.created-new-task', data);
                    }, function (error) {
                        scope.isCreateDisabled = false;
                        scope.isCreatingOnline = false;
                        scope.offline = true;
                    }, function (notify) {
                        scope.isCreatingOnline = !notify.offline;
                    });
                };
            }
        };
    }]);

questionsModule.directive('questionTasklistClockinclockoutUi', ['$timeout', 'appUpdateMonitor',
    function ($timeout, appUpdateMonitor) {
        return {
            templateUrl: 'question_tasklist_clockinclockout_ui.html',
            restrict: 'E',
            link: function (scope, element, attrs) {
                appUpdateMonitor.addPauser(scope);      // Pause updates whilst our scope is alive

                scope.viewinggroupinformation = false;
                scope.showingdocumentation = false;
                scope.showSetTaskType = false;

                scope.showGroupInformation = function () {
                    scope.viewinggroupinformation = true;
                };

                scope.gotoTask = function (task) {
                    scope.viewinggroupinformation = false;

                    scope.showTaskById(task.id);
                };

                scope.toggleShowDocuments = function () {
                    scope.showingdocumentation = !scope.showingdocumentation;
                };

                scope.showChangeTaskType = function () {
                    scope.showSetTaskType = true;
                };

                scope.$on('question-task-list-task-type-editor.changed', function () {
                    scope.showSetTaskType = false;
                });

                scope.canStart = function (task) {
                    if (task.starttime) {
                        return moment().isAfter(task.starttime);
                    }
                    return true;
                };

                scope.completeactivities = function (activities) {
                    if(activities == null)
                    {
                        return;
                    }

                    angular.forEach(activities, function (activity) {
                        activity.completed = true;
                    });
                }
            }
        };
    }]);

questionsModule.directive('questionTasklistTaskTypeEditor', [
    function () {
        return {
            templateUrl: 'questions/question_tasklist_helpers/question_tasklist_task_type_editor.html',
            restrict: 'E',
            scope: {
                task: '=ngModel'
            },
            link: function (scope, element, attrs) {
                scope.setTaskType = function (taskType) {
                    scope.task.subtasktypeid = taskType.id;

                    scope.$emit('question-task-list-task-type-editor.changed');
                };
            }
        };
    }]);

questionsModule.directive('questionTasklistDesktopUi', ['$http', '$sce', '$filter', 'appUpdateMonitor',
    function ($http, $sce, $filter, appUpdateMonitor) {
        return {
            templateUrl: 'question_tasklist_desktop_ui.html',
            restrict: 'E',
            link: function (scope, element, attrs) {
                appUpdateMonitor.addPauser(scope);      // Pause updates whilst our scope is alive

                scope.addComment = function (task) {
                    scope.clockinAndOut(task, task.pausingnotes).then(function (data) {
                        task.pausingnotes = "";
                        scope.hasFocus = false;
                        scope.getTaskWorklog(scope.editing);
                    });
                };

                scope.commitChanges = function (task) {
                    var selected = $filter('filter')(scope.editing.statuses, { id: scope.editing.statusid }, true)[0];

                    if (selected.referencenumbermode == 2 && (scope.editing.referencenumber == null || scope.editing.referencenumber == '')) {
                        alert('Reference number is mandatory');
                        return;
                    }
                    else if (selected.referencenumbermode == 0) {
                        task.referencenumber = '';
                    }

                    scope.editing.hasChanges = false;

                    scope.saveChanges({ statusid: task.statusid, referencenumber: task.referencenumber });
                };

                scope.$watch('editing', function (newValue, oldValue) {
                    if (newValue != null) {
                        scope.getTaskWorklog(newValue);
                    }
                });

                scope.buildODataUrl = function (feed) {
                    var url = scope.presented.localurl;

                    if (url == null || url == '') {
                        url = 'odata';
                    }

                    url = url + "/" + feed;

                    return url;
                };

                scope.getTasksInGroup = function () {
                    var url = scope.buildODataUrl('Tasks');

                    url = url + "?$filter=GroupId eq guid'" + scope.editing.projectjobtaskgroupid + "'&$orderby=DateCreated asc&datascope=IncludeCurrentUser";
                    $http({ url: url, method: 'GET' })
                    .success(function (data) {
                        scope.editing.relatedtasks = data.value;
                    });
                };

                scope.showRelatedTask = function (task) {
                    scope.viewingtask = true;
                    scope.currenttask = task;

                    scope.getChecklistDetailsForTask(task);
                };

                scope.getChecklistDetailsForTask = function (task) {
                    var url = scope.buildODataUrl('ChecklistResults');

                    url = url + "?$filter=TaskId eq guid'" + task.Id + "'&$orderby=Checklist&datascope=IncludeCurrentUser";
                    $http({ url: url, method: 'GET' })
                    .success(function (data) {
                        // struggling to get the groupby filter to behave, so I am going to manually do the grouping here
                        var checklists = {};
                        angular.forEach(data.value, function (value, index) {
                            if (angular.isDefined(checklists[value.WorkingDocumentId]) == false) {
                                checklists[value.WorkingDocumentId] =
                                {
                                    Name: value.Checklist,
                                    Questions: []
                                };
                            }

                            checklists[value.WorkingDocumentId].Questions.push(value);
                        });

                        task.checklists = checklists;
                    });
                };

                if (scope.editing.projectjobtaskgroupid != null) {
                    // we need to go off and load the set of tasks associated with this task
                    scope.getTasksInGroup();
                }
            }
        };
    }]);

questionsModule.directive('questionTasklistStartfinishUi', ['$timeout', 'appUpdateMonitor',
    function ($timeout, appUpdateMonitor) {
        return {
            templateUrl: 'question_tasklist_startfinish_ui.html',
            restrict: 'E',
            link: function (scope, element, attrs) {
                appUpdateMonitor.addPauser(scope);      // Pause updates whilst our scope is alive

                scope.doFinish = function () {
                    scope.editing.complete = true;

                    scope.finish(scope.editing, '', true);
                };
            }
        };
    }]);

questionsModule.directive('questionTasklistProductorderingUi', ['$timeout', 'appUpdateMonitor', 'bworkflowApi',
    function ($timeout, appUpdateMonitor, bworkflowApi) {
        return {
            templateUrl: 'question_tasklist_productordering_ui.html',
            restrict: 'E',
            link: function (scope, element, attrs) {
                appUpdateMonitor.addPauser(scope);      // Pause updates whilst our scope is alive

                scope.reviewing = false;

                scope.editOrder = function (task, catalog) {
                    var order = null;
                    angular.forEach(task.orders, function (o) {
                        if (o.catalogid == catalog.id) {
                            order = o;
                        }
                    });

                    if (order == null) {
                        order = {
                            __isnew: true,
                            id: generateCombGuid(),
                            items: [],
                            catalogid: catalog.id
                        };

                        task.orders.push(order);
                    }

                    scope.currentOrder = order;
                    scope.currentCatalog = angular.copy(catalog);

                    scope.populateCatalog(scope.currentCatalog, scope.currentOrder, false);
                };

                // takes an order and populates the catalog with the quantities ordered
                scope.populateCatalog = function (catalog, order, onlyOrders) {
                    var toRemove = [];

                    angular.forEach(catalog.items, function (item) {
                        item.quantity = null;

                        if (catalog.stockcountwhenordering == true) {
                            item.stock = null;
                        }

                        var orderItem = null;
                        angular.forEach(order.items, function (oi) {
                            if (oi.productid == item.id) {
                                orderItem = oi;
                            }
                        });

                        if (orderItem != null) {
                            item.quantity = orderItem.quantity;
                            item.price = orderItem.price;

                            if (catalog.stockcountwhenordering == true) {
                                item.stock = orderItem.stock;
                            }
                        }

                        if (onlyOrders) {
                            if (scope.isZeroValue(item.quantity) && scope.isZeroValue(item.stock)) {
                                toRemove.push(item);
                            }
                        }
                    });

                    angular.forEach(toRemove, function (r) {
                        var index = catalog.items.indexOf(r);
                        if (index != -1) {
                            catalog.items.splice(index, 1);
                        }
                    });
                };

                scope.isZeroValue = function (value) {
                    return angular.isDefined(value) == false ||
                        value == null ||
                        value == 0;
                };

                scope.calculateQuantity = function (item, catalog) {
                    if (catalog.stockcountwhenordering == false) {
                        return;
                    }

                    var stock = parseInt(item.stock);

                    if (isNaN(stock)) {
                        return;
                    }

                    item.quantity = item.minstocklevel - stock;

                    if (item.quantity < 0) {
                        item.quantity = 0;
                    }
                };

                scope.changeReviewing = function (isReviewing) {
                    scope.reviewing = isReviewing;
                };

                scope.populateOrder = function (catalog, order) {
                    var items = order.items; // at the end this will hold the items that should be removed
                    order.items = [];

                    angular.forEach(catalog.items, function (item) {
                        if (scope.isZeroValue(item.quantity) && scope.isZeroValue(item.stock)) {
                            return;
                        }

                        var orderItem = null;
                        angular.forEach(items, function (oi) {
                            if (oi.productid == item.id) {
                                orderItem = oi;
                            }
                        });

                        if (orderItem == null) {
                            orderItem = {
                                id: generateCombGuid(),
                                __isnew: true,
                                productid: item.id
                            };
                        }
                        else {
                            orderItem.__isnew = false;
                            var index = items.indexOf(orderItem);
                            if (index != -1) {
                                items.splice(index, 1);
                            }
                        }

                        order.items.push(orderItem);
                        orderItem.quantity = item.quantity;
                        orderItem.price = item.price;

                        if (catalog.stockcountwhenordering == true) {
                            orderItem.stock = item.stock;
                            orderItem.minstocklevel = item.minstocklevel; // we store this against the order item so that we maintain history of the value at the time of ordering
                        }
                    });
                };

                scope.saveOrder = function (catalog, order) {
                    scope.populateOrder(catalog, order);

                    bworkflowApi.execute('TaskListManagement', 'SaveOrder', {
                        taskid: scope.editing.id,
                        order: angular.copy(scope.currentOrder),
                        userid: scope.presented.userid
                    }).then(function (data) {
                        order.__isnew = false;

                        angular.forEach(order.items, function (item) {
                            item.__isnew = false;
                        });
                    });

                    scope.currentOrder = null;
                    scope.currentCatalog = null;
                };

                scope.cancelEdit = function (catalog, order) {
                    scope.currentOrder = null;
                    scope.currentCatalog = null;
                };

                scope.deleteOrder = function (catalog, order) {
                    if (confirm('Are you sure you want to delete the order') == false) {
                        return;
                    }

                    var index = scope.editing.orders.indexOf(scope.currentOrder);
                    if (index != -1) {
                        scope.editing.orders.splice(index, 1);
                    }

                    if (angular.isDefined(scope.currentOrder.__isnew) == false || scope.currentOrder.__isnew == false) {
                        // it's been saved onto the server, so lets let that know
                        bworkflowApi.execute('TaskListManagement', 'DeleteOrder', { id: scope.currentOrder.id });
                    }

                    scope.currentOrder = null;
                    scope.currentCatalog = null;
                };

                scope.reviewOrders = function () {
                    scope.reviewCatalogs = [];

                    angular.forEach(scope.editing.orders, function (order) {
                        var catalog = null;

                        angular.forEach(scope.editing.catalogs, function (c) {
                            if (order.catalogid == c.id) {
                                catalog = c;
                            }
                        });

                        catalog = angular.copy(catalog);

                        scope.populateCatalog(catalog, order, true);

                        scope.reviewCatalogs.push(catalog);
                    });

                    scope.reviewing = true;
                }

                scope.doFinish = function () {
                    // ok so if nothing is ordered, then we'll finish this as incomplete
                    var hasItems = false;

                    angular.forEach(scope.editing.orders, function (order) {
                        angular.forEach(order.items, function (item) {
                            if (scope.isZeroValue(item.quantity) == false || scope.isZeroValue(item.stock) == false) {
                                hasItems = true;
                            }
                        });
                    });

                    if (hasItems == false) {
                        scope.finishingIncomplete(scope.editing);
                        scope.editing.complete = false;
                    }
                    else {
                        scope.editing.complete = true;
                    }

                    scope.finish(scope.editing, '', true, true);
                };
            }
        };
    }]);

questionsModule.directive('questionMultiplefacilityorder', ['$timeout', '$sce', 'orderSvc', 'bworkflowApi', 'languageTranslate', function ($timeout, $sce, orderSvc, bworkflowApi, languageTranslate) {
    return $.extend({}, questionDirectiveBase, {
        templateUrl: 'question_multiplefacilityorder.html',
        link: function (scope, elt, attrs) {
            questionDirectiveBase.link(scope, undefined, bworkflowApi, languageTranslate);

            scope.answer = scope.presented.Answer;
        }
    })
}]);

questionsModule.directive('questionTaskBatchOperations', ['$timeout', 'languageTranslate', function ($timeout, languageTranslate) {
    return $.extend({}, questionDirectiveBase, {
        templateUrl: 'question_task_batch_operations.html',
        link: function (scope, elt, attrs) {
            questionDirectiveBase.link(scope, undefined, undefined, languageTranslate);

        }
    })
}]);

questionsModule.directive('questionTaskSchedule', ['bworkflowApi', '$interval', '$sce', '$cookieStore', '$timeout', 'languageTranslate', function (bworkflowApi, $interval, $sce, $cookieStore, $timeout, languageTranslate) {
    return $.extend({}, questionDirectiveBase, {
        templateUrl: 'question_task_schedule.html',
        link: function (scope, elt, attrs) {
            questionDirectiveBase.link(scope, undefined, bworkflowApi, languageTranslate);

            scope.init = function () {
                scope.answer = {};
                scope.answer.state = 'loading';
            };

            scope.getschedule = function (criteria) {
                scope.init();

                var parameters = { userid: scope.presented.userid, criteria: criteria };

                bworkflowApi.execute('TaskScheduleView', 'GetSchedule', parameters)
                    .then(function (data) {
                        scope.answer.hourwidth = 50;

                        scope.answer.hours = [];
                        for (var i = 0; i < 24; i++) {
                            scope.answer.hours.push(i);
                        }

                        scope.answer.schedule = data;
                        scope.answer.state = "loaded";
                    }, function (tasks) {

                    });
            }

            scope.datetimeAsSeconds = function (time) {
                var startOfDay = moment(time).startOf('day');

                return moment(time).diff(startOfDay, 'seconds');
            };

            scope.getschedule(scope.presented.criteria);
        }
    });
}]);

questionsModule.directive('questionTaskAdministration', ['bworkflowApi', '$sce', 'languageTranslate', function (bworkflowApi, $sce, languageTranslate) {
    return $.extend({}, questionDirectiveBase, {
        templateUrl: 'question_task_administration.html',
        link: function (scope, element, attrs, ngModel) {
            questionDirectiveBase.link(scope, undefined, bworkflowApi, languageTranslate);
            scope.criteria = {};

            scope.editingScheduledTask = false;
            scope.currentScheduledTask = null;

            scope.applyFormatsToScheduledTask = function (value) {
                value.calendarrule.startdate = moment(value.calendarrule.startdate).format('DD-MM-YYYY');

                if (value.calendarrule.enddate != null) {
                    value.calendarrule.enddate = moment(value.calendarrule.enddate).format('DD-MM-YYYY');
                }
            };

            scope.loadTasks = function () {
                if (angular.isDefined(scope.criteria.projectid) == false) {
                    return;
                }

                if (angular.isDefined(scope.criteria.userid) == false) {
                    return;
                }

                bworkflowApi.execute('TaskAdministration', 'GetTasks', scope.criteria)
                .then(function (data) {

                    angular.forEach(data.scheduledtasks, function (value, key) {
                        scope.applyFormatsToScheduledTask(value);
                    });

                    scope.schedules = data.scheduledtasks;
                }, function (tasks) {

                });
            };

            scope.editScheduledTask = function (s) {
                scope.currentScheduledTask = s;
                scope.editingScheduledTask = true;
            };

            scope.newScheduledTask = function () {
                if (angular.isDefined(scope.criteria.projectid) == false) {
                    return;
                }

                if (angular.isDefined(scope.criteria.userid) == false) {
                    return;
                }

                scope.currentScheduledTask = {
                    name: 'New Task',
                    projectid: scope.criteria.projectid,
                    userid: scope.criteria.userid,
                    calendarrule: {
                        frequency: 'Daily',
                        startdate: moment().format('DD-MM-YYYY'),
                        enddate: null,
                        sunday: false,
                        monday: false,
                        tuesday: false,
                        wednesday: false,
                        thursday: false,
                        friday: false,
                        saturday: false
                    }
                };

                scope.editingScheduledTask = true;
            }

            scope.saveScheduledTask = function (s) {
                var copy = angular.copy(s);

                copy.calendarrule.startdate = moment(s.calendarrule.startdate, 'DD-MM-YYYY').format();
                if (s.calendarrule.enddate != null) {
                    copy.calendarrule.enddate = moment(s.calendarrule.enddate, 'DD-MM-YYYY').format();
                }

                bworkflowApi.execute('TaskAdministration', 'SaveScheduledTask', copy)
                .then(function (data) {
                    scope.applyFormatsToScheduledTask(data);

                    angular.copy(data, s);
                }, function (tasks) {

                });
            };
        }
    });
}]);

questionsModule.directive('questionTimesheetApproval', ['bworkflowApi', '$interval', '$sce', '$cookieStore', '$timeout', 'languageTranslate', function (bworkflowApi, $interval, $sce, $cookieStore, $timeout, languageTranslate) {
    return $.extend({}, questionDirectiveBase, {
        templateUrl: 'question_timesheet_approval.html',
        link: function (scope, elt, attrs) {
            questionDirectiveBase.link(scope, undefined, bworkflowApi, languageTranslate);
            scope.init = function () {
                scope.answer = {};
                scope.answer.criteriastate = 'loading';
                scope.answer.selectedcriteria = {};

                scope.answer.timesheetstate = 'empty';

                scope.answer.timesheetdetails = {};
            };

            scope.getcriteria = function () {
                scope.init();

                var parameters = { userid: scope.presented.userid };

                bworkflowApi.execute('TimesheetApproval', 'GetCriteria', parameters)
                    .then(function (data) {
                        scope.answer.availablecriteria = data;
                        scope.answer.selectedcriteria.startdate = moment(data.startdate).format('DD-MM-YYYY');
                        scope.answer.selectedcriteria.enddate = moment(data.enddate).format('DD-MM-YYYY');

                        scope.answer.criteriastate = "loaded";
                    }, function (tasks) {

                    });
            };

            scope.gettimesheet = function (criteria) {
                criteria.userid = scope.presented.userid;

                bworkflowApi.execute('TimesheetApproval', 'GetTimesheet', criteria)
                    .then(function (data) {
                        scope.answer.timesheet = data;
                        scope.answer.timesheetstate = 'loaded';
                    }, function (tasks) {

                    });
            };

            scope.formatDate = function (date) {
                return moment(date).format('DD-MM-YYYY');
            };

            scope.formatTime = function (time) {
                if (time == null) {
                    return "no entries";
                }

                return moment().startOf('day').seconds(time).format('H:mm:ss');
            };

            scope.hummanizeTime = function (timeAsString) {
                if (timeAsString == null || timeAsString == '') {
                    return '';
                }

                if (moment(timeAsString, 'HH:mm:ss').isValid() == false) {
                    return "invalid time";
                }

                return moment.duration(timeAsString).format('h [hrs] m [min]');
            };

            scope.datetimeAsSeconds = function (time) {
                var startOfDay = moment(time).startOf('day');

                return moment(time).diff(startOfDay, 'seconds');
            };

            scope.timeAsSeconds = function (time) {
                var startOfDay = moment(time, 'HH:mm:ss').startOf('day');

                return moment(time, 'HH:mm:ss').diff(startOfDay, 'seconds');
            };

            scope.approveforuser = function (item) {
                var parameters = { userid: scope.presented.userid, projectid: scope.answer.selectedcriteria.projectid, startdate: scope.answer.selectedcriteria.startdate, enddate: scope.answer.selectedcriteria.enddate, empid: item.userid }

                bworkflowApi.execute('TimesheetApproval', 'EditTimesheetForUser', parameters)
                    .then(function (data) {
                        angular.copy(data.items[0], item);
                    }, function (tasks) {

                    });
            };

            scope.showdetails = function (item, log) {
                var parameters = { userid: scope.presented.userid, empid: item.userid, date: log.date, projectid: scope.answer.selectedcriteria.projectid };

                bworkflowApi.execute('TimesheetApproval', 'GetTimesheetDetail', parameters)
                    .then(function (data) {
                        scope.answer.timesheetdetails.projectname = data.projectname;
                        scope.answer.timesheetdetails.totalduration = data.totalduration;
                        scope.answer.timesheetdetails.list = data.list;
                        scope.answer.timesheetdetails.timesheet = data.timesheet;
                        scope.answer.timesheetdetails.approvedDuration = '';

                        if (data.timesheet.duration != null) {
                            scope.answer.timesheetdetails.approvedDuration = scope.formatTime(data.timesheet.duration);
                        }

                        scope.answer.timesheetdetails.rowtitle = data.rowtitle;

                        scope.answer.current = {};
                        scope.answer.current.item = item;
                        scope.answer.current.log = log;

                        scope.answer.timesheetdetails.name = item.name;
                        scope.answer.timesheetdetails.date = log.date;
                        scope.answer.timesheetdetails.hourwidth = 50;
                        scope.answer.timesheetdetails.hours = [];
                        for (var i = 0; i < 24; i++) {
                            scope.answer.timesheetdetails.hours.push(i);
                        }

                        scope.answer.showdetails = true;

                        $timeout(function () { elt.find('.timesheetdetails').modal('show'); });
                    }, function (tasks) {

                    });
            };

            scope.saveandclosedetails = function (timesheetdetails) {
                // we need to manage the duration variable 
                if (timesheetdetails.approvedDuration == null || timesheetdetails.approvedDuration == "") {
                    timesheetdetails.timesheet.duration = null;
                }
                else {

                    if (moment(timesheetdetails.approvedDuration, 'HH:mm:ss').isValid() == false) {
                        return;
                    }

                    timesheetdetails.timesheet.duration = scope.timeAsSeconds(timesheetdetails.approvedDuration);
                }

                scope.edittimesheetdetail(timesheetdetails.timesheet);

                elt.find('.timesheetdetails').modal('hide');
            };

            scope.edittimesheetdetail = function (item) {

                item.userid = scope.presented.userid;

                bworkflowApi.execute('TimesheetApproval', 'EditTimesheetDetail', item)
                    .then(function (data) {

                        var parameters = { userid: scope.presented.userid, projectid: scope.answer.selectedcriteria.projectid, empid: scope.answer.current.item.userid, startdate: moment(scope.answer.current.log.date).format('DD-MM-YYYY') };

                        bworkflowApi.execute('TimesheetApproval', 'GetTimesheet', parameters)
                            .then(function (data) {
                                if (data.items.length > 0) {
                                    angular.copy(data.items[0].logs[0], scope.answer.current.log);
                                }
                                else {
                                    // so there is no clock in or a timesheet for the user in the project on the day (the server has nothing), we'll blank things out ourselves
                                    scope.answer.current.log.actualduration = null;
                                    scope.answer.current.log.approvedduration = null;
                                }

                            }, function (tasks) {

                            });

                    }, function (tasks) {

                    });
            };

            scope.getcriteria();
        }
    });
}]);

questionsModule.directive('uniqueusername', ['bworkflowApi', function (bworkflowApi) {
    return {
        require: "ngModel",
        link: function (scope, element, attrs, ngModel) {
            element.blur(function () {
                var parameters = { username: element.val() };

                bworkflowApi.execute('CompanySummary', 'UsernameExists', parameters)
                    .then(function (data) {
                        ngModel.$setValidity('uniqueusername', !data.exists);
                    }, function (tasks) {
                        ngModel.$setValidity('uniqueusername', false);
                    });
            });
        }
    };
}]);

questionsModule.directive('questionCompanySummary', ['bworkflowApi', '$interval', '$sce', '$cookieStore', '$timeout', 'languageTranslate', function (bworkflowApi, $interval, $sce, $cookieStore, $timeout, languageTranslate) {
    return $.extend({}, questionDirectiveBase, {
        templateUrl: 'question_company_summary.html',
        link: function (scope, elt, attrs) {
            questionDirectiveBase.link(scope, undefined, bworkflowApi, languageTranslate);
            scope.init = function () {
                scope.answer = {};
                scope.answer.state = 'loading';
            };

            scope.getsummary = function (criteria) {
                scope.init();

                var parameters = { userid: scope.presented.userid, criteria: criteria };

                bworkflowApi.execute('CompanySummary', 'GetSummary', parameters)
                    .then(function (data) {

                        scope.answer.availableusertypes = data.availableusertypes;
                        scope.answer.company = data;
                        scope.answer.users = data.users;
                        scope.answer.state = "loaded";
                    }, function (tasks) {

                    });
            };

            scope.getusers = function (criteria) {
                scope.init();

                var parameters = { userid: scope.presented.userid };

                bworkflowApi.execute('CompanySummary', 'GetUsers', parameters)
                    .then(function (data) {

                        scope.answer.users = data.users;
                        scope.answer.state = "loaded";
                    }, function (tasks) {

                    });
            }

            scope.shownewuserdialog = function () {
                scope.answer.currentuser = {};

                scope.answer.currentuser.name = '';
                scope.answer.currentuser.username = '';
                scope.answer.currentuser.email = '';
                scope.answer.currentuser.password = '';
                scope.answer.currentuser.confirmpassword = '';
                scope.answer.currentuser.type = '';
                scope.answer.currentuser.exists = false;

                scope.answer.shownewuser = true;

                $timeout(function () { elt.find('.newuserdialog').modal('show'); });
            };

            scope.createnewuser = function (newuser) {
                var params = { userid: scope.presented.userid, user: newuser };

                bworkflowApi.execute('CompanySummary', 'NewUser', params)
                    .then(function (data) {
                        newuser.exists = false;

                        scope.answer.users.push(data);

                        $timeout(function () { elt.find('.newuserdialog').modal('hide'); });
                    }, function (tasks) {

                    });
            };

            scope.$on('help_add_steps', function (event, args) {
                args.push({ element: '#companysummary-address-' + scope.presented.Id, intro: 'You can view the address information currently stored in the system for the company', position: 'bottom' });
                args.push({ element: '#companysummary-add-' + scope.presented.Id, intro: 'You can add new employees to your company using the add button', position: 'left' });
                args.push({ element: '#companysummary-employees-' + scope.presented.Id, intro: 'Employees already in the company will be displayed in company employees table', position: 'top' });
            });

            scope.getsummary(scope.presented.criteria);
        }
    });
}]);

questionsModule.directive('questionCompanyConstructor', ['bworkflowApi', '$interval', '$sce', '$cookieStore', '$timeout', 'leafletData', '$filter', 'languageTranslate', function (bworkflowApi, $interval, $sce, $cookieStore, $timeout, leafletData, $filter, languageTranslate) {
    return $.extend({}, questionDirectiveBase, {
        templateUrl: 'question_company_constructor.html',
        link: function (scope, elt, attrs) {
            questionDirectiveBase.link(scope, undefined, bworkflowApi, languageTranslate);
            scope.init = function () {
                scope.state = 'loading';
            };

            scope.getinitdata = function () {
                scope.init();
                var parameters = { userid: scope.presented.userid, criteria: scope.presented.criteria };

                bworkflowApi.execute('CompanyConstructor', 'GetCriteria', parameters)
                    .then(function (data) {
                        scope.countrystates = data.countrystates;
                        scope.availableusertypes = data.availableusertypes;
                    }, function (tasks) {

                    });
            };

            scope.shownewcompanydialog = function () {
                scope.currentcompany = {};
                scope.currentcompany.name = '';
                scope.currentcompany.street = '';
                scope.currentcompany.town = '';
                scope.currentcompany.state = '';
                scope.currentcompany.postcode = '';

                scope.currentcompany.user = { name: '', email: '', type: '', password: '', confirmpassword: '' };

                scope.statemachine = 1;
                scope.shownewcompany = true;

                $timeout(function () {
                    elt.find('.newcompanydialog').modal('show');
                });

            };

            scope.cangeocode = function (company) {
                if (scope.presented.geocodeurl == null || scope.presented.geocodeurl == '') {
                    return false;
                }

                if (company.address.street == '' || company.address.town == '' || company.address.state == '' || company.address.postcode == '') {
                    return false;
                }
            }

            scope.showexistingcompanies = function (pos) {
                var parameters = { userid: scope.presented.userid, location: pos };

                bworkflowApi.execute('CompanyConstructor', 'GetCompanies', parameters)
                .then(function (data) {
                    markers = [];

                    var currentMarker = scope.map.markers[0];

                    angular.forEach(data.companies, function (value, key) {
                        if (value.location.lat == null || value.location.lng == null) {
                            return;
                        }

                        var marker = { lat: value.location.lat, lng: value.location.lng, focus: false, draggable: false, message: value.name, icon: { iconUrl: bworkflowApi.getfullurl('~/content/images/marker-icon-green.png'), shadowUrl: bworkflowApi.getfullurl('~/content/images/marker-shadow.png') } }

                        scope.map.markers.splice(0, 0, marker);
                    });

                }, function (tasks) {

                });
            }

            scope.geocode = function (company, isAlternateSearch) {
                if (scope.cangeocode(company) == false) {
                    return;
                }

                var state = $filter('filter')(scope.countrystates, { id: company.address.state }, true)[0];

                var domap = function (pos) {
                    var map = scope.map;

                    map.center.lat = parseFloat(pos.lat);
                    map.center.lng = parseFloat(pos.lon);
                    map.center.zoom = 16;

                    var marker = { lat: map.center.lat, lng: map.center.lng, focus: true, draggable: true, message: company.name };
                    company.location = marker;
                    map.markers.length = 0;
                    map.markers.push(marker);

                    scope.map = map;

                    scope.showexistingcompanies(pos);
                }

                var alternateSearch = false;
                if (angular.isDefined(isAlternateSearch) == true) {
                    alternateSearch = isAlternateSearch;
                }

                if (alternateSearch == false) {
                    scope.locationoperation = 'primarysearch';
                }

                bworkflowApi.geolocate(alternateSearch == true ? "" : company.address.street, company.address.town, company.address.postcode, state.name, 'australia', scope.presented.geocodeurl)
                    .then(function (data) {
                        if (data.length == 0) {
                            if (alternateSearch == false) {
                                // lets try with an alternative search by cutting of the street info
                                scope.locationoperation = 'alternatesearch';

                                $timeout(function () {
                                    scope.geocode(company, true);
                                });
                            }
                            else {
                                scope.locationoperation = 'alternatefailed';
                            }

                            return;
                        }

                        if (alternateSearch == false) {
                            scope.locationoperation = null;
                        }
                        else {
                            scope.locationoperation = 'alternatefound';
                        }

                        domap(data[0]);
                    }, function (data) {
                    });
            }

            scope.forwards = function () {
                var newState = scope.statemachine + 1;

                if (newState == 2) {

                    scope.geocode(scope.currentcompany);

                    // make sure the map is sized correctly
                    $timeout(function () {
                        leafletData.getMap().then(function (map) {
                            map.invalidateSize();
                        });
                    });
                }
                else if (newState == 3 && scope.presented.criteria.createdefaultuser == false) {
                    newState = 4;
                }

                scope.statemachine = newState;
            }

            scope.backwards = function () {
                var newState = scope.statemachine - 1;

                if (newState == 3 && scope.presented.criteria.createdefaultuser == false) {
                    newState = 2;
                }

                scope.statemachine = newState;
            }

            scope.createnewcompany = function (company) {
                var parameters = { userid: scope.presented.userid, company: company, criteria: scope.presented.criteria };

                bworkflowApi.execute('CompanyConstructor', 'Create', parameters)
                    .then(function (data) {
                        $timeout(function () {
                            elt.find('.newcompanydialog').modal('hide');

                            // let the player no so that it can let anything else know with a broadcast down
                            data.change = 'new';
                            scope.$emit('player_broadcast', { name: 'companies_changed', data: data });
                        });
                    }, function (tasks) {

                    });
            };

            scope.map = {}
            scope.map.center = {
                lat: 52.374004,
                lng: 4.890359,
                zoom: 1
            };
            scope.map.markers = [];

            scope.getinitdata();
        }
    });
}]);


// a directive that models editing a company.
questionsModule.directive('editcompany', ['bworkflowApi', 'cachedExecutionHandlers', '$sce', '$timeout', 'leafletData', '$filter', '$interval', function (bworkflowApi, cachedExecutionHandlers, $sce, $timeout, leafletData, $filter, $interval) {
    return {
        templateUrl: 'tasks/editcompany.html',
        link: function (scope, element, attrs, ngModel) {
            scope.map = {}

            scope.map.center = {
                lat: 52.374004,
                lng: 4.890359,
                zoom: 1
            };

            scope.map.markers = [];

            scope.map.show = false;

            scope.positionMap = function (location) {
                // the address lookup uses lon, but everything else uses lng, so jump a small hurdle to cope with that
                if (angular.isDefined(location.lng) == false) {
                    location.lng = location.lon;
                }

                var map = scope.map;

                map.center.lat = parseFloat(location.lat);
                map.center.lng = parseFloat(location.lng);
                map.center.zoom = 16;

                if (map.companymarker == null) {
                    map.companymarker = { lat: map.center.lat, lng: map.center.lng, focus: true, draggable: true, message: scope.company.name };

                    map.markers.push(map.companymarker);
                }

                map.companymarker.lat = map.center.lat;
                map.companymarker.lng = map.center.lng;

                scope.map.show = true;
            };

            var pending;
            scope.startpendinggeocode = function () {
                scope.stoppendinggeocode();

                pending = $interval(function () {
                    scope.geocode();
                }, 1000); // we'll wait a second for something to cancel the pending lookup
            };

            scope.stoppendinggeocode = function () {
                if (angular.isDefined(pending)) {
                    $interval.cancel(pending);
                    pending = undefined;
                }
            };

            scope.geocode = function (isAlternateSearch) {
                if (scope.company.address.street == null || scope.company.address.town == null || scope.company.address.stateid == null || scope.company.address.postcode == null) {
                    scope.map.show = false;
                    return;
                }

                var alternateSearch = false;
                if (angular.isDefined(isAlternateSearch) == true) {
                    alternateSearch = isAlternateSearch;
                }

                if (alternateSearch == false && scope.company.address.street == scope.lastaddress.street && scope.company.address.town == scope.lastaddress.town && scope.company.address.stateid == scope.lastaddress.stateid && scope.company.address.postcode == scope.lastaddress.postcode) {
                    // nothing has changed
                    return;
                }

                var company = scope.company;

                var state = $filter('filter')(scope.countrystates, { id: company.address.stateid }, true)[0];

                if (alternateSearch == false) {
                    scope.locationoperation = 'primarysearch';
                }

                scope.lastaddress = angular.copy(scope.company.address);

                bworkflowApi.geolocate(alternateSearch == true ? "" : company.address.street, company.address.town, company.address.postcode, state.name, 'australia', scope.presented.geocodeurl)
                    .then(function (data) {
                        if (data.length == 0) {
                            if (alternateSearch == false) {
                                // lets try with an alternative search by cutting of the street info
                                scope.locationoperation = 'alternatesearch';

                                $timeout(function () {
                                    scope.geocode(true);
                                });
                            }
                            else {
                                scope.locationoperation = 'alternatefailed';
                            }

                            return;
                        }


                        if (alternateSearch == false) {
                            scope.locationoperation = null;
                        }
                        else {
                            scope.locationoperation = 'alternatefound';
                        }

                        scope.positionMap(data[0]);
                    }, function (data) {
                    });
            }

            scope.setcompany = function (data) {
                scope.company = angular.copy(data.company);
                scope.lastaddress = angular.copy(data.company.address);
                scope.original = data.company;

                scope.locationoperation = null;
            }

            // we can be notified to show a different company with an event
            scope.$on('editcompany', function (evt, data) {
                scope.setcompany(data);
            });

            scope.$on('editcompany-shown', function (evt, data) {
                scope.map.show = false;

                $timeout(function () {
                    leafletData.getMap().then(function (map) {
                        map.invalidateSize();

                        if (scope.company.location.lat != null && scope.company.location.lng != null) {
                            scope.positionMap(scope.company.location);
                        }
                    });
                });
            });

            scope.$on('editcompany-save', function (evt, data) {
                // we copy our data across
                angular.copy(scope.company, scope.original);

                if (scope.map.companymarker != null) {
                    scope.original.location.lat = scope.map.companymarker.lat;
                    scope.original.location.lng = scope.map.companymarker.lng;
                }

            });

            if (ngModel != null) {
                scope.setcompany(ngModel);
            }

            cachedExecutionHandlers.getAllCountryStates().then(function (data) {
                scope.countrystates = data;
            });
        }
    };
}]);


questionsModule.directive('questionCompanyDirectory', ['bworkflowApi', '$interval', '$sce', '$cookieStore', '$timeout', 'leafletData', '$filter', 'languageTranslate', function (bworkflowApi, $interval, $sce, $cookieStore, $timeout, leafletData, $filter, languageTranslate) {
    return $.extend({}, questionDirectiveBase, {
        templateUrl: 'question_company_directory.html',
        link: function (scope, elt, attrs) {
            questionDirectiveBase.link(scope, undefined, bworkflowApi, languageTranslate);
            scope.init = function () {
                scope.searchtext = '';
                scope.page = 1;
                scope.count = 10;
                scope.state = 'loading';

                var parameters = { userid: scope.presented.userid };

                bworkflowApi.execute('CountryStates', 'GetAll', parameters)
                    .then(function (data) {
                        scope.countrystates = data.countrystates;
                    }, function (tasks) {

                    });
            };

            scope.initcompany = function (company) {
                if (company.location.lat != null) {
                    company.mapcenter = { lat: parseFloat(company.location.lat), lng: parseFloat(company.location.lng), zoom: 16 };
                    company.mapmarkers = [];
                    company.mapmarkers.push(company.mapcenter);
                }

                company.ratingInitialised = false;
            };

            scope.searchcompanies = function (text) {
                var parameters = { userid: scope.presented.userid, text: text, page: scope.page, count: scope.count };

                bworkflowApi.execute('CompanyDirectory', 'Search', parameters)
                    .then(function (data) {
                        angular.forEach(data.companies, function (value, key) {
                            scope.initcompany(value);

                            // event handler hook up to perform the saving of ratings if changed by the user
                            scope.$watch('companies[' + key + '].rating', function (newValue, oldValue) {
                                var company = scope.companies[key];

                                // this is getting raised when items are loaded in, we don't want to go back and save them as nothing
                                // has actually changed (old was null, new is what ever). Use the ratingInitialised property to detect
                                // the first hit and get out
                                if (company.ratingInitialised == false) {
                                    company.ratingInitialised = true;
                                    return;
                                }

                                var params = { userid: scope.presented.userid, companyid: company.id, rating: newValue };
                                bworkflowApi.execute('CompanyDirectory', 'SaveRating', params);
                            });
                        });

                        scope.companies = data.companies;
                    }, function (tasks) {

                    });
            };

            scope.showeditcompany = function (company) {
                scope.currentcompany = company;

                $timeout(function () {
                    scope.$broadcast('editcompany', { company: company, countrystates: scope.countrystates });

                    // we have to hook into the shown event and notify the edit company directive its visible as
                    // leaflet map has problems sizing its self unless its visible
                    var modal = elt.find('.editcompanydialog').modal('show');

                    modal.on('shown', function () {
                        scope.$broadcast('editcompany-shown', {});
                    });

                    modal.on('hidden', function () {
                        modal.off('shown');
                        modal.off('hidden');
                    });
                });
            };

            scope.savecompany = function () {
                scope.$broadcast('editcompany-save', {});

                // we need to let the above happen
                $timeout(function () {
                    var parameters = { userid: scope.presented.userid, company: scope.currentcompany };

                    bworkflowApi.execute('CompanyDirectory', 'Save', parameters)
                    .then(function (data) {

                        scope.initcompany(data.company);
                        angular.copy(data.company, scope.currentcompany);


                        elt.find('.editcompanydialog').modal('hide');

                        scope.iseditngcompany = false;
                    }, function (tasks) {

                    });
                });
            };

            scope.mapdefaults = { attributionControl: false, zoomControl: false }

            scope.init();
        }
    });
}]);

questionsModule.directive('questionJobUsersSignature', ['$timeout', 'bworkflowApi', 'languageTranslate', function ($timeout, bworkflowApi, languageTranslate) {
    return $.extend({}, questionDirectiveBase, {
        templateUrl: 'question_job_users_signature.html',
        link: function (scope, elt, attrs) {
            questionDirectiveBase.link(scope, undefined, bworkflowApi, languageTranslate);

            scope.signaturedimensions = { width: 200, height: 100 };

            scope.answer = scope.presented.Answer;

            scope.$on('signatureReady', function (ev, elt, user) {
                var hidden = elt.find("input");
                var canvas = elt.find("canvas");
                var sigWrapper = elt.find(".sigWrapper");

                elt.attr("style", "width:" + scope.signaturedimensions.width + "px");
                sigWrapper.attr("style", "height:" + scope.signaturedimensions.height + "px");
                canvas.attr("width", scope.signaturedimensions.width);
                canvas.attr("height", scope.signaturedimensions.height);

                user.signaturePad = elt.signaturePad({ output: hidden, validateFields: false, drawOnly: true, lineTop: scope.signaturedimensions.height - 20 });

                //if (scope.answer.Signature != null && scope.answer.Signature != "") {
                //    scope.signaturePad.regenerate(scope.answer.Signature);
                //}

                ev.stopPropagation();
            });

            scope.clearSignature = function (user) {
                user.signaturePad.clearCanvas();
            }

            scope.$on('populateAnswer', function (ev) {
                scope.answer.signatures = [];

                angular.forEach(scope.users, function (value, key) {
                    if (value.selected == true) {
                        sign = { userid: value.id, signature: value.signaturePad.getSignatureString() };
                        scope.answer.signatures.push(sign);
                    }
                });
            });

            scope.loadAvailableUsers = function () {
                var parameters = { userid: scope.presented.userid, workingdocumentid: scope.presented.workingdocumentid };

                bworkflowApi.execute('ProjectJobUsersSignOnGlass', 'GetAvailableUsers', parameters)
                .then(function (data) {
                    scope.users = data.users;
                });
            };

            scope.loadAvailableUsers();
        }
    });
}]);

questionsModule.directive('questionDatasource', ['$timeout',
    'bworkflowApi',
    '$http',
    '$interval',
    '$sce',
    '$filter',
    '$interpolate',
    '$q',
    'RequestsErrorHandler', 'languageTranslate', function ($timeout, bworkflowApi, $http, $interval, $sce, $filter, $interpolate, $q, RequestsErrorHandler, languageTranslate) {
        return $.extend({}, questionDirectiveBase, {
            templateUrl: 'question_datasource.html',
            link: function (scope, elt, attrs) {
                scope.feedManagers = {};

                // Expose feedManagers on pageScope under name of this DataSource (eg DS.Tasks exposed as pageScope.DS.Tasks)
                questionDirectiveBase.link(scope, scope.feedManagers, bworkflowApi, languageTranslate);

                //some utility methods to support sensible default values for date parameters

                scope.adjustDate = function (date, amount, units) {
                    return moment(date).add(amount, units).toDate();
                };

                // current time
                scope.now = function () {
                    return new Date();
                };

                // current day
                scope.today = function () {
                    var now = scope.now();

                    return new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0);
                };

                scope.tomorrow = function () {
                    var today = scope.today();

                    return moment(today).add(1, 'days').toDate();
                };

                scope.yesterday = function () {
                    var today = scope.today();

                    return moment(today).subtract(1, 'days').toDate();
                };

                scope.startOfWeek = function () {
                    var today = scope.today();

                    return moment(today).startOf('week').toDate();
                };

                scope.endOfWeek = function () {
                    var today = scope.today();

                    return moment(today).endOf('week').toDate();
                };

                scope.startOfMonth = function () {
                    var today = scope.today();

                    return moment(today).startOf('month').toDate();
                };

                scope.endOfMonth = function () {
                    var today = scope.today();

                    return moment(today).endOf('month').toDate();
                };

                scope.constructFeeds = function () {
                    angular.forEach(scope.presented.Feeds, function (value, index) {
                        var feedManager = scope.constructFeed(value);

                        scope.feedManagers[feedManager.template.name] = feedManager;

                        bworkflowApi.registerDataFeed(feedManager);
                    });

                    // first hit
                    scope.checkUpdatesRequired();

                    // now start the periodic refresh
                    scope.startRefreshes();
                };

                scope.constructFeed = function (feed) {
                    return bworkflowApi.createDataFeed(feed, scope);
                };

                scope.checkUpdatesRequired = function () {
                    if (angular.isDefined(scope.feedManagers) == false) {
                        return;
                    }

                    angular.forEach(scope.feedManagers, function (value, key) {
                        if (value.countdown == null || value.countdown == "" || value.countdown == -1) {
                            return;
                        }

                        if (value.isAjaxing == true) {
                            return; // its already doing its thing
                        }

                        value.countdown = value.countdown - 1;

                        if (value.countdown == 0) {
                            value.getData(false, { refresh: true });
                        }
                    });
                };

                scope.startRefreshes = function () {
                    if (angular.isDefined(scope.stop)) return;

                    scope.stop = $interval(function () {
                        scope.checkUpdatesRequired();
                    }, 1000);
                };

                scope.stopRefreshes = function () {
                    if (angular.isDefined(scope.stop)) {
                        $interval.cancel(scope.stop);
                        scope.stop = undefined;
                    }
                };

                scope.$on('$destroy', function () {
                    // Make sure that the interval is destroyed too
                    scope.stopRefreshes();
                });

                scope.constructFeeds();
            }
        });
    }]);

questionsModule.directive('questionMergedhierarchicaldatasource', ['$timeout',
    'bworkflowApi', '$filter', 'languageTranslate', function ($timeout, bworkflowApi, $filter, languageTranslate) {
        return $.extend({}, questionDirectiveBase, {
            templateUrl: 'question_mergedhierarchicaldatasource.html',
            link: function (scope, elt, attrs) {
                questionDirectiveBase.link(scope, undefined, bworkflowApi, languageTranslate);
                if (scope.presented.Name == null || scope.presented.Name == '') {
                    alert('A merged hierarchical datasource requires a name for it to function');
                    return;
                }

                // we attempt to build up the following hierarchy structure
                //
                // Task
                //   Checklist
                //     ChecklistResult
                //     ChecklistMediaStream
                //   TaskWorkLog
                //
                // or part there of (so for example if ChecklistMediaStream isn't supplied we
                // build up the rest of the hierarchy ommitting the media stream. 
                // If a high level item such as the task isn't available we'll build around the checklist.
                // If we have a Task feed and no checklist feed, but a checklistresult, then the checklist result
                // will feature at the level of the checklist.

                // the structure we create is hard coded based on the feeds we have.
                // we currently only support a single feed of each type. We keep direct
                // pointers to each of these to keep managing the hierarchy simple
                scope.relationships = [];

                scope.feed = {
                    notifier: { refreshes: 0, id: scope.presented.Name },
                    data: [],
                };

                scope.waitingCount = 0;

                // builds a map of rows in the data of the feed mapped to the relationship
                // by parentids
                scope.buildRelationshipHash = function (relationship) {
                    if (relationship.isTop == true) {
                        return;
                    }

                    relationship.mappedData = {};

                    angular.forEach(relationship.feed.data, function (item) {
                        var key = item.alldata[relationship.foreignKey.field];

                        if (angular.isDefined(relationship.mappedData[key]) == false) {
                            relationship.mappedData[key] = [];
                        }

                        relationship.mappedData[key].push(item);
                    });
                };

                scope.buildRelationshipHashes = function () {
                    angular.forEach(scope.relationships, function (relationship) {
                        scope.buildRelationshipHash(relationship);
                    });
                };

                scope.traverseRelationshipThread = function (current) {
                    var children = $filter('filter')(scope.relationships, { parent: current.id }, true);

                    if (children.length == 0) {
                        return;
                    }

                    var data = current.feed.data;

                    for (var i = 0; i < data.length; i++) {
                        var item = data[i];

                        if (angular.isDefined(item.children) == false) {
                            item.children = {};
                        }

                        for (var j = 0; j < children.length; j++) {
                            var child = children[j];

                            item.children[child.feed.template.name] = {
                                template: child.feed.template,
                                data: child.mappedData[item.alldata[child.foreignKey.mapsToParentField]]
                            };

                            scope.traverseRelationshipThread(child);
                        }
                    }
                };

                scope.buildHierarchy = function (feed) {
                    // we only rebuild the hierarchy if the top level feed 
                    if (scope.waitingCount > 0) {
                        // we aren't doing all the work of putting the hierarchy together
                        // until we have all the data we are expecting. We essentially use reference
                        // counting to know this
                        return;
                    }

                    var topRelationship = null;
                    angular.forEach(scope.relationships, function (relation) {
                        if (relation.canBeTop == true) {
                            if (topRelationship == null || (topRelationship.parent != null && relation.parent == null)) {
                                topRelationship = relation;
                            }
                        }
                    });

                    if (topRelationship == null) {
                        console.warn(scope.presented.Name + ' can not build a hierarchy as there is no top relationship feed supplied');
                        return;
                    }

                    topRelationship.isTop = true;

                    scope.buildRelationshipHashes();

                    scope.traverseRelationshipThread(topRelationship);
                };

                if (scope.presented.datasource) {
                    var split = scope.presented.datasource.split(',');

                    scope.waitingCount = split.length;

                    // we store each of the feeds in a varibale based on their name
                    angular.forEach(split, function (source) {
                        var promise = bworkflowApi.getDataFeed(source);

                        if (promise != null) {
                            promise.then(function (feed) {
                                scope.waitingCount = scope.waitingCount - 1;

                                switch (feed.template.feed) {
                                    case "Tasks":
                                        scope.relationships.push({ id: 'task', canBeTop: true, feed: feed, parent: null, foreignKey: null })
                                        break;
                                    case "TaskWorklogs":
                                        scope.relationships.push({ id: 'worklog', canBeTop: false, feed: feed, parent: 'task', foreignKey: { field: 'TaskId', mapsToParentField: 'Id' } });
                                        break;
                                    case "Checklists":
                                        scope.relationships.push({ id: 'checklist', canBeTop: true, feed: feed, parent: 'task', foreignKey: { field: 'TaskId', mapsToParentField: 'Id' } });
                                        break;
                                    case "ChecklistResults":
                                        scope.relationships.push({ id: 'checklistresults', canBeTop: false, feed: feed, parent: 'checklist', foreignKey: { field: 'WorkingDocumentId', mapsToParentField: 'WorkingDocumentId' } })
                                        break;
                                    case "ChecklistMediaStream":
                                        scope.relationships.push({ id: 'checklistmediastream', canBeTop: false, feed: feed, parent: 'checklist', foreignKey: { field: 'WorkingDocumentId', mapsToParentField: 'WorkingDocumentId' } });
                                        break;
                                }

                                feed.beforeLoadHooks.push(function (f) {
                                    scope.waitingCount = scope.waitingCount + 1;
                                });

                                feed.afterLoadHooks.push(function (f) {
                                    scope.waitingCount = scope.waitingCount - 1;

                                    if (scope.waitingCount < 0) {
                                        scope.waitingCount = 0;
                                    }

                                    scope.buildHierarchy();
                                });

                                // try to build the hierarchy with the feeds we now have
                                scope.buildHierarchy();
                            });
                        }
                    });
                }
            }
        });
    }]);

questionsModule.factory('liveBeaconSvc', ['$injector', function ($injector) {
    if (window && window.cordova) {
        return angular.extend({
            isSupported: true
        }, $injector.get('beaconSvc'));
    } else {
        return {
            isSupported: false
        }
    }
}]);

questionsModule.directive('questionMapping', ['$injector', '$timeout', '$interpolate', '$parse',
    'bworkflowApi', '$filter', 'leafletData', 'liveBeaconSvc', '$geolocation', 'VmAdminClasses', 'L-destroyer', 'directiveApi', 'geolocation-registry', 'languageTranslate',
    function ($injector, $timeout, $interpolate, $parse, bworkflowApi, $filter, leafletData, liveBeaconSvc, $geolocation, VmAdmin, Ldestroyer, directiveApi, geolocationRegistry, languageTranslate) {
        return $.extend({}, questionDirectiveBase, {
            templateUrl: 'question_mapping.html',
            link: function (scope, elt, attrs) {
                questionDirectiveBase.link(scope, undefined, bworkflowApi, languageTranslate);

                scope.beaconSvc = null;

                scope.LocationProviders = geolocationRegistry.byindex;
                scope.LocationProvidersByName = geolocationRegistry.byname;

                $geolocation.getCurrentPosition({ enableHighAccuracy: true }).then(function (position) {
                    scope.GPS = $geolocation;
                    if (scope.GPS) {
                        geolocationRegistry.add('GPS', 0, 'GPS', $geolocation.position);

                        // Default BestEstimate to GPS (this is fine for Desktop, VMPlayer will override this)
                        var be = geolocationRegistry.add('BestEstimate', 6, 'Best Estimate', $geolocation.position);
                        be.isBestEstimate = true;
                    }
                });

                var _followPositionWatch = null;
                scope.followPosition = function (source) {
                    scope.unfollowPosition();

                    source = source || "BestEstimate";
                    _followPositionWatch = scope.$watch('LocationProvidersByName.' + source + '.position.coords.floorPlanId', function (newValue) {
                        if (angular.isDefined(newValue) && angular.isDefined(scope.floorplans)) {
                            var newFloorPlan = scope.floorplans[newValue];
                            if (newFloorPlan) {
                                newFloorPlan.select(true);
                            }
                        }
                    });
                }

                scope.unfollowPosition = function () {
                    if (_followPositionWatch) {
                        _followPositionWatch();
                        _followPositionWatch = null;
                    }
                }

                scope.followPosition();

                if (window.razordata.environment == 'mobile') {
                    scope.beaconSvc = $injector.get('beaconSvc');
                    scope.LiveBeacons = scope.beaconSvc.getBeacons();
                }

                var Ldestroy = Ldestroyer(scope);

                function distanceKms(lat1, lon1, lat2, lon2) {
                    var p = 0.017453292519943295;    // Math.PI / 180
                    var c = Math.cos;
                    var a = 0.5 - c((lat2 - lat1) * p) / 2 +
                            c(lat1 * p) * c(lat2 * p) *
                            (1 - c((lon2 - lon1) * p)) / 2;

                    return 12742 * Math.asin(Math.sqrt(a)); // 2 * R; R = 6371 km
                }

                scope.floorplanApi = directiveApi();

                function getMap(fn) {
                    scope.floorplanApi.getMap().then(fn);
                }
                scope.getMap = getMap;

                function Building(id, name) {
                    var building = this;

                    this.id = id;
                    this.name = name;
                    this.floorplans = [];
                    this.selectedFloorplan = null;

                    this.contextmenu = [{
                        text: '<h5>' + name + '</h5>',
                        disabled: true
                    }];

                    this.Floorplan = function Floorplan(dto) {
                        var floorplan = this;
                        angular.extend(this, dto);
                        this.id = dto.Id;
                        this.visible = false;
                        this.building = building;

                        building.contextmenu.push({
                            text: floorplan.Floor.Name,
                            iconCls: function () {
                                return building.selectedFloorplan === floorplan ? 'icon-ok' : 'icon-blank';
                            },
                            callback: function () {
                                floorplan.select(true);         // User selection = Force select floor plan
                            }
                        });

                        var _Limage = null;
                        this.Limage = function () {
                            if (_Limage == null) {
                                _Limage = L.imageOverlay.rotated(
                                    floorplan.ImageURL,
                                    L.latLng(floorplan.TopLeftLatitude, floorplan.TopLeftLongitude),
                                    L.latLng(floorplan.TopRightLatitude, floorplan.TopRightLongitude),
                                    L.latLng(floorplan.BottomLeftLatitude, floorplan.BottomLeftLongitude),
                                    {
                                        opacity: 0.8,
                                        interactive: true,
                                        contextmenu: true,
                                        contextmenuItems: building.contextmenu
                                    }
                                );
                            }
                            return _Limage;
                        }

                        building.floorplans.push(floorplan);
                        building.defaultFloorplan = floorplan.Floor.Level === 0 ? floorplan : building.defaultFloorplan || floorplan;

                        this.select = function (forceSelect) {
                            // If Building has a ForceSelected floor plan then ignore this request 
                            if (!forceSelect && building.forceSelectedFloorplan) {
                                return;
                            }

                            if (building.selectedFloorplan !== floorplan) {
                                if (building.selectedFloorplan != null) {
                                    building.selectedFloorplan.Limage().remove();
                                    building.selectedFloorplan.visible = false;
                                }

                                building.selectedFloorplan = floorplan;
                                floorplan.visible = true;

                                getMap(function (map) {
                                    floorplan.Limage().addTo(map);
                                });

                                Object.keys(scope.dataWatchs).forEach(function (key) {
                                    scope.dataWatchs[key].refresh();
                                });

                                if (forceSelect) {
                                    building.forceSelectedFloorplan = floorplan;
                                }
                            }
                        }
                    }
                }

                scope.floorplanFeed = bworkflowApi.createDataFeed({
                    name: 'floorplans',
                    feed: 'FloorPlans',
                    filter: '',                 // Fetch and cache all floorplans
                    expandfields: 'Floor',
                    orderbyfields: 'BuildingId,Floor/Level',
                    idfields: ['Id'],
                    usepaging: false,
                    parameterdefinitions: []
                }, scope);

                scope.floorplanFeed.addAfterLoadHook(function (feed) {
                    scope.buildings = {};
                    scope.floorplans = {};
                    feed.data.forEach(function (floorplanDto) {
                        var building = scope.buildings[floorplanDto.alldata.BuildingId];
                        if (angular.isUndefined(building)) {
                            building = new Building(floorplanDto.alldata.BuildingId, floorplanDto.alldata.BuildingName);
                            scope.buildings[building.id] = building;
                        }
                        var floorplan = new building.Floorplan(floorplanDto.alldata);
                        building.floorplans.push(floorplan);
                        scope.floorplans[floorplan.id] = floorplan;
                    });

                    Object.keys(scope.buildings).forEach(function (key) {
                        var building = scope.buildings[key];
                        building.defaultFloorplan.select();
                    })
                })
                scope.floorplanFeed.getData(true);

                scope.options = {};
                scope.addOptions = function (options) {
                    Object.keys(options).forEach(function (key) {
                        var option = options[key];
                        if ('icon' in option) {
                            if (!(option.icon instanceof L.icon)) {
                                // It defines an Icon, create it ..
                                var icon = null;
                                if ('html' in option.icon) {
                                    icon = L.divIcon(option.icon);
                                } else {
                                    icon = L.icon(option.icon);
                                }
                                option.icon = icon;
                            }
                        }
                        scope.options[key] = option;
                    })
                }

                var fnGetOption = function (value) {
                    if (angular.isString(value)) {
                        return value in scope.options ? scope.options[value] : angular.undefined;
                    } else {
                        return value;
                    }
                }

                var fnMakeOptions = function (options, dataScope) {
                    // option is a Leaflet option definition suitable to the Layer type being rendered (Marker, Polyline, Polygon etc)
                    //   options: { option definition }
                    //   options: 'option name' 
                    //   options: [
                    //     ['angular boolean expression 0', ['option name when true' | { option definition when true }, 'option name when false' | { option definition when false} | undefined]]
                    //     ['angular integer expression 1', ['option name 0' | { option definition }, 'option name 1' | { option definition }...]]
                    //     ['angular string expression 2', { 'string0': 'option name 0' | { option definition }, 'string1': 'option name 1' | { option definition }...}]
                    //     ['angular expression which evaluates to either option name or option definition']
                    //   ]
                    //   options: function(v, mergeOption)

                    if (angular.isDefined(options)) {
                        //   options: function(v, mergeOption)
                        if (angular.isFunction(options)) {
                            return function (v, mergeOption) {
                                return fnGetOption(options(v, mergeOption));
                            }
                        }
                        else if (angular.isString(options)) {
                            //   options: 'option name' 
                            return function (v, mergeOption) {
                                return options in scope.options ? scope.options[options] : null;
                            }
                        }
                        else if (angular.isArray(options)) {
                            //   options: [
                            //     ['angular boolean expression 0', ['option name when true' | { option definition when true }, 'option name when false' | { option definition when false} | undefined]]
                            //     ['angular integer expression 1', ['option name 0' | { option definition }, 'option name 1' | { option definition }...]]
                            //     ['angular string expression 2', { 'string0': 'option name 0' | { option definition }, 'string1': 'option name 1' | { option definition }...}]
                            //     ['angular expression which evaluates to either option name or option definition']
                            //   ]
                            var tests = [];
                            options.forEach(function (match) {
                                var fnExpr = $parse(match[0]);
                                var fnValue = fnGetOption;     // Handles ['angular expression which evaluates to either option name or option definition']
                                var fnContinueMerge = function () {
                                    return false;
                                }
                                if (match.length >= 2) {
                                    var values = match[1];
                                    //     ['angular integer expression 0', ['option name 0' | { option definition }, 'option name 1' | { option definition }...]
                                    //     ['angular string expression 1', { 'string0': 'option name 0' | { option definition }, 'string1': 'option name 1' | { option definition }...}]
                                    if (values) {
                                        fnValue = function (value) {
                                            if (value == true) {
                                                return fnGetOption(values[0]);
                                            } else if (value == false) {
                                                return fnGetOption(values[1]);
                                            }
                                            return fnGetOption(values[value]);
                                        }
                                    }

                                    if (match.length >= 3) {
                                        if (angular.isString(match[2])) {
                                            fnContinueMerge = $parse(match[2]);
                                        } else {
                                            fnContinueMerge = function () {
                                                return match[2] ? true : false;
                                            }
                                        }
                                    }
                                }
                                tests.push({ fnExpr: fnExpr, fnValue: fnValue, fnContinueMerge: fnContinueMerge });
                            })
                            return function (v, mergeOption) {
                                var option = null;
                                tests.find(function (test) {
                                    var testValue = test.fnExpr(v, dataScope);
                                    if (angular.isDefined(testValue)) {
                                        option = test.fnValue(testValue);

                                        // extending the mergeOption allows us to define a base set of options and then
                                        // have later option selectors merge in extra definitions
                                        if (angular.isDefined(option)) {
                                            angular.extend(mergeOption, option);
                                            return !test.fnContinueMerge(v, dataScope);
                                        }
                                    }
                                    return false;
                                });

                                return mergeOption;
                            }
                        } else if (angular.isObject(options)) {
                            //   options: { Leaflet options }
                            return function (v, mergeOption) {
                                return options;
                            }
                        }
                    }
                    // fall through
                    return function (v, mergeOption) {
                        return mergeOption;
                    }
                };

                scope.dataWatchs = {};

                var _nextId = 0;
                function DataWatch(data) {
                    var feed = null;
                    // For feed objects, we only want their data[] ..
                    if (data && angular.isFunction(data.addAfterLoadHook)) {
                        feed = data;
                        data = feed.data;
                    }

                    var me = this;
                    this.floorplanFilter = new FloorplanFilter();

                    var id = 'dataWatch' + _nextId++;
                    scope.dataWatchs[id] = this;

                    this.data = data;
                    this.Lobjects = [];
                    this.latLngBounds = null;
                    this.fitBoundsCount = 1;            // Fit bounds 1st time only
                    this.visible = false;

                    this.init = function (fnLoad, map) {
                        me.fnLoad = fnLoad;
                        me.map = map;
                    }

                    this.addLobject = function (Lobject, floorplanId) {
                        me.Lobjects.push(Lobject);
                        me.floorplanFilter.include(Lobject, floorplanId);
                    }

                    this.refresh = function () {
                        if (this.visible) {
                            me.Lobjects.forEach(function (Lobj) {
                                var floorplan = Lobj.floorplan;
                                if (floorplan) {
                                    if (floorplan.visible) {
                                        Lobj.addTo(me.map);
                                    } else {
                                        Lobj.remove();
                                    }
                                } else {
                                    // No associated floor
                                    Lobj.addTo(me.map);
                                }                                
                            })
                        } else {
                            me.Lobjects.forEach(function (Lobj) {
                                Lobj.remove();
                            });
                        }
                    }

                    this.show = function () {
                        if (!me.visible) {
                            me.visible = true;

                            // For a feed, we watch afterLoadHook which is better performance than angular watch deep
                            if (feed) {
                                me.unwatch = feed.addAfterLoadHook(function (_, context) {
                                    // If feed is Auto Refreshed then do not create new FloorplanFilter (keeps user chosen floors shown)
                                    me.floorplanFilter = new FloorplanFilter(context && !context.refresh);
                                    me.fnLoad(me);
                                })
                            } else {
                                me.unwatch = scope.$watch('dataWatchs.' + id + '.data', function (newValue, oldValue) {
                                    me.floorplanFilter = new FloorplanFilter();
                                    me.fnLoad(me);
                                }, true);
                            }

                            me.refresh();
                        }
                        return me;
                    }

                    this.hide = function () {
                        if (me.visible) {
                            me.visible = false;
                            if (me.unwatch) {
                                me.unwatch();
                                me.unwatch = null;
                            }
                            me.Lobjects.forEach(function (Lobj) {
                                Lobj.remove();
                            });
                        }
                        return me;
                    }

                    this.destroy = function () {
                        if (me.unwatch) {
                            me.unwatch();
                            me.unwatch = null;
                        }
                        Ldestroy(me.Lobjects);

                        delete scope.dataWatchs[id];
                    }
                }

                function DataScope(pageScope, pre) {
                    var me = this;
                    this.scope = pageScope.$new(false);

                    pre = angular.isUndefined(pre) ? '' : pre;
                    var $index = '$' + pre + 'index';
                    var $first = '$' + pre + 'first';
                    var $last = '$' + pre + 'last';
                    var $middle = '$' + pre + 'middle';
                    var $odd = '$' + pre + 'odd';
                    var $even = '$' + pre + 'even';

                    var scope = this.scope;

                    this.reset = function (last) {
                        me.index = -1;
                        me.last = last;
                    }

                    this.next = function () {
                        me.index = me.index + 1;
                        scope[$index] = me.index;
                        scope[$first] = me.index === 0;
                        scope[$last] = me.index === me.last;
                        scope[$middle] = !(scope[$first] || scope[$last]);
                        scope[$odd] = !(scope[$even] = (me.index & 1) === 0);
                    }
                }

                // 
                function fnAddOnEvents(fnLobject, options) {
                    if (angular.isObject(options.on)) {
                        return function (objArgs, objOptions) {
                            var Lobject = fnLobject(objArgs, objOptions);
                            Object.keys(options.on).forEach(function (key) {
                                var fn = options.on[key];
                                if (angular.isFunction(fn)) {
                                    Lobject.on(key, function (ev) {
                                        $timeout(fn, 0, true, Lobject.context, ev);
                                    });
                                }
                            })
                            return Lobject;
                        }
                    }
                    return fnLobject;
                }

                function FloorplanFilter(forceSelect) {
                    var me = this;
                    this.includedFloorplans = {};

                    this.include = function (Lobject, floorplanid) {
                        if (floorplanid) {
                            Lobject.floorplan = scope.floorplans[floorplanid];
                            if (Lobject.floorplan) {
                                if (!(Lobject.floorplan.building.id in me.includedFloorplans)) {
                                    me.includedFloorplans[Lobject.floorplan.building.id] = Lobject.floorplan;
                                }
                            }
                        }
                    }

                    this.select = function () {
                        // Select floorplan for each Building
                        Object.keys(me.includedFloorplans).forEach(function (buildingId) {
                            me.includedFloorplans[buildingId].select(forceSelect);
                        });
                    }
                }

                var fnMakeHtmlOptions = function (options) {
                    if (!options) {
                        return null;
                    }
                    if (angular.isFunction(options)) {
                        return function (v) {
                            var r = options(v);
                            if (angular.isString(r)) {
                                return {
                                    html: r,
                                    options: null
                                }
                            } else {
                                return r;
                            }
                        }
                    } else if (angular.isString(options)) {
                        var interp = $interpolate(options);
                        return function (v) {
                            return {
                                html: interp(v),
                                options: null
                            }
                        }
                    } else if ('html' in options) {
                        var interp = $interpolate(options.html);
                        return function (v) {
                            return {
                                html: interp(v),
                                options: options.options
                            }
                        }
                    }
                    return options; // not sure what it is, return it
                }

                var methods = ['polyline', 'polygon'];
                methods.forEach(function (polyxxx) {
                    var Polyxxx = polyxxx.capitalize();

                    scope['add' + Polyxxx] = function (data, options) {
                        var dataWatch = new DataWatch(data);
                        if (data) {
                            getMap(function (map) {
                                // options = {
                                //   latitude: 'alldata.BestEstimateLocation.Latitude' || function(),
                                //   longitude: 'alldata.BestEstimateLocation.Longitude' || function(),
                                //   filter: 'data | groupBy: alldata.AssetId' || function(),
                                //   options: see fnMakeOptions above
                                // }
                                options = options || {};

                                // dataScope allows us to access ngRepeater like $index, $first, $last, $middle, $even, $odd fields
                                var groupScope = new DataScope(scope.pageScope, 'group');
                                var dataScope = new DataScope(groupScope.scope);

                                var useScope = dataScope.scope;

                                var fnFilter = angular.isFunction(options.filter) ? options.filter : $parse(options.filter || 'data');
                                var fnLatitude = angular.isFunction(options.latitude) ? options.latitude : $parse(options.latitude || 'alldata.Latitude');
                                var fnLongitude = angular.isFunction(options.longitude) ? options.longitude : $parse(options.longitude || 'alldata.Longitude');
                                var fnFloorplanId = angular.isFunction(options.floorplanid) ? options.floorplanid : (options.floorplanid ? $parse(options.floorplanid) : null);

                                var Lpolyxxx = L[polyxxx];
                                var fnPolyxxx = function (pts, polyOptions) {
                                    var polyxxx = Lpolyxxx(pts, polyOptions);//.addTo(map);
                                    polyxxx.context = pts;
                                    return polyxxx;
                                }

                                var fnPopup = fnMakeHtmlOptions(options.popup);
                                var fnAddPopup = fnPolyxxx;
                                if (fnPopup) {
                                    fnAddPopup = function (pts, polyOptions) {
                                        var polyxxx = fnPolyxxx(pts, polyOptions);
                                        if (polyxxx) {
                                            var p = fnPopup(pts);
                                            if (p) {
                                                polyxxx.bindPopup(p.html, p.options);
                                            }
                                        }
                                        return polyxxx;
                                    }
                                }
                                var fnTooltip = fnMakeHtmlOptions(options.tooltip);
                                var fnAddToolTip = fnAddPopup;
                                if (fnTooltip) {
                                    fnAddToolTip = function (pts, markerOptions) {
                                        var polyxxx = fnAddPopup(pts, markerOptions);
                                        if (polyxxx) {
                                            var t = fnTooltip(pts);
                                            if (t) {
                                                polyxxx.bindTooltip(t.html, t.options);
                                            }
                                        }
                                        return polyxxx;
                                    }
                                }
                            var fnMakePolyxxx = fnAddOnEvents(fnAddToolTip, options);

                                var fnMakePolyOptions = fnMakeOptions(options.options, useScope);

                                var makePolyxxx = function (values) {
                                    if (values.length) {
                                        dataScope.reset(values.length);

                                        var pts = [];
                                        values.forEach(function (v) {
                                            dataScope.next();
                                            var latLng = L.latLng(fnLatitude(v, useScope), fnLongitude(v, useScope));
                                            if (latLng) {
                                                pts.push(latLng);
                                            }
                                        });

                                        if (pts.length) {
                                            dataWatch.latLngBounds = dataWatch.latLngBounds || L.latLngBounds(pts[0], pts[0]);
                                            pts.forEach(function (pt) {
                                                dataWatch.latLngBounds.extend(pt);
                                            });
                                        }

                                        var polyOptions = fnMakePolyOptions(values[0], {});
                                        var polyline = fnMakePolyxxx(pts, polyOptions);

                                        dataWatch.addLobject(polyline, fnFloorplanId && fnFloorplanId(values[0]));
                                    }
                                }

                                var loadPolyxxx = function (dataWatch) {
                                    // Remove old 
                                    Ldestroy([dataWatch.Lobjects]);
                                    dataWatch.latLngBounds = null;

                                    // Use $filter tree to filter, group, map, order data - note we pass pageScope which allows
                                    // it to define custom filters :)
                                    var result = fnFilter(dataWatch, scope.pageScope);

                                    // result will be either an Array or Object (if groupBy used)
                                    if (angular.isArray(result)) {
                                        makePolyxxx(result);
                                    } else {
                                        var keys = Object.keys(result);
                                        groupScope.reset(keys.length);
                                        keys.forEach(function (key, index) {
                                            groupScope.next();
                                            makePolyxxx(result[key]);
                                        });
                                    }

                                    if (angular.isDefined(options.fitBounds) && dataWatch.latLngBounds && dataWatch.fitBoundsCount) {
                                        dataWatch.fitBoundsCount--;
                                        map.fitBounds(dataWatch.latLngBounds, options.fitBounds);
                                    }

                                    dataWatch.refresh();
                                }

                                dataWatch.init(loadPolyxxx, map);

                                dataWatch.show();
                            });
                        }
                        return dataWatch;
                    }
                });

                var methods = [
                    { method: 'Marker', Lmethod: 'marker' },
                    { method: 'Circle', Lmethod: 'circle' },
                    { method: 'CircleMarker', Lmethod: 'circleMarker' }
                ];
                methods.forEach(function (method) {
                    scope['add' + method.method] = function (data, options) {
                        var dataWatch = new DataWatch(data);

                        if (data) {
                            getMap(function (map) {
                                // options = {
                                //   filter: 'alldata.LocationType == 4',
                                //   latitude: 'alldata.BestEstimateLocation.Latitude' || function(),
                                //   longitude: 'alldata.BestEstimateLocation.Longitude' || function(),
                                //   radius: 'alldata.BestEstimateLocation.Distance' || function(),
                                //   icon: 'icon name' || function(),
                                //   popup: '<b>{{ alldata.Name }}</b>' || function(),
                                //   tooltip: '<b>{{ alldata.Name }}</b>' || function(),
                                //   options: see fnMakeOptions above,
                                //   on: {
                                //     click: function(),
                                //     dblclick: function(),
                                //     mousedown: function(),
                                //     mouseover: function(),
                                //     mouseout: function(),
                                //     contextmenu: function()
                                //  },
                                //  fitBounds: [padding X, padding Y],
                                //  floorplanid: 'alldata.BestEstimateLocation.FacilityFloor.OnFloorPlanId',
                                //  beaconid: 'alldata.BeaconId'
                                // }
                                options = options || {};

                                // dataScope allows us to access ngRepeater like $index, $first, $last, $middle, $even, $odd fields
                                var dataScope = new DataScope(scope.pageScope);
                                var useScope = dataScope.scope;

                                var fnFilter = angular.isFunction(options.filter) ? options.filter : (options.filter ? $parse(options.filter) : null);
                                var fnLatitude = angular.isFunction(options.latitude) ? options.latitude : $parse(options.latitude || 'alldata.Latitude');
                                var fnLongitude = angular.isFunction(options.longitude) ? options.longitude : $parse(options.longitude || 'alldata.Longitude');
                                var fnRadius = angular.isFunction(options.radius) ? options.radius : (options.radius ? $parse(options.radius) : null);
                                var fnFloorplanId = angular.isFunction(options.floorplanid) ? options.floorplanid : (options.floorplanid ? $parse(options.floorplanid) : null);
                                var fnLiveBeaconId = scope.beaconSvc && (angular.isFunction(options.livebeaconid) ? options.livebeaconid : (options.livebeaconid ? $parse(options.livebeaconid) : null));

                                var Lmarker = L[method.Lmethod];
                                var fnMarker = function (v, markerOptions) {
                                    var latLng = L.latLng(fnLatitude(v, useScope), fnLongitude(v, useScope));
                                    if (!latLng) {
                                        return null;
                                    }
                                    var marker = Lmarker(latLng, markerOptions);
                                    marker.context = v;
                                    return marker;
                                }

                                var fnAddLiveBeacon = angular.identity;
                                if (fnLiveBeaconId) {
                                    fnAddLiveBeacon = function (v) {
                                        var beaconId = fnLiveBeaconId(v);
                                        if (angular.isDefined(beaconId)) {
                                            v.liveBeacon = scope.liveBeacons.find(function (b) {
                                                return b.beaconId === beaconId;
                                            });
                                        }
                                        return v;
                                    }
                                }

                                var fnPopup = fnMakeHtmlOptions(options.popup);
                                var fnAddPopup = fnMarker;
                                if (fnPopup) {
                                    fnAddPopup = function (v, markerOptions) {
                                        var marker = fnMarker(v, markerOptions);
                                        if (marker) {
                                            var p = fnPopup(v);
                                            if (p) {
                                                marker.bindPopup(p.html, p.options);
                                            }
                                        }
                                        return marker;
                                    }
                                }
                                var fnTooltip = fnMakeHtmlOptions(options.tooltip);
                                var fnAddToolTip = fnAddPopup;
                                if (fnTooltip) {
                                    fnAddToolTip = function (v, markerOptions) {
                                        var marker = fnAddPopup(v, markerOptions);
                                        if (marker) {
                                            var t = fnTooltip(v);
                                            if (t) {
                                                marker.bindTooltip(t.html, t.options);
                                            }
                                        }
                                        return marker;
                                    }
                                }
                                var fnMakeMarker = fnAddOnEvents(fnAddToolTip, options);

                                var fnMakeBasicOptions = fnMakeOptions(options.options, useScope);
                                var fnMakeMarkerOptions = fnMakeBasicOptions;
                                if (options.radius) {
                                    fnMakeMarkerOptions = function (v, mergeOption) {
                                        var markerOptions = fnMakeBasicOptions(v, mergeOption);
                                        if (markerOptions) {
                                            markerOptions.radius = Number(fnRadius(v, useScope));
                                            if (markerOptions.radius === 0 || isNaN(markerOptions.radius) || !isFinite(markerOptions.radius)) {
                                                return null;
                                            }
                                        }
                                        return markerOptions;
                                    }
                                }

                                var loadMarkers = function (dataWatch) {
                                    // Remove old markers
                                    Ldestroy([dataWatch.Lobjects]);
                                    dataWatch.latLngBounds = null;
                                    var floorplanid = null;

                                    // And add new ones
                                    dataScope.reset(dataWatch.data.length);
                                    dataWatch.data.forEach(function (v) {
                                        if (!fnFilter || fnFilter(v)) {
                                        
                                            dataScope.next();
                                            v = fnAddLiveBeacon(v);
                                            var markerOptions = fnMakeMarkerOptions(v, {});
                                            var marker = fnMakeMarker(v, markerOptions);
                                            if (marker) {
                                                dataWatch.latLngBounds = dataWatch.latLngBounds || L.latLngBounds(marker.getLatLng(), marker.getLatLng());
                                                dataWatch.latLngBounds.extend(marker.getLatLng());

                                                dataWatch.addLobject(marker, fnFloorplanId && fnFloorplanId(v));
                                            }

                                            // get a pointer to the datawatch object onto the odata one
                                            // so we can then have easy access to it's methods from angular templates
                                            // etc in a dashboard. This allows us to put popups etc through other dashboard
                                            // element.
                                            v.marker = function () {
                                                return marker;
                                            }
                                        }
                                    });

                                    if (angular.isDefined(options.fitBounds) && dataWatch.latLngBounds && dataWatch.fitBoundsCount) {
                                        dataWatch.fitBoundsCount--;
                                        map.fitBounds(dataWatch.latLngBounds, options.fitBounds);
                                    }

                                    dataWatch.refresh();
                                }

                                dataWatch.init(loadMarkers, map);
                                dataWatch.show();
                            });
                        }
                        return dataWatch;
                    }
                })
            }
        });
    }]);


questionsModule.directive('questionLeaveManagement', ['bworkflowApi', '$filter', 'languageTranslate', function (bworkflowApi, $filter, languageTranslate) {
    return $.extend({}, questionDirectiveBase, {
        templateUrl: 'question_leave_management.html',
        link: function (scope, elt, attrs) {
            questionDirectiveBase.link(scope, undefined, bworkflowApi, languageTranslate);
            scope.addingLeave = null;
            scope.showAdding = false;

            scope.leaveTypeTemplate = {
                name: 'leavetype',
                feed: 'LeaveTypes',
                orderbyfields: 'Name',
                idfields: ['Id']
            };

            scope.leaveTemplate = {
                name: 'leave',
                feed: "Leave",
                orderbyfields: 'StartDate desc, EndDate desc',
                idfields: ['Id'],
                itemsperpage: 10
            };

            scope.memberFeedTemplate = {
                name: 'members',
                feed: 'Members',
                filter: "substringof('[[searchmemberssearch]]', Name) and IsASite eq false",
                orderbyfields: 'Name',
                idfields: ['UserId'],
                itemsperpage: 5,
                datascope: {
                    ExcludeOutsideHierarchies: false,
                    IncludeCurrentUser: true,
                    ExcludeExpired: true
                },
                parameterdefinitions: [
                    {
                        internalname: 'searchmemberssearch'
                    }]
            };

            scope.replaceMemberFeedTemplate = {
                name: 'members',
                feed: 'Members',
                filter: "substringof('[[searchreplacedbysearch]]', Name) and IsASite eq false",
                orderbyfields: 'Name',
                idfields: ['UserId'],
                itemsperpage: 5,
                datascope: {
                    ExcludeOutsideHierarchies: false,
                    IncludeCurrentUser: true,
                    ExcludeExpired: true
                },
                parameterdefinitions: [
                    {
                        internalname: 'searchreplacedbysearch'
                    }]
            };

            scope.scheduleFeedTemplate = {
                name: 'rosterroles',
                feed: 'RosterRoles',
                filter: "UserId eq guid'[[ownerid]]'",
                orderbyfields: 'Roster,Role',
                idfields: ['RosterId', 'HierarchyBucketId'],
                usepaging: false,
                datascope: {
                    QueryScope: '1'
                },
                parameterdefinitions: [
                    {
                        internalname: 'ownerid'
                    }
                ]
            };

            scope.newLeave = { originalUser: null, replacingUser: null };
            scope.selectedRole = {};
            scope.selectedRow = null;
            scope.showReplacing = false;

            scope.leaveTypeFeed = bworkflowApi.createDataFeed(scope.leaveTypeTemplate, scope);
            scope.leaveFeed = bworkflowApi.createDataFeed(scope.leaveTemplate, scope);
            scope.memberFeed = bworkflowApi.createDataFeed(scope.memberFeedTemplate, scope);
            scope.replaceMemberFeed = bworkflowApi.createDataFeed(scope.replaceMemberFeedTemplate, scope);
            scope.scheduleFeed = bworkflowApi.createDataFeed(scope.scheduleFeedTemplate, scope);

            scope.scheduleRoles = null;

            scope.scheduleFeed.afterLoadHooks.push(function (feed) {
                // we are going to do a little bit of processing on this to make things easier
                // to present in the HTML template
                scope.scheduleRoles = [];
                var roleCache = {};

                angular.forEach(feed.data, function (item) {
                    var key = item.alldata.RoleId + item.alldata.Roster;
                    if (angular.isDefined(roleCache[key]) == true) {
                        return;
                    }

                    // we wrap these odata objects in one of our own so that
                    // we can help out the UI a bit regarding what the user selects
                    scope.scheduleRoles.push({
                        leaveType: scope.addingLeave.alldata.Id,
                        originalUser: scope.newLeave.originalUser,
                        role: item,
                        sd: null,
                        ed: null,
                        replacingUser: null,
                        notes: null,
                        valid: false,
                        saved: false
                    });

                    roleCache[key] = '';
                });
            });

            scope.leaveTypeFeed.getData(true);
            scope.leaveFeed.getData(true);

            scope.addLeave = function (type) {
                scope.addingLeave = type;
                scope.showAdding = true;
                scope.selectedRow = null;
            };

            scope.$watch('newLeave.originalUser', function (newValue, oldValue) {
                if (newValue == null) {
                    return;
                }

                scope.scheduleRoles = null;
                scope.scheduleFeed.parameters.ownerid = newValue.alldata.UserId;
                scope.scheduleFeed.getData(true);
            });

            scope.$watch('newLeave.replacingUser', function (newValue, oldValue) {
                if (scope.selectedRole == null) {
                    return;
                }

                scope.selectedRole.replacingUser = newValue;
            });

            scope.$watch('selectedRole', function (newValue, oldValue) {
                if (scope.selectedRole == null) {
                    return;
                }

                scope.selectedRole.valid = moment(scope.selectedRole.sd).isValid() && moment(scope.selectedRole.ed).isValid();
            }, true);

            scope.selectRole = function (role) {
                scope.selectedRole = role;
                scope.selectedRole.replacingUser = role.replacingUser;
                scope.showReplacing = true;

                if (role.replacingUser == null) {
                    scope.$broadcast('OdataTypeahead.clear', { name: 'searchreplacedby' });
                }
            };

            scope.clearSelections = function () {
                scope.selectedRole = {};
                scope.scheduleRoles = null;
                scope.addingLeave = null;
                scope.showAdding = false;
                scope.showReplacing = false;
            };

            scope.saveLeave = function () {
                var valid = scope.buildValidLeaves();

                angular.forEach(valid, function (r) {
                    r.originalUserId = r.originalUser.alldata.UserId;
                    if (r.replacingUser != null) {
                        r.replacingUserId = r.replacingUser.alldata.UserId;
                    }

                    r.startDate = moment(r.sd).format('DD-MM-YYYY');
                    r.endDate = moment(r.ed).format('DD-MM-YYYY');

                    r.roleId = r.role.alldata.RoleId;
                    r.hierarchyBucketId = r.role.alldata.HierarchyBucketId;
                });

                var parameters = {
                    userid: scope.presented.userid,
                    leave: valid
                };

                bworkflowApi.execute('LeaveManagement', 'AddLeave', parameters)
                .then(function (data) {
                    var issue = 0;

                    angular.forEach(data.leave, function (item, key) {

                        var v = $filter('filter')(valid, item.hierarchyBucketId, true);

                        if (item.success == false) {
                            if (v.length > 0) {
                                v[0].error = item.message;
                            }

                            issue++;
                        }
                        else {
                            v[0].saved = true;
                        }
                    });

                    if (issue == 0) {
                        scope.clearSelections();
                    }

                    scope.leaveFeed.getData(true);

                }, function (tasks) {

                });
            };

            scope.buildValidLeaves = function () {
                var valid = [];

                angular.forEach(scope.scheduleRoles, function (r) {
                    if (r.valid == false || r.saved == true) {
                        return;
                    }

                    valid.push(r);
                });

                return valid;
            };

            scope.selectRow = function (row) {
                scope.selectedRow = row;
            };

            scope.delete = function (leave) {
                if (confirm('Are you sure you want to delete this leave entry') == false) {
                    return;
                }

                var parameters = {
                    userid: scope.presented.userid,
                    id: leave.alldata.Id
                };

                bworkflowApi.execute('LeaveManagement', 'DeleteLeave', parameters)
                .then(function (data) {
                    scope.selectedRow = null;
                    scope.leaveFeed.getData(true);

                }, function (tasks) {

                });
            };
        }
    });
}]);


questionsModule.directive('questionTimesheetManagement', ['bworkflowApi', '$filter', '$timeout', 'languageTranslate', function (bworkflowApi, $filter, $timeout, languageTranslate) {
    return $.extend({}, questionDirectiveBase, {
        templateUrl: 'question_timesheet_management.html',
        link: function (scope, elt, attrs) {
            questionDirectiveBase.link(scope, undefined, bworkflowApi, languageTranslate);

            scope.selectedRoster = { alldata: { Id: -1 } };
            scope.selectedUser = null;
            scope.highlightDuration = 10;
            scope.startDate = null;
            scope.endDate = null;
            scope.cachedTimesheets = {};
            scope.selectedTimesheet = null;
            scope.selectedTimesheetItem = null;
            scope.selectedTimesheetItemData = null;
            scope.issaving = false;

            scope.timesheetItemTypesTemplate = {
                name: 'timesheetitemtypes',
                feed: 'TimesheetItemTypes',
                orderbyfields: 'Name',
                idfields: ['Id'],
                usepaging: false
            };

            scope.leaveTypeTemplate = {
                name: 'leavetype',
                feed: 'LeaveTypes',
                orderbyfields: 'Name',
                idfields: ['Id']
            };

            scope.rosterTemplate = {
                name: 'rosters',
                feed: 'Rosters',
                orderbyfields: 'Name',
                idfields: ['Id'],
                usepaging: false,
                selectfields: 'Id,Name'
            };

            scope.teamHierarchyTemplate = {
                name: 'teamhierarchy',
                feed: 'TeamHierarchys',
                filter: "RosterId eq [[RosterId]]",
                orderbyfields: 'LeftIndex',
                idfields: ['Id'],
                usepaging: false,
                selectfields: 'Id,LeftIndex,RoleName,RoleId,Name,UserId',
                parameterdefinitions: [
                {
                    internalname: 'RosterId'
                }]
            };

            scope.timesheetItemTypeFeed = bworkflowApi.createDataFeed(scope.timesheetItemTypesTemplate, scope);
            scope.leaveTypeFeed = bworkflowApi.createDataFeed(scope.leaveTypeTemplate, scope);
            scope.rosterFeed = bworkflowApi.createDataFeed(scope.rosterTemplate, scope);
            scope.teamHierarchyFeed = bworkflowApi.createDataFeed(scope.teamHierarchyTemplate, scope);

            // we hook into the load of the team hierarchy, to manage the cached timesheets
            scope.teamHierarchyFeed.afterLoadHooks.push(function (feed) {
                scope.cachedTimesheets = {};
                angular.forEach(feed.data, function (value, index) {
                    if (value.alldata.UserId == null) {
                        return;
                    }

                    value.__status = "Empty";

                    scope.cachedTimesheets[value.alldata.Id.toString()] = { __isLoaded: false, item: value, data: null };

                    scope.loadTimesheet(value);
                });
            });

            scope.rosterFeed.afterLoadHooks.push(function (feed) {
                scope.selectedRoster = feed.data[0];
            });

            scope.timesheetItemTypeFeed.getData(true);
            scope.leaveTypeFeed.getData(true);
            scope.rosterFeed.getData(true);

            scope.$watch('selectedRoster', function (newValue, oldValue) {
                if (newValue == null || newValue.alldata.Id == -1)
                {
                    return;
                }

                scope.teamHierarchyFeed.parameters.RosterId = newValue.alldata.Id;
                scope.teamHierarchyFeed.getData(true);
            });

            scope.$watch('highlightDuration', function (newValue, oldValue) {
                if (Number.isInteger(newValue) == false) {
                    return;
                }

                scope.setHighlighting();
            });

            scope.setHighlighting = function () {
                if (scope.selectedTimesheet == null) {
                    return;
                }

                angular.forEach(scope.selectedTimesheet.data, function (item) {
                    var diff = Math.abs(moment.duration(item.__actualstarttime.diff(item.__scheduledstarttime)).asMinutes());

                    if (diff > scope.highlightDuration) {
                        item.__highlightstarttime = true;
                    }
                    else {
                        item.__highlightstarttime = false;
                    }

                    diff = Math.abs(moment.duration(item.__actualendtime.diff(item.__scheduledendtime)).asMinutes());

                    if (diff > scope.highlightDuration) {
                        item.__highlightendtime = true;
                    }
                    else {
                        item.__highlightendtime = false;
                    }
                });
            };

            scope.canShowTeamHierarchy = function () {
                return scope.teamHierarchyFeed.data.length > 0 && moment(scope.startDate).isValid() && moment(scope.endDate).isValid();
            };

            scope.loadTimesheetFromCache = function (u) {
                return scope.cachedTimesheets[u.alldata.Id.toString()];
            }

            scope.loadTimesheet = function (u) {
                if (u.alldata.UserId == null) {
                    return;
                }

                var sheet = scope.loadTimesheetFromCache(u);

                if(sheet.__isLoaded == false)
                {
                    var parameters = {
                        userid: scope.presented.userid,
                        startdate: moment(scope.startDate).format('DD-MM-YYYY'),
                        enddate: moment(scope.endDate).format('DD-MM-YYYY'),
                        ownerid: u.alldata.UserId,
                        rosterid: scope.selectedRoster.alldata.Id,
                        roleid: u.alldata.RoleId
                    };

                    bworkflowApi.execute('TimesheetManagement', 'CalculateTimesheet', parameters)
                    .then(function (data) {
                        angular.forEach(data.timesheetitems, function (value, index) {
                            value.__approvedsd = moment(value.approvedstartdate, 'YYYY-MM-DDTHH:mm:ss');
                            value.__approvedstartdate = moment(value.approvedstartdate).startOf('day');
                            value.__approvedstartdatestring = value.__approvedstartdate.format('DD-MM-YYYY');
                            value.__approvedstarttime = moment(value.approvedstartdate);
                            value.__approvedstarttimestring = value.__approvedstarttime.format('HH:mm');

                            value.__scheduledstarttime = moment(value.scheduledstartdate);
                            value.__schedulestarttimestring = value.__scheduledstarttime.format('DD-MM-YYYY HH:mm');
                            value.__scheduledendtime = moment(value.scheduledenddate);
                            value.__scheduleendtimestring = value.__scheduledendtime.format('DD-MM-YYYY HH:mm');

                            value.__actualstarttime = moment(value.actualstartdate);
                            value.__actualstarttimestring = value.__actualstarttime.format('DD-MM-YYYY HH:mm');
                            value.__actualendtime = moment(value.actualenddate);
                            value.__actualendtimestring = value.__actualendtime.format('DD-MM-YYYY HH:mm');

                            value.__scheduledlunchsd = moment(value.scheduledlunchstartdate, 'YYYY-MM-DDTHH:mm:ss');
                            value.__scheduledlunchstartdate = moment(value.scheduledlunchstartdate).startOf('day');
                            value.__scheduledlunchstartdatestring = value.__scheduledlunchstartdate.format('DD-MM-YYYY');
                            value.__scheduledlunchstarttime = moment(value.scheduledlunchstartdate);
                            value.__scheduledlunchstarttimestring = value.__scheduledlunchstarttime.format('HH:mm');

                            value.__approveded = moment(value.approvedenddate, 'YYYY-MM-DDTHH:mm:ss');
                            value.__approvedenddate = moment(value.approvedenddate).startOf('day');
                            value.__approvedenddatestring = value.__approvedenddate.format('DD-MM-YYYY');
                            value.__approvedendtime = moment(value.approvedenddate);
                            value.__approvedendtimestring = value.__approvedendtime.format('HH:mm');

                            value.__scheduledlunched = moment(value.scheduledlunchenddate, 'YYYY-MM-DDTHH:mm:ss');
                            value.__scheduledlunchenddate = moment(value.scheduledlunchenddate).startOf('day');
                            value.__scheduledlunchenddatestring = value.__scheduledlunchenddate.format('DD-MM-YYYY');
                            value.__scheduledlunchendtime = moment(value.scheduledlunchenddate);
                            value.__scheduledlunchendtimestring = value.__scheduledlunchendtime.format('HH:mm');

                            value.__highlightstarttime = false;
                            value.__highlightendtime = false;
                        });

                        sheet.data = data.timesheetitems;
                        sheet.__isLoaded = true;

                        var sheetStatus = "Empty";
                        angular.forEach(sheet.data, function (item, index) {
                            if (item.id != null) {
                                if (sheetStatus != "Partial") {
                                    sheetStatus = "Full";
                                }
                                item.__status = "Full";
                            }
                            else {
                                sheetStatus = "Partial";
                                item.__status = "Partial";
                            }

                            // ok it could still get the partial status if clockins aren't mapped in
                            angular.forEach(item.timesheetedtaskworklogs, function (log, index) {
                                if (log.timesheetitemid == 0) {
                                    sheetStatus = "Partial";
                                    log.__status = "Partial";
                                    item.__status = "Partial";
                                }
                                else {
                                    log.__status = "Full";
                                }
                            });
                        });
                        sheet.item.__status = sheetStatus;

                    }, function (tasks) {

                    });
                }

                return sheet;
            };

            scope.selectUser = function (u) {
                if(scope.selectedUser != null)
                {
                    scope.selectedUser.__isSelected = false;
                }

                scope.selectedUser = u;
                scope.selectedUser.__isSelected = true;

                scope.issaving = false;

                scope.selectedTimesheet = scope.loadTimesheet(u);
                scope.setHighlighting();
            };

            scope.findWorkLogMapping = function (log, sheet) {
                var result = null;
                angular.forEach(sheet.data, function (item, index) {
                    angular.forEach(item.timesheetedtaskworklogs, function (l, index) {
                        if (l.projectjobtaskworklogid == log.id) {
                            result = l;
                        }
                    });
                });

                return result;
            }

            scope.selectTimesheet = function (t) {
                if (scope.selectedTimesheetItem == t)
                {
                    scope.selectedTimesheetItem = null;
                    return;
                }

                var parameters = {
                    rosterid: scope.selectedRoster.alldata.Id,
                    sheeted: t.timesheetedtaskworklogs
                };

                bworkflowApi.execute('TimesheetManagement', 'GetTimesheetItemDetails', parameters)
                .then(function (data) {
                    scope.selectedTimesheetItemData = data;

                    angular.forEach(scope.selectedTimesheetItemData.worklog, function (d, index) {
                        var map = scope.findWorkLogMapping(d, scope.selectedTimesheet);
                        if (map != null) {
                            d.__status = map.__status;
                        }
                    });

                }, function (tasks) {

                });

                scope.selectedTimesheetItem = t;
            };

            scope.combineDateAndTime = function (d, t) {
                var day = d.clone();
                var tDay = t.clone().startOf('day');
                var time = t.clone();

                var r = day.add(time.diff(tDay, 'seconds'), 'seconds');

                return r;
            };

            scope.calculateApprovedDuration = function (t) {
                t.__approvedsd = scope.combineDateAndTime(t.__approvedstartdate, t.__approvedstarttime);
                t.__approveded = scope.combineDateAndTime(t.__approvedenddate, t.__approvedendtime);

                t.__scheduledlunchsd = scope.combineDateAndTime(t.__scheduledlunchstartdate, t.__scheduledlunchstarttime);
                t.__scheduledlunched = scope.combineDateAndTime(t.__scheduledlunchenddate, t.__scheduledlunchendtime);

                t.approvedduration = t.__approveded.diff(t.__approvedsd, 'seconds');
                t.lunchduration = t.__scheduledlunched.diff(t.__scheduledlunchsd, 'seconds');

                if(t.islunchpaid == false && t.lunchduration != null)
                {
                    t.approvedduration = t.approvedduration - t.lunchduration;
                }
            };

            scope.save = function (selected) {
                // the save method saves each user timesheet 1 at a time.
                // the pattern used to do this is to create a set of actions, which
                // are loaded in sequence and chained together. The commit method of
                // each action is expected to do it's thing and then chain onto the
                // next action in the list of actions.
                scope.savingItemIndex = -1;
                scope.issaving = true;

                scope.saveactions = [];

                var startdate = moment(scope.startDate).format('DD-MM-YYYY');
                var enddate = moment(scope.endDate).format('DD-MM-YYYY');

                // loop through our cached timesheets and build up save actions for each one based on their state
                angular.forEach(scope.cachedTimesheets, function (value, index) {
                    if (angular.isDefined(selected) == true && value != selected) {
                        return;
                    }

                    var action = {
                        parameters: {
                            userid: scope.presented.userid,
                            startdate: startdate,
                            enddate: enddate,
                            ownerid: value.item.alldata.UserId,
                            rosterid: scope.selectedRoster.alldata.Id,
                            roleid: value.item.alldata.RoleId,
                            timesheetitems: value.data
                        },
                        text: value.item.alldata.Name,
                        loading: false,
                        complete: false,
                        loadAttempts: 0,
                        sheet: value,
                        commit: function (data) {
                            data.loading = true;

                            angular.forEach(data.parameters.timesheetitems, function (t, v) {
                                // need to copy from our working variables to the ones the server is going to use
                                t.approvedstartdate = t.__approvedsd.format('YYYY-MM-DDTHH:mm:ss');
                                t.approvedenddate = t.__approveded.format('YYYY-MM-DDTHH:mm:ss');

                                t.scheduledlunchstartdate = t.__scheduledlunchsd.format('YYYY-MM-DDTHH:mm:ss');
                                t.scheduledlunchenddate = t.__scheduledlunched.format('YYYY-MM-DDTHH:mm:ss');
                            });

                            bworkflowApi.execute('TimesheetManagement', 'Save', data.parameters)
                            .then(function (response) {
                                if (response.success == true)
                                {
                                    data.loading = false;
                                    data.complete = true;
                                    data.loadAttempts = data.loadAttempts + 1;
                                }
                                else {
                                    data.loading = false;
                                    data.complete = false;
                                    data.loadAttempts = data.loadAttempts + 1;
                                }

                                // reload what was saved
                                data.sheet.__isLoaded = false;
                                data.sheet.data = null;
                                scope.loadTimesheet(data.sheet.item);

                                scope.doSaveNextItem(); // next item in the chain please
                            }, function (tasks) {
                                data.loading = false;
                                data.complete = false;
                                data.loadAttempts = data.loadAttempts + 1;

                                scope.doSaveNextItem(); // next item in the chain please
                            });
                        }
                    };

                    scope.saveactions.push(action);
                });

                // give the UI an opportunity to present things to the user
                $timeout(function () {
                    // we let the actions chain themselves together one at a time
                    // by committing them one at a time. When an action finishes,
                    // it should call the doApproveNextItem method so that the
                    // next action can do its thing. We start the chain here
                    scope.doSaveNextItem();
                });
            };

            scope.doSaveNextItem = function () {
                scope.savingItemIndex = scope.savingItemIndex + 1;

                if (scope.savingItemIndex >= scope.saveactions.length) {
                    scope.issavingcomplete = true;
                    return;
                }

                var currentAction = scope.saveactions[scope.savingItemIndex];

                currentAction.commit(currentAction);
            };
        }
    });
}]);

questionsModule.factory('eway-payment', ['$http', '$q', '$window', 'pendingPaymentsService', '$timeout', function ($http, $q, $window, pendingPaymentsService, $timeout) {
    var deferReady = $q.defer();
    var _api = $window.razordata.apiprefix;

    // Inject the eWay JS Script ..
    var scriptTag = document.createElement('script');
    scriptTag.src = 'https://secure.ewaypayments.com/scripts/eCrypt.js';
    scriptTag.onload = function () {
        deferReady.resolve();
    };
    var head = document.getElementsByTagName('head')[0];
    head.appendChild(scriptTag);

    return {
        ready: function () {
            return deferReady.promise;
        },

        processPreparedPayment: function (sharedPaymentUrl) {
            var defer = $q.defer();
            eCrypt.showModalPayment({
                sharedPaymentUrl: sharedPaymentUrl
            }, function (result, transactionID, errors) {
                if (result === "Complete") {
                    defer.resolve({
                        transactionID: transactionID
                    });
                } else if (result === "Cancel") {
                    defer.resolve({
                        cancelled: true
                    });
                } else if (errors) {
                    defer.reject(angular.isArray(errors) ? errors : [ errors ]);
                } 
            });
            return defer.promise;
        },

        processPayment: function (pendingPaymentId) {
            var self = this;
            return $http({
                url: _api + 'eWayPayment/PreparePayment', method: 'POST', data: { pendingPaymentId: pendingPaymentId }
            }).then(function (response) {
                if (response.data.SharedPaymentUrl) {
                    return self.processPreparedPayment(response.data.SharedPaymentUrl);
                } else if (response.data.Errors) {
                    $http({
                        url: _api + 'eWayPayment/GetErrorDescriptions', method: 'POST', data: response.data.Errors
                    }).then(function (errors) {
                        return $q.reject(errors.data);
                    });
                }
            });
        },

        queryTransaction: function (pendingPaymentId) {
            var self = this;
            return $http({
                url: _api + 'eWayPayment/QueryTransaction', method: 'POST', data: { pendingPaymentId: pendingPaymentId }
            }).then(function (response) {
                if (response.data && response.data.PendingPayments) {
                    angular.forEach(response.data.PendingPayments, function (pp) {
                        pendingPaymentsService.modelPendingPayment(pp);
                    });
                } else {
                    // Delay and try again, server is now handling the actual trigger of the payment
                    return $timeout(function () {
                        return self.queryTransaction(pendingPaymentId);
                    }, 5000);
                }
                return response.data;
            });
        }
    };
}]);

questionsModule.directive('questionEwayPayment', ['$log', 'bworkflowApi', 'eway-payment', 'playerButtons', 'playerActions', 'languageTranslate', function ($log, bworkflowApi, ewayPayment, playerButtons, playerActions, languageTranslate) {
    return $.extend({}, questionDirectiveBase, {
        templateUrl: 'question_eway_payment.html',
        scope: {
            presented: '=',
            pageScope: '='
        },
        link: function (scope, elt, attrs) {
            questionDirectiveBase.link(scope, undefined, bworkflowApi, languageTranslate);

            scope.acceptedTCs = true;           // pageScope should set this to false if required
            scope.termsAndConditionsURL = '';
            scope.pageScopeReady = true;        // pageScope can set to false/true to customise 
            ewayPayment.ready().then(function () {
                scope.ready = true;           
            });

            scope.acceptTCs = function () {
                scope.acceptedTCs = !scope.acceptedTCs;
            };

            scope.readyToPay = function () {
                return scope.ready && scope.pageScopeReady && scope.acceptedTCs && !scope.success && !scope.presented.responseerrors && scope.PayPendingPayment && !scope.finalising;
            };

            scope.Transaction = scope.presented.transaction;

            scope.setPendingPayment = function(pp) {
                scope.PayPendingPayment = pp;
                if (scope.PayPendingPayment) {
                    scope.PayPendingPayment.payNowState = 1;
                    playerButtons.canNext = false;
                }
            };

            scope.answer = scope.presented.Answer;

            // this supports validation as each question type stores it's answer in a different way
            scope.getAnswerValue = function () {
                return scope.answer.TransactionID;
            };

            scope.payNow = function () {
                scope.PayPendingPayment.payNowState = 2;
                scope.processing = true;
                scope.success = false;
                delete scope.errors;

                ewayPayment.processPayment(scope.PayPendingPayment.Id).then(function (result) {
                    scope.processing = false;
                    if (!result.cancelled) {
                        scope.finalising = true;
                        ewayPayment.queryTransaction(scope.PayPendingPayment.Id).then(function (response) {
                            scope.finalising = false;
                            scope.paymentResult = response.TransactionResponse;
                            scope.success = scope.paymentResult.TransactionStatus.Status;
                            if (scope.paymentResult.TransactionStatus.Status) {
                                delete scope.PayPendingPayment.payNowState;
                                scope.answer.TransactionID = scope.paymentResult.TransactionStatus.TransactionID;

                                // Force next page
                                playerActions.doTransition('next');
                            } else {
                                scope.PayPendingPayment.payNowState = 1;
                            }
                        }, function (error) {
                            scope.finalising = false;
                            scope.PayPendingPayment.payNowState = 1;
                            scope.errors = [error];
                        });
                    } else {
                        scope.PayPendingPayment.payNowState = 1;
                    }
                }, function (errors) {
                    scope.PayPendingPayment.payNowState = 1;
                    scope.errors = errors;
                    scope.processing = false;
                });
            };
        }
    });
}]);

questionsModule.factory('pendingPaymentsService', ['$http', '$window', function ($http, $window) {
    var _api = $window.razordata.apiprefix;

    var svc = {
        modelPendingPayment: function (pp) {
            pp.ActivatedDateUtc = pp.ActivatedDateUtc ? moment(pp.ActivatedDateUtc) : null;
            pp.PaidInFullDateUtc = pp.PaidInFullDateUtc ? moment(pp.PaidInFullDateUtc) : null;
            pp.CancelledDateUtc = pp.CancelledDateUtc ? moment(pp.CancelledDateUtc) : null;
            pp.DueDateUtc = pp.DueDateUtc ? moment(pp.DueDateUtc) : null;
            pp.FromUtc = pp.FromUtc ? moment(pp.FromUtc) : null;
            pp.ToUtc = pp.ToUtc ? moment(pp.ToUtc) : null;
            pp.PendingExpiryUtc = pp.PendingExpiryUtc ? moment(pp.PendingExpiryUtc) : null;
        },

        modelPendingPayments: function (pendingPayments) {
            if (!pendingPayments) {
                return;
            }
            var self = this;
            if (pendingPayments.Plan) {
                pendingPayments.Plan.Start = moment(pendingPayments.Plan.Start);
                pendingPayments.Plan.End = moment(pendingPayments.Plan.End);
                pendingPayments.Plan.FirstBillingDateUtc = moment(pendingPayments.Plan.FirstBillingDateUtc);
                pendingPayments.Plan.LastBillingDateUtc = moment(pendingPayments.Plan.LastBillingDateUtc);
            }
            pendingPayments.totalDue = 0;
            pendingPayments.totalFull = 0;
            pendingPayments.totalDiscount = 0;
            angular.forEach(pendingPayments.Payments, function (pp) {
                self.modelPendingPayment(pp);
                pendingPayments.totalDue += pp.DueAmount;
                pendingPayments.totalFull += pp.FullAmount;
                pendingPayments.totalDiscount += pp.DiscountAmount;
            });
        },

        applyCouponCode: function (couponCode, orderId) {
            var self = this;
            return $http({
                url: _api + 'Order/ApplyCouponCode', method: 'POST', data: { couponCode: couponCode, orderId: orderId }
            }).then(function (response) {
                if (response.data && response.data.payments) {
                    self.modelPendingPayments(response.data.payments);
                }
                return response.data;
            });
        },

        removeCoupon: function (couponId, orderId) {
            var self = this;
            return $http({
                url: _api + 'Order/RemoveCouponCode', method: 'POST', data: { couponId: couponId, orderId: orderId }
            }).then(function (response) {
                if (response.data && response.data.payments) {
                    self.modelPendingPayments(response.data.payments);
                }
                return response.data;
            });
        },

        orderCoupons: function (orderId) {
            var self = this;
            return $http({
                url: _api + 'Order/OrderCoupons', method: 'POST', data: { orderId: orderId }
            }).then(function (response) {
                if (response.data && response.data.payments) {
                    self.modelPendingPayments(response.data.payments);
                    response.data.order.CancelledDateUtc = response.data.order.CancelledDateUtc ? moment(response.data.order.CancelledDateUtc) : null;
                }
                return response.data;
            });
        },

        cancelOrder: function (orderId) {
            var self = this;
            return $http({
                url: _api + 'Order/CancelOrder', method: 'POST', data: { orderId: orderId }
            }).then(function (response) {
                if (response.data && response.data.payments) {
                    self.modelPendingPayments(response.data.payments);
                    response.data.order.CancelledDateUtc = response.data.order.CancelledDateUtc ? moment(response.data.order.CancelledDateUtc) : null;
                }
                return response.data;
            });
        }

    };
    return svc;
}]);

questionsModule.directive('questionPendingPayments', ['pendingPaymentsService', 'bworkflowApi', 'languageTranslate', function (pendingPaymentsService, bworkflowApi, languageTranslate) {
    return $.extend({}, questionDirectiveBase, {
        templateUrl: 'question_pending_payments.html',
        scope: {
            presented: '=',
            pageScope: '='
        },
        link: function (scope, elt, attribs) {
            var couponErrors = {
                "InvalidCode": "Invalid code",
                "OutOfDate": "Invalid code",
                "AlreadyApplied": "Already applied",
                "CouponLimitReached": "Coupon limit reached",
                "NotApplied": "Not applied"
            };

            questionDirectiveBase.link(scope, undefined, bworkflowApi, languageTranslate);
            pendingPaymentsService.modelPendingPayments(scope.presented.pendingpayments);

            scope.presented.couponcodename = scope.presented.couponcodename || 'Coupon code';
            scope.pendingPayments = scope.presented.pendingpayments;
            scope.customer = scope.presented.customer;

            scope.coupon = {
                applied: []
            };
            scope.resetCouponCode = function () {
                scope.coupon.error = null;
                scope.coupon.message = null;
            };

            function updateCouponResult(response) {
                scope.pendingPayments = scope.pendingPayments || response.payments;
                angular.forEach(response.payments.Payments, function (p) {
                    var pp = scope.pendingPayments.Payments.find(function (_) { return _.Id === p.Id; });
                    angular.extend(pp, p);
                });
                scope.pendingPayments.totalDue = response.payments.totalDue;
                scope.pendingPayments.totalFull = response.payments.totalFull;
                scope.pendingPayments.totalDiscount = response.payments.totalDiscount;
                scope.coupon.applied = response.appliedCoupons;
                scope.order = response.order;
            }

            scope.applyCouponCode = function () {
                scope.coupon.checking = true;
                pendingPaymentsService.applyCouponCode(scope.coupon.couponcode, scope.presented.orderid).then(function (response) {
                    scope.coupon.checking = false;
                    if (response.result === "Applied") {
                        scope.coupon.couponcode = '';
                        updateCouponResult(response);
                    } else {
                        scope.coupon.error = couponErrors[response.result] || "Invalid code";
                        scope.coupon.message = response.message;
                    }
                }, function (err) {
                    scope.coupon.error = "Error";
                    scope.coupon.message = null;
                    scope.coupon.checking = false;
                });
            };

            scope.removeCoupon = function (coupon) {
                pendingPaymentsService.removeCoupon(coupon.id, scope.presented.orderid).then(function (response) {
                    updateCouponResult(response);
                });
            };

            // Get fresh list of applied coupons now ..
            pendingPaymentsService.orderCoupons(scope.presented.orderid).then(function (response) {
                updateCouponResult(response);
            });

            // A little strange having the PendingPayments question handle Order cancelling, but easiest place to put it currently
            scope.cancelOrder = function () {
                if (confirm('Cancelling an Order is irreversible, are you sure you want to cancel this Order ?')) {
                    pendingPaymentsService.cancelOrder(scope.presented.orderid).then(function (response) {
                        updateCouponResult(response);

                        scope.$emit('order.cancelled', scope.presented.orderid);
                    });
                } 
            }
        }
    });
}]);

questionsModule.directive('questionSelectfacilitystructure', ['bworkflowApi', '$interval', 'languageTranslate', function (bworkflowApi, $interval, languageTranslate) {
    return $.extend({}, questionDirectiveBase, {
        templateUrl: 'question_selectfacilitystructure.html',
        link: function (scope, elt, attrs) {
            questionDirectiveBase.link(scope, undefined, bworkflowApi, languageTranslate);

            scope.answer = scope.presented.Answer;

            scope.facilityStructureFeedFilters = [];

            scope.addFacilityStructureFeedFilter = function (filterFunction) {
                scope.facilityStructureFeedFilters.push(filterFunction);
            };

            scope.applyFacilityStructureFeedFilters = function (feed) {
                if (scope.facilityStructureFeedFilters.length == 0) {
                    return; // nothing to do
                }

                // run through each filer function and remove anything it indicates to filter out
                angular.forEach(scope.facilityStructureFeedFilters, function (filterFunction) {
                    var removals = filterFunction(feed.data);

                    angular.forEach(removals, function (remove) {
                        var index = feed.data.indexOf(remove);

                        if (index != -1) {
                            feed.data.splice(index, 1);
                        }
                    });
                });
            };

            scope.refreshFacilityStructureFeed = function () {
                scope.facilityStructureFeed.getData(true);
            };

            scope.getAnswerValue = function () {
                return scope.answer.SelectedFacilityStructureId;
            };

            scope.facilityStructureFeedTemplate = {
                name: 'facilitystructure',
                feed: 'FacilityStructures',
                filter: "Archived eq false and ParentId eq [[selectedFacilityStructureId]]",
                orderbyfields: 'SortOrder,Name',
                idfields: ['Id'],
                itemsperpage: 100,
                parameterdefinitions: [
                    {
                        internalname: 'selectedFacilityStructureId'
                    }]
            };

            scope.selectedFacilityStructureId = null;

            scope.defaultSelectedFacilityStructureId = "null";
            if (scope.presented.startfromparentid != null) {
                scope.defaultSelectedFacilityStructureId = scope.presented.startfromparentid;
            }

            scope.selectedHistory = [];

            scope.facilityStructureFeed = bworkflowApi.createDataFeed(scope.facilityStructureFeedTemplate, scope);
            
            scope.facilityStructureFeed.addAfterLoadHook(function (feed) {
                scope.applyFacilityStructureFeedFilters(feed);

                if (feed.data.length == 0) {
                    // assume we are at the bottom and select our answer
                    scope.answer.SelectedFacilityStructureId = scope.selectedFacilityStructureId;
                }
            });

            // On error, retry
            scope.facilityStructureFeed.addAfterErrorHook(function (feed) {
                feed.countdown = 5;
                feed.timer = $interval(function () {
                    if (--feed.countdown == 0) {
                        $interval.cancel(feed.timer);
                        feed.getData(true);
                    }
                }, 1000);
            });

            scope.$watch('selectedFacilityStructureId', function (newValue, oldValue) {
                if (newValue === null) {
                    // we change the filter as a null variable get's inserted into the filter as ''
                    scope.facilityStructureFeed.template.filter = "Archived eq false and ParentId eq " + scope.defaultSelectedFacilityStructureId;
                }
                else {
                    scope.facilityStructureFeed.template.filter = "Archived eq false and ParentId eq [[selectedFacilityStructureId]]";
                }

                scope.facilityStructureFeed.parameters.selectedFacilityStructureId = newValue;
                scope.facilityStructureFeed.getData(true);
            });

            scope.selectFacilityStructure = function (structure, index, $event) {
                // a final answer can't be arrived at if they've just made a selection as more data
                // needs to get loaded through odata for the next level. If nothing comes back on that (see afterloadhook)
                // then we'll set the answer
                scope.answer.SelectedFacilityStructureId = null;

                if (index !== -1) {
                    // 0 or positive value indicates a backward movement (using breadcrumbs), so we need to chop off the end
                    // of our history to just before this
                    scope.selectedHistory.splice(index, scope.selectedHistory.length - index);
                }

                if (structure !== null) {

                    scope.selectedHistory.push(structure);

                    scope.selectedFacilityStructureId = structure.alldata.Id;
                }
                else {
                    scope.selectedFacilityStructureId = null;
                }

                if (angular.isDefined($event) === true) {
                    $event.preventDefault();
                }
            };

            scope.raiseEvent = function (eventName, args) {
                // allows someone to raise an event to get other elements to do things,
                // for example clicking a button in the label refreshing a task list
                scope.$emit('player_broadcast', { name: eventName, data: args });
            };

            scope.getSelectedFacilityStructure = function () {
                if (!scope.answer.SelectedFacilityStructureId) {
                    return null;
                }
                return scope.selectedHistory[scope.selectedHistory.length - 1].alldata;
            };

            scope.resetSelection = function () {
                scope.selectedFacilityStructureId = null;
                scope.selectedHistory = [];
            };

            scope.popToParent = function () {
                var fs = scope.getSelectedFacilityStructure();
                if (fs && fs.ParentId && fs.Id !== scope.defaultSelectedFacilityStructureId) {
                    scope.selectedFacilityStructureId = fs.ParentId;
                    scope.selectedHistory.pop();
                    scope.answer.SelectedFacilityStructureId = null;
                } else {
                    scope.resetSelection();
                }
            };
        }
    })
}]);

questionsModule.factory('orderSvc', ['jsFunctionSvc', '$sce', '$http', '$window', function (jsFunctionSvc, $sce, $http, $window) {
    var _api = $window.razordata.apiprefix;

    var svc = {
        units: ['', 'qty', 'm', 'm<sup>2</sup>', 'm<sup>3</sup>', 'm<sup>2</sup>'],

        createTotaller: function () {
            return jsFunctionSvc.getScript().then(function (result) {
                var fns = result.functions;
                var now = result.utcnow;                // but use servertime if possible
                var fnItemPrice = function (item, order, orders) {
                    // Reset all item calculated properties in prep for the custom function
                    item.discount = 0;
                    item.surcharge = 0;
                    delete item.totalPrice;
                    delete item.priceNotes;
                    delete item.error;

                    // Pricing function can customise the units ..
                    item.units = svc.units[item.producttype.units];

                    var notes = [];
                    if (fns && item.pricejsfunctionid) {
                        item.totalPrice = fns.byId[item.pricejsfunctionid]({
                            item: item,
                            notes: notes,
                            order: order,
                            orders: orders,
                            now: now
                        });

                        if (angular.isUndefined(item.priceNotes)) {
                            item.priceNotes = $sce.trustAsHtml(notes.join('<br/>'));
                        }
                    }
                    else {
                        // By default we accept Quantity as whole +ve numbers to be valid, any special Quantities must be handled by a custom JS function
                        if (item.quantity > 0 && Math.round(item.quantity) === item.quantity) {
                            item.totalPrice = item.quantity * item.price;
                        } else if (item.quantity) {
                            delete item.totalPrice;
                            item.error = 'Invalid quantity';
                        }
                    }

                    return item.totalPrice;
                };

                var fnTotal = function (order, orders) {
                    if (angular.isUndefined(orders) && angular.isDefined(order.length)) {
                        orders = order;

                        var grandTotalFn = [];
                        orders.grandTotalFn = function (fn, priority) {
                            grandTotalFn.push({ fn: fn, priority: priority || 0 });
                        };

                        // its a number of orders we are being asked to total up
                        angular.forEach(orders, function (o) {
                            fnTotal(o, orders);
                        });

                        grandTotalFn = grandTotalFn.sortBy(function (tfn) {
                            return tfn.priority;
                        });

                        // GrandTotal is *always* the sum of all Order totals, custom pricing must adjust an order.total and not expect anything but sum('total')
                        orders.grandTotal = orders.sum('total');
                        angular.forEach(grandTotalFn, function (tfn) {
                            tfn.fn(orders);

                            // Resum the grandTotal for the next fn to work with
                            orders.grandTotal = orders.sum('total');
                        });

                        return orders.grandTotal;
                    }
                    else {
                        var totalFn = [];
                        order.totalFn = function (fn, priority) {
                            totalFn.push({ fn: fn, priority: priority || 0 });
                        };

                        // its a single order we are totalling up
                        angular.forEach(order.items, function (item) {
                            fnItemPrice(item, order, orders);
                        });

                        totalFn = totalFn.sortBy(function (tfn) {
                            return tfn.priority;
                        });

                        // Order total is *always* the sum of each item's totalPrice
                        order.total = order.items.sum(function (i) { return i.quantity ? i.totalPrice || 0 : 0; });
                        angular.forEach(totalFn, function (tfn) {
                            tfn.fn(order);

                            // Resum the order total for the next fn to work with
                            order.total = order.items.sum(function (i) { return i.quantity ? i.totalPrice || 0 : 0; });
                        });

                        // Orders with 'fixed' items need to be identified
                        order.fixedCount = order.items.count(function (i) { return i.fixed; });

                        return order.total;
                    }
                };
                return {
                    total: fnTotal
                };
            });
        },

        getProductCatalog: function (fsId) {
            return $http({ url: _api + 'ProductCatalog/CatalogsForFacilityStructure?id=' + fsId.toString() }).then(function (response) {
                return response.data;
            });
        }
    };
    return svc;
}]);

// This is a directive used by questionOrder to present the UI for catalogs
questionsModule.directive('questionOrderCatalog', ['bworkflowApi', '$sce', 'orderSvc', '$filter', '$timeout', function (bworkflowApi, $sce, orderSvc, $filter, $timeout) {
    return {
        require: "ngModel",
        templateUrl: 'question_order_catalog.html',
        scope: {
            template: '=',
            order: '=ngModel'
        },
        link: function (scope, element, attrs, ngModel) {
            scope.selected = { product: -1, manualproducts: '', children: {} };
            scope.productstoconfigure = [];

            scope.units = orderSvc.units;
            scope.showconfigurechild = false;

            scope.trustAsHtml = function (txt) {
                return $sce.trustAsHtml(txt);
            };

            scope.removeitem = function (item) {
                delete item.quantity;
                if (angular.isDefined(item.width)) {
                    delete item.width;
                }
                if (angular.isDefined(item.length)) {
                    delete item.length;
                }
                delete item.error;
                item.added = false;
            };

            orderSvc.createTotaller().then(function (totaller) {
                scope.$watch('order.items.length', function () {
                    totaller.total(scope.order);
                });

                scope.changeItemQuantity = function (item) {
                    totaller.total(scope.order);
                };

                scope.changeItemLength = function (item) {
                    if (!item.length || item.length < 0 || !item.width || item.width < 0) {
                        // can't have an area
                        delete item.quantity;
                    } else {
                        item.quantity = item.length * item.width;
                    }

                    totaller.total(scope.order);
                };

                scope.changeItemWidth = function (item) {
                    if (!item.length || item.length < 0 || !item.width || item.width < 0) {
                        // can't have an area
                        delete item.quantity;
                    } else {
                        item.quantity = item.length * item.width;
                    }

                    totaller.total(scope.order);
                };
            });

            scope.$watch("selected.product", function (newValue, oldValue) {
                if (angular.isDefined(newValue) === false || newValue === null) {
                    return;
                }

                var item = null;
                angular.forEach(scope.order.items, function (i) {
                    if (i.productid === scope.selected.product.productid) {
                        item = i;
                    }
                });

                if (item === null) {
                    return;
                }

                if (item.childproducts.length === 0) {
                    item.added = true;
                    return;
                }

                scope.productstoconfigure.push(scope.selected.product);
                scope.showconfigurechild = true;
            });

            scope.processManualEntry = function () {
                if (angular.isDefined(scope.selected.manualproducts) === false || scope.selected.manualproducts === null) {
                    return;
                }

                scope.productstoconfigure = [];

                var parts = scope.selected.manualproducts.split(',');
                angular.forEach(parts, function (part) {
                    var p = part.trim();

                    var item = null;
                    angular.forEach(scope.order.items, function (i) {
                        if (i.product == p || i.code == p) {
                            item = i;
                        }
                    });

                    if (item === null) {
                        return;
                    }

                    if (item.childproducts.length === 0) {
                        item.added = true;
                        return;
                    }

                    scope.productstoconfigure.push(item);
                    scope.showconfigurechild = true;
                    return;
                });
            };

            scope.removeProductToConfigure = function (p) {
                var index = scope.productstoconfigure.indexOf(p);
                if (index != -1) {
                    scope.productstoconfigure.splice(index, 1);
                }

                if (scope.productstoconfigure.length == 0) {
                    scope.selected.manualproducts = '';
                    scope.showconfigurechild = false;
                }
            };

            scope.confirmChild = function (parent, child) {
                scope.selected.product = child;

                scope.removeProductToConfigure(parent);
            };

            scope.cancelChild = function (parent, child) {
                scope.removeProductToConfigure(parent);
            };
        }
    };
}]);

questionsModule.directive('questionOrder', ['$timeout', '$sce', 'orderSvc', 'bworkflowApi', '$q', 'languageTranslate', function ($timeout, $sce, orderSvc, bworkflowApi, $q, languageTranslate) {
    return $.extend({}, questionDirectiveBase, {
        templateUrl: 'question_order.html',
        link: function (scope, elt, attrs) {
            questionDirectiveBase.link(scope, undefined, bworkflowApi, languageTranslate);

            scope.answer = scope.presented.Answer;

            scope.visibleOrders = [];

            scope.findOrderForCatalog = function (orders, catalog, fsId) {
                var order = null;

                angular.forEach(orders, function (o) {
                    if (o.id === catalog.id && (!fsId || o.facilityStructureId === fsId)) {
                        order = o;
                    }
                });

                return order;
            };

            scope.findTypeForProduct = function (item) {
                var type = null;
                angular.forEach(scope.presented.template.producttypes, function (t) {
                    if (t.id === item.typeid) {
                        type = t;
                    }
                });

                return type;
            };

            scope.hasQuantity = function (i) {
                return angular.isDefined(i.quantity) && i.quantity !== null && i.quantity !== '' && i.quantity !== 0;
            };

            scope.populateOrders = function (orders, catalogs, fs) {
                var result = [];
                angular.forEach(catalogs, function (cat) {
                    var order = scope.findOrderForCatalog(orders, cat, fs ? fs.Id : null);

                    if (order === null) {
                        order = angular.copy(cat);
                        order.uniqueid = generateCombGuid();

                        if (fs) {
                            order.facilityStructureId = fs.Id;
                            order.title = fs.Name;
                        }

                        orders.push(order);

                        // map in the product types
                        angular.forEach(order.items, function (i) {
                            i.producttype = scope.findTypeForProduct(i);

                            // to keep things simple for the addremove template, we have a property that indicates if a product
                            // has been added to an order
                            i.added = false;
                            i.valid = true;
                            if (scope.hasQuantity(i)) {
                                i.added = true;
                            }
                        });
                    }

                    result.push(order);
                });

                return result;
            };

            scope.isOrderVisible = function (order) {
                if (scope.presented.template.showcatalogs.length === 0) {
                    return true;
                }

                var visible = false;

                angular.forEach(scope.presented.template.showcatalogs, function (cat) {
                    if (order.name.toLowerCase() === cat.toLowerCase()) {
                        visible = true;
                    }
                });

                return visible;
            };

            scope.fillVisibleOrders = function (orders) {
                scope.visibleOrders = [];
                angular.forEach(orders, function (o) {
                    if (scope.isOrderVisible(o)) {
                        scope.visibleOrders.push(o);
                    }
                });
            };

            orderSvc.createTotaller().then(function (totaller) {
                scope.total = function (orders) {
                    return totaller.total(orders);
                };
            });

            scope.populateOrders(scope.answer.orders, scope.presented.template.catalogs);
            scope.fillVisibleOrders(scope.answer.orders);

            scope.getAnswerValue = function () {
                return "This will get wrapped back round into us below";
            };

            scope.mandatoryvalidator = function (question, value, v, stage) {
                var result = "should pass mandatory";
                var addedCount = 0;
                var errorCount = 0;

                angular.forEach(scope.answer.orders, function (order) {
                    angular.forEach(order.items, function (item) {
                        item.valid = true;

                        if (item.added === true) {
                            addedCount = addedCount + 1;

                            if (item.error) {
                                errorCount++;
                            }
                            if (scope.hasQuantity(item) === false || item.error) {
                                item.valid = false;
                                result = null; // so there is an item that is marked as added, but has no quantity, so this aint right
                            }
                        }
                    });
                });

                if (errorCount) {
                    return { passed: false, message: 'There are errors with 1 or more items' };
                }

                if (addedCount === 0) {
                    result = null;
                }

                if (result !== null) {
                    return { passed: true };
                }
                else {
                    return { passed: false, message: 'At least 1 item must be selected and all items selected must have a quantity set' };
                }
            };

            scope.removeOrder = function (order) {
                if (order.total > 0) {
                    if (!confirm("Are you sure you want to remove order " + (order.title || order.name) + " ?")) {
                        return;
                    }
                }

                var i = scope.answer.orders.indexOf(order);
                if (i >= 0) {
                    scope.answer.orders.splice(i, 1);
                    scope.fillVisibleOrders(scope.answer.orders);

                    scope.$emit('order.removed', order);
                }
            };

            scope.selectOrderTab = function (order) {
                $timeout(function () {
                    $('#atab' + order.uniqueid).tab('show');
                });
            };

            // This PageScope callable function allows a Facility Structure question and Order question to pair up to provide support for ordering from multiple facility structure catalogs
            scope.pairWithFSQuestion = function (pairedFSQuestion, events) {
                scope.pairedFSQuestion = pairedFSQuestion;
                pairedFSQuestion.pairedOrderQuestion = scope;

                pairedFSQuestion.$watch('answer.SelectedFacilityStructureId', function (fsId) {
                    if (fsId) {
                        var fs = pairedFSQuestion.getSelectedFacilityStructure();
                        scope.addFacilityStructureCatalog(fs).then(function (selectedOrders) {
                            // Prepare FS question for next selection
                            pairedFSQuestion.resetSelection();

                            if (events && angular.isFunction(events.selectedFacilityStructure)) {
                                events.selectedFacilityStructure(fs);
                            }
                            if (selectedOrders.length > 0) {
                                scope.selectOrderTab(selectedOrders[0]);
                            }
                        });
                    }
                });
            };

            scope.addFacilityStructureCatalog = function (facilityStructure, filterFn) {
                var pq;
                if (angular.isNumber(facilityStructure)) {
                    var feedTemplate = {
                        name: 'facilitystructure',
                        feed: 'FacilityStructures',
                        filter: 'Id eq ' + facilityStructure.toString(),
                        orderbyfields: 'Name',
                        idfields: ['Id'],
                        itemsperpage: 1
                    };
                    var feed = bworkflowApi.createDataFeed(feedTemplate);
                    pq = feed.getData(true).then(function (data) {
                        if (data.length > 0) {
                            return data[0].alldata;
                        } else {
                            return null;
                        }
                    });
                } else {
                    pq = $q.when(facilityStructure);
                }
                return pq.then(function (fs) {
                    return orderSvc.getProductCatalog(fs.Id).then(function (catalogs) {
                        if (filterFn) {
                            catalogs = filterFn(catalogs);
                        }
                        var selected = scope.populateOrders(scope.answer.orders, catalogs, fs);
                        scope.fillVisibleOrders(scope.answer.orders);
                        return selected;
                    });
                });
            };

            scope.orderTemplate = function (order) {
                if (angular.isUndefined(order.template)) {
                    order.template = angular.copy(scope.presented.template);
                }
                return order.template;
            };
        }
    });
}]);

questionsModule.directive('questionRecaptcha', ['bworkflowApi', 'languageTranslate', function (bworkflowApi, languageTranslate) {
    return $.extend({}, questionDirectiveBase, {
        templateUrl: 'question_recaptcha.html',
        link: function (scope, elt, attrs) {
            scope.answer = scope.presented.Answer;

            questionDirectiveBase.link(scope, undefined, bworkflowApi, languageTranslate);

            scope.answer.recaptchaResponse = "";
        }
    });
}]);

questionsModule.factory('phoneBookMaker', function () {
    return function (opts) {
        opts = angular.extend({
            getName: angular.identity,
            click: angular.identity
        }, opts || {});
        opts.itemsPerLetterBucket = opts.itemsPerLetterBucket || 20;

        return function (items) {      
            var maxSplitLength = 0;

            function split(letterBucket, splitLength) {
                maxSplitLength = maxSplitLength < splitLength ? splitLength : maxSplitLength;
                var split = [];
                letterBucket.items.forEach(function (item) {
                    pushItem(item, split, splitLength);
                })
                return split;
            }

            function pushItem(item, letterBucket, splitLength) {
                var letters = opts.getName(item).trim();
                var bucket = letterBucket.find(function (b) {
                    return b.match(letters);
                });
                if (bucket == null) {
                    bucket = {
                        calcLetters: function () {
                            var first, last;
                            var length = maxSplitLength < splitLength ? splitLength : maxSplitLength;
                            bucket.items.forEach(function (i) {
                                last = opts.getName(i).trim();
                                if (!first) {
                                    first = last;
                                }
                            });

                            if (first === last) {
                                return first.substring(0, length) + '...';
                            }
                            for (var i = 0; i < first.length > last.length ? last.length : first.length; i++) {
                                if (first[i] !== last[i]) {
                                    return first.substring(0, i+1) + '...' + last.substring(0, i+1);
                                }
                            }
                            return first + '...' + last;
                        },
                        match: function (test) {
                            if (test.length < splitLength) {
                                return false;
                            }
                            return test.substring(0, splitLength).toLowerCase() === letters.substring(0, splitLength).toLowerCase();
                        },
                        items: [],
                        click: function () {
                            opts.click(bucket);
                        }
                    }
                    letterBucket.push(bucket);
                }
                bucket.items.push(item);
                if (bucket.items.length > opts.itemsPerLetterBucket) {
                    var splitBuckets = split(bucket, splitLength + 1);
                    var i = letterBucket.indexOf(bucket);
                    letterBucket.splice.apply(letterBucket, [i, 1].concat(splitBuckets));
                }
            }

            var sortedItems = items.slice().sort(function (a, b) {
                var aName = opts.getName(a);
                var bName = opts.getName(b);
                return aName.localeCompare(bName);
            });
            var root = [];
            sortedItems.forEach(function (item) {
                pushItem(item, root, 1);
            });
            root.forEach(function (bucket) {
                bucket.letters = bucket.calcLetters();
            });
            if (opts.sortBucket) {
                root.forEach(function (bucket) {
                    bucket.items.sort(opts.sortBucket);
                });
            }
            return root;
        }
    }
});

questionsModule.directive('questionClaimHierarchyBucketTasks', ['bworkflowApi', 'sharedScope', 'languageTranslate', 'phoneBookMaker', function (bworkflowApi, sharedScope, languageTranslate, phoneBookMaker) {
    return $.extend({}, questionDirectiveBase, {
        templateUrl: 'question_claim_hierarchy_bucket_tasks.html',
        link: function (scope, elt, attrs) {
            questionDirectiveBase.link(scope, undefined, bworkflowApi, languageTranslate);

            function scrollToTop() {
                elt[0].scrollIntoView(true);//{ behavior: 'smooth', block: 'start' });
                // hack - since the page header actually sits _on top_ of the scrollable body it means the above scrollIntoView has positioned our element _underneath_ the header
                // so adjust slightly
                window.scrollBy(0, -50);
            }

            scope.sharedScope = sharedScope.shared;

            scope.uistack = [];
            next('start');
            scope.buckets = [];
            scope.bucket = null;
            scope.roster = null;
            scope.isvisible = false;
            scope.providingRosters = null;
            scope.receivingRosters = null;
            scope.rosterUsers = null;

            var rootObj = scope.presented.template.listento;
            scope.presented.HidePrompt = scope.presented.template.usecompactui;
            scope.maximumUsersPerLetterBucket = scope.presented.template.maximumusersperletterbucket || 20;

            function sortRoster(a, b) {
                return a.alldata.RosterSortOrder - b.alldata.RosterSortOrder;
            }

            function sortBucket(a, b) {
                var aTeamData = scope.teamHierarchyFeed.data.find(function (d) { return d.alldata.Id === a.id; });
                var bTeamData = scope.teamHierarchyFeed.data.find(function (d) { return d.alldata.Id === b.id; });
                return aTeamData.alldata.BucketSortOrder - bTeamData.alldata.BucketSortOrder;
            }

            // Phonebook logic
            var userPhoneBook = phoneBookMaker({
                itemsPerLetterBucket: scope.maximumUsersPerLetterBucket,
                getName: function (item) {
                    return item.alldata.Name;
                },
                click: function (firstLetter) {
                    scope.rosterUsers = firstLetter.items;
                }
            })

            var providingRosterPhoneBook = phoneBookMaker({
                itemsPerLetterBucket: scope.maximumUsersPerLetterBucket,
                getName: function (item) {
                    return item.alldata.RosterName;
                },
                click: function (firstLetter) {
                    scope.providingRosters = firstLetter.items;
                },
                sortBucket: sortRoster
            })

            var bucketsPhoneBook = phoneBookMaker({
                itemsPerLetterBucket: scope.maximumUsersPerLetterBucket,
                getName: function (item) {
                    return item.name;
                },
                click: function (firstLetter) {
                    scope.rosterBuckets = firstLetter.items;
                },
                sortBucket: sortBucket
            })

            var receivingRosterPhoneBook = phoneBookMaker({
                itemsPerLetterBucket: scope.maximumUsersPerLetterBucket,
                getName: function (item) {
                    return item.alldata.RosterName;
                },
                click: function (firstLetter) {
                    scope.receivingRosters = firstLetter.items;
                },
                sortBucket: sortRoster
            })

            scope.teamHierarchyFeedTemplate = {
                name: 'teamhierarchy',
                feed: 'TeamHierarchys',
                filter: "",
                orderbyfields: 'BucketSortOrder,Name',
                idfields: ['Id'],
                itemsperpage: null
            };

            scope.teamHierarchyFeed = bworkflowApi.createDataFeed(scope.teamHierarchyFeedTemplate, scope);

            function getTeamHierarchy() {
                return scope.teamHierarchyFeed.getData(false).then(function (data) {
                    var allRosters = data.unique(function (r) {
                        return r.alldata.RosterId;
                    });
                    var teamData = {
                        data: data,
                        providingRosters: allRosters.filter(function (r) {
                            return r.alldata.ProvidesClaimableDutyList;
                        }),

                        receivingRosters: allRosters.filter(function (r) {
                            return r.alldata.ReceivesClaimableDutyList;
                        })
                    }
                    scope.providingRosters = teamData.providingRosters;
                    scope.receivingRosters = teamData.receivingRosters;

                    return teamData;
                });
            }

            if (rootObj != null) {
                var i = rootObj.indexOf('.');
                if (i != -1)
                    rootObj = rootObj.first(i);

                scope.$watch('sharedScope.' + rootObj, function (newValue, oldValue) {
                    if (newValue == null) {
                        return;
                    }

                    if (newValue.tasks.length > 0 && scope.presented.template.hidewhentaskspresent == true) {
                        // there are tasks so nothing for us to do
                        return;
                    }

                    scope.isvisible = true;

                    // so there are zero tasks, so we need to swing into action and give the user a choice
                    // of buckets to copy the tasks from
                    scope.showHideTaskList(false);

                    scope.getClaimableTasks();
                });
            }

            scope.getClaimableTasks = function () {
                bworkflowApi.execute('TaskUtilities', 'GetClaimableTasks', {})
                    .then(function (data) {
                        scope.buckets = data.buckets;

                        if (data.buckets.length == 0) {
                            // nothing to claim, so let's just show the task list
                            scope.showHideTaskList(true);
                        }
                    }, function (tasks) {

                    });
            };

            function next(state) {
                if (scope.ui) {
                    scope.uistack.push(scope.ui);
                }
                scope.ui = state;
                scrollToTop();
            }

            scope.back = function () {
                if (scope.uistack.length) {
                    scope.ui = scope.uistack.pop();
                    scrollToTop();
                }
            }

            scope.showProvidingRosters = function () {
                getTeamHierarchy().then(function (teamData) {
                    var populatedRosters = teamData.providingRosters.filter(function (r) {
                        return scope.buckets.find(function (b) {
                            return b.rosterId === r.alldata.RosterId;
                        }) != null;
                    });
                    scope.providingRostersFirstLetter = providingRosterPhoneBook(populatedRosters);
                    scope.providingRosters = scope.providingRostersFirstLetter.length === 1 ? scope.providingRostersFirstLetter[0].items : null;
                });
                next('showprovidingrosters');
            }

            scope.selectProvidingRoster = function (roster) {
                // Grab all the buckets for the selected roster
                var rosterBuckets = scope.buckets.filter(function (b) {
                    return b.rosterId == roster.alldata.RosterId;
                });
                scope.bucketsFirstLetter = bucketsPhoneBook(rosterBuckets);
                scope.rosterBuckets = scope.bucketsFirstLetter.length === 1 ? scope.bucketsFirstLetter[0].items : null;
                next('showbuckets');
            }

            scope.selectRosterBucket = function (bucket) {
                scope.bucket = bucket;
                next('confirmbucket');
            };

            scope.assignBucket = function (bucket) {
                scope.bucket = bucket;
                getTeamHierarchy().then(function (teamData) {
                    scope.receivingRostersFirstLetter = receivingRosterPhoneBook(teamData.receivingRosters);
                    scope.receivingRosters = scope.receivingRostersFirstLetter.length === 1 ? scope.receivingRostersFirstLetter[0].items : null;
                });
                next('showreceivingrosters');
            };

            scope.selectReceivingRoster = function (roster) {
                var usersInRoster = scope.teamHierarchyFeed.data.filter(function (r) {
                    return r.alldata.RosterId === roster.alldata.RosterId && r.alldata.UserId != null;
                });

                scope.rosterUsersFirstLetter = userPhoneBook(usersInRoster);
                scope.rosterUsers = scope.rosterUsersFirstLetter.length === 1 ? scope.rosterUsersFirstLetter[0].items : null;
                scope.roster = roster;
                next('selectrosteruser');
            };

            scope.showHideTaskList = function (show) {
                if (scope.presented.template.hideshowtasklist == true) {
                    scope.$emit('player_broadcast', {
                        name: 'question-task-list.visiblechange',
                        data: { name: scope.presented.template.listento, visible: show }
                    });
                }
            };

            scope.claimBucket = function (bucket, userid, rosterid) {
                var params = { tasks: bucket.tasks };

                if (angular.isDefined(userid)) {
                    params.assignto = userid;
                    params.assigntorosterid = rosterid;
                }

                next('assigning');

                bworkflowApi.execute('TaskUtilities', 'Reassign', params)
                    .then(function (data) {
                        scope.bucket = null;
                        scope.uistack = [];
                        next('start');

                        scope.isvisible = false;

                        scope.$emit('player_broadcast', {
                            name: 'question-task-list.refreshTasks',
                            data: { name: scope.presented.template.listento, forceonline: true }
                        });

                        scope.showHideTaskList(true);

                        scope.getClaimableTasks();

                    }, function (tasks) {

                    });
            };
        }
    });
}]);

questionsModule.directive('questionPresentation', ['bworkflowApi', '$sce', 'languageTranslate', function (bworkflowApi, $sce, languageTranslate) {
    return $.extend({}, questionDirectiveBase, {
        templateUrl: 'question_presentation.html',
        link: function (scope, elt, attrs) {
            scope.answer = scope.presented.Answer;

            questionDirectiveBase.link(scope, undefined, bworkflowApi, languageTranslate);

            // a list of objects with information about each page
            // this information is calculated as a media, for example a word document
            // can have multiple pages. So this array is all pages that will be presented
            // as a part of the presentation of a module
            scope.pages = [];
            scope.media = [];
            scope.pagenumber = 0;
            scope.previouspage = null;
            scope.currentpage = null;
            scope.modulejson = null;

            scope.forwards = function () {
                if (scope.pagenumber > scope.pages.length) {
                    return;
                }

                scope.previouspage = scope.currentpage;

                scope.pagenumber = scope.pagenumber + 1;
                scope.currentpage = scope.pages[scope.pagenumber - 1];
                scope.notifyPageView(true);
            };

            scope.backwards = function () {
                if (scope.pagenumber < 0) {
                    return;
                }

                scope.previouspage = scope.currentpage;

                scope.pagenumber = scope.pagenumber - 1;
                scope.currentpage = scope.pages[scope.pagenumber - 1];
                scope.notifyPageView(false);
            };

            scope.notifyPageView = function (forwards) {
                if (angular.isDefined(scope.currentpage.id) == false) {
                    // it doesn't map to something in the media library, so can't track
                    return;
                }

                if (scope.presented.template.trackviewing == false) {
                    return;
                }

                if (scope.previouspage != null && scope.currentpage.__tracking != scope.previouspage.__tracking) {
                    // we've moved between media, so the current page view isn't relevant
                    scope.currentpageview = null;
                }

                var parameters = {
                    userid: scope.presented.userid,
                    workingdocumentid: scope.presented.workingdocumentid,
                    mediaid: scope.currentpage.id,
                    previousmediaid: scope.previouspage == null ? null : scope.previouspage.id,
                    currentpageview: scope.currentpageview,
                    nextpage: scope.currentpage.index
                };

                bworkflowApi.execute('MediaViewing', 'Notify', parameters)
                    .then(function (data) {
                        scope.currentpageview = data.currentpageview;
                    }, function (tasks) {

                    });
            };

            scope.selectModuleJson = function () {
                if (angular.isDefined(scope.presented.template.module) == false ||
                    scope.presented.template.module == null ||
                    scope.presented.template.module == '') {
                    scope.modulejson = scope.selectFirstModule();

                    return;
                }

                angular.forEach(scope.presented.template.manifestjson, function (module) {
                    if (module.name == scope.presented.template.module) {
                        scope.modulejson = module;
                    }
                });

                if (scope.modulejson == null) {
                    // nothing found, so go with the first
                    scope.modulejson = scope.selectFirstModule();
                }
            };

            scope.selectFirstModule = function () {
                return scope.presented.template.manifestjson[0];
            };

            scope.buildPages = function (module) {
                // first we need to build up a list of all of the refernces
                // that are possibly multi page docs, so we can request from the server
                // how long each one is
                scope.media = [];
                scope.pages = [];
                scope.pagenumner = 0;
                var multipagedocs = [];
                var id = 0;
                var j = 0;
                angular.forEach(module.chapters, function (chapter) {
                    angular.forEach(chapter.media, function (media) {
                        media.__tracking = j; // this allows us to track which pages belong to the same media
                        j++;
                        if (media.type.toLowerCase() != "word") {
                            media.pages = 1;
                            scope.media.push(media);
                            return;
                        }

                        // we use this id to match a return object from the
                        // server in a simple fashion later on
                        media.__id = id;
                        scope.media.push(media);
                        multipagedocs.push(media);
                        id++;
                    });
                });

                bworkflowApi.execute('Presentation', 'GetPageCounts', { media: multipagedocs })
                    .then(function (data) {

                        // the data that comes back is the same as what we sent down + a pages property
                        angular.forEach(data.media, function (d) {
                            var match = null;
                            angular.forEach(scope.media, function (media) {
                                if (angular.isDefined(media.__id) == false) {
                                    return;
                                }

                                if (media.__id == d.__id) {
                                    match = media;
                                }
                            });

                            if (match != null) {
                                match.id = d.id; // the server might also add the id of the media if it's referenced in the manifest through the name rather than the id
                                match.name = d.name; // same above but for name
                                match.filename = d.filename; // same as above but for filename
                                match.pages = d.pages;
                            }
                        });

                        // ok so now all our media have page counts, we can now use this to build up
                        // a script of pages
                        angular.forEach(scope.media, function (media) {
                            for (var i = 0; i < media.pages; i++) {
                                var page = angular.copy(media);
                                page.index = i + 1;

                                if (angular.isDefined(page.overrides) == true) {
                                    var over = null;
                                    angular.forEach(page.overrides, function (o) {
                                        if (page.index == o.page) {
                                            angular.forEach(o.media, function (v,k) {
                                                page[k] = v;
                                            });
                                        }
                                    });
                                }

                                if (page.type.toLowerCase() == 'word') {
                                    page.url = $sce.trustAsResourceUrl(bworkflowApi.getfullurl("~/MediaPreviewImage/" + page.id + ".png?scale=1&pageindex=" + i));
                                }
                                else if (page.type.toLowerCase() == 'image' && angular.isDefined(page.id) == true)
                                {
                                    page.url = $sce.trustAsResourceUrl(bworkflowApi.getfullurl("~/MediaImage/" + page.id + ".png?scale=1&pageindex=" + i));
                                }
                                else {
                                    // at the moment any other type requires a url property
                                    page.url = $sce.trustAsResourceUrl(page.url);
                                }

                                scope.pages.push(page);
                            }
                        });

                        scope.forwards();
                    }, function (tasks) {

                    });
            };

            scope.getAnswerValue = function () {
                return scope.media;
            };

            scope.$on('populateAnswer', function (ev) {
                scope.answer.Data = scope.media; 
            });

            scope.mandatoryvalidator = function (question, value, v, stage) {
                if (scope.pages.length != scope.pagenumber) {
                    return { passed: false, message: v.message };
                }

                return { passed: true };
            };

            scope.selectModuleJson();
            scope.buildPages(scope.modulejson);
        }
    });
}]);

