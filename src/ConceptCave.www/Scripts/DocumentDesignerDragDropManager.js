﻿(function ($) {
    var methods = {
        init: function (selector, options) {
            options.start = function (event, ui) {
                // we need to store the parent the item was originally under
                ui.item.data('originalParent', ui.item.parent());

                if (options.moveStarting == undefined) {
                    return;
                }

                var data = {
                    originalParent: ui.item.data('originalParent'),
                    item: ui.item
                };

                options.moveStarting(data);
            }

            options.stop = function (event, ui) {
                var data = {
                    originalParent: ui.item.data('originalParent'),
                    newParent: ui.item.parent(),
                    item: ui.item,
                    newIndex: ui.item.index(),
                    cancel: false
                };

                options.moveFinished(data);

                if (data.cancel) {
                    $(selector).sortable('cancel');
                }
            }

            $(selector).sortable(options);

            return this;
        }
    };

    $.fn.documentdesignerdragdropmanager = function (method) {

        var args = arguments[0] || {}; // It's your object of arguments

        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.tooltip');
        }
    }
})(jQuery);