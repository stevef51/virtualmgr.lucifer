﻿(function (d3) {
    /*
    This is the hierarchy tree designer for bworkflow.
    Responsibilities of this file:
        -Draw the svg nodes of a supplied tree
        -Redraw the svg nodes when the data is updated
        -Manage the events for supplied drag targets, and fire callbacks when a successful drag happens
        -Keep track of currently selected node
        -Provide api for zooming/paning svg canvas
        -Draw buttons for these operations
    It's not the job of this file to:
        -Draw the drag targets
        -Draw other controls outside the svg canvas (e.g duplicate, delete buttons)

    Usage:
        var treeManager = new HierarchyTreeManager({
                                                    option: value
                                                    ...
                                                    });
        treeManager.callback = function(){...};
        treeManager.data = {...};
        treeManager.update();
    */

    //Public interface
    window.HierarchyTreeManager = function (theseOptions) {
        //First, merge options with defaults
        this.options = cloneDefaultOptions();
        for (var key in theseOptions) {
            this.options[key] = theseOptions[key];
        }
        //some options are mandatory
        if (!(theseOptions.svgCanvas && theseOptions.svgCanvas.tagName &&
            theseOptions.svgCanvas.tagName.toLowerCase() == 'svg'))
            throw new Error('SVG DOM element must be supplied');
        //If available in the options, set some of this properties
        if (typeof (theseOptions.treeData) !== 'undefined')
            this.treeData = theseOptions.treeData;
        if (typeof (theseOptions.onDragDrop) !== 'undefined')
            this.onDragDrop = theseOptions.onDragDrop;
        this.svgCanvas = theseOptions.svgCanvas;

        //OK, options good. Let's draw something
        this.prepareSVGCanvas();
        if (typeof (this.treeData) === 'undefined')
            this.updateViewFirstEmpty();
        else
            this.updateViewFirstData();
    };

    //API functions
    HierarchyTreeManager.prototype.updateView = function () {
        if (typeof (this.treeData) === 'undefined')
            this.updateViewFirstEmpty();
        else if (typeof (this.treeData.__nodedata) === 'undefined')
            //I don't think this has been drawn before
            this.updateViewFirstData();
        else
            this.updateViewData();
    };

    //Callbacks that should be attached to
    HierarchyTreeManager.prototype.onDragDrop = function () { };

    //Public data
    HierarchyTreeManager.prototype.treeData = undefined;
    HierarchyTreeManager.prototype.options = {};

    //"Private" functions
    HierarchyTreeManager.prototype.prepareSVGCanvas = function () {
        //This function is called on object construction, before any SVG DOM manipulation
        //It sanitises the <svg> element by dumping anything inside and adding a single <g>
        //this g is set to this.vis
        while (this.svgCanvas.lastChild)
            this.svgCanvas.removeChild(this.svgCanvas.lastChild);
        //SVG structure: one g that surrounds everything (for zoom/pan) + fills window
        //one rectangle that fills svg (to handle zoom/pan events)
        //another g.container in this g to store all actual drawing
        this.zoomGroup = d3.select(this.svgCanvas).append('g');
        this.vis = this.zoomGroup.append('g').classed('container', true);
    };

    HierarchyTreeManager.prototype.prepareZoomEvents = function () {
        //This method wires up the d3 zoom behaviour and the event handlers to support panning and zooming
        //Preconditions: Data has already been rendered, this.treeLayoutActualBounds is set
        var thisObj = this; //need to save this context
        //This selector puts the rect in the enclosing g element
        this.zoomRect = this.zoomGroup.insert('rect', ':first-child')
                        .attr('opacity', 0)
                        .attr('x', 0).attr('y', 0)
                        .attr('width', this.getWindowSize().width)
                        .attr('height', this.getWindowSize().height);
        //calculate minimum scale such that entire tree is in window
        var winSize = this.getWindowSize();
        var winWidth = winSize.width;
        var winHeight = winSize.height;
        this.zoomBehaviour = d3.behavior.zoom()
                                .on('zoom', function () {
                                    thisObj.retransformSVGCanvas(d3.event.scale, d3.event.translate);
                                });
        this.zoomGroup.call(this.zoomBehaviour);
        //Update zoom bounds for the first time
        this.updateZoomBoundaries();
    };

    HierarchyTreeManager.prototype.retransformSVGCanvas = function (scale, translation) {
        //This method performs transforms on the <svg> element in response to some kind of zooming
        //We gotta change the scaling/translating on the enclosing g
        this.vis.attr('transform', 'translate(' + translation + ')'
            + 'scale(' + scale + ')');
    };

    HierarchyTreeManager.prototype.updateZoomBoundaries = function () {
        var treeSize = this.calculateLayoutActualSize();
        var winSize = this.getWindowSize();
        var winWidth = winSize.width;
        var winHeight = winSize.height;
        var minZoom = Math.min(winWidth / treeSize.width, winHeight / treeSize.height, 1); //cap it at 1
        this.zoomBehaviour.scaleExtent([minZoom, 1]);
        this.zoomRect.call(this.zoomBehaviour);
    };

    HierarchyTreeManager.prototype.updateViewEmpty = function () { };
    HierarchyTreeManager.prototype.updateViewData = function () {
        //This method updates the tree
        //precondition: should be called after changing this.treeData

        //ensure Ids on everything
        this.validatePrivateNodedata();
        this.calculateLayoutSize();

        //need to regen layout; is different size
        this.treeLayout = d3.layout.tree()
                                    .size([this.treeLayoutWidth, this.treeLayoutHeight])
                                    .separation(function (a, b) {
                                        return 6 * (a.parent == b.parent ? 1 : 2)
                                    });
        //Now prepare initial nodes/links
        this.treeLayoutNodes = this.treeLayout(this.treeData);
        this.treeLayoutLinks = this.treeLayout.links(this.treeLayoutNodes);

        var newLinks = this.vis.selectAll(this.treeLinkSelector())
                                .data(this.treeLayoutLinks, this.treeLinkIndexer);
        var newNodes = this.vis.selectAll(this.treeNodeSelector())
                                .data(this.treeLayoutNodes, this.treeNodeIndexer);

        //Make new links
        //Prepend them, don't append them
        var thisO = this;
        newLinks.enter().insert('path', ':first-child')
                        .attr('id', this.treeLinkIndexer)
                        .attr('class', this.options.linkClass)
                        .attr('d', function (d) {
                            var origin = { x: d.source.__nodedata.x0, y: d.source.__nodedata.y0 };
                            return thisO.diagonal({ source: origin, target: origin })
                        })
                        .transition().duration(this.options.animationDuration)
                        .attr('d', this.diagonal);

        newLinks.transition().duration(this.options.animationDuration)
                .attr('d', this.diagonal);

        //make new nodes at parent position
        newNodes.enter().append('g')
                        .attr('id', this.treeNodeIndexer)
                        .attr('class', this.options.nodeClass)
                        .attr('transform', function (d) {
                            var tx = d.parent.__nodedata.x0;
                            var ty = d.parent.__nodedata.y0;
                            return 'translate(' + [tx, ty] + ')';
                        })
                        .transition().duration(this.options.animationDuration)
                        .attr('transform', function (d) {
                            d.__nodedata.x0 = d.x;
                            d.__nodedata.y0 = d.y;
                            return 'translate(' + d.x + ', ' + d.y + ')';
                        });


        //Update existing nodes
        newNodes.transition().duration(this.options.animationDuration)
                        .attr('transform', function (d) {
                            d.__nodedata.x0 = d.x;
                            d.__nodedata.y0 = d.y;
                            return 'translate(' + d.x + ', ' + d.y + ')';
                        });

        //delete nodes
        //TODO: Deletion vs hiding once collapsing is implemented
        newNodes.exit().transition().duration(this.options.animationDuration)
                        .style('opacity', 1e-6)
                        .remove();
        newLinks.exit().transition().duration(this.options.animationDuration)
                        .style('opacity', 1e-6)
                        .remove();

        this.linkPathSelector = newLinks;
        this.nodeGroupSelector = newNodes;

        //Actually draw the info on the nodes
        this.drawNodeElements();

        //Adjust the zoom once the animation is finished
        var thisO = this;
        window.setTimeout(function () { thisO.updateZoomBoundaries(); }, this.options.animationDuration);
    };
    HierarchyTreeManager.prototype.updateViewFirstData = function () {
        //This method does the initial display of the tree data
        //precondition: this.treeData is defined; this.prepareSVGCanvas() has been called

        //first, ensure we have Ids on everything, and prepare for drawing
        this.prepareSVGCanvas();
        this.validatePrivateNodedata();
        this.calculateLayoutSize();

        var wSize = this.getWindowSize();
        var w = 2 * wSize.width;
        var h = 2 * wSize.height;
        
        var padding = this.options.layoutPadding;
        this.treeLayout = d3.layout.tree()
                                    .size([this.treeLayoutWidth, this.treeLayoutHeight])
                                    .separation(function(a,b){
                                        return 6*(a.parent == b.parent ? 1 : 2)
                                    });
        this.diagonal = d3.svg.diagonal();
        //Now prepare initial nodes/links
        this.treeLayoutNodes = this.treeLayout(this.treeData);
        this.treeLayoutLinks = this.treeLayout.links(this.treeLayoutNodes);
        //links
        this.linkPathSelector = this.vis.selectAll(this.treeLinkSelector())
                                        .data(this.treeLayoutLinks, this.treeLinkIndexer)
                                        .enter()
                                        .append('path')
                                        .attr('id', this.treeLinkIndexer)
                                        .attr('class', this.options.linkClass)
                                        .attr('d', this.diagonal);
        //nodes:
        var thiso = this;
        this.nodeGroupSelector = this.vis.selectAll(this.treeNodeSelector())
                                        .data(this.treeLayoutNodes, this.treeNodeIndexer)
                                        .enter()
                                        .append('g')
                                        .attr('class', this.options.nodeClass)
                                        .attr('id', this.treeNodeIndexer)
                                        .attr('transform', function (d) {
                                            d.__nodedata.x0 = d.x;
                                            d.__nodedata.y0 = d.y;
                                            return 'translate(' + d.x + ', ' + d.y + ')';
                                        });
        //Actually draw the info on the nodes
        this.drawNodeElements();

        //Now we can do post-draw operations
        this.prepareZoomEvents();
        this.displayDefaultViewport();
    };
    HierarchyTreeManager.prototype.updateViewFirstEmpty = function () {
        //This method is called if there is nothing in the view to update to draw a nice UI saying, "drag me!"

        this.prepareSVGCanvas();
        //draw a rect with a green dashed border
        var winSize = this.getWindowSize();
        var rect = this.vis.append('rect')
                            .attr('x', 10).attr('y', 10)
                            .attr('width', winSize.width - 20).attr('height', winSize.height - 20)
                            .attr('fill', 'rgb(200, 200, 200)')
                            .attr('fill-opacity', 0.15)
                            .attr('stroke', 'rgb(0, 230, 0)')
                            .attr('stroke-width', 10)
                            .attr('stroke-dasharray', '40, 40');
        var tblock = this.vis.append('text')
                            .attr('text-anchor', 'middle')
                            .attr('x', winSize.width / 2).attr('y', winSize.height / 2)
                            .style('font-size', '18pt');
        var line1 = tblock.append('tspan')
                            .attr('text-anchor', 'middle')
                            .attr('x', winSize.width / 2).attr('y', winSize.height / 2)
                            .text('Drag a role from the right onto here');
        var line2 = tblock.append('tspan')
                            .attr('dy', '1.2em')
                            .attr('x', winSize.width / 2).attr('y', winSize.height / 2)
                            .attr('text-anchor', 'middle')
                            .text('to start building your hierarchy');

        //need to wire up a temporary
    };

    HierarchyTreeManager.prototype.validatePrivateNodedata = function () {
        //This method is called to ensure that every node in this.treeData has a __nodedata
        //element, used to store, almongst other things, a unique ID serving as the key for the
        //d3 data join.

        if (!this.treeData) return;
        //walk tree, ensure __nodedata.id is set; if not, make it a new Guid()
        (function thisfunc(node) {
            if (!node.__nodedata)
                node.__nodedata = { id: 'GUID-' +  Guid() };
            else if (!node.__nodedata.id)
                node.__nodedata.id = 'GUID-' + Guid();
            if (node.children) {
                for (var i = 0; i < node.children.length; i++)
                    thisfunc(node.children[i]);
            }
        })(this.treeData);
    };

    HierarchyTreeManager.prototype.drawNodeElements = function () {
        var thisO = this;
        this.nodeGroupSelector.each(function (d, i) {
            //dump anything already in here
            while (this.firstChild)
                this.removeChild(this.firstChild);
            //wire up event handler here, so it gets called with both first and normal render
            d3.select(this).on('click', function () {
                d3.selectAll(thisO.treeNodeSelector()).classed(thisO.options.nodeSelectedClass, false);
                d3.select(this).classed(thisO.options.nodeSelectedClass, true);
                thisO.options.onNodeClick(this);
            }, true);
            d3.select(this).append('text')
                            .attr('x', 0)
                            .attr('y', -thisO.options.nodeRectHeight / 2 + thisO.options.nodeRectHeight * 0.3)
                            .attr('text-anchor', 'middle')
                            .each(function () {
                                //Use textflow library to wrap text
                                ddy = textFlow(d.name, this, thisO.options.nodeRectWidth * 0.8, 0, '1.2em', false);
                            });
            var textSize = d3.select(this).select('text')[0][0].getBBox();
            d3.select(this).append('rect')
                              .classed(thisO.options.mainRectClass, true)
                              .attr('id', 'GUID-' + Guid())
                              .attr('x', -thisO.options.nodeRectWidth / 2)
                              .attr('y', -(thisO.options.nodeRectHeight) / 2)
                              .attr('width', thisO.options.nodeRectWidth)
                              .attr('height', thisO.options.nodeRectHeight + textSize.height)
                              .attr('fill', thisO.options.nodeRectFillColour)
                              .attr('rx', thisO.options.cornerRadius)
                              .attr('ry', thisO.options.cornerRadius);
            //We had to render the text first to know how big the rect needed to be
            //But now that this is done, we need to take the text and put it AFTER the rect so that it's actually visible
            var textEl = d3.select(this).select('text').remove()[0][0];
            this.appendChild(textEl);
            //Dotted line to represent drop area
            d3.select(this).append('rect')
                            .classed(thisO.options.userContainerRectClass, true)
                            .attr('x', -thisO.options.nodeRectWidth / 2 + 0.1 * thisO.options.nodeRectWidth)
                            .attr('y', textSize.y + textSize.height + 10)
                            .attr('width', thisO.options.nodeRectWidth * 0.8)
                            .attr('height', thisO.options.nodeRectHeight * 0.6)
                            .attr('stroke-dasharray', '5, 5')
                            .attr('stroke', 'black')
                            .attr('fill', 'white')
                            .attr('fill-opacity', '0.4');
            //And now we diverge. If we have a user, we should put a rect for them in here. otherwise, we should tell them to put a user in here
            if (typeof (d.user) === 'undefined' || d.user == null) {
                //draw a plus
                var tx = 0;
                var ty = textSize.y + textSize.height + 10 + 0.3 * thisO.options.nodeRectHeight;
                var s = (0.6*thisO.options.nodeRectHeight)*0.05*0.7; //s such that (20px)*s == 0.7*userContainerRect height

                d3.select(this).append('path')
                            .classed(thisO.options.noUserPathClass, true)
                            .attr('d', 'M0 -10 v20 m-10 -10 h20')
                            .attr('stroke-width', 5)
                            .attr('stroke', 'rgb(0, 230, 0)')
                            .attr('fill', 'none')
                            .attr('transform', 'translate(' + [tx, ty] + ')scale(' + s + ')');
            } else {
                //draw another rect representing the user
                d3.select(this).append('rect')
                            .classed(thisO.options.userRectClass, true)
                            .attr('x', -thisO.options.nodeRectWidth / 2 + 0.1 * thisO.options.nodeRectWidth + 5)
                            .attr('y', textSize.y + textSize.height + 10 + 5)
                            .attr('width', thisO.options.nodeRectWidth * 0.8 - 10)
                            .attr('height', thisO.options.nodeRectHeight * 0.6 - 10)
                            .attr('fill', 'rgb(238, 238, 224)')
                            .attr('rx', thisO.options.cornerRadius / 3)
                            .attr('ry', thisO.options.cornerRadius / 3);
                //And a namer
                var uText = d3.select(this).append('text')
                            .attr('x', -thisO.options.nodeRectWidth / 2 + 0.1 * thisO.options.nodeRectWidth + 10)
                            .attr('y', textSize.y + textSize.height + 10 + 20)
                            .attr('text-anchor', 'left');
                textFlow(d.user.name, uText[0][0], thisO.options.nodeRectWidth * 0.8 - 10 - 10, 
                    -thisO.options.nodeRectWidth / 2 + 0.1 * thisO.options.nodeRectWidth + 10, '1.2em', false);
            }
            //am I primary? If not, apply the not-primary class to the line above me
            if (typeof (d.parent) !== 'undefined') {
                var parentLinkId = d.parent.__nodedata.id + '-' + d.__nodedata.id;
                if (d.isPrimary == null || d.isPrimary) {
                    d3.select('#' + parentLinkId).classed(thisO.options.nonPrimaryLinkClass, false);
                }
                else {
                    d3.select('#' + parentLinkId).classed(thisO.options.nonPrimaryLinkClass, true);
                }
            }
        });
    };

    HierarchyTreeManager.prototype.displayDefaultViewport = function () {
        //The transformation is excuted translate first
        //this means we can move the middle of the g to the middle of the svg, then scale it to min allowable scale
        var winSize = this.getWindowSize();
        var scaleFactor = this.zoomBehaviour.scaleExtent()[0];

        //We want to move the tree to the left so that it is actually centered on the origin
        var gsize = this.calculateLayoutActualSize();
        //var transX = (-this.treeLayoutActualBounds.left + this.options.nodeRectWidth / 2)*scaleFactor;
        //var transY = (-this.treeLayoutActualBounds.top + this.options.nodeRectHeight / 2)*scaleFactor;
        var transX = -gsize.x * scaleFactor;
        var transY = -gsize.y * scaleFactor;
        //If the tree is taller than it is wide, we should use our translation to center it
        if (gsize.x < gsize.y) {
            var gcentre = (gsize.x + gsize.width / 2) * scaleFactor;
            transX += gcentre;
        }
        this.zoomBehaviour.scale(scaleFactor);
        this.zoomBehaviour.translate([transX, transY]);
        this.retransformSVGCanvas(scaleFactor, [transX, transY]);
    };

    HierarchyTreeManager.prototype.treeLinkSelector = function () {
        return 'path.' + this.options.linkClass;
    };

    HierarchyTreeManager.prototype.treeNodeSelector = function () {
        return 'g.' + this.options.nodeClass;
    };

    HierarchyTreeManager.prototype.treeLinkIndexer = function (d) {
        //This function accepts a link object (comes from d3 bound data) and returns its id
        return d.source.__nodedata.id + '-' + d.target.__nodedata.id;
    };

    HierarchyTreeManager.prototype.treeNodeIndexer = function (d) {
        //This function accepts a node object (comes from d3 bound data) and returns its id
        return d.__nodedata.id;
    };

    HierarchyTreeManager.prototype.calculateLayoutSize = function () {
        //We are making width proportional to the number of nodes in the level with the most nodes
        //Height is proportional to tree depth
        var levelList = [];
        //recurse tree
        (function thisfunc(node, level) {
            if (typeof (levelList[level]) === 'undefined')
                levelList[level] = 0;
            levelList[level]++;
            if (node.children) {
                for (var i = 0; i < node.children.length; i++)
                    thisfunc(node.children[i], level + 1);
            }
        })(this.treeData, 0);
        //now we know width and height
        var nodesWide = Math.max.apply(null, levelList);
        var nodesTall = levelList.length;
        //so we can apply some formula to determine the width
        this.treeLayoutWidth = 2 * this.options.nodeRectWidth * nodesWide - this.options.layoutPadding/2;
        this.treeLayoutHeight = this.options.layoutLinkHeightFactor * nodesTall - this.options.layoutPadding / 2;
    };

    HierarchyTreeManager.prototype.calculateLayoutActualSize = function () {
        //This method works out the actual, rendered size of the tree layout in unzoomed coordinates
        //Takes into account the size of node rectangles
        return this.vis[0][0].getBBox();
    };

    HierarchyTreeManager.prototype.getWindowSize = function () {
        var w = Math.round(parseFloat(d3.select(this.svgCanvas).style('width')));
        var h = Math.round(parseFloat(d3.select(this.svgCanvas).style('height')));
        return { width: w, height: h };
    };

    //"Private" data
    HierarchyTreeManager.prototype.treeLayout = undefined;
    HierarchyTreeManager.prototype.treeLayoutNodes = undefined;
    HierarchyTreeManager.prototype.treeLayoutLinks = undefined;
    HierarchyTreeManager.prototype.vis = undefined;
    HierarchyTreeManager.prototype.svgCanvas = undefined;
    HierarchyTreeManager.prototype.diagonal = undefined;
    HierarchyTreeManager.prototype.linkPathSelector = undefined;
    HierarchyTreeManager.prototype.nodeGroupSelector = undefined
    HierarchyTreeManager.prototype.treeLayoutWidth = undefined;
    HierarchyTreeManager.prototype.treeLayoutHeight = undefined;
    HierarchyTreeManager.prototype.zoomBehaviour = undefined;
    HierarchyTreeManager.prototype.zoomGroup = undefined;
    HierarchyTreeManager.prototype.zoomRect = undefined;
    HierarchyTreeManager.prototype.treeLayoutActualBounds = undefined;

    //private static data
    var defaultOptions = {
        layoutPadding: 0,
        animationDuration: 750,
        linkClass: 'link',
        nodeClass: 'node',
        nodeTextProp: 'name',
        nodeRectWidth: 173,
        nodeRectHeight: 100,
        nodeRectFillColour: 'red',
        layoutLinkHeightFactor: 163,
        cornerRadius: 15,
        mainRectClass: 'main-rect',
        userContainerRectClass: 'user-container-rect',
        userRectClass: 'user-rect',
        noUserPathClass: 'no-user-path',
        nodeSelectedClass: 'node-selected',
        onNodeClick: function (node) { return; },
        nonPrimaryLinkClass: 'link-nonprimary'
    };

    var cloneDefaultOptions = function () {
        var newOpts = {};
        for (var k in defaultOptions)
            if (defaultOptions.hasOwnProperty(k))
                newOpts[k] = defaultOptions[k];
        return newOpts;
    }

    //private utility methods
    var Guid = function () {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    };

})(d3);

//drag and drop manager for the hierarchy
(function (d3) {
    window.HierarchyDragDropManager = function (theseOptions) {
        //First, merge options with defaults
        this.options = cloneDefaultOptions();
        for (var key in theseOptions) {
            this.options[key] = theseOptions[key];
        }
        //some options are mandatory
        if (!theseOptions.treeManager)
            throw new Error('Tree Manager must be supplied');
        if (this.options.dragTargetSelectors.length == 0)
            throw new Error('Selectors for drag targets must be supplied');
        //If available in the options, set some of this properties
        this.treeManager = this.options.treeManager;
        this.dragTargetSelectors = this.options.dragTargetSelectors;

        //wire d3 selector event handler up to drag target
        var thisO = this;
        this.dragBehaviour = d3.behavior.drag()
                .origin(function (d) {
                    //this is now dom element
                    var rect = this.getBoundingClientRect();
                    return { x: rect.left, y: rect.top };
                })
                .on('dragstart', function (d) { return thisO.dragStartHandler(d, this); })
                .on('drag', function (d) { return thisO.dragHandler(d, this); })
                .on('dragend', function (d) { return thisO.dragEndHandler(d, this); });
        for (var i = 0; i < this.dragTargetSelectors.length; i++)
            d3.selectAll(this.dragTargetSelectors[i]).call(this.dragBehaviour);
    };

    //Event handlers
    HierarchyDragDropManager.prototype.dragStartHandler = function (d, el) {
        //Create ghost
        var clonedNode = el.cloneNode(true);
        clonedNode.id = 'GUID-' + Guid();
        d3.select(el).attr('data-ghostid', clonedNode.id);
        el.insertBefore(clonedNode, el.firstChild);
        var iBox = el.getBoundingClientRect();
        d3.select(clonedNode)
            .style('position', 'fixed')
            .classed('hover-dragme', true)
            .style('opacity', 0.5)
            .style('left', iBox.left + 'px')
            .style('top', iBox.top + 'px');
        //We need to sort the svg elements to know how to highlight
        //Special case handling: should we highlight a rectangle representing "Drag to make first node"?
        if (this.options.shouldHighlightWhenEmpty && this.options.treeManager.vis.selectAll(this.options.treeManager.treeNodeSelector())[0].length == 0) {
            //yes, we should, and are
            this.handleEmptyTree = true;
            this.emptyTreeBBox = this.options.treeManager.vis.select('rect')[0][0].getBoundingClientRect();
        }
        else {
            this.sortedNodePositions = this.getSortedNodeBBoxes();
            this.handleEmptyTree = false;
        }
        
    };

    HierarchyDragDropManager.prototype.dragHandler = function (d, el) {
        var ghostId = d3.select(el).attr('data-ghostid');
        var ghostEl = document.getElementById(ghostId);
        d3.select(ghostEl)
            .style('left', d3.event.x + 'px')
            .style('top', d3.event.y + 'px');
        var mePos = ghostEl.getBoundingClientRect();
        //Special case handling: should we highlight a rectangle representing "Drag to make first node"?
        var nodeToStyle = null;
        if (this.handleEmptyTree == true) {
            //are we colliding this this bound?
            var valInRange = function (val, min, max) {
                return (val >= min && val <= max);
            }
            var xOverlap = valInRange(mePos.left, this.emptyTreeBBox.left, this.emptyTreeBBox.right) ||
                            valInRange(this.emptyTreeBBox.left, mePos.left, mePos.right);
            var yOverlap = valInRange(mePos.top, this.emptyTreeBBox.top, this.emptyTreeBBox.bottom) ||
                            valInRange(this.emptyTreeBBox.top, mePos.top, mePos.bottom);
            if (xOverlap && yOverlap) {
                //does collide
                nodeToStyle = this.options.treeManager.vis; //we actually class the container so that the same > rect selector for styling works
            }
        }
        else {
            nodeToStyle = getMostCollidingNode(mePos, this.sortedNodePositions);
        }
        if (nodeToStyle != null && this.handleEmptyTree) {
            nodeToStyle.classed(this.options.hoverSvgClass, true);
        }
        else if (nodeToStyle != null && this.options.onShouldClass(el, nodeToStyle)) {
            d3.selectAll(this.treeManager.treeNodeSelector() + ':not(#' + nodeToStyle.id + ')')
                .classed(this.options.hoverSvgClass, false);
            d3.select(nodeToStyle).classed(this.options.hoverSvgClass, true);
        }
        else if (this.handleEmptyTree) {
            this.treeManager.vis.classed(this.options.hoverSvgClass, false);
        }
        else {
            d3.selectAll(this.treeManager.treeNodeSelector())
                .classed(this.options.hoverSvgClass, false);
        }
    };

    HierarchyDragDropManager.prototype.dragEndHandler = function (d, el) {
        //save the drop target before we unclass it
        var dropTarget = d3.select('.' + this.options.hoverSvgClass)[0][0];
        d3.selectAll(this.treeManager.treeNodeSelector())
                .classed(this.options.hoverSvgClass, false);
        this.treeManager.vis.classed(this.options.hoverSvgClass, false);
        var ghostId = d3.select(el).attr('data-ghostid');
        d3.select('#' + ghostId).remove();
        //now fire the e handler
        if (dropTarget != null)
            this.options.onDrop(el, dropTarget, this.handleEmptyTree);
    };

    HierarchyDragDropManager.prototype.treeManager = undefined;
    HierarchyDragDropManager.prototype.dragTargetSelectors = undefined;
    HierarchyDragDropManager.prototype.dragBehaviour = undefined;
    HierarchyDragDropManager.prototype.sortedNodePositions = undefined;
    HierarchyDragDropManager.prototype.handleEmptyTree = false;
    HierarchyDragDropManager.prototype.emptyTreeBBox = undefined;

    var defaultOptions = {
        treeManager: undefined,
        dragTargetSelectors: [],
        hoverSvgClass: 'dragdrop-hover',
        ghostClass: 'dragdrop-ghost',
        shouldHighlightWhenEmpty: true,
        onShouldClass: function (source, target) { return true;},
        onDrop: function (source, target, isEmptyData) { return; }
    };
    var cloneDefaultOptions = function () {
        var newOpts = {};
        for (var k in defaultOptions)
            if (defaultOptions.hasOwnProperty(k))
                newOpts[k] = defaultOptions[k];
        return newOpts;
    }

    HierarchyDragDropManager.prototype.getSortedNodeBBoxes = function () {
        var nodes = d3.selectAll(this.treeManager.treeNodeSelector())[0];
        var bboxObjs = nodes.map(function (node) {
            var rect = node.getBoundingClientRect();
            return {
                left: rect.left,
                right: rect.right,
                top: rect.top,
                bottom: rect.bottom,
                node: node
            };
        });

        //now mergesort them into four arrays
        var mergeOnProp = function (arr1, arr2, propName) {
            var ret = [];
            while (arr1.length != 0 && arr2.length != 0) {
                if (arr1[0][propName] < arr2[0][propName])
                    ret.push(arr1.splice(0, 1)[0]); //array.splice deletes the first element and returns it in an array
                else
                    ret.push(arr2.splice(0, 1)[0]);
            }
            ret.push.apply(ret, arr1);
            ret.push.apply(ret, arr2);
            return ret;
        };
        var mergeSortOnProp = function msop(objArr, propName) {
            if (objArr.length <= 1)
                return objArr;
            var pivot = Math.floor(objArr.length / 2);
            var sortedLHS = msop(objArr.slice(0, pivot), propName);
            var sortedRHS = msop(objArr.slice(pivot, objArr.length), propName); //slice includes first but not second arg
            return mergeOnProp(sortedLHS, sortedRHS, propName);
        };

        return {
            left: mergeSortOnProp(bboxObjs, 'left'), right: mergeSortOnProp(bboxObjs, 'right'),
            top: mergeSortOnProp(bboxObjs, 'top'), bottom: mergeSortOnProp(bboxObjs, 'bottom')
        };
    };

    var binarySearchBoundsOnProp = function (arr, propName, min, max) {
        var loInsertionIndex = (function thisfunc(lo, hi) {
            if (hi < lo)
                return lo;
            var i = Math.floor((lo + hi) / 2);
            if (arr[i][propName] >= min)
                return thisfunc(lo, i - 1);
            else
                return thisfunc(i + 1, hi);
        })(0, arr.length - 1);

        if (loInsertionIndex >= arr.length)
            return [];

        var hiInsertionIndex = (function thisfunc(lo, hi) {
            if (hi < lo)
                return lo;
            var i = Math.floor((lo + hi) / 2);
            if (arr[i][propName] > max)
                return thisfunc(lo, i - 1);
            else
                return thisfunc(i + 1, hi);
        })(loInsertionIndex, arr.length - 1);

        if (hiInsertionIndex > arr.length)
            return arr.slice(loInsertionIndex);
        else
            return arr.slice(loInsertionIndex, hiInsertionIndex);
    };

    var getMostCollidingNode = function (mePos, sortedNodePositions) {
        var nodesInRange = {
            left: binarySearchBoundsOnProp(sortedNodePositions.right, 'right', mePos.left, mePos.right),
            right: binarySearchBoundsOnProp(sortedNodePositions.left, 'left', mePos.left, mePos.right),
            top: binarySearchBoundsOnProp(sortedNodePositions.bottom, 'bottom', mePos.top, mePos.bottom),
            bottom: binarySearchBoundsOnProp(sortedNodePositions.top, 'top', mePos.top, mePos.bottom)
        };
        var topBottomFilter = function (el) {
            return nodesInRange.top.indexOf(el) != -1 || nodesInRange.bottom.indexOf(el) != -1;
        };
        var leftRightFilter = function (el) {
            return nodesInRange.left.indexOf(el) != -1 || nodesInRange.right.indexOf(el) != -1;
        };
        var nodesWithCorner = {
            left: nodesInRange.left.filter(topBottomFilter),
            right: nodesInRange.right.filter(topBottomFilter),
            top: nodesInRange.top.filter(leftRightFilter),
            bottom: nodesInRange.bottom.filter(leftRightFilter)
        };
        //The above considers nodes contained within me, or nodes overlapping a corner iwth me
        //we also need to consider nodes that me is contained within
        var nodesInInsideRange = {
            left: binarySearchBoundsOnProp(sortedNodePositions.left, 'left', -2000000, mePos.left),
            right: binarySearchBoundsOnProp(sortedNodePositions.right, 'right', mePos.right, 2000000),
            top: binarySearchBoundsOnProp(sortedNodePositions.top, 'top', -2000000, mePos.top),
            bottom: binarySearchBoundsOnProp(sortedNodePositions.bottom, 'bottom', mePos.bottom, 2000000)
        };
        topBottomFilter = function (el) {
            return nodesInInsideRange.top.indexOf(el) != -1 && nodesInInsideRange.bottom.indexOf(el) != -1
                && el.left <= mePos.right && el.right >= mePos.left;
        };
        leftRightFilter = function (el) {
            return nodesInInsideRange.left.indexOf(el) != -1 && nodesInInsideRange.right.indexOf(el) != -1
                && el.bottom >= mePos.top && el.top <= mePos.bottom;
        };
        nodesWithCorner.left = nodesWithCorner.left.concat(nodesInInsideRange.left.filter(leftRightFilter));
        nodesWithCorner.top = nodesWithCorner.top.concat(nodesInInsideRange.top.filter(topBottomFilter));

        //merge nocollide using dict keys
        var candidateDict = {};
        for (var side in nodesWithCorner) {
            if (!nodesWithCorner.hasOwnProperty(side)) continue;
            for (var noderecti in nodesWithCorner[side]) {
                noderect = nodesWithCorner[side][noderecti];
                candidateDict[noderect.node.id] = noderect;
            }
        }
        //now find the candidate closest to our center
        meCentre = { left: (mePos.right + mePos.left) / 2, top: (mePos.bottom + mePos.top) / 2 };
        var closestNode = undefined;
        var closestDist = undefined;
        for (var key in candidateDict) {
            if (!candidateDict.hasOwnProperty(key)) continue;
            val = candidateDict[key];
            valCentre = { left: (val.right + val.left) / 2, top: (val.bottom + val.top) / 2 };
            valDist = Math.sqrt(Math.pow(meCentre.left - valCentre.left, 2) +
                Math.pow(meCentre.top - valCentre.top, 2));
            if (typeof (closestNode) === 'undefined' || valDist < closestDist) {
                closestNode = val;
                closestDist = valDist;
            }
        }
        //we have the closest node.
        if (typeof (closestNode) === 'undefined')
            return null;
        else return closestNode.node;
    };

    var Guid = function () {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    };
})(d3);