﻿(function ($) {
    var methods = {
        setModel: function (model) {
            $(this).removeData('locked');

            $(this).data('model', model);
        },
        getModel: function () {
            return $(this).data('model');
        },
        addItem: function (item) {
            $(this).data('model').Items.push(item);
        },
        getItem: function (id) {
            var item = null;
            $.each($(this).data('model').Items, function (index, value) {
                if (value.Id == id) {

                    item = value;
                    return false;
                }
            });

            return item;
        }
    };

    $.fn.jsondatamodel = function (method) {
        var args = arguments[0] || {}; // It's your object of arguments

        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.tooltip');
        }
    }
})(jQuery);