﻿/*
  This polyfil is probably incomplete, I made this just to use localforage on internet explorer.
  This polyfill is tested only with this library and works well, but I don't know if it will work i other cases.
  This polyfill is based on this code: https://github.com/nolanlawson/blob-util/blob/4ae6c7a14d3240d977bbdab9ae2f0bb8905ecc37/lib/index.js#L123-L141
*/

if (window.FileReader) {
    if (typeof FileReader.prototype.readAsBinaryString !== 'function') {
        console.log("Warning: Using readAsBinaryString polyfill");
        FileReader.prototype.readAsBinaryString = function (blb) {
            var reader = new FileReader();
            var that = this;
            var conversor = function (e) {
                var toConvert = e.target.result || '';
                var binary = '';
                var bytes = new Uint8Array(toConvert);
                var length = bytes.byteLength;
                var i = -1;
                while (++i < length) {
                    binary += String.fromCharCode(bytes[i]);
                }

                Object.defineProperty(that, 'result', { enumerable: true, configurable: false, writable: false, value: binary });
                Object.defineProperty(that, 'readyState', { enumerable: true, configurable: false, writable: false, value: 2 });
                var f = {};
                for (var property in e) {
                    if (property != "target") {
                        f[property] = e[property];
                    }
                }
                f.target = {};
                for (var property in e.target) {
                    if (property != "result") {
                        f.target[property] = e.target[property];
                    }
                }
                f.target.result = binary;
                that.onloadend.call(that, f);
            };
            if (!(this.onloadend === undefined || this.onloadend === null)) {
                reader.onloadend = conversor;
            };
            if (!(this.onerror === undefined || this.onerror === null)) {
                reader.onerror = this.onerror;
            };
            if (!(this.onabort === undefined || this.onabort === null)) {
                reader.onabort = this.onabort;
            };
            if (!(this.onloadstart === undefined || this.onloadstart === null)) {
                reader.onloadstart = this.onloadstart;
            };
            if (!(this.onprogress === undefined || this.onprogress === null)) {
                reader.onprogress = this.onprogress;
            };
            if (!(this.onload === undefined || this.onload === null)) {
                reader.onload = conversor;
            };
            //abort not implemented !!!
            reader.readAsArrayBuffer(blb);
        }
    }
}
var SquareIT = SquareIT || {

    GetGPSLocation: function (options, resultFn, failFn) {
        navigator.geolocation.getCurrentPosition(resultFn, failFn);
    },

    WatchGPSLocation: function(options, resultFn, failFn) {
        return navigator.geolocation.watchPosition(resultFn, failFn, options);
    },

    ClearWatchGPSLocation: function(id) {
        navigator.geolocation.clearWatch(id);
    },

    AfterInitialise: function (fn) {
        fn();
    },
    
    BWorkflow: {
        HandleUploads: function (options, resultFn, failFn) {
            var files = options.files;
            var resizedFiles = [];

            //callback for when all files are loaded
            var allFilesLoaded = function () {
                //only actually do something once all files are loaded
                if (files.length > resizedFiles.length)
                    return;

                //ok, return this list
                resultFn(resizedFiles);
            };

            //function to resize images
            var resizeImage = function (imobj, file) {
                var resizeCanvas = $('<canvas></canvas>');
                $(resizeCanvas).css('display', 'none');
                $('body').append(resizeCanvas);
                var resizeCanvasDOM = $(resizeCanvas).get(0);

                var clampDims = window.clampDimensions(imobj.width, imobj.height, options.maxWidth, options.maxHeight);
                resizeCanvasDOM.width = clampDims.width;
                resizeCanvasDOM.height = clampDims.height;

                var ctx = resizeCanvasDOM.getContext('2d');
                ctx.drawImage(imobj, 0, 0, clampDims.width, clampDims.height);

                //now save it to an image
                var resizedImage = new Image();
                resizedImage.onload = function () {
                    //after resized image is loaded, add it to resizedFiles
                    $(resizeCanvas).remove();
                    resizedFiles.push({
                        type: 'image',
                        fname: file.name,
                        name: file.name,
                        file: file,
                        data: resizedImage
                    });
                    allFilesLoaded();
                };
                resizedImage.src = resizeCanvasDOM.toDataURL('image/jpeg');
            };

            var rotateImage = function (imObj, file, orientation, callback) {
                var resizeCanvas = $('<canvas></canvas>');
                $(resizeCanvas).css('display', 'none');
                $('body').append(resizeCanvas);
                var canvas = $(resizeCanvas).get(0);

                canvas.width = imObj.width;
                canvas.height = imObj.height;

                var width = canvas.width
                var height = canvas.height
                var styleWidth = canvas.style.width
                var styleHeight = canvas.style.height

                var ctx = canvas.getContext('2d')

                if (!orientation || orientation > 8) {
                    $(resizeCanvas).remove();
                    callback(imObj);

                    return
                }
                if (orientation > 4) {
                    canvas.width = height
                    canvas.height = width
                    canvas.style.width = styleHeight
                    canvas.style.height = styleWidth
                }

                switch (orientation) {
                    case 2:
                        // horizontal flip
                        ctx.translate(width, 0)
                        ctx.scale(-1, 1)
                        break
                    case 3:
                        // 180° rotate left
                        ctx.translate(width, height)
                        ctx.rotate(Math.PI)
                        break
                    case 4:
                        // vertical flip
                        ctx.translate(0, height)
                        ctx.scale(1, -1)
                        break
                    case 5:
                        // vertical flip + 90 rotate right
                        ctx.rotate(0.5 * Math.PI)
                        ctx.scale(1, -1)
                        break
                    case 6:
                        // 90° rotate right
                        ctx.rotate(0.5 * Math.PI)
                        ctx.translate(0, -height)
                        break
                    case 7:
                        // horizontal flip + 90 rotate right
                        ctx.rotate(0.5 * Math.PI)
                        ctx.translate(width, -height)
                        ctx.scale(-1, 1)
                        break
                    case 8:
                        // 90° rotate left
                        ctx.rotate(-0.5 * Math.PI)
                        ctx.translate(-width, 0)
                        break
                }

                ctx.drawImage(imObj, 0, 0, imObj.width, imObj.height);

                var rotated = new Image();
                rotated.onload = function () {
                    //after resized image is loaded, add it to resizedFiles
                    $(resizeCanvas).remove();

                    callback(this);
                };
                rotated.src = canvas.toDataURL('image/jpeg');
            };

            for (var i = 0; i < files.length; i++) {
                var reader = new FileReader();
                //again, no idea why this closure is needed, but there you go
                (function (file, reader) {
                    if (window.isImage(file.name)) {
                        reader.onloadend = function () {
                            var img = new Image();
                            img.onload = function () {
                                var exif = EXIF.getData(file, function () {
                                    var o = EXIF.getTag(this, 'Orientation');
                                    rotateImage(img, file, o, function (rotated) {
                                        resizeImage(rotated, file);
                                    });
                                });
                            };
                            img.src = reader.result;
                        };
                        reader.readAsDataURL(file);
                    } else {
                        //not an image, just a dumb file
                        reader.onloadend = function (e) {
                            resizedFiles.push({
                                type: 'file',
                                fname: file.name,
                                name: file.name,
                                file: file,
                                data: e.target.result
                            });
                            allFilesLoaded();
                        };
                        reader.readAsBinaryString(file);
                    }

                })(files[i], reader);
            }
        },

        UploadMedia: function (options, resultFn, failFn) {
            //on non-app: make an input element, click it, resize it, return it
            var inputEl = $('<input type="file" multiple="true" />');
            $(inputEl).css('display', 'none');
            $('body').append(inputEl);


            $(inputEl).on('change', function (ev) {
                //what do do when files are selected

                var files = $(inputEl).get(0).files;
                window.SquareIT.BWorkflow.HandleUploads({
                    files: files,
                    maxWidth: options.maxWidth,
                    maxHeight: options.maxHeight
                }, function (resized) {
                    $(inputEl).remove();
                    resultFn(resized);
                });
            });

            $(inputEl).click();
        }
    }
};

SquareIT.Loaded = true;
window.SqIT = SquareIT;
window.SquareIT.app = false;
$(document).trigger('SquareITLoaded');

