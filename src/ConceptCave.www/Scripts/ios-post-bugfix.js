﻿//Bugfix hack script due to IOS 6.0 caching POST requests if parameters haven't changed
//See: http://stackoverflow.com/questions/12506897/is-safari-on-ios-6-caching-ajax-results
$(function () {
    $.ajaxSetup({
        type: 'POST',
        headers: { "cache-control": "no-cache" }
    });
});