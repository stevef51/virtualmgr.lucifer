﻿using System;
using System.Threading.Tasks;
using ConceptCave.www.App_Start;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;
using VirtualMgr.Membership;
using VirtualMgr.Membership.Direct.IdentityOverrides;
using Ninject;
using System.Configuration;
using VirtualMgr.Central;
using Microsoft.Owin.Security.Interop;
using Microsoft.AspNetCore.DataProtection;
using System.IO;
using Microsoft.AspNetCore.DataProtection.AuthenticatedEncryption;
using Microsoft.Owin.Security;

[assembly: OwinStartup(typeof(ConceptCave.www.Startup))]

namespace ConceptCave.www
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }

        public static ApplicationRoleManager CreateRoleManager(IdentityFactoryOptions<ApplicationRoleManager> options, IOwinContext context)
        {
            var kernel = NinjectWebCommon.TheKernel;

            return kernel.Get<ApplicationRoleManager>();
        }

        public static ApplicationUserManager CreateUserManager(IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context)
        {
            var kernel = NinjectWebCommon.TheKernel;

            return kernel.Get<ApplicationUserManager>();
        }

        public static RolesWrapper CreateRoleWrapper(IdentityFactoryOptions<RolesWrapper> options, IOwinContext context)
        {
            var kernel = NinjectWebCommon.TheKernel;

            return kernel.Get<RolesWrapper>();
        }

        public static UserStore<ApplicationUser, ApplicationRole, string, IdentityUserLogin, IdentityUserRole, IdentityUserClaim> CreateUserStore(IdentityFactoryOptions<UserStore<ApplicationUser, ApplicationRole, string, IdentityUserLogin, IdentityUserRole, IdentityUserClaim>> options, IOwinContext context)
        {
            var kernel = NinjectWebCommon.TheKernel;

            return kernel.Get<UserStore<ApplicationUser, ApplicationRole, string, IdentityUserLogin, IdentityUserRole, IdentityUserClaim>>();
        }

        public static ApplicationDbContext CreateDbContext(IdentityFactoryOptions<ApplicationDbContext> options, IOwinContext context)
        {
            var kernel = NinjectWebCommon.TheKernel;

            return kernel.Get<ApplicationDbContext>();
        }

        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureAuth(IAppBuilder app)
        {
            var encryptionSettings = new Microsoft.AspNetCore.DataProtection.AuthenticatedEncryption.ConfigurationModel.AuthenticatedEncryptorConfiguration()
            {
                EncryptionAlgorithm = EncryptionAlgorithm.AES_256_CBC,
                ValidationAlgorithm = ValidationAlgorithm.HMACSHA256
            };

            var protectionProvider = DataProtectionProvider.Create(new DirectoryInfo(@"c:\shared-auth-ticket-keys\"), options =>
            {
                options.SetDefaultKeyLifetime(TimeSpan.FromDays(365 * 20));
                options.UseCryptographicAlgorithms(encryptionSettings);
            });

            var dataProtector = protectionProvider.CreateProtector(
                "CookieAuthenticationMiddleware",
                "Cookie",
                "v2");

            // Enable the application to use a cookie to store information for the signed in user
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                CookieName = ".VirtualManager.Cookies",
                LoginPath = new PathString("/Authenticate"),
                TicketDataFormat = new AspNetTicketDataFormat(
                    new DataProtectorShim(dataProtector))
            });

            app.SetDefaultSignInAsAuthenticationType(CookieAuthenticationDefaults.AuthenticationType);

            app.CreatePerOwinContext<ApplicationDbContext>(CreateDbContext);
            app.CreatePerOwinContext<UserStore<ApplicationUser, ApplicationRole, string, IdentityUserLogin, IdentityUserRole, IdentityUserClaim>>(CreateUserStore);
            app.CreatePerOwinContext<ApplicationUserManager>(CreateUserManager);
            app.CreatePerOwinContext<ApplicationRoleManager>(CreateRoleManager);
            app.CreatePerOwinContext<RolesWrapper>(CreateRoleWrapper);

            //app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            //// App.Secrets is application specific and holds values in CodePasteKeys.json
            //// Values are NOT included in repro – auto-created on first load
            //if (!string.IsNullOrEmpty(App.Secrets.GoogleClientId))
            //{
            //    app.UseGoogleAuthentication(
            //        clientId: App.Secrets.GoogleClientId,
            //        clientSecret: App.Secrets.GoogleClientSecret);
            //}

            //if (!string.IsNullOrEmpty(App.Secrets.TwitterConsumerKey))
            //{
            //    app.UseTwitterAuthentication(
            //        consumerKey: App.Secrets.TwitterConsumerKey,
            //        consumerSecret: App.Secrets.TwitterConsumerSecret);
            //}

            //if (!string.IsNullOrEmpty(App.Secrets.GitHubClientId))
            //{
            //    app.UseGitHubAuthentication(
            //        clientId: App.Secrets.GitHubClientId,
            //        clientSecret: App.Secrets.GitHubClientSecret);
            //}

            //AntiForgeryConfig.UniqueClaimTypeIdentifier = ClaimTypes.NameIdentifier;
        }
    }
}
