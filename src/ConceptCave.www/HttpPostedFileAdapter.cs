﻿using ConceptCave.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConceptCave.www
{
    public class HttpPostedFileAdapter : IPostedFile
    {
        public HttpPostedFileBase _file;

        public HttpPostedFileAdapter(HttpPostedFileBase file)
        {
            _file = file;
        }

        public string FileName => _file.FileName;

        public string ContentType => _file.ContentType;

        public long ContentLength => _file.ContentLength;

        public long Read(byte[] data, long offset, long length) => _file.InputStream.Read(data, (int)offset, (int)length);
    }
}