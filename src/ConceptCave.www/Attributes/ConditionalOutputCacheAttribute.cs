﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ConceptCave.www.Attributes
{
    public class ConditionalOutputCacheAttribute : OutputCacheAttribute
    {
        public bool CachingEnabled
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["ConceptCave.www.Controllers.CdnController.Caching"] == null)
                {
                    return true;
                }

                return bool.Parse(System.Configuration.ConfigurationManager.AppSettings["ConceptCave.www.Controllers.CdnController.Caching"]);
            }
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (CachingEnabled == false)
            {
                return;
            }

            base.OnActionExecuting(filterContext);
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            if (CachingEnabled == false)
            {
                return;
            }

            base.OnActionExecuted(filterContext);

        }

        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            if (CachingEnabled == false)
            {
                return;
            }

            base.OnResultExecuting(filterContext);
        }

        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            if (CachingEnabled == false)
            {
                return;
            }

            base.OnResultExecuted(filterContext);
        }
    }
}