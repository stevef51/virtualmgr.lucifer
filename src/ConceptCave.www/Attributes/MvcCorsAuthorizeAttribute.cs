﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;


namespace ConceptCave.www.Attributes
{
    public class HttpOriginMatcher
    {
        private bool _anyOrigin = true;
        private string[] _origins = new string[0];
        public string Origins
        {
            get
            {
                return string.Join(",", _origins);
            }
            set
            {
                _anyOrigin = value == "*";
                _origins = value.ToLower().Split(',');
            }
        }

        public bool MatchOrigin(string origin)
        {
            if (origin == null)
                return false;

            if (_anyOrigin)
                return true;

            foreach (var testOrigin in _origins)
            {
                if (testOrigin.StartsWith("^"))
                {
                    Regex rex = new Regex(testOrigin);
                    if (rex.IsMatch(origin))
                        return true;
                }
                else
                {
                    if (testOrigin == origin)
                        return true;

                    var s = testOrigin.IndexOf('*');
                    if (s >= 0)
                    {
                        if (s > origin.Length)
                            continue;

                        if (origin.Substring(0, s) == testOrigin.Substring(0, s))
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }


    }
    public class MvcCorsAuthorizeAttribute : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            // CORS requests send an options request down that doesn't contain
            // authentication information (cookies, headers etc). These requests
            // are done by the browser, with no control in script, so we can't
            // control this client side. Based on the HTTP Spec we can treat options
            // requests as authorized
            if (httpContext.Request.HttpMethod.ToLower() == "options")
            {
                return true;
            }

            return base.AuthorizeCore(httpContext);
        }
    }

    public class MvcCorsOptionsAllowAttribute : ActionFilterAttribute
    {
        private HttpOriginMatcher _origins = new HttpOriginMatcher();

        public string Origins
        {
            get
            {
                return _origins.Origins;
            }
            set
            {
                _origins.Origins = value;
            }
        }

        public override void OnActionExecuting(ActionExecutingContext actionContext)
        {
            string origin = null;
            if (actionContext.HttpContext.Request.Headers.AllKeys.Any(h => h.ToLower() == "origin"))
                origin = (actionContext.HttpContext.Request.Headers.GetValues("origin").FirstOrDefault() ?? "").ToLower();
            if (_origins.MatchOrigin(origin))
            {
                actionContext.HttpContext.Response.Headers.Add("Access-Control-Allow-Origin", origin);
                actionContext.HttpContext.Response.Headers.Add("Access-Control-Allow-Headers", "Content-Type, bworkflow-handle-errors-generically, Authorization");
                actionContext.HttpContext.Response.Headers.Add("Access-Control-Allow-Methods", "*");
                actionContext.HttpContext.Response.Headers.Add("Access-Control-Allow-Credentials", "true");
                    
                // If this is OPTIONS then return empty result 
                if (actionContext.HttpContext.Request.HttpMethod.ToLower() == "options")
                {
                    actionContext.Result = new EmptyResult();
                    return;
                }
            }

            base.OnActionExecuting(actionContext);
        }
    }
}