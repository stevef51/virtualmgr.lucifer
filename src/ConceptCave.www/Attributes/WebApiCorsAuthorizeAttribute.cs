﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Filters;


namespace ConceptCave.www.Attributes
{
    public static class Consts
    {                                                 
        public const string DefaultCorsAllowOrigin = @"http://localhost:*,http://127.0.0.1:*";
        public const string SecureDomains = @"^https://.*\.getevs\.com$,^https://.*\.gethealthclean\.tech$,^https://.*\.getsmartcleaner\.org$,^https://.*\.getfoodsafe\.tech$,^https://.*\.vmgr\.net$";
    }

    public class WebApiCorsAuthorizeAttribute : AuthorizeAttribute
    {
        protected override bool IsAuthorized(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            // CORS requests send an options request down that doesn't contain
            // authentication information (cookies, headers etc). These requests
            // are done by the browser, with no control in script, so we can't
            // control this client side. Based on the HTTP Spec we can treat options
            // requests as authorized
            if(actionContext.Request.Method == System.Net.Http.HttpMethod.Options)
            {
                return true;
            }

            return base.IsAuthorized(actionContext);
        }
    }

    public class WebApiCorsOptionsAllowAttribute : ActionFilterAttribute
    {
        private HttpOriginMatcher _origins;
        public Func<string> Origins { get; set; } = () => Consts.DefaultCorsAllowOrigin;

        public WebApiCorsOptionsAllowAttribute()
        {
        }

        public WebApiCorsOptionsAllowAttribute(string origins)
        {
            Origins = () => origins;
        }

        public override void OnActionExecuting(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            if (_origins == null)
            {
                _origins = new HttpOriginMatcher() { Origins = Origins() };
            }
            if (actionContext.Request.Method == System.Net.Http.HttpMethod.Options)
            {
                string origin = null;
                if (actionContext.Request.Headers.Contains("origin"))
                    origin = (actionContext.Request.Headers.GetValues("origin").FirstOrDefault() ?? "").ToLower();
                if (_origins.MatchOrigin(origin))
                {
                    if (actionContext.Response == null)
                        actionContext.Response = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.OK);

                    actionContext.Response.StatusCode = System.Net.HttpStatusCode.OK;
                    actionContext.Response.Headers.Add("Access-Control-Allow-Origin", origin);
                    actionContext.Response.Headers.Add("Access-Control-Allow-Headers", "Content-Type, bworkflow-handle-errors-generically, Authorization");
                    actionContext.Response.Headers.Add("Access-Control-Allow-Methods", "*");
                    actionContext.Response.Headers.Add("Access-Control-Allow-Credentials", "true");

                    return;
                }
            }
            base.OnActionExecuting(actionContext);
        }

        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            base.OnActionExecuted(actionExecutedContext);
            if (_origins == null)
            {
                _origins = new HttpOriginMatcher() { Origins = Origins() };
            }

            if (actionExecutedContext.Request.Method != System.Net.Http.HttpMethod.Options)
            {
                string origin = null;
                if (actionExecutedContext.Request.Headers.Contains("origin"))
                    origin = (actionExecutedContext.Request.Headers.GetValues("origin").FirstOrDefault() ?? "").ToLower();
                if (_origins.MatchOrigin(origin))
                {
                    if (actionExecutedContext.Response == null)
                        actionExecutedContext.Response = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.OK);

                    if (!actionExecutedContext.Response.Headers.Contains("Access-Control-Allow-Origin"))
                    {
                        actionExecutedContext.Response.Headers.Add("Access-Control-Allow-Origin", origin);
                        actionExecutedContext.Response.Headers.Add("Access-Control-Allow-Headers", "Content-Type, bworkflow-handle-errors-generically, Authorization");
                        actionExecutedContext.Response.Headers.Add("Access-Control-Allow-Methods", "*");
                        actionExecutedContext.Response.Headers.Add("Access-Control-Allow-Credentials", "true");
                    }
                }
            }

        }
    }

    // Look up the CORS Secure Domains from AppSettings (to allow the list to grow)
    public class WebApiCorsAuthorizeSecureDomainsAttribute : WebApiCorsOptionsAllowAttribute
    {
        public static string SecureDomains()
        {
            return ConfigurationManager.AppSettings["CORSSecureDomains"] ?? (Consts.DefaultCorsAllowOrigin + "," + Consts.SecureDomains);
        }

        public WebApiCorsAuthorizeSecureDomainsAttribute()
        {
            Origins = SecureDomains;
        }
    }

}