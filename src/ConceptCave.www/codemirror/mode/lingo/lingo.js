﻿CodeMirror.defineMode("lingo", function (config) {
    function words(str) {
        var obj = {}, words = str.split(" ");
        for (var i = 0; i < words.length; ++i) obj[words[i]] = true;
        return obj;
    }
    var keywords = words("present section question answer finish with all sections itemat parent isnull ifnull subsection getquestion facility answers time datetime href from select where contains as display orderby");
    var indent_keywords = words("when is loop");
    var dedent_keywords = words("end");

    var atoms = { "null": true };

    var isOperatorChar = /[+\-*&%=<>!?|\/]/;

    function tokenBase(stream, state) {
        var ch = stream.next();
        if (ch == "#" && state.startOfLine) {
            stream.skipToEnd();
            return "meta";
        }
        if (ch == '"' || ch == "'") {
            state.tokenize = tokenString(ch);
            return state.tokenize(stream, state);
        }

        if (/[\[\]{}\(\),;\:\.]/.test(ch)) {
            return null;
        }
        if (/\d/.test(ch)) {
            stream.eatWhile(/[\w\.]/);
            return "number";
        }
        if (ch == "/") {
            if (stream.eat("/")) {
                stream.skipToEnd();
                return "comment";
            }
        }
        if (isOperatorChar.test(ch)) {
            stream.eatWhile(isOperatorChar);
            return "operator";
        }
        stream.eatWhile(/[\w\$_]/);
        var cur = stream.current();
        if (keywords.propertyIsEnumerable(cur))
            return "keyword";
        if (indent_keywords.propertyIsEnumerable(cur))
            return "keyword";
        if (dedent_keywords.propertyIsEnumerable(cur))
            return "keyword";
        if (atoms.propertyIsEnumerable(cur)) return "atom";
        return "variable";
    }

    function tokenString(quote) {
        return function (stream, state) {
            var escaped = false, next, end = false;
            while ((next = stream.next()) != null) {
                if (next == quote && !escaped) { end = true; break; }
                escaped = !escaped && next == "\\";
            }
            if (end || !escaped) state.tokenize = null;
            return "string";
        };
    }

    function tokenComment(stream, state) {
        stream.skipToEnd();
        return "comment";
    }

    // Interface

    return {
        electricChars: "", //"dDpPtTfFeE ",
        startState: function (basecolumn) {
            return {
                tokenize: tokenBase,
                lastToken: null,
                currentIndent: 0,
                nextLineIndent: 0,
                doInCurrentLine: false


            };
        },

        token: function (stream, state) {
            if (stream.eatSpace()) return null;
            var style = (state.tokenize || tokenBase)(stream, state);
            if (style == "comment" || style == "meta") return style;
            return style;
        },

/*        indent: function (state, textAfter) {
            var trueText = textAfter.replace(/^\s+|\s+$/g, '');
            if (trueText.match(dedent_keywords))
                return 2 * (state.currentIndent - 1);
            if (state.currentIndent < 0) return 0;
            return state.currentIndent;
        } */
    };
});

CodeMirror.defineMIME("text/x-lingo", "lingo");
