﻿using ConceptCave.BusinessLogic;
using ConceptCave.BusinessLogic.Configuration;
using ConceptCave.BusinessLogic.Invoices;
using ConceptCave.BusinessLogic.ReportData;
using ConceptCave.BusinessLogic.ScheduleManagement;
using ConceptCave.BusinessLogic.Context;
using ConceptCave.BusinessLogic.Workloading;
using ConceptCave.Checklist;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.PowerBi;
using ConceptCave.Repository.Injected;
using ConceptCave.Repository.PolicyManagement;
using ConceptCave.Repository.TaskEvents;
using ConceptCave.RepositoryInterfaces.PolicyManagement;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.RepositoryInterfaces.ScheduleManagement;
using ConceptCave.RepositoryInterfaces.TaskEvents;
using ConceptCave.RepositoryInterfaces.Workloading;
using ConceptCave.www.Language;
using ConceptCave.www.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using VirtualMgr.Central;
using VirtualMgr.Central.Interfaces;
using Ninject;
using ConceptCave.Repository.ImportMapping;
using Newtonsoft.Json;
using ConceptCave.Checklist.Facilities;
using eWAY.Rapid;
using ConceptCave.BusinessLogic.PeriodicExecution;
using ConceptCave.BusinessLogic.LingoDataObjects;
using ConceptCave.BusinessLogic.Facilities;
using ConceptCave.Repository.PeriodicExecution;
using Microsoft.AspNetCore.NodeServices;
using System.Web.Hosting;
using VirtualMgr.NodeService;
using Microsoft.Extensions.Logging;
using ConceptCave.RepositoryInterfaces;
using VirtualMgr.Aspose;
using VirtualMgr.PeriodicExecution;
using VirtualMgr.JwtAuth;
using System.Text;
using VirtualMgr.Membership;
using VirtualMgr.Membership.Direct.IdentityOverrides;
using Microsoft.AspNet.Identity.EntityFramework;
using VirtualMgr.Membership.Direct;
using VirtualMgr.Membership.Interfaces;

namespace ConceptCave.www.App_Start
{
    public class ServicesNinjectModule : Ninject.Modules.NinjectModule
    {
        public override void Load()
        {
            var kernel = this;
            
            kernel.Bind<HttpContext>().ToMethod(c => HttpContext.Current);

            kernel.Bind<IDateTimeProvider>().To<RealtimeDateTimeProvider>().InSingletonScope();

            kernel.Bind<EndOfDayManagement>().ToSelf();
            kernel.Bind<ScheduleApprovalManagement>().ToSelf();

            // we do this here as virtual manager direct ninject doesn't reference the multi tenant stuff
            Func<ConnectionStringSettings> fnConnectionStringSettings = () => new ConnectionStringSettings(MultiTenantManager.CurrentTenant.Name, MultiTenantManager.CurrentTenant.ConnectionString);
            kernel.Bind<DatabaseConnectionManager>().ToMethod(context => new DatabaseConnectionManager(fnConnectionStringSettings));

            kernel.Bind<ICurrentContextProvider>().To<MembershipDirectCurrentContextProvider>();

            kernel.Bind<IPublishingGroupRepository>().To<OPublishingGroupRepository>();
            kernel.Bind<IPublishingGroupResourceRepository>().To<OPublishingGroupResourceRepository>();
            kernel.Bind<IUserAgentRepository>().To<OUserAgentRepository>();
            kernel.Bind<IWorkingDocumentRepository>().To<OWorkingDocumentRepository>();
            kernel.Bind<IBlockedRepository>().To<OBlockedRepository>();
            kernel.Bind<IMembershipRepository>().To<OMembershipRepository>();
            kernel.Bind<IMembershipTypeRepository>().To<OMembershipTypeRepository>();
            kernel.Bind<IReportingRepository>().To<OReportingRepository>();
            kernel.Bind<ILabelRepository>().To<OLabelRepository>();
            kernel.Bind<IErrorLogRepository>().To<OErrorLogRepository>();
            kernel.Bind<IMediaRepository>().To<OMediaRepository>();
            kernel.Bind<ISequenceRepository>().To<OSequenceRepository>();
            //            kernel.Bind<IMessagingRepository>().To<OMessagingRepository>();
            kernel.Bind<IHierarchyRepository>().To<OHierarchyRepository>();
            kernel.Bind<IGlobalSettingsRepository>().To<OGlobalSettingsRepository>();
            kernel.Bind<IArticleRepository>().To<OArticleRepository>();
            kernel.Bind<IReportManagementRepository>().To<OReportManagementRepository>();
            kernel.Bind<IReportTicketRepository>().To<OReportTicketRepository>();

            kernel.Bind<ICalendarRuleRepository>().To<OCalendarRuleRepository>();

            kernel.Bind<ICompanyRepository>().To<OCompanyRepository>();
            kernel.Bind<ICountryStateRepository>().To<OCountryStateRepository>();

            kernel.Bind<IExecutionHandlerQueueRepository>().To<OExecutionHandlerQueueRepository>();

            kernel.Bind<IMediaPolicyManager>().To<MediaPolicyManager>();
            kernel.Bind<INewsFeedRepository>().To<ONewsFeedRepository>();

            kernel.Bind<IRosterRepository>().To<ORosterRepository>();
            kernel.Bind<ITaskTypeRepository>().To<OTaskTypeRepository>();
            kernel.Bind<IFinishedStatusRepository>().To<OFinishedStatusRepository>();
            kernel.Bind<IStatusRepository>().To<OStatusRepository>();
            kernel.Bind<IHierarchyBucketOverrideTypeRepository>().To<OHierarchyBucketOverrideTypeRepository>();
            kernel.Bind<ITaskRepository>().To<OTaskRepository>();
            kernel.Bind<ITaskWorkLogRepository>().To<OTaskWorkLogRepository>();
            kernel.Bind<IProjectJobTaskWorkingDocumentRepository>().To<OProjectJobTaskWorkingDocumentRepository>();
            kernel.Bind<IScheduledTaskRepository>().To<OScheduledTaskRepository>();
            kernel.Bind<ITaskChangeRequestRepository>().To<OTaskChangeRequestRepository>();
            kernel.Bind<ILanguageRepository>().To<OLanguageRepository>();
            kernel.Bind<ILanguageTranslationRepository>().To<OLanguageTranslationRepository>();
            kernel.Bind<IWorkLoadingBookRepository>().To<OWorkLoadingBookRepository>();
            kernel.Bind<IWorkLoadingActivityRepository>().To<OWorkLoadingActivityRepository>();
            kernel.Bind<IWorkLoadingFeatureRepository>().To<OWorkLoadingFeatureRepository>();
            kernel.Bind<ISiteFeatureRepository>().To<OSiteFeatureRepository>();
            kernel.Bind<IMeasurementUnitRepository>().To<OMeasurmentUnitRepository>();
            kernel.Bind<IAssetTrackerRepository>().To<OAssetTrackerRepository>();
            kernel.Bind<ITabletRepository>().To<OTabletRepository>();
            kernel.Bind<ITabletProfileRepository>().To<OTabletProfileRepository>();
            kernel.Bind<IAssetTypeRepository>().To<OAssetTypeRepository>();
            kernel.Bind<IAssetRepository>().To<OAssetRepository>();
            kernel.Bind<IAssetScheduleRepository>().To<OAssetScheduleRepository>();
            kernel.Bind<IAssetTypeCalendarRepository>().To<OAssetTypeCalendarRepository>();
            kernel.Bind<IAssetTypeCalendarTaskRepository>().To<OAssetTypeCalendarTaskRepository>();
            kernel.Bind<IFacilityStructureRepository>().To<OFacilityStructureRepository>();
            kernel.Bind<IGpsLocationRepository>().To<OGpsLocationRepository>();

            kernel.Bind<IProductRepository>().To<OProductRepository>();
            kernel.Bind<IProductCatalogRepository>().To<OProductCatalogRepository>();
            kernel.Bind<IProductCatalogItemRepository>().To<OProductCatalogItemRepository>();
            kernel.Bind<IOrderRepository>().To<OOrderRepository>();

            kernel.Bind<IDWPopulate>().To<ODWTenantPopulateBuildingFloorsDimension>().Named("Tenant_Dim_BuildingFloors");
            kernel.Bind<IDWPopulate>().To<ODWTenantPopulateCompaniesDimension>().Named("Tenant_Dim_Companies");
            kernel.Bind<IDWPopulate>().To<ODWTenantPopulateDatesDimension>().Named("Tenant_Dim_Dates");
            kernel.Bind<IDWPopulate>().To<ODWTenantPopulateDateTimesDimension>().Named("Tenant_Dim_DateTimes");
            kernel.Bind<IDWPopulate>().To<ODWTenantPopulateFacilitiesDimension>().Named("Tenant_Dim_Facilities");
            kernel.Bind<IDWPopulate>().To<ODWTenantPopulateFacilityBuildingsDimension>().Named("Tenant_Dim_FacilityBuildings");
            kernel.Bind<IDWPopulate>().To<ODWTenantPopulateFloorZonesDimension>().Named("Tenant_Dim_FloorZones");
            kernel.Bind<IDWPopulate>().To<ODWTenantPopulateRostersDimension>().Named("Tenant_Dim_Rosters");
            kernel.Bind<IDWPopulate>().To<ODWTenantPopulateSitesDimension>().Named("Tenant_Dim_Sites");
            kernel.Bind<IDWPopulate>().To<ODWTenantPopulateTaskCustomStatusDimension>().Named("Tenant_Dim_TaskCustomStatus");
            kernel.Bind<IDWPopulate>().To<ODWTenantPopulateTaskStatusDimension>().Named("Tenant_Dim_TaskStatus");
            kernel.Bind<IDWPopulate>().To<ODWTenantPopulateTaskTypesDimension>().Named("Tenant_Dim_TaskTypes");
            kernel.Bind<IDWPopulate>().To<ODWTenantPopulateUsersDimension>().Named("Tenant_Dim_Users");
            kernel.Bind<IDWPopulate>().To<ODWTenantPopulateZoneSitesDimension>().Named("Tenant_Dim_ZoneSites");
            kernel.Bind<IDWPopulate>().To<ODWTenantPopulateTaskFacts>().Named("Tenant_Fact_Tasks");

            kernel.Bind<IDWPopulate>().To<ODWTenantMasterPopulateBuildingFloorsDimension>().Named("TenantMaster_Dim_BuildingFloors");
            kernel.Bind<IDWPopulate>().To<ODWTenantMasterPopulateCompaniesDimension>().Named("TenantMaster_Dim_Companies");
            kernel.Bind<IDWPopulate>().To<ODWTenantMasterPopulateDatesDimension>().Named("TenantMaster_Dim_Dates");
            kernel.Bind<IDWPopulate>().To<ODWTenantMasterPopulateDateTimesDimension>().Named("TenantMaster_Dim_DateTimes");
            kernel.Bind<IDWPopulate>().To<ODWTenantMasterPopulateFacilitiesDimension>().Named("TenantMaster_Dim_Facilities");
            kernel.Bind<IDWPopulate>().To<ODWTenantMasterPopulateFacilityBuildingsDimension>().Named("TenantMaster_Dim_FacilityBuildings");
            kernel.Bind<IDWPopulate>().To<ODWTenantMasterPopulateFloorZonesDimension>().Named("TenantMaster_Dim_FloorZones");
            kernel.Bind<IDWPopulate>().To<ODWTenantMasterPopulateRostersDimension>().Named("TenantMaster_Dim_Rosters");
            kernel.Bind<IDWPopulate>().To<ODWTenantMasterPopulateSitesDimension>().Named("TenantMaster_Dim_Sites");
            kernel.Bind<IDWPopulate>().To<ODWTenantMasterPopulateTaskCustomStatusDimension>().Named("TenantMaster_Dim_TaskCustomStatus");
            kernel.Bind<IDWPopulate>().To<ODWTenantMasterPopulateTaskStatusDimension>().Named("TenantMaster_Dim_TaskStatus");
            kernel.Bind<IDWPopulate>().To<ODWTenantMasterPopulateTaskTypesDimension>().Named("TenantMaster_Dim_TaskTypes");
            kernel.Bind<IDWPopulate>().To<ODWTenantMasterPopulateUsersDimension>().Named("TenantMaster_Dim_Users");
            kernel.Bind<IDWPopulate>().To<ODWTenantMasterPopulateZoneSitesDimension>().Named("TenantMaster_Dim_ZoneSites");
            kernel.Bind<IDWPopulate>().To<ODWTenantMasterPopulateTaskFacts>().Named("TenantMaster_Fact_Tasks");
            kernel.Bind<IDWPopulate>().To<ODWTenantMasterPopulateTenantsDimension>().Named("TenantMaster_Dim_Tenants");
            kernel.Bind<Func<string, IDWPopulate>>().ToMethod(ctx => name => ctx.Kernel.Get<IDWPopulate>(name));

            kernel.Bind<Func<Type, IPeriodicExecutionHandler>>().ToMethod(ctx => type => ctx.Kernel.Get(type) as IPeriodicExecutionHandler);

            kernel.Bind<ITimesheetItemTypeRepository>().To<OTimesheetItemTypeRepository>();
            kernel.Bind<ITimesheetItemRepository>().To<OTimesheetItemRepository>();

            kernel.Bind<IVersionsRepository>().To<OVersionsRepository>();
            kernel.Bind<IUserBillingTypeRepository>().To<OUserBillingTypeRepository>();
            kernel.Bind<IInvoiceRepository>().To<OInvoiceRepository>();
            kernel.Bind<InvoiceManager>().ToSelf();

            kernel.Bind<IJSFunctionRepository>().To<OJSFunctionRepository>();
            kernel.Bind<IQRCodeRepository>().To<OQRCodeRepository>();

            kernel.Bind<ILanguageTranslateService>().ToMethod(ctx =>
            {
                var apiKey = ConfigurationManager.AppSettings["AzureTranslateApiKey"] ?? "4aa859888a9b4e47ad0ba28f04606626";
                return new AzureTranslateServiceV3(apiKey);
            });

            kernel.Bind<IAuditTaskConditionActionRepository>().To<OAuditTaskConditionActionRepository>();

            kernel.Bind<IScheduleManagement>().To<ScheduleManagementBase>();

            kernel.Bind<IWorkingDocumentFactory>().To<ConceptCave.www.API.v1.PlayerController>();

            kernel.Bind<IMediaManager>().To<MediaManager>().InThreadScope();
            kernel.Bind<IContextManagerFactory>().To<ContextManagerFactory>().InThreadScope();
            kernel.Bind<IReportingTableWriter>().To<ReportingTableWriter>().InThreadScope();

            kernel.Bind<IWorkloadingDurationCalculator>().To<WorkloadingDurationCalculator>();
            kernel.Bind<IWorkloadingActivityFactory>().To<WorkloadingActivityFactory>();
            kernel.Bind<IWorkingDocumentStateManager>().To<WorkingDocumentStateManager>().InThreadScope();
            kernel.Bind<RenderConverter>().ToSelf().InSingletonScope();
            kernel.Bind<AnswerConverter>().ToSelf().InSingletonScope();

            kernel.Bind<ServerUrlParser>().ToMethod(ctx =>
            {
                return new ServerUrlParser(ctx.Kernel.Get<ITenantContextProvider>(), () =>
                {
                    var gsr = ctx.Kernel.Get<IGlobalSettingsRepository>();
                    return gsr.GetSetting<string>("JsReportServerUrl") ?? "https://jsreport.vmgr.net";
                });
            });
            kernel.Bind<IUrlParser>().ToMethod(ctx => ctx.Kernel.Get<ServerUrlParser>());
            kernel.Bind<IServerUrls>().ToMethod(ctx => ctx.Kernel.Get<ServerUrlParser>());
            kernel.Bind<ITenantContextProvider>().ToMethod(ctx => MultiTenantManager.TenantContextProvider);
            kernel.Bind<IMultiTenantRepository>().To<MultiTenantManager>();

            kernel.Bind<IMediaExporterFactoryAsync>().To<TarGzMedia.ExporterFactoryAsync>();
            kernel.Bind<IMediaImporterFactoryAsync>().To<TarGzMedia.ImporterFactoryAsync>();

            kernel.Bind<IESProbeRepository>().To<OESProbeRepository>();
            kernel.Bind<IESSensorRepository>().To<OESSensorRepository>();
            kernel.Bind<IESSensorReadingRepository>().To<OESSensorReadingRepository>();

            kernel.Bind<IPowerBiConfiguration>().To<PowerBiConfigurationAppConfig>();
            kernel.Bind<IPowerBiManager>().To<PowerBiManager>();

            kernel.Bind<IReflectionFactory>().ToMethod(ctx => AspNetReflectionFactory.Current);

            kernel.Bind<IRosterTaskEventsManager>().To<RosterTaskEventsManager>();

            //Use the existing Web.config plumbinb in the RepositorySectionManager
            kernel.Bind<IRepositorySectionManager>()
                  .ToMethod(
                      ctx =>
                      (IRepositorySectionManager)ConfigurationManager.GetSection("conceptcave.repository.resources"));
            //DON'T ACTUALLY NEED TO DO THIS!
            //Turns out Ninject does pretty much the same thing by default
            //kernel.Bind<HttpContext>().ToMethod((ctx) => HttpContext.Current).InRequestScope();

            kernel.Bind<IEmailConfiguration, TenantEmailConfiguration>().To<TenantEmailConfiguration>().InSingletonScope();

            kernel.Bind<ISendGridConfiguration>().To<SendGridConfiguration>();

            kernel.Bind<MembershipImportParser>().ToSelf();
            kernel.Bind<Func<MembershipImportParser>>().ToMethod(ctx => () => ctx.Kernel.Get<MembershipImportParser>());
            kernel.Bind<ProductCatalogExportParser>().ToSelf();
            kernel.Bind<Func<ProductCatalogExportParser>>().ToMethod(ctx => () => ctx.Kernel.Get<ProductCatalogExportParser>());
            kernel.Bind<ProductCatalogImportWriter>().ToSelf();
            kernel.Bind<Func<ProductCatalogImportWriter>>().ToMethod(ctx => () => ctx.Kernel.Get<ProductCatalogImportWriter>());
            kernel.Bind<QRCodeImportWriter>().ToSelf();
            kernel.Bind<Func<QRCodeImportWriter>>().ToMethod(ctx => () => ctx.Kernel.Get<QRCodeImportWriter>());
            kernel.Bind<QRCodeExportParser>().ToSelf();
            kernel.Bind<Func<QRCodeExportParser>>().ToMethod(ctx => () => ctx.Kernel.Get<QRCodeExportParser>());

            // Custom PEJob bindings ..
            kernel.Bind<IPeriodicExecutionHandler>().To<ExecutePublishedChecklistPEJob>().Named("ExecutePublishedChecklist");
            kernel.Bind<IPeriodicExecutionHandler>().To<eWayPaymentPeriodicExecutionHandler>().Named("eWayProcessPendingPayment");
            kernel.Bind<Func<CustomJobPeriodicExecutionType, IPeriodicExecutionHandler>>().ToMethod(ctx => jobType => ctx.Kernel.Get<IPeriodicExecutionHandler>(jobType.ToString()));
            kernel.Bind<Func<string, IPeriodicExecutionHandler>>().ToMethod(ctx => jobType => ctx.Kernel.Get<IPeriodicExecutionHandler>(jobType));
            kernel.Bind<API.v1.PeriodicExecutionLogic>().ToSelf();
            kernel.Bind<IPEJobRepository>().To<OPEJobRepository>();

            kernel.Bind<IMediaPreviewCreator>().To<WordDocumentPreviewCreator>().Named("application/msword");
            kernel.Bind<IMediaPreviewCreator>().To<WordDocumentPreviewCreator>().Named("application/vnd.openxmlformats-officedocument.wordprocessingml.document");

            kernel.Bind<Func<string, IMediaPreviewCreator>>().ToMethod(ctx => mime =>
            {
                return ctx.Kernel.TryGet<IMediaPreviewCreator>(mime.ToLower());
            });
            kernel.Bind<IPeriodicExecutionHistoryRepository>().To<OPeriodicExecutionHistoryRepository>();

            kernel.Bind<Func<bool, eWAY.Rapid.IRapidClientSync>>().ToMethod(ctx => useSandbox =>
            {
                var rapidEndpoint = useSandbox ? "Sandbox" : "Production";
                var gsr = ctx.Kernel.Get<IGlobalSettingsRepository>();
                var merchantConfigJson = gsr.GetSetting<string>("eWayPayment_" + rapidEndpoint);

                var merchantConfig = JsonConvert.DeserializeObject<eWayPaymentMerchantConfig>(merchantConfigJson);

                return new RapidClientSync(RapidClientFactory.NewRapidClient(merchantConfig.APIKey, merchantConfig.Password, rapidEndpoint));
            });

            kernel.Bind<IPendingPaymentRepository>().To<OPendingPaymentRepository>();
            kernel.Bind<IPaymentResponseRepository>().To<OPaymentResponseRepository>();

            kernel.Bind<IPaymentGatewayFacility>().To<eWayPaymentFacility>().Named(eWayPaymentFacility.GATEWAY_NAME);
            kernel.Bind<Func<string, IPaymentGatewayFacility>>().ToMethod(ctx => gatewayName => ctx.Kernel.Get<IPaymentGatewayFacility>(gatewayName));
            kernel.Bind<eWayPaymentFacility>().ToSelf();    
            kernel.Bind<PendingPaymentFacility>().ToSelf();
            kernel.Bind<IGatewayCustomerRepository>().To<OGatewayCustomerRepository>();
            kernel.Bind<eWayPaymentPeriodicExecutionHandler>().ToSelf();

            kernel.Bind<Func<Guid, FacilityUserHelper>>().ToMethod(ctx => userId => new FacilityUserHelper(ctx.Kernel.Get<IMembershipRepository>(), ctx.Kernel.Get<ILabelRepository>(), ctx.Kernel.Get<IContextManagerFactory>(), userId));
            kernel.Bind<Func<PendingPaymentFacility>>().ToMethod(ctx => () => ctx.Kernel.Get<PendingPaymentFacility>());

            kernel.Bind<ILoggerFactory>().To<NLog.Extensions.Logging.NLogLoggerFactory>().InSingletonScope();

            // Note a fix for a bug in Ninject's InSingletonScope where it creates new instances on every request since its bounds to a Method??
            INodeServices nodeServicesSingleton = null;
            kernel.Bind<INodeServices>().ToMethod(ctx =>
                {
                    if (nodeServicesSingleton == null)
                    {
                        var options = new NodeServicesOptions(AspNetReflectionFactory.Current)
                        {
                            ProjectPath = HostingEnvironment.ApplicationPhysicalPath,
                            LaunchWithDebugging = false,
                            ApplicationStoppingToken = NinjectWebCommon.ApplicationRunning.Token
                        };
                        options.EnvironmentVariables["NODE_ENV"] = HostingEnvironment.IsDevelopmentEnvironment ? "development" : "production";
                        nodeServicesSingleton = NodeServicesFactory.CreateNodeServices(options);
                    }
                    return nodeServicesSingleton;
                });//.InSingletonScope();

            kernel.Bind<JsFunctionService>().ToSelf().InSingletonScope();
            kernel.Bind<IJsFunctionService>().ToMethod(ctx => ctx.Kernel.Get<JsFunctionService>());

            kernel.Bind<ITenantContext>().ToMethod(ctx => MultiTenantManager.CurrentTenant);
            kernel.Bind<Func<ITenantContext>>().ToMethod(ctx => () => MultiTenantManager.CurrentTenant);

            kernel.Bind<TokenGenerator>().ToMethod(ctx => new TokenGenerator()
            {
                Secret = Encoding.UTF8.GetBytes(MultiTenantManager.CurrentTenant.BaseUrl.ToString() + "279B7487-7F3D-4E77-AE5E-FD6073508E32")
            });
            kernel.Bind<ITokenGenerator>().ToMethod(ctx => ctx.Kernel.Get<TokenGenerator>());
            kernel.Bind<ITokenValidator>().ToMethod(ctx => ctx.Kernel.Get<TokenGenerator>());
            kernel.Bind<Func<ITokenValidator>>().ToMethod(ctx => () => ctx.Kernel.Get<ITokenValidator>());
        }
    }
}