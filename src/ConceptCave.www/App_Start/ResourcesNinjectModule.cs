﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using VirtualMgr.Central.Interfaces;

namespace ConceptCave.www.App_Start
{
    public class ResourcesNinjectModule : Ninject.Modules.NinjectModule
    {
        public class UpdateScript : IUpdateScript
        {
            public int Order { get; set; }

            public string ScriptName { get; set; }
            public string ScriptText { get; set; }
        }

        public override void Load()
        {
            // The ConceptCave DACPAC
            Bind<Func<Stream>>().ToMethod(ctx => () =>
            {
                return File.OpenRead(System.Web.Hosting.HostingEnvironment.MapPath(@"~/App_GlobalResources/ConceptCave.Sql.dacpac"));
            }).Named("ConceptCave.sql.DacPac");

            // The list of SQL Scripts to run BEFORE schema update
            Bind<Func<IEnumerable<IUpdateScript>>>().ToMethod(ctx => () =>
            {
                return GetUpdateScripts(@"~/App_GlobalResources/Data/BeforeSchema", "Before Schema: ");
            }).Named("BeforeSchemaUpdateScripts");

            // The list of SQL Scripts to run AFTER schema update
            // Note - AFTER scripts have always been around, the BEFORE scripts are a new addition and we need
            // to make sure they are name spaced as to not clash, so the BEFORE scripts a prepended with "Before Schema:"
            // whilst AFTER scripts are not - they need to keep their name otherwise they will be applied a 2nd time since
            // they will be detected as a new script
            Bind<Func<IEnumerable<IUpdateScript>>>().ToMethod(ctx => () =>
            {
                return GetUpdateScripts(@"~/App_GlobalResources/Data", "");     // Note no prepend 
            }).Named("AfterSchemaUpdateScripts");

        }

        private static IEnumerable<IUpdateScript> GetUpdateScripts(string scriptsPath, string prependName)
        {
            List<UpdateScript> scripts = new List<UpdateScript>();
            foreach (string file in Directory.EnumerateFiles(System.Web.Hosting.HostingEnvironment.MapPath(scriptsPath), "*.sql"))
            {
                int order = 0;
                if (int.TryParse(Path.GetFileNameWithoutExtension(file), out order))
                {
                    var content = File.ReadAllLines(file);
                    var name = Path.GetFileName(file);
                    if (content[0].Trim().StartsWith("--"))
                    {
                        name = content[0].Trim().Substring(2).Trim();
                    }

                    var updateScript = new UpdateScript()
                    {
                        Order = order,
                        ScriptName = string.Format("{0}{1} - {2}", prependName, order, name),
                        ScriptText = string.Join("\n", content)
                    };
                    scripts.Add(updateScript);
                }
            }
            return scripts.OrderBy(s => s.Order);
        }
    }
}