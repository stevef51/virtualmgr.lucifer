using System.Collections.Generic;
using System.Web.Http.Dependencies;
using System.Web.Mvc;
using System.Web.Routing;
using ConceptCave.Checklist;
using Ninject.Syntax;
using IDependencyResolver = System.Web.Http.Dependencies.IDependencyResolver;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(ConceptCave.www.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(ConceptCave.www.App_Start.NinjectWebCommon), "Stop")]

namespace ConceptCave.www.App_Start
{
    using System;
    using System.Threading;
    using System.Web;

    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;
    using NLog;

    public static class NinjectWebCommon 
    {
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        public static readonly CancellationTokenSource ApplicationRunning = new CancellationTokenSource();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            SD.LLBLGen.Pro.ORMSupportClasses.RuntimeConfiguration.AddConnectionString("VirtualMgr.Central", System.Configuration.ConfigurationManager.ConnectionStrings["VirtualMgr.Central"].ConnectionString);

            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            ApplicationRunning.Cancel();
            bootstrapper.ShutDown();
        }

        public static IKernel TheKernel 
        { 
            get
            {
                return bootstrapper.Kernel;
            }
        }

        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            LoggingStopWatch lsw = new LoggingStopWatch("CreateKernel", null);
            lsw.Mark("Creating modules");
            var kernel = new StandardKernel(new ServicesNinjectModule(), new AzureNinjectModule(), new ResourcesNinjectModule(), new VirtualMgr.Membership.NinjectModule());
            kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => kernel);
            kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

            lsw.Mark("Dependency Resolver");
            var resolver = new NinjectDependencyResolver(kernel);
            System.Web.Http.GlobalConfiguration.Configuration.DependencyResolver = resolver;
            System.Web.Mvc.ControllerBuilder.Current.SetControllerFactory(new NinjectMVCControllerFactory(resolver));

            lsw.Log(NLog.LogLevel.Info);
            return kernel;
        }
    }

    //This guy is basically a singleton that uses the asp.net DI
    //to instantiate types
    public class AspNetReflectionFactory : IReflectionFactory, IServiceProvider
    {
        private static AspNetReflectionFactory _current = null;
        private static readonly object Currentlock = new object();
        public static AspNetReflectionFactory Current
        {
            get
            {
                lock (Currentlock)
                {
                    return _current ?? (_current = new AspNetReflectionFactory());
                }
            }
        }

        public T InstantiateObject<T>()
        {
            return (T) InstantiateObject(typeof (T));
        }

        public object InstantiateObject(Type type)
        {
            //Do we have a ninject dep resolver?
            var resolver = System.Web.Http.GlobalConfiguration.Configuration.DependencyResolver;
            var ninjectResolver = resolver as NinjectDependencyResolver;

            var service = resolver.GetService(type);
            if (service != null) return service; //all is good with the world

            //or, it is not good. Can we Activator.CreateInstance?
            try
            {
                service = Activator.CreateInstance(type);
            }
            catch (Exception e)
            {
                //We did not succeed. The error message in e will be kind of useless
                //What we actually want to do is throw the result of calling a checked ninjectresolver if we can
                if (ninjectResolver != null)
                {
                    service = ninjectResolver.GetService(type); //might throw
                }
                else
                {
                    //no choice but to use the exception we got
                    throw;
                }
            }

            return service;
        }

        public object GetService(Type serviceType)
        {
            return InstantiateObject(serviceType);
        }
    }

    public class NinjectDependencyScope : IDependencyScope
    {
        public IResolutionRoot Resolver;

        public NinjectDependencyScope(IResolutionRoot resolver)
        {
            Resolver = resolver;
        }

        public object GetService(Type serviceType)
        {
            if (Resolver == null)
                throw new ObjectDisposedException("this", "This scope has been disposed");
            return Resolver.TryGet(serviceType);
        }

        public object GetServiceChecked(Type serviceType)
        {
            if (Resolver == null)
                throw new ObjectDisposedException("this", "This scope has been disposed");
            return Resolver.Get(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            if (Resolver == null)
                throw new ObjectDisposedException("this", "This scope has been disposed");
            return Resolver.GetAll(serviceType);
        }

        public void Dispose()
        {
            var disposable = Resolver as IDisposable;
            if (disposable != null)
                disposable.Dispose();
            Resolver = null;
        }
    }

    public class NinjectDependencyResolver : NinjectDependencyScope, IDependencyResolver
    {
        private readonly IKernel _kernel;

        public NinjectDependencyResolver(IKernel kernel) : base(kernel)
        {
            _kernel = kernel;
        }

        public IDependencyScope BeginScope()
        {
            return new NinjectDependencyScope(_kernel.BeginBlock());
        }
    }

    public class NinjectMVCControllerFactory : DefaultControllerFactory
    {
        private readonly NinjectDependencyResolver _resolver;
        private readonly IControllerFactory _innerFactory;
        private readonly IDictionary<IController, IDependencyScope> _scopes; 

        public NinjectMVCControllerFactory(NinjectDependencyResolver resolver)
        {
            _resolver = resolver;
            _innerFactory = new DefaultControllerFactory();
            _scopes = new Dictionary<IController, IDependencyScope>();
        }

        protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
        {
            if (controllerType == null)
                return null;
            var scope = _resolver.BeginScope();
            var ctrl = (IController)scope.GetService(controllerType);
            _scopes.Add(ctrl, scope);
            return ctrl;
        }

        public override void ReleaseController(IController controller)
        {
            IDependencyScope scope;
            if (_scopes.TryGetValue(controller, out scope))
            {
                scope.Dispose();
                _scopes.Remove(controller);
            }
            base.ReleaseController(controller);
        }
    }
}
