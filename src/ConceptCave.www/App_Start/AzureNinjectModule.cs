﻿using System;
using System.Configuration;
using Ninject;
using Microsoft.Azure.Management.Fluent;

namespace ConceptCave.www.App_Start
{
    public class AzureNinjectModule : Ninject.Modules.NinjectModule
    {
        public override void Load()
        {
/*            Bind<AzureEnvironment>().ToConstant(AzureEnvironment.PublicEnvironments[EnvironmentName.AzureCloud]);

            Bind<Microsoft.Azure.TokenCloudCredentials>().ToMethod(ctx =>
            {
                var tenantId = ConfigurationManager.AppSettings["AzureManagementTenantId"];
                var clientId = ConfigurationManager.AppSettings["AzureManagementClientId"];
                var clientSecret = ConfigurationManager.AppSettings["AzureManagementClientSecret"];
                var subscription = ConfigurationManager.AppSettings["AzureSubscriptionId"];

                var environment = ctx.Kernel.Get<AzureEnvironment>();
                var authority = String.Format("{0}{1}", environment.Endpoints[AzureEnvironment.Endpoint.ActiveDirectory], tenantId);
                var authContext = new AuthenticationContext(authority);
                var credential = new ClientCredential(clientId, clientSecret);
                var authResult = authContext.AcquireToken(environment.Endpoints[AzureEnvironment.Endpoint.ActiveDirectoryServiceEndpointResourceId], credential);
                return new Microsoft.Azure.TokenCloudCredentials(subscription, authResult.AccessToken);
            });


            // Hook up the AzureManagement certificate
            Bind<X509Certificate2>().ToMethod(ctx =>
            {
                var path = ConfigurationManager.AppSettings["AzureManagementCertificatePath"];
                path = System.Web.Hosting.HostingEnvironment.MapPath(path);
                var pwd = ConfigurationManager.AppSettings["AzureManagementCertificatePassword"];
                return new X509Certificate2(path, pwd, X509KeyStorageFlags.MachineKeySet);
            }).InSingletonScope().Named("AzureManagement");

            Bind<Microsoft.Azure.CertificateCloudCredentials>().ToMethod(ctx => new Microsoft.Azure.CertificateCloudCredentials(ConfigurationManager.AppSettings["AzureSubscriptionId"], ctx.Kernel.Get<X509Certificate2>("AzureManagement"))).InSingletonScope();
            Bind<Microsoft.WindowsAzure.SubscriptionCloudCredentials>().ToMethod(ctx => new Microsoft.WindowsAzure.CertificateCloudCredentials(ConfigurationManager.AppSettings["AzureSubscriptionId"], ctx.Kernel.Get<X509Certificate2>("AzureManagement"))).InSingletonScope().Named("AzureManagement");
            Bind<Microsoft.Azure.SubscriptionCloudCredentials>().ToMethod(ctx => 
                new Microsoft.Azure.TokenCloudCredentials(
                    ConfigurationManager.AppSettings["AzureSubscriptionId"],
                    ctx.Kernel.Get<Microsoft.Azure.TokenCloudCredentials>().Token)
            ).Named("AzureManagement");
            //            kernel.Bind<Microsoft.WindowsAzure.SubscriptionCloudCredentials>().ToMethod(ctx => new Microsoft.WindowsAzure.TokenCloudCredentials(ConfigurationManager.AppSettings["AzureSubscriptionId"], ctx.Kernel.Get<string>("AzureAuthenticationToken"))).Named("AzureManagement");

            Bind<Microsoft.Rest.ServiceClientCredentials>().ToMethod(ctx => new Microsoft.Rest.TokenCredentials(ctx.Kernel.Get<Microsoft.Azure.TokenCloudCredentials>().Token));

            Bind<Func<SqlManagementClient>>().ToMethod(ctx => () => new SqlManagementClient(ctx.Kernel.Get<Microsoft.Azure.CertificateCloudCredentials>()));
            Bind<Func<ResourceManagementClient>>().ToMethod(ctx => () =>
            {
                var client = new ResourceManagementClient(ctx.Kernel.Get<Microsoft.Rest.ServiceClientCredentials>("AzureManagement"));
                client.SubscriptionId = ConfigurationManager.AppSettings["AzureSubscriptionId"];
                return client;
            });

            Bind<Microsoft.WindowsAzure.Storage.Auth.StorageCredentials>().ToMethod(ctx => new Microsoft.WindowsAzure.Storage.Auth.StorageCredentials(ConfigurationManager.AppSettings["AzureStorageAccountName"], ConfigurationManager.AppSettings["AzureStorageKeyValue"]));
            Bind<Microsoft.WindowsAzure.Storage.CloudStorageAccount>().ToMethod(ctx => new Microsoft.WindowsAzure.Storage.CloudStorageAccount(ctx.Kernel.Get<Microsoft.WindowsAzure.Storage.Auth.StorageCredentials>(), true));
            */
            Bind<ConceptCave.www.Areas.Admin.Controllers.MultiTenantAdminConfig>().ToConstant(
                new ConceptCave.www.Areas.Admin.Controllers.MultiTenantAdminConfig()
                {
                    DNSZone = new Areas.Admin.Controllers.ResourceGroupItem(ConfigurationManager.AppSettings["AzureDNSZone"])
                });
            /*
            Bind<Func<DnsManagementClient>>().ToMethod(ctx => () =>
            {
                return new DnsManagementClient(ctx.Kernel.Get<Microsoft.Azure.SubscriptionCloudCredentials>("AzureManagement"));
            });

            // ARM WebSite API
            Bind<Func<Microsoft.Azure.Management.WebSites.WebSiteManagementClient>>().ToMethod(ctx => () =>
            {
                var environment = ctx.Kernel.Get<AzureEnvironment>();
                return new Microsoft.Azure.Management.WebSites.WebSiteManagementClient(environment.GetEndpointAsUri(AzureEnvironment.Endpoint.ResourceManager), ctx.Kernel.Get<Microsoft.Rest.ServiceClientCredentials>())
                {
                    SubscriptionId = ConfigurationManager.AppSettings["AzureSubscriptionId"]
                };
            });

            // Classic WebSite API
            Bind<Func<Microsoft.WindowsAzure.Management.WebSites.WebSiteManagementClient>>().ToMethod(ctx => () =>
                {
                    return new Microsoft.WindowsAzure.Management.WebSites.WebSiteManagementClient(ctx.Kernel.Get<Microsoft.Azure.SubscriptionCloudCredentials>());
                });
*/

            Bind<Func<IAzure>>().ToMethod(ctx => () =>
            {
                var creds = new Microsoft.Azure.Management.ResourceManager.Fluent.Authentication.AzureCredentials(
                    new Microsoft.Azure.Management.ResourceManager.Fluent.Authentication.ServicePrincipalLoginInformation()
                    {
                        ClientId = ConfigurationManager.AppSettings["AzureManagementClientId"],
                        ClientSecret = ConfigurationManager.AppSettings["AzureManagementClientSecret"]
                    },

                    ConfigurationManager.AppSettings["AzureManagementTenantId"],
                    Microsoft.Azure.Management.ResourceManager.Fluent.AzureEnvironment.AzureGlobalCloud);

                try
                {
                    var azure = Azure.Configure().Authenticate(creds).WithSubscription(ConfigurationManager.AppSettings["AzureSubscriptionId"]);

                    return azure;
                }
                catch (Exception ex)
                {
                    return null;
                }
            });
        }
    }
}