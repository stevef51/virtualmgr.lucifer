﻿-- Drop old FMA tables

IF OBJECT_ID('asset.tblBeaconLocation', 'U') IS NOT NULL
	DROP TABLE asset.tblBeaconLocation

IF OBJECT_ID('asset.tblBeaconSession', 'U') IS NOT NULL
	DROP TABLE asset.tblBeaconSession

IF COL_LENGTH('asset.tblTabletLocationHistory','IndoorAtasLocationId') IS NOT NULL
	ALTER TABLE asset.tblTabletLocationHistory DROP COLUMN IndoorAtlasLocationId

IF COL_LENGTH('asset.tblTabletLocationHistory','StaticBeaconLocationId') IS NOT NULL
	ALTER TABLE asset.tblTabletLocationHistory DROP COLUMN StaticBeaconLocationId

IF COL_LENGTH('asset.tblTabletLocationHistory','TaskLocationId') IS NOT NULL
	ALTER TABLE asset.tblTabletLocationHistory DROP COLUMN TaskLocationId

IF COL_LENGTH('asset.tblTabletLocationHistory','BestEstimateLocationId') IS NOT NULL
	ALTER TABLE asset.tblTabletLocationHistory DROP COLUMN BestEstimateLocationId

