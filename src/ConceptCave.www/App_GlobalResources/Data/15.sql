﻿MERGE INTO Roles t USING (VALUES 
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', '6fb872fa-c3c2-46ca-b8d6-6e255d232483', 'TaskDelete')
	) AS s (ApplicationId, RoleId, RoleName) ON t.ApplicationId = s.ApplicationId AND t.RoleId = s.RoleId
	WHEN MATCHED THEN UPDATE SET RoleName = s.RoleName
	WHEN NOT MATCHED THEN INSERT (ApplicationId, RoleId, RoleName) VALUES (s.ApplicationId, s.RoleId, s.RoleName);