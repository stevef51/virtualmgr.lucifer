﻿-- EVS-668 Email white list configuration in TenantEmailConfiguration can't be enabled properly

SET IDENTITY_INSERT [mtc].[tblTenantProfiles] ON

MERGE INTO [mtc].[tblTenantProfiles] tgt USING (VALUES 
(1,N'Development',N'For Development only',1,1,N'*@virtualmgr.*',1,0,NULL,1,1),
(2,N'Staging',N'For staging new features',0,1,N'*@virtualmgr.*',1,0,NULL,1,1),
(3,N'Demo',N'For Demos only',1,1,N'*@virtualmgr.*',1,0,NULL,1,1),
(4,N'Production',N'For live clients',0,1,N'',1,0,N'This will create a Production system, are you sure?',1,1),
(5,N'Tenant Source', N'Soley for setting up base point for new clients to clone from',0,1,N'*@virtualmgr.*',1,0,NULL,1,1),
(6,N'Tenant Master',N'The Tenant Master',0,0,N'',0,1,NULL,0,0)
) AS src (Id,[Name],[Description],CanDelete,EmailEnabled,EmailWhitelist,CanClone,Hidden,CreateAlertMessage,CanUpdateSchema,CanDisable) ON (tgt.id = src.id)
WHEN MATCHED THEN UPDATE SET 
	tgt.Name=src.Name,
	tgt.[Description]=src.[Description],
	tgt.CanDelete=src.CanDelete,
	tgt.EmailEnabled=src.EmailEnabled,
	tgt.EmailWhiteList=src.EmailWhiteList,
	tgt.CanClone=src.CanClone,
	tgt.[Hidden]=src.[Hidden],
	tgt.CreateAlertMessage=src.CreateAlertMessage,
	tgt.CanUpdateSchema=src.CanUpdateSchema,
	tgt.CanDisable=src.CanDisable
WHEN NOT MATCHED BY TARGET THEN INSERT (Id,[Name],[Description],CanDelete,EmailEnabled,EmailWhitelist,CanClone,Hidden,CreateAlertMessage,CanUpdateSchema,CanDisable) 
VALUES (src.Id,src.[Name],src.[Description],src.CanDelete,src.EmailEnabled,src.EmailWhitelist,src.CanClone,src.Hidden,src.CreateAlertMessage,src.CanUpdateSchema,src.CanDisable)
WHEN NOT MATCHED BY SOURCE THEN DELETE;

SET IDENTITY_INSERT [mtc].[tblTenantProfiles] OFF
