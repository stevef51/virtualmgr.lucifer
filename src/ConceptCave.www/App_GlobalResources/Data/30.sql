﻿MERGE INTO [stock].tblProductType p USING (VALUES 
		(1, 'Item', 1),
		(2, 'Length', 2),
		(3, 'Area', 3),
		(4, 'Volume', 4),
		(5, 'Length * Width', 5),
		(6, 'Facility Structure Rental', 6)
	) AS s ([Id], [Name], Units) ON p.Id = s.Id
	WHEN MATCHED THEN UPDATE SET Units = p.Units
	WHEN NOT MATCHED THEN INSERT (Id, [Name], Units) VALUES (s.Id, s.[Name], s.Units);