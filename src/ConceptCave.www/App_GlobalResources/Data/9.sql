﻿-- Set's up roster event types with timed event type

MERGE INTO [gce].[tblRosterProjectJobTaskEvent] t USING (VALUES 
		(1, 'Schedule Approval Automatic', 0),
		(2, 'End of Day Automatic', 0),
		(3, 'Time', 1)
	) AS s (Id, [Name], [Timed]) ON t.Id = s.Id
	WHEN MATCHED THEN UPDATE SET [Id] = s.[Id], Name = s.Name, Timed = s.Timed
	WHEN NOT MATCHED THEN INSERT (Id, [Name], Timed) VALUES (s.Id, s.[Name], s.Timed);