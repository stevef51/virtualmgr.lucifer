﻿-- Set Tenant Profile Domain Append field + add DeleteTenant role

MERGE INTO Roles t USING (VALUES 
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', 'B4659744-B889-43D6-8663-DCB7A4BE3655', 'DeleteTenant')
	) AS s (ApplicationId, RoleId, RoleName) ON t.ApplicationId = s.ApplicationId AND t.RoleId = s.RoleId
	WHEN MATCHED THEN UPDATE SET RoleName = s.RoleName
	WHEN NOT MATCHED THEN INSERT (ApplicationId, RoleId, RoleName) VALUES (s.ApplicationId, s.RoleId, s.RoleName);

SET IDENTITY_INSERT [mtc].[tblTenantProfiles] ON

-- Add in Quality Assurance Tenant Profile
MERGE INTO [mtc].[tblTenantProfiles] tgt USING (VALUES 
(1,N'Development',N'For Development only',1,1,N'*@virtualmgr.*',1,0,NULL,1,1,N'-dev',N'-development'),
(2,N'Staging',N'For staging new features',1,1,N'*@virtualmgr.*',1,0,NULL,1,1,N'-stg',N'-staging'),
(3,N'Demo',N'For Demos only',1,1,N'*@virtualmgr.*',1,0,NULL,1,1,N'-demo',N'-demo'),
(4,N'Production',N'For live clients',0,1,N'',1,0,N'This will create a Production system, are you sure?',1,1,N'',N'-production'),
(5,N'Tenant Source', N'Soley for setting up base point for new clients to clone from',0,1,N'*@virtualmgr.*',1,0,NULL,1,1,N'-src','-tenant-source'),
(6,N'Tenant Master',N'The Tenant Master',0,0,N'',0,1,NULL,0,0,N'',N'-tenant-master'),
(7,N'Quality Assurance',N'For QA Testing',1,1,N'*@virtualmgr.*',1,0,NULL,1,1,N'-qa',N'-qa')
) AS src (Id,[Name],[Description],CanDelete,EmailEnabled,EmailWhitelist,CanClone,Hidden,CreateAlertMessage,CanUpdateSchema,CanDisable,DomainAppend,DatabaseAppend) ON (tgt.id = src.id)
WHEN MATCHED THEN UPDATE SET 
	tgt.Name=src.Name,
	tgt.[Description]=src.[Description],
	tgt.CanDelete=src.CanDelete,
	tgt.EmailEnabled=src.EmailEnabled,
	tgt.EmailWhiteList=src.EmailWhiteList,
	tgt.CanClone=src.CanClone,
	tgt.[Hidden]=src.[Hidden],
	tgt.CreateAlertMessage=src.CreateAlertMessage,
	tgt.CanUpdateSchema=src.CanUpdateSchema,
	tgt.CanDisable=src.CanDisable,
	tgt.DomainAppend=src.DomainAppend,
	tgt.DatabaseAppend=src.DatabaseAppend
WHEN NOT MATCHED BY TARGET THEN INSERT (Id,[Name],[Description],CanDelete,EmailEnabled,EmailWhitelist,CanClone,Hidden,CreateAlertMessage,CanUpdateSchema,CanDisable,DomainAppend,DatabaseAppend) 
VALUES (src.Id,src.[Name],src.[Description],src.CanDelete,src.EmailEnabled,src.EmailWhitelist,src.CanClone,src.Hidden,src.CreateAlertMessage,src.CanUpdateSchema,src.CanDisable,src.DomainAppend,src.DatabaseAppend)
WHEN NOT MATCHED BY SOURCE THEN DELETE;

SET IDENTITY_INSERT [mtc].[tblTenantProfiles] OFF

-- Default TargetProfileId to current Profile 
UPDATE mtc.tblTenants SET TargetProfileId = TenantProfileId WHERE TargetProfileId IS NULL

