﻿-- EVS-689 Kiosk Mode, add TabletAdmin role, setup TabletProfile table
MERGE INTO Roles t USING (VALUES 
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', '0BD27B6B-31CD-45C9-8A8D-798A348FAD35', 'TabletAdmin')
	) AS s (ApplicationId, RoleId, RoleName) ON t.ApplicationId = s.ApplicationId AND t.RoleId = s.RoleId
	WHEN MATCHED THEN UPDATE SET RoleName = s.RoleName
	WHEN NOT MATCHED THEN INSERT (ApplicationId, RoleId, RoleName) VALUES (s.ApplicationId, s.RoleId, s.RoleName);

SET IDENTITY_INSERT [asset].[tblTabletProfile] ON

MERGE INTO asset.tblTabletProfile tp USING (VALUES 
		(1, 'Kiosk', 15, 30, 1),
		(2, 'Normal', NULL, NULL, 0)
	) AS s (Id, Name, AutoLogoutShortSecs, AutoLogoutLongSecs, AllowNumberPadLogin) ON tp.Id = s.Id 
	WHEN MATCHED THEN UPDATE SET Name = s.Name, AutoLogoutShortSecs = s.AutoLogoutShortSecs, AutoLogoutLongSecs = s.AutoLogoutLongSecs, AllowNumberPadLogin = s.AllowNumberPadLogin
	WHEN NOT MATCHED THEN INSERT (Id, Name, AutoLogoutShortSecs, AutoLogoutLongSecs, AllowNumberPadLogin) VALUES (s.Id, s.Name, s.AutoLogoutShortSecs, s.AutoLogoutLongSecs, s.AllowNumberPadLogin);

SET IDENTITY_INSERT [asset].[tblTabletProfile] OFF

