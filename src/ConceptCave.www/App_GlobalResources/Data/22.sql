﻿-- Fix for EVS-1209

begin tran
	truncate table [dw].fact_tasks
	delete from [dw].tabletracking
	delete from [dw].Dim_Dates
	delete from [dw].Dim_DateTimes
commit tran