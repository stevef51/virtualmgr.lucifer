﻿-- EVS-1304 Clearing out FMA beacon sightings ready for new update #3

BEGIN TRAN

DELETE FROM asset.tblBeaconSighting
DELETE FROM asset.tblTabletLocationHistory
UPDATE asset.tblBeacon SET LastGPSLocationId = NULL WHERE LastGPSLocationId IS NOT NULL
UPDATE asset.tblTabletUUID SET LastGPSLocationId = NULL WHERE LastGPSLocationId IS NOT NULL
DELETE FROM asset.tblGPSLocation WHERE ID NOT IN (SELECT StaticLocationId FROM asset.tblBeacon WHERE StaticLocationId IS NOT NULL)

COMMIT
