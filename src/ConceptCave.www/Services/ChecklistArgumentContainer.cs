﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;
using ConceptCave.Core.Coding;

namespace ConceptCave.www.Services
{
    public class ChecklistArgumentContainer : IChecklistArgumentContainer
    {
        public ChecklistArgumentContainer(HttpContextBase httpctx)
        {
            Arguments = new Dictionary<string, object>(StringComparer.InvariantCultureIgnoreCase);
            foreach (var getarg in httpctx.Request.Params.AllKeys)
            {
                Arguments[getarg] = StringToX(httpctx.Request.Params[getarg]);
            }
        }

        public IDictionary<string, object> Arguments
        {
            get;
            private set;
        }

        private object StringToX(string value)
        {
            Func<object>[] delegates = {
                                           () => int.Parse(value),
                                           () => Int64.Parse(value),
                                           () => decimal.Parse(value),
                                           () => Guid.Parse(value),
                                           () => bool.Parse(value),
                                           () => value
                                       };
            Stack<Func<object>> delegateStack = new Stack<Func<object>>(delegates.Reverse());
            while (true) //the last delegate in the stack will always not throw.
            {
                try
                {
                    return delegateStack.Pop()();
                }
                catch (Exception) { }
            }
        }
    }
}