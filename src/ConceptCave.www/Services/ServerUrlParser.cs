﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using ConceptCave.BusinessLogic.Configuration;
using ConceptCave.RepositoryInterfaces;
using VirtualMgr.Central.Interfaces;

namespace ConceptCave.www.Services
{
    public class ServerUrlParser : IUrlParser, IServerUrls
    {
        private static ITenantContextProvider _tenantContextProvider;
        private Func<string> _fnJsReportServerUrl;

        public ServerUrlParser(ITenantContextProvider tenantContextProvider, Func<string> fnJsReportServerUrl)
        {
            _tenantContextProvider = tenantContextProvider;
            _fnJsReportServerUrl = fnJsReportServerUrl;
        }

        public string ResolveVirtualPath(string path)
        {
            var httpctx = HttpContext.Current;
            var routeData = new RouteData();
            var httpctxbase = new HttpContextWrapper(httpctx);
            var reqCtx = new RequestContext(httpctxbase, routeData);

            var helper = new UrlHelper(reqCtx);
            return helper.ToFullyQualifiedUrl(path);
        }

        public string ReportingUrl
        {
            get { return GetReportingUrl(); }
        }

        public static string GetReportingUrl()
        {
            var uri = new Uri(BaseUri(), "Reporting");
            return uri.ToString();
        }

        public string ODataUrl
        {
            get { return GetODataUrl(); }
        }

        public string JsReportServerUrl { get => _fnJsReportServerUrl(); }

        public static string GetODataUrl()
        {
            var uri = new Uri(BaseUri(), "OData/OData-v3");
            return uri.ToString();
        }
        private static Uri BaseUri()
        {
            try
            {
                return new Uri(HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority));
            }
            catch
            {
                // Not running in HttpContext, assume HTTP and rely on a HTTPS redirect if required
                return new Uri(string.Format("http://{0}", _tenantContextProvider.GetCurrentTenant().HostName));
            }
        }

    }
}