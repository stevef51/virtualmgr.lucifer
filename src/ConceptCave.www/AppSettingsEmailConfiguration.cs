﻿using ConceptCave.Checklist.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Web;

namespace ConceptCave.www
{
    public class AppSettingsEmailConfiguration : IEmailConfiguration
    {
        private bool _emailEnabled;
        private string[] _whiteListAddresses;

        public AppSettingsEmailConfiguration()
        {
            bool.TryParse(System.Configuration.ConfigurationManager.AppSettings["EmailEnabled"], out _emailEnabled);
            var addresses = System.Configuration.ConfigurationManager.AppSettings["EmailWhiteList"];
            if (addresses.Length == 0)
                addresses = null;
            _whiteListAddresses = addresses != null ? addresses.Split(',') : null;
        }

        public bool Enabled
        {
            get { return _emailEnabled; }
        }

        public string FilterAddress(string address)
        {
            address = address.Replace(";", ",").Trim(',', ' ');

            if (_whiteListAddresses == null)
                return address;

            var addresses = address.Split(',');
            var filtered = new List<string>();
            foreach(var a in addresses)
            {
                var saneAddress = a.ToLower().Trim();
                var ok = false;
                foreach(var wla in _whiteListAddresses)
                {
                    var saneWla = wla.ToLower().Trim();
                    var rex = Regex.Escape( saneWla ).Replace( @"\*", ".*" ).Replace( @"\?", "." ).Replace(@"#", @"\d");
                    var reg = new Regex(rex);

                    if (reg.IsMatch(saneAddress))
                    {
                        ok = true;
                        break;
                    }
                }
                if (ok)
                    filtered.Add(a);
            }
            return string.Join(",", filtered);
        }


        public string SubjectAppend
        {
            get { return null; }
        }

        public MailAddress From
        {
            get { return null; }
        }
    }
}