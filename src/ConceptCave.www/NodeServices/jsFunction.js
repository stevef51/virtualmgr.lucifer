﻿
const Eval = require('eval');
const Promise = require('promise');

var _jsFuncsByName;
var _allJsFuncs;

function init(callback, allJsFuncs) {
    // allJsFuncs is an array expected to be of the form
    // [{ id: 1, name: 'func1', type: 1, script: function (a, b, c...) {
    //   ...
    //   return result;
    // }, id: 2, name: 'func2', type: 1, script: function ...
    console.debug('jsFunction.init');
    if (_allJsFuncs !== allJsFuncs) {
        _allJsFuncs = allJsFuncs;
        _jsFuncsByName = {};

        var errors = [];
        allJsFuncs.forEach(function (i) {
            try {
                var fn = Eval(i.script, '/jsFunc/' + i.name, null, true);
                _jsFuncsByName[i.name] = fn;
            } catch (e) {
                errors.push({
                    jsFuncName: i.name,
                    error: {
                        code: e.code,
                        message: e.message,
                        stack: e.stack
                    }
                });
            }
        });

        if (errors.length) {
            console.log(`jsFunction.init: ${JSON.stringify(errors)}`);
            return callback(JSON.stringify(errors));
        } else {
            console.log('jsFunction.init: compiled ' + allJsFuncs.length);
        }
    }

    return callback(null, true);
}

function execute(callback, jsFuncName, wrappedArgs) {
    if (_jsFuncsByName === undefined) {
        return callback(null, { runInit: true });
    }

    var fn = _jsFuncsByName[jsFuncName];
    if (fn === undefined) {
        console.warn(`jsFunction.execute("${jsFuncName}") -> NOT FOUND`);
        return callback(`jsFunc not found`);
    } 
    console.debug(`execute ${jsFuncName}(${JSON.stringify(wrappedArgs)})`);

    try {
        var r = fn.call(
            _jsFuncsByName,
            wrappedArgs.args,
            wrappedArgs.context, {
                console: console
            });

        var p = Promise.resolve(r);

        p.then(function (result) {
            callback(null, {
                result: result
            });
        }, function (error) {
            console.error(`error ${jsFuncName} ${JSON.stringify(error)}`);
            callback(error);
        });
    } catch (e) {
        console.error(`error ${jsFuncName} ${JSON.stringify(e)}`);
        return callback(e);
    }
}

module.exports = {
    init: init,
    execute: execute
};
