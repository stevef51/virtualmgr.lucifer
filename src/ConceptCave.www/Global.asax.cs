﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Routing;
using System.Text;
using ConceptCave.BusinessLogic.Configuration;
using ConceptCave.Core.XmlCodable;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.www.App_Start;
using ConceptCave.www.Squisher;
using ConceptCave.www.ThemeVirtualProvider;
using Microsoft.Web.Mvc;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Repository;
using ConceptCave.Configuration;
using ConceptCave.Data.HelperClasses;
using System.Collections.Specialized;
using System.Web.Security;
using System.Diagnostics;
using ConceptCave.www.Services;
using System.Web.Http;
using ConceptCave.www.API.Support;
using System.Web.Configuration;
using SquishIt.Framework;
using System.Web.Http.Hosting;
using ConceptCave.Checklist;
using ConceptCave.Core.SBONCodable;
using ConceptCave.www.Areas.Admin.Controllers;
using VirtualMgr.Central;
using System.Threading.Tasks;
using Ninject;
using System.IO;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.DTO.DTOClasses;
using System.Threading;
using VirtualMgr.Central.Interfaces;
using System.Web.Http.Dispatcher;
using System.Web.Http.Controllers;
using ConceptCave.www.API.v1;
using NLog;

namespace ConceptCave.www
{
    public class LoggingStopWatch
    {
        private readonly Logger _logger;
        private string _overall;
        private readonly Stopwatch _overallStopwatch = new Stopwatch();
        private readonly Stopwatch _markStopwatch = new Stopwatch();
        private string _lastMark = null;
        private List<string> _logs = new List<string>();
        private static List<string> _globalLogs = new List<string>();

        public LoggingStopWatch(string overall, Logger logger)
        {
            _overall = overall;
            _logger = logger;
            _overallStopwatch.Start();
        }

        public void Mark(string text)
        {
            if (_lastMark != null)
            {
                _logs.Add(string.Format("{0} took {1}ms", _lastMark, _markStopwatch.ElapsedMilliseconds));
            }
            _markStopwatch.Restart();
            _lastMark = text;
        }

        public void Log(LogLevel level)
        {
            _logs.Add(string.Format("{0} took {1}ms", _lastMark, _markStopwatch.ElapsedMilliseconds));
            _logs.Add(string.Format("Overall {0} took {1}ms", _overall, _overallStopwatch.ElapsedMilliseconds));
            var allLogs = string.Join("\n\r", _logs);

            if (_logger == null)
                _globalLogs.AddRange(_logs);
            else
            {
                if (_globalLogs != null)
                {
                    allLogs = string.Join("\n\r", _globalLogs.Concat(_logs));
                    _globalLogs = null;
                }
                _logger.Log(level, allLogs);
            }
        }
    }

    public static class MultiTenantInit
    {
        private static bool _started = false;

        public static void StartUp()
        {
            if (!_started)
            {
                _started = true;

                MultiTenantManager.TenantMasterConnectionString = ConfigurationManager.ConnectionStrings["VirtualMgr.Central"].ConnectionString;
                MultiTenantManager.TenantDataAccessAdapterType = typeof(ConceptCave.Data.DatabaseSpecific.DataAccessAdapter);
                MultiTenantManager.TenantContextProvider = new VirtualMgr.Central.MultiTenantContextProvider(() => System.Web.HttpContext.Current.Request.Url.Host.ToLower());


                //    Func<ConnectionStringSettings> fnConnectionStringSettings = () => new ConnectionStringSettings(MultiTenantManager.CurrentTenant.Name, MultiTenantManager.CurrentTenant.ConnectionString);

                //    VirtualMgr.Membership.Direct.DatabaseConnectionManager.FnGetConnectionStringSettings = fnConnectionStringSettings;

                //    MultiTenantManager.TenantMasterConnectionString = ConfigurationManager.ConnectionStrings["VirtualMgr.Central"].ConnectionString;                
                //    MultiTenantManager.TenantDataAccessAdapterType = typeof(ConceptCave.Data.DatabaseSpecific.DataAccessAdapter);
                //    MultiTenantManager.TenantContextProvider = new VirtualMgr.Central.MultiTenantContextProvider(() => System.Web.HttpContext.Current.Request.Url.Host.ToLower());
            }
        }
    }

    public class MembershipConfigOverridesSource : IConfigOverridesSource
    {
        public NameValueCollection GetOverrides()
        {
            // we have to set this here as the initial call to the membership provider occurs before app start (sigh)
            MultiTenantInit.StartUp();

            string overridesString = GlobalSettingsRepository.GetTenantMasterSetting<string>("MembershipProvider.Overrides");

            if (string.IsNullOrEmpty(overridesString))
            {
                return new NameValueCollection();
            }

            // ok we are expecting a JSON string, so for example to override the min passwordlength and min alpha chars we'd have
            // { "minRequiredPasswordLength": "3", "minRequiredNonalphanumericCharacters": "4" }
            // the keys in the JSON are the same as those in the web.config
            var overridesJ = Newtonsoft.Json.Linq.JObject.Parse(overridesString);

            NameValueCollection result = new NameValueCollection();

            foreach (var p in overridesJ)
            {
                result.Add(p.Key, p.Value.ToString());
            }

            return result;
        }
    }

    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();

        //private IMembershipRepository _membershipRepo;

       private IMembershipRepository MembershipRepo
        {
            get
            {
                var kernel = NinjectWebCommon.TheKernel;
                return kernel.Get<IMembershipRepository>();
            }
        }

        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes, IKernel kernel)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("quartz/{*pathInfo}");

            routes.MapHttpRoute("API route",
                "API/v1/{controller}/{action}/{id}",
                new {action = "Index", id = RouteParameter.Optional }, null,
                new BasicAuthMessageHandler(GlobalConfiguration.Configuration, kernel.Get<Func<VirtualMgr.Membership.IMembership>>(), kernel.Get<Func<Checklist.Interfaces.ITokenValidator>>()) //Allow basic auth on this route
            );

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );

            // add routing for the theme side of things
            routes.MapRoute("cdn-theme", "cdn/theme/{*current}", new { controller = "Cdn", action = "Index" });

            routes.MapHttpRoute(
                name: "Error404",
                routeTemplate: "{*url}",
                defaults: new { controller = "Error", action = "Handle404" }
            );
        }

#if DEBUG
        //private void WSFederationAuthenticationModule_RedirectingToIdentityProvider(object sender, RedirectingToIdentityProviderEventArgs e)
        //{
        //    // for doing development we'll override the settings in the web.config and force this to go against the localhost relying party application setup in ACS in Azure
        //    e.SignInRequestMessage.Reply = "http://localhost/conceptcave.www/";
        //    e.SignInRequestMessage.Realm = "http://localhost/conceptcave.www/";
            
        //    //// Get the request url.
        //    //var request = HttpContext.Current.Request;
        //    //var requestUrl = request.Url;

        //    //// Build the realm url.
        //    //var realmUrl = new StringBuilder();
        //    //realmUrl.Append(requestUrl.Scheme);
        //    //realmUrl.Append("://");
        //    //realmUrl.Append(request.Headers["Host"] ?? requestUrl.Authority);
        //    //realmUrl.Append(request.ApplicationPath);
        //    //if (!request.ApplicationPath.EndsWith("/"))
        //    //    realmUrl.Append("/");
        //    //e.SignInRequestMessage.Realm = realmUrl.ToString();
        //}
#endif

        protected void Application_Start()
        {
            var kernel = NinjectWebCommon.TheKernel;
            var globalRepo = kernel.Get<IGlobalSettingsRepository>();
            var mediaRepo = kernel.Get<IMediaRepository>();
//            _membershipRepo = kernel.Get<IMembershipRepository>();

            HttpRuntime.Cache.Insert("WebAppStartTime", DateTime.UtcNow);

            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12 | System.Net.SecurityProtocolType.Tls11 | System.Net.SecurityProtocolType.Tls;

            // Cache logs since Logging may not have started up completely, so we miss first few entries
            LoggingStopWatch lsw = new LoggingStopWatch("Application_Start", _logger);

            /*ViewEngines.Engines.Remove(ViewEngines.Engines.OfType<RazorViewEngine>().First());
            ViewEngines.Engines.Add(new MobileCapableRazorViewEngine());
            ViewEngines.Engines.Remove(ViewEngines.Engines.OfType<WebFormViewEngine>().First());
            ViewEngines.Engines.Add(new MobileCapableWebFormViewEngine());
             * */

            lsw.Mark("AsposeWordsLicence");
            Debug.Write("In Application_Start");

            Aspose.Words.License license = new Aspose.Words.License();
            license.SetLicense("Aspose.Words.lic");

            lsw.Mark("GlobalConfiguration & Routing");
            // We need to handle 404 errors for web api through our own custom mechanism to avoid possibly opening up XSS vector (EVS-943)
            GlobalConfiguration.Configuration.Services.Replace(typeof(IHttpControllerSelector), new HttpNotFoundAwareDefaultHttpControllerSelector(GlobalConfiguration.Configuration));
            GlobalConfiguration.Configuration.Services.Replace(typeof(IHttpActionSelector), new HttpNotFoundAwareControllerActionSelector());


            if (DynamicAreaManger.Current != null)
            {
                DynamicAreaManger.Current.Items.ForEach(a =>
                {
                    ControllerBuilder.Current.DefaultNamespaces.Add(a.ControllerNamespace);
                }); 
            }

            AreaRegistration.RegisterAllAreas();

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes, kernel);

            //the routing table
            Debug.Write("The routing table:");
            foreach (var route in RouteTable.Routes)
            {
                var croute = route as Route;
                if (croute == null) continue;
                Debug.Write("\t" + croute.Url);
            }

            //Set up the output formatters for web api
            var xmlFormatter = GlobalConfiguration.Configuration.Formatters
                .FirstOrDefault(f => f.SupportedMediaTypes.Any(v => v.MediaType == "text/xml"));
            if (xmlFormatter != null)
                GlobalConfiguration.Configuration.Formatters.Remove(xmlFormatter);

            var jsonFormatter = GlobalConfiguration.Configuration.Formatters.JsonFormatter;

            // Setup to Multi-tenancy support
            lsw.Mark("MultiTenancy Setup");
            MultiTenantInit.StartUp();

            lsw.Mark("Settings & Converters");

            //Conditionally make things pretty
            string jsonSetting = WebConfigurationManager.AppSettings["prettyPrintJson"];
            if (!string.IsNullOrEmpty(jsonSetting) && bool.Parse(jsonSetting))
                jsonFormatter.SerializerSettings.Formatting = Newtonsoft.Json.Formatting.Indented;

            //make enums render as strings
            var enumConverter = new Newtonsoft.Json.Converters.StringEnumConverter();
            jsonFormatter.SerializerSettings.Converters.Add(enumConverter);

            //Exception filter
            GlobalConfiguration.Configuration.Filters.Add(new CustomExceptionFilterAttribute());
            GlobalConfiguration.Configuration.IncludeErrorDetailPolicy = IncludeErrorDetailPolicy.Never;

            //Initialise global type cache
            var resolver =
                ((IRepositorySectionManager) ConfigurationManager.GetSection("conceptcave.repository.resources"))
                    .OverrideResolver;
            XmlCoderTypeCache.InitGlobalTypeCache(resolver);
            XmlElementConverter.InitConverters(AspNetReflectionFactory.Current);
            SBONDecoder.FnGlobalInstantiate = t => AspNetReflectionFactory.Current.InstantiateObject(t);

            /*Subscribe to static events*/
            RequestEventManager.LogIn += new RequestEventManager.LogInEventHandler(EventManager_OnLogIn);
            RequestEventManager.LogOut += new RequestEventManager.LogOutEventHandler(EventManager_OnLogOut);

            lsw.Mark("Making Bundles");
            HostingEnvironment.RegisterVirtualPathProvider(new MultiTenantVirtualFolder("~/Content",
                ButterflyEnvironment.Current.ThemePath));

            foreach(var tenant in VirtualMgr.Central.MultiTenantManager.Tenants)
            {
                MultiTenantManager.TenantContextHostName = tenant.HostName;
                try
                {
                    // CSS Bundle is specific to each Tenant to support their theme
                    MakeBundles.MakeCSSBundle();

                    var themeMediaId = globalRepo.GetSetting<Guid?>("ThemeMediaId");
                    if (themeMediaId.HasValue && ButterflyEnvironment.Current.HasTheme() == false)
                    {
                        var themeMedia = mediaRepo.GetById(themeMediaId.Value, MediaLoadInstruction.Data);
                        ButterflyEnvironment.Current.SetTheme(themeMedia.MediaData.Data);
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("Exception on tenant " + tenant.HostName + ": " + ex.Message);
                }
                MultiTenantManager.TenantContextHostName = null;
            };

            //Now build up squishit bundles, JSBundle is global to all tenants (currently)
            MakeBundles.MakeJSBundle();
            HttpRuntime.Cache.Insert("ForcedAppUpdate", Guid.NewGuid());

            lsw.Mark("Finishing");
            GlobalConfiguration.Configuration.Services.Replace(typeof(IHostBufferPolicySelector), new NoBufferPolicySelector());

            CoderFactory.DefaultFactory = new XmlDocumentCoderFactory();

            lsw.Log(LogLevel.Info);
        }

        /*This section handles login/out events for time tracking*/
        public void EventManager_OnLogIn(HttpContext context, Guid userId, string tabletUUID)
        {
            //try and steal position from request data
            decimal? latitude = null;
            decimal? longitude = null;
            try {
                //Only log if we are supposed to from settings
                if (GlobalSettingsRepository.GetSetting<bool>("StoreLoginCoordinates") &&
                    context.Request.Form["longitude"] != null && context.Request.Form["latitude"] != null)
                {
                    latitude = Decimal.Parse(context.Request.Form["latitude"]);
                    longitude = Decimal.Parse(context.Request.Form["longitude"]);
                }
            }
            catch (FormatException) {
                latitude = null; longitude = null;
            }

            //settings.StoreLoginSite: true if we should try resolve what site is being logged in at
            Guid logInKey = MembershipRepo.RecordLogin(userId, latitude, longitude, GlobalSettingsRepository.GetSetting<bool>("StoreLoginSite"), tabletUUID, Double.Parse(WebConfigurationManager.AppSettings["MaxDistanceForLogin"]));
            //always set up a new cookie
            HttpLoginKey.SetLoginKey(logInKey);
        }

        public void EventManager_OnLogOut(HttpContext context, Guid userId)
        {
            //Record a signout for this person

            try
            {
                var logInKey = HttpLoginKey.GetLoginKey();
                MembershipRepo.RecordLogOut(userId, logInKey);
            }
            catch (NullReferenceException) { }
            //not sure what machinekey.decode will throw if doesn't pass verification
            //scrap cookies
            context.Response.Cookies["LogId"].Expires = DateTime.UtcNow.Subtract(new TimeSpan(400, 0, 0));
        }
    }
}