﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Hosting;
using VirtualMgr.Central;

namespace ConceptCave.www.ThemeVirtualProvider
{
    public class MultiTenantVirtualFolder : VirtualPathProvider
    {
        private readonly string _virtualPath;
        private readonly string _physicalPath;
        private readonly string _defaultPath;

        public MultiTenantVirtualFolder(string virtualPath, string physicalPath) 
            : base()
        {
            _virtualPath = NormalizeVirtualPath(virtualPath);
            _physicalPath = physicalPath;
            _defaultPath = HostingEnvironment.MapPath(virtualPath);
        }

        //Routines from/adapted from imageresizer
        protected string NormalizeVirtualPath(string path)
        {
            if (!path.StartsWith("/")) 
                path = HostingEnvironment.ApplicationVirtualPath.TrimEnd('/') + '/' + (path.StartsWith("~") ? path.Substring(1) : path).TrimStart('/');
            return path;
        }

        protected string LocalMapPath(string virtualPath)
        {
            virtualPath = NormalizeVirtualPath(virtualPath);
            if (virtualPath.StartsWith(_virtualPath, StringComparison.OrdinalIgnoreCase))
            {
                return Path.Combine(_physicalPath, MultiTenantManager.TenantContextHostName, virtualPath.Substring(_virtualPath.Length).TrimStart('/').Replace('/', Path.DirectorySeparatorChar));
            }
            return null;
        }

        protected bool IsInOurVirtualPath(string virtualPath)
        {
            virtualPath = NormalizeVirtualPath(virtualPath);
            return virtualPath.StartsWith(_virtualPath, StringComparison.OrdinalIgnoreCase);
        }

        protected bool CanHandleVirtualPath(string path)
        {
            var norm = NormalizeVirtualPath(path);
            return IsInOurVirtualPath(norm) && File.Exists(LocalMapPath(norm));
        }

        //Override with same implementation to ensure that our copy of isOnlyVirtualPath is used
        //If this was java and all methods were virtual, this wouldn't be needed.
        public override bool FileExists(string virtualPath)
        {
            if (CanHandleVirtualPath(virtualPath))
                return true;
            else
                return Previous.FileExists(virtualPath);
        }

        /// <summary>
        /// Returns the virtual path to the file. If the file exists in the theme, this path will be returned
        /// otherwise the default path (under content) is returned.
        /// </summary>
        /// <param name="virtualPath"></param>
        /// <returns></returns>
        //public string ResolveVirtualPath(string virtualPath)
        //{
        //    if (CanHandleVirtualPath(virtualPath))
        //        return "~/App_Data/theme/" + MultiTenantManager.TenantContextHostName + "/" + virtualPath.Substring(virtualPath.IndexOf("Content") + 8, virtualPath.Length - virtualPath.IndexOf("Content") - 8);
        //    else
        //        return virtualPath;
        //}

        
        public override VirtualFile GetFile(string virtualPath)
        {
            if (CanHandleVirtualPath(virtualPath))
                return new SimpleVirtualFile(virtualPath, LocalMapPath(virtualPath));
            else
                return Previous.GetFile(virtualPath);
        }

        public override VirtualDirectory GetDirectory(string virtualDir)
        {
            if (IsInOurVirtualPath(virtualDir))
            {
                //need to work out how far to drill down physicalpath
                var normpath = NormalizeVirtualPath(virtualDir);
                string pathLeft = normpath.Substring(_virtualPath.Length).TrimStart('/');
                var pathElements = new List<string>();
                while (pathLeft.Length > 0)
                {
                    //keep peeling off a path element
                    var nextCut = pathLeft.IndexOf('/');
                    string pathFragment;
                    if (nextCut != -1)
                    {
                        pathFragment = pathLeft.Substring(0, nextCut);
                        pathLeft = pathLeft.Substring(nextCut).TrimStart('/');
                    }
                    else
                    {
                        //we're done
                        pathFragment = pathLeft;
                        pathLeft = "";
                    }
                    pathElements.Add(pathFragment);
                }

                //Now convert this virtual path sequence to a directory
                var physDirInfo = new DirectoryInfo(Path.Combine(_physicalPath, MultiTenantManager.TenantContextHostName));
                var defDirInfo = new DirectoryInfo(_defaultPath);
                while (pathElements.Any() && (physDirInfo != null || defDirInfo != null))
                {
                    if (physDirInfo != null)
                    {
                        if (!physDirInfo.Exists)
                            physDirInfo = null;
                        else
                            physDirInfo = physDirInfo.GetDirectories(pathElements[0], SearchOption.TopDirectoryOnly).FirstOrDefault();
                    }
                    if (defDirInfo != null)
                    {
                        if (!defDirInfo.Exists)
                            defDirInfo = null;
                        else
                            defDirInfo = defDirInfo.GetDirectories(pathElements[0], SearchOption.TopDirectoryOnly).FirstOrDefault();
                    }
                    pathElements.RemoveAt(0);
                }
                
                //now what to do?
                if (physDirInfo == null && defDirInfo == null)
                    return Previous.GetDirectory(virtualDir);
                else if (physDirInfo != null && defDirInfo == null)
                    return new UnionVirtualDirectory(virtualDir, physDirInfo.FullName, null);
                else if (physDirInfo == null && defDirInfo != null)
                    return new UnionVirtualDirectory(virtualDir, defDirInfo.FullName, null);
                else
                    return new UnionVirtualDirectory(virtualDir, physDirInfo.FullName, defDirInfo.FullName);
            }
            else
                return Previous.GetDirectory(virtualDir);
        }
    }
}
