﻿using System.IO;
using System.Web.Hosting;

namespace ConceptCave.www.ThemeVirtualProvider
{
    public class SimpleVirtualFile : VirtualFile
    {
        private readonly string _virtualPath;
        private readonly string _physicalPath;

        public SimpleVirtualFile(string virtualPath, string physicalPath) : base(virtualPath)
        {
            _virtualPath = virtualPath;
            _physicalPath = physicalPath;
        }


        public override Stream Open()
        {
            return new FileStream(_physicalPath, FileMode.Open, FileAccess.Read);
        }
    }
}
