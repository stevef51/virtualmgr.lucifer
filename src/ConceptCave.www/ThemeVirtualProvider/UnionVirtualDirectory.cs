﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Hosting;

namespace ConceptCave.www.ThemeVirtualProvider
{
    public class UnionVirtualDirectory : VirtualDirectory
    {
        private string _virtualPath;
        private readonly string _preferredPhysicalDir;
        private readonly string _fallbackPhysicalDir;

        public UnionVirtualDirectory(string virtualPath, string preferredPhysicalDir, string fallbackPhysicalDir)
            : base(virtualPath)
        {
            _virtualPath = virtualPath;
            _preferredPhysicalDir = preferredPhysicalDir;
            _fallbackPhysicalDir = fallbackPhysicalDir;
        }

        private IList<VirtualFile> GetFiles()
        {
            var files = new List<VirtualFile>();
            var prefdirinfo = new DirectoryInfo(_preferredPhysicalDir);
            var fbdirinfo = _fallbackPhysicalDir != null ? new DirectoryInfo(_fallbackPhysicalDir) : null;

            if (prefdirinfo.Exists)
            {
                foreach (var preffile in prefdirinfo.GetFiles())
                {
                    var vp = _virtualPath.TrimEnd('/') + "/" + preffile.Name;
                    files.Add(new SimpleVirtualFile(vp, preffile.FullName));
                }
            }

            if (fbdirinfo != null && fbdirinfo.Exists)
            {
                foreach (var fbfile in fbdirinfo.GetFiles())
                {
                    if (files.Any(f => f.Name == fbfile.Name))
                        continue;
                    var vp = _virtualPath.TrimEnd('/') + '/' + fbfile.Name;
                    files.Add(new SimpleVirtualFile(vp, fbfile.FullName));
                }
            }

            return files;
        }

        private IList<VirtualDirectory> GetSubdirs()
        {
            var dirs = new List<VirtualDirectory>();
            var prefdirinfo = new DirectoryInfo(_preferredPhysicalDir);
            var fbdirinfo = _fallbackPhysicalDir != null ? new DirectoryInfo(_fallbackPhysicalDir) : null;

            if (prefdirinfo.Exists)
            {
                foreach (var prefdir in prefdirinfo.GetDirectories())
                {
                    var vp = _virtualPath.TrimEnd('/') + "/" + prefdir.Name;
                    var fbdir = _fallbackPhysicalDir != null
                        ? fbdirinfo.GetDirectories(prefdir.Name, SearchOption.TopDirectoryOnly).FirstOrDefault()
                        : null;

                    dirs.Add(fbdir != null
                        ? new UnionVirtualDirectory(vp, prefdir.FullName, fbdir.FullName)
                        : new UnionVirtualDirectory(vp, prefdir.Name, null));
                }
            }
            if (fbdirinfo != null && fbdirinfo.Exists)
            {
                //also add paths that are only in fallback dir
                foreach (var fbdir in fbdirinfo.GetDirectories())
                {
                    if (dirs.Any(d => d.Name == fbdir.Name))
                        continue; //they're already in there
                    var vp = _virtualPath.TrimEnd('/') + "/" + fbdir.Name;
                    dirs.Add(new UnionVirtualDirectory(vp, fbdir.FullName, null));
                }
            }

            return dirs;
        }

        public override IEnumerable Directories
        {
            get
            {
                return GetSubdirs();
            }
        }

        public override IEnumerable Files
        {
            get
            {
                return GetFiles();
            }
        }

        public override IEnumerable Children
        {
            get
            {
                var all = new List<VirtualFileBase>();
                all.AddRange(GetFiles());
                all.AddRange(GetSubdirs());
                return all; ;
            }
        }
    }
}
