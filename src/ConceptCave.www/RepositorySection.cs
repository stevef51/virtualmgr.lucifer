﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Xml;
using ConceptCave.Checklist;
using ConceptCave.Repository;
using ConceptCave.www.App_Start;

namespace ConceptCave.www
{
    public class RepositorySection : IConfigurationSectionHandler
    {
        public object Create(object parent, object configContext, XmlNode section)
        {
            XmlNodeList nodes = section.SelectNodes("//add");

            var manager = new ConceptCave.BusinessLogic.Configuration.RepositorySectionManager(section, AspNetReflectionFactory.Current);
            return manager;
        }
    }
}