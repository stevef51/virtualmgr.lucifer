﻿// Copyright (c) Microsoft Open Technologies, Inc. All rights reserved. See License.txt in the project root for license information.

using System;
using System.Net.Http;
using System.Web;
using System.Web.Http.Hosting;
using System.Web.Http.WebHost;

namespace ConceptCave.www
{
    /// <summary>
    /// Provides an implementation of <see cref="IHostBufferPolicySelector"/> suited for use
    /// in an ASP.NET environment which provides direct support for input and output buffering.
    /// </summary>
    public class NoBufferPolicySelector : WebHostBufferPolicySelector
    {
        /// <summary>
        /// Determines whether the host should buffer the entity body when processing a request with content.
        /// </summary>
        /// <param name="hostContext">The host-specific context.  In this case, it is an instance
        /// of <see cref="HttpContextBase"/>.</param>
        /// <returns><c>true</c> if buffering should be used; otherwise a streamed request should be used.</returns>
        public override bool UseBufferedInputStream(object hostContext)
        {
            var context = hostContext as HttpContextBase;

            if (context != null)
            {
                if (string.Equals(context.Request.RequestContext.RouteData.Values["controller"].ToString(), "BinarySync", StringComparison.InvariantCultureIgnoreCase))
                    return false;
            }

            return true;
        }

        /// <summary>
        /// Determines whether the host should buffer the <see cref="HttpResponseMessage"/> entity body.
        /// </summary>
        /// <param name="response">The <see cref="HttpResponseMessage"/>response for which to determine
        /// whether host output buffering should be used for the response entity body.</param>
        /// <returns><c>true</c> if buffering should be used; otherwise a streamed response should be used.</returns>
        public override bool UseBufferedOutputStream(HttpResponseMessage response)
        {
            return !string.Equals(response.RequestMessage.GetRouteData().Values["controller"].ToString(), "BinarySync", StringComparison.InvariantCultureIgnoreCase);
        }
    }
}
