﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConceptCave.www.Models
{
    public class LinkToMediaModel
    {
        public Guid MediaId { get; set; }
        public string Text { get; set; }
        public bool NewWindow { get; set; }
        public string LinkClass { get; set; }
    }
}