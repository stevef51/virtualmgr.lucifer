﻿using ConceptCave.BusinessLogic.Json;
using ConceptCave.RepositoryInterfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;

namespace ConceptCave.www.Models
{
    public class ContinuousProgressTextResponse : IContinuousProgress
    {
        public string NewLine { get; set; } = "<br/>";

        public class Category : IContinuousProgressCategory
        {
            private readonly object _lock = new object();

            private readonly ContinuousProgressTextResponse _progress;
            private string _message;
            private string _header;

            internal Category(ContinuousProgressTextResponse progress, string header)
            {
                _progress = progress;
                _header = header;
                Update(_header);
            }

            public void NewItem()
            {
                _message = null;
            }

            private void Update(string state, string message = null, params object[] formatArgs)
            {
                lock (_lock)
                {
                    var msg = state + ":" + string.Format(message ?? "", formatArgs);
                    if (_message != msg)
                    {
                        _message = msg;
                        _progress.Update(_message);
                    }
                }
            }
            public void Error(string message = null, params object[] formatArgs)
            {
                Update("Error", message, formatArgs);
            }

            public void Complete(string message = null, params object[] formatArgs)
            {
                Update("Complete", message, formatArgs);
            }

            public void Warning(string message = null, params object[] formatArgs)
            {
                Update("Warning", message, formatArgs);
            }

            public void Working(string message = null, params object[] formatArgs)
            {
                Update("Working", message, formatArgs);
            }

            public IContinuousProgressCategory NewCategory(string name)
            {
                return _progress.NewCategory(name);
            }
        }

        private Stream _stream;        

        public List<Category> categories { get; set; }
        public bool finished { get; private set; }

        private Thread _tickerThread;
        private AutoResetEvent _tickEvent;

        public ContinuousProgressTextResponse() : this((Stream)null)
        {

        }

        public ContinuousProgressTextResponse(HttpResponseBase response) : this(response.OutputStream)
        {
            response.BufferOutput = false;
        }

        public ContinuousProgressTextResponse(Stream stream)
        {
            categories = new List<Category>();

            _stream = stream;
        }

        private void TickerThreadLoop()
        {
            while(!finished)
            {
                if (!_tickEvent.WaitOne(10000))
                {
                    var stream = _stream;
                    if (stream != null)
                    {
                        lock (stream)
                        {
                            byte[] bytes = System.Text.UTF8Encoding.UTF8.GetBytes(".");
                            try
                            {
                                stream.Write(bytes, 0, bytes.Length);
                                stream.Flush();
                            }
                            catch
                            {
                                finished = true;
                            }
                        }
                    }
                }
            }
        }

        public void Update(string message)
        {
            if (_stream == null)
                return;

            if (_tickerThread == null)
            {
                _tickEvent = new AutoResetEvent(false);
                _tickerThread = new Thread(new ThreadStart(TickerThreadLoop));
                _tickerThread.Start();
            }

            lock (_stream)
            {
                byte[] bytes = System.Text.UTF8Encoding.UTF8.GetBytes(NewLine + message);
                _stream.Write(bytes, 0, bytes.Length);
                _stream.Flush();
            }
            _tickEvent.Set();
        }

        public IContinuousProgressCategory NewCategory(string name)
        {
            var category = new Category(this, name);
            categories.Add(category);
            return category;
        }

        public void Finished()
        {
            if (!finished)
            {
                finished = true;
                Update("Finished.");
                if (_tickerThread != null)
                {
                    _tickEvent.Set();
                    _stream = null;
                }
            }
        }

        public int ErrorCount()
        {
            return 0;
        }
    }
}