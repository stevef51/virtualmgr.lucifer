﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace ConceptCave.www.Models
{
    public class AuthenticateModel
    {
        [Required]
        public string Username { get; set; }

        public bool StoreLoginCoordinates { get; set; }

        public bool HasForgottenPasswordManagement { get; set; }

        public Guid[] RefreshShortcutLoginLabels { get; set; }

        public bool IsApiLogin { get; set; }
        public bool iOS13Fix { get; set; }
        
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public string TabletUUID { get; set; }
    }

    public class AuthenticateResult
    {
        public bool Success { get;set;}

        public List<AuthenticateShortcutLoginResult> LabelShortcutLogins { get; set; }
    }

    public class AuthenticateShortcutLoginResult
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Username { get; set; }
    }
}