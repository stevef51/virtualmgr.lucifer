﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConceptCave.Data.EntityClasses;

namespace ConceptCave.www.Models
{
    public class SelectMultipleChecklistsModel
    {
        public List<SelectMultipleChecklistsModelItem> Items { get; set; }
        
        public string SearchChecklistUrl { get; set; }

        public bool RenderList { get; set; }

        public SelectMultipleChecklistsModel()
        {
            Items = new List<SelectMultipleChecklistsModelItem>();
            RenderList = true;
        }
    }

    public class SelectMultipleChecklistsModelItem
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        public SelectMultipleChecklistsModelItem() { }

        public SelectMultipleChecklistsModelItem(PublishingGroupResourceEntity entity)
        {
            Id = entity.Resource.NodeId;
            Name = entity.Resource.Name;
        }
    }
}