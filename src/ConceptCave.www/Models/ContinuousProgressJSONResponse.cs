﻿using ConceptCave.BusinessLogic.Json;
using ConceptCave.RepositoryInterfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace ConceptCave.www.Models
{
    public class ContinuousProgressJSONResponse : IContinuousProgress
    {
        public enum ItemState
        {
            Working,
            Complete,
            Error,
            Warning
        }

        public class Item
        {
            public string message { get; set; }
            public ItemState state { get; set; }

        }
        public class Category : IContinuousProgressCategory
        {
            private readonly object _lock = new object();

            private readonly ContinuousProgressJSONResponse _progress;
            private Item _currentItem;


            public string name { get; set; }
            public List<Item> items { get; private set; }
            public ItemState state 
            { 
                get
                {
                    ItemState s = ItemState.Working;
                    if (items.Any(i => i.state == ItemState.Error))
                        s = ItemState.Error;
                    else if (items.Any(i => i.state == ItemState.Warning))
                        s = ItemState.Warning;
                    else if (items.All(i => i.state == ItemState.Complete))
                        s = ItemState.Complete;
                    return s;
                }
            }
            internal Category(ContinuousProgressJSONResponse progress, string name)
            {
                _progress = progress;
                this.name = name;
                this.items = new List<Item>();
            }

            public void NewItem()
            {
                _currentItem = null;
            }

            public void Update()
            {
                _progress.Update();
            }

            private void SetMessageState(bool newItemAfter, string message = null, object[] formatArgs = null, ItemState? state = null)
            {
                if (_currentItem == null)
                {
                    _currentItem = new Item();
                    items.Add(_currentItem);
                }

                bool updateRequired = false;
                if (message != null)
                {
                    var text = string.Format(message, formatArgs);
                    if (_currentItem.message != text)
                    {
                        _currentItem.message = text;
                        updateRequired = true;
                    }
                }
                if (state.HasValue && _currentItem.state != state.Value)
                {
                    _currentItem.state = state.Value;
                    updateRequired = true;
                }
                if (newItemAfter)
                {
                    _currentItem = null;
                }
                if (updateRequired)
                {
                    Update();
                }
            }

            public void Error(string message = null, params object[] formatArgs)
            {
                lock (_lock)
                {
                    SetMessageState(true, message, formatArgs, ItemState.Error);
                }
            }

            public void Complete(string message = null, params object[] formatArgs)
            {
                lock (_lock)
                {
                    SetMessageState(true, message, formatArgs, ItemState.Complete);
                }
            }

            public void Warning(string message = null, params object[] formatArgs)
            {
                lock (_lock)
                {
                    SetMessageState(true, message, formatArgs, ItemState.Warning);
                }
            }

            public void Working(string message = null, params object[] formatArgs)
            {
                lock (_lock)
                {
                    SetMessageState(false, message, formatArgs, ItemState.Working);
                }
            }

            public IContinuousProgressCategory NewCategory(string name)
            {
                return _progress.NewCategory(name);
            }
        }

        private readonly Stream _stream;        
        private readonly JsonSerializerSettings _serializerSettings;

        public List<Category> categories { get; set; }
        public bool finished { get; private set; }


        public ContinuousProgressJSONResponse() : this((Stream)null)
        {

        }

        public ContinuousProgressJSONResponse(HttpResponseBase response) : this(response.OutputStream)
        {
            response.BufferOutput = false;
        }

        public ContinuousProgressJSONResponse(Stream stream)
        {
            categories = new List<Category>();

            _stream = stream;
            if (_stream != null)
            {
                _serializerSettings = new JsonSerializerSettings();
                LowercaseContractResolver resolver = new LowercaseContractResolver();
                _serializerSettings.ContractResolver = resolver;

                _serializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            }
        }

        public void Update()
        {
            if (_stream == null)
                return;

            lock (_serializerSettings)
            {
                var json = JsonConvert.SerializeObject(this, Formatting.Indented, _serializerSettings) + "\0";

                byte[] bytes = System.Text.UTF8Encoding.UTF8.GetBytes(json);
                _stream.Write(bytes, 0, bytes.Length);
                _stream.Flush();
            }
        }

        public IContinuousProgressCategory NewCategory(string name)
        {
            var category = new Category(this, name);
            categories.Add(category);
            return category;
        }

        public void Finished()
        {
            if (!finished)
            {
                finished = true;
                Update();
            }
        }

        public int ErrorCount()
        {
            return (from c in categories from i in c.items where i.state == ItemState.Error select i).Count();
        }
    }
}