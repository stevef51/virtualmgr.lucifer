﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.www.Models
{
    public class RenderSelectedLabelsModel
    {
        public List<SelectMultipleLablesModelItem> SelectedLabels { get; set; }

        public RenderSelectedLabelsModel(IEnumerable<LabelEntity> selectedLables)
        {
            SelectedLabels = new List<SelectMultipleLablesModelItem>();

            if (selectedLables == null)
            {
                return;
            }

            foreach(var l in selectedLables)
            {
                SelectedLabels.Add(new SelectMultipleLablesModelItem(l));
            }
        }

        public RenderSelectedLabelsModel(IEnumerable<LabelDTO> selectedLables)
        {
            SelectedLabels = new List<SelectMultipleLablesModelItem>();

            if (selectedLables == null)
            {
                return;
            }

            foreach (var l in selectedLables)
            {
                SelectedLabels.Add(new SelectMultipleLablesModelItem(l));
            }
        }
    }
}