﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.www.Models
{
    public class SelectMultiplePublishedChecklists
    {
        public List<SelectMultiplePublishedChecklistsGroup> Groups { get; set; }
        public int[] SelectedPublishedChecklists { get; set; }
        public string FieldName { get; set; }

        public SelectMultiplePublishedChecklists()
        {
            FieldName = "SelectedPublishedChecklists";
            Groups = new List<SelectMultiplePublishedChecklistsGroup>();
            SelectedPublishedChecklists = new int[] { };
        }

        public SelectMultiplePublishedChecklists(List<PublishingGroupEntity> items) : this()
        {
            foreach (var p in items)
            {
                Groups.Add(new SelectMultiplePublishedChecklistsGroup(p));
            }
        }

        public SelectMultiplePublishedChecklists(IList<PublishingGroupDTO> items)
            : this()
        {
            foreach (var p in items)
            {
                Groups.Add(new SelectMultiplePublishedChecklistsGroup(p));
            }
        }
    }

    public class SelectMultiplePublishedChecklistsGroup
    {
        public string Name { get; set; }

        public List<SelectMultiplePublishedChecklistsItem> Items { get; set; }

        public SelectMultiplePublishedChecklistsGroup()
        {
            Items = new List<SelectMultiplePublishedChecklistsItem>();
        }

        public SelectMultiplePublishedChecklistsGroup(PublishingGroupEntity entity) : this()
        {
            Name = entity.Name;

            foreach (var r in entity.PublishingGroupResources)
            {
                Items.Add(new SelectMultiplePublishedChecklistsItem(r));
            }
        }

        public SelectMultiplePublishedChecklistsGroup(PublishingGroupDTO entity)
            : this()
        {
            Name = entity.Name;

            foreach (var r in entity.PublishingGroupResources)
            {
                Items.Add(new SelectMultiplePublishedChecklistsItem(r));
            }
        }
    }

    public class SelectMultiplePublishedChecklistsItem
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public SelectMultiplePublishedChecklistsItem() { }

        public SelectMultiplePublishedChecklistsItem(PublishingGroupResourceEntity entity)
        {
            Id = entity.Id;
            Name = entity.Name;
        }

        public SelectMultiplePublishedChecklistsItem(PublishingGroupResourceDTO entity)
        {
            Id = entity.Id;
            Name = entity.Name;
        }
    }
}