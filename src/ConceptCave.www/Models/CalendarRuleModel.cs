﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;

namespace ConceptCave.www.Models
{
    public class CalendarRuleModel
    {
        [HiddenInput(DisplayValue=false)]
        public int? Id { get; set; }

        [Display(Name="Start Date")]
        public DateTime StartDate { get; set; }
        [Display(Name = "End Date")]
        [UIHint("DateTimeOrNull")]
        public DateTime? EndDate { get; set; }
        
        [Display(Name = "Frequency")]
        [UIHint("Enum")]
        public CalendarRuleFrequency Frequency { get; set; }

        [Display(Name = "Sunday")]
        public bool ExecuteSunday { get; set; }
        [Display(Name = "Monday")]
        public bool ExecuteMonday { get; set; }
        [Display(Name = "Tuesday")]
        public bool ExecuteTuesday { get; set; }
        [Display(Name = "Wednesday")]
        public bool ExecuteWednesday { get; set; }
        [Display(Name = "Thursday")]
        public bool ExecuteThursday { get; set; }
        [Display(Name = "Friday")]
        public bool ExecuteFriday { get; set; }
        [Display(Name = "Saturday")]
        public bool ExecuteSaturday { get; set; }

        public CalendarRuleModel()
        {
            StartDate = DateTime.UtcNow;
            Frequency = CalendarRuleFrequency.Single;
        }

        public CalendarRuleModel(CalendarRuleDTO dto)
        {
            Id = dto.Id;
            StartDate = dto.StartDate;
            EndDate = dto.EndDate;
            Frequency = (CalendarRuleFrequency)Enum.Parse(typeof(CalendarRuleFrequency), dto.Frequency);
            ExecuteSunday = dto.ExecuteSunday;
            ExecuteMonday = dto.ExecuteMonday;
            ExecuteTuesday = dto.ExecuteTuesday;
            ExecuteWednesday = dto.ExecuteWednesday;
            ExecuteThursday = dto.ExecuteThursday;
            ExecuteFriday = dto.ExecuteFriday;
            ExecuteSaturday = dto.ExecuteSaturday;
        }

        public void PopulateDto(CalendarRuleDTO dto)
        {
            dto.StartDate = StartDate.ToUniversalTime();
            dto.EndDate = EndDate;
            if (dto.EndDate.HasValue == true)
            {
                dto.EndDate = dto.EndDate.Value.ToUniversalTime();
            }
            dto.Frequency = Frequency.ToString();
            dto.ExecuteSunday = ExecuteSunday;
            dto.ExecuteMonday = ExecuteMonday;
            dto.ExecuteTuesday = ExecuteTuesday;
            dto.ExecuteWednesday = ExecuteWednesday;
            dto.ExecuteThursday = ExecuteThursday;
            dto.ExecuteFriday = ExecuteFriday;
            dto.ExecuteSaturday = ExecuteSaturday;
        }
    }
}