﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConceptCave.www.Models
{
    public class SearchAssetModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}