﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConceptCave.Data.EntityClasses;

namespace ConceptCave.www.Models
{
    public class PublicVariableGridModel
    {
        public string[] Columns { get; set; }

        public IEnumerable<PublicVariableGridModelDataRow> Rows { get; set; }

        public PublicVariableGridModel(IEnumerable<WorkingDocumentEntity> entities, PublishingGroupResourceEntity publish)
        {
            Columns = new string[] { };
            List<PublicVariableGridModelDataRow> rows = new List<PublicVariableGridModelDataRow>();
            Rows = rows;

            if (publish == null || publish.PublishingGroupResourcePublicVariables.Count == 0)
            {
                return;
            }

            Columns = (from p in publish.PublishingGroupResourcePublicVariables select p.Name).ToArray();

            entities.ToList().ForEach(e =>
            {
                PublicVariableGridModelDataRow row = new PublicVariableGridModelDataRow(e, Columns);
                rows.Add(row);
            });
        }
    }

    public class PublicVariableGridModelDataRow
    {
        public Guid WorkingDocumentId { get; set; }

        public IEnumerable<PublicVariableGridModelDataField> Fields { get; set; }

        public PublicVariableGridModelDataRow(WorkingDocumentEntity entity, string[] columns)
        {
            WorkingDocumentId = entity.Id;

            List<PublicVariableGridModelDataField> fields = new List<PublicVariableGridModelDataField>();

            columns.ToList().ForEach(c =>
            {
                PublicVariableGridModelDataField field = new PublicVariableGridModelDataField(c);
                fields.Add(field);

                var items = (from d in entity.WorkingDocumentPublicVariables where d.Name == c select d);

                if (items.Count() == 0)
                {
                    return;
                }

                var item = items.First();

                field.Value = item.Value;
                field.IsPassed = item.PassFail;
                field.Score = item.Score;
            });

            Fields = fields;
        }
    }

    public class PublicVariableGridModelDataField
    {
        public PublicVariableGridModelDataField(string name)
        {
            Name = name;
        }

        public string Name { get; set; }
        public string Value { get; set; }
        public bool? IsPassed { get; set; }
        public decimal? Score { get; set; }
    }
}