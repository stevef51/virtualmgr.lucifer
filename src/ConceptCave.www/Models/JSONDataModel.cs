﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Helpers;
using ConceptCave.Core;

namespace ConceptCave.www.Models
{
    public class JSONDataModelBinder : DefaultModelBinder
    {
        public const string FormFieldName = "jsonmodel";

        protected override object CreateModel(ControllerContext controllerContext, ModelBindingContext bindingContext, Type modelType)
        {
            var jsonmodelString = bindingContext.ValueProvider.GetValue(FormFieldName).AttemptedValue;

            JSONDataModel model = Json.Decode<JSONDataModel>(jsonmodelString);

            return model;
        }
    }

    public class JSONDataModel
    {
        public string RootId { get; set; }

        public List<JSONDataModelItem> Items { get; set; }

        public JSONDataModel()
        {
            Items = new List<JSONDataModelItem>();
        }
    }

    public class JSONDataModelItem
    {
        public string Id { get; set; }

        public List<JSONDataModelItemProperty> Properties;

        public JSONDataModelItem()
        {
            Properties = new List<JSONDataModelItemProperty>();
        }
    }

    public class JSONDataModelItemProperty
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }

    /// <summary>
    /// Implemented by objects that convert data model item objects into the underlying model, so for example take
    /// the JSON data model item and populate the relevant properties on a FreeTextQuestion.
    /// </summary>
    public interface IJSONDataModelItemConverter
    {
        void Convert(JSONDataModelItem item, IUniqueNode node);
    }
}