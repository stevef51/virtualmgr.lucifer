﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConceptCave.Checklist.Interfaces;

namespace ConceptCave.www.Models
{
    public class FileUploadModel
    {
        public string Action { get; set; }
        public string Controller { get; set; }
        public string Area { get; set; }
        
        /// <summary>
        /// Call back used by picup when uploading from IOS devices
        /// </summary>
        public string IOSCallbackUrl { get; set; }

        public bool ShowNameEditor { get; set; }
        public bool ShowDescriptionEditor { get; set; }

        public int? MaxWidth { get; set; }
        public int? MaxHeight { get; set; }
        public bool KeepAspect { get; set; }
        public bool AllowImage { get; set; }
        public bool AllowVideo { get; set; }
        public bool AllowAudio { get; set; }

        public string Title { get; set; }
        public string ButtonTitle { get; set; }

        public string ButtonClass { get; set; }

        public bool ShowButton { get; set; }

        public string ActivateOnElementIdClicked { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }

        public bool AjaxSubmit { get; set; }

        public bool ShowAjaxResponseInDialog { get; set; }

        public List<FileUploadModelField> HiddenFields { get; protected set; }

        public FileUploadModel()
        {
            HiddenFields = new List<FileUploadModelField>();

            ShowButton = true;
            ShowNameEditor = true;
            ShowDescriptionEditor = true;
        }
    }

    public class FileUploadModelField
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}