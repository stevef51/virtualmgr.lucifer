﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConceptCave.Data.EntityClasses;

namespace ConceptCave.www.Models
{
    public class SelectMultipleUsersModel
    {
        public List<SelectMultipleUsersModelItem> Items { get; set; }

        public string SearchUserUrl { get; set; }

        public SelectMultipleUsersModel()
        {
            Items = new List<SelectMultipleUsersModelItem>();
        }
    }

    public class SelectMultipleUsersModelItem
    {
        public Guid Id {get;set;}
        public string Name {get;set;}

        public SelectMultipleUsersModelItem() { }

        public SelectMultipleUsersModelItem(PublishingGroupActorEntity entity)
        {
            Id = entity.UserData.UserId;
            Name = entity.UserData.Name;
        }
    }
}