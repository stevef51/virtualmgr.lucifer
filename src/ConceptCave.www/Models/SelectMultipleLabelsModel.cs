﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConceptCave.Data.EntityClasses;
using System.Drawing;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.www.Models
{
    public class SelectMultipleLabelsModel
    {
        public SelectMultipleLabelsModel()
        {
            FormElementName = "labels";
            AvailableLabels = new List<SelectMultipleLablesModelItem>();
            SelectedLabels = new List<SelectMultipleLablesModelItem>();
        }

        public SelectMultipleLabelsModel(IEnumerable<LabelEntity> availableLabels, IEnumerable<LabelEntity> selectedLables) : this()
        {
            if (availableLabels == null)
            {
                return;
            }

            foreach (var l in availableLabels)
            {
                AvailableLabels.Add(new SelectMultipleLablesModelItem(l));
            }

            if (selectedLables == null)
            {
                return;
            }

            foreach(var l in selectedLables)
            {
                SelectedLabels.Add(new SelectMultipleLablesModelItem(l));
            }
        }

        public SelectMultipleLabelsModel(IEnumerable<LabelDTO> availableLabels, IEnumerable<LabelDTO> selectedLables) : this()
        {
            if (availableLabels == null)
            {
                return;
            }

            foreach (var l in availableLabels)
            {
                AvailableLabels.Add(new SelectMultipleLablesModelItem(l));
            }

            if (selectedLables == null)
            {
                return;
            }

            foreach (var l in selectedLables)
            {
                SelectedLabels.Add(new SelectMultipleLablesModelItem(l));
            }
        }

        public List<SelectMultipleLablesModelItem> AvailableLabels { get; set; }
        public List<SelectMultipleLablesModelItem> SelectedLabels { get; set; }
        public string FormElementName { get; set; }
    }

    public class SelectMultipleLablesModelItem
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string ForeColor { get; set; }
        public string BackColor { get; set; }

        public SelectMultipleLablesModelItem(LabelEntity label)
        {
            Id = label.Id;
            Name = label.Name;
            ForeColor = System.Drawing.ColorTranslator.ToHtml(Color.FromArgb(label.Forecolor));
            BackColor = System.Drawing.ColorTranslator.ToHtml(Color.FromArgb(label.Backcolor));
        }

        public SelectMultipleLablesModelItem(LabelDTO label)
        {
            Id = label.Id;
            Name = label.Name;
            ForeColor = System.Drawing.ColorTranslator.ToHtml(Color.FromArgb(label.Forecolor));
            BackColor = System.Drawing.ColorTranslator.ToHtml(Color.FromArgb(label.Backcolor));
        }
    }
}