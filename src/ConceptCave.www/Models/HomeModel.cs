﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.www.Areas.Player.Models;

namespace ConceptCave.www.Models
{
    public class HomeModel
    {
        public EntityCollection<WorkingDocumentEntity> Documents { get; set; }

        /// <summary>
        /// Populated if a custom dashboard control is being used
        /// </summary>
        public string DashboardControl { get; set; }
    }
}