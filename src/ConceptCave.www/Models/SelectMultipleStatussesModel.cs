﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConceptCave.Data.EntityClasses;
using System.Drawing;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.www.Models
{
    public class SelectMultipleStatussesModel
    {
        public SelectMultipleStatussesModel()
        {
            FormElementName = "labels";
            AvailableStatusses = new List<SelectMultipleStatussesModelItem>();
            SelectedStatusses = new List<SelectMultipleStatussesModelItem>();
        }

        public SelectMultipleStatussesModel(IEnumerable<ProjectJobStatusDTO> availableStatusses, IEnumerable<ProjectJobStatusDTO> selectedStatusses)
            : this()
        {
            if (availableStatusses == null)
            {
                return;
            }

            foreach (var l in availableStatusses)
            {
                AvailableStatusses.Add(new SelectMultipleStatussesModelItem(l));
            }

            if (selectedStatusses == null)
            {
                return;
            }

            foreach (var l in selectedStatusses)
            {
                SelectedStatusses.Add(new SelectMultipleStatussesModelItem(l));
            }
        }

        public List<SelectMultipleStatussesModelItem> AvailableStatusses { get; set; }
        public List<SelectMultipleStatussesModelItem> SelectedStatusses { get; set; }
        public string FormElementName { get; set; }
    }
    public class SelectMultipleStatussesModelItem
    {
        public Guid Id { get; set; }
        public string Text { get; set; }

        public SelectMultipleStatussesModelItem(ProjectJobStatusDTO label)
        {
            Id = label.Id;
            Text = label.Text;
        }
    }
}