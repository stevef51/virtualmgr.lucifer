﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConceptCave.www.Models
{
    public class SearchMediaModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}