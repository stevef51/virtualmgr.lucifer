﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ImageResizer.Plugins.MediaReader;
using ConceptCave.www.Areas.Player.Models;

namespace ConceptCave.www.Models
{
    public class MediaImageModel
    {
        public static string[] ImageExtensions = new string[] { ".jpeg", ".jpg", ".png", ".tiff" };
        public static string[] VideoExtensions = new string[] { ".mov", ".mpg", ".mpeg", ".avi", ".mkv" };

        public Guid RootId { get; set; }
        public Guid MediaId { get; set; }
        public Guid PresentedId { get; set; }
        public Guid ItemId { get; set; }
        public string UserTypeContextName { get; set; }

        public int? Width { get; set; }
        public int? Height { get; set; }

        public int? PageIndex { get; set; }
        public int? PageCount { get; set; }
        public float? Scale { get; set; }

        public string Extension { get; set; }

        public bool IsImage { get { return ImageExtensions.Contains(Extension.ToLower()); } }
        public bool IsVideo { get { return VideoExtensions.Contains(Extension.ToLower()); } }

        public string Url
        {
            get
            {
                string result = string.Empty;

                if (PresentedId == Guid.Empty && MediaId == Guid.Empty)
                {
                    result = string.Format("~/{0}/{1}{2}", MediaReaderPlugin.MediaImagePath, RootId, Extension);
                }
                else if (MediaId != Guid.Empty)
                {
                    result = string.Format("~/{0}/{1}{2}", MediaReaderPlugin.DocumentPreviewMediaImagePath, RootId, Extension);
                }
                else
                {
                    result = string.Format("~/{0}/{1}{2}", string.IsNullOrEmpty(UserTypeContextName) ? MediaReaderPlugin.WorkingDocumentMediaImagePath : MediaReaderPlugin.MembershipWorkingDocumentMediaImagePath, RootId, Extension);
                }

                if (PresentedId != Guid.Empty || MediaId != Guid.Empty || Width.HasValue || Height.HasValue || PageIndex.HasValue || PageCount.HasValue || Scale.HasValue)
                {
                    result += "?";

                    if (MediaId != Guid.Empty)
                    {
                        result += string.Format("mediaid={0}&", MediaId);
                    }

                    if (PresentedId != Guid.Empty)
                    {
                        result += string.Format("presentedid={0}&", PresentedId);

                        if (string.IsNullOrEmpty(UserTypeContextName) == false)
                        {
                            result += string.Format("usertypecontextname={0}&", UserTypeContextName);
                        }

                        if (ItemId != Guid.Empty)
                        {
                            result += string.Format("itemId={0}&", ItemId);
                        }
                    }

                    if (Width.HasValue)
                    {
                        result += string.Format("width={0}&", Width.Value);
                    }

                    if (Height.HasValue)
                    {
                        result += string.Format("height={0}&", Height.Value);
                    }

                    if (Width.HasValue == false && Height.HasValue == false)
                    {
                        result += "scale=1&";
                    }

                    if (PageIndex.HasValue)
                    {
                        result += string.Format("pageindex={0}&", PageIndex.Value);
                    }

                    if (PageCount.HasValue)
                    {
                        result += string.Format("pagecount={0}&", PageCount.Value);
                    }

                    if (Scale.HasValue)
                    {
                        result += string.Format("scale={0}&", Scale.Value);
                    }

                    result = result.TrimEnd('&');
                }

                return result;
            }
        }
    }
}