﻿using ConceptCave.Checklist.Interfaces;
using ConceptCave.Data.EntityClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConceptCave.www.Models
{
    public class LanguageLocalisableStringModel
    {
        public IEnumerable<LanguageEntity> Languages { get; set; }
        public ILocalisableString LocalisableString { get; set; }
        public string DisplayName { get; set; }
        public string Name { get; set; }
    }
}