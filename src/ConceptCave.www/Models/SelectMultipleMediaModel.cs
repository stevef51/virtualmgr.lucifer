﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConceptCave.Data.EntityClasses;

namespace ConceptCave.www.Models
{
    public class SelectMultipleMediaModel
    {
        public List<SelectMultipleMediaModelItem> Items { get; set; }
        
        public string SearchMediaUrl { get; set; }

        public SelectMultipleMediaModel()
        {
            Items = new List<SelectMultipleMediaModelItem>();
        }
    }

    public class SelectMultipleMediaModelItem
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        public SelectMultipleMediaModelItem() { }

        public SelectMultipleMediaModelItem(MediaEntity entity)
        {
            Id = entity.Id;
            Name = entity.Name;
        }
    }
}