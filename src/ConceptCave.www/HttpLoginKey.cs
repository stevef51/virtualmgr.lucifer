﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace ConceptCave.www
{
    public static class HttpLoginKey
    {
        public static void SetLoginKey(Guid logInKey)
        {
            System.Web.HttpContext.Current.Response.Cookies["LogId"].Value = MachineKey.Encode(logInKey.ToByteArray(), MachineKeyProtection.All);
            System.Web.HttpContext.Current.Response.Cookies["LogId"].Expires = DateTime.UtcNow.AddHours(200); //To be longer than forms ticket
        }

        public static Guid GetLoginKey()
        {
            string cookiekey = System.Web.HttpContext.Current.Request.Cookies["LogId"].Value;
            byte[] logInKeyBytes = MachineKey.Decode(cookiekey, MachineKeyProtection.All);
            var logInKey = new Guid(logInKeyBytes);

            return logInKey;
        }
    }
}