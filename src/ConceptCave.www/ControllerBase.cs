﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.Core;
using System.Web.Security;
using ConceptCave.Checklist.Interfaces;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json;
using ConceptCave.BusinessLogic.Json;
using NLog;
using System.IO;
using System.Threading;
using System.Net;
using VirtualMgr.Membership;
using Microsoft.AspNet.Identity;
using ConceptCave.RepositoryInterfaces.Repositories;

namespace ConceptCave.www
{
    [Authorize(Roles = "User")]
    public class ControllerBase : AsyncController
    {
        private static readonly Logger log = LogManager.GetCurrentClassLogger();

        public bool IsInRole(string role)
        {
            return Thread.CurrentPrincipal.IsInRole(role);
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            base.OnException(filterContext);

            var telemetary = new Microsoft.ApplicationInsights.TelemetryClient();
            telemetary.TrackException(filterContext.Exception);

            LogEventInfo logEvent = new LogEventInfo(NLog.LogLevel.Error, "MVC", filterContext.Exception.Message);

            logEvent.Properties["Username"] = "Unknown";
            if (System.Threading.Thread.CurrentPrincipal != null)
            {
                logEvent.Properties["Username"] = System.Threading.Thread.CurrentPrincipal.Identity.Name;
            }
            logEvent.Exception = filterContext.Exception;


            if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.Request != null)
            {
                logEvent.Properties["RequestRawUrl"] = System.Web.HttpContext.Current.Request.RawUrl;
                logEvent.Properties["RequestMethod"] = System.Web.HttpContext.Current.Request.HttpMethod;
                try
                {
                    System.Web.HttpContext.Current.Request.InputStream.Position = 0;
                    var reader = new StreamReader(System.Web.HttpContext.Current.Request.InputStream);
                    logEvent.Properties["RequestBody"] = reader.ReadToEnd();
                }
                catch (Exception e)
                { }
            }

            log.Log(logEvent);
        }

        public ActionResult ReturnJson(object obj)
        {
            return Content(JsonText(obj, true));
        }

        public ActionResult ReturnJsonRetainCase(object obj)
        {
            return Content(JsonText(obj, false));
        }

        public static string JsonText(object obj, bool lowercase = true)
        {
            var settings = new JsonSerializerSettings();
            if (lowercase)
            {
                LowercaseContractResolver resolver = new LowercaseContractResolver();
                settings.ContractResolver = resolver;
            }
            settings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;

            return JsonConvert.SerializeObject(obj, Formatting.Indented, settings);
        }

        public static string JsonChunk(object obj, bool lowercase = true)
        {
            return JsonText(obj, lowercase) + "\0";
        }

        public T FromJson<T>(Stream stream)
        {
            var js = new JsonSerializer();
            using (var sr = new StreamReader(stream))
            {
                using (var jtr = new JsonTextReader(sr))
                {
                    return js.Deserialize<T>(jtr);
                }
            }
        }

        public string GetString(byte[] bytes)
        {
            char[] chars = new char[bytes.Length / sizeof(char)];
            System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
            return new string(chars);
        }
    }

    // Controllers which need access to CurrentUserId can derive from this
    [Authorize(Roles = "User")]
    public class ControllerBaseEx : ControllerBase
    {
        protected readonly IMembership _membership;
        protected readonly IMembershipRepository _memberRepo;

        public ControllerBaseEx(IMembership membership, IMembershipRepository memberRepo)
        {
            _membership = membership;
            _memberRepo = memberRepo;
        }

        public object CurrentUserId
        {
            get
            {
                var id = System.Web.HttpContext.Current.User.Identity.GetUserId();
                var user = _memberRepo.GetById(id);

                return user.UserId;
            }
        }
    }

    public interface IAlternateHandlerResult
    {
        IResource Resource { get; set; }
    }

    public interface IAlternateHandler
    {
        ActionResult Handle(string method, params object[] models);
        IAlternateHandlerResult GetResource(Guid id);

        IDesignDocument GetDocument(IAlternateHandlerResult result);

        void SaveResource(IAlternateHandlerResult result);

        void SetModelForHandler(IModelFromAlternateHandler model);
    }

    public interface IModelFromAlternateHandler
    {
        string AlternateHandler { get; set; }
        string AlternateHandlerMethod { get; set; }

        List<ControllerWithAlternateHandlerContextField> ContextFields { get; set; }
    }

    public class ModelFromAlternateHandler : IModelFromAlternateHandler
    {
        [HiddenInput(DisplayValue = false)]
        public string AlternateHandler { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string AlternateHandlerMethod { get; set; }

        [HiddenInput(DisplayValue = false)]
        public List<ControllerWithAlternateHandlerContextField> ContextFields { get; set; }

        public ModelFromAlternateHandler()
        {
            ContextFields = new List<ControllerWithAlternateHandlerContextField>();
        }
    }

    public class ControllerWithAlternateHandlerContextField
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }

    public class ControllerWithAlternateHandler : ControllerBase
    {
        public static string AlternameHandlerFieldName = "ah";
        public static string AlternameHandlerMethodFieldName = "ahm";
        public static string ContextFieldName = "c";

        public static string ContextFieldsString(List<ControllerWithAlternateHandlerContextField> fields)
        {
            string result = string.Empty;
            fields.ForEach(f =>
            {
                if (string.IsNullOrEmpty(f.Value))
                {
                    return;
                }

                result += string.Format("{0}:{1};", f.Name, f.Value);
            });

            return result;
        }

        public static List<ControllerWithAlternateHandlerContextField> ContextListFromString(string fields)
        {
            List<ControllerWithAlternateHandlerContextField> result = new List<ControllerWithAlternateHandlerContextField>();

            string[] split = fields.Split(';');

            split.ToList().ForEach(f =>
            {
                if (string.IsNullOrEmpty(f))
                {
                    return;
                }

                string[] item = f.Split(':');
                ControllerWithAlternateHandlerContextField field = new ControllerWithAlternateHandlerContextField() { Name = item[0], Value = item[1] };

                result.Add(field);
            });

            return result;
        }

        public string CurrentAlternateHandlerName
        {
            get
            {
                return System.Web.HttpContext.Current.Request[AlternameHandlerFieldName];
            }
        }

        public string CurrentAlternateHandlerMethodName
        {
            get
            {
                return System.Web.HttpContext.Current.Request[AlternameHandlerMethodFieldName];
            }
        }

        public List<ControllerWithAlternateHandlerContextField> ContextList()
        {
            return ControllerWithAlternateHandler.ContextListFromString(System.Web.HttpContext.Current.Request[ContextFieldName]);
        }

        public IAlternateHandler CreateAlternateHandler()
        {
            string alternateHandler = System.Web.HttpContext.Current.Request[AlternameHandlerFieldName];

            if (string.IsNullOrEmpty(alternateHandler))
            {
                alternateHandler ="AlternateHandler.Default";
            }

            if (string.IsNullOrEmpty(alternateHandler) == false)
            {
                string alternateHandlerType = System.Configuration.ConfigurationManager.AppSettings[alternateHandler];

                IAlternateHandler handler = (IAlternateHandler)Type.GetType(alternateHandlerType).GetConstructor(Type.EmptyTypes).Invoke(null);

                return handler;
            }

            return null;
        }
    }
}