﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace ConceptCave.www
{
    /*
     * This class exists to hold events that can be fired by one part of the application
     * and listened to for another
     * Developed originally to decouple the login/logout controller event 
     * */
    public static class RequestEventManager
    {
        public delegate void LogInEventHandler(HttpContext context, Guid userId, string tabletUUID);
        public delegate void LogOutEventHandler(HttpContext context, Guid userId);

        public static event LogInEventHandler LogIn;
        public static event LogOutEventHandler LogOut;

        public static void RaiseLogIn(HttpContext context, Guid userId, string tabletUUID)
        {
            LogIn(context, userId, tabletUUID);
        }

        public static void RaiseLogOut(HttpContext context, Guid userId)
        {
            LogOut(context, userId);
        }
    }
}