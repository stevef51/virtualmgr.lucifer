﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using ConceptCave.www.Attributes;

namespace ConceptCave.www.Controllers
{
    [ConditionalOutputCache(Duration = 604800, Location = OutputCacheLocation.Any)]
    public class TemplateController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}
