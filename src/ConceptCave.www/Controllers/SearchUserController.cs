﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Repository;
using ConceptCave.www.Models;
using ConceptCave.Data.EntityClasses;
using ConceptCave.RepositoryInterfaces.Repositories;

namespace ConceptCave.www.Controllers
{
    public class SearchUserController : ControllerBase
    {
        private readonly IMembershipRepository _membershipRepo;

        public SearchUserController(IMembershipRepository membershipRepo)
        {
            _membershipRepo = membershipRepo;
        }

        //
        // GET: /SearchUser/

        public ActionResult Index(string query, bool? isASite, bool? withCount)
        {
            var items = _membershipRepo.Search("%" + query + "%", false, 0, 20, RepositoryInterfaces.Enums.MembershipLoadInstructions.AspNetMembership | RepositoryInterfaces.Enums.MembershipLoadInstructions.MembershipType, null);

            List<SearchUserModel> result = new List<SearchUserModel>();

            var search = from i in items select i;
            if (isASite.HasValue)
            {
                search = items.Where(i => i.UserType.IsAsite == isASite.Value);
            }

            search.ToList().ForEach(i =>
            {
                result.Add(new SearchUserModel()
                {
                    Name = i.Name,
                    Username = i.AspNetUser.UserName,
                    Id = i.UserId
                });
            });

            if(withCount.HasValue && withCount.Value == true)
            {
                return ReturnJson(new { count = result.Count, items = result });
            }

            HttpResponseBase response = HttpContext.Response;
            response.ContentType = "text/json";
            
            return View(result);
        }

        public ActionResult Details(Guid id)
        {
            var details = _membershipRepo.GetById(id, RepositoryInterfaces.Enums.MembershipLoadInstructions.AspNetMembership);

            HttpResponseBase response = HttpContext.Response;
            response.ContentType = "text/json";

            return Json(new SearchUserModel() { 
                Name = details.Name, 
                Username = details.AspNetUser.UserName,
                Id = details.UserId }, JsonRequestBehavior.AllowGet);
        }
    }
}
