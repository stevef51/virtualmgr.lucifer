﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Repository;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.www.Models;
using ConceptCave.Configuration;
using ConceptCave.Checklist.Editor;
using ConceptCave.Checklist.RunTime;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.www.Areas.Player.Models;

namespace ConceptCave.www.Controllers
{
    public class ErrorController : ControllerBase
    {
        public ViewResult Index()
        {
            var exception = Server.GetLastError();
            var telemetary = new Microsoft.ApplicationInsights.TelemetryClient();
            telemetary.TrackException(exception);

            //very simple, return the angular app
            return View();
        }

        public ViewResult NotFound()
        {
            Response.StatusCode = 404; 
            return View("ErrorNotFound");
        }
    }
}
