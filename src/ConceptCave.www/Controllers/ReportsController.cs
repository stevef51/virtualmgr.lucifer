﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Repository;
using ConceptCave.www.Models;
using ConceptCave.Data.EntityClasses;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces;
using ConceptCave.PowerBi;
using Newtonsoft.Json.Linq;
using VirtualMgr.Central.Interfaces;

namespace ConceptCave.www.Controllers
{
    [Authorize(Roles = RoleRepository.COREROLE_VIEWREPORTS)]
    public class ReportsController : ControllerBaseEx
    {
        public class ReportHelper
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string Description { get; set; }
            
            public ReportType Type { get; set; }
            public string ShortId { get; set; }
            public string Configuration { get; set; }

            public ReportHelper(ReportDTO report)
            {
                FromDTO(report);
            }

            public void FromDTO(ReportDTO report)
            {
                Id = report.Id;
                Name = report.Name;
                Description = report.Description;
                Configuration = report.Configuration;

                Type = (ReportType)report.Type;
                if (Type == ReportType.JsReport) {
                    ShortId = System.Text.Encoding.UTF8.GetString(report.ReportData.Data);
                }
            }
        }

        public class ReportServerUrlHelper
        {
            public string Url { get; set; }
            public string JsReportServerUrl { get; set; }
        }


        public class AccessKeyHelper
        {
            public string AccessToken { get; set; }
            public string ReportId { get; set; }
        }

        protected IReportManagementRepository _reportRepo;
        protected IMembershipRepository _membershipRepo;
        protected IGlobalSettingsRepository _globalSettingRepo;
        protected readonly IServerUrls _serverUrls;
        protected IPowerBiManager _powerBi;

        public ReportsController(IReportManagementRepository reportRepo, 
            IMembershipRepository memberRepo,
            IGlobalSettingsRepository globalSettingRepo,
            IServerUrls serverUrls,
            IPowerBiManager powerBi,
            VirtualMgr.Membership.IMembership membership) : base(membership, memberRepo)
        {
            _reportRepo = reportRepo;
            _membershipRepo = memberRepo;
            _globalSettingRepo = globalSettingRepo;
            _serverUrls = serverUrls;
            _powerBi = powerBi;
        }

        [HttpGet]
        public ActionResult Reports()
        {
            var roles = _membershipRepo.GetRolesForUser((Guid)this.CurrentUserId).ToArray();

            var reports = _reportRepo.GetReports(roles, RepositoryInterfaces.Enums.ReportLoadInstructions.Data);

            return ReturnJson(reports.Select(r => new ReportHelper(r)).ToList());
        }

        [HttpGet]
        public ActionResult ReportServerUrl()
        {
            return ReturnJson(new ReportServerUrlHelper() { Url = _serverUrls.ReportingUrl, JsReportServerUrl = _serverUrls.JsReportServerUrl });
        }

        [HttpGet]
        public ActionResult AccessKey(int id)
        {
            var workspaceId = _globalSettingRepo.GetSetting<string>(GlobalSettingsConstants.SystemSetting_AzureWorkspaceId);

            var report = _reportRepo.GetById(id, ReportLoadInstructions.Data);

            var s = GetString(report.ReportData.Data);
            JObject obj = JObject.Parse(s);

            var reportId = obj["report"]["id"].ToString();

            var token = _powerBi.CreateReportEmbedToken(workspaceId, reportId);

            return ReturnJson(new AccessKeyHelper() { AccessToken = token.AccessToken, ReportId = reportId });
        }

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult RenderJsReport()
        {
            return View();
        }
    }
}
