﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Repository;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.www.Models;
using ConceptCave.Configuration;
using ConceptCave.Checklist.Editor;
using ConceptCave.Checklist.RunTime;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.www.Areas.Player.Models;
using ConceptCave.www.API.v1;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.RepositoryInterfaces;

namespace ConceptCave.www.Controllers
{
    public class PeriodicExecutionController : AsyncController
    {
        private PeriodicExecutionLogic _logic;

        public PeriodicExecutionController(PeriodicExecutionLogic logic)
        {
            _logic = logic;
        }


        [OutputCache(Duration = 0)]
        [HttpGet]
        public ActionResult Execute(string format = "text")
        {
            var result = new PeriodicExecutionLogic.ExecutionResults();

            IContinuousProgress progress = null;
            switch(format.ToLower())
            {
                case "text":
                    progress = new ContinuousProgressTextResponse(Response);
                    break;

                case "json":
                    progress = new ContinuousProgressJSONResponse(Response);
                    break;

                default:
                    progress = new ContinuousProgressTextResponse();
                    break;
            }
            _logic.PerformExecute(null, result, progress);
            progress.Finished();

            return null;
        }

        [HttpPost]
        public ActionResult ForceFinish()
        {
            return Json(_logic.ForceFinished());
        }
    }
}
