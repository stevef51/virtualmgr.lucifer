﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Repository;
using ConceptCave.www.Models;

namespace ConceptCave.www.Controllers
{
    public class SearchMediaController : ControllerBase
    {
        public ActionResult Index(string query)
        {
            EntityCollection<MediaEntity> items = MediaRepository.GetLikeName("%" + query + "%");

            List<SearchMediaModel> result = new List<SearchMediaModel>();

            items.ToList().ForEach(i =>
            {
                result.Add(new SearchMediaModel() { Id = i.Id, Name = i.Name });
            });

            HttpResponseBase response = HttpContext.Response;
            response.ContentType = "text/json";

            return View(result);
        }
    }
}
