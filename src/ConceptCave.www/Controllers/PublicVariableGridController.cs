﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Repository;
using ConceptCave.Data.HelperClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.www.Models;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.Data.DTOConverters;

namespace ConceptCave.www.Controllers
{
    public class PublicVariableGridController : ControllerBase
    {
        private readonly IPublishingGroupResourceRepository _publishingGroupResourceRepo;
        
        public PublicVariableGridController(IPublishingGroupResourceRepository publishingGroupResourceRepo)
        {
            _publishingGroupResourceRepo = publishingGroupResourceRepo;
        }
        //
        // GET: /PublicVariableGrid/

        public ActionResult Index(int id)
        {
            var publish = _publishingGroupResourceRepo.GetById(id, PublishingGroupResourceLoadInstruction.PublicVariables).ToEntity();

            EntityCollection<WorkingDocumentEntity> workingDocuments = WorkingDocumentRepository.GetCurrentWorkingDocuments(id, WorkingDocumentLoadInstructions.PublicVariables);

            PublicVariableGridModel model = new PublicVariableGridModel(workingDocuments, publish);

            return View(model);
        }

    }
}
