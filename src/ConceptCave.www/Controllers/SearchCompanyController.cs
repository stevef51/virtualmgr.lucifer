﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Repository;
using ConceptCave.www.Models;
using ConceptCave.Data.EntityClasses;
using ConceptCave.RepositoryInterfaces.Repositories;

namespace ConceptCave.www.Controllers
{
    public class SearchCompanyController : ControllerBase
    {
        private ICompanyRepository _companyRepo;

        public SearchCompanyController(ICompanyRepository companyRepo)
        {
            _companyRepo = companyRepo;
        }

        //
        // GET: /SearchCompany/

        public ActionResult Index(string query)
        {
            var items = _companyRepo.Search("%" + query + "%", 0, 10, RepositoryInterfaces.Enums.CompanyLoadInstructions.None);

            List<SearchCompanyModel> result = new List<SearchCompanyModel>();

            items.ToList().ForEach(i =>
            {
                result.Add(new SearchCompanyModel()
                {
                    Name = i.Name,
                    Id = i.Id
                });
            });


            return ReturnJson(new { count = result.Count, items = result });
        }

        public ActionResult Details(Guid id)
        {
            var details = _companyRepo.GetById(id, RepositoryInterfaces.Enums.CompanyLoadInstructions.None);

            return ReturnJson(new SearchCompanyModel()
            {
                Name = details.Name,
                Id = details.Id
            });
        }
    }
}
