﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Repository;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.www.Models;
using ConceptCave.Checklist.Editor;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.Repository.Injected;

namespace ConceptCave.www.Controllers
{
    public class SearchChecklistController : ControllerBase
    {
        private readonly IPublishingGroupRepository _publishingGroupRepo;
        private readonly IPublishingGroupResourceRepository _publishingGroupResourceRepo;

        public SearchChecklistController(IPublishingGroupResourceRepository publishingGroupResourceRepo, IPublishingGroupRepository publishingGroupRepo)
        {
            _publishingGroupRepo = publishingGroupRepo;
            _publishingGroupResourceRepo = publishingGroupResourceRepo;
        }
        //
        // GET: /SearchChecklists/

        public ActionResult Index(string query)
        {
            EntityCollection<ResourceEntity> items = ResourceRepository.GetLikeName("%" + query + "%", StandardResourceCategories.Checklists.ToString(), ResourceLoadInstructions.ResourceLabel | ResourceLoadInstructions.Label);

            List<SearchChecklistModel> result = new List<SearchChecklistModel>();

            items.ToList().ForEach(i =>
            {
                result.Add(new SearchChecklistModel() { Id = i.NodeId, Name = i.Name });
            });

            HttpResponseBase response = HttpContext.Response;
            response.ContentType = "text/json";

            return View(result);
        }

        public ActionResult PublishingGroup(string query)
        {
            var items = _publishingGroupRepo.GetLikeName("%" + query + "%");
            var result = new List<SearchPublishingGroupModel>();
            foreach (var i in items)
            {
                result.Add(new SearchPublishingGroupModel(){Id = i.Id, Name = i.Name});
            }
            //HttpResponseBase response = HttpContext.Response;
            //response.ContentType = "text/json";

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PublishedResource(string query, int publishingGroup)
        {
            var items = _publishingGroupResourceRepo.GetLikeName("%" + query + "%", publishingGroup);
            var result = new List<SearchPublishingGroupModel>();
            foreach (var i in items)
                result.Add(new SearchPublishingGroupModel() { Id = i.Id, Name = i.Name });
            //return View("PublishingGroup", result);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SearchPublishedResources(string search)
        {
            var items = _publishingGroupResourceRepo.GetLikeName("%" + search + "%", PublishingGroupResourceLoadInstruction.Group, 1, 5);

            return ReturnJson(items);
        }

        public ActionResult GetAllQuestions(int publishedResourceId, string questionTypeFriendlyName)
        {
            //Load up the design document
            var resource = _publishingGroupResourceRepo.GetById(publishedResourceId, 
                PublishingGroupResourceLoadInstruction.ResourceData | PublishingGroupResourceLoadInstruction.Resource |
                PublishingGroupResourceLoadInstruction.Data);
            var doc = OPublishingGroupResourceRepository.DesignDocumentFromDTO(resource);
            var result = new List<SearchChecklistModel>();
            foreach (var p in (from q in doc.AsDepthFirstEnumerable() where q is IQuestion select q as IQuestion))
            {
                if (RepositorySectionManager.Current.ItemForObjectType(p.GetType()).FriendlyName
                    == questionTypeFriendlyName)
                    result.Add(new SearchChecklistModel() { Id = p.Id, Name = p.Name });
            }

            //HttpResponseBase response = HttpContext.Response;
            //response.ContentType = "text/json";

            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}
