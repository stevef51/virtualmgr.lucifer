﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Repository;
using ConceptCave.www.Models;
using ConceptCave.Data.EntityClasses;
using ConceptCave.RepositoryInterfaces.Repositories;

namespace ConceptCave.www.Controllers
{
    public class SearchAssetController : ControllerBase
    {
        private IAssetRepository _assetRepo;

        public SearchAssetController(IAssetRepository assetRepo)
        {
            _assetRepo = assetRepo;
        }

        //
        // GET: /SearchCompany/

        public ActionResult Index(string query)
        {
            int tot = 0;
            var items = _assetRepo.Search("%" + query + "%", 0, 10, false, ref tot);

            List<SearchAssetModel> result = new List<SearchAssetModel>();

            items.ToList().ForEach(i =>
            {
                result.Add(new SearchAssetModel()
                {
                    Name = i.Name,
                    Id = i.Id
                });
            });


            return ReturnJson(new { count = result.Count, items = result });
        }

        public ActionResult Details(int id)
        {
            var details = _assetRepo.GetById(id);

            return ReturnJson(new SearchAssetModel()
            {
                Name = details.Name,
                Id = details.Id
            });
        }
    }
}
