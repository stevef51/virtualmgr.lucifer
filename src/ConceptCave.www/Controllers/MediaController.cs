﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using ConceptCave.Checklist;
using ConceptCave.Data.EntityClasses;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Repository;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.www.Attributes;

namespace ConceptCave.www.Controllers
{
    [MvcCorsOptionsAllow(Origins = Consts.DefaultCorsAllowOrigin)]
    public class MediaController : ControllerBase
    {
        protected IMediaRepository _mediaRepo;

        public MediaController(IMediaRepository mediaRepo)
        {
            _mediaRepo = mediaRepo;
        }

        public FileResult Index(Guid id, bool inline = false)
        {
            MediaDTO media = _mediaRepo.GetById(id, MediaLoadInstruction.Data);

            if (media == null)
            {
                // ok it could be a link to a media folder file, lets see
                var mediaFile = _mediaRepo.GetFolderFileById(id, MediaFolderFileLoadInstruction.Media | MediaFolderFileLoadInstruction.MediaData);

                if (mediaFile == null)
                {
                    throw new ArgumentException("No media with the specified id exists");
                }

                media = mediaFile.Medium;
            }

            MimeTypeEntity mime = MimeTypeCache.GetByExtension(System.IO.Path.GetExtension(media.Filename));

            if(inline)
            {
                Response.AddHeader("Content-Disposition", "inline; filename=" + media.Filename);
                return this.File(media.MediaData.Data, mime == null ? media.Type : mime.MimeType);
            }

            return this.File(media.MediaData.Data, mime == null ? media.Type : mime.MimeType, media.Filename);
        }

        public FileResult Version(Guid id)
        {
            var media = _mediaRepo.GetMediaVersionById(id, MediaVersionLoadInstructions.Data);

            MimeTypeEntity mime = MimeTypeCache.GetByExtension(System.IO.Path.GetExtension(media.Filename));

            return this.File(media.MediaVersionData.Data, mime == null ? "unknown" : mime.MimeType, media.Filename);
        }

    }
}
