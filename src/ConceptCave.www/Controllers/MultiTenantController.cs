﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Repository;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.www.Models;
using ConceptCave.Configuration;
using ConceptCave.Checklist.Editor;
using ConceptCave.Checklist.RunTime;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.www.Areas.Player.Models;
using ConceptCave.www.API.v1;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.RepositoryInterfaces;
using System.Threading.Tasks;
using ConceptCave.www.App_Start;
using System.IO;
using VirtualMgr.Central;
using NLog;
using Ninject;
using VirtualMgr.Central.Interfaces;
using ConceptCave.DTO.DTOClasses;
using System.Configuration;
using Microsoft.SqlServer.Dac;

namespace ConceptCave.www.Controllers
{
    public class MultiTenantController : AsyncController
    {
        private static readonly Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        private readonly Func<Stream> _fnDacpacStream;
        private readonly Func<IEnumerable<IUpdateScript>> _fnBeforeScripts;
        private readonly Func<IEnumerable<IUpdateScript>> _fnAfterScripts;
        private readonly IErrorLogRepository _errorLogRepo;

        public MultiTenantController(
            [Named("ConceptCave.sql.DacPac")] Func<Stream> fnDacpacStream,
            [Named("BeforeSchemaUpdateScripts")] Func<IEnumerable<IUpdateScript>> fnBeforeScripts,
            [Named("AfterSchemaUpdateScripts")] Func<IEnumerable<IUpdateScript>> fnAfterScripts,
            IErrorLogRepository errorLogRepo
        )
        {
            _fnDacpacStream = fnDacpacStream;
            _fnBeforeScripts = fnBeforeScripts;
            _fnAfterScripts = fnAfterScripts;
            _errorLogRepo = errorLogRepo;
        }

        private void UpdateTenantMasterDatabaseSchema(IContinuousProgressCategory progress)
        {
            var tdb = MultiTenantManager.GetTenantMasterDatabase();
            var beforeScripts = _fnBeforeScripts();
            var afterScripts = _fnAfterScripts();

            progress.Working("Running before schema scripts...");
            tdb.UpdateDatabaseScripts(beforeScripts, null);

            using (var dacpacStream = _fnDacpacStream())
            {
                progress.Working("Running schema update...");
                tdb.UpdateDatabaseSchema(dacpacStream, true, (msg) => progress.Working(msg));
            }

            progress.Working("Running after schema scripts...");
            tdb.UpdateDatabaseScripts(afterScripts, null);

            progress.Working("Schema updated");

        }

        private void UpdateTenantDatabaseSchema(IContinuousProgress progress, string tenantName, bool force)
        {
            var runningOnAzure = ConfigurationManager.AppSettings["WEBSITE_SITE_NAME"] != null;

            var swt = new LoggingStopWatch("CheckTenantDatabaseForUpdates", _logger);
            swt.Mark("Checking tenant databases for upgrades...");
            var beforeScripts = _fnBeforeScripts();
            var afterScripts = _fnAfterScripts();


            var progressCategory = progress.NewCategory("Source DACPAC");
            string dacpacVersion = null;
            using (var dacpacStream = _fnDacpacStream())
            {
                DacPackage package = DacPackage.Load(dacpacStream);
                dacpacVersion = package.Version.ToString();
            }
            progressCategory.Complete("Schema Version = " + dacpacVersion);

            var uniqueConnectionStrings = new HashSet<string>();
            var sortedTenants = VirtualMgr.Central.MultiTenantManager.Tenants.OrderBy(t => t.Profile.IsTenantMaster ? 0 : t.Profile.IsProduction ? 1 : 2).ThenBy(t => t.Profile.Name).ToArray();
            foreach (var tenant in sortedTenants)
            {
                if(string.IsNullOrEmpty(tenantName) == false && tenant.Name != tenantName)
                {
                    continue;
                }

                if (uniqueConnectionStrings.Contains(tenant.ConnectionString))
                    continue;

                uniqueConnectionStrings.Add(tenant.ConnectionString);

                progressCategory = progress.NewCategory($"{tenant.Name} ({tenant.Profile.Name})");
                var sw = new LoggingStopWatch("Checking " + tenant.HostName, _logger);

                if (!tenant.Profile.IsDevelopment && (tenant.IsAzureDatabase && !runningOnAzure))
                {
                    progressCategory.Warning("Cannot update Azure database outside Azure");
                    continue;
                }

                MultiTenantManager.TenantContextHostName = tenant.HostName;
                try
                {
                    var tdb = tenant.GetTenantDatabase();
                    string tdsv = tdb.DatabaseSchemaVersion;
                    progressCategory.Working("Schema Version = " + tdsv);
                    if (!force && tdsv == dacpacVersion)
                    {
                        progressCategory.Complete("Skipping, version match");
                        continue;
                    }
                    progressCategory.Working("Running before schema scripts...");
                    sw.Mark("Running Before Schema scripts");
                    tdb.UpdateDatabaseScripts(beforeScripts, null);
                    using (var dacpacStream = _fnDacpacStream())
                    {
                        sw.Mark("Running Schema update");
                        progressCategory.Working("Running schema update...");
                        tdb.UpdateDatabaseSchema(dacpacStream, true, (msg) => progressCategory.Working(msg));
                    }
                    progressCategory.Working("Running after schema scripts...");
                    sw.Mark("Running After Schema scripts");
                    tdb.UpdateDatabaseScripts(afterScripts, null);
                    progressCategory.Complete("Schema updated");
                }
                catch (Exception ex)
                {
                    try
                    {
                        progressCategory.Error(ex.Message);
                        sw.Mark("Exception on Tenant Database Update " + ex.Message);

                        _errorLogRepo.LogError(new ErrorLogTypeDTO()
                        {
                            Type = "server",
                            Source = "CheckTenantDatabaseForUpdates",
                            Message = ex.Message,
                            StackTrace = ex.StackTrace
                        }, new ErrorLogItemDTO()
                        {
                            UserId = null,
                            UserAgentId = null,
                            Data = tenant.HostName
                        });
                    }
                    catch (Exception ex2)
                    {
                        // Not good, we threw trying to log errors
                        sw.Mark("Threw trying to log error " + ex2.Message);
                    }
                }
                sw.Log(LogLevel.Info);
            }
            swt.Log(LogLevel.Info);
        }

        [OutputCache(Duration = 0)]
        [HttpGet]
        public void UpdateDatabaseSchemas(string format = "text", string tenantName = "", bool force = false)
        {
            IContinuousProgress progress = null;
            switch (format.ToLower())
            {
                case "text":
                    progress = new ContinuousProgressTextResponse(Response);
                    break;

                case "json":
                    progress = new ContinuousProgressJSONResponse(Response);
                    break;

                default:
                    progress = new ContinuousProgressTextResponse();
                    break;
            }

/*          
 *          if(string.IsNullOrEmpty(tenantName) || tenantName.ToLower() == "tenant master")
            {
                UpdateTenantMasterDatabaseSchema(progress.NewCategory("Tenant Master"));
            }
            */

            UpdateTenantDatabaseSchema(progress, tenantName, force);
            progress.Finished();
        }
    }
}
