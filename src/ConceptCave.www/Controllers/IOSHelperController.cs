﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Repository;
using ConceptCave.Checklist.RunTime;
using ConceptCave.Checklist.Interfaces;
using System.Transactions;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.www.Areas.Player.Models;

namespace ConceptCave.www.Controllers
{
    public class IOSHelperController : ControllerBase
    {
        public ActionResult SaveNoteAttachment(Guid rootId, Guid presentedId, HttpPostedFileBase fileUpload)
        {
            WorkingDocumentRepository.SaveNotesAttachment(rootId, presentedId, new HttpPostedFileAdapter(fileUpload));

            return PartialView();
        }

        public ActionResult Continue()
        {
            return View();
        }
    }
}
