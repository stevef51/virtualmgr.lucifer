﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.Data.HelperClasses;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Repository;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.www.Models;
using ConceptCave.Configuration;
using ConceptCave.Checklist.Editor;
using ConceptCave.Checklist.RunTime;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.www.Areas.Player.Models;

namespace ConceptCave.www.Controllers
{
    public class HomeController : ControllerBase
    {
        [ValidateInput(false)]
        public ActionResult Index()
        {
            //very simple, return the angular app
            return View();
        }

        public ActionResult About()
        {
            return View();
        }
    }
}
