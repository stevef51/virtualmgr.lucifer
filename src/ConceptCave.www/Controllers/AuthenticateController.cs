﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConceptCave.BusinessLogic.LingoDataObjects;
using ConceptCave.BusinessLogic.Context;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.www.Models;
using System.Web.Security;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Repository;
using System.Transactions;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Repository.Facilities;
using ConceptCave.Checklist.Lingo;
using ConceptCave.Checklist.RunTime;
using System.Net.Mail;
using ConceptCave.Checklist;
using Newtonsoft.Json;
using ConceptCave.BusinessLogic.Json;
using ConceptCave.www.Attributes;
using Newtonsoft.Json.Linq;
using Microsoft.Owin.Security;
using VirtualMgr.Membership.Direct.IdentityOverrides;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace ConceptCave.www.Controllers
{
    [MvcCorsOptionsAllow(Origins = Consts.DefaultCorsAllowOrigin)]
    public class AuthenticateController : Controller
    {
        private readonly IMembershipRepository _memberRepo;
        private readonly ILabelRepository _labelRepo;
        private readonly ContextManagerFactory _ctxFac;
        private readonly IEmailConfiguration _emailConfiguration;
        private readonly ITokenGenerator _tokenGenerator;
        private readonly IPublishingGroupResourceRepository _publishingGroupResourceRepo;
        private readonly VirtualMgr.Membership.IMembership _membership;

        public AuthenticateController(IMembershipRepository memberRepo, ILabelRepository labelRepo, ContextManagerFactory ctxFac, IEmailConfiguration emailConfiguration, IPublishingGroupResourceRepository publishingGroupResourceRepo, VirtualMgr.Membership.IMembership membership, ITokenGenerator tokenGenerator)
        {
            _memberRepo = memberRepo;
            _labelRepo = labelRepo;
            _ctxFac = ctxFac;
            _emailConfiguration = emailConfiguration;
            _publishingGroupResourceRepo = publishingGroupResourceRepo;
            _membership = membership;
            _tokenGenerator = tokenGenerator;
        }

        public static bool PersistentLogin
        {
            get
            {
                return GlobalSettingsRepository.GetSetting<bool>("PersistentLogin");
            }
        }

        public ActionResult Index(Guid? ticket)
        {
            if (ticket.HasValue)
            {
                return AnonymousLogin(ticket.Value);
            }

            return View(new AuthenticateModel() { 
                StoreLoginCoordinates = GlobalSettingsRepository.GetSetting<bool>("StoreLoginCoordinates"),
                HasForgottenPasswordManagement = string.IsNullOrEmpty(GlobalSettingsRepository.GetSetting<string>(GlobalSettingsConstants.SystemSetting_ForgottenPasswordEmailBody)) == false
            });
        }

        protected bool ValidateUser(string username, string password)
        {
            if (_membership.ValidateUser(username, password) == false)
            {
                return false;
            }
            

            // need to see if they're login has expired or not
            var user = _memberRepo.GetByUsername(username, MembershipLoadInstructions.MembershipType);

            //but first, are they of a loginable type?
            if (!user.UserType.CanLogin)
                return false;

            if (user.ExpiryDate.HasValue == false)
            {
                // this one doesn't expire
                return true;
            }

            DateTime utcNow = DateTime.UtcNow;
            DateTime timeInZone = TimeZoneInfo.ConvertTimeFromUtc(utcNow, user.TimeZoneInfo);

            if (timeInZone > user.ExpiryDate)
            {
                return false;
            }

            return true;
        }

        protected IAuthenticationManager GetSignInManager()
        {
            return System.Web.HttpContext.Current.GetOwinContext().Authentication;
        }

        protected ApplicationUserManager GetUserManager()
        {
            return System.Web.HttpContext.Current.GetOwinContext().Get<ApplicationUserManager>();
        }

        protected void SignIn(string username)
        {
            var signInManager = GetSignInManager();
            var userManager = GetUserManager();

            var user = userManager.FindByName(username);
            var internalUser = _memberRepo.GetById(user.Id);

            var userIdentity = userManager.CreateIdentity(user, DefaultAuthenticationTypes.ApplicationCookie);
            // we add a claim so that we can easily get at the internal id of the user without having to go back to the database
            userIdentity.AddClaim(new System.Security.Claims.Claim(MembershipDirectCurrentContextProvider.InternalUserIdClaimType, internalUser.UserId.ToString()));

            signInManager.SignIn(new AuthenticationProperties() { IsPersistent = false  }, userIdentity);
        }

        public ActionResult ApiLogin(AuthenticateModel model)
        {
            if (this.IsValidLoginArgument(model.Username, model.Password))
            {
                if (ValidateUser(model.Username, model.Password))
                {
                    var uid = _memberRepo.GetByUsername(model.Username, MembershipLoadInstructions.None).UserId;
                    RequestEventManager.RaiseLogIn(System.Web.HttpContext.Current, uid, model.TabletUUID);

                    SignIn(model.Username);

                    // it would be nice to do success and failure through HTTP response codes
                    // but forms authentication is getting in the way of doing this and 
                    // redirects back round to the login page, which we don't want
                    return Content("true");
                }
            }

            return Content("false");
        }

        //[HttpPost]
        public ActionResult Login(AuthenticateModel model)
        {
            if(model.IsApiLogin == true)
            {
                return ApiLogin(model);
            }

            if (this.IsValidLoginArgument(model.Username, model.Password))
            {
                if (ValidateUser(model.Username, model.Password))
                {
                    var uid = _memberRepo.GetByUsername(model.Username, MembershipLoadInstructions.None).UserId;
                    RequestEventManager.RaiseLogIn(System.Web.HttpContext.Current, uid, model.TabletUUID);
                    this.RedirectFromLoginPage(model.Username, Url.Content("~/"));
                }
                else
                    this.ViewData["LoginFailed"] = true;
            }

            return View("Index", new AuthenticateModel() {
                StoreLoginCoordinates = GlobalSettingsRepository.GetSetting<bool>("StoreLoginCoordinates"),
                HasForgottenPasswordManagement = string.IsNullOrEmpty(GlobalSettingsRepository.GetSetting<string>(GlobalSettingsConstants.SystemSetting_ForgottenPasswordEmailBody)) == false
            });
        }

        [HttpPost]
        public ActionResult ValidateAuthentication(AuthenticateModel model)
        {
            var result = new Models.AuthenticateResult();

            if (this.IsValidLoginArgument(model.Username, model.Password))
            {
                if (ValidateUser(model.Username, model.Password))
                {
                    result.Success = true;
                }
                else
                {
                    result.Success = false;
                }
            }

            if(result.Success == true && model.RefreshShortcutLoginLabels != null && model.RefreshShortcutLoginLabels.Length > 0)
            {
                // ok, the validation request has also come in with a set of labels, for which any users associated
                // with it also need to be stored on the device. Grab them and send them back.
                var users = _memberRepo.GetAllUsersByLabel(model.RefreshShortcutLoginLabels);

                result.LabelShortcutLogins = new List<AuthenticateShortcutLoginResult>();
                foreach(var u in users)
                {
                    result.LabelShortcutLogins.Add(new AuthenticateShortcutLoginResult() { Id = u.UserId, Name = u.Name, Username = u.Username });
                }
            }

            var settings = new JsonSerializerSettings();
            LowercaseContractResolver resolver = new LowercaseContractResolver();
            settings.ContractResolver = resolver;
            settings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;

            return Content(JsonConvert.SerializeObject(result, Formatting.Indented, settings));
        }

        public ActionResult Logoff()
        {
            //TODO: Sign out event
            var uid = _memberRepo.GetByUsername(HttpContext.User.Identity.Name, MembershipLoadInstructions.None).UserId;
            RequestEventManager.RaiseLogOut(System.Web.HttpContext.Current, uid);
            //Session.Clear();

            var signInManager = GetSignInManager();
            signInManager.SignOut(new AuthenticationProperties() { });

            return this.RedirectToAction("Index");
        }

        public ActionResult AnonymousLogin(Guid ticket)
        {
            PublicTicketEntity t = PublicTicketRepository.GetById(ticket, PublicTicketLoadInstructions.WorkingDocumentTickets | PublicTicketLoadInstructions.PublishingGroupResourceTickets);

            if (t == null)
            {
                throw new HttpException(401, "Unauthorized");
            }

            if (t.Status == (int)PublicTicketStatus.Disabled)
            {
                throw new HttpException(401, "Unauthorized");
            }

            if (t.PublicWorkingDocuments.Count == 0 && t.PublicPublishingGroupResources.Count == 0)
            {
                throw new HttpException(401, "Unauthorized");
            }

            if (t.PublicWorkingDocuments.Count > 0)
            {
                WorkingDocumentEntity wd = WorkingDocumentRepository.GetById(t.PublicWorkingDocuments[0].WorkingDocumentId, WorkingDocumentLoadInstructions.None);

                if (wd.Status == (int)WorkingDocumentStatus.Completed)
                {
                    throw new HttpException(401, "Unauthorized");
                }

                var user = _membership.GetUser(wd.ReviewerId);

                RedirectFromLoginPage(user.UserName, Url.Content(string.Format("~/#/player/play/continue?id={0}", wd.Id)));
            }
            else
            {
                // ok, so its an anonymous checklist generated from a published one
                var groupResource = _publishingGroupResourceRepo.GetById(t.PublicPublishingGroupResources[0].PublishingGroupResourceId, PublishingGroupResourceLoadInstruction.None);
                var user = _membership.GetUser(t.PublicPublishingGroupResources[0].UserId);

                string args = "";
                JObject obj = null;
                foreach (var v in HttpContext.Request.QueryString.AllKeys)
                {
                    if(v.ToLower() == "ticket")
                    {
                        continue;
                    }

                    if(obj == null)
                    {
                        obj = new JObject();
                    }

                    obj[v] = HttpContext.Request.QueryString[v];
                }

                if(obj != null)
                {
                    args = obj.ToString(Formatting.None);
                    args = Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(args));
                }

                RedirectFromLoginPage(user.UserName, Url.Content(string.Format("~/#/player/begin/?groupId={0}&resourceid={2}&revieweeid={1}&publishingGroupResourceId={3}&args={4}", groupResource.PublishingGroupId, t.PublicPublishingGroupResources[0].UserId, groupResource.ResourceId, t.PublicPublishingGroupResources[0].PublishingGroupResourceId, args)));
            }

            return View("Index");
        }

        private void RedirectFromLoginPage(string userName, string ReturnUrl)
        {
            SignIn(userName);

            if (!string.IsNullOrEmpty(ReturnUrl))
                Response.Redirect(ReturnUrl);
            else
                Response.Redirect(FormsAuthentication.DefaultUrl);
        }

        private bool IsValidLoginArgument(string userName, string password)
        {
            return !(string.IsNullOrEmpty(userName) && string.IsNullOrEmpty(password));
        }

        public ActionResult StartForgottenPassword()
        {
            return View();
        }

        public JsonResult ForgottenPassword(string username)
        {
            var jsonResult = new Dictionary<string, object>();
            jsonResult["Success"] = false;
            jsonResult["Message"] = "";
            if (string.IsNullOrEmpty(username))
            {
                jsonResult["Message"] = "A username must be supplied";

                return Json(jsonResult);
            }

            var member = _membership.GetUser(username);

            if (member == null)
            {
                jsonResult["Message"] = "The username does not exist";

                return Json(jsonResult);
            }

            if (string.IsNullOrEmpty(member.Email) == true)
            {
                string message = "An email address is not associated with the username, you will need to contact the system administrator to reset your password.";

                string m = GlobalSettingsRepository.GetSetting<string>(GlobalSettingsConstants.SystemSetting_ForgottenPasswordNoEmailAddressMessage);
                if (string.IsNullOrEmpty(m) == false)
                {
                    message = m;
                }

                jsonResult["Message"] = message;

                return Json(jsonResult);
            }

            var user = _memberRepo.GetById((Guid)member.ProviderUserKey, MembershipLoadInstructions.AspNetMembership);
            
            // create a password reset ticket
            PublicTicketEntity ticket = new PublicTicketEntity();
            ticket.Id = CombFactory.NewComb();
            ticket.Status = (int)PublicTicketStatus.Enabled;
            ticket.PasswordResetTicket = new PasswordResetTicketEntity() { DateCreated = DateTime.UtcNow, ExpiryDate = DateTime.UtcNow.AddDays(1), UserId = user.UserId };
            PublicTicketRepository.Save(ticket, true, true);

            string emailBody = GlobalSettingsRepository.GetSetting<string>(GlobalSettingsConstants.SystemSetting_ForgottenPasswordEmailBody);

            LingoProgramDefinition definition = new LingoProgramDefinition() { SourceCode = "" };
            WorkingDocument wd = new WorkingDocument();
            ILingoProgram program = definition.Compile(wd);
            var vb = wd.Variables;

            vb.Variable<object>("User", new FacilityUserHelper(_memberRepo, _labelRepo, _ctxFac, user.UserId));
            vb.Variable<object>("TicketId", ticket.Id);
            vb.Variable<object>("ResetUrl", string.Format("{0}/Authenticate/PasswordReset?t={1}", Url.ToFullyQualifiedUrl("~/").TrimEnd('/'), ticket.Id.ToString()));

            emailBody = program.ExpandString(emailBody);

            SmtpClient client = new SmtpClient();
            var mail = new MailMessage();
            mail.To.Add(member.Email);
            mail.Subject = "Password Reset Request";
            mail.Body = emailBody;
            mail.IsBodyHtml = true;
            if (_emailConfiguration != null)
            {
                mail.From = _emailConfiguration.From;
            }
            client.Send(mail);

            jsonResult["Success"] = true;

            return Json(jsonResult);
        }

        public ActionResult PasswordReset(Guid t)
        {
            var ticket = PublicTicketRepository.GetById(t, PublicTicketLoadInstructions.PasswordReset);

            if (ticket == null || ticket.PasswordResetTicket == null || ticket.Status == 0)
            {
                throw new InvalidOperationException("This is not a valid password reset ticket");
            }

            return View("PasswordReset", t);
        }

        public JsonResult SetPassword(Guid t, string username, string password, string confirm)
        {
            var jsonResult = new Dictionary<string, object>();
            jsonResult["Success"] = false;
            jsonResult["Message"] = "";

            var ticket = PublicTicketRepository.GetById(t, PublicTicketLoadInstructions.PasswordReset);

            if (ticket == null || ticket.PasswordResetTicket == null || ticket.Status == 0)
            {
                jsonResult["Message"] = "This is not a valid password reset ticket";

                return Json(jsonResult);
            }

            if (string.IsNullOrEmpty(username))
            {
                jsonResult["Message"] = "A username must be supplied";

                return Json(jsonResult);
            }

            var member = _membership.GetUser(username);

            if (member == null)
            {
                jsonResult["Message"] = "The username does not exist";

                return Json(jsonResult);
            }

            if (ticket.PasswordResetTicket.UserId != (Guid)member.ProviderUserKey)
            {
                jsonResult["Message"] = "This ticket is not associated with this user";

                return Json(jsonResult);
            }

            if (string.IsNullOrEmpty(password) || string.IsNullOrEmpty(confirm))
            {
                jsonResult["Message"] = "You must supply matching values for the password and password confirmation";

                return Json(jsonResult);
            }

            if (password != confirm)
            {
                jsonResult["Message"] = "You must supply matching values for the password and password confirmation";

                return Json(jsonResult);
            }

            bool result = false;
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    // EVS-415 Change Password Checklist To Support "Unlocking" Locked Users
                    // When ever password is changed successfully we unlock the user
                    if (member.IsLockedOut)
                        member.UnlockUser();
                    result = member.ChangePassword(member.ResetPassword(), password);

                    ticket.Status = (int)PublicTicketStatus.Disabled;
                    PublicTicketRepository.Save(ticket, false, true);

                    scope.Complete();
                }
            }
            catch (Exception e)
            {
                jsonResult["Message"] = string.Format("There was a problem changing your password. The minimum length for a password is {0}, please make sure the password you have entered is at least {0} characters in length.", _membership.MinRequiredPasswordLength);
            }

            jsonResult["Success"] = result;

            return Json(jsonResult);
        }
    }
}
