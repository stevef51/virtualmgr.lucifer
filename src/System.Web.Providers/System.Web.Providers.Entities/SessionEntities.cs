using System;
using System.Data.EntityClient;
using System.Data.Objects;
namespace System.Web.Providers.Entities
{
	internal sealed class SessionEntities : ObjectContext
	{
		private ObjectSet<SessionEntity> _Sessions;
		public ObjectSet<SessionEntity> Sessions
		{
			get
			{
				if (this._Sessions == null)
				{
					this._Sessions = base.CreateObjectSet<SessionEntity>("Sessions");
				}
				return this._Sessions;
			}
		}
		public SessionEntities(EntityConnection connection) : base(connection, "SessionEntities")
		{
			base.ContextOptions.LazyLoadingEnabled = true;
		}
	}
}
