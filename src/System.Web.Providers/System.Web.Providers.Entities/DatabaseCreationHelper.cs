using System;
using System.Data;
using System.Data.EntityClient;
using System.Data.Objects;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Security;
using System.Threading;
namespace System.Web.Providers.Entities
{
	internal static class DatabaseCreationHelper
	{
		internal const string DataDirectory = "|datadirectory|";
		private static readonly Type StackOverflowType = typeof(StackOverflowException);
		private static readonly Type OutOfMemoryType = typeof(OutOfMemoryException);
		private static readonly Type ThreadAbortType = typeof(ThreadAbortException);
		private static readonly Type NullReferenceType = typeof(NullReferenceException);
		private static readonly Type AccessViolationType = typeof(AccessViolationException);
		private static readonly Type SecurityType = typeof(SecurityException);
		internal static bool ManualWorkNeeded(ObjectContext context, out SqlConnection sqlConnection, out SqlConnectionStringBuilder connectionStringBuilder)
		{
			EntityConnection entityConnection = (EntityConnection)context.Connection;
			sqlConnection = (entityConnection.StoreConnection as SqlConnection);
			if (sqlConnection == null)
			{
				connectionStringBuilder = null;
				return false;
			}
			connectionStringBuilder = new SqlConnectionStringBuilder(sqlConnection.ConnectionString);
			return !string.IsNullOrEmpty(connectionStringBuilder.AttachDBFilename);
		}
		internal static void CreateDatabase(ObjectContext context)
		{
			SqlConnection sqlConnection;
			SqlConnectionStringBuilder connectionStringBuilder;
			if (!DatabaseCreationHelper.ManualWorkNeeded(context, out sqlConnection, out connectionStringBuilder))
			{
				context.CreateDatabase();
				return;
			}
			string databaseName;
			string dataFileName;
			string logFileName;
			DatabaseCreationHelper.GetOrGenerateDatabaseNameAndGetFileNames(connectionStringBuilder, out databaseName, out dataFileName, out logFileName);
			string createDatabaseScript = SqlDdlBuilder.CreateDatabaseScript(databaseName, dataFileName, logFileName);
			string createObjectsScript = context.CreateDatabaseScript();
			DatabaseCreationHelper.UsingMasterConnection(sqlConnection, delegate(SqlConnection conn)
			{
				DatabaseCreationHelper.CreateCommand(conn, createDatabaseScript).ExecuteNonQuery();
			}
			);
			try
			{
				SqlConnection.ClearPool(sqlConnection);
				DatabaseCreationHelper.UsingConnection(sqlConnection, delegate(SqlConnection conn)
				{
					DatabaseCreationHelper.CreateCommand(conn, createObjectsScript).ExecuteNonQuery();
				}
				);
			}
			catch (Exception ex)
			{
				if (DatabaseCreationHelper.IsCatchableExceptionType(ex))
				{
					throw new InvalidOperationException("The database creation succeeded, but the creation of the database objects failed. The consequent attempt to drop the database also failed. See InnerException for details.", ex);
				}
				throw;
			}
		}
		internal static bool DatabaseExists(ObjectContext context)
		{
			SqlConnection sqlConnection;
			SqlConnectionStringBuilder sqlConnectionStringBuilder;
			if (!DatabaseCreationHelper.ManualWorkNeeded(context, out sqlConnection, out sqlConnectionStringBuilder))
			{
				return context.DatabaseExists();
			}
			return (!string.IsNullOrEmpty(sqlConnectionStringBuilder.InitialCatalog) && context.DatabaseExists()) || File.Exists(DatabaseCreationHelper.GetMdfFileName(sqlConnectionStringBuilder.AttachDBFilename));
		}
		internal static bool IsCatchableExceptionType(Exception e)
		{
			Type type = e.GetType();
			return type != DatabaseCreationHelper.StackOverflowType && type != DatabaseCreationHelper.OutOfMemoryType && type != DatabaseCreationHelper.ThreadAbortType && type != DatabaseCreationHelper.NullReferenceType && type != DatabaseCreationHelper.AccessViolationType && !DatabaseCreationHelper.SecurityType.IsAssignableFrom(type);
		}
		private static void UsingMasterConnection(SqlConnection sqlConnection, Action<SqlConnection> act)
		{
			SqlConnectionStringBuilder sqlConnectionStringBuilder = new SqlConnectionStringBuilder(sqlConnection.ConnectionString)
			{
				InitialCatalog = "master",
				AttachDBFilename = string.Empty
			};
			try
			{
				using (SqlConnection sqlConnection2 = new SqlConnection(sqlConnectionStringBuilder.ConnectionString))
				{
					DatabaseCreationHelper.UsingConnection(sqlConnection2, act);
				}
			}
			catch (SqlException innerException)
			{
				if (!sqlConnectionStringBuilder.IntegratedSecurity && (string.IsNullOrEmpty(sqlConnectionStringBuilder.UserID) || string.IsNullOrEmpty(sqlConnectionStringBuilder.Password)))
				{
					throw new InvalidOperationException("This operation requires a connection to the 'master' database. Unable to create a connection to the 'master' database because the original database connection has been opened and credentials have been removed from the connection string. Supply an unopened connection.", innerException);
				}
				throw;
			}
		}
		private static void UsingConnection(SqlConnection sqlConnection, Action<SqlConnection> act)
		{
			string connectionString = sqlConnection.ConnectionString;
			bool flag = sqlConnection.State == ConnectionState.Closed;
			if (flag)
			{
				sqlConnection.Open();
			}
			try
			{
				act(sqlConnection);
			}
			finally
			{
				if (flag && sqlConnection.State == ConnectionState.Open)
				{
					sqlConnection.Close();
				}
				if (sqlConnection.ConnectionString != connectionString)
				{
					sqlConnection.ConnectionString = connectionString;
				}
			}
		}
		private static SqlCommand CreateCommand(SqlConnection sqlConnection, string commandText)
		{
			if (string.IsNullOrEmpty(commandText))
			{
				commandText = Environment.NewLine;
			}
			return new SqlCommand(commandText, sqlConnection);
		}
		private static void GetOrGenerateDatabaseNameAndGetFileNames(SqlConnectionStringBuilder connectionStringBuilder, out string databaseName, out string dataFileName, out string logFileName)
		{
			string attachDBFilename = connectionStringBuilder.AttachDBFilename;
			if (string.IsNullOrEmpty(attachDBFilename))
			{
				dataFileName = null;
				logFileName = null;
			}
			else
			{
				dataFileName = DatabaseCreationHelper.GetMdfFileName(attachDBFilename);
				logFileName = DatabaseCreationHelper.GetLdfFileName(dataFileName);
			}
			if (!string.IsNullOrEmpty(connectionStringBuilder.InitialCatalog))
			{
				databaseName = connectionStringBuilder.InitialCatalog;
				return;
			}
			databaseName = DatabaseCreationHelper.GenerateDatabaseName(dataFileName);
		}
		private static string GetLdfFileName(string dataFileName)
		{
			DirectoryInfo directory = new FileInfo(dataFileName).Directory;
			return Path.Combine(directory.FullName, Path.GetFileNameWithoutExtension(dataFileName) + "_log.ldf");
		}
		private static string GetMdfFileName(string attachDBFile)
		{
			string text = DatabaseCreationHelper.ExpandDataDirectory("AttachDBFilename", attachDBFile);
			return text ?? attachDBFile;
		}
		private static string GenerateDatabaseName(string mdfFileName)
		{
			string path = mdfFileName.ToUpper(CultureInfo.InvariantCulture);
			char[] array = Path.GetFileNameWithoutExtension(path).ToCharArray();
			for (int i = 0; i < array.Length; i++)
			{
				if (!char.IsLetterOrDigit(array[i]))
				{
					array[i] = '_';
				}
			}
			string text = new string(array);
			string result;
			if (text.Length > 30)
			{
				result = string.Format(CultureInfo.InvariantCulture, "{0}_{1}", new object[]
				{
					text.Substring(0, 30),
					Guid.NewGuid().ToString("N", CultureInfo.InvariantCulture)
				});
			}
			else
			{
				result = string.Format(CultureInfo.InvariantCulture, "{0}_{1}", new object[]
				{
					text,
					Guid.NewGuid().ToString("N", CultureInfo.InvariantCulture)
				});
			}
			return result;
		}
		internal static string ExpandDataDirectory(string keyword, string value)
		{
			string text = null;
			if (value != null && value.StartsWith("|datadirectory|", StringComparison.OrdinalIgnoreCase))
			{
				object data = AppDomain.CurrentDomain.GetData("DataDirectory");
				string text2 = data as string;
				if (data != null && text2 == null)
				{
					throw new InvalidOperationException("The DataDirectory substitute is not a string");
				}
				if (text2 == string.Empty)
				{
					text2 = AppDomain.CurrentDomain.BaseDirectory;
				}
				if (text2 == null)
				{
					text2 = "";
				}
				int length = "|datadirectory|".Length;
				bool flag = 0 < text2.Length && text2[text2.Length - 1] == '\\';
				bool flag2 = length < value.Length && value[length] == '\\';
				if (!flag && !flag2)
				{
					text = text2 + '\\' + value.Substring(length);
				}
				else
				{
					if (flag && flag2)
					{
						text = text2 + value.Substring(length + 1);
					}
					else
					{
						text = text2 + value.Substring(length);
					}
				}
				if (!Path.GetFullPath(text).StartsWith(text2, StringComparison.Ordinal))
				{
					throw new ArgumentException(string.Format(CultureInfo.CurrentCulture, "Invalid value for key '{0}'.", new object[]
					{
						keyword
					}));
				}
			}
			return text;
		}
	}
}
