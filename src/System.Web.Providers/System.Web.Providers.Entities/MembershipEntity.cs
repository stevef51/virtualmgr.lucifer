using System;
using System.Data.Objects.DataClasses;
namespace System.Web.Providers.Entities
{
	[EdmEntityType(NamespaceName = "System.Web.Providers.Entities", Name = "Membership")]
	public class MembershipEntity : EntityObject
	{
		private Guid _ApplicationId;
		private Guid _UserId;
		private string _Password;
		private int _PasswordFormat;
		private string _PasswordSalt;
		private string _Email;
		private string _PasswordQuestion;
		private string _PasswordAnswer;
		private bool _IsApproved;
		private bool _IsLockedOut;
		private DateTime _CreateDate;
		private DateTime _LastLoginDate;
		private DateTime _LastPasswordChangedDate;
		private DateTime _LastLockoutDate;
		private int _FailedPasswordAttemptCount;
		private DateTime _FailedPasswordAttemptWindowStart;
		private int _FailedPasswordAnswerAttemptCount;
		private DateTime _FailedPasswordAnswerAttemptWindowsStart;
		private string _Comment;
		[EdmScalarProperty(EntityKeyProperty = false, IsNullable = false)]
		public Guid ApplicationId
		{
			get
			{
				return this._ApplicationId;
			}
			set
			{
				this.ReportPropertyChanging("ApplicationId");
				this._ApplicationId = StructuralObject.SetValidValue(value);
				this.ReportPropertyChanged("ApplicationId");
			}
		}
		[EdmScalarProperty(EntityKeyProperty = true, IsNullable = false)]
		public Guid UserId
		{
			get
			{
				return this._UserId;
			}
			set
			{
				if (this._UserId != value)
				{
					this.ReportPropertyChanging("UserId");
					this._UserId = StructuralObject.SetValidValue(value);
					this.ReportPropertyChanged("UserId");
				}
			}
		}
		[EdmScalarProperty(EntityKeyProperty = false, IsNullable = false)]
		public string Password
		{
			get
			{
				return this._Password;
			}
			set
			{
				this.ReportPropertyChanging("Password");
				this._Password = StructuralObject.SetValidValue(value, false);
				this.ReportPropertyChanged("Password");
			}
		}
		[EdmScalarProperty(EntityKeyProperty = false, IsNullable = false)]
		public int PasswordFormat
		{
			get
			{
				return this._PasswordFormat;
			}
			set
			{
				this.ReportPropertyChanging("PasswordFormat");
				this._PasswordFormat = StructuralObject.SetValidValue(value);
				this.ReportPropertyChanged("PasswordFormat");
			}
		}
		[EdmScalarProperty(EntityKeyProperty = false, IsNullable = false)]
		public string PasswordSalt
		{
			get
			{
				return this._PasswordSalt;
			}
			set
			{
				this.ReportPropertyChanging("PasswordSalt");
				this._PasswordSalt = StructuralObject.SetValidValue(value, false);
				this.ReportPropertyChanged("PasswordSalt");
			}
		}
		[EdmScalarProperty(EntityKeyProperty = false, IsNullable = true)]
		public string Email
		{
			get
			{
				return this._Email;
			}
			set
			{
				this.ReportPropertyChanging("Email");
				this._Email = StructuralObject.SetValidValue(value, true);
				this.ReportPropertyChanged("Email");
			}
		}
		[EdmScalarProperty(EntityKeyProperty = false, IsNullable = true)]
		public string PasswordQuestion
		{
			get
			{
				return this._PasswordQuestion;
			}
			set
			{
				this.ReportPropertyChanging("PasswordQuestion");
				this._PasswordQuestion = StructuralObject.SetValidValue(value, true);
				this.ReportPropertyChanged("PasswordQuestion");
			}
		}
		[EdmScalarProperty(EntityKeyProperty = false, IsNullable = true)]
		public string PasswordAnswer
		{
			get
			{
				return this._PasswordAnswer;
			}
			set
			{
				this.ReportPropertyChanging("PasswordAnswer");
				this._PasswordAnswer = StructuralObject.SetValidValue(value, true);
				this.ReportPropertyChanged("PasswordAnswer");
			}
		}
		[EdmScalarProperty(EntityKeyProperty = false, IsNullable = false)]
		public bool IsApproved
		{
			get
			{
				return this._IsApproved;
			}
			set
			{
				this.ReportPropertyChanging("IsApproved");
				this._IsApproved = StructuralObject.SetValidValue(value);
				this.ReportPropertyChanged("IsApproved");
			}
		}
		[EdmScalarProperty(EntityKeyProperty = false, IsNullable = false)]
		public bool IsLockedOut
		{
			get
			{
				return this._IsLockedOut;
			}
			set
			{
				this.ReportPropertyChanging("IsLockedOut");
				this._IsLockedOut = StructuralObject.SetValidValue(value);
				this.ReportPropertyChanged("IsLockedOut");
			}
		}
		[EdmScalarProperty(EntityKeyProperty = false, IsNullable = false)]
		public DateTime CreateDate
		{
			get
			{
				return this._CreateDate;
			}
			set
			{
				this.ReportPropertyChanging("CreateDate");
				this._CreateDate = StructuralObject.SetValidValue(value);
				this.ReportPropertyChanged("CreateDate");
			}
		}
		[EdmScalarProperty(EntityKeyProperty = false, IsNullable = false)]
		public DateTime LastLoginDate
		{
			get
			{
				return this._LastLoginDate;
			}
			set
			{
				this.ReportPropertyChanging("LastLoginDate");
				this._LastLoginDate = StructuralObject.SetValidValue(value);
				this.ReportPropertyChanged("LastLoginDate");
			}
		}
		[EdmScalarProperty(EntityKeyProperty = false, IsNullable = false)]
		public DateTime LastPasswordChangedDate
		{
			get
			{
				return this._LastPasswordChangedDate;
			}
			set
			{
				this.ReportPropertyChanging("LastPasswordChangedDate");
				this._LastPasswordChangedDate = StructuralObject.SetValidValue(value);
				this.ReportPropertyChanged("LastPasswordChangedDate");
			}
		}
		[EdmScalarProperty(EntityKeyProperty = false, IsNullable = false)]
		public DateTime LastLockoutDate
		{
			get
			{
				return this._LastLockoutDate;
			}
			set
			{
				this.ReportPropertyChanging("LastLockoutDate");
				this._LastLockoutDate = StructuralObject.SetValidValue(value);
				this.ReportPropertyChanged("LastLockoutDate");
			}
		}
		[EdmScalarProperty(EntityKeyProperty = false, IsNullable = false)]
		public int FailedPasswordAttemptCount
		{
			get
			{
				return this._FailedPasswordAttemptCount;
			}
			set
			{
				this.ReportPropertyChanging("FailedPasswordAttemptCount");
				this._FailedPasswordAttemptCount = StructuralObject.SetValidValue(value);
				this.ReportPropertyChanged("FailedPasswordAttemptCount");
			}
		}
		[EdmScalarProperty(EntityKeyProperty = false, IsNullable = false)]
		public DateTime FailedPasswordAttemptWindowStart
		{
			get
			{
				return this._FailedPasswordAttemptWindowStart;
			}
			set
			{
				this.ReportPropertyChanging("FailedPasswordAttemptWindowStart");
				this._FailedPasswordAttemptWindowStart = StructuralObject.SetValidValue(value);
				this.ReportPropertyChanged("FailedPasswordAttemptWindowStart");
			}
		}
		[EdmScalarProperty(EntityKeyProperty = false, IsNullable = false)]
		public int FailedPasswordAnswerAttemptCount
		{
			get
			{
				return this._FailedPasswordAnswerAttemptCount;
			}
			set
			{
				this.ReportPropertyChanging("FailedPasswordAnswerAttemptCount");
				this._FailedPasswordAnswerAttemptCount = StructuralObject.SetValidValue(value);
				this.ReportPropertyChanged("FailedPasswordAnswerAttemptCount");
			}
		}
		[EdmScalarProperty(EntityKeyProperty = false, IsNullable = false)]
		public DateTime FailedPasswordAnswerAttemptWindowsStart
		{
			get
			{
				return this._FailedPasswordAnswerAttemptWindowsStart;
			}
			set
			{
				this.ReportPropertyChanging("FailedPasswordAnswerAttemptWindowsStart");
				this._FailedPasswordAnswerAttemptWindowsStart = StructuralObject.SetValidValue(value);
				this.ReportPropertyChanged("FailedPasswordAnswerAttemptWindowsStart");
			}
		}
		[EdmScalarProperty(EntityKeyProperty = false, IsNullable = true)]
		public string Comment
		{
			get
			{
				return this._Comment;
			}
			set
			{
				this.ReportPropertyChanging("Comment");
				this._Comment = StructuralObject.SetValidValue(value, true);
				this.ReportPropertyChanged("Comment");
			}
		}
		[EdmRelationshipNavigationProperty("System.Web.Providers.Entities", "MembershipUser", "User")]
		public User User
		{
			get
			{
				return ((IEntityWithRelationships)this).RelationshipManager.GetRelatedReference<User>("System.Web.Providers.Entities.MembershipUser", "User").Value;
			}
			set
			{
				((IEntityWithRelationships)this).RelationshipManager.GetRelatedReference<User>("System.Web.Providers.Entities.MembershipUser", "User").Value = value;
			}
		}
		public EntityReference<User> UserReference
		{
			get
			{
				return ((IEntityWithRelationships)this).RelationshipManager.GetRelatedReference<User>("System.Web.Providers.Entities.MembershipUser", "User");
			}
			set
			{
				if (value != null)
				{
					((IEntityWithRelationships)this).RelationshipManager.InitializeRelatedReference<User>("System.Web.Providers.Entities.MembershipUser", "User", value);
				}
			}
		}
		[EdmRelationshipNavigationProperty("System.Web.Providers.Entities", "MembershipApplication", "Application")]
		public Application Application
		{
			get
			{
				return ((IEntityWithRelationships)this).RelationshipManager.GetRelatedReference<Application>("System.Web.Providers.Entities.MembershipApplication", "Application").Value;
			}
			set
			{
				((IEntityWithRelationships)this).RelationshipManager.GetRelatedReference<Application>("System.Web.Providers.Entities.MembershipApplication", "Application").Value = value;
			}
		}
		public EntityReference<Application> ApplicationReference
		{
			get
			{
				return ((IEntityWithRelationships)this).RelationshipManager.GetRelatedReference<Application>("System.Web.Providers.Entities.MembershipApplication", "Application");
			}
			set
			{
				if (value != null)
				{
					((IEntityWithRelationships)this).RelationshipManager.InitializeRelatedReference<Application>("System.Web.Providers.Entities.MembershipApplication", "Application", value);
				}
			}
		}
	}
}
