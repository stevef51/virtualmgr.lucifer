using System;
using System.Data.EntityClient;
using System.Data.Objects;
namespace System.Web.Providers.Entities
{
	internal sealed class MembershipEntities : ObjectContext
	{
		private ObjectSet<Application> _Applications;
		private ObjectSet<MembershipEntity> _Memberships;
		private ObjectSet<ProfileEntity> _Profiles;
		private ObjectSet<RoleEntity> _Roles;
		private ObjectSet<User> _Users;
		private ObjectSet<UsersInRole> _UsersInRoles;
		public ObjectSet<Application> Applications
		{
			get
			{
				if (this._Applications == null)
				{
					this._Applications = base.CreateObjectSet<Application>("Applications");
				}
				return this._Applications;
			}
		}
		public ObjectSet<MembershipEntity> Memberships
		{
			get
			{
				if (this._Memberships == null)
				{
					this._Memberships = base.CreateObjectSet<MembershipEntity>("Memberships");
				}
				return this._Memberships;
			}
		}
		public ObjectSet<ProfileEntity> Profiles
		{
			get
			{
				if (this._Profiles == null)
				{
					this._Profiles = base.CreateObjectSet<ProfileEntity>("Profiles");
				}
				return this._Profiles;
			}
		}
		public ObjectSet<RoleEntity> Roles
		{
			get
			{
				if (this._Roles == null)
				{
					this._Roles = base.CreateObjectSet<RoleEntity>("Roles");
				}
				return this._Roles;
			}
		}
		public ObjectSet<User> Users
		{
			get
			{
				if (this._Users == null)
				{
					this._Users = base.CreateObjectSet<User>("Users");
				}
				return this._Users;
			}
		}
		public ObjectSet<UsersInRole> UsersInRoles
		{
			get
			{
				if (this._UsersInRoles == null)
				{
					this._UsersInRoles = base.CreateObjectSet<UsersInRole>("UsersInRoles");
				}
				return this._UsersInRoles;
			}
		}
		public MembershipEntities(EntityConnection connection) : base(connection, "MembershipEntities")
		{
			base.ContextOptions.LazyLoadingEnabled = true;
		}
	}
}
