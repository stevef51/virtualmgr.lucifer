using System;
using System.ComponentModel;
using System.Data.Objects.DataClasses;
namespace System.Web.Providers.Entities
{
	[EdmEntityType(NamespaceName = "System.Web.Providers.Entities", Name = "User")]
	public class User : EntityObject
	{
		private Guid _ApplicationId;
		private Guid _UserId;
		private string _UserName;
		private bool _IsAnonymous;
		private DateTime _LastActivityDate;
		[EdmScalarProperty(EntityKeyProperty = false, IsNullable = false)]
		public Guid ApplicationId
		{
			get
			{
				return this._ApplicationId;
			}
			set
			{
				this.ReportPropertyChanging("ApplicationId");
				this._ApplicationId = StructuralObject.SetValidValue(value);
				this.ReportPropertyChanged("ApplicationId");
			}
		}
		[EdmScalarProperty(EntityKeyProperty = true, IsNullable = false)]
		public Guid UserId
		{
			get
			{
				return this._UserId;
			}
			set
			{
				if (this._UserId != value)
				{
					this.ReportPropertyChanging("UserId");
					this._UserId = StructuralObject.SetValidValue(value);
					this.ReportPropertyChanged("UserId");
				}
			}
		}
		[EdmScalarProperty(EntityKeyProperty = false, IsNullable = false)]
		public string UserName
		{
			get
			{
				return this._UserName;
			}
			set
			{
				this.ReportPropertyChanging("UserName");
				this._UserName = StructuralObject.SetValidValue(value, false);
				this.ReportPropertyChanged("UserName");
			}
		}
		[EdmScalarProperty(EntityKeyProperty = false, IsNullable = false)]
		public bool IsAnonymous
		{
			get
			{
				return this._IsAnonymous;
			}
			set
			{
				this.ReportPropertyChanging("IsAnonymous");
				this._IsAnonymous = StructuralObject.SetValidValue(value);
				this.ReportPropertyChanged("IsAnonymous");
			}
		}
		[EdmScalarProperty(EntityKeyProperty = false, IsNullable = false)]
		public DateTime LastActivityDate
		{
			get
			{
				return this._LastActivityDate;
			}
			set
			{
				this.ReportPropertyChanging("LastActivityDate");
				this._LastActivityDate = StructuralObject.SetValidValue(value);
				this.ReportPropertyChanged("LastActivityDate");
			}
		}
		[EdmRelationshipNavigationProperty("System.Web.Providers.Entities", "UserApplication", "Application")]
		public Application Applications
		{
			get
			{
				return ((IEntityWithRelationships)this).RelationshipManager.GetRelatedReference<Application>("System.Web.Providers.Entities.UserApplication", "Application").Value;
			}
			set
			{
				((IEntityWithRelationships)this).RelationshipManager.GetRelatedReference<Application>("System.Web.Providers.Entities.UserApplication", "Application").Value = value;
			}
		}
		[Browsable(false)]
		public EntityReference<Application> ApplicationsReference
		{
			get
			{
				return ((IEntityWithRelationships)this).RelationshipManager.GetRelatedReference<Application>("System.Web.Providers.Entities.UserApplication", "Application");
			}
			set
			{
				if (value != null)
				{
					((IEntityWithRelationships)this).RelationshipManager.InitializeRelatedReference<Application>("System.Web.Providers.Entities.UserApplication", "Application", value);
				}
			}
		}
		[EdmRelationshipNavigationProperty("System.Web.Providers.Entities", "UsersInRoleUser", "UsersInRole")]
		public EntityCollection<UsersInRole> UsersInRoles
		{
			get
			{
				return ((IEntityWithRelationships)this).RelationshipManager.GetRelatedCollection<UsersInRole>("System.Web.Providers.Entities.UsersInRoleUser", "UsersInRole");
			}
			set
			{
				if (value != null)
				{
					((IEntityWithRelationships)this).RelationshipManager.InitializeRelatedCollection<UsersInRole>("System.Web.Providers.Entities.UsersInRoleUser", "UsersInRole", value);
				}
			}
		}
		[EdmRelationshipNavigationProperty("System.Web.Providers.Entities", "MembershipUser", "Membership")]
		public MembershipEntity Membership
		{
			get
			{
				return ((IEntityWithRelationships)this).RelationshipManager.GetRelatedReference<MembershipEntity>("System.Web.Providers.Entities.MembershipUser", "Membership").Value;
			}
			set
			{
				((IEntityWithRelationships)this).RelationshipManager.GetRelatedReference<MembershipEntity>("System.Web.Providers.Entities.MembershipUser", "Membership").Value = value;
			}
		}
		[Browsable(false)]
		public EntityReference<MembershipEntity> MembershipReference
		{
			get
			{
				return ((IEntityWithRelationships)this).RelationshipManager.GetRelatedReference<MembershipEntity>("System.Web.Providers.Entities.MembershipUser", "Membership");
			}
			set
			{
				if (value != null)
				{
					((IEntityWithRelationships)this).RelationshipManager.InitializeRelatedReference<MembershipEntity>("System.Web.Providers.Entities.MembershipUser", "Membership", value);
				}
			}
		}
		[EdmRelationshipNavigationProperty("System.Web.Providers.Entities", "UserProfile", "Profile")]
		public ProfileEntity Profile
		{
			get
			{
				return ((IEntityWithRelationships)this).RelationshipManager.GetRelatedReference<ProfileEntity>("System.Web.Providers.Entities.UserProfile", "Profile").Value;
			}
			set
			{
				((IEntityWithRelationships)this).RelationshipManager.GetRelatedReference<ProfileEntity>("System.Web.Providers.Entities.UserProfile", "Profile").Value = value;
			}
		}
		[Browsable(false)]
		public EntityReference<ProfileEntity> ProfileReference
		{
			get
			{
				return ((IEntityWithRelationships)this).RelationshipManager.GetRelatedReference<ProfileEntity>("System.Web.Providers.Entities.UserProfile", "Profile");
			}
			set
			{
				if (value != null)
				{
					((IEntityWithRelationships)this).RelationshipManager.InitializeRelatedReference<ProfileEntity>("System.Web.Providers.Entities.UserProfile", "Profile", value);
				}
			}
		}
	}
}
