using System;
using System.ComponentModel;
using System.Data.Objects.DataClasses;
namespace System.Web.Providers.Entities
{
	[EdmEntityType(NamespaceName = "System.Web.Providers.Entities", Name = "ProfileEntitiy")]
	public class ProfileEntity : EntityObject
	{
		private Guid _UserId;
		private string _PropertyNames;
		private string _PropertyValueStrings;
		private byte[] _PropertyValueBinary;
		private DateTime _LastUpdatedDate;
		[EdmScalarProperty(EntityKeyProperty = true, IsNullable = false)]
		public Guid UserId
		{
			get
			{
				return this._UserId;
			}
			set
			{
				if (this._UserId != value)
				{
					this.ReportPropertyChanging("UserId");
					this._UserId = StructuralObject.SetValidValue(value);
					this.ReportPropertyChanged("UserId");
				}
			}
		}
		[EdmScalarProperty(EntityKeyProperty = false, IsNullable = false)]
		public string PropertyNames
		{
			get
			{
				return this._PropertyNames;
			}
			set
			{
				this.ReportPropertyChanging("PropertyNames");
				this._PropertyNames = StructuralObject.SetValidValue(value, false);
				this.ReportPropertyChanged("PropertyNames");
			}
		}
		[EdmScalarProperty(EntityKeyProperty = false, IsNullable = false)]
		public string PropertyValueStrings
		{
			get
			{
				return this._PropertyValueStrings;
			}
			set
			{
				this.ReportPropertyChanging("PropertyValueStrings");
				this._PropertyValueStrings = StructuralObject.SetValidValue(value, false);
				this.ReportPropertyChanged("PropertyValueStrings");
			}
		}
		[EdmScalarProperty(EntityKeyProperty = false, IsNullable = false)]
		public byte[] PropertyValueBinary
		{
			get
			{
				return StructuralObject.GetValidValue(this._PropertyValueBinary);
			}
			set
			{
				this.ReportPropertyChanging("PropertyValueBinary");
				this._PropertyValueBinary = StructuralObject.SetValidValue(value, false);
				this.ReportPropertyChanged("PropertyValueBinary");
			}
		}
		[EdmScalarProperty(EntityKeyProperty = false, IsNullable = false)]
		public DateTime LastUpdatedDate
		{
			get
			{
				return this._LastUpdatedDate;
			}
			set
			{
				this.ReportPropertyChanging("LastUpdatedDate");
				this._LastUpdatedDate = StructuralObject.SetValidValue(value);
				this.ReportPropertyChanged("LastUpdatedDate");
			}
		}
		[EdmRelationshipNavigationProperty("System.Web.Providers.Entities", "UserProfile", "User")]
		public User User
		{
			get
			{
				return ((IEntityWithRelationships)this).RelationshipManager.GetRelatedReference<User>("System.Web.Providers.Entities.UserProfile", "User").Value;
			}
			set
			{
				((IEntityWithRelationships)this).RelationshipManager.GetRelatedReference<User>("System.Web.Providers.Entities.UserProfile", "User").Value = value;
			}
		}
		[Browsable(false)]
		public EntityReference<User> UserReference
		{
			get
			{
				return ((IEntityWithRelationships)this).RelationshipManager.GetRelatedReference<User>("System.Web.Providers.Entities.UserProfile", "User");
			}
			set
			{
				if (value != null)
				{
					((IEntityWithRelationships)this).RelationshipManager.InitializeRelatedReference<User>("System.Web.Providers.Entities.UserProfile", "User", value);
				}
			}
		}
	}
}
