using System;
using System.ComponentModel;
using System.Data.Objects.DataClasses;
namespace System.Web.Providers.Entities
{
	[EdmEntityType(NamespaceName = "System.Web.Providers.Entities", Name = "UsersInRole")]
	public class UsersInRole : EntityObject
	{
		private Guid _UserId;
		private Guid _RoleId;
		[EdmScalarProperty(EntityKeyProperty = true, IsNullable = false)]
		public Guid UserId
		{
			get
			{
				return this._UserId;
			}
			set
			{
				if (this._UserId != value)
				{
					this.ReportPropertyChanging("UserId");
					this._UserId = StructuralObject.SetValidValue(value);
					this.ReportPropertyChanged("UserId");
				}
			}
		}
		[EdmScalarProperty(EntityKeyProperty = true, IsNullable = false)]
		public Guid RoleId
		{
			get
			{
				return this._RoleId;
			}
			set
			{
				if (this._RoleId != value)
				{
					this.ReportPropertyChanging("RoleId");
					this._RoleId = StructuralObject.SetValidValue(value);
					this.ReportPropertyChanged("RoleId");
				}
			}
		}
		[EdmRelationshipNavigationProperty("System.Web.Providers.Entities", "UsersInRoleUser", "User")]
		public User Users
		{
			get
			{
				return ((IEntityWithRelationships)this).RelationshipManager.GetRelatedReference<User>("System.Web.Providers.Entities.UsersInRoleUser", "User").Value;
			}
			set
			{
				((IEntityWithRelationships)this).RelationshipManager.GetRelatedReference<User>("System.Web.Providers.Entities.UsersInRoleUser", "User").Value = value;
			}
		}
		[Browsable(false)]
		public EntityReference<User> UsersReference
		{
			get
			{
				return ((IEntityWithRelationships)this).RelationshipManager.GetRelatedReference<User>("System.Web.Providers.Entities.UsersInRoleUser", "User");
			}
			set
			{
				if (value != null)
				{
					((IEntityWithRelationships)this).RelationshipManager.InitializeRelatedReference<User>("System.Web.Providers.Entities.UsersInRoleUser", "User", value);
				}
			}
		}
		[EdmRelationshipNavigationProperty("System.Web.Providers.Entities", "UsersInRoleRole", "Role")]
		public RoleEntity Roles
		{
			get
			{
				return ((IEntityWithRelationships)this).RelationshipManager.GetRelatedReference<RoleEntity>("System.Web.Providers.Entities.UsersInRoleRole", "Role").Value;
			}
			set
			{
				((IEntityWithRelationships)this).RelationshipManager.GetRelatedReference<RoleEntity>("System.Web.Providers.Entities.UsersInRoleRole", "Role").Value = value;
			}
		}
		[Browsable(false)]
		public EntityReference<RoleEntity> RolesReference
		{
			get
			{
				return ((IEntityWithRelationships)this).RelationshipManager.GetRelatedReference<RoleEntity>("System.Web.Providers.Entities.UsersInRoleRole", "Role");
			}
			set
			{
				if (value != null)
				{
					((IEntityWithRelationships)this).RelationshipManager.InitializeRelatedReference<RoleEntity>("System.Web.Providers.Entities.UsersInRoleRole", "Role", value);
				}
			}
		}
	}
}
