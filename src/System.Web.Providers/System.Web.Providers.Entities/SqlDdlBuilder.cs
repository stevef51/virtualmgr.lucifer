using System;
using System.IO;
using System.Text;
namespace System.Web.Providers.Entities
{
	internal sealed class SqlDdlBuilder
	{
		private readonly StringBuilder unencodedStringBuilder = new StringBuilder();
		internal static string CreateDatabaseScript(string databaseName, string dataFileName, string logFileName)
		{
			SqlDdlBuilder sqlDdlBuilder = new SqlDdlBuilder();
			sqlDdlBuilder.AppendSql("create database ");
			sqlDdlBuilder.AppendIdentifier(databaseName);
			sqlDdlBuilder.AppendSql(" on primary ");
			sqlDdlBuilder.AppendFileName(dataFileName);
			sqlDdlBuilder.AppendSql(" log on ");
			sqlDdlBuilder.AppendFileName(logFileName);
			return sqlDdlBuilder.unencodedStringBuilder.ToString();
		}
		private void AppendStringLiteral(string literalValue)
		{
			this.AppendSql("N'" + literalValue.Replace("'", "''") + "'");
		}
		private void AppendIdentifier(string identifier)
		{
			this.AppendSql("[" + identifier.Replace("]", "]]") + "]");
		}
		private void AppendFileName(string path)
		{
			this.AppendSql("(name=");
			this.AppendStringLiteral(Path.GetFileName(path));
			this.AppendSql(", filename=");
			this.AppendStringLiteral(path);
			this.AppendSql(")");
		}
		private void AppendSql(string text)
		{
			this.unencodedStringBuilder.Append(text);
		}
	}
}
