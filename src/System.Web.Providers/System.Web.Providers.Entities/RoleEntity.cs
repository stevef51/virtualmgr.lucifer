using System;
using System.Data.Objects.DataClasses;
namespace System.Web.Providers.Entities
{
	[EdmEntityType(NamespaceName = "System.Web.Providers.Entities", Name = "RoleEntity")]
	public class RoleEntity : EntityObject
	{
		private Guid _ApplicationId;
		private Guid _RoleId;
		private string _RoleName;
		private string _Description;
		[EdmScalarProperty(EntityKeyProperty = false, IsNullable = false)]
		public Guid ApplicationId
		{
			get
			{
				return this._ApplicationId;
			}
			set
			{
				this.ReportPropertyChanging("ApplicationId");
				this._ApplicationId = StructuralObject.SetValidValue(value);
				this.ReportPropertyChanged("ApplicationId");
			}
		}
		[EdmScalarProperty(EntityKeyProperty = true, IsNullable = false)]
		public Guid RoleId
		{
			get
			{
				return this._RoleId;
			}
			set
			{
				if (this._RoleId != value)
				{
					this.ReportPropertyChanging("RoleId");
					this._RoleId = StructuralObject.SetValidValue(value);
					this.ReportPropertyChanged("RoleId");
				}
			}
		}
		[EdmScalarProperty(EntityKeyProperty = false, IsNullable = false)]
		public string RoleName
		{
			get
			{
				return this._RoleName;
			}
			set
			{
				this.ReportPropertyChanging("RoleName");
				this._RoleName = StructuralObject.SetValidValue(value, false);
				this.ReportPropertyChanged("RoleName");
			}
		}
		[EdmScalarProperty(EntityKeyProperty = false, IsNullable = true)]
		public string Description
		{
			get
			{
				return this._Description;
			}
			set
			{
				this.ReportPropertyChanging("Description");
				this._Description = StructuralObject.SetValidValue(value, true);
				this.ReportPropertyChanged("Description");
			}
		}
		[EdmRelationshipNavigationProperty("System.Web.Providers.Entities", "UsersInRoleRole", "UsersInRole")]
		public EntityCollection<UsersInRole> UsersInRoles
		{
			get
			{
				return ((IEntityWithRelationships)this).RelationshipManager.GetRelatedCollection<UsersInRole>("System.Web.Providers.Entities.UsersInRoleRole", "UsersInRole");
			}
			set
			{
				if (value != null)
				{
					((IEntityWithRelationships)this).RelationshipManager.InitializeRelatedCollection<UsersInRole>("System.Web.Providers.Entities.UsersInRoleRole", "UsersInRole", value);
				}
			}
		}
		[EdmRelationshipNavigationProperty("System.Web.Providers.Entities", "RoleApplication", "Application")]
		public Application Application
		{
			get
			{
				return ((IEntityWithRelationships)this).RelationshipManager.GetRelatedReference<Application>("System.Web.Providers.Entities.RoleApplication", "Application").Value;
			}
			set
			{
				((IEntityWithRelationships)this).RelationshipManager.GetRelatedReference<Application>("System.Web.Providers.Entities.RoleApplication", "Application").Value = value;
			}
		}
		public EntityReference<Application> ApplicationReference
		{
			get
			{
				return ((IEntityWithRelationships)this).RelationshipManager.GetRelatedReference<Application>("System.Web.Providers.Entities.RoleApplication", "Application");
			}
			set
			{
				if (value != null)
				{
					((IEntityWithRelationships)this).RelationshipManager.InitializeRelatedReference<Application>("System.Web.Providers.Entities.RoleApplication", "Application", value);
				}
			}
		}
	}
}
