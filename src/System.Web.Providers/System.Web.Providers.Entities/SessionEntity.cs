using System;
using System.Data.Objects.DataClasses;
namespace System.Web.Providers.Entities
{
	[EdmEntityType(NamespaceName = "System.Web.Providers.Entities", Name = "SessionEntity")]
	public class SessionEntity : EntityObject
	{
		private string _SessionId;
		private DateTime _Created;
		private DateTime _Expires;
		private DateTime _LockDate;
		private int _LockCookie = 1;
		private bool _Locked;
		private byte[] _SessionItem;
		private int _Flags;
		private int _Timeout;
		[EdmScalarProperty(EntityKeyProperty = true, IsNullable = false)]
		public string SessionId
		{
			get
			{
				return this._SessionId;
			}
			set
			{
				if (this._SessionId != value)
				{
					this.ReportPropertyChanging("SessionId");
					this._SessionId = StructuralObject.SetValidValue(value, false);
					this.ReportPropertyChanged("SessionId");
				}
			}
		}
		[EdmScalarProperty(EntityKeyProperty = false, IsNullable = false)]
		public DateTime Created
		{
			get
			{
				return this._Created;
			}
			set
			{
				this.ReportPropertyChanging("Created");
				this._Created = StructuralObject.SetValidValue(value);
				this.ReportPropertyChanged("Created");
			}
		}
		[EdmScalarProperty(EntityKeyProperty = false, IsNullable = false)]
		public DateTime Expires
		{
			get
			{
				return this._Expires;
			}
			set
			{
				this.ReportPropertyChanging("Expires");
				this._Expires = StructuralObject.SetValidValue(value);
				this.ReportPropertyChanged("Expires");
			}
		}
		[EdmScalarProperty(EntityKeyProperty = false, IsNullable = false)]
		public DateTime LockDate
		{
			get
			{
				return this._LockDate;
			}
			set
			{
				this.ReportPropertyChanging("LockDate");
				this._LockDate = StructuralObject.SetValidValue(value);
				this.ReportPropertyChanged("LockDate");
			}
		}
		[EdmScalarProperty(EntityKeyProperty = false, IsNullable = false)]
		public int LockCookie
		{
			get
			{
				return this._LockCookie;
			}
			set
			{
				this.ReportPropertyChanging("LockCookie");
				this._LockCookie = StructuralObject.SetValidValue(value);
				this.ReportPropertyChanged("LockCookie");
			}
		}
		[EdmScalarProperty(EntityKeyProperty = false, IsNullable = false)]
		public bool Locked
		{
			get
			{
				return this._Locked;
			}
			set
			{
				this.ReportPropertyChanging("Locked");
				this._Locked = StructuralObject.SetValidValue(value);
				this.ReportPropertyChanged("Locked");
			}
		}
		[EdmScalarProperty(EntityKeyProperty = false, IsNullable = true)]
		public byte[] SessionItem
		{
			get
			{
				return StructuralObject.GetValidValue(this._SessionItem);
			}
			set
			{
				this.ReportPropertyChanging("SessionItem");
				this._SessionItem = StructuralObject.SetValidValue(value, true);
				this.ReportPropertyChanged("SessionItem");
			}
		}
		[EdmScalarProperty(EntityKeyProperty = false, IsNullable = false)]
		public int Flags
		{
			get
			{
				return this._Flags;
			}
			set
			{
				this.ReportPropertyChanging("Flags");
				this._Flags = StructuralObject.SetValidValue(value);
				this.ReportPropertyChanged("Flags");
			}
		}
		[EdmScalarProperty(EntityKeyProperty = false, IsNullable = false)]
		public int Timeout
		{
			get
			{
				return this._Timeout;
			}
			set
			{
				this.ReportPropertyChanging("Timeout");
				this._Timeout = StructuralObject.SetValidValue(value);
				this.ReportPropertyChanged("Timeout");
			}
		}
	}
}
