using System;
using System.Data.Objects.DataClasses;
namespace System.Web.Providers.Entities
{
	[EdmEntityType(NamespaceName = "System.Web.Providers.Entities", Name = "Application")]
	public class Application : EntityObject
	{
		private string _ApplicationName;
		private Guid _ApplicationId;
		private string _Description;
		[EdmScalarProperty(EntityKeyProperty = false, IsNullable = false)]
		public string ApplicationName
		{
			get
			{
				return this._ApplicationName;
			}
			set
			{
				this.ReportPropertyChanging("ApplicationName");
				this._ApplicationName = StructuralObject.SetValidValue(value, false);
				this.ReportPropertyChanged("ApplicationName");
			}
		}
		[EdmScalarProperty(EntityKeyProperty = true, IsNullable = false)]
		public Guid ApplicationId
		{
			get
			{
				return this._ApplicationId;
			}
			set
			{
				if (this._ApplicationId != value)
				{
					this.ReportPropertyChanging("ApplicationId");
					this._ApplicationId = StructuralObject.SetValidValue(value);
					this.ReportPropertyChanged("ApplicationId");
				}
			}
		}
		[EdmScalarProperty(EntityKeyProperty = false, IsNullable = true)]
		public string Description
		{
			get
			{
				return this._Description;
			}
			set
			{
				this.ReportPropertyChanging("Description");
				this._Description = StructuralObject.SetValidValue(value, true);
				this.ReportPropertyChanged("Description");
			}
		}
		[EdmRelationshipNavigationProperty("System.Web.Providers.Entities", "UserApplication", "User")]
		public EntityCollection<User> Users
		{
			get
			{
				return ((IEntityWithRelationships)this).RelationshipManager.GetRelatedCollection<User>("System.Web.Providers.Entities.UserApplication", "User");
			}
			set
			{
				if (value != null)
				{
					((IEntityWithRelationships)this).RelationshipManager.InitializeRelatedCollection<User>("System.Web.Providers.Entities.UserApplication", "User", value);
				}
			}
		}
		[EdmRelationshipNavigationProperty("System.Web.Providers.Entities", "MembershipApplication", "Membership")]
		public EntityCollection<MembershipEntity> Memberships
		{
			get
			{
				return ((IEntityWithRelationships)this).RelationshipManager.GetRelatedCollection<MembershipEntity>("System.Web.Providers.Entities.MembershipApplication", "Membership");
			}
			set
			{
				if (value != null)
				{
					((IEntityWithRelationships)this).RelationshipManager.InitializeRelatedCollection<MembershipEntity>("System.Web.Providers.Entities.MembershipApplication", "Membership", value);
				}
			}
		}
		[EdmRelationshipNavigationProperty("System.Web.Providers.Entities", "RoleApplication", "Role")]
		public EntityCollection<RoleEntity> Roles
		{
			get
			{
				return ((IEntityWithRelationships)this).RelationshipManager.GetRelatedCollection<RoleEntity>("System.Web.Providers.Entities.RoleApplication", "Role");
			}
			set
			{
				if (value != null)
				{
					((IEntityWithRelationships)this).RelationshipManager.InitializeRelatedCollection<RoleEntity>("System.Web.Providers.Entities.RoleApplication", "Role", value);
				}
			}
		}
	}
}
