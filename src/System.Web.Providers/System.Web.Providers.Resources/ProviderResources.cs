using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;
namespace System.Web.Providers.Resources
{
	[GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0"), DebuggerNonUserCode, CompilerGenerated]
	internal class ProviderResources
	{
		private static ResourceManager resourceMan;
		private static CultureInfo resourceCulture;
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		internal static ResourceManager ResourceManager
		{
			get
			{
				if (object.ReferenceEquals(ProviderResources.resourceMan, null))
				{
					ResourceManager resourceManager = new ResourceManager("System.Web.Providers.Resources.ProviderResources", typeof(ProviderResources).Assembly);
					ProviderResources.resourceMan = resourceManager;
				}
				return ProviderResources.resourceMan;
			}
		}
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		internal static CultureInfo Culture
		{
			get
			{
				return ProviderResources.resourceCulture;
			}
			set
			{
				ProviderResources.resourceCulture = value;
			}
		}
		internal static string Connection_name_not_specified
		{
			get
			{
				return ProviderResources.ResourceManager.GetString("Connection_name_not_specified", ProviderResources.resourceCulture);
			}
		}
		internal static string Connection_string_not_found
		{
			get
			{
				return ProviderResources.ResourceManager.GetString("Connection_string_not_found", ProviderResources.resourceCulture);
			}
		}
		internal static string Invalid_hash_algorithm
		{
			get
			{
				return ProviderResources.ResourceManager.GetString("Invalid_hash_algorithm", ProviderResources.resourceCulture);
			}
		}
		internal static string Invalid_key
		{
			get
			{
				return ProviderResources.ResourceManager.GetString("Invalid_key", ProviderResources.resourceCulture);
			}
		}
		internal static string Invalid_session_state
		{
			get
			{
				return ProviderResources.ResourceManager.GetString("Invalid_session_state", ProviderResources.resourceCulture);
			}
		}
		internal static string Membership_AccountLockOut
		{
			get
			{
				return ProviderResources.ResourceManager.GetString("Membership_AccountLockOut", ProviderResources.resourceCulture);
			}
		}
		internal static string Membership_Custom_Password_Validation_Failure
		{
			get
			{
				return ProviderResources.ResourceManager.GetString("Membership_Custom_Password_Validation_Failure", ProviderResources.resourceCulture);
			}
		}
		internal static string Membership_InvalidAnswer
		{
			get
			{
				return ProviderResources.ResourceManager.GetString("Membership_InvalidAnswer", ProviderResources.resourceCulture);
			}
		}
		internal static string Membership_InvalidEmail
		{
			get
			{
				return ProviderResources.ResourceManager.GetString("Membership_InvalidEmail", ProviderResources.resourceCulture);
			}
		}
		internal static string Membership_InvalidPassword
		{
			get
			{
				return ProviderResources.ResourceManager.GetString("Membership_InvalidPassword", ProviderResources.resourceCulture);
			}
		}
		internal static string Membership_InvalidProviderUserKey
		{
			get
			{
				return ProviderResources.ResourceManager.GetString("Membership_InvalidProviderUserKey", ProviderResources.resourceCulture);
			}
		}
		internal static string Membership_InvalidQuestion
		{
			get
			{
				return ProviderResources.ResourceManager.GetString("Membership_InvalidQuestion", ProviderResources.resourceCulture);
			}
		}
		internal static string Membership_PasswordRetrieval_not_supported
		{
			get
			{
				return ProviderResources.ResourceManager.GetString("Membership_PasswordRetrieval_not_supported", ProviderResources.resourceCulture);
			}
		}
		internal static string Membership_UserNotFound
		{
			get
			{
				return ProviderResources.ResourceManager.GetString("Membership_UserNotFound", ProviderResources.resourceCulture);
			}
		}
		internal static string Membership_WrongAnswer
		{
			get
			{
				return ProviderResources.ResourceManager.GetString("Membership_WrongAnswer", ProviderResources.resourceCulture);
			}
		}
		internal static string Membership_WrongPassword
		{
			get
			{
				return ProviderResources.ResourceManager.GetString("Membership_WrongPassword", ProviderResources.resourceCulture);
			}
		}
		internal static string MembershipProvider_description
		{
			get
			{
				return ProviderResources.ResourceManager.GetString("MembershipProvider_description", ProviderResources.resourceCulture);
			}
		}
		internal static string Not_configured_to_support_password_resets
		{
			get
			{
				return ProviderResources.ResourceManager.GetString("Not_configured_to_support_password_resets", ProviderResources.resourceCulture);
			}
		}
		internal static string Parameter_array_empty
		{
			get
			{
				return ProviderResources.ResourceManager.GetString("Parameter_array_empty", ProviderResources.resourceCulture);
			}
		}
		internal static string Parameter_can_not_be_empty
		{
			get
			{
				return ProviderResources.ResourceManager.GetString("Parameter_can_not_be_empty", ProviderResources.resourceCulture);
			}
		}
		internal static string Parameter_can_not_contain_comma
		{
			get
			{
				return ProviderResources.ResourceManager.GetString("Parameter_can_not_contain_comma", ProviderResources.resourceCulture);
			}
		}
		internal static string Parameter_collection_empty
		{
			get
			{
				return ProviderResources.ResourceManager.GetString("Parameter_collection_empty", ProviderResources.resourceCulture);
			}
		}
		internal static string Parameter_duplicate_array_element
		{
			get
			{
				return ProviderResources.ResourceManager.GetString("Parameter_duplicate_array_element", ProviderResources.resourceCulture);
			}
		}
		internal static string Parameter_too_long
		{
			get
			{
				return ProviderResources.ResourceManager.GetString("Parameter_too_long", ProviderResources.resourceCulture);
			}
		}
		internal static string Password_does_not_match_regular_expression
		{
			get
			{
				return ProviderResources.ResourceManager.GetString("Password_does_not_match_regular_expression", ProviderResources.resourceCulture);
			}
		}
		internal static string Password_need_more_non_alpha_numeric_chars
		{
			get
			{
				return ProviderResources.ResourceManager.GetString("Password_need_more_non_alpha_numeric_chars", ProviderResources.resourceCulture);
			}
		}
		internal static string Password_too_short
		{
			get
			{
				return ProviderResources.ResourceManager.GetString("Password_too_short", ProviderResources.resourceCulture);
			}
		}
		internal static string ProfileProvider_description
		{
			get
			{
				return ProviderResources.ResourceManager.GetString("ProfileProvider_description", ProviderResources.resourceCulture);
			}
		}
		internal static string Provider_can_not_decode_hashed_password
		{
			get
			{
				return ProviderResources.ResourceManager.GetString("Provider_can_not_decode_hashed_password", ProviderResources.resourceCulture);
			}
		}
		internal static string Provider_can_not_retrieve_hashed_password
		{
			get
			{
				return ProviderResources.ResourceManager.GetString("Provider_can_not_retrieve_hashed_password", ProviderResources.resourceCulture);
			}
		}
		internal static string Provider_Error
		{
			get
			{
				return ProviderResources.ResourceManager.GetString("Provider_Error", ProviderResources.resourceCulture);
			}
		}
		internal static string Provider_role_already_exists
		{
			get
			{
				return ProviderResources.ResourceManager.GetString("Provider_role_already_exists", ProviderResources.resourceCulture);
			}
		}
		internal static string Provider_role_not_found
		{
			get
			{
				return ProviderResources.ResourceManager.GetString("Provider_role_not_found", ProviderResources.resourceCulture);
			}
		}
		internal static string Provider_this_user_already_in_role
		{
			get
			{
				return ProviderResources.ResourceManager.GetString("Provider_this_user_already_in_role", ProviderResources.resourceCulture);
			}
		}
		internal static string Provider_this_user_already_not_in_role
		{
			get
			{
				return ProviderResources.ResourceManager.GetString("Provider_this_user_already_not_in_role", ProviderResources.resourceCulture);
			}
		}
		internal static string Provider_this_user_not_found
		{
			get
			{
				return ProviderResources.ResourceManager.GetString("Provider_this_user_not_found", ProviderResources.resourceCulture);
			}
		}
		internal static string Provider_unrecognized_attribute
		{
			get
			{
				return ProviderResources.ResourceManager.GetString("Provider_unrecognized_attribute", ProviderResources.resourceCulture);
			}
		}
		internal static string Role_is_not_empty
		{
			get
			{
				return ProviderResources.ResourceManager.GetString("Role_is_not_empty", ProviderResources.resourceCulture);
			}
		}
		internal static string RoleProvider_description
		{
			get
			{
				return ProviderResources.ResourceManager.GetString("RoleProvider_description", ProviderResources.resourceCulture);
			}
		}
		internal static string Session_id_too_long
		{
			get
			{
				return ProviderResources.ResourceManager.GetString("Session_id_too_long", ProviderResources.resourceCulture);
			}
		}
		internal static string Session_not_found
		{
			get
			{
				return ProviderResources.ResourceManager.GetString("Session_not_found", ProviderResources.resourceCulture);
			}
		}
		internal static string Unable_to_convert_type_to_string
		{
			get
			{
				return ProviderResources.ResourceManager.GetString("Unable_to_convert_type_to_string", ProviderResources.resourceCulture);
			}
		}
		internal ProviderResources()
		{
		}
	}
}
