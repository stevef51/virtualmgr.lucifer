using System;
using System.Data.Common;
using System.Data.Objects;
using System.Linq;
using System.Reflection;
using System.Web.Profile;
using System.Web.Providers.Entities;
using System.Web.Security;
namespace System.Web.Providers
{
	internal static class QueryHelper
	{
		internal class EFMembershipUser
		{
			public string UserName
			{
				get;
				set;
			}
			public Guid UserId
			{
				get;
				set;
			}
			public string Email
			{
				get;
				set;
			}
			public string PasswordQuestion
			{
				get;
				set;
			}
			public string Comment
			{
				get;
				set;
			}
			public bool IsApproved
			{
				get;
				set;
			}
			public bool IsLockedOut
			{
				get;
				set;
			}
			public DateTime CreateDate
			{
				get;
				set;
			}
			public DateTime LastLoginDate
			{
				get;
				set;
			}
			public DateTime LastActivityDate
			{
				get;
				set;
			}
			public DateTime LastPasswordChangedDate
			{
				get;
				set;
			}
			public DateTime LastLockoutDate
			{
				get;
				set;
			}
		}
		internal class EFProfileInfo
		{
			public string UserName
			{
				get;
				set;
			}
			public bool IsAnonymous
			{
				get;
				set;
			}
			public DateTime LastActivityDate
			{
				get;
				set;
			}
			public DateTime LastUpdatedDate
			{
				get;
				set;
			}
			public byte[] PropertyValueBinary
			{
				get;
				set;
			}
			public int Size
			{
				get;
				set;
			}
			public ProfileInfo ToProfileInfo()
			{
				return new ProfileInfo(this.UserName, this.IsAnonymous, this.LastActivityDate, this.LastUpdatedDate, this.Size + this.PropertyValueBinary.Length);
			}
		}
		private const string BaseGetAllMembershipsQuery = "select u.UserName, u.UserId, m.Email, m.PasswordQuestion, m.Comment, m.IsApproved, m.IsLockedOut, m.CreateDate, m.LastLoginDate, u.LastActivityDate, m.LastPasswordChangedDate, m.LastLockoutDate FROM Users as u, Memberships as m, Applications as a WHERE ToLower(a.ApplicationName) = @appName AND a.ApplicationId = m.ApplicationId AND m.UserId = u.UserId";
		internal static T Convert<T>(DbDataRecord record) where T : new()
		{
			T t = (default(T) == null) ? Activator.CreateInstance<T>() : default(T);
			for (int i = 0; i < record.FieldCount; i++)
			{
				PropertyInfo property = t.GetType().GetProperty(record.GetName(i));
				if (property != null && property.PropertyType == record.GetFieldType(i) && !record.IsDBNull(i))
				{
					property.SetValue(t, record.GetValue(i), null);
				}
			}
			return t;
		}
		internal static Application GetApplication(MembershipEntities ctx, string applicationName)
		{
			return ctx.CreateQuery<Application>("select value a FROM Applications as a WHERE ToLower(a.ApplicationName) = @name", new ObjectParameter[]
			{
				new ObjectParameter("name", applicationName.ToLowerInvariant())
			}).FirstOrDefault<Application>();
		}
		internal static User GetUser(MembershipEntities ctx, string userName, string applicationName)
		{
			ObjectQuery<User> source = ctx.CreateQuery<User>("select value u FROM Users as u, Applications as a WHERE ToLower(a.ApplicationName) = @appName AND a.ApplicationId = u.ApplicationId AND ToLower(u.UserName) = @userName", new ObjectParameter[]
			{
				new ObjectParameter("userName", userName.ToLowerInvariant()),
				new ObjectParameter("appName", applicationName.ToLowerInvariant())
			});
			return source.FirstOrDefault<User>();
		}
		internal static User GetUser(MembershipEntities ctx, Guid userId, string applicationName)
		{
			ObjectQuery<User> source = ctx.CreateQuery<User>("select value u FROM Users as u, Applications as a WHERE u.UserId = @userId AND ToLower(a.ApplicationName) = @appName AND a.ApplicationId = u.ApplicationId", new ObjectParameter[]
			{
				new ObjectParameter("userId", userId),
				new ObjectParameter("appName", applicationName.ToLowerInvariant())
			});
			return source.FirstOrDefault<User>();
		}
		internal static Guid GetUserIdFromUserName(MembershipEntities ctx, string userName, string applicationName)
		{
			ObjectQuery<Guid> source = ctx.CreateQuery<Guid>("select value u.UserId FROM Users as u, Applications as a WHERE ToLower(a.ApplicationName) = @appName AND a.ApplicationId = u.ApplicationId AND ToLower(u.UserName) = @userName", new ObjectParameter[]
			{
				new ObjectParameter("appName", applicationName.ToLowerInvariant()),
				new ObjectParameter("userName", userName.ToLowerInvariant())
			});
			return source.FirstOrDefault<Guid>();
		}
		internal static MembershipUser CreateMembershipUserFromDbRecord(MembershipEntities ctx, DbDataRecord record, string providerName, string applicationName, bool userIsOnline = false)
		{
			if (record != null)
			{
				QueryHelper.EFMembershipUser eFMembershipUser = QueryHelper.Convert<QueryHelper.EFMembershipUser>(record);
				if (userIsOnline)
				{
					User user = QueryHelper.GetUser(ctx, eFMembershipUser.UserId, applicationName);
					user.LastActivityDate = DateTime.UtcNow;
					ctx.SaveChanges();
				}
				return new MembershipUser(providerName, eFMembershipUser.UserName, eFMembershipUser.UserId, eFMembershipUser.Email, eFMembershipUser.PasswordQuestion, eFMembershipUser.Comment, eFMembershipUser.IsApproved, eFMembershipUser.IsLockedOut, eFMembershipUser.CreateDate.ToLocalTime(), eFMembershipUser.LastLoginDate.ToLocalTime(), eFMembershipUser.LastActivityDate.ToLocalTime(), eFMembershipUser.LastPasswordChangedDate.ToLocalTime(), eFMembershipUser.LastLockoutDate.ToLocalTime());
			}
			return null;
		}
		internal static MembershipUser GetMembershipUser(MembershipEntities ctx, string username, string applicationName, bool userIsOnline, string providerName)
		{
			ObjectQuery<DbDataRecord> source = ctx.CreateQuery<DbDataRecord>("select u.UserName, u.UserId, m.Email, m.PasswordQuestion, m.Comment, m.IsApproved, m.IsLockedOut, m.CreateDate, m.LastLoginDate, u.LastActivityDate, m.LastPasswordChangedDate, m.LastLockoutDate FROM Users as u, Memberships as m, Applications as a WHERE ToLower(a.ApplicationName) = @appName AND a.ApplicationId = m.ApplicationId AND m.UserId = u.UserId AND ToLower(u.UserName) = @userName", new ObjectParameter[]
			{
				new ObjectParameter("appName", applicationName.ToLowerInvariant()),
				new ObjectParameter("userName", username.ToLowerInvariant())
			});
			return QueryHelper.CreateMembershipUserFromDbRecord(ctx, source.FirstOrDefault<DbDataRecord>(), providerName, applicationName, userIsOnline);
		}
		internal static MembershipUser GetMembershipUser(MembershipEntities ctx, Guid userId, string applicationName, bool userIsOnline, string providerName)
		{
			ObjectQuery<DbDataRecord> source = ctx.CreateQuery<DbDataRecord>("select u.UserName, u.UserId, m.Email, m.PasswordQuestion, m.Comment, m.IsApproved, m.IsLockedOut, m.CreateDate, m.LastLoginDate, u.LastActivityDate, m.LastPasswordChangedDate, m.LastLockoutDate FROM Users as u, Memberships as m, Applications as a WHERE ToLower(a.ApplicationName) = @appName AND a.ApplicationId = m.ApplicationId AND m.UserId = u.UserId AND u.UserId = @userId", new ObjectParameter[]
			{
				new ObjectParameter("appName", applicationName.ToLowerInvariant()),
				new ObjectParameter("userId", userId)
			});
			return QueryHelper.CreateMembershipUserFromDbRecord(ctx, source.FirstOrDefault<DbDataRecord>(), providerName, applicationName, userIsOnline);
		}
		private static string AppendUserNameSkipLimitIfNeeded(string query, int pageIndex, int pageSize)
		{
			if (pageIndex != -1 && pageSize != -1)
			{
				int num = pageIndex * pageSize;
				object obj = query;
				query = string.Concat(new object[]
				{
					obj,
					" ORDER BY u.UserName SKIP(",
					num,
					") LIMIT(",
					pageSize,
					")"
				});
			}
			return query;
		}
		internal static IQueryable<DbDataRecord> GetAllMembershipUsers(MembershipEntities ctx, string applicationName, int pageIndex = -1, int pageSize = -1)
		{
			string queryString = QueryHelper.AppendUserNameSkipLimitIfNeeded("select u.UserName, u.UserId, m.Email, m.PasswordQuestion, m.Comment, m.IsApproved, m.IsLockedOut, m.CreateDate, m.LastLoginDate, u.LastActivityDate, m.LastPasswordChangedDate, m.LastLockoutDate FROM Users as u, Memberships as m, Applications as a WHERE ToLower(a.ApplicationName) = @appName AND a.ApplicationId = m.ApplicationId AND m.UserId = u.UserId", pageIndex, pageSize);
			return ctx.CreateQuery<DbDataRecord>(queryString, new ObjectParameter[]
			{
				new ObjectParameter("appName", applicationName.ToLowerInvariant())
			});
		}
		internal static IQueryable<DbDataRecord> GetAllMembershipUsersLikeUserName(MembershipEntities ctx, string applicationName, string userName, int pageIndex = -1, int pageSize = -1)
		{
			string text = "select u.UserName, u.UserId, m.Email, m.PasswordQuestion, m.Comment, m.IsApproved, m.IsLockedOut, m.CreateDate, m.LastLoginDate, u.LastActivityDate, m.LastPasswordChangedDate, m.LastLockoutDate FROM Users as u, Memberships as m, Applications as a WHERE ToLower(a.ApplicationName) = @appName AND a.ApplicationId = m.ApplicationId AND m.UserId = u.UserId AND ToLower(u.UserName) LIKE @userName";
			text = QueryHelper.AppendUserNameSkipLimitIfNeeded(text, pageIndex, pageSize);
			return ctx.CreateQuery<DbDataRecord>(text, new ObjectParameter[]
			{
				new ObjectParameter("appName", applicationName.ToLowerInvariant()),
				new ObjectParameter("userName", userName.ToLowerInvariant())
			});
		}
		internal static IQueryable<DbDataRecord> GetAllMembershipUsersLikeEmail(MembershipEntities ctx, string applicationName, string email, int pageIndex = -1, int pageSize = -1)
		{
			if (string.IsNullOrEmpty(email))
			{
				string text = "select u.UserName, u.UserId, m.Email, m.PasswordQuestion, m.Comment, m.IsApproved, m.IsLockedOut, m.CreateDate, m.LastLoginDate, u.LastActivityDate, m.LastPasswordChangedDate, m.LastLockoutDate FROM Users as u, Memberships as m, Applications as a WHERE ToLower(a.ApplicationName) = @appName AND a.ApplicationId = m.ApplicationId AND m.UserId = u.UserId AND m.email = null";
				text = QueryHelper.AppendUserNameSkipLimitIfNeeded(text, pageIndex, pageSize);
				return ctx.CreateQuery<DbDataRecord>(text, new ObjectParameter[]
				{
					new ObjectParameter("appName", applicationName.ToLowerInvariant())
				});
			}
			string text2 = "select u.UserName, u.UserId, m.Email, m.PasswordQuestion, m.Comment, m.IsApproved, m.IsLockedOut, m.CreateDate, m.LastLoginDate, u.LastActivityDate, m.LastPasswordChangedDate, m.LastLockoutDate FROM Users as u, Memberships as m, Applications as a WHERE ToLower(a.ApplicationName) = @appName AND a.ApplicationId = m.ApplicationId AND m.UserId = u.UserId AND ToLower(m.email) LIKE @email";
			text2 = QueryHelper.AppendUserNameSkipLimitIfNeeded(text2, pageIndex, pageSize);
			return ctx.CreateQuery<DbDataRecord>(text2, new ObjectParameter[]
			{
				new ObjectParameter("appName", applicationName.ToLowerInvariant()),
				new ObjectParameter("email", email.ToLowerInvariant())
			});
		}
		internal static string GetUserNameFromEmail(MembershipEntities ctx, string email, string applicationName)
		{
			ObjectQuery<string> source = ctx.CreateQuery<string>("select value u.UserName FROM Users as u, Memberships as m, Applications as a WHERE ToLower(a.ApplicationName) = @appName AND a.ApplicationId = m.ApplicationId AND a.ApplicationId = u.ApplicationId AND m.UserId = u.UserId AND m.Email = @email", new ObjectParameter[]
			{
				new ObjectParameter("appName", applicationName.ToLowerInvariant()),
				new ObjectParameter("email", email.ToLowerInvariant())
			});
			return source.FirstOrDefault<string>();
		}
		internal static MembershipEntity GetMembership(MembershipEntities ctx, string applicationName, Guid userId)
		{
			ObjectQuery<MembershipEntity> source = ctx.CreateQuery<MembershipEntity>("select value m FROM Memberships as m, Applications as a WHERE ToLower(a.ApplicationName) = @appName AND a.ApplicationId = m.ApplicationId AND m.UserId = @userId", new ObjectParameter[]
			{
				new ObjectParameter("userId", userId),
				new ObjectParameter("appName", applicationName.ToLowerInvariant())
			});
			return source.FirstOrDefault<MembershipEntity>();
		}
		internal static MembershipEntity GetMembership(MembershipEntities ctx, string applicationName, string userName)
		{
			return ctx.CreateQuery<MembershipEntity>("select value m FROM Users as u, Memberships as m, Applications as a WHERE ToLower(a.ApplicationName) = @an AND a.ApplicationId = m.ApplicationId AND m.UserId = u.UserId AND ToLower(u.UserName) = @userName", new ObjectParameter[]
			{
				new ObjectParameter("an", applicationName.ToLowerInvariant()),
				new ObjectParameter("userName", userName.ToLowerInvariant())
			}).FirstOrDefault<MembershipEntity>();
		}
		internal static bool DuplicateEmailExists(MembershipEntities ctx, string applicationName, Guid userId, string email)
		{
			ObjectQuery<string> source = ctx.CreateQuery<string>("select value m.Email FROM Memberships as m, Applications as a WHERE ToLower(a.ApplicationName) = @appName AND a.ApplicationId = m.ApplicationId AND a.ApplicationId = m.ApplicationId AND m.UserId != @userId AND ToLower(m.Email) = @email", new ObjectParameter[]
			{
				new ObjectParameter("userId", userId),
				new ObjectParameter("appName", applicationName.ToLowerInvariant()),
				new ObjectParameter("email", email.ToLowerInvariant())
			});
			return source.FirstOrDefault<string>() != null;
		}
		internal static int GetNumberOfOnlineUsers(MembershipEntities ctx, string applicationName, DateTime dateactive)
		{
			ObjectQuery<Guid> source = ctx.CreateQuery<Guid>("select u.UserId FROM Users as u, Memberships as m, Applications as a WHERE ToLower(a.ApplicationName) = @an AND a.ApplicationId = m.ApplicationId AND m.UserId = u.UserId AND u.LastActivityDate > @ad", new ObjectParameter[]
			{
				new ObjectParameter("an", applicationName.ToLowerInvariant()),
				new ObjectParameter("ad", dateactive)
			});
			return source.Count<Guid>();
		}
		internal static RoleEntity GetRole(MembershipEntities ctx, Guid roleId, string applicationName)
		{
			ObjectQuery<RoleEntity> source = ctx.CreateQuery<RoleEntity>("select value r FROM Roles as r, Applications as a WHERE ToLower(a.ApplicationName) = @appName AND a.ApplicationId = r.ApplicationId AND r.RoleId = @roleId", new ObjectParameter[]
			{
				new ObjectParameter("roleId", roleId),
				new ObjectParameter("appName", applicationName.ToLowerInvariant())
			});
			return source.FirstOrDefault<RoleEntity>();
		}
		internal static RoleEntity GetRole(MembershipEntities ctx, string roleName, string applicationName)
		{
			ObjectQuery<RoleEntity> source = ctx.CreateQuery<RoleEntity>("select value r FROM Roles as r, Applications as a WHERE ToLower(a.ApplicationName) = @appName AND a.ApplicationId = r.ApplicationId AND ToLower(r.RoleName) = @roleName", new ObjectParameter[]
			{
				new ObjectParameter("roleName", roleName.ToLowerInvariant()),
				new ObjectParameter("appName", applicationName.ToLowerInvariant())
			});
			return source.FirstOrDefault<RoleEntity>();
		}
		internal static string[] GetAllRoleNames(MembershipEntities ctx, string applicationName)
		{
			ObjectQuery<string> source = ctx.CreateQuery<string>("select value r.RoleName FROM Roles as r, Applications as a WHERE ToLower(a.ApplicationName) = @appName AND a.ApplicationId = r.ApplicationId ORDER BY r.RoleName", new ObjectParameter[]
			{
				new ObjectParameter("appName", applicationName.ToLowerInvariant())
			});
			return source.ToArray<string>();
		}
		internal static string[] GetRolesNamesForUser(MembershipEntities ctx, string applicationName, string username)
		{
			ObjectQuery<string> source = ctx.CreateQuery<string>("select value r.RoleName FROM Users as u, Roles as r, Applications as a, UsersInRoles as ur WHERE ToLower(a.ApplicationName) = @appName AND a.ApplicationId = r.ApplicationId AND ToLower(u.UserName) = @userName AND u.UserId = ur.UserId AND ur.RoleId = r.RoleId ORDER BY r.RoleName", new ObjectParameter[]
			{
				new ObjectParameter("appName", applicationName.ToLowerInvariant()),
				new ObjectParameter("userName", username.ToLowerInvariant())
			});
			return source.ToArray<string>();
		}
		internal static IQueryable<UsersInRole> GetRolesForUser(MembershipEntities ctx, string applicationName, string username)
		{
			return ctx.CreateQuery<UsersInRole>("select value ur FROM Users as u, Roles as r, Applications as a, UsersInRoles as ur WHERE ToLower(a.ApplicationName) = @appName AND a.ApplicationId = r.ApplicationId AND ToLower(u.UserName) = @userName AND u.UserId = ur.UserId AND ur.RoleId = r.RoleId ORDER BY r.RoleName", new ObjectParameter[]
			{
				new ObjectParameter("appName", applicationName.ToLowerInvariant()),
				new ObjectParameter("userName", username.ToLowerInvariant())
			});
		}
		internal static UsersInRole GetUserInRole(MembershipEntities ctx, string applicationName, string roleName, string userName)
		{
			ObjectQuery<UsersInRole> source = ctx.CreateQuery<UsersInRole>("select value ur FROM Roles as r, Applications as a, Users as u, UsersInRoles as ur WHERE ToLower(a.ApplicationName) = @appName AND a.ApplicationId = r.ApplicationId AND ToLower(r.RoleName) = @roleName AND ur.RoleId = r.RoleId AND ToLower(u.UserName) = @userName AND u.UserId = ur.UserId && u.ApplicationId = a.ApplicationId", new ObjectParameter[]
			{
				new ObjectParameter("roleName", roleName.ToLowerInvariant()),
				new ObjectParameter("userName", userName.ToLowerInvariant()),
				new ObjectParameter("appName", applicationName.ToLowerInvariant())
			});
			return source.FirstOrDefault<UsersInRole>();
		}
		internal static string[] GetUserNamesInRole(MembershipEntities ctx, string applicationName, string roleName, string userNameToMatch = "")
		{
			string text = "select value u.UserName FROM Roles as r, Applications as a, Users as u, UsersInRoles as ur WHERE ToLower(a.ApplicationName) = @appName AND a.ApplicationId = r.ApplicationId AND ToLower(r.RoleName) = @roleName AND ur.RoleId = r.RoleId AND u.UserId = ur.UserId && u.ApplicationId = a.ApplicationId";
			if (!string.IsNullOrEmpty(userNameToMatch))
			{
				text += " AND IndexOf( @userName, ToLower(u.UserName) ) > 0";
			}
			text += " ORDER BY u.UserName";
			ObjectQuery<string> source = ctx.CreateQuery<string>(text, new ObjectParameter[]
			{
				new ObjectParameter("roleName", roleName.ToLowerInvariant()),
				new ObjectParameter("userName", userNameToMatch.ToLowerInvariant()),
				new ObjectParameter("appName", applicationName.ToLowerInvariant())
			});
			return source.ToArray<string>();
		}
		internal static IQueryable<UsersInRole> GetUserRolesInRole(MembershipEntities ctx, string applicationName, string roleName)
		{
			return ctx.CreateQuery<UsersInRole>("select value ur FROM Roles as r, Applications as a, UsersInRoles as ur WHERE ToLower(a.ApplicationName) = @appName AND a.ApplicationId = r.ApplicationId AND ToLower(r.RoleName) = @roleName AND ur.RoleId = r.RoleId", new ObjectParameter[]
			{
				new ObjectParameter("roleName", roleName.ToLowerInvariant()),
				new ObjectParameter("appName", applicationName.ToLowerInvariant())
			});
		}
		internal static ProfileEntity GetProfile(MembershipEntities ctx, string applicationName, string username)
		{
			ObjectQuery<ProfileEntity> source = ctx.CreateQuery<ProfileEntity>("select value p FROM Users as u, Profiles as p, Applications as a WHERE ToLower(a.ApplicationName) = @appName AND a.ApplicationId = u.ApplicationId AND ToLower(u.UserName) = @userName AND u.UserId = p.UserId", new ObjectParameter[]
			{
				new ObjectParameter("appName", applicationName.ToLowerInvariant()),
				new ObjectParameter("userName", username.ToLowerInvariant())
			});
			return source.FirstOrDefault<ProfileEntity>();
		}
		private static string AppendProfileAuthenticationOption(string query, ProfileAuthenticationOption authenticationOption)
		{
			switch (authenticationOption)
			{
			case ProfileAuthenticationOption.Anonymous:
				query += " AND u.IsAnonymous";
				break;

			case ProfileAuthenticationOption.Authenticated:
				query += " AND !u.IsAnonymous";
				break;
			}
			return query;
		}
		internal static IQueryable<DbDataRecord> GetProfileInfos(MembershipEntities ctx, string applicationName, ProfileAuthenticationOption authenticationOption, DateTime inactiveSince, string userNameContains = null, int pageIndex = -1, int pageSize = -1)
		{
			string text = "select u.UserName, u.IsAnonymous, u.LastActivityDate, p.LastUpdatedDate, p.PropertyValueBinary, (length(p.PropertyNames) + length(p.PropertyValueStrings)) As Size FROM Users as u, Profiles as p, Applications as a WHERE ToLower(a.ApplicationName) = @appName AND a.ApplicationId = u.ApplicationId AND p.UserId = u.UserId";
			text = QueryHelper.AppendProfileAuthenticationOption(text, authenticationOption);
			text += " AND u.LastActivityDate < @inactiveSince";
			if (!string.IsNullOrEmpty(userNameContains))
			{
				text += " AND IndexOf(@userNameContains, ToLower(u.UserName)) > 0";
			}
			else
			{
				userNameContains = "";
			}
			text = QueryHelper.AppendUserNameSkipLimitIfNeeded(text, pageIndex, pageSize);
			return ctx.CreateQuery<DbDataRecord>(text, new ObjectParameter[]
			{
				new ObjectParameter("appName", applicationName.ToLowerInvariant()),
				new ObjectParameter("inactiveSince", inactiveSince.ToUniversalTime()),
				new ObjectParameter("userNameContains", userNameContains.ToLowerInvariant())
			});
		}
		internal static IQueryable<ProfileEntity> GetAllProfiles(MembershipEntities ctx, string applicationName, int pageIndex = -1, int pageSize = -1)
		{
			string text = "select value p FROM Profiles as p, Applications as a, Users as u WHERE ToLower(a.ApplicationName) = @appName AND a.ApplicationId = u.ApplicationId AND u.UserId = p.UserId";
			text = QueryHelper.AppendUserNameSkipLimitIfNeeded(text, pageIndex, pageSize);
			return ctx.CreateQuery<ProfileEntity>(text, new ObjectParameter[]
			{
				new ObjectParameter("appName", applicationName.ToLowerInvariant())
			});
		}
		internal static IQueryable<ProfileEntity> GetAllInactiveProfiles(MembershipEntities ctx, string applicationName, ProfileAuthenticationOption authenticationOption, DateTime inactiveSinceDate, string userNameContains = null, int pageIndex = -1, int pageSize = -1)
		{
			string text = "select value p FROM Users as u, Profiles as p, Applications as a WHERE ToLower(a.ApplicationName) = @appName AND a.ApplicationId = u.ApplicationId AND p.UserId = u.UserId AND u.LastActivityDate < @inactiveSince";
			text = QueryHelper.AppendProfileAuthenticationOption(text, authenticationOption);
			if (!string.IsNullOrEmpty(userNameContains))
			{
				text += " AND IndexOf(@userNameContains, ToLower(u.UserName)) > 0";
			}
			else
			{
				userNameContains = "";
			}
			text = QueryHelper.AppendUserNameSkipLimitIfNeeded(text, pageIndex, pageSize);
			return ctx.CreateQuery<ProfileEntity>(text, new ObjectParameter[]
			{
				new ObjectParameter("appName", applicationName.ToLowerInvariant()),
				new ObjectParameter("inactiveSince", inactiveSinceDate.ToUniversalTime()),
				new ObjectParameter("userNameContains", userNameContains.ToLowerInvariant())
			});
		}
		internal static SessionEntity GetSession(SessionEntities ctx, string id)
		{
			ObjectQuery<SessionEntity> source = ctx.CreateQuery<SessionEntity>("select value s FROM Sessions as s WHERE s.SessionId = @id", new ObjectParameter[]
			{
				new ObjectParameter("id", id)
			});
			return source.FirstOrDefault<SessionEntity>();
		}
		internal static IQueryable<SessionEntity> GetExpiredSessions(SessionEntities ctx)
		{
			return ctx.CreateQuery<SessionEntity>("select value s FROM Sessions as s WHERE s.Expires < @now", new ObjectParameter[]
			{
				new ObjectParameter("now", DateTime.UtcNow)
			});
		}
	}
}
