using System;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Configuration;
using System.Configuration.Provider;
using System.Data.Common;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Web.Profile;
using System.Web.Providers.Entities;
using System.Web.Providers.Resources;
using System.Xml.Serialization;
namespace System.Web.Providers
{
	public class DefaultProfileProvider : ProfileProvider
	{
		public override string ApplicationName
		{
			get;
			set;
		}
        public static Func<ConnectionStringSettings> FnGetConnectionStringSettings { get; set; }

        private ConnectionStringSettings _connectionString;
        private ConnectionStringSettings ConnectionString
        {
            get
            {
                if (FnGetConnectionStringSettings != null)
                    return FnGetConnectionStringSettings();
                return _connectionString;
            }
            set
            {
                _connectionString = value;
            }
        }
        public override int DeleteInactiveProfiles(ProfileAuthenticationOption authenticationOption, DateTime userInactiveSinceDate)
		{
			int result;
			using (MembershipEntities membershipEntities = ModelHelper.CreateMembershipEntities(this.ConnectionString))
			{
				IQueryable<ProfileEntity> allInactiveProfiles = QueryHelper.GetAllInactiveProfiles(membershipEntities, this.ApplicationName, authenticationOption, userInactiveSinceDate, null, -1, -1);
				int num = 0;
				foreach (ProfileEntity current in allInactiveProfiles)
				{
					membershipEntities.Profiles.DeleteObject(current);
					num++;
				}
				membershipEntities.SaveChanges();
				result = num;
			}
			return result;
		}
		public override int DeleteProfiles(string[] usernames)
		{
			bool checkForNull = true;
			bool checkIfEmpty = true;
			bool checkForCommas = true;
			int maxSize = 256;
			string paramName = "usernames";
			Exception ex = ValidationHelper.CheckArrayParameter(ref usernames, checkForNull, checkIfEmpty, checkForCommas, maxSize, paramName);
			if (ex != null)
			{
				throw ex;
			}
			int num = 0;
			using (MembershipEntities membershipEntities = ModelHelper.CreateMembershipEntities(this.ConnectionString))
			{
				string[] array = usernames;
				for (int i = 0; i < array.Length; i++)
				{
					string username = array[i];
					ProfileEntity profile = QueryHelper.GetProfile(membershipEntities, this.ApplicationName, username);
					if (profile != null)
					{
						num++;
						membershipEntities.Profiles.DeleteObject(profile);
					}
				}
				membershipEntities.SaveChanges();
			}
			return num;
		}
		public override int DeleteProfiles(ProfileInfoCollection profiles)
		{
			if (profiles == null)
			{
				throw new ArgumentNullException("profiles");
			}
			if (profiles.Count < 1)
			{
				throw new ArgumentException(string.Format(CultureInfo.CurrentCulture, ProviderResources.Parameter_collection_empty, new object[]
				{
					"profiles"
				}), "profiles");
			}
			int num = 0;
			using (MembershipEntities membershipEntities = ModelHelper.CreateMembershipEntities(this.ConnectionString))
			{
				foreach (ProfileInfo profileInfo in profiles)
				{
					ProfileEntity profile = QueryHelper.GetProfile(membershipEntities, this.ApplicationName, profileInfo.UserName);
					if (profile != null)
					{
						num++;
						membershipEntities.Profiles.DeleteObject(profile);
					}
				}
				membershipEntities.SaveChanges();
			}
			return num;
		}
		public override ProfileInfoCollection FindInactiveProfilesByUserName(ProfileAuthenticationOption authenticationOption, string usernameToMatch, DateTime userInactiveSinceDate, int pageIndex, int pageSize, out int totalRecords)
		{
			bool checkForNull = true;
			bool checkIfEmpty = true;
			bool checkForCommas = false;
			int maxSize = 256;
			string paramName = "usernameToMatch";
			Exception ex = ValidationHelper.CheckParameter(ref usernameToMatch, checkForNull, checkIfEmpty, checkForCommas, maxSize, paramName);
			if (ex != null)
			{
				throw ex;
			}
			ProfileInfoCollection profileInfoCollection = new ProfileInfoCollection();
			using (MembershipEntities membershipEntities = ModelHelper.CreateMembershipEntities(this.ConnectionString))
			{
				totalRecords = QueryHelper.GetProfileInfos(membershipEntities, this.ApplicationName, authenticationOption, userInactiveSinceDate, usernameToMatch, -1, -1).Count<DbDataRecord>();
				IQueryable<DbDataRecord> profileInfos = QueryHelper.GetProfileInfos(membershipEntities, this.ApplicationName, authenticationOption, userInactiveSinceDate, usernameToMatch, pageIndex, pageSize);
				foreach (DbDataRecord current in profileInfos)
				{
					profileInfoCollection.Add(QueryHelper.Convert<QueryHelper.EFProfileInfo>(current).ToProfileInfo());
				}
			}
			return profileInfoCollection;
		}
		public override ProfileInfoCollection FindProfilesByUserName(ProfileAuthenticationOption authenticationOption, string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
		{
			bool checkForNull = true;
			bool checkIfEmpty = true;
			bool checkForCommas = false;
			int maxSize = 256;
			string paramName = "usernameToMatch";
			Exception ex = ValidationHelper.CheckParameter(ref usernameToMatch, checkForNull, checkIfEmpty, checkForCommas, maxSize, paramName);
			if (ex != null)
			{
				throw ex;
			}
			ProfileInfoCollection profileInfoCollection = new ProfileInfoCollection();
			using (MembershipEntities membershipEntities = ModelHelper.CreateMembershipEntities(this.ConnectionString))
			{
				DateTime inactiveSince = DateTime.UtcNow.AddDays(1.0);
				totalRecords = QueryHelper.GetProfileInfos(membershipEntities, this.ApplicationName, authenticationOption, inactiveSince, usernameToMatch, -1, -1).Count<DbDataRecord>();
				IQueryable<DbDataRecord> profileInfos = QueryHelper.GetProfileInfos(membershipEntities, this.ApplicationName, authenticationOption, inactiveSince, usernameToMatch, pageIndex, pageSize);
				foreach (DbDataRecord current in profileInfos)
				{
					profileInfoCollection.Add(QueryHelper.Convert<QueryHelper.EFProfileInfo>(current).ToProfileInfo());
				}
			}
			return profileInfoCollection;
		}
		public override ProfileInfoCollection GetAllInactiveProfiles(ProfileAuthenticationOption authenticationOption, DateTime userInactiveSinceDate, int pageIndex, int pageSize, out int totalRecords)
		{
			ProfileInfoCollection profileInfoCollection = new ProfileInfoCollection();
			using (MembershipEntities membershipEntities = ModelHelper.CreateMembershipEntities(this.ConnectionString))
			{
				totalRecords = QueryHelper.GetProfileInfos(membershipEntities, this.ApplicationName, authenticationOption, userInactiveSinceDate, null, -1, -1).Count<DbDataRecord>();
				IQueryable<DbDataRecord> profileInfos = QueryHelper.GetProfileInfos(membershipEntities, this.ApplicationName, authenticationOption, userInactiveSinceDate, null, pageIndex, pageSize);
				foreach (DbDataRecord current in profileInfos)
				{
					profileInfoCollection.Add(QueryHelper.Convert<QueryHelper.EFProfileInfo>(current).ToProfileInfo());
				}
			}
			return profileInfoCollection;
		}
		public override ProfileInfoCollection GetAllProfiles(ProfileAuthenticationOption authenticationOption, int pageIndex, int pageSize, out int totalRecords)
		{
			ProfileInfoCollection profileInfoCollection = new ProfileInfoCollection();
			using (MembershipEntities membershipEntities = ModelHelper.CreateMembershipEntities(this.ConnectionString))
			{
				DateTime inactiveSince = DateTime.UtcNow.AddDays(1.0);
				totalRecords = QueryHelper.GetProfileInfos(membershipEntities, this.ApplicationName, authenticationOption, inactiveSince, null, -1, -1).Count<DbDataRecord>();
				IQueryable<DbDataRecord> profileInfos = QueryHelper.GetProfileInfos(membershipEntities, this.ApplicationName, authenticationOption, inactiveSince, null, pageIndex, pageSize);
				foreach (DbDataRecord current in profileInfos)
				{
					profileInfoCollection.Add(QueryHelper.Convert<QueryHelper.EFProfileInfo>(current).ToProfileInfo());
				}
			}
			return profileInfoCollection;
		}
		public override int GetNumberOfInactiveProfiles(ProfileAuthenticationOption authenticationOption, DateTime userInactiveSinceDate)
		{
			int result;
			using (MembershipEntities membershipEntities = ModelHelper.CreateMembershipEntities(this.ConnectionString))
			{
				result = QueryHelper.GetProfileInfos(membershipEntities, this.ApplicationName, authenticationOption, userInactiveSinceDate, null, -1, -1).Count<DbDataRecord>();
			}
			return result;
		}
		public override SettingsPropertyValueCollection GetPropertyValues(SettingsContext context, SettingsPropertyCollection collection)
		{
			if (collection == null)
			{
				throw new ArgumentNullException("collection");
			}
			if (context == null)
			{
				throw new ArgumentNullException("context");
			}
			SettingsPropertyValueCollection settingsPropertyValueCollection = new SettingsPropertyValueCollection();
			if (collection.Count >= 1)
			{
				string text = (string)context["UserName"];
				foreach (SettingsProperty settingsProperty in collection)
				{
					if (settingsProperty.SerializeAs == SettingsSerializeAs.ProviderSpecific)
					{
						if (settingsProperty.PropertyType.IsPrimitive || settingsProperty.PropertyType == typeof(string))
						{
							settingsProperty.SerializeAs = SettingsSerializeAs.String;
						}
						else
						{
							settingsProperty.SerializeAs = SettingsSerializeAs.Xml;
						}
					}
					settingsPropertyValueCollection.Add(new SettingsPropertyValue(settingsProperty));
				}
				if (!string.IsNullOrEmpty(text))
				{
					using (MembershipEntities membershipEntities = ModelHelper.CreateMembershipEntities(this.ConnectionString))
					{
						ProfileEntity profile = QueryHelper.GetProfile(membershipEntities, this.ApplicationName, text);
						if (profile != null)
						{
							DefaultProfileProvider.ParseDataFromDB(profile.PropertyNames.Split(new char[]
							{
								':'
							}), profile.PropertyValueStrings, settingsPropertyValueCollection);
						}
					}
				}
			}
			return settingsPropertyValueCollection;
		}
		public override void SetPropertyValues(SettingsContext context, SettingsPropertyValueCollection collection)
		{
			if (context == null)
			{
				throw new ArgumentNullException("context");
			}
			if (collection == null)
			{
				throw new ArgumentNullException("collection");
			}
			string text = (string)context["UserName"];
			bool flag = (bool)context["IsAuthenticated"];
			if (!string.IsNullOrEmpty(text) && collection.Count > 0)
			{
				string empty = string.Empty;
				string empty2 = string.Empty;
				DefaultProfileProvider.PrepareDataForSaving(ref empty, ref empty2, collection, flag);
				if (empty.Length == 0)
				{
					return;
				}
				using (MembershipEntities membershipEntities = ModelHelper.CreateMembershipEntities(this.ConnectionString))
				{
					ProfileEntity profileEntity = QueryHelper.GetProfile(membershipEntities, this.ApplicationName, text);
					User user;
					if (profileEntity == null)
					{
						Application application = QueryHelper.GetApplication(membershipEntities, this.ApplicationName);
						if (application == null)
						{
							application = ModelHelper.CreateApplication(membershipEntities, this.ApplicationName);
						}
						user = QueryHelper.GetUser(membershipEntities, text, this.ApplicationName);
						if (user == null)
						{
							user = ModelHelper.CreateUser(membershipEntities, Guid.NewGuid(), text, application.ApplicationId, !flag);
						}
						profileEntity = new ProfileEntity();
						profileEntity.UserId = user.UserId;
						membershipEntities.Profiles.AddObject(profileEntity);
					}
					else
					{
						user = QueryHelper.GetUser(membershipEntities, text, this.ApplicationName);
					}
					user.LastActivityDate = DateTime.UtcNow;
					profileEntity.LastUpdatedDate = DateTime.UtcNow;
					profileEntity.PropertyNames = empty;
					profileEntity.PropertyValueStrings = empty2;
					profileEntity.PropertyValueBinary = new byte[0];
					membershipEntities.SaveChanges();
				}
			}
		}
		private static void ParseDataFromDB(string[] names, string values, SettingsPropertyValueCollection properties)
		{
			if (names == null || values == null || properties == null)
			{
				return;
			}
			try
			{
				for (int i = 0; i < names.Length / 3; i++)
				{
					int num = i * 3;
					string name = names[num];
					SettingsPropertyValue settingsPropertyValue = properties[name];
					if (settingsPropertyValue != null)
					{
						int num2 = int.Parse(names[num + 1], CultureInfo.InvariantCulture);
						int num3 = int.Parse(names[num + 2], CultureInfo.InvariantCulture);
						if (num3 == -1 && !settingsPropertyValue.Property.PropertyType.IsValueType)
						{
							settingsPropertyValue.PropertyValue = null;
							settingsPropertyValue.IsDirty = false;
							settingsPropertyValue.Deserialized = true;
						}
						else
						{
							if (num2 >= 0 && num3 > 0 && values.Length >= num2 + num3)
							{
								settingsPropertyValue.PropertyValue = DefaultProfileProvider.DeserializeFromString(settingsPropertyValue.Property, values.Substring(num2, num3));
								settingsPropertyValue.IsDirty = false;
								settingsPropertyValue.Deserialized = true;
							}
						}
					}
				}
			}
			catch (Exception)
			{
			}
		}
		private static object DeserializeFromString(SettingsProperty property, string value)
		{
			if (string.IsNullOrEmpty(value))
			{
				return null;
			}
			Type propertyType = property.PropertyType;
			SettingsSerializeAs serializeAs = property.SerializeAs;
			if (propertyType == typeof(string) && (value == null || value.Length < 1 || serializeAs == SettingsSerializeAs.String))
			{
				return value;
			}
			switch (serializeAs)
			{
			case SettingsSerializeAs.String:
				goto IL_AB;

			case SettingsSerializeAs.Xml:
				break;

			case SettingsSerializeAs.Binary:
				{
					byte[] buffer = Convert.FromBase64String(value);
					MemoryStream memoryStream = null;
					try
					{
						memoryStream = new MemoryStream(buffer);
						object result = new NetDataContractSerializer().Deserialize(memoryStream);
						return result;
					}
					finally
					{
						if (memoryStream != null)
						{
							memoryStream.Close();
						}
					}
					break;
				}

			default:
				return null;
			}
			using (StringReader stringReader = new StringReader(value))
			{
				XmlSerializer xmlSerializer = new XmlSerializer(propertyType);
				object result = xmlSerializer.Deserialize(stringReader);
				return result;
			}
			IL_AB:
			TypeConverter converter = TypeDescriptor.GetConverter(propertyType);
			if (converter != null && converter.CanConvertTo(typeof(string)) && converter.CanConvertFrom(typeof(string)))
			{
				return converter.ConvertFromInvariantString(value);
			}
			throw new ProviderException(string.Format(CultureInfo.CurrentCulture, ProviderResources.Unable_to_convert_type_to_string, new object[]
			{
				propertyType.ToString()
			}));
		}
		private static string SerializeToString(SettingsPropertyValue value)
		{
			if (value.PropertyValue == null)
			{
				return null;
			}
			SettingsSerializeAs settingsSerializeAs = value.Property.SerializeAs;
			Type propertyType = value.Property.PropertyType;
			if (settingsSerializeAs == SettingsSerializeAs.ProviderSpecific)
			{
				if (propertyType == typeof(string) || propertyType.IsPrimitive)
				{
					settingsSerializeAs = SettingsSerializeAs.String;
				}
				else
				{
					settingsSerializeAs = SettingsSerializeAs.Xml;
				}
			}
			switch (settingsSerializeAs)
			{
			case SettingsSerializeAs.String:
				{
					TypeConverter converter = TypeDescriptor.GetConverter(propertyType);
					if (converter != null && converter.CanConvertTo(typeof(string)) && converter.CanConvertFrom(typeof(string)))
					{
						return converter.ConvertToInvariantString(value.PropertyValue);
					}
					throw new ProviderException(string.Format(CultureInfo.CurrentCulture, ProviderResources.Unable_to_convert_type_to_string, new object[]
					{
						propertyType.ToString()
					}));
				}

			case SettingsSerializeAs.Xml:
				using (StringWriter stringWriter = new StringWriter(CultureInfo.InvariantCulture))
				{
					XmlSerializer xmlSerializer = new XmlSerializer(propertyType);
					xmlSerializer.Serialize(stringWriter, value.PropertyValue);
					string result = stringWriter.ToString();
					return result;
				}
				break;

			case SettingsSerializeAs.Binary:
				break;

			default:
				goto IL_12F;
			}
			MemoryStream memoryStream = new MemoryStream();
			try
			{
				NetDataContractSerializer netDataContractSerializer = new NetDataContractSerializer();
				netDataContractSerializer.Serialize(memoryStream, value.PropertyValue);
				string result = Convert.ToBase64String(memoryStream.ToArray());
				return result;
			}
			finally
			{
				memoryStream.Close();
			}
			IL_12F:
			return null;
		}
		private static void PrepareDataForSaving(ref string allNames, ref string allValues, SettingsPropertyValueCollection properties, bool userIsAuthenticated)
		{
			StringBuilder stringBuilder = new StringBuilder();
			StringBuilder stringBuilder2 = new StringBuilder();
			bool flag = false;
			foreach (SettingsPropertyValue settingsPropertyValue in properties)
			{
				if (settingsPropertyValue.IsDirty)
				{
					if (!userIsAuthenticated)
					{
						bool flag2 = (bool)settingsPropertyValue.Property.Attributes["AllowAnonymous"];
						if (!flag2)
						{
							continue;
						}
					}
					flag = true;
					break;
				}
			}
			if (!flag)
			{
				return;
			}
			foreach (SettingsPropertyValue settingsPropertyValue2 in properties)
			{
				if (!userIsAuthenticated)
				{
					bool flag3 = (bool)settingsPropertyValue2.Property.Attributes["AllowAnonymous"];
					if (!flag3)
					{
						continue;
					}
				}
				if (settingsPropertyValue2.IsDirty || !settingsPropertyValue2.UsingDefaultValue)
				{
					int num = 0;
					string text = null;
					int num2;
					if (settingsPropertyValue2.Deserialized && settingsPropertyValue2.PropertyValue == null)
					{
						num2 = -1;
					}
					else
					{
						text = DefaultProfileProvider.SerializeToString(settingsPropertyValue2);
						num2 = text.Length;
						num = stringBuilder2.Length;
					}
					stringBuilder.Append(string.Concat(new string[]
					{
						settingsPropertyValue2.Name,
						":",
						num.ToString(CultureInfo.InvariantCulture),
						":",
						num2.ToString(CultureInfo.InvariantCulture),
						":"
					}));
					if (text != null)
					{
						stringBuilder2.Append(text);
					}
				}
			}
			allNames = stringBuilder.ToString();
			allValues = stringBuilder2.ToString();
		}
		public override void Initialize(string name, NameValueCollection config)
		{
			if (config == null)
			{
				throw new ArgumentNullException("config");
			}
			if (string.IsNullOrEmpty(name))
			{
				name = "DefaultProfileProvider";
			}
			if (string.IsNullOrEmpty(config["description"]))
			{
				config.Remove("description");
				config.Add("description", string.Format(CultureInfo.CurrentCulture, ProviderResources.ProfileProvider_description, new object[0]));
			}
			base.Initialize(name, config);
			if (!string.IsNullOrEmpty(config["applicationName"]))
			{
				this.ApplicationName = config["applicationName"];
			}
			else
			{
				this.ApplicationName = ModelHelper.GetDefaultAppName();
			}
			config.Remove("applicationName");

            string connectionStringName = config["connectionStringName"];
            this.ConnectionString = ModelHelper.GetConnectionString(connectionStringName);
			config.Remove("connectionStringName");

            // check that there isn't an azure config that overrides this
            string extra = ConceptCave.Configuration.ButterflyEnvironment.Current.GetConfigurationSettingValue(connectionStringName);
            if (string.IsNullOrEmpty(extra) == false)
            {
                this.ConnectionString = new ConnectionStringSettings(this.ConnectionString.Name, extra, this.ConnectionString.ProviderName);
            }

			if (config.Count > 0)
			{
				string key = config.GetKey(0);
				if (!string.IsNullOrEmpty(key))
				{
					throw new ProviderException(string.Format(CultureInfo.CurrentCulture, ProviderResources.Provider_unrecognized_attribute, new object[]
					{
						key
					}));
				}
			}
		}
	}
}
