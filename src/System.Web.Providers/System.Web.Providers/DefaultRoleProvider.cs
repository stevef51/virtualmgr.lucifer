using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Configuration.Provider;
using System.Globalization;
using System.Linq;
using System.Web.Providers.Entities;
using System.Web.Providers.Resources;
using System.Web.Security;
namespace System.Web.Providers
{
	public class DefaultRoleProvider : RoleProvider
	{
		public override string ApplicationName
		{
			get;
			set;
		}
        public static Func<ConnectionStringSettings> FnGetConnectionStringSettings { get; set; }

        private ConnectionStringSettings _connectionString;
        private ConnectionStringSettings ConnectionString
        {
            get
            {
                if (FnGetConnectionStringSettings != null)
                    return FnGetConnectionStringSettings();
                return _connectionString;
            }
            set
            {
                _connectionString = value;
            }
        }
        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
		{
			if (usernames == null)
			{
				throw new ArgumentNullException("usernames");
			}
			if (roleNames == null)
			{
				throw new ArgumentNullException("roleNames");
			}
			using (MembershipEntities membershipEntities = ModelHelper.CreateMembershipEntities(this.ConnectionString))
			{
				if (QueryHelper.GetApplication(membershipEntities, this.ApplicationName) == null)
				{
					Application application = ModelHelper.CreateApplication(membershipEntities, this.ApplicationName);
				}
				List<User> list = new List<User>();
				for (int i = 0; i < usernames.Length; i++)
				{
					string text = usernames[i];
					User user = QueryHelper.GetUser(membershipEntities, text, this.ApplicationName);
					if (user == null)
					{
						throw new ProviderException(string.Format(CultureInfo.CurrentCulture, ProviderResources.Provider_this_user_not_found, new object[]
						{
							text
						}));
					}
					list.Add(user);
				}
				List<RoleEntity> list2 = new List<RoleEntity>();
				for (int j = 0; j < roleNames.Length; j++)
				{
					string text2 = roleNames[j];
					RoleEntity role = QueryHelper.GetRole(membershipEntities, text2, this.ApplicationName);
					if (role == null)
					{
						throw new ProviderException(string.Format(CultureInfo.CurrentCulture, ProviderResources.Provider_role_not_found, new object[]
						{
							text2
						}));
					}
					list2.Add(role);
				}
				foreach (User current in list)
				{
					foreach (RoleEntity current2 in list2)
					{
						UsersInRole userInRole = QueryHelper.GetUserInRole(membershipEntities, this.ApplicationName, current2.RoleName, current.UserName);
						if (userInRole != null)
						{
							throw new ProviderException(string.Format(CultureInfo.CurrentCulture, ProviderResources.Provider_this_user_already_in_role, new object[]
							{
								current.UserName,
								current2.RoleName
							}));
						}
						UsersInRole usersInRole = new UsersInRole();
						usersInRole.UserId = current.UserId;
						usersInRole.RoleId = current2.RoleId;
						membershipEntities.UsersInRoles.AddObject(usersInRole);
					}
				}
				membershipEntities.SaveChanges();
			}
		}
		public override void CreateRole(string roleName)
		{
			bool checkForNull = true;
			bool checkIfEmpty = true;
			bool checkForCommas = true;
			int maxSize = 256;
			string paramName = "roleName";
			Exception ex = ValidationHelper.CheckParameter(ref roleName, checkForNull, checkIfEmpty, checkForCommas, maxSize, paramName);
			if (ex != null)
			{
				throw ex;
			}
			using (MembershipEntities membershipEntities = ModelHelper.CreateMembershipEntities(this.ConnectionString))
			{
				Application application = QueryHelper.GetApplication(membershipEntities, this.ApplicationName);
				if (application == null)
				{
					application = ModelHelper.CreateApplication(membershipEntities, this.ApplicationName);
				}
				Guid applicationId = application.ApplicationId;
				RoleEntity role = QueryHelper.GetRole(membershipEntities, roleName, this.ApplicationName);
				if (role != null)
				{
					throw new ProviderException(string.Format(CultureInfo.CurrentCulture, ProviderResources.Provider_role_already_exists, new object[]
					{
						roleName
					}));
				}
				RoleEntity roleEntity = new RoleEntity();
				roleEntity.RoleId = Guid.NewGuid();
				roleEntity.ApplicationId = applicationId;
				roleEntity.RoleName = roleName;
				membershipEntities.Roles.AddObject(roleEntity);
				membershipEntities.SaveChanges();
			}
		}
		public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
		{
			bool checkForNull = true;
			bool checkIfEmpty = true;
			bool checkForCommas = true;
			int maxSize = 256;
			string paramName = "roleName";
			Exception ex = ValidationHelper.CheckParameter(ref roleName, checkForNull, checkIfEmpty, checkForCommas, maxSize, paramName);
			if (ex != null)
			{
				throw ex;
			}
			bool result;
			using (MembershipEntities membershipEntities = ModelHelper.CreateMembershipEntities(this.ConnectionString))
			{
				RoleEntity role = QueryHelper.GetRole(membershipEntities, roleName, this.ApplicationName);
				if (role == null)
				{
					result = false;
				}
				else
				{
					IQueryable<UsersInRole> userRolesInRole = QueryHelper.GetUserRolesInRole(membershipEntities, this.ApplicationName, roleName);
					if (throwOnPopulatedRole && userRolesInRole.Count<UsersInRole>() > 0)
					{
						throw new ProviderException(string.Format(CultureInfo.CurrentCulture, ProviderResources.Role_is_not_empty, new object[0]));
					}
					foreach (UsersInRole current in userRolesInRole)
					{
						membershipEntities.UsersInRoles.DeleteObject(current);
					}
					membershipEntities.Roles.DeleteObject(role);
					membershipEntities.SaveChanges();
					result = true;
				}
			}
			return result;
		}
		public override string[] FindUsersInRole(string roleName, string usernameToMatch)
		{
			bool checkForNull = true;
			bool checkIfEmpty = true;
			bool checkForCommas = false;
			int maxSize = 256;
			string paramName = "usernameToMatch";
			Exception ex = ValidationHelper.CheckParameter(ref usernameToMatch, checkForNull, checkIfEmpty, checkForCommas, maxSize, paramName);
			if (ex != null)
			{
				throw ex;
			}
			bool checkForNull2 = true;
			bool checkIfEmpty2 = true;
			bool checkForCommas2 = true;
			int maxSize2 = 256;
			string paramName2 = "roleName";
			ex = ValidationHelper.CheckParameter(ref roleName, checkForNull2, checkIfEmpty2, checkForCommas2, maxSize2, paramName2);
			if (ex != null)
			{
				throw ex;
			}
			string[] userNamesInRole;
			using (MembershipEntities membershipEntities = ModelHelper.CreateMembershipEntities(this.ConnectionString))
			{
				if (QueryHelper.GetRole(membershipEntities, roleName, this.ApplicationName) == null)
				{
					throw new ProviderException(string.Format(CultureInfo.CurrentCulture, ProviderResources.Provider_role_not_found, new object[]
					{
						roleName
					}));
				}
				userNamesInRole = QueryHelper.GetUserNamesInRole(membershipEntities, this.ApplicationName, roleName, usernameToMatch);
			}
			return userNamesInRole;
		}
		public override string[] GetAllRoles()
		{
			string[] allRoleNames;
			using (MembershipEntities membershipEntities = ModelHelper.CreateMembershipEntities(this.ConnectionString))
			{
				allRoleNames = QueryHelper.GetAllRoleNames(membershipEntities, this.ApplicationName);
			}
			return allRoleNames;
		}
		public override string[] GetRolesForUser(string username)
		{
			bool checkForNull = true;
			bool checkIfEmpty = false;
			bool checkForCommas = true;
			int maxSize = 256;
			string paramName = "username";
			Exception ex = ValidationHelper.CheckParameter(ref username, checkForNull, checkIfEmpty, checkForCommas, maxSize, paramName);
			if (ex != null)
			{
				throw ex;
			}
			if (username.Length < 1)
			{
				return new string[0];
			}
			string[] rolesNamesForUser;
			using (MembershipEntities membershipEntities = ModelHelper.CreateMembershipEntities(this.ConnectionString))
			{
				rolesNamesForUser = QueryHelper.GetRolesNamesForUser(membershipEntities, this.ApplicationName, username);
			}
			return rolesNamesForUser;
		}
		public override string[] GetUsersInRole(string roleName)
		{
			bool checkForNull = true;
			bool checkIfEmpty = true;
			bool checkForCommas = true;
			int maxSize = 256;
			string paramName = "roleName";
			Exception ex = ValidationHelper.CheckParameter(ref roleName, checkForNull, checkIfEmpty, checkForCommas, maxSize, paramName);
			if (ex != null)
			{
				throw ex;
			}
			string[] userNamesInRole;
			using (MembershipEntities membershipEntities = ModelHelper.CreateMembershipEntities(this.ConnectionString))
			{
				userNamesInRole = QueryHelper.GetUserNamesInRole(membershipEntities, this.ApplicationName, roleName, "");
			}
			return userNamesInRole;
		}
		public override void Initialize(string name, NameValueCollection config)
		{
			if (config == null)
			{
				throw new ArgumentNullException("config");
			}
			if (string.IsNullOrEmpty(name))
			{
				name = "DefaultRoleProvider";
			}

            string connectionStringName = config["connectionStringName"];
            this.ConnectionString = ModelHelper.GetConnectionString(connectionStringName);
			config.Remove("connectionStringName");

            // check that there isn't an azure config that overrides this
            string extra = ConceptCave.Configuration.ButterflyEnvironment.Current.GetConfigurationSettingValue(connectionStringName);
            if (string.IsNullOrEmpty(extra) == false)
            {
                this.ConnectionString = new ConnectionStringSettings(this.ConnectionString.Name, extra, this.ConnectionString.ProviderName);
            }

			if (string.IsNullOrEmpty(config["description"]))
			{
				config.Remove("description");
				config.Add("description", string.Format(CultureInfo.CurrentCulture, ProviderResources.RoleProvider_description, new object[0]));
			}
			base.Initialize(name, config);
			if (!string.IsNullOrEmpty(config["applicationName"]))
			{
				this.ApplicationName = config["applicationName"];
			}
			else
			{
				this.ApplicationName = ModelHelper.GetDefaultAppName();
			}
			config.Remove("applicationName");
			if (config.Count > 0)
			{
				string key = config.GetKey(0);
				if (!string.IsNullOrEmpty(key))
				{
					throw new ProviderException(string.Format(CultureInfo.CurrentCulture, ProviderResources.Provider_unrecognized_attribute, new object[]
					{
						key
					}));
				}
			}
		}
		public override bool IsUserInRole(string username, string roleName)
		{
			bool checkForNull = true;
			bool checkIfEmpty = false;
			bool checkForCommas = true;
			int maxSize = 256;
			string paramName = "username";
			Exception ex = ValidationHelper.CheckParameter(ref username, checkForNull, checkIfEmpty, checkForCommas, maxSize, paramName);
			if (ex != null)
			{
				throw ex;
			}
			bool checkForNull2 = true;
			bool checkIfEmpty2 = true;
			bool checkForCommas2 = true;
			int maxSize2 = 256;
			string paramName2 = "roleName";
			ex = ValidationHelper.CheckParameter(ref roleName, checkForNull2, checkIfEmpty2, checkForCommas2, maxSize2, paramName2);
			if (ex != null)
			{
				throw ex;
			}
			if (username.Length < 1)
			{
				return false;
			}
			bool result;
			using (MembershipEntities membershipEntities = ModelHelper.CreateMembershipEntities(this.ConnectionString))
			{
				result = (QueryHelper.GetUserInRole(membershipEntities, this.ApplicationName, roleName, username) != null);
			}
			return result;
		}
		public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
		{
			if (usernames == null)
			{
				throw new ArgumentNullException("usernames");
			}
			if (roleNames == null)
			{
				throw new ArgumentNullException("roleNames");
			}
			using (MembershipEntities membershipEntities = ModelHelper.CreateMembershipEntities(this.ConnectionString))
			{
				if (QueryHelper.GetApplication(membershipEntities, this.ApplicationName) == null)
				{
					Application application = ModelHelper.CreateApplication(membershipEntities, this.ApplicationName);
				}
				for (int i = 0; i < usernames.Length; i++)
				{
					string text = usernames[i];
					if (QueryHelper.GetUser(membershipEntities, text, this.ApplicationName) == null)
					{
						throw new ProviderException(string.Format(CultureInfo.CurrentCulture, ProviderResources.Provider_this_user_not_found, new object[]
						{
							text
						}));
					}
				}
				for (int j = 0; j < roleNames.Length; j++)
				{
					string text2 = roleNames[j];
					if (QueryHelper.GetRole(membershipEntities, text2, this.ApplicationName) == null)
					{
						throw new ProviderException(string.Format(CultureInfo.CurrentCulture, ProviderResources.Provider_role_not_found, new object[]
						{
							text2
						}));
					}
				}
				for (int k = 0; k < usernames.Length; k++)
				{
					string text3 = usernames[k];
					for (int l = 0; l < roleNames.Length; l++)
					{
						string text4 = roleNames[l];
						UsersInRole userInRole = QueryHelper.GetUserInRole(membershipEntities, this.ApplicationName, text4, text3);
						if (userInRole == null)
						{
							throw new ProviderException(string.Format(CultureInfo.CurrentCulture, ProviderResources.Provider_this_user_already_not_in_role, new object[]
							{
								text3,
								text4
							}));
						}
						membershipEntities.UsersInRoles.DeleteObject(userInRole);
					}
				}
				membershipEntities.SaveChanges();
			}
		}
		public override bool RoleExists(string roleName)
		{
			bool checkForNull = true;
			bool checkIfEmpty = true;
			bool checkForCommas = true;
			int maxSize = 256;
			string paramName = "roleName";
			Exception ex = ValidationHelper.CheckParameter(ref roleName, checkForNull, checkIfEmpty, checkForCommas, maxSize, paramName);
			if (ex != null)
			{
				throw ex;
			}
			bool result;
			using (MembershipEntities membershipEntities = ModelHelper.CreateMembershipEntities(this.ConnectionString))
			{
				RoleEntity role = QueryHelper.GetRole(membershipEntities, roleName, this.ApplicationName);
				result = (role != null);
			}
			return result;
		}
	}
}
