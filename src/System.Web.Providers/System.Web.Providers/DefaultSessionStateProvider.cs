using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Security;
using System.Threading;
using System.Web.Configuration;
using System.Web.Providers.Entities;
using System.Web.Providers.Resources;
using System.Web.SessionState;
namespace System.Web.Providers
{
	public class DefaultSessionStateProvider : SessionStateStoreProviderBase
	{
		private const int ITEM_SHORT_LENGTH = 7000;
		private const double SessionExpiresFrequencyCheckInSeconds = 30.0;
		private long _lastSessionPurgeTicks;
        public static Func<ConnectionStringSettings> FnGetConnectionStringSettings { get; set; }

        private ConnectionStringSettings _connectionString;
        private ConnectionStringSettings ConnectionString
        {
            get
            {
                if (FnGetConnectionStringSettings != null)
                    return FnGetConnectionStringSettings();
                return _connectionString;
            }
            set
            {
                _connectionString = value;
            }
        }
        private bool CompressionEnabled
		{
			get;
			set;
		}
		internal long LastSessionPurgeTicks
		{
			get
			{
				return Interlocked.Read(ref this._lastSessionPurgeTicks);
			}
			set
			{
				Interlocked.Exchange(ref this._lastSessionPurgeTicks, value);
			}
		}
		private static string AppendAppId(string id)
		{
			if (id.EndsWith(HttpRuntime.AppDomainAppId, StringComparison.OrdinalIgnoreCase))
			{
				return id;
			}
			return id + HttpRuntime.AppDomainAppId;
		}
		public override void Initialize(string name, NameValueCollection config)
		{
			if (config == null)
			{
				throw new ArgumentNullException("config");
			}
			if (string.IsNullOrEmpty(name))
			{
				name = "DefaultSessionStateProvider";
			}
			base.Initialize(name, config);

            string connectionStringName = config["connectionStringName"];
            this.ConnectionString = ModelHelper.GetConnectionString(connectionStringName);
			config.Remove("connectionStringName");

            // check that there isn't an azure config that overrides this
            string extra = ConceptCave.Configuration.ButterflyEnvironment.Current.GetConfigurationSettingValue(connectionStringName);
            if (string.IsNullOrEmpty(extra) == false)
            {
                this.ConnectionString = new ConnectionStringSettings(this.ConnectionString.Name, extra, this.ConnectionString.ProviderName);
            }

			try
			{
				SessionStateSection sessionStateSection = (SessionStateSection)ConfigurationManager.GetSection("system.web/sessionState");
				this.CompressionEnabled = sessionStateSection.CompressionEnabled;
			}
			catch (SecurityException)
			{
			}
		}
		public override SessionStateStoreData CreateNewStoreData(HttpContext context, int timeout)
		{
			HttpStaticObjectsCollection staticObjects = null;
			if (context != null)
			{
				staticObjects = SessionStateUtility.GetSessionStaticObjects(context);
			}
			return new SessionStateStoreData(new SessionStateItemCollection(), staticObjects, timeout);
		}
		private static SessionEntity NewSession(string id, int timeout)
		{
			SessionEntity sessionEntity = new SessionEntity();
			DateTime utcNow = DateTime.UtcNow;
			sessionEntity.Created = utcNow;
			sessionEntity.SessionId = id;
			sessionEntity.Timeout = timeout;
			sessionEntity.Expires = utcNow.AddMinutes((double)timeout);
			sessionEntity.Locked = false;
			sessionEntity.LockDate = utcNow;
			sessionEntity.LockCookie = 0;
			sessionEntity.Flags = 0;
			return sessionEntity;
		}
		public override void CreateUninitializedItem(HttpContext context, string id, int timeout)
		{
			if (id == null)
			{
				throw new ArgumentNullException("id");
			}
			if (id.Length > SessionIDManager.SessionIDMaxLength)
			{
				throw new ArgumentException(ProviderResources.Session_id_too_long);
			}
			id = DefaultSessionStateProvider.AppendAppId(id);
			using (SessionEntities sessionEntities = ModelHelper.CreateSessionEntities(this.ConnectionString))
			{
				if (QueryHelper.GetSession(sessionEntities, id) == null)
				{
					SessionEntity sessionEntity = DefaultSessionStateProvider.NewSession(id, timeout);
					SessionStateStoreData item = new SessionStateStoreData(new SessionStateItemCollection(), SessionStateUtility.GetSessionStaticObjects(context), sessionEntity.Timeout);
					DefaultSessionStateProvider.SaveItemToSession(sessionEntity, item, this.CompressionEnabled);
					sessionEntities.Sessions.AddObject(sessionEntity);
					sessionEntities.SaveChanges();
				}
			}
		}
		public override void Dispose()
		{
		}
		public override void EndRequest(HttpContext context)
		{
		}
		private void PurgeExpiredSessions()
		{
			using (SessionEntities sessionEntities = ModelHelper.CreateSessionEntities(this.ConnectionString))
			{
				IQueryable<SessionEntity> expiredSessions = QueryHelper.GetExpiredSessions(sessionEntities);
				foreach (SessionEntity current in expiredSessions)
				{
					sessionEntities.DeleteObject(current);
				}
				sessionEntities.SaveChanges();
				this.LastSessionPurgeTicks = DateTime.UtcNow.Ticks;
			}
		}
		internal bool CanPurge()
		{
			long ticks = DateTime.UtcNow.Ticks;
			return TimeSpan.FromTicks(ticks - this.LastSessionPurgeTicks).TotalSeconds > 30.0;
		}
		private void PurgeIfNeeded()
		{
			if (this.CanPurge())
			{
				this.PurgeExpiredSessions();
			}
		}
		private static SessionStateStoreData InitializeSessionItem(HttpContext context, SessionEntity session, bool compression)
		{
			SessionStateStoreData sessionStateStoreData = new SessionStateStoreData(new SessionStateItemCollection(), SessionStateUtility.GetSessionStaticObjects(context), session.Timeout);
			DefaultSessionStateProvider.SaveItemToSession(session, sessionStateStoreData, compression);
			session.Flags = 0;
			return sessionStateStoreData;
		}
		private SessionStateStoreData DoGet(HttpContext context, string id, bool exclusive, out bool locked, out TimeSpan lockAge, out object lockId, out SessionStateActions actions)
		{
			if (id.Length > SessionIDManager.SessionIDMaxLength)
			{
				throw new ArgumentException(ProviderResources.Session_id_too_long);
			}
			id = DefaultSessionStateProvider.AppendAppId(id);
			locked = false;
			lockAge = TimeSpan.Zero;
			lockId = null;
			actions = SessionStateActions.None;
			SessionStateStoreData result;
			using (SessionEntities sessionEntities = ModelHelper.CreateSessionEntities(this.ConnectionString))
			{
				DateTime utcNow = DateTime.UtcNow;
				SessionEntity session = QueryHelper.GetSession(sessionEntities, id);
				if (session != null && session.Expires > utcNow)
				{
					session.Expires = DateTime.UtcNow.AddMinutes((double)session.Timeout);
					locked = session.Locked;
					lockId = session.LockCookie;
					SessionStateStoreData sessionStateStoreData = null;
					if (locked)
					{
						lockAge = TimeSpan.FromSeconds((double)(DateTime.UtcNow - session.LockDate).Seconds);
					}
					else
					{
						if (exclusive)
						{
							session.Locked = true;
							session.LockDate = DateTime.UtcNow;
						}
						byte[] sessionItem = session.SessionItem;
						if (session.Flags == 1)
						{
							sessionStateStoreData = DefaultSessionStateProvider.InitializeSessionItem(context, session, this.CompressionEnabled);
						}
						else
						{
							using (MemoryStream memoryStream = new MemoryStream(sessionItem))
							{
								sessionStateStoreData = DefaultSessionStateProvider.DeserializeStoreData(context, memoryStream, this.CompressionEnabled);
							}
						}
					}
					int num = sessionEntities.SaveChanges();
					num++;
					result = sessionStateStoreData;
				}
				else
				{
					result = null;
				}
			}
			return result;
		}
		public override SessionStateStoreData GetItem(HttpContext context, string id, out bool locked, out TimeSpan lockAge, out object lockId, out SessionStateActions actions)
		{
			return this.DoGet(context, id, false, out locked, out lockAge, out lockId, out actions);
		}
		public override SessionStateStoreData GetItemExclusive(HttpContext context, string id, out bool locked, out TimeSpan lockAge, out object lockId, out SessionStateActions actions)
		{
			return this.DoGet(context, id, true, out locked, out lockAge, out lockId, out actions);
		}
		public override void InitializeRequest(HttpContext context)
		{
			this.PurgeIfNeeded();
		}
		private static void ReleaseItemNoSave(SessionEntities ctx, string id, object lockId)
		{
			id = DefaultSessionStateProvider.AppendAppId(id);
			SessionEntity session = QueryHelper.GetSession(ctx, id);
			if (session != null && session.Locked && session.LockCookie == (int)lockId)
			{
				session.Locked = false;
			}
		}
		public override void ReleaseItemExclusive(HttpContext context, string id, object lockId)
		{
			if (id == null)
			{
				throw new ArgumentNullException("id");
			}
			if (id.Length > SessionIDManager.SessionIDMaxLength)
			{
				throw new ArgumentException(ProviderResources.Session_id_too_long);
			}
			using (SessionEntities sessionEntities = ModelHelper.CreateSessionEntities(this.ConnectionString))
			{
				DefaultSessionStateProvider.ReleaseItemNoSave(sessionEntities, id, lockId);
				sessionEntities.SaveChanges();
			}
		}
		public override void RemoveItem(HttpContext context, string id, object lockId, SessionStateStoreData item)
		{
			if (id == null)
			{
				throw new ArgumentNullException("id");
			}
			if (id.Length > SessionIDManager.SessionIDMaxLength)
			{
				throw new ArgumentException(ProviderResources.Session_id_too_long);
			}
			id = DefaultSessionStateProvider.AppendAppId(id);
			using (SessionEntities sessionEntities = ModelHelper.CreateSessionEntities(this.ConnectionString))
			{
				SessionEntity session = QueryHelper.GetSession(sessionEntities, id);
				if (session != null && session.LockCookie == (int)lockId)
				{
					sessionEntities.DeleteObject(session);
					sessionEntities.SaveChanges();
				}
			}
		}
		public override void ResetItemTimeout(HttpContext context, string id)
		{
			if (id == null)
			{
				throw new ArgumentNullException("id");
			}
			if (id.Length > SessionIDManager.SessionIDMaxLength)
			{
				throw new ArgumentException(ProviderResources.Session_id_too_long);
			}
			id = DefaultSessionStateProvider.AppendAppId(id);
			using (SessionEntities sessionEntities = ModelHelper.CreateSessionEntities(this.ConnectionString))
			{
				SessionEntity session = QueryHelper.GetSession(sessionEntities, id);
				if (session != null)
				{
					session.Expires = DateTime.UtcNow.AddMinutes((double)session.Timeout);
					sessionEntities.SaveChanges();
				}
			}
		}
		private static void SaveItemToSession(SessionEntity session, SessionStateStoreData item, bool compression)
		{
			byte[] sessionItem = null;
			int num = 0;
			DefaultSessionStateProvider.SerializeStoreData(item, 7000, out sessionItem, out num, compression);
			session.SessionItem = sessionItem;
		}
		private static SessionStateStoreData DeserializeStoreData(HttpContext context, Stream stream, bool compressionEnabled)
		{
			if (compressionEnabled)
			{
				using (DeflateStream deflateStream = new DeflateStream(stream, CompressionMode.Decompress, true))
				{
					return DefaultSessionStateProvider.Deserialize(context, deflateStream);
				}
			}
			return DefaultSessionStateProvider.Deserialize(context, stream);
		}
		private static void Serialize(SessionStateStoreData item, Stream stream)
		{
			bool flag = true;
			bool flag2 = true;
			BinaryWriter binaryWriter = new BinaryWriter(stream);
			binaryWriter.Write(item.Timeout);
			if (item.Items == null || item.Items.Count == 0)
			{
				flag = false;
			}
			binaryWriter.Write(flag);
			if (item.StaticObjects == null || item.StaticObjects.NeverAccessed)
			{
				flag2 = false;
			}
			binaryWriter.Write(flag2);
			if (flag)
			{
				((SessionStateItemCollection)item.Items).Serialize(binaryWriter);
			}
			if (flag2)
			{
				item.StaticObjects.Serialize(binaryWriter);
			}
			binaryWriter.Write(255);
		}
		private static SessionStateStoreData Deserialize(HttpContext context, Stream stream)
		{
			int timeout;
			SessionStateItemCollection sessionItems;
			HttpStaticObjectsCollection staticObjects;
			try
			{
				BinaryReader binaryReader = new BinaryReader(stream);
				timeout = binaryReader.ReadInt32();
				bool flag = binaryReader.ReadBoolean();
				bool flag2 = binaryReader.ReadBoolean();
				if (flag)
				{
					sessionItems = SessionStateItemCollection.Deserialize(binaryReader);
				}
				else
				{
					sessionItems = new SessionStateItemCollection();
				}
				if (flag2)
				{
					staticObjects = HttpStaticObjectsCollection.Deserialize(binaryReader);
				}
				else
				{
					staticObjects = SessionStateUtility.GetSessionStaticObjects(context);
				}
				byte b = binaryReader.ReadByte();
				if (b != 255)
				{
					throw new HttpException(string.Format(CultureInfo.CurrentCulture, ProviderResources.Invalid_session_state, new object[0]));
				}
			}
			catch (EndOfStreamException)
			{
				throw new HttpException(string.Format(CultureInfo.CurrentCulture, ProviderResources.Invalid_session_state, new object[0]));
			}
			return new SessionStateStoreData(sessionItems, staticObjects, timeout);
		}
		private static void SerializeStoreData(SessionStateStoreData item, int initialStreamSize, out byte[] buf, out int length, bool compressionEnabled)
		{
			using (MemoryStream memoryStream = new MemoryStream(initialStreamSize))
			{
				DefaultSessionStateProvider.Serialize(item, memoryStream);
				if (compressionEnabled)
				{
					byte[] buffer = memoryStream.GetBuffer();
					int count = (int)memoryStream.Length;
					memoryStream.SetLength(0L);
					using (DeflateStream deflateStream = new DeflateStream(memoryStream, CompressionMode.Compress, true))
					{
						deflateStream.Write(buffer, 0, count);
					}
					memoryStream.WriteByte(255);
				}
				buf = memoryStream.GetBuffer();
				length = (int)memoryStream.Length;
			}
		}
		public override void SetAndReleaseItemExclusive(HttpContext context, string id, SessionStateStoreData item, object lockId, bool newItem)
		{
			if (item == null)
			{
				throw new ArgumentNullException("item");
			}
			if (id == null)
			{
				throw new ArgumentNullException("id");
			}
			if (id.Length > SessionIDManager.SessionIDMaxLength)
			{
				throw new ArgumentException(ProviderResources.Session_id_too_long);
			}
			id = DefaultSessionStateProvider.AppendAppId(id);
			using (SessionEntities sessionEntities = ModelHelper.CreateSessionEntities(this.ConnectionString))
			{
				SessionEntity sessionEntity = QueryHelper.GetSession(sessionEntities, id);
				if (sessionEntity == null)
				{
					if (newItem)
					{
						sessionEntity = DefaultSessionStateProvider.NewSession(id, item.Timeout);
						sessionEntities.Sessions.AddObject(sessionEntity);
					}
					else
					{
						if (sessionEntity == null)
						{
							throw new InvalidOperationException(ProviderResources.Session_not_found);
						}
					}
				}
				else
				{
					if (lockId == null)
					{
						sessionEntity.LockCookie = 0;
					}
					else
					{
						sessionEntity.LockCookie = (int)lockId;
					}
					sessionEntity.Locked = false;
					sessionEntity.Timeout = item.Timeout;
				}
				DefaultSessionStateProvider.SaveItemToSession(sessionEntity, item, this.CompressionEnabled);
				sessionEntities.SaveChanges();
			}
		}
		public override bool SetItemExpireCallback(SessionStateItemExpireCallback expireCallback)
		{
			return false;
		}
	}
}
