using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Configuration.Provider;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Providers.Entities;
using System.Web.Providers.Resources;
using System.Web.Security;
namespace System.Web.Providers
{
	public class DefaultMembershipProvider : MembershipProvider
	{
		internal const int SALT_SIZE = 16;
		internal const int MaxPasswordSize = 128;
		internal const int MaxSimpleStringSize = 256;
		private Regex _PasswordStrengthRegEx;
		internal static DateTime NullDate = new DateTime(1754, 1, 1);
		private string _HashAlgorithm;
		private string ProviderName
		{
			get
			{
				return this.Name;
			}
		}
		internal bool EnablePasswordResetInternal
		{
			get;
			set;
		}
		internal bool EnablePasswordRetrievalInternal
		{
			get;
			set;
		}
		internal int MaxInvalidPasswordAttemptsInternal
		{
			get;
			set;
		}
		internal int MinRequiredNonAlphanumericCharactersInternal
		{
			get;
			set;
		}
		internal int MinRequiredPasswordLengthInternal
		{
			get;
			set;
		}
		internal int PasswordAttemptWindowInternal
		{
			get;
			set;
		}
		internal MembershipPasswordFormat PasswordFormatInternal
		{
			get;
			set;
		}
		internal string PasswordStrengthRegularExpressionInternal
		{
			get;
			set;
		}
		internal bool RequiresQuestionAndAnswerInternal
		{
			get;
			set;
		}
		internal bool RequiresUniqueEmailInternal
		{
			get;
			set;
		}
		public override string ApplicationName
		{
			get;
			set;
		}
        public static Func<ConnectionStringSettings> FnGetConnectionStringSettings { get; set; }

        private ConnectionStringSettings _connectionString;
		private ConnectionStringSettings ConnectionString
		{
            get
            {
                if (FnGetConnectionStringSettings != null)
                    return FnGetConnectionStringSettings();
                return _connectionString;
            }
            set
            {
                _connectionString = value;
            }
		}
		public override bool EnablePasswordReset
		{
			get
			{
				return this.EnablePasswordResetInternal;
			}
		}
		public override bool EnablePasswordRetrieval
		{
			get
			{
				return this.EnablePasswordRetrievalInternal;
			}
		}
		public override int MaxInvalidPasswordAttempts
		{
			get
			{
				return this.MaxInvalidPasswordAttemptsInternal;
			}
		}
		public override int MinRequiredNonAlphanumericCharacters
		{
			get
			{
				return this.MinRequiredNonAlphanumericCharactersInternal;
			}
		}
		public override int MinRequiredPasswordLength
		{
			get
			{
				return this.MinRequiredPasswordLengthInternal;
			}
		}
		public override int PasswordAttemptWindow
		{
			get
			{
				return this.PasswordAttemptWindowInternal;
			}
		}
		public override MembershipPasswordFormat PasswordFormat
		{
			get
			{
				return this.PasswordFormatInternal;
			}
		}
		public override string PasswordStrengthRegularExpression
		{
			get
			{
				return this.PasswordStrengthRegularExpressionInternal;
			}
		}
		public override bool RequiresQuestionAndAnswer
		{
			get
			{
				return this.RequiresQuestionAndAnswerInternal;
			}
		}
		public override bool RequiresUniqueEmail
		{
			get
			{
				return this.RequiresUniqueEmailInternal;
			}
		}
		public override void Initialize(string name, NameValueCollection configs)
		{
			if (configs == null)
			{
				throw new ArgumentNullException("config");
			}

            // we merge the configs handed in, with those returned from the overrides function (from the db)
            var config = new NameValueCollection();

            var overrides = ConceptCave.Configuration.ButterflyEnvironment.Current.GetConfigOverrides("System.Web.Providers.MembershipProvider");

            if (overrides == null)
            {
                overrides = new NameValueCollection();
            }

            foreach (var c in configs.AllKeys)
            {
                if(overrides.AllKeys.Contains(c))
                {
                    config.Add(c, overrides[c]);
                    continue;
                }

                config.Add(c, configs[c]);
            }

            if (string.IsNullOrEmpty(name))
			{
				name = "DefaultMembershipProvider";
			}
			if (string.IsNullOrEmpty(config["description"]))
			{
				config.Remove("description");
				config.Add("description", ProviderResources.MembershipProvider_description);
			}
			base.Initialize(name, config);
			if (!string.IsNullOrEmpty(config["applicationName"]))
			{
				this.ApplicationName = config["applicationName"];
			}
			else
			{
				this.ApplicationName = ModelHelper.GetDefaultAppName();
			}

            string connectionStringName = config["connectionStringName"];

            this.ConnectionString = ModelHelper.GetConnectionString(connectionStringName);
            config.Remove("connectionStringName");

            // check that there isn't an azure config that overrides this
            string extra = ConceptCave.Configuration.ButterflyEnvironment.Current.GetConfigurationSettingValue(connectionStringName);
            if (string.IsNullOrEmpty(extra) == false)
            {
                this.ConnectionString = new ConnectionStringSettings(this.ConnectionString.Name, extra, this.ConnectionString.ProviderName);
            }

			if (config["enablePasswordReset"] != null)
			{
				this.EnablePasswordResetInternal = Convert.ToBoolean(config["enablePasswordReset"], CultureInfo.InvariantCulture);
			}
			else
			{
				this.EnablePasswordResetInternal = true;
			}
			if (config["enablePasswordRetrieval"] != null)
			{
				this.EnablePasswordRetrievalInternal = Convert.ToBoolean(config["enablePasswordRetrieval"], CultureInfo.InvariantCulture);
			}
			else
			{
				this.EnablePasswordRetrievalInternal = true;
			}
			if (config["maxInvalidPasswordAttempts"] != null)
			{
				this.MaxInvalidPasswordAttemptsInternal = Convert.ToInt32(config["maxInvalidPasswordAttempts"], CultureInfo.InvariantCulture);
			}
			else
			{
				this.MaxInvalidPasswordAttemptsInternal = 5;
			}
			if (config["minRequiredNonalphanumericCharacters"] != null)
			{
				this.MinRequiredNonAlphanumericCharactersInternal = Convert.ToInt32(config["minRequiredNonalphanumericCharacters"], CultureInfo.InvariantCulture);
			}
			else
			{
				this.MinRequiredNonAlphanumericCharactersInternal = 1;
			}
			if (config["minRequiredPasswordLength"] != null)
			{
				this.MinRequiredPasswordLengthInternal = Convert.ToInt32(config["minRequiredPasswordLength"], CultureInfo.InvariantCulture);
			}
			else
			{
				this.MinRequiredPasswordLengthInternal = 7;
			}
			if (config["passwordAttemptWindow"] != null)
			{
				this.PasswordAttemptWindowInternal = Convert.ToInt32(config["passwordAttemptWindow"], CultureInfo.InvariantCulture);
			}
			else
			{
				this.PasswordAttemptWindowInternal = 10;
			}
			if (config["passwordFormat"] != null)
			{
				this.PasswordFormatInternal = (MembershipPasswordFormat)Enum.Parse(typeof(MembershipPasswordFormat), config["passwordFormat"]);
			}
			else
			{
				this.PasswordFormatInternal = MembershipPasswordFormat.Hashed;
			}
			if (config["passwordStrengthRegularExpression"] != null)
			{
				this.PasswordStrengthRegularExpressionInternal = config["passwordStrengthRegularExpression"];
				try
				{
					this._PasswordStrengthRegEx = new Regex(this.PasswordStrengthRegularExpressionInternal);
					goto IL_24F;
				}
				catch (ArgumentException ex)
				{
					throw new ProviderException(ex.Message, ex);
				}
			}
			this.PasswordStrengthRegularExpressionInternal = string.Empty;
			IL_24F:
			if (config["requiresQuestionAndAnswer"] != null)
			{
				this.RequiresQuestionAndAnswerInternal = Convert.ToBoolean(config["requiresQuestionAndAnswer"], CultureInfo.InvariantCulture);
			}
			else
			{
				this.RequiresQuestionAndAnswerInternal = true;
			}
			if (config["requiresUniqueEmail"] != null)
			{
				this.RequiresUniqueEmailInternal = Convert.ToBoolean(config["requiresUniqueEmail"], CultureInfo.InvariantCulture);
			}
			else
			{
				this.RequiresUniqueEmailInternal = true;
			}
			if (this.PasswordFormat == MembershipPasswordFormat.Hashed && this.EnablePasswordRetrieval)
			{
				throw new ProviderException(ProviderResources.Provider_can_not_retrieve_hashed_password);
			}
			config.Remove("applicationName");
			config.Remove("enablePasswordReset");
			config.Remove("enablePasswordRetrieval");
			config.Remove("maxInvalidPasswordAttempts");
			config.Remove("minRequiredNonalphanumericCharacters");
			config.Remove("minRequiredPasswordLength");
			config.Remove("passwordAttemptWindow");
			config.Remove("passwordFormat");
			config.Remove("passwordStrengthRegularExpression");
			config.Remove("requiresQuestionAndAnswer");
			config.Remove("requiresUniqueEmail");
			if (config.Count > 0)
			{
				string key = config.GetKey(0);
				if (!string.IsNullOrEmpty(key))
				{
					throw new ProviderException(string.Format(CultureInfo.CurrentCulture, ProviderResources.Provider_unrecognized_attribute, new object[]
					{
						key
					}));
				}
			}
		}
		public override bool ChangePassword(string username, string oldPassword, string newPassword)
		{
			bool checkForNull = true;
			bool checkIfEmpty = true;
			bool checkForCommas = true;
			int maxSize = 256;
			string paramName = "username";
			Exception ex = ValidationHelper.CheckParameter(ref username, checkForNull, checkIfEmpty, checkForCommas, maxSize, paramName);
			if (ex != null)
			{
				throw ex;
			}
			bool checkForNull2 = true;
			bool checkIfEmpty2 = true;
			bool checkForCommas2 = false;
			int maxSize2 = 128;
			string paramName2 = "oldPassword";
			ex = ValidationHelper.CheckParameter(ref oldPassword, checkForNull2, checkIfEmpty2, checkForCommas2, maxSize2, paramName2);
			if (ex != null)
			{
				throw ex;
			}
			bool checkForNull3 = true;
			bool checkIfEmpty3 = true;
			bool checkForCommas3 = false;
			int maxSize3 = 128;
			string paramName3 = "newPassword";
			ex = ValidationHelper.CheckParameter(ref newPassword, checkForNull3, checkIfEmpty3, checkForCommas3, maxSize3, paramName3);
			if (ex != null)
			{
				throw ex;
			}
			string text;
			int passwordFormat;
			if (!this.CheckPassword(username, oldPassword, false, false, out text, out passwordFormat))
			{
				return false;
			}
			if (newPassword.Length < this.MinRequiredPasswordLength)
			{
				throw new ArgumentException(string.Format(CultureInfo.CurrentCulture, ProviderResources.Password_too_short, new object[]
				{
					"newPassword",
					this.MinRequiredPasswordLength.ToString(CultureInfo.InvariantCulture)
				}));
			}
			if (this.MinRequiredNonAlphanumericCharacters > 0)
			{
				int num = 0;
				for (int i = 0; i < newPassword.Length; i++)
				{
					if (!char.IsLetterOrDigit(newPassword[i]))
					{
						num++;
					}
				}
				if (num < this.MinRequiredNonAlphanumericCharacters)
				{
					throw new ArgumentException(string.Format(CultureInfo.CurrentCulture, ProviderResources.Password_need_more_non_alpha_numeric_chars, new object[]
					{
						"newPassword",
						this.MinRequiredNonAlphanumericCharacters.ToString(CultureInfo.InvariantCulture)
					}));
				}
			}
			if (this._PasswordStrengthRegEx != null && !this._PasswordStrengthRegEx.IsMatch(newPassword))
			{
				throw new ArgumentException(string.Format(CultureInfo.CurrentCulture, ProviderResources.Password_does_not_match_regular_expression, new object[]
				{
					"newPassword"
				}));
			}
			ValidatePasswordEventArgs validatePasswordEventArgs = new ValidatePasswordEventArgs(username, newPassword, true);
			this.OnValidatingPassword(validatePasswordEventArgs);
			if (!validatePasswordEventArgs.Cancel)
			{
				string password = this.EncodePassword(newPassword, passwordFormat, text);
				bool result;
				using (MembershipEntities membershipEntities = ModelHelper.CreateMembershipEntities(this.ConnectionString))
				{
					MembershipEntity membership = QueryHelper.GetMembership(membershipEntities, this.ApplicationName, username);
					if (membership == null)
					{
						result = false;
					}
					else
					{
						membership.Password = password;
						membership.PasswordSalt = text;
						membership.LastPasswordChangedDate = DateTime.UtcNow;
						membershipEntities.SaveChanges();
						result = true;
					}
				}
				return result;
			}
			if (validatePasswordEventArgs.FailureInformation != null)
			{
				throw validatePasswordEventArgs.FailureInformation;
			}
			throw new ArgumentException(string.Format(CultureInfo.CurrentCulture, ProviderResources.Membership_Custom_Password_Validation_Failure, new object[0]), "newPassword");
		}
		public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer)
		{
			bool checkForNull = true;
			bool checkIfEmpty = true;
			bool checkForCommas = true;
			int maxSize = 256;
			string paramName = "username";
			Exception ex = ValidationHelper.CheckParameter(ref username, checkForNull, checkIfEmpty, checkForCommas, maxSize, paramName);
			if (ex != null)
			{
				throw ex;
			}
			bool checkForNull2 = true;
			bool checkIfEmpty2 = true;
			bool checkForCommas2 = false;
			int maxSize2 = 128;
			string paramName2 = "password";
			ex = ValidationHelper.CheckParameter(ref password, checkForNull2, checkIfEmpty2, checkForCommas2, maxSize2, paramName2);
			if (ex != null)
			{
				throw ex;
			}
			string salt;
			int passwordFormat;
			if (!this.CheckPassword(username, password, false, false, out salt, out passwordFormat))
			{
				return false;
			}
			bool requiresQuestionAndAnswer = this.RequiresQuestionAndAnswer;
			bool requiresQuestionAndAnswer2 = this.RequiresQuestionAndAnswer;
			bool checkForCommas3 = false;
			int maxSize3 = 256;
			string paramName3 = "newPasswordQuestion";
			ex = ValidationHelper.CheckParameter(ref newPasswordQuestion, requiresQuestionAndAnswer, requiresQuestionAndAnswer2, checkForCommas3, maxSize3, paramName3);
			if (ex != null)
			{
				throw ex;
			}
			bool requiresQuestionAndAnswer3 = this.RequiresQuestionAndAnswer;
			bool requiresQuestionAndAnswer4 = this.RequiresQuestionAndAnswer;
			bool checkForCommas4 = false;
			int maxSize4 = 128;
			string paramName4 = "newPasswordAnswer";
			ex = ValidationHelper.CheckParameter(ref newPasswordAnswer, requiresQuestionAndAnswer3, requiresQuestionAndAnswer4, checkForCommas4, maxSize4, paramName4);
			if (ex != null)
			{
				throw ex;
			}
			string passwordAnswer;
			if (!string.IsNullOrEmpty(newPasswordAnswer))
			{
				passwordAnswer = this.EncodePassword(newPasswordAnswer, passwordFormat, salt);
			}
			else
			{
				passwordAnswer = newPasswordAnswer;
			}
			bool requiresQuestionAndAnswer5 = this.RequiresQuestionAndAnswer;
			bool requiresQuestionAndAnswer6 = this.RequiresQuestionAndAnswer;
			bool checkForCommas5 = false;
			int maxSize5 = 128;
			string paramName5 = "newPasswordAnswer";
			ex = ValidationHelper.CheckParameter(ref passwordAnswer, requiresQuestionAndAnswer5, requiresQuestionAndAnswer6, checkForCommas5, maxSize5, paramName5);
			if (ex != null)
			{
				throw ex;
			}
			bool result;
			using (MembershipEntities membershipEntities = ModelHelper.CreateMembershipEntities(this.ConnectionString))
			{
				MembershipEntity membership = QueryHelper.GetMembership(membershipEntities, this.ApplicationName, username);
				if (membership == null)
				{
					result = false;
				}
				else
				{
					membership.PasswordQuestion = newPasswordQuestion;
					membership.PasswordAnswer = passwordAnswer;
					membershipEntities.SaveChanges();
					result = true;
				}
			}
			return result;
		}
		public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status)
		{
			string salt = DefaultMembershipProvider.GenerateSalt();
			bool checkForNull = true;
			bool checkIfEmpty = true;
			bool checkForCommas = false;
			int maxSize = 128;
			if (!ValidationHelper.ValidateParameter(ref password, checkForNull, checkIfEmpty, checkForCommas, maxSize))
			{
				status = MembershipCreateStatus.InvalidPassword;
				return null;
			}
			string password2 = this.EncodePassword(password, (int)this.PasswordFormat, salt);
			if (passwordAnswer != null)
			{
				passwordAnswer = passwordAnswer.Trim();
			}
			string passwordAnswer2;
			if (!string.IsNullOrEmpty(passwordAnswer))
			{
				passwordAnswer2 = this.EncodePassword(passwordAnswer, (int)this.PasswordFormat, salt);
			}
			else
			{
				passwordAnswer2 = passwordAnswer;
			}
			bool requiresQuestionAndAnswer = this.RequiresQuestionAndAnswer;
			bool checkIfEmpty2 = true;
			bool checkForCommas2 = false;
			int maxSize2 = 128;
			if (!ValidationHelper.ValidateParameter(ref passwordAnswer2, requiresQuestionAndAnswer, checkIfEmpty2, checkForCommas2, maxSize2))
			{
				status = MembershipCreateStatus.InvalidAnswer;
				return null;
			}
			bool requiresQuestionAndAnswer2 = this.RequiresQuestionAndAnswer;
			bool checkIfEmpty3 = true;
			bool checkForCommas3 = false;
			int maxSize3 = 256;
			if (!ValidationHelper.ValidateParameter(ref passwordQuestion, requiresQuestionAndAnswer2, checkIfEmpty3, checkForCommas3, maxSize3))
			{
				status = MembershipCreateStatus.InvalidQuestion;
				return null;
			}
			bool checkForNull2 = true;
			bool checkIfEmpty4 = true;
			bool checkForCommas4 = true;
			int maxSize4 = 256;
			if (!ValidationHelper.ValidateParameter(ref username, checkForNull2, checkIfEmpty4, checkForCommas4, maxSize4))
			{
				status = MembershipCreateStatus.InvalidUserName;
				return null;
			}
			bool requiresUniqueEmail = this.RequiresUniqueEmail;
			bool requiresUniqueEmail2 = this.RequiresUniqueEmail;
			bool checkForCommas5 = false;
			int maxSize5 = 256;
			if (!ValidationHelper.ValidateParameter(ref email, requiresUniqueEmail, requiresUniqueEmail2, checkForCommas5, maxSize5))
			{
				status = MembershipCreateStatus.InvalidEmail;
				return null;
			}
			if (providerUserKey != null && !(providerUserKey is Guid))
			{
				status = MembershipCreateStatus.InvalidProviderUserKey;
				return null;
			}
			if (password == null || password.Length < this.MinRequiredPasswordLength)
			{
				status = MembershipCreateStatus.InvalidPassword;
				return null;
			}
			if (this.MinRequiredNonAlphanumericCharacters > 0)
			{
				int num = 0;
				for (int i = 0; i < password.Length; i++)
				{
					if (!char.IsLetterOrDigit(password[i]))
					{
						num++;
					}
				}
				if (num < this.MinRequiredNonAlphanumericCharacters)
				{
					status = MembershipCreateStatus.InvalidPassword;
					return null;
				}
			}
			if (this._PasswordStrengthRegEx != null && !this._PasswordStrengthRegEx.IsMatch(password))
			{
				status = MembershipCreateStatus.InvalidPassword;
				return null;
			}
			ValidatePasswordEventArgs validatePasswordEventArgs = new ValidatePasswordEventArgs(username, password, true);
			this.OnValidatingPassword(validatePasswordEventArgs);
			if (validatePasswordEventArgs.Cancel)
			{
				status = MembershipCreateStatus.InvalidPassword;
				return null;
			}
			DateTime dateTime;
			int num2 = this.Membership_CreateUser(this.ApplicationName, username, password2, salt, email, passwordQuestion, passwordAnswer2, isApproved, out dateTime, this.RequiresUniqueEmail, (int)this.PasswordFormat, ref providerUserKey);
			if (num2 < 0 || num2 > 11)
			{
				num2 = 11;
			}
			status = (MembershipCreateStatus)num2;
			if (status != 0)
			{
				return null;
			}
			return new MembershipUser(this.ProviderName, username, providerUserKey, email, passwordQuestion, null, isApproved, false, dateTime, dateTime, dateTime, dateTime, DefaultMembershipProvider.NullDate);
		}
		public override bool DeleteUser(string username, bool deleteAllRelatedData)
		{
			bool checkForNull = true;
			bool checkIfEmpty = true;
			bool checkForCommas = true;
			int maxSize = 256;
			string paramName = "username";
			Exception ex = ValidationHelper.CheckParameter(ref username, checkForNull, checkIfEmpty, checkForCommas, maxSize, paramName);
			if (ex != null)
			{
				throw ex;
			}
			bool result;
			using (MembershipEntities membershipEntities = ModelHelper.CreateMembershipEntities(this.ConnectionString))
			{
				MembershipEntity membership = QueryHelper.GetMembership(membershipEntities, this.ApplicationName, username);
				if (membership == null)
				{
					result = false;
				}
				else
				{
					membershipEntities.Memberships.DeleteObject(membership);
					if (deleteAllRelatedData)
					{
						IQueryable<UsersInRole> rolesForUser = QueryHelper.GetRolesForUser(membershipEntities, this.ApplicationName, username);
						foreach (UsersInRole current in rolesForUser)
						{
							membershipEntities.UsersInRoles.DeleteObject(current);
						}
						ProfileEntity profile = QueryHelper.GetProfile(membershipEntities, this.ApplicationName, username);
						if (profile != null)
						{
							membershipEntities.Profiles.DeleteObject(profile);
						}
						User user = QueryHelper.GetUser(membershipEntities, membership.UserId, this.ApplicationName);
						if (user != null)
						{
							membershipEntities.Users.DeleteObject(user);
						}
					}
					membershipEntities.SaveChanges();
					result = true;
				}
			}
			return result;
		}
		public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
		{
			bool requiresUniqueEmail = this.RequiresUniqueEmail;
			bool requiresUniqueEmail2 = this.RequiresUniqueEmail;
			bool checkForCommas = false;
			int maxSize = 256;
			string paramName = "emailToMatch";
			Exception ex = ValidationHelper.CheckParameter(ref emailToMatch, requiresUniqueEmail, requiresUniqueEmail2, checkForCommas, maxSize, paramName);
			if (ex != null)
			{
				throw ex;
			}
			MembershipUserCollection membershipUserCollection = new MembershipUserCollection();
			using (MembershipEntities membershipEntities = ModelHelper.CreateMembershipEntities(this.ConnectionString))
			{
				totalRecords = QueryHelper.GetAllMembershipUsersLikeEmail(membershipEntities, this.ApplicationName, emailToMatch, -1, -1).Count<DbDataRecord>();
				IQueryable<DbDataRecord> allMembershipUsersLikeEmail = QueryHelper.GetAllMembershipUsersLikeEmail(membershipEntities, this.ApplicationName, emailToMatch, pageIndex, pageSize);
				foreach (DbDataRecord current in allMembershipUsersLikeEmail)
				{
					membershipUserCollection.Add(QueryHelper.CreateMembershipUserFromDbRecord(membershipEntities, current, this.ProviderName, this.ApplicationName, false));
				}
			}
			return membershipUserCollection;
		}
		public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
		{
			bool checkForNull = true;
			bool checkIfEmpty = true;
			bool checkForCommas = true;
			int maxSize = 256;
			string paramName = "usernameToMatch";
			Exception ex = ValidationHelper.CheckParameter(ref usernameToMatch, checkForNull, checkIfEmpty, checkForCommas, maxSize, paramName);
			if (ex != null)
			{
				throw ex;
			}
			MembershipUserCollection membershipUserCollection = new MembershipUserCollection();
			using (MembershipEntities membershipEntities = ModelHelper.CreateMembershipEntities(this.ConnectionString))
			{
				totalRecords = QueryHelper.GetAllMembershipUsersLikeUserName(membershipEntities, this.ApplicationName, usernameToMatch, -1, -1).Count<DbDataRecord>();
				IQueryable<DbDataRecord> allMembershipUsersLikeUserName = QueryHelper.GetAllMembershipUsersLikeUserName(membershipEntities, this.ApplicationName, usernameToMatch, pageIndex, pageSize);
				foreach (DbDataRecord current in allMembershipUsersLikeUserName)
				{
					membershipUserCollection.Add(QueryHelper.CreateMembershipUserFromDbRecord(membershipEntities, current, this.ProviderName, this.ApplicationName, false));
				}
			}
			return membershipUserCollection;
		}
		public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
		{
			MembershipUserCollection membershipUserCollection = new MembershipUserCollection();
			using (MembershipEntities membershipEntities = ModelHelper.CreateMembershipEntities(this.ConnectionString))
			{
				totalRecords = QueryHelper.GetAllMembershipUsers(membershipEntities, this.ApplicationName, -1, -1).Count<DbDataRecord>();
				IQueryable<DbDataRecord> allMembershipUsers = QueryHelper.GetAllMembershipUsers(membershipEntities, this.ApplicationName, pageIndex, pageSize);
				foreach (DbDataRecord current in allMembershipUsers)
				{
					membershipUserCollection.Add(QueryHelper.CreateMembershipUserFromDbRecord(membershipEntities, current, this.ProviderName, this.ApplicationName, false));
				}
			}
			return membershipUserCollection;
		}
		public override int GetNumberOfUsersOnline()
		{
			DateTime dateactive = DateTime.UtcNow.Subtract(TimeSpan.FromMinutes((double)Membership.UserIsOnlineTimeWindow));
			int numberOfOnlineUsers;
			using (MembershipEntities membershipEntities = ModelHelper.CreateMembershipEntities(this.ConnectionString))
			{
				numberOfOnlineUsers = QueryHelper.GetNumberOfOnlineUsers(membershipEntities, this.ApplicationName, dateactive);
			}
			return numberOfOnlineUsers;
		}
		public override string GetPassword(string username, string answer)
		{
			if (!this.EnablePasswordRetrieval)
			{
				throw new NotSupportedException(ProviderResources.Membership_PasswordRetrieval_not_supported);
			}
			bool checkForNull = true;
			bool checkIfEmpty = true;
			bool checkForCommas = true;
			int maxSize = 256;
			string paramName = "username";
			Exception ex = ValidationHelper.CheckParameter(ref username, checkForNull, checkIfEmpty, checkForCommas, maxSize, paramName);
			if (ex != null)
			{
				throw ex;
			}
			string strB;
			if (answer != null)
			{
				strB = this.GetEncodedPasswordAnswer(username, answer);
			}
			else
			{
				strB = answer;
			}
			bool requiresQuestionAndAnswer = this.RequiresQuestionAndAnswer;
			bool requiresQuestionAndAnswer2 = this.RequiresQuestionAndAnswer;
			bool checkForCommas2 = false;
			int maxSize2 = 128;
			string paramName2 = "passwordAnswer";
			ex = ValidationHelper.CheckParameter(ref strB, requiresQuestionAndAnswer, requiresQuestionAndAnswer2, checkForCommas2, maxSize2, paramName2);
			if (ex != null)
			{
				throw ex;
			}
			int num = 0;
			using (MembershipEntities membershipEntities = ModelHelper.CreateMembershipEntities(this.ConnectionString))
			{
				MembershipEntity membership = QueryHelper.GetMembership(membershipEntities, this.ApplicationName, username);
				if (membership == null)
				{
					num = 1;
				}
				else
				{
					if (membership.IsLockedOut)
					{
						num = 99;
					}
					else
					{
						DateTime utcNow = DateTime.UtcNow;
						if (this.RequiresQuestionAndAnswer && membership.PasswordAnswer != null)
						{
							if (string.Compare(membership.PasswordAnswer, strB, StringComparison.OrdinalIgnoreCase) != 0)
							{
								membership.FailedPasswordAnswerAttemptWindowsStart = utcNow;
								if (utcNow > membership.FailedPasswordAnswerAttemptWindowsStart.AddMinutes((double)this.PasswordAttemptWindow))
								{
									membership.FailedPasswordAnswerAttemptCount = 1;
								}
								else
								{
									membership.FailedPasswordAnswerAttemptCount++;
								}
								if (membership.FailedPasswordAnswerAttemptCount >= this.MaxInvalidPasswordAttempts)
								{
									membership.IsLockedOut = true;
									membership.LastLockoutDate = utcNow;
								}
								num = 3;
							}
							else
							{
								if (membership.FailedPasswordAnswerAttemptCount > 0)
								{
									membership.FailedPasswordAnswerAttemptCount = 0;
									membership.FailedPasswordAnswerAttemptWindowsStart = DefaultMembershipProvider.NullDate;
								}
							}
							membershipEntities.SaveChanges();
						}
					}
				}
				if (num == 0 && membership.Password != null)
				{
					return this.UnEncodePassword(membership.Password, membership.PasswordFormat);
				}
			}
			DefaultMembershipProvider.ValidateStatus(num);
			return null;
		}
		public override MembershipUser GetUser(string username, bool userIsOnline)
		{
			bool checkForNull = true;
			bool checkIfEmpty = false;
			bool checkForCommas = true;
			int maxSize = 256;
			string paramName = "username";
			Exception ex = ValidationHelper.CheckParameter(ref username, checkForNull, checkIfEmpty, checkForCommas, maxSize, paramName);
			if (ex != null)
			{
				throw ex;
			}
			MembershipUser membershipUser;
			using (MembershipEntities membershipEntities = ModelHelper.CreateMembershipEntities(this.ConnectionString))
			{
				membershipUser = QueryHelper.GetMembershipUser(membershipEntities, username, this.ApplicationName, userIsOnline, this.ProviderName);
			}
			return membershipUser;
		}
		public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
		{
			if (providerUserKey == null)
			{
				throw new ArgumentNullException("providerUserKey");
			}
			if (!(providerUserKey is Guid))
			{
				throw new ArgumentException(ProviderResources.Membership_InvalidProviderUserKey, "providerUserKey");
			}
			MembershipUser membershipUser;
			using (MembershipEntities membershipEntities = ModelHelper.CreateMembershipEntities(this.ConnectionString))
			{
				membershipUser = QueryHelper.GetMembershipUser(membershipEntities, (Guid)providerUserKey, this.ApplicationName, userIsOnline, this.ProviderName);
			}
			return membershipUser;
		}
		public override string GetUserNameByEmail(string email)
		{
			bool requiresUniqueEmail = this.RequiresUniqueEmail;
			bool requiresUniqueEmail2 = this.RequiresUniqueEmail;
			bool checkForCommas = false;
			int maxSize = 256;
			string paramName = "email";
			Exception ex = ValidationHelper.CheckParameter(ref email, requiresUniqueEmail, requiresUniqueEmail2, checkForCommas, maxSize, paramName);
			if (ex != null)
			{
				throw ex;
			}
			string userNameFromEmail;
			using (MembershipEntities membershipEntities = ModelHelper.CreateMembershipEntities(this.ConnectionString))
			{
				userNameFromEmail = QueryHelper.GetUserNameFromEmail(membershipEntities, email, this.ApplicationName);
			}
			return userNameFromEmail;
		}
		private static void ValidateStatus(int status)
		{
			if (status == 0)
			{
				return;
			}
			string exceptionText = DefaultMembershipProvider.GetExceptionText(status);
			if (DefaultMembershipProvider.IsStatusDueToBadPassword(status))
			{
				throw new MembershipPasswordException(exceptionText);
			}
			throw new ProviderException(exceptionText);
		}
		private static string GetExceptionText(int status)
		{
			switch (status)
			{
			case 0:
				return string.Empty;

			case 1:
				return ProviderResources.Membership_UserNotFound;

			case 2:
				return ProviderResources.Membership_WrongPassword;

			case 3:
				return ProviderResources.Membership_WrongAnswer;

			case 4:
				return ProviderResources.Membership_InvalidPassword;

			case 5:
				return ProviderResources.Membership_InvalidQuestion;

			case 6:
				return ProviderResources.Membership_InvalidAnswer;

			case 7:
				return ProviderResources.Membership_InvalidEmail;

			default:
				if (status != 99)
				{
					return ProviderResources.Provider_Error;
				}
				return ProviderResources.Membership_AccountLockOut;
			}
		}
		private static bool IsStatusDueToBadPassword(int status)
		{
			return (status >= 2 && status <= 6) || status == 99;
		}
		public override string ResetPassword(string username, string answer)
		{
			if (!this.EnablePasswordReset)
			{
				throw new NotSupportedException(ProviderResources.Not_configured_to_support_password_resets);
			}
			bool checkForNull = true;
			bool checkIfEmpty = true;
			bool checkForCommas = true;
			int maxSize = 256;
			string paramName = "username";
			Exception ex = ValidationHelper.CheckParameter(ref username, checkForNull, checkIfEmpty, checkForCommas, maxSize, paramName);
			if (ex != null)
			{
				throw ex;
			}
			int status;
			string text;
			int passwordFormat;
			string text2;
			int num;
			int num2;
			bool flag;
			DateTime dateTime;
			DateTime dateTime2;
			this.GetPasswordWithFormat(username, false, out status, out text, out passwordFormat, out text2, out num, out num2, out flag, out dateTime, out dateTime2);
			DefaultMembershipProvider.ValidateStatus(status);
			if (answer != null)
			{
				answer = answer.Trim();
			}
			string strB;
			if (!string.IsNullOrEmpty(answer))
			{
				strB = this.EncodePassword(answer, passwordFormat, text2);
			}
			else
			{
				strB = answer;
			}
			bool requiresQuestionAndAnswer = this.RequiresQuestionAndAnswer;
			bool requiresQuestionAndAnswer2 = this.RequiresQuestionAndAnswer;
			bool checkForCommas2 = false;
			int maxSize2 = 128;
			string paramName2 = "answer";
			ex = ValidationHelper.CheckParameter(ref strB, requiresQuestionAndAnswer, requiresQuestionAndAnswer2, checkForCommas2, maxSize2, paramName2);
			if (ex != null)
			{
				throw ex;
			}
			string text3 = Membership.GeneratePassword((this.MinRequiredPasswordLength < 14) ? 14 : this.MinRequiredPasswordLength, this.MinRequiredNonAlphanumericCharacters);
			ValidatePasswordEventArgs validatePasswordEventArgs = new ValidatePasswordEventArgs(username, text3, false);
			this.OnValidatingPassword(validatePasswordEventArgs);
			if (!validatePasswordEventArgs.Cancel)
			{
				using (MembershipEntities membershipEntities = ModelHelper.CreateMembershipEntities(this.ConnectionString))
				{
					status = 0;
					DateTime utcNow = DateTime.UtcNow;
					MembershipEntity membership = QueryHelper.GetMembership(membershipEntities, this.ApplicationName, username);
					if (membership == null)
					{
						status = 1;
					}
					else
					{
						if (membership.IsLockedOut)
						{
							status = 99;
						}
						else
						{
							if (answer == null || string.Compare(membership.PasswordAnswer, strB, StringComparison.OrdinalIgnoreCase) == 0)
							{
								membership.Password = this.EncodePassword(text3, passwordFormat, text2);
								membership.LastPasswordChangedDate = DateTime.UtcNow;
								membership.PasswordFormat = passwordFormat;
								membership.PasswordSalt = text2;
								if (membership.FailedPasswordAnswerAttemptCount > 0)
								{
									membership.FailedPasswordAnswerAttemptCount = 0;
									membership.FailedPasswordAnswerAttemptWindowsStart = DefaultMembershipProvider.NullDate;
								}
							}
							else
							{
								if (utcNow > membership.FailedPasswordAnswerAttemptWindowsStart.AddMinutes((double)this.PasswordAttemptWindow))
								{
									membership.FailedPasswordAnswerAttemptCount = 1;
								}
								else
								{
									membership.FailedPasswordAnswerAttemptCount++;
								}
								membership.FailedPasswordAnswerAttemptWindowsStart = utcNow;
								if (membership.FailedPasswordAnswerAttemptCount >= this.MaxInvalidPasswordAttempts)
								{
									membership.IsLockedOut = true;
									membership.LastLockoutDate = utcNow;
								}
								status = 3;
							}
							membershipEntities.SaveChanges();
						}
					}
					DefaultMembershipProvider.ValidateStatus(status);
				}
				return text3;
			}
			if (validatePasswordEventArgs.FailureInformation != null)
			{
				throw validatePasswordEventArgs.FailureInformation;
			}
			throw new ProviderException(string.Format(CultureInfo.CurrentCulture, ProviderResources.Membership_Custom_Password_Validation_Failure, new object[0]));
		}
		public override bool UnlockUser(string userName)
		{
			bool checkForNull = true;
			bool checkIfEmpty = true;
			bool checkForCommas = true;
			int maxSize = 256;
			string paramName = "username";
			Exception ex = ValidationHelper.CheckParameter(ref userName, checkForNull, checkIfEmpty, checkForCommas, maxSize, paramName);
			if (ex != null)
			{
				throw ex;
			}
			bool result;
			using (MembershipEntities membershipEntities = ModelHelper.CreateMembershipEntities(this.ConnectionString))
			{
				MembershipEntity membership = QueryHelper.GetMembership(membershipEntities, this.ApplicationName, userName);
				if (membership != null)
				{
					membership.IsLockedOut = false;
					membership.FailedPasswordAnswerAttemptCount = 0;
					membership.FailedPasswordAnswerAttemptWindowsStart = DefaultMembershipProvider.NullDate;
					membership.FailedPasswordAttemptCount = 0;
					membership.FailedPasswordAttemptWindowStart = DefaultMembershipProvider.NullDate;
					membership.LastLockoutDate = DefaultMembershipProvider.NullDate;
					membershipEntities.SaveChanges();
					result = true;
				}
				else
				{
					result = false;
				}
			}
			return result;
		}
		public override void UpdateUser(MembershipUser user)
		{
			if (user == null)
			{
				throw new ArgumentNullException("user");
			}
			string userName = user.UserName;
			bool checkForNull = true;
			bool checkIfEmpty = true;
			bool checkForCommas = true;
			int maxSize = 256;
			string paramName = "user.UserName";
			Exception ex = ValidationHelper.CheckParameter(ref userName, checkForNull, checkIfEmpty, checkForCommas, maxSize, paramName);
			if (ex != null)
			{
				throw ex;
			}
			string email = user.Email;
			bool requiresUniqueEmail = this.RequiresUniqueEmail;
			bool requiresUniqueEmail2 = this.RequiresUniqueEmail;
			bool checkForCommas2 = false;
			int maxSize2 = 256;
			string paramName2 = "user.Email";
			ex = ValidationHelper.CheckParameter(ref email, requiresUniqueEmail, requiresUniqueEmail2, checkForCommas2, maxSize2, paramName2);
			if (ex != null)
			{
				throw ex;
			}
			using (MembershipEntities membershipEntities = ModelHelper.CreateMembershipEntities(this.ConnectionString))
			{
				int num = 0;
				Guid? guid = (Guid?)user.ProviderUserKey;
				if (!guid.HasValue)
				{
					num = 1;
				}
				if (num == 0 && this.RequiresUniqueEmail && QueryHelper.DuplicateEmailExists(membershipEntities, this.ApplicationName, guid.Value, user.Email))
				{
					num = 7;
				}
				if (num == 0)
				{
					User user2 = QueryHelper.GetUser(membershipEntities, guid.Value, this.ApplicationName);
					user2.LastActivityDate = DateTime.UtcNow;
					MembershipEntity membership = QueryHelper.GetMembership(membershipEntities, this.ApplicationName, guid.Value);
					membership.Email = user.Email;
					membership.Comment = user.Comment;
					membership.IsApproved = user.IsApproved;
					membership.LastLoginDate = user.LastLoginDate;
					membershipEntities.SaveChanges();
				}
				else
				{
					DefaultMembershipProvider.ValidateStatus(num);
				}
			}
		}
		public override bool ValidateUser(string username, string password)
		{
			bool checkForNull = true;
			bool checkIfEmpty = true;
			bool checkForCommas = false;
			int maxSize = 128;
			if (ValidationHelper.ValidateParameter(ref password, checkForNull, checkIfEmpty, checkForCommas, maxSize))
			{
				bool checkForNull2 = true;
				bool checkIfEmpty2 = true;
				bool checkForCommas2 = true;
				int maxSize2 = 256;
				if (ValidationHelper.ValidateParameter(ref username, checkForNull2, checkIfEmpty2, checkForCommas2, maxSize2))
				{
					bool updateLastActivityDate = true;
					bool failIfNotApproved = true;
					if (this.CheckPassword(username, password, updateLastActivityDate, failIfNotApproved))
					{
						return true;
					}
				}
			}
			return false;
		}
		private static string GenerateSalt()
		{
			string result;
			using (RNGCryptoServiceProvider rNGCryptoServiceProvider = new RNGCryptoServiceProvider())
			{
				byte[] array = new byte[16];
				rNGCryptoServiceProvider.GetBytes(array);
				result = Convert.ToBase64String(array);
			}
			return result;
		}
		private HashAlgorithm GetHashAlgorithm()
		{
			if (this._HashAlgorithm != null)
			{
				return HashAlgorithm.Create(this._HashAlgorithm);
			}
			string hashAlgorithmType = Membership.HashAlgorithmType;
			HashAlgorithm hashAlgorithm = HashAlgorithm.Create(hashAlgorithmType);
			if (hashAlgorithm == null)
			{
				throw new ConfigurationErrorsException(string.Format(CultureInfo.CurrentCulture, ProviderResources.Invalid_hash_algorithm, new object[]
				{
					this._HashAlgorithm
				}));
			}
			this._HashAlgorithm = hashAlgorithmType;
			return hashAlgorithm;
		}
		private string EncodePassword(string pass, int passwordFormat, string salt)
		{
			if (passwordFormat == 0)
			{
				return pass;
			}
			byte[] bytes = Encoding.Unicode.GetBytes(pass);
			byte[] array = Convert.FromBase64String(salt);
			byte[] inArray;
			if (passwordFormat == 1)
			{
				HashAlgorithm hashAlgorithm = this.GetHashAlgorithm();
				KeyedHashAlgorithm keyedHashAlgorithm = hashAlgorithm as KeyedHashAlgorithm;
				if (keyedHashAlgorithm != null)
				{
					if (keyedHashAlgorithm.Key.Length == array.Length)
					{
						keyedHashAlgorithm.Key = array;
					}
					else
					{
						if (keyedHashAlgorithm.Key.Length < array.Length)
						{
							byte[] array2 = new byte[keyedHashAlgorithm.Key.Length];
							Buffer.BlockCopy(array, 0, array2, 0, array2.Length);
							keyedHashAlgorithm.Key = array2;
						}
						else
						{
							byte[] array3 = new byte[keyedHashAlgorithm.Key.Length];
							int num;
							for (int i = 0; i < array3.Length; i += num)
							{
								num = Math.Min(array.Length, array3.Length - i);
								Buffer.BlockCopy(array, 0, array3, i, num);
							}
							keyedHashAlgorithm.Key = array3;
						}
					}
					inArray = keyedHashAlgorithm.ComputeHash(bytes);
				}
				else
				{
					byte[] array4 = new byte[array.Length + bytes.Length];
					Buffer.BlockCopy(array, 0, array4, 0, array.Length);
					Buffer.BlockCopy(bytes, 0, array4, array.Length, bytes.Length);
					inArray = hashAlgorithm.ComputeHash(array4);
				}
			}
			else
			{
				byte[] array5 = new byte[array.Length + bytes.Length];
				Buffer.BlockCopy(array, 0, array5, 0, array.Length);
				Buffer.BlockCopy(bytes, 0, array5, array.Length, bytes.Length);
				inArray = this.EncryptPassword(array5);
			}
			return Convert.ToBase64String(inArray);
		}
		private string UnEncodePassword(string pass, int passwordFormat)
		{
			switch (passwordFormat)
			{
			case 0:
				return pass;

			case 1:
				throw new ProviderException(string.Format(CultureInfo.CurrentCulture, ProviderResources.Provider_can_not_decode_hashed_password, new object[0]));

			default:
				{
					byte[] encodedPassword = Convert.FromBase64String(pass);
					byte[] array = this.DecryptPassword(encodedPassword);
					if (array == null)
					{
						return null;
					}
					return Encoding.Unicode.GetString(array, 16, array.Length - 16);
				}
			}
		}
		private bool CheckPassword(string userName, string password, bool updateLastActivityDate, bool failIfNotApproved)
		{
			string text;
			int num;
			return this.CheckPassword(userName, password, updateLastActivityDate, failIfNotApproved, out text, out num);
		}
		private bool CheckPassword(string userName, string password, bool updateLastActivityDate, bool failIfNotApproved, out string salt, out int passwordFormat)
		{
			int num;
			string strB;
			int num2;
			int num3;
			bool flag;
			DateTime lastLoginDate;
			DateTime lastActivityDate;
			this.GetPasswordWithFormat(userName, updateLastActivityDate, out num, out strB, out passwordFormat, out salt, out num2, out num3, out flag, out lastLoginDate, out lastActivityDate);
			if (num == 0)
			{
				if (!flag && failIfNotApproved)
				{
					return false;
				}
				string strA = this.EncodePassword(password, passwordFormat, salt);
				bool flag2 = string.Compare(strA, strB, StringComparison.Ordinal) == 0;
				DateTime utcNow = DateTime.UtcNow;
				using (MembershipEntities membershipEntities = ModelHelper.CreateMembershipEntities(this.ConnectionString))
				{
					MembershipEntity membership = QueryHelper.GetMembership(membershipEntities, this.ApplicationName, userName);
					if (membership != null && !membership.IsLockedOut)
					{
						if (flag2)
						{
							if (membership.FailedPasswordAttemptCount > 0 || membership.FailedPasswordAnswerAttemptCount > 0)
							{
								membership.FailedPasswordAnswerAttemptCount = 0;
								membership.FailedPasswordAnswerAttemptWindowsStart = DefaultMembershipProvider.NullDate;
								membership.FailedPasswordAttemptCount = 0;
								membership.FailedPasswordAttemptWindowStart = DefaultMembershipProvider.NullDate;
								membership.LastLockoutDate = DefaultMembershipProvider.NullDate;
							}
						}
						else
						{
							if (utcNow > membership.FailedPasswordAttemptWindowStart.AddMinutes((double)this.PasswordAttemptWindow))
							{
								membership.FailedPasswordAttemptCount = 1;
							}
							else
							{
								membership.FailedPasswordAttemptCount++;
							}
							membership.FailedPasswordAttemptWindowStart = utcNow;
							if (membership.FailedPasswordAttemptCount >= this.MaxInvalidPasswordAttempts)
							{
								membership.IsLockedOut = true;
								membership.LastLockoutDate = utcNow;
							}
						}
						if (updateLastActivityDate)
						{
							membership.LastLoginDate = lastLoginDate;
							User user = QueryHelper.GetUser(membershipEntities, membership.UserId, this.ApplicationName);
							user.LastActivityDate = lastActivityDate;
						}
						membershipEntities.SaveChanges();
					}
					return flag2;
				}
				return false;
			}
			return false;
		}
		private string GetEncodedPasswordAnswer(string userName, string passwordAnswer)
		{
			if (passwordAnswer != null)
			{
				passwordAnswer = passwordAnswer.Trim();
			}
			if (string.IsNullOrEmpty(passwordAnswer))
			{
				return passwordAnswer;
			}
			int status;
			string text;
			int passwordFormat;
			string salt;
			int num;
			int num2;
			bool flag;
			DateTime dateTime;
			DateTime dateTime2;
			this.GetPasswordWithFormat(userName, false, out status, out text, out passwordFormat, out salt, out num, out num2, out flag, out dateTime, out dateTime2);
			DefaultMembershipProvider.ValidateStatus(status);
			return this.EncodePassword(passwordAnswer, passwordFormat, salt);
		}
		private int Membership_CreateUser(string applicationName, string userName, string password, string salt, string email, string passwordQuestion, string passwordAnswer, bool isApproved, out DateTime createDate, bool uniqueEmail, int passwordFormat, ref object providerUserKey)
		{
			createDate = DateTime.UtcNow;
			int result;
			using (MembershipEntities membershipEntities = ModelHelper.CreateMembershipEntities(this.ConnectionString))
			{
				Application application = QueryHelper.GetApplication(membershipEntities, applicationName);
				if (application == null)
				{
					application = ModelHelper.CreateApplication(membershipEntities, applicationName);
				}
				Guid applicationId = application.ApplicationId;
				Guid? guid = (Guid?)providerUserKey;
				User user = QueryHelper.GetUser(membershipEntities, userName, applicationName);
				Guid? guid2 = (user == null) ? null : new Guid?(user.UserId);
				bool flag;
				if (!guid2.HasValue)
				{
					if (!guid.HasValue)
					{
						guid2 = new Guid?(Guid.NewGuid());
					}
					else
					{
						Guid value = guid.Value;
						User user2 = QueryHelper.GetUser(membershipEntities, value, applicationName);
						if (user2 != null)
						{
							result = 10;
							return result;
						}
						guid2 = new Guid?(guid.Value);
					}
					ModelHelper.CreateUser(membershipEntities, guid2.Value, userName, applicationId, false);
					flag = true;
				}
				else
				{
					flag = false;
					if (guid.HasValue && guid2 != guid.Value)
					{
						result = 6;
						return result;
					}
				}
				MembershipEntity membership = QueryHelper.GetMembership(membershipEntities, applicationName, guid2.Value);
				if (membership != null)
				{
					result = 6;
				}
				else
				{
					if (uniqueEmail)
					{
						string userNameFromEmail = QueryHelper.GetUserNameFromEmail(membershipEntities, email, applicationName);
						if (userNameFromEmail != null)
						{
							result = 7;
							return result;
						}
					}
					if (!flag)
					{
						user.LastActivityDate = createDate;
					}
					MembershipEntity membershipEntity = new MembershipEntity();
					membershipEntity.ApplicationId = applicationId;
					membershipEntity.CreateDate = createDate;
					membershipEntity.Email = email;
					membershipEntity.FailedPasswordAnswerAttemptCount = 0;
					membershipEntity.FailedPasswordAnswerAttemptWindowsStart = DefaultMembershipProvider.NullDate;
					membershipEntity.FailedPasswordAttemptCount = 0;
					membershipEntity.FailedPasswordAttemptWindowStart = DefaultMembershipProvider.NullDate;
					membershipEntity.IsApproved = isApproved;
					membershipEntity.IsLockedOut = false;
					membershipEntity.LastLockoutDate = DefaultMembershipProvider.NullDate;
					membershipEntity.LastLoginDate = createDate;
					membershipEntity.LastPasswordChangedDate = createDate;
					membershipEntity.Password = password;
					membershipEntity.PasswordAnswer = passwordAnswer;
					membershipEntity.PasswordFormat = passwordFormat;
					membershipEntity.PasswordQuestion = passwordQuestion;
					membershipEntity.PasswordSalt = salt;
					membershipEntity.UserId = guid2.Value;
					providerUserKey = guid2.Value;
					membershipEntities.Memberships.AddObject(membershipEntity);
					membershipEntities.SaveChanges();
					result = 0;
				}
			}
			return result;
		}
		private void GetPasswordWithFormat(string userName, bool updateLastLoginActivityDate, out int status, out string password, out int format, out string salt, out int failedPasswordAttemptCount, out int failedPasswordAnswerAttemptCount, out bool isApproved, out DateTime lastLoginDate, out DateTime lastActivityDate)
		{
			password = null;
			format = 0;
			salt = null;
			failedPasswordAttemptCount = 0;
			failedPasswordAnswerAttemptCount = 0;
			isApproved = false;
			lastLoginDate = (lastActivityDate = DateTime.UtcNow);
			status = 1;
			using (MembershipEntities membershipEntities = ModelHelper.CreateMembershipEntities(this.ConnectionString))
			{
				MembershipEntity membership = QueryHelper.GetMembership(membershipEntities, this.ApplicationName, userName);
				if (membership != null)
				{
					if (membership.IsLockedOut)
					{
						status = 99;
					}
					else
					{
						password = membership.Password;
						format = membership.PasswordFormat;
						salt = membership.PasswordSalt;
						failedPasswordAttemptCount = membership.FailedPasswordAttemptCount;
						failedPasswordAnswerAttemptCount = membership.FailedPasswordAnswerAttemptCount;
						isApproved = membership.IsApproved;
						User user = QueryHelper.GetUser(membershipEntities, membership.UserId, this.ApplicationName);
						if (updateLastLoginActivityDate)
						{
							membership.LastLoginDate = (user.LastActivityDate = DateTime.UtcNow);
							membershipEntities.SaveChanges();
						}
						lastLoginDate = membership.LastLoginDate;
						lastActivityDate = user.LastActivityDate;
						status = 0;
					}
				}
			}
		}
	}
}
