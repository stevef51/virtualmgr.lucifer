﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.PowerBi
{
    /// <summary>
    /// Interface to abstract away the retrieval of the configuration information for 
    /// Power BI Embedded
    /// </summary>
    public interface IPowerBiConfiguration
    {
        string WorkspaceCollection { get; }
        string AccessKey { get; }
        string ApiUrl { get; }
    }
}
