﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.PowerBI.Api.V1;
using Microsoft.PowerBI.Api.V1.Models;
using System.IO;

namespace ConceptCave.PowerBi
{
    public interface IPowerBiManager
    {
        IPowerBiConfiguration Configuration { get; }
        string CreateWorkspace();
        string[] GetWorkspaces();

        bool HasWorkspace(string workspaceId);

        IImportedPbix ImportPbix(string workspaceId, string datasetName, Stream pbix);

        void SetConnectionInformation(string workspaceId, string datasetKey, string connectionString, string username, string password);

        void DeletePbix(string workspaceId, string datasetKey);

        IImportedReport[] GetReports(string workspaceId);

        IPowerBiToken CreateReportEmbedToken(string workpsaceId, string reportId);
    }

    public interface IPowerBiToken
    {
        string AccessToken { get; set; }
        string Audience { get; set; }
        DateTime? Expiration { get; set; }
        string Issuer { get; set; }
    }
}
