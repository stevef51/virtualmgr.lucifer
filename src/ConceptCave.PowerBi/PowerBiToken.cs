﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.PowerBi
{
    public class PowerBiToken : IPowerBiToken
    {
        public string AccessToken
        {
            get;
            set;
        }

        public string Audience
        {
            get;
            set;
        }

        public DateTime? Expiration
        {
            get;
            set;
        }

        public string Issuer
        {
            get;
            set;
        }
    }
}
