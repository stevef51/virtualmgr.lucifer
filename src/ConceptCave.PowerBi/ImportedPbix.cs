﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.PowerBi
{
    public class ImportedPbix : IImportedPbix
    {
        public ImportedPbix()
        {
            Report = new ImportedReport();
            Dataset = new ImportedDataset();
        }

        public string Id
        {
            get;
            set;
        }

        public IImportedReport Report
        {
            get;
            set;
        }

        public IImportedDataset Dataset
        {
            get;
            set;
        }

        public string ToJson()
        {
            JObject result = new JObject();
            result["id"] = Id;

            if(Report != null)
            {
                result["report"] = Report.ToJson();
            }

            if(Dataset != null)
            {
                result["dataset"] = Dataset.ToJson();
            }

            return result.ToString();
        }
    }

    public class ImportedReport : IImportedReport
    {
        public string Id
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public string EmbedUrl
        {
            get;
            set;
        }

        public string WebUrl
        {
            get;
            set;
        }
        public JObject ToJson()
        {
            JObject result = new JObject();
            result["id"] = Id;
            result["name"] = Name;
            result["embedurl"] = EmbedUrl;
            result["weburl"] = WebUrl;

            return result;
        }
    }

    public class ImportedDataset : IImportedDataset
    {
        public string Id
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public string WebUrl
        {
            get;
            set;
        }

        public JObject ToJson()
        {
            JObject result = new JObject();
            result["id"] = Id;
            result["name"] = Name;
            result["weburl"] = WebUrl;

            return result;
        }
    }
}
