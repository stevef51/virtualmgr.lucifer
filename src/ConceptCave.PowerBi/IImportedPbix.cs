﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.PowerBi
{
    public interface IImportedPbix
    {
        string Id { get; }

        IImportedReport Report { get; }

        IImportedDataset Dataset { get; }

        string ToJson();
    }

    public interface IImportedReport
    {
        string Id { get; }
        string Name { get; }
        string EmbedUrl { get; }
        string WebUrl { get; }

        Newtonsoft.Json.Linq.JObject ToJson();
    }

    public interface IImportedDataset
    {
        string Id { get; }
        string Name { get; }
        string WebUrl { get; }

        Newtonsoft.Json.Linq.JObject ToJson();
    }
}
