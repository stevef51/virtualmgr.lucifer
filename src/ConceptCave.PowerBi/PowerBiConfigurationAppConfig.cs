﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.PowerBi
{
    public class PowerBiConfigurationAppConfig : IPowerBiConfiguration
    {
        public string WorkspaceCollection
        {
            get 
            { 
                return System.Configuration.ConfigurationManager.AppSettings["AzurePowerBi.WorkspaceCollection"];
            }
        }

        public string AccessKey
        {
            get { return System.Configuration.ConfigurationManager.AppSettings["AzurePowerBi.AccessKey"]; }
        }

        public string ApiUrl
        {
            get { return System.Configuration.ConfigurationManager.AppSettings["AzurePowerBi.ApiUrl"]; }
        }
    }
}
