﻿using Microsoft.PowerBI.Api.V1;
using Microsoft.PowerBI.Api.V1.Models;
using Microsoft.Rest;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.PowerBi
{
    public class PowerBiManager : IPowerBiManager
    {
        public PowerBiManager(IPowerBiConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IPowerBiConfiguration Configuration
        {
            get; protected set;
        }

        protected PowerBIClient GetClient()
        {
            var credentials = new TokenCredentials(Configuration.AccessKey, "AppKey");
            var client = new PowerBIClient(credentials)
            {
                BaseUri = new Uri(Configuration.ApiUrl)
            };

            return client;
        }

        public string CreateWorkspace()
        {
            using(var client = GetClient())
            {
                return client.Workspaces.PostWorkspace(Configuration.WorkspaceCollection).WorkspaceId;
            }
        }

        public string[] GetWorkspaces()
        {
            using(var client = GetClient())
            {
                return client.Workspaces.GetWorkspacesByCollectionName(Configuration.WorkspaceCollection).Value.Select(v => v.WorkspaceId).ToArray();
            }
        }

        public bool HasWorkspace(string workspaceId)
        {
            var spaces = GetWorkspaces();

            return (from s in spaces where s == workspaceId select s).Count() > 0;
        }

        public void DeleteDataset(string workspaceId, string datasetName, PowerBIClient client)
        {
            try
            {
                client.Datasets.DeleteDatasetById(Configuration.WorkspaceCollection, workspaceId, datasetName);
            }
            catch(Exception e)
            {
                // going to assume this occurred as the dataset couldn't be found, lets just get on with life
            }
        }

        public IImportedPbix ImportPbix(string workspaceId, string datasetName, Stream pbix)
        {
            using(var client = GetClient())
            {
                client.HttpClient.Timeout = TimeSpan.FromMinutes(60);
                client.HttpClient.DefaultRequestHeaders.Add("ActivityId", Guid.NewGuid().ToString());

                var import = client.Imports.PostImportWithFile(Configuration.WorkspaceCollection, workspaceId, pbix, datasetName, "Ignore");

                Microsoft.PowerBI.Api.V1.Models.Import result = null;

                for(int i = 0; i<10; i++)
                {
                    // err the API comes back with a blank return if we don't wait awhile, horrible
                    // hacky type code ensues
                    System.Threading.Thread.Sleep(5000);

                    result = client.Imports.GetImportById(Configuration.WorkspaceCollection, workspaceId, import.Id);

                    if(result.Reports.Count() > 0)
                    {
                        break;
                    }
                }

                var px = new ImportedPbix()
                {
                    Id = result.Id,
                };

                ((ImportedReport)px.Report).Id = result.Reports[0].Id;
                ((ImportedReport)px.Report).Name = result.Reports[0].Name;
                ((ImportedReport)px.Report).EmbedUrl = result.Reports[0].EmbedUrl;
                ((ImportedReport)px.Report).WebUrl = result.Reports[0].WebUrl;

                ((ImportedDataset)px.Dataset).Id = result.Datasets[0].Id;
                ((ImportedDataset)px.Dataset).Name = result.Datasets[0].Name;
                ((ImportedDataset)px.Dataset).WebUrl = result.Datasets[0].WebUrl;

                return px;
            }
        }

        public void SetConnectionInformation(string workspaceId, string datasetKey, string connectionString, string username, string password)
        {
            using(var client = GetClient())
            {
                if(string.IsNullOrEmpty(connectionString) == false)
                {
                    var dataset = client.Datasets.GetDatasetById(Configuration.WorkspaceCollection, workspaceId, datasetKey);

                    var connectionParameters = new Dictionary<string, object>
                    {
                        { "connectionString", connectionString }
                    };

                    client.Datasets.SetAllConnections(Configuration.WorkspaceCollection, workspaceId, datasetKey, connectionParameters);
                }

                if(string.IsNullOrEmpty(username) == false)
                {
                    var datasources = client.Datasets.GetGatewayDatasources(Configuration.WorkspaceCollection, workspaceId, datasetKey);

                    var delta = new GatewayDatasource
                    {
                        CredentialType = "Basic",
                        BasicCredentials = new BasicCredentials
                        {
                            Username = username,
                            Password = password
                        }
                    };

                    client.Gateways.PatchDatasource(Configuration.WorkspaceCollection, workspaceId, datasources.Value[0].GatewayId, datasources.Value[0].Id, delta);
                }
            }
        }

        public void DeletePbix(string workspaceId, string datasetKey)
        {
            using(var client = GetClient())
            {
                client.Datasets.DeleteDatasetById(Configuration.WorkspaceCollection, workspaceId, datasetKey);
            }
        }

        public IImportedReport[] GetReports(string workspaceId)
        {
            var result = new List<IImportedReport>(); 

            using (var client = GetClient())
            {
                var reports = client.Reports.GetReports(Configuration.WorkspaceCollection, workspaceId);

                foreach(var report in reports.Value)
                {
                    var r = new ImportedReport()
                    {
                        Id = report.Id,
                        EmbedUrl = report.EmbedUrl,
                        Name = report.Name,
                        WebUrl = report.WebUrl
                    };

                    result.Add(r);
                }
            }

            return result.ToArray();
        }

        public IPowerBiToken CreateReportEmbedToken(string workpsaceId, string reportId)
        {
            var token = Microsoft.PowerBI.Security.PowerBIToken.CreateReportEmbedToken(Configuration.WorkspaceCollection, workpsaceId, reportId);

            string accessToken = token.Generate(Configuration.AccessKey);

            return new PowerBiToken()
            {
                AccessToken = accessToken,
                Audience = token.Audience,
                Expiration = token.Expiration,
                Issuer = token.Issuer
            };
        }
    }
}
