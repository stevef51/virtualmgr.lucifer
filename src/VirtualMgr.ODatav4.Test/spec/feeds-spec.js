const requestPromise = require('request-promise');
const _ = require('lodash');
const xml2js = require('xml2js');

// Note, the url below is always same no matter which tenant we want to hit
// as this is run internally within Docker, we access the tenant we want by
// altering the Host header
const odataPromise = arg => {
    var opt = Object.assign({
        url: `http://odatav4/odata/odata-v4/${arg.feed}`,
        headers: {
            Host: 'tenantmaster.localhost'
        },
        json: true
    })
    console.log(`Requesting ${opt.url}`);
    return requestPromise(opt);
}

var feedsToTest = [
    'AssetTypes',
    'Assets?$expand=Tasks',
    'Beacons?$expand=User,TaskType',
    'ChecklistMediaStream',
    'ChecklistResults?$expand=ChecklistResultLabels',
    'Checklists?$expand=ChecklistLabels',
    'Companies',
    'CurrentMediaVersionSummary',
    'DBObjectSizes',
    'DBSizes',
    'ESProbes?$expand=SensorReadings',
    'ESSensorReadings?$expand=Probe,Sensor',
    'ESSensors',
    'FacilityStructures?$expand=OnFloorPlan,FSFacility,FSBuilding,FSFloor,FSZone,FSSite',
    'FinishStatuses',
    'FloorPlans?$expand=Floor,Building,Facility',
    'GlobalSettings',
    'GpsLocation',
    'IncompleteTasks',
    'Leave',
    'LeaveTypes',
    'Media',
    'MediaPageViews',
    'MediaViews',
    'MemberContexts',
    'Members?$expand=MembershipLabels,MembershipContexts,FSSite',
    'MembersMedia',
    'NewsFeeds',
    'OrderItems?$expand=Order,FacilityStructure',
    'Orders?$expand=OrderItems,OrderMedias',
    'PaymentResponses?$expand=PendingPayment',
    'PendingPayments?$expand=PaymentResponses',
    'PeriodicExecutionHistory',
    'Products',
    'QRCodes?$expand=User,TaskType',
    'RosterRoles',
    'Rosters',
    'Schedules',
    'TabletLocationHistory?$expand=GpsLocation,Tablet',
    'Tablets',
    'TaskFinishStatus?$expand=Task',
    'TaskMediaStream',
    'TaskTypeFinishedStatus?$expand=FinishStatus',
    'TaskTypes',
    'TaskWorkLoadingActivities',
    'TaskWorklogs?$expand=Task',
    'Tasks?$expand=TaskLabels,TaskFinishStatus,TaskWorkLogs,OwnerCompany,Asset,HierarchyBucket,FSSite',
    'TeamHierarchys',
    'Tenants',
    'TimesheetItemTypes',
    'UserLogs?$expand=Tablet,User',
    'UserTypeTaskTypes?$expand=TaskType',
    'WorkingDocuments'
];
feedsToTest.sort();

const allFeeds = [
    'AssetLastKnownLocation',
    'AssetLocationHistory',
    'Calendar',
    'CurrentUserTasksRosterSummary',
    'CurrentUserTasksSummary',
    'Resource',
    'ResourceLabel',
    'TimesheetItems',
]
    .concat(feedsToTest)
    .map(f => f.split('?')[0])              // Strip off the ?$expand
    .sort();


jasmine.DEFAULT_TIMEOUT_INTERVAL = 30000;

describe('OData tests', function () {
    it('all feeds are tested', async function (done) {
        var feeds = await odataPromise({ feed: '' });
        var availableFeeds = feeds.value.map(f => f.name).sort();

        expect(availableFeeds).toEqual(allFeeds);

        done();
    });

    feedsToTest.forEach(feed => {
        it(`${feed} should work`, async function (done) {
            var data = await odataPromise({
                feed: `${feed}${feed.indexOf('?') == -1 ? '?' : '&'}$top=1`
            });

            expect(data.error).toBeUndefined();
            expect(data.value).toBeDefined();
            //expect(data.value.length).not.toBe(0);
            done();
        });
    })
});