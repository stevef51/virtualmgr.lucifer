# VirtualMgr.ODatav4.Test

This Node project uses Jasmine to run a set of tests to briefly check that the ODatav4 service is running correctly, note by default it runs against

`
tenantmaster.localhost
`

which is required to be present and also to have the latest schema updates applied to it.

To run, from within a dev-container (eg ODatav4)

```
cd VirtualMgr.ODatav4.Test
clear && npm test
```

The `clear` command simply clears the console allowing last results to easily be perused.