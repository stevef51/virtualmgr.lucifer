﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.RepositoryInterfaces.Importing
{
    public class ImportParserResult
    {
        public bool Success
        {
            get
            {
                return Errors.Count == 0;
            }
        }

        public List<string> Errors { get; set; }

        public object Data { get; set; }

        public int Count { get; set; }

        public ImportParserResult()
        {
            Errors = new List<string>();
        }
    }

    public class ImportParserResultException : Exception
    {
        public ImportParserResult Result { get; set; }

        public ImportParserResultException(string message, ImportParserResult result)
            : base(message)
        {
            Result = result;
        }
    }
}
