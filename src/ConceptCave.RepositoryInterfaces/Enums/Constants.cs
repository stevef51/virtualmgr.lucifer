﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.RepositoryInterfaces.Enums
{
    public class GlobalSettingsConstants
    {
        public const string SystemSetting_StoreLoginSite = "StoreLoginSite";
        public const string SystemSetting_StoreLoginCoordinates = "StoreLoginCoordinates";
        public const string SystemSetting_ScheduledProcessEnabled = "ScheduledProcessEnabled";
        public const string SystemSetting_ScheduledProcessThreadCount = "ScheduledProcessThreadCount";
        public const string SystemSetting_SystemName = "Name";
        public const string SystemSetting_ThemeMediaId = "ThemeMediaId";
        public const string SystemSetting_FavIconId = "FavIconMediaId";
        public const string SystemSetting_SiteId = "SiteId";
        public const string SystemSetting_UTCLastModified = "UtcLastModified";
        public const string SystemSetting_ForgottenPasswordEmailBody = "ForgottenPasswordEmailBody";
        public const string SystemSetting_EmailFromAddress = "EmailFromAddress";
        public const string SystemSetting_ForgottenPasswordNoEmailAddressMessage = "ForgottenPasswordNoEmailForUser";

        public const string SystemSetting_MembershipImportTransactionTimeout = "MembershipImportTransactionTimeout";
        public const string SystemSetting_MembershipImportIndividualTransactions = "MembershipImportIndividualTransactions";
        public const string SystemSetting_MembershipImportTemplateId = "MembershipImportTemplateId";
        public const string SystemSetting_MembershipImportMapping = "MembershipImportMap";
        public const string SystemSetting_MembershipImportTransform = "MembershipImportTransform";

        public const string SystemSetting_MembershipNoMediaImageId = "MembershipNoMediaImageId";

        public const string SystemSetting_UploadMediaQuestionUploadFolderId = "UploadMediaQuestionRootFolderId";

        public const string SystemSetting_TemporaryMediaFolder = "TemporaryMediaFolder";

        public const string Module_Workloading = "Module.Workloading";

        public const string SystemSetting_AzureWorkspaceId = "AzurePowerBi.WorkspaceId";
    }
}
