﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.RepositoryInterfaces.Enums
{
    #region PublishingGroup
    [Flags]
    public enum PublishingGroupLoadInstruction
    {
        None = 1,
        Actors = 2,
        Resources = 4,
        Labels = 8,
        ResourceTickets = 16,
        ResourceData = 32
    }

    public enum PublishingGroupActorType
    {
        Reviewer = 1,
        Reviewee = 2
    }
    #endregion

    #region PublishingGroupResource
    [Flags]
    public enum PublishingGroupResourceLoadInstruction
    {
        None = 1,
        Data = 2,
        Resource = 4,
        ResourceData = 8,
        PublicVariables = 16,
        PublicTickets = 32,
        ResourceLabels = 64,
        Group = 128
    }
    #endregion

    #region WorkingDocument

    [Flags]
    public enum WorkingDocumentLoadInstructions
    {
        None = 1,
        Data = 2,
        References = 4,
        CompletedData = 8,
        Publishing = 16,
        Resource = 32,
        ResourceLabels = 64,
        Users = 128,
        PublicVariables = 256
    }

    public enum WorkingDocumentStatus
    {
        Started = 1,
        Completed = 2,
        PendingCompletion = 3,
        Processing = 4,
        Failed = 5,
        Cancelled = 6,
    }

    #endregion

    #region Membership
    [Flags]
    public enum MembershipLoadInstructions
    {
        None = 1,
        MembershipType = 2,
        MembershipRole = 4,
        MembershipLabel = 8,
        Label = 16,
        AspNetMembership = 32,
        ContextData = 64,
        RelatedMedia = 128,
        ContextResources = 256,
        Company = 512,
        Media = 1024,
        MediaData = 2048,
        Features = 4096,
        Areas = 8192,
        TabletProfile = 16384
    }

    [Flags]
    public enum MembershipTypeLoadInstructions
    {
        None = 1,
        UserTypeContext = 2,
        DefaultLabels = 4,
        Label = 8,
        DefaultRoles = 16,
        Role = 32,
        Dashboards = 64,
        DashboardData = 128
    }

    [Flags]
    public enum UserTypeContextLoadInstructions
    {
        None = 1,
        PublishingGroupResource = 2,
        PublishingGroupResourceData = 4,
        Resource = 8
    }
    #endregion

    #region Media

    public enum MediaLoadInstruction
    {
        None = 1,
        Data = 2,
        MediaLabel = 4,
        Label = 8,
        MediaVersions = 16,
        MediaVersionsData = 32,
        MediaPolicies = 64,
        Roles = 128,
        AllTranslated = 256
    }

    [Flags]
    public enum MediaFolderLoadInstruction
    {
        None = 1,
        FolderFile = 2,
        FolderFileMedia = 4,
        MediaFolderPolicies = 8,
        FolderFileMediaPolicies = 16,
        Roles = 32,
        AllTranslated = 64,
    }

    [Flags]
    public enum MediaFolderFileLoadInstruction
    {
        None = 1,
        Folder = 2,
        Media = 4,
        MediaData = 8,
        MediaLabels = 16,
        MediaVersions = 32,
        MediaVersionsData = 64,
        MediaPolicies = 128,
        MediaRoles = 256,
        AllTranslated = 512
    }

    [Flags]
    public enum MediaVersionLoadInstructions
    {
        None = 1,
        Data = 2
    }

    [Flags]
    public enum MediaViewingLoadInstruction
    {
        None = 1,
        PageViews = 2,
        Users = 4,
        Media = 8
    }

    public enum MediaPreviewSize
    {
        Small,
        Medium
    }

    #endregion

    #region Article
    [Flags]
    public enum ArticleLoadInstructions
    {
        None = 1,                               // Load only the Article record
        PrimaryFields = 2                       // Load the Article record + its primary fields
    }
    #endregion

    public enum GeoLocationStatus
    {
        Success = 0,
        FailurePermissionDenied = 1,
        FailurePositionUnavailable = 2,
        FailureTmeout = 3,
        Unknown = -1
    }


    [Flags]
    public enum LabelFor
    {
        None = 0,
        Users = 1,
        Media = 2,
        Resources = 4,
        Questions = 8,
        Tasks = 16
    }

    [Flags]
    public enum CompanyLoadInstructions
    {
        None = 1,
        CountryState = 2
    }

    [Flags]
    public enum ReportLoadInstructions
    {
        None = 1,
        Data = 2,
        Roles = 4
    }

    public enum ReportType
    {
        Telerik = 0,
        PowerBI = 1,
        JsReport = 2
    }

    [Flags]
    public enum HierarchyLoadInstructions
    {
        None = 1,
        Rosters = 2
    }


    [Flags]
    public enum HierarchyBucketLoadInstructions
    {
        None = 1,
        BucketRole = 2,
        User = 4,
        Overrides = 8,
        OverridesUsers = 16,
        BucketLabel = 32,
        Labels = 64,
        Hierarchy = 128,
        HierarchyRosters = 256
    }

    #region Project
    public enum RosterEndOfDayDefaultStatus
    {
        /// <summary>
        /// The task status should default to the FinishedByManagement status
        /// </summary>
        FinishedByManagenent = 0,
        /// <summary>
        /// The task should be left in a state where it continues on to the same user it was assigned to
        /// for them to complete the next time they work in that role
        /// </summary>
        ContinueNextShiftForUser = 1
    }

    public enum ScheduleApprovalDefaultStatus
    {
        /// <summary>
        /// The task status should default to Unapproved
        /// </summary>
        Unapproved = 0,
        /// <summary>
        /// The task status should default to Approved
        /// </summary>
        Approved = 1,
    }

    public enum ProjectJobTaskStatus
    {
        /// <summary>
        /// Default state for a task, not approved
        /// </summary>
        Unapproved = 0,
        /// <summary>
        /// Approved to be completed, but not underway yet
        /// </summary>
        Approved = 1,
        /// <summary>
        /// The task has been started by the user
        /// </summary>
        Started = 2,
        /// <summary>
        /// The task has been clocked out of
        /// </summary>
        Paused = 3,
        /// <summary>
        /// The task has been completed as defined
        /// </summary>
        FinishedComplete = 4,
        /// <summary>
        /// The task has been completed, but there were some apsects of it left incomplete
        /// </summary>
        FinishedIncomplete = 5,
        /// <summary>
        /// The task has been cancelled by admin
        /// </summary>
        Cancelled = 6,
        /// <summary>
        /// The task has been marked as complete by management rather than by the user assigned the task
        /// </summary>
        FinishedByManagement = 7,
        /// <summary>
        /// The task has been approved to be continued at the next shift for the owner as a part of the end of day process
        /// </summary>
        ApprovedContinued = 8,
        /// <summary>
        /// The task has been marked as needing to be completed in another roster. There is a change request waiting to be accepted
        /// in the other roster
        /// </summary>
        ChangeRosterRequested = 9,
        /// <summary>
        /// The task has been rejected as being handed across into the requested roster.
        /// </summary>
        ChangeRosterRejected = 10,
        /// <summary>
        /// The task has been accepted as being handed across into the requested roster. A new task will be created to
        /// represent the task in the new roster and the two tasks placed in a group
        /// </summary>
        ChangeRosterAccepted = 11,
        /// <summary>
        /// The task has been marked as complete by the system rather than by end of day performed by a user or an ESW finishing a task
        /// </summary>
        FinishedBySystem = 12
    }

    public class ProjectJobTaskStatusGroups
    {
        /// <summary>
        /// The set of status that mark a task as cancelled
        /// </summary>
        public static ProjectJobTaskStatus[] CancelledStates = new ProjectJobTaskStatus[] {
            ProjectJobTaskStatus.Cancelled
        };
        /// <summary>
        /// The set of status that mark a task as finished
        /// </summary>
        public static ProjectJobTaskStatus[] FinishedStates = new ProjectJobTaskStatus[] {
            ProjectJobTaskStatus.FinishedByManagement,
            ProjectJobTaskStatus.FinishedBySystem,
            ProjectJobTaskStatus.FinishedComplete,
            ProjectJobTaskStatus.FinishedIncomplete
        };

        /// <summary>
        /// The set of status that mark a task as approved
        /// </summary>
        public static ProjectJobTaskStatus[] ApprovedStates = new ProjectJobTaskStatus[] {
            ProjectJobTaskStatus.Approved,
            ProjectJobTaskStatus.ApprovedContinued
        };

        public static ProjectJobTaskStatus[] LiveStates = new ProjectJobTaskStatus[] {
            ProjectJobTaskStatus.Approved,
            ProjectJobTaskStatus.ApprovedContinued,
            ProjectJobTaskStatus.ChangeRosterAccepted,
            ProjectJobTaskStatus.ChangeRosterRejected,
            ProjectJobTaskStatus.ChangeRosterRequested,
            ProjectJobTaskStatus.Paused,
            ProjectJobTaskStatus.Started
        };

        public static ProjectJobTaskStatus[] NotStarted = new ProjectJobTaskStatus[] {
            ProjectJobTaskStatus.Unapproved,
            ProjectJobTaskStatus.Approved,
            ProjectJobTaskStatus.ApprovedContinued
        };
    }

    [Flags]
    public enum ProjectJobTaskLoadInstructions
    {
        None = 1,
        Users = 2,
        WorkLog = 4,
        WorkLogUser = 8,
        TaskType = 0x10,
        TaskTypePublishedResources = 0x20,
        WorkingDocuments = 0x40,
        Attachments = 0x80,
        Signature = 0x100,
        Roster = 0x200,
        WorkloadingActivities = 0x400,
        TaskTypeDocumentation = 0x800,
        TaskTypeDocumentationFolder = 0x1000,
        TaskTypeFinishedStatuses = 0x2000,
        TaskTypeStatuses = 0x4000,
        TaskTypeRelationships = 0x8000,
        Labels = 0x10000,
        AllTranslated = 0x20000,
        Media = 0x40000,
        Site = 0x80000,
        Order = 0x100000,
        Asset = 0x200000
    }

    [Flags]
    public enum ProjectJobTaskChangeRequestLoadInstructions
    {
        None = 1,
        Task = 2,
        Rosters = 4,
        Users = 8,
        TaskType = 16,
        TaskUsers = 32,
        TaskRole = 64
    }

    public enum ProjectJobTaskChangeRequestStatus
    {
        Requested = 0,
        Rejected = 1,
        Accepted = 2
    }

    [Flags]
    public enum ProjectJobTaskTypeLoadInstructions
    {
        None = 1,
        PublishedResources = 2,
        Media = 4,
        MediaData = 8,
        Documentation = 16,
        DocumentationFolders = 32,
        FinishedStatusses = 64,
        Statusses = 128,
        Relationships = 256,
        FacilityOverrides = 512,
        FacilityOverrideStructures = 1024,
        RelatedUserTypes = 2048
    }

    public enum ProjectJobTaskTypeReferenceNumberMode
    {
        Hidden = 0,
        Shown = 1,
        Mandatory = 2
    }

    public enum ProjectJobTaskTypePhotoSupport
    {
        /// <summary>
        /// Photo's not supported
        /// </summary>
        None = 0,
        /// <summary>
        /// Photo's can be uploaded
        /// </summary>
        Optional = 1,
        /// <summary>
        /// At least 1 photo must be uploaded
        /// </summary>
        Mandatory = 2
    }

    [Flags]
    public enum TimesheetItemLoadInstructions
    {
        None = 1,
        Roster = 2,
        Users = 4,
        Worklogs = 8
    }

    [Flags]
    public enum RosterLoadInstructions
    {
        None = 1,
        Conditions = 2,
        ConditionActions = 4,
        ScheduleDays = 8
    }

    public enum RosterTimesheetRoundingMethod
    {
        None = 0,
        Down = 1,
        Up = 2,
        Nearest = 3,
        Shortest = 4,
        Longest = 5
    }

    public enum RosterTimesheetSelectionMethod
    {
        ActualTimes = 0,
        ScheduledTimes = 1,
        Minimised = 2,
        Maximised = 3
    }

    [Flags]
    public enum ProjectJobScheduledTaskLoadInstructions
    {
        None = 1,
        PublishedChecklists = 2,
        UserLabels = 4,
        CalendarRule = 8,
        Attachments = 16,
        ScheduleLabels = 32,
        Labels = 64,
        AllTranslated = 128,
        TaskType = 256,
        Users = 512,
        Roster = 1024,
        HierarchyRole = 2048,
        UsersWorkloadingFeatures = 4096,
        UsersWorkloadingFeaturesUnits = 8192,
        UsersWorkloadingFeaturesAreas = 16384,
        WorkloadingActivities = 32768,
        WorkloadingActivitiesSiteFeatures = 65536,
        WorkloadingActivitiesStandards = 131072,
        AuditSampling = 262144
    }

    [Flags]
    public enum ProjectJobTaskWorkLogLoadInstructions
    {
        None = 1,
        User = 2,
        Task = 4,
        TaskSite = 8
    }

    [Flags]
    public enum CalendarEventLoadInstructions
    {
        None = 1,
        Rules = 2
    }

    public enum CalendarRuleFrequency
    {
        Single,
        Daily,
        Cron
    }

    [Flags]
    public enum ProjectJobTaskWorkingDocumentLoadInstructions
    {
        None = 1,
        Task = 2,
        WorkingDocument = 4,
        PublishingGroupResource = 8
    }

    [Flags]
    public enum WorkLoadingBookLoadInstructions
    {
        None = 0,
        Standard = 1,
        StandardActivities = 2,
        StandardFeatures = 4
    }

    [Flags]
    public enum WorkLoadingActivityLoadInstructions
    {
        None = 0,
    }

    [Flags]
    public enum WorkLoadingFeatureLoadInstructions
    {
        None = 0,
    }

    [Flags]
    public enum WorkLoadingStandardLoadInstructions
    {
        None = 0,
        Books = 1,
        Features = 2,
        Activities = 4,
        Units = 8
    }

    [Flags]
    public enum MeasurementUnitLoadInstructions
    {
        None = 0,
    }

    [Flags]
    public enum SiteAreaLoadInstructions
    {
        None = 0,
        Site = 1,
        SiteFeature = 2
    }

    [Flags]
    public enum SiteFeatureLoadInstructions
    {
        None = 0,
        Site = 1,
        Area = 2,
        Feature = 4,
        Units = 8
    }
    #endregion
    [Flags]
    public enum ReportTicketLoadInstructions
    {
        None = 0,
        Report = 1
    }

    [Flags]
    public enum VersionLoadInstructions
    {
        None = 0
    }

    [Flags]
    public enum UserBillingTypeLoadInstructions
    {
        None = 0,
        MembershipType = 1,
        AndMembership = 2
    }

    [Flags]
    public enum InvoiceLoadInstructions
    {
        None = 0,
        LineItem = 1
    }

    [Flags]
    public enum InvoiceLineItemLoadInstructions
    {
        None = 0,
        Invoice = 1
    }

    [Flags]
    public enum AssetTrackerBeaconLoadInstructions
    {
        None = 0,
        StaticLocation = 1,
        FacilityStructure = 2,
        LastGpsLocation = 4,

        User = 8,
        TaskType = 16
    }

    [Flags]
    public enum TabletLoadInstructions
    {
        None = 0,
        TabletProfile = 1,
        TabletUUID = 2
    }

    [Flags]
    public enum TabletUuidLoadInstructions
    {
        None,
        Tablet = 1,
        AndTabletProfile = 2,
    }

    [Flags]
    public enum TabletProfileLoadInstructions
    {
        None = 0,
    }

    [Flags]

    public enum AssetLoadInstructions
    {
        None = 0,
        AssetType = 1,
        AssetTypeContextPublishedResource = 2,
        Beacon = 4,
        ContextData = 8,
        PublishingGroupResource = 16,
        PublishingGroupResourceData = 32,
        Schedule = 64,
        ScheduleCalendar = 128
    }

    [Flags]
    public enum AssetScheduleLoadInstructions
    {
        None = 0,
        Calendar = 1,
        Asset = 2
    }
    [Flags]
    public enum AssetTypeLoadInstructions
    {
        None = 0,
        AssetTypeContext = 1,
        Calendar = 2,
        CalendarTask = 4
    }

    [Flags]
    public enum AssetTypeCalendarLoadInstructions
    {
        None = 0,
        CalendarTask = 1
    }

    [Flags]
    public enum AssetTypeCalendarTaskLoadInstructions
    {
        None = 0,
        Calendar = 1,
        Task = 2,
        HierarchyRole = 4,
        Labels = 8
    }

    [Flags]
    public enum FacilityStructureLoadInstructions
    {
        None,
        Parent = 1,
        ParentDeep = 2,
        Children = 4,
        ChildrenDeep = 8,
        Address = 16,
        UserData = 32,
        FloorPlan = 64,
        ProductCatalogs = 128,
        ProductCatalogItems = 256,
        ProductCatalogItemProducts = 512,
        ProductCatalogItemProductHierarchies = 1024,
        ProductCatalogItemProductTypes = 2048
    }

    [Flags]
    public enum ProductLoadInstructions
    {
        None,
        ChildProductHierarchy = 1,
        ParentProductHierarchy = 2,
    }

    [Flags]
    public enum GpsLocationLoadInstructions
    {
        None
    }

    [Flags]
    public enum ProductCatalogItemLoadInstructions
    {
        None,
        Product = 1
    }

    [Flags]
    public enum OrderLoadInstructions
    {
        None,
        Items = 1,
        ItemProducts = 2
    }

    public enum UserInterfaceType
    {
        ClockInClockOut = 0,
        Desktop = 1,
        SaveFinish = 2,
        Ordering = 3,
        Hidden = 4
    }

    [Flags]
    public enum ESProbeLoadInstructions
    {
        None = 0,
        Sensors = 1
    }

    [Flags]
    public enum ESSensorLoadInstructions
    {
        None = 0,
        Probe = 1,
        Readings = 2
    }

    [Flags]
    public enum ESSensorReadingsLoadInstructions
    {
        None = 0,
        Sensor = 1,
        SensorDevice = 2
    }

    [Flags]
    public enum QRCodeLoadInstructions
    {
        None = 0,
        User = 1,
        TaskType = 2,
        Asset = 4
    }

    [Flags]
    public enum PEJobLoadInstructions
    {
        None = 0,
        History = 1
    }

    [Flags]
    public enum PendingPaymentLoadInstructions
    {
        None = 0,
        PaymentResponses = 1,
    }

    [Flags]
    public enum PaymentResponseLoadInstructions
    {
        None = 0,
        PendingPayment = 1
    }
    
    [Flags]
    public enum GatewayCustomerLoadInstructions
    {
        None = 0,
    }

}
