﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.RepositoryInterfaces.Workloading
{
    public interface IWorkloadingDurationCalculator
    {
        /// <summary>
        /// Populates the estimated durartion property of the activity based on its properties
        /// </summary>
        /// <param name="activity">Activity for which the duration should be estimated</param>
        void EstimateDuration(IWorkloadingActivity activity);

        void EstimateDuration(IList<IWorkloadingActivity> activities);

        /// <summary>
        /// Populates the estimated duration property of the task and its activities.
        /// </summary>
        /// <param name="task">Task for which the duration should be estiamted</param>
        void EstimateDuration(IWorkloadingTask task);
    }
}
