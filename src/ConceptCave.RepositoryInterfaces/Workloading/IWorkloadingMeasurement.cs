﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.RepositoryInterfaces.Workloading
{
    /// <summary>
    /// Interface to model a measurment in work loading. This models both the value (size)
    /// of the measurement and the units used to do the measurment (square feet etc).
    /// </summary>
    public interface IWorkloadingMeasurement
    {
        decimal Value { get; set; }

        decimal AsMetric();

        IWorkloadingMeasurementUnit Unit { get; set; }

        object Data { get; set; }
    }
}
