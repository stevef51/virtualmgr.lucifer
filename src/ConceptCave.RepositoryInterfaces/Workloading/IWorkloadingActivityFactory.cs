﻿using ConceptCave.DTO.DTOClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.RepositoryInterfaces.Workloading
{
    public interface IWorkloadingActivityFactory
    {
        IList<IWorkloadingActivity> NewForSite(Guid standardId, Guid siteId);

        IList<IWorkloadingActivity> NewForSite(WorkLoadingStandardDTO standard, UserDataDTO site);

        IWorkloadingActivity NewForFeature(Guid standardId, Guid siteFeatureId);

        bool ShouldCreate(ProjectJobScheduledTaskWorkloadingActivityDTO activity, DateTime localDate, TimeZoneInfo timezone);
    }
}
