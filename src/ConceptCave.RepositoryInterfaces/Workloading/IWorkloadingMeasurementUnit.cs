﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.RepositoryInterfaces.Workloading
{
    public interface IWorkloadingMeasurementUnit
    {
        /// <summary>
        /// The name of the unit of measure, for example feet
        /// </summary>
        string Name { get; set; }
        /// <summary>
        /// The conversion multiplier to convert to a metric unit from this unit type
        /// </summary>
        decimal PerMetric { get; set; }
        /// <summary>
        /// The number of dimensions modelled by this unit. So for area it would be 2, volumne 3 etc
        /// </summary>
        int Dimensions { get; set; }
    }
}
