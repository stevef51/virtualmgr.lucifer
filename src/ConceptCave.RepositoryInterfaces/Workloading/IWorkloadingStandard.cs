﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.RepositoryInterfaces.Workloading
{
    public interface IWorkloadingStandard
    {
        /// <summary>
        /// The length of time in seconds to complete the task per measurement value
        /// </summary>
        decimal Seconds { get; set; }

        /// <summary>
        /// The measurement associated with the number of seconds. So for example this 
        /// maybe 10 square metres etc.
        /// </summary>
        IWorkloadingMeasurement Measurement { get; set; }

        /// <summary>
        /// Calculates out the number of seconds taken to perform the standard per single
        /// metric unit. So for example if it was area we'd get the number of seconds per
        /// square metre out of this.
        /// </summary>
        /// <returns>Number of seconds</returns>s
        decimal SecondsPerSingleMetricUnit();

        object Data { get; set; }
    }
}
