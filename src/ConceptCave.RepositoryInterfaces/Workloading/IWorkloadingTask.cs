﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.RepositoryInterfaces.Workloading
{
    public interface IWorkloadingTask
    {
        object Data { get; set; }
        IList<IWorkloadingActivity> Activities { get; set; }

        decimal EstimatedDuration { get; set; }
    }
}
