﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public interface IHierarchyRepository
    {
        IList<HierarchyDTO> GetAllHierarchies(HierarchyLoadInstructions instructions = HierarchyLoadInstructions.None);
        HierarchyDTO GetHierarchy(int id);
        HierarchyDTO GetHierarchy(string name);

        IList<HierarchyDTO> Search(string name, int page, int itemsPerPage, ref int totalItems);

        IList<HierarchyRoleDTO> GetRolesByName(string name);
        IList<HierarchyRoleDTO> GetRoles();
        HierarchyRoleDTO GetRoleByName(string name);
        HierarchyRoleDTO GetRoleById(int id);

        IList<HierarchyBucketDTO> GetBucketsForHierarchy(int hierarchyId, HierarchyBucketLoadInstructions instructions);
        IList<HierarchyBucketDTO> GetBucketsForHierarchy(int hierarchyId, int? roleId, HierarchyBucketLoadInstructions instructions);
        IList<HierarchyBucketDTO> GetBucketsForUser(Guid userId, bool? isPrimary, HierarchyBucketLoadInstructions instructions);
        IList<HierarchyBucketDTO> GetBucketsForUser(Guid userId, int? hierarchyId, bool? isPrimary, HierarchyBucketLoadInstructions instructions, DateTime? date = null);

        IList<HierarchyDTO> GetHierarchiesForUser(Guid userId);

        HierarchyBucketDTO GetPrimaryEntity(Guid userId);

        HierarchyBucketDTO GetRootBucket(int hierarchyId);

        HierarchyBucketDTO GetById(int id);
        HierarchyBucketDTO GetById(int id, HierarchyBucketLoadInstructions instructions, DateTime? date = null);
        HierarchyBucketDTO Save(HierarchyBucketDTO bucket, bool refetch, bool recurse);
        HierarchyDTO Save(HierarchyDTO hierarchy, bool refetch, bool recurse);

        HierarchyBucketOverrideDTO Save(HierarchyBucketOverrideDTO hierarchy, bool refetch, bool recurse);

        HierarchyRoleDTO Save(HierarchyRoleDTO role, bool refetch, bool recurse);

        void DeleteHierarchyBucketOverride(int id);

        /// <summary>
        /// Retrieves any bucket overrides that span the date for the user
        /// </summary>
        /// <param name="date">date spanned by override</param>
        /// <param name="userId">UserId for which overrides are to be retrieved</param>
        /// <returns>List of overrides that span the date</returns>
        IList<HierarchyBucketOverrideDTO> GetBucketOverrides(DateTime date, Guid userId, int hierarchyBucketId);
        IList<HierarchyBucketOverrideDTO> GetBucketOverrides(DateTime startDate, DateTime endDate, Guid userId, int hierarchyBucketId);

        HierarchyBucketOverrideDTO GetBucketOverride(int id);

        IList<HierarchyBucketDTO> GetSubTreeBuckets(int hierarchyId, int left, int right, HierarchyBucketLoadInstructions instructions, DateTime? date = null, bool inclusiveOfLeftAndRight = false);

        IList<HierarchyBucketDTO> GetAllDirectDescendantEntities(int parentBucketId);
        HierarchyBucketDTO GetPrimaryParentEntityPunchThrough(Guid userId);
        HierarchyBucketDTO LoadInMemory(int id);
        void SetTreeNumbering(HierarchyBucketDTO root);
        void DumpAndSave(HierarchyBucketDTO newRoot);

        void AddBucketLabels(HierarchyBucketLabelDTO[] labels);
        void RemoveBucketLabels(HierarchyBucketLabelDTO[] labels);

        IList<MergedTeamHierarchyWithLabelDTO> GetMergedHierarchyBuckets(int bucketId, bool includePrimaryRecord);
        HierarchyBucketDTO GetParentDeep(Guid userId, int depth, int hierarchyId);
        IEnumerable<HierarchyBucketDTO> GetBucketsWithLabel(Guid labelId, HierarchyBucketLoadInstructions loadInstructions = HierarchyBucketLoadInstructions.None);
    }
}
