﻿using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public interface IWorkLoadingBookRepository
    {
        WorkLoadingBookDTO GetById(Guid id, WorkLoadingBookLoadInstructions instructions);
        IList<WorkLoadingBookDTO> GetAllBooks(WorkLoadingBookLoadInstructions instructions);
        IList<WorkLoadingBookDTO> Search(string nameLike, int? pageNumber, int? itemsPerPage, bool includeArchived, ref int totalItems, WorkLoadingBookLoadInstructions instructions);

        WorkLoadingBookDTO Save(WorkLoadingBookDTO dto, bool refetch, bool recurse);
        void DeleteBook(Guid id);

        WorkLoadingStandardDTO GetStandard(Guid bookId, Guid featureId, Guid activityId, WorkLoadingStandardLoadInstructions instructions);
        WorkLoadingStandardDTO GetStandardById(Guid id, WorkLoadingStandardLoadInstructions instructions);
        WorkLoadingStandardDTO SaveStandard(WorkLoadingStandardDTO dto, bool refetch, bool recurse);

        IList<WorkLoadingStandardDTO> GetStandardsForFeatures(Guid[] featureids, WorkLoadingStandardLoadInstructions instructions);

        void DeleteStandard(Guid id, bool archiveIfRequired, out bool archived);
    }
    
    public interface IWorkLoadingActivityRepository
    {
        WorkLoadingActivityDTO GetById(Guid id, WorkLoadingActivityLoadInstructions instructions);

        IList<WorkLoadingActivityDTO> GetAllActivities(WorkLoadingActivityLoadInstructions instructions);

        WorkLoadingActivityDTO Save(WorkLoadingActivityDTO dto, bool refecth, bool recurse);

        IList<WorkLoadingActivityDTO> Search(string nameLike, int? pageNumber, int? itemsPerPage, bool includeArchived, ref int totalItems, WorkLoadingActivityLoadInstructions instructions);
        void Delete(Guid id, bool archiveIfRequired, out bool archived);
    }

    public interface IWorkLoadingFeatureRepository
    {
        WorkLoadingFeatureDTO GetById(Guid id, WorkLoadingFeatureLoadInstructions instructions);

        IList<WorkLoadingFeatureDTO> GetAll(WorkLoadingFeatureLoadInstructions instructions);

        WorkLoadingFeatureDTO Save(WorkLoadingFeatureDTO dto, bool refecth, bool recurse);

        IList<WorkLoadingFeatureDTO> Search(string nameLike, int? pageNumber, int? itemsPerPage, bool includeArchived, ref int totalItems, WorkLoadingFeatureLoadInstructions instructions);

        void Delete(Guid id, bool archiveIfRequired, out bool archived);
    }

    public interface ISiteFeatureRepository 
    {
        SiteFeatureDTO GetFeatureById(Guid siteFeatureId, SiteFeatureLoadInstructions instructions);
        SiteAreaDTO GetAreaById(Guid siteAreaId, SiteAreaLoadInstructions instructions);
        IList<SiteAreaDTO> GetAreasBySiteId(Guid siteId, SiteAreaLoadInstructions instructions);
        IList<SiteAreaDTO> GetAllAreas(SiteAreaLoadInstructions instructions);
        IList<SiteFeatureDTO> GetFeaturesBySiteId(Guid siteId, SiteFeatureLoadInstructions instructions);
        SiteFeatureDTO Save(SiteFeatureDTO dto, bool refetch, bool recurse);
        SiteAreaDTO Save(SiteAreaDTO dto, bool refetch, bool recurse);
        IList<UserDataDTO> SearchSites(string like, int? pageNumber, int? itemsPerPage, ref int totalItems);

        void DeleteSiteFeature(Guid siteFeatureId);
        void DeleteSiteArea(Guid siteAreaId);
    }
}
