﻿using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public interface IOrderRepository
    {
        OrderDTO GetById(Guid id, OrderLoadInstructions instructions);
        void Remove(Guid id);

        void RemoveItems(Guid[] ids);

        OrderDTO Save(OrderDTO dto, bool refetch, bool recurse);
    }
}
