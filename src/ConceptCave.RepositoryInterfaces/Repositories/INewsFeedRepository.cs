﻿using ConceptCave.DTO.DTOClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public interface INewsFeedRepository
    {
        IList<NewsFeedDTO> Get(string channel, string program, DateTime? startDate, DateTime? endDate, int count);
        IList<NewsFeedDTO> Get(string[] channels, DateTime? validTo, int count);

        NewsFeedDTO Save(NewsFeedDTO item, bool refetch, bool recurse);

        IList<string> AvailableChannels();
        IList<string> AvailableProgramsForChannel(string channel);
    }
}
