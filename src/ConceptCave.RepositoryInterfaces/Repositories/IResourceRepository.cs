using System;
using System.Collections.Generic;
using ConceptCave.Core;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    [Flags]
    public enum ResourceLoadInstructions
    {
        None = 1,
        Data = 2,
        ResourceLabel = 4,
        Label = 8
    }

    public enum StandardResourceCategories
    {
        Questions,
        Sections,
        Checklists,
        Actions
    }

    public interface IResourceRepository
    {
        IList<ResourceDTO> GetLikeName(string name, string category, ResourceLoadInstructions instructions, int pageNumber = 1, int pageSize = 20);
        ResourceDTO GetById(int id, ResourceLoadInstructions instructions);

        ResourceDTO GetByNodeId(Guid id, ResourceLoadInstructions instructions);

        void Remove(Guid id);

        ResourceDTO Save(ResourceDTO resource, bool refetch, bool recurse);

        void Save(IResource resource, ResourceDTO resourceEntity, bool refetch, bool recurse);
        void RemoveFromLabels(Guid[] labelIds, int id);
    }
}
