
using System.Collections.Generic;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public interface ICompletedLabelWorkingDocumentDimensionRepository
    {
        void Save(IEnumerable<CompletedLabelWorkingDocumentDimensionDTO> items, bool refetch, bool recurse);
    }
}
