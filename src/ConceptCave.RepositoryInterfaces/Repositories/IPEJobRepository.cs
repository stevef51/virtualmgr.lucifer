﻿using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public interface IPEJobRepository
    {
        PEJobDTO GetById(int id, PEJobLoadInstructions instructions = PEJobLoadInstructions.None);
        PEJobDTO Save(PEJobDTO job, bool refetch);

        void Delete(int id, bool archiveIfRequired, out bool archived);

        PEJobHistoryDTO Save(PEJobHistoryDTO history, bool refetch);

        IList<PEJobDTO> FindOverdue(DateTime dueUtc, PEJobLoadInstructions instructions = PEJobLoadInstructions.None);

        IList<PEJobDTO> GetAll(PEJobLoadInstructions instructions = PEJobLoadInstructions.None);

        IList<PEJobDTO> Search(string nameLike, int? pageNumber, int? itemsPerPage, bool includeArchived, ref int totalItems);

    }
}
