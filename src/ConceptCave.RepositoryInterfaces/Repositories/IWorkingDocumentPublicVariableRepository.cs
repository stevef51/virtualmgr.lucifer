using System;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public interface IWorkingDocumentPublicVariableRepository
    {
        void InsertOrUpdate(Guid workingDocumentId, string variableName, string variableValue, bool? variablePassFail, decimal? variableScore);
    }
}
