﻿using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public interface ITabletProfileRepository
    {
        IList<TabletProfileDTO> GetAll(TabletProfileLoadInstructions instructions = TabletProfileLoadInstructions.None);

        TabletProfileDTO GetTabletProfileById(int id, TabletProfileLoadInstructions instructions);

        TabletProfileDTO SaveTabletProfile(TabletProfileDTO dto, bool refetch, bool recurse);

        IList<TabletProfileDTO> Search(string nameLike, int? pageNumber, int? itemsPerPage, bool includeArchived, ref int totalItems);

        void Delete(int id, bool archiveIfRequired, out bool archived);

    }
}
