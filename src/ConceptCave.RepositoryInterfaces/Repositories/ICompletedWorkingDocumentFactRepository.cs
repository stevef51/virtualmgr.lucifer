using System;
using System.Collections.Generic;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    [Flags]
    public enum CompletedWorkingDocumentFactLoadInstructions
    {
        None = 1,
        CompletedWorkingDocumentDimension = 2,
        CompletedUserDimension = 4,
        CompletedLabelDimension = 8
    }

    public interface ICompletedWorkingDocumentFactRepository
    {
        void Save(CompletedWorkingDocumentFactDTO dto, bool refetch, bool recurse);

        CompletedWorkingDocumentFactDTO Get(Guid workingDocumentId, CompletedWorkingDocumentFactLoadInstructions instructions);
        IList<CompletedWorkingDocumentFactDTO> Get(DateTime startDate, DateTime endDate, CompletedWorkingDocumentFactLoadInstructions instructions);
    }
}
