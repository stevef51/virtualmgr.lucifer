﻿using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public interface IESProbeRepository
    {
        ESProbeDTO GetById(int id, ESProbeLoadInstructions instructions = ESProbeLoadInstructions.None);
        ESProbeDTO GetByKey(string key, ESProbeLoadInstructions instructions = ESProbeLoadInstructions.None);
        ESProbeDTO GetByModelSerialNumber(string model, string serialNumber, ESProbeLoadInstructions instructions = ESProbeLoadInstructions.None);

        ESProbeDTO Save(ESProbeDTO dto, bool refetch, bool recurse);

        IList<ESProbeDTO> Search(string nameLike, int? pageNumber, int? itemsPerPage, bool includeArchived, ref int totalItems, int? searchFlags, int? includeFlags);

        void Delete(int id, bool archiveIfRequired, out bool archived);
    }
}
