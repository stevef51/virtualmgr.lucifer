﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public interface IRosterRepository
    {
        IList<RosterDTO> GetAll();

        IList<RosterDTO> GetAll(RosterLoadInstructions instructions);

        RosterDTO FindByName(string name, RosterLoadInstructions instructions);

        IList<RosterDTO> GetForHierarchies(int[] ids, RosterLoadInstructions instructions = RosterLoadInstructions.None);

        IList<RosterDTO> GetPendingAutomaticScheduleApprovalRosters(DateTime localTime, RosterLoadInstructions instructions = RosterLoadInstructions.None);
        IList<RosterDTO> GetPendingAutomaticEndOfDayRosters(DateTime localTime, RosterLoadInstructions instructions = RosterLoadInstructions.None);

        RosterDTO GetById(int id, RosterLoadInstructions instructions = RosterLoadInstructions.None);

        RosterDTO Save(RosterDTO roster, bool refetch, bool recurse);

        IList<RosterProjectJobTaskEventDTO> GetAllTaskEventTypes();

        void RemoveRosterEventConditions(int[] ids);
        void RemoveRosterEventConditionActions(int[] ids);
    }
}
