﻿using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public enum PendingPaymentStatus
    {
        Inactive = 0,
        DueNow = 1,
        Scheduled = 2,
        Paid = 3,
        Cancelled = 4,
        Processing = 5
    }

    public interface IPendingPaymentRepository
    {
        PendingPaymentDTO GetPendingPaymentById(Guid id, PendingPaymentLoadInstructions instructions = PendingPaymentLoadInstructions.None);

        PendingPaymentDTO Save(PendingPaymentDTO dto, bool refetch, bool recurse);
        IEnumerable<PendingPaymentDTO> Save(IEnumerable<PendingPaymentDTO> dtos, bool refetch, bool recurse);

        IList<PendingPaymentDTO> FindScheduled(string gatewayName, DateTime fromUtc, PendingPaymentLoadInstructions instructions = PendingPaymentLoadInstructions.None);
        IList<PendingPaymentDTO> GetByOrderId(Guid orderId, PendingPaymentLoadInstructions instructions = PendingPaymentLoadInstructions.None);
        IList<PendingPaymentDTO> FindProcessing(string gatewayName);
    }
}
