using System;
using System.Collections.Generic;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public enum PublishingGroupActorLoadInstruction
    {
        None = 1,
        Membership = 2,
        PublishingGroup = 4,
        Resource = 8
    }

    public interface IPublishingGroupActorRepository
    {
        PublishingGroupActorDTO Save(PublishingGroupActorDTO publish, bool refetch, bool recurse);

        IList<PublishingGroupActorDTO> GetForMembershipAndType(Guid membershipId, PublishingGroupActorType type, PublishingGroupActorLoadInstruction instructions);
        PublishingGroupActorDTO GetById(int publishedGroupId, Guid membershipId, PublishingGroupActorType type, PublishingGroupActorLoadInstruction instructions);
        PublishingGroupActorDTO Add(int publishedGroupId, Guid membershipId, PublishingGroupActorType type);

        void Remove(int publishedGroupId, Guid membershipId, PublishingGroupActorType type);
    }
}
