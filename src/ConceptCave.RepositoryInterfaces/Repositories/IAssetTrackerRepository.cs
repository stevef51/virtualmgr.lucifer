﻿using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public enum BeaconType
    {
        Unknown,
        StaticBeacon,
        AssetBeacon
    }

    public enum FMARecordType
    {
        Beacon,
        BestStaticBeacon,
        Task
    }

    public enum BeaconAttachmentType
    {
        UserSite,
        TaskType,
    }

    public class FMARecord
    {
        public FMARecordType RecordType { get; set; }
        public string TabletUUID { get; set; }
        public DateTime TabletUtc { get; set; }
        public string BeaconId { get; set; }
        public int Range { get; set; }
        public DateTime RangeUtc { get; set; }
        public DateTime? FirstRangeUtc { get; set; }
        public Guid? TaskId { get; set; }
        public int? BatteryPercent { get; set; }
    }

    public interface IAssetTrackerRepository
    {
        
        BeaconDTO GetBeacon(string beaconId, AssetTrackerBeaconLoadInstructions loadInstructions);

        BeaconDTO GetBeaconByAssetId(int assetId, AssetTrackerBeaconLoadInstructions loadInstructions);

        BeaconDTO GetBeaconByFacilityStructureId(int facilityStructureId, AssetTrackerBeaconLoadInstructions assetTrackerBeaconLoadInstructions);

        BeaconDTO Save(BeaconDTO dto, bool refetch, bool recurse);

        IList<BeaconDTO> FindUnassignedBeacons();

        IList<BeaconDTO> GetFacilityStructureBeacons(int facilityStructureId, AssetTrackerBeaconLoadInstructions instructions);

        List<BeaconDTO> GetStaticBeacons(IEnumerable<string> beaconIds);

        TabletLocationHistoryDTO Save(TabletLocationHistoryDTO dto, bool refetch, bool recurse);

        TabletLocationHistoryDTO FindLastTabletLocation(string tabletUUID);


        void WriteFMARecords(IEnumerable<FMARecord> records, Dictionary<string, BeaconType> outBeacons);

        IList<BeaconDTO> Search(string like, int? pageNumber, int? itemsPerPage, bool includeArchived, ref int totalItems, int? searchFlags, int? includeFlags, AssetTrackerBeaconLoadInstructions instructions = AssetTrackerBeaconLoadInstructions.None);

    }
}
