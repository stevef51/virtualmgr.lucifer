using System;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public interface IMembershipMediaRepository
    {
        void AddMapping(Guid mediaId, Guid userId);
    }
}
