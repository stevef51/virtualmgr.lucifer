﻿using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public interface IGatewayCustomerRepository
    {
        GatewayCustomerDTO GetById(Guid userId, string gatewayName, GatewayCustomerLoadInstructions instructions = GatewayCustomerLoadInstructions.None);

        GatewayCustomerDTO Save(GatewayCustomerDTO dto, bool refetch, bool recurse);
    }
}
