using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public interface IWorkingDocumentReferenceRepository
    {
        WorkingDocumentReferenceDTO Save(WorkingDocumentReferenceDTO dto, bool refetch, bool recurse);
    }
}
