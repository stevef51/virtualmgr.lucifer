﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public interface IArticleRepository
    {
        void SaveArticle(ArticleDTO article);

        ArticleDTO LoadArticle(Guid articleId, ArticleLoadInstructions loadInstructions = ArticleLoadInstructions.PrimaryFields);
        ArticleDTO LoadArticleWithPropertyNames(Guid articleId, string name);

        IEnumerable<ArticleBoolDTO> LoadArticleBool(Guid articleId, string name, bool? value, int? index);
        IEnumerable<ArticleBigIntDTO> LoadArticleBigInt(Guid articleId, string name, Int64? value, int? index);
        IEnumerable<ArticleStringDTO> LoadArticleString(Guid articleId, string name, string value, int? index);
        IEnumerable<ArticleDateTimeDTO> LoadArticleDateTime(Guid articleId, string name, DateTime? value, int? index);
        IEnumerable<ArticleNumberDTO> LoadArticleNumber(Guid articleId, string name, decimal? value, int? index);
        IEnumerable<ArticleGuidDTO> LoadArticleGuid(Guid articleId, string name, Guid? value, int? index);
        IEnumerable<ArticleSubArticleDTO> LoadArticleSubArticle(Guid articleId, string name, Guid? value, int? index);

        void SaveArticleBool(ArticleBoolDTO dto);
        void SaveArticleBigInt(ArticleBigIntDTO dto);
        void SaveArticleString(ArticleStringDTO dto);
        void SaveArticleDateTime(ArticleDateTimeDTO dto);
        void SaveArticleNumber(ArticleNumberDTO dto);
        void SaveArticleGuid(ArticleGuidDTO dto);
        void SaveArticleSubArticle(ArticleSubArticleDTO dto);

        void DeleteArticlePropertyName(Guid articleId, string name);

        IEnumerable<ArticleDTO> Find(string[] fieldNames, object[] equalsValue);
    }
}
