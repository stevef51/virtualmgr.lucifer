﻿using ConceptCave.DTO.DTOClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public interface IMeasurementUnitRepository
    {
        MeasurementUnitDTO GetById(int id);
        IList<MeasurementUnitDTO> GetAll();
    }
}
