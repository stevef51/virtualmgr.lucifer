﻿using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public enum ProductTypeUnits
    {
        Item = 1,
        Length = 2,
        Area = 3,
        Volume = 4
    }

    public interface IProductRepository
    {
        ProductDTO GetByName(string name);
        ProductDTO GetByCode(string code, bool isCoupon, ProductLoadInstructions instructions = ProductLoadInstructions.None);

        ProductDTO GetById(int id, ProductLoadInstructions instructions = ProductLoadInstructions.None);

        IList<ProductDTO> GetById(int[] id, ProductLoadInstructions instructions = ProductLoadInstructions.None);

        ProductDTO Save(ProductDTO product, bool refetch, bool recurse = false);

        IList<ProductTypeDTO> GetAllProductTypes();

        IList<ProductDTO> Search(string nameLike, bool? isCoupon, int? pageNumber, int? itemsPerPage, bool includeArchived, ref int totalItems);

        void RemoveProductHierarchies(int parentId);
    }
}
