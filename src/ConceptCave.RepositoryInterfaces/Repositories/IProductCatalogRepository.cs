﻿using ConceptCave.DTO.DTOClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public enum ProductCatalogFacilityStructureRuleType
    {
        Inherited,
        Excluded,
        Included
    }

    public interface IProductCatalogRepository
    {
        ProductCatalogDTO GetById(int id);

        ProductCatalogDTO Save(ProductCatalogDTO dto, bool refetch);

        void GetCatalogsForFacilityStructure(int id, IList<ProductCatalogDTO> catalogs, IList<ProductCatalogFacilityStructureRuleDTO> rules);

        void GetCatalogsForFacilityStructure(FacilityStructureDTO structure, IList<ProductCatalogDTO> catalogs, IList<ProductCatalogFacilityStructureRuleDTO> rules);

        void GetCatalogsForSite(Guid id, IList<ProductCatalogDTO> catalogs, IList<ProductCatalogFacilityStructureRuleDTO> rules);
        void Delete(int id, bool archiveIfRequired, out bool archived);

        // Return any Catalogs for a specific FacilityStructure without ascending parents
        IList<ProductCatalogDTO> GetFacilityStructureProductCatalogs(int facilityStructureId);

        void GetCatalogStructureRules(int facilityStructureId, IList<ProductCatalogFacilityStructureRuleDTO> rules, bool recurseUp);

        ProductCatalogFacilityStructureRuleDTO GetCatalogStructureRule(int facilityStructureId, int catalogid);

        ProductCatalogFacilityStructureRuleDTO Save(ProductCatalogFacilityStructureRuleDTO rule, bool refetch);

        void Delete(int facilitystructureId, int catalogid);
    }
}
