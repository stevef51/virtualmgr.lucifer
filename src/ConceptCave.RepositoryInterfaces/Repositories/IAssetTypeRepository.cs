﻿using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public interface IAssetTypeRepository
    {
        AssetTypeDTO GetById(int id, AssetTypeLoadInstructions instructions = AssetTypeLoadInstructions.None);

        AssetTypeDTO SaveAssetType(AssetTypeDTO dto, bool refetch, bool recurse);

        IList<AssetTypeDTO> Search(string nameLike, int? pageNumber, int? itemsPerPage, bool includeArchived, ref int totalItems);

        void RemoveContextsFromAssetType(int[] publishingGroupResourceId, int assetTypeId);
        IList<AssetTypeContextPublishedResourceDTO> AddContextsToAssetType(int[] publishingGroupResources, int assetTypeId);

        void Delete(int id, bool archiveIfRequired, out bool archived);
    }
}
