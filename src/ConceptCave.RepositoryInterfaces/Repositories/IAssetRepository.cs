﻿using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public interface IAssetRepository
    {
        AssetDTO GetById(int id, AssetLoadInstructions instructions = AssetLoadInstructions.None);

        AssetDTO SaveAsset(AssetDTO dto, bool refetch, bool recurse);

        IList<AssetDTO> Search(string nameLike, int? pageNumber, int? itemsPerPage, bool includeArchived, ref int totalItems);

        void Delete(int id, bool archiveIfRequired, out bool archived);

        AssetContextDataDTO CreateOrUpdateAssetCtxEntry(int assetId, int publishingGroupResourceId, Guid workingDocumentId);

        IList<AssetDTO> GetByAssetType(int assetTypeId, AssetLoadInstructions instructions = AssetLoadInstructions.None);

        IList<Tuple<AssetDTO, AssetTypeCalendarDTO>> GetAssetsWithMissingCalendarSchedules();
    }
}
