﻿using System;
using System.Collections.Generic;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public interface IPostedFile
    {
        string FileName { get; }
        string ContentType { get; }
        long ContentLength { get; }
        long Read(byte[] data, long offset, long length);
    }

    public interface IMediaPreviewGenerator
    {
        byte[] Generate(MediaPreviewSize size);
    }

    public interface IMediaRepository
    {
        MediaDTO GetById(Guid id, MediaLoadInstruction instructions);
        MediaDTO GetByFileNameAndType(string filename, string mimetype, MediaLoadInstruction instructions);
        MediaDTO GetByNameAndType(string name, string mimetype, MediaLoadInstruction instructions);

        UserMediaDTO MapMediaToUser(Guid uid, Guid mediaId);
        OrderMediaDTO MapMediaToOrder(Guid orderId, Guid mediaId, string tag = "");
        MediaDTO Save(MediaDTO media, bool refetch, bool recurse);
        void Delete(Guid mediaId);

        MediaDTO CreateMediaFromPostedFile(byte[] data, string name, string description, string filename, string contenttype, bool useVersioning, Guid userId);
        MediaDTO CreateMediaFromPostedFile(IPostedFile postedFile, string name = null, string description = null);
        MediaDTO UpdateMediaFromPostedFile(byte[] data, Guid id, string name, string description, string filename, string contenttype, bool useVersioning, Guid userId);

        void RemoveFromLabels(Guid[] labelIds, Guid id);
        void RemoveFromRoles(string[] roleIds, Guid id);

        IList<MediaFolderDTO> GetFolders(bool includeNoneHierarchy, string[] roles, MediaFolderLoadInstruction instructions, bool includeHidden = false);

        IList<MediaFolderDTO> GetFolders(Guid? parentId, string[] roles, MediaFolderLoadInstruction instructions, bool includeHidden = false);

        IList<MediaFolderDTO> GetThread(Guid parentId, string[] roles, MediaFolderLoadInstruction instructions, bool includeHidden = false);

        MediaFolderDTO GetFolderById(Guid id, MediaFolderLoadInstruction instructions, bool includeHidden = false);
        MediaFolderDTO GetFolder(Guid? parentId, string folderName, MediaFolderLoadInstruction instructions, bool includeHidden = false);
        MediaFolderDTO SaveFolder(MediaFolderDTO folder, bool refetch, bool recurse);
        void DeleteFolder(Guid id);
        void RemoveFolderFromRoles(Guid folderId, string[] roleIds);

        MediaFolderFileDTO GetFolderFile(Guid parentId, string name, MediaFolderFileLoadInstruction instructions);
        MediaFolderFileDTO GetFolderFileById(Guid id, MediaFolderFileLoadInstruction instructions);
        MediaFolderFileDTO SaveFolderFile(MediaFolderFileDTO file, bool refetch, bool recurse);

        MediaVersionDTO GetMediaVersionById(Guid id, MediaVersionLoadInstructions instructions);

        MediaPreviewDTO GetMediaPreviewById(Guid id, int page);

        MediaViewingDTO GetMediaViewingById(Guid id, MediaViewingLoadInstruction instructions);
        MediaViewingDTO GetMediaViewingByWorkingId(Guid id, MediaViewingLoadInstruction instructions);
        MediaViewingDTO GetMediaViewingByWorkingId(Guid id, Guid mediaId, MediaViewingLoadInstruction instructions);

        IList<MediaViewingDTO> GetLatestMediaViewingsForUser(Guid userId);
        IList<MediaViewingDTO> GetLatestMediaViewingsForUsers(Guid[] userId, Guid mediaId);
        IList<MediaViewingDTO> GetLatestMediaViewingsForFolder(Guid folderid, MediaViewingLoadInstruction instructions);
        IList<MediaViewingDTO> GetLatestMediaViewingsForFolder(Guid folderid, Guid userId, MediaViewingLoadInstruction instructions);

        MediaViewingDTO SaveMediaViewing(MediaViewingDTO viewing, bool refetch, bool recurse);
        IList<MediaDTO> GetForLabel(string label, MediaLoadInstruction instructions = MediaLoadInstruction.None);
    }
}
