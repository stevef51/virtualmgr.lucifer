﻿using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public enum TaskTypeUserInterfaceType
    {
        /// <summary>
        /// Tasks of this type should be presented with a clockin/clockout user interface. Typically
        /// used by users out in the field where they're times at a particular task are monitored.
        /// </summary>
        ClockinClockout = 0,
        /// <summary>
        /// Tasks of this type should be presented with a user interface based around desktop users.
        /// Users should not be required to formally clockin and clock out (though clockinngs maybe
        /// gathered through some other means by the UI).
        /// </summary>
        DesktopNoClockings = 1
    }

    public enum UserTypeTaskTypeRelationship
    {
        CanPerform = 0,
    }

    public interface ITaskTypeRepository
    {
        IList<ProjectJobTaskTypeDTO> GetAll(ProjectJobTaskTypeLoadInstructions instructions);

        ProjectJobTaskTypeDTO GetById(Guid id, ProjectJobTaskTypeLoadInstructions instructions);
        ProjectJobTaskTypeDTO GetByName(string name, ProjectJobTaskTypeLoadInstructions instructions);

        ProjectJobTaskTypeDTO Save(ProjectJobTaskTypeDTO task, bool refetch, bool recurse);

        void RemoveTaskTypeResources(Guid taskTypeId, int[] resourceids);

        void RemoveTaskTypeDocumentation(Guid taskTypeId, Guid[] mediaId);

        void RemoveTaskTypeFinishedStatuses(Guid taskTypeId, Guid[] finishedstatusesids);
        void RemoveTaskTypeStatuses(Guid taskTypeId, Guid[] statusesids);

        void RemoveTaskTypeRelationships(Guid taskTypeId, Guid[] destinationids);

        void RemoveTaskTypeFacilityOverrides(Guid taskTypeId, int[] facilityStructureIds);

        void RemoveUserTypeRelationships(Guid taskTypeId, int[] userTypeIds, UserTypeTaskTypeRelationship relationship);
    }
}
