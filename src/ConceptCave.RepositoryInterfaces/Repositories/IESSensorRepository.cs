﻿using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public interface IESSensorRepository
    {
        ESSensorDTO GetById(int id, ESSensorLoadInstructions instructions = ESSensorLoadInstructions.None);
        ESSensorDTO GetByProbeIdSensorIndex(int probeId, int sensorIndex, ESSensorLoadInstructions instructions = ESSensorLoadInstructions.None);

        ESSensorDTO Save(ESSensorDTO dto, bool refetch, bool recurse);
    }
}
