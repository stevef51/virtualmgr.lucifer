﻿using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public interface IPaymentResponseRepository
    {
        PaymentResponseDTO GetPaymentResponseById(Guid id, PaymentResponseLoadInstructions instructions = PaymentResponseLoadInstructions.None);
        IList<PaymentResponseDTO> GetPaymentResponsesForPendingPayment(Guid pendingPaymentId, PaymentResponseLoadInstructions instructions = PaymentResponseLoadInstructions.None);
        PaymentResponseDTO GetPaymentResponseForTransactionId(string gatewayName, bool sandbox, string transactionId, PaymentResponseLoadInstructions instructions = PaymentResponseLoadInstructions.None);
    }
}
