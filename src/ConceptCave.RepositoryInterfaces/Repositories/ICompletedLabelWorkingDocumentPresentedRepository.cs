using System.Collections.Generic;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public interface ICompletedLabelWorkingDocumentPresentedRepository
    {
        void Save(IEnumerable<CompletedLabelWorkingDocumentPresentedDimensionDTO> items, bool refetch, bool recurse);
    }
}
