﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public interface IPublishingGroupResourceRepository
    {
        string DesignDocumentRootElementName { get; }

        PublishingGroupResourceDTO Add(int publishedGroupId, Guid resourceId);
        void AddLabel(int publishingGroupId, Guid labelId, PublishingGroupActorType actorType);
        PublishingGroupResourceDTO GetByCompoundId(int groupId, int resourceId, PublishingGroupResourceLoadInstruction instructions);
        PublishingGroupResourceDTO GetByCompoundName(string groupName, string resourceName, PublishingGroupResourceLoadInstruction instructions = PublishingGroupResourceLoadInstruction.None);
        PublishingGroupResourceDTO GetByGroupAndName(int groupId, string resourceName, PublishingGroupResourceLoadInstruction instructions = PublishingGroupResourceLoadInstruction.None);
        PublishingGroupResourceDTO GetById(int id, PublishingGroupResourceLoadInstruction instructions = PublishingGroupResourceLoadInstruction.None);
        PublishingGroupResourceDTO GetById(int publishedGroupId, Guid resourceId, PublishingGroupResourceLoadInstruction instructions = PublishingGroupResourceLoadInstruction.None);
        PublishingGroupResourceDTO GetById(int publishedGroupId, int resourceId, PublishingGroupResourceLoadInstruction instructions = PublishingGroupResourceLoadInstruction.None);
        IEnumerable<PublishingGroupResourceDTO> GetByIds(int[] id, PublishingGroupResourceLoadInstruction instructions = PublishingGroupResourceLoadInstruction.None);
        IEnumerable<PublishingGroupResourceDTO> GetChecklistsUserCanDo(string userName);
        IEnumerable<PublishingGroupResourceDTO> GetLikeName(string query, int publishingGroupId);
        IList<PublishingGroupResourceDTO> GetLikeName(string name, PublishingGroupResourceLoadInstruction instructions, int pageNumber = 1, int pageSize = 20);
        PublishingGroupResourceDTO RefreshFromResource(int publishedGroupId, Guid resourceId);
        void Remove(int publishedGroupId, Guid resourceId);
        void RemoveLabel(int publishingGroupId, Guid labelId, PublishingGroupActorType actorType);
        void RemovePublicTicket(int publishingGroupResourceId);
        PublishingGroupResourceDTO Save(PublishingGroupResourceDTO resource, bool refetch, bool recurse);
    }
}
