﻿using ConceptCave.DTO.DTOClasses;
using System;
using System.Collections.Generic;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public interface IStatusRepository
    {
        IList<ProjectJobStatusDTO> GetAll();

        ProjectJobStatusDTO GetById(Guid id);
        IList<ProjectJobStatusDTO> GetById(IEnumerable<Guid> ids);

        ProjectJobStatusDTO Save(ProjectJobStatusDTO status, bool refetch, bool recurse);
    }
}
