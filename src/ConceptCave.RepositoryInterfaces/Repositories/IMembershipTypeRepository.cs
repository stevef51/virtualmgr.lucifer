﻿using System;
using System.Collections.Generic;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public interface IMembershipTypeRepository
    {
        IList<UserTypeDTO> GetAll();
        UserTypeDTO GetById(int id, MembershipTypeLoadInstructions instructions);
        UserTypeDTO GetByName(string name);
        IList<UserTypeDTO> GetSiteTypes();
        void Save(UserTypeDTO entity, bool refetch, bool recurse);
        IList<UserTypeContextPublishedResourceDTO> GetByPublishingGroupResource(int publishingGroupResource);
        UserTypeContextPublishedResourceDTO GetByUserType(int userTypeId, string name, UserTypeContextLoadInstructions instructions = UserTypeContextLoadInstructions.None);
        IList<UserTypeContextPublishedResourceDTO> GetByUserType(int userTypeId, UserTypeContextLoadInstructions instructions = UserTypeContextLoadInstructions.None);
        IList<UserTypeContextPublishedResourceDTO> AddContextsToUserType(int[] publishingGroupResources, int userTypeId);
        void RemoveContextsFromUserType(int[] publishingGroupResources, int userTypeId);
        void RemoveLabelsFromUserType(Guid[] labels, int userTypeId);
        void RemoveRolesFromUserType(string[] roles, int userTypeId);
        void AddDashboardsToUserType(int[] publishingGroupResources, int userTypeId);
        void RemoveDashboardsFromUserType(int[] publishingGroupResources, int userTypeId);
    }
}
