﻿using ConceptCave.DTO.DTOClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public interface ILanguageTranslationRepository
    {
        IList<LanguageTranslationDTO>   GetAllTranslations(string cultureName);

        LanguageTranslationDTO GetTranslation(string cultureName, string englishText);
        LanguageTranslationDTO SaveTranslation(LanguageTranslationDTO dto, bool refetch, bool recurse);

        void DeleteTranslation(string cultureName, string englishText);

        void SaveTranslations(IEnumerable<LanguageTranslationDTO> dtos);
    }
}
