﻿using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public interface ITabletRepository
    {
        TabletDTO GetTabletById(int id, TabletLoadInstructions instructions);

        TabletDTO SaveTablet(TabletDTO dto, bool refetch, bool recurse);

        IList<TabletDTO> Search(string nameLike, int? pageNumber, int? itemsPerPage, bool includeArchived, ref int totalItems);

        TabletUuidDTO GetTabletUuid(string uuid, TabletUuidLoadInstructions instructions);

        TabletUuidDTO SaveTabletUuid(TabletUuidDTO dto, bool refetch, bool recurse);

        IList<TabletUuidDTO> FindUnassignedTabletUuids();

        void Delete(int id, bool archiveIfRequired, out bool archived);
    }
}
