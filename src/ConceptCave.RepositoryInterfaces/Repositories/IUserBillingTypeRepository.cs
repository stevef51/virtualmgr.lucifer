﻿using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public interface IUserBillingTypeRepository
    {
        UserBillingTypeDTO GetById(int id, UserBillingTypeLoadInstructions instructions = UserBillingTypeLoadInstructions.None);

        UserBillingTypeDTO Save(UserBillingTypeDTO dto, bool refetch);

        IList<UserBillingTypeDTO> GetAll(UserBillingTypeLoadInstructions instructions = UserBillingTypeLoadInstructions.None);

        IList<UserBillingTypeDTO> Search(string name, int? pageNumber, int? itemsPerPage, bool includeArchived, ref int totalItems, UserBillingTypeLoadInstructions userBillingTypeLoadInstructions = UserBillingTypeLoadInstructions.None);
    }
}
