﻿using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public interface IAssetTypeCalendarRepository
    {
        AssetTypeCalendarDTO GetById(int id, AssetTypeCalendarLoadInstructions instructions = AssetTypeCalendarLoadInstructions.None);

        AssetTypeCalendarDTO Save(AssetTypeCalendarDTO dto, bool refetch, bool recurse);

        IList<AssetTypeCalendarDTO> Search(string nameLike, int? pageNumber, int? itemsPerPage, bool includeArchived, ref int totalItems);

        void Delete(int id, bool archiveIfRequired, out bool archived);
        IList<AssetTypeCalendarDTO> GetAll(AssetTypeCalendarLoadInstructions instructions = AssetTypeCalendarLoadInstructions.None);
    }
}
