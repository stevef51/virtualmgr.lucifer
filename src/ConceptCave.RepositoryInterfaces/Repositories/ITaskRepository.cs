﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    [Flags]
    public enum TaskStatusFilter
    {
        All = 0,
        Unapproved = 1,
        Approved = 2,
        Started = 4,
        Paused = 8,
        FinishedComplete = 16,
        FinishedIncomplete = 32,
        Cancelled = 64,
        FinishedByManagement = 128,
        ApprovedContinued = 256,
        ChangeRosterRequested = 512,
        ChangeRosterRejected = 1024,
        FinishedBySystem = 2048
    }
    public interface ITaskRepository
    {
        IList<ProjectJobTaskDTO> Search(string text, int page, int count, ProjectJobTaskLoadInstructions instructions);
        ProjectJobTaskDTO GetById(Guid id, ProjectJobTaskLoadInstructions instructions);

        IList<ProjectJobTaskDTO> GetById(Guid[] id, ProjectJobTaskLoadInstructions instructions, IDictionary<object, object> cache = null);

        IList<ProjectJobTaskDTO> GetByWorkingGroupId(Guid workingGroupId, ProjectJobTaskLoadInstructions instructions);

        ProjectJobTaskDTO GetByWorkingDocumentId(Guid workingDocumentId, ProjectJobTaskLoadInstructions instructions);

        ProjectJobTaskDTO Save(ProjectJobTaskDTO task, bool refetch, bool recurse);
        IList<ProjectJobTaskDTO> Save(IList<ProjectJobTaskDTO> tasks, bool refetch, bool recurse);

        ProjectJobTaskFinishedStatusDTO Save(ProjectJobTaskFinishedStatusDTO status, bool refetch, bool recurse);

        void Save(ProjectJobTaskWorkloadingActivityDTO[] activities, bool refetch, bool recurse);

        ProjectJobTaskDTO New(Guid? taskid, string name, Guid ownerId, int rosterId, Guid taskTypeId, int? bucketId, IMembershipRepository memberRepo, Guid? originalOwnerId = null, Guid? siteId = null, ProjectJobTaskStatus status = ProjectJobTaskStatus.Unapproved, Guid? groupId = null, int? assetId = null, int? taskLevel = null, string lengthInDays = null);
        ProjectJobTaskDTO New(Guid? taskId, string name, string description, Guid ownerId, int rosterId, Guid taskTypeId, int? bucketId, IMembershipRepository memberRepo, Guid? originalOwnerId = null, Guid? siteId = null, ProjectJobTaskStatus status = ProjectJobTaskStatus.Unapproved, Guid? groupId = null, int? assetId = null, int? taskLevel = null, string lengthInDays = null);
        ProjectJobTaskDTO New(string name, Guid ownerId, int rosterId, Guid taskTypeId, int? bucketId, IMembershipRepository memberRepo, Guid? originalOwnerId = null, Guid? siteId = null, ProjectJobTaskStatus status = ProjectJobTaskStatus.Unapproved, Guid? groupId = null, int? assetId = null, int? taskLevel = null, string lengthInDays = null);
        ProjectJobTaskDTO New(string name, string description, Guid ownerId, int rosterId, Guid taskTypeId, int? bucketId, IMembershipRepository memberRepo, Guid? originalOwnerId = null, Guid? siteId = null, ProjectJobTaskStatus status = ProjectJobTaskStatus.Unapproved, Guid? groupId = null, int? assetId = null, int? taskLevel = null, string lengthInDays = null);
        ProjectJobTaskDTO New(ProjectJobScheduledTaskDTO dto, TimeZoneInfo tz, Guid ownerId, int? bucketId, IMembershipRepository memberRepo, Guid? originalOwnerId = null, Guid? scheduledOwnerId = null, ProjectJobTaskStatus status = ProjectJobTaskStatus.Unapproved);

        IList<ProjectJobTaskDTO> GetForUser(Guid userId, bool onlyOpenTasks, bool onlyTopLevel, TaskStatusFilter filter, ProjectJobTaskLoadInstructions instructions, int? bucketId = null, Guid[] labels = null, Guid[] statusses = null);
        IList<ProjectJobTaskDTO> GetForUser(Guid userId, DateTime date, TimeZoneInfo timezone, ProjectJobTaskStatus status, ProjectJobTaskLoadInstructions instructions, int? bucketId = null, Guid[] labels = null, Guid[] statusses = null);
        ProjectJobTaskDTO GetForUser(Guid userId, Guid scheduleId, int rosterId, int roleId, DateTime date, TimeZoneInfo timezone, ProjectJobTaskLoadInstructions instructions, Guid[] labels = null, Guid[] statusses = null);

        IList<ProjectJobTaskDTO> GetForBucket(int bucketId, bool onlyOpenTasks, bool onlyTopLevel, TaskStatusFilter filter, ProjectJobTaskLoadInstructions instructions, Guid[] labels = null, Guid[] statusses = null);
        IList<ProjectJobTaskDTO> GetForBucket(int bucketId, DateTime date, TimeZoneInfo timezone, ProjectJobTaskStatus status, ProjectJobTaskLoadInstructions instructions, Guid[] labels = null, Guid[] statusses = null);

        IList<ProjectJobTaskDTO> GetForRoster(int rosterId, DateTime startDateCreated, DateTime endDateCreated, TaskStatusFilter filter, ProjectJobTaskLoadInstructions instructions);

        IList<ProjectJobTaskDTO> GetForRoster(int rosterId, Guid? userId, DateTime startDateCreated, DateTime endDateCreated, TaskStatusFilter filter, ProjectJobTaskLoadInstructions instructions);

        IList<ProjectJobTaskDTO> GetForRoster(int rosterId, DateTime endsOnUtc, TaskStatusFilter filter, ProjectJobTaskLoadInstructions instructions);
        IList<ProjectJobTaskDTO> GetForRoster(int rosterId, TaskStatusFilter filter, ProjectJobTaskLoadInstructions instructions);
        void Remove(Guid[] ids);

        ProjectJobTaskAttachmentDTO AddAttachment(Guid taskId, Guid mediaId);
        void RemoveAttachment(Guid taskId, Guid mediaId);

        byte[] SignatureToImage(string json, bool autoscale = true);

        void SetTaskStatusWhenFinishing(ProjectJobTaskDTO task);

        ProjectJobTaskRelationshipTypeDTO GetRelationshipTypeByName(string name);

        ProjectJobTaskRelationshipTypeDTO GetRelationshipTypeById(int id);

        ProjectJobTaskRelationshipDTO GetRelationshipBySourceId(Guid id, int relationshipType);

        ProjectJobTaskRelationshipDTO GetRelationshipByDestinationId(Guid id, int relationshipType);

        void RemoveMedia(Guid taskId, Guid mediaId);

        /// <summary>
        /// Moves any live tasks currently mapped to the user going on leave between the dates of leave
        /// across to the user that is replacing them.
        /// </summary>
        /// <param name="leave">DTO modelling the leave being taken</param>
        /// <param name="timezone">Timezone of the user being replaced</param>
        void ManageTasksForNewLeave(HierarchyBucketOverrideDTO leave, TimeZoneInfo timezone);
        /// <summary>
        /// Moves any live tasks that were originally scheduled to the user that was going on leave that
        /// are mapped to the replacing user, back to the original user.
        /// </summary>
        /// <param name="leave">DTO modelling the leave that was being taken</param>
        /// <param name="timezone">Timezone of the user being replaced</param>
        void ManageTasksForLeaveRemoval(HierarchyBucketOverrideDTO leave, TimeZoneInfo timezone);

        ProjectJobTaskDTO CopyTask(ProjectJobTaskDTO source);

        DateTime CalculateTaskLengthInDays(int rosterId, string lengthInDays, IDictionary<int, RosterDTO> rosterCache);

    }
}
