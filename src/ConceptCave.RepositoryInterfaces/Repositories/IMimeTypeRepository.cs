using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public interface IMimeTypeRepository
    {
        MimeTypeDTO GetByFilename(string filename);
        MimeTypeDTO GetByExtension(string extension);
    }
}
