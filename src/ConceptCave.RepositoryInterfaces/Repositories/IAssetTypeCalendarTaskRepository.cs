﻿using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public interface IAssetTypeCalendarTaskRepository
    {
        AssetTypeCalendarTaskDTO GetById(int id, AssetTypeCalendarTaskLoadInstructions instructions = AssetTypeCalendarTaskLoadInstructions.None);

        AssetTypeCalendarTaskDTO Save(AssetTypeCalendarTaskDTO dto, bool refetch, bool recurse);

        void Delete(int id, bool archiveIfRequired, out bool archived);

        AssetTypeCalendarTaskLabelDTO SaveLabel(AssetTypeCalendarTaskLabelDTO dto, bool refetch, bool recurse);

        void DeleteLabel(int assetTypeCalendarTaskId, Guid labelId);
    }
}
