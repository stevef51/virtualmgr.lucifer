﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public interface ILabelRepository
    {
        IList<LabelDTO> GetAll(LabelFor labelsFor);
        LabelDTO GetByName(string name);
        IList<LabelDTO> GetByIds(IList<Guid> ids);
        LabelDTO GetById(Guid id);
        LabelDTO Save(LabelDTO label, bool refetch);
        void Remove(Guid id);
    }
}
