﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public interface ICompanyRepository
    {
        IList<CompanyDTO> GetAll(CompanyLoadInstructions instructions);
        IList<CompanyDTO> Search(string text, int page, int count, CompanyLoadInstructions instructions);
        CompanyDTO GetById(Guid id, CompanyLoadInstructions instructions);

        CompanyDTO Save(CompanyDTO project, bool refetch, bool recurse);

        CompanyDTO New(string name);
    }
}
