﻿using ConceptCave.DTO.DTOClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public interface ILanguageRepository
    {
        IList<LanguageDTO> AllLanguages();

        LanguageDTO Save(LanguageDTO language, bool refetch, bool recurse);

        void Delete(string cultureName);
    }
}
