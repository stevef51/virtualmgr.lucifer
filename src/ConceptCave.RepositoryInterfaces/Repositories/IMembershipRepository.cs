using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.ViewDefs;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public class ChangePasswordResult
    {
        public bool Success { get; set; }
        public string Message { get; set; }
    }

    public interface IMembershipRepository
    {
        void AddLabels(Guid uid, IEnumerable<Guid> labelIds);
        void AddRoles(Guid membershipId, IEnumerable<string> roleIds);
        void AddUserInRole(Guid uid, string roleName);
        ChangePasswordResult ChangePassword(Guid userId, string newPassword);
        UserDataDTO Create(string username, string password, string defaultTimeZone = null, MembershipLoadInstructions instructions = MembershipLoadInstructions.None);
        UserContextDataDTO CreateOrUpdateUserCtxEntry(Guid userId, int publishedResourceid, Guid completedWorkingDocumentid);
        UserDataDTO FindByQrcode(string qrcode);
        string GeneratePassword(int length, int numberOfNonNumericCharacters);
        IEnumerable<UserListResponse> GetAllRealUsers();
        IEnumerable<UserDataDTO> GetAllSites(MembershipLoadInstructions instructions);
        IEnumerable<UserListResponse> GetAllUsers();
        IEnumerable<UserListResponse> GetAllUsersByLabel(IEnumerable<Guid> labels);
        IEnumerable<UserListResponse> GetAllUsersByRoles(IEnumerable<string> roles, bool includeSites = false);
        UserDataDTO GetById(Guid id, MembershipLoadInstructions instructions = MembershipLoadInstructions.None);
        UserDataDTO GetById(string id, MembershipLoadInstructions instructions = MembershipLoadInstructions.None);
        IEnumerable<UserDataDTO> GetByIds(IEnumerable<Guid> ids, MembershipLoadInstructions instructions = MembershipLoadInstructions.None);
        UserDataDTO GetByName(string name, MembershipLoadInstructions instructions = MembershipLoadInstructions.None);
        UserDataDTO GetByUsername(string username, MembershipLoadInstructions instructions = MembershipLoadInstructions.None);
        IEnumerable<UserDataDTO> GetByUserType(int type, MembershipLoadInstructions instructions = MembershipLoadInstructions.None);
        IEnumerable<UserDataDTO> GetByUserTypes(IEnumerable<int> userTypeIds, MembershipLoadInstructions instructions = MembershipLoadInstructions.None);
		IList<ClosestUsersResultEntry> GetClosestUsers(decimal latitude, decimal longitude, int count, bool? isASite);
        IEnumerable<UserTypeDashboardDTO> GetDashboardData(Guid uid);
        IEnumerable<int> GetDashboardResourceIds(Guid uid);
        IEnumerable<UserDataDTO> GetForLabels(IEnumerable<Guid> labels);
        UserLogDTO GetLogin(Guid sessionId);
        IEnumerable<string> GetRolesForUser(Guid id);
        UserContextDataDTO GetUserContextData(Guid userId, int userTypeContextPublishedResourceId);
        UserContextDataDTO GetUserContextData(Guid userId, string contextName);
        IEnumerable<PublishingGroupResourceDTO> GetUserContextResourcesForType(int typeId, PublishingGroupResourceLoadInstruction instructions);
        IList<UserDataDTO> GetUsersByType(int type, MembershipLoadInstructions instructions = MembershipLoadInstructions.None);
        UserTypeDTO GetUserTypeById(int id, MembershipTypeLoadInstructions instructions = MembershipTypeLoadInstructions.None);
        UserTypeDTO GetUserTypeByName(string name, MembershipTypeLoadInstructions instructions = MembershipTypeLoadInstructions.None);
        Guid RecordLogin(Guid userId, decimal? latitude, decimal? longitude, bool resolveLocation, string tabletUUID, double maxDistanceForLogin);
        void RecordLogOut(Guid userId, Guid sessionId);
        void Remove(Guid id);
        void RemoveFromLabels(IEnumerable<Guid> labels, Guid userId);
        void RemoveFromRoles(IEnumerable<string> roleIds, Guid userId);
        void RemoveUserContextData(Guid userId);
        void Save(UserContextDataDTO entity, bool refetch, bool recurse);
        UserDataDTO Save(UserDataDTO user, bool refetch, bool recurse);
        void SaveLogin(UserLogDTO loginLog);
        IEnumerable<UserDataDTO> Search(string text, bool includeExpired, int page, int count, MembershipLoadInstructions instructions = MembershipLoadInstructions.None, Guid? companyId = null);
        IEnumerable<UserDataDTO> Search(string text, int page, int count);
        IEnumerable<UserDataDTO> Search(string text, int page, int count, MembershipLoadInstructions instructions = MembershipLoadInstructions.None, Guid? companyId = null);
        bool UserIsInRole(Guid uid, string roleName);
        bool ValidateUser(string username, string password);        
    }
}
