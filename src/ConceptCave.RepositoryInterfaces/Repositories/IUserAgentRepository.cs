﻿using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public interface IUserAgentRepository
    {
        UserAgentDTO GetOrCreate(string userAgent);
        UserAgentDTO GetByUserAgent(string userAgent);

        UserAgentDTO Save(UserAgentDTO dto, bool refetch, bool recurse);
    }
}
