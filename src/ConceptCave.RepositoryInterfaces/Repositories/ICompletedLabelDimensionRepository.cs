using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public interface ICompletedLabelDimensionRepository
    {
        CompletedLabelDimensionDTO GetById(Guid id);

        CompletedLabelDimensionDTO CreateFromLabel(LabelDTO label);

        CompletedLabelDimensionDTO GetOrCreate(LabelDTO label);
    }
}