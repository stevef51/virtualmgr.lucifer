﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public interface IMessagingRepository
    {
        void SaveSent(MessageDTO message, IList<Guid> recipientUsers, IList<Guid> recipientLabels);
        IList<Guid> ResolveRecipients(IEnumerable<Guid> users, IEnumerable<Guid> labels);
        IList<MailboxDTO> GetRecievedMessages(Guid recipientId, int count);
        IList<MessageDTO> GetSentMessages(Guid senderId, int count); 
        void MarkAsRead(Guid mailboxId);
    }
}
