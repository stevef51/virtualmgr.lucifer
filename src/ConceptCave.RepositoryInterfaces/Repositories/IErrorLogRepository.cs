﻿using ConceptCave.DTO.DTOClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public interface IErrorLogRepository
    {
        void LogError(ErrorLogTypeDTO type, ErrorLogItemDTO item);

        ErrorLogTypeDTO Save(ErrorLogTypeDTO dto, bool refetch);
        ErrorLogItemDTO Save(ErrorLogItemDTO dto, bool refetch);

        ErrorLogTypeDTO Find(string type, string source, string message, string stackTrace);
    }
}
