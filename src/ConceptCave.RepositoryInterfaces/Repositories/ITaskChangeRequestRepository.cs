﻿using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public interface ITaskChangeRequestRepository
    {
        IList<ProjectJobTaskChangeRequestDTO> GetOpenRequestsByTaskId(Guid taskId, ProjectJobTaskChangeRequestLoadInstructions instructions = ProjectJobTaskChangeRequestLoadInstructions.None);
        IList<ProjectJobTaskChangeRequestDTO> GetOpenRequestsByFromRosterId(int rosterId, ProjectJobTaskChangeRequestLoadInstructions instructions = ProjectJobTaskChangeRequestLoadInstructions.None);
        IList<ProjectJobTaskChangeRequestDTO> GetOpenRequestsByToRosterId(int rosterId, ProjectJobTaskChangeRequestLoadInstructions instructions = ProjectJobTaskChangeRequestLoadInstructions.None);
        void DeleteOpenRequestsByTaskId(Guid taskId);

        ProjectJobTaskChangeRequestDTO Save(ProjectJobTaskChangeRequestDTO task, bool refetch, bool recurse);

    }
}
