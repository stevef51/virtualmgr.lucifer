﻿using ConceptCave.DTO.DTOClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public interface IAuditTaskConditionActionRepository
    {
        IList<AuditTaskConditionActionDTO> GetByIds(Guid[] scheduleIds, string group);

        AuditTaskConditionActionDTO Save(AuditTaskConditionActionDTO dto, bool refetch, bool recurse);
    }
}
