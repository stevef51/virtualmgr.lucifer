﻿using ConceptCave.DTO.DTOClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public class ExecutionHandlerQueueGroup
    {
        public string Handler { get; set; }
        public string PrimaryEntityId { get; set; }
    }

    public interface IExecutionHandlerQueueRepository
    {
        int GetUnProcessedCountForPrimaryEntity(string handler, string primaryEntityId);
        void Queue(string handler, string method, string primaryEntityId, string data, string exceptionDetails);

        ExecutionHandlerQueueDTO GetById(Guid id);

        void Save(ExecutionHandlerQueueDTO dto, bool refetch, bool recurse);

        IList<ExecutionHandlerQueueGroup> GetOpenHandlers();
        IList<ExecutionHandlerQueueDTO> GetOpenQueueItems(string handler, string primaryEntityId);
    }
}
