﻿using ConceptCave.DTO.DTOClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public interface ITimesheetItemTypeRepository
    {
        IList<TimesheetItemTypeDTO> GetAll();

        TimesheetItemTypeDTO GetById(int id);

        TimesheetItemTypeDTO Save(TimesheetItemTypeDTO dto, bool refetch, bool recurse);
    }
}
