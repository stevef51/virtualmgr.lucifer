using System;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public interface ICompletedUserDimensionRepository
    {
        CompletedUserDimensionDTO GetByUsername(string username);

        CompletedUserDimensionDTO GetById(Guid id);

        CompletedUserDimensionDTO CreateFromUser(UserDataDTO user);

        CompletedUserDimensionDTO GetOrCreate(UserDataDTO user);
    }
}
