﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public interface IProjectJobTaskWorkingDocumentRepository
    {
        IList<ProjectJobTaskWorkingDocumentDTO> GetForTaskAndPublishingGroupResource(Guid taskId, int publishingGroupResourceId, ProjectJobTaskWorkingDocumentLoadInstructions instructions);
        ProjectJobTaskWorkingDocumentDTO GetForWorkingDocument(Guid workingDocumentId, ProjectJobTaskWorkingDocumentLoadInstructions instructions);

        ProjectJobTaskWorkingDocumentDTO Save(ProjectJobTaskWorkingDocumentDTO task, bool refetch, bool recurse);
    }
}
