using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public interface IMembershipTypeContextPublishedResource
    {
        UserTypeContextPublishedResourceDTO GetById(int id, UserTypeContextLoadInstructions instructions);
    }
}
