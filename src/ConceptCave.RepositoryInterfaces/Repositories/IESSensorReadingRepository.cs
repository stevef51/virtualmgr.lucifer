﻿using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public class ESSensorReadingRecord
    {
        public int SensorId { get; set; }
        public DateTime SensorTimestampUtc { get; set; }
        public double Value { get; set; }
        public string TabletUUID { get; set; }
    }

    public interface IESSensorReadingRepository
    {
        void WriteSensorReadingRecords(IEnumerable<ESSensorReadingRecord> records);
    }
}
