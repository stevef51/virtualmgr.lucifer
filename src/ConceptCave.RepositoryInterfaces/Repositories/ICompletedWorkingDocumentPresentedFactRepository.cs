using System;
using System.Collections.Generic;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    [Flags]
    public enum CompletedWorkingDocumentPresentedFactLoadInstructions
    {
        None = 1,
        CompletedPresentedFactValue = 2,
        CompletedUploadMedia = 4
    }

    public interface ICompletedWorkingDocumentPresentedFactRepository
    {
        void Save(IEnumerable<CompletedWorkingDocumentPresentedFactDTO> dtos, bool refetch, bool recurse);
        IList<CompletedWorkingDocumentPresentedFactDTO> Get(Guid workingDocumentId, CompletedWorkingDocumentPresentedFactLoadInstructions instructions);
    }
}
