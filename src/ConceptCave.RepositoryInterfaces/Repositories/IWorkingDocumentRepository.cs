﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public class WorkingDocumentDTOHierachyItem
    {
        public WorkingDocumentDTOHierachyItem()
        {
            Children = new List<WorkingDocumentDTOHierachyItem>();
        }

        public WorkingDocumentDTO Dto { get; set; }

        public List<WorkingDocumentDTOHierachyItem> Children { get; set; }

        public IEnumerable<WorkingDocumentDTOHierachyItem> AsDepthFirstEnumerable()
        {
            yield return this;

            foreach (var c in Children)
            {
                foreach (var presentable in c.AsDepthFirstEnumerable())
                    yield return presentable;
            }
        }
    }

    public interface IWorkingDocumentRepository
    {
        void Save(IWorkingDocument document, WorkingDocumentDTO dto, bool refetch, bool recurse);
        void DeleteWorkingDocumentsForUser(Guid userId);
        WorkingDocumentDTO Save(WorkingDocumentDTO dto, bool refetch, bool recurse);
        WorkingDocumentDTO GetByInternalId(Guid id, WorkingDocumentLoadInstructions instructions);
        WorkingDocumentDTO GetById(Guid id, WorkingDocumentLoadInstructions instructions);
        Task<WorkingDocumentDTO> GetByIdAsync(Guid id, WorkingDocumentLoadInstructions instructions);
        List<WorkingDocumentDTO> GetByParentId(Guid parentId, WorkingDocumentLoadInstructions instructions);
        WorkingDocumentDTOHierachyItem GetByIdWithDescendants(Guid id, WorkingDocumentLoadInstructions instructions);

        void DeleteWorkingDocument(Guid id, bool deleteReportingData);

        IList<WorkingDocumentDTO> GetCurrentWorkingDocuments(int groupid, int resourceid, Guid reviewerid, Guid? revieweeid,
                                                          WorkingDocumentLoadInstructions instructions);
        IList<WorkingDocumentDTO> GetCurrentWorkingDocuments(int publishingGroupResourceId, WorkingDocumentLoadInstructions instructions);
        List<WorkingDocumentDTO> GetCurrentWorkingDocuments(Guid reviewerid, WorkingDocumentLoadInstructions instructions = WorkingDocumentLoadInstructions.None);
        void DeleteIncompleteWorkingDocuments(DateTime inMaxDate);
        void DeleteCompleteWorkingDocuments(DateTime inMaxDate);

        IList<WorkingDocumentDTO> GetByDate(DateTime startDate, DateTime endDate, WorkingDocumentLoadInstructions instructions,
                                         WorkingDocumentStatus? status);

        IAnswer SaveNotesAttachment(Guid rootId, Guid presentedId, IPostedFile fileUpload);
    }
}
