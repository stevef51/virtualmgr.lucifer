﻿using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public interface IReportManagementRepository
    {
        IList<ReportDTO> GetAll(int? reportType = null, ReportLoadInstructions instructions = ReportLoadInstructions.None);

        ReportDTO GetById(int id, ReportLoadInstructions instructions);

        IList<ReportDTO> GetReports(string[] roles, ReportLoadInstructions instructions);

        ReportDTO Save(ReportDTO report, bool refetch, bool recurse);

        void Remove(int reportId);

        void RemoveReportFromRoles(int id, string[] roleIds);

        IList<ReportDTO> GetByName(string name, ReportLoadInstructions instructions);

        ReportDataDTO GetReportByData(Byte[] data);
    }
}
