using System;
using System.Collections.Generic;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public interface IRoleRepository
    {
        IList<AspNetRoleDTO> GetAll();
        AspNetRoleDTO GetByName(string name);

        AspNetRoleDTO Save(AspNetRoleDTO role);

        bool IsUserInRole(Guid userId, string role);
        string[] CheckCoreRolesExist(IDictionary<string, string> coreRoles, IDictionary<string, string> optionalRoles);
    }
}
