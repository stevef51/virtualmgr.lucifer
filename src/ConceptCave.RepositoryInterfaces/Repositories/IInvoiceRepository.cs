﻿using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public interface IInvoiceRepository
    {
        InvoiceDTO GetInvoiceById(string id, InvoiceLoadInstructions instructions = InvoiceLoadInstructions.None);

        InvoiceLineItemDTO GetLineItemById(int id, InvoiceLineItemLoadInstructions instructions = InvoiceLineItemLoadInstructions.None);

        InvoiceDTO SaveInvoice(InvoiceDTO dto, bool refetch);

        InvoiceDTO MarkAsUnpaid(string id);

        bool DeleteInvoice(string id);

        InvoiceDTO GetLastInvoice(InvoiceLoadInstructions instructions = InvoiceLoadInstructions.None);

        IEnumerable<InvoiceDTO> Search(string search, int? pageNumber, int? itemsPerPage, bool includeArchived, ref int totalItems);

        InvoiceDTO FindInvoiceCovering(DateTimeOffset date);

        IEnumerable<InvoiceDTO> FindNew();
    }
}
