﻿using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public interface IAssetScheduleRepository
    {
        AssetScheduleDTO GetById(int id, AssetScheduleLoadInstructions instructions = AssetScheduleLoadInstructions.None);

        AssetScheduleDTO Save(AssetScheduleDTO dto, bool refetch, bool recurse);
        IList<AssetScheduleDTO> FindExpired(DateTime utcNow, AssetScheduleLoadInstructions instructions = AssetScheduleLoadInstructions.None);
    }
}
