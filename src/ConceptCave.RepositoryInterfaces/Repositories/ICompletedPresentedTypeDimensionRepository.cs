using System;
using System.Collections.Generic;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public interface ICompletedPresentedTypeDimensionRepository
    {
        CompletedPresentedTypeDimensionDTO GetByType(string type);
        CompletedPresentedTypeDimensionDTO CreateFromType(string type);
        CompletedPresentedTypeDimensionDTO GetOrCreate(string type);
    }
}
