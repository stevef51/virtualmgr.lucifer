﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public interface IScheduledTaskRepository
    {
        ProjectJobScheduledTaskDTO GetById(Guid id, ProjectJobScheduledTaskLoadInstructions instructions);
        List<ProjectJobScheduledTaskDTO> GetById(Guid[] id, ProjectJobScheduledTaskLoadInstructions instructions);

        ProjectJobScheduledTaskDTO New(string name, Guid jobId, int calendarRuleId);
        ProjectJobScheduledTaskDTO Save(ProjectJobScheduledTaskDTO task, bool refetch, bool recurse);

        IList<ProjectJobScheduledTaskDTO> GetForUser(Guid userId, int hierarchyRoleId, int rosterId, DateTime date, TimeZoneInfo timezone, bool onlyTopLevel, bool removeSchedulesWithLiveTasks, ProjectJobScheduledTaskLoadInstructions instructions);
        IList<ProjectJobScheduledTaskDTO> GetForUser(Guid userId, ProjectJobScheduledTaskLoadInstructions instructions);

        IList<ProjectJobScheduledTaskDTO> GetForRoster(int rosterId, int? roleId, ProjectJobScheduledTaskLoadInstructions instructions);
        IList<ProjectJobScheduledTaskDTO> GetForRoster(int rosterId, int[] roleId, ProjectJobScheduledTaskLoadInstructions instructions);

        void Delete(Guid id);
        void DeleteScheduledTaskPublishedChecklists(Guid ScheduledTaskId, int[] publishedGroupResourceIds);
        void DeleteScheduledTaskUserLabels(Guid ScheduledTaskId, Guid[] labelIds);

        ProjectJobScheduledTaskAttachmentDTO AddAttachment(Guid taskId, Guid mediaId);
        void RemoveAttachment(Guid taskId, Guid mediaId);
        void RemoveLabels(Guid taskId);

        void RemoveWorkloadingActivities(Guid taskId);

        IList<ProjectJobScheduleDayDTO> GetDaySchedule(int rosterId, int? roleId, int? day);

        ProjectJobScheduleDayDTO Save(ProjectJobScheduleDayDTO dto, bool refetch, bool recurse);
    }
}
