﻿using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public enum FacilityStructureType
    {
        Facility,
        Building,
        Floor,
        Zone,
        Site
    }

    public interface IFacilityStructureRepository
    {
        FacilityStructureDTO GetById(int id, FacilityStructureLoadInstructions instructions = FacilityStructureLoadInstructions.None);
        FacilityStructureDTO GetDeepParent(int id, FacilityStructureType parentType, FacilityStructureLoadInstructions instructions = FacilityStructureLoadInstructions.None);

        FacilityStructureDTO Save(FacilityStructureDTO dto, bool refetch, bool recurse);

        IList<FacilityStructureDTO> Search(int? parentId, string nameLike, int? pageNumber, int? itemsPerPage, bool includeArchived, ref int totalItems, int? searchFlags);

        void Delete(int id, bool archiveIfRequired, out bool archived);

        IList<UserDataDTO> SearchSites(string search, int top);

        FacilityStructureDTO FindFloorByIndoorAtlasFloorPlanId(string indoorAtlasFloorPlanId);

        FacilityStructureDTO FindBySiteId(Guid siteId, FacilityStructureLoadInstructions instructions = FacilityStructureLoadInstructions.None);

        FacilityStructureDTO FindByName(FacilityStructureType facilityStructureType, string exactName);
        FacilityStructureDTO FindByName(int? parentId, string exactName);
    }
}
