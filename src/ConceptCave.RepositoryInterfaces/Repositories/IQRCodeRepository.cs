﻿using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public enum IQRCodeAttachmentType
    {
        UserSite,
        TaskType,
        Asset
    }

    public interface IQRCodeRepository
    {
        QrcodeDTO GetById(int id, QRCodeLoadInstructions instructions = QRCodeLoadInstructions.None);

        QrcodeDTO Save(QrcodeDTO dto, bool refetch, bool recurse);

        IList<QrcodeDTO> Search(string qrCodeLike, int? pageNumber, int? itemsPerPage, bool includeArchived, ref int totalItems, int? searchFlags, int? includeFlags,  bool? showSelected, QRCodeLoadInstructions instructions = QRCodeLoadInstructions.None);

        void Delete(int id, bool archiveIfRequired, out bool archived);
    }
}
