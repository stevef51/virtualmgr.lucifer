﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public interface IMediaExporterFactoryAsync
    {
        Task<IMediaExporterAsync> CreateAsync(IMediaRepository repo, Stream toStream);
    }

    public interface IMediaExporterAsync : IDisposable
    {
        Task<string> ExportFolderAsync(Guid fromFolderId);
    }
}
