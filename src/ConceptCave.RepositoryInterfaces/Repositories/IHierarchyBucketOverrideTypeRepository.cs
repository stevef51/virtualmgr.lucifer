﻿using ConceptCave.DTO.DTOClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public interface IHierarchyBucketOverrideTypeRepository
    {
        IList<HierarchyBucketOverrideTypeDTO> GetAll();
        HierarchyBucketOverrideTypeDTO GetById(int id);

        HierarchyBucketOverrideTypeDTO Save(HierarchyBucketOverrideTypeDTO dto, bool refetch, bool recurse);
    }
}
