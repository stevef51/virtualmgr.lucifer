﻿using ConceptCave.DTO.DTOClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public enum JSFunctionType
    {
        LineItemPrice = 1,
        Coupon = 2
    }

    public interface IJSFunctionRepository
    {
        JsfunctionDTO GetById(int id);
        JsfunctionDTO GetByName(string name);

        JsfunctionDTO Save(JsfunctionDTO item, bool refetch);

        IList<JsfunctionDTO> GetAll(JSFunctionType? type = null);
        IList<JsfunctionDTO> Search(string nameLike, int? pageNumber, int? itemsPerPage, bool includeArchived, ref int totalItems);
        void Delete(int id, bool archiveIfRequired, out bool archived);
    }
}
