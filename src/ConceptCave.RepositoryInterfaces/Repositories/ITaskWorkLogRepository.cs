﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public interface ITaskWorkLogRepository
    {
        ProjectJobTaskWorkLogDTO GetById(Guid id, ProjectJobTaskWorkLogLoadInstructions instructions);
        IList<ProjectJobTaskWorkLogDTO> GetById(Guid[] id, ProjectJobTaskWorkLogLoadInstructions instructions);

        ProjectJobTaskWorkLogDTO GetCurrentActive(Guid taskId, Guid userId, ProjectJobTaskWorkLogLoadInstructions instructions);

        ProjectJobTaskWorkLogDTO GetLast(Guid userId, ProjectJobTaskWorkLogLoadInstructions instructions);

        IList<ProjectJobTaskWorkLogDTO> GetCurrentActive(Guid userId, ProjectJobTaskWorkLogLoadInstructions instructions);

        IList<ProjectJobTaskWorkLogDTO> GetBetweenDates(Guid? companyId, Guid? userId, DateTime startDate, DateTime endDate, RepositoryInterfaces.Enums.ProjectJobTaskWorkLogLoadInstructions instructions);

        IList<ProjectJobTaskWorkLogDTO> GetBetweenDates(int rosterId, Guid? userId, DateTime startDate, DateTime endDate, TaskStatusFilter status, RepositoryInterfaces.Enums.ProjectJobTaskWorkLogLoadInstructions instructions, bool forTimesheetsOnly = false);

        ProjectJobTaskWorkLogDTO Save(ProjectJobTaskWorkLogDTO log, bool refetch, bool recurse);

        ProjectJobTaskWorkLogDTO New(Guid taskId, Guid userId, DateTime dateCreated);
    }
}
