﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public interface IPublishingGroupRepository
    {
        IEnumerable<PublishingGroupDTO> GetAll(PublishingGroupLoadInstruction instructions = PublishingGroupLoadInstruction.None);
        IEnumerable<PublishingGroupDTO> GetByAllowedForAssetContext(PublishingGroupLoadInstruction instructions);
        IEnumerable<PublishingGroupDTO> GetByAllowedForDashboard(PublishingGroupLoadInstruction instructions = PublishingGroupLoadInstruction.None);
        IEnumerable<PublishingGroupDTO> GetByAllowedForTaskWorkflows(PublishingGroupLoadInstruction instructions);
        IEnumerable<PublishingGroupDTO> GetByAllowedForUserContext(PublishingGroupLoadInstruction instructions = PublishingGroupLoadInstruction.None);
        PublishingGroupDTO GetById(int id, PublishingGroupLoadInstruction instructions = PublishingGroupLoadInstruction.None);
        PublishingGroupDTO GetByName(string name, PublishingGroupLoadInstruction instructions = PublishingGroupLoadInstruction.None);
        IEnumerable<PublishingGroupDTO> GetForLabels(Guid[] labels, PublishingGroupActorType type, PublishingGroupLoadInstruction instructions = PublishingGroupLoadInstruction.None);
        IEnumerable<PublishingGroupDTO> GetGroupsUserCanDo(Guid userId, PublishingGroupActorType actorType);
        IEnumerable<PublishingGroupDTO> GetGroupsUserCanDo(string userName, PublishingGroupActorType actorType);
        IEnumerable<PublishingGroupDTO> GetGroupsUserCanDo(UserDataDTO member, PublishingGroupActorType actorType);
        IEnumerable<PublishingGroupDTO> GetLikeName(string name);
        IEnumerable<UserDataDTO> GetUsersForGroup(int groupId, PublishingGroupActorType type);
        void Remove(int id);
        PublishingGroupDTO Save(PublishingGroupDTO publish, bool refetch, bool recurse);
        bool UserIsAllowedGroup(string userName, int groupId, PublishingGroupActorType type);
        bool UserIsAllowedGroup(string userName, PublishingGroupDTO group, PublishingGroupActorType type);
        bool UserIsAllowedGroup(UserDataDTO member, PublishingGroupDTO groupEnt, PublishingGroupActorType type);

    }
}
