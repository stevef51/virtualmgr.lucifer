﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public interface IVersionsRepository
    {
        VersionDTO GetVersion(string id, VersionLoadInstructions loadInstructions);

        void Save(VersionDTO dto);

        IEnumerable<VersionDTO> GetAll(VersionLoadInstructions loadInstructions);
        VersionDTO GetTenantMasterVersion(string id, VersionLoadInstructions loadInstructions);
    }
}
