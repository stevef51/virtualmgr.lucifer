﻿using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public enum GpsLocationType
    {
        GPS = 0,                // Location is based on GPS
        IndoorAtlas = 1,        // Location is based on Indoor Atlas
        StaticBeacon = 2,       // Location is of a Static Beacon (of which physical location is known)
        Task = 3,               // Location is of a specific Task (of which physical location is known)
        Building = 4,           // Location of a Facility Building
        CloseToStaticBeacon = 5,// Location is an estimated distance from a Static Beacon,
        BestEstimate = 6,       // Location of best estimated position
        BeaconTrilateration = 7 // Location based on Beacon Trilateration
    }

    public interface IGpsLocationRepository
    {
        GpsLocationDTO GetById(long id, GpsLocationLoadInstructions instructions = GpsLocationLoadInstructions.None);
        GpsLocationDTO Save(GpsLocationDTO dto, bool refetch, bool recurse);
        void Delete(long id, bool archiveIfRequired, out bool archived);
        GpsLocationDTO FindLastGPSLocation(GpsLocationType locationType, string tabletUUID);
    }
}
