﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public interface IBlockedRepository
    {
        void Save(BlockedDTO entity);
        BlockedDTO Load(Guid blockedId, Guid blockedById);
        IList<BlockedDTO> GetByWaitingForId(Guid blockedById);
        IList<BlockedDTO> GetByBlockedId(Guid blockedId);
        void Delete(BlockedDTO dto);
    }
}
