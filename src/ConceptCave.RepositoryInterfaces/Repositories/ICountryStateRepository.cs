﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public interface ICountryStateRepository
    {
        IList<CountryStateDTO> GetAll();

        CountryStateDTO GetById(int id);
    }
}
