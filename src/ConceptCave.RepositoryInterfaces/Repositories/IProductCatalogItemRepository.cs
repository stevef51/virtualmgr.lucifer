﻿using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public interface IProductCatalogItemRepository
    {
        ProductCatalogItemDTO GetById(int productId, int catalogId, ProductCatalogItemLoadInstructions instructions);

        ProductCatalogItemDTO Save(ProductCatalogItemDTO dto, bool refetch);

        void Remove(int productId, int catalogId);
        void DeleteFromCatalog(int catalogId);
    }
}
