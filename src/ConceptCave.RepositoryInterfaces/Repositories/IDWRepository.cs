﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public interface IDWPopulate
    {
        int Populate(int tenantId, IContinuousProgressCategory progress);
    }
}
