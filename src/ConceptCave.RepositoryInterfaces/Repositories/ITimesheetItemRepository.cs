﻿using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public interface ITimesheetItemRepository
    {
        TimesheetItemDTO GetById(int id);

        IList<TimesheetItemDTO> GetForRosterBetweenDates(int rosterId, Guid? userId, DateTime startDate, DateTime endDate, TimesheetItemLoadInstructions instructions);

        IList<TimesheetItemDTO> GetForRosterForExport(int rosterId, TimesheetItemLoadInstructions instructions);

        TimesheetItemDTO Save(TimesheetItemDTO dto, bool refetch, bool recurse);
    }
}
