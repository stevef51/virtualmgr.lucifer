﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.RepositoryInterfaces.Repositories
{

    public class MediaImportInstructions
    {
        public Guid UserId { get; set; }
        public bool TestOnly { get; set; }
    }

    public class MediaImportResult
    {
        public enum ResultType
        {
            Created,
            WouldCreate,
            Updated,
            WouldUpdate,
            NoChange,
            Error
        }

        public class Item
        {
            public string Name { get; set; }
            public string Result { get; set; }
            public ResultType ResultType { get; set; }
        }

        public List<Item> Items { get; set; }

        public MediaImportResult()
        {
            Items = new List<Item>();
        }
    }

    public interface IMediaImporterFactoryAsync
    {
        Task<IMediaImporterAsync> CreateAsync(IMediaRepository repo, Stream fromStream, MediaImportInstructions instructions);
    }

    public interface IMediaImporterAsync : IDisposable
    {
        Task<MediaImportResult> ImportFolderAsync(Guid? toFolderId);
    }
}
