﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public interface ICalendarRuleRepository
    {
        CalendarRuleDTO New(DateTime startDate, DateTime? endDate, CalendarRuleFrequency frequency, bool sunday, bool monday, bool tuesday, bool wednesday, bool thursday, bool friday, bool saturday);
        CalendarRuleDTO Save(CalendarRuleDTO rule, bool refetch, bool recurse);
    }
}
