﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public interface IReportingRepository
    {
        IList<CompletedWorkingDocumentPresentedFactDTO> GetContextAnswers(Guid workingDocumentId);

        IList<CompletedUserDimensionDTO> AddToUsersDimension(IList<Guid> userIds);
        IList<CompletedLabelDimensionDTO> AddToLabelsDimension(IList<LabelDTO> labels);
        CompletedWorkingDocumentDimensionDTO AddToWorkingDocumentDimension(WorkingDocumentDTO doc);

        CompletedWorkingDocumentFactDTO WriteNewWorkingDocumentFact(WorkingDocumentDTO doc,
            CompletedUserDimensionDTO reviewer, CompletedUserDimensionDTO reviewee,
            CompletedWorkingDocumentDimensionDTO documentDimension);

        void WriteLabelRelations(CompletedWorkingDocumentFactDTO doc, IList<CompletedLabelDimensionDTO> labels);

        void WriteLabelRelations(CompletedWorkingDocumentPresentedFactDTO presented,
            IList<CompletedLabelDimensionDTO> labels);

        IList<CompletedPresentedTypeDimensionDTO> AddToTypeDimension(IList<string> qTypeNames);

        CompletedWorkingDocumentFactDTO Save(CompletedWorkingDocumentFactDTO graph, bool refetch, bool recurse);

        void DeletePresentedFactsForDocument(Guid completedWorkingDocumentId);
    }
}
