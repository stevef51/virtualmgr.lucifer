using System;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    [Flags]
    public enum PublicTicketLoadInstructions
    {
        None = 0,
        WorkingDocumentTickets = 1,
        PublishingGroupResourceTickets = 2,
        PasswordReset = 4
    }

    public enum PublicTicketStatus
    {
        Disabled = 0,
        Enabled = 1
    }

    public interface IPublicTicketRepository
    {
        PublicTicketDTO GetById(Guid id, PublicTicketLoadInstructions instructions = PublicTicketLoadInstructions.None);

        void Save(PublicTicketDTO ticket, bool refetch, bool recurse);
    }
}
