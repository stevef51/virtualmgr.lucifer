﻿using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.RepositoryInterfaces.Repositories
{
    public interface IReportTicketRepository
    {
        void SaveTicket(ReportTicketDTO dto);

        ReportTicketDTO LoadTicket(Guid id, ReportTicketLoadInstructions loadInstructions);
    }
}
