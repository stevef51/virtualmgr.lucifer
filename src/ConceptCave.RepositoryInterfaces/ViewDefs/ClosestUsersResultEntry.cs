﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.RepositoryInterfaces.ViewDefs
{
    public class ClosestUsersResultEntry
    {
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public double Distance { get; set; }
        public Guid UserId { get; set; }
    }
}
