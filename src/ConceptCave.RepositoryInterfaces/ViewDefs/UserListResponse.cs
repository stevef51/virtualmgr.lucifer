﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.RepositoryInterfaces.ViewDefs
{
    public class UserListResponse
    {
        public Guid UserId { get; set; }
        public string Name { get; set; }
        public string Username { get; set; }
    }
}
