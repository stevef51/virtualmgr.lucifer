﻿using ConceptCave.RepositoryInterfaces.TaskEvents.Actions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.RepositoryInterfaces.TaskEvents
{
    public interface ITaskEventConditionActionFactory
    {
        ITaskEventConditionAction Create(string id);
    }
}
