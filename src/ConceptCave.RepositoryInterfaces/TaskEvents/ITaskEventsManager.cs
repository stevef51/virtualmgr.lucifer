﻿using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.TaskEvents.Conditions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.RepositoryInterfaces.TaskEvents
{
    public interface ITaskEventsManager
    {
        void Run(TaskEventStage stage, IList<ProjectJobTaskDTO> tasks, IList<ITaskEventCondition> conditions);
    }
}
