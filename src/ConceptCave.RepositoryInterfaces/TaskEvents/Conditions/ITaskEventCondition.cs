﻿using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.TaskEvents.Actions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.RepositoryInterfaces.TaskEvents.Conditions
{
    public interface ITaskEventCondition
    {
        void Configure(string data, TaskEventStage stage, IList<ITaskEventConditionAction> actions);
        /// <summary>
        /// Used to test the condition to see if it applies to a particular stage (schedule approval, end of day etc)
        /// </summary>
        /// <param name="stage">Stage to be tested</param>
        /// <returns>True if condition applies to the stage, false if not</returns>
        bool Applies(TaskEventStage stage);

        /// <summary>
        /// Called to have the condition apply its rules to the set of tasks handed in.
        /// </summary>
        /// <param name="tasks">Source set of tasks to be matched</param>
        /// <param name="matches">Set of tasks that meet the conditions criteria</param>
        /// <param name="notMatched">Set of tasks that don't mee the conditions criteria</param>
        IList<ProjectJobTaskDTO> Matches(IList<ProjectJobTaskDTO> tasks);

        IList<ITaskEventConditionAction> Actions { get; }

        /// <summary>
        /// The next time (in local time) that the condition will be run at. 
        /// </summary>
        /// <param name="referenceDate">The date time of the last time it was run, the parameter is treated as though it's in the timezone of the roster</param>
        /// <returns>Null if the condtion is not a timed condition, otherwise the DateTime it should next be run at</returns>
        DateTime? NextLocalRunTime(DateTime referenceDate);
    }
}
