﻿using ConceptCave.DTO.DTOClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.RepositoryInterfaces.TaskEvents
{
    /// <summary>
    /// A more specific task events manager that deals with the case where the conditions are attached to the roster.
    /// Implementors of this will typically just convert to the DTO's into ITaskEventCondition objects and call the more
    /// generic ITaskEventsManager Run implementation.
    /// </summary>
    public interface IRosterTaskEventsManager : ITaskEventsManager
    {
        void Run(TaskEventStage stage, IList<ProjectJobTaskDTO> tasks, IList<RosterEventConditionDTO> conditions);

        void SetNextUTCRunTime(DateTime referenceDate, TimeZoneInfo timezone, IList<RosterEventConditionDTO> conditions);
    }
}
