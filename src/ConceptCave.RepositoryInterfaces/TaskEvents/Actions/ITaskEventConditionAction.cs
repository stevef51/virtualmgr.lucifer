﻿using ConceptCave.DTO.DTOClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.RepositoryInterfaces.TaskEvents.Actions
{
    public interface ITaskEventConditionAction
    {
        void Configure(string data);
        void Apply(IList<ProjectJobTaskDTO> matches, IList<ProjectJobTaskDTO> notMatched);
    }
}
