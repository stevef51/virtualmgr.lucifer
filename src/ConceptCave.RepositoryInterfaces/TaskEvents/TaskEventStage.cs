﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.RepositoryInterfaces.TaskEvents
{
    [Flags]
    public enum TaskEventStage
    {
        ScheduleApprovalAutomatic = 1,
        EndOfDayAutomatic = 2,
        Time = 3
    }
}
