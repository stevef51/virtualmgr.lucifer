﻿using System;
using System.Collections.Generic;

namespace ConceptCave.RepositoryInterfaces
{
    public interface IContinuousProgress
    {
        IContinuousProgressCategory NewCategory(string name);

        void Finished();

        int ErrorCount();
    }

    public interface IContinuousProgressCategory
    {
        void NewItem();

        void Error(string message = null, params object[] formatArgs);
        void Complete(string message = null, params object[] formatArgs);

        void Warning(string message = null, params object[] formatArgs);

        void Working(string message = null, params object[] formatArgs);

        IContinuousProgressCategory NewCategory(string name);
    }
}