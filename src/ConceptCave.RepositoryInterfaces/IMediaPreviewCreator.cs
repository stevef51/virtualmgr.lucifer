﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.RepositoryInterfaces
{
    public interface IMediaPreviewCreator
    {
        int? CreateForMedia(Guid mediaId, Stream mediaStream);
    }
}
