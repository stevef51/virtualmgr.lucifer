﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.RepositoryInterfaces.PolicyManagement
{
    public interface IMediaPolicyManager
    {
        void Run();
        void Run(MediaFolderDTO current);
    }
}
