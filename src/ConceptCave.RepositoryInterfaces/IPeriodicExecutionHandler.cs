﻿using ConceptCave.RepositoryInterfaces;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.Repository.PeriodicExecution
{
    public interface IPeriodicExecutionHandlerResult
    {
        bool Success { get; }
        JObject Data { get; }
        JArray Issues { get; }
    }

    public class PeriodicExecutionHandlerResult : IPeriodicExecutionHandlerResult
    {
        public bool Success { get; set; }

        public JObject Data { get; set; }
        public JArray Issues { get; set; }
    }

    public interface IPeriodicExecutionHandler
    {
        string Name { get; }
        IPeriodicExecutionHandlerResult Execute(JObject parameters, IContinuousProgressCategory progress);
    }
}
