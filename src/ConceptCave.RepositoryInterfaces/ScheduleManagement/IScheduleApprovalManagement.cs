﻿using ConceptCave.DTO.DTOClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.RepositoryInterfaces.ScheduleManagement
{
    public interface IScheduleApprovalManagement
    {
        DateTime GetStartOfCurrentShiftInTimezone(int rosterId, TimeZoneInfo tz);
        DateTime GetStartOfCurrentShiftInTimezone(RosterDTO roster, TimeZoneInfo tz);
        ScheduleApprovalJson Buckets(int rosterId, DateTime date, TimeZoneInfo tz, Guid userId, bool includeWorkloading, bool includeRootNode = false);
        ScheduleApprovalJson Buckets(int rosterId, DateTime date, TimeZoneInfo tz, bool includeWorkloading, bool includeRootNode = false);
        BucketJson ApproveUnchanged(DateTime date, TimeZoneInfo tz, int roster, int[] slots, Guid processedByUserId, bool includeWorkloading);
        void RejectCancelledSecondaryTasks(IEnumerable<SlotJson> slots, Guid processedByUserId);
        void ApprovePrimaryBuckets(IEnumerable<BucketJson> buckets, TimeZoneInfo tz, Guid processedByUserId);
        SlotJson LoadSlot(int id, DateTime date, TimeZoneInfo tz, bool includeWorkloading);
    }
}
