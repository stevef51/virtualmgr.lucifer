﻿using ConceptCave.DTO.DTOClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.RepositoryInterfaces.ScheduleManagement
{
    public interface IEndOfDayManagement
    {
        DateTime GetStartOfCurrentShiftInTimezone(int rosterId, TimeZoneInfo tz);
        DateTime GetStartOfCurrentShiftInTimezone(RosterDTO roster, TimeZoneInfo tz);

        List<EndOfDayBucketJson> Buckets(int rosterId, Guid userId, DateTime rosterStartDate, TimeZoneInfo tz);
        void Approve(List<EndOfDayBucketJson> buckets, Guid userId);

    }
}
