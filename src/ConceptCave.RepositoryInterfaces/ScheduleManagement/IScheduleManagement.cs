﻿using ConceptCave.DTO.DTOClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.RepositoryInterfaces.ScheduleManagement
{
    public interface IScheduleManagement
    {
        DateTime GetDSTSafeStartTime(RosterDTO roster, DateTime dateofShiftInRosterTimezone, TimeZoneInfo rosterTZ);
        DateTime GetStartOfShiftInTimezone(RosterDTO roster, DateTime dateOfShiftInRosterTimezone, TimeZoneInfo rosterTZ, TimeZoneInfo outputTZ);
        DateTime GetStartOfCurrentShiftInTimezone(int rosterId, TimeZoneInfo tz);
        DateTime GetStartOfCurrentShiftInTimezone(int rosterId, TimeZoneInfo tz, IDictionary<int, RosterDTO> rosterCache);
        DateTime GetStartOfCurrentShiftInTimezone(RosterDTO roster, TimeZoneInfo tz);
        DateTime GetEndOfCurrentShiftInTimezone(int rosterId, TimeZoneInfo tz);
        DateTime GetEndOfCurrentShiftInTimezone(int rosterId, TimeZoneInfo tz, IDictionary<int, RosterDTO> rosterCache);
        DateTime GetEndOfCurrentShiftInTimezone(RosterDTO rosteer, TimeZoneInfo tz);
        DateTime GetEndOfShiftInTimezone(RosterDTO roster, DateTime startOfShiftInRosterTimezone, TimeZoneInfo rosterTZ, TimeZoneInfo outputTZ);
    }
}
