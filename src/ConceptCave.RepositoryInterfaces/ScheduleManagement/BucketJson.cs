﻿using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Workloading;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.RepositoryInterfaces.ScheduleManagement
{
    public class ScheduleApprovalJson
    {
        public IList<BucketJson> PrimaryBuckets { get; set; }
        public BucketJson SecondaryBucket { get; set; }

        public ScheduleApprovalJson()
        {
            PrimaryBuckets = new List<BucketJson>();
        }
    }

    public class EndOfDayBucketJson
    {
        public BucketJson PrimaryBucket { get; set; }
        public BucketJson SecondaryBucket { get; set; }
    }

    /// <summary>
    /// Top level object for storing the information to do with the schedule for a roster
    /// </summary>
    public class BucketJson
    {
        /// <summary>
        /// The date on which this bucket is storing data
        /// </summary>
        public DateTime Date { get; set; }
        /// <summary>
        /// The id of the roster that this bucket represents
        /// </summary>
        public int RosterId { get; set; }

        public string RosterName { get; set; }
        /// <summary>
        /// The role the current user occupies in this roster
        /// </summary>
        public string Role { get; set; }
        /// <summary>
        /// The set of slots (users) to be scheduled
        /// </summary>
        public List<SlotJson> Slots { get; set; }

        public BucketJson()
        {
            Slots = new List<SlotJson>();
        }
    }

    /// <summary>
    /// Stores the schedule to do with a user in a roster
    /// </summary>
    public class SlotJson
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int RoleId { get; set; }
        public string Role { get; set; }
        public Guid? UserId { get; set; }

        /// <summary>
        /// Indicates wether or not the slot contains normal tasks assigned to a role/person or 
        /// if its being used as a holder for something else (like tasks from another roster)
        /// </summary>
        public bool IsNormal { get; set; }

        public Guid? OverriddenUserId { get; set; }
        public string OverriddenUserName { get; set; }

        public List<ScheduleJson> AssignedTasks { get; set; }
        public int AssignedTasksCount { get; set; }
        public List<ScheduleJson> UnassignedTasks { get; set; }
        public int UnassignedTasksCount { get; set; }
        public List<ScheduleJson> CancelledTasks { get; set; }
        public int CancelledTasksCount { get; set; }
        public List<ScheduleJson> FinishedByManagementTasks { get; set; }
        public int FinishedByManagementTasksCount { get; set; }

        public bool IsHeader { get; set; }

        public bool AssignsTasksThroughLabels { get; set; }

        public SlotJson()
        {
            IsNormal = true;

            AssignedTasks = new List<ScheduleJson>();
            UnassignedTasks = new List<ScheduleJson>();
            CancelledTasks = new List<ScheduleJson>();
            FinishedByManagementTasks = new List<ScheduleJson>();
        }

        /// <summary>
        /// This just populates the various count values and then strips off the actual schedulejson properties.
        /// This can be used to get summary information about a slot without getting the details
        /// </summary>
        public void PrepareAsHeader()
        {
            AssignedTasksCount = AssignedTasks.Count;
            AssignedTasks = new List<ScheduleJson>();

            UnassignedTasksCount = UnassignedTasks.Count;
            UnassignedTasks = new List<ScheduleJson>();

            CancelledTasksCount = CancelledTasks.Count;
            CancelledTasks = new List<ScheduleJson>();

            FinishedByManagementTasksCount = FinishedByManagementTasks.Count;
            FinishedByManagementTasks = new List<ScheduleJson>();

            IsHeader = true;
        }

        public void PrepareAsDetailed()
        {
            AssignedTasksCount = AssignedTasks.Count;
            UnassignedTasksCount = UnassignedTasks.Count;
            CancelledTasksCount = CancelledTasks.Count;
            FinishedByManagementTasksCount = FinishedByManagementTasks.Count;

            IsHeader = false;
        }
    }

    public enum ScheduleJsonType
    {
        Task,
        Scheduled,
        OtherRoster
    }

    [DataContract]
    /// <summary>
    /// Stores a Task or scheduled task
    /// </summary>
    public class ScheduleJson
    {
        public ScheduleJson()
        {
            Translated = new List<ScheduleTranslation>();
            ReplicatedTasks = new List<ProjectJobTaskDTO>();
        }

        [DataMember]
        public bool __IsNew { get; set; }
        [DataMember]
        public Guid Id { get; set; }
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public int RosterId { get; set; }
        [DataMember]
        public Guid TaskTypeId { get; set; }
        [DataMember]
        public string TaskTypeName { get; set; }
        [DataMember]
        public int? RoleId { get; set; }
        [DataMember]
        public string RoleName { get; set; }
        [DataMember]
        public Guid? UserId { get; set; }
        [DataMember]
        public string OwnerName { get; set; }
        [DataMember]
        public Guid? SiteId { get; set; }
        [DataMember]
        public string SiteName { get; set; }
        [DataMember]
        public Guid? OriginalUserId { get; set; }
        [DataMember]
        public int Level { get; set; }
        [DataMember]
        public DateTime? DateCreated { get; set; }
        [DataMember]
        public DateTime? CompareDate { get; set; }
        [DataMember]
        public decimal? EstimatedDuration { get; set; }
        [DataMember]
        public string FromUserName { get; set; }
        [DataMember]
        public List<ScheduleTranslation> Translated { get; set; }
        [DataMember]
        public bool CanEdit { get; set; }
        [DataMember]
        public string StartTime { get; set; }

        public ProjectJobTaskDTO Task { get; set; }

        public IList<ProjectJobTaskDTO> ReplicatedTasks { get; set; }
        public HierarchyBucketDTO Bucket { get; set; }

        public void FromDto(ProjectJobTaskDTO task, DateTime compareDate, bool includeWorkloading, ScheduleJsonType type = ScheduleJsonType.Task)
        {
            this.Id = task.Id;
            this.Type = type.ToString();
            this.Name = task.Name;
            this.Description = task.Description;
            this.RosterId = task.RosterId;
            this.RoleId = task.RoleId;
            this.UserId = task.OwnerId;
            this.OriginalUserId = task.OriginalOwnerId;
            this.Level = task.Level;
            this.DateCreated = task.DateCreated;
            this.CompareDate = compareDate.ToUniversalTime();
            this.TaskTypeId = task.TaskTypeId;
            this.SiteId = task.SiteId;
            if(task.StartTimeUtc.HasValue == true)
            {
                // Have to extract the Local Time from the Task (instances) StartTimeUtc field
                this.StartTime = task.StartTimeUtc.Value.TimeOfDay.ToString();
            }

            if(includeWorkloading == true)
            {
                this.EstimatedDuration = task.AllActivitiesEstimatedDuration;
            }

            if (task.ProjectJobTaskType != null)
            {
                this.TaskTypeName = task.ProjectJobTaskType.Name;
            }

            if (task.SiteId.HasValue && task.Site != null)
            {
                this.SiteName = task.Site.Name;
            }

            if (task.Site != null)
            {
                this.OwnerName = task.Owner.Name;
            }

            if (task.HierarchyRole != null)
            {
                this.RoleName = task.HierarchyRole.Name;
            }

            foreach (var trans in task.Translated)
            {
                this.Translated.Add(new ScheduleTranslation() { Id = trans.Id, CultureName = trans.CultureName, Name = trans.Name, Description = trans.Description, });
            }
        }

        public void FromDto(ProjectJobScheduledTaskDTO schedule, Guid? userId, DateTime localDate, TimeZoneInfo timezone, IWorkloadingActivityFactory activityFactory, IWorkloadingDurationCalculator durationCalculator, ScheduleJsonType type = ScheduleJsonType.Scheduled)
        {
            this.Id = schedule.Id;
            this.Type = type.ToString();
            this.Name = schedule.Name;
            this.Description = schedule.Description;
            this.RosterId = schedule.RosterId;
            this.RoleId = schedule.RoleId;
            this.UserId = userId;
            this.OriginalUserId = userId;
            this.Level = schedule.Level;
            this.TaskTypeId = schedule.TaskTypeId;
            this.SiteId = schedule.SiteId;

            if (schedule.StartTime.HasValue == true)
            {
                // The TaskSchedule StartTime is already a TimeSpan
                this.StartTime = schedule.StartTime.Value.ToString();
            }

            if (schedule.ProjectJobTaskType != null)
            {
                this.TaskTypeName = schedule.ProjectJobTaskType.Name;
            }

            if (schedule.SiteId.HasValue && schedule.UserData_ != null)
            {
                this.SiteName = schedule.UserData_.Name;
            }

            foreach (var trans in schedule.Translated)
            {
                this.Translated.Add(new ScheduleTranslation() { Id = trans.Id, CultureName = trans.CultureName, Name = trans.Name, Description = trans.Description, });
            }

            this.EstimatedDuration = null;

            if(activityFactory != null)
            {
                this.EstimatedDuration = 0; 
                foreach (var activity in schedule.ProjectJobScheduledTaskWorkloadingActivities)
                {
                    if (activityFactory.ShouldCreate(activity, localDate, timezone) == false)
                    {
                        continue;
                    }

                    if (activity.EstimatedDuration.HasValue == true)
                    {
                        this.EstimatedDuration += activity.EstimatedDuration.Value;
                        continue;
                    }

                    // we need to calculate this
                    IWorkloadingActivity wlActivity = activityFactory.NewForFeature(activity.WorkloadingStandardId, activity.SiteFeatureId);
                    durationCalculator.EstimateDuration(wlActivity);

                    this.EstimatedDuration += wlActivity.EstimatedDuration;
                }
            }
        }
    }

    public class ScheduleTranslation
    {
        public bool __IsNew { get; set; }
        public Guid Id { get; set; }
        public string CultureName { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }

}
