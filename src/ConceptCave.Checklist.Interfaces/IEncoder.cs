﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConceptCave.Core.Coding
{
    public interface IEncoder : ICoder
    {
        void Encode<T>(string name, T value);
    }
}
