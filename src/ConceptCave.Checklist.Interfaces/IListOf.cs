﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace ConceptCave.Core
{
    public interface IListOf<TItem> : IUniqueNode, IEnumerable<TItem>, IList<TItem>, ICollection<TItem>, IList, ICollection, IEnumerable
    {
        List<TItem> Items { get; }
    }

}
