﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Core;

namespace ConceptCave.Checklist.Interfaces
{
    public interface IAnswer : IUniqueNode
    {
        /// <summary>
        /// Answer supplied by the user to the question
        /// </summary>
        object AnswerValue { get; }

        string AnswerAsString { get; }

        /// <summary>
        /// The Pass fail status of the question (typically "Pass", "Fail" or "NA")
        /// </summary>
        bool? PassFail { get; set; }

        /// <summary>
        /// What the score was for the question
        /// </summary>
        decimal Score { get; }

        /// <summary>
        /// Any extra notes entered against the question
        /// </summary>
        IAnswerNotes Notes { get; }

        DateTime UtcDateAnswered { get; }

        /// <summary>
        /// The Question this Answer came from
        /// </summary>
        IPresentedQuestion PresentedQuestion { get; }

        void CopyFrom(IAnswer answer);
    }

    public interface IAnswerExtraValues : IAnswer
    {
        Dictionary<string, object> ExtraValues { get; set; }
    }

}
