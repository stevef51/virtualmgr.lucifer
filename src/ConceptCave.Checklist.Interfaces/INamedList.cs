﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace ConceptCave.Checklist.Interfaces
{
    public interface INamedList<T> : IList<T> where T : IHasName
    {
        int BaseIndex { get; }
        T Lookup(int index);
        T Lookup(object name);
    }
}
