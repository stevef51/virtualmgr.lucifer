﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Core.Coding;

namespace ConceptCave.Checklist.Interfaces
{
    public interface IVariable : ICodable
    {
        string Name { get; }
        object ObjectValue { get; set; }
    }

    public interface IVariable<T> : IVariable
    {
        T Value { get; set; }
    }
}
