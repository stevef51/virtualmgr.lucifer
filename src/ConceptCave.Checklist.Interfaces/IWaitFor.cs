﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConceptCave.Checklist.Interfaces
{
    public interface IBlocker
    {
        Guid BlockedById { get; }
        bool IsBlocked { get; }
        IEnumerable<IBlockable> Blockables { get; }
        void Block(IBlockable blockable);
        void Unblock();
    }

    public interface IBlockable
    {
        bool IsBlocked { get; }
        IEnumerable<IBlocker> Blockers { get; }
        IBlocker LastUnblockedBlocker { get; }
        void BlockOn(IBlocker blocker);
        void UnblockFrom(IBlocker blocker);
    }
}
