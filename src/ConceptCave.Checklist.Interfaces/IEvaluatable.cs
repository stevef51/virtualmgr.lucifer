﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;

namespace ConceptCave.Checklist.Interfaces
{
    public interface IEvaluatable 
    {
        object Evaluate(IContextContainer context);
    }
}
