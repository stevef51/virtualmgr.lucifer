﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Core;

namespace ConceptCave.Checklist.Interfaces
{
    public interface IMultiChoiceAnswer : IAnswer
    {
        IListOf<IMultiChoiceItem> ChosenItems { get; }
    }
}
