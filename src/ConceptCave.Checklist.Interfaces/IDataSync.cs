﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace ConceptCave.Checklist.Interfaces
{
    public class DataSyncException : ApplicationException
    {
        public DataSyncException(string message)
            : base(message)
        {
        }

        public DataSyncException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }

    public interface IDataSync
    {
        void Synchronise(DateTime syncDateUTC, IDataSyncSrc source, IDataSyncDest destination);
    }

    public interface IDataSyncDest
    {
        IDataSyncSessionDest OpenSession(DateTime syncDateUTC);
    }

    public interface IDataSyncSessionDest : IDisposable
    {
        DateTime SyncDateUTC { get; }
        IDataSyncTableDest OpenTable(string table);
    }

    public interface IDataSyncTableDest : IDisposable
    {
        string TableName { get; }
        int InsertRow(IDataSyncRow row);
        int UpdateRow(IDataSyncRow row);
        int DeleteRow(IDataSyncRow row);
    }

    public interface IDataSyncSrc
    {
        IDataSyncSessionSrc OpenSession(DateTime syncDateUTC);
    }

    public interface IDataSyncSessionSrc : IDisposable
    {
        DateTime SyncDateUTC { get; }
        string[] SyncTables { get; }
        IDataSyncTableSrc OpenTable(string table);
    }

    public interface IDataSyncTableSrc : IDisposable
    {
        string TableName { get; }
        IEnumerable<IDataSyncRow> Rows { get; }
    }

    public enum DataSyncAction
    {
        Insert  = 0,
        Update  = 1,
        Delete  = 2
    }

    public interface IDataSyncPrimaryKey
    {
        IEnumerable<IDataSyncColumn> KeyColumns { get; }
    }

    public interface IDataSyncRow
    {
        DataSyncAction Action { get; }

        DateTime ActionTimeStampUTC { get; }
        IDataSyncPrimaryKey PrimaryKey { get; }
        IEnumerable<IDataSyncColumn> Columns { get; }
    }
    
    public interface IDataSyncColumn
    {
        string Name { get; }
        object Value { get; }
    }
}
