﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using ConceptCave.Checklist.Interfaces;

namespace ConceptCave.Checklist.Interfaces
{
    public interface ILingoDataColumn : IObjectGetter
    {
        string Name { get; }
        Type EntityType { get; }
        Type DataType { get; }
        Expression LinqReference(ParameterExpression pe);
    }
}
