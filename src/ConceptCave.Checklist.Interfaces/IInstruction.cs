﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Core;

namespace ConceptCave.Checklist.Interfaces
{
    public enum InstructionResult
    {
        Unfinished,
        Finished
    }

    public interface ILingoInstruction
    {
        int Address { get; set; }
        void Reset(ILingoProgram program, bool recurse);
        InstructionResult Execute(IContextContainer context);        
    }

    public interface ILingoInstructionBlock
    {
        ILingoInstruction CurrentInstruction { get; }
    }
}
