﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Core;

namespace ConceptCave.Checklist.Interfaces
{
    public interface IObjectGetter
    {
        bool GetObject(IContextContainer context, string name, out object result);
    }

    public interface IObjectSetter : IObjectGetter
    {
        bool SetObject(IContextContainer context, string name, object value);
    }
}
