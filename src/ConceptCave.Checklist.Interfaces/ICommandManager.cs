﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConceptCave.Core
{
    public interface ICommandManager : IUniqueNode, IHasDocument
    {
        void Do(ICommand command);
        void Undo();
        void Redo();

        ICommand PeekUndo();
        ICommand PeekRedo();

        bool CanUndo { get; }
        bool CanRedo { get; }

        bool RecordingDisabled { get; set; }
    }
}
