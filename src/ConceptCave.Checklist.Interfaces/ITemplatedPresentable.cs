﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConceptCave.Checklist.Interfaces
{
    public interface ITemplatedPresentable : IPresentable
    {
        int? TemplatePublishedResourceId { get; set; }
        Guid? TemplateQuestionId { get; set; }
        void MutateFromTemplate(ITemplatedPresentable template);
    }
}
