﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConceptCave.Core.Coding
{
    public interface IDecoder : ICoder
    {
        T Decode<T>(string name);
        T Decode<T>(string name, T defaultValue);
        bool Decode<T>(string name, out T value);

        List<IDecoderPostProcessing> PostProcessingRequired { get; } 
    }

    // marker interface for objects to indicate they require post processing
    public interface IDecoderPostProcessing {} 
}
