﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Core.Coding;
using ConceptCave.Core;

namespace ConceptCave.Checklist.Interfaces
{
    public interface IPlayerStringRenderer : ICodable
    {
        string RenderIntoString(IPresented presented, IWorkingDocument wd, IContextContainer context);

        string ReplaceUrls(string html);
        string ReplaceIcons(string html);
    }
}
