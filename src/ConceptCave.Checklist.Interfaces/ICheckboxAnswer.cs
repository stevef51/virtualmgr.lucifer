﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConceptCave.Checklist.Interfaces
{
    public interface ICheckboxAnswer : IAnswer
    {
        bool? AnswerBool { get; }
    }
}
