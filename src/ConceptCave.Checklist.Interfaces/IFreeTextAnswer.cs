﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConceptCave.Checklist.Interfaces
{
    public interface IFreeTextAnswer : IAnswer
    {
        string AnswerAsString { get; }

        void SetValue(string value);
    }
}
