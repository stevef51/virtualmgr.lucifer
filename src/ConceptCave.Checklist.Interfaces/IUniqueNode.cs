﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using ConceptCave.Core;
using ConceptCave.Core.Coding;

namespace ConceptCave.Core
{
    public interface IUniqueNode : ICodable
    {
        Guid Id { get; }
        void UseDecodedId(Guid id);
    }

    public interface IIdAssignableUniqueNode : IUniqueNode
    {
        void ChangeId(Guid newId);
    }
}
