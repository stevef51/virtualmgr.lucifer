﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;
using ConceptCave.Core.Coding;

namespace ConceptCave.Checklist.Interfaces
{
    public enum RunMode
    {
        Running,
        Presenting,
        Blocked,
        Finishing,
        Finished
    }

    public interface ILingoProgram : ICodable, IContextContainer
    {
        ILingoProgramDefinition ProgramDefinition { get; }
        IWorkingDocument WorkingDocument { get; }
        IVariable<RunMode> RunMode { get; }
        IVariable<T> PrivateVariable<T>(ILingoInstruction instruction, string keyName, T defaultValue);
        void Do(ICommand command);
        InstructionResult RunUntilStopped();
        InstructionResult RunFinisher();

        string ExpandString(string template);
        string ExpandString(string template, Func<string, string> fnEscape);
    }

    public interface ILingoProgramDefinition : ICodable
    {
        string SourceCode { get; set; }
        ILingoProgram Compile(IWorkingDocument document);
        ILingoInstruction ProgramRoot { get; }
        List<ILingoInstruction> AllInstructions { get; }
    }
}
