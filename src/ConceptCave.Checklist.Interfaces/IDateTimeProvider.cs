﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.Checklist.Interfaces
{
    // By interfacing out DateTime.UtcNow we can unit test against any point in time 
    public interface IDateTimeProvider
    {
        DateTime UtcNow { get; }
    }
}
