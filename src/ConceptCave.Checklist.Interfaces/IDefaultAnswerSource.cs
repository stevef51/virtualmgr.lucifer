﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Core;

namespace ConceptCave.Checklist.Interfaces
{
    public interface IDefaultAnswerSource : IUniqueNode
    {
        object Get(IQuestion question, IWorkingDocument workingDocument, Guid containerId, Dictionary<string, int> presentationCache);

        bool Add(IPresentedQuestion presented, object answer, Guid containerId, IWorkingDocument workingDocument);

        void PopulateFrom(IWorkingDocument wd);
    }

    public interface IDoNotStoreInAnswerSource : IQuestion
    { }
}
