﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConceptCave.Checklist.Interfaces
{
    public enum PayPalAnswerStatus
    {
        /// <summary>
        /// The answer has been created, but nothing has been done with it
        /// </summary>
        Created,
        /// <summary>
        /// The answer has been indicated to having gone off to PayPal, but we've not heard anything back from PayPal
        /// regarding it
        /// </summary>
        Pending,
        /// <summary>
        /// PayPal have come back and have confirmed that the payment has gone through
        /// </summary>
        Confirmed,
        /// <summary>
        /// PayPal have come back and have rejected the payment
        /// </summary>
        Rejected,
        /// <summary>
        /// The payment has been refunded for somereason through PayPal
        /// </summary>
        Refunded
    }

    public interface IPayPalIPN
    {
        string RawData { get; }

        /// <summary>
        /// Maps to the txn_id in the IPN message
        /// </summary>
        string TransactionId { get; }
        /// <summary>
        /// Maps to the txn_type in the IPN message
        /// </summary>
        string TrasnactionType { get; }

        string PayerEmail { get; }
        string PayerStatus { get; }
        string PayerFirstname { get; }
        string PayerSurname { get; }

        string ItemName { get; }
        decimal Amount { get; }

        string PaymentStatus { get; }
        string PaymentType { get; }
    }

    public interface IPayPalAnswer
    {
        PayPalAnswerStatus Status { get; }

        string StatusReason { get; }

        void AddNotification(string ipnRequestData);

        List<IPayPalIPN> Notifications { get; }
    }
}
