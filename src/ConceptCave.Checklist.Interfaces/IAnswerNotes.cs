﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Core;

namespace ConceptCave.Checklist.Interfaces
{
    public interface IAnswerNotes : IUniqueNode
    {
        string Text { get; set; }

        List<IAnswerNotesAttachment> Attachments { get; }
    }

    public interface IAnswerNotesAttachment
    {
        /// <summary>
        /// Name of the attachment. Stored in the API so that we don't have to keep going back to the provider for this info
        /// </summary>
        string Name { get; set; }

        string ContentType { get; set; }

        /// <summary>
        /// Id of the attachment from provider.
        /// </summary>
        Guid ProviderId { get; set; }
    }
}
