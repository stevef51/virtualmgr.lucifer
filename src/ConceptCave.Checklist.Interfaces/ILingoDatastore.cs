﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using ConceptCave.Core;

namespace ConceptCave.Checklist.Interfaces
{
    public interface ILingoDatastore
    {
        IDictionary<string, ILingoDataColumn> Columns { get; }
        Type EntityType { get; }
        ILingoDatastoreTransaction BeginTransaction(IContextContainer context);
    }
}