﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConceptCave.Checklist.Interfaces
{
    public interface IPdfServiceConnector
    {
        byte[] GetPdfBytes(string html);
        void AttachPdfToUser(string html, Guid userId, string fileName, string name, string description);
    }
}
