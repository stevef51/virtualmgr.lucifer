﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.Checklist.Interfaces
{
    public interface ITokenGenerator
    {
        string GenerateToken(string username, int expireMinutes = 20);
    }
}
