﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConceptCave.Core
{
    public interface IWatchableStack<T> : IEnumerable<T>
    {
        event Action<IWatchableStack<T>, T> OnBeforePush;
        event Action<IWatchableStack<T>> OnAfterPush;
        event Action<IWatchableStack<T>> OnBeforePop;
        event Action<IWatchableStack<T>, T> OnAfterPop;

        void Push(T item);
        T Pop();
        int Count { get; }
        T Peek(int indexFromTop);
    }
}
