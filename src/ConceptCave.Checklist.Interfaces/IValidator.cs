﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Core;
using Newtonsoft.Json.Linq;

namespace ConceptCave.Checklist.Interfaces
{
    public interface IInvalidReason
    {
        string Reason { get; }
        IAnswer Answer { get; }
    }

    public interface IValidatorResult
    {
        bool Invalid { get; }
        List<IInvalidReason> InvalidReasons { get; }
        void AddInvalidReason(string reason, IAnswer answer);
        void AddInvalidReason(string reason);
    }

    public interface IProcessingRuleContext
    {
        IAnswer Answer {get; set;}
        IValidatorResult ValidatorResult {get; set;}
        IContextContainer Context {get; set;}
        ProcessDirection Direction { get; set; }
    }

    public interface IProcessingRule : IUniqueNode
    {
        string Name { get; set; }
        int Order { get; }
        void Process(IProcessingRuleContext context);

        JObject ToJson();
    }

    public interface IMandatoryValidatorOverride
    {
        bool IsValid();
    }
}
