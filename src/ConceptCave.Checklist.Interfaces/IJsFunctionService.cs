﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.Checklist.Interfaces
{
    public class JsExecuteResult<TResult>
    {
        public TResult result { get; set; }
    }

    public interface IJsFunctionService
    {
        JsExecuteResult<TResult>  Execute<TResult>(string name, object args);
        Task<JsExecuteResult<TResult>> ExecuteAsync<TResult>(string name, object args);
        JsExecuteResult<TResult> Execute<TResult>(int id, object args);
        Task<JsExecuteResult<TResult>> ExecuteAsync<TResult>(int id, object args);

        void Reload();
    }
}
