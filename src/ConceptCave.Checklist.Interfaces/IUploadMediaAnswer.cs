﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Core;

namespace ConceptCave.Checklist.Interfaces
{
    public interface IUploadMediaAnswer
    {
        IListOf<IUploadMediaAnswerItem> Items { get; }
    }

    public interface IUploadMediaAnswerItem : IUniqueNode
    {
        string Filename { get; set; }
        string ContentType { get; set; }
        string Name { get; set; }
        string Description { get; set; }
        Guid MediaId { get; set; }
        DateTime UploadDate { get; set; }
    }
}
