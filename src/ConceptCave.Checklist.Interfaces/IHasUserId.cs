﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.Checklist.Interfaces
{
    /// <summary>
    /// Lingo helper interface so that an object with a userid can just be assigned to the SelectUserQuestion
    /// without having to refer to the userid in Lingo script
    /// </summary>
    public interface IHasUserId
    {
        Guid UserId { get; }
    }
}
