﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using ConceptCave.Core;

namespace ConceptCave.Checklist.Interfaces
{
    public interface IHasName
    {
        /// <summary>
        /// User readable unique name of the Presentable thing
        /// </summary>
        string Name { get; set; }
    }

    /// <summary>
    /// Something that has a Localisable Name for display - note, cannot be used as a Key since it changes dependant on CurrentUICulture
    /// </summary>
    public interface IHasDisplayName
    {
        string DisplayName { get; set; }
        ILocalisableString LocalisableDisplayName { get; }
    }

    public interface IHasIndex
    {
        Func<int> Index { get; set; }
    }

    // Something that is Presentable either directly or indirectly through its children
    public interface IPresentable
    {
        IPresented CreatePresented(IWorkingDocument document, string nameAs, IDisplayIndexProvider parent);

        /// <summary>
        /// Allows this to be queried in linq queries (so something like from s in section where s.id==someid select s), with all children being searched over
        /// </summary>
        /// <returns></returns>
        IEnumerable<IPresentable> AsDepthFirstEnumerable();

        IClientModel ClientModel { get; }

        /// <summary>
        /// If this field is set on a Presentable object, then it does not get presented at all by the present command in lingo. This therefore has the side effects that this object is never rendered or even sent to the client side, validation rules are not run on this object, and it is not recorded in the reporting tables.
        /// </summary>
        bool Enabled { get; set; }
    }

    public interface ILocalisableString
    {
        string EnglishValue { get; set; }
        string CurrentCultureValue { get; set; }

        string GetCultureValue(string cultureName);
        void SetCultureValue(string cultureName, string value);

        IDictionary<string, string> ToDictionary();
        void FromDictionary(IDictionary<string, string> translations);
    }

    public interface IQuestion : IHasName, IHasIndex, IPresentable, IUniqueNode
    {
        /// <summary>
        /// The text of the Question to the user
        /// </summary>
        string Prompt { get; set; }
        ILocalisableString LocalisablePrompt { get; }
        bool HidePrompt { get; set; }


        string Note { get; set; }
        ILocalisableString LocalisableNote { get; }

        bool AllowNotesOnAnswer { get; set; }

        /// <summary>
        /// Maximum score that could be received as a part of answering the question
        /// </summary>
        decimal PossibleScore { get; set; }

        object DefaultAnswer { get; set; }

        IAnswer CreateBlankAnswer(IPresentedQuestion presentedQuestion);

        /// <summary>
        /// List of Validators attached to this Question
        /// </summary>
        List<IProcessingRule> Validators { get; }

        bool IsAnswerable { get; }

        IHtmlPresentationHint HtmlPresentation { get; }

        //Labels
        ISet<Guid> Labels { get; }

        HtmlPresentationHintLabelPlacement HelpPosition { get; set; }
        string HelpContent { get; set; }
        ILocalisableString LocalisableHelpContent { get; }
    }

    public interface IDisplayIndexFormatter
    {
        string Format(IDisplayIndex displayIndex);
    }

    public interface IDisplayIndex
    {
        bool IsLeaf { get; }
        IEnumerable<int> Indexes { get; }
        string Format(IDisplayIndexFormatter formatter);
    }

    public interface IDisplayIndexProvider
    {
        IDisplayIndexProvider Parent { get; }
        int IndexOfChild(IDisplayIndexProvider child);

        IDisplayIndex DisplayIndex { get; }
    }

    public interface IClientModel : IUniqueNode
    {
        bool Visible { get; set; }
        List<IClientModelEvent> Events { get; }
    }

    public interface IClientModelEvent : IUniqueNode
    {
        string Name { get; set; }
        IClientModelEventAction Action { get; set; }
    }

    public interface IClientModelEventAction : IUniqueNode
    {

    }

    public interface ITargettedClientModelEventAction : IClientModelEventAction
    {
        string TargetName { get; set; }
    }


    public interface IClientModelVisibilityAction : ITargettedClientModelEventAction
    {
        ClienModelAnimationType EnterAnimation { get; set; }
        ClienModelAnimationType LeaveAnimation { get; set; }
    }

    public enum ClienModelAnimationType
    {
        None,
        Fade,
        Slide,
        Drop,
        Explode
    }

    public interface IPresented : IUniqueNode, IDisplayIndexProvider
    {
        IEnumerable<IPresented> AsDepthFirstEnumerable();
        IPresentable Presentable { get; }

        DateTime UtcDatePresented { get; }
        /// <summary>
        /// Field used to flag if the presented is being presented with a hidden clause (present hidden section 1)
        /// This results in the GUI not being shown, though outside of that, from the objects point of view its been presented.
        /// </summary>
        bool Hidden { get; set; }
    }

    /// <summary>
    /// Represents an actual instance of a Question being asked, it has a Sequence # indicating how many times this Question has been asked 
    /// </summary>
    public interface IPresentedQuestion : IPresented
    {
        /// <summary>
        /// Question being asked
        /// </summary>
        IQuestion Question { get; }

        string Prompt { get; }
        string Note { get; }

        /// <summary>
        /// Name of this Presented Question (dy default is the Name of the Question but can be overriden)
        /// </summary>
        string NameAs { get; }

        /// <summary>
        /// Sequence of the Question being asked
        /// </summary>
        int Sequence { get; }

        IAnswer GetAnswer();

        void AddToDefaultAnswerSource(IDefaultAnswerSource source, Guid containerId, IWorkingDocument workingDocument);

        bool WasHidden { get; set; }
    }

    public interface IPresentedSection : IPresented
    {
        List<IPresented> Children { get; }
        ISection Section { get; }

    }

    public enum HtmlPresentationHintAlignment
    {
        Default,
        Left,
        Right
    }

    public enum HtmlPresentationHintClear
    {
        Left,
        Right,
        Both,
        None
    }

    public enum HtmlSectionLayout
    {
        Flow,
        Table,
        TableRow,
        Tabbed,
        Modal,
        FullScreen
    }

    public enum HtmlPresentationHintLabelPlacement
    {
        Right,
        Left,
        Top,
        Bottom
    }

    public interface IHtmlPresentationHint
    {
        HtmlSectionLayout Layout { get; set; }
        HtmlPresentationHintAlignment Alignment { get; set; }
        HtmlPresentationHintClear Clear { get; set; }
        HtmlPresentationHintLabelPlacement LabelPlacement { get; set; }

        string CustomStyling { get; set; }
        string CustomClass { get; set; }
    }

    public interface ISection : IHasName, IHasIndex, IPresentable, IUniqueNode
    {
        string Prompt { get; set; }
        ILocalisableString LocalisablePrompt { get; }
        IHtmlPresentationHint HtmlPresentation { get; }
        List<IPresentable> Children { get; }

        bool AllowFilterByCategory { get; set; }

        string DataSource { get; set; }

        bool PresentAsFinal { get; }

        void CopyChildrenFrom(IPresentedSection sourceSection, bool copyAnswersToDefaults);

        string NgController { get; set; }
        bool EnableDeepEdit { get; set; }
    }

    public interface IFreeTextQuestion : IQuestion
    {
        bool AllowMultiLine { get; set; }
    }

    public interface INumberQuestion : IQuestion
    {
        int DecimalPlaces { get; set; }
        decimal? MaxValue { get; set; }
        decimal? MinValue { get; set; }
    }

    public enum CheckboxQuestionNoAnswerBehaviour
    {
        AnswerIsFalse,
        AnswerIsNull
    }

    public interface ICheckboxQuestion : IQuestion
    {
        CheckboxQuestionNoAnswerBehaviour NoAnswerBehaviour { get; set; }
    }

    public interface ILabelQuestion : IQuestion
    {
        string Text { get; set; }
        ILocalisableString LocalisableText { get; }

        bool PopulateReporting { get; set; }
    }

    public interface IMultiChoiceItem : IUniqueNode
    {
        string Answer { get; set; }
        string Display { get; set; }
        ILocalisableString LocalisableDisplay { get; }

        bool? PassFail { get; set; }
        decimal Score { get; set; }
    }

    public enum MultiChoiceQuestionFormat
    {
        /// <summary>
        /// For single select radio items are used, for multi select a list of checkboxes
        /// </summary>
        List,
        /// <summary>
        /// The list should be rendered horizontally across the page rather than vertically.
        /// </summary>
        List_Horizontal,
        /// <summary>
        /// For single select a drop combo and for multi select a multi select drop combo
        /// </summary>
        Compact
    }

    public interface IMultiChoiceQuestion : IQuestion, ITemplatedPresentable
    {
        IListOf<IMultiChoiceItem> List { get; }

        bool AllowMultiSelect { get; set; }

        MultiChoiceQuestionFormat Format { get; set; }

        /// <summary>
        /// The number of options that can be selected for the question. Should only be != 1 if AllowMultiSelet == true
        /// </summary>
        int NumberOfChoicesAllowed { get; set; }

        /// <summary>
        /// If true then all chosen answers must pass for the answer to pass, if false, one must pass
        /// </summary>
        int NumberOfAnswersMustPass { get; set; }

        /// <summary>
        /// Indicates that the set of choices should be presented to the user in a random order
        /// </summary>
        bool RandomizeChoices { get; set; }
    }

    public interface IDateTimeQuestion : IQuestion
    {
        bool AllowTimeEntry { get; set; }

        string UpdateDataSources { get; set; }
    }

    public interface IGeoLocationQuestion : IQuestion
    {
        bool LookUpAddress { get; set; }
    }

    public enum UploadMediaQuestionDestination
    {
        /// <summary>
        /// The media should be uploaded and not filed anywhere in particular
        /// </summary>
        NotFiled,
        /// <summary>
        /// The media should be filed against the reviewer
        /// </summary>
        Reviewer,
        /// <summary>
        /// The media should be filed against the reviewee
        /// </summary>
        Reviewee,
        /// <summary>
        /// The media should be filed against the reviewer and then sub filed against the reviewee
        /// </summary>
        ReviewerReviewee,
        /// <summary>
        /// The media should be filed against the reviewee and then sub filed against the reviewer
        /// </summary>
        RevieweeReviewer
    }
    public interface IUploadMediaQuestion : IQuestion
    {
        int MaxUploadCount { get; set; }
        int? MaxWidth { get; set; }
        int? MaxHeight { get; set; }
        bool KeepAspect { get; set; }
        bool AllowImage { get; set; }
        bool AllowVideo { get; set; }
        bool AllowAudio { get; set; }
        bool AllowAnyMediaType { get; set; }

        string UploadPrompt { get; set; }

        UploadMediaQuestionDestination Destination { get; set; }
    }

    public interface IPresentMediaQuestion : IQuestion
    {
        string Url { get; set; }
        PresentMediaQuestionType MediaType { get; set; }

        IDimension Width { get; set; }
        IDimension Height { get; set; }

        IPresentMediaQuestionSettings Settings { get; set; }
    }

    public enum PresentMediaQuestionType
    {
        /// <summary>
        /// Indicates that the media is to be hosted in a standard html5 video tag
        /// </summary>
        // Html5,
        /// <summary>
        /// Indicates that the media is to be hosted on YouTube
        /// </summary>
        YouTube,
        /// <summary>
        /// Indicates that the media is to be hosted on Vimeo
        /// </summary>
        // Vimeo,
        /// <summary>
        /// Indicates that there isn't any medai and that the player is just going to be used
        /// as a means of timing events
        /// </summary>
        // Timing
    }

    public interface IPresentedMediaQuestionTrackEvent : IClientModelEvent
    {
        decimal Start { get; set; }
        decimal End { get; set; }
    }

    public interface IPresentMediaQuestionSettings
    {

    }

    public interface IYouTubePresentMediaQuestionSettings : IPresentMediaQuestionSettings
    {
        /// <summary>
        /// Enables/Disables the full screen support in the YouTube Player
        /// </summary>
        bool AllowFullScreen { get; set; }
        /// <summary>
        /// If false sets the YouTube player up to use the modest branding GUI, which pretty much
        /// removes YouTube branding
        /// </summary>
        bool ModestBranding { get; set; }
        /// <summary>
        /// If false related videos at the end of the current video will not be shown
        /// </summary>
        bool ShowRelatedVideos { get; set; }
        /// <summary>
        /// If false the info bar at the top of the YouTube videos won't be shown
        /// </summary>
        bool ShowInfo { get; set; }

        YouTubePresentedMediaQuestionSettingsControls ControlsType { get; set; }
    }

    public enum YouTubePresentedMediaQuestionSettingsControls
    {
        /// <summary>
        /// No controls will be displayed
        /// </summary>
        NoControls = 0,
        /// <summary>
        /// Controls are displayed and the code for this is loaded when the control initiates
        /// </summary>
        ImmediateControls = 1,
        /// <summary>
        /// Controls are displayed, but are loaded once player code has loaded (quicker user experience, but there
        /// are issues with this that YouTube have yet to address)
        /// </summary>
        DelayedControls = 2
    }

    public interface IDimension
    {
        decimal Value { get; set; }
        DimensionType DimensionType { get; set; }
    }

    public enum DimensionType
    {
        Pixels,
        Percentage
    }

    public enum PayPalCurrency
    {
        AUD,
        BRL,
        CAD,
        CZK,
        DKK,
        EUR,
        HKD,
        HUF,
        ILS,
        JPY,
        MYR,
        MXN,
        NOK,
        NZD,
        PHP,
        PLN,
        GBP,
        SGD,
        SEK,
        CHF,
        TWD,
        USD
    }

    public interface IPresentWordDocumentQuestion : IQuestion
    {
        bool AllowDownload { get; set; }
        WordDocumentPreviewType PreviewType { get; set; }

        Guid MediaId { get; set; }
    }

    public enum WordDocumentPreviewType
    {
        None,
        Png
    }

    public interface IPayPalQuestion : IQuestion
    {
        string MerchantId { get; set; }
        decimal Price { get; set; }
        decimal? TaxRate { get; set; }
        PayPalCurrency Currency { get; set; }
        bool UseSandbox { get; set; }
        string DevelopmentUrl { get; set; }
    }

    public interface IPaymentGatewayQuestion
    {
        bool UseSandbox { get; set; }
    }

    public interface IeWayPaymentQuestion : IPaymentGatewayQuestion
    {
    }   

    public interface IProgressIndicatorQuestion : IQuestion
    {
        decimal MaxValue { get; set; }
        decimal MinValue { get; set; }

        decimal Value { get; set; }

        decimal ValueAsPercentage { get; }
    }

    public interface ISignatureQuestion : IQuestion
    {
        bool AllowTypedEntry { get; set; }
        IDimension Width { get; set; }
        IDimension Height { get; set; }

        bool TakePictureWhenSigning { get; set; }
        bool ShowTakenPicture { get; set; }
    }

    public interface IVariableDataQuestion : IQuestion
    {

    }
}
