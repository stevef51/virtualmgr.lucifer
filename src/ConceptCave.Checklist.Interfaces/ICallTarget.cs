﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Core;

namespace ConceptCave.Checklist.Interfaces
{
    public interface ICallTargetFactory
    {
        ICallTarget Create(IContextContainer context);
    }

    public interface ICallTarget
    {
        object Call(IContextContainer context, object[] args);
    }
}
