﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConceptCave.Checklist
{
    public interface IServerUrls
    {
        string ODataV3Url { get; }
        string ODataV4Url { get; }
        string JsReportServerUrl { get; }
    }
}
