﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;

namespace ConceptCave.Checklist.Interfaces
{
    public interface IBooleanTest
    {
        bool Test(object value, IContextContainer context);
    }
}
