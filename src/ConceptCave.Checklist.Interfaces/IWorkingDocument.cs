﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using ConceptCave.Core;

namespace ConceptCave.Checklist.Interfaces
{
    public enum ProcessDirection
    {
        Forward,
        Backward
    }


    public interface IWorkingDocument : ILingoDocument, IDisplayIndexProvider, IBlockable
    {
        /// <summary>
        /// Flat list of all Presentables (Pages, Sections, Questions) in order the were presented
        /// </summary>
        List<IPresented> Presented { get; }
        List<IAnswer> Answers { get; }
        List<IFacilityAction> Actions { get; }
        
        ILingoProgram Program { get; }
        IPresented GetPresented();

        IDesignDocument DesignDocument { get; }

        IDefaultAnswerSource DefaultAnswerSource { get; set; }

        RunMode Next();
        bool Previous();
        bool CanPrevious();
        RunMode RunMode { get; }

        IValidatorResult ProcessRules();
        IValidatorResult ProcessRules(ProcessDirection direction);

        IEnumerable<IPresented> PresentedAsDepthFirstEnumerable();
    }
}
