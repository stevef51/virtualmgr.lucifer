﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.Checklist.Interfaces
{
    public interface ITypeOverrideResolver
    {
        Type ResolveType(string name);
        string TypeName(Type type);
        string ResolveOverriddenToTarget(string name);
        string ResolveTargetToOverridden(string name);
    }
}
