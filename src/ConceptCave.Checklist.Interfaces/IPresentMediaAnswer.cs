﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConceptCave.Checklist.Interfaces
{
    public interface IPresentMediaAnswer
    {
        /// <summary>
        /// The position in the media to which the user had watched.
        /// </summary>
        decimal? Position { get; set; }
    }
}
