﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConceptCave.Checklist.Interfaces
{
    public interface IDesignDocument : ILingoDocument
    {
        IVariableBag Globals { get; }
        ILingoProgramDefinition ProgramDefinition { get; }
    }
}
