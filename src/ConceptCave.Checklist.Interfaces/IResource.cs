﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;

namespace ConceptCave.Core
{
    public interface IResource : IUniqueNode, IHasName
    {
    }

    public interface IFromNameValues
    {
        /// <summary>
        /// Allows a question to be configured through a set of dynamic objects representing Name Value pairs
        /// </summary>
        /// <param name="pairs"></param>
        void FromNameValuePairs(NameValueCollection pairs);

        List<NameValueCollection> ToNameValuePairs();
    }
}
