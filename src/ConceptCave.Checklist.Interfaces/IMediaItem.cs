﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Core;

namespace ConceptCave.Checklist.Interfaces
{
    public interface IMediaItem
    {
        Guid Id { get; }
        string Name { get; set; }
        string MediaType { get; set; }
        byte[] GetData(IContextContainer context);
    }

    public interface ISizeableMediaItem : IMediaItem
    {
        int? Width { get; set; }
        int? Height { get; set; }
    }
}
