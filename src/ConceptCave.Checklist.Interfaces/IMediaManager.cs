﻿using System;
using System.Collections.Generic;
using System.IO;

namespace ConceptCave.Checklist.Interfaces
{
    public interface IMediaManager
    {
        Stream GetStream(string filename, string mimeType);
        Stream GetStream(Guid id);
        Guid SaveStream(Stream stream, string filename, string mimeType, Guid? mediaId, IWorkingDocument document);

        Guid SaveStream(Stream stream, string filename, string mimeType, Guid? folderId, Guid? mediaId, IWorkingDocument document);

        void MapMediaToUser(Guid mediaId, Guid userId);
        void MapMediaToOrder(Guid mediaId, Guid orderId, string tag = "");
        void Delete(Guid mediaId);

        IMediaItem GetById(Guid id);
        IMediaItem GetByMediaFileId(Guid id);

        IMediaItem GetByName(string name, string type);

        string MediaName(Guid id);
        string MediaType(Guid id);

        IList<string> MediaLabels(Guid id);

        void SetMediaViewingAccepted(Guid workingDocumentId, bool accepted, string acceptionNotes);

        void CopyMediaViewing(Guid workingDocumentId, Guid mediaId, Guid[] userIds);
    }
}
