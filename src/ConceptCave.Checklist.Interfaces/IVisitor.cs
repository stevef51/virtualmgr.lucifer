﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConceptCave.Checklist.Interfaces
{
    public interface IVisitee
    {
        void AcceptVisitor(IVisitor visitor, Func<bool> recurse);
    }

    public interface IVisitor
    {
        void BeginVisit(IVisitee visitee);
        void EndVisit(IVisitee visitee);
    }

    public class Visitor<T> : IVisitor where T : IVisitee
    {
        public Action<T> BeginVisit;
        public Action<T> EndVisit;

        #region IVisitor Members

        void IVisitor.BeginVisit(IVisitee visitee)
        {
            if (BeginVisit != null)
                BeginVisit((T)visitee);
        }

        void IVisitor.EndVisit(IVisitee visitee)
        {
            if (EndVisit != null)
                EndVisit((T)visitee);
        }
        #endregion
    }
}
