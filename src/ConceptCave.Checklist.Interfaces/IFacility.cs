﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Core;
using System.Net.Mail;

namespace ConceptCave.Checklist.Interfaces
{
    public interface IFacility : IHasName, IHasIndex, IUniqueNode
    {
    }

    public interface IFacilityAction : IUniqueNode
    {
//        IFacility Facility { get; }
    }

    public interface IServerBasedAction
    {
        void ServerExecute(IContextContainer context);
    }

    public interface IEmailConfiguration
    {
        bool Enabled { get; }
        string FilterAddress(string address);
        string SubjectAppend { get; }
        MailAddress From { get; }
    }

    public interface ISendGridConfiguration
    {
        void AddXsmtpApiHeader(MailMessage message);
    }
}
