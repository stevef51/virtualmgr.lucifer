﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.Checklist.Interfaces
{
    public interface ILingoDatastoreTransaction : IDisposable
    {
        IQueryable Queryable { get; }
        IQueryable ExecuteQuery(Expression queryExpr, IQueryable queryable);
    }
}
