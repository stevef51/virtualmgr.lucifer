﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConceptCave.Checklist.Interfaces
{
    public interface ISignatureAnswer : IAnswer
    {
        string AnswerAsJsonString { get; }
        IMediaItem Picture { get; }
    }
}
