﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConceptCave.Checklist.Interfaces
{
    public interface IVariableBag : IEnumerable<IVariable>
    {
        IVariableBag Parent { get; set; }
        IVariable<T> Variable<T>(string name, T defaultCreateValue);
        IVariable<T> Variable<T>(string name);
        void Clear();
    }
}
