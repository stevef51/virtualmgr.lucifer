﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConceptCave.Checklist.Interfaces
{
    public interface ILatLng
    {
        decimal Latitude { get; }
        decimal Longitude { get; }
        decimal Accuracy { get; }
        string Error { get; }

        decimal MetresDistanceFrom(ILatLng latLng);
        decimal MetresDistanceFrom(decimal lat, decimal lng);
    }

    public interface IGeoLocationAnswer
    {
        ILatLng Coordinates { get; }
    }
}
