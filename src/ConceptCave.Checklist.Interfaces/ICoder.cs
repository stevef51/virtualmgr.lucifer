﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConceptCave.Core.Coding
{
    public interface ICoder
    {
        IContextContainer Context { get; }
    }
}
