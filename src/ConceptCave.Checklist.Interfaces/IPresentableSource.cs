﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;

namespace ConceptCave.Checklist.Interfaces
{
    public interface IPresentableSource
    {
        IPresentable GetPresentable(ILingoProgram program, ref string nameAs);
    }
}
