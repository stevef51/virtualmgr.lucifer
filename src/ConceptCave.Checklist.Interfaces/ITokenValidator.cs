﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.Checklist.Interfaces
{
    public interface ITokenValidator
    {
        bool ValidateToken(string token, out string username);
    }
}
