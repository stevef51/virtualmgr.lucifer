﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConceptCave.Core
{
    public interface IContextContainer
    {
        bool Has<T>();
        bool Has<T>(string name);

        T Get<T>();
        T Get<T>(Func<T> defaultValueCallback);
        T Get<T>(string name);
        T Get<T>(string name, Func<T> defaultValueCallback);

        void Set<T>(string name, T context);
        void Set<T>(T context);
        void Set(Type T, string name, object context);

        void CopyTo(IContextContainer target);
    }

    public class RequiresContextContainerArgs
    {
        public IContextContainer Context { get; set; }
    }

    public delegate void RequiresContextContainerHandler(object source, RequiresContextContainerArgs args);

    public interface IRequiresContextContainer
    {
        event RequiresContextContainerHandler ContextRequired;
    }
}
