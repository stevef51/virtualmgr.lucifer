﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.Checklist.Interfaces
{
    public interface ILanguageTranslateService
    {
        Task<string> TranslateAsync(string fromCultureName, string fromText, string toCultureName);
        Task<string[]> TranslateAsync(string fromCultureName, string[] fromText, string toCultureName);
    }
}
