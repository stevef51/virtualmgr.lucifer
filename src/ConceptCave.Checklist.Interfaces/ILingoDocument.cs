﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Core;

namespace ConceptCave.Checklist.Interfaces
{

    public enum FinishMode
    {
        /// <summary>
        /// The user should need to hit a finish button or some other indicator that they wish to finish the checklist.
        /// </summary>
        Manual,
        /// <summary>
        /// The checklist should move to the finish state of its own accord.
        /// </summary>
        Auto
    }

    public interface ILingoDocument : IDocument
    {
        INamedList<IQuestion> Questions { get; }
        INamedList<ISection> Sections { get; }
        IVariableBag Variables { get; }
        INamedList<IFacility> Facilities { get; }
        IEnumerable<IPresentable> AsDepthFirstEnumerable();
        FinishMode FinishMode { get; set; }
    }
}
