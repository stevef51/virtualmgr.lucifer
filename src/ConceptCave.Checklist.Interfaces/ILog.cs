//
//  Author:
//    Steve Fillingham steve.fillingham@bworkflow.com
//
//  Copyright (c) 2013, SquareIT Software PTY LTD
//
//
//
using System;

namespace ConceptCave.Core
{
    public enum LogLevel
    {
        None,
        Error,
        Warning,
        Info,
        Debug,
    }

    public interface ILog
    {
        ILogFilter Filter { get; set; }
        ILogger GetLogger(Type type);
        ILogger GetLogger(object instance);
		string LogFile {get;}
		void DisablePersistantLogging();
		void EnablePersistantLogging();
    }

    public interface ILogFilter
    {
        bool IsFiltered(Type type);
    }

    public interface ILogger
    {
		bool IsLogDebug { get; }
		bool IsLogInfo { get; }
		bool IsLogWarning { get; }
		bool IsLogError { get; }
        ILog Log { get; }
        LogLevel LogLevel { get; }
        void LogDebug(string format, params object[] args);
        void LogInfo(string format, params object[] args);
        void LogWarning(string format, params object[] args);
        void LogError(string format, params object[] args);
        void LogError(Exception ex, string format, params object[] args);
        void LogError(Exception ex);
    }
}

