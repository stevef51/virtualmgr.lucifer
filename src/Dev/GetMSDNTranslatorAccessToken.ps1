[System.Reflection.Assembly]::LoadWithPartialName("System.Web")

#region Construct Azure Datamarket access_token 
#Get ClientId and Client_Secret from https://datamarket.azure.com/developer/applications/
#Refer obtaining AccessToken (http://msdn.microsoft.com/en-us/library/hh454950.aspx) 

$ClientID = 'virtual-manager'
$client_Secret = '//NpaKLB8UR3l84pObfd4DhA9IAlQSpyGVAWTMhU41o='

# If ClientId or Client_Secret has special characters, UrlEncode before sending request
$clientIDEncoded = [System.Web.HttpUtility]::UrlEncode($ClientID)
$client_SecretEncoded = [System.Web.HttpUtility]::UrlEncode($client_Secret)

#Define uri for Azure Data Market
$Uri = "https://datamarket.accesscontrol.windows.net/v2/OAuth2-13"

#Define the body of the request
$Body = "grant_type=client_credentials&client_id=$clientIDEncoded&client_secret=$client_SecretEncoded&scope=http://api.microsofttranslator.com"

#Define the content type for the request
$ContentType = "application/x-www-form-urlencoded"

#Invoke REST method.  This handles the deserialization of the JSON result.  Less effort than invoke-webrequest
$admAuth=Invoke-RestMethod -Uri $Uri -Body $Body -ContentType $ContentType -Method Post

#Construct the header value with the access_token just recieved
$HeaderValue = "Bearer " + $admauth.access_token
#endregion

[System.Console]::WriteLine($admauth.access_token);

#region Construct and invoke REST request to Microsoft Translator Service
[string] $text = "Use pixels to express measurements for padding and margins.";
[string] $textEncoded = [System.Web.HttpUtility]::UrlEncode($text)
[string] $from = "en";
[string] $to = "de";
[string] $uri = "http://api.microsofttranslator.com/v2/Http.svc/Translate?text=" + $text + "&from=" + $from + "&to=" + $to;

$result = Invoke-RestMethod -Uri $uri -Headers @{Authorization = $HeaderValue} 
#endregion

$result.string.'#text'