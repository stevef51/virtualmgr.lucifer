﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Checklist.Lingo;
using Irony.Parsing;

namespace ConceptCave.LanguageDataExport
{
    class Program
    {
        static void Main(string[] args)
        {
            //Simple console application for exporting the lingo language data into a file
            //rerun when the language data is changed

            var fname = args[0];
            using (var writer = new BinaryWriter(new FileStream(fname, FileMode.OpenOrCreate)))
            {
                var ld = new LanguageData(LingoGrammar.Instance);
                ld.ParserData.Serialise(writer);
            }
        }
    }
}
