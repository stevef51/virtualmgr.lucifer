﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.SimpleExtensions
{
    public static class DictionaryHelper
    {
        /// <summary>
        /// Simple String template expansion.  Uses @key to replace in the template.  Uses string.Format so format specifiers are allowed
        /// eg "Hello {@name} your age in hex is {@age:x2}"
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="self"></param>
        /// <param name="template"></param>
        /// <returns></returns>
        public static string SimpleExpand<T>(this Dictionary<string, T> self, string template)
        {
            int s = template.IndexOf("{@");
            while (s != -1)
            {
                int e = template.IndexOf("}", s);
                if (e != -1)
                {
                    string key, format = "";
                    key = template.Substring(s + 2, e - s - 2);
                    int c = key.IndexOf(":");
                    if (c > -1)
                    {
                        format = key.Substring(c);
                        key = key.Substring(0, c);
                    }

                    string replace = "";
                    T v = default(T);
                    if (self.TryGetValue(key, out v))
                        replace = string.Format("{0" + format + "}", v);

                    template = template.Substring(0, s) + replace + template.Substring(e + 1);
                }
                else
                    break;
                s = template.IndexOf("{@");
            }
            return template;
        }


        public static T GetOrDefault<T>(this Dictionary<string, T> self, string key, Func<T> defaultFnT)
        {
            T t = default(T);
            if (self.TryGetValue(key, out t))
                return t;

            return defaultFnT();
        }

        public static T GetOrDefault<T>(this Dictionary<string, T> self, string key)
        {
            T t = default(T);
            self.TryGetValue(key, out t);
            return t;
        }
    }
}
