﻿using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Newtonsoft.Json.Converters
{
    public static class JsonExtensions
    {
        public static T JsonDeserialize<T>(this string self)
        {
            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(self);
        }

        public static object JsonDeserialize(this string self, Type type)
        {
            return Newtonsoft.Json.JsonConvert.DeserializeObject(self, type);
        }

        public static object JsonDeserialize(this TextReader self, Type type)
        {
            JsonSerializer s = new JsonSerializer();
            using (var jtr = new JsonTextReader(self) { CloseInput = false })
                return s.Deserialize(jtr, type);
        }

        public static T JsonDeserialize<T>(this TextReader self)
        {
            JsonSerializer s = new JsonSerializer();
            using (var jtr = new JsonTextReader(self) { CloseInput = false })
                return s.Deserialize<T>(jtr);
        }

        public static object JsonDeserialize(this Stream self, Type type)
        {
            using (var sr = new StreamReader(self))
            {
                return sr.JsonDeserialize(type);
            }
        }

        public static T JsonDeserialize<T>(this Stream self)
        {
            using (var sr = new StreamReader(self))
            {
                return sr.JsonDeserialize<T>();
            }
        }

        public static string JsonSerialize<T>(this List<T> self)
        {
            return self.JsonSerialize<T>(Formatting.None);
        }

        public static string JsonSerialize<T>(this List<T> self, Formatting formatting)
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(self, formatting);
        }

        public static string JsonSerialize<T>(this T self)
        {
            return self.JsonSerialize<T>(Formatting.None);
        }

        public static string JsonSerialize<T>(this T self, Formatting formatting)
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(self, formatting);
        }

        public static void JsonSerialize<T>(this T self, TextWriter writer)
        {
            JsonSerialize<T>(self, writer, Formatting.None);
        }

        public static void JsonSerialize<T>(this T self, TextWriter writer, Formatting formatting)
        {
            JsonSerializer s = new JsonSerializer();
            s.Formatting = formatting;
            var jtw = new JsonTextWriter(writer);
            s.Serialize(jtw, self);
        }

        public static void JsonSerialize<T>(this T self, Stream stream, Formatting formatting)
        {
            var sw = new StreamWriter(stream);
            self.JsonSerialize(sw, formatting);
            sw.Flush();
        }

    }
}

