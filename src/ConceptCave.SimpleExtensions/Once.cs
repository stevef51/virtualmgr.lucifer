//
//  Author:
//    Steve Fillingham steve.fillingham@bworkflow.com
//
//  Copyright (c) 2013, SquareIT Software PTY LTD
//
//
//
using System;

namespace ConceptCave.SimpleExtensions
{
	public class Once<T>
	{
		private Func<T> _fnT;
		private T _value;
		private bool _done;
		private object _lock = new object();

		public T Value 
		{
			get
			{
                if (!_done)
                {
                    lock (_lock)
                    {
                        if (!_done)
                        {
                            _value = _fnT();
                            _done = true;
                        }
                    }
                }

				return _value;
			}
		}

		public Once()
		{
		}

		public Once(Func<T> fnT)
		{
			_fnT = fnT;
		}

		public void Reset()
		{
			_done = false;
		}

		public Func<T> Fn
		{
			get { return _fnT; }
			set
			{
				_fnT = value;
				_done = false;
			}
		}

		public static implicit operator T (Once<T> once) 
		{
			return once.Value;
		}
	}
}

