﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace ConceptCave.SimpleExtensions
{
    public static class ArrayExtensions
    {
        #region Hexify/Unhexify
        private static char[] _hex16 = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };

        public static string Hexify(this byte[] self)
        {
            int len = self.Length;
            char[] str = new char[len * 2];
            for (int i = 0; i < len; i++)
            {
                str[i << 1] = _hex16[self[i] >> 4];
                str[(i << 1) + 1] = _hex16[self[i] & 0x0F];
            }
            return new string(str, 0, len * 2);
        }

        public static byte[] Unhexify(this string hexString)
        {
            int byteLength = hexString.Length >> 1;
            if (byteLength << 1 != hexString.Length)
                throw new InvalidOperationException("hexString must be even length (bytes)");

            byte[] result = new byte[byteLength];
            for (int i = 0, j = 0; i < byteLength; i++, j += 2)
            {
                int u = (int)hexString[j + 0];
                if (u < 0x30)
                    throw new InvalidOperationException("hexString contains none Hex characters");
                else if (u > 0x39)
                {
                    u &= ~0x20;         // Uppercase
                    if (u < 0x41 || u > 0x46)
                        throw new InvalidOperationException("hexString contains none Hex characters");
                    u = u - 0x41 + 0xA;
                }
                else
                    u -= 0x30;

                int l = (int)hexString[j + 1];
                if (l < 0x30)
                    throw new InvalidOperationException("hexString contains none Hex characters");
                else if (l > 0x39)
                {
                    l &= ~0x20;         // Uppercase
                    if (l < 0x41 || u > 0x46)
                        throw new InvalidOperationException("hexString contains none Hex characters");
                    l = l - 0x41 + 0xA;
                }
                else
                    l -= 0x30;
                result[i] = (byte)(u << 4 | l);
            }

            return result;
        }
        #endregion

        public static bool EqualArray(this byte[] self, byte[] compare)
        {
            var length = self.Length;
            if (length != compare.Length)
                return false;
            for (var i = 0; i < length; i++)
                if (self[i] != compare[i])
                    return false;
            return true;
        }

        public static byte[] SHA1Hash(this byte[] bytes)
        {
            using (var sha1 = SHA1.Create())
                return sha1.ComputeHash(bytes);
        }
    }
}
