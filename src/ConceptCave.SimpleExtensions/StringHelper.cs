﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

namespace ConceptCave.SimpleExtensions
{
    public static class StringHelper
    {
		/// <summary>
		/// Stringify an enumerable list with a specific seperator between each element.  Handy to make ABC -> A,B,C
		/// </summary>
		/// <param name="self">The IEnumerable</param>
		/// <param name="fn">Stringify an item</param>
		/// <param name="seperator">The Seperator</param>
		/// <typeparam name="T">Type of item</typeparam>
        public static string Seperate<T>(this IEnumerable<T> self, Func<T, string> fn, string seperator = ",")
        {
            string s = "";
            IEnumerator<T> ie = self.GetEnumerator();
            while (ie.MoveNext())       
            {
                if (s.Length > 0)
                    s += seperator;
                s += fn(ie.Current);
            }
            return s;
        }

        /// <summary>
        /// Stringify an enumerable list with a specific seperator between each element.  Handy to make ABC -> A,B,C
        /// </summary>
        /// <param name="self">The IEnumerable</param>
        /// <param name="fn">Stringify an item</param>
        /// <param name="seperator">The Seperator</param>
        /// <typeparam name="T">Type of item</typeparam>
        public static string Seperate<T>(this IEnumerable<T> self, Func<T, string> fn, Func<T, string> fnSeperator)
        {
            string s = "";
            IEnumerator<T> ie = self.GetEnumerator();
            while (ie.MoveNext())
            {
                if (s.Length > 0)
                    s += fnSeperator(ie.Current);
                s += fn(ie.Current);
            }
            return s;
        }

        /// <summary>
        /// Substring on steroids
        /// </summary>
        /// <param name="self"></param>
        /// <param name="startIndex">0 or +ve = start from left, -ve = start from right</param>
        /// <param name="count">+ve = count from start, -ve = count from end</param>
        /// <returns></returns>
        public static string SteroidSubstring(this string self, int start, int count)
        {
            int s, c;
            if (start >= 0)
            {
                if (count < 0)
                {
                    // 0123456789
                    //  xxxxxxxxx   s = 1, c = -1     
                    //  xxxxxxxx    s = 1, c = -2
                    //  xxxxxxx     s = 1, c = -3     
                    s = start;
                    c = self.Length + (count+1) - start;
                }
                else
                {
                    // 0123456789
                    //              s = 1, c = 0
                    //  x           s = 1, c = 1
                    //  xx          s = 1, c = 2
                    //  xxx         s = 1, c = 3
                    s = start;
                    c = count;
                }
            }
            else
            {
                if (count >= 0)
                {
                    // 0123456789
                    //              s = -2, c = 0
                    //         x    s = -2, c = 1
                    //        xx    s = -2, c = 2
                    //       xxx    s = -2, c = 3
                    s = self.Length + (start+1) - count;
                    c = count;
                }
                else
                {
                    // 0123456789
                    // xxxxxxxxx    s = -2, c = -1
                    //  xxxxxxxx    s = -2, c = -2
                    //   xxxxxxx    s = -2, c = -3
                    s = -(count+1);
                    c = (self.Length + (start+1)) - s;
                }
            }

            return self.Substring(s, c);
        }

        public static string Truncate(this string self, int maxLength)
        {
            if (string.IsNullOrEmpty(self))
                return self;
            if (self.Length <= maxLength)
                return self;
            return self.Substring(0, maxLength);
        }

		/*
		public static DateTime ParseRoundTripDateTime(this string self)
		{
            return DateTime.Parse(self).ToUniversalTime();
		}

		public static string FormatRoundTripString(this DateTime self)
		{
			if (self.Kind != DateTimeKind.Utc)
				throw new ArgumentException ("DateTime must be UTC");

			return self.ToString ("o");
		}
		*/

		public static string ReflectedToString(this object self)
		{
			StringBuilder sb = new StringBuilder ();
			Type t = self.GetType ();
			foreach (PropertyInfo pi in t.GetProperties())
			{
				sb.AppendFormat ("{0} = {1},", pi.Name, pi.GetValue (self));
			}
			return sb.ToString ();
		}

        public static string Argumentize(this string self, string start, string end, Func<int, string, string> replaceFn)
        {
            int s = 0;
            int index = 0;
            while (true)
            {
                s = self.IndexOf(start, s);
                if (s == -1)
                    break;
                int e = self.IndexOf(end, s + start.Length);
                if (e == -1)
                    break;
                var tokenLen = e - s - start.Length;
                var r = replaceFn(index++, self.Substring(s + start.Length, tokenLen));
                self = self.Substring(0, s) + r + self.Substring(e + end.Length);
                s += r.Length;
            }
            return self;
        }

    }
}
