//
//  Author:
//    Steve Fillingham steve.fillingham@bworkflow.com
//
//  Copyright (c) 2013, SquareIT Software PTY LTD
//
//
//
using System;
using System.Collections.Generic;

namespace ConceptCave.SimpleExtensions
{
	public static class Params
	{
		public static IEnumerable<TCast> AsEnumerable<TItem,TCast>(params TItem[] values) where TItem : TCast
		{
			foreach (TItem t in values)
				yield return (TCast)t;
		}

		public static IEnumerable<TItem> AsEnumerable<TItem>(params TItem[] values)
		{
			foreach (TItem t in values)
				yield return t;
		}
	}
}

