﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Newtonsoft.Json.Converters
{
    /// <summary>
    /// Forces a DateTime to be rendered in UTC timezone
    /// </summary>
    public class JsonUTCDateTimeConverter : IsoDateTimeConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            DateTime? dt = value as DateTime?;
            if (dt.HasValue)
            {
                dt = DateTime.SpecifyKind(dt.Value, DateTimeKind.Utc);
                base.WriteJson(writer, dt.Value, serializer);
            }
            else
            {
                base.WriteJson(writer, value, serializer);
            }
        }
    }
}
