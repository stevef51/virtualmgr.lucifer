﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.SimpleExtensions
{
    public class LambdaComparer<T> : IComparer<T>, IEqualityComparer<T>
    {
        private Func<T, int> _fnGetHashCode;
        private Func<T, T, int> _fnCompare;

        public LambdaComparer(Func<T, int> fnGetHashCode, Func<T, T, int> fnCompare)
        {
            _fnGetHashCode = fnGetHashCode;
            _fnCompare = fnCompare;
        }

        public int Compare(T x, T y)
        {
            return _fnCompare(x, y);
        }

        public bool Equals(T x, T y)
        {
            return _fnCompare(x, y) == 0;
        }

        public int GetHashCode(T obj)
        {
            return _fnGetHashCode(obj);
        }
    }
}
