﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.SimpleExtensions
{
    public class ValueRef<T>
    {
        public T Value { get; set; }
        public ValueRef()
        {
        }

        public ValueRef(T initial)
        {
            Value = initial;
        }
    }
}
