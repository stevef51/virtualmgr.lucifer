﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Threading.Tasks
{
    public static class TaskExtensions
    {
        public static void RunAndForget(this Task self)
        {
            // Don't do anything, the Task is already started - this method simply gets rid of the Compiler warning
        }
    }
}
