﻿CREATE TABLE [stock].[tblProductCatalog]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [FacilityStructureId] INT NOT NULL, 
    [Name] NVARCHAR(50) NOT NULL, 
    [StockCountWhenOrdering] BIT NOT NULL DEFAULT 0, 
    [ShowPricingWhenOrdering] BIT NOT NULL DEFAULT 0, 
    [Archived] BIT NOT NULL DEFAULT 0, 
    [ShowLineItemNotesWhenOrdering] BIT NOT NULL DEFAULT 0, 
    [Description] NVARCHAR(MAX) NULL, 
    CONSTRAINT [FK_tblProductCatalog_tblFacilityStructure] FOREIGN KEY ([FacilityStructureId]) REFERENCES [asset].[tblFacilityStructure]([Id]) ON DELETE CASCADE
)
