﻿CREATE TABLE [stock].[tblJSFunction]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY,
	[Archived] BIT NOT NULL DEFAULT(0),
	[Type] INT NOT NULL,
	[Name] NVARCHAR(255) NOT NULL,
	[Script] NVARCHAR(MAX) NOT NULL
)

GO

CREATE INDEX [IX_tblJSFunction_Type] ON [stock].[tblJSFunction] ([Type])
GO

CREATE INDEX [IX_tblJSFunction_Archived] ON [stock].[tblJSFunction] ([Archived])
GO

CREATE UNIQUE INDEX [IX_tblJSFunction_TypeName] ON [stock].[tblJSFunction] ([Type], [Name])
