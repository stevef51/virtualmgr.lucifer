﻿CREATE TABLE [stock].[tblProductHierarchy]
(
	[ParentProductId] INT NOT NULL , 
    [ChildProductId] INT NOT NULL, 
    [Name] NVARCHAR(MAX) NULL, 
    PRIMARY KEY ([ChildProductId], [ParentProductId]), 
    CONSTRAINT [FK_tblProductHierarchy_ToParentProduct] FOREIGN KEY ([ParentProductId]) REFERENCES [stock].[tblProduct]([Id]),
	CONSTRAINT [FK_tblProductHierarchy_ToChildProduct] FOREIGN KEY ([ChildProductId]) REFERENCES [stock].[tblProduct]([Id])
)
