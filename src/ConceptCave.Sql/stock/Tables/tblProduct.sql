﻿CREATE TABLE [stock].[tblProduct]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Name] NVARCHAR(100) NOT NULL, 
    [Description] NVARCHAR(MAX) NULL, 
    [Code] NVARCHAR(50) NULL, 
    [MediaId] UNIQUEIDENTIFIER NULL, 
    [Price] DECIMAL(18, 2) NULL, 
    [ProductTypeId] INT NOT NULL DEFAULT 1, 
    [PriceJsFunctionId] INT NULL, 
    [IsCoupon] BIT NOT NULL DEFAULT 0, 
    [Extra] NVARCHAR(512) NULL, 
    [ValidFrom] DATETIME NULL, 
    [ValidTo] DATETIME NULL, 
    CONSTRAINT [FK_tblProduct_TotblMedia] FOREIGN KEY ([MediaId]) REFERENCES [tblMedia]([Id]), 
    CONSTRAINT [FK_tblProduct_TotblProductType] FOREIGN KEY ([ProductTypeId]) REFERENCES [stock].[tblProductType]([Id]),
    CONSTRAINT [FK_tblProduct_tblJsFunction] FOREIGN KEY (PriceJsFunctionId) REFERENCES [stock].tblJsFunction([Id])
)

GO

CREATE UNIQUE INDEX [IX_tblProduct_Code] ON [stock].[tblProduct] ([Code], [IsCoupon]) WHERE Code IS NOT NULL
