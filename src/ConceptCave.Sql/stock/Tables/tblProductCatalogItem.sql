﻿CREATE TABLE [stock].[tblProductCatalogItem]
(
	[ProductId] INT NOT NULL,
	[ProductCatalogId] INT NOT NULL,
	[MinStockLevel]  DECIMAL(18, 10)
    CONSTRAINT [PK_tblProductCatalogItem] PRIMARY KEY ([ProductCatalogId], [ProductId]), 
    [Price] DECIMAL(18, 2) NULL, 
    [PriceJsFunctionId] INT NULL, 
    [SortOrder] INT NOT NULL DEFAULT (0), 
    CONSTRAINT [FK_tblProductCatalogItem_tblProduct] FOREIGN KEY ([ProductId]) REFERENCES [stock].[tblProduct]([Id]),
    CONSTRAINT [FK_tblProductCatalogItem_tblProductCatalog] FOREIGN KEY ([ProductCatalogId]) REFERENCES [stock].[tblProductCatalog]([Id]),
    CONSTRAINT [FK_tblProductCatalogItem_tblJsFunction] FOREIGN KEY (PriceJsFunctionId) REFERENCES [stock].tblJsFunction([Id])
)
