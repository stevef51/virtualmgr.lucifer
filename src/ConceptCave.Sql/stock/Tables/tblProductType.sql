﻿CREATE TABLE [stock].[tblProductType]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [Name] NVARCHAR(50) NOT NULL, 
    [Units] INT NOT NULL DEFAULT 1
)
