﻿CREATE TABLE [stock].[tblOrderItem]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY,
	[OrderId] UNIQUEIDENTIFIER NOT NULL,
	[ProductId] INT NOT NULL,
	[Quantity] DECIMAL(18, 10) NOT NULL, 
    [Stock] DECIMAL(18, 10) NULL, 
    [MinStockLevel] DECIMAL(18, 10) NULL, 
    [Price] DECIMAL(18, 2) NULL, 
    [TotalPrice] DECIMAL(18, 2) NULL, 
    [Notes] NVARCHAR(4000) NULL, 
    [StartDate] DATETIME NULL, 
    [EndDate] DATETIME NULL, 
    CONSTRAINT [FK_tblOrderItem_tblOrder] FOREIGN KEY ([OrderId]) REFERENCES [stock].[tblOrder]([Id]) ON DELETE CASCADE, 
    CONSTRAINT [FK_tblOrderItem_tblProduct] FOREIGN KEY ([ProductId]) REFERENCES [stock].[tblProduct]([Id])
)

GO

CREATE INDEX [IX_tblOrderItem_OrderId] ON [stock].[tblOrderItem] ([OrderId])

GO

CREATE INDEX [IX_tblOrderItem_StartDate] ON [stock].[tblOrderItem] ([StartDate])

GO

CREATE INDEX [IX_tblOrderItem_EndDate] ON [stock].[tblOrderItem] ([EndDate])

GO

CREATE INDEX [IX_tblOrderItem_StartDateEndDate] ON [stock].[tblOrderItem] ([StartDate], [EndDate])
