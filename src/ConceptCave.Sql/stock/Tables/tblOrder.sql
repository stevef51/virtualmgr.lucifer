﻿CREATE TABLE [stock].[tblOrder]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY,
	[ProductCatalogId] INT NOT NULL,
	[OrderedById] UNIQUEIDENTIFIER NOT NULL,
	[FacilityStructureId] INT NOT NULL,
	[DateCreated] DATETIME NOT NULL DEFAULT getutcdate(),
	[ExpectedDeliveryDate] DATETIME, 
    [ProjectJobTaskId] UNIQUEIDENTIFIER NULL, 
    [TotalPrice] DECIMAL(18, 2) NULL,  
    [PendingExpiryDate] DATETIME NULL, 
    [MaxCoupons] INT NOT NULL DEFAULT 1, 
    [ExtensionOfOrderId] UNIQUEIDENTIFIER NULL, 
    [CancelledDateUtc] DATETIME NULL , 
    [Archived] BIT NOT NULL DEFAULT 0, 
    [WorkflowState] NVARCHAR(20) NULL , 
    CONSTRAINT [FK_tblOrder_tblProductCategory] FOREIGN KEY ([ProductCatalogId]) REFERENCES [stock].[tblProductCatalog]([Id]),
    CONSTRAINT [FK_tblOrder_tblUserData] FOREIGN KEY ([OrderedById]) REFERENCES [tblUserData]([UserId]),
    CONSTRAINT [FK_tblOrder_tblFacilityStructure] FOREIGN KEY ([FacilityStructureId]) REFERENCES [asset].[tblFacilityStructure]([Id]), 
    CONSTRAINT [FK_tblOrder_tblProjectJobTask] FOREIGN KEY ([ProjectJobTaskId]) REFERENCES [gce].[tblProjectJobTask]([Id])
)

GO

CREATE INDEX [IX_tblOrder_ProjectJobTaskId] ON [stock].[tblOrder] ([ProjectJobTaskId])

GO

CREATE INDEX [IX_tblOrder_PendingExpiryDate] ON [stock].[tblOrder] ([PendingExpiryDate])

GO

CREATE INDEX [IX_tblOrder_Cancelled] ON [stock].[tblOrder] ([CancelledDateUtc])
GO

CREATE INDEX [IX_tblOrder_ExtensionOfOrderId] ON [stock].[tblOrder] ([ExtensionOfOrderId])
GO

CREATE INDEX [IX_tblOrder_WorkflowState] ON [stock].[tblOrder] ([WorkflowState])
