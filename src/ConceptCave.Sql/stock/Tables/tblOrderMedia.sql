﻿CREATE TABLE [stock].[tblOrderMedia]
(
	[Id] INT NOT NULL IDENTITY PRIMARY KEY,
	[OrderId] UNIQUEIDENTIFIER NOT NULL,
	[MediaId] UNIQUEIDENTIFIER NOT NULL,
	[DateCreated] DATETIME NOT NULL DEFAULT (getutcdate()), 
	[Tag] NVARCHAR(256) NOT NULL DEFAULT '', 
    CONSTRAINT [FK_tblOrderMedia_tblMedia] FOREIGN KEY ([MediaId]) REFERENCES [dbo].[tblMedia] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_tblOrderMedia_tblUserData] FOREIGN KEY ([OrderId]) REFERENCES [stock].[tblOrder] ([Id]) ON DELETE CASCADE
)

GO

CREATE UNIQUE INDEX [IX_tblOrderMedia_OrderIdMediaId] ON [stock].[tblOrderMedia] ([OrderId], [MediaId], [Tag])
