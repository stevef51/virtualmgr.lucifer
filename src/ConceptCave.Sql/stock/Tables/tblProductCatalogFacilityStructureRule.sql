﻿CREATE TABLE [stock].[tblProductCatalogFacilityStructureRule]
(
	[ProductCatalogId] INT NOT NULL , 
    [FacilityStructureId] INT NOT NULL, 

    [Exclude] BIT NOT NULL, 
    PRIMARY KEY ([ProductCatalogId], [FacilityStructureId]), 
    CONSTRAINT [FK_tblProductCatalogFacilityExclusion_tblFacilityStructure] FOREIGN KEY ([FacilityStructureId]) REFERENCES [asset].[tblFacilityStructure]([Id]), 
    CONSTRAINT [FK_tblProductCatalogFacilityExclusion_tblProductCatalog] FOREIGN KEY ([ProductCatalogId]) REFERENCES [stock].[tblProductCatalog]([Id])
)
