﻿CREATE PROCEDURE [es].[WriteSensorReading]
	@sensorId INT,
	@sensorTimestampUtc DATETIME,
	@value DECIMAL(18, 10),
	@tabletUUID NVARCHAR(36)
AS
	-- Make sure the TabletUUID exists, if not create it
	IF NOT EXISTS (SELECT UUID FROM asset.tblTabletUUID WHERE UUID = @tabletUUID)
	BEGIN
		INSERT INTO asset.tblTabletUUID (UUID) VALUES (@tabletUUID)
	END

	INSERT INTO [es].[tblSensorReadings] (
		SensorId, 
		SensorTimestampUtc, 
		ServerTimestampUtc,
		[Value],
		TabletUUID
	) VALUES (
		@sensorId,
		@sensorTimestampUtc,
		GETUTCDATE(),
		@value,
		@tabletUUID
	)

RETURN 0
