﻿CREATE TYPE [es].[SensorReadingRecord] AS TABLE
(
	SensorId INT,
	SensorTimestampUtc DATETIME,
	[Value] DECIMAL(18,10),
	TabletUUID NVARCHAR(50) NULL
);
GO

CREATE PROCEDURE [es].[WriteSensorReadingRecords]
	@table [es].[SensorReadingRecord] READONLY
AS
	DECLARE records CURSOR FOR SELECT * FROM @table
	
	DECLARE 
		@sensorId INT,
		@sensorTimestampUtc DATETIME,
		@value DECIMAL(18,10),
		@tabletUUID NVARCHAR(36)

	OPEN records

	FETCH NEXT FROM records INTO
		@sensorId,
		@sensorTimestampUtc,
		@value,
		@tabletUUID

	WHILE @@FETCH_STATUS = 0
	BEGIN
		EXEC es.WriteSensorReading @sensorId, @sensorTimestampUtc, @value, @tabletUUID

		FETCH NEXT FROM records INTO
			@sensorId,
			@sensorTimestampUtc,
			@value,
			@tabletUUID
	END

RETURN 0
