﻿CREATE TABLE [es].[tblSensors]
(
	[Id] INT NOT NULL IDENTITY PRIMARY KEY, 
    [ProbeId] INT NOT NULL, 
    [SensorIndex] INT NOT NULL, 
    [SensorType] INT NOT NULL, 
    [Compliance] NVARCHAR(50) NULL, 
    CONSTRAINT [FK_tblSensors_ToProbe] FOREIGN KEY ([ProbeId]) REFERENCES [es].[tblProbes]([Id])
)

GO

CREATE UNIQUE INDEX [IX_tblSensors_ProbeSensorIndex] ON [es].[tblSensors] ([ProbeId], [SensorIndex])
GO

CREATE INDEX [IX_tblSensors_Compliance] ON [es].[tblSensors] ([Compliance])
GO
