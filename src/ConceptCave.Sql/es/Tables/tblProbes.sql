﻿CREATE TABLE [es].[tblProbes]
(
	[Id] INT NOT NULL IDENTITY PRIMARY KEY, 
	[Archived] BIT NOT NULL,
	[Key] NVARCHAR(128) NOT NULL,
	[Model] NVARCHAR(128) NOT NULL,
	[SerialNumber] NVARCHAR(128) NOT NULL,
    [Name] NVARCHAR(255) NOT NULL, 
    [AssetId] INT NULL, 
    CONSTRAINT [FK_tblProbes_tblAsset] FOREIGN KEY ([AssetId]) REFERENCES [asset].[tblAsset]([Id])
)

GO

CREATE UNIQUE INDEX [IX_tblProbes_Key] ON [es].[tblProbes] ([Key])
