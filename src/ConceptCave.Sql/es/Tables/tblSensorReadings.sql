﻿CREATE TABLE [es].[tblSensorReadings]
(
	[Id] INT NOT NULL IDENTITY PRIMARY KEY, 
	[SensorTimestampUtc] DATETIME NOT NULL,
	[ServerTimestampUtc] DATETIME NOT NULL,
    [SensorId] INT NOT NULL, 
    [Value] NUMERIC(18, 10) NOT NULL, 
    [TabletUUID] NVARCHAR(36) NULL, 
    CONSTRAINT [FK_tblSensorReadings_ToSensors] FOREIGN KEY ([SensorId]) REFERENCES [es].[tblSensors]([Id]), 
    CONSTRAINT [FK_tblSensorReadings_ToTabletUUID] FOREIGN KEY ([TabletUUID]) REFERENCES [asset].[tblTabletUUID]([UUID])
)

GO

CREATE INDEX [IX_tblSensorReadings_SensorTimestampUtc] ON [es].[tblSensorReadings] ([SensorTimestampUtc])
GO

CREATE INDEX [IX_tblSensorReadings_SensorId] ON [es].[tblSensorReadings] ([SensorId])
