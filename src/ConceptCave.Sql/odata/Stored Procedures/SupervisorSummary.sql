﻿CREATE PROCEDURE [odata].[SupervisorSummary]
	@startDate as datetime, 
	@endDate as datetime
AS
BEGIN
	SET NOCOUNT ON
		
	SELECT 
		RosterId, 
		RosterName,
		SupervisorId, 
		SupervisorName, 
		SUM(TotalTasks) AS TotalTasks,
		SUM(TasksApproved) AS TasksApproved,
		SUM(TasksStarted) AS TasksStarted,
		SUM(TasksPaused) AS TasksPaused,
		SUM(TasksFinishedComplete) AS TasksFinishedComplete,
		SUM(TasksFinishedIncomplete) AS TasksFinishedIncomplete,
		SUM(TasksFinishedByManagement) AS TasksFinishedByManagement,
		SUM(TasksFinishedBySystem) AS TasksFinishedBySystem,
		SUM(TasksCancelled) AS TasksCancelled,
		(SELECT COUNT(Id) from [gce].[tblProjectJobTask] pjt WHERE ([OwnerId] = t.SupervisorId ) AND [Status] = 4 AND [DateCompleted] BETWEEN @startDate AND @endDate) AS TasksFinishedCompleteBySupervisor
	FROM (
	SELECT 
		u.UserId,
		u.Name,
		u.CompanyId,
		u.UserTypeId,
		ut.Name as UserType,
		r.id as RosterId,
		r.Name as RosterName,
		phb.userid as SupervisorId,
		su.Name as SupervisorName,
		(SELECT COUNT(Id) from [gce].[tblProjectJobTask] t WHERE ([OwnerId] = u.UserId AND t.RosterId = r.Id) AND ([Status] IN (1,2,3,8) OR ([Status] IN (4,5,7,12) AND [DateCompleted] BETWEEN @startDate AND @endDate)) ) AS TotalTasks,
		(SELECT COUNT(Id) from [gce].[tblProjectJobTask] t WHERE ([OwnerId] = u.UserId AND t.RosterId = r.Id) AND [Status] IN (1,8)) AS TasksApproved,
		(SELECT COUNT(Id) from [gce].[tblProjectJobTask] t WHERE ([OwnerId] = u.UserId AND t.RosterId = r.Id) AND [Status] = 2) AS TasksStarted,
		(SELECT COUNT(Id) from [gce].[tblProjectJobTask] t WHERE ([OwnerId] = u.UserId AND t.RosterId = r.Id) AND [Status] = 3) AS TasksPaused,
		(SELECT COUNT(Id) from [gce].[tblProjectJobTask] t WHERE ([OwnerId] = u.UserId AND t.RosterId = r.Id) AND [Status] = 4 AND [DateCompleted] BETWEEN @startDate AND @endDate) AS TasksFinishedComplete,
		(SELECT COUNT(Id) from [gce].[tblProjectJobTask] t WHERE ([OwnerId] = u.UserId AND t.RosterId = r.Id) AND [Status] = 5 AND [DateCompleted] BETWEEN @startDate AND @endDate) AS TasksFinishedIncomplete,
		(SELECT COUNT(Id) from [gce].[tblProjectJobTask] t WHERE ([OwnerId] = u.UserId AND t.RosterId = r.Id) AND [Status] = 7 AND [DateCompleted] BETWEEN @startDate AND @endDate) AS TasksFinishedByManagement,
		(SELECT COUNT(Id) from [gce].[tblProjectJobTask] t WHERE ([OwnerId] = u.UserId AND t.RosterId = r.Id) AND [Status] = 12 AND [DateCompleted] BETWEEN @startDate AND @endDate) AS TasksFinishedBySystem,
		(SELECT COUNT(Id) from [gce].[tblProjectJobTask] t WHERE ([OwnerId] = u.UserId AND t.RosterId = r.Id) AND [Status] = 6 AND [StatusDate] BETWEEN @startDate AND @endDate) AS TasksCancelled
	FROM [dbo].[tblUserData] u
	INNER JOIN [dbo].[tblUserType] ut ON ut.Id = u.UserTypeId
	RIGHT OUTER JOIN [dbo].[tblHierarchyBucket] HB ON HB.UserId = u.UserId
	INNER JOIN [dbo].[tblHierarchy] H ON HB.HierarchyId = H.Id
	INNER JOIN [gce].[tblRoster] R ON R.HierarchyId = H.Id
	INNER JOIN [dbo].[tblHierarchyBucket] PHB ON HB.ParentId = PHB.Id
	INNER JOIN [dbo].[tblUserData] su ON PHB.UserId = su.UserId
	WHERE ut.IsASite = 0
	) t GROUP BY t.SupervisorId, t.SupervisorName, t.RosterId, t.RosterName
END
