﻿CREATE PROCEDURE [odata].[UserTaskSummary]
	@startDate as datetime, 
	@endDate as datetime,
	@hierarchyId as int,
	@leftIndex as int,
	@rightIndex as int,
	@enforeHierarchy as bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF @enforeHierarchy = 0
		BEGIN
			SELECT 
				u.UserId,
				u.Name,
				u.CompanyId,
				u.UserTypeId,
				ut.Name as UserType,
				(SELECT COUNT(Id) from [gce].[tblProjectJobTask] WHERE [OwnerId] = UserId AND ([Status] IN (1,2,3,8) OR ([Status] IN (4,5,7,12) AND [DateCompleted] BETWEEN @startDate AND @endDate)) ) AS TotalTasks,
				(SELECT COUNT(Id) from [gce].[tblProjectJobTask] WHERE [OwnerId] = UserId AND [Status] IN (1,8)) AS TasksApproved,
				(SELECT COUNT(Id) from [gce].[tblProjectJobTask] WHERE [OwnerId] = UserId AND [Status] = 2) AS TasksStarted,
				(SELECT COUNT(Id) from [gce].[tblProjectJobTask] WHERE [OwnerId] = UserId AND [Status] = 3) AS TasksPaused,
				(SELECT COUNT(Id) from [gce].[tblProjectJobTask] WHERE [OwnerId] = UserId AND [Status] = 4 AND [DateCompleted] BETWEEN @startDate AND @endDate) AS TasksFinishedComplete,
				(SELECT COUNT(Id) from [gce].[tblProjectJobTask] WHERE [OwnerId] = UserId AND [Status] = 5 AND [DateCompleted] BETWEEN @startDate AND @endDate) AS TasksFinishedIncomplete,
				(SELECT COUNT(Id) from [gce].[tblProjectJobTask] WHERE [OwnerId] = UserId AND [Status] IN (7,12) AND [DateCompleted] BETWEEN @startDate AND @endDate) AS TasksFinishedByManagement,
				(SELECT COUNT(Id) from [gce].[tblProjectJobTask] WHERE [OwnerId] = UserId AND [Status] = 6 AND [StatusDate] BETWEEN @startDate AND @endDate) AS TasksCancelled,
				(SELECT TOP 1 Name from [gce].[tblProjectJobTask] WHERE [OwnerId] = UserId AND [Status] = 2 ORDER BY StatusDate DESC) AS CurrentTask,
				(SELECT TOP 1 tt.Name from [gce].[tblProjectJobTask] t INNER JOIN [gce].[tblProjectJobTaskType] tt on tt.Id = t.TaskTypeId WHERE [OwnerId] = UserId AND [Status] = 2 ORDER BY StatusDate DESC) AS CurrentTaskType,
				(SELECT TOP 1 Name from [gce].[tblProjectJobTask] WHERE [OwnerId] = UserId AND [Status] IN (4,5,7,12) ORDER BY StatusDate DESC) AS LastTask,
				(SELECT TOP 1 tt.Name from [gce].[tblProjectJobTask] t INNER JOIN [gce].[tblProjectJobTaskType] tt on tt.Id = t.TaskTypeId WHERE [OwnerId] = UserId AND [Status] IN (4,5,7,12) ORDER BY StatusDate DESC) AS LastTaskType,
				ISNULL((SELECT TOP 1 DateCreated from [gce].[tblProjectJobTaskWorkLog] WHERE [gce].[tblProjectJobTaskWorkLog].[UserId] = u.UserId AND DateCompleted is null and DateCreated >= @startDate ORDER BY DateCreated Desc), (SELECT TOP 1 DateCompleted FROM [gce].[tblProjectJobTaskWorkLog] WHERE [gce].[tblProjectJobTaskWorkLog].[UserId] = u.UserId and DateCreated >= @startDate ORDER BY DateCompleted Desc)) AS LastActivityDate,
				(SELECT TOP 1 DateCreated from [gce].[tblProjectJobTaskWorkLog] WHERE [gce].[tblProjectJobTaskWorkLog].[UserId] = u.UserId AND DateCreated >= @startDate ORDER BY DateCreated asc) AS FirstActivityDate,
				(SELECT SUM(ActualDuration) from [gce].[tblProjectJobTask] t WHERE [OwnerId] = userId AND [StatusDate] BETWEEN @startDate and @endDate) AS ActiveDuration,
				(SELECT SUM(InactiveDuration) from [gce].[tblProjectJobTask] t WHERE [OwnerId] = userId AND [StatusDate] BETWEEN @startDate and @endDate) AS InactiveDuration,
				(SELECT SUM(AllActivitiesEstimatedDuration) from [gce].[tblProjectJobTask] t WHERE [OwnerId] = userId AND [StatusDate] BETWEEN @startDate and @endDate) AS AllActivitiesEstimatedDuration,
				(SELECT SUM(CompletedActivitiesEstimatedDuration) from [gce].[tblProjectJobTask] t WHERE [OwnerId] = userId AND [StatusDate] BETWEEN @startDate and @endDate) AS CompletedActivitiesEstimatedDuration,
				(SELECT SUM(WorkingDocumentCount) from [gce].[tblProjectJobTask] t WHERE [OwnerId] = userId AND [StatusDate] BETWEEN @startDate and @endDate) AS WorkingDocumentCount
			FROM [dbo].[tblUserData] u
			INNER JOIN [dbo].[tblUserType] ut ON ut.Id = u.UserTypeId
			WHERE ut.IsASite = 0
		END
	ELSE
		BEGIN
			SELECT 
				u.UserId,
				u.Name,
				u.CompanyId,
				u.UserTypeId,
				ut.Name as UserType,
				(SELECT COUNT(Id) from [gce].[tblProjectJobTask] WHERE [OwnerId] = UserId AND [HierarchyBucketId] IN (SELECT Id FROM tblHierarchyBucket WHERE [HierarchyId] = @hierarchyId AND [LeftIndex] > @leftIndex AND [RightIndex] < @rightIndex) AND ([Status] IN (1,2,3,8) OR ([Status] IN (4,5,7,12) AND [DateCompleted] BETWEEN @startDate AND @endDate)) ) AS TotalTasks,
				(SELECT COUNT(Id) from [gce].[tblProjectJobTask] WHERE [OwnerId] = UserId AND [HierarchyBucketId] IN (SELECT Id FROM tblHierarchyBucket WHERE [HierarchyId] = @hierarchyId AND [LeftIndex] > @leftIndex AND [RightIndex] < @rightIndex) AND [Status] IN (1,8)) AS TasksApproved,
				(SELECT COUNT(Id) from [gce].[tblProjectJobTask] WHERE [OwnerId] = UserId AND [HierarchyBucketId] IN (SELECT Id FROM tblHierarchyBucket WHERE [HierarchyId] = @hierarchyId AND [LeftIndex] > @leftIndex AND [RightIndex] < @rightIndex) AND [Status] = 2) AS TasksStarted,
				(SELECT COUNT(Id) from [gce].[tblProjectJobTask] WHERE [OwnerId] = UserId AND [HierarchyBucketId] IN (SELECT Id FROM tblHierarchyBucket WHERE [HierarchyId] = @hierarchyId AND [LeftIndex] > @leftIndex AND [RightIndex] < @rightIndex) AND [Status] = 3) AS TasksPaused,
				(SELECT COUNT(Id) from [gce].[tblProjectJobTask] WHERE [OwnerId] = UserId AND [HierarchyBucketId] IN (SELECT Id FROM tblHierarchyBucket WHERE [HierarchyId] = @hierarchyId AND [LeftIndex] > @leftIndex AND [RightIndex] < @rightIndex) AND [Status] = 4 AND [DateCompleted] BETWEEN @startDate AND @endDate) AS TasksFinishedComplete,
				(SELECT COUNT(Id) from [gce].[tblProjectJobTask] WHERE [OwnerId] = UserId AND [HierarchyBucketId] IN (SELECT Id FROM tblHierarchyBucket WHERE [HierarchyId] = @hierarchyId AND [LeftIndex] > @leftIndex AND [RightIndex] < @rightIndex) AND [Status] = 5 AND [DateCompleted] BETWEEN @startDate AND @endDate) AS TasksFinishedIncomplete,
				(SELECT COUNT(Id) from [gce].[tblProjectJobTask] WHERE [OwnerId] = UserId AND [HierarchyBucketId] IN (SELECT Id FROM tblHierarchyBucket WHERE [HierarchyId] = @hierarchyId AND [LeftIndex] > @leftIndex AND [RightIndex] < @rightIndex) AND [Status] IN (7,12) AND [DateCompleted] BETWEEN @startDate AND @endDate) AS TasksFinishedByManagement,
				(SELECT COUNT(Id) from [gce].[tblProjectJobTask] WHERE [OwnerId] = UserId AND [HierarchyBucketId] IN (SELECT Id FROM tblHierarchyBucket WHERE [HierarchyId] = @hierarchyId AND [LeftIndex] > @leftIndex AND [RightIndex] < @rightIndex) AND [Status] = 6 AND [StatusDate] BETWEEN @startDate AND @endDate) AS TasksCancelled,
				(SELECT TOP 1 Name from [gce].[tblProjectJobTask] WHERE [OwnerId] = UserId AND [HierarchyBucketId] IN (SELECT Id FROM tblHierarchyBucket WHERE [HierarchyId] = @hierarchyId AND [LeftIndex] > @leftIndex AND [RightIndex] < @rightIndex) AND [Status] = 2 ORDER BY StatusDate DESC) AS CurrentTask,
				(SELECT TOP 1 tt.Name from [gce].[tblProjectJobTask] t INNER JOIN [gce].[tblProjectJobTaskType] tt on tt.Id = t.TaskTypeId WHERE [OwnerId] = UserId AND [HierarchyBucketId] IN (SELECT Id FROM tblHierarchyBucket WHERE [HierarchyId] = @hierarchyId AND [LeftIndex] > @leftIndex AND [RightIndex] < @rightIndex) AND [Status] = 2 ORDER BY StatusDate DESC) AS CurrentTaskType,
				(SELECT TOP 1 Name from [gce].[tblProjectJobTask] WHERE [OwnerId] = UserId AND [HierarchyBucketId] IN (SELECT Id FROM tblHierarchyBucket WHERE [HierarchyId] = @hierarchyId AND [LeftIndex] > @leftIndex AND [RightIndex] < @rightIndex) AND [Status] IN (4,5,7,12) ORDER BY StatusDate DESC) AS LastTask,
				(SELECT TOP 1 tt.Name from [gce].[tblProjectJobTask] t INNER JOIN [gce].[tblProjectJobTaskType] tt on tt.Id = t.TaskTypeId WHERE [OwnerId] = UserId AND [HierarchyBucketId] IN (SELECT Id FROM tblHierarchyBucket WHERE [HierarchyId] = @hierarchyId AND [LeftIndex] > @leftIndex AND [RightIndex] < @rightIndex) AND [Status] IN (4,5,7,12) ORDER BY StatusDate DESC) AS LastTaskType,
				ISNULL((SELECT TOP 1 DateCreated from [gce].[tblProjectJobTaskWorkLog] WHERE [gce].[tblProjectJobTaskWorkLog].[UserId] = u.UserId AND DateCompleted is null and DateCreated >= @startDate ORDER BY DateCreated Desc), (SELECT TOP 1 DateCompleted FROM [gce].[tblProjectJobTaskWorkLog] WHERE [gce].[tblProjectJobTaskWorkLog].[UserId] = u.UserId and DateCreated >= @startDate ORDER BY DateCompleted Desc)) AS LastActivityDate,
				(SELECT TOP 1 w.DateCreated from [gce].[tblProjectJobTaskWorkLog] w INNER JOIN [gce].[tblProjectJobTask] t ON t.Id = w.ProjectJobTaskId WHERE w.[UserId] = u.UserId AND [HierarchyBucketId] IN (SELECT Id FROM tblHierarchyBucket WHERE [HierarchyId] = @hierarchyId AND [LeftIndex] > @leftIndex AND [RightIndex] < @rightIndex) AND w.DateCreated >= @startDate ORDER BY w.DateCreated asc) AS FirstActivityDate,
				(SELECT SUM(ActualDuration) from [gce].[tblProjectJobTask] t WHERE [OwnerId] = userId AND [HierarchyBucketId] IN (SELECT Id FROM tblHierarchyBucket WHERE [HierarchyId] = @hierarchyId AND [LeftIndex] > @leftIndex AND [RightIndex] < @rightIndex) AND [StatusDate] BETWEEN @startDate and @endDate) AS ActiveDuration,
				(SELECT SUM(InactiveDuration) from [gce].[tblProjectJobTask] t WHERE [OwnerId] = userId AND [HierarchyBucketId] IN (SELECT Id FROM tblHierarchyBucket WHERE [HierarchyId] = @hierarchyId AND [LeftIndex] > @leftIndex AND [RightIndex] < @rightIndex) AND [StatusDate] BETWEEN @startDate and @endDate) AS InactiveDuration,
				(SELECT SUM(AllActivitiesEstimatedDuration) from [gce].[tblProjectJobTask] t WHERE [OwnerId] = userId AND [HierarchyBucketId] IN (SELECT Id FROM tblHierarchyBucket WHERE [HierarchyId] = @hierarchyId AND [LeftIndex] > @leftIndex AND [RightIndex] < @rightIndex) AND [StatusDate] BETWEEN @startDate and @endDate) AS AllActivitiesEstimatedDuration,
				(SELECT SUM(CompletedActivitiesEstimatedDuration) from [gce].[tblProjectJobTask] t WHERE [OwnerId] = userId AND [HierarchyBucketId] IN (SELECT Id FROM tblHierarchyBucket WHERE [HierarchyId] = @hierarchyId AND [LeftIndex] > @leftIndex AND [RightIndex] < @rightIndex) AND [StatusDate] BETWEEN @startDate and @endDate) AS CompletedActivitiesEstimatedDuration,
				(SELECT SUM(WorkingDocumentCount) from [gce].[tblProjectJobTask] t WHERE [OwnerId] = userId AND [HierarchyBucketId] IN (SELECT Id FROM tblHierarchyBucket WHERE [HierarchyId] = @hierarchyId AND [LeftIndex] > @leftIndex AND [RightIndex] < @rightIndex) AND [StatusDate] BETWEEN @startDate and @endDate) AS WorkingDocumentCount
			FROM [dbo].[tblUserData] u
			INNER JOIN [dbo].[tblUserType] ut ON ut.Id = u.UserTypeId
			WHERE ut.IsASite = 0 AND u.UserId IN (SELECT UserId FROM tblHierarchyBucket WHERE [HierarchyId] = @hierarchyId AND [LeftIndex] > @leftIndex AND [RightIndex] < @rightIndex)
		END
END