﻿CREATE VIEW [odata].[GpsLocation]
	AS
SELECT
	gpsl.Id,
	gpsl.LocationType,
	gpsl.TimestampUtc,
	gpsl.LastTimestampUtc,
	gpsl.TimestampCount,
	gpsl.TabletUUID,
	gpsl.Latitude,
	gpsl.Longitude,
	gpsl.AccuracyMetres,
	gpsl.Altitude,
	gpsl.AltitudeAccuracyMetres,
	gpsl.[Floor],
	gpsl.FacilityStructureFloorId,
	gpsl.StaticBeaconId,
	gpsl.TaskId,
	gpsl.Archived
FROM
	asset.tblGPSLocation gpsl