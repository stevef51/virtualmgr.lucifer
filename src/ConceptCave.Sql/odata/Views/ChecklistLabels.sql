﻿CREATE VIEW [odata].[ChecklistLabels]
	AS SELECT
	CompletedWorkingDocumentFactId as Id,
	CompletedLabelDimensionId as LabelId,
	l.Name,
	l.Backcolor,
	l.Forecolor
	FROM [tblCompletedLabelWorkingDocumentDimension] d
	INNER JOIN tblCompletedLabelDimension l on l.Id = d.CompletedLabelDimensionId