﻿CREATE VIEW [odata].[TaskLabels]
	AS SELECT
	ProjectJobTaskId as Id,
	LabelId,
	l.Name,
	l.Backcolor,
	l.Forecolor
	FROM [gce].tblProjectJobTaskLabel d
	INNER JOIN tblLabel l on l.Id = d.LabelId