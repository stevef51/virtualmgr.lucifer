﻿CREATE VIEW [odata].[MediaViews]
	AS SELECT 
		v.Id,
		v.MediaId,
		v.UserId,
		d.Name AS Document,
		u.Name,
		m.UserName,
		u.UserTypeId,
		t.Name AS UserType,
		v.[Version],
		v.DateStarted,
		v.DateCompleted,
		v.TotalSeconds,
		v.Accepted,
		v.AcceptionNotes,
		v.WorkingDocumentId
	FROM [tblMediaViewing] as v
	INNER JOIN [dbo].tblMedia AS d ON d.Id = v.MediaId
	INNER JOIN [dbo].tblUserData AS u ON u.UserId = v.UserId
	INNER JOIN [dbo].Users AS m ON m.UserId = v.UserId
	INNER JOIN [dbo].tblUserType AS t ON t.Id = u.UserTypeId
