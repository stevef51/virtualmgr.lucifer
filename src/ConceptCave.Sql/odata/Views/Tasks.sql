﻿CREATE VIEW [odata].[Tasks] AS
SELECT 
	t.Id, 
	RosterId, 
	t.ProjectJobTaskGroupId as GroupId,
	r.Name as Roster,
	TaskTypeId, 
	tt.Name as TaskType,
	OriginalTaskTypeId,
	tto.Name as OriginalTaskType,
	DateCreated,
	(SELECT TOP 1 DateCreated FROM [gce].tblProjectJobTaskWorkLog WHERE ProjectJobTaskId = t.Id ORDER BY DateCreated asc) as DateStarted,
	DateCompleted,
	(DateDiff(d, DateCompleted, GETUTCDATE())) AS DaysSinceCompletion,
	t.Name,
	t.[Description],
	OwnerId,
	o.Name as [Owner],
	o.UserTypeId AS [OwnerTypeId],
	o.CompanyId as OwnerCompanyId,
	OriginalOwnerId,
	oo.Name as [OriginalOwner],
	oo.UserTypeId AS [OriginalOwnerTypeId],
	ScheduledOwnerId,
	so.Name as [ScheduledOwner],
	so.UserTypeId AS [ScheduledOwnerTypeId],
	t.SiteId,
	s.Name as SiteName,
	s.UserTypeId AS SiteTypeId,
	SiteLatitude,
	SiteLongitude,
	RoleId,
	rl.Name as [Role],
	t.[Level],
	CompletionNotes,
	[Status],
	StatusDate,
	(CASE 
		WHEN [Status] = 0 THEN 'Unapproved'
		WHEN [Status] = 1 THEN 'Approved'
		WHEN [Status] = 2 THEN 'Started'
		WHEN [Status] = 3 THEN 'Paused'
		WHEN [Status] = 4 THEN 'FinishedComplete'
		WHEN [Status] = 5 THEN 'FinishedIncomplete'
		WHEN [Status] = 6 THEN 'Cancelled'
		WHEN [Status] = 7 THEN 'FinishedByManagement'
		WHEN [Status] = 8 THEN 'ApprovedContinued'
		WHEN [Status] = 9 THEN 'ChangeRosterRequested'
		WHEN [Status] = 10 THEN 'ChangeRosterRejected'
		WHEN [Status] = 11 THEN 'ChangeRosterAccepted'
		WHEN [Status] = 12 THEN 'FinishedBySystem'
	END
	) as StatusText,
	StatusId as [CustomStatusId],
	pjs.Text as [CustomStatusText],
	t.ActualDuration,
	t.InactiveDuration,
	t.AllActivitiesEstimatedDuration,
	t.CompletedActivitiesEstimatedDuration,
	t.EstimatedDurationSeconds,
	t.WorkingDocumentCount,
	t.ProjectJobScheduledTaskId,
	HierarchyBucketId,
	CONVERT(DATETIME, t.StartTimeUtc, 1) AS StartTime,
	CONVERT(DATETIME, t.LateAfterUtc, 1) AS LateAfter,
	t.HasOrders,
	t.AssetId,
	t.EndsOn,
	zonesites.Id AS FSSiteId,
	t.SortOrder
FROM [gce].[tblProjectJobTask] as t
INNER JOIN [gce].[tblRoster] r on r.Id = t.RosterId
INNER JOIN [gce].[tblProjectJobTaskType] tt on tt.Id = t.TaskTypeId
INNER JOIN [gce].[tblProjectJobTaskType] tto on tto.Id = t.OriginalTaskTypeId
INNER JOIN [dbo].[tblUserData] o on o.UserId = t.OwnerId
INNER JOIN [dbo].[tblUserData] oo on oo.UserId = t.OriginalOwnerId
LEFT JOIN [dbo].[tblUserData] so on so.UserId = t.ScheduledOwnerId
LEFT JOIN [dbo].[tblUserData] s on s.UserId = t.SiteId
LEFT JOIN [dbo].[tblHierarchyRole] rl on rl.Id = t.RoleId
LEFT JOIN [gce].[tblProjectJobStatus] pjs on pjs.Id = t.StatusId
LEFT JOIN [odata].FSZoneSites zonesites on t.SiteId = zonesites.SiteId AND zonesites.Archived = 0
