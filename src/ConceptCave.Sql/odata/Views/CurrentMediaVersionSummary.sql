﻿CREATE VIEW [odata].[CurrentMediaVersionSummary]
	AS SELECT 
		u.UserId,
		m.Id AS MediaId,
		u.Name as Name, 
		m.Name as Document,
		m.[Version] as CurrentVersion,
		(SELECT TOP 1 [Version] FROM tblMediaViewing WHERE UserId = u.UserId AND MediaId = m.Id ORDER BY [Version] desc) AS LatestVersionViewed
	FROM tblUserData u, tblMedia m
	WHERE m.Id in (SELECT MediaId FROM tblMediaFolderFile)