﻿CREATE VIEW [odata].[Schedules]
	AS SELECT 
		s.Id, 
		s.Name, 
		s.Description, 
		s.[Level], 
		s.IsArchived, 
		s.LengthInDays, 
		s.StartTime,
		s.StartTimeWindowSeconds,
		s.EstimatedDurationSeconds,
		s.CalendarRuleId, 
		c.DateCreated, 
		c.StartDate, 
		c.EndDate, 
		c.ExecuteSunday, 
		c.ExecuteMonday, 
		c.ExecuteTuesday, 
		c.ExecuteWednesday, 
		c.ExecuteThursday, 
		c.ExecuteFriday, 
		c.ExecuteSaturday, 
		c.CRONExpression, 
		c.Frequency, 
		c.Period, 
		u.UserId AS OwnerId, 
		u.Name AS Owner, 
        u.CompanyId AS OwnerCompanyId, 
		s.SiteId, 
		st.Name AS SiteName, 
		s.TaskTypeId, 
		tt.Name AS TaskType, 
		s.RosterId, 
		r.Name AS Roster, 
		s.RoleId, 
		hr.Name AS Role, 
		b.Id AS HierarchyBucketId,
		siteType.Name AS SiteType,
		s.SortOrder
FROM  gce.tblProjectJobScheduledTask AS s INNER JOIN
         dbo.tblCalendarRule AS c ON s.CalendarRuleId = c.Id INNER JOIN
         gce.tblRoster AS r ON s.RosterId = r.Id RIGHT OUTER JOIN
         dbo.tblHierarchyBucket AS b ON s.RoleId = b.RoleId AND r.HierarchyId = b.HierarchyId LEFT OUTER JOIN
         dbo.tblUserData AS u ON b.UserId = u.UserId LEFT OUTER JOIN
         dbo.tblUserData AS st ON s.SiteId = st.UserId LEFT OUTER JOIN
		 dbo.tblUserType AS siteType ON siteType.Id = st.UserTypeId INNER JOIN
         gce.tblProjectJobTaskType AS tt ON s.TaskTypeId = tt.Id LEFT OUTER JOIN
         dbo.tblHierarchyRole AS hr ON s.RoleId = hr.Id 