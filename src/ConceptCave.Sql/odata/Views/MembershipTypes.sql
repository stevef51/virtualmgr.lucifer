﻿CREATE VIEW odata.MembershipTypes AS
SELECT 
	ut.Id,
	ut.[Name],
	ut.IsASite,
	ut.CanLogin,
	ut.DefaultDashboard
FROM 
	tblUserType ut 
	
