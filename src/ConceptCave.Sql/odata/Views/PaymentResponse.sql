﻿CREATE VIEW [odata].[PaymentResponse]
AS SELECT 
	pr.Id,
	pr.PendingPaymentId,
	pr.TransactionStatus,
	pr.DateUtc,
	pr.DateUtc as DateLocal,
	pr.RequestedAmount,
	pr.TransactedAmount,
	pr.GatewayTransactionId,
	pr.GatewayAuthorisationCode,
	pr.Response,
    pr.ResponseCode,
    pr.ResponseMessage,
    pr.Errors,
	pp.GatewayName,
	pp.Sandbox,
	pp.InvoiceNumber,
	pp.OrderId
FROM 
	[epay].[tblPaymentResponse] pr INNER JOIN epay.tblPendingPayment pp ON pr.PendingPaymentId = pp.Id
WHERE
	pp.Archived = 0
