﻿CREATE VIEW [odata].[TimeBetweenTask]
	AS SELECT 
	-- Need a unique column otherwise EF will use 1st column as key which is not unique producing
	-- duplicate EF rows for each view row
		ROW_NUMBER() OVER(ORDER BY A.StartTime) as [Row],
		A.*, 
	-- BeforeLongIdleTask is the record (in datecreated reverse order) AFTER the longest idle task for a user on a day
	(SELECT TOP 1 
		TT1.Name
	FROM gce.tblProjectJobTaskWorkLog TWL1 
		INNER JOIN gce.tblProjectJobTask T1 ON T1.Id = TWL1.ProjectJobTaskId   
		INNER JOIN gce.tblProjectJobTaskType TT1 ON T1.TaskTypeId = TT1.Id 
	WHERE 
		TWL1.UserId = A.UserId AND
		TWL1.DateCreated < (SELECT TOP 1 TWL2.DateCreated FROM gce.tblProjectJobTaskWorkLog TWL2 WHERE TWL2.UserId = A.UserId AND TWL2.DateCreated BETWEEN A.StartTime AND A.EndTime AND TWL2.InactiveDuration <> 0 ORDER BY TWL2.InactiveDuration DESC)
	ORDER BY 
		TWL1.DateCreated DESC
	) AS BeforeLongIdleTask,

	-- AfterLongIdleTask is a little simpler since the MAX(InactiveDuration) for a user on a given day is this task
	(SELECT TOP 1 
		TT1.Name 
	FROM gce.tblProjectJobTaskWorkLog TWL1 
		INNER JOIN gce.tblProjectJobTask T1 ON T1.Id = TWL1.ProjectJobTaskId AND TWL1.UserId = A.UserId 
		INNER JOIN gce.tblProjectJobTaskType TT1 ON T1.TaskTypeId = TT1.Id 
	WHERE 
		TWL1.DateCreated BETWEEN A.StartTime AND A.EndTime 
		AND TWL1.InactiveDuration <> 0 
	ORDER BY 
		TWL1.InactiveDuration DESC) AS AfterLongIdleTask

FROM (
	SELECT 
		U.UserId,
		U.Name, 
		U.CompanyId,
		T.HierarchyBucketId,
		CAST(TWL.DateCreated AS date) AS [Date], 
		MIN(TWL.DateCreated) AS StartTime,
		MAX(TWL.DateCompleted) AS EndTime,
		SUM(TWL.ActualDuration) AS ActualTime,
		SUM(TWL.InactiveDuration) AS IdleTime,
		COUNT(1) AS Count
	FROM gce.tblProjectJobTaskWorkLog TWL
		INNER JOIN tblUserData U ON TWL.UserId = U.UserId
		INNER JOIN gce.tblProjectJobTask T ON T.Id = TWL.ProjectJobTaskId
	GROUP BY 
		U.UserId,
		U.Name, 
		U.CompanyId,
		T.HierarchyBucketId,
		CAST(TWL.DateCreated AS date)
) A
