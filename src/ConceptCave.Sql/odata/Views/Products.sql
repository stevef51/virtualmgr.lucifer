﻿CREATE VIEW [odata].[Products]
AS SELECT 
	p.Id,
	p.Code,
	p.[Name],
	p.[Description],
	p.IsCoupon,
	p.MediaId,
	p.Price,
	p.PriceJsFunctionId,
	p.ProductTypeId,
	p.Extra
FROM [stock].[tblProduct] p
