﻿CREATE VIEW [odata].[PeriodicExecutionHistory]
AS SELECT 

	Id,
	StartUtc,
	FinishUtc,
	ForceFinished

FROM [pejob].[tblPeriodicExecutionHistory]
