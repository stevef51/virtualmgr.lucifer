﻿CREATE VIEW [odata].[ResourceLabels]
AS 
	SELECT 
		ROW_NUMBER() OVER (ORDER BY lr.ResourceId, lr.LabelId) AS Row,
		r.Id AS ResourceId, 
		l.* 
	FROM 
		dbo.tblResource r 
		INNER JOIN dbo.tblLabelResource lr ON lr.ResourceId = r.Id
		INNER JOIN dbo.tblLabel l ON l.Id = lr.LabelId
