﻿CREATE VIEW [odata].[TaskTypeRelationships]
AS SELECT 
	ttr.ProjectJobTaskTypeDestinationId AS DestinationTaskTypeId,
	ttd.Name AS DestinationTaskTypeName,
	ttr.ProjectJobTaskTypeSourceId AS SourceTaskTypeId,
	tts.Name AS SourceTaskTypeName,
	ttr.RelationshipType
FROM gce.tblProjectJobTaskTypeRelationship ttr
INNER JOIN gce.tblProjectJobTaskType ttd ON ttr.ProjectJobTaskTypeDestinationId = ttd.Id
INNER JOIN gce.tblProjectJobTaskType tts ON ttr.ProjectJobTaskTypeSourceId = tts.Id