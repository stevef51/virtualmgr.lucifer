﻿CREATE VIEW [odata].[UserTypeTaskType]
	AS SELECT 
		utttr.RelationshipType,
		utttr.UserTypeId,
		utttr.TaskTypeId,
		ut.[Name] AS UserTypeName,
		ut.IsASite,
		tt.[Name] AS TaskTypeName
	FROM [gce].[tblUserTypeTaskTypeRelationship] utttr
	INNER JOIN [dbo].[tblUserType] ut ON utttr.UserTypeId = ut.Id
	INNER JOIN [gce].[tblProjectJobTaskType] tt ON utttr.TaskTypeId = tt.Id
