﻿CREATE VIEW [odata].[Assets]
	AS SELECT
	a.Id as Id,
	a.Name,
	a.AssetTypeId as AssetTypeId,
	t.Name as AssetTypeName,
	a.DateCreated,
	a.DateDecomissioned,
	t.CanTrack,
	t.Forecolor,
	t.Backcolor,
	b.Id as BeaconId,
	a.FacilityStructureId,
	a.CompanyId
	FROM [asset].tblAsset a
	INNER JOIN [asset].tblAssetType t on t.Id = a.AssetTypeId
	LEFT JOIN [asset].tblBeacon b on b.AssetId = a.Id
