﻿CREATE VIEW [dbo].[ScheduleDays]
	AS SELECT 
		sd.RoleId,
		hr.Name as RoleName,
		sd.RosterId,
		r.Name as RosterName,
		sd.StartTime,
		sd.EndTime,
		sd.Day,
		(CASE 
			WHEN sd.Day = 1 THEN 'Sunday'
			WHEN sd.Day = 2 THEN 'Monday'
			WHEN sd.Day = 3 THEN 'Tuesday'
			WHEN sd.Day = 4 THEN 'Wednesday'
			WHEN sd.Day = 5 THEN 'Thursday'
			WHEN sd.Day = 6 THEN 'Friday'
			WHEN sd.Day = 7 THEN 'Saturday'
		END
		) as [DayOfWeek],
		sd.LunchStart,
		sd.LunchEnd
	FROM [gce].tblProjectJobScheduleDays sd
	INNER JOIN [gce].tblRoster r on r.Id = sd.RosterId
	INNER JOIN tblHierarchyRole hr on hr.Id = sd.RoleId
