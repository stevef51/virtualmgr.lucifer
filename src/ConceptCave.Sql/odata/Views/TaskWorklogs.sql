﻿CREATE VIEW [odata].[TaskWorklogs]
	AS SELECT 
	w.Id,
	w.ProjectJobTaskId as TaskId,
	t.Name as TaskName,
	w.UserId,
	u.Name as [Owner],
	u.CompanyId as [OwnerCompanyId],
	s.Name as SiteName,
	s.UserId as SiteId,
	w.DateCreated,
	w.ClockinLatitude,
	w.ClockinLongitude,
	w.ClockinAccuracy,
	w.ClockinLocationStatus,
	w.ClockinMetresFromSite,
	w.DateCompleted,
	w.ClockoutLatitude,
	w.ClockoutLongitude,
	w.ClockoutAccuracy,
	w.ClockoutLocationStatus,
	w.ClockoutMetresFromSite,
	w.Notes,
	w.ActualDuration,
	w.InactiveDuration,
	t.HierarchyBucketId,
	i.DateCreated AS ArrivedAtServerDateCreated,
	i.DateCompleted AS ArrivedAtServerDateCompleted,
	i.CreationQueueTimeSeconds,
	i.CompletedQueueTimeSeconds
	FROM [gce].tblProjectJobTaskWorkLog as w
	INNER JOIN [gce].tblProjectJobTask t on t.Id = w.ProjectJobTaskId
	INNER JOIN [dbo].tblUserData u on u.UserId = w.UserId
	LEFT JOIN [gce].tblProjectJobTaskWorkLogTiming i ON i.Id = w.Id
	LEFT OUTER JOIN [dbo].tblUserData s on s.UserId = t.SiteId
	
