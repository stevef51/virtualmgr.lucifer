﻿CREATE VIEW [odata].[Beacons]
	AS 
SELECT
	b.Id,
	b.Name,
	b.LastContactUtc,
	b.AssetId,
	a.Name AS AssetName,
	a.AssetTypeId AS AssetTypeId,
	at.Name AS AssetTypeName,
	b.BeaconConfigurationId,
	b.FacilityStructureId,
	fs.Name AS FacilityStructureName,
	fs.FloorPlanId AS FacilityStructureFloorPlanId,
	fs.[Level],
	fs.StructureType AS FacilityStructureType,
	fs.SiteId AS SiteId,
	b.StaticLocationId,
	b.StaticHeightFromFloor,
	gps.Latitude,
	gps.Longitude,
	gps.Altitude,
	b.BatteryPercent,
	b.UserId,
	b.TaskTypeId,
	ut.IsASite

FROM [asset].tblBeacon b
LEFT OUTER JOIN [asset].tblGPSLocation gps ON b.StaticLocationId = gps.Id
LEFT OUTER JOIN [asset].tblFacilityStructure fs ON b.FacilityStructureId = fs.Id
LEFT OUTER JOIN [asset].tblAsset a ON b.AssetId = a.Id
LEFT OUTER JOIN [asset].tblAssetType at ON a.AssetTypeId = at.Id
LEFT OUTER JOIN dbo.tblUserData ud ON b.UserId = ud.UserId
LEFT OUTER JOIN dbo.tblUserType ut ON ud.UserTypeId = ut.Id

