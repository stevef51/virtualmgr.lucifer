﻿CREATE VIEW [odata].[Tenants]
	AS SELECT
		t.Id,
		t.HostName,
		t.Name,
		t.[Enabled],
		t.InvoiceIdPrefix,
		t.TimeZone,
		t.BillingAddress,
		t.BillingCompanyName,
		t.Currency,
		t.EmailFromAddress,
		t.TenantHostId,
		h.Name AS TenantHostName,
		h.[Location] AS TenantHostLocation,
		h.WebAppResourceGroup AS TenantResourceGroup,
		t.TenantProfileId,
		p.Name AS TenantProfileName,
		p.[Description] AS TenantProfileDescription,
		p.CanDelete AS TenantProfileCanDelete,
		p.CanClone AS TenantProfileCanClone,
		p.CanDisable AS TenantProfileCanDisable,
		p.CanUpdateSchema AS TenantProfileCanUpdateSchema,
		p.EmailEnabled AS TenantProfileEmailEnabled,
		p.EmailWhitelist As TenantProfileEmailWhitelist,
		p.[Hidden] AS TenantProfileHidden,
		p.CreateAlertMessage AS TenantProfileCreateAlertMessage
	FROM [mtc].[tblTenants] t
	INNER JOIN [mtc].tblTenantProfiles p ON p.Id = t.TenantProfileId
	LEFT JOIN [mtc].tblTenantHosts h  ON h.Id = t.TenantProfileId
