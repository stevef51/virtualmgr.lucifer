﻿CREATE VIEW [odata].[Leave]
	AS SELECT 
		o.Id,
		o.HierarchyBucketId,
		b.[HierarchyId],
		h.Name AS Hierarchy,
		rst.Id AS RosterId,
		rst.Name AS Roster,
		o.RoleId,
		r.Name AS [Role],
		o.OriginalUserId,
		orig.Name AS OriginalUser,
		orig.CompanyId as OriginalUserCompanyId,
		o.UserId AS ReplacementUserId,
		u.Name AS ReplacementUser,
		o.HierarchyBucketOverrideTypeId AS LeaveTypeId,
		t.Name AS LeaveType,
		o.DateCreated,
		o.StartDate,
		o.EndDate,
		o.Notes
	FROM tblHierarchyBucketOverride o
	INNER JOIN tblHierarchyBucket b on b.Id = o.HierarchyBucketId
	INNER JOIN tblHierarchy h on h.Id = b.[HierarchyId]
	INNER JOIN [gce].tblRoster rst on rst.[HierarchyId] = b.[HierarchyId]
	INNER JOIN tblHierarchyRole r on r.Id = o.RoleId
	INNER JOIN tblUserData orig on orig.UserId = o.OriginalUserId
	LEFT OUTER JOIN tblUserData u on u.UserId = o.UserId
	INNER JOIN tblHierarchyBucketOverrideType t on t.Id = o.HierarchyBucketOverrideTypeId
