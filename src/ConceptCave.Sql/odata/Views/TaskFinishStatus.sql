﻿CREATE VIEW [odata].[TaskFinishStatus]
	AS SELECT
	TaskId as Id,
	d.ProjectJobFinishedStatusId,
	l.Stage,
	l.[Text],
	d.Notes
	FROM [gce].tblProjectJobTaskFinishedStatus d
	INNER JOIN [gce].tblProjectJobFinishedStatus l on l.Id = d.ProjectJobFinishedStatusId