﻿CREATE VIEW [odata].[QRCodes]
AS 
	SELECT 
		qr.Id,
		qr.QRCode,
		qr.UserId,
		qr.TaskTypeId,
		qr.AssetId,
		ut.IsASite
	FROM 
		[dbo].[tblQRCode] qr

	LEFT OUTER JOIN 
		dbo.tblUserData ud ON qr.UserId = ud.UserId

	LEFT OUTER JOIN
		dbo.tblUserType ut ON ud.UserTypeId = ut.Id
