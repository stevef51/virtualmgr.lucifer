﻿CREATE VIEW [odata].[ChecklistMediaStream]
AS SELECT 
	presentedFact.PresentedId,
	cwdf.WorkingDocumentId AS WorkingDocumentId,
	cwdd.Name AS Checklist,
	cwdf.DateStarted AS ChecklistDateStarted,
	cwdf.DateCompleted AS ChecklistDateCompleted,
	presentedFact.Prompt AS Prompt,
	uploadMedia.MediaId,
	reviewer.UserId AS ReviewerId,
	reviewer.Name AS ReviewerName,
	reviewerType.Name AS ReviewerType,
	reviewerCompany.Id AS ReviewerCompanyId,
	reviewerCompany.Name AS ReviewerCompanyName,
	reviewee.UserId as RevieweeId,
	reviewee.Name AS RevieweeName,
	revieweeType.Name AS RevieweeType,
	revieweeCompany.Id AS RevieweeCompanyId,
	revieweeCompany.Name AS RevieweeCompanyName,
	task.Id AS TaskId,
	task.Name AS TaskName,
	task.[Description] AS TaskDescription,
	task.DateCreated AS TaskDateCreated,
	task.DateCompleted AS TaskDateCompleted,
	task.[Level] AS TaskLevel,
	task.StatusDate AS TaskStatusDate,
	task.CompletionNotes AS TaskCompletionNotes,
	(CASE 
		WHEN [Status] = 0 THEN 'Unapproved'
		WHEN [Status] = 1 THEN 'Approved'
		WHEN [Status] = 2 THEN 'Started'
		WHEN [Status] = 3 THEN 'Paused'
		WHEN [Status] = 4 THEN 'FinishedComplete'
		WHEN [Status] = 5 THEN 'FinishedIncomplete'
		WHEN [Status] = 6 THEN 'Cancelled'
		WHEN [Status] = 7 THEN 'FinishedByManagement'
		WHEN [Status] = 8 THEN 'ApprovedContinued'
		WHEN [Status] = 9 THEN 'ChangeRosterRequested'
		WHEN [Status] = 10 THEN 'ChangeRosterRejected'
		WHEN [Status] = 11 THEN 'ChangeRosterAccepted'
		WHEN [Status] = 12 THEN 'FinishedBySystem'
	END
	) as TaskStatusText,
	task.HierarchyBucketId AS HierarchyBucketId,
	taskType.Id AS TaskTypeId,
	taskType.Name AS TaskType
FROM tblCompletedPresentedUploadMediaFactValue uploadMedia
INNER JOIN tblCompletedWorkingDocumentPresentedFact presentedFact ON presentedFact.Id = uploadMedia.CompletedWorkingDocumentPresentedFactId
INNER JOIN tblCompletedWorkingDocumentFact cwdf ON cwdf.WorkingDocumentId = presentedFact.WorkingDocumentId
INNER JOIN tblCompletedWorkingDocumentDimension cwdd ON cwdd.Id = cwdf.ReportingWorkingDocumentId
INNER JOIN tblUserData reviewer ON reviewer.UserId = cwdf.ReportingUserReviewerId
INNER JOIN tblUserType reviewerType ON reviewerType.Id = reviewer.UserTypeId
LEFT JOIN [gce].tblCompany reviewerCompany ON reviewerCompany.Id = reviewer.CompanyId
INNER JOIN tblUserData reviewee ON reviewee.UserId = cwdf.ReportingUserRevieweeId
INNER JOIN tblUserType revieweeType ON revieweeType.Id = reviewee.UserTypeId
LEFT JOIN [gce].tblCompany revieweeCompany ON revieweeCompany.Id = reviewee.CompanyId
LEFT JOIN [gce].tblProjectJobTaskWorkingDocument taskwd ON taskwd.WorkingDocumentId = cwdf.WorkingDocumentId
LEFT JOIN [gce].tblProjectJobTask task on task.Id = taskwd.ProjectJobTaskId
LEFT JOIN [gce].tblProjectJobTaskType taskType on taskType.Id = task.TaskTypeId
WHERE cwdf.IsUserContext = 0
