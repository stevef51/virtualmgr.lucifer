﻿CREATE VIEW [odata].[FloorPlan]
	AS 
SELECT 
	fp.Id,
	fp.ImageURL,
	fp.IndoorAtlasFloorPlanId,
	fp.TopLeftLatitude,
	fp.TopLeftLongitude,
	fp.TopRightLatitude,
	fp.TopRightLongitude,
	fp.BottomLeftLatitude,
	fp.BottomLeftLongitude,
	[floor].Id AS FloorId,
	[floor].[Name] AS FloorName,
	building.Id AS BuildingId,
	building.[Name] AS BuildingName,
	facility.Id AS FacilityId,
	facility.Name AS FacilityName
FROM 
	[asset].tblFloorPlan fp
INNER JOIN
	[asset].tblFacilityStructure [floor]
ON 
	[floor].FloorPlanId = fp.Id
INNER JOIN
	[asset].tblFacilityStructure building
ON
	[floor].ParentId = building.Id
INNER JOIN
	[asset].tblFacilityStructure facility
ON
	building.ParentId = facility.Id


