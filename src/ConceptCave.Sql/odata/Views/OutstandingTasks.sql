﻿CREATE VIEW odata.OutstandingTasks
AS
	SELECT 
		ud.name as SiteName, 
		esw.name AS OwnerName, 
		pjtt.Name AS TaskTypeName,
		cte.*,
		dbo.FnTaskStatusToText(Status) AS StatusText
	FROM 
		tblUserData ud 
		INNER JOIN tblUserType ut ON ud.UserTypeId = ut.id AND ut.IsASite = 1
		CROSS APPLY (SELECT Id FROM gce.tblProjectJobTaskType) taskType
		CROSS APPLY (
			SELECT TOP 1 * FROM (
				SELECT 
					SiteId, 
					Status, 
					Name AS TaskName, 
					Description as TaskDescription,
					RosterName,
					HierarchyId,
					HierarchyBucketId,
					LeftIndex,
					RightIndex,
					TaskTypeId, 
					OwnerId,
					count(*) AS Count, 
					MIN(StatusDate) AS MinStatusDate, 
					MAX(StatusDate) AS MaxStatusDate 
				FROM (
					SELECT 
						ROW_NUMBER () OVER (PARTITION BY t1.TaskTypeId,t1.OwnerId ORDER BY t1.StatusDate,t1.ID DESC) AS [Row],
						ROW_NUMBER () OVER (PARTITION BY t1.TaskTypeId,t1.OwnerId ORDER BY t1.StatusDate,t1.ID DESC) - RANK () OVER (PARTITION BY t1.Status,t1.TaskTypeId,t1.OwnerId ORDER BY t1.StatusDate,t1.ID DESC) AS [Rank],
						t1.ID,
						t1.Status,
						t1.StatusDate,
						t1.SiteId,
						t1.OwnerId,
						t1.TaskTypeId,
						t1.Name,
						t1.Description,
						r.Name as RosterName,
						hb.Id as HierarchyBucketId,
						hb.HierarchyId, 
						hb.LeftIndex, 
						hb.RightIndex
					FROM gce.tblProjectJobTask t1
					INNER JOIN [dbo].[tblHierarchyBucket] hb ON t1.OwnerId = hb.UserId 
					INNER JOIN [dbo].[tblHierarchy] h ON hb.HierarchyId = h.Id
					INNER JOIN [gce].[tblRoster] r ON h.Id = r.HierarchyId
					WHERE
						t1.SiteId = ud.UserId AND
						t1.TaskTypeId = taskType.Id 
				) a GROUP BY Rank, Status, SiteId, TaskTypeId, OwnerId, Name, Description, RosterName, HierarchyId, HierarchyBucketId, LeftIndex, RightIndex
			) b
			ORDER BY b.maxstatusdate DESC
		) cte 
		INNER JOIN tblUserData esw ON cte.OwnerId = esw.UserId
		INNER JOIN gce.tblProjectJobTaskType pjtt ON cte.TaskTypeId = pjtt.Id
	WHERE 		
		STATUS IN (/* FinishedByManagement */ 7, /* FinishedBySystem */ 12)  
