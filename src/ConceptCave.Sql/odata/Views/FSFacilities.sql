﻿CREATE VIEW [odata].[FSFacilities]
	AS SELECT 
		facility.Id,
		facility.Archived,
		facility.StructureType,
		facility.[Name],
		facility.[Description],
		facility.MediaId,
		facility.HierarchyId,
		facility.SortOrder
	FROM [asset].tblFacilityStructure facility
	WHERE
		facility.StructureType = 0
GO

CREATE VIEW [odata].[FSFacilityBuildings]
	AS SELECT
		building.Id,
		building.Archived,
		building.StructureType,
		building.ParentId AS FacilityId,
		building.[Name],
		building.[Description],
		building.MediaId,
		building.HierarchyId,
		building.SortOrder
	FROM [asset].tblFacilityStructure building
	WHERE
		building.StructureType = 1
GO

CREATE VIEW [odata].[FSBuildingFloors]
	AS SELECT
		buildingfloor.Id,
		buildingfloor.Archived,
		buildingfloor.StructureType,
		buildingfloor.ParentId AS BuildingId,
		building.ParentId AS FacilityId,
		buildingfloor.[Name],
		buildingfloor.[Description],
		buildingfloor.FloorHeight,
		buildingfloor.FloorPlanId,
		buildingfloor.[Level],
		buildingfloor.MediaId,
		buildingfloor.HierarchyId,
		buildingfloor.SortOrder
	FROM [asset].tblFacilityStructure buildingfloor
	INNER JOIN [asset].tblFacilityStructure building ON buildingfloor.ParentId = building.Id
	WHERE
		buildingfloor.StructureType = 2
GO


CREATE VIEW [odata].[FSFloorZones]
	AS SELECT
		floorzone.Id,
		floorzone.Archived,
		floorzone.StructureType,
		floorzone.ParentId AS FloorId,
		buildingfloor.ParentId AS BuildingId,
		building.ParentId AS FacilityId,
		floorzone.[Name],
		floorzone.[Description],
		buildingfloor.FloorPlanId,
		buildingfloor.[Level],
		floorzone.MediaId,
		floorzone.HierarchyId,
		floorzone.SortOrder
	FROM [asset].tblFacilityStructure floorzone
	INNER JOIN [asset].tblFacilityStructure buildingfloor ON floorzone.ParentId = buildingfloor.Id
	INNER JOIN [asset].tblFacilityStructure building ON buildingfloor.ParentId = building.Id
	WHERE
		floorzone.StructureType = 3
GO

CREATE VIEW [odata].[FSZoneSites]
	AS SELECT
		zonesite.Id,
		zonesite.Archived,
		zonesite.StructureType,
		zonesite.ParentId AS ZoneId,
		floorzone.ParentId AS FloorId,
		buildingfloor.ParentId AS BuildingId,
		building.ParentId AS FacilityId,
		ISNULL(zonesite.[Name], ud.[Name]) AS [Name],
		zonesite.[Description],
		buildingfloor.FloorPlanId,
		buildingfloor.[Level],
		zonesite.SiteId,
		zonesite.MediaId,
		zonesite.HierarchyId,
		zonesite.SortOrder
	FROM [asset].tblFacilityStructure zonesite
	LEFT OUTER JOIN [dbo].tblUserData ud on zonesite.SiteId = ud.UserId
	INNER JOIN [asset].tblFacilityStructure floorzone ON zonesite.ParentId = floorzone.Id
	INNER JOIN [asset].tblFacilityStructure buildingfloor ON floorzone.ParentId = buildingfloor.Id
	INNER JOIN [asset].tblFacilityStructure building ON buildingfloor.ParentId = building.Id
	WHERE
		zonesite.StructureType = 4
GO

