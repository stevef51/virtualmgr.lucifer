﻿CREATE VIEW [odata].[OrderItems]
	AS SELECT
	i.Id,
	i.OrderId,
	o.FacilityStructureId,
	o.ProductCatalogId AS CatalogId,
	o.OrderedById,
	u.CompanyId AS OrderedByCompanyId,
	c.[Name] AS CatalogName,
	o.ProjectJobTaskId AS TaskId,
	t.HierarchyBucketId,
	i.ProductId,
	p.[Name] AS ProductName,
	p.[Description] AS ProductDescription,
	p.[Code] AS ProductCode,
	p.IsCoupon,
	i.Quantity,
	i.Stock,
	i.MinStockLevel,
	i.Price,
	i.TotalPrice,
	i.Notes,
	i.StartDate,
	i.EndDate,
	o.DateCreated,
	pci.SortOrder,
	o.PendingExpiryDate,
	o.ExtensionOfOrderId,
	extO.Id AS ExtensionOrderId,
	o.CancelledDateUtc,
	o.CancelledDateUtc as CancelledDateLocal,
	o.WorkflowState
FROM [stock].[tblOrderItem] i
	INNER JOIN [stock].tblOrder o on o.Id = i.OrderId
	INNER JOIN [stock].tblProduct p on p.Id = i.ProductId
	INNER JOIN [stock].tblProductCatalog c on c.Id = o.ProductCatalogId
	LEFT JOIN [stock].tblProductCatalogItem pci on o.ProductCatalogId = pci.ProductCatalogId and i.ProductId = pci.ProductId
	INNER JOIN tblUserData u on u.UserId = o.OrderedById
	LEFT JOIN [gce].tblProjectJobTask t on t.Id = o.ProjectJobTaskId
	LEFT JOIN [stock].tblOrder extO ON extO.ExtensionOfOrderId = o.Id
WHERE
	o.Archived = 0
