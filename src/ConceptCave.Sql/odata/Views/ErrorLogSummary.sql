﻿CREATE VIEW [odata].[ErrorLogSummary] AS 
	WITH eli (ErrorLogTypeId, ErrorCount, LastDate, FirstDate) AS (
		SELECT ErrorLogTypeId, COUNT(1), MAX(DateCreated), MIN(DateCreated)
		FROM tblErrorLogItem
		GROUP BY ErrorLogTypeId
	)
	SELECT 
	elt.Id,
	elt.[Type],
	elt.[Source],
	elt.[Message],
	elt.StackTrace,
	eli.ErrorCount,
	eli.LastDate,
	eli.FirstDate
	FROM [tblErrorLogType] elt
	JOIN eli ON eli.ErrorLogTypeId = elt.Id
