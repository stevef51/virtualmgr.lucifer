﻿CREATE VIEW [odata].[WorkingDocuments] 
	AS SELECT 
		wd.Id,
		wd.PublishingGroupResourceId,
		r.[Name] as PublisingGroupResourceName,
		wd.ReviewerId,
		reviewer.[Name] as ReviewerName,
		reviewer.CompanyId as ReviewerCompanyId,
		wd.RevieweeId,
		reviewee.[Name] as RevieweeName,
		reviewee.CompanyId as RevieweeCompanyId,
		wd.[Name],
		wd.DateCreated,
		wd.Lat,
		wd.Long,
		wd.ClientIp,
		wd.UserAgentId,
		ua.UserAgent as UserAgentName,
		wd.[Status],
		(CASE 
			WHEN wd.[Status] = 1 THEN 'Started'
			WHEN wd.[Status] = 2 THEN 'Completed'
			WHEN wd.[Status] = 3 THEN 'PendingCompletion'
			WHEN wd.[Status] = 4 THEN 'Processing'
			WHEN wd.[Status] = 5 THEN 'Failed'
			WHEN wd.[Status] = 6 THEN 'Cancelled'
		END
		) as StatusText,
		wd.ParentId,
		td.HierarchyBucketId
	FROM tblWorkingDocument wd
	INNER JOIN tblPublishingGroupResource r on r.Id = wd.PublishingGroupResourceId
	INNER JOIN tblUserData reviewer on reviewer.UserId = wd.ReviewerId
	LEFT JOIN tblUserData reviewee on reviewee.UserId = wd.RevieweeId
	INNER JOIN tblUserAgent ua on ua.Id = wd.UserAgentId
	LEFT JOIN [gce].tblProjectJobTaskWorkingDocument twd on twd.WorkingDocumentId = wd.Id
	LEFT JOIN [gce].tblProjectJobTask td on td.Id = twd.ProjectJobTaskId