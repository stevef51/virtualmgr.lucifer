﻿CREATE VIEW [odata].[ESSensorReadings] AS 

SELECT 
	sr.Id,
	sr.SensorTimestampUtc,
	sr.ServerTimestampUtc,
	sr.TabletUUID,
	sr.[Value],
	sr.SensorId,
	s.ProbeId,
	s.SensorIndex,
	s.SensorType,
	s.Compliance,
	a.CompanyId,
	a.FacilityStructureId
FROM 
	[es].tblSensorReadings sr INNER JOIN [es].tblSensors s ON sr.SensorId = s.Id
	INNER JOIN [es].tblProbes p ON s.ProbeId = p.Id
	LEFT OUTER JOIN [asset].tblAsset a on a.Id = p.AssetId
WHERE
	p.Archived = 0

