﻿CREATE VIEW [odata].[IncompleteTasks] AS
SELECT 
	t.Id, 
	RosterId, 
	t.ProjectJobTaskGroupId as GroupId,
	r.Name as Roster,
	TaskTypeId, 
	tt.Name as TaskType,
	DateCreated,
	(SELECT TOP 1 DateCreated FROM [gce].tblProjectJobTaskWorkLog WHERE ProjectJobTaskId = t.Id ORDER BY DateCreated asc) as DateStarted,
	DateCompleted,
	(DateDiff(d, DateCompleted, GETUTCDATE())) AS DaysSinceCompletion,
	t.Name,
	t.[Description],
	OwnerId,
	o.Name as [Owner],
	o.CompanyId as OwnerCompanyId,
	OriginalOwnerId,
	oo.Name as [OriginalOwner],
	ScheduledOwnerId,
	so.Name as [ScheduledOwner],
	SiteId,
	s.Name as SiteName,
	SiteLatitude,
	SiteLongitude,
	RoleId,
	rl.Name as [Role],
	[Level],
	CompletionNotes,
	[Status],
	StatusDate,
	StatusId as [CustomStatusId],
	pjs.Text as [CustomStatusText],
	t.ActualDuration,
	t.InactiveDuration,
	t.AllActivitiesEstimatedDuration,
	t.CompletedActivitiesEstimatedDuration,
	t.EstimatedDurationSeconds,
	HierarchyBucketId,
	tfs.ProjectJobFinishedStatusId as FinishedStatusId,
	fs.Text as FinishedStatusText,
	tfs.Notes as FinishedStatusNotes
FROM [gce].[tblProjectJobTask] as t
INNER JOIN [gce].[tblRoster] r on r.Id = t.RosterId
INNER JOIN [gce].[tblProjectJobTaskType] tt on tt.Id = t.TaskTypeId
INNER JOIN [dbo].[tblUserData] o on o.UserId = t.OwnerId
INNER JOIN [dbo].[tblUserData] oo on oo.UserId = t.OriginalOwnerId
LEFT JOIN [dbo].[tblUserData] so on so.UserId = t.ScheduledOwnerId
LEFT JOIN [dbo].[tblUserData] s on s.UserId = t.SiteId
LEFT JOIN [dbo].[tblHierarchyRole] rl on rl.Id = t.RoleId
LEFT JOIN [gce].[tblProjectJobStatus] pjs on pjs.Id = t.StatusId
RIGHT JOIN [gce].tblProjectJobTaskFinishedStatus tfs on tfs.TaskId = t.Id
LEFT JOIN [gce].tblProjectJobFinishedStatus fs on fs.Id = tfs.ProjectJobFinishedStatusId