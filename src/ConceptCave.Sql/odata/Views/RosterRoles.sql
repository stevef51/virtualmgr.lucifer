﻿CREATE VIEW [odata].[RosterRoles]
	AS SELECT 
		r.Id AS RosterId,
		r.Name AS Roster,
		h.Id AS [HierarchyId],
		h.Name AS Hierarchy,
		b.Id AS HierarchyBucketId,
		l.Id AS RoleId,
		l.Name AS [Role],
		b.UserId AS UserId,
		u.Name AS Name,
		u.CompanyId
	FROM [gce].tblRoster r
	INNER JOIN tblHierarchy h on h.Id = r.[HierarchyId]
	INNER JOIN tblHierarchyBucket b on b.[HierarchyId] = h.Id
	LEFT JOIN tblHierarchyRole l on l.Id = b.RoleId
	LEFT JOIN tblUserData u on u.UserId = b.UserId

