﻿CREATE VIEW [odata].[TabletLocationHistory]
	AS 
SELECT 
	tlh.Id,
	tlh.TabletUUID,
	tlh.TabletTimestampUtc,
	tlh.ServerTimestampUtc,
	tlh.GPSLocationId

FROM [asset].[tblTabletLocationHistory] tlh
