﻿CREATE VIEW [odata].[MergedTeamHierarchyWithLabels]
	AS SELECT
		hb.Id,
		hb.LeftIndex,
		hb.RightIndex,
		hb.ParentId,
		hb.[HierarchyId],
		h.Name as HierarchyName,
		hb.RoleId,
		hr.Name as RoleName,
		hb.Name AS BucketName,
		hb.UserId,
		u.Name,
		u.CompanyId,
		r.Id as RosterId,
		r.Name as RosterName,
		hb.IsPrimary,
		hb.TasksCanBeClaimed,
		hb.AssignTasksThroughLabels,
		hb.OpenSecurityThroughLabels,
		u.ExpiryDate AS ExpiryDate,
		u.TimeZone AS TimeZone,
		1 AS PrimaryRecord
	FROM [tblHierarchyBucket] as hb
	INNER JOIN tblHierarchy h on h.Id = hb.HierarchyId
	INNER JOIN [gce].tblRoster r on r.[HierarchyId] = h.Id
	LEFT JOIN tblUserData u on u.UserId = hb.UserId 
	INNER JOIN tblHierarchyRole hr on hr.Id = hb.RoleId
UNION
SELECT
		hb.Id,
		hb.LeftIndex,
		hb.RightIndex,
		hb.ParentId,
		hb.[HierarchyId],
		h.Name as HierarchyName,
		hb.RoleId,
		hr.Name as RoleName,
		hb.Name as BucketName,
		lu.UserId,
		u.Name,
		u.CompanyId,
		r.Id as RosterId,
		r.Name as RosterName,
		hb.IsPrimary,
		hb.TasksCanBeClaimed,
		hb.AssignTasksThroughLabels,
		hb.OpenSecurityThroughLabels,
		u.ExpiryDate AS ExpiryDate,
		u.TimeZone AS TimeZone,
		0 AS PrimaryRecord
FROM tblLabelUser as lu
	RIGHT JOIN [tblHierarchyBucketLabel] hbl on lu.LabelId = hbl.LabelId
	INNER JOIN [tblHierarchyBucket] hb ON hb.Id = hbl.HierarchyBucketId
	INNER JOIN tblHierarchy h on h.Id = hb.HierarchyId
	INNER JOIN [gce].tblRoster r on r.[HierarchyId] = h.Id
	LEFT JOIN tblUserData u on u.UserId = lu.UserId 
	INNER JOIN tblHierarchyRole hr on hr.Id = hb.RoleId
WHERE
		hb.UserId <> lu.UserId