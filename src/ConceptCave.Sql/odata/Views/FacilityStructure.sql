﻿CREATE VIEW [odata].[FacilityStructure]
	AS 
SELECT        
	fs.Id, 
	fs.Archived, 
	fs.StructureType, 
	fs.ParentId, 
	(CASE WHEN fs.SiteId IS NULL THEN fs.Name ELSE ud.Name END) AS Name, 
	fs.Description, 
	fs.[Level], 
	fs.AddressId, 
	fs.SiteId,
	fs.FloorPlanId,
	fs.FloorHeight,
	fs.MediaId,
	fs.SortOrder,
	(CASE WHEN fs.StructureType = 2 THEN fs.Id WHEN fs.StructureType = 3 THEN fsp1.Id WHEN fs.StructureType = 4 THEN fsp2.Id END) AS OnFloorId,
	(CASE WHEN fs.StructureType = 2 THEN fs.FloorPlanId WHEN fs.StructureType = 3 THEN fsp1.FloorPlanId WHEN fs.StructureType = 4 THEN fsp2.FloorPlanId END) AS OnFloorPlanId,

	(CASE WHEN fs.StructureType = 0 THEN fs.Id WHEN fs.StructureType = 1 THEN fsp1.Id WHEN fs.StructureType = 2 THEN fsp2.Id WHEN fs.StructureType = 3 THEN fsp3.Id WHEN fs.StructureType = 4 THEN fsp4.Id ELSE NULL END) AS FSFacilityId,
	(CASE WHEN fs.StructureType = 1 THEN fs.Id WHEN fs.StructureType = 2 THEN fsp1.Id WHEN fs.StructureType = 3 THEN fsp2.Id WHEN fs.StructureType = 4 THEN fsp3.Id ELSE NULL END) AS FSBuildingId,
	(CASE WHEN fs.StructureType = 2 THEN fs.Id WHEN fs.StructureType = 3 THEN fsp1.Id WHEN fs.StructureType = 4 THEN fsp2.Id ELSE NULL END) AS FSFloorId,
	(CASE WHEN fs.StructureType = 3 THEN fs.Id WHEN fs.StructureType = 4 THEN fsp1.Id ELSE NULL END) AS FSZoneId,
	(CASE WHEN fs.StructureType = 4 THEN fs.Id ELSE NULL END) AS FSSiteId

FROM asset.tblFacilityStructure AS fs 
	LEFT OUTER JOIN dbo.tblUserData AS ud ON fs.SiteId = ud.UserId
	LEFT OUTER JOIN asset.tblFacilityStructure fsp1 ON fsp1.Id = fs.ParentId
	LEFT OUTER JOIN asset.tblFacilityStructure fsp2 ON fsp2.Id = fsp1.ParentId
	LEFT OUTER JOIN asset.tblFacilityStructure fsp3 ON fsp3.Id = fsp2.ParentId
	LEFT OUTER JOIN asset.tblFacilityStructure fsp4 ON fsp4.Id = fsp3.ParentId
