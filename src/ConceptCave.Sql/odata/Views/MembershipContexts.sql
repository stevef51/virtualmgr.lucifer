﻿CREATE VIEW [odata].[MembershipContexts]
	AS SELECT 
	presentedFact.Id As Id,
	presentedFact.PresentedId AS PresentedId,
	cwdf.WorkingDocumentId AS WorkingDocumentId,
	cwdd.Name AS Checklist,
	cwdf.DateStarted AS ChecklistDateStarted,
	cwdf.DateCompleted AS ChecklistDateCompleted,
	presentedFact.Prompt AS Prompt,
	(CASE
		WHEN typeDim.[Type] = 'UploadMedia' THEN 
		stuff((SELECT ', ' + CAST(MediaId AS VARCHAR(36)) FROM tblCompletedPresentedUploadMediaFactValue WHERE CompletedWorkingDocumentPresentedFactId = presentedFact.Id for xml path('')),1,2,'')
		ELSE factValue.Value
	 END
	) AS Value,
	typeDim.[Type] AS QuestionType,
	presentedFact.PresentedOrder AS PresentedOrder,
	presentedFact.DateStarted AS QuestionDateStarted,
	presentedFact.DateCompleted AS QuestionDateCompleted,
	presentedFact.PassFail,
	presentedFact.PossibleScore,
	presentedFact.Score,
	reviewer.UserId AS UserId,
	reviewer.Name AS [Name],
	reviewer.UserTypeId as MembershipTypeId,
	reviewerType.Name AS MembershipType,
	reviewerCompany.Id AS CompanyId,
	reviewerCompany.Name AS CompanyName,
	reviewer.ExpiryDate
FROM tblCompletedWorkingDocumentPresentedFact presentedFact
INNER JOIN tblCompletedPresentedFactValue factValue ON factValue.CompletedWorkingDocumentPresentedFactId = presentedFact.Id
INNER JOIN tblCompletedWorkingDocumentFact cwdf ON cwdf.WorkingDocumentId = presentedFact.WorkingDocumentId
INNER JOIN tblCompletedWorkingDocumentDimension cwdd ON cwdd.Id = cwdf.ReportingWorkingDocumentId
INNER JOIN tblCompletedPresentedTypeDimension typeDim ON typeDim.Id = presentedFact.ReportingPresentedTypeId
INNER JOIN tblUserData reviewer ON reviewer.UserId = cwdf.ReportingUserReviewerId
INNER JOIN tblUserType reviewerType ON reviewerType.Id = reviewer.UserTypeId
LEFT JOIN [gce].tblCompany reviewerCompany ON reviewerCompany.Id = reviewer.CompanyId
WHERE presentedFact.IsUserContext = 1
