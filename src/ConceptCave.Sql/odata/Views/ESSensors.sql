﻿CREATE VIEW [odata].[ESSensors] AS 

SELECT 
	s.Id,
	s.ProbeId,
	s.SensorIndex,
	s.SensorType,
	s.Compliance
FROM
	es.tblSensors s

