﻿CREATE VIEW [odata].[ESProbes] AS 

SELECT 
	p.Id,
	p.Archived,
	p.[Key],
	p.Model,
	p.[Name],
	p.SerialNumber,
	p.AssetId,
	a.CompanyId,
	a.FacilityStructureId
FROM
	es.tblProbes p
LEFT OUTER JOIN [asset].tblAsset a on a.Id = p.AssetId

