﻿CREATE VIEW [odata].[Tablets]
	AS 
SELECT 
	tuuid.UUID,
	t.Archived,
	t.[Name],
	t.SiteId,
	siteData.[Name] AS SiteName,
	t.TabletProfileId,
	tp.[Name] AS TabletProfileName,
	tuuid.[Description],
	tuuid.LastContactUtc AS LastLoginUtc,
    JSON_VALUE(tuuid.Description, '$.versionNumber') AS VersionNumber, 
    JSON_VALUE(tuuid.Description, '$.platform') AS Platform ,
    JSON_VALUE(tuuid.Description, '$.model') AS Model,
    JSON_VALUE(tuuid.Description, '$.serial') AS Serial,
    JSON_VALUE(tuuid.Description, '$.manufacturer') AS Manufacturer,
    JSON_VALUE(tuuid.Description, '$.ssid') AS SSID,
	tuuid.LastUserId,
	userData.[Name] AS LastUserName
FROM 
	[asset].tblTabletUUID tuuid
LEFT OUTER JOIN
	[asset].tblTablet t ON t.UUID = tuuid.UUID
LEFT OUTER JOIN
	[asset].tblTabletProfile tp ON t.TabletProfileId = tp.Id
LEFT OUTER JOIN
	dbo.tblUserData siteData ON t.SiteId = siteData.UserId
LEFT OUTER JOIN
	dbo.tblUserData userData ON tuuid.LastUserId = userData.UserId
