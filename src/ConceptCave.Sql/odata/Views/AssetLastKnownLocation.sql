﻿CREATE VIEW [odata].[AssetLastKnownLocation]
	AS SELECT 
		a.Id,
		a.AssetTypeId,
		t.[Name] as AssetTypeName,
		a.DateCreated,
		a.DateDecomissioned,
		a.[Name],
		b.Id as BeaconId,
		b.[Name] as BeaconName,
		b.LastGPSLocationId,
		g.AccuracyMetres as LastLocationAccuracyMetres,
		g.Altitude as LastLocationAltitude,
		g.AltitudeAccuracyMetres as LastLocationAltitudeAccuracyMetres,
		g.FacilityStructureFloorId as LastLocationFacilityStructureFloorId,
		g.[Floor] as LastLocationFloor,
		g.LastTimestampUtc as LastLocationLastTimestampUtc,
		g.Latitude as LastLocationLatitude,
		g.Longitude as LastLocationLongitude,
		g.LocationType as LastLocationLocationType,
		g.TabletUUID as LastLocationTabletUUID,
		g.TaskId as LastLocationTaskId,
		g.TimestampCount as LastLocationTimestampCount,
		g.TimestampUtc as LastLocationTimestampUtc,
		fs.FloorPlanId as OnFloorPlanId
	FROM [asset].tblAsset a
	INNER JOIN [asset].tblBeacon b on b.AssetId = a.Id
	INNER JOIN [asset].tblAssetType t on t.Id = a.AssetTypeId
	INNER JOIN [asset].tblGPSLocation g on g.Id = b.LastGPSLocationId
	INNER JOIN [asset].tblFacilityStructure fs on fs.Id = g.FacilityStructureFloorId
