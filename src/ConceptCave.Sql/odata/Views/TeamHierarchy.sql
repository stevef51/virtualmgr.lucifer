﻿CREATE VIEW [odata].[TeamHierarchy]
	AS SELECT
		hb.Id,
		hb.LeftIndex,
		hb.RightIndex,
		hb.ParentId,
		hb.[HierarchyId],
		h.Name as HierarchyName,
		hb.RoleId,
		hr.Name as RoleName,
		hb.Name AS BucketName,
		hb.UserId,
		u.Name,
		u.CompanyId,
		r.Id as RosterId,
		r.Name as RosterName,
		hb.IsPrimary,
		hb.TasksCanBeClaimed,
		r.ProvidesClaimableDutyList,
		r.ReceivesClaimableDutyList,
		r.SortOrder as RosterSortOrder,
		hb.SortOrder as BucketSortOrder
	FROM [tblHierarchyBucket] as hb
	INNER JOIN tblHierarchy h on h.Id = hb.HierarchyId
	INNER JOIN [gce].tblRoster r on r.[HierarchyId] = h.Id
	LEFT JOIN tblUserData u on u.UserId = hb.UserId 
	INNER JOIN tblHierarchyRole hr on hr.Id = hb.RoleId
    