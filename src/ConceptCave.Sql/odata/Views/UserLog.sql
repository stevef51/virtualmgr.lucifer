﻿CREATE VIEW [odata].[UserLog]
AS 
SELECT 
	ul.Id,
	ul.UserId,
	ul.LogInTime,
	ul.LogOutTime,
	ul.Latitude,
	ul.Longitude,
	ul.TabletUUID,
	ul.NearestLocation,
	ul.LoginMediaId

FROM dbo.tblUserLog ul
