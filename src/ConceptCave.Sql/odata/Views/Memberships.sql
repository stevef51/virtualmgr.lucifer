﻿CREATE VIEW [odata].[Memberships]
	AS SELECT d.UserId, 
		t.Name as UserType,
		u.UserName, 
		d.Name, 
		m.Email,
		d.CompanyId,
		c.Name as CompanyName, 
		d.Latitude,
		d.Longitude,
		d.TimeZone,
		d.ExpiryDate,
		u.LastActivityDate, 
		m.CreateDate,
		m.LastLoginDate,
		m.LastLockoutDate,
		m.LastPasswordChangedDate,
		m.FailedPasswordAttemptCount,
		m.FailedPasswordAttemptWindowStart,
		m.IsLockedOut, 
		t.IsASite,
		d.MediaId,
		d.UserTypeId,
		zonesites.Id AS FSSiteId,
		d.DefaultLanguageCode
		FROM [dbo].[tblUserData] AS d
	INNER JOIN [dbo].Memberships AS m ON m.UserId = d.UserId
	INNER JOIN [dbo].[Users] AS u ON u.UserId = d.UserId
	INNER JOIN [dbo].[tblUserType] AS t ON t.Id = d.UserTypeId
	LEFT JOIN  [gce].[tblCompany] AS c ON c.Id = d.CompanyId
	LEFT JOIN [odata].FSZoneSites zonesites ON d.UserId = zonesites.SiteId AND zonesites.Archived = 0
