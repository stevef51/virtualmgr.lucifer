﻿CREATE VIEW [odata].[TaskTypes]
	AS SELECT 
		tt.[Id],
		tt.[Name],
		tt.[UserInterfaceType],
		tt.[MediaId],
		tt.[DefaultLengthInDays],
		tt.[AutoStart1stChecklist],
		tt.[PhotoSupport],
		tt.[AutoFinishAfterLastChecklist],
		tt.[EstimatedDurationSeconds],
		tt.[ProductCatalogs],
		tt.[MinimumDuration],
		tt.ExcludeFromTimesheets,
		tt.AutoFinishAfterStart,
		(SELECT COUNT(1) FROM gce.tblProjectJobTaskTypePublishingGroupResource pgr WHERE pgr.TaskTypeId = tt.Id) AS ChecklistCount 
	FROM [gce].[tblProjectJobTaskType] tt
