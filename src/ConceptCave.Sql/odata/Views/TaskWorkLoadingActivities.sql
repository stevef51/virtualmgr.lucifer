﻿CREATE VIEW [odata].[TaskWorkLoadingActivities]
	AS SELECT 
	w.WorkLoadingStandardId as WorkLoadingStandardId,
	w.SiteFeatureId as SiteFeatureId,
	w.ProjectJobTaskId as TaskId,
	t.Name as TaskName,
	t.OwnerId,
	u.Name as [Owner],
	u.CompanyId as [OwnerCompanyId],
	s.Name as SiteName,
	sa.Name as AreaName,
	wlf.Name as Feature,
	wla.Name as Activity,
	wlb.Name as Book,
	w.EstimatedDuration,
	w.Completed,
	w.[Text],
	t.HierarchyBucketId
	FROM [wl].tblProjectJobTaskWorkloadingActivity as w
	INNER JOIN [gce].tblProjectJobTask t on t.Id = w.ProjectJobTaskId
	INNER JOIN [dbo].tblUserData u on u.UserId = t.OwnerId
	INNER JOIN [wl].tblSiteFeature sf on sf.Id = w.SiteFeatureId
	INNER JOIN [wl].tblSiteArea sa on sa.Id = sf.SiteAreaId
	INNER JOIN [wl].tblWorkLoadingStandard wls on wls.Id = w.WorkLoadingStandardId
	INNER JOIN [wl].tblWorkLoadingFeature wlf on wlf.Id = sf.WorkLoadingFeatureId
	INNER JOIN [wl].tblWorkLoadingBook wlb on wlb.Id = wls.BookId
	INNER JOIN [wl].tblWorkLoadingActivity wla on wla.Id = wls.ActivityId
	LEFT OUTER JOIN [dbo].tblUserData s on s.UserId = t.SiteId