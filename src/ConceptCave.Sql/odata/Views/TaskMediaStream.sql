﻿CREATE VIEW [odata].[TaskMediaStream]
	AS SELECT
	u.UserId AS OwnerId,
	u.Name AS [Owner],
	u.CompanyId AS OwnerCompanyId,
	s.UserId AS SiteId,
	s.Name AS [SiteName],
	tm.MediaId,
	task.Id AS TaskId,
	task.Name AS TaskName,
	task.[Description] AS TaskDescription,
	task.DateCreated AS TaskDateCreated,
	task.DateCompleted AS TaskDateCompleted,
	task.[Level] AS TaskLevel,
	task.StatusDate AS TaskStatusDate,
	task.CompletionNotes AS TaskCompletionNotes,
	(CASE 
		WHEN [Status] = 0 THEN 'Unapproved'
		WHEN [Status] = 1 THEN 'Approved'
		WHEN [Status] = 2 THEN 'Started'
		WHEN [Status] = 3 THEN 'Paused'
		WHEN [Status] = 4 THEN 'FinishedComplete'
		WHEN [Status] = 5 THEN 'FinishedIncomplete'
		WHEN [Status] = 6 THEN 'Cancelled'
		WHEN [Status] = 7 THEN 'FinishedByManagement'
		WHEN [Status] = 8 THEN 'ApprovedContinued'
		WHEN [Status] = 9 THEN 'ChangeRosterRequested'
		WHEN [Status] = 10 THEN 'ChangeRosterRejected'
		WHEN [Status] = 11 THEN 'ChangeRosterAccepted'
		WHEN [Status] = 12 THEN 'FinishedBySystem'
	END
	) as TaskStatusText,
	task.HierarchyBucketId AS HierarchyBucketId,
	taskType.Id AS TaskTypeId,
	taskType.Name AS TaskType
	FROM [gce].tblProjectJobTaskMedia tm
	INNER JOIN [gce].tblProjectJobTask task on task.Id = tm.TaskId
	INNER JOIN [gce].tblProjectJobTaskType taskType on taskType.Id = task.TaskTypeId
	INNER JOIN tblUserData u on u.UserId = task.OwnerId
	INNER JOIN tblUserData s on s.UserId = task.SiteId
