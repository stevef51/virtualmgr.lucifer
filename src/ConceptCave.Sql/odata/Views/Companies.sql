﻿CREATE VIEW [odata].[Companies]
	AS SELECT 
		c.Id,
		c.[Name],
		c.Latitude,
		c.Longitude,
		c.Postcode,
		c.Rating,
		c.Street,
		c.Town,
		s.Id as StateId,
		s.[Name] as StateName,
		s.Abbreviation as StateAbbreviation,
		s.TimeZone as StateTimeZone
	FROM [gce].tblCompany c
	LEFT JOIN [gce].tblCountryState s on s.Id = c.CountryStateId
