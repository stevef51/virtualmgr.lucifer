﻿CREATE VIEW [odata].[TaskTypeFinishedStatus]
	AS SELECT 
		ttfs.Id,
		ttfs.ProjectJobFinishedStatusId,
		ttfs.TaskTypeId
	FROM 
		gce.tblProjectJobTaskTypeFinishedStatus ttfs
