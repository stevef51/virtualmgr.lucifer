﻿CREATE VIEW [odata].[MediaPageViews]
	AS SELECT 
		p.Id,
		p.MediaViewingId,
		p.[Page],
		p.StartDate,
		p.EndDate,
		p.TotalSeconds,
		v.MediaId,
		d.Name AS Document,
		u.Name AS Name,
		m.UserName,
		u.UserTypeId,
		t.Name AS UserType
	FROM [dbo].[tblMediaPageViewing] AS p
	INNER JOIN [dbo].[tblMediaViewing] AS v ON v.Id = p.MediaViewingId
	INNER JOIN [dbo].[tblMedia] AS d ON d.Id = v.MediaId
	INNER JOIN [dbo].[tblUserData] AS u ON u.UserId = v.UserId
	INNER JOIN [dbo].[Users] AS m ON m.UserId = u.UserId
	INNER JOIN [dbo].[tblUserType] AS t ON t.Id = u.UserTypeId
