﻿CREATE VIEW [odata].[MembershipMedia]
	AS SELECT 
		u.UserId,
		u.Name,
		m.Id AS MediaId,
		m.Name AS Document,
		m.WorkingDocumentId,
		um.DateCreated,
		u.CompanyId
	FROM [tblUserData] u 
	INNER JOIN tblUserMedia um on um.UserId = u.UserId
	INNER JOIN tblMedia m on m.Id = um.MediaId
