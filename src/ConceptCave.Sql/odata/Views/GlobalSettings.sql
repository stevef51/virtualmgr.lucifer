﻿CREATE VIEW [odata].[GlobalSettings]
As

	SELECT       
		[Id], 
		RTRIM([Name]) AS [Name], 
		[Value]
	FROM
		dbo.tblGlobalSetting
	WHERE
		[Hidden] = 0
GO