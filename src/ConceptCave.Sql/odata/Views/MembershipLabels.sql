﻿CREATE VIEW [odata].[MembershipLabels]
AS 
	SELECT 
		ROW_NUMBER() OVER (ORDER BY lu.UserId, lu.LabelId) AS Row,
		ud.UserId, 
		l.* 
	FROM 
		dbo.tblUserData ud 
		INNER JOIN dbo.tblLabelUser lu ON lu.UserId = ud.UserId
		INNER JOIN dbo.tblLabel l ON l.Id = lu.LabelId
