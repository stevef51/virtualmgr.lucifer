﻿CREATE VIEW [odata].[Media]
	AS SELECT 
		m.Id, 
		m.Name,
		m.[Description],
		m.[Filename],
		m.[Type],
		u.Name AS CreatedBy,
		m.DateCreated,
		m.[Version],
		m.[DateVersionCreated],
		m.UseVersioning,
		m.PreviewCount AS [PageCount],
		mf.Id AS FolderId,
		mf.Text AS Folder
	FROM [dbo].[tblMedia] as m
		LEFT JOIN [dbo].[tblUserData] as u on u.UserId = m.CreatedBy
		INNER JOIN [dbo].tblMediaFolderFile as f on f.MediaId = m.Id
		INNER JOIN [dbo].tblMediaFolder as mf on mf.Id = f.MediaFolderId

