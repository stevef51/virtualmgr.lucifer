﻿CREATE VIEW [odata].[OrderMedia]
AS SELECT 
	om.Id,
	om.OrderId,
	om.MediaId,
	om.DateCreated,
	om.Tag,
	m.Name AS MediaName,
	m.Filename AS MediaFilename,
	m.WorkingDocumentId,
	m.Type AS MediaType
FROM [stock].tblOrderMedia om 
INNER JOIN [dbo].tblMedia m ON om.MediaId = m.Id
INNER JOIN [stock].tblOrder o ON om.OrderId = o.Id
WHERE
	o.Archived = 0