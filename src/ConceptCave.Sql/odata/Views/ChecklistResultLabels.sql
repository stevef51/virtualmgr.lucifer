﻿CREATE VIEW [odata].[ChecklistResultLabels]
	AS SELECT
	CompletedWorkingDocumentPresentedFactId as Id,
	CompletedLabelDimensionId as LabelId,
	l.Name,
	l.Backcolor,
	l.Forecolor
	FROM [tblCompletedLabelWorkingDocumentPresentedDimension] d
	INNER JOIN tblCompletedLabelDimension l on l.Id = d.CompletedLabelDimensionId