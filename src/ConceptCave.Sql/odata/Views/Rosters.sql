﻿CREATE VIEW [odata].[Rosters] 
	AS SELECT 
		r.Id,
		r.Name,
		r.StartTime,
		r.EndTime,
		r.Timezone,
		r.AutomaticScheduleApprovalTime,
		r.AutomaticEndOfDayTime,
		r.ScheduleApprovalDefaultTaskStatus,
		r.[HierarchyId],
		h.Name as HierarchyName,
		r.ProvidesClaimableDutyList,
		r.ReceivesClaimableDutyList,
		r.SortOrder
	FROM [gce].[tblRoster] as r
	INNER JOIN tblHierarchy h on h.Id = r.[HierarchyId]
