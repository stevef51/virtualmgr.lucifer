﻿CREATE VIEW [odata].[AssetLocationHistory]
	AS 
SELECT
	sighting.Id AS Id,
	asset.Id AS AssetId,
	beacon.Id AS BeaconId,
	asset.[Name] AS AssetName,
	beacon.[Name] AS BeaconName,
	assetType.Id As AssetTypeId,
	assetType.Name As AssetTypeName,
	sighting.GPSLocationId,
	gpsl.Altitude AS StaticBeaconAltitude,
	gpsl.Latitude AS StaticBeaconLatitude,
	gpsl.Longitude AS StaticBeaconLongitude,
	gpsl.[Floor] AS StaticBeaconFloor,
	gpsl.FacilityStructureFloorId AS StaticBeaconFloorId,
	staticBeaconFloor.FloorPlanId AS StaticBeaconFloorPlanId,
	staticBeacon.Id AS StaticBeaconId,
	staticBeacon.[Name] AS StaticBeaconName,
	sighting.FirstRangeUtc,
	sighting.BestRangeUtc,
	sighting.GoneUtc,
	sighting.BestRange,
	sighting.TabletUUID,
	sighting.PrevStaticBeaconSightingId,
	prevStaticBeacon.Id AS PrevStaticBeaconId,
	prevStaticBeacon.[Name] AS PrevStaticBeaconName,
	prevStaticBeaconSighting.BestRangeUtc AS PrevStaticBeaconRangeUtc,
	sighting.NextStaticBeaconSightingId,
	nextStaticBeacon.Id AS NextStaticBeaconId,
	nextStaticBeacon.[Name] AS NextStaticBeaconName,
	nextStaticBeaconSighting.BestRangeUtc AS NextStaticBeaconRangeUtc,
	sighting.BatteryPercent,
	sighting.LoggedInUserId,
	loggedInUser.[Name] AS LoggedInUserName,
	tablet.[Name] AS TabletName
FROM asset.tblBeaconSighting sighting
INNER JOIN asset.tblBeacon beacon ON sighting.BeaconId = beacon.Id
INNER JOIN asset.tblAsset asset ON beacon.AssetId = asset.Id
INNER JOIN asset.tblAssetType assetType ON asset.AssetTypeId = assetType.Id
LEFT JOIN asset.tblGPSLocation gpsl ON sighting.GPSLocationId = gpsl.Id
LEFT JOIN asset.tblBeacon staticBeacon ON staticBeacon.StaticLocationId = gpsl.Id
LEFT JOIN asset.tblFacilityStructure staticBeaconFloor ON gpsl.FacilityStructureFloorId = staticBeaconFloor.Id
LEFT JOIN asset.tblBeaconSighting prevStaticBeaconSighting ON sighting.PrevStaticBeaconSightingId = prevStaticBeaconSighting.Id
LEFT JOIN asset.tblBeacon prevStaticBeacon ON prevStaticBeaconSighting.BeaconId = prevStaticBeacon.Id
LEFT JOIN asset.tblBeaconSighting nextStaticBeaconSighting ON sighting.NextStaticBeaconSightingId = nextStaticBeaconSighting.Id
LEFT JOIN asset.tblBeacon nextStaticBeacon ON nextStaticBeaconSighting.BeaconId = nextStaticBeacon.Id
LEFT JOIN dbo.tblUserData loggedInUser ON sighting.LoggedInUserId = loggedInUser.UserId
LEFT JOIN asset.tblTabletUUID tabletUUID ON sighting.TabletUUID = tabletUUID.UUID
LEFT JOIN asset.tblTablet tablet ON tabletUUID.TabletId = tablet.Id
