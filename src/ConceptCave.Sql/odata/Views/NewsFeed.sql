﻿CREATE VIEW [odata].[NewsFeed]
	AS SELECT 
		n.Id,
		n.CreatedBy AS CreatedByUserId,
		u.[Name] AS CreatedByName,
		u.CompanyId AS CreatedByUserCompanyId,
		n.Channel,
		n.DateCreated,
		n.Headline,
		n.Program,
		n.Title,
		n.ValidTo
	FROM tblNewsFeed n
	INNER JOIN tblUserData u ON u.UserId = n.CreatedBy