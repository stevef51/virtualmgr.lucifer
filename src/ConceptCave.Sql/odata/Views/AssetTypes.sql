﻿CREATE VIEW [odata].[AssetTypes]
	AS SELECT 
		at.Id,
		at.Name,
		at.CanTrack,
		at.Forecolor,
		at.Backcolor,
		at.Archived
	FROM [asset].[tblAssetType] at
