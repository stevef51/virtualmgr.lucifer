﻿CREATE VIEW [odata].[ErrorLogDetail]
	AS SELECT eli.Id,
	eli.UserId,
	u.Name,
	DateCreated,
	UserAgentId,
	a.UserAgent,
	eli.ErrorLogTypeId,
	elt.[Type],
	elt.[Source],
	elt.[Message],
	elt.StackTrace,
	eli.[Data]
	FROM [tblErrorLogItem] eli
	INNER JOIN tblErrorLogType elt ON eli.ErrorLogTypeId = elt.Id
	INNER JOIN tblUserData u ON u.UserId = eli.UserId
	INNER JOIN tblUserAgent a ON a.Id = eli.UserAgentId
