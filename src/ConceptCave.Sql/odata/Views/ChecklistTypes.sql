﻿CREATE VIEW odata.ChecklistTypes AS
SELECT 
	pgr.Id AS Id,
	pgr.Name AS [Name],
	pgr.Description AS [Description],
	pgr.PublishingGroupId AS GroupId,
	pg.Name AS [Group],
	pg.Description AS GroupDescription,
	pgr.EstimatedPageCount
FROM 
	tblPublishingGroupResource pgr 
	INNER JOIN tblPublishingGroup pg ON pgr.PublishingGroupId = pg.Id

