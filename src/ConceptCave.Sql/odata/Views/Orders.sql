﻿CREATE VIEW [odata].[Orders]
	AS SELECT 
		o.Id,
		o.ProductCatalogId AS CatalogId,
		p.[Name] AS [Catalog],
		o.OrderedById,
		u.[Name] AS OrderedBy,
		u.CompanyId AS OrderedByCompanyId,
		o.FacilityStructureId,
		f.[Name] AS FacilityStructure,
		o.DateCreated,
		o.ExpectedDeliveryDate,
		o.ProjectJobTaskId AS TaskId,
		t.[Name] AS TaskName,
		t.TaskTypeId,
		t.HierarchyBucketId,
		tt.[Name],
		o.TotalPrice,
		o.PendingExpiryDate,
		o.ExtensionOfOrderId,
		extO.Id AS ExtensionOrderId,
		o.CancelledDateUtc,
		o.CancelledDateUtc as CancelledDateLocal,
		o.WorkflowState
FROM [stock].tblOrder o
	INNER JOIN tblUserData u on u.UserId = o.OrderedById
	INNER JOIN [stock].tblProductCatalog p on p.Id = o.ProductCatalogId
	INNER JOIN [asset].tblFacilityStructure f on f.Id = o.FacilityStructureId
	LEFT JOIN [gce].tblProjectJobTask t on t.Id = o.ProjectJobTaskId
	LEFT JOIN [gce].tblProjectJobTaskType tt on tt.Id = t.TaskTypeId
	LEFT JOIN [stock].tblOrder extO ON extO.ExtensionOfOrderId = o.Id
WHERE 
	o.Archived = 0