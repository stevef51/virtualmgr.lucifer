﻿CREATE TABLE [mtc].[tblCurrency]
(
	[Code] NVARCHAR(10) NOT NULL PRIMARY KEY, 
    [Description] NVARCHAR(128) NOT NULL
)
