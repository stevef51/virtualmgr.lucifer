﻿CREATE TABLE [mtc].[tblTenantProfiles]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
	[Name] NVARCHAR(255) NOT NULL,
	[Description] NVARCHAR(1000) NOT NULL DEFAULT '',
    [CanDelete] BIT NOT NULL DEFAULT 0,
    [EmailEnabled] BIT NOT NULL, 
    [EmailWhitelist] NVARCHAR(1000) NOT NULL,
	[CanClone] BIT NOT NULL DEFAULT 1, 
    [Hidden] BIT NOT NULL DEFAULT 0, 
    [CreateAlertMessage] NVARCHAR(1000) NULL,
	[CanUpdateSchema] BIT NOT NULL DEFAULT 1,
	[CanDisable] BIT NOT NULL DEFAULT 1, 
    [DomainAppend] NVARCHAR(20) NULL, 
    [DatabaseAppend] NVARCHAR(20) NULL,

)
