﻿CREATE TABLE [mtc].[tblTenantHosts]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY,
	[Name] NVARCHAR(255) NOT NULL,
	[Location] NVARCHAR(255) NOT NULL, 
    [SqlServerName] NVARCHAR(255) NOT NULL, 
	[ConnectionStringTemplate] NVARCHAR(500) NOT NULL,
    [WebAppName] NVARCHAR(255) NOT NULL, 
    [WebAppResourceGroup] NVARCHAR(255) NOT NULL,
	[TrafficManagerName] NVARCHAR(255) NULL, 
    [BackupWebAppName] NVARCHAR(255) NULL
)
