﻿CREATE TABLE [mtc].[tblTenants]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [HostName] NVARCHAR(255) NULL, 
    [ConnectionString] NVARCHAR(1024) NULL, 
    [Name] NVARCHAR(255) NOT NULL, 
    [Enabled] BIT NOT NULL DEFAULT (1), 
    [TenantHostId] INT NULL, 
    [TenantProfileId] INT NULL, 
    [InvoiceIdPrefix] NVARCHAR(16) NULL, 
    [TimeZone]                 NVARCHAR (50)    CONSTRAINT [DF_tblTenants_TimeZone] DEFAULT (N'UTC') NOT NULL,
    [BillingAddress] NVARCHAR(1024) NULL, 
    [BillingCompanyName] NVARCHAR(1024) NULL, 
    [Currency] NVARCHAR(10) NOT NULL DEFAULT N'AUD', 
	[EmailFromAddress] NVARCHAR(255) NOT NULL DEFAULT N'',
    [TargetProfileId] INT NULL, 
    [EnableDatawareHousePush] BIT NOT NULL DEFAULT 0, 
	[LuciferRelease] NVARCHAR(255) NOT NULL DEFAULT N'latest',
    CONSTRAINT [FK_tblTenants_ToTenantHosts] FOREIGN KEY ([TenantHostId]) REFERENCES [mtc].[tblTenantHosts]([Id]),
    CONSTRAINT [FK_tblTenants_ToTenantProfiles] FOREIGN KEY ([TenantProfileId]) REFERENCES [mtc].[tblTenantProfiles]([Id]),
    CONSTRAINT [FK_tblTenants_ToTargetTenantProfiles] FOREIGN KEY ([TargetProfileId]) REFERENCES [mtc].[tblTenantProfiles]([Id])
)

GO

CREATE UNIQUE INDEX [IX_tblTenants_HostName] ON [mtc].[tblTenants] ([HostName])
