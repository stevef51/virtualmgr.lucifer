﻿CREATE TYPE [dw].[Dim_FacilityRecord] AS TABLE
(
	PkId VARCHAR(50) NOT NULL,
	TenantId INT,
	Id INT,
	[Name] NVARCHAR(255) NULL,
	[Description] NVARCHAR(1024) NULL
)
GO

CREATE PROCEDURE [dw].[GetRecords_Dim_Facilities]
	@tracking ROWVERSION
AS
	SELECT MAX(Tracking) AS Tracking FROM [dw].[Dim_Facilities] 
	SELECT PkId, TenantId, Id, [Name], [Description] FROM [dw].[Dim_Facilities] WHERE Tracking > ISNULL(@tracking, 0)
GO

CREATE PROCEDURE [dw].[Merge_Dim_Facilities]
	@records [dw].[Dim_FacilityRecord] READONLY
AS
	MERGE [dw].[Dim_Facilities] AS Target USING 
	(
		SELECT PkId, TenantId, Id, [Name], [Description] FROM @records 
	) 
	AS Source ON (Target.PkId = Source.PkId)
	WHEN MATCHED AND Target.[Name] != Source.[Name] OR Target.[Description] != Source.[Description] THEN
		UPDATE SET Target.[Name] = Source.[Name], Target.[Description] = Source.[Description]
	WHEN NOT MATCHED BY TARGET THEN 
		INSERT (PkId, TenantId, Id, [Name], [Description]) VALUES (Source.PkId, Source.TenantId, Source.Id, Source.[Name], Source.[Description]);

	SELECT @@ROWCOUNT AS AffectedCount
GO

CREATE PROCEDURE [dw].[Populate_Dim_Facilities]
	@tenantId AS int
AS
	DECLARE @records AS [dw].[Dim_FacilityRecord]

	INSERT INTO @records 
		SELECT 
			dw.MakePkIdINT(@tenantId, Id),
			@tenantId, 
			Id, 
			[Name], 
			[Description] 
		FROM asset.tblFacilityStructure WHERE StructureType = 0

	EXEC [dw].[Merge_Dim_Facilities] @records

RETURN 0

