﻿CREATE TYPE [dw].[Dim_TaskCustomStatusRecord] AS TABLE
(
	PkId VARCHAR(50) NOT NULL,
	TenantId INT,
	Id UNIQUEIDENTIFIER,
	[Text] NVARCHAR(50),
	[Description] NVARCHAR(MAX)
)
GO

CREATE PROCEDURE [dw].[GetRecords_Dim_TaskCustomStatus]
	@tracking ROWVERSION
AS
	SELECT MAX(Tracking) AS Tracking FROM [dw].[Dim_TaskCustomStatus] 
	SELECT PkId, TenantId, Id, [Text], [Description] FROM [dw].[Dim_TaskCustomStatus] WHERE Tracking > ISNULL(@tracking, 0)
GO

CREATE PROCEDURE [dw].[Merge_Dim_TaskCustomStatus]
	@records [dw].[Dim_TaskCustomStatusRecord] READONLY
AS
	MERGE [dw].[Dim_TaskCustomStatus] AS Target USING 
	(
		SELECT PkId, TenantId, Id, [Text], [Description] FROM @records 
	) 
	AS Source ON (Target.PkId = Source.PkId)
	WHEN MATCHED AND Target.[Text] != Source.[Text] OR Target.[Description] != Source.[Description] THEN
		UPDATE SET Target.[Text] = Source.[Text], Target.[Description] = Source.[Description]
	WHEN NOT MATCHED BY TARGET THEN 
		INSERT (PkId, TenantId, Id, [Text], [Description]) VALUES (Source.PkId, Source.TenantId, Source.Id, Source.[Text], Source.[Description]);
	
	SELECT @@ROWCOUNT AS AffectedCount
GO

CREATE PROCEDURE [dw].[Populate_Dim_TaskCustomStatus]
	@tenantId AS int
AS
	DECLARE @records AS [dw].[Dim_TaskCustomStatusRecord]

	INSERT INTO @records 
		SELECT 
			dw.MakePkIdGUID(@tenantId, Id),
			@tenantId, 
			Id, 
			[Text], 
			[Description] 
		FROM [gce].[tblProjectJobStatus] 

	DECLARE @trackingBefore ROWVERSION
	SELECT @trackingBefore = MAX(Tracking) FROM [dw].[Dim_TaskCustomStatus]

	EXEC [dw].[Merge_Dim_TaskCustomStatus] @records

	-- Return the trackingBefore so we can record "where we are upto"
	SELECT @trackingBefore AS Tracking

RETURN 0

