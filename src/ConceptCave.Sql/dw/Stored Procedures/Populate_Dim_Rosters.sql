﻿CREATE TYPE [dw].[Dim_RosterRecord] AS TABLE
(
	PkId VARCHAR(50) NOT NULL,
	TenantId INT,
	Id INT,
	[Name] NVARCHAR(50)
)
GO

CREATE PROCEDURE [dw].[GetRecords_Dim_Rosters]
	@tracking ROWVERSION
AS
	SELECT MAX(Tracking) AS Tracking FROM [dw].[Dim_Rosters] 
	SELECT PkId, TenantId, Id, [Name] FROM [dw].[Dim_Rosters] WHERE Tracking > ISNULL(@tracking, 0)
GO

CREATE PROCEDURE [dw].[Merge_Dim_Rosters]
	@records [dw].[Dim_RosterRecord] READONLY
AS
	MERGE [dw].[Dim_Rosters] AS Target USING 
	(
		SELECT PkId, TenantId, Id, [Name] FROM @records 
	) 
	AS Source ON (Target.PkID = Source.PkId)
	WHEN MATCHED AND Target.[Name] != Source.[Name] THEN
		UPDATE SET Target.[Name] = Source.[Name] 
	WHEN NOT MATCHED BY TARGET THEN 
		INSERT (PkId, TenantId, Id, [Name]) VALUES (Source.PkId, Source.TenantId, Source.Id, Source.[Name]);

	SELECT @@ROWCOUNT AS AffectedCount
GO

CREATE PROCEDURE [dw].[Populate_Dim_Rosters]
	@tenantId AS int
AS
	DECLARE @records AS [dw].[Dim_RosterRecord]

	INSERT INTO @records 
		SELECT 
			dw.MakePkIdINT(@tenantId, Id),
			@tenantId, 
			Id, 
			[Name] 
		FROM [gce].[tblRoster]

	EXEC [dw].[Merge_Dim_Rosters] @records

RETURN 0
