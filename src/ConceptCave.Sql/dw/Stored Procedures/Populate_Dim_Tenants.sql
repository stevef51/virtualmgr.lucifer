﻿CREATE TYPE [dw].[Dim_TenantRecord] AS TABLE
(
	[Id] INT NOT NULL,
	[Name] NVARCHAR(255) NOT NULL
);
GO

CREATE PROCEDURE [dw].[GetRecords_Dim_Tenants]
	@tracking ROWVERSION
AS
	SELECT MAX(Tracking) AS Tracking FROM [dw].[Dim_Tenants] 
	SELECT Id, [Name] FROM [dw].[Dim_Tenants] WHERE Tracking > ISNULL(@tracking, 0)
GO

CREATE PROCEDURE [dw].[Merge_Dim_Tenants]
	@records [dw].[Dim_TenantRecord] READONLY
AS
	MERGE [dw].[Dim_Tenants] AS Target USING 
	(
		SELECT Id, [Name] FROM @records 
	) 
	AS Source ON (Target.Id = Source.Id)
	WHEN MATCHED AND Target.[Name] != Source.[Name] THEN
		UPDATE SET Target.[Name] = Source.[Name] 
	WHEN NOT MATCHED BY TARGET THEN 
		INSERT (Id, [Name]) VALUES (Source.Id, Source.[Name]);

	SELECT @@ROWCOUNT AS AffectedCount
GO

CREATE PROCEDURE [dw].[Populate_Dim_Tenants]
	@tenantId AS int
AS
	DECLARE @records AS [dw].[Dim_TenantRecord]

	INSERT INTO @records SELECT Id, [Name] FROM [mtc].[tblTenants] WHERE EnableDatawareHousePush = 1

	EXEC [dw].[Merge_Dim_Tenants] @records

RETURN 0
