﻿CREATE TYPE [dw].[Dim_UserRecord] AS TABLE
(
	PkId VARCHAR(50) NOT NULL,
	TenantId INT,
	UserId UNIQUEIDENTIFIER,
	UserFullName NVARCHAR(200)
)
GO

CREATE PROCEDURE [dw].[GetRecords_Dim_Users]
	@tracking ROWVERSION
AS
	SELECT MAX(Tracking) AS Tracking FROM [dw].[Dim_Users] 
	SELECT PkId, TenantId, UserId, UserFullName FROM [dw].[Dim_Users] WHERE Tracking > ISNULL(@tracking, 0)
GO

CREATE PROCEDURE [dw].[Merge_Dim_Users]
	@records [dw].[Dim_UserRecord] READONLY
AS
	MERGE [dw].[Dim_Users] AS Target USING 
	(
		SELECT PkId, TenantId, UserId, UserFullName FROM @records 
	) 
	AS Source ON (Target.PkId = Source.PkId)
	WHEN MATCHED AND Target.UserFullName != Source.UserFullName THEN
		UPDATE SET Target.UserFullName = Source.UserFullName
	WHEN NOT MATCHED BY TARGET THEN 
		INSERT (PkId, TenantId, UserId, UserFullName) VALUES (Source.PkId, Source.TenantId, Source.UserId, Source.UserFullName);

	SELECT @@ROWCOUNT AS AffectedCount
GO

CREATE PROCEDURE [dw].[Populate_Dim_Users]
	@tenantId AS int
AS
	DECLARE @records AS [dw].[Dim_UserRecord]

	INSERT INTO @records 
		SELECT 
			dw.MakePkIdGUID(@tenantId, UserId),
			@tenantId, 
			UserId, 
			ud.[Name] 
		FROM tblUserData ud inner join tblUserType ut on ut.Id = ud.UserTypeId where ut.IsASite = 0

	DECLARE @trackingBefore ROWVERSION
	SELECT @trackingBefore = MAX(Tracking) FROM [dw].[Dim_Users]

	EXEC [dw].[Merge_Dim_Users] @records

	-- Return the trackingBefore so we can record "where we are upto"
	SELECT @trackingBefore AS Tracking

RETURN 0
