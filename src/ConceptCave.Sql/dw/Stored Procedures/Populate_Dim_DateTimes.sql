﻿CREATE TYPE [dw].[Dim_DateTimeRecord] AS TABLE
(
	[Date] DATETIME NOT NULL, 
    [Year] INT NOT NULL, 
    [MonthOfYear] INT NOT NULL, 
    [QuarterOfYear] INT NOT NULL, 
    [DayOfYear] INT NOT NULL, 
    [DayOfWeek] INT NOT NULL, 
    [ISOWeekOfYear] INT NOT NULL, 
    [DayOfMonth] INT NOT NULL, 
    [Month] NVARCHAR(50) NOT NULL, 
    [Quarter] NVARCHAR(50) NOT NULL, 
    [WeekEnding] DATETIME NOT NULL, 
    [Day] NVARCHAR(9) NOT NULL, 
    [HourOfDay] INT NOT NULL, 
    [IsAM] BIT NOT NULL
);
GO

CREATE PROCEDURE [dw].[GetRecords_Dim_DateTimes]
	@tracking ROWVERSION
AS
	SELECT MAX(Tracking) AS Tracking FROM [dw].[Dim_DateTimes] 
	SELECT 
		[Date], 
		[Year], 
		[MonthOfYear], 
		[QuarterOfYear], 
		[DayOfYear], 
		[DayOfWeek], 
		[ISOWeekOfYear], 
		[DayOfMonth], 
		[Month],
		[Quarter], 
		[WeekEnding], 
		[Day],
		[HourOfDay],
		[IsAM]
	FROM [dw].[Dim_DateTimes] WHERE Tracking > ISNULL(@tracking, 0)
GO

CREATE PROCEDURE [dw].[Merge_Dim_DateTimes]
	@records [dw].[Dim_DateTimeRecord] READONLY
AS
	MERGE [dw].[Dim_DateTimes] AS Target USING 
	(
		SELECT 
			[Date], 
			[Year], 
			[MonthOfYear], 
			[QuarterOfYear], 
			[DayOfYear], 
			[DayOfWeek], 
			[ISOWeekOfYear], 
			[DayOfMonth], 
			[Month],
			[Quarter], 
			[WeekEnding], 
			[Day],
			[HourOfDay],
			[IsAM]
		FROM @records 
	) 
	AS Source ON (Target.[Date] = Source.[Date])
	WHEN MATCHED AND 
		Target.[Year] != Source.[Year] OR
		Target.[MonthOfYear] != Source.[MonthOfYear] OR
		Target.[QuarterOfYear] != Source.[QuarterOfYear] OR
		Target.[DayOfYear] != Source.[DayOfYear] OR
		Target.[DayOfWeek] != Source.[DayOfWeek] OR
		Target.[ISOWeekOfYear] != Source.[ISOWeekOfYear] OR
		Target.[DayOfMonth] != Source.[DayOfMonth] OR
		Target.[Month] != Source.[Month] OR
		Target.[Quarter] != Source.[Quarter] OR
		Target.[WeekEnding] != Source.[WeekEnding] OR
		Target.[Day] != Source.[Day] OR
		Target.[HourOfDay] != Source.[HourOfDay] OR
		Target.[IsAM] != Source.[IsAM]
	THEN
		UPDATE SET 
			Target.[Year] = Source.[Year],
			Target.[MonthOfYear] = Source.[MonthOfYear],
			Target.[QuarterOfYear] = Source.[QuarterOfYear],
			Target.[DayOfYear] = Source.[DayOfYear],
			Target.[DayOfWeek] = Source.[DayOfWeek],
			Target.[ISOWeekOfYear] = Source.[ISOWeekOfYear],
			Target.[DayOfMonth] = Source.[DayOfMonth],
			Target.[Month] = Source.[Month],
			Target.[Quarter] = Source.[Quarter],
			Target.[WeekEnding] = Source.[WeekEnding],
			Target.[Day] = Source.[Day],
			Target.[HourOfDay] = Source.[HourOfDay],
			Target.[IsAM] = Source.[IsAM]
	WHEN NOT MATCHED BY TARGET THEN 
		INSERT (
			[Date], 
			[Year], 
			[MonthOfYear], 
			[QuarterOfYear], 
			[DayOfYear], 
			[DayOfWeek], 
			[ISOWeekOfYear], 
			[DayOfMonth], 
			[Month],
			[Quarter], 
			[WeekEnding], 
			[Day],
			[HourOfDay],
			[IsAM]
		) VALUES (
			Source.[Date], 
			Source.[Year], 
			Source.[MonthOfYear], 
			Source.[QuarterOfYear], 
			Source.[DayOfYear], 
			Source.[DayOfWeek], 
			Source.[ISOWeekOfYear], 
			Source.[DayOfMonth], 
			Source.[Month],
			Source.[Quarter], 
			Source.[WeekEnding], 
			Source.[Day],
			Source.[HourOfDay],
			Source.[IsAM]
		);

	SELECT @@ROWCOUNT AS AffectedCount
GO

CREATE PROCEDURE [dw].[Populate_Dim_DateTimes]
	@tenantId AS int
AS
	DECLARE @records AS [dw].[Dim_DateTimeRecord]


	DECLARE 
		@fromDate DATETIME,
		@toDate DATETIME,
		@fromDate1 DATETIME,
		@fromDate2 DATETIME,
		@toDate1 DATETIME,
		@toDate2 DATETIME

	SELECT 
		@fromDate1 = MIN([Date]),
		@toDate1   = DATEADD(DAY, DATEDIFF(DAY, 0, GETUTCDATE()), 1)		-- Tomorrow morning at 12am
		FROM dw.Dim_DateTimes

	SELECT 
		@fromDate2 = MIN(DateCreated),
		@toDate2 = MAX(DateCreated) 
		FROM gce.tblProjectJobTask

	IF @fromDate1 <= ISNULL(@fromDate2, @fromDate1)
		SELECT @fromDate = MAX([Date]) FROM dw.Dim_DateTimes
	ELSE
		SELECT @fromDate = @fromDate2

	SELECT @toDate = IIF(@toDate1 > ISNULL(@toDate2, @toDate1), @toDate1, ISNULL(@toDate2, @toDate1))

	-- Round to midnight
	SELECT 
		@fromDate = DATEADD(DAY, DATEDIFF(DAY, 0, @fromDate), 0),
		@toDate   = DATEADD(DAY, DATEDIFF(DAY, 0, @toDate), 1)			-- Tomorrow morning at 12am

/*
	PRINT 'FromDate1 = ' + CONVERT(NVARCHAR, @fromDate1)
	PRINT 'ToDate1   = ' + CONVERT(NVARCHAR, @toDate1)
	PRINT 'FromDate2 = ' + CONVERT(NVARCHAR, @fromDate2)
	PRINT 'ToDate2   = ' + CONVERT(NVARCHAR, @toDate2)
	PRINT 'FromDate  = ' + CONVERT(NVARCHAR, @fromDate)
	PRINT 'ToDate    = ' + CONVERT(NVARCHAR, @toDate)
*/

	WHILE @fromDate <= @toDate
	BEGIN
--		PRINT 'Make ' + CONVERT(NVARCHAR, @fromDate)
		IF NOT EXISTS (SELECT [Date] FROM dw.Dim_DateTimes WHERE [Date] = @fromDate) 
		BEGIN
			INSERT INTO @records (
				[Date],
				[Year],
				[MonthOfYear],
				[QuarterOfYear],
				[DayOfYear],
				[DayOfWeek],
				[ISOWeekOfYear],
				[DayOfMonth],
				[Month],
				[Quarter],
				[WeekEnding],
				[Day],
				[HourOfDay],
				[IsAM]
			) VALUES (
				@fromDate,
				DATEPART(YEAR, @fromDate),
				DATEPART(MONTH, @fromDate),
				DATEPART(QUARTER, @fromDate),
				DATEPART(DAYOFYEAR, @fromDate),
				DATEPART(WEEKDAY, @fromDate),
				DATEPART(ISO_WEEK, @fromDate),
				DATEPART(DAY, @fromDate),
				DATENAME(MONTH, @fromDate),
				'Q' + CONVERT(NVARCHAR, DATEPART(QUARTER, @fromDate)) + ' ' + CONVERT(NVARCHAR, DATEPART(YEAR, @fromDate)),
				DATEADD(DAY, DATEDIFF(DAY, 0, @fromDate + 7 - DATEPART(WEEKDAY, @fromDate)), 0),
				DATENAME(DAY, @fromDate),
				DATEPART(HOUR, @fromDate),
				CASE WHEN DATEPART(HOUR, @fromDate) < 12 THEN 1 ELSE 0 END
			)
		END
		SELECT @fromDate = DATEADD(HOUR, 1, @fromDate)
	END

	EXEC [dw].[Merge_Dim_DateTimes] @records

RETURN 0

