﻿CREATE TYPE [dw].[Dim_TaskTypeRecord] AS TABLE
(	
	PkId VARCHAR(50) NOT NULL,
	TenantId INT,
	Id UNIQUEIDENTIFIER,
	[Name] NVARCHAR(50),
	ParentTaskTypePkId VARCHAR(50) NULL
)
GO

CREATE PROCEDURE [dw].[GetRecords_Dim_TaskTypes]
	@tracking ROWVERSION
AS
	SELECT MAX(Tracking) AS Tracking FROM [dw].[Dim_TaskTypes] 
	SELECT PkId, TenantId, Id, [Name], ParentTaskTypePkId FROM [dw].[Dim_TaskTypes] WHERE Tracking > ISNULL(@tracking, 0)
GO

CREATE PROCEDURE [dw].[Merge_Dim_TaskTypes]
	@records [dw].[Dim_TaskTypeRecord] READONLY
AS
	MERGE [dw].[Dim_TaskTypes] AS Target USING 
	(
		SELECT PkId, TenantId, Id, [Name], ParentTaskTypePkId FROM @records 
	) 
	AS Source ON (Target.PkId = Source.PkId)
	WHEN MATCHED AND Target.[Name] != Source.[Name] OR Target.ParentTaskTypePkId != Source.ParentTaskTypePkId THEN
		UPDATE SET Target.[Name] = Source.[Name], Target.ParentTaskTypePkId = Source.ParentTaskTypePkId
	WHEN NOT MATCHED BY TARGET THEN 
		INSERT (PkId, TenantId, Id, [Name], ParentTaskTypePkId) VALUES (Source.PkId, Source.TenantId, Source.Id, Source.[Name], Source.ParentTaskTypePkId);

	SELECT @@ROWCOUNT AS AffectedCount
GO

CREATE PROCEDURE [dw].[Populate_Dim_TaskTypes]
	@tenantId AS int
AS
	DECLARE @records AS [dw].[Dim_TaskTypeRecord]

	INSERT INTO @records 
		SELECT 
			dw.MakePkIdGUID(@tenantId, tt.Id),
			@tenantId, 
			tt.Id, 
			tt.[Name], 
			dw.MakePkIdGUID(@tenantId, ttr.ProjectJobTaskTypeSourceId)
		FROM gce.tblProjectJobTaskType tt 
			LEFT JOIN gce.tblProjectJobTaskTypeRelationship ttr ON tt.Id = ttr.ProjectJobTaskTypeDestinationId AND ttr.RelationshipType = 0	

	DECLARE @trackingBefore ROWVERSION
	SELECT @trackingBefore = MAX(Tracking) FROM [dw].[Dim_TaskTypes]

	EXEC [dw].[Merge_Dim_TaskTypes] @records

	-- Return the trackingBefore so we can record "where we are upto"
	SELECT @trackingBefore AS Tracking

RETURN 0

