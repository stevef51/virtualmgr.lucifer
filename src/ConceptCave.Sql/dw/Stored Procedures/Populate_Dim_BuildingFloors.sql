﻿CREATE TYPE [dw].[Dim_BuildingFloorRecord] AS TABLE
(
	PkId VARCHAR(50) NOT NULL,
	TenantId INT,
	Id INT,
	[Name] NVARCHAR(255) NULL,
	[Description] NVARCHAR(1024) NULL,
	[Level] INT,
	BuildingPkId VARCHAR(50) NOT NULL
)
GO

CREATE PROCEDURE [dw].[GetRecords_Dim_BuildingFloors]
	@tracking ROWVERSION
AS
	SELECT MAX(Tracking) AS Tracking FROM [dw].[Dim_BuildingFloors]
	SELECT PkId, TenantId, Id, [Name], [Description], [Level], BuildingPkId FROM [dw].[Dim_BuildingFloors] WHERE Tracking > ISNULL(@tracking, 0)
GO

CREATE PROCEDURE [dw].[Merge_Dim_BuildingFloors]
	@records [dw].[Dim_BuildingFloorRecord] READONLY
AS
	MERGE [dw].[Dim_BuildingFloors] AS Target USING 
	(
		SELECT PkId, TenantId, Id, [Name], [Description], [Level], BuildingPkId FROM @records 
	) 
	AS Source ON (Target.PkId = Source.PkId)
	WHEN MATCHED AND Target.[Name] != Source.[Name] OR Target.[Description] != Source.[Description] OR Target.[Level] != Source.[Level] OR Target.BuildingPkId != Source.BuildingPkId THEN
		UPDATE SET Target.[Name] = Source.[Name], Target.[Description] = Source.[Description], Target.[Level] = Source.[Level], Target.BuildingPkId = Source.BuildingPkId
	WHEN NOT MATCHED BY TARGET THEN 
		INSERT (PkId, TenantId, Id, [Name], [Description], [Level], BuildingPkId) VALUES (Source.PkId, Source.TenantId, Source.Id, Source.[Name], Source.[Description], Source.[Level], Source.BuildingPkId);

	SELECT @@ROWCOUNT AS AffectedCount
GO

CREATE PROCEDURE [dw].[Populate_Dim_BuildingFloors]
	@tenantId AS int
AS
	DECLARE @records AS [dw].[Dim_BuildingFloorRecord]

	INSERT INTO @records 
		SELECT 
			dw.MakePkIdINT(@tenantId, Id),
			@tenantId, 
			Id, 
			[Name], 
			[Description], 
			[Level], 
			dw.MakePkIdINT(@tenantId, ParentId)
		FROM asset.tblFacilityStructure WHERE StructureType = 2 

	EXEC [dw].[Merge_Dim_BuildingFloors] @records

RETURN 0

