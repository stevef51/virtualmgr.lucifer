﻿CREATE TYPE [dw].[Dim_FloorZoneRecord] AS TABLE
(
	PkId VARCHAR(50) NOT NULL,
	TenantId INT,
	Id INT,
	[Name] NVARCHAR(255) NULL,
	[Description] NVARCHAR(1024) NULL,
	FloorPkId VARCHAR(50) NOT NULL
)
GO

CREATE PROCEDURE [dw].[GetRecords_Dim_FloorZones]
	@tracking ROWVERSION
AS
	SELECT MAX(Tracking) AS Tracking FROM [dw].[Dim_FloorZones] 
	SELECT PkId, TenantId, Id, [Name], [Description], FloorPkId FROM [dw].[Dim_FloorZones] WHERE Tracking > ISNULL(@tracking, 0)
GO

CREATE PROCEDURE [dw].[Merge_Dim_FloorZones]
	@records [dw].[Dim_FloorZoneRecord] READONLY
AS
	MERGE [dw].[Dim_FloorZones] AS Target USING 
	(
		SELECT PkId, TenantId, Id, [Name], [Description], FloorPkId FROM @records 
	) 
	AS Source ON (Target.PkId = Source.PkId)
	WHEN MATCHED AND Target.[Name] != Source.[Name] OR Target.[Description] != Source.[Description] OR Target.FloorPkId != Source.FloorPkId THEN
		UPDATE SET Target.[Name] = Source.[Name], Target.[Description] = Source.[Description], Target.FloorPkId = Source.FloorPkId
	WHEN NOT MATCHED BY TARGET THEN 
		INSERT (PkId, TenantId, Id, [Name], [Description], FloorPkId) VALUES (Source.PkId, Source.TenantId, Source.Id, Source.[Name], Source.[Description], Source.FloorPkId);

	SELECT @@ROWCOUNT AS AffectedCount 
GO

CREATE PROCEDURE [dw].[Populate_Dim_FloorZones]
	@tenantId AS int
AS
	DECLARE @records AS [dw].[Dim_FloorZoneRecord]

	INSERT INTO @records 
		SELECT 
			dw.MakePkIdINT(@tenantId, Id),
			@tenantId, 
			Id, 
			[Name],
			[Description], 
			dw.MakePkIdINT(@tenantId, ParentId)
		FROM asset.tblFacilityStructure WHERE StructureType = 3

	EXEC [dw].[Merge_Dim_FloorZones] @records

RETURN 0

