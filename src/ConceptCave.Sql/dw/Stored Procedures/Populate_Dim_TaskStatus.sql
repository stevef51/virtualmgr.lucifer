﻿CREATE TYPE [dw].[Dim_TaskStatusRecord] AS TABLE
(
	Id INT,
	[Status] NVARCHAR(50)
)
GO

CREATE PROCEDURE [dw].[GetRecords_Dim_TaskStatus]
	@tracking ROWVERSION
AS
	SELECT MAX(Tracking) AS Tracking FROM [dw].[Dim_TaskStatus] 
	SELECT Id, [Status] FROM [dw].[Dim_TaskStatus] WHERE Tracking > ISNULL(@tracking, 0)
GO

CREATE PROCEDURE [dw].[Merge_Dim_TaskStatus]
	@records [dw].[Dim_TaskStatusRecord] READONLY
AS
	MERGE [dw].[Dim_TaskStatus] AS Target USING 
	(
		SELECT Id, [Status] FROM @records 
	) 
	AS Source ON (Target.Id = Source.Id)
	WHEN MATCHED AND Target.[Status] != Source.[Status] THEN
		UPDATE SET Target.[Status] = Source.[Status]
	WHEN NOT MATCHED BY TARGET THEN 
		INSERT (Id, [Status]) VALUES (Source.Id, Source.[Status]);

	SELECT @@ROWCOUNT AS AffectedCount
GO

CREATE PROCEDURE [dw].[Populate_Dim_TaskStatus]
	@tenantId AS int
AS
	DECLARE @records AS [dw].[Dim_TaskStatusRecord]

	INSERT INTO @records (Id, [Status]) VALUES (0, N'Unapproved')
	INSERT INTO @records (Id, [Status]) VALUES (1, N'Approved')
	INSERT INTO @records (Id, [Status]) VALUES (2, N'Started')
	INSERT INTO @records (Id, [Status]) VALUES (3, N'Paused')
	INSERT INTO @records (Id, [Status]) VALUES (4, N'Finished Complete')
	INSERT INTO @records (Id, [Status]) VALUES (5, N'Finished Incomplete')
	INSERT INTO @records (Id, [Status]) VALUES (6, N'Cancelled')
	INSERT INTO @records (Id, [Status]) VALUES (7, N'Finished by Management')
	INSERT INTO @records (Id, [Status]) VALUES (8, N'Approved Continued')
	INSERT INTO @records (Id, [Status]) VALUES (9, N'Change Roster Requested')
	INSERT INTO @records (Id, [Status]) VALUES (10, N'Change Roster Rejected')
	INSERT INTO @records (Id, [Status]) VALUES (11, N'Change Roster Accepted')
	INSERT INTO @records (Id, [Status]) VALUES (12, N'Finished by System')

	DECLARE @trackingBefore ROWVERSION
	SELECT @trackingBefore = MAX(Tracking) FROM [dw].[Dim_TaskStatus]

	EXEC [dw].[Merge_Dim_TaskStatus] @records

	-- Return the trackingBefore so we can record "where we are upto"
	SELECT @trackingBefore AS Tracking

RETURN 0

