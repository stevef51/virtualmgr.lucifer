﻿CREATE TYPE [dw].[Dim_CompanyRecord] AS TABLE
(
	[PkId] VARCHAR(50) NOT NULL,
	[TenantId] INT NOT NULL,
	[Id] UNIQUEIDENTIFIER NOT NULL,
	[Name] NVARCHAR(50) NULL
);
GO

CREATE PROCEDURE [dw].[GetRecords_Dim_Companies]
	@tracking ROWVERSION
AS
	SELECT MAX(Tracking) AS Tracking FROM [dw].[Dim_Companies] 
	SELECT PkId, TenantId, Company_Id AS Id, Company_Name AS [Name] FROM [dw].[Dim_Companies] WHERE Tracking > ISNULL(@tracking, 0)
GO

CREATE PROCEDURE [dw].[Merge_Dim_Companies]
	@records [dw].[Dim_CompanyRecord] READONLY
AS
	MERGE [dw].[Dim_Companies] AS Target USING 
	(
		SELECT PkId, TenantId, Id, [Name] FROM @records 
	) 
	AS Source ON (Target.PkId = Source.PkId)
	WHEN MATCHED AND Target.Company_Name != Source.Name THEN
		UPDATE SET Target.Company_Name = Source.[Name] 
	WHEN NOT MATCHED BY TARGET THEN 
		INSERT (PkId, TenantId, Company_Id,  Company_Name) VALUES (Source.PkId, Source.TenantId, Source.Id, Source.[Name]);

	SELECT @@ROWCOUNT AS AffectedCount
GO

CREATE PROCEDURE [dw].[Populate_Dim_Companies]
	@tenantId AS int
AS
	DECLARE @records AS [dw].[Dim_CompanyRecord]

	INSERT INTO @records 
	SELECT 
		dw.MakePkIdGUID(@tenantId, Id),
		@tenantId, 
		Id, 
		[Name] 
	FROM [gce].[tblCompany]

	EXEC [dw].[Merge_Dim_Companies] @records

RETURN 0
