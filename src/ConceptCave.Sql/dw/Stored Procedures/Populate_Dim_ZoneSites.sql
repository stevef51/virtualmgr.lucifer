﻿CREATE TYPE [dw].[Dim_ZoneSiteRecord] AS TABLE
(
	PkId VARCHAR(50) NOT NULL,
	TenantId INT,
	Id INT,
	[Name] NVARCHAR(255) NULL,
	[Description] NVARCHAR(1024) NULL,
	ZonePkId VARCHAR(50) NOT NULL
)		
GO

CREATE PROCEDURE [dw].[GetRecords_Dim_ZoneSites]
	@tracking ROWVERSION
AS
	SELECT MAX(Tracking) AS Tracking FROM [dw].[Dim_ZoneSites] 	
	SELECT PkId, TenantId, Id, [Name], [Description], ZonePkId FROM [dw].[Dim_ZoneSites] WHERE Tracking > ISNULL(@tracking, 0)
GO

CREATE PROCEDURE [dw].[Merge_Dim_ZoneSites]
	@records [dw].[Dim_ZoneSiteRecord] READONLY
AS
	MERGE [dw].[Dim_ZoneSites] AS Target USING 
	(
		SELECT PkId, TenantId, Id, [Name], [Description], ZonePkId FROM @records 
	) 
	AS Source ON (Target.PkId = Source.PkId)
	WHEN MATCHED AND Target.[Name] != Source.[Name] OR Target.[Description] != Source.[Description] OR Target.ZonePkId != Source.ZonePkId THEN
		UPDATE SET Target.[Name] = Source.[Name], Target.[Description] = Source.[Description], Target.ZonePkId = Source.ZonePkId
	WHEN NOT MATCHED BY TARGET THEN 
		INSERT (PkId, TenantId, Id, [Name], [Description], ZonePkId) VALUES (Source.PkId, Source.TenantId, Source.Id, Source.[Name], Source.[Description], Source.ZonePkId);

	SELECT @@ROWCOUNT AS AffectedCount
GO

CREATE PROCEDURE [dw].[Populate_Dim_ZoneSites]
	@tenantId AS int
AS
	DECLARE @records AS [dw].[Dim_ZoneSiteRecord]

	INSERT INTO @records 
		SELECT 
			dw.MakePkIdINT(@tenantId, Id),
			@tenantId, 
			Id, 
			[Name], 
			[Description], 
			dw.MakePkIdINT(@tenantId, ParentId)
		FROM asset.tblFacilityStructure WHERE StructureType = 4

	DECLARE @trackingBefore ROWVERSION
	SELECT @trackingBefore = MAX(Tracking) FROM [dw].[Dim_ZoneSites]

	EXEC [dw].[Merge_Dim_ZoneSites] @records

	-- Return the trackingBefore so we can record "where we are upto"
	SELECT @trackingBefore AS Tracking

RETURN 0

