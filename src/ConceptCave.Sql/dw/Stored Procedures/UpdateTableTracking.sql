﻿CREATE PROCEDURE [dw].[UpdateTableTracking]
	@tenantId INT,
	@table NVARCHAR(100),
	@tracking ROWVERSION
AS
	SET NOCOUNT ON

	IF NOT EXISTS (SELECT 1 FROM [dw].[TableTracking] WHERE TenantId = @tenantId AND [Table] = @table) 
		INSERT INTO [dw].[TableTracking] (TenantID, [Table], LastTracking) VALUES (@tenantId, @table, @tracking)
	ELSE
		UPDATE [dw].[TableTracking] SET LastTracking = @tracking WHERE TenantId = @tenantId AND [Table] = @table

	RETURN 0
GO