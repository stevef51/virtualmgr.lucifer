﻿CREATE TYPE [dw].[Dim_FacilityBuildingRecord] AS TABLE
(
	PkId VARCHAR(50) NOT NULL,
	TenantId INT,
	Id INT,
	[Name] NVARCHAR(255) NULL,
	[Description] NVARCHAR(1024) NULL,
	FacilityPkId VARCHAR(50) NOT NULL
)
GO

CREATE PROCEDURE [dw].[GetRecords_Dim_FacilityBuildings]
	@tracking ROWVERSION
AS
	SELECT MAX(Tracking) AS Tracking FROM [dw].[Dim_FacilityBuildings] 
	SELECT PkId, TenantId, Id, [Name], [Description], FacilityPkId FROM [dw].[Dim_FacilityBuildings] WHERE Tracking > ISNULL(@tracking, 0)
GO

CREATE PROCEDURE [dw].[Merge_Dim_FacilityBuildings]
	@records [dw].[Dim_FacilityBuildingRecord] READONLY
AS
	MERGE [dw].[Dim_FacilityBuildings] AS Target USING 
	(
		SELECT PkId, TenantId, Id, [Name], [Description], FacilityPkId FROM @records 
	) 
	AS Source ON (Target.PkId = Source.PkId)
	WHEN MATCHED AND Target.[Name] != Source.[Name] OR Target.[Description] != Source.[Description] OR Target.FacilityPkId != Source.FacilityPkId THEN
		UPDATE SET Target.[Name] = Source.[Name], Target.[Description] = Source.[Description], Target.FacilityPkId = Source.FacilityPkId
	WHEN NOT MATCHED BY TARGET THEN 
		INSERT (PkId, TenantId, Id, [Name], [Description], FacilityPkId) VALUES (Source.PkId, Source.TenantId, Source.Id, Source.[Name], Source.[Description], Source.FacilityPkId);

	SELECT @@ROWCOUNT AS AffectedCount
GO

CREATE PROCEDURE [dw].[Populate_Dim_FacilityBuildings]
	@tenantId AS int
AS
	DECLARE @records AS [dw].[Dim_FacilityBuildingRecord]

	INSERT INTO @records 
		SELECT 
			dw.MakePkIdINT(@tenantId, Id),
			@tenantId, 
			Id, 
			[Name], 
			[Description], 
			dw.MakePkIdINT(@tenantId, ParentId)
		FROM asset.tblFacilityStructure WHERE StructureType = 1 

	EXEC [dw].[Merge_Dim_FacilityBuildings] @records

RETURN 0

