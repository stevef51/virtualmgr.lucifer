﻿CREATE TYPE [dw].[Dim_SiteRecord] AS TABLE
(
	PkId VARCHAR(50) NOT NULL,
	TenantId INT,
	SiteId UNIQUEIDENTIFIER,
	SiteName NVARCHAR(200)
)
GO

CREATE PROCEDURE [dw].[GetRecords_Dim_Sites]
	@tracking ROWVERSION
AS
	SELECT MAX(Tracking) AS Tracking FROM [dw].[Dim_Sites] 
	SELECT PkId, TenantId, SiteId, SiteName FROM [dw].[Dim_Sites] WHERE Tracking > ISNULL(@tracking, 0)
GO

CREATE PROCEDURE [dw].[Merge_Dim_Sites]
	@records [dw].[Dim_SiteRecord] READONLY
AS
	MERGE [dw].[Dim_Sites] AS Target USING 
	(
		SELECT PkId, TenantId, SiteId, SiteName FROM @records 
	) 
	AS Source ON (Target.PkId = Source.PkId)
	WHEN MATCHED AND Target.SiteName != Source.SiteName THEN
		UPDATE SET Target.SiteName = Source.SiteName
	WHEN NOT MATCHED BY TARGET THEN 
		INSERT (PkId, TenantId, SiteId, SiteName) VALUES (Source.PkId, Source.TenantId, Source.SiteId, Source.SiteName);

	SELECT @@ROWCOUNT AS AffectedCount
GO

CREATE PROCEDURE [dw].[Populate_Dim_Sites]
	@tenantId AS int
AS
	DECLARE @records AS [dw].[Dim_SiteRecord]

	INSERT INTO @records 
		SELECT 
			dw.MakePkIdGUID(@tenantId, UserId),
			@tenantId, 
			UserId, 
			ud.[Name] 
		FROM tblUserData ud inner join tblUserType ut on ut.Id = ud.UserTypeId where ut.IsASite = 1

	DECLARE @trackingBefore ROWVERSION
	SELECT @trackingBefore = MAX(Tracking) FROM [dw].[Dim_Sites]

	EXEC [dw].[Merge_Dim_Sites] @records

	-- Return the trackingBefore so we can record "where we are upto"
	SELECT @trackingBefore AS Tracking

RETURN 0

