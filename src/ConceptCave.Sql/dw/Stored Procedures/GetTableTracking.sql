﻿CREATE PROCEDURE [dw].[GetTableTracking]
	@tenantId INT,
	@table NVARCHAR(100)
AS
	SET NOCOUNT ON

	SELECT LastTracking FROM [dw].[TableTracking] WHERE TenantId = @tenantId AND [Table] = @table

	RETURN 0
GO