﻿CREATE TABLE [dw].[Dim_Rosters]
(
	[PkId] VARCHAR(50) NOT NULL,
    [TenantId] INT NOT NULL, 
	[Id] INT NOT NULL, 
    [Tracking] ROWVERSION NOT NULL, 
    [Name] NVARCHAR(50) NOT NULL,
    CONSTRAINT [PK_Dim_Rosters] PRIMARY KEY ([PkId]),
)
GO

CREATE INDEX [IX_Dim_Rosters_Tracking] ON [dw].[Dim_Rosters] ([Tracking])

