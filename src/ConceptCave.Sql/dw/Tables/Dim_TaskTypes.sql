﻿CREATE TABLE [dw].[Dim_TaskTypes]
(
	[PkId] VARCHAR(50) NOT NULL,
    [TenantId] INT NOT NULL,
	[Id] UNIQUEIDENTIFIER NOT NULL, 
	[Tracking] ROWVERSION,
    [Name] NVARCHAR(50) NOT NULL,
	[ParentTaskTypePkId] VARCHAR(50) NULL,
    CONSTRAINT [PK_Dim_TaskTypes] PRIMARY KEY ([PkId]),
)
GO

CREATE INDEX [IX_Dim_TaskTypes_Tracking] ON [dw].[Dim_TaskTypes] ([Tracking])

