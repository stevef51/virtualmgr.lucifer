﻿CREATE TABLE [dw].[Dim_Users]
(
	[PkId] VARCHAR(50) NOT NULL,
	[TenantId] INT NOT NULL,
	[UserId] UNIQUEIDENTIFIER NOT NULL, 
	[Tracking] ROWVERSION,
    [UserFullName] NVARCHAR(200) NOT NULL,
    CONSTRAINT [PK_Dim_Users] PRIMARY KEY ([PkId]),
)

GO

CREATE INDEX [IX_Dim_Users_Tracking] ON [dw].[Dim_Users] ([Tracking])
