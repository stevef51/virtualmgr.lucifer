﻿CREATE TABLE [dw].[Dim_TaskStatus]
(
	[Id] INT NOT NULL,
    [Status] NVARCHAR(50) NOT NULL,
	[Tracking] ROWVERSION NOT NULL, 
    CONSTRAINT [PK_Dim_TaskStatus] PRIMARY KEY ([Id]),
)
GO

CREATE INDEX [IX_Dim_TaskStatus_Tracking] ON [dw].[Dim_TaskStatus] ([Tracking])
