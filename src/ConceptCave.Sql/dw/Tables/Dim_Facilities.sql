﻿CREATE TABLE [dw].[Dim_Facilities]
(
	[PkId] VARCHAR(50) NOT NULL,
	[TenantId] INT NOT NULL, 
	[Id] INT NOT NULL, 
    [Tracking] ROWVERSION NOT NULL, 
	[Name] NVARCHAR(255) NULL,
	[Description] NVARCHAR(1024) NULL,
    CONSTRAINT [PK_Dim_Facilities] PRIMARY KEY ([PkId]),
)
GO

CREATE INDEX [IX_Dim_Facilities_Tracking] ON [dw].[Dim_Facilities] ([Tracking])

