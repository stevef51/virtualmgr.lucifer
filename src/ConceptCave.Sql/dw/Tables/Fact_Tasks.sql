﻿CREATE TABLE [dw].[Fact_Tasks] (
	[PkId] VARCHAR(50) NOT NULL,
	[TenantId] INT NOT NULL,
    [Id]           UNIQUEIDENTIFIER NOT NULL,
	[Tracking] ROWVERSION,
	[TrackingLong] AS CONVERT(BIGINT, Tracking),
	[RosterId] INT NOT NULL,
	[RosterPkId] VARCHAR(50) NOT NULL,
	[TaskTypeId] UNIQUEIDENTIFIER NOT NULL,
	[TaskTypePkId] VARCHAR(50) NOT NULL,
	[UtcDateTimeCreated] DATETIME NULL ,
	[LocalDateTimeCreated] DATETIME NULL,
	[UtcDateTimeStarted] DATETIME NULL,
	[LocalDateTimeStarted] DATETIME NULL,
	[UtcDateTimeCompleted] DATETIME NULL,
	[LocalDateTimeCompleted] DATETIME NULL,
    [Name]         NVARCHAR (4000)    NOT NULL,
    [Description]  NVARCHAR (MAX)   NULL,
	[OwnerId] UNIQUEIDENTIFIER NOT NULL,
    [OwnerPkId]      VARCHAR(50) NOT NULL,
	[OriginalOwnerId] UNIQUEIDENTIFIER NOT NULL,
	[OriginalOwnerPkId] VARCHAR(50) NOT NULL,
	[SiteId] UNIQUEIDENTIFIER NULL,
	[SitePkId] VARCHAR(50) NULL,
	[RoleId] INT NULL,
	[RolePkId] VARCHAR(50) NULL,
	[HierarchyBucketId] INT NULL,
	[HierarchyBucketPkId] VARCHAR(50) NULL,
    [Level] INT NOT NULL DEFAULT 0, 
    [RequireSignature] BIT NOT NULL DEFAULT 0, 
    [PhotoDuringSignature] BIT NOT NULL DEFAULT 0,
    [CompletionNotes] NVARCHAR(MAX) NULL, 
    [Status] INT NOT NULL DEFAULT 0, 
    [UtcStatusDate] DATETIME NOT NULL, 
	[LocalStatusDate] DATETIME NULL,
	[ProjectJobTaskGroupId] UNIQUEIDENTIFIER NULL,
    [ProjectJobTaskGroupPkId] VARCHAR(50) NULL, 
	[WorkingGroupId] UNIQUEIDENTIFIER NULL,
    [WorkingGroupPkId] VARCHAR(50) NULL, 
	[StatusId] UNIQUEIDENTIFIER NULL,
    [StatusPkId] VARCHAR(50) NULL, 
    [ReferenceNumber] NVARCHAR(20) NULL, 
    [ActualDuration] DECIMAL(18, 2) NULL, 
    [InactiveDuration] DECIMAL(18, 2) NULL, 
    [WorkingDocumentCount] INT NOT NULL DEFAULT 0, 
    [AllActivitiesEstimatedDuration] DECIMAL(18, 2) NOT NULL DEFAULT 0, 
    [CompletedActivitiesEstimatedDuration] DECIMAL(18, 2) NOT NULL DEFAULT 0, 
	[EstimatedDurationSeconds] DECIMAL(18, 2) NULL,
	[ScheduledOwnerId] UNIQUEIDENTIFIER NULL,
    [ScheduledOwnerPkId] VARCHAR(50) NULL, 
	[OriginalTaskTypeId] UNIQUEIDENTIFIER NULL,
    [OriginalTaskTypePkId] VARCHAR(50) NULL, 
    [SortOrder] INT NOT NULL DEFAULT 0, 
    [HasOrders] BIT NOT NULL DEFAULT 0, 
    [MinimumDuration] DECIMAL(18, 2) NULL, 
	[DimUtcDateTimeCreated] DATETIME NULL,
	[DimLocalDateTimeCreated] DATETIME NULL,
	[DimUtcDateCreated] DATETIME NULL,
	[DimLocalDateCreated] DATETIME NULL,
	[IsScheduled] BIT NULL,
    [ActualVsEstimatedPercentage] AS (ActualDuration - EstimatedDurationSeconds)/EstimatedDurationSeconds,
    [ActualVsEstimatedPercentile] AS round((ActualDuration-EstimatedDurationSeconds) / EstimatedDurationSeconds, 1), 
    CONSTRAINT [PK_Fact_Task] PRIMARY KEY CLUSTERED ([PkId] ASC),
    CONSTRAINT [FK_Fact_Task_To_Dim_Users] FOREIGN KEY ([OwnerPkId]) REFERENCES [dw].[Dim_Users] ([PkId]), 
    CONSTRAINT [FK_Fact_Task_To_Dim_TaskTypes] FOREIGN KEY ([TaskTypePkId]) REFERENCES [dw].[Dim_TaskTypes]([PkId]), 
    CONSTRAINT [FK_Fact_Task_To_Dim_Sites] FOREIGN KEY ([SitePkId]) REFERENCES [dw].[Dim_Sites]([PkId]), 
    CONSTRAINT [FK_Fact_Task_To_Dim_CustomStatus] FOREIGN KEY ([StatusPkId]) REFERENCES [dw].[Dim_TaskCustomStatus]([PkId]), 
    CONSTRAINT [FK_Fact_Task_To_Dim_Status] FOREIGN KEY ([Status]) REFERENCES [dw].[Dim_TaskStatus]([Id]), 
    CONSTRAINT [FK_Fact_Task_OriginalTaskType_To_Dim_TaskTypes] FOREIGN KEY ([OriginalTaskTypePkId]) REFERENCES [dw].[Dim_TaskTypes]([PkId]), 
    CONSTRAINT [FK_Fact_Task_UtcDateCreated_To_Dim_DateTimes] FOREIGN KEY ([DimUtcDateTimeCreated]) REFERENCES [dw].[Dim_DateTimes]([Date]), 
    CONSTRAINT [FK_Fact_Task_LocalDateCreated_To_Dim_DateTimes] FOREIGN KEY ([DimLocalDateTimeCreated]) REFERENCES [dw].[Dim_DateTimes]([Date]),
	CONSTRAINT [FK_Fact_Task_UtcDateCreated_To_Dim_Dates] FOREIGN KEY ([DimUtcDateCreated]) REFERENCES [dw].[Dim_Dates]([Date]), 
    CONSTRAINT [FK_Fact_Task_LocalDateCreated_To_Dim_Dates] FOREIGN KEY ([DimLocalDateCreated]) REFERENCES [dw].[Dim_Dates]([Date]),
	CONSTRAINT [FK_Fact_Task_Dim_Roster] FOREIGN KEY ([RosterPkId]) REFERENCES [dw].[Dim_Rosters] (PkId)
);

GO

CREATE INDEX [IX_Fact_Tasks_UtcDateTimeCreated] ON [dw].[Fact_Tasks] ([DimUtcDateTimeCreated])

GO

CREATE INDEX [IX_Fact_Tasks_LocalDateTimeCreated] ON [dw].[Fact_Tasks] (DimLocalDateTimeCreated)

GO

CREATE INDEX [IX_Fact_Tasks_UtcDateCreated] ON [dw].[Fact_Tasks] ([DimUtcDateCreated])

GO

CREATE INDEX [IX_Fact_Tasks_LocalDateCreated] ON [dw].[Fact_Tasks] (DimLocalDateCreated)

GO
CREATE INDEX [IX_Fact_Tasks_Tracking] ON [dw].[Fact_Tasks] ([Tracking])

GO
CREATE INDEX [IX_Fact_Tasks_TrackingLong] ON [dw].[Fact_Tasks] ([TrackingLong])
