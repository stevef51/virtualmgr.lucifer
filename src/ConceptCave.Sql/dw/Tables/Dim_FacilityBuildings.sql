﻿CREATE TABLE [dw].[Dim_FacilityBuildings]
(
	[PkId] VARCHAR(50) NOT NULL,
	[TenantId] INT NOT NULL,
	[Id] INT NOT NULL, 
    [Tracking] ROWVERSION NOT NULL, 
	[Name] NVARCHAR(255) NULL,
	[Description] NVARCHAR(1024) NULL,
	[FacilityPkId] VARCHAR(50) NOT NULL,
    CONSTRAINT [PK_Dim_FacilityBuildings] PRIMARY KEY ([PkId]),
	CONSTRAINT [FK_Dim_FacilityBuildings_Facilities] FOREIGN KEY (FacilityPkId) REFERENCES [dw].[Dim_Facilities] (PkId)
)
GO

CREATE INDEX [IX_Dim_FacilityBuildings_Tracking] ON [dw].[Dim_FacilityBuildings] ([Tracking])
