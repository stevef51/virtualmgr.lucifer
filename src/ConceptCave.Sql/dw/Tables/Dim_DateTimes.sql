﻿CREATE TABLE [dw].[Dim_DateTimes]
(
	[Date] DATETIME NOT NULL PRIMARY KEY, 
    [Year] INT NOT NULL, 
    [MonthOfYear] INT NOT NULL, 
    [QuarterOfYear] INT NOT NULL, 
    [DayOfYear] INT NOT NULL, 
    [DayOfWeek] INT NOT NULL, 
    [ISOWeekOfYear] INT NOT NULL, 
    [DayOfMonth] INT NOT NULL, 
    [Month] NVARCHAR(50) NOT NULL, 
    [Quarter] NVARCHAR(50) NOT NULL, 
    [WeekEnding] DATETIME NOT NULL, 
    [Day] NVARCHAR(9) NOT NULL, 
    [HourOfDay] INT NOT NULL, 
    [IsAM] BIT NOT NULL, 
    [Tracking] ROWVERSION NOT NULL
)
GO

CREATE INDEX [IX_Dim_DateTimes_Tracking] ON [dw].[Dim_DateTimes] ([Tracking])
