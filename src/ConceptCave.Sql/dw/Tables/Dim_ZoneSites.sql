﻿CREATE TABLE [dw].[Dim_ZoneSites]
(
	[PkId] VARCHAR(50) NOT NULL,
	[TenantId] INT NOT NULL,
	[Id] INT NOT NULL, 
    [Tracking] ROWVERSION NOT NULL, 
	[Name] NVARCHAR(255) NULL,
	[Description] NVARCHAR(1024) NULL,
	[ZonePkId] VARCHAR(50) NOT NULL,
    CONSTRAINT [PK_Dim_ZoneSites] PRIMARY KEY ([PkId]),
	CONSTRAINT [FK_Dim_ZoneSites_FloorZones] FOREIGN KEY (ZonePkId) REFERENCES [dw].[Dim_FloorZones] (PkId)
)

GO

CREATE INDEX [IX_Dim_ZoneSites_Tracking] ON [dw].[Dim_ZoneSites] ([Tracking])
