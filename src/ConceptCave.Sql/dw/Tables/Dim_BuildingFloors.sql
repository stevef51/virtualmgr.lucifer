﻿CREATE TABLE [dw].[Dim_BuildingFloors]
(
	[PkId] VARCHAR(50) NOT NULL,
	[TenantId] INT NOT NULL,
	[Id] INT NOT NULL, 
	[Tracking] ROWVERSION NOT NULL, 
	[Name] NVARCHAR(255) NULL,
	[Description] NVARCHAR(1024) NULL,
	[Level] INT NULL,
	[BuildingPkId] VARCHAR(50) NOT NULL,
    CONSTRAINT [PK_Dim_BuildingFloors] PRIMARY KEY ([PkId]),
	CONSTRAINT [FK_Dim_BuildingFloors_FacilityBuilding] FOREIGN KEY (BuildingPkId) REFERENCES [dw].[Dim_FacilityBuildings] (PkId)
)

GO

CREATE INDEX [IX_Dim_BuildingFloors_Tracking] ON [dw].[Dim_BuildingFloors] ([Tracking])
