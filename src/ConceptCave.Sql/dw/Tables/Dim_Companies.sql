﻿CREATE TABLE [dw].[Dim_Companies]
(
	[PkId] VARCHAR(50) NOT NULL,
	[TenantId] INT NOT NULL,
	[Company_Id] UNIQUEIDENTIFIER NOT NULL, 
	[Tracking] ROWVERSION,
    [Company_Name] NVARCHAR(200) NULL
    CONSTRAINT [PK_Dim_Companies] PRIMARY KEY ([PkId]),
)
GO

CREATE INDEX [IX_Dim_Companies_Tracking] ON [dw].[Dim_Companies] ([Tracking])
