﻿
CREATE TABLE [dw].[Dim_TaskCustomStatus] (
	[PkId] VARCHAR(50) NOT NULL,
	[TenantId] INT NOT NULL,
	[Id] UNIQUEIDENTIFIER NOT NULL,
    [Tracking] ROWVERSION NOT NULL, 
	[Text] NVARCHAR(50) NOT NULL,
	[Description] NVARCHAR(MAX) NULL,
    CONSTRAINT [PK_Dim_TaskCustomStatus] PRIMARY KEY ([PkId]),
)
GO

CREATE INDEX [IX_Dim_TaskCustomStatus_Tracking] ON [dw].[Dim_TaskCustomStatus] ([Tracking])
