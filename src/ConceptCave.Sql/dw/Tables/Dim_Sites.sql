﻿CREATE TABLE [dw].[Dim_Sites]
(
	[PkId] VARCHAR(50) NOT NULL,
	[TenantId] INT NOT NULL,
	[SiteId] UNIQUEIDENTIFIER NOT NULL, 
	[Tracking] ROWVERSION,
    [SiteName] NVARCHAR(200) NOT NULL,
    CONSTRAINT [PK_Dim_Sites] PRIMARY KEY ([PkId]),
)
GO

CREATE INDEX [IX_Dim_Sites_Tracking] ON [dw].[Dim_Sites] ([Tracking])


