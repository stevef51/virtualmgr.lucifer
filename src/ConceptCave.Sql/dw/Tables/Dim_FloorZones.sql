﻿CREATE TABLE [dw].[Dim_FloorZones]
(
	[PkId] VARCHAR(50) NOT NULL,
	[TenantId] INT NOT NULL,
	[Id] INT NOT NULL, 
	[Tracking] ROWVERSION NOT NULL, 
	[Name] NVARCHAR(255) NULL,
	[Description] NVARCHAR(1024) NULL,
	[FloorPkId] VARCHAR(50) NOT NULL,
    CONSTRAINT [PK_Dim_FloorZones] PRIMARY KEY ([PkId]),
	CONSTRAINT [FK_Dim_FloorZones_BuildingFloors] FOREIGN KEY (FloorPkId) REFERENCES [dw].[Dim_BuildingFloors] (PkId)
)

GO

CREATE INDEX [IX_Dim_FloorZones_Tracking] ON [dw].[Dim_FloorZones] ([Tracking])
