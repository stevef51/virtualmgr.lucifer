﻿CREATE TABLE [dw].[TableTracking]
(
	[Table] NVARCHAR(100) NOT NULL,
	[TenantId] INT NOT NULL,
	[LastTracking] BINARY(8) NULL,
    CONSTRAINT [PK_Dim_TableTracking] PRIMARY KEY ([TenantId],[Table]),
)
