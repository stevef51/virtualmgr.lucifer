﻿CREATE PROCEDURE [dba].[TrimWorkingDocumentData]
	@topN INT = 100,
	@daysToKeep INT = 90,
	@batchSize INT = 5
AS
	DECLARE @now DATETIME
	SELECT @now = GETUTCDATE()

	DECLARE @deleteFrom DATETIME, @affected INT, @totalAffected INT
	SELECT	
		@deleteFrom = DATEADD(DAY, -@daysToKeep, @now),
		@totalAffected = 0

	SET NOCOUNT ON

	WHILE (1 = 1)
	BEGIN
		BEGIN TRAN;
		DELETE TOP (@batchSize) dbo.tblWorkingDocumentData FROM dbo.tblWorkingDocumentData wdd INNER JOIN dbo.tblWorkingDocument wd ON wdd.WorkingDocumentId = wd.Id WHERE wd.DateCreated < @deleteFrom

		SELECT @affected = @@ROWCOUNT
		SELECT @totalAffected = @totalAffected + @affected
		SELECT @topN = @topN - @batchSize

		COMMIT

		IF @affected < @batchSize OR @topN <= 0
			BREAK

	END

	-- Not the right place to do this but easiest at the moment, delete any expired Orders (we give an extra 10minutes to the expiry date to be sure)
	DECLARE @pendingOrderFrom DATETIME
	SELECT @pendingOrderFrom = DATEADD(MINUTE, 10, @now)

	-- Have to mark as Archived any PendingPayments which are are DueNow or Scheduled may have been created (but not yet activated) for these Orders aswell ..
	UPDATE epay.tblPendingPayment SET Archived = 1 WHERE 
		[Status] IN (1, 2) AND						-- DueNow, Scheduled
		ActivatedDateUtc IS NULL AND
		OrderId IN (SELECT Id FROM stock.tblOrder WHERE Archived = 0 AND PendingExpiryDate IS NOT NULL AND @now > DATEADD(MINUTE, 10, PendingExpiryDate))
	
	UPDATE stock.tblOrder SET Archived = 1 
		WHERE 
			Archived = 0 AND 
			PendingExpiryDate IS NOT NULL AND @now > DATEADD(MINUTE, 10, PendingExpiryDate) AND
			Id IN (SELECT OrderId FROM epay.tblPendingPayment WHERE
				[Status] IN (1, 2) AND						-- DueNow, Scheduled
				ActivatedDateUtc IS NULL)			

	RETURN @totalAffected



