﻿CREATE TABLE [pejob].[tblPeriodicExecutionHistory]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [StartUtc] DATETIME NOT NULL, 
    [FinishUtc] DATETIME NULL, 
    [ForceFinished] BIT NOT NULL
)

GO

CREATE INDEX [IX_tblPeriodicExecutionHistory_StartUtc] ON [pejob].[tblPeriodicExecutionHistory] ([StartUtc])
GO

CREATE INDEX [IX_tblPeriodicExecutionHistory_FinishUtc] ON [pejob].[tblPeriodicExecutionHistory] ([FinishUtc])
