﻿CREATE TABLE [pejob].[tblJobs]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
	[Archived] BIT NOT NULL,
	[Enabled] BIT NOT NULL,
    [Name] NVARCHAR(128) NOT NULL, 
    [CRONExpression] NVARCHAR(32) NOT NULL,
	[LastRunUtc] DATETIME NULL,
    [NextRunUtc] DATETIME NULL,
	[JobType] NVARCHAR(255) NOT NULL,
	[JobData] NVARCHAR(4000) NULL
)
