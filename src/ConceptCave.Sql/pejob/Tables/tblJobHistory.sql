﻿CREATE TABLE [pejob].[tblJobHistory]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [JobId] INT NOT NULL, 
    [RanAtUtc] DATETIME NOT NULL, 
    [Success] BIT NOT NULL, 
    [Message] NVARCHAR(512) NULL, 
    CONSTRAINT [FK_tblJobHistory_ToJob] FOREIGN KEY ([JobId]) REFERENCES [pejob].[tblJobs]([Id])
)

GO

CREATE INDEX [IX_tblJobHistory_RanAtUtc] ON [pejob].[tblJobHistory] ([RanAtUtc])
