﻿CREATE TABLE [epay].[tblPaymentResponse]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY,
	[PendingPaymentId] UNIQUEIDENTIFIER NOT NULL,
	[TransactionStatus] BIT NOT NULL,
	[DateUtc] DATETIME NOT NULL,
	[RequestedAmount] DECIMAL(18, 2) NOT NULL,
	[TransactedAmount] DECIMAL(18, 2) NULL,
	[GatewayTransactionId] NVARCHAR(36) NULL,
	[GatewayAuthorisationCode] NVARCHAR(36) NULL,
	[Response] NVARCHAR(4000) NOT NULL, 
    [ResponseCode] NVARCHAR(10) NOT NULL, 
    [ResponseMessage] NVARCHAR(512) NULL, 
    [Errors] NVARCHAR(512) NULL, 
    CONSTRAINT [FK_tblPaymentResponse_ToPendingPayment] FOREIGN KEY ([PendingPaymentId]) REFERENCES [epay].[tblPendingPayment]([Id])
)

GO
CREATE INDEX [IX_tblPaymentResponse_TransactionStatus] ON [epay].[tblPaymentResponse]([TransactionStatus])

GO
CREATE INDEX [IX_tblPaymentResponse_PendingPaymentId] ON [epay].[tblPaymentResponse]([PendingPaymentId])

GO
CREATE INDEX [IX_tblPaymentResponse_GatewayTransactionId] ON [epay].[tblPaymentResponse]([GatewayTransactionId])
