﻿CREATE TABLE [epay].[tblGatewayCustomer]
(
	[UserId] UNIQUEIDENTIFIER NOT NULL,
	[GatewayName] NVARCHAR(50) NOT NULL,
	[TokenCustomerId] NVARCHAR(50) NOT NULL, 
    CONSTRAINT [PK_tblGatewayCustomer] PRIMARY KEY ([UserId], [GatewayName]), 
    CONSTRAINT [FK_tblGatewayCustomer_ToUserData] FOREIGN KEY ([UserId]) REFERENCES dbo.tblUserData([UserId])
)
