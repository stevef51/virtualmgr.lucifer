﻿CREATE TABLE [epay].[tblPendingPayment]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY,
	[OrderBy] INT NOT NULL,
    [Status] INT NOT NULL, 
	[StatusDateUtc] DATETIME NOT NULL,
	[GatewayName] NVARCHAR(50) NOT NULL,
    [GatewayContext] NVARCHAR(MAX) NULL, 
	[CreatedDateUtc] DATETIME NOT NULL,
	[ActivatedDateUtc] DATETIME NULL,
	[CancelledDateUtc] DATETIME NULL,
	[PaidInFullDateUtc] DATETIME NULL,
	[DueDateUtc] DATETIME NULL,
	[LastFailedDateUtc] DATETIME NULL,
	[FullAmount] DECIMAL(18, 2) NOT NULL DEFAULT 0,
	[DiscountAmount] DECIMAL(18, 2) NOT NULL DEFAULT 0,
	[DueAmount] DECIMAL(18, 2) NOT NULL,
	[Currency] NVARCHAR(6) NOT NULL,
	[OrderId] UNIQUEIDENTIFIER NOT NULL, 
    [Description] NVARCHAR(256) NOT NULL, 
    [FromUtc] DATETIME NULL, 
    [ToUtc] DATETIME NULL, 
    [Sandbox] BIT NOT NULL, 
    [UserId] UNIQUEIDENTIFIER NOT NULL, 
    [Quantity] DECIMAL(18, 10) NULL, 
    [Archived] BIT NOT NULL DEFAULT 0, 
    [InvoiceNumber] INT NULL, 
    CONSTRAINT [FK_tblPendingPayment_ToUserData] FOREIGN KEY ([UserId]) REFERENCES dbo.tblUserData([UserId])    
)

GO
CREATE INDEX [IX_tblPendingPayment_CreatedDateUtc] ON [epay].[tblPendingPayment] ([CreatedDateUtc])

GO
CREATE INDEX [IX_tblPendingPayment_CancelledDateUtc] ON [epay].[tblPendingPayment] ([CancelledDateUtc])

GO
CREATE INDEX [IX_tblPendingPayment_PaidInFullDateUtc] ON [epay].[tblPendingPayment] ([PaidInFullDateUtc])

GO
CREATE INDEX [IX_tblPendingPayment_DueDateUtc] ON [epay].[tblPendingPayment] ([DueDateUtc])

GO
CREATE INDEX [IX_tblPendingPayment_OrderId] ON [epay].[tblPendingPayment] ([OrderId])

GO
CREATE INDEX [IX_tblPendingPayment_Status] ON [epay].[tblPendingPayment] ([Status])

GO
CREATE INDEX [IX_tblPendingPayment_StatusDateUtc] ON [epay].[tblPendingPayment] ([StatusDateUtc])
