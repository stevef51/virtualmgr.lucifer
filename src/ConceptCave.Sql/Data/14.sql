﻿-- EVS-1089 addition of default data


ALTER TABLE stock.tblProduct DROP CONSTRAINT [FK_tblProduct_TotblProductType]
GO

DELETE FROM stock.tblProductType

Insert INTO [stock].tblProductType (Id, [Name], Units) VALUES (1, 'Item', 1)
Insert INTO [stock].tblProductType (Id, [Name], Units) VALUES (2, 'Length', 2)
Insert INTO [stock].tblProductType (Id, [Name], Units) VALUES (3, 'Area', 3)
Insert INTO [stock].tblProductType (Id, [Name], Units) VALUES (4, 'Volume', 4)

Update [stock].tblProduct set ProductTypeId = 1

ALTER TABLE stock.tblProduct ADD CONSTRAINT [FK_tblProduct_TotblProductType] FOREIGN KEY ([ProductTypeId]) REFERENCES [stock].[tblProductType]([Id])
GO

-- Also updating roles as some roles have previously been missed being added in

MERGE INTO Roles t USING (VALUES 
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', 'c9cfae8a-37da-4561-b9d5-2017af0bb841', 'User'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', 'ccf45535-3c4d-44ad-81b8-6b27d882dce2', 'Navigation'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', 'b477fbb6-ab92-4b3c-a360-8d94e3a623a5', 'ResourceManager'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', 'c07e81b4-3423-48b1-b855-1cba151f52c1', 'PublishingManager'), 
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', '5fbb1f2d-b85c-43fd-878c-a372cb1c3bf7', 'CoreReports'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', 'F83F2539-0AA4-4E85-8330-8331B1A96FAB', 'OrganisationRoles'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', '6E26F79F-DDF0-4BAA-A3B1-5211CD9E35B4', 'OrganisationHierarchy'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', '102aebf1-c1db-4b8c-9f2f-bccec0e1cec9', 'LabelManager'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', '592C0E4F-E0A8-480D-AD97-5AD424AB630F', 'CompanyManager'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', 'ef72aa97-c769-4466-896a-f9ef71c20386', 'MembershipManager'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', 'FB0B9537-D759-4012-8EF1-E732B1A50800', 'MembershipImportManager'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', 'BE7F2A15-E48D-4A55-B5E2-86EA72387C51', 'MembershipTypeManager'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', '83A215E5-C48C-4625-AD1C-45B684B16532', 'MediaManager'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', '579B9CE1-2C9B-4DB2-9E02-D804513EA37C', 'Settings'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', '0D632D32-A698-4974-B1DE-302493437E52', 'TaskTypeManager'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', 'EE06715D-C462-449A-8312-678E0E2DA03C', 'RosterManager'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', '8A665395-467A-4E84-9F56-43BE21B7CAC0', 'Roles'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', '49FFA168-7C8A-4417-9450-7BAFDB9183F1', 'NewsFeed'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', '69D2D548-CA38-4A30-8EB2-2201BE22C18E', 'LanguageManager'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', '2B15FD1D-628B-4820-A9AF-61D8876145B1', 'ScheduleManager'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', 'CD776A53-5659-4616-B846-994F0C909296', 'ScheduleApprovalManager'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', '3A812317-CA34-49D6-AB8C-C86E3CD71572', 'CronManager'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', 'E917FA1A-C307-4BC3-A8C2-25211EEE8B6E', 'NotLimitedToCompany'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', '5E4566FE-7945-4877-9137-103F4927298A', 'NotLimitedToHierarchy'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', '2FA782E1-3150-4309-BE52-CD8116881591', 'EndOfDayManager'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', '11403898-AE62-4E8C-B451-731E1CEAE022', 'Developer'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', '2A3184CD-74D1-4F48-BB8A-EF663832929E', 'ViewReports'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', '628D31DD-71DF-42CC-88A1-59BD3B412AD1', 'DeviceManagement'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', '8EBEACD8-549D-454B-A3F6-3AFD840B0F2F', 'WorkLoading'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', 'ED3B9B36-C35E-4594-BD23-F0D4E8B9C706', 'TaskUtilityOperationsAssignTasksToOthers'),	
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', '49C54814-A5A1-4382-B308-2C7A2A4F353F', 'TaskUtilityOperationsAcquireTasksFromOthers'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', '9B6BEF9A-D9EE-444E-8E2C-D5E3651F1231', 'TaskUtilityOperationsCancel'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', '9F4E91C3-E177-4C14-B6C2-82218C688174', 'TaskUtilityOperationsFinishByManagement'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', 'E2E1E00A-B2DD-4A23-8C1C-72D08E094396', 'TaskUtilityOperationsDelete'),		
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', '16718F03-8525-45ED-B1BF-FD301577B56C', 'Report Tickets'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', '42F2EAF0-4510-4F5D-81EE-6F8842889F9B', 'UserBillingType'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', 'E671F91E-B00B-42A9-9FBF-18965FE0F7C5', 'InvoiceAdmin'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', '0BD27B6B-31CD-45C9-8A8D-798A348FAD35', 'TabletAdmin'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', 'C709E776-0B37-4522-936E-967B8CC2B783', 'AssetAdmin'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', '40CA2CD1-33F7-4326-9964-EDBF6B2BB2F9', 'AssetTypeAdmin'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', 'F12B3694-4BB3-4DFB-914D-C90D4ADFD1A5', 'TimesheetItemTypeManager'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', '1EB2F4CC-D54B-450A-A2E8-A33D8196D3F1', 'FacilityStructureAdmin'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', '6A6ADEE8-CBAF-409B-8E16-70DEA67CC4EF', 'ProductManager'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', 'B6FE0146-2D36-4D00-A4FB-DBDB65DDB8F2', 'CanSideStepHierarchy')
	) AS s (ApplicationId, RoleId, RoleName) ON t.ApplicationId = s.ApplicationId AND t.RoleId = s.RoleId
	WHEN MATCHED THEN UPDATE SET RoleName = s.RoleName
	WHEN NOT MATCHED THEN INSERT (ApplicationId, RoleId, RoleName) VALUES (s.ApplicationId, s.RoleId, s.RoleName);