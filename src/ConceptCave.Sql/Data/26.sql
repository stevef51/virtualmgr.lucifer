-- Delete Fact_Tasks unused columns & other columns

IF COL_LENGTH('dw.Fact_Tasks','OffsetDateTimeCreated') IS NOT NULL
	ALTER TABLE dw.Fact_Tasks DROP COLUMN OffsetDateTimeCreated

IF COL_LENGTH('dw.Fact_Tasks','OffsetDateTimeStarted') IS NOT NULL
	ALTER TABLE dw.Fact_Tasks DROP COLUMN OffsetDateTimeStarted

IF COL_LENGTH('dw.Fact_Tasks','OffsetDateTimeCompleted') IS NOT NULL
	ALTER TABLE dw.Fact_Tasks DROP COLUMN OffsetDateTimeCompleted

IF COL_LENGTH('dw.Fact_Tasks','OffsetStatusDate') IS NOT NULL
	ALTER TABLE dw.Fact_Tasks DROP COLUMN OffsetStatusDate

IF COL_LENGTH('dw.Dim_TaskStatus','TenantId') IS NOT NULL
	ALTER TABLE dw.Dim_TaskStatus DROP COLUMN TenantId
