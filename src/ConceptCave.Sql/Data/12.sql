﻿-- EVS-1016 addition of the productmanager role

MERGE INTO Roles t USING (VALUES 
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', '6A6ADEE8-CBAF-409B-8E16-70DEA67CC4EF', 'ProductManager')
	) AS s (ApplicationId, RoleId, RoleName) ON t.ApplicationId = s.ApplicationId AND t.RoleId = s.RoleId
	WHEN MATCHED THEN UPDATE SET RoleName = s.RoleName
	WHEN NOT MATCHED THEN INSERT (ApplicationId, RoleId, RoleName) VALUES (s.ApplicationId, s.RoleId, s.RoleName);