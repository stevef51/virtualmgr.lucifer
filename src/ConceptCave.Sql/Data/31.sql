﻿MERGE INTO dbo.tblWorkingDocumentStatus p USING (VALUES 
		(6, 'Cancelled')
	) AS s ([Id], [Text]) ON p.Id = s.Id
	WHEN NOT MATCHED THEN INSERT (Id, [Text]) VALUES (s.Id, s.[Text]);