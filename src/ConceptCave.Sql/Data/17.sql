-- EVS-1129 UI for entering length x width to get at an area for an order item

MERGE INTO [stock].tblProductType p USING (VALUES 
		(1, 'Item', 1),
		(2, 'Length', 2),
		(3, 'Area', 3),
		(4, 'Volume', 4),
		(5, 'Length * Width', 5)
	) AS s ([Id], [Name], Units) ON p.Id = s.Id
	WHEN MATCHED THEN UPDATE SET Units = p.Units
	WHEN NOT MATCHED THEN INSERT (Id, [Name], Units) VALUES (s.Id, s.[Name], s.Units);

update tblCompletedWorkingDocumentFact set ContextType = 1 where IsUserContext = 1
update tblCompletedWorkingDocumentPresentedFact set ContextType = 1 where IsUserContext = 1
