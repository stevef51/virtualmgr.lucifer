﻿-- EVS-1118 Product discount feature

MERGE INTO Roles t USING (VALUES 
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', 'A8ACE172-712C-4EAB-89E2-6337DD47C9E5', 'JsFunctionManager')
	) AS s (ApplicationId, RoleId, RoleName) ON t.ApplicationId = s.ApplicationId AND t.RoleId = s.RoleId
	WHEN MATCHED THEN UPDATE SET RoleName = s.RoleName
	WHEN NOT MATCHED THEN INSERT (ApplicationId, RoleId, RoleName) VALUES (s.ApplicationId, s.RoleId, s.RoleName);
