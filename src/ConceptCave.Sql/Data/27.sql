﻿-- Move site QR codes over to QR Code area
IF COL_LENGTH('dbo.tblUserData','QRCode') IS NOT NULL
BEGIN
	-- Copy the codes across
	INSERT INTO dbo.tblQRCode (QRCode, UserId) SELECT QRCode, UserId FROM dbo.tblUserData WHERE QRCode IS NOT NULL

	ALTER TABLE dbo.tblUserData DROP COLUMN QRCode
END

MERGE INTO Roles t USING (VALUES 
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', 'CF08B6FF-BDFD-41C9-8315-AE6CA8622327', 'QRCodeAdmin')
	) AS s (ApplicationId, RoleId, RoleName) ON t.ApplicationId = s.ApplicationId AND t.RoleId = s.RoleId
	WHEN MATCHED THEN UPDATE SET RoleName = s.RoleName
	WHEN NOT MATCHED THEN INSERT (ApplicationId, RoleId, RoleName) VALUES (s.ApplicationId, s.RoleId, s.RoleName);
