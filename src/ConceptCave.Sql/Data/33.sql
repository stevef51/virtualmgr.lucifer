﻿-- Copy across roles and users from old asp.net membership to the asp.net identity table

-- roles first
MERGE INTO AspNetRoles AS destination USING (
	SELECT RoleId, RoleName
FROM Roles
) AS source ON source.RoleId = destination.Id WHEN MATCHED THEN
	UPDATE SET destination.Name = source.RoleName, destination.NormalizedName = UPPER(source.RoleName)
WHEN NOT MATCHED THEN
	INSERT (Id, [Name], NormalizedName) VALUES (source.RoleId, source.RoleName, UPPER(source.RoleName));

-- now the users side of things
IF EXISTS(SELECT TOP 1
	Id
FROM AspNetUsers) 
BEGIN
	PRINT N'Users already exist in AspNetUsers, abandoning migration of users across to identity tables';
END
ELSE BEGIN
	PRINT N'No users in AspNetUsers, migrating users across to identity tables';

	-- Disable the tblUserData insert trigger temporarily
	DISABLE TRIGGER dbo.InsertIntoTblUserData ON dbo.AspNetUsers;

	BEGIN TRANSACTION MigrateUsers
	/* Migrate users */
	INSERT INTO AspNetUsers
		(
		Id,
		UserName,
		NormalizedUserName,
		PasswordHash,
		SecurityStamp,
		EmailConfirmed,
		PhoneNumber,
		PhoneNumberConfirmed,
		TwoFactorEnabled,
		LockoutEnd,
		LockoutEnabled,
		AccessFailedCount,
		Email,
		NormalizedEmail)
	SELECT
		Users.UserId,
		Users.UserName,
		UPPER(Users.UserName),
		(Memberships.Password+'|'+CAST(Memberships.PasswordFormat as varchar)+'|'+Memberships.PasswordSalt),
		NewID(),
		1,
		NULL,
		0,
		0,
		CASE WHEN Memberships.IsLockedOut = 1 THEN DATEADD(YEAR, 1000, SYSUTCDATETIME()) ELSE NULL END,
		1,
		0,
		Memberships.Email,
		UPPER(Memberships.Email)
	FROM Users
		LEFT OUTER JOIN Memberships ON Memberships.ApplicationId = Users.ApplicationId
			AND Users.UserId = Memberships.UserId
		LEFT OUTER JOIN AspNetUsers ON Memberships.UserId = AspNetUsers.Id
	WHERE AspNetUsers.Id IS NULL;

	ENABLE TRIGGER dbo.InsertIntoTblUserData ON dbo.AspNetUsers;

	IF @@ERROR <> 0 
	BEGIN
		ROLLBACK TRANSACTION MigrateUsers
	END
    ELSE 
    BEGIN
		COMMIT TRANSACTION MigrateUsers
	END
END

-- and now map the roles to the users
MERGE INTO AspNetUserRoles AS destination USING (
	SELECT UserId, RoleId
FROM UsersInRoles
) AS source ON source.RoleId = destination.RoleId WHEN NOT MATCHED THEN
	INSERT VALUES (source.UserId, source.RoleId);

-- we need to populate the aspnetuserid fields in tbluserdata with the new ids
UPDATE tblUserData SET AspNetUserId = UserId


