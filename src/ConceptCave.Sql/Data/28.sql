﻿-- EVS-1288 Fix up Static Beacon GPS Locations to have valid Altitude, Level and FloorId
-- This will run through all Static Beacons and update their GpsLocation records to fill in the Altitude, Level and FacilityStructureFloorId

begin tran

declare c cursor local for
	select 
		Id, 
		StaticLocationId, 
		FacilityStructureId, 
		StaticHeightFromFloor
	from
		asset.tblBeacon
	where
		StaticLocationId is not null

open c

declare @id nvarchar(255), @slid bigint, @fsid bigint, @shff decimal(18,2)

fetch next from c into 
	@id,
	@slid,
	@fsid,
	@shff
while @@FETCH_STATUS = 0
begin
	declare @onbuildingid bigint, @onfloorid bigint, @onlevel int
	select @onfloorid = OnFloorId, @onlevel = [Level] from odata.FacilityStructure where id = @fsid
	select @onbuildingid = buildingid from odata.FSBuildingFloors where id = @onfloorid

	declare @floorheight decimal(18,2)
	-- Account for basement levels (-ve altitude)
	select @floorheight = ISNULL(-SUM(FloorHeight), 0) FROM odata.FSBuildingFloors where buildingid = @onbuildingid and [level] < 0

	-- and ground levels and above
	select @floorheight = @floorheight + ISNULL(SUM(FloorHeight), 0) FROM odata.FSBuildingFloors where buildingid = @onbuildingid and [level] < @onlevel - 1 


	declare @altitude decimal(18,2)
	select @altitude = @floorheight + @shff

	print @id + ' on level ' + convert(nvarchar, @onlevel) + ' altitude ' + convert(nvarchar, @altitude)

	update asset.tblGpsLocation set altitude = @altitude, [floor] = @onlevel, facilitystructurefloorid = @onfloorid where id = @slid

	fetch next from c into 
		@id,
		@slid,
		@fsid,
		@shff
end

close c
deallocate c

commit

