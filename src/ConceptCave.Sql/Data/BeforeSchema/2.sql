﻿-- EVS-1101 Drop possible NotificationPrompt colum from Tasks
IF COL_LENGTH('gce.tblProjectJobTask','NotificationPrompt') IS NOT NULL
BEGIN
	ALTER TABLE gce.tblProjectJobTask DROP COLUMN NotificationPrompt
END
