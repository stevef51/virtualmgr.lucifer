﻿-- Delete redundant tables/views/stored procs

IF OBJECT_ID('asset.tblTabletBeaconDistance', 'U') IS NOT NULL
	DROP TABLE asset.tblTabletBeaconDistance

IF OBJECT_ID('asset.tblTabletLocation', 'U') IS NOT NULL
	DROP TABLE asset.tblTabletLocation

IF OBJECT_ID('dbo.tblCurrency', 'U') IS NOT NULL
	DROP TABLE dbo.tblCurrency

IF OBJECT_ID('dbo.tblErrorLog', 'U') IS NOT NULL
	DROP TABLE dbo.tblErrorLog

IF OBJECT_ID('dbo.tblSqlServers', 'U') IS NOT NULL
	DROP TABLE dbo.tblSqlServers

IF OBJECT_ID('odata.AssetTracker', 'V') IS NOT NULL
	DROP VIEW odata.AssetTracker

IF OBJECT_ID('odata.ErrorLog', 'V') IS NOT NULL
	DROP VIEW odata.ErrorLog

IF OBJECT_ID('dbo.MakeDataSyncTable', 'P') IS NOT NULL
	DROP PROCEDURE dbo.MakeDataSyncTable

