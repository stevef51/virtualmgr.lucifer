-- Drop DataSync tables

DECLARE TB CURSOR FOR SELECT table_name FROM INFORMATION_SCHEMA.tables  
	WHERE table_name like '%_DataSync' or table_name like '_DataSync%'
OPEN TB  

DECLARE @tn NVARCHAR(256),
	@c NVARCHAR(256)
FETCH NEXT FROM TB INTO @tn

WHILE @@FETCH_STATUS = 0  
BEGIN  
    SELECT @c = 'DROP TABLE ' + @tn
	--PRINT @c
	EXEC sp_executesql @c
	
	FETCH NEXT FROM TB INTO @tn
END

CLOSE TB
DEALLOCATE TB

