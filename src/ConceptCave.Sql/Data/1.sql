﻿-- Setup default Currencies
MERGE INTO [mtc].[tblCurrency] tgt USING (VALUES 
	(N'AUD', N'Australian Dollar'),
	(N'USD', N'US Dollar'),
	(N'GBP', N'British Pound')
) AS src (code, [description]) ON (tgt.code = src.code)
WHEN NOT MATCHED THEN INSERT (code, [description]) VALUES (src.code, src.description);


