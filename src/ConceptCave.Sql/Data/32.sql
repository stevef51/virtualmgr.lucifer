﻿-- Fix CompletedWorkingDocumentFact.IsUserContext, ContextType

-- tblCompletedWorkingDocumentPresentedFact records have their IsUserContext and ContextType field set correctly (see fix in ReportingTableWriter.cs)
UPDATE tblCompletedWorkingDocumentFact 
	SET 
		tblCompletedWorkingDocumentFact.ContextType = cwdpf.ContextType,
		tblCompletedWorkingDocumentFact.IsUserContext = cwdpf.IsUserContext
	FROM tblCompletedWorkingDocumentFact cdwf
	INNER JOIN tblCompletedWorkingDocumentPresentedFact cwdpf ON cwdpf.WorkingDocumentId = cdwf.WorkingDocumentId

