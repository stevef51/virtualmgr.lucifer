﻿-- EVS-686 Asset Admin UI
MERGE INTO Roles t USING (VALUES 
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', 'C709E776-0B37-4522-936E-967B8CC2B783', 'AssetAdmin'),
		('c2a59fc5-96fd-4881-abeb-9b753db84d3c', '40CA2CD1-33F7-4326-9964-EDBF6B2BB2F9', 'AssetTypeAdmin')
	) AS s (ApplicationId, RoleId, RoleName) ON t.ApplicationId = s.ApplicationId AND t.RoleId = s.RoleId
	WHEN MATCHED THEN UPDATE SET RoleName = s.RoleName
	WHEN NOT MATCHED THEN INSERT (ApplicationId, RoleId, RoleName) VALUES (s.ApplicationId, s.RoleId, s.RoleName);


	