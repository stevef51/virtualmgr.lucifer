﻿CREATE TABLE [asset].[tblTabletProfile]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Name] NVARCHAR(50) NOT NULL, 
    [AutoLogoutShortSecs] INT NULL, 
    [AutoLogoutLongSecs] INT NULL, 
    [AllowNumberPadLogin] BIT NOT NULL DEFAULT 0, 
    [ConfirmLogout] INT NOT NULL DEFAULT 0, 
    [EnableFMA] BIT NOT NULL DEFAULT 0, 
    [SettingsPassword] NVARCHAR(20) NULL, 
    [LogoutPassword] NVARCHAR(20) NULL, 
    [Archived] BIT NOT NULL DEFAULT 0, 
    [EnableEnvironmentalSensing] BIT NOT NULL DEFAULT 0, 
    [LoginPhotoEvery] INT NOT NULL DEFAULT 0
)
