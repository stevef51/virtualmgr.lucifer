﻿CREATE TABLE [asset].[tblAssetTypeCalendarTask]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY,
	[Archived] BIT NOT NULL,
	[AssetTypeCalendarId] INT NOT NULL,
	[TaskTypeId] UNIQUEIDENTIFIER NOT NULL,
	[HierarchyRoleId] INT NOT NULL, 
    [TaskSiteId] UNIQUEIDENTIFIER NOT NULL, 
    [TaskLengthInDays] INT NULL, 
    [TaskLevel] INT NULL, 
	[TaskName] NVARCHAR(4000),
    [TaskDescription] NVARCHAR(MAX) NULL, 
    [StartTimeLocal] TIME NULL , 
    CONSTRAINT [FK_tblAssetTypeCalendarTask_ToAssetTypeCalendar] FOREIGN KEY ([AssetTypeCalendarId]) REFERENCES [asset].[tblAssetTypeCalendar]([Id]),
    CONSTRAINT [FK_tblAssetTypeCalendarTask_ToTaskType] FOREIGN KEY ([TaskTypeId]) REFERENCES [gce].[tblProjectJobTaskType]([Id]),
    CONSTRAINT [FK_tblAssetTypeCalendarTask_ToHierarchyRole] FOREIGN KEY ([HierarchyRoleId]) REFERENCES [dbo].[tblHierarchyRole]([Id]),
)
