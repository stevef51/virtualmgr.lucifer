﻿CREATE TABLE [asset].[tblFacilityStructure]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY,
	[Archived] BIT NOT NULL,
	[StructureType] INT NOT NULL,
	[ParentId] INT NULL,
	[Name] NVARCHAR(255) NULL,
	[Description] NVARCHAR(1024) NULL, 
	[Level] INT NULL,
    [AddressId] INT NULL, 
    [SiteId] UNIQUEIDENTIFIER NULL, 
    [FloorPlanId] INT NULL, 
    [FloorHeight] DECIMAL(18, 2) NULL, 
    [MediaId] UNIQUEIDENTIFIER NULL, 
    [HierarchyId] INT NULL, 
    [SortOrder] INT NOT NULL DEFAULT 0, 
    CONSTRAINT [FK_tblFacilityStructure_ToFacilityStructure] FOREIGN KEY ([ParentId]) REFERENCES [asset].[tblFacilityStructure]([Id]), 
    CONSTRAINT [FK_tblFacilityStructure_ToAddress] FOREIGN KEY ([AddressId]) REFERENCES [asset].[tblAddress]([Id]),
	CONSTRAINT [FK_tblFacilityStructure_ToSite] FOREIGN KEY ([SiteId]) REFERENCES [dbo].[tblUserData]([UserId]), 
    CONSTRAINT [FK_tblFacilityStructure_ToFloorPlan] FOREIGN KEY ([FloorPlanId]) REFERENCES [asset].[tblFloorPlan]([Id]), 
    CONSTRAINT [FK_tblFacilityStructure_TotblMedia] FOREIGN KEY ([MediaId]) REFERENCES [tblMedia]([Id]),
	CONSTRAINT [FK_tblFacilityStructure_TotblHierarchy] FOREIGN KEY ([HierarchyId]) REFERENCES [dbo].[tblHierarchy]([Id])
)

GO

CREATE INDEX [IX_tblFacilityStructure_Name] ON [asset].[tblFacilityStructure] ([Name])


GO

CREATE INDEX [IX_tblFacilityStructure_StructureType] ON [asset].[tblFacilityStructure] ([StructureType])

GO

CREATE INDEX [IX_tblFacilityStructure_SiteId] ON [asset].[tblFacilityStructure] ([SiteId])
GO

CREATE INDEX [IX_tblFacilityStructure_ParentId] ON [asset].[tblFacilityStructure] ([ParentId])

