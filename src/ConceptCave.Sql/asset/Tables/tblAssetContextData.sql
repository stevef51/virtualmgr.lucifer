﻿CREATE TABLE [asset].[tblAssetContextData]
(
	[AssetId] INT NOT NULL,
	[AssetTypeContextPublishedResourceId] INT              NOT NULL,
    [CompletedWorkingDocumentId]         UNIQUEIDENTIFIER NOT NULL,
	CONSTRAINT [PK_tblAssetContextData] PRIMARY KEY CLUSTERED ([AssetId] ASC, [AssetTypeContextPublishedResourceId] ASC, [CompletedWorkingDocumentId] ASC),
	CONSTRAINT [FK_tblAssetContextData_tblCompletedWorkingDocumentFact] FOREIGN KEY ([CompletedWorkingDocumentId]) REFERENCES [dbo].[tblCompletedWorkingDocumentFact] ([WorkingDocumentId]),
    CONSTRAINT [FK_tblAssetContextData_tblAsset] FOREIGN KEY ([AssetId]) REFERENCES [asset].[tblAsset] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_tblAssetContextData_tblAssetTypeContextPublishedResource] FOREIGN KEY ([AssetTypeContextPublishedResourceId]) REFERENCES [asset].[tblAssetTypeContextPublishedResource] ([Id]) ON DELETE CASCADE
)
