﻿CREATE TABLE [asset].[tblTablet]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Archived] BIT NOT NULL DEFAULT 0, 
	[UUID] NVARCHAR(36) NULL,
	[Name] NVARCHAR(256) NULL,
    [TabletProfileId] INT NULL, 
    [SiteId] UNIQUEIDENTIFIER NULL, 
    CONSTRAINT [FK_tblTablet_TotblTabletProfile] FOREIGN KEY ([TabletProfileId]) REFERENCES [asset].[tblTabletProfile]([Id]), 
    CONSTRAINT [FK_tblTablet_ToTabletUUID] FOREIGN KEY ([UUID]) REFERENCES [asset].[tblTabletUUID]([UUID])
)


GO

CREATE INDEX [IX_tblTablet_Name] ON [asset].[tblTablet] ([Name])

GO

CREATE INDEX [IX_tblTablet_Archived] ON [asset].[tblTablet] ([Archived])
