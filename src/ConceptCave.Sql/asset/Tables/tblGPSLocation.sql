﻿CREATE TABLE [asset].[tblGPSLocation]
(
	[Id] BIGINT NOT NULL PRIMARY KEY IDENTITY,
	[LocationType] INT NOT NULL,
	[TabletUUID] NVARCHAR(36) NULL,
    [TimestampUtc] DATETIME NULL, 
	[LastTimestampUtc] DATETIME NULL,
	[TimestampCount] INT NULL,
	[Latitude] DECIMAL(18, 10) NOT NULL, 
    [Longitude] DECIMAL(18, 10) NOT NULL, 
    [AccuracyMetres] DECIMAL(18, 2) NOT NULL, 
	[Altitude] DECIMAL(18,10) NULL,
	[AltitudeAccuracyMetres] DECIMAL(18,2) NULL,
    [Floor] INT NULL, 
    [FacilityStructureFloorId] INT NULL, 
    [StaticBeaconId] NVARCHAR(50) NULL, 
    [TaskId] UNIQUEIDENTIFIER NULL,
	[Archived] BIT DEFAULT(0) NOT NULL
)

GO

CREATE INDEX [IX_tblGPSLocation_LocationType] ON [asset].[tblGPSLocation] ([LocationType])

GO

CREATE INDEX [IX_tblGPSLocation_TimestampUtc] ON [asset].[tblGPSLocation] ([TimestampUtc])
GO
CREATE INDEX [IX_tblGPSLocation_LastTimestampUtc] ON [asset].[tblGPSLocation] ([LastTimestampUtc])
GO

CREATE INDEX [IX_tblGPSLocation_TabletUUID] ON [asset].[tblGPSLocation] ([TabletUUID])
GO

CREATE INDEX [IX_tblGPSLocation_Archived] ON [asset].[tblGPSLocation] ([Archived])

GO
