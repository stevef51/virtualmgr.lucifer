﻿CREATE TABLE [asset].[tblAssetTypeContextPublishedResource]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [AssetTypeId] INT NOT NULL, 
    [PublishingGroupResourceId] INT NOT NULL, 
    CONSTRAINT [FK_tblAssetTypeContextPublishedResource_TotblAssetType] FOREIGN KEY ([AssetTypeId]) REFERENCES [asset].[tblAssetType]([Id]), 
    CONSTRAINT [FK_tblAssetTypeContextPublishedResource_TotblPublishingGroupResource] FOREIGN KEY ([PublishingGroupResourceId]) REFERENCES [tblPublishingGroupResource]([Id]), 
    CONSTRAINT [AK_tblAssetTypeContextPublishedResource_AssetTypeIdAndPGR] UNIQUE NONCLUSTERED ([AssetTypeId] ASC, [PublishingGroupResourceId] ASC)
)
