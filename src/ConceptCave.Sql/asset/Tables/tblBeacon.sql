﻿CREATE TABLE [asset].[tblBeacon]
(
	[Id] NVARCHAR(50) NOT NULL PRIMARY KEY,
	[Latitude] DECIMAL(18,10) NULL,
    [Longitude] DECIMAL(18,10) NULL,
    [AccuracyMetres] DECIMAL(18,2) NULL, 
	[StaticLocationId] BIGINT NULL,
    [LastContactUtc] DATETIME NULL, 
    [LastButtonPressedUtc] DATETIME NULL, 
    [FacilityStructureId] INT NULL, 
    [AssetId] INT NULL, 
    [Name] NVARCHAR(255) NULL, 
    [BeaconConfigurationId] INT NULL, 
    [StaticHeightFromFloor] DECIMAL(18, 2) NULL,
    [LastGPSLocationId] BIGINT NULL, 
    [BatteryPercent] INT NULL, 
    [TaskTypeId] UNIQUEIDENTIFIER NULL, 
    [UserId] UNIQUEIDENTIFIER NULL, 
    CONSTRAINT [FK_tblBeacon_ToFacilityStructure] FOREIGN KEY ([FacilityStructureId]) REFERENCES [asset].[tblFacilityStructure]([Id]),
    CONSTRAINT [FK_tblBeacon_ToAsset] FOREIGN KEY ([AssetId]) REFERENCES [asset].[tblAsset]([Id]),
    CONSTRAINT [FK_tblBeacon_ToStaticLocation] FOREIGN KEY ([StaticLocationId]) REFERENCES [asset].[tblGPSLocation]([Id]),
    CONSTRAINT [FK_tblBeacon_ToBestEstimateLocation] FOREIGN KEY ([LastGPSLocationId]) REFERENCES [asset].[tblGPSLocation]([Id]),
	CONSTRAINT [FK_tblBeacon_ToUserData] FOREIGN KEY ([UserId]) REFERENCES [tblUserData]([UserId]),
    CONSTRAINT [FK_tblBeacon_ToTaskType] FOREIGN KEY ([TaskTypeId]) REFERENCES [gce].[tblProjectJobTaskType]([Id])

)

GO

CREATE INDEX [IX_tblBeacon_LastContactUtc] ON [asset].[tblBeacon] ([LastContactUtc])

GO

CREATE INDEX [IX_tblBeacon_LastButtonPressedUtc] ON [asset].[tblBeacon] ([LastButtonPressedUtc])

GO

CREATE UNIQUE INDEX [IX_tblBeacon_AssetId] ON [asset].[tblBeacon] ([AssetId]) WHERE AssetId IS NOT NULL