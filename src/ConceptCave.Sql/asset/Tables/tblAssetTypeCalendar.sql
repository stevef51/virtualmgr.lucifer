﻿CREATE TABLE [asset].[tblAssetTypeCalendar]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
	[Archived] BIT NOT NULL,
    [Name] NVARCHAR(250) NOT NULL, 
    [AssetTypeId] INT NOT NULL, 
    [ScheduleExpression] NVARCHAR(MAX) NOT NULL, 
    [TimeZone] NVARCHAR(50) NULL , 
    CONSTRAINT [FK_tblAssetTypeCalendar_ToAssetType] FOREIGN KEY ([AssetTypeId]) REFERENCES [asset].[tblAssetType]([Id])
)

GO

CREATE INDEX [IX_tblAssetTypeCalendar_Archived] ON [asset].[tblAssetTypeCalendar] ([Archived])

GO

CREATE INDEX [IX_tblAssetTypeCalendar_AssetTypeId] ON [asset].[tblAssetTypeCalendar] ([AssetTypeId])
