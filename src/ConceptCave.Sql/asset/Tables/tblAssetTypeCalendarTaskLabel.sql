﻿CREATE TABLE [asset].[tblAssetTypeCalendarTaskLabel]
(
	[AssetTypeCalendarTaskId] INT NOT NULL,
	[LabelId] UNIQUEIDENTIFIER NOT NULL, 
    CONSTRAINT [FK_tblAssetTypeCalendarTaskLabel_ToLabel] FOREIGN KEY ([LabelId]) REFERENCES [dbo].[tblLabel]([Id]),
    CONSTRAINT [FK_tblAssetTypeCalendarTaskLabel_ToAssetTypeCalendarTask] FOREIGN KEY ([AssetTypeCalendarTaskId]) REFERENCES [asset].[tblAssetTypeCalendarTask]([Id]), 
    CONSTRAINT [PK_tblAssetTypeCalendarTaskLabel] PRIMARY KEY ([AssetTypeCalendarTaskId], [LabelId])
)



GO

CREATE INDEX [IX_tblAssetTypeCalendarTaskLabel_AssetTypeCalendarTaskId] ON [asset].[tblAssetTypeCalendarTaskLabel] ([AssetTypeCalendarTaskId])
