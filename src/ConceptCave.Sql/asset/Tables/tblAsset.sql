﻿CREATE TABLE [asset].[tblAsset]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
	[Archived] BIT NOT NULL DEFAULT 0,
	[AssetTypeId] INT NOT NULL,
    [Name] NVARCHAR(255) NOT NULL, 
    [DateCreated] DATETIME NOT NULL, 
    [DateDecomissioned] DATETIME NULL, 
    [FacilityStructureId] INT NULL, 
    [CompanyId] UNIQUEIDENTIFIER NULL, 
    CONSTRAINT [FK_tblAsset_TotblAssetType] FOREIGN KEY ([AssetTypeId]) REFERENCES [asset].[tblAssetType]([Id]), 
)

GO

CREATE INDEX [IX_tblAsset_AssetTypeId] ON [asset].[tblAsset] ([AssetTypeId])

GO

CREATE INDEX [IX_tblAsset_Archived] ON [asset].[tblAsset] ([Archived])
