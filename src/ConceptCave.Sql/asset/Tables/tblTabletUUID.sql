﻿CREATE TABLE [asset].[tblTabletUUID]
(
	[UUID] NVARCHAR(36) NOT NULL ,
	[TabletId] INT NULL, 
	[LastContactUtc] DATETIME NULL, 
    [Description] NVARCHAR(1024) NULL, 
	[LastGPSLocationId] BIGINT NULL, 
    [LastRange] INT NULL, 
    [LastRangeUtc] DATETIME NULL, 
    [LastUserId] UNIQUEIDENTIFIER NULL, 
    CONSTRAINT [PK_tblTabletUUID] PRIMARY KEY ([UUID]), 
    CONSTRAINT [FK_tblTabletUUID_ToTablet] FOREIGN KEY ([TabletId]) REFERENCES [asset].[tblTablet]([Id]),
    CONSTRAINT [FK_tblTabletUUID_ToUser] FOREIGN KEY ([LastUserId]) REFERENCES [dbo].[tblUserData]([UserId]),

)

GO

CREATE INDEX [IX_tblTabletUUID_TabletId] ON [asset].[tblTabletUUID] ([TabletId])
