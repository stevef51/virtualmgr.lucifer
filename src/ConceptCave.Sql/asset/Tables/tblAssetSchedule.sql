﻿CREATE TABLE [asset].[tblAssetSchedule]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [AssetId] INT NOT NULL, 
    [AssetTypeCalendarId] INT NOT NULL, 
    [LastRunUtc] DATETIME NULL, 
    [NextRunUtc] DATETIME NULL, 
    [OverrideStartTimeLocal] TIME NULL, 
    CONSTRAINT [FK_tblAssetSchedule_ToTable] FOREIGN KEY ([AssetId]) REFERENCES [asset].[tblAsset]([Id]),
    CONSTRAINT [FK_tblAssetSchedule_ToAssetTypeCalendar] FOREIGN KEY ([AssetTypeCalendarId]) REFERENCES [asset].[tblAssetTypeCalendar]([Id])
)

GO

CREATE INDEX [IX_tblAssetSchedule_NextRunUtc] ON [asset].[tblAssetSchedule] ([NextRunUtc])
GO

CREATE INDEX [IX_tblAssetSchedule_AssetId] ON [asset].[tblAssetSchedule] ([AssetId])
GO

CREATE INDEX [IX_tblAssetSchedule_AssetTypeCalendarId] ON [asset].[tblAssetSchedule] ([AssetTypeCalendarId])
