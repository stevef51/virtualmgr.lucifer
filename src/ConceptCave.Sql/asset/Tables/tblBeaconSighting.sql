﻿CREATE TABLE [asset].[tblBeaconSighting]
(
	[Id] BIGINT NOT NULL PRIMARY KEY IDENTITY,
	[TabletUUID] NVARCHAR(36) NOT NULL,
	[BeaconId] NVARCHAR(50) NOT NULL,
	[BestRange] INT NOT NULL,
	[FirstRangeUtc] DATETIME NOT NULL,
	[BestRangeUtc] DATETIME NOT NULL,
	[GoneUtc] DATETIME, 
    [GPSLocationId] BIGINT NULL, 
    [PrevStaticBeaconSightingId] BIGINT NULL, 
    [NextStaticBeaconSightingId] BIGINT NULL, 
    [LoggedInUserId] UNIQUEIDENTIFIER NULL, 
    [BatteryPercent] INT NULL, 
    CONSTRAINT [FK_tblBeaconSighting_ToTablet] FOREIGN KEY ([TabletUUID]) REFERENCES [asset].[tblTabletUUID]([UUID]),
	CONSTRAINT [FK_tblBeaconSighting_ToBeacon] FOREIGN KEY ([BeaconId]) REFERENCES [asset].[tblBeacon]([Id]),
)


GO

CREATE INDEX [IX_tblBeaconSighting_TabletUUID] ON [asset].[tblBeaconSighting] ([TabletUUID])
GO

CREATE INDEX [IX_tblBeaconSighting_BeaconId] ON [asset].[tblBeaconSighting] ([BeaconId])
GO

CREATE INDEX [IX_tblBeaconSighting_FirstRangeUtc] ON [asset].[tblBeaconSighting] (FirstRangeUtc)
GO

CREATE INDEX [IX_tblBeaconSighting_PrevStaticBeaconSightingId] ON [asset].[tblBeaconSighting] (PrevStaticBeaconSightingId)
GO

CREATE INDEX [IX_tblBeaconSighting_NextStaticBeaconSightingId] ON [asset].[tblBeaconSighting] (NextStaticBeaconSightingId)
GO

CREATE INDEX [IX_tblBeaconSighting_LoggedInUserId] ON [asset].[tblBeaconSighting] (LoggedInUserId)
GO
