﻿CREATE TABLE [asset].[tblAssetType]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
	[Archived] BIT NOT NULL DEFAULT 0,
    [Name] NVARCHAR(255) NOT NULL, 
    [CanTrack] BIT NOT NULL DEFAULT 0, 
    [Forecolor] INT NOT NULL, 
    [Backcolor] INT NOT NULL
)

GO

CREATE INDEX [IX_tblAssetType_Archived] ON [asset].[tblAssetType] ([Archived])

GO

CREATE INDEX [IX_tblAssetType_Name] ON [asset].[tblAssetType] ([Name])
