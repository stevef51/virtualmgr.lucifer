﻿CREATE TABLE [asset].[tblTabletLocationHistory]
(
	[Id] BIGINT NOT NULL PRIMARY KEY IDENTITY,
	[TabletTimestampUtc] DATETIME NOT NULL,
	[ServerTimestampUtc] DATETIME NOT NULL,
	[TabletUUID] NVARCHAR(36) NOT NULL,
	[GPSLocationId] BIGINT NULL,
    [Range] INT NULL, 
    [RangeUtc] DATETIME NULL, 
    [LoggedInUserId] UNIQUEIDENTIFIER NULL, 
    CONSTRAINT [FK_tblTabletLocationHistory_ToGPSLocation] FOREIGN KEY ([GPSLocationId]) REFERENCES [asset].[tblGPSLocation]([Id]),
)

GO
CREATE INDEX [IX_tblTabletLocationHistory_TabletTimestampUtc] ON [asset].[tblTabletLocationHistory] ([TabletTimestampUtc])
GO

CREATE INDEX [IX_tblTabletLocationHistory_ServerTimestampUtc] ON [asset].[tblTabletLocationHistory] ([ServerTimestampUtc])
GO

CREATE INDEX [IX_tblTabletLocationHistory_TabletUUID] ON [asset].[tblTabletLocationHistory] ([TabletUUID])
GO

CREATE INDEX [IX_tblTabletLocationHistory_LoggedInUserId] ON [asset].[tblTabletLocationHistory] ([LoggedInUserId])
GO
