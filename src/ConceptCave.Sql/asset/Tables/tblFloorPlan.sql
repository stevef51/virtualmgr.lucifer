﻿CREATE TABLE [asset].[tblFloorPlan]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [ImageURL] NVARCHAR(512) NOT NULL, 
    [TopLeftLatitude] DECIMAL(18, 10) NOT NULL, 
    [TopLeftLongitude] DECIMAL(18, 10) NOT NULL, 
    [TopRightLatitude] DECIMAL(18, 10) NOT NULL, 
    [TopRightLongitude] DECIMAL(18, 10) NOT NULL, 
    [BottomLeftLatitude] DECIMAL(18, 10) NOT NULL, 
    [BottomLeftLongitude] DECIMAL(18, 10) NOT NULL, 
    [IndoorAtlasFloorPlanId] NVARCHAR(50) NULL
)

GO

CREATE INDEX [IX_tblFloorPlan_IndoorAtlasFloorPlanId] ON [asset].[tblFloorPlan] ([IndoorAtlasFloorPlanId])
