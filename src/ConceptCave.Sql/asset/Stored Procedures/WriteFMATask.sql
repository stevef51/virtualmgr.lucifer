﻿CREATE PROCEDURE [asset].[WriteFMATask]
	@tabletUUID NVARCHAR(36),
	@tabletUtc DATETIME,
	@taskId UNIQUEIDENTIFIER
AS
	-- Make sure the TabletUUID exists, if not create it
	IF NOT EXISTS (SELECT UUID FROM asset.tblTabletUUID WHERE UUID = @tabletUUID)
	BEGIN
		INSERT INTO asset.tblTabletUUID (UUID) VALUES (@tabletUUID)
	END

	-- Grab the Tablets last known location and Range from it, this is where we will update any Asset Beacons to
	DECLARE 
		@tabletLastGPSLocationId BIGINT,
		@tabletLastRange INT,
		@tabletLastRangeUtc DATETIME	

	SELECT  
		@tabletLastGPSLocationId = lastGPSLocationId,
		@tabletLastRange = LastRange,
		@tabletLastRangeUtc = LastRangeUtc
	FROM 
		asset.tblTabletUUID
	WHERE 
		UUID = @tabletUUID

	-- TODO Grab the Tasks Site Location and update the Tablets last known location to it with an Immediate range (0)

RETURN 0
