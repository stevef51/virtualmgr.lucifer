﻿CREATE TYPE [asset].[FMARecord] AS TABLE
(
	RecordType INT,
	TabletUUID NVARCHAR(36),
	TabletUtc DATETIME,
	BeaconId NVARCHAR(50) NULL,
	[Range] INT NULL,
	RangeUtc DATETIME NULL,
	FirstRangeUtc DATETIME NULL,
	TaskId UNIQUEIDENTIFIER NULL,
	BatteryPercent INT NULL
);
GO

CREATE PROCEDURE [asset].[WriteFMARecords]
	@table [asset].[FMARecord] READONLY
AS
	DECLARE records CURSOR FOR SELECT * FROM @table
	
	DECLARE 
		@recordType INT,
		@tabletUUID NVARCHAR(36),
		@tabletUtc DATETIME,
		@beaconId NVARCHAR(50),
		@range INT,
		@rangeUtc DATETIME,
		@firstRangeUtc DATETIME,
		@taskId UNIQUEIDENTIFIER,
		@batteryPercent INT

	OPEN records

	FETCH NEXT FROM records INTO
		@recordType,
		@tabletUUID,
		@tabletUtc,
		@beaconId,
		@range,
		@rangeUtc,
		@firstRangeUtc,
		@taskId,
		@batteryPercent

	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF @recordType = 0
			EXEC asset.WriteFMABeacon @tabletUUID, @tabletUTC, @beaconId, @range, @rangeUtc, @firstRangeUtc, @batteryPercent

/*
		ELSE IF @recordType = 1
			EXEC asset.WriteFMABestStaticBeacon @tabletUUID, @tabletUTC, @beaconId, @range, @rangeUtc
		
		ELSE IF @recordType = 2
			EXEC asset.WriteFMATask @tabletUUID, @tabletUTC, @taskId
*/

		FETCH NEXT FROM records INTO
			@recordType,
			@tabletUUID,
			@tabletUtc,
			@beaconId,
			@range,
			@rangeUtc,
			@firstRangeUtc,
			@taskId
	END

	-- We return list of Beacons indicating their BeaconType
	SELECT Id, StaticLocationId FROM asset.tblBeacon WHERE Id IN (SELECT BeaconId FROM @table)

RETURN 0
