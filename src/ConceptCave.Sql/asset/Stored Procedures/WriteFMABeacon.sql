﻿CREATE PROCEDURE [asset].[WriteFMABeacon]
	@tabletUUID NVARCHAR(36),
	@tabletUtc DATETIME,
	@beaconId NVARCHAR(50),
	@range INT,
	@rangeUtc DATETIME,
	@firstRangeUtc DATETIME,
	@batteryPercent INT
AS
	-- Make sure the TabletUUID exists, if not create it
	IF NOT EXISTS (SELECT UUID FROM asset.tblTabletUUID WHERE UUID = @tabletUUID)
	BEGIN
		INSERT INTO asset.tblTabletUUID (UUID) VALUES (@tabletUUID)
	END

	-- Grab the Tablets last known location and Range from it, this is where we will update any Asset Beacons to
	DECLARE 
		@tabletLastGPSLocationId BIGINT,
		@tabletLastRange INT,
		@tabletLastRangeUtc DATETIME,
		@tabletLoggedInUserId UNIQUEIDENTIFIER

	SELECT  
		@tabletLastGPSLocationId = lastGPSLocationId,
		@tabletLastRange = LastRange,
		@tabletLastRangeUtc = LastRangeUtc,
		@tabletLoggedInUserId = LastUserId
	FROM 
		asset.tblTabletUUID
	WHERE 
		UUID = @tabletUUID

	-- Make sure the Beacon exists, if not create it
	IF NOT EXISTS (SELECT Id FROM asset.tblBeacon WHERE Id = @beaconId) 
	BEGIN
		INSERT INTO asset.tblBeacon (Id, LastContactUtc, BatteryPercent) VALUES (@beaconId, @rangeUtc, @batteryPercent)
	END
	ELSE
	BEGIN
		UPDATE asset.tblBeacon SET LastContactUtc = @rangeUtc, BatteryPercent = @batteryPercent WHERE Id = @beaconId
	END

	DECLARE @staticLocationId BIGINT
	SELECT @staticLocationId = StaticLocationId FROM asset.tblBeacon WHERE Id = @beaconId

	-- Update or create the Beacons most recent Sighting from the Tablet at its current location
	IF @range = 3			-- Gone - we "close" any open beacon sightings
	BEGIN
		UPDATE asset.tblBeaconSighting SET 
			GoneUtc = @rangeUtc
		WHERE 
			TabletUUID = @tabletUUID AND 
			BeaconId = @beaconId AND 
			FirstRangeUtc = @firstRangeUtc AND
			GoneUtc IS NULL				-- Should not need this filter but hey

		IF @staticLocationId IS NOT NULL
			-- Its a Static Beacon that has Gone, update the Tablets position to "Unknown" 
			UPDATE asset.tblTabletUUID SET
				LastGPSLocationId = NULL, LastRange = NULL, LastRangeUtc = NULL
			WHERE
				UUID = @tabletUUID AND 
				LastGPSLocationId = @staticLocationId		-- Only if this is the Static Beacon we saw
	END
	ELSE 
	BEGIN
		-- If a Static Beacon then update the tablets position
		IF @staticLocationId IS NOT NULL AND (@staticLocationId != @tabletLastGPSLocationId OR @tabletLastGPSLocationId IS NULL)
		BEGIN
			INSERT INTO asset.tblTabletLocationHistory 
				(TabletUUID, TabletTimestampUtc, ServerTimestampUtc, [Range], RangeUtc, GPSLocationId, LoggedInUserId) 
			VALUES
				(@tabletUUID, @tabletUtc, GETUTCDATE(), @range, @rangeUtc, @staticLocationId, @tabletLoggedInUserId)

			UPDATE asset.tblTabletUUID SET
				LastGPSLocationId = @staticLocationId, LastRange = @range, LastRangeUtc = @rangeUtc
			WHERE
				UUID = @tabletUUID

			SELECT @tabletLastGPSLocationId = @staticLocationId, @tabletLastRange = @range, @tabletLastRangeUtc = @rangeUtc
		END

		DECLARE @beaconSightingId BIGINT
		SELECT @beaconSightingId = Id FROM 
			asset.tblBeaconSighting 
		WHERE 
			TabletUUID = @tabletUUID AND 
			BeaconId = @beaconId AND 
			FirstRangeUtc = @firstRangeUtc

		IF @beaconSightingId IS NULL
		BEGIN
			-- New beacon sighting, grab the tablets last Static beacon sighting and set the PrevStaticBeaconSightingId
			DECLARE @prevStaticBeaconSightingId BIGINT
			SELECT @prevStaticBeaconSightingId = (SELECT TOP 1 bs.Id 
				FROM asset.tblBeaconSighting bs 
				INNER JOIN asset.tblBeacon b ON bs.BeaconId = b.Id AND b.StaticLocationId IS NOT NULL
				WHERE
					TabletUUID = @tabletUUID AND
					FirstRangeUtc < @firstRangeUtc
				ORDER BY
					FirstRangeUtc DESC)

			INSERT INTO asset.tblBeaconSighting 
				(TabletUUID, BeaconId, FirstRangeUtc, BestRangeUtc, BestRange, GPSLocationId, PrevStaticBeaconSightingId, LoggedInUserId, BatteryPercent) 
			VALUES 
				(@tabletUUID, @beaconId, @firstRangeUtc, @rangeUtc, @range, @tabletLastGPSLocationId, @prevStaticBeaconSightingId, @tabletLoggedInUserId, @batteryPercent)

			SELECT @beaconSightingId = SCOPE_IDENTITY()
			-- Only for Asset beacons we can update its position too (NOTE, @tabletLastGPSLocationId could be NULL here)
			IF @staticLocationId IS NULL
				UPDATE asset.tblBeacon SET LastGPSLocationId = @tabletLastGPSLocationId WHERE Id = @beaconId
		END
		ELSE
		BEGIN
			-- Update range if its better 
			UPDATE asset.tblBeaconSighting SET
				BestRange = @range, BestRangeUtc = @rangeUtc, GPSLocationId = @tabletLastGPSLocationId
			WHERE 
				Id = @beaconSightingId AND
				@range < BestRange

			-- If above affected then we can update the beacon's last known location
			IF @@ROWCOUNT = 1 AND @staticLocationId IS NULL
			BEGIN
				UPDATE asset.tblBeacon SET LastGPSLocationId = @tabletLastGPSLocationId WHERE Id = @beaconId
			END
		END

		IF @staticLocationId IS NOT NULL
		BEGIN
			-- Since this is a static beacon we can update Asset sightings records NextStaticBeaconSightingId for this tablet
			UPDATE asset.tblBeaconSighting SET
				NextStaticBeaconSightingId = @beaconSightingId
			WHERE
				Id != @beaconSightingId AND
				TabletUUID = @tabletUUID AND
				NextStaticBeaconSightingId IS NULL
		END
	END

RETURN 0
