﻿CREATE PROCEDURE [asset].[WriteFMABestStaticBeacon]
	@tabletUUID NVARCHAR(36),
	@tabletUtc DATETIME,
	@beaconId NVARCHAR(50),
	@range INT,
	@rangeUtc DATETIME
AS
	-- Make sure the TabletUUID exists, if not create it
	IF NOT EXISTS (SELECT UUID FROM asset.tblTabletUUID WHERE UUID = @tabletUUID)
	BEGIN
		INSERT INTO asset.tblTabletUUID (UUID) VALUES (@tabletUUID)
	END

	-- Grab the Tablets last known location and Range from it, this is where we will update any Asset Beacons to
	DECLARE 
		@tabletLastGPSLocationId BIGINT,
		@tabletLastRange INT,
		@tabletLastRangeUtc DATETIME	

	SELECT  
		@tabletLastGPSLocationId = lastGPSLocationId,
		@tabletLastRange = LastRange,
		@tabletLastRangeUtc = LastRangeUtc
	FROM 
		asset.tblTabletUUID
	WHERE 
		UUID = @tabletUUID

	-- Make sure the Beacon exists, we cannot create it since its a Static Beacon (at least its meant to be)
	DECLARE @staticLocationId BIGINT
	SELECT @staticLocationId = StaticLocationId FROM asset.tblBeacon WHERE Id = @beaconId
	IF @staticLocationId IS NOT NULL
	BEGIN
		UPDATE asset.tblBeacon SET LastContactUtc = @rangeUtc WHERE Id = @beaconId

		IF @tabletLastGPSLocationId IS NULL OR @tabletLastRange IS NULL OR (@staticLocationId != @tabletLastGPSLocationId OR @range < @tabletLastRange)
		BEGIN
			SELECT @tabletLastGPSLocationId = @staticLocationId

			UPDATE asset.tblTabletUUID SET
				LastContactUtc = @tabletUtc,
				LastGPSLocationId = @staticLocationId,
				LastRange = @range,
				LastRangeUtc = @rangeUtc
			WHERE
				UUID = @tabletUUID

			INSERT INTO asset.tblTabletLocationHistory
				(TabletUUID, ServerTimestampUtc, TabletTimestampUtc, GPSLocationId, [Range], RangeUtc)
			VALUES
				(@tabletUUID, GETUTCDATE(), @tabletUtc, @staticLocationId, @range, @rangeUtc)
		END
	END

RETURN 0
