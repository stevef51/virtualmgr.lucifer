﻿CREATE TABLE [wl].[tblSiteFeature]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY,
	[SiteId] UNIQUEIDENTIFIER NOT NULL,
	[WorkLoadingFeatureId] UNIQUEIDENTIFIER NOT NULL,
	-- Dependant on Feature Dimensions ..
	-- 0 = Count of items
	-- 1 = Total length in units
	-- 2 = Total area in square units
	-- 3 = Total volume in cubic units
	[Measure] DECIMAL(28,14) NOT NULL,
	[UnitId] INT NOT NULL, 
    [Name] NVARCHAR(100) NOT NULL, 
	[SiteAreaId] UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [FK_tblSiteFeature_TotblUserData] FOREIGN KEY ([SiteId]) REFERENCES [dbo].[tblUserData]([UserId]), 
    CONSTRAINT [FK_tblSiteFeature_TotblWorkLoadingFeature] FOREIGN KEY ([WorkLoadingFeatureId]) REFERENCES [wl].[tblWorkLoadingFeature]([Id]), 
    CONSTRAINT [FK_tblSiteFeature_TotblMeasurementUnit] FOREIGN KEY ([UnitId]) REFERENCES [wl].[tblMeasurementUnit]([Id]),
    CONSTRAINT [FK_tblSiteFeature_TotblSiteArea] FOREIGN KEY ([SiteAreaId]) REFERENCES [wl].[tblSiteArea]([Id])
)

GO

CREATE INDEX [IX_tblSiteFeature_SiteId] ON [wl].[tblSiteFeature] ([SiteId])

GO
CREATE INDEX [IX_tblSiteFeature_WorkLoadingFeatureId] ON [wl].[tblSiteFeature] ([WorkLoadingFeatureId])
