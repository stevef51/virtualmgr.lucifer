﻿CREATE TABLE [wl].[tblProjectJobTaskWorkloadingActivity]
(
	[ProjectJobTaskId] UNIQUEIDENTIFIER NOT NULL,
	[WorkloadingStandardId] UNIQUEIDENTIFIER NOT NULL, 
	[SiteFeatureId] UNIQUEIDENTIFIER NOT NULL,
	[WorkloadingStandardSeconds] DECIMAL(28,14) NOT NULL,
	[WorkloadingStandardMeasure] DECIMAL(28,14) NOT NULL,
	[WorkloadingStandardUnitId] INT NOT NULL, 
	[Text] NVARCHAR(MAX) NOT NULL,
    [Completed] BIT NULL, 
    [EstimatedDuration] DECIMAL(18, 2) NOT NULL DEFAULT 0, 
    CONSTRAINT [PK_tblProjectJobTaskWorkloadingActivity] PRIMARY KEY ([ProjectJobTaskId], [WorkLoadingStandardId], [SiteFeatureId]), 
    CONSTRAINT [FK_tblProjectJobTaskWorkloadingActivity_TotblProjectJobTask] FOREIGN KEY ([ProjectJobTaskId]) REFERENCES [gce].[tblProjectJobTask]([Id]) ON DELETE CASCADE, 
    CONSTRAINT [FK_tblProjectJobTaskWorkloadingActivity_tblWorkloadingStandard] FOREIGN KEY ([WorkLoadingStandardId]) REFERENCES [wl].[tblWorkLoadingStandard]([Id]), 
    CONSTRAINT [FK_tblProjectJobTaskWorkloadingActivity_tblSiteFeature] FOREIGN KEY ([SiteFeatureId]) REFERENCES [wl].[tblSiteFeature]([Id])
)