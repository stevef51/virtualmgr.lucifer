﻿CREATE TABLE [wl].[tblWorkLoadingFeature]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY,
	[Name] NVARCHAR(512) NOT NULL,
	-- 0 = Count
	-- 1 = Length
	-- 2 = Area
	-- 3 = Volume
	[Dimensions] INT NOT NULL, 
    [Archived] BIT NOT NULL DEFAULT 0
)
