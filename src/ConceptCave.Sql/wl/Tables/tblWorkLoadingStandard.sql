﻿CREATE TABLE [wl].[tblWorkLoadingStandard]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY,
	[BookId] UNIQUEIDENTIFIER NOT NULL,
	[FeatureId] UNIQUEIDENTIFIER NOT NULL,
	[ActivityId] UNIQUEIDENTIFIER NOT NULL,
	[Seconds] DECIMAL(28,14) NOT NULL,
	[Measure] DECIMAL(28,14) NOT NULL,
	[UnitId] INT NOT NULL, 
    [Archived] BIT NOT NULL DEFAULT 0, 
    CONSTRAINT [FK_tblWorkLoadingStandard_Book] FOREIGN KEY ([BookId]) REFERENCES [wl].[tblWorkLoadingBook] ([Id]),
    CONSTRAINT [FK_tblWorkLoadingStandard_Feature] FOREIGN KEY ([FeatureId]) REFERENCES [wl].[tblWorkLoadingFeature] ([Id]),
    CONSTRAINT [FK_tblWorkLoadingStandard_Activity] FOREIGN KEY ([ActivityId]) REFERENCES [wl].[tblWorkLoadingActivity] ([Id]), 
    CONSTRAINT [FK_tblWorkLoadingStandard_ToMeasurementUnit] FOREIGN KEY ([UnitId]) REFERENCES [wl].[tblMeasurementUnit]([Id])
)

GO

CREATE INDEX [IX_tblWorkLoadingStandard_BookFeatureActivity] ON [wl].[tblWorkLoadingStandard] ([BookId],[FeatureId],[ActivityId])

