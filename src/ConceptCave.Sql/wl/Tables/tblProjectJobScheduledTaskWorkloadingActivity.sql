﻿CREATE TABLE [wl].[tblProjectJobScheduledTaskWorkloadingActivity]
(
	[ProjectJobScheduledTaskId] UNIQUEIDENTIFIER NOT NULL , 
	[WorkloadingStandardId] UNIQUEIDENTIFIER NOT NULL,
	[SiteFeatureId] UNIQUEIDENTIFIER NOT NULL,
    [Text] NVARCHAR(MAX) NOT NULL, 
	[ExecuteSunday] BIT,
	[ExecuteMonday] BIT,
	[ExecuteTuesday] BIT,
	[ExecuteWednesday] BIT,
	[ExecuteThursday] BIT,
	[ExecuteFriday] BIT,
	[ExecuteSaturday] BIT,
    [Frequency] INT NOT NULL DEFAULT 1, 
    [EstimatedDuration] DECIMAL(18, 2) NULL, 
    PRIMARY KEY ([ProjectJobScheduledTaskId], [WorkloadingStandardId], [SiteFeatureId]), 
    CONSTRAINT [FK_tblProjectJobScheduledTaskWorkloadingActivity_tblProjectJobScheduledTask] FOREIGN KEY ([ProjectJobScheduledTaskId]) REFERENCES [gce].[tblProjectJobScheduledTask]([Id]) ON DELETE CASCADE, 
    CONSTRAINT [FK_tblProjectJobScheduledTaskWorkloadingActivity_tblSiteFeature] FOREIGN KEY ([SiteFeatureId]) REFERENCES [wl].[tblSiteFeature]([Id]) ON DELETE CASCADE, 
    CONSTRAINT [FK_tblProjectJobScheduledTaskWorkloadingActivity_tblWorkLoadingStandard] FOREIGN KEY ([WorkloadingStandardId]) REFERENCES [wl].[tblWorkLoadingStandard]([Id])
)
