﻿CREATE TABLE [gce].[tblRosterEventConditionAction]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY,
	[RosterEventConditionId] INT NOT NULL,
	[Action] NVARCHAR(MAX) NOT NULL,
	[Data] NVARCHAR(MAX), 
    CONSTRAINT [FK_tblRosterEventConditionAction_ToRosterProjectJobTaskEventCondition] FOREIGN KEY ([RosterEventConditionId]) REFERENCES [gce].[tblRosterEventCondition]([Id]) ON DELETE CASCADE
)

GO

CREATE INDEX [IX_tblRosterEventConditionAction_RosterEventCondition] ON [gce].[tblRosterEventConditionAction] ([RosterEventConditionId])
