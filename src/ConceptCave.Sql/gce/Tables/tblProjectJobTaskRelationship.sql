﻿CREATE TABLE [gce].[tblProjectJobTaskRelationship]
(
	[SourceProjectJobTaskId] UNIQUEIDENTIFIER NOT NULL , 
    [DestinationProjectJobTaskId] UNIQUEIDENTIFIER NOT NULL, 
    [ProjectJobTaskRelationshipTypeId] INT NOT NULL, 
    PRIMARY KEY ([SourceProjectJobTaskId], [DestinationProjectJobTaskId], [ProjectJobTaskRelationshipTypeId]), 
    CONSTRAINT [FK_tblProjectJobTaskRelationship_ToProjectJobTaskSource] FOREIGN KEY ([SourceProjectJobTaskId]) REFERENCES [gce].[tblProjectJobTask]([Id]), 
    CONSTRAINT [FK_tblProjectJobTaskRelationship_ToProjectJobTaskDestination] FOREIGN KEY ([DestinationProjectJobTaskId]) REFERENCES [gce].[tblProjectJobTask]([Id]), 
    CONSTRAINT [FK_tblProjectJobTaskRelationship_ToProjectJobTaskRelationshipType] FOREIGN KEY ([ProjectJobTaskRelationshipTypeId]) REFERENCES [gce].[tblProjectJobTaskRelationshipType]([Id])
)

GO

CREATE INDEX [IX_tblProjectJobTaskRelationship_TypeSource] ON [gce].[tblProjectJobTaskRelationship] ([ProjectJobTaskRelationshipTypeId], [SourceProjectJobTaskId])
