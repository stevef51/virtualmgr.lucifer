﻿CREATE TABLE [gce].[tblProjectJobTaskWorkLog]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY, 
    [ProjectJobTaskId] UNIQUEIDENTIFIER NOT NULL, 
    [UserId] UNIQUEIDENTIFIER NOT NULL, 
    [DateCreated] DATETIME NOT NULL, 
    [ClockinLatitude] DECIMAL(18, 8) NULL, 
    [ClockinLongitude] DECIMAL(18, 8) NULL, 
	[ClockinAccuracy] DECIMAL(18,4) NULL,
	[ClockinLocationStatus] INT DEFAULT -1,
	[ClockinMetresFromSite] DECIMAL(18,4) NULL,
	[DateCompleted] DATETIME NULL,
    [ClockoutLatitude] DECIMAL(18, 8) NULL, 
    [ClockoutLongitude] DECIMAL(18, 8) NULL, 
	[ClockoutAccuracy] DECIMAL(18, 4) NULL,
	[ClockoutLocationStatus] INT DEFAULT -1,
	[ClockoutMetresFromSite] DECIMAL(18,4) NULL,
    [Notes] NVARCHAR(MAX) NULL, 
    [ActualDuration] DECIMAL(18, 2) NULL, 
    [ApprovedDuration] DECIMAL(18, 2) NULL, 
    [InactiveDuration] DECIMAL(18, 2) NOT NULL DEFAULT 0, 
    CONSTRAINT [FK_tblProjectJobTaskWorkLog_ToProjectJobTask] FOREIGN KEY ([ProjectJobTaskId]) REFERENCES [gce].[tblProjectJobTask] ON DELETE CASCADE,
	CONSTRAINT [FK_tblProjectJobTaskWorkLog_ToUserData] FOREIGN KEY ([UserId]) REFERENCES [dbo].[tblUserData]
)
GO

CREATE INDEX [IX_tblProjectJobTaskWorkLog_ProjectJobTaskId] ON [gce].[tblProjectJobTaskWorkLog] ([ProjectJobTaskId])
GO

CREATE INDEX [IX_tblProjectJobTaskWorkLog_UserId] ON [gce].[tblProjectJobTaskWorkLog] ([UserId])
GO

CREATE INDEX [IX_tblProjectJobTaskWorkLog_DateCreated] ON [gce].[tblProjectJobTaskWorkLog] ([DateCreated])
GO

CREATE NONCLUSTERED INDEX [IX_tblProjectJobTaskWorkLog_UserIdDateCreated]
ON [gce].[tblProjectJobTaskWorkLog] ([UserId],[DateCreated])
INCLUDE ([DateCompleted])
GO

CREATE TRIGGER [gce].[Trigger_tblProjectJobTaskWorkLog_Update]
    ON [gce].[tblProjectJobTaskWorkLog]
    FOR UPDATE
    AS
    BEGIN
        SET NoCount ON

		/* Fix for EVS-1558, we are going to create a record in the timings table to match the new row(s) and store the
		server side date created info */

		DECLARE @now DATETIME
		SET @now = GETUTCDATE()

		UPDATE t
			SET DateCompleted = inserted.DateCompleted, CompletedQueueTimeSeconds = ROUND(DATEDIFF(ms, inserted.DateCompleted, @now)/1000.0, 2)
		FROM [gce].tblProjectJobTaskWorkLogTiming t INNER JOIN inserted ON t.Id = inserted.Id

		/* End of fix for EVS-1558 */

		IF UPDATE(DateCompleted)
		BEGIN
			UPDATE t SET 
				t.ActualDuration = IIF(t.DateCompleted is null, null, DateDiff(SECOND, t.DateCreated, t.DateCompleted)) 
			FROM [gce].[tblProjectJobTaskWorkLog] AS t 
			INNER JOIN inserted i on i.Id = t.Id

			UPDATE t SET
					t.ActualDuration = (SELECT SUM(ActualDuration) FROM [gce].tblProjectJobTaskWorkLog WHERE ProjectJobTaskId = t.Id)
			FROM [gce].[tblProjectJobTask] AS t 
			INNER JOIN inserted i on i.ProjectJobTaskId = t.Id
		END
    END
GO

CREATE TRIGGER [gce].[Trigger_tblProjectJobTaskWorkLog_Insert]
    ON [gce].[tblProjectJobTaskWorkLog]
    FOR INSERT
    AS
    BEGIN
        SET NoCount ON

		/* Fix for EVS-1558, we are going to create a record in the timings table to match the new row(s) and store the
		server side date created info */

		DECLARE @now DATETIME
		SET @now = GETUTCDATE()

		INSERT INTO [gce].tblProjectJobTaskWorkLogTiming
			(Id, DateCreated, CreationQueueTimeSeconds)
		SELECT Id, @now, ROUND(DATEDIFF(ms, i.DateCreated, @now)/1000.0, 2) FROM inserted i

		/* End of fix for EVS-1558 */

		UPDATE t SET
			t.InactiveDuration = IIF(
				(SELECT TOP 1 DateCompleted FROM [gce].[tblProjectJobTaskWorkLog] WHERE ProjectJobTaskId = t.ProjectJobTaskId ORDER BY DateCompleted DESC) is null, 
				0, 
				DATEDIFF(SECOND, (SELECT TOP 1 DateCompleted FROM [gce].[tblProjectJobTaskWorkLog] WHERE ProjectJobTaskId = t.ProjectJobTaskId ORDER BY DateCompleted DESC), t.DateCreated))
		FROM [gce].[tblProjectJobTaskWorkLog] AS t 
		INNER JOIN inserted i on i.Id = t.Id

		UPDATE t SET
				t.InactiveDuration = (SELECT SUM(InactiveDuration) FROM [gce].tblProjectJobTaskWorkLog WHERE ProjectJobTaskId = i.ProjectJobTaskId)
		FROM [gce].[tblProjectJobTask] AS t 
		INNER JOIN inserted i on i.ProjectJobTaskId = t.Id

    END