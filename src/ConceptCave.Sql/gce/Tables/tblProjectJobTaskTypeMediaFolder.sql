﻿CREATE TABLE [gce].[tblProjectJobTaskTypeMediaFolder]
(
	[TaskTypeId] UNIQUEIDENTIFIER NOT NULL,
	[MediaFolderId] UNIQUEIDENTIFIER NOT NULL,
	[Text] NVARCHAR(50) NULL, 
    CONSTRAINT [PK_tblProjectJobTaskTypeMediaFolder] PRIMARY KEY ([TaskTypeId], [MediaFolderId]), 
    CONSTRAINT [FK_tblProjectJobTaskTypeMediaFolder_ToTaskType] FOREIGN KEY ([TaskTypeId]) REFERENCES [gce].[tblProjectJobTaskType]([Id]) ON DELETE CASCADE, 
    CONSTRAINT [FK_tblProjectJobTaskTypeMediaFolder_ToMediaFolder] FOREIGN KEY ([MediaFolderId]) REFERENCES [tblMediaFolder]([Id]) ON DELETE CASCADE
)
