﻿CREATE TABLE [gce].[tblProjectJobTaskWorkingDocument]
(
	[ProjectJobTaskId] UNIQUEIDENTIFIER NOT NULL , 
    [PublishingGroupResourceId] INT NOT NULL, 
    [WorkingDocumentId] UNIQUEIDENTIFIER NOT NULL, 
    PRIMARY KEY ([PublishingGroupResourceId], [WorkingDocumentId]), 
    CONSTRAINT [FK_tblProjectJobTaskWorkingDocument_ToProjectJobTask] FOREIGN KEY ([ProjectJobTaskId]) REFERENCES [gce].[tblProjectJobTask]([Id]) ON DELETE CASCADE, 
    CONSTRAINT [FK_tblProjectJobTaskWorkingDocument_ToPublishingGroupResource] FOREIGN KEY ([PublishingGroupResourceId]) REFERENCES [dbo].[tblPublishingGroupResource]([Id]),
	CONSTRAINT [FK_tblProjectJobTaskWorkingDocument_ToWorkingDocument] FOREIGN KEY ([WorkingDocumentId]) REFERENCES [dbo].[tblWorkingDocument]([Id]),
)

GO
CREATE NONCLUSTERED INDEX [nci_wi_tblProjectJobTaskWorkingDocument_414B9EF5EB86A2008F4A00107BA988A8] 
	ON [gce].[tblProjectJobTaskWorkingDocument] ([ProjectJobTaskId]) 
	WITH (ONLINE = ON)