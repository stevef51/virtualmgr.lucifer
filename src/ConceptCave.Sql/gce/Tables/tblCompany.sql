﻿CREATE TABLE [gce].[tblCompany]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY, 
    [Name] NVARCHAR(50) NULL, 
    [Street] NVARCHAR(255) NULL, 
    [Town] NVARCHAR(100) NULL, 
    [Postcode] NVARCHAR(10) NULL, 
    [CountryStateId] INT NULL, 
	[Latitude] DECIMAL (18, 8)  NULL,
    [Longitude] DECIMAL (18, 8)  NULL,
    [Rating] INT NULL, 
    CONSTRAINT [FK_tblCompany_ToCountryState] FOREIGN KEY ([CountryStateId]) REFERENCES [gce].[tblCountryState]([Id])
)
