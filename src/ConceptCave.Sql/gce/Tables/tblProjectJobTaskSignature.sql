﻿CREATE TABLE [gce].[tblProjectJobTaskSignature]
(
	[ProjectJobTaskId] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY, 
    [SignatureData] NVARCHAR(MAX) NOT NULL, 
    [SignatureMediaId] UNIQUEIDENTIFIER NULL, 
    [PhotoMediaId] UNIQUEIDENTIFIER NULL, 
    [Latitude] DECIMAL(18, 8) NULL, 
    [Longitude] DECIMAL(18, 8) NULL, 
    [Accuracy] DECIMAL(18, 4) NULL, 
    [LocationStatus] INT NOT NULL DEFAULT 0, 
    CONSTRAINT [FK_tblTaskSignature_ToTask] FOREIGN KEY ([ProjectJobTaskId]) REFERENCES [gce].[tblProjectJobTask]([Id]) ON DELETE CASCADE, 
    CONSTRAINT [FK_tblTaskSignature_ToMediaViaSignature] FOREIGN KEY ([SignatureMediaId]) REFERENCES [dbo].[tblMedia]([Id]), 
    CONSTRAINT [FK_tblTaskSignature_ToMediaViaPhoto] FOREIGN KEY ([PhotoMediaId]) REFERENCES [dbo].[tblMedia]([Id])
)
