﻿CREATE TABLE [gce].[tblRosterProjectJobTaskEvent]
(
	[Id] INT NOT NULL PRIMARY KEY,
	[Name] NVARCHAR(50) NOT NULL, 
    [Timed] BIT NOT NULL DEFAULT 0
)
