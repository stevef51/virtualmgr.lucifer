﻿CREATE TABLE [gce].[tblProjectJobStatus]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY, 
    [Text] NVARCHAR(50) NOT NULL, 
    [Description] NVARCHAR(MAX) NULL
)
