﻿CREATE TABLE [gce].[tblUserTypeTaskTypeRelationship]
(
	[UserTypeId] INT NOT NULL,
	[RelationshipType] INT NOT NULL, 
	[TaskTypeId] UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_tblUserTaskType] PRIMARY KEY ([UserTypeId], [RelationshipType], [TaskTypeId]), 
    CONSTRAINT [FK_tblUserTypeTaskType_ToTaskType] FOREIGN KEY ([TaskTypeId]) REFERENCES [gce].[tblProjectJobTaskType]([Id]),
    CONSTRAINT [FK_tblUserTypeTaskType_ToUserType] FOREIGN KEY ([UserTypeId]) REFERENCES [dbo].[tblUserType]([Id])
)
