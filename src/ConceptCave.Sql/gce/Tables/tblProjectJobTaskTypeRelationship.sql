﻿CREATE TABLE [gce].[tblProjectJobTaskTypeRelationship]
(
	[ProjectJobTaskTypeSourceId] UNIQUEIDENTIFIER NOT NULL ,
	[ProjectJobTaskTypeDestinationId] UNIQUEIDENTIFIER NOT NULL,
	RelationshipType int NOT NULL DEFAULT 0, 
    PRIMARY KEY ([ProjectJobTaskTypeSourceId], [ProjectJobTaskTypeDestinationId]), 
    CONSTRAINT [FK_tblProjectJobTaskTypeRelationship_ToSourceTaskType] FOREIGN KEY ([ProjectJobTaskTypeSourceId]) REFERENCES [gce].[tblProjectJobTaskType]([Id]), 
    CONSTRAINT [FK_tblProjectJobTaskTypeRelationship_ToDestinationTaskType] FOREIGN KEY ([ProjectJobTaskTypeDestinationId]) REFERENCES [gce].[tblProjectJobTaskType]([Id])
)
