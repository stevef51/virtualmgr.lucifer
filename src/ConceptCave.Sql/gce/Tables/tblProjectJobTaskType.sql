﻿CREATE TABLE [gce].[tblProjectJobTaskType]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY, 
    [Name] NVARCHAR(50) NOT NULL, 
    [UserInterfaceType] INT NOT NULL DEFAULT 0, 
    [MediaId] UNIQUEIDENTIFIER NULL, 
    [DefaultLengthInDays] NVARCHAR(50) NOT NULL DEFAULT 1, 
    [AutoStart1stChecklist] BIT NOT NULL DEFAULT 0, 
    [PhotoSupport] INT NOT NULL DEFAULT 0, 
	[AutoFinishAfterLastChecklist] BIT NOT NULL DEFAULT 0,
	[EstimatedDurationSeconds] DECIMAL(18, 2) NULL,
    [ProductCatalogs] NVARCHAR(4000) NULL, 
    [MinimumDuration] NVARCHAR(20) NULL, 
    [ExcludeFromTimesheets] BIT NOT NULL DEFAULT 0, 
    [AutoFinishAfterStart] BIT NOT NULL DEFAULT 0, 
    CONSTRAINT [FK_tblProjectJobTaskType_Media] FOREIGN KEY ([MediaId]) REFERENCES [tblMedia]([Id]) ON DELETE CASCADE
)
