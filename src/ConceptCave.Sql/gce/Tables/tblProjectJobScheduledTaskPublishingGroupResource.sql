﻿CREATE TABLE [gce].[tblProjectJobScheduledTaskPublishingGroupResource]
(
	[ProjectJobScheduledTaskId] UNIQUEIDENTIFIER NOT NULL , 
    [PublishingGroupResourceId] INT NOT NULL, 
    [AllowMultipleInstances] BIT NOT NULL DEFAULT 0, 
    [IsMandatory] BIT NOT NULL DEFAULT 0, 
    PRIMARY KEY ([PublishingGroupResourceId], [ProjectJobScheduledTaskId]),
	CONSTRAINT [FK_tblProjectJobScheduledTaskPublishingGroupResource_ToProjectJobTask] FOREIGN KEY ([ProjectJobScheduledTaskId]) REFERENCES [gce].[tblProjectJobScheduledTask]([Id]), 
    CONSTRAINT [FK_tblProjectJobScheduledTaskPublishingGroupResource_ToPublishingGroupResource] FOREIGN KEY ([PublishingGroupResourceId]) REFERENCES [dbo].[tblPublishingGroupResource]([Id]), 
)
