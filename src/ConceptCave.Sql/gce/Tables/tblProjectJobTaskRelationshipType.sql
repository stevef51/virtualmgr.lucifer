﻿CREATE TABLE [gce].[tblProjectJobTaskRelationshipType]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Name] NVARCHAR(50) NOT NULL
)
