﻿CREATE TABLE [gce].[tblProjectJobTaskMedia]
(
    [TaskId] UNIQUEIDENTIFIER NOT NULL, 
    [MediaId] UNIQUEIDENTIFIER NOT NULL, 
    [Notes] NVARCHAR(MAX) NULL, 
    CONSTRAINT [PK_tblProjectJobTaskMedia] PRIMARY KEY ([TaskId], [MediaId]), 
    CONSTRAINT [FK_tblProjectJobTaskMedia_TotblProjectJobTask] FOREIGN KEY ([TaskId]) REFERENCES [gce].[tblProjectJobTask]([Id]), 
    CONSTRAINT [FK_tblProjectJobTaskMedia_TotblMedia] FOREIGN KEY ([MediaId]) REFERENCES [tblMedia]([Id])
)
