﻿CREATE TABLE [gce].[tblProjectJobTaskTypeFacilityOverride]
(
	[TaskTypeId] UNIQUEIDENTIFIER NOT NULL,
	[FacilityStructureId] INT NOT NULL,
	[EstimatedDurationSeconds] INT NULL, 
    [MinimumDuration] NVARCHAR(20) NULL, 
    CONSTRAINT [FK_tblProjectJobTaskTypeFacilityOverride_ToTaskType] FOREIGN KEY ([TaskTypeId]) REFERENCES [gce].[tblProjectJobTaskType]([Id]),
    CONSTRAINT [FK_tblProjectJobTaskTypeFacilityOverride_ToFacilityStructure] FOREIGN KEY ([FacilityStructureId]) REFERENCES [asset].[tblFacilityStructure]([Id]), 
    CONSTRAINT [PK_tblProjectJobTaskTypeFacilityOverride] PRIMARY KEY ([TaskTypeId], [FacilityStructureId])
)
