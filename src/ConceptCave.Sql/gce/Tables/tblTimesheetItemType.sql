﻿CREATE TABLE [gce].[tblTimesheetItemType]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Name] NVARCHAR(100) NOT NULL, 
    [IsWeekDayPay] BIT NOT NULL DEFAULT 1, 
    [IsSaturdayPay] BIT NOT NULL DEFAULT 0, 
    [IsSundayPay] BIT NOT NULL DEFAULT 0, 
    [IsLeave] BIT NOT NULL DEFAULT 0
)
