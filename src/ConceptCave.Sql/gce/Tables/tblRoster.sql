﻿CREATE TABLE [gce].[tblRoster]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY,
	[Name] NVARCHAR(50) NOT NULL,
	[HierarchyId] INT NOT NULL, 
	[StartTime] DATETIME NOT NULL DEFAULT CONVERT(datetime, '2000-01-01 12:00:00 AM'),
	[EndTime] DATETIME NOT NULL DEFAULT CONVERT(datetime, '2000-01-01 11:59:59 PM'),
    [EndOfDayDefaultTaskStatus] INT NOT NULL DEFAULT 0, 
    [ScheduleApprovalDefaultTaskStatus] INT NOT NULL DEFAULT 0, 
    [AutomaticScheduleApprovalTime] DATETIME NULL, 
    [AutomaticEndOfDayTime] DATETIME NULL, 
    [AutomaticScheduleApprovalTimeLastRun] DATETIME NULL, 
    [AutomaticEndOfDayTimeLastRun] DATETIME NULL, 
    [Timezone] NVARCHAR(50) NOT NULL DEFAULT (N'UTC'), 
    [IncludeLunchInTimesheetDurations] BIT NOT NULL DEFAULT 1, 
    [RoundTimesheetToNearestMins] INT NOT NULL DEFAULT 10, 
    [RoundTimesheetToNearestMethod] INT NOT NULL DEFAULT 0, 
    [TimesheetApprovedSourceMethod] INT NOT NULL DEFAULT 0, 
    [TimesheetUseScheduledEndWhenFinishedBySystem] BIT NOT NULL DEFAULT 1, 
    [ProvidesClaimableDutyList] BIT NOT NULL DEFAULT 0, 
    [ReceivesClaimableDutyList] BIT NOT NULL DEFAULT 0, 
    [SortOrder] INT NOT NULL DEFAULT 0, 
    CONSTRAINT [FK_tblRoster_TotblHierarchy] FOREIGN KEY ([HierarchyId]) REFERENCES [dbo].[tblHierarchy]([Id]) ON DELETE CASCADE
)
