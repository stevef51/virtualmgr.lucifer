﻿CREATE TABLE [gce].[tblCountryState]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [Name] NVARCHAR(50) NOT NULL, 
    [Abbreviation] NVARCHAR(10) NOT NULL,
	[TimeZone] NVARCHAR (50) CONSTRAINT [DF_tblCountryState_TimeZone] DEFAULT (N'UTC') NOT NULL
)
