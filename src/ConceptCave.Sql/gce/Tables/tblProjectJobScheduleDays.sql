﻿CREATE TABLE [gce].[tblProjectJobScheduleDays]
(
    [Day] INT NOT NULL, 
    [RosterId] INT NOT NULL, 
    [RoleId] INT NOT NULL, 
    [StartTime] DATETIME NOT NULL, 
    [EndTime] DATETIME NOT NULL, 
    [LunchStart] DATETIME NOT NULL, 
    [LunchEnd] DATETIME NOT NULL, 
    [OverrideRoleId] INT NULL, 
    CONSTRAINT [PK_tblProjectJobScheduleDays] PRIMARY KEY ([Day], [RosterId], [RoleId]), 
    CONSTRAINT [FK_tblProjectJobScheduleDays_ToRoster] FOREIGN KEY ([RosterId]) REFERENCES [gce].[tblRoster]([Id]), 
    CONSTRAINT [FK_tblProjectJobScheduleDays_ToHierarchyBucketRole] FOREIGN KEY ([RoleId]) REFERENCES [dbo].[tblHierarchyRole]([Id]), 
    CONSTRAINT [FK_tblProjectJobScheduleDays_ToHierarchyBucketRoleOverride] FOREIGN KEY ([OverrideRoleId]) REFERENCES [tblHierarchyRole]([Id]) ON DELETE SET NULL
)
