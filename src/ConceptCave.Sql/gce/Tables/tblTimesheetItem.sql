﻿CREATE TABLE [gce].[tblTimesheetItem]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Date] DATETIME NOT NULL, 
    [UserId] UNIQUEIDENTIFIER NOT NULL, 
	[RosterId] INT NOT NULL,
	[HierarchyRoleId] INT,
	[TimesheetItemTypeId] INT NOT NULL,
	[DateCreated] DATETIME NOT NULL,
	[HierarchyBucketOverrideTypeId] INT NULL,
	[ScheduledStartDate] DATETIME NULL,
	[ScheduledEndDate] DATETIME NULL,
	[ScheduledDuration] DECIMAL(18,2),
    [ActualDuration] DECIMAL(18, 2) NULL, 
	[ActualStartDate] DATETIME NOT NULL,
	[ActualEndDate] DATETIME NOT NULL,
	[ApprovedStartDate] DATETIME NOT NULL,
	[ApprovedEndDate] DATETIME NOT NULL,
	[ApprovedDuration] DECIMAL(18,2) NOT NULL, 
    [ApprovedByUserId] UNIQUEIDENTIFIER NOT NULL, 
    [IsLunchPaid] BIT NOT NULL DEFAULT 1, 
    [LunchDuration] DECIMAL(18, 2) NULL, 
    [ExportedDate] DATETIME NULL, 
    [ScheduledLunchStartDate] DATETIME NULL, 
    [ScheduledLunchEndDate] DATETIME NULL, 
    [PayAsTimesheetItemTypeId] INT NULL, 
    CONSTRAINT [FK_tblTimesheet_ToUser] FOREIGN KEY ([UserId]) REFERENCES [tblUserData]([UserId]), 
    CONSTRAINT [FK_tblTimesheet_ToTimesheetType] FOREIGN KEY ([TimesheetItemTypeId]) REFERENCES [gce].[tblTimesheetItemType]([Id]), 
    CONSTRAINT [FK_tblTimesheetItem_ToRoster] FOREIGN KEY ([RosterId]) REFERENCES [gce].[tblRoster]([Id]), 
    CONSTRAINT [FK_tblTimesheetItem_ToUserApprovedBy] FOREIGN KEY ([ApprovedByUserId]) REFERENCES [tblUserData]([UserId]), 
    CONSTRAINT [FK_tblTimesheetItem_ToHierarchyBucketOverrideType] FOREIGN KEY ([HierarchyBucketOverrideTypeId]) REFERENCES [tblHierarchyBucketOverrideType]([Id]),
	CONSTRAINT [FK_tblTimesheetItem_ToHierarchyRole] FOREIGN KEY ([HierarchyRoleId]) REFERENCES [tblHierarchyRole]([Id]),
    CONSTRAINT [FK_tblTimesheet_ToPayAsTimesheetType] FOREIGN KEY ([PayAsTimesheetItemTypeId]) REFERENCES [gce].[tblTimesheetItemType]([Id])

)

GO

CREATE INDEX [IX_tblTimesheet_DateUserId] ON [gce].[tblTimesheetItem] ([Date], [UserId])

GO

CREATE INDEX [IX_tblTimesheetItem_DateRosterId] ON [gce].[tblTimesheetItem] ([Date], [RosterId])

GO

CREATE UNIQUE INDEX [IX_tblTimesheetItem_DateRosterUserType] ON [gce].[tblTimesheetItem] ([Date], [RosterId], [HierarchyRoleId], [UserId], [TimesheetItemTypeId])