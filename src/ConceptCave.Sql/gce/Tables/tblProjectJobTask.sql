﻿CREATE TABLE [gce].[tblProjectJobTask] (
    [Id]           UNIQUEIDENTIFIER NOT NULL,
	[ProjectJobScheduledTaskId] UNIQUEIDENTIFIER,
	[RosterId] INT NOT NULL,
	[TaskTypeId] UNIQUEIDENTIFIER NOT NULL,
	[DateCreated] DATETIME NOT NULL DEFAULT getutcdate() ,
	[DateCompleted] DATETIME,
    [Name]         NVARCHAR (4000)    NOT NULL,
    [Description]  NVARCHAR (MAX)   NULL,
	[Forecolor] INT NULL, 
    [Backcolor] INT NULL, 
    [OwnerId]      UNIQUEIDENTIFIER NOT NULL,
	[OriginalOwnerId] UNIQUEIDENTIFIER NOT NULL,
	[SiteId] UNIQUEIDENTIFIER NULL,
	[SiteLatitude] DECIMAL(18, 8) NULL, 
	[SiteLongitude] DECIMAL(18, 8) NULL,
	[RoleId] INT,
	[HierarchyBucketId] INT NULL,
    [Level] INT NOT NULL DEFAULT 0, 
    [RequireSignature] BIT NOT NULL DEFAULT 0, 
    [PhotoDuringSignature] BIT NOT NULL DEFAULT 0,
    [CompletionNotes] NVARCHAR(MAX) NULL, 
    [Status] INT NOT NULL DEFAULT 0, 
    [StatusDate] DATETIME NOT NULL DEFAULT getutcdate(), 
    [ProjectJobTaskGroupId] UNIQUEIDENTIFIER NULL, 
    [WorkingGroupId] UNIQUEIDENTIFIER NULL, 
    [StatusId] UNIQUEIDENTIFIER NULL, 
    [ReferenceNumber] NVARCHAR(20) NULL, 
    [EndsOn] DATETIME NOT NULL DEFAULT '2000-01-01', 
    [ActualDuration] DECIMAL(18, 2) NULL, 
    [InactiveDuration] DECIMAL(18, 2) NULL, 
    [WorkingDocumentCount] INT NOT NULL DEFAULT 0, 
    [AllActivitiesEstimatedDuration] DECIMAL(18, 2) NOT NULL DEFAULT 0, 
    [CompletedActivitiesEstimatedDuration] DECIMAL(18, 2) NOT NULL DEFAULT 0, 
    [StartTime] TIME NULL, 
    [LateAfter] TIME NULL, 
    [StartTimeUtc] DATETIMEOFFSET NULL, 
    [LateAfterUtc] DATETIMEOFFSET NULL, 
	  [EstimatedDurationSeconds] DECIMAL(18, 2) NULL,
    [ScheduledOwnerId] UNIQUEIDENTIFIER NULL, 
    [OriginalTaskTypeId] UNIQUEIDENTIFIER NULL, 
    [SortOrder] INT NOT NULL DEFAULT 0, 
    [HasOrders] BIT NOT NULL DEFAULT 0, 
    [MinimumDuration] DECIMAL(18, 2) NULL, 
    [Context] NVARCHAR(MAX) NULL, 
	  [Tracking] ROWVERSION NOT NULL,
    [AssetId] INT NULL, 
	[NotifyUser] BIT NOT NULL DEFAULT 0,
    CONSTRAINT [PK_tblProjectJobTask] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_tblProjectJobTask_Users] FOREIGN KEY ([OwnerId]) REFERENCES [dbo].[tblUserData] ([UserId]), 
    CONSTRAINT [FK_tblProjectJobTask_ToProjectJobScheduledTask] FOREIGN KEY ([ProjectJobScheduledTaskId]) REFERENCES [gce].[tblProjectJobScheduledTask]([Id]), 
    CONSTRAINT [FK_tblProjectJobTask_TotblRoster] FOREIGN KEY ([RosterId]) REFERENCES [gce].[tblRoster]([Id]) ON DELETE CASCADE, 
    CONSTRAINT [FK_tblProjectJobTask_TotblHierarchyRole] FOREIGN KEY ([RoleId]) REFERENCES [dbo].[tblHierarchyRole]([Id]), 
    CONSTRAINT [FK_tblProjectJobTask_TotblTaskType] FOREIGN KEY ([TaskTypeId]) REFERENCES [gce].[tblProjectJobTaskType]([Id]), 
    CONSTRAINT [FK_tblProjectJobTask_UsersThroughSite] FOREIGN KEY ([SiteId]) REFERENCES [dbo].[tblUserData]([UserId]), 
    CONSTRAINT [FK_tblProjectJobTask_TotblHierarchyBucket] FOREIGN KEY ([HierarchyBucketId]) REFERENCES [tblHierarchyBucket]([Id]), 
    CONSTRAINT [FK_tblProjectJobTask_ToStatus] FOREIGN KEY ([StatusId]) REFERENCES [gce].[tblProjectJobStatus]([Id]), 
    CONSTRAINT [FK_tblProjectJobTask_TotblTaskTypeOriginalTaskType] FOREIGN KEY ([OriginalTaskTypeId]) REFERENCES [gce].[tblProjectJobTaskType]([Id]), 
    CONSTRAINT [FK_tblProjectJobTask_ToAsset] FOREIGN KEY ([AssetId]) REFERENCES [asset].[tblAsset]([Id]), 
);


GO

CREATE INDEX [IX_tblProjectJobTask_ProjectJobTaskGroupId] ON [gce].[tblProjectJobTask] ([ProjectJobTaskGroupId])

GO

CREATE INDEX [IX_tblProjectJobTask_ProjectJobScheduledTaskId] ON [gce].[tblProjectJobTask] ([ProjectJobScheduledTaskId])

GO

CREATE INDEX [IX_tblProjectJobTask_OwnerId] ON [gce].[tblProjectJobTask] ([OwnerId])

GO

CREATE INDEX [IX_tblProjectJobTask_SiteId] ON [gce].[tblProjectJobTask] ([SiteId])

GO

CREATE INDEX [IX_tblProjectJobTask_ReferenceNumber] ON [gce].[tblProjectJobTask] ([ReferenceNumber])

GO

CREATE INDEX [IX_tblProjectJobTask_AssetId] ON [gce].[tblProjectJobTask] ([AssetId])

GO
/* This index was recomended by the Azure index tuning service and reported it would have substantial impact */
CREATE NONCLUSTERED INDEX [nci_wi_tblProjectJobTask_9F06A1B2A8FB3C4A3D77] ON
[gce].[tblProjectJobTask] ([HierarchyBucketId], [Status], [DateCreated]) INCLUDE
([ActualDuration], [Backcolor], [CompletionNotes], [DateCompleted], [Description], [EndsOn],
[Forecolor], [Id], [InactiveDuration], [Level], [Name], [OriginalOwnerId], [OwnerId],
[PhotoDuringSignature], [ProjectJobScheduledTaskId], [ProjectJobTaskGroupId], [ReferenceNumber],
[RequireSignature], [RoleId], [RosterId], [SiteId], [SiteLatitude], [SiteLongitude],
[StatusDate], [StatusId], [TaskTypeId], [WorkingDocumentCount], [WorkingGroupId]) WITH (ONLINE =
ON)

GO



CREATE NONCLUSTERED INDEX [nci_wi_tblProjectJobTask_4021FC4FB9FE5D4340DB] ON
[gce].[tblProjectJobTask] ([OwnerId], [Status]) INCLUDE ([DateCreated], [HierarchyBucketId],
[OriginalOwnerId], [RosterId], [StatusDate], [TaskTypeId]) WITH (ONLINE = ON)
GO


CREATE TRIGGER [gce].[Trigger_tblProjectJobTask_Update]
    ON [gce].[tblProjectJobTask]
    FOR UPDATE
    AS
    BEGIN
        SET NoCount ON

		IF UPDATE(DateCompleted)
		BEGIN
			UPDATE t SET
				t.WorkingDocumentCount = (SELECT COUNT(WorkingDocumentId) FROM [gce].tblProjectJobTaskWorkingDocument WHERE ProjectJobTaskId = t.Id),
				t.CompletedActivitiesEstimatedDuration = ISNULL((SELECT SUM(EstimatedDuration) FROM [wl].tblProjectJobTaskWorkloadingActivity WHERE ProjectJobTaskId = t.ID AND Completed = 1), 0)
			FROM [gce].[tblProjectJobTask] AS t 
			INNER JOIN inserted i on i.Id = t.Id
		END
    END
GO

-- Azure suggested high impact indexes ..
CREATE NONCLUSTERED INDEX [nci_wi_tblProjectJobTask_DAD53CF9D530BAB27C5687F6F74D1D0C] ON [gce].[tblProjectJobTask] ([OwnerId], [RosterId], [Status], [DateCompleted]) WITH (ONLINE = ON)
GO

CREATE NONCLUSTERED INDEX [nci_wi_tblProjectJobTask_BE4F0D9854032348FB24483AFA560920] ON [gce].[tblProjectJobTask] ([TaskTypeId], [DateCreated]) INCLUDE ([OriginalOwnerId], [OriginalTaskTypeId], [OwnerId], [RosterId], [Status]) WITH (ONLINE = ON)
GO

CREATE INDEX [IX_tblProjectJobTask_Tracking] ON [gce].[tblProjectJobTask] ([Tracking])
GO

CREATE NONCLUSTERED INDEX [nci_wi_tblProjectJobTask_7D0AB71FCE2E7169E133457C6B5717AB] ON [gce].[tblProjectJobTask] ([Tracking]) INCLUDE ([OwnerId], [SiteId]) WITH (ONLINE = ON)