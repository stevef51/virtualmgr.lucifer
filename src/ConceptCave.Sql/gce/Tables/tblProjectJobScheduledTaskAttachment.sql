﻿CREATE TABLE [gce].[tblProjectJobScheduledTaskAttachment]
(
	[ProjectJobScheduledTaskId] UNIQUEIDENTIFIER NOT NULL , 
    [MediaId] UNIQUEIDENTIFIER NOT NULL, 
    PRIMARY KEY ([ProjectJobScheduledTaskId], [MediaId]), 
    CONSTRAINT [FK_tblProjectJobScheduledTaskAttachment_ToMedia] FOREIGN KEY ([MediaId]) REFERENCES [dbo].[tblMedia]([Id]),
    CONSTRAINT [FK_tblProjectJobScheduledTaskAttachment_ToProjectJobScheduledTask] FOREIGN KEY ([ProjectJobScheduledTaskId]) REFERENCES [gce].[tblProjectJobScheduledTask]([Id]) ON DELETE CASCADE
)
