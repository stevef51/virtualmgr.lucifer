﻿CREATE TABLE [gce].[tblTimesheetedTaskWorkLog]
(
	[TimesheetItemId] INT NOT NULL , 
    [ProjectJobTaskWorkLogId] UNIQUEIDENTIFIER NOT NULL, 
    PRIMARY KEY ([TimesheetItemId], [ProjectJobTaskWorkLogId]), 
    CONSTRAINT [FK_tblTimesheetedTask_ToTimesheetItem] FOREIGN KEY ([TimesheetItemId]) REFERENCES [gce].[tblTimesheetItem]([Id]), 
    CONSTRAINT [FK_tblTimesheetedTask_ToTable] FOREIGN KEY ([ProjectJobTaskWorkLogId]) REFERENCES [gce].[tblProjectJobTaskWorkLog]([Id])
)
