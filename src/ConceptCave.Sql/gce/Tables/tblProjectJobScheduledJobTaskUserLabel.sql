﻿CREATE TABLE [gce].[tblProjectJobScheduledJobTaskUserLabel]
(
	[ProjectJobScheduledTaskId] UNIQUEIDENTIFIER NOT NULL , 
    [LabelId] UNIQUEIDENTIFIER NOT NULL, 
    PRIMARY KEY ([LabelId], [ProjectJobScheduledTaskId]), 
    CONSTRAINT [FK_tblProjectJobScheduledJobTaskUserLabel_ToProjectJobScheduledTask] FOREIGN KEY ([ProjectJobScheduledTaskId]) REFERENCES [gce].[tblProjectJobScheduledTask]([Id]), 
    CONSTRAINT [FK_tblProjectJobScheduledJobTaskUserLabel_ToLabel] FOREIGN KEY ([LabelId]) REFERENCES [dbo].[tblLabel]([Id])
)
