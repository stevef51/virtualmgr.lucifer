﻿CREATE TABLE [gce].[tblProjectJobTaskWorkLogTiming]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY, 
    [DateCreated] DATETIME NOT NULL, 
    [DateCompleted] DATETIME NULL, 
    [CreationQueueTimeSeconds] DECIMAL(18, 2) NOT NULL, 
    [CompletedQueueTimeSeconds] DECIMAL(18, 2) NULL, 
    CONSTRAINT [FK_tblProjectJobTaskWorkLogTiming_TotblProjectJobTaskWorkLog] FOREIGN KEY ([Id]) REFERENCES [gce].[tblProjectJobTaskWorkLog]([Id])
)
