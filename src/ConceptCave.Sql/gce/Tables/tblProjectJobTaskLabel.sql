﻿CREATE TABLE [gce].[tblProjectJobTaskLabel]
(
	[ProjectJobTaskId] UNIQUEIDENTIFIER NOT NULL ,
	[LabelId] UNIQUEIDENTIFIER NOT NULL, 
    PRIMARY KEY ([ProjectJobTaskId], [LabelId]), 
    CONSTRAINT [FK_tblProjectJobTaskLabel_ProjectJobTask] FOREIGN KEY ([ProjectJobTaskId]) REFERENCES [gce].[tblProjectJobTask]([Id]) ON DELETE CASCADE, 
    CONSTRAINT [FK_tblProjectJobTaskLabel_Label] FOREIGN KEY ([LabelId]) REFERENCES [dbo].[tblLabel]([Id]) ON DELETE CASCADE
)
GO
CREATE NONCLUSTERED INDEX [nci_wi_tblProjectJobTaskLabel_05BE84CAA6D2CFB4B0AE000FE7AFE317] 
	ON [gce].[tblProjectJobTaskLabel] ([LabelId]) 
	WITH (ONLINE = ON)
