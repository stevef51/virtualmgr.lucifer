﻿CREATE TABLE [gce].[tblProjectJobFinishedStatus]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY,
	[Text] NVARCHAR(100) NOT NULL,
	[RequiresExtraNotes] bit NOT NULL DEFAULT 0, 
    [Stage] INT NOT NULL DEFAULT 0
)
