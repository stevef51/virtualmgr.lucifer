﻿CREATE TABLE [gce].[tblProjectJobTaskTypeStatus]
(
	[TaskTypeId] UNIQUEIDENTIFIER NOT NULL ,
	[StatusId] UNIQUEIDENTIFIER NOT NULL, 
	[SortOrder] INT NOT NULL DEFAULT 0,

    [DefaultToWhenCreated] BIT NOT NULL DEFAULT 0, 
    [SetToWhenFinished] BIT NOT NULL DEFAULT 0, 
    [ReferenceNumberMode] INT NOT NULL DEFAULT 0, 
    CONSTRAINT [FK_tblProjectJobTaskTypeStatus_TaskType] FOREIGN KEY ([TaskTypeId]) REFERENCES [gce].[tblProjectJobTaskType]([Id]) ON DELETE CASCADE, 
    CONSTRAINT [FK_tblProjectJobTaskTypeStatus_Status] FOREIGN KEY ([StatusId]) REFERENCES [gce].[tblProjectJobStatus]([Id]) ON DELETE CASCADE, 
    PRIMARY KEY ([TaskTypeId], [StatusId])
)
