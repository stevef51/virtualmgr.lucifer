﻿CREATE TABLE [gce].[tblProjectJobTaskFinishedStatus]
(
	[TaskId] UNIQUEIDENTIFIER NOT NULL ,
	[ProjectJobFinishedStatusId] UNIQUEIDENTIFIER NOT NULL, 
    [Notes] NVARCHAR(4000) NULL, 
    PRIMARY KEY ([TaskId], [ProjectJobFinishedStatusId]), 
    CONSTRAINT [FK_tblProjectJobTaskFinishedStatus_ToTask] FOREIGN KEY ([TaskId]) REFERENCES [gce].[tblProjectJobTask]([Id]) ON DELETE CASCADE, 
    CONSTRAINT [FK_tblProjectJobTaskFinishedStatus_ToFinishedStatus] FOREIGN KEY ([ProjectJobFinishedStatusId]) REFERENCES [gce].[tblProjectJobFinishedStatus]([Id]),
)
