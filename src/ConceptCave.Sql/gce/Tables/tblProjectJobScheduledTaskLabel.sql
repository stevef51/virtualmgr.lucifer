﻿CREATE TABLE [gce].[tblProjectJobScheduledTaskLabel]
(
	[ProjectJobScheduledTaskId] UNIQUEIDENTIFIER NOT NULL ,
	[LabelId] UNIQUEIDENTIFIER NOT NULL, 
    PRIMARY KEY ([ProjectJobScheduledTaskId], [LabelId]), 
    CONSTRAINT [FK_tblProjectJobScheduledTaskLabel_ScheduledTask] FOREIGN KEY ([ProjectJobScheduledTaskId]) REFERENCES [gce].[tblProjectJobScheduledTask]([Id]) ON DELETE CASCADE, 
    CONSTRAINT [FK_tblProjectJobScheduledTaskLabel_Label] FOREIGN KEY ([LabelId]) REFERENCES [dbo].[tblLabel]([Id]) ON DELETE CASCADE
)
