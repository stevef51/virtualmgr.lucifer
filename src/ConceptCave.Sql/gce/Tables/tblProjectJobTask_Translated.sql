﻿CREATE TABLE [gce].[tblProjectJobTask_Translated]
(
	[Id] UNIQUEIDENTIFIER NOT NULL , 
    [CultureName] NVARCHAR(10) NOT NULL, 
    [Name] NVARCHAR(4000) NOT NULL, 
    [Description] NVARCHAR(MAX) NULL, 
    PRIMARY KEY ([Id], [CultureName]), 
    CONSTRAINT [FK_tblProjectJobScheduledTask_Translated_ToTask] FOREIGN KEY ([Id]) REFERENCES [gce].[tblProjectJobTask]([Id]) ON DELETE CASCADE
)
