﻿CREATE TABLE [gce].[tblProjectJobTaskTypePublishingGroupResource]
(
	[TaskTypeId] UNIQUEIDENTIFIER NOT NULL, 
    [PublishingGroupResourceId] INT NOT NULL, 
    [PreventFinishing] BIT NOT NULL DEFAULT 0, 
    [Mandatory] BIT NOT NULL DEFAULT 0, 
    [AllowMultiple] BIT NOT NULL DEFAULT 0, 
    CONSTRAINT [PK_tblProjectJobTaskTypePublishingGroupResource] PRIMARY KEY ([TaskTypeId], [PublishingGroupResourceId]), 
    CONSTRAINT [FK_tblProjectJobTaskTypePublishingGroupResource_TaskType] FOREIGN KEY ([TaskTypeId]) REFERENCES [gce].[tblProjectJobTaskType]([Id]), 
    CONSTRAINT [FK_tblProjectJobTaskTypePublishingGroupResource_ToTable] FOREIGN KEY ([PublishingGroupResourceId]) REFERENCES [dbo].[tblPublishingGroupResource]([Id])
)
