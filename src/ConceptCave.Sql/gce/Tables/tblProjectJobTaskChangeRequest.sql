﻿CREATE TABLE [gce].[tblProjectJobTaskChangeRequest]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY,
	[ProjectJobTaskId] UNIQUEIDENTIFIER NOT NULL,
	[ChangeType] INT NOT NULL DEFAULT 1,
	[RequestedByUserId] UNIQUEIDENTIFIER NOT NULL,
	[ProcessedByUserId] UNIQUEIDENTIFIER NULL,
	[FromRosterId] INT NULL,
	[ToRosterId] INT NULL, 
    [Status] INT NOT NULL DEFAULT 0, 
    CONSTRAINT [FK_tblProjectJobTaskChangeRequest_ToProjectJobTask] FOREIGN KEY ([ProjectJobTaskId]) REFERENCES [gce].[tblProjectJobTask]([Id]) ON DELETE CASCADE, 
    CONSTRAINT [FK_tblProjectJobTaskChangeRequest_ToUserDataViaRequestedBy] FOREIGN KEY ([RequestedByUserId]) REFERENCES [dbo].[tblUserData]([UserId]), 
    CONSTRAINT [FK_tblProjectJobTaskChangeRequest_ToUserDataViaProcessedBy] FOREIGN KEY ([ProcessedByUserId]) REFERENCES [dbo].[tblUserData]([UserId]), 
    CONSTRAINT [FK_tblProjectJobTaskChangeRequest_ToRosterViaFromRoster] FOREIGN KEY ([FromRosterId]) REFERENCES [gce].[tblRoster]([Id]), 
    CONSTRAINT [FK_tblProjectJobTaskChangeRequest_ToRosterViaToRoster] FOREIGN KEY ([ToRosterId]) REFERENCES [gce].[tblRoster]([Id])
)

GO

CREATE INDEX [IX_tblProjectJobTaskChangeRequest_ToRoster] ON [gce].[tblProjectJobTaskChangeRequest] ([ToRosterId])

GO

CREATE INDEX [IX_tblProjectJobTaskChangeRequest_ProjectJobTask] ON [gce].[tblProjectJobTaskChangeRequest] ([ProjectJobTaskId])
