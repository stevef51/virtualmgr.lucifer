﻿CREATE TABLE [gce].[tblProjectJobTaskTypeFinishedStatus]
(
	[Id] UNIQUEIDENTIFIER NOT NULL,
	[TaskTypeId] UNIQUEIDENTIFIER NOT NULL,
	[ProjectJobFinishedStatusId] UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [FK_tblProjectJobTaskTypeFinishedStatus_ToTaskType] FOREIGN KEY ([TaskTypeId]) REFERENCES [gce].[tblProjectJobTaskType]([Id]), 
    CONSTRAINT [FK_tblProjectJobTaskTypeFinishedStatus_ToFinishedStatus] FOREIGN KEY ([ProjectJobFinishedStatusId]) REFERENCES [gce].[tblProjectJobFinishedStatus]([Id]), 
    CONSTRAINT [PK_tblProjectJobTaskTypeFinishedStatus] PRIMARY KEY ([Id])
)

GO


CREATE UNIQUE INDEX [IX_tblProjectJobTaskTypeFinishedStatus_TaskTypeFinishedStatus] ON [gce].[tblProjectJobTaskTypeFinishedStatus] ([TaskTypeId], [ProjectJobFinishedStatusId])
