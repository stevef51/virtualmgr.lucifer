﻿CREATE TABLE [gce].[tblProjectJobTaskAttachment]
(
	[ProjectJobTaskId] UNIQUEIDENTIFIER NOT NULL , 
    [MediaId] UNIQUEIDENTIFIER NOT NULL, 
    PRIMARY KEY ([ProjectJobTaskId], [MediaId]), 
    CONSTRAINT [FK_tblProjectJobTaskAttachment_ToMedia] FOREIGN KEY ([MediaId]) REFERENCES [dbo].[tblMedia]([Id]), 
    CONSTRAINT [FK_tblProjectJobTaskAttachment_ToProjectJobTask] FOREIGN KEY ([ProjectJobTaskId]) REFERENCES [gce].[tblProjectJobTask]([Id]) ON DELETE CASCADE
)
