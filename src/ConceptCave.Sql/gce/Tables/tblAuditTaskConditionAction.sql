﻿CREATE TABLE [gce].[tblAuditTaskConditionAction]
(
	[Id] int NOT NULL IDENTITY,
	[ProjectJobScheduledTaskId] UniqueIdentifier NOT NULL,
	[Group] nvarchar(50) NOT NULL,
	[SampleCount] int NOT NULL DEFAULT 0, 
    CONSTRAINT [FK_tblRandomSampleTaskConditionAction_ToProjectJobScheduledTask] FOREIGN KEY ([ProjectJobScheduledTaskId]) REFERENCES [gce].[tblProjectJobScheduledTask]([Id]) ON DELETE CASCADE, 
    CONSTRAINT [PK_tblRandomSampleTaskConditionAction] PRIMARY KEY ([Id])
)

GO

CREATE UNIQUE INDEX [IX_tblAuditTaskConditionAction_ScheduledTaskIdAndGroup] ON [gce].[tblAuditTaskConditionAction] ([ProjectJobScheduledTaskId], [Group])
