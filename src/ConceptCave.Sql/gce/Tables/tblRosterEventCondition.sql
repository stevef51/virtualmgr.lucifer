﻿CREATE TABLE [gce].[tblRosterEventCondition]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY,
	[RosterId] INT NOT NULL,
	[RosterProjectJobTaskEvents] INT NOT NULL,
	[Condition] NVARCHAR(MAX) NOT NULL,
	[Data] NVARCHAR(MAX), 
    [NextRunTime] DATETIME NULL, 
    CONSTRAINT [FK_tblRosterEventCondition_ToRoster] FOREIGN KEY ([RosterId]) REFERENCES [gce].[tblRoster]([Id]), 
)

GO

CREATE INDEX [IX_tblRosterEventCondition_RosterEvents] ON [gce].[tblRosterEventCondition] ([RosterId], [RosterProjectJobTaskEvents])
