﻿CREATE TABLE [schema].[tblUpdateScripts]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [ScriptName] NVARCHAR(128) NOT NULL, 
	[ScriptTextHashcode] INT NOT NULL,
    [AppliedDate] DATETIME NOT NULL, 
    [Success] BIT NOT NULL, 
    [ResultText] NVARCHAR(MAX) NOT NULL
)
