﻿
CREATE PROCEDURE [dbo].[sp_tr_GetCountInSet] 
	@Key varchar(255) = 0
AS
BEGIN
	SET NOCOUNT ON;

	SELECT COUNT(1) 
	FROM [dbo].[tr_Set] 
	WHERE [Id] = @Key
END