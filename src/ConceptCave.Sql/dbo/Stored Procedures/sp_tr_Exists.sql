﻿
CREATE PROCEDURE [dbo].[sp_tr_Exists] 
	@Key varchar(255) = 0
AS
BEGIN
	SET NOCOUNT ON;

	IF EXISTS (SELECT 1 FROM [dbo].[tr_String] WHERE [Id] = @Key)
		RETURN 1
	ELSE 
		IF EXISTS (SELECT 1 FROM [dbo].[tr_Object] WHERE [Id] = @Key)
			RETURN 1
		ELSE
			RETURN 0
END