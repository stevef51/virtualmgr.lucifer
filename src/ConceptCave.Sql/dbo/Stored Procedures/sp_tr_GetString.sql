﻿
CREATE PROCEDURE [dbo].[sp_tr_GetString] 
	@Key varchar(255) = 0
AS
BEGIN
	SET NOCOUNT ON;

	SELECT Value 
	FROM [dbo].[tr_String] 
	WHERE Id = @Key
END