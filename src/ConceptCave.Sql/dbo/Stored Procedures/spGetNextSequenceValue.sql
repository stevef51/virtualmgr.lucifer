﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spGetNextSequenceValue] 
	@seq_name nvarchar(50), @seq_value INT output
AS
BEGIN
  DECLARE @ot TABLE (Value INT)
  /* We use the merge statement to either update the sequence value if there is already a sequence with that name, otherwise we create a new sequence */
  MERGE tblSequence as T USING (SELECT @seq_name as Name) as S on T.Name = S.Name
	WHEN matched then
		UPDATE set T.Value = T.Value + 1
	WHEN not matched then
		INSERT (Name, Value) values (S.Name, 1)
	OUTPUT INSERTED.Value INTO @ot;

  SELECT @seq_value = Value FROM @ot;
END