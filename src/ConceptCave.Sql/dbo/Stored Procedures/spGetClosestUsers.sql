﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spGetClosestUsers]
	-- Add the parameters for the stored procedure here
	@latitude as decimal(18,8),
	@longitude as decimal(18,8),
	@count as integer
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT top (@count) userid, latitude, longitude, SQRT(
    POWER(69.1 * (latitude - @latitude), 2) +
    POWER(69.1 * (@longitude - longitude) * COS(latitude / 57.3), 2)) * 1.609344 AS distance
	FROM tbluserdata where latitude is not null and longitude is not null order by distance
	
END