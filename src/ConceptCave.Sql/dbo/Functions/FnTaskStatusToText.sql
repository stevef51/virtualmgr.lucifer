﻿
CREATE FUNCTION [dbo].[FnTaskStatusToText] 
(
	@status INT
)
RETURNS NVARCHAR(50)
AS
BEGIN
	RETURN (CASE 
			WHEN @Status = 0 THEN 'Unapproved'
			WHEN @Status = 1 THEN 'Approved'
			WHEN @Status = 2 THEN 'Started'
			WHEN @Status = 3 THEN 'Paused'
			WHEN @Status = 4 THEN 'FinishedComplete'
			WHEN @Status = 5 THEN 'FinishedIncomplete'
			WHEN @Status = 6 THEN 'Cancelled'
			WHEN @Status = 7 THEN 'FinishedByManagement'
			WHEN @Status = 8 THEN 'ApprovedContinued'
			WHEN @Status = 9 THEN 'ChangeRosterRequested'
			WHEN @Status = 10 THEN 'ChangeRosterRejected'
			WHEN @Status = 11 THEN 'ChangeRosterAccepted'
			WHEN @Status = 12 THEN 'FinishedBySystem'
			ELSE NULL
		END) 
END
GO

