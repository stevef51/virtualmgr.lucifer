﻿
CREATE FUNCTION [dbo].[FnTaskTextToStatus] 
(
	@status NVARCHAR(50)
)
RETURNS INT
AS
BEGIN
	RETURN (CASE 
			WHEN @Status = 'Unapproved' THEN 0
			WHEN @Status = 'Approved' THEN 1
			WHEN @Status = 'Started' THEN 2
			WHEN @Status = 'Paused' THEN 3
			WHEN @Status = 'FinishedComplete' THEN 4
			WHEN @Status = 'FinishedIncomplete' THEN 5
			WHEN @Status = 'Cancelled' THEN 6
			WHEN @Status = 'FinishedByManagement' THEN 7
			WHEN @Status = 'ApprovedContinued' THEN 8
			WHEN @Status = 'ChangeRosterRequested' THEN 9
			WHEN @Status = 'ChangeRosterRejected' THEN 10
			WHEN @Status = 'ChangeRosterAccepted' THEN 11
			WHEN @Status = 'FinishedBySystem' THEN 12
			ELSE NULL
		END) 
END
GO

