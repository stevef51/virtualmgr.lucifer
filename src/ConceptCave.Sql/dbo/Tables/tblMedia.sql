﻿CREATE TABLE [dbo].[tblMedia] (
    [Id]                UNIQUEIDENTIFIER NOT NULL,
    [Filename]          NVARCHAR (200)   NOT NULL,
    [Type]              NVARCHAR (200)   NOT NULL,
    [DateCreated]       DATETIME         CONSTRAINT [DF_tblMedia_DateCreated] DEFAULT (getdate()) NOT NULL,
    [Name]              NVARCHAR (200)   CONSTRAINT [DF_tblMedia_Name] DEFAULT ('None') NOT NULL,
    [Description]       NVARCHAR (4000)  NULL,
    [WorkingDocumentId] UNIQUEIDENTIFIER NULL,
    [Version] INT NOT NULL DEFAULT 1, 
    [DateVersionCreated] DATETIME NOT NULL DEFAULT (getdate()), 
    [UseVersioning] BIT NOT NULL DEFAULT 0, 
    [CreatedBy] UNIQUEIDENTIFIER NULL, 
    [PreviewCount] INT NULL, 
    [SortOrder] INT NOT NULL DEFAULT 0, 
    CONSTRAINT [PK_tblMedia] PRIMARY KEY CLUSTERED ([Id] ASC), 
    CONSTRAINT [FK_tblMedia_CreatedByUser] FOREIGN KEY ([CreatedBy]) REFERENCES [tblUserData]([UserId])
);


