﻿CREATE TABLE [dbo].[tr_String] (
    [Id]    VARCHAR (255)   NOT NULL,
    [Value] NVARCHAR (4000) NOT NULL,
    CONSTRAINT [pk_strng_9912B79F] PRIMARY KEY CLUSTERED ([Id] ASC)
);

