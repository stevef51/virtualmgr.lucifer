﻿CREATE TABLE [dbo].[tblErrorLogType]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
	[Type] NVARCHAR(32) NOT NULL,
    [Source] NVARCHAR(128) NOT NULL,
    [Message] NVARCHAR(256) NOT NULL, 
    [StackTrace] NVARCHAR(MAX) NOT NULL, 
    [StackTraceHash] INT NOT NULL, 
)

GO

CREATE INDEX [IX_tblErrorLogType_Type] ON [dbo].[tblErrorLogType] ([Type])
GO

CREATE INDEX [IX_tblErrorLogType_Source] ON [dbo].[tblErrorLogType] ([Source])
GO

CREATE INDEX [IX_tblErrorLogType_Message] ON [dbo].[tblErrorLogType] ([Message])
GO

CREATE INDEX [IX_tblErrorLogType_StackTraceHash] ON [dbo].[tblErrorLogType] ([StackTraceHash])
