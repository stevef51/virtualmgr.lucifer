﻿CREATE TABLE [dbo].[tr_Set] (
    [Id]     VARCHAR (255) NOT NULL,
    [Member] VARCHAR (255) NOT NULL,
    CONSTRAINT [pk_st_DE59633C] PRIMARY KEY CLUSTERED ([Id] ASC, [Member] ASC)
);

