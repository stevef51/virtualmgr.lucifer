﻿CREATE TABLE [dbo].[tblLabelPublishingGroupResource] (
    [LabelId]                   UNIQUEIDENTIFIER NOT NULL,
    [PublishingGroupResourceId] INT              NOT NULL,
    CONSTRAINT [PK_tblLabelPublishingGroupResource] PRIMARY KEY CLUSTERED ([LabelId] ASC, [PublishingGroupResourceId] ASC),
    CONSTRAINT [FK_tblLabelPublishingGroupResource_tblLabel] FOREIGN KEY ([LabelId]) REFERENCES [dbo].[tblLabel] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_tblLabelPublishingGroupResource_tblPublishingGroupResource] FOREIGN KEY ([PublishingGroupResourceId]) REFERENCES [dbo].[tblPublishingGroupResource] ([Id]) ON DELETE CASCADE
);

