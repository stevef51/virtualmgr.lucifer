﻿CREATE TABLE [dbo].[tblWorkingDocumentStatus] (
    [Id]   INT           NOT NULL,
    [Text] NVARCHAR (25) NOT NULL,
    CONSTRAINT [PK_tblWorkingDocumentStatus] PRIMARY KEY CLUSTERED ([Id] ASC)
);

