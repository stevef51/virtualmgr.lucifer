﻿CREATE TABLE [dbo].[tblReport]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY,
	[Name] NVARCHAR(50) NOT NULL,
	[Description] NVARCHAR(max), 
    [Type] INT NOT NULL DEFAULT 0, 
    [Configuration] NVARCHAR(MAX) NULL
)
