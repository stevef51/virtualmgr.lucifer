﻿CREATE TABLE [dbo].[tblPublishingGroup] (
    [Id]                    INT            IDENTITY (1, 1) NOT NULL,
    [Name]                  NVARCHAR (200) NOT NULL,
    [AllowUseAsUserContext] BIT            CONSTRAINT [DF_tblPublishingGroup_AllowUseAsUserContext] DEFAULT ((0)) NOT NULL,
    [Description]           NVARCHAR (200) CONSTRAINT [DF__tblPublis__Descr__2AD55B43] DEFAULT ('') NULL,
    [AllowUseAsDashboard]   BIT            CONSTRAINT [DF_tblPublishingGroup_AllowUseAsDashboard] DEFAULT ((0)) NOT NULL,
    [AllowUseAsTaskWorkflow] BIT NOT NULL DEFAULT 0, 
    [AllowUseAsAssetContext] BIT NOT NULL DEFAULT 0, 
    CONSTRAINT [PK_tblPublished] PRIMARY KEY CLUSTERED ([Id] ASC)
);


