﻿CREATE TABLE [dbo].[tblWorkingDocumentReference] (
    [Id]                UNIQUEIDENTIFIER NOT NULL,
    [WorkingDocumentId] UNIQUEIDENTIFIER NOT NULL,
    [Name]              NVARCHAR (200)   NOT NULL,
    [Url]               NVARCHAR (2000)  NOT NULL,
    CONSTRAINT [PK_tblWorkingDocumentReference] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_tblWorkingDocumentReference_tblWorkingDocument] FOREIGN KEY ([WorkingDocumentId]) REFERENCES [dbo].[tblWorkingDocument] ([Id]) ON DELETE CASCADE
);

