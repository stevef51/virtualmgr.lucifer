﻿CREATE TABLE [dbo].[tblMediaPreview] (
    [MediaId] UNIQUEIDENTIFIER NOT NULL,
	[Page] INT NOT NULL,
    [Data]    VARBINARY (MAX)  NOT NULL,
    CONSTRAINT [PK_tblMediaPreview] PRIMARY KEY CLUSTERED ([MediaId], [Page]),
    CONSTRAINT [FK_tblMediaPreview_tblMedia] FOREIGN KEY ([MediaId]) REFERENCES [dbo].[tblMedia] ([Id]) ON DELETE CASCADE
);

