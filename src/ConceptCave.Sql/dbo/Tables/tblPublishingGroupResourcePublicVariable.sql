﻿CREATE TABLE [dbo].[tblPublishingGroupResourcePublicVariable] (
    [Id]                        INT            IDENTITY (1, 1) NOT NULL,
    [PublishingGroupResourceId] INT            NOT NULL,
    [Name]                      NVARCHAR (255) NOT NULL,
    CONSTRAINT [PK_tblPublishingGroupResourcePublicVariable] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_tblPublishingGroupResourcePublicVariable_tblPublishingGroupResource] FOREIGN KEY ([PublishingGroupResourceId]) REFERENCES [dbo].[tblPublishingGroupResource] ([Id]) ON DELETE CASCADE
);

