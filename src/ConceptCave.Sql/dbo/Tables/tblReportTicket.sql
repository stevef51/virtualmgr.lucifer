﻿CREATE TABLE [dbo].[tblReportTicket]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY, 
    [ReportId] INT NOT NULL, 
    [JsonParameters] NVARCHAR(MAX) NULL, 
    [RunAsUserId] UNIQUEIDENTIFIER NOT NULL, 
    [DefaultFormat] NVARCHAR(50) NULL, 
    [DateCreatedUtc] DATETIME NOT NULL DEFAULT GETUTCDATE(), 
    [LastRunOnUtc] DATETIME NULL, 
    CONSTRAINT [FK_tblReportTicket_Report] FOREIGN KEY ([ReportId]) REFERENCES [tblReport]([Id])
)
