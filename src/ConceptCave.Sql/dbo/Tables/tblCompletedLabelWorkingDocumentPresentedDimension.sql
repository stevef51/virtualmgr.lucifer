﻿CREATE TABLE [dbo].[tblCompletedLabelWorkingDocumentPresentedDimension] (
    [CompletedLabelDimensionId]               UNIQUEIDENTIFIER NOT NULL,
    [CompletedWorkingDocumentPresentedFactId] UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_tblCompletedLabelWorkingDocumentPresentedDimension] PRIMARY KEY CLUSTERED ([CompletedLabelDimensionId] ASC, [CompletedWorkingDocumentPresentedFactId] ASC),
    CONSTRAINT [FK_tblCompletedLabelWorkingDocumentPresentedDimension_tblCompletedLabelDimension] FOREIGN KEY ([CompletedLabelDimensionId]) REFERENCES [dbo].[tblCompletedLabelDimension] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_tblCompletedLabelWorkingDocumentPresentedDimension_tblCompletedWorkingDocumentPresentedFact] FOREIGN KEY ([CompletedWorkingDocumentPresentedFactId]) REFERENCES [dbo].[tblCompletedWorkingDocumentPresentedFact] ([Id]) ON DELETE CASCADE
);


