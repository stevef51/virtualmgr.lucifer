﻿CREATE TABLE [dbo].[tblHierarchyRole] (
    [Id]       INT          IDENTITY (1, 1) NOT NULL,
    [Name]     VARCHAR (50) NOT NULL,
    [IsPerson] BIT          CONSTRAINT [DF_tblHierarchyRole_IsPerson] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_tblHierarchyRole] PRIMARY KEY CLUSTERED ([Id] ASC)
);


