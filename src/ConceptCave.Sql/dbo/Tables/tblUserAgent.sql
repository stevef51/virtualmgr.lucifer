﻿CREATE TABLE [dbo].[tblUserAgent] (
    [Id]        INT            IDENTITY (1, 1) NOT NULL,
    [UserAgent] NVARCHAR (256) NOT NULL,
    CONSTRAINT [PK_tblUserAgent] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO

CREATE INDEX [IX_tblUserAgent_UserAgent] ON [dbo].[tblUserAgent] ([UserAgent])
