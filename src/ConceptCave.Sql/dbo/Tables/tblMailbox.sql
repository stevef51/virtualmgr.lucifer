﻿CREATE TABLE [dbo].[tblMailbox] (
    [Id]        UNIQUEIDENTIFIER NOT NULL,
    [UserId]    UNIQUEIDENTIFIER NOT NULL,
    [MessageId] UNIQUEIDENTIFIER NOT NULL,
    [IsRead]    BIT              NULL,
    CONSTRAINT [PK_tblMailbox] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_tblMailbox_tblMessages] FOREIGN KEY ([MessageId]) REFERENCES [dbo].[tblMessages] ([Id]),
    CONSTRAINT [FK_tblMailbox_tblUserData] FOREIGN KEY ([UserId]) REFERENCES [dbo].[tblUserData] ([UserId])
);

