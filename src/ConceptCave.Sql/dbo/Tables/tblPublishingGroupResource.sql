﻿CREATE TABLE [dbo].[tblPublishingGroupResource] (
    [Id]                        INT              IDENTITY (1, 1) NOT NULL,
    [ResourceId]                INT              NOT NULL,
    [PublishingGroupId]         INT              NOT NULL,
    [Name]                      NVARCHAR (50)    CONSTRAINT [DF_tblPublishingGroupResource_Name] DEFAULT ('') NOT NULL,
    [IfNotFinishedCleanUpAfter] INT              NULL,
    [IfFinishedCleanUpAfter]    INT              NULL,
    [PopulateReportingData]     BIT              CONSTRAINT [DF_tblPublishingGroupResource_PopulateReportingData] DEFAULT ((1)) NOT NULL,
    [UtcLastModified]           DATETIME         CONSTRAINT [DF_tblPublishingGroupResource_UtcLastModified] DEFAULT (getutcdate()) NOT NULL,
    [PictureId]                 UNIQUEIDENTIFIER NULL,
    [Description]               NVARCHAR (200)   CONSTRAINT [DF__tblPublis__Descr__2BC97F7C] DEFAULT ('') NOT NULL,
    [EstimatedPageCount] INT NOT NULL DEFAULT 1, 
    CONSTRAINT [PK_tblPublishingGroupResource] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_tblPublishingGroupResource_tblMedia] FOREIGN KEY ([PictureId]) REFERENCES [dbo].[tblMedia] ([Id]),
    CONSTRAINT [FK_tblPublishingGroupResource_tblPublishingGroup] FOREIGN KEY ([PublishingGroupId]) REFERENCES [dbo].[tblPublishingGroup] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_tblPublishingGroupResource_tblResource] FOREIGN KEY ([ResourceId]) REFERENCES [dbo].[tblResource] ([Id]) ON DELETE CASCADE
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tblPublishingGroupResource]
    ON [dbo].[tblPublishingGroupResource]([PublishingGroupId] ASC, [ResourceId] ASC);


