﻿CREATE TABLE [dbo].[tblMediaViewing]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY, 
    [MediaId] UNIQUEIDENTIFIER NOT NULL, 
    [UserId] UNIQUEIDENTIFIER NOT NULL, 
	[WorkingDocumentId] UNIQUEIDENTIFIER NOT NULL,
    [Version] INT NOT NULL, 
    [DateStarted] DATETIME NOT NULL, 
    [DateCompleted] DATETIME NOT NULL, 
    [TotalSeconds] INT NOT NULL, 
    [Accepted] BIT NOT NULL DEFAULT 0, 
    [AcceptionNotes] NVARCHAR(4000) NULL, 
    CONSTRAINT [FK_tblMediaViewing_MediaId] FOREIGN KEY ([MediaId]) REFERENCES [tblMedia]([Id]) ON DELETE CASCADE, 
    CONSTRAINT [FK_tblMediaViewing_User] FOREIGN KEY ([UserId]) REFERENCES [tblUserData]([UserId])
)

GO

CREATE INDEX [IX_tblMediaViewing_MediaId] ON [dbo].[tblMediaViewing] ([MediaId])

GO

CREATE INDEX [IX_tblMediaViewing_UserId] ON [dbo].[tblMediaViewing] ([UserId])

GO

CREATE INDEX [IX_tblMediaViewing_WorkingDocumentId] ON [dbo].[tblMediaViewing] ([WorkingDocumentId])
