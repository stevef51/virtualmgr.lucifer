﻿CREATE TABLE [dbo].[tblCompletedLabelDimension] (
    [Id]        UNIQUEIDENTIFIER NOT NULL,
    [Name]      NVARCHAR (50)    NOT NULL,
    [Forecolor] INT              NOT NULL,
    [Backcolor] INT              NOT NULL,
    CONSTRAINT [PK_tblReportingLabel] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_tblReportingLabel]
    ON [dbo].[tblCompletedLabelDimension]([Name] ASC);


