﻿CREATE TABLE [dbo].[tblUserTypeContextPublishedResource] (
    [Id]                        INT IDENTITY (1, 1) NOT NULL,
    [UserTypeId]                INT NOT NULL,
    [PublishingGroupResourceId] INT NOT NULL,
    CONSTRAINT [PK_tblUserTypeContextPublishedResource] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_tblUserTypeContextPublishedResource_tblPublishingGroupResource] FOREIGN KEY ([PublishingGroupResourceId]) REFERENCES [dbo].[tblPublishingGroupResource] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_tblUserTypeContextPublishedResource_tblUserType] FOREIGN KEY ([UserTypeId]) REFERENCES [dbo].[tblUserType] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [IX_tblUserTypeContextPublishedResource] UNIQUE NONCLUSTERED ([UserTypeId] ASC, [PublishingGroupResourceId] ASC)
);


