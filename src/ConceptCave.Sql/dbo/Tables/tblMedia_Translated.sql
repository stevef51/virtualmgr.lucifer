﻿CREATE TABLE [dbo].[tblMedia_Translated]
(
	[Id] [uniqueidentifier] NOT NULL,
	[CultureName] [nvarchar](10) NOT NULL,
	[Filename] [nvarchar](200) NOT NULL,
	[Name] [nvarchar](200) NOT NULL,
	[Description] [nvarchar](4000) NOT NULL,
	PRIMARY KEY ([Id], [CultureName]),
	CONSTRAINT [FK_Media_Translated_Id_Media_Id] FOREIGN KEY ([Id]) REFERENCES [tblMedia] ([Id]) ON DELETE CASCADE

)
