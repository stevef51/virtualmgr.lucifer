﻿CREATE TABLE [dbo].[tblUserData] (
    [UserId]                   UNIQUEIDENTIFIER DEFAULT(NEWSEQUENTIALID()) NOT NULL,
    [UserTypeId]               INT              CONSTRAINT [DF_tblMembership_MembershipTypeId] DEFAULT ((1)) NOT NULL,
    [Name]                     NVARCHAR (200)   NULL,
    [AuthorizationProviderKey] NVARCHAR (100)   NULL,
    [TimeZone]                 NVARCHAR (50)    CONSTRAINT [DF_tblUserData_TimeZone] DEFAULT (N'UTC') NOT NULL,
    [Latitude]                 DECIMAL (18, 8)  NULL,
    [Longitude]                DECIMAL (18, 8)  NULL,
    [ExpiryDate]               DATETIME         NULL,
    [CompanyId] UNIQUEIDENTIFIER NULL, 
    [MediaId] UNIQUEIDENTIFIER NULL, 
    [CommencementDate] DATETIME NULL, 
    [DefaultTabletProfileId] INT NULL, 
    [Mobile] NVARCHAR(50) NULL, 
    [QRCode] NVARCHAR(10) NULL, 
    [DefaultLanguageCode] NVARCHAR(20) NOT NULL DEFAULT 'en', 
    [AspNetUserId] NVARCHAR(128) NULL, 
    CONSTRAINT [PK_tblMembership_1] PRIMARY KEY CLUSTERED ([UserId] ASC),
    CONSTRAINT [FK_tblMembership_tblMembershipType] FOREIGN KEY ([UserTypeId]) REFERENCES [dbo].[tblUserType] ([Id]),
    CONSTRAINT [FK_tblUserData_Users] FOREIGN KEY ([AspNetUserId]) REFERENCES [dbo].[AspNetUsers] ([Id]) ON DELETE CASCADE, 
    CONSTRAINT [FK_tblUserData_ToCompany] FOREIGN KEY ([CompanyId]) REFERENCES [gce].[tblCompany]([Id]), 
    CONSTRAINT [FK_tblUserData_ToMedia] FOREIGN KEY ([MediaId]) REFERENCES [tblMedia]([Id]) ON DELETE CASCADE, 
    CONSTRAINT [FK_tblUserData_ToTabletProfile] FOREIGN KEY ([DefaultTabletProfileId]) REFERENCES [asset].[tblTabletProfile]([Id])
);


GO

CREATE INDEX [IX_tblUserData_AspNetUserId] ON [dbo].[tblUserData] ([AspNetUserId])
