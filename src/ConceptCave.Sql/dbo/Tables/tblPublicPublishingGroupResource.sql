﻿CREATE TABLE [dbo].[tblPublicPublishingGroupResource] (
    [PublishingGroupResourceId] INT              NOT NULL,
    [PublicTicketId]            UNIQUEIDENTIFIER NOT NULL,
    [UserId]                    UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_tblPublicPublishingGroupResource] PRIMARY KEY CLUSTERED ([PublishingGroupResourceId] ASC, [PublicTicketId] ASC),
    CONSTRAINT [FK_tblPublicPublishingGroupResource_tblPublicTicket] FOREIGN KEY ([PublicTicketId]) REFERENCES [dbo].[tblPublicTicket] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_tblPublicPublishingGroupResource_tblPublishingGroupResource] FOREIGN KEY ([PublishingGroupResourceId]) REFERENCES [dbo].[tblPublishingGroupResource] ([Id]) ON DELETE CASCADE
);

