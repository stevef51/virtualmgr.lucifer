﻿CREATE TABLE [dbo].[tblPublishingGroupResourceData] (
    [PublishingGroupResourceId] INT  NOT NULL,
    [Data]                      NTEXT NOT NULL,
    CONSTRAINT [PK_tblPublishingGroupResourceData] PRIMARY KEY CLUSTERED ([PublishingGroupResourceId] ASC),
    CONSTRAINT [FK_tblPublishingGroupResourceData_tblPublishingGroupResource] FOREIGN KEY ([PublishingGroupResourceId]) REFERENCES [dbo].[tblPublishingGroupResource] ([Id]) ON DELETE CASCADE
);


