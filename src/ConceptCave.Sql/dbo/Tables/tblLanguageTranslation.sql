﻿CREATE TABLE [dbo].[tblLanguageTranslation]
(
    [CultureName] NVARCHAR(10) NOT NULL, 
    [TranslationId] NVARCHAR(1024) NOT NULL, 
    [NativeText] NVARCHAR(1024) NOT NULL, 
    CONSTRAINT [PK_tblLanguageTranslation] PRIMARY KEY ([CultureName], [Translationid])
)

GO

CREATE INDEX [IX_tblLanguageTranslation_CultureName] ON [dbo].[tblLanguageTranslation] ([CultureName])
