﻿CREATE TABLE [dbo].[tblArticleNumber] (
    [ArticleId] UNIQUEIDENTIFIER NOT NULL,
    [Name]      NVARCHAR (50)    NOT NULL,
    [Index]     INT              NOT NULL,
    [Value]     MONEY            NOT NULL,
    CONSTRAINT [PK_tblArticleNumber] PRIMARY KEY CLUSTERED ([ArticleId] ASC, [Name] ASC, [Index] ASC),
    CONSTRAINT [FK_tblArticleNumber_ArticleId] FOREIGN KEY ([ArticleId]) REFERENCES [dbo].[tblArticle] ([Id]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_tblArticleNumber_Index]
    ON [dbo].[tblArticleNumber]([Index] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_tblArticleNumber_Name]
    ON [dbo].[tblArticleNumber]([Name] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_tblArticleNumber_Value]
    ON [dbo].[tblArticleNumber]([Value] ASC);


