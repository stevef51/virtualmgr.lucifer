﻿CREATE TABLE [dbo].[tblMediaVersionData]
(
	[MediaVersionId] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY, 
    [Data] VARBINARY(MAX) NOT NULL, 
    CONSTRAINT [FK_tblMediaVersionData_MediaVersion] FOREIGN KEY ([MediaVersionId]) REFERENCES [tblMediaVersion]([Id]) ON DELETE CASCADE
)
