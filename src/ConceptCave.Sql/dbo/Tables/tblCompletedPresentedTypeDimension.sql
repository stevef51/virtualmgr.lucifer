﻿CREATE TABLE [dbo].[tblCompletedPresentedTypeDimension] (
    [Id]   UNIQUEIDENTIFIER NOT NULL,
    [Type] NVARCHAR (200)   NOT NULL,
    CONSTRAINT [PK_tblCompletedPresentedDimension] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_tblCompletedPresentedDimension]
    ON [dbo].[tblCompletedPresentedTypeDimension]([Type] ASC);


