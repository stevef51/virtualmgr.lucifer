﻿CREATE TABLE [dbo].[tblQRCode]
(	
	[Id] INT NOT NULL PRIMARY KEY IDENTITY,
	[QRCode] NVARCHAR(32) NOT NULL, 
    [UserId] UNIQUEIDENTIFIER NULL, 
    [TaskTypeId] UNIQUEIDENTIFIER NULL, 
    [AssetId] INT NULL, 
    [Selected] BIT NOT NULL DEFAULT 0, 
    CONSTRAINT [FK_tblQRCode_ToUserData] FOREIGN KEY ([UserId]) REFERENCES [tblUserData]([UserId]),
    CONSTRAINT [FK_tblQRCode_ToTaskType] FOREIGN KEY ([TaskTypeId]) REFERENCES [gce].[tblProjectJobTaskType]([Id]),
    CONSTRAINT [FK_tblQRCode_ToAsset] FOREIGN KEY ([AssetId]) REFERENCES [asset].[tblAsset]([Id])
)

GO

CREATE UNIQUE INDEX [IX_tblQRCode_QRCode] ON [dbo].[tblQRCode] ([QRCode])
