﻿CREATE TABLE [dbo].[tblPublishingGroupResourceActorCalendar]
(
	[PublishingGroupResourceCalendarId] uniqueidentifier NOT NULL , 
    [NextRunDate] DATETIME NOT NULL, 
    CONSTRAINT [PK_tblPublishingGroupResourceActorCalendar] PRIMARY KEY ([PublishingGroupResourceCalendarId]), 
    CONSTRAINT [FK_tblPublishingGroupResourceActorCalendar_ToPublishingGroupResourceCalendar] FOREIGN KEY ([PublishingGroupResourceCalendarId]) REFERENCES [dbo].[tblPublishingGroupResourceCalendar]([Id]), 
)
