﻿CREATE TABLE [dbo].[tblUserTypeDefaultRole]
(
	[UserTypeId] INT NOT NULL , 
    [RoleId] NVARCHAR(128) NOT NULL, 
    CONSTRAINT [FK_tblUserTypeDefaultRole_ToUserType] FOREIGN KEY ([UserTypeId]) REFERENCES [tblUserType]([Id]), 
    CONSTRAINT [FK_tblUserTypeDefaultRole_ToRole] FOREIGN KEY ([RoleId]) REFERENCES [AspNetRoles]([Id]), 
    PRIMARY KEY ([UserTypeId], [RoleId])
)
