﻿CREATE TABLE [dbo].[tblWorkingDocumentData] (
    [WorkingDocumentId] UNIQUEIDENTIFIER NOT NULL,
    [Data]              NTEXT             NOT NULL,
    [UtcLastModified]   DATETIME         CONSTRAINT [DF_tblWorkingDocumentData_UtcLastModified] DEFAULT (getutcdate()) NOT NULL,
    CONSTRAINT [PK_tblWorkingDocumentData] PRIMARY KEY CLUSTERED ([WorkingDocumentId] ASC),
    CONSTRAINT [FK_tblWorkingDocumentData_tblWorkingDocument] FOREIGN KEY ([WorkingDocumentId]) REFERENCES [dbo].[tblWorkingDocument] ([Id]) ON DELETE CASCADE
);


