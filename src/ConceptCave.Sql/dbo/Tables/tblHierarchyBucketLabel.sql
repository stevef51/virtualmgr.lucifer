﻿CREATE TABLE [dbo].[tblHierarchyBucketLabel]
(
	[HierarchyBucketId] INT NOT NULL , 
    [LabelId] UNIQUEIDENTIFIER NOT NULL, 
    PRIMARY KEY ([HierarchyBucketId], [LabelId]), 
    CONSTRAINT [FK_tblHierarchyBucketLabel_TotblHierarchyBucket] FOREIGN KEY ([HierarchyBucketId]) REFERENCES [tblHierarchyBucket]([Id]) ON DELETE CASCADE, 
    CONSTRAINT [FK_tblHierarchyBucketLabel_TotblLabel] FOREIGN KEY ([LabelId]) REFERENCES [tblLabel]([Id]) ON DELETE CASCADE
)
