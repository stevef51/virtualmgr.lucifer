﻿CREATE TABLE [dbo].[tblCompletedUserDimension] (
    [Id]       UNIQUEIDENTIFIER NOT NULL,
    [Username] NVARCHAR (256)    NOT NULL,
    [Name]     NVARCHAR (200)   NOT NULL,
    [Email]    NVARCHAR (256)   NULL,
    [TimeZone] NVARCHAR (50)    CONSTRAINT [DF_tblCompletedUserDimension_TimeZone] DEFAULT ('UTC') NOT NULL,
    CONSTRAINT [PK_tblReportingUser] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_tblCompletedUserDimension]
    ON [dbo].[tblCompletedUserDimension]([Username] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_tblReportingUser]
    ON [dbo].[tblCompletedUserDimension]([Name] ASC);


