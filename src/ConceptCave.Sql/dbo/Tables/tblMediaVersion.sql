﻿CREATE TABLE [dbo].[tblMediaVersion]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY, 
    [MediaId] UNIQUEIDENTIFIER NOT NULL, 
    [Version] INT NOT NULL, 
	[Filename] NVARCHAR(200) NOT NULL,
	[Name] NVARCHAR(200) NOT NULL,
	[Description] NVARCHAR(4000) NULL,
    [DateVersionCreated] DATETIME NOT NULL, 
    [DateVersionClosed] DATETIME NOT NULL DEFAULT getdate(), 
    [VersionCreatedBy] UNIQUEIDENTIFIER NOT NULL, 
    [VersionReplacedBy] UNIQUEIDENTIFIER NOT NULL, 
    CONSTRAINT [FK_tblMediaVersion_Media] FOREIGN KEY ([MediaId]) REFERENCES [tblMedia]([Id]) ON DELETE CASCADE, 
    CONSTRAINT [FK_tblMediaVersion_UserCreatedBy] FOREIGN KEY ([VersionCreatedBy]) REFERENCES [tblUserData]([UserId]), 
    CONSTRAINT [FK_tblMediaVersion_UserReplacedBy] FOREIGN KEY ([VersionReplacedBy]) REFERENCES [tblUserData]([UserId])
)

GO

CREATE INDEX [IX_tblMediaVersion_MediaId] ON [dbo].[tblMediaVersion] ([MediaId])
