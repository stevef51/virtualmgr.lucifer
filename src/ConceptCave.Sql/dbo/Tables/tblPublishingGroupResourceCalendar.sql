﻿CREATE TABLE [dbo].[tblPublishingGroupResourceCalendar]
(
	[Id] uniqueidentifier NOT NULL,
	[PublishingGroupResourceId] INT NOT NULL , 
    [CalendarEventId] INT NOT NULL, 
    PRIMARY KEY ([Id]), 
    CONSTRAINT [FK_tblPublishingGroupResourceCalendar_ToPublishingGroupResource] FOREIGN KEY ([PublishingGroupResourceId]) REFERENCES [dbo].[tblPublishingGroupResource]([Id]),
	CONSTRAINT [FK_tblPublishingGroupResourceCalendar_ToCalendarEvent] FOREIGN KEY (CalendarEventId) REFERENCES [dbo].[tblCalendarEvent]([Id])
)

GO

CREATE INDEX [IX_tblPublishingGroupResourceCalendar_PublishingGroupResource] ON [dbo].[tblPublishingGroupResourceCalendar] ([PublishingGroupResourceId])

GO

CREATE INDEX [IX_tblPublishingGroupResourceCalendar_CalendarEvent] ON [dbo].[tblPublishingGroupResourceCalendar] ([CalendarEventId])
