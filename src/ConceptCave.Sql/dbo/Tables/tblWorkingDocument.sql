﻿CREATE TABLE [dbo].[tblWorkingDocument] (
    [Id]                        UNIQUEIDENTIFIER NOT NULL,
    [PublishingGroupResourceId] INT              NOT NULL,
    [ReviewerId]                UNIQUEIDENTIFIER NOT NULL,
    [RevieweeId]                UNIQUEIDENTIFIER NULL,
    [Name]                      NVARCHAR (50)    NOT NULL,
    [DateCreated]               DATETIME         CONSTRAINT [DF_tblWorkingDocument_DateCreated] DEFAULT (getdate()) NOT NULL,
    [Lat]                       DECIMAL (18, 7)  NULL,
    [Long]                      DECIMAL (18, 7)  NULL,
    [ClientIP]                  NVARCHAR (48)    NULL,
    [UserAgentId]               INT              NULL,
    [Status]                    INT              NOT NULL,
    [ParentId]                  UNIQUEIDENTIFIER NULL,
    [CleanUpIfNotFinishedAfter] DATETIME         NULL,
    [CleanUpIfFinishedAfter]    DATETIME         NULL,
    [UtcLastModified]           DATETIME         CONSTRAINT [DF_tblWorkingDocument_UtcLastModified] DEFAULT (getutcdate()) NOT NULL,
    CONSTRAINT [PK_tblWorkingDocument] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_tblWorkingDocument_tblPublishingGroupResource] FOREIGN KEY ([PublishingGroupResourceId]) REFERENCES [dbo].[tblPublishingGroupResource] ([Id]),
    CONSTRAINT [FK_tblWorkingDocument_tblUserAgent] FOREIGN KEY ([UserAgentId]) REFERENCES [dbo].[tblUserAgent] ([Id]) ON DELETE SET NULL,
    CONSTRAINT [FK_tblWorkingDocument_tblUserData] FOREIGN KEY ([ReviewerId]) REFERENCES [dbo].[tblUserData] ([UserId]),
    CONSTRAINT [FK_tblWorkingDocument_tblUserData1] FOREIGN KEY ([RevieweeId]) REFERENCES [dbo].[tblUserData] ([UserId]),
    CONSTRAINT [FK_tblWorkingDocument_tblWorkingDocumentStatus] FOREIGN KEY ([Status]) REFERENCES [dbo].[tblWorkingDocumentStatus] ([Id])
);
GO

CREATE NONCLUSTERED INDEX [IX_tblWorkingDocument_DateCreated]
	ON [dbo].tblWorkingDocument([DateCreated] ASC);
GO

