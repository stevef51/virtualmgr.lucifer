﻿CREATE TABLE [dbo].[tblWait] (
    [BlockedId]    UNIQUEIDENTIFIER NOT NULL,
    [WaitingForId] UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_tblWait] PRIMARY KEY CLUSTERED ([BlockedId] ASC, [WaitingForId] ASC)
);

