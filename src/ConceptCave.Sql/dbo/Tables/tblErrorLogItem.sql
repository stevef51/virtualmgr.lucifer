﻿CREATE TABLE [dbo].[tblErrorLogItem]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [ErrorLogTypeId] INT NOT NULL, 
    [DateCreated] DATETIME NOT NULL DEFAULT getutcdate(), 
    [Data] NVARCHAR(MAX) NULL, 
    [UserId] UNIQUEIDENTIFIER NULL, 
    [UserAgentId] INT NULL, 
    CONSTRAINT [FK_tblErrorLogItem_ErrorLogType] FOREIGN KEY ([ErrorLogTypeId]) REFERENCES [tblErrorLogType]([Id]) ON DELETE CASCADE
)

GO

CREATE INDEX [IX_tblErrorLogItem_DateCreated] ON [dbo].[tblErrorLogItem] ([DateCreated])

GO

CREATE INDEX [IX_tblErrorLogItem_ErrorLogTypeId] ON [dbo].[tblErrorLogItem] ([ErrorLogTypeId])
