﻿CREATE TABLE [dbo].[tblPublishingGroupActorLabel] (
    [PublishingGroupId] INT              NOT NULL,
    [LabelId]           UNIQUEIDENTIFIER NOT NULL,
    [PublishingTypeId]  INT              NOT NULL,
    CONSTRAINT [PK_tblPublishingGroupActorLabel] PRIMARY KEY CLUSTERED ([PublishingGroupId] ASC, [LabelId] ASC, [PublishingTypeId] ASC),
    CONSTRAINT [FK_tblPublishingGroupActorLabel_tblLabel] FOREIGN KEY ([LabelId]) REFERENCES [dbo].[tblLabel] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_tblPublishingGroupActorLabel_tblPublishingGroup] FOREIGN KEY ([PublishingGroupId]) REFERENCES [dbo].[tblPublishingGroup] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_tblPublishingGroupActorLabel_tblPublishingGroupActorType] FOREIGN KEY ([PublishingTypeId]) REFERENCES [dbo].[tblPublishingGroupActorType] ([Id]) ON DELETE CASCADE
);

