﻿CREATE TABLE [dbo].[tblUserLog] (
    [Id]              UNIQUEIDENTIFIER NOT NULL,
    [UserId]          UNIQUEIDENTIFIER NOT NULL,
    [LogInTime]       DATETIME         NOT NULL,
    [LogOutTime]      DATETIME         NULL,
    [Latitude]        DECIMAL (18, 8)  NULL,
    [Longitude]       DECIMAL (18, 8)  NULL,
    [NearestLocation] UNIQUEIDENTIFIER NULL,
    [TabletUUID] NVARCHAR(36) NULL, 
    [LoginMediaId] UNIQUEIDENTIFIER NULL, 
    CONSTRAINT [PK_tblUserLog] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_tblUserLog_tblUserData] FOREIGN KEY ([UserId]) REFERENCES [dbo].[tblUserData] ([UserId]),
    CONSTRAINT [FK_tblUserLog_tblUserData1] FOREIGN KEY ([NearestLocation]) REFERENCES [dbo].[tblUserData] ([UserId]),
	CONSTRAINT [FK_tblUserLog_LoginMediaId] FOREIGN KEY ([LoginMediaId]) REFERENCES [dbo].[tblMedia] ([Id]) ON DELETE SET NULL
);

