﻿CREATE TABLE [dbo].[Applications] (
    [ApplicationName] NVARCHAR (235)   NOT NULL,
    [ApplicationId]   UNIQUEIDENTIFIER NOT NULL,
    [Description]     NVARCHAR (256)   NULL,
    CONSTRAINT [PK__tmp_ms_x__C93A4C99AF6A0056] PRIMARY KEY CLUSTERED ([ApplicationId] ASC)
);

