﻿CREATE TABLE [dbo].[tblUserType] (
    [Id]               INT            IDENTITY (1, 1) NOT NULL,
    [Name]             NVARCHAR (50)  NOT NULL,
    [DefaultDashboard] NVARCHAR (255) NULL,
    [HasGeoCordinates] BIT            CONSTRAINT [DF_tblUserType_HasGeoCordinates] DEFAULT ((0)) NOT NULL,
    [IsASite]          BIT            CONSTRAINT [DF_tblUserType_IsASite] DEFAULT ((0)) NOT NULL,
    [CanLogin]         BIT            CONSTRAINT [DF_tblUserType_CanLogin] DEFAULT ((1)) NOT NULL,
    [UserBillingTypeId] INT NULL, 
    CONSTRAINT [PK_tblMembershipType] PRIMARY KEY CLUSTERED ([Id] ASC), 
    CONSTRAINT [FK_tblUserType_tblUserBillingType] FOREIGN KEY ([UserBillingTypeId]) REFERENCES [tblUserBillingType]([Id])
);


