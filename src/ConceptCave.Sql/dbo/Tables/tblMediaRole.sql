﻿CREATE TABLE [dbo].[tblMediaRole]
(
	[MediaId] UNIQUEIDENTIFIER NOT NULL,
	[RoleId] NVARCHAR(128) NOT NULL, 
    CONSTRAINT [PK_tblMediaFileRole] PRIMARY KEY ([MediaId], [RoleId]), 
    CONSTRAINT [FK_tblMediaFileRole_MediaFile] FOREIGN KEY ([MediaId]) REFERENCES [tblMedia]([Id]) ON DELETE CASCADE, 
    CONSTRAINT [FK_tblMediaFolderFileRole_Role] FOREIGN KEY ([RoleId]) REFERENCES [AspNetRoles]([Id]),
)

