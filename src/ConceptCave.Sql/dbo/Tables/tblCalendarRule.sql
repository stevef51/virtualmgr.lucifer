﻿CREATE TABLE [dbo].[tblCalendarRule] (
    [Id]              INT         IDENTITY (1, 1) NOT NULL,
    [DateCreated]     DATETIME    CONSTRAINT [DF_tblCalendarRule_DateCreated] DEFAULT (getutcdate()) NOT NULL,
    [StartDate]       DATETIME    NOT NULL,
    [EndDate]         DATETIME    NULL,
    [Frequency]       NVARCHAR (8) CONSTRAINT [DF_tblCalendarRule_Frequency] DEFAULT ('single') NOT NULL,
    [ExecuteSunday] BIT NOT NULL DEFAULT 1, 
    [ExecuteMonday] BIT NOT NULL DEFAULT 1, 
	[ExecuteTuesday] BIT NOT NULL DEFAULT 1, 
    [ExecuteWednesday] BIT NOT NULL DEFAULT 1, 
	[ExecuteThursday] BIT NOT NULL DEFAULT 1, 
    [ExecuteFriday] BIT NOT NULL DEFAULT 1, 
	[ExecuteSaturday] BIT NOT NULL DEFAULT 1, 
    [CRONExpression] NVARCHAR(100) NULL, 
    [Period] INT NOT NULL DEFAULT 1, 
    CONSTRAINT [PK_tblCalendarRule] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO

CREATE INDEX [IX_tblCalendarRule_StartDate] ON [dbo].[tblCalendarRule] ([StartDate])
