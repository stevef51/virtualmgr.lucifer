﻿CREATE TABLE [dbo].[tblPluginData] (
    [PluginId] UNIQUEIDENTIFIER NOT NULL,
    [Data]     VARBINARY (MAX)  NOT NULL,
    CONSTRAINT [PK_tblPluginData] PRIMARY KEY CLUSTERED ([PluginId] ASC),
    CONSTRAINT [FK_tblPluginData_tblPlugin] FOREIGN KEY ([PluginId]) REFERENCES [dbo].[tblPlugin] ([Id]) ON DELETE CASCADE
);

