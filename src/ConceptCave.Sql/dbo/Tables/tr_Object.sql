﻿CREATE TABLE [dbo].[tr_Object] (
    [Id]    VARCHAR (255) NOT NULL,
    [Value] IMAGE         NOT NULL,
    CONSTRAINT [pk_tr_Object] PRIMARY KEY CLUSTERED ([Id] ASC)
);

