﻿CREATE TABLE [dbo].[tblArticleBool] (
    [ArticleId] UNIQUEIDENTIFIER NOT NULL,
    [Name]      NVARCHAR (50)    NOT NULL,
    [Index]     INT              NOT NULL,
    [Value]     BIT              NOT NULL,
    CONSTRAINT [PK_tblArticleBool] PRIMARY KEY CLUSTERED ([ArticleId] ASC, [Name] ASC, [Index] ASC),
    CONSTRAINT [FK_tblArticleBool_ArticleId] FOREIGN KEY ([ArticleId]) REFERENCES [dbo].[tblArticle] ([Id]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_tblArticleBool_Index]
    ON [dbo].[tblArticleBool]([Index] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_tblArticleBool_Name]
    ON [dbo].[tblArticleBool]([Name] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_tblArticleBool_Value]
    ON [dbo].[tblArticleBool]([Value] ASC);


