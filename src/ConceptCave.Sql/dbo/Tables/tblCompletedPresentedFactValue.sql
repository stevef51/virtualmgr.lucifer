﻿CREATE TABLE [dbo].[tblCompletedPresentedFactValue] (
    [Id]                                      UNIQUEIDENTIFIER NOT NULL,
    [CompletedWorkingDocumentPresentedFactId] UNIQUEIDENTIFIER NOT NULL,
    [Value]                                   NVARCHAR (MAX)   NULL,
    [SearchValue]                             NVARCHAR (250)   NULL,
    CONSTRAINT [PK_tblCompletedPresentedFactValue] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_tblCompletedPresentedFactValue_tblCompletedWorkingDocumentPresentedFact] FOREIGN KEY ([CompletedWorkingDocumentPresentedFactId]) REFERENCES [dbo].[tblCompletedWorkingDocumentPresentedFact] ([Id]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_tblCompletedPresentedFactValue]
    ON [dbo].[tblCompletedPresentedFactValue]([CompletedWorkingDocumentPresentedFactId] ASC);


