﻿CREATE TABLE [dbo].[tblUserBillingType]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY,
	[Name] NVARCHAR(50) NOT NULL,
	[Archived] BIT NOT NULL,
	[AmountPerMonth] MONEY NOT NULL
)
