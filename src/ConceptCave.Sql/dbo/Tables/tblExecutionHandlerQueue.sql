﻿CREATE TABLE [dbo].[tblExecutionHandlerQueue]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY, 
    [Handler] NVARCHAR(100) NOT NULL, 
    [Method] NVARCHAR(100) NOT NULL, 
    [PrimaryEntityId] NVARCHAR(50) NOT NULL, 
    [DateCreated] DATETIME NOT NULL, 
    [Processed] BIT NOT NULL DEFAULT 0, 
    [Data] NVARCHAR(MAX) NOT NULL, 
    [LatestException] NVARCHAR(MAX) NULL
)

GO

CREATE INDEX [IX_tblExecutionHandlerQueue_PrimaryEntity] ON [dbo].[tblExecutionHandlerQueue] ([Handler],[PrimaryEntityId],[Processed])

GO

CREATE INDEX [IX_tblExecutionHandlerQueue_DateCreated] ON [dbo].[tblExecutionHandlerQueue] ([DateCreated])
