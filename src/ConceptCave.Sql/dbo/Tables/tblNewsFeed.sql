﻿CREATE TABLE [dbo].[tblNewsFeed]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY, 
	[DateCreated] DATETIME NOT NULL,
	[CreatedBy] UNIQUEIDENTIFIER NOT NULL,
    [Channel] NVARCHAR(50) NOT NULL, 
	[Program] NVARCHAR(50) NOT NULL,
	[Title] NVARCHAR(50) NULL,
    [Headline] NVARCHAR(500) NOT NULL, 
    [ValidTo] DATETIME NOT NULL, 
    CONSTRAINT [FK_tblNewsFeed_User] FOREIGN KEY ([CreatedBy]) REFERENCES [tblUserData]([UserId])
)

GO

CREATE INDEX [IX_tblNewsFeed_Channel] ON [dbo].[tblNewsFeed] ([Channel])

GO

CREATE INDEX [IX_tblNewsFeed_Program] ON [dbo].[tblNewsFeed] ([Program])

GO

CREATE INDEX [IX_tblNewsFeed_ChannelProgram] ON [dbo].[tblNewsFeed] ([Channel], [Program])
