﻿CREATE TABLE [dbo].[tblCompletedPresentedGeoLocationFactValue] (
    [Id]                                      UNIQUEIDENTIFIER NOT NULL,
    [CompletedWorkingDocumentPresentedFactId] UNIQUEIDENTIFIER NOT NULL,
    [Latitude]                                DECIMAL (18, 8)  NOT NULL,
    [Longitude]                               DECIMAL (18, 8)  NOT NULL,
    [Accuracy]                                DECIMAL (18, 4)  NOT NULL,
    [Altitude]                                DECIMAL (18, 4)  NULL,
    [AltitudeAccuracy]                        DECIMAL (18, 4)  NULL,
    [Heading]                                 DECIMAL (18, 4)  NULL,
    [Speed]                                   DECIMAL (18, 4)  NULL,
    CONSTRAINT [PK_tblCompletedPresentedGeoLocationFactValue] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_tblCompletedPresentedGeoLocationFactValue_tblCompletedWorkingDocumentPresentedFact] FOREIGN KEY ([CompletedWorkingDocumentPresentedFactId]) REFERENCES [dbo].[tblCompletedWorkingDocumentPresentedFact] ([Id]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_tblCompletedPresentedGeoLocationFactValue]
    ON [dbo].[tblCompletedPresentedGeoLocationFactValue]([CompletedWorkingDocumentPresentedFactId] ASC);


