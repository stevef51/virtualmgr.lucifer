﻿CREATE TABLE [dbo].[tblMessages] (
    [Id]          UNIQUEIDENTIFIER NOT NULL,
    [Text]        NVARCHAR (MAX)   NOT NULL,
    [InReplyTo]   UNIQUEIDENTIFIER NULL,
    [SentAt]      DATETIME         NOT NULL,
    [Sender]      UNIQUEIDENTIFIER NOT NULL,
    [RequiresAck] BIT              NOT NULL,
    CONSTRAINT [PK_tblMessages] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_tblMessages_tblMessages] FOREIGN KEY ([InReplyTo]) REFERENCES [dbo].[tblMessages] ([Id]),
    CONSTRAINT [FK_tblMessages_tblUserData] FOREIGN KEY ([Sender]) REFERENCES [dbo].[tblUserData] ([UserId])
);

