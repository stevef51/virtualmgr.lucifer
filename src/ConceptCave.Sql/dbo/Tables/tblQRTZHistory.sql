﻿CREATE TABLE [dbo].[tblQRTZHistory]
(
	[Id] UNIQUEIDENTIFIER NOT NULL,
	[Job] NVARCHAR(250) NOT NULL , 
    [Group] NVARCHAR(250) NOT NULL, 
    [Trigger] NVARCHAR(250) NOT NULL, 
    [RunTime] DATETIME NOT NULL, 
    [Completed] BIT NOT NULL, 
    [Notes] NVARCHAR(MAX) NOT NULL, 
    CONSTRAINT [PK_tblQRTZHistory] PRIMARY KEY ([Id])
)

GO

CREATE INDEX [IX_tblQRTZHistory_Completed] ON [dbo].[tblQRTZHistory] ([Completed])

GO

CREATE INDEX [IX_tblQRTZHistory_RunTime] ON [dbo].[tblQRTZHistory] ([RunTime])
