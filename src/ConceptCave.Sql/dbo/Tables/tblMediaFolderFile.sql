﻿CREATE TABLE [dbo].[tblMediaFolderFile]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY,
	[MediaFolderId] UNIQUEIDENTIFIER NOT NULL,
	[MediaId] UNIQUEIDENTIFIER NOT NULL, 
    [AutoCreated] BIT NOT NULL DEFAULT 0, 
    [ShowViaChecklistId] INT NULL, 
    [Hidden] BIT NOT NULL DEFAULT 0, 
    CONSTRAINT [FK_tblMediaFolderFile_ToMediaFolder] FOREIGN KEY ([MediaFolderId]) REFERENCES [tblMediaFolder]([Id]) ON DELETE CASCADE, 
    CONSTRAINT [FK_tblMediaFolderFile_ToMedia] FOREIGN KEY ([MediaId]) REFERENCES [tblMedia]([Id]) ON DELETE CASCADE
)

GO

CREATE INDEX [IX_tblMediaFolderFile_MediaFolderId] ON [dbo].[tblMediaFolderFile] ([MediaFolderId])

GO

CREATE INDEX [IX_tblMediaFolderFile_MediaId] ON [dbo].[tblMediaFolderFile] ([MediaId])
