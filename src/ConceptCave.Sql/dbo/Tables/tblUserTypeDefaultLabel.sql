﻿CREATE TABLE [dbo].[tblUserTypeDefaultLabel]
(
	[UserTypeId] INT NOT NULL , 
    [LabelId] UNIQUEIDENTIFIER NOT NULL, 
    CONSTRAINT [FK_tblUserTypeDefaultLabel_ToUserType] FOREIGN KEY ([UserTypeId]) REFERENCES [tblUserType]([Id]) ON DELETE CASCADE, 
    CONSTRAINT [FK_tblUserTypeDefaultLabel_ToLabel] FOREIGN KEY ([LabelId]) REFERENCES [tblLabel]([Id]) ON DELETE CASCADE, 
    PRIMARY KEY ([UserTypeId], [LabelId])
)
