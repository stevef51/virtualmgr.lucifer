﻿CREATE TABLE [dbo].[Roles] (
    [ApplicationId] UNIQUEIDENTIFIER NOT NULL,
    [RoleId]        UNIQUEIDENTIFIER NOT NULL,
    [RoleName]      NVARCHAR (256)   NOT NULL,
    [Description]   NVARCHAR (256)   NULL,
    CONSTRAINT [PK__tmp_ms_x__8AFACE1A0DCF5404] PRIMARY KEY CLUSTERED ([RoleId] ASC),
    CONSTRAINT [RoleApplication] FOREIGN KEY ([ApplicationId]) REFERENCES [dbo].[Applications] ([ApplicationId])
);

