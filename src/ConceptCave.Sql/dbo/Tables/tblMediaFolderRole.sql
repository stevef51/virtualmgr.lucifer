﻿CREATE TABLE [dbo].[tblMediaFolderRole]
(
	[MediaFolderId] UNIQUEIDENTIFIER NOT NULL ,
	[RoleId] NVARCHAR(128) NOT NULL, 
    PRIMARY KEY ([MediaFolderId], [RoleId]), 
    CONSTRAINT [FK_tblMediaFolderRole_MediaFolder] FOREIGN KEY ([MediaFolderId]) REFERENCES [tblMediaFolder]([Id]), 
    CONSTRAINT [FK_tblMediaFolderRole_Role] FOREIGN KEY ([RoleId]) REFERENCES [AspNetRoles]([Id])
)
