﻿CREATE TABLE [dbo].[tblCompletedPresentedFactExtraValue]
(
	[Id]                                      UNIQUEIDENTIFIER NOT NULL,
    [CompletedWorkingDocumentPresentedFactId] UNIQUEIDENTIFIER NOT NULL,
	[Key] NVARCHAR(32) NOT NULL,
	[Value] NVARCHAR(MAX) NULL,
    CONSTRAINT [PK_tblCompletedPresentedFactExtraValue] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_tblCompletedPresentedFactExtraValue_tblCompletedWorkingDocumentPresentedFact] FOREIGN KEY ([CompletedWorkingDocumentPresentedFactId]) REFERENCES [dbo].[tblCompletedWorkingDocumentPresentedFact] ([Id]) ON DELETE CASCADE
)
