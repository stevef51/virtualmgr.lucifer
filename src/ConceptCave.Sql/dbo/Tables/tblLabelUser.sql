﻿CREATE TABLE [dbo].[tblLabelUser] (
    [LabelId] UNIQUEIDENTIFIER NOT NULL,
    [UserId]  UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_tblLabelUser] PRIMARY KEY CLUSTERED ([LabelId] ASC, [UserId] ASC),
    CONSTRAINT [FK_tblLabelMembership_tblLabel] FOREIGN KEY ([LabelId]) REFERENCES [dbo].[tblLabel] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_tblLabelUser_tblUserData] FOREIGN KEY ([UserId]) REFERENCES [dbo].[tblUserData] ([UserId]) ON DELETE CASCADE
);


