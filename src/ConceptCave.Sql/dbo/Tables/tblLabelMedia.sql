﻿CREATE TABLE [dbo].[tblLabelMedia]
(
	[LabelId] UNIQUEIDENTIFIER NOT NULL,
	[MediaId] UNIQUEIDENTIFIER NOT NULL, 
    CONSTRAINT [PK_tblLabelMedia] PRIMARY KEY ([LabelId], [MediaId]), 
    CONSTRAINT [FK_tblLabelMedia_Label] FOREIGN KEY ([LabelId]) REFERENCES [tblLabel]([Id]) ON DELETE CASCADE, 
    CONSTRAINT [FK_tblLabelMedia_Media] FOREIGN KEY ([MediaId]) REFERENCES [tblMedia]([Id]) ON DELETE CASCADE
)
