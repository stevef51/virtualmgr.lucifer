﻿CREATE TABLE [dbo].[tblPublishingGroupActorType] (
    [Id]   INT           NOT NULL,
    [Name] NVARCHAR (25) NOT NULL,
    CONSTRAINT [PK_tblPublishingGroupActorType] PRIMARY KEY CLUSTERED ([Id] ASC)
);

