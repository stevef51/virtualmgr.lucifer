﻿CREATE TABLE [dbo].[tblMimeType] (
    [Id]                    INT            IDENTITY (1, 1) NOT NULL,
    [Extension]             NVARCHAR (10)  NOT NULL,
    [MimeType]              NVARCHAR (255) NOT NULL,
    [MediaPreviewGenerator] NVARCHAR (500) NULL,
    CONSTRAINT [PK_tblMimeType] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [IX_tblMimeType] UNIQUE NONCLUSTERED ([Extension] ASC)
);


