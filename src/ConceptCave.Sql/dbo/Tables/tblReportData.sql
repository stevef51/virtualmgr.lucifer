﻿CREATE TABLE [dbo].[tblReportData]
(
	[ReportId] INT NOT NULL PRIMARY KEY,
	[Data] VARBINARY(MAX) NOT NULL, 
    CONSTRAINT [FK_tblReportData_ToReport] FOREIGN KEY ([ReportId]) REFERENCES [tblReport]([Id]) ON DELETE CASCADE
)
