﻿CREATE TABLE [dbo].[tblUserTypeDashboard] (
    [Id]                        INT IDENTITY (1, 1) NOT NULL,
    [PublishingGroupResourceId] INT NOT NULL,
    [UserTypeId]                INT NOT NULL,
    [PlayOffline]               BIT CONSTRAINT [DF__tblUserTy__PlayO__0E04126B] DEFAULT ((0)) NOT NULL,
    [Title] NVARCHAR(50) NULL, 
    [Tooltip] NVARCHAR(4000) NULL, 
    [SortOrder] INT NOT NULL DEFAULT 0, 
    [Config] NVARCHAR(4000) NULL, 
    CONSTRAINT [PK__tblUserT__3214EC077DA8D4BC] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK__tblUserTy__Publi__2022C2A6] FOREIGN KEY ([PublishingGroupResourceId]) REFERENCES [dbo].[tblPublishingGroupResource] ([Id]),
    CONSTRAINT [FK__tblUserTy__UserT__2116E6DF] FOREIGN KEY ([UserTypeId]) REFERENCES [dbo].[tblUserType] ([Id])
);


