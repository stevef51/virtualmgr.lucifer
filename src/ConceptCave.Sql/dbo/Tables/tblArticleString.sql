﻿CREATE TABLE [dbo].[tblArticleString] (
    [ArticleId] UNIQUEIDENTIFIER NOT NULL,
    [Name]      NVARCHAR (50)    NOT NULL,
    [Index]     INT              NOT NULL,
    [Value]     NVARCHAR (450)   NULL,
    CONSTRAINT [PK_tblArticleString] PRIMARY KEY CLUSTERED ([ArticleId] ASC, [Name] ASC, [Index] ASC),
    CONSTRAINT [FK_tblArticleString_ArticleId] FOREIGN KEY ([ArticleId]) REFERENCES [dbo].[tblArticle] ([Id]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_tblArticleString_Index]
    ON [dbo].[tblArticleString]([Index] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_tblArticleString_Name]
    ON [dbo].[tblArticleString]([Name] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_tblArticleString_Value]
    ON [dbo].[tblArticleString]([Value] ASC);


