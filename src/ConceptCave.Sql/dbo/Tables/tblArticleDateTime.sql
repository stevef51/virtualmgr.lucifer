﻿CREATE TABLE [dbo].[tblArticleDateTime] (
    [ArticleId] UNIQUEIDENTIFIER NOT NULL,
    [Name]      NVARCHAR (50)    NOT NULL,
    [Index]     INT              NOT NULL,
    [Value]     DATETIME         NOT NULL,
    CONSTRAINT [PK_tblArticleDateTime] PRIMARY KEY CLUSTERED ([ArticleId] ASC, [Name] ASC, [Index] ASC),
    CONSTRAINT [FK_tblArticleDateTime_ArticleId] FOREIGN KEY ([ArticleId]) REFERENCES [dbo].[tblArticle] ([Id]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_tblArticleDateTime_Index]
    ON [dbo].[tblArticleDateTime]([Index] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_tblArticleDateTime_Name]
    ON [dbo].[tblArticleDateTime]([Name] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_tblArticleDateTime_Value]
    ON [dbo].[tblArticleDateTime]([Value] ASC);


