﻿CREATE TABLE [dbo].[tblCompletedWorkingDocumentDimension] (
    [Id]   UNIQUEIDENTIFIER NOT NULL,
    [Name] NVARCHAR (50)    NOT NULL,
    CONSTRAINT [PK_tblReportingWorkingDocument] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_tblReportingWorkingDocument]
    ON [dbo].[tblCompletedWorkingDocumentDimension]([Name] ASC);


