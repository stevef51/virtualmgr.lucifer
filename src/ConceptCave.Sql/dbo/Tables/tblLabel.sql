﻿CREATE TABLE [dbo].[tblLabel] (
    [Id]           UNIQUEIDENTIFIER NOT NULL,
    [Name]         NVARCHAR (50)    NOT NULL,
    [Forecolor]    INT              NOT NULL,
    [Backcolor]    INT              NOT NULL,
    [ForUsers]     BIT              CONSTRAINT [DF_tblLabel_ForUsers] DEFAULT ((0)) NOT NULL,
    [ForMedia]     BIT              CONSTRAINT [DF_tblLabel_ForMedia] DEFAULT ((0)) NOT NULL,
    [ForResources] BIT              CONSTRAINT [DF_tblLabel_ForResources] DEFAULT ((0)) NOT NULL,
    [ForQuestions] BIT              CONSTRAINT [ColumnDefault_44799dd7-b275-48d2-b599-3efd3fdc4625] DEFAULT ((0)) NOT NULL,
    [ForTasks] BIT NOT NULL DEFAULT 0, 
    CONSTRAINT [PK_tblLabel] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_tblLabel]
    ON [dbo].[tblLabel]([Name] ASC);


