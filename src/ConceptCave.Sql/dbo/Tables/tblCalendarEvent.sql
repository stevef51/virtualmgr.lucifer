﻿CREATE TABLE [dbo].[tblCalendarEvent] (
    [Id]          INT              IDENTITY (1, 1) NOT NULL,
	[CalendarRuleId] INT NOT NULL,
    [Title]       NVARCHAR (4000)   NOT NULL,
    [Description] NVARCHAR (MAX)  NULL,
    CONSTRAINT [PK_tblCalendarEvent] PRIMARY KEY CLUSTERED ([Id] ASC), 
    CONSTRAINT [FK_tblCalendarEvent_TotblCalendarRule] FOREIGN KEY ([CalendarRuleId]) REFERENCES [tblCalendarRule]([Id]), 
);

