﻿CREATE TABLE [dbo].[tblCompletedPresentedUploadMediaFactValue] (
    [CompletedWorkingDocumentPresentedFactId] UNIQUEIDENTIFIER NOT NULL,
    [MediaId]                                 UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_tblCompletedPresentedUploadMediaFactValue] PRIMARY KEY CLUSTERED ([CompletedWorkingDocumentPresentedFactId] ASC, [MediaId] ASC),
    CONSTRAINT [FK_tblCompletedPresentedUploadMediaFactValue_tblCompletedWorkingDocumentPresentedFact] FOREIGN KEY ([CompletedWorkingDocumentPresentedFactId]) REFERENCES [dbo].[tblCompletedWorkingDocumentPresentedFact] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_tblCompletedPresentedUploadMediaFactValue_tblMedia] FOREIGN KEY ([MediaId]) REFERENCES [dbo].[tblMedia] ([Id]) ON DELETE CASCADE
);


