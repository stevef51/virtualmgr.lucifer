﻿CREATE TABLE [dbo].[tblMediaFolder]
(
	[Id] UniqueIdentifier NOT NULL PRIMARY KEY,
	[ParentId] UniqueIdentifier NULL,
	[Text] nvarchar(50) NOT NULL, 
    [UseVersioning] BIT NOT NULL DEFAULT 0, 
    [SortOrder] INT NOT NULL DEFAULT 0, 
    [AutoCreated] BIT NOT NULL DEFAULT 0, 
    [ShowViaChecklistId] INT NULL, 
    [Hidden] BIT NOT NULL DEFAULT 0, 
    CONSTRAINT [FK_tblMediaFolder_ToParent] FOREIGN KEY ([ParentId]) REFERENCES [tblMediaFolder]([Id])
)

GO

CREATE INDEX [IX_tblMediaFolder_ParentId] ON [dbo].[tblMediaFolder] ([ParentId])
