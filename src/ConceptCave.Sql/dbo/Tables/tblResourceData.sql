﻿CREATE TABLE [dbo].[tblResourceData] (
    [ResourceId] INT  NOT NULL,
    [Data]       NTEXT NOT NULL,
    CONSTRAINT [PK_tblResourceData] PRIMARY KEY CLUSTERED ([ResourceId] ASC),
    CONSTRAINT [FK_tblResourceData_tblResource] FOREIGN KEY ([ResourceId]) REFERENCES [dbo].[tblResource] ([Id]) ON DELETE CASCADE
);

