﻿CREATE TABLE [dbo].[tblWorkingDocumentPublicVariable] (
    [Id]                UNIQUEIDENTIFIER NOT NULL,
    [WorkingDocumentId] UNIQUEIDENTIFIER NOT NULL,
    [Name]              NVARCHAR (255)   NOT NULL,
    [Value]             NVARCHAR (255)   NULL,
    [PassFail]          BIT              NULL,
    [Score]             DECIMAL (18, 2)  NULL,
    CONSTRAINT [PK_tblWorkingDocumentPublicVariable] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_tblWorkingDocumentPublicVariable_tblWorkingDocument] FOREIGN KEY ([WorkingDocumentId]) REFERENCES [dbo].[tblWorkingDocument] ([Id]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_tblWorkingDocumentPublicVariable]
    ON [dbo].[tblWorkingDocumentPublicVariable]([WorkingDocumentId] ASC);

