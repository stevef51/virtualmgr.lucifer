﻿CREATE TABLE [dbo].[tblSequence] (
    [Name]  NVARCHAR (50) NOT NULL,
    [Value] INT           NOT NULL,
    CONSTRAINT [PK_tblSequence] PRIMARY KEY CLUSTERED ([Name] ASC)
);

