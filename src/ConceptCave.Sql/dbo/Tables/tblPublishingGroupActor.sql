﻿CREATE TABLE [dbo].[tblPublishingGroupActor] (
    [PublishingGroupId] INT              NOT NULL,
    [UserId]            UNIQUEIDENTIFIER NOT NULL,
    [PublishingTypeId]  INT              NOT NULL,
    CONSTRAINT [PK_tblPublishingGroupActor_1] PRIMARY KEY CLUSTERED ([PublishingGroupId] ASC, [UserId] ASC, [PublishingTypeId] ASC),
    CONSTRAINT [FK_tblPublishingGroupActor_tblPublishingGroup] FOREIGN KEY ([PublishingGroupId]) REFERENCES [dbo].[tblPublishingGroup] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_tblPublishingGroupActor_tblPublishingGroupActorType] FOREIGN KEY ([PublishingTypeId]) REFERENCES [dbo].[tblPublishingGroupActorType] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_tblPublishingGroupActor_tblUserData] FOREIGN KEY ([UserId]) REFERENCES [dbo].[tblUserData] ([UserId]) ON DELETE CASCADE
);


