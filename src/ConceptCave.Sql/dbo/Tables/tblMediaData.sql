﻿CREATE TABLE [dbo].[tblMediaData] (
    [MediaId] UNIQUEIDENTIFIER NOT NULL,
    [Data]    VARBINARY (MAX)  NOT NULL,
    CONSTRAINT [PK_tblMediaData] PRIMARY KEY CLUSTERED ([MediaId] ASC),
    CONSTRAINT [FK_tblMediaData_tblMedia] FOREIGN KEY ([MediaId]) REFERENCES [dbo].[tblMedia] ([Id]) ON DELETE CASCADE
);


