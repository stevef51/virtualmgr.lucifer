﻿CREATE TABLE [dbo].[tblPublicTicket] (
    [Id]     UNIQUEIDENTIFIER NOT NULL,
    [Status] INT              CONSTRAINT [DF_tblPublicTicket_Status] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_tblPublicTicket] PRIMARY KEY CLUSTERED ([Id] ASC)
);

