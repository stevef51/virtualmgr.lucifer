﻿CREATE TABLE [dbo].[tblMediaPolicy]
(
	[MediaId] UNIQUEIDENTIFIER NOT NULL , 
    [Data] NVARCHAR(MAX) NOT NULL, 
    PRIMARY KEY ([MediaId]), 
    CONSTRAINT [FK_tblMediaPolicy_Media] FOREIGN KEY ([MediaId]) REFERENCES [tblMedia]([Id]) ON DELETE CASCADE, 
)
