﻿CREATE TABLE [dbo].[tblMediaFolder_Translated]
(
	[Id] UNIQUEIDENTIFIER NOT NULL , 
    [CultureName] NVARCHAR(10) NOT NULL, 
    [Text] NVARCHAR(50) NOT NULL, 
    PRIMARY KEY ([Id], [CultureName]), 
    CONSTRAINT [FK_MediaFolder_Translated_Id_MediaFolder_Id] FOREIGN KEY ([Id]) REFERENCES [tblMediaFolder] ([Id]) ON DELETE CASCADE
)
