﻿CREATE TABLE [dbo].[tblPlugin] (
    [Id]          UNIQUEIDENTIFIER NOT NULL,
    [Name]        NVARCHAR (255)   NOT NULL,
    [Description] NVARCHAR (4000)  NULL,
    [DateCreated] DATETIME         CONSTRAINT [DF_tblPlugin_DateCreated] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_tblPlugin] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tblPlugin]
    ON [dbo].[tblPlugin]([Name] ASC);

