﻿CREATE TABLE [dbo].[tblArticleBigInt] (
    [ArticleId] UNIQUEIDENTIFIER NOT NULL,
    [Name]      NVARCHAR (50)    NOT NULL,
    [Index]     INT              NOT NULL,
    [Value]     BIGINT           NOT NULL,
    CONSTRAINT [PK_tblArticleBigInt] PRIMARY KEY CLUSTERED ([ArticleId] ASC, [Name] ASC, [Index] ASC),
    CONSTRAINT [FK_tblArticleBigInt_ArticleId] FOREIGN KEY ([ArticleId]) REFERENCES [dbo].[tblArticle] ([Id]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_tblArticleBigInt_Index]
    ON [dbo].[tblArticleBigInt]([Index] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_tblArticleBigInt_Name]
    ON [dbo].[tblArticleBigInt]([Name] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_tblArticleBigInt_Value]
    ON [dbo].[tblArticleBigInt]([Value] ASC);


