﻿CREATE TABLE [dbo].[tblMediaFolderPolicy]
(
	[MediaFolderId] UNIQUEIDENTIFIER NOT NULL , 
    [Data] NVARCHAR(MAX) NOT NULL, 
    PRIMARY KEY ([MediaFolderId]), 
    CONSTRAINT [FK_tblMediaFolderPolicy_MediaFolder] FOREIGN KEY ([MediaFolderId]) REFERENCES [tblMediaFolder]([id]), 
)
