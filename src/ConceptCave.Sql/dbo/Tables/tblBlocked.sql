﻿CREATE TABLE [dbo].[tblBlocked] (
    [BlockedById] UNIQUEIDENTIFIER NOT NULL,
    [BlockedId]   UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_tblBlocked] PRIMARY KEY CLUSTERED ([BlockedById] ASC, [BlockedId] ASC)
);


