﻿CREATE TABLE [dbo].[tblLabelResource] (
    [LabelId]    UNIQUEIDENTIFIER NOT NULL,
    [ResourceId] INT              NOT NULL,
    CONSTRAINT [PK_tblLabelResource] PRIMARY KEY CLUSTERED ([LabelId] ASC, [ResourceId] ASC),
    CONSTRAINT [FK_tblLabelResource_tblLabel] FOREIGN KEY ([LabelId]) REFERENCES [dbo].[tblLabel] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_tblLabelResource_tblResource] FOREIGN KEY ([ResourceId]) REFERENCES [dbo].[tblResource] ([Id]) ON DELETE CASCADE
);


