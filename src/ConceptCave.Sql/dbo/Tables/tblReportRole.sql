﻿CREATE TABLE [dbo].[tblReportRole]
(
	[ReportId] INT NOT NULL ,
	[RoleId] NVARCHAR(128) NOT NULL, 
    CONSTRAINT [FK_tblReportRole_ToReport] FOREIGN KEY ([ReportId]) REFERENCES [tblReport]([Id]) ON DELETE CASCADE, 
    CONSTRAINT [FK_tblReportRole_ToRole] FOREIGN KEY ([RoleId]) REFERENCES [AspNetRoles]([Id]) ON DELETE CASCADE, 
    PRIMARY KEY ([ReportId], [RoleId])
)
