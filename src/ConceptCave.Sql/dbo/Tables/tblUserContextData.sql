﻿CREATE TABLE [dbo].[tblUserContextData] (
    [UserId]                             UNIQUEIDENTIFIER NOT NULL,
    [UserTypeContextPublishedResourceId] INT              NOT NULL,
    [CompletedWorkingDocumentId]         UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_tblUserContextData] PRIMARY KEY CLUSTERED ([UserId] ASC, [UserTypeContextPublishedResourceId] ASC, [CompletedWorkingDocumentId] ASC),
    CONSTRAINT [FK_tblUserContextData_tblCompletedWorkingDocumentFact] FOREIGN KEY ([CompletedWorkingDocumentId]) REFERENCES [dbo].[tblCompletedWorkingDocumentFact] ([WorkingDocumentId]),
    CONSTRAINT [FK_tblUserContextData_tblUserData] FOREIGN KEY ([UserId]) REFERENCES [dbo].[tblUserData] ([UserId]) ON DELETE CASCADE,
    CONSTRAINT [FK_tblUserContextData_tblUserTypeContextPublishedResource] FOREIGN KEY ([UserTypeContextPublishedResourceId]) REFERENCES [dbo].[tblUserTypeContextPublishedResource] ([Id]) ON DELETE CASCADE
);


