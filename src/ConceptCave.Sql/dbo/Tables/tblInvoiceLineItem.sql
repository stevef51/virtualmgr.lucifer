﻿CREATE TABLE [dbo].[tblInvoiceLineItem]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [InvoiceId] NVARCHAR(32) NOT NULL,
	[Line] NVARCHAR(50) NOT NULL,
	[DetailLevel] INT NOT NULL,
	[ParentLine] NVARCHAR(50) NULL,
	[Description] NVARCHAR(1024) NOT NULL,
    [UserId] UNIQUEIDENTIFIER NULL, 
    [UnitPrice] MONEY NOT NULL, 
	[Quantity] DECIMAL(19,10) NOT NULL,
	[Total] MONEY NOT NULL,
    CONSTRAINT [FK_tblInvoiceLineItem_tblInvoice_id] FOREIGN KEY ([InvoiceId]) REFERENCES [tblInvoice]([Id]) ON DELETE CASCADE, 
)

GO

CREATE INDEX [IX_tblInvoiceLineItem_InvoiceId] ON [dbo].[tblInvoiceLineItem] ([InvoiceId])
GO

CREATE INDEX [IX_tblInvoiceLineItem_UserId] ON [dbo].[tblInvoiceLineItem] ([UserId])

