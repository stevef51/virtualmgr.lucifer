﻿CREATE TABLE [dbo].[tblCompletedWorkingDocumentPresentedFact] (
    [Id]                         UNIQUEIDENTIFIER NOT NULL,
    [WorkingDocumentId]          UNIQUEIDENTIFIER NOT NULL,
    [PresentedId]                UNIQUEIDENTIFIER NOT NULL,
    [InternalId]                 UNIQUEIDENTIFIER NOT NULL,
    [ReportingWorkingDocumentId] UNIQUEIDENTIFIER NOT NULL,
    [ReportingUserReviewerId]    UNIQUEIDENTIFIER NOT NULL,
    [ReportingUserRevieweeId]    UNIQUEIDENTIFIER NOT NULL,
    [ReportingPresentedTypeId]   UNIQUEIDENTIFIER NOT NULL,
    [Prompt]                     NVARCHAR (4000)  NOT NULL DEFAULT '',
    [DateStarted]                DATETIME         NOT NULL,
    [DateCompleted]              DATETIME         NOT NULL,
    [PossibleScore]              DECIMAL (18, 2)  NOT NULL,
    [Score]                      DECIMAL (18, 2)  NOT NULL,
    [PassFail]                   BIT              NULL,
    [ParentWorkingDocumentId]    UNIQUEIDENTIFIER NULL,
    [NotesText]                  NVARCHAR (4000)  NULL,
    [IsUserContext]              BIT              CONSTRAINT [DF_tblCompletedWorkingDocumentPresentedFact_IsUserContext] DEFAULT ((0)) NOT NULL,
    [PresentedOrder]             INT              CONSTRAINT [DF_tblCompletedWorkingDocumentPresentedFact_PresentedOrder] DEFAULT ((0)) NOT NULL,
    [ContextType] INT NOT NULL DEFAULT 0, 
    CONSTRAINT [PK_tblCompletedWorkingDocumentQuestionFact] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_tblCompletedWorkingDocumentPresentedFact_tblCompletedPresentedTypeDimension] FOREIGN KEY ([ReportingPresentedTypeId]) REFERENCES [dbo].[tblCompletedPresentedTypeDimension] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_tblCompletedWorkingDocumentPresentedFact_tblCompletedUserDimension] FOREIGN KEY ([ReportingUserReviewerId]) REFERENCES [dbo].[tblCompletedUserDimension] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_tblCompletedWorkingDocumentPresentedFact_tblCompletedUserDimension1] FOREIGN KEY ([ReportingUserRevieweeId]) REFERENCES [dbo].[tblCompletedUserDimension] ([Id]),
    CONSTRAINT [FK_tblCompletedWorkingDocumentPresentedFact_tblCompletedWorkingDocumentDimension] FOREIGN KEY ([ReportingWorkingDocumentId]) REFERENCES [dbo].[tblCompletedWorkingDocumentDimension] ([Id]),
    CONSTRAINT [FK_tblCompletedWorkingDocumentPresentedFact_tblCompletedWorkingDocumentFact] FOREIGN KEY ([WorkingDocumentId]) REFERENCES [dbo].[tblCompletedWorkingDocumentFact] ([WorkingDocumentId])
);


GO
CREATE NONCLUSTERED INDEX [IX_tblCompletedWorkingDocumentPresentedFact]
    ON [dbo].[tblCompletedWorkingDocumentPresentedFact]([WorkingDocumentId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_tblCompletedWorkingDocumentPresentedFact_1]
    ON [dbo].[tblCompletedWorkingDocumentPresentedFact]([ReportingWorkingDocumentId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_tblCompletedWorkingDocumentPresentedFact_2]
    ON [dbo].[tblCompletedWorkingDocumentPresentedFact]([ReportingUserRevieweeId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_tblCompletedWorkingDocumentPresentedFact_4]
    ON [dbo].[tblCompletedWorkingDocumentPresentedFact]([ReportingPresentedTypeId] ASC);

GO
CREATE NONCLUSTERED INDEX [nci_wi_tblCompletedWorkingDocumentPresentedFact_B8B44F5DD6C67AC0E4FCD450407B3A58] 
	ON [dbo].[tblCompletedWorkingDocumentPresentedFact] ([PassFail], [WorkingDocumentId]) 
	INCLUDE ([DateCompleted], [DateStarted], [PossibleScore], [PresentedId], [PresentedOrder], [Prompt], [ReportingPresentedTypeId], [Score]) 
	WITH (ONLINE = ON)