﻿CREATE TABLE [dbo].[tblUserMedia] (
    [UserId]      UNIQUEIDENTIFIER NOT NULL,
    [MediaId]     UNIQUEIDENTIFIER NOT NULL,
    [DateCreated] DATETIME         CONSTRAINT [DF_tblUserMedia_DateCreated] DEFAULT (getutcdate()) NOT NULL,
    CONSTRAINT [PK_tblUserMedia] PRIMARY KEY CLUSTERED ([UserId] ASC, [MediaId] ASC),
    CONSTRAINT [FK_tblUserMedia_tblMedia] FOREIGN KEY ([MediaId]) REFERENCES [dbo].[tblMedia] ([Id]),
    CONSTRAINT [FK_tblUserMedia_tblUserData] FOREIGN KEY ([UserId]) REFERENCES [dbo].[tblUserData] ([UserId]) ON DELETE CASCADE
);


