﻿CREATE TABLE [dbo].[tblCompletedLabelWorkingDocumentDimension] (
    [CompletedLabelDimensionId]      UNIQUEIDENTIFIER NOT NULL,
    [CompletedWorkingDocumentFactId] UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_tblCompletedLabelWorkingDocumentDimension] PRIMARY KEY CLUSTERED ([CompletedLabelDimensionId] ASC, [CompletedWorkingDocumentFactId] ASC),
    CONSTRAINT [FK_tblCompletedLabelWorkingDocumentDimension_tblCompletedLabelDimension] FOREIGN KEY ([CompletedLabelDimensionId]) REFERENCES [dbo].[tblCompletedLabelDimension] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_tblCompletedLabelWorkingDocumentDimension_tblCompletedWorkingDocumentFact] FOREIGN KEY ([CompletedWorkingDocumentFactId]) REFERENCES [dbo].[tblCompletedWorkingDocumentFact] ([WorkingDocumentId]) ON DELETE CASCADE
);


