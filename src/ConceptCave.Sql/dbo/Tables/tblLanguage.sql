﻿CREATE TABLE [dbo].[tblLanguage]
(
    [CultureName] NVARCHAR(10) NOT NULL, 
    [LanguageName] NVARCHAR(50) NOT NULL, 
    [NativeName] NVARCHAR(50) NOT NULL, 
    CONSTRAINT [PK_tblLanguage] PRIMARY KEY ([CultureName] ASC)
)
