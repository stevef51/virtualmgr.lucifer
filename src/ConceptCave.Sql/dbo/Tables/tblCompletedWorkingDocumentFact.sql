﻿CREATE TABLE [dbo].[tblCompletedWorkingDocumentFact] (
    [WorkingDocumentId]          UNIQUEIDENTIFIER NOT NULL,
    [ReportingWorkingDocumentId] UNIQUEIDENTIFIER NOT NULL,
    [ReportingUserReviewerId]    UNIQUEIDENTIFIER NOT NULL,
    [ReportingUserRevieweeId]    UNIQUEIDENTIFIER NOT NULL,
    [DateStarted]                DATETIME         NOT NULL,
    [DateCompleted]              DATETIME         NOT NULL,
    [TotalQuestionCount]         INT              NOT NULL,
    [TotalPossibleScore]         DECIMAL (18, 2)  NOT NULL,
    [TotalScore]                 DECIMAL (18, 2)  NOT NULL,
    [PassedQuestionCount]        INT              NOT NULL,
    [FailedQuestionCount]        INT              NOT NULL,
    [NAQuestionCount]            INT              NOT NULL,
    [ClientIP]                   NVARCHAR (50)    NULL,
    [UserAgentId]                INT              NULL,
    [ParentWorkingDocumentId]    UNIQUEIDENTIFIER NULL,
    [IsUserContext]              BIT              CONSTRAINT [DF_tblCompletedWorkingDocumentFact_IsUserContext] DEFAULT ((0)) NOT NULL,
    [ContextType] INT NOT NULL DEFAULT 0, 
    CONSTRAINT [PK_tblCompletedWorkingDocumentFact] PRIMARY KEY CLUSTERED ([WorkingDocumentId] ASC),
    CONSTRAINT [FK_tblCompletedWorkingDocumentFact_tblCompletedUserDimension] FOREIGN KEY ([ReportingUserReviewerId]) REFERENCES [dbo].[tblCompletedUserDimension] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_tblCompletedWorkingDocumentFact_tblCompletedUserDimension1] FOREIGN KEY ([ReportingUserRevieweeId]) REFERENCES [dbo].[tblCompletedUserDimension] ([Id]),
    CONSTRAINT [FK_tblCompletedWorkingDocumentFact_tblCompletedWorkingDocumentDimension] FOREIGN KEY ([ReportingWorkingDocumentId]) REFERENCES [dbo].[tblCompletedWorkingDocumentDimension] ([Id]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_tblCompletedWorkingDocumentFact]
    ON [dbo].[tblCompletedWorkingDocumentFact]([ReportingUserRevieweeId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_tblCompletedWorkingDocumentFact_1]
    ON [dbo].[tblCompletedWorkingDocumentFact]([ReportingUserReviewerId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_tblCompletedWorkingDocumentFact_2]
    ON [dbo].[tblCompletedWorkingDocumentFact]([ReportingWorkingDocumentId] ASC);


