﻿CREATE TABLE [dbo].[tblGlobalSetting] (
    [Id]    INT        IDENTITY (1, 1) NOT NULL,
    [Name]  NVARCHAR(256) NOT NULL,
    [Value] NTEXT      NULL,
    [Hidden] BIT NOT NULL DEFAULT 0, 
    CONSTRAINT [PK_tblGlobalSetting] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [IX_tblGlobalSetting] UNIQUE NONCLUSTERED ([Name] ASC)
);