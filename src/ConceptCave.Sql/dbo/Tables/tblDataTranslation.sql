﻿CREATE TABLE [dbo].[tblDataTranslation]
(
	[Table] NVARCHAR(50) NOT NULL , 
    [Column] NVARCHAR(50) NOT NULL, 
    [ColumnKey] NVARCHAR(100) NOT NULL, 
    [CultureName] NVARCHAR(10) NOT NULL, 
    [NativeText] NVARCHAR(1000) NOT NULL, 
    PRIMARY KEY ([Table], [Column], [ColumnKey], [CultureName])
)
