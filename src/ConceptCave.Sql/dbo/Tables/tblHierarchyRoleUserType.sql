﻿CREATE TABLE [dbo].[tblHierarchyRoleUserType] (
    [Id]         INT IDENTITY (1, 1) NOT NULL,
    [RoleId]     INT NOT NULL,
    [UserTypeId] INT NOT NULL,
    CONSTRAINT [PK_tblHierarchyRoleUserType] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_tblHierarchyRoleUserType_tblHierarchyRole] FOREIGN KEY ([RoleId]) REFERENCES [dbo].[tblHierarchyRole] ([Id]),
    CONSTRAINT [FK_tblHierarchyRoleUserType_tblUserType] FOREIGN KEY ([UserTypeId]) REFERENCES [dbo].[tblUserType] ([Id])
);


