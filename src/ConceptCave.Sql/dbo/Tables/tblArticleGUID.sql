﻿CREATE TABLE [dbo].[tblArticleGUID] (
    [ArticleId] UNIQUEIDENTIFIER NOT NULL,
    [Name]      NVARCHAR (50)    NOT NULL,
    [Index]     INT              NOT NULL,
    [Value]     UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_tblArticleGUID] PRIMARY KEY CLUSTERED ([ArticleId] ASC, [Name] ASC, [Index] ASC),
    CONSTRAINT [FK_tblArticleGUID_ArticleId] FOREIGN KEY ([ArticleId]) REFERENCES [dbo].[tblArticle] ([Id]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_tblArticleGuid_Index]
    ON [dbo].[tblArticleGUID]([Index] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_tblArticleGuid_Name]
    ON [dbo].[tblArticleGUID]([Name] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_tblArticleGuid_Value]
    ON [dbo].[tblArticleGUID]([Value] ASC);

