﻿CREATE TABLE [dbo].[tblResource] (
    [Id]     INT              IDENTITY (1, 1) NOT NULL,
    [NodeId] UNIQUEIDENTIFIER NOT NULL,
    [Name]   NVARCHAR (50)    NOT NULL,
    [Type]   NVARCHAR (200)   NOT NULL,
    CONSTRAINT [PK_tblResource] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tblResource]
    ON [dbo].[tblResource]([NodeId] ASC);


