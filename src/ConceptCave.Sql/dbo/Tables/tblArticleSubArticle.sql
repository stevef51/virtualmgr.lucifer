﻿CREATE TABLE [dbo].[tblArticleSubArticle] (    [ArticleId]    UNIQUEIDENTIFIER NOT NULL,
    [Name]         NVARCHAR (50)    NOT NULL,
    [Index]        INT              NOT NULL,
    [SubArticleId] UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_tblArticleSubArticle] PRIMARY KEY CLUSTERED ([ArticleId] ASC, [Name] ASC, [Index] ASC),
    CONSTRAINT [FK_tblArticleSubArticle_ArticleId] FOREIGN KEY ([ArticleId]) REFERENCES [dbo].[tblArticle] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_tblArticleSubArticle_SubArticleId] FOREIGN KEY ([SubArticleId]) REFERENCES [dbo].[tblArticle] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_tblArticleSubArticle_Index]
    ON [dbo].[tblArticleSubArticle]([Index] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_tblArticleSubArticle_Name]
    ON [dbo].[tblArticleSubArticle]([Name] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_tblArticleSubArticle_SubArticleId]
    ON [dbo].[tblArticleSubArticle]([SubArticleId] ASC);


