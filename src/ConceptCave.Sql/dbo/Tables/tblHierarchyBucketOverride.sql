﻿CREATE TABLE [dbo].[tblHierarchyBucketOverride]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [HierarchyBucketId] INT NOT NULL, 
    [RoleId] INT NOT NULL, 
    [OriginalUserId] UNIQUEIDENTIFIER NULL, 
    [UserId] UNIQUEIDENTIFIER NULL, 
    [StartDate] DATETIME NOT NULL, 
    [EndDate] DATETIME NOT NULL, 
    [DateCreated] DATETIME NOT NULL DEFAULT getutcdate(), 
	HierarchyBucketOverrideTypeId INT NULL,
    [Notes] NVARCHAR(MAX) NULL
    CONSTRAINT [FK_tblHierarchyBucketOverride_HierarchyBucket] FOREIGN KEY ([HierarchyBucketId]) REFERENCES [tblHierarchyBucket]([Id]), 
    CONSTRAINT [FK_tblHierarchyBucketOverride_HierarchyRole] FOREIGN KEY ([RoleId]) REFERENCES [tblHierarchyRole]([Id]), 
    CONSTRAINT [FK_tblHierarchyBucketOverride_OriginalUser] FOREIGN KEY ([OriginalUserId]) REFERENCES [tblUserData]([UserId]), 
    CONSTRAINT [FK_tblHierarchyBucketOverride_User] FOREIGN KEY ([UserId]) REFERENCES [tblUserData]([UserId]), 
    CONSTRAINT [FK_tblHierarchyBucketOverride_HierarchyOverrideType] FOREIGN KEY ([HierarchyBucketOverrideTypeId]) REFERENCES [tblHierarchyBucketOverrideType]([Id])
)

GO

CREATE INDEX [IX_tblHierarchyBucketOverride_HierarchyBucketId] ON [dbo].[tblHierarchyBucketOverride] ([HierarchyBucketId])

GO

CREATE INDEX [IX_tblHierarchyBucketOverride_OriginalUserId] ON [dbo].[tblHierarchyBucketOverride] ([OriginalUserId])
