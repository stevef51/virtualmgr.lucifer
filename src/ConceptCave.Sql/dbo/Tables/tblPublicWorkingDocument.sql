﻿CREATE TABLE [dbo].[tblPublicWorkingDocument] (
    [WorkingDocumentId] UNIQUEIDENTIFIER NOT NULL,
    [PublicTicketId]    UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_tblPublicWorkingDocument] PRIMARY KEY CLUSTERED ([WorkingDocumentId] ASC, [PublicTicketId] ASC),
    CONSTRAINT [FK_tblPublicWorkingDocument_tblPublicTicket] FOREIGN KEY ([PublicTicketId]) REFERENCES [dbo].[tblPublicTicket] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_tblPublicWorkingDocument_tblWorkingDocument] FOREIGN KEY ([WorkingDocumentId]) REFERENCES [dbo].[tblWorkingDocument] ([Id]) ON DELETE CASCADE
);

