﻿CREATE TABLE [dbo].[tblMediaPageViewing]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY, 
    [MediaViewingId] UNIQUEIDENTIFIER NOT NULL, 
    [Page] INT NOT NULL, 
    [StartDate] DATETIME NOT NULL, 
    [EndDate] DATETIME NOT NULL, 
    [TotalSeconds] INT NOT NULL, 
    CONSTRAINT [FK_tblMediaPageViewing_MediaViewing] FOREIGN KEY ([MediaViewingId]) REFERENCES [tblMediaViewing]([Id])
)

GO

CREATE INDEX [IX_tblMediaPageViewing_MediaViewing] ON [dbo].[tblMediaPageViewing] ([MediaViewingId])
