using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;

public static class PerTenantStaticFileExtensions
{
    /// <summary>
    /// Add per-tenant static files to your application.  
    /// </summary>
    /// <param name="app">The IApplicationBuilder instance  </param>
    /// <param name="pathPrefix">The prefix to look for in the route indicating tenant-specific static files e.g. 'tenantfile'</param>
    /// <param name="tenantFolderResolver">A function for obtaining the tenant specific sub folder in which files reside, for a given TTenant</param>
    /// <typeparam name="TTenant">The type of the tenant, as registered in Startup</typeparam>
    /// <returns></returns>
    public static IApplicationBuilder UsePerTenantStaticFiles<TTenant>(this IApplicationBuilder app, string pathPrefix, Func<TTenant, Task<string>> tenantFolderResolver)
    {
        var tenantPrefix = new PathString(pathPrefix);
        app.Use(async (context, next) =>
        {
            var tenantContext = context.GetTenantContext<TTenant>();
            var originalPath = context.Request.Path;
            PathString pathMatched, pathRemaining;
            if (tenantContext != null && originalPath.StartsWithSegments(tenantPrefix, StringComparison.Ordinal, out pathMatched, out pathRemaining))
            {
                var tenantFolder = await tenantFolderResolver(tenantContext.Tenant);
                var newPath = new PathString(tenantFolder).Add(pathRemaining);

                context.Request.Path = newPath;
            }
            await next();
            context.Request.Path = originalPath;
        });
        return app;
    }
}