﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Serilog.Context;

namespace VirtualMgr.MultiTenant
{
    public class SerilogAppTenantMiddleware
    {
        private readonly RequestDelegate _next;
        public SerilogAppTenantMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            // Enrich Serilog with AppTenant
            var appTenant = context.GetTenantContext<AppTenant>();
            if (appTenant?.Tenant != null)
            {
                using (LogContext.PushProperty("AppTenant", appTenant.Tenant.PrimaryHostname))
                {
                    await _next(context);
                }
            }
            else
            {
                await _next(context);
            }
        }
    }
}