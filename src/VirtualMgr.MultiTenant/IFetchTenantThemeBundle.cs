using System.IO;
using System.Threading.Tasks;

namespace VirtualMgr.MultiTenant
{
    public interface IFetchTenantThemeBundle
    {
        Task<Stream> FetchBundleStreamAsync(AppTenant tenant);
    }
}