using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace VirtualMgr.MultiTenant
{
    public class TenantUnresolvedMiddleware<TTenant>
    {
        private readonly RequestDelegate _next;
        public TenantUnresolvedMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            var tenant = context.GetTenantContext<TTenant>();

            if (tenant == null)
            {
                context.Response.StatusCode = (int)HttpStatusCode.BadGateway;
            }
            else
            {
                await _next(context);
            }
        }
    }
}