using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace VirtualMgr.MultiTenant
{
    public interface IFetchAppTenants
    {
        Task<IEnumerable<AppTenant>> FetchAppTenantsAsync();
    }
}