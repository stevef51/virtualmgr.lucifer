using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;

namespace VirtualMgr.MultiTenant
{
    public class ListTenantDomainResolver : ITenantDomainResolver
    {
        public List<ITenantDomainResolver> Resolvers { get; } = new List<ITenantDomainResolver>();

        public string ResolveDomain(HttpContext context)
        {
            foreach (var resolver in Resolvers)
            {
                var domain = resolver.ResolveDomain(context);
                if (domain != null)
                {
                    return domain;
                }
            }
            return null;
        }
    }
}