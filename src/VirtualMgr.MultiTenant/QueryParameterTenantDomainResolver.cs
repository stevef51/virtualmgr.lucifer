﻿using System.Linq;
using Microsoft.AspNetCore.Http;

namespace VirtualMgr.MultiTenant
{
    public class QueryParameterTenantDomainResolver : ITenantDomainResolver
    {
        public readonly string ParameterName;

        public QueryParameterTenantDomainResolver(string parameterName)
        {
            ParameterName = parameterName;
        }

        public string ResolveDomain(HttpContext context)
        {
            return context.Request.Query[ParameterName].FirstOrDefault()?.ToLower();
        }
    }
}