using System.Collections.Generic;
using System.Threading.Tasks;

namespace VirtualMgr.MultiTenant
{
    public interface ITenantsCache
    {
        Task RefreshTenantsAsync();
        Task<IEnumerable<AppTenant>> GetTenantsAsync(bool force);
    }
}