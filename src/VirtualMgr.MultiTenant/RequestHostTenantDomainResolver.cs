using Microsoft.AspNetCore.Http;

namespace VirtualMgr.MultiTenant
{
    public class RequestHostTenantDomainResolver : ITenantDomainResolver
    {
        public string ResolveDomain(HttpContext context) => context.Request.Host.Host.ToLower();
    }
}