using System.Linq;
using Microsoft.AspNetCore.Http;

namespace VirtualMgr.MultiTenant
{
    public class CustomHeaderTenantDomainResolver : ITenantDomainResolver
    {
        public readonly string UseIfHost;
        public readonly string HeaderName;
        public CustomHeaderTenantDomainResolver(string useIfHost, string headerName)
        {
            UseIfHost = useIfHost.Replace("http://", "").Replace("https://", "");
            HeaderName = headerName;
        }

        public string ResolveDomain(HttpContext context)
        {
            if (string.Compare(context.Request.Host.Host, UseIfHost, true) != 0 || !context.Request.Headers.ContainsKey(HeaderName))
            {
                return null;
            }
            return context.Request.Headers[HeaderName].ToArray().FirstOrDefault();
        }
    }
}