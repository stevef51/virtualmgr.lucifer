using System;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog.Context;
using VirtualMgr.AspNetCore;

namespace VirtualMgr.MultiTenant
{
    public static class ApplicationBuilderExtensions
    {

        public static void UseVirtualMgrAppTenant(this IApplicationBuilder app, bool useTenantUnresolvedMiddleware = true)
        {
            app.UseMultitenancy<AppTenant>();
            if (useTenantUnresolvedMiddleware)
            {
                app.UseMiddleware<TenantUnresolvedMiddleware<AppTenant>>();
            }
            app.UseMiddleware<SerilogAppTenantMiddleware>();
            app.UseMiddleware<AuthRedirectAppTenantMiddleware>();
        }
    }
}
