using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace VirtualMgr.MultiTenant
{
    // Note, the TenantsCacheSingleton must be registered as IOC Singleton
    public class TenantsCacheSingleton : ITenantsCache
    {
        private IFetchAppTenants _fetchAppTenants;
        private IList<AppTenant> _tenants;
        private SemaphoreSlim _tenantsLock = new SemaphoreSlim(1, 1);
        private readonly ILogger _logger;

        public TenantsCacheSingleton(
            ILogger<TenantsCacheSingleton> logger,
            IFetchAppTenants fetchAppTenants)
        {
            _fetchAppTenants = fetchAppTenants;
            _logger = logger;
        }

        public async Task RefreshTenantsAsync()
        {
            _logger.LogInformation("Clearing Tenants cache..");
            if (_tenants != null)
            {
                await _tenantsLock.WaitAsync();
                try
                {
                    _tenants = null;
                }
                finally
                {
                    _tenantsLock.Release();
                }
            }
        }

        public async Task<IEnumerable<AppTenant>> GetTenantsAsync(bool force)
        {
            var tenants = _tenants;
            if (tenants == null || force)
            {
                await _tenantsLock.WaitAsync();
                try
                {
                    tenants = _tenants;
                    if (tenants == null || force)
                    {
                        tenants = _tenants = (await _fetchAppTenants.FetchAppTenantsAsync()).ToList();
                        _logger.LogInformation("Refreshed {count} tenants, force = {force}", tenants.Count, force);
                    }
                }
                finally
                {
                    _tenantsLock.Release();
                }
            }

            return tenants;
        }
    }
}