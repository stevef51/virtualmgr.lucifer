using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using SaasKit.Multitenancy;

namespace VirtualMgr.MultiTenant
{
    public class AppTenantResolver : ITenantResolver<AppTenant>
    {
        private readonly ITenantDomainResolver _tenantDomainResolver;
        private readonly ILogger _logger;
        private readonly ITenantsCache _tenantsCache;
        public AppTenantResolver(
            ILogger<AppTenantResolver> logger,
            ITenantDomainResolver tenantDomainResolver,
            ITenantsCache tenantsCache
        )
        {
            _tenantDomainResolver = tenantDomainResolver;
            _tenantsCache = tenantsCache;
            _logger = logger;
        }

        private async Task<AppTenant> findTenantAsync(string host, bool force)
        {
            var tenants = await _tenantsCache.GetTenantsAsync(force);
            return tenants.Where(
                t => t.PrimaryHostname == host
                || (host.EndsWith($".{t.PrimaryHostname}", StringComparison.Ordinal)
                    && !host.Substring(0, host.Length - t.PrimaryHostname.Length - 1).Contains("."))
            ).OrderByDescending(t => t.PrimaryHostname.Length).FirstOrDefault();
        }

        public async Task<TenantContext<AppTenant>> ResolveAsync(HttpContext context)
        {
            var host = _tenantDomainResolver.ResolveDomain(context);
            if (host == null)
            {
                return null;
            }
            var tenant = await findTenantAsync(host, false);

            if (tenant == null)
            {
                _logger.LogDebug("Tenant {host} not found, force refreshing Tenant Cache and retrying...", host);
                tenant = await findTenantAsync(host, true);

                if (tenant == null)
                {
                    return new TenantContext<AppTenant>(new AppTenant(host));
                }
            }

            return new TenantContext<AppTenant>(tenant);
        }
    }
}
