using Microsoft.AspNetCore.Http;

namespace VirtualMgr.MultiTenant
{
    public interface ITenantDomainResolver
    {
        string ResolveDomain(HttpContext context);
    }
}