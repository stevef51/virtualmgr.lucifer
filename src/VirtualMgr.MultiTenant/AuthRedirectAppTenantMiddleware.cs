﻿using System;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Serilog.Context;
using VirtualMgr.AspNetCore;

namespace VirtualMgr.MultiTenant
{
    public class AuthRedirectAppTenantMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ServiceOptions _serviceOptions;

        public AuthRedirectAppTenantMiddleware(RequestDelegate next, IOptions<ServiceOptions> serviceOptions)
        {
            _next = next;
            _serviceOptions = serviceOptions.Value;
        }

        public async Task Invoke(HttpContext context)
        {
            await _next(context);

            // Look for Redirect to 
            if (context.Response.StatusCode == StatusCodes.Status302Found)
            {
                var appTenant = context.GetTenantContext<AppTenant>();
                if (appTenant?.Tenant != null)
                {
                    string location = context.Response.Headers[Microsoft.Net.Http.Headers.HeaderNames.Location];

                    var authRedirectRegex = $@"^(?<scheme>https?):\/\/{_serviceOptions.Services.Auth}\.([^\/]*)(?<path>.*)";
                    location = Regex.Replace(location, authRedirectRegex, $"${{scheme}}://{_serviceOptions.Services.Auth}.{appTenant.Tenant.PrimaryHostname}${{path}}");

                    context.Response.Headers[Microsoft.Net.Http.Headers.HeaderNames.Location] = location;
                }
            }
        }
    }
}