using System.Threading.Tasks;
using MassTransit;
using VirtualMgr.MassTransit.Contracts;

namespace VirtualMgr.MultiTenant.Consumers
{
    public class TenantConnectionsUpdatedConsumer : IConsumer<TenantConnectionsUpdated>
    {
        private readonly ITenantsCache _tenantsCache;
        public TenantConnectionsUpdatedConsumer(ITenantsCache tenantsCache)
        {
            _tenantsCache = tenantsCache;
        }

        public Task Consume(ConsumeContext<TenantConnectionsUpdated> context)
        {
            return _tenantsCache.RefreshTenantsAsync();
        }
    }
}