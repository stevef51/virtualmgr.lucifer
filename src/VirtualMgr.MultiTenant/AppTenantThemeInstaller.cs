using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;

namespace VirtualMgr.MultiTenant
{
    // This class will grab the tenants current theme from the database and install it to the provided path
    public class AppTenantThemeInstaller
    {
        private static Dictionary<string, DateTime> _tenantThemeUnzippedUtc = new Dictionary<string, DateTime>();
        private readonly IDistributedCache _cache;
        private readonly IFetchTenantThemeBundle _fetchThemeBundle;
        private readonly ILogger<AppTenantThemeInstaller> _logger;

        public AppTenantThemeInstaller(ILogger<AppTenantThemeInstaller> logger, IDistributedCache cache, IFetchTenantThemeBundle fetchThemeBundle)
        {
            _logger = logger;
            _cache = cache;
            _fetchThemeBundle = fetchThemeBundle;
        }

        public async Task InstallThemeIfRequiredAsync(AppTenant tenant, string folder)
        {
            bool unzipRequired = false;
            DateTime unzippedUtc = DateTime.MinValue;
            if (_tenantThemeUnzippedUtc.TryGetValue(tenant.Folder, out unzippedUtc))
            {
                var tenantThemeUpdatedUtcString = await _cache.GetStringAsync($"{tenant.Folder}.ThemeUpdatedUtc");
                if (tenantThemeUpdatedUtcString != null)
                {
                    var tenantThemeUpdatedUtc = DateTime.Parse(tenantThemeUpdatedUtcString);
                    unzipRequired = tenantThemeUpdatedUtc > unzippedUtc;
                }
            }
            else
            {
                unzipRequired = true;
            }

            if (unzipRequired)
            {
                _logger.LogInformation($"Fetching Theme Bundle for {tenant.PrimaryHostname}");
                var bundleStream = await _fetchThemeBundle.FetchBundleStreamAsync(tenant);
                if (bundleStream == null)
                {
                    _logger.LogInformation($"No theme");
                }
                else
                {
                    _logger.LogInformation($"Clearing out Theme folder {folder}");
                    if (System.IO.Directory.Exists(folder))
                    {
                        System.IO.Directory.Delete(folder, true);
                    }
                    System.IO.Directory.CreateDirectory(folder);
                    _logger.LogInformation($"Extracting Theme into for {folder}");
                    using (ZipArchive archive = new ZipArchive(bundleStream, ZipArchiveMode.Read))
                    {
                        archive.ExtractToDirectory(folder);
                        _tenantThemeUnzippedUtc[tenant.Folder] = DateTime.UtcNow;
                    }
                }
            }
        }
    }
}