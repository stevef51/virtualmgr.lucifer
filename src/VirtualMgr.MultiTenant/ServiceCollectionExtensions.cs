using MassTransit;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using VirtualMgr.MultiTenant.Consumers;

namespace VirtualMgr.MultiTenant
{
    public static class ServiceCollectionExtensions
    {
        public static void AddVirtualMgrAppTenant(this IServiceCollection services, IConfiguration config)
        {
            services.AddMultitenancy<AppTenant, AppTenantResolver>();
            services.AddSingleton<ITenantsCache, TenantsCacheSingleton>();

            services.AddTransient<TenantConnectionsUpdatedConsumer>();
        }

        public static void AddVirtualMgrAppTenant<TDomainResolver>(this IServiceCollection services, IConfiguration config) where TDomainResolver : class, ITenantDomainResolver
        {
            services.AddSingleton<ITenantDomainResolver, TDomainResolver>();
            services.AddVirtualMgrAppTenant(config);
        }
    }
}