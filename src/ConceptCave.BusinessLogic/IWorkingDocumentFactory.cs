﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.BusinessLogic.Models;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.BusinessLogic
{
    public interface IWorkingDocumentFactory
    {
        PlayerModel CreateStartAndSave(int groupId, int resourceId, Guid reviewerId, Guid? revieweeId, string args, int? publishingGroupResourceId = null);

        PlayerModel CreateStartAndSave(int groupId, int resourceId, string args);

        IWorkingDocument CreateFrom(WorkingDocumentDTO dto);
    }
}
