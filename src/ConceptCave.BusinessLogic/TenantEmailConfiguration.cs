﻿using ConceptCave.Checklist.Interfaces;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using VirtualMgr.MultiTenant;

namespace ConceptCave.BusinessLogic
{
    public class TenantEmailConfiguration : IEmailConfiguration
    {
        private readonly AppTenant _appTenant;

        public TenantEmailConfiguration(AppTenant appTenant)
        {
            _appTenant = appTenant;
        }

        public bool Enabled
        {
            get { return _appTenant.ActiveProfile.IsEmailEnabled; }
        }

        public string FilterAddress(string address)
        {
            address = address.Replace(";", ",").Trim(',', ' ');

            var whiteListAddresses = _appTenant.ActiveProfile.EmailWhitelist != null ? _appTenant.ActiveProfile.EmailWhitelist.Split(',') : null;
            if (whiteListAddresses == null || whiteListAddresses.Length == 0)
                return address;

            var addresses = address.Split(',');
            var filtered = new List<string>();
            foreach (var a in addresses)
            {
                var saneAddress = a.ToLower().Trim();
                var ok = false;
                foreach (var wla in whiteListAddresses)
                {
                    var saneWla = wla.ToLower().Trim();
                    var rex = Regex.Escape(saneWla).Replace(@"\*", ".*").Replace(@"\?", ".").Replace(@"#", @"\d");
                    var reg = new Regex(rex);

                    if (reg.IsMatch(saneAddress))
                    {
                        ok = true;
                        break;
                    }
                }
                if (ok)
                    filtered.Add(a);
            }
            return string.Join(",", filtered);
        }


        public string SubjectAppend
        {
            get
            {
                if (!string.IsNullOrEmpty(_appTenant.ActiveProfile.EmailWhitelist))
                {
                    return $" ({_appTenant.PrimaryHostname} {_appTenant.ActiveProfile.Name})";
                }
                return null;
            }
        }

        public System.Net.Mail.MailAddress From
        {
            get
            {
                var address = _appTenant.EmailFromAddress;
                if (string.IsNullOrEmpty(address))
                {
                    return new System.Net.Mail.MailAddress($"noreply@{_appTenant.PrimaryHostname}");
                }
                else
                {
                    return new System.Net.Mail.MailAddress(address);
                }
            }
        }
    }
}