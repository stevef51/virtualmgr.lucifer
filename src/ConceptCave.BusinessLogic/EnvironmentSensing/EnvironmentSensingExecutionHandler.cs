﻿using ConceptCave.BusinessLogic.Models;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.HelperClasses;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.SimpleExtensions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.BusinessLogic.EnvironmentSensing
{
    public class EnvironmentSensingExecutionHandler : IExecutionMethodHandler
    {
        private readonly IESProbeRepository _probeRepo;
        private readonly IESSensorReadingRepository _sensorReadingRepo;
        private readonly IESSensorRepository _sensorRepo;

        public class SensorReadingParameter
        {
            [JsonProperty("rid")]
            public Guid RequestId { get; set; }
            [JsonProperty("tuuid")]
            public string TabletUUID { get; set; }
            [JsonProperty("psr")]
            public List<ProbeSensorReading> ProbeSensorReadings { get; set; }
            [JsonProperty("tim")]
            public DateTime TimestampUtc { get; set; }
            public class ProbeSensorReading
            {
                [JsonProperty("id")]
                public int? ProbeId { get; set; }

                [JsonProperty("key")]
                public string ProbeKey { get; set; }
                [JsonProperty("model")]
                public string ProbeModel { get; set; }
                [JsonProperty("serial")]
                public string ProbeSerialNumber { get; set; }

                [JsonProperty("sid")]
                public int? SensorId { get; set; }

                [JsonProperty("six")]
                public int? SensorIndex { get; set; }            
                [JsonProperty("st")]
                public int? SensorType { get; set; }

                [JsonProperty("val")]
                public double ReadingValue { get; set; }
                [JsonProperty("tim")]
                public DateTime ReadingTimestampUtc { get; set; }
            }
        }

        public class SensorReadingResponse
        {
        }

        public EnvironmentSensingExecutionHandler(
            IESProbeRepository probeRepo, 
            IESSensorRepository sensorRepo, 
            IESSensorReadingRepository sensorReadingRepo)
        {
            _probeRepo = probeRepo;
            _sensorRepo = sensorRepo;
            _sensorReadingRepo = sensorReadingRepo;
        }

        public string Name
        {
            get { return "EnvironmentSensing"; }
        }

        public Newtonsoft.Json.Linq.JToken Execute(string method, Newtonsoft.Json.Linq.JToken parameters, IExecutionMethodHandlerContext context)
        {
            switch(method)
            {
                case "SensorReadings":
                    return JObject.FromObject(SensorReadings(parameters.ToObject<SensorReadingParameter>()));
            }
            throw new InvalidOperationException(string.Format("{0} does not exist as a method on the EnvironmentSensingExecutionHandler", method));
        }

        protected SensorReadingResponse SensorReadings(SensorReadingParameter parameters)
        {
            if (parameters.ProbeSensorReadings != null)
            {
                Dictionary<int, ESProbeDTO> probeDtoCache = new Dictionary<int, ESProbeDTO>();
                Dictionary<int, ESSensorDTO> sensorDtoCache = new Dictionary<int, ESSensorDTO>();

                List<ESSensorReadingRecord> records = new List<ESSensorReadingRecord>();
                foreach (var psr in parameters.ProbeSensorReadings)
                {
                    ESProbeDTO probeDto = null;
                    if (psr.ProbeId.HasValue)
                    {
                        if (!probeDtoCache.TryGetValue(psr.ProbeId.Value, out probeDto))
                        {
                            probeDto = _probeRepo.GetById(psr.ProbeId.Value, ESProbeLoadInstructions.None);
                            if (probeDto != null)
                            {
                                probeDtoCache.Add(probeDto.Id, probeDto);
                            }
                        }
                    }
                    else
                    {
                        // Lookup by Model,Serial ..
                        if (psr.ProbeKey != null && psr.ProbeKey != null)
                        {
                            probeDto = probeDtoCache.Values.FirstOrDefault(p => p.Key == psr.ProbeKey);
                            if (probeDto == null)
                            {
                                probeDto = _probeRepo.GetByKey(psr.ProbeKey, ESProbeLoadInstructions.None);
                                if (probeDto == null)
                                {
                                    probeDto = new ESProbeDTO()
                                    {
                                        __IsNew = true,
                                        Key = psr.ProbeKey,
                                        Model = psr.ProbeModel,
                                        SerialNumber = psr.ProbeSerialNumber,
                                        Name = psr.ProbeModel + " " + psr.ProbeSerialNumber
                                    };
                                    probeDto = _probeRepo.Save(probeDto, true, false);
                                }

                                probeDtoCache.Add(probeDto.Id, probeDto);
                            }
                        }
                    }

                    ESSensorDTO sensorDto = null;
                    if (psr.SensorId.HasValue)
                    {
                        if (!sensorDtoCache.TryGetValue(psr.SensorId.Value, out sensorDto))
                        {
                            sensorDto = _sensorRepo.GetById(psr.SensorId.Value);
                            if (sensorDto != null)
                            {
                                sensorDtoCache.Add(sensorDto.Id, sensorDto);
                            }
                        }
                    }
                    else if (probeDto != null)
                    {
                        if (psr.SensorIndex.HasValue)
                        {
                            sensorDto = sensorDtoCache.Values.FirstOrDefault(s => s.ProbeId == probeDto.Id && s.SensorIndex == psr.SensorIndex.Value);
                            if (sensorDto == null)
                            {
                                sensorDto = _sensorRepo.GetByProbeIdSensorIndex(probeDto.Id, psr.SensorIndex.Value, ESSensorLoadInstructions.None);
                                if (sensorDto == null && psr.SensorType.HasValue)
                                {
                                    sensorDto = new ESSensorDTO()
                                    {
                                        __IsNew = true,
                                        ProbeId = probeDto.Id,
                                        SensorIndex = psr.SensorIndex.Value,
                                        SensorType = psr.SensorType.Value
                                    };
                                    sensorDto = _sensorRepo.Save(sensorDto, true, false);
                                }
                                if (sensorDto != null)
                                {
                                    sensorDtoCache.Add(sensorDto.Id, sensorDto);
                                }
                            }
                        }
                    }

                    if (probeDto != null && sensorDto != null)
                    {
                        records.Add(new ESSensorReadingRecord()
                        {
                            SensorId = sensorDto.Id,
                            SensorTimestampUtc = psr.ReadingTimestampUtc,
                            Value = psr.ReadingValue,
                            TabletUUID = parameters.TabletUUID
                        });
                    }
                }

                if (records.Any())
                {
                    _sensorReadingRepo.WriteSensorReadingRecords(records);
                }
            }
            return new SensorReadingResponse();
        }
    }
}
