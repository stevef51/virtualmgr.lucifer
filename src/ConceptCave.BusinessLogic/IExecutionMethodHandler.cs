﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace ConceptCave.BusinessLogic
{
    /// <summary>
    /// Interface implemented by objects that are end points for the execute method of the play controller.
    /// This method eases the wiring up required to support questions or client side objects that need to make
    /// call backs specific to their implementation to the server code.
    /// </summary>
    public interface IExecutionMethodHandler
    {
        string Name { get; }

        JToken Execute(string method, JToken parameters, IExecutionMethodHandlerContext context);
    }

    public interface IExecutionMethodHandlerContext
    {
        object CurrentUserId { get; }
    }

    /// <summary>
    /// Interface implemented by execution handlers that also support their messages being queued
    /// if there is a problem and played back later on.
    /// </summary>
    public interface IServerSideQueuedExecutionHandler : IExecutionMethodHandler
    {
        /// <summary>
        /// Called to test to see if the specific method supports queueing
        /// </summary>
        /// <param name="method">Name of method who's data is looking to be queued</param>
        /// <returns>True if queuing is supported otherwise false</returns>
        bool MethodSupportsQueue(string method, JToken parameters);

        /// <summary>
        /// Used to extract the ID of the primary object (for example task). This will be used
        /// to work out if there are any queued requests and react accordingly.
        /// </summary>
        /// <param name="method">Method that is looking to be queued</param>
        /// <param name="parameters">JToken containing method parameters</param>
        /// <returns></returns>
        string ExractPrimaryEntityId(string method, JToken parameters);
    }
}
