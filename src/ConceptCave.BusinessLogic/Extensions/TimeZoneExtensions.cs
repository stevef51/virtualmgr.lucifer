﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.DTO.DTOClasses;
using VirtualMgr.Common;

namespace ConceptCave.BusinessLogic.Extensions
{
    public static class TimeZoneExtensions
    {
        public static TimeZoneInfo TimeZoneInfo(this UserDataDTO ud)
        {
            return TimeZoneInfoResolver.ResolveWindows(ud.TimeZone);
        }

        public static DateTime DateCreatedInReviewerTimeZone(this WorkingDocumentDTO wd)
        {
            var dt = DateTime.SpecifyKind(wd.DateCreated, DateTimeKind.Utc);
            var tz = wd.Reviewer.TimeZoneInfo();
            return System.TimeZoneInfo.ConvertTimeFromUtc(dt, tz);
        }

        public static DateTime DateCreatedInRevieweeTimeZone(this WorkingDocumentDTO wd)
        {
            var dt = DateTime.SpecifyKind(wd.DateCreated, DateTimeKind.Utc);
            if (wd.Reviewee == null)
                return dt;
            var tz = wd.Reviewee.TimeZoneInfo();
            return System.TimeZoneInfo.ConvertTimeFromUtc(dt, tz);
        }
    }
}
