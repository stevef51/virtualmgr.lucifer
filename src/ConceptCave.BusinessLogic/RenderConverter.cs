﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using ConceptCave.BusinessLogic.Configuration;
using ConceptCave.BusinessLogic.Questions;
using ConceptCave.Checklist.Editor;
using ConceptCave.Checklist.Editor.Reporting;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.Lingo;
using ConceptCave.Checklist.RunTime;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Repositories;
using Newtonsoft.Json.Linq;
using ConceptCave.RepositoryInterfaces;
using ConceptCave.BusinessLogic.Facilities;
using Newtonsoft.Json;
using ConceptCave.Checklist;

namespace ConceptCave.BusinessLogic
{
    public delegate JObject RendererFunc(
        IPresentedQuestion pQuestion, IEnumerable<IInvalidReason> invalidReasons, IWorkingDocument wd);

    public class RenderConverter : TypedCachedMethodDispatcher<RendererFunc>
    {
        private readonly IRepositorySectionManager _repoSectionMgr;
        private readonly IMediaRepository _mediaRepo;
        private readonly IMembershipRepository _memberRepo;
        private readonly INewsFeedRepository _newsFeedRepo;
        private readonly IGlobalSettingsRepository _globalSettingsRepo;
        private readonly ITaskTypeRepository _taskTypeRepo;
        private readonly IProductRepository _productRepo;
        private readonly IServerUrls _serverUrls;
        private readonly Func<PendingPaymentFacility> _fnPendingPaymentFacility;
        private readonly IProductCatalogRepository _productCatalogRepo;

        public RenderConverter(IRepositorySectionManager repoSectionMgr,
            IMediaRepository mediaRepo,
            IMembershipRepository memberRepo,
            INewsFeedRepository newsFeedRepo,
            IGlobalSettingsRepository globalSettingsRepo,
            ITaskTypeRepository taskTypeRepo,
            IProductRepository productRepo,
            IServerUrls serverUrls,
            Func<PendingPaymentFacility> fnPendingPaymentFacility,
            IProductCatalogRepository productCatalogRepo
            ) : this(repoSectionMgr, mediaRepo, memberRepo, newsFeedRepo, globalSettingsRepo, taskTypeRepo, productRepo, serverUrls, fnPendingPaymentFacility, productCatalogRepo, true)
        {
        }

        public RenderConverter(IRepositorySectionManager repoSectionMgr,
            IMediaRepository mediaRepo,
            IMembershipRepository memberRepo,
            INewsFeedRepository newsFeedRepo,
            IGlobalSettingsRepository globalSettingsRepo,
            ITaskTypeRepository taskTypeRepo,
            IProductRepository productRepo,
            IServerUrls serverUrls,
            Func<PendingPaymentFacility> fnPendingPaymentFacility,
            IProductCatalogRepository productCatalogRepo,
            bool shouldCache) : base(shouldCache)
        {
            _repoSectionMgr = repoSectionMgr;
            _mediaRepo = mediaRepo;
            _memberRepo = memberRepo;
            _newsFeedRepo = newsFeedRepo;
            _globalSettingsRepo = globalSettingsRepo;
            _taskTypeRepo = taskTypeRepo;
            _productRepo = productRepo;
            _serverUrls = serverUrls;
            _fnPendingPaymentFacility = fnPendingPaymentFacility;
            _productCatalogRepo = productCatalogRepo;
        }

        #region Common Code/Dispatch
        public JObject MakePresentedRenderable(IPresented presented, IValidatorResult validatorResult, IWorkingDocument wd)
        {
            //Start by finding something to handle this and follow the call graph down
            if (presented is IPresentedSection)
                try
                {
                    var jObj = SectionRenderer(presented as IPresentedSection, validatorResult, wd);
                    if (((IPresentedSection)presented).Parent is IWorkingDocument)
                    {
                        // This is the root section, we can add some useful stuff here
                        var jUrls = new JObject();
                        jUrls["odata"] = _serverUrls.ODataV3Url;
                        jUrls["odatav3"] = _serverUrls.ODataV3Url;
                        jUrls["odatav4"] = _serverUrls.ODataV4Url;
                        //                        jUrls["reporting"] = _serverUrls.ReportingUrl;            // Telerik is gone!
                        jObj["urls"] = jUrls;

                        jObj["presentasfinal"] = ((IPresentedSection)presented).Section.PresentAsFinal;
                    }
                    return jObj;
                }
                catch (ExpandStringException e)
                {
                    e.QuestionName = ((IPresentedSection)presented).Section.Name;
                    e.QuestionType = "Section";
                    throw;
                }
            else if (presented is IPresentedQuestion)
                try
                {
                    return QuestionRenderer(presented as IPresentedQuestion, validatorResult, wd);
                }
                catch (ExpandStringException e)
                {
                    e.QuestionName = ((IPresentedQuestion)presented).Question.Name;
                    e.QuestionType = ((IPresentedQuestion)presented).Question.GetType().Name;
                    throw;
                }
            else
                throw new InvalidOperationException("Only presented questions/sections can be serialised to JSON");

        }

        protected JObject SectionRenderer(IPresentedSection pSection, IValidatorResult validatorResult, IWorkingDocument wd)
        {
            if (!pSection.Section.Enabled)
                return null;

            var model = new JObject();
            model["PresentableId"] = pSection.Id.ToString();
            model["Id"] = pSection.Section.Id.ToString();
            model["Visible"] = pSection.Section.ClientModel.Visible;
            model["Name"] = pSection.Section.Name;
            model["HtmlPresentation"] = RenderHtmlPresentation(pSection.Section.HtmlPresentation, wd);
            model["Prompt"] = wd.Program.ExpandString(pSection.Section.Prompt);
            model["PresentedType"] = "Section";
            model["Directive"] = "question-section";
            model["AllowFilterByCategory"] = pSection.Section.AllowFilterByCategory;
            model["datasource"] = wd.Program.ExpandString(pSection.Section.DataSource);
            model["ngcontroller"] = pSection.Section.NgController;
            model["enabledeepedit"] = pSection.Section.EnableDeepEdit;
            JArray categories = new JArray();

            if (pSection.Section.AllowFilterByCategory == true)
            {
                // get the distinct set of categories first
                (from c in pSection.Section.AsDepthFirstEnumerable() where c is IQuestion from v in ((IQuestion)c).Validators where v is ConceptCave.Checklist.ProcessingRules.CategoryRule select ((ConceptCave.Checklist.ProcessingRules.CategoryRule)v).Text)
                    .Distinct()
                    .ToList()
                    .ForEach(s =>
                    {
                        var obj = new JObject();
                        obj["Category"] = s;
                        obj["ErrorCount"] = 0;

                        categories.Add(obj);
                    });

                if (validatorResult != null)
                {
                    // now use this to work out how many validation errors we have for each category
                    foreach (var obj in categories)
                    {
                        obj["ErrorCount"] = (from c in pSection.Section.AsDepthFirstEnumerable()
                                             where c is IQuestion
                                             from v in ((IQuestion)c).Validators
                                             where v is ConceptCave.Checklist.ProcessingRules.CategoryRule &&
                                                 ((ConceptCave.Checklist.ProcessingRules.CategoryRule)v).Text == obj["Category"].ToString()
                                             select validatorResult.InvalidReasons.Where(r => r.Answer != null && r.Answer.PresentedQuestion.Question.Id == ((IQuestion)c).Id).Count()).Sum();
                    }
                }
            }

            model["ChildCategories"] = categories;

            model["Children"] = new JArray();
            foreach (var child in pSection.Children)
            {
                var childModel = MakePresentedRenderable(child, validatorResult, wd);
                if (childModel != null) //don't add models without a render converter
                    ((JArray)model["Children"]).Add(childModel);
            }
            return model;
        }

        protected JObject QuestionRenderer(IPresentedQuestion pQuestion, IValidatorResult validatorResult, IWorkingDocument wd)
        {
            var qtype = pQuestion.Question.GetType();
            RendererFunc delg;
            try
            {
                delg = GetMethod(qtype);
            }
            catch (NotImplementedException)
            {
                //no renderer, do nothing
                return null;
            }

            var q = pQuestion.Question;
            //Grab the IInvalidReason for htis question
            var invalidReasons = validatorResult != null
                ? validatorResult.InvalidReasons.Where(r => r.Answer != null && r.Answer.PresentedQuestion.Question.Id == q.Id).ToList()
                : new List<IInvalidReason>();

            var model = delg(pQuestion, invalidReasons, wd);

            // Generically handle Answer ExtraValues
            var extraValues = pQuestion.GetAnswer() as IAnswerExtraValues;
            if (extraValues != null)
            {
                JToken answerToken = null;
                if (!model.TryGetValue("Answer", out answerToken))
                {
                    model["Answer"] = new JObject();
                }

                var answer = model["Answer"];
                var dict = extraValues.ExtraValues;
                if (dict.Any())
                {
                    answer["ExtraValues"] = new JObject();
                    foreach (var ev in dict)
                    {
                        answer["ExtraValues"][ev.Key] = ev.Value?.ToString();
                    }
                }
            }

            //Common properties
            //Visible by default
            model["RootId"] = wd.Id.ToString();
            model["Visible"] = pQuestion.Question.ClientModel == null || pQuestion.Question.ClientModel.Visible;
            model["Prompt"] = pQuestion.Prompt;
            model["HidePrompt"] = q.HidePrompt;
            model["HtmlPresentation"] = RenderHtmlPresentation(q.HtmlPresentation, wd);
            model["Note"] = pQuestion.Note;
            model["AllowNotes"] = q.AllowNotesOnAnswer;
            model["PresentedId"] = pQuestion.Id.ToString();
            model["Id"] = pQuestion.Id.ToString();
            model["QuestionId"] = pQuestion.Question.Id.ToString();
            model["Name"] = wd.Program.ExpandString(pQuestion.NameAs);
            model["IsAnswerable"] = q.IsAnswerable;
            model["PresentedType"] = "Question";
            model["QuestionType"] = q.GetType().Name;
            model["Directive"] =
                _repoSectionMgr.ItemForAssemblyQualifiedName(q.GetType().AssemblyQualifiedName).DirectiveName;

            JArray validators = new JArray();
            foreach (var validator in pQuestion.Question.Validators)
            {
                // we are going to do something slightly different for the validators. Rather than having a method that
                // we get to through reflection etc, we just have a method on the interface we can call
                var jVal = validator.ToJson();

                if (jVal != null)
                {
                    jVal["type"] = validator.GetType().Name.ToLower();
                    validators.Add(jVal);
                }
            }

            model["validators"] = validators;

            JArray categories = new JArray();

            (from r in pQuestion.Question.Validators where r is ConceptCave.Checklist.ProcessingRules.CategoryRule select r)
                .Cast<ConceptCave.Checklist.ProcessingRules.CategoryRule>()
                .ToList().ForEach(r => categories.Add(r.Text));

            model["Categories"] = categories;

            model["ValidationErrors"] = JArray.FromObject(invalidReasons.Select(r => r.Reason));

            model["HelpPosition"] = q.HelpPosition.ToString();
            model["HelpContent"] = q.HelpContent;

            return model;
        }

        protected JObject RenderHtmlPresentation(IHtmlPresentationHint pHint, IWorkingDocument wd)
        {
            var obj = JObject.FromObject(pHint);
            obj["CustomClass"] = wd.Program.ExpandString(pHint.CustomClass);

            string customStyling = wd.Program.ExpandString(pHint.CustomStyling);
            obj["CustomStyling"] = customStyling;

            return obj;
        }

        protected override RendererFunc ConstructDelegate(MethodInfo method)
        {
            return (RendererFunc)Delegate.CreateDelegate(typeof(RendererFunc), this, method);
        }
        #endregion

        [HandlesType(typeof(FreeTextQuestion))]
        protected JObject FreeTextQuestionRenderer(IPresentedQuestion pQuestion,
                                                   IEnumerable<IInvalidReason> validatorResult, IWorkingDocument wd)
        {
            var model = new JObject();
            var q = (pQuestion.Question as FreeTextQuestion);
            model["AllowMultiline"] = q.AllowMultiLine;

            var answer = pQuestion.GetAnswer() as IFreeTextAnswer;
            model["Answer"] = new JObject();
            model["Answer"]["Text"] = answer == null ? "" : (answer.AnswerAsString ?? "");
            //just makes sure there are no nullreferences

            return model;
        }

        [HandlesType(typeof(LabelQuestion))]
        protected JObject LabelQuestionRenderer(IPresentedQuestion pQuestion,
                                                IEnumerable<IInvalidReason> validatorResults, IWorkingDocument wd)
        {
            var q = (pQuestion.Question as LabelQuestion);

            var model = new JObject();
            model["Text"] = wd.Program.ExpandString(q.Text);
            model["Listen"] = q.Listen;
            model["ListenTo"] = q.ListenTo;
            model["HideUntilFirstListen"] = q.HideUntilFirstListen;
            //no answer!
            return model;
        }

        [HandlesType(typeof(MultiChoiceQuestion))]
        protected JObject MultiChoiceQuestionRenderer(IPresentedQuestion pQuestion,
                                                      IEnumerable<IInvalidReason> validatorResult, IWorkingDocument wd)
        {
            var model = new JObject();
            var q = (pQuestion.Question as MultiChoiceQuestion);

            model["DisplayFormat"] = q.Format.ToString();
            model["AllowMultiSelect"] = q.AllowMultiSelect;
            model["Choices"] =
                JArray.FromObject(q.List.Select(i => new { Display = wd.Program.ExpandString(i.Display), Id = i.Id }));

            var answer = pQuestion.GetAnswer() as IMultiChoiceAnswer;
            model["Answer"] = new JObject();
            //Do the inverse of the AnswerConverter for this
            if (q.AllowMultiSelect)
            {
                var choiceMap = new JObject();
                foreach (var mcItem in q.Options)
                {
                    //Adds a kvp where key is a Guid and val is a bool whether or not it is selected
                    choiceMap.Add(mcItem.Id.ToString(),
                        answer.ChosenItems.Any(i => i.Id == mcItem.Id));
                }
                model["Answer"]["Selected"] = choiceMap;
            }
            else
            {
                if (answer.ChosenItems.Any())
                    model["Answer"]["Selected"] = answer.ChosenItems.Single().Id.ToString();
                else
                    model["Answer"]["Selected"] = null;
            }

            return model;
        }

        [HandlesType(typeof(CheckboxQuestion))]
        protected JObject CheckboxQuestionRenderer(IPresentedQuestion pQuestion,
                                                      IEnumerable<IInvalidReason> validatorResult, IWorkingDocument wd)
        {
            var model = new JObject();
            var q = (pQuestion.Question as CheckboxQuestion);

            var answer = pQuestion.GetAnswer() as ICheckboxAnswer;

            model["Answer"] = new JObject();
            model["Answer"]["Value"] = answer.AnswerBool.HasValue == true ? answer.AnswerBool.Value : false;

            return model;
        }

        [HandlesType(typeof(SelectUserQuestion))]
        protected JObject SelectUserQuestionRenderer(IPresentedQuestion pQuestion,
            IEnumerable<IInvalidReason> validatorResult, IWorkingDocument wd)
        {
            var model = new JObject();
            //the answer
            var q = (pQuestion.Question as SelectUserQuestion);
            var answer = pQuestion.GetAnswer() as SelectUserAnswer;
            model["Answer"] = new JObject();
            if (answer.SelectedUserId.HasValue)
            {
                model["Answer"]["SelectedId"] = answer.SelectedUserId.Value.ToString();
                model["Answer"]["SelectedName"] = answer.SelectedUser.Name;
            }
            else
            {
                model["Answer"]["SelectedId"] = null;
                model["Answer"]["SelectedName"] = null;
            }
            model["AllowedLabels"] = new JArray(q.AllowedLabelIds);

            model["datasource"] = wd.Program.ExpandString(q.DataSource);
            if (string.IsNullOrEmpty(q.DataSource) == true)
            {
                model["datasource"] = null;
            }

            model["updatedatasources"] = wd.Program.ExpandString(q.UpdateDataSources);
            if (string.IsNullOrEmpty(q.UpdateDataSources) == true)
            {
                model["updatedatasources"] = null;
            }

            model["cleardatasources"] = wd.Program.ExpandString(q.ClearDataSources);
            if (string.IsNullOrEmpty(q.ClearDataSources) == true)
            {
                model["cleardatasources"] = null;
            }

            return model;
        }

        [HandlesType(typeof(SelectODataQuestion))]
        protected JObject SelectODataQuestionRenderer(IPresentedQuestion pQuestion,
            IEnumerable<IInvalidReason> validatorResult, IWorkingDocument wd)
        {
            var model = new JObject();
            //the answer
            var q = (pQuestion.Question as SelectODataQuestion);
            var answer = pQuestion.GetAnswer() as SelectODataAnswer;
            model["Answer"] = new JObject();
            model["Answer"]["Selected"] = answer.JObject;

            model["datasource"] = wd.Program.ExpandString(q.DataSource);
            if (string.IsNullOrEmpty(q.DataSource) == true)
            {
                model["datasource"] = null;
            }

            model["updatedatasources"] = wd.Program.ExpandString(q.UpdateDataSources);
            if (string.IsNullOrEmpty(q.UpdateDataSources) == true)
            {
                model["updatedatasources"] = null;
            }

            model["cleardatasources"] = wd.Program.ExpandString(q.ClearDataSources);
            if (string.IsNullOrEmpty(q.ClearDataSources) == true)
            {
                model["cleardatasources"] = null;
            }

            return model;
        }



        [HandlesType(typeof(SelectFacilityStructureQuestion))]
        protected JObject SelectFacilityStructureQuestionRenderer(IPresentedQuestion pQuestion,
    IEnumerable<IInvalidReason> validatorResult, IWorkingDocument wd)
        {
            var model = new JObject();
            //the answer
            var q = (pQuestion.Question as SelectFacilityStructureQuestion);
            var answer = pQuestion.GetAnswer() as SelectFacilityStructureAnswer;

            model["includenextbutton"] = q.IncludeNextButton;
            model["startfromparentid"] = q.StartFromParentId;

            model["Answer"] = new JObject();
            model["Answer"]["SelectedFacilityStructureId"] = answer.SelectedStructureId;

            return model;
        }

        [HandlesType(typeof(NumberQuestion))]
        protected JObject NumberQuestionRenderer(IPresentedQuestion pQuestion, IEnumerable<IInvalidReason> validatorResult, IWorkingDocument wd)
        {
            var model = new JObject();
            var q = (pQuestion.Question as NumberQuestion);
            var answer = pQuestion.GetAnswer() as INumberAnswer;
            model["Answer"] = new JObject();
            model["Answer"]["Number"] = answer == null ? null : answer.AnswerNumber;
            model["DecimalPlaces"] = q.DecimalPlaces;
            model["Step"] = 1 / Math.Pow(10, q.DecimalPlaces);
            if (q.MaxValue.HasValue)
                model["MaxValue"] = q.MaxValue.Value;
            if (q.MinValue.HasValue)
                model["MinValue"] = q.MinValue.Value;

            model["ShowKeypad"] = q.ShowKeypad;
            model["UseDecimalKeypad"] = q.UseDecimalKeypad;

            return model;
        }

        [HandlesType(typeof(TemperatureProbeQuestion))]
        protected JObject TemperatureProbeQuestionRenderer(IPresentedQuestion pQuestion, IEnumerable<IInvalidReason> validatorResult, IWorkingDocument wd)
        {
            var model = new JObject();
            var q = (pQuestion.Question as TemperatureProbeQuestion);
            var answer = pQuestion.GetAnswer() as TemperatureProbeAnswer;
            model["Answer"] = new JObject();
            model["Answer"]["Celcius"] = answer == null ? null : answer.Celcius;

            model["DisplayAsFarenheit"] = q.DisplayAsFarenheit;
            model["RefreshPeriod"] = q.RefreshPeriod;
            model["MinimumPassReading"] = q.MinimumPassReading;
            model["MaximumPassReading"] = q.MaximumPassReading;
            model["DisconnectProbeTimeout"] = q.DisconnectProbeTimeout;
            model["AllowedProbeProviders"] = q.AllowedProbeProviders;
            model["AllowedProbeSerialNumbers"] = q.AllowedProbeSerialNumbers;
            model["ProbeOutOfRangeTimeout"] = q.ProbeOutOfRangeTimeout;
            model["TemperatureReading"] = q.TemperatureReading.ToString();

            return model;
        }

        [HandlesType(typeof(pHProbeQuestion))]
        protected JObject pHProbeQuestionRenderer(IPresentedQuestion pQuestion, IEnumerable<IInvalidReason> validatorResult, IWorkingDocument wd)
        {
            var model = new JObject();
            var q = (pQuestion.Question as pHProbeQuestion);
            var answer = pQuestion.GetAnswer() as pHProbeAnswer;
            model["Answer"] = new JObject();
            model["Answer"]["pH"] = answer == null ? null : answer.pH;

            model["RefreshPeriod"] = q.RefreshPeriod;
            model["MinimumPassReading"] = q.MinimumPassReading;
            model["MaximumPassReading"] = q.MaximumPassReading;

            return model;
        }

        [HandlesType(typeof(DateTimeQuestion))]
        protected JObject DateTimeQuestionRenderer(IPresentedQuestion pQuestion, IEnumerable<IInvalidReason> validatorResult, IWorkingDocument wd)
        {
            var model = new JObject();
            var q = (pQuestion.Question as IDateTimeQuestion);
            var answer = pQuestion.GetAnswer() as IDateTimeAnswer;
            model["Answer"] = new JObject();

            if (answer.AnswerDateTime.HasValue)
            {
                model["Answer"]["DateTime"] = answer.AnswerDateTime.Value.ToString("yyyy-MM-dd");
            }
            else
            {
                model["Answer"]["DateTime"] = null;
            }

            model["updatedatasources"] = wd.Program.ExpandString(q.UpdateDataSources);

            return model;
        }

        [HandlesType(typeof(GeoLocationQuestion))]
        protected JObject GeoLocationQuestionRenderer(IPresentedQuestion pQuestion, IEnumerable<IInvalidReason> validatorResult, IWorkingDocument wd)
        {
            var model = new JObject();
            var q = (pQuestion.Question as IGeoLocationAnswer);
            var answer = pQuestion.GetAnswer() as IGeoLocationAnswer;
            model["Answer"] = new JObject();
            if (answer != null && answer.Coordinates != null)
                model["Answer"]["Coordinates"] = JObject.FromObject(answer.Coordinates);

            return model;
        }

        [HandlesType(typeof(SignatureQuestion))]
        protected JObject SignatureQuestionRenderer(IPresentedQuestion pQuestion, IEnumerable<IInvalidReason> validatorResult, IWorkingDocument wd)
        {
            var model = new JObject();
            var q = (pQuestion.Question as ISignatureQuestion);
            var answer = pQuestion.GetAnswer() as ISignatureAnswer;
            model["Answer"] = new JObject();

            model["Width"] = RenderDimension(q.Width);
            model["Height"] = RenderDimension(q.Height);
            model["AllowTypedEntry"] = q.AllowTypedEntry;
            model["TakePictureWhenSigning"] = q.TakePictureWhenSigning;
            model["ShowTakenPicture"] = q.ShowTakenPicture;

            if (answer != null && answer.AnswerAsJsonString != null)
                model["Answer"]["Signature"] = answer.AnswerAsJsonString;

            return model;
        }

        protected JObject RenderDimension(IDimension dim)
        {
            var result = new JObject();
            result["Value"] = dim.Value;
            result["Type"] = dim.DimensionType.ToString();

            return result;
        }

        [HandlesType(typeof(UploadMediaQuestion))]
        protected JObject UploadMediaQuestionRenderer(IPresentedQuestion pQuestion, IEnumerable<IInvalidReason> validatorResult, IWorkingDocument wd)
        {
            var model = new JObject();
            var q = (pQuestion.Question as IUploadMediaQuestion);
            var answer = pQuestion.GetAnswer() as IUploadMediaAnswer;

            var options = new JObject();
            options["maxUploadCount"] = q.MaxUploadCount;
            if (q.MaxWidth.HasValue)
                options["maxWidth"] = q.MaxWidth;
            if (q.MaxHeight.HasValue)
                options["maxHeight"] = q.MaxHeight;
            options["keepAspect"] = q.KeepAspect;
            options["allowImage"] = q.AllowImage;
            options["allowAudio"] = q.AllowAudio;
            options["allowVideo"] = q.AllowVideo;
            options["allowanymedia"] = q.AllowAnyMediaType;
            options["uploadprompt"] = q.UploadPrompt;

            var postValues = new JObject();
            postValues["rootId"] = wd.Id.ToString();
            postValues["presentedId"] = pQuestion.Id.ToString();
            options["postValues"] = postValues;

            model["options"] = options;

            model["Answer"] = new JObject();
            model["Answer"]["Items"] = new JArray();
            foreach (var item in answer.Items)
            {
                var jItem = new JObject(); //yeah we're weay off consistancy with casing
                jItem["mediaId"] = item.MediaId.ToString();
                jItem["name"] = item.Name;
                jItem["fname"] = item.Filename;
                jItem["description"] = item.Description;
                ((JArray)model["Answer"]["Items"]).Add(jItem);
            }
            return model;
        }

        [HandlesType(typeof(ProgressIndicatorQuestion))]
        protected JObject ProgressIndicatorQuestionRenderer(IPresentedQuestion pQuestion,
            IEnumerable<IInvalidReason> validatorResult, IWorkingDocument wd)
        {
            var model = new JObject();
            var q = (pQuestion.Question as ProgressIndicatorQuestion);
            model["percentage"] = q.ValueAsPercentage * 100;
            return model;
        }

        [HandlesType(typeof(PresentWordDocumentQuestion))]
        protected JObject PresentWordDocumentQuestionRenderer(IPresentedQuestion pQuestion,
            IEnumerable<IInvalidReason> validatorResult, IWorkingDocument wd)
        {
            var model = new JObject();
            var q = (pQuestion.Question as PresentWordDocumentQuestion);
            var a = (pQuestion.GetAnswer() as PresentWordDocumentAnswer);

            var currentUsers = wd.Program.Get<ConceptCave.BusinessLogic.LingoDataObjects.CurrentContextFacilityCurrentUsers>();

            var media = _mediaRepo.GetById(q.MediaId, RepositoryInterfaces.Enums.MediaLoadInstruction.None);

            model["userid"] = currentUsers.ReviewerId;
            model["workingdocumentid"] = wd.Id;

            model["allowdownload"] = q.AllowDownload;
            model["mediaid"] = q.MediaId;
            model["version"] = media.Version;
            model["mergewithchecklist"] = q.MergeWithChecklist;
            model["previewtype"] = q.PreviewType.ToString();
            model["usehighquality"] = q.UseUseHighQualityRendering;
            model["useantialiasing"] = q.UseAntiAliasing;
            model["resolution"] = q.Resolution;
            model["pagecount"] = media.PreviewCount;
            model["trackviewing"] = q.TrackPageViewing;

            model["Answer"] = new JObject();
            model["Answer"]["lastpageread"] = a.LastPageRead;
            model["Answer"]["allpagesread"] = a.AllPagesRead;

            return model;
        }

        [HandlesType(typeof(MediaDirectoryQuestion))]
        protected JObject MediaDirectoryQuestionRenderer(IPresentedQuestion pQuestion,
            IEnumerable<IInvalidReason> validatorResult, IWorkingDocument wd)
        {
            var model = new JObject();
            var q = (pQuestion.Question as MediaDirectoryQuestion);

            var currentUsers = wd.Program.Get<ConceptCave.BusinessLogic.LingoDataObjects.CurrentContextFacilityCurrentUsers>();

            model["userid"] = currentUsers.ReviewerId;
            model["showpageviewings"] = q.ShowPageViewings;

            return model;
        }

        [HandlesType(typeof(MediaViewingSummaryQuestion))]
        protected JObject MediaViewingSummaryQuestionRenderer(IPresentedQuestion pQuestion,
            IEnumerable<IInvalidReason> validatorResult, IWorkingDocument wd)
        {
            var model = new JObject();
            var q = (pQuestion.Question as MediaViewingSummaryQuestion);

            var currentUsers = wd.Program.Get<ConceptCave.BusinessLogic.LingoDataObjects.CurrentContextFacilityCurrentUsers>();

            model["userid"] = currentUsers.ReviewerId;

            model["listen"] = q.Listen;
            model["listento"] = q.ListenTo;

            return model;
        }

        [HandlesType(typeof(TableQuestion))]
        protected JObject TableQuestionRenderer(IPresentedQuestion pQuestion,
            IEnumerable<IInvalidReason> validatorResult, IWorkingDocument wd)
        {
            var model = new JObject();
            var q = (pQuestion.Question as TableQuestion);

            model["showcontrols"] = q.ShowControls;

            model["stripes"] = q.Stripes;
            model["bordered"] = q.Bordered;
            model["hover"] = q.Hover;
            model["condense"] = q.Condense;

            model["hidewhennodata"] = q.HideWhenNoData;

            if (q.DataTable == null)
            {
                model["datasource"] = wd.Program.ExpandString(q.DataSource);
                model["usedatasourcevisiblefields"] = q.UseDataSourceVisibleFields;
                model["updatedatasources"] = wd.Program.ExpandString(q.UpdateDataSources);
                model["cleardatasources"] = wd.Program.ExpandString(q.ClearDataSources);

                if (q.UseDataSourceVisibleFields == false)
                {
                    JArray displayFields = new JArray();
                    foreach (var field in q.VisibleFields.Split(','))
                    {
                        displayFields.Add(field.Trim());
                    }

                    model["displayfields"] = displayFields;
                }
            }
            else
            {

            }

            return model;
        }

        [HandlesType(typeof(RepeaterQuestion))]
        protected JObject RepeaterQuestionRenderer(IPresentedQuestion pQuestion,
            IEnumerable<IInvalidReason> validatorResult, IWorkingDocument wd)
        {
            var model = new JObject();
            var q = (pQuestion.Question as RepeaterQuestion);

            //    model["showcontrols"] = q.ShowControls;

            model["datasource"] = wd.Program.ExpandString(q.DataSource);
            model["updatedatasources"] = wd.Program.ExpandString(q.UpdateDataSources);
            model["cleardatasources"] = wd.Program.ExpandString(q.ClearDataSources);
            model["showcontrols"] = q.ShowControls;
            model["repeatcontent"] = wd.Program.ExpandString(q.RepeatContent);
            model["hidewhennodata"] = q.HideWhenNoData;

            return model;
        }

        [HandlesType(typeof(TimelineQuestion))]
        protected JObject TimelineQuestionRenderer(IPresentedQuestion pQuestion,
            IEnumerable<IInvalidReason> validatorResult, IWorkingDocument wd)
        {
            var model = new JObject();
            var q = (pQuestion.Question as TimelineQuestion);

            model["datasource"] = wd.Program.ExpandString(q.DataSource);
            model["updatedatasources"] = wd.Program.ExpandString(q.UpdateDataSources);
            model["cleardatasources"] = wd.Program.ExpandString(q.ClearDataSources);
            model["fillheight"] = q.FillHeight;
            model["fillheightbottompadding"] = q.BottomPaddingWhenFillingHeight;
            model["dateformat"] = q.DateFormat;

            return model;
        }

        [HandlesType(typeof(LineChartQuestion))]
        protected JObject LineChartQuestionRenderer(IPresentedQuestion pQuestion,
            IEnumerable<IInvalidReason> validatorResult, IWorkingDocument wd)
        {
            var model = new JObject();
            var q = (pQuestion.Question as LineChartQuestion);


            var seriesArray = new JArray();
            var highchart = new JObject();

            foreach (var s in q.Series)
            {
                var seriesObject = new JObject();

                s.ToJson(seriesObject);

                seriesArray.Add(seriesObject);
            }

            highchart["chart"] = new JObject();
            highchart["chart"]["type"] = q.ChartType;
            highchart["credits"] = new JObject();
            highchart["credits"]["text"] = "";
            highchart["legend"] = new JObject();
            highchart["legend"]["align"] = "right";
            highchart["legend"]["layout"] = "vertical";
            highchart["legend"]["verticalAlign"] = "top";

            highchart["title"] = new JObject();
            if (!q.HidePrompt && q.Prompt != null)
            {
                highchart["title"]["text"] = q.Prompt;
            }
            else
            {
                highchart["title"]["text"] = "";
            }

            highchart["xAxis"] = new JObject();
            highchart["xAxis"]["title"] = new JObject();
            highchart["xAxis"]["title"]["text"] = q.XAxisName;
            highchart["xAxis"]["type"] = q.XAxisUnits.ToString().ToLower();

            JObject x = new JObject();
            if (q.XAxes != null)
            {
                q.XAxes.ToJson(x);
            }
            highchart["xAxis"]["labels"] = x;

            highchart["yAxis"] = new JObject();
            highchart["yAxis"]["title"] = new JObject();
            highchart["yAxis"]["title"]["text"] = q.YAxisName;


            highchart["series"] = seriesArray;
            model["highchart"] = highchart;
            return model;
        }

        [HandlesType(typeof(PieChartQuestion))]
        protected JObject PieChartQuestionRenderer(IPresentedQuestion pQuestion,
            IEnumerable<IInvalidReason> validatorResult, IWorkingDocument wd)
        {
            var model = new JObject();
            var q = (pQuestion.Question as PieChartQuestion);
            Func<DateTime, double> toJSTimestamp =
                time => Math.Round(time.Subtract(new DateTime(1970, 1, 1, 0, 0, 0)).TotalMilliseconds);

            var seriesArray = new JArray();
            var highchart = new JObject();
            var seriesObject = new JObject();

            //           var piedata = q.Series.XYData.Select(r => new Tuple<string, double>(r.GetColumn(1).ToString(),
            //               Convert.ToDouble(r.GetColumn(2))));

            //           seriesObject["data"] = new JArray(piedata.Select(p => new JArray(p.Item1, p.Item2)));


            highchart["chart"] = new JObject();
            highchart["chart"]["defaultSeriesType"] = "pie";
            highchart["credits"] = new JObject();
            highchart["credits"]["text"] = "";
            highchart["legend"] = new JObject();
            highchart["legend"]["align"] = "right";
            highchart["legend"]["layout"] = "vertical";
            highchart["legend"]["verticalAlign"] = "top";
            highchart["plotOptions"] = new JObject();
            highchart["plotOptions"]["series"] = new JObject();
            highchart["plotOptions"]["series"]["animation"] = false;
            highchart["plotOptions"]["series"]["enableMouseTracking"] = false;
            highchart["plotOptions"]["series"]["shadow"] = false;
            highchart["plotOptions"]["pie"] = new JObject();
            highchart["plotOptions"]["pie"]["showInLegend"] = true;
            highchart["title"] = new JObject();
            if (!q.HidePrompt && q.Prompt != null)
            {
                highchart["title"]["text"] = q.Prompt;
            }
            else
            {
                highchart["title"]["text"] = "";
            }

            seriesArray.Add(seriesObject);
            highchart["series"] = seriesArray;
            model["highchart"] = highchart;
            return model;
        }

        [HandlesType(typeof(ListNewsFeedQuestion))]
        protected JObject ListNewsFeedQuestionRenderer(IPresentedQuestion pQuestion,
            IEnumerable<IInvalidReason> validatorResult, IWorkingDocument wd)
        {
            var model = new JObject();
            var q = (pQuestion.Question as ListNewsFeedQuestion);

            var currentUsers = wd.Program.Get<ConceptCave.BusinessLogic.LingoDataObjects.CurrentContextFacilityCurrentUsers>();

            model["userid"] = currentUsers.ReviewerId;

            model["viewas"] = q.ViewAs.ToString();
            model["itemcount"] = q.ItemCount;
            model["channels"] = q.Channels;

            var channels = q.Channels.Split(',').ToList();

            var items = _newsFeedRepo.Get(channels.Select(c => c.Trim()).ToArray(), DateTime.UtcNow, q.ItemCount);

            var jItems = new JArray();

            foreach (var item in items)
            {
                var ji = new JObject();
                ji["id"] = item.Id;
                ji["title"] = item.Title;
                ji["headline"] = item.Headline;
                ji["channel"] = item.Channel;
                ji["program"] = item.Program;
                ji["datecreated"] = item.DateCreated;
                ji["validto"] = item.ValidTo;

                jItems.Add(ji);
            }

            model["items"] = jItems;

            return model;
        }

        [HandlesType(typeof(TaskListQuestion))]
        protected JObject TaskListQuestionRenderer(IPresentedQuestion pQuestion,
            IEnumerable<IInvalidReason> validatorResult, IWorkingDocument wd)
        {
            var model = new JObject();
            var q = (pQuestion.Question as TaskListQuestion);

            var currentUsers = wd.Program.Get<ConceptCave.BusinessLogic.LingoDataObjects.CurrentContextFacilityCurrentUsers>();

            model["userid"] = currentUsers.ReviewerId;

            model["template"] = new JObject();
            model["template"]["enforcesingleclockin"] = q.EnforceSingleClockin;
            model["template"]["sorttype"] = q.SortType.ToString();
            model["template"]["sortthrottleseconds"] = q.SortThrottleSeconds;
            model["template"]["measurementtype"] = q.MeasurementType.ToString();
            model["template"]["allowtaskgrouping"] = q.AllowTaskGrouping;
            model["template"]["showgeneralnotesforincompletetasks"] = q.ShowGeneralNotesForIncompleteTasks;
            model["template"]["listen"] = q.Listen;
            model["template"]["refreshperiodseconds"] = q.RefreshPeriodSeconds;
            model["template"]["staleperiodseconds"] = q.StalePeriodSeconds;
            model["template"]["supportoffline"] = q.SupportOffline;
            model["template"]["tasksaddedmessage"] = q.TasksAddedMessage;
            model["template"]["tasksremovedmessage"] = q.TasksRemovedMessage;
            model["template"]["showtasksites"] = q.ShowTaskSites;
            model["template"]["showtaskdistances"] = q.ShowTaskDistances;
            model["template"]["showstarttimes"] = q.ShowStartTimes;
            model["template"]["minimumtaskdurationmessage"] = q.MinimumTaskDurationMessage;
            model["template"]["includeparentstasks"] = q.AllowClaimTasksFromParent;
            model["template"]["allowbuttonstart"] = q.AllowButtonStart;
            model["template"]["allowqrcodestart"] = q.AllowQRCodeStart;
            model["template"]["allowbeaconstart"] = q.AllowBeaconStart;
            model["template"]["allowbuttonfinish"] = q.AllowButtonFinish;
            model["template"]["allowqrcodefinish"] = q.AllowQRCodeFinish;
            model["template"]["promptlatetasks"] = q.PromptLateTasks;

            var types = _taskTypeRepo.GetAll(ConceptCave.RepositoryInterfaces.Enums.ProjectJobTaskTypeLoadInstructions.None);

            JArray creationTemplates = new JArray();
            foreach (var type in q.TaskCreationTemplates)
            {
                var taskType = from t in types where t.Id == type.TaskTypeId select t;

                if (taskType.Count() == 0)
                {
                    continue;
                }

                JObject template = new JObject();
                template["tasktypeid"] = type.TaskTypeId;
                template["mediaid"] = taskType.First().MediaId;

                template["text"] = type.Text;
                template["requiresbasicdetails"] = type.RequiresBasicDetails;
                template["requiressitedetails"] = type.RequiresSiteDetails;
                template["type"] = type.Type.ToString();
                template["defaulttoroster"] = type.DefaultToRoster;

                creationTemplates.Add(template);
            }

            model["template"]["taskcreationtemplates"] = creationTemplates;

            JArray labels = new JArray();
            foreach (var id in q.FilterByLabelIds)
            {
                labels.Add(id);
            }
            model["filterbylabels"] = labels;

            JArray statusses = new JArray();
            foreach (var id in q.FilterByStatusIds)
            {
                statusses.Add(id);
            }
            model["filterbystatusses"] = statusses;

            return model;
        }

        [HandlesType(typeof(ClaimHierarchyBucketTasksQuestion))]
        protected JObject ClaimHierarchyBucketTasksQuestionRenderer(IPresentedQuestion pQuestion,
    IEnumerable<IInvalidReason> validatorResult, IWorkingDocument wd)
        {
            var model = new JObject();
            var q = (pQuestion.Question as ClaimHierarchyBucketTasksQuestion);

            var currentUsers = wd.Program.Get<ConceptCave.BusinessLogic.LingoDataObjects.CurrentContextFacilityCurrentUsers>();

            model["userid"] = currentUsers.ReviewerId;

            model["template"] = new JObject();
            model["template"]["hidewhentaskspresent"] = q.HideWhenTasksPresent;
            model["template"]["hideshowtasklist"] = q.HideShowTaskList;
            model["template"]["usecompactui"] = q.UseCompactUI;
            model["template"]["allowassignmenttootherusers"] = q.AllowAssignmentToOtherUsers;
            model["template"]["listento"] = q.ListenTo;
            model["template"]["maximumusersperletterbucket"] = q.MaximumUsersPerLetterBucket;

            return model;
        }

        public JArray CatalogsForFacilityStructure(int? fsId)
        {
            var catalogs = new JArray();
            var result = new List<ProductCatalogDTO>();
            var rules = new List<ProductCatalogFacilityStructureRuleDTO>();
            if (fsId.HasValue)
            {
                _productCatalogRepo.GetCatalogsForFacilityStructure(fsId.Value, result, rules);
            }
            var catalogDtos = (from r in result where r.Excluded == false select r).ToList();

            foreach (var c in catalogDtos)
            {
                JObject cat = new JObject();
                cat["id"] = c.Id;
                cat["name"] = c.Name;
                cat["description"] = c.Description;
                cat["showpricingwhenordering"] = c.ShowPricingWhenOrdering;
                cat["stockcountwhenordering"] = c.StockCountWhenOrdering;
                cat["showlineitemnoteswhenordering"] = c.ShowLineItemNotesWhenOrdering;

                cat["display"] = true;

                JArray cis = new JArray();

                foreach (var catItem in c.ProductCatalogItems)
                {
                    JObject ci = new JObject();
                    ci["productid"] = catItem.ProductId;
                    ci["minstocklevel"] = catItem.MinStockLevel;
                    var itemPrice = catItem.Price.HasValue ? catItem.Price.Value : catItem.Product.Price;
                    ci["price"] = itemPrice;
                    var itemFunction = catItem.PriceJsFunctionId ?? catItem.Product.PriceJsFunctionId;
                    ci["pricejsfunctionid"] = itemFunction;

                    ci["product"] = catItem.Product.Name;
                    ci["description"] = catItem.Product.Description;
                    ci["code"] = catItem.Product.Code;
                    ci["mediaid"] = catItem.Product.MediaId;
                    ci["typeid"] = catItem.Product.ProductTypeId;
                    ci["extra"] = string.IsNullOrEmpty(catItem.Product.Extra) ? null : JObject.Parse(catItem.Product.Extra);
                    ci["parentid"] = null;

                    JArray children = new JArray();
                    ci["childproducts"] = children;

                    cis.Add(ci);

                    // we are going to create an entry in the catalog for each child product
                    // to keep how these are ordered consistent.
                    foreach (var hierarchy in catItem.Product.ParentProductHierarchies)
                    {
                        // store a mapping of the parent to child relationship so the UI can hook things together
                        JObject h = new JObject();
                        h["productid"] = hierarchy.ChildProductId;
                        children.Add(h);

                        // now add the child product to the catalog like any other product
                        h = new JObject();
                        h["productid"] = hierarchy.ChildProductId;
                        h["minstocklevel"] = catItem.MinStockLevel;
                        h["price"] = hierarchy.ChildProduct.Price.HasValue ? hierarchy.ChildProduct.Price.Value : itemPrice;
                        h["pricejsfunctionid"] = hierarchy.ChildProduct.PriceJsFunctionId.HasValue ? hierarchy.ChildProduct.PriceJsFunctionId.Value : itemFunction;
                        h["product"] = hierarchy.Name;
                        h["description"] = hierarchy.ChildProduct.Description;
                        h["code"] = hierarchy.ChildProduct.Code;
                        h["mediaid"] = hierarchy.ChildProduct.MediaId;
                        h["typeid"] = hierarchy.ChildProduct.ProductTypeId;
                        h["parentid"] = catItem.ProductId;
                        h["childproducts"] = new JArray(); // we don't support anymore than 1 level down at the moment, but this is here in case we want to. We'd need to refactor things a little to be recursive.

                        cis.Add(h);
                    }
                }

                cat["items"] = cis;

                catalogs.Add(cat);
            }
            return catalogs;
        }

        [HandlesType(typeof(OrderQuestion))]
        protected JObject OrderQuestionRenderer(IPresentedQuestion pQuestion,
            IEnumerable<IInvalidReason> validatorResult, IWorkingDocument wd)
        {
            var q = (pQuestion.Question as OrderQuestion);
            var model = new JObject();

            var currentUsers = wd.Program.Get<ConceptCave.BusinessLogic.LingoDataObjects.CurrentContextFacilityCurrentUsers>();
            model["userid"] = currentUsers.ReviewerId;

            model["template"] = new JObject();
            model["template"]["facilitystructureid"] = q.FacilityStructureId;
            model["template"]["showgrandtotal"] = q.ShowGrandTotal;
            model["template"]["showsubtotals"] = q.ShowSubTotals;
            model["template"]["showcatalogdescriptions"] = q.ShowCatalogDescriptions;

            model["template"]["producttitletext"] = q.ProductTitleText;
            model["template"]["pricetitletext"] = q.PriceTitleText;
            model["template"]["minstocktitletext"] = q.MinStockTitleText;
            model["template"]["stocktitletext"] = q.StockTitleText;
            model["template"]["quantitytitletext"] = q.QuantityTitleText;
            model["template"]["lineitemtotaltitletext"] = q.LineItemTotalText;
            model["template"]["catalogtotaltitletext"] = q.CatalogTotalText;
            model["template"]["grandtotaltitletext"] = q.GrandTotalText;
            model["template"]["lineitemnotestitletext"] = q.LineItemNotesTitleText;

            model["template"]["lengthbywidthlengthtext"] = q.LengthByWidthLengthText;
            model["template"]["lengthbywidthwidthtext"] = q.LengthByWidthWidthText;
            model["template"]["lengthbywidthareatext"] = q.LengthByWidthAreaText;
            model["template"]["addremovemanualentryprompt"] = q.AddRemoveManualEntryPrompt;
            model["template"]["addremovemanualentrynotes"] = q.AddRemoveManualEntryNotes;

            model["template"]["ui"] = q.UI.ToString().ToLower();

            var productTypes = _productRepo.GetAllProductTypes();
            var pts = new JArray();
            foreach (var p in productTypes)
            {
                var pt = new JObject();
                pt["id"] = p.Id;
                pt["name"] = p.Name;
                pt["units"] = p.Units;

                pts.Add(pt);
            }
            model["template"]["producttypes"] = pts;

            string[] split = new string[0];
            if (string.IsNullOrEmpty(q.Catalogs) == false)
            {
                split = q.Catalogs.Split(',');
            }

            JArray showcatalogs = new JArray();

            // if there is a catalogs filer defined on the question, let's use that to filter what goes down
            if (split.Length > 0)
            {
                (from s in split select s).ToList().ForEach(s => showcatalogs.Add(s.Trim()));
            }

            model["template"]["showcatalogs"] = showcatalogs;

            model["template"]["catalogs"] = CatalogsForFacilityStructure(q.FacilityStructureId);

            var answer = pQuestion.GetAnswer() as OrderAnswer;
            model["Answer"] = new JObject();
            model["Answer"]["orders"] = answer == null ? new JArray() : string.IsNullOrEmpty(answer.Orders) ? new JArray() : JArray.Parse(answer.Orders);

            return model;
        }

        [HandlesType(typeof(TaskBatchOperationsQuestion))]
        protected JObject TaskBatchOperationsQuestionRenderer(IPresentedQuestion pQuestion,
            IEnumerable<IInvalidReason> validatorResult, IWorkingDocument wd)
        {
            var q = (pQuestion.Question as TaskBatchOperationsQuestion);
            var model = new JObject();
            model["template"] = new JObject();

            var currentUsers = wd.Program.Get<ConceptCave.BusinessLogic.LingoDataObjects.CurrentContextFacilityCurrentUsers>();

            bool isInClaimRole = _memberRepo.UserIsInRole(currentUsers.ReviewerId, TaskUtilitiesQuestionExecutionHandler.TASKUTILITIESROLE_ACQUIRETASKSFROMOTHERS);
            bool isInAssignRole = _memberRepo.UserIsInRole(currentUsers.ReviewerId, TaskUtilitiesQuestionExecutionHandler.TASKUTILITIESROLE_ASSIGNTASKSTOOTHERS);
            bool isInCancelRole = _memberRepo.UserIsInRole(currentUsers.ReviewerId, TaskUtilitiesQuestionExecutionHandler.TASKUTILITIESROLE_CANCEL);
            bool isInFinishRole = _memberRepo.UserIsInRole(currentUsers.ReviewerId, TaskUtilitiesQuestionExecutionHandler.TASKUTILITIESROLE_FINISHBYMANAGEMENT);
            bool isInDeleteRole = _memberRepo.UserIsInRole(currentUsers.ReviewerId, TaskUtilitiesQuestionExecutionHandler.TASKUTILITIESROLE_DELETE);
            bool isInClearStartTimeRole = _memberRepo.UserIsInRole(currentUsers.ReviewerId, TaskUtilitiesQuestionExecutionHandler.TASKUTILITIESROLE_CLEARSTARTTIME);

            model["template"]["assigntodatasource"] = wd.Program.ExpandString(q.AssignToDatasource);
            model["template"]["assigntoparametername"] = wd.Program.ExpandString(q.AssignToParameterName);
            model["template"]["updatedatasource"] = wd.Program.ExpandString(q.UpdateDatasource);
            model["template"]["usebuttons"] = q.UseButtons;

            model["template"]["features"] = new JObject();

            model["template"]["features"]["allowreassign"] = q.AllowReassign && (isInAssignRole || isInClaimRole);
            model["template"]["features"]["allowcancellation"] = q.AllowCancellation && isInCancelRole;
            model["template"]["features"]["allowfinishbymanagement"] = q.AllowFinishByManagement && isInFinishRole;
            model["template"]["features"]["allowfinishcomplete"] = q.AllowFinishComplete && isInFinishRole;
            model["template"]["features"]["aquiretaskswhenfinishing"] = q.AquireTasksWhenFinishing && isInFinishRole;
            model["template"]["features"]["allowdelete"] = q.AllowDelete && isInDeleteRole;
            model["template"]["features"]["allowclearstarttime"] = q.AllowClearStartTime && isInClearStartTimeRole;

            model["template"]["features"]["reassignallowclaim"] = isInClaimRole;
            model["template"]["features"]["reassignallowassignother"] = isInAssignRole;

            model["template"]["features"]["requireauthentication"] = q.RequireAuthentication;
            model["template"]["features"]["kioskmode"] = q.KioskMode;

            return model;
        }

        [HandlesType(typeof(LeaveManagementQuestion))]
        protected JObject LeaveManagementQuestionRenderer(IPresentedQuestion pQuestion,
            IEnumerable<IInvalidReason> validatorResult, IWorkingDocument wd)
        {
            var q = (pQuestion.Question as LeaveManagementQuestion);
            var model = new JObject();

            var currentUsers = wd.Program.Get<ConceptCave.BusinessLogic.LingoDataObjects.CurrentContextFacilityCurrentUsers>();
            model["userid"] = currentUsers.ReviewerId;

            model["template"] = new JObject();
            model["template"]["allowreplacedbyuser"] = q.AllowReplacedByUser;

            return model;
        }

        [HandlesType(typeof(TimesheetManagementQuestion))]
        protected JObject TimesheetManagementQuestionRenderer(IPresentedQuestion pQuestion,
            IEnumerable<IInvalidReason> validatorResult, IWorkingDocument wd)
        {
            var q = (pQuestion.Question as TimesheetManagementQuestion);
            var model = new JObject();

            var currentUsers = wd.Program.Get<ConceptCave.BusinessLogic.LingoDataObjects.CurrentContextFacilityCurrentUsers>();
            model["userid"] = currentUsers.ReviewerId;

            model["template"] = new JObject();

            return model;
        }

        [HandlesType(typeof(TaskScheduleQuestion))]
        protected JObject TaskScheduleRenderer(IPresentedQuestion pQuestion,
            IEnumerable<IInvalidReason> validatorResult, IWorkingDocument wd)
        {
            var model = new JObject();
            var q = (pQuestion.Question as TaskScheduleQuestion);

            var currentUsers = wd.Program.Get<ConceptCave.BusinessLogic.LingoDataObjects.CurrentContextFacilityCurrentUsers>();

            model["userid"] = currentUsers.ReviewerId;

            model["criteria"] = new JObject();
            model["criteria"]["showjobs"] = q.ShowJobs;
            model["criteria"]["userid"] = q.UserId;
            model["criteria"]["companyid"] = (Guid?)q.CompanyId;

            return model;
        }


        [HandlesType(typeof(CompanySummaryQuestion))]
        protected JObject CompanySummaryRenderer(IPresentedQuestion pQuestion,
            IEnumerable<IInvalidReason> validatorResult, IWorkingDocument wd)
        {
            var model = new JObject();
            var q = (pQuestion.Question as CompanySummaryQuestion);

            var currentUsers = wd.Program.Get<ConceptCave.BusinessLogic.LingoDataObjects.CurrentContextFacilityCurrentUsers>();

            model["userid"] = currentUsers.ReviewerId;

            model["criteria"] = new JObject();
            model["criteria"]["showuserlist"] = q.ShowUserList;
            model["criteria"]["cancreateusers"] = q.CanCreateUsers;

            JArray availableusertypes = new JArray();
            foreach (var i in q.AllowedUserTypes)
            {
                availableusertypes.Add(i);
            }

            model["criteria"]["allowedusertypes"] = availableusertypes;

            return model;
        }

        [HandlesType(typeof(CompanyConstructorQuestion))]
        protected JObject CompanyContructorRenderer(IPresentedQuestion pQuestion,
            IEnumerable<IInvalidReason> validatorResult, IWorkingDocument wd)
        {
            var model = new JObject();
            var q = (pQuestion.Question as CompanyConstructorQuestion);

            var currentUsers = wd.Program.Get<ConceptCave.BusinessLogic.LingoDataObjects.CurrentContextFacilityCurrentUsers>();

            model["userid"] = currentUsers.ReviewerId;
            model["tileurl"] = q.TileUrl;
            model["geocodeurl"] = q.GeocodingUrl;

            model["criteria"] = new JObject();
            model["criteria"]["createdefaultuser"] = q.CreateDefaultUser;

            JArray availableusertypes = new JArray();
            foreach (var i in q.AllowedUserTypes)
            {
                availableusertypes.Add(i);
            }

            model["criteria"]["allowedusertypes"] = availableusertypes;

            return model;
        }

        [HandlesType(typeof(CompanyDirectoryQuestion))]
        protected JObject CompanyDirectoryRenderer(IPresentedQuestion pQuestion,
            IEnumerable<IInvalidReason> validatorResult, IWorkingDocument wd)
        {
            var model = new JObject();
            var q = (pQuestion.Question as CompanyDirectoryQuestion);

            var currentUsers = wd.Program.Get<ConceptCave.BusinessLogic.LingoDataObjects.CurrentContextFacilityCurrentUsers>();

            model["userid"] = currentUsers.ReviewerId;
            model["tileurl"] = q.TileUrl;
            model["allowrating"] = q.AllowRating;
            model["allowediting"] = q.AllowEditing;

            return model;
        }

        [HandlesType(typeof(SelectCompanyQuestion))]
        protected JObject SelectCompanyQuestionRenderer(IPresentedQuestion pQuestion,
            IEnumerable<IInvalidReason> validatorResult, IWorkingDocument wd)
        {
            var model = new JObject();
            //the answer
            var q = (pQuestion.Question as SelectCompanyQuestion);
            var answer = pQuestion.GetAnswer() as SelectCompanyAnswer;

            var currentUsers = wd.Program.Get<ConceptCave.BusinessLogic.LingoDataObjects.CurrentContextFacilityCurrentUsers>();

            model["userid"] = currentUsers.ReviewerId;

            model["Answer"] = new JObject();
            if (answer.SelectedCompanyId.HasValue)
            {
                model["Answer"]["SelectedId"] = answer.SelectedCompanyId.Value.ToString();
                model["Answer"]["SelectedName"] = answer.SelectedCompany.Name;
            }
            else
            {
                model["Answer"]["SelectedId"] = null;
                model["Answer"]["SelectedName"] = null;
            }

            return model;
        }

        [HandlesType(typeof(DataSourceQuestion))]
        protected JObject DataSourceQuestionRenderer(IPresentedQuestion pQuestion,
            IEnumerable<IInvalidReason> validatorResult, IWorkingDocument wd)
        {
            var model = new JObject();
            //the answer
            var q = (pQuestion.Question as DataSourceQuestion);

            model["showui"] = q.ShowUserInterface;
            model["Feeds"] = q.FeedsToJson(wd);

            return model;
        }

        [HandlesType(typeof(MappingQuestion))]
        protected JObject MappingQuestionRenderer(IPresentedQuestion pQuestion,
            IEnumerable<IInvalidReason> validatorResult, IWorkingDocument wd)
        {
            var model = new JObject();

            return model;
        }


        [HandlesType(typeof(MergedHierarchicalDataSourceQuestion))]
        protected JObject MergedHierarchicalDataSourceQuestionRenderer(IPresentedQuestion pQuestion,
            IEnumerable<IInvalidReason> validatorResult, IWorkingDocument wd)
        {
            var model = new JObject();
            //the answer
            var q = (pQuestion.Question as MergedHierarchicalDataSourceQuestion);

            model["datasource"] = wd.Program.ExpandString(q.DataSource);

            return model;
        }

        [HandlesType(typeof(eWayPaymentQuestion))]
        protected JObject eWayPaymentQuestionRenderer(IPresentedQuestion pQuestion, IEnumerable<IInvalidReason> validatorResult, IWorkingDocument wd)
        {
            var model = new JObject();
            var q = (pQuestion.Question as eWayPaymentQuestion);
            var answer = pQuestion.GetAnswer() as eWayPaymentAnswer;
            model["Answer"] = new JObject();
            model["Answer"]["TransactionID"] = answer?.TransactionID;
            if (q._transaction != null)
            {
                model["transaction"] = JObject.FromObject(q._transaction);
            }

            return model;
        }

        [HandlesType(typeof(PendingPaymentQuestion))]
        protected JObject PendingPaymentQuestionRenderer(IPresentedQuestion pQuestion, IEnumerable<IInvalidReason> validatorResult, IWorkingDocument wd)
        {
            var model = new JObject();
            var q = (pQuestion.Question as PendingPaymentQuestion);
            var answer = pQuestion.GetAnswer() as PendingPaymentAnswer;
            model["Answer"] = new JObject();
            model["Mode"] = (int)q.Mode;
            if (q.OrderId != null)
            {
                model["orderid"] = (Guid)q.OrderId;
            }
            if (q.Customer != null)
            {
                model["customer"] = JObject.FromObject(q.Customer);
            }
            if (q.PendingPayments != null)
            {
                // Refresh the PendingPayments from the database now, this means if customer Pays and then hits Page refresh
                // they will get a fresh page saying they have paid
                var ppf = _fnPendingPaymentFacility();
                ppf.Refresh(q.PendingPayments);
                model["pendingpayments"] = JObject.FromObject(q.PendingPayments);

                if (q.OrderId == null && q.PendingPayments.Payments.Any())
                {
                    PendingPaymentFacility.PendingPayment pp = q.PendingPayments.Payments.First() as PendingPaymentFacility.PendingPayment;
                    if (pp != null)
                    {
                        model["orderid"] = pp.OrderId;
                    }
                }
            }

            return model;
        }

        [HandlesType(typeof(RecaptchaQuestion))]
        protected JObject RecaptchaQuestionRenderer(IPresentedQuestion pQuestion,
                                        IEnumerable<IInvalidReason> validatorResults, IWorkingDocument wd)
        {
            var q = (pQuestion.Question as RecaptchaQuestion);

            var model = new JObject();

            string siteKey = _globalSettingsRepo.GetSetting<string>(RecaptchaQuestion.RecaptchaSiteKeyName);
            model["sitekey"] = siteKey;

            model["Answer"] = new JObject();

            //no answer!
            return model;
        }

        [HandlesType(typeof(PresentationQuestion))]
        protected JObject PresentationQuestionRenderer(IPresentedQuestion pQuestion,
                                IEnumerable<IInvalidReason> validatorResults, IWorkingDocument wd)
        {
            var q = (pQuestion.Question as PresentationQuestion);

            var model = new JObject();

            var currentUsers = wd.Program.Get<ConceptCave.BusinessLogic.LingoDataObjects.CurrentContextFacilityCurrentUsers>();

            model["userid"] = currentUsers.ReviewerId;
            model["workingdocumentid"] = wd.Id;

            model["template"] = new JObject();
            model["template"]["manifestjson"] = JArray.Parse(q.ManifestJson);
            model["template"]["module"] = q.Module;
            model["template"]["trackviewing"] = q.TrackViewing;
            model["template"]["nextbuttontext"] = string.IsNullOrEmpty(q.NextButtonText) ? "Next" : q.NextButtonText;
            model["template"]["backbuttontext"] = string.IsNullOrEmpty(q.BackButtonText) ? "Previous" : q.BackButtonText;

            var answer = pQuestion.GetAnswer() as PresentationQuestionAnswer;
            model["Answer"] = new JObject();
            model["Answer"]["Data"] = answer == null ? new JArray() : (string.IsNullOrEmpty(answer.Data) ? new JArray() : JArray.Parse(answer.Data));

            return model;
        }
    }
}