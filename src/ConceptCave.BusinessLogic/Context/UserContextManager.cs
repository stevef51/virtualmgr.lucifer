﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConceptCave.BusinessLogic.Configuration;
using ConceptCave.BusinessLogic.LingoDataObjects;
using ConceptCave.BusinessLogic.ReportData;
using ConceptCave.Checklist;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.RunTime;
using ConceptCave.Core;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;

namespace ConceptCave.BusinessLogic.Context
{
    //Contains logic for getting and setting values in user context
    //used in the facilityuserhelper
    //Code originally from there
    //Uses a cache for reads
    public class UserContextManager : ContextManager<UserContextDataDTO>
    {
        private UserContextManager(IPublishingGroupResourceRepository pubResourceRepo,
            IWorkingDocumentStateManager wdMgr, IReportingRepository reportRepo, IRepositorySectionManager sectionMgr,
            ReportingTableWriter reportWriter) : base(pubResourceRepo, wdMgr, reportRepo, sectionMgr,reportWriter)
        {

        }

        public UserContextManager(IMembershipRepository membershipRepo, Guid uid,
            IPublishingGroupResourceRepository pubResourceRepo,
            IWorkingDocumentStateManager wdMgr, IReportingRepository reportRepo, IRepositorySectionManager sectionMgr,
            ReportingTableWriter reportWriter)
            : base(membershipRepo, uid, pubResourceRepo, wdMgr, reportRepo, sectionMgr, reportWriter)
        {
        }

        public UserContextManager(UserDataDTO user, IPublishingGroupResourceRepository pubResourceRepo,
            IWorkingDocumentStateManager wdMgr, IReportingRepository reportRepo, IRepositorySectionManager sectionMgr,
            ReportingTableWriter reportWriter)
            : base(user, pubResourceRepo, wdMgr, reportRepo, sectionMgr, reportWriter)
        {
        }

        public override List<string> ContextNames()
        {
            return _user
                    .UserType.UserTypeContextPublishedResources.Select(e => e.PublishingGroupResource.Name)
                    .ToList();
        }

        protected override Guid GetCompletedWorkingDocumentId(UserContextDataDTO contextRow)
        {
            return contextRow.CompletedWorkingDocumentId == Guid.Empty
                ? CombFactory.NewComb() //first time it's been done
                : contextRow.CompletedWorkingDocumentId; //been done before
        }

        protected override int GetPublishingGroupId(UserContextDataDTO contextRow)
        {
            return contextRow.UserTypeContextPublishedResource.PublishingGroupResourceId;
        }

        protected override void ConfigureWorkingDocument(UserContextDataDTO contextRow, WorkingDocument w)
        {
            w.Name = contextRow.UserTypeContextPublishedResource.PublishingGroupResource.Name;
        }

        protected override ContextWorkingDocumentObjectGetter GetContextWorkingDocumentObjectGetter(int userTypeContextPublishedResourceId, string contextName, Guid userId, IReportingTableWriter reportWriter, IWorkingDocument workingDocument)
        {
            return new UserContextWorkingDocumentObjectGetter(userTypeContextPublishedResourceId, contextName, userId, reportWriter, workingDocument);
        }

        public override ContextWorkingDocumentObjectGetter LoadContextDocument(int publishedResourceid,
            IContextContainer ctx = null)
        {
            //try for a cache hit so we always get the same getter when asking for the same context
            ContextWorkingDocumentObjectGetter result;
            if (_docCache.TryGetValue(publishedResourceid, out result))
                return result;

            var contextRow =
                _user.UserContextDatas.SingleOrDefault(
                    c => c.UserTypeContextPublishedResource.PublishingGroupResourceId == publishedResourceid && c.UserTypeContextPublishedResource.UserTypeId == _user.UserTypeId);

            //Did we get a null row? That means the context checklist hasn't been done before
            if (contextRow == null)
            {
                var userTypeContext =
                    _user.UserType.UserTypeContextPublishedResources.Single(
                        ut => ut.PublishingGroupResourceId == publishedResourceid);
                contextRow = new UserContextDataDTO()
                {
                    __IsNew = true,
                    UserId = _user.UserId,
                    CompletedWorkingDocumentId = Guid.Empty,
                    UserTypeContextPublishedResource = userTypeContext,
                    UserTypeContextPublishedResourceId = userTypeContext.Id
                };
            }


            return LoadContextDocument(contextRow, ctx);
        }

        public override ContextWorkingDocumentObjectGetter LoadContextDocument(string name, IContextContainer ctx = null)
        {
            //translate this into an id call
            var id =
                _user.UserType.UserTypeContextPublishedResources.First(r => 
                    r.PublishingGroupResource.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase))
                    .PublishingGroupResourceId;
            return LoadContextDocument(id, ctx);
        }
    }
}
