﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConceptCave.BusinessLogic.Configuration;
using ConceptCave.BusinessLogic.LingoDataObjects;
using ConceptCave.BusinessLogic.ReportData;
using ConceptCave.Checklist;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.RunTime;
using ConceptCave.Core;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;

namespace ConceptCave.BusinessLogic.Context
{
    //Contains logic for getting and setting values in user context
    //used in the facilityuserhelper
    //Code originally from there
    //Uses a cache for reads
    public class AssetContextManager : ContextManager<AssetContextDataDTO>
    {
        private readonly AssetDTO _asset;

        //Three constructors
        //Private one sets the injected stuff
        //the two public ones set _user based on an ID or a DTO

        private AssetContextManager(IPublishingGroupResourceRepository pubResourceRepo,
            IWorkingDocumentStateManager wdMgr, IReportingRepository reportRepo, IRepositorySectionManager sectionMgr,
            ReportingTableWriter reportWriter) : base(pubResourceRepo, wdMgr, reportRepo, sectionMgr,reportWriter)
        {

        }

        public AssetContextManager(IMembershipRepository memberRepo, IAssetRepository assetRepo, Guid uid, int id,
            IPublishingGroupResourceRepository pubResourceRepo,
            IWorkingDocumentStateManager wdMgr, IReportingRepository reportRepo, IRepositorySectionManager sectionMgr,
            ReportingTableWriter reportWriter)
            : base(memberRepo, uid, pubResourceRepo, wdMgr, reportRepo, sectionMgr, reportWriter)
        {
            _asset = assetRepo.GetById(id, AssetLoadInstructions.AssetType | 
                AssetLoadInstructions.ContextData | 
                AssetLoadInstructions.PublishingGroupResource |
                AssetLoadInstructions.AssetTypeContextPublishedResource);
        }

        public AssetContextManager(AssetDTO asset, IMembershipRepository memberRepo, Guid uid, IPublishingGroupResourceRepository pubResourceRepo,
            IWorkingDocumentStateManager wdMgr, IReportingRepository reportRepo, IRepositorySectionManager sectionMgr,
            ReportingTableWriter reportWriter)
            : base(memberRepo, uid, pubResourceRepo, wdMgr, reportRepo, sectionMgr, reportWriter)
        {
            _asset = asset;
        }

        public override List<string> ContextNames()
        {
            return _asset
                    .AssetType.AssetTypeContextPublishedResources.Select(e => e.PublishingGroupResource.Name)
                    .ToList();
        }

        protected override Guid GetCompletedWorkingDocumentId(AssetContextDataDTO contextRow)
        {
            return contextRow.CompletedWorkingDocumentId == Guid.Empty
                ? CombFactory.NewComb() //first time it's been done
                : contextRow.CompletedWorkingDocumentId; //been done before
        }

        protected override int GetPublishingGroupId(AssetContextDataDTO contextRow)
        {
            return contextRow.AssetTypeContextPublishedResource.PublishingGroupResourceId;
        }

        protected override void ConfigureWorkingDocument(AssetContextDataDTO contextRow, WorkingDocument w)
        {
            // ok, there are 2 scenario's here. The first is the info has come from the db, in which case
            // we can just set things up directly, otherwise we need to do a bit of LINQ
            if(contextRow.__IsNew == false && contextRow.AssetTypeContextPublishedResource != null && contextRow.AssetTypeContextPublishedResource.PublishingGroupResource != null)
            {
                w.Name = contextRow.AssetTypeContextPublishedResource.PublishingGroupResource.Name;
            }
            else
            {
                var pr = GetPublishingGroupResource(contextRow.AssetTypeContextPublishedResource.PublishingGroupResourceId);
                w.Name = pr.Name;
            }
        }

        protected override ContextWorkingDocumentObjectGetter GetContextWorkingDocumentObjectGetter(int userTypeContextPublishedResourceId, string contextName, Guid userId, IReportingTableWriter reportWriter, IWorkingDocument workingDocument)
        {
            return new AssetContextWorkingDocumentObjectGetter(userTypeContextPublishedResourceId, _asset.Id, contextName, userId, reportWriter, workingDocument);
        }

        public override ContextWorkingDocumentObjectGetter LoadContextDocument(int publishedResourceid,
            IContextContainer ctx = null)
        {
            //try for a cache hit so we always get the same getter when asking for the same context
            ContextWorkingDocumentObjectGetter result;
            if (_docCache.TryGetValue(publishedResourceid, out result))
                return result;

            var contextRow =
                _asset.AssetContextDatas.SingleOrDefault(
                    c => c.AssetTypeContextPublishedResource.PublishingGroupResourceId == publishedResourceid && c.AssetTypeContextPublishedResource.AssetTypeId == _asset.AssetTypeId);

            //Did we get a null row? That means the context checklist hasn't been done before
            if (contextRow == null)
            {
                var assetTypeContext =
                    _asset.AssetType.AssetTypeContextPublishedResources.Single(
                        ut => ut.PublishingGroupResourceId == publishedResourceid);
                contextRow = new AssetContextDataDTO()
                {
                    __IsNew = true,
                    AssetId = _asset.Id,
                    CompletedWorkingDocumentId = Guid.Empty,
                    AssetTypeContextPublishedResource = assetTypeContext,
                    AssetTypeContextPublishedResourceId = assetTypeContext.Id
                };
            }

            return LoadContextDocument(contextRow, ctx);
        }

        public override ContextWorkingDocumentObjectGetter LoadContextDocument(string name, IContextContainer ctx = null)
        {
            //translate this into an id call
            var id =
                _asset.AssetType.AssetTypeContextPublishedResources.First(r => 
                    r.PublishingGroupResource.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase))
                    .PublishingGroupResourceId;

            return LoadContextDocument(id, ctx);
        }
    }
}
