﻿using System.Collections.Generic;
using ConceptCave.BusinessLogic.LingoDataObjects;
using ConceptCave.Core;

namespace ConceptCave.BusinessLogic.Context
{
    public interface IContextManager {
        List<string> ContextNames();
        ContextWorkingDocumentObjectGetter LoadContextDocument(string name, IContextContainer ctx = null);

        ContextWorkingDocumentObjectGetter LoadContextDocument(int publishedResourceid,
            IContextContainer ctx = null);
    }
}