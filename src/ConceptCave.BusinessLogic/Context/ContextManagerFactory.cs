﻿using System;
using ConceptCave.BusinessLogic.Configuration;
using ConceptCave.BusinessLogic.ReportData;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Repositories;

namespace ConceptCave.BusinessLogic.Context
{
    public class ContextManagerFactory : IContextManagerFactory
    {

        private readonly IMembershipRepository _membershipRepo;
        private readonly IAssetRepository _assetRepo;
        private readonly IPublishingGroupResourceRepository _pubResourceRepo;
        private readonly IWorkingDocumentStateManager _wdStateMgr;
        private readonly IReportingRepository _reportingRepo;
        private readonly IRepositorySectionManager _sectionManager;
        private readonly ReportingTableWriter _reportWriter;

        public ContextManagerFactory(IMembershipRepository membershipRepo, IAssetRepository assetRepo,
            IPublishingGroupResourceRepository pubResourceRepo, IWorkingDocumentStateManager wdStateMgr,
            IReportingRepository reportingRepo, IRepositorySectionManager sectionManager,
            ReportingTableWriter reportWriter)
        {
            _membershipRepo = membershipRepo;
            _assetRepo = assetRepo;
            _pubResourceRepo = pubResourceRepo;
            _wdStateMgr = wdStateMgr;
            _reportingRepo = reportingRepo;
            _sectionManager = sectionManager;
            _reportWriter = reportWriter;
        }

        public IContextManager GetInstance(object id, ContextType cType, Guid userId)
        {
            if(cType == ContextType.Membership)
            {
                if(id is Guid)
                {
                    return new UserContextManager(_membershipRepo, (Guid)id, _pubResourceRepo, _wdStateMgr, _reportingRepo,
                        _sectionManager, _reportWriter);
                }
                else if(id is UserDataDTO)
                {
                    return new UserContextManager((UserDataDTO)id, _pubResourceRepo, _wdStateMgr, _reportingRepo, _sectionManager,
                        _reportWriter);
                }
            }
            else if(cType == ContextType.Asset)
            {
                if(id is int)
                {
                    return new AssetContextManager(_membershipRepo, _assetRepo, userId, (int)id, _pubResourceRepo, _wdStateMgr, _reportingRepo, _sectionManager, _reportWriter);
                }
                else if(id is AssetDTO)
                {
                    return new AssetContextManager((AssetDTO)id, _membershipRepo, userId, _pubResourceRepo, _wdStateMgr, _reportingRepo, _sectionManager, _reportWriter);
                }
            }

            throw new ArgumentException("id must be a valid object type for translation into a context document and/or context type cannot be none");
        }
    }
}
