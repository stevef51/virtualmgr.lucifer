﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.BusinessLogic.Context
{
    public enum ContextType
    {
        None = 0,
        Membership = 1,
        Asset = 2
    }

    public interface IContextManagerFactory
    {
        IContextManager GetInstance(object id, ContextType cType, Guid userId);
    }
}
