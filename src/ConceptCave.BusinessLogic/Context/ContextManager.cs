﻿using ConceptCave.BusinessLogic.Configuration;
using ConceptCave.BusinessLogic.LingoDataObjects;
using ConceptCave.BusinessLogic.ReportData;
using ConceptCave.Checklist;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.RunTime;
using ConceptCave.Core;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.BusinessLogic.Context
{
    public abstract class ContextManager<T> : IContextManager
    {
        protected readonly UserDataDTO _user;
        protected readonly IDictionary<int, ContextWorkingDocumentObjectGetter> _docCache;
        protected readonly IDictionary<int, PublishingGroupResourceDTO> _resourceCache;
        protected readonly IPublishingGroupResourceRepository _pubResourceRepo;
        protected readonly IWorkingDocumentStateManager _wdMgr;
        protected readonly IReportingRepository _reportRepo;
        protected readonly IRepositorySectionManager _sectionMgr;
        protected readonly ReportingTableWriter _reportWriter;

        //Three constructors
        //Private one sets the injected stuff
        //the two public ones set _user based on an ID or a DTO

        protected ContextManager(IPublishingGroupResourceRepository pubResourceRepo,
            IWorkingDocumentStateManager wdMgr, IReportingRepository reportRepo, IRepositorySectionManager sectionMgr,
            ReportingTableWriter reportWriter)
        {
            _pubResourceRepo = pubResourceRepo;
            _wdMgr = wdMgr;
            _reportRepo = reportRepo;
            _sectionMgr = sectionMgr;
            _reportWriter = reportWriter;
            _docCache = new Dictionary<int, ContextWorkingDocumentObjectGetter>();
            _resourceCache = new Dictionary<int, PublishingGroupResourceDTO>();
        }

        public ContextManager(IMembershipRepository membershipRepo, Guid uid,
            IPublishingGroupResourceRepository pubResourceRepo,
            IWorkingDocumentStateManager wdMgr, IReportingRepository reportRepo, IRepositorySectionManager sectionMgr,
            ReportingTableWriter reportWriter)
            : this(pubResourceRepo, wdMgr, reportRepo, sectionMgr, reportWriter)
        {
            _user = membershipRepo.GetById(uid,
                MembershipLoadInstructions.MembershipType |
                MembershipLoadInstructions.ContextData |
                MembershipLoadInstructions.ContextResources);
        }

        public ContextManager(UserDataDTO user, IPublishingGroupResourceRepository pubResourceRepo,
            IWorkingDocumentStateManager wdMgr, IReportingRepository reportRepo, IRepositorySectionManager sectionMgr,
            ReportingTableWriter reportWriter)
            : this(pubResourceRepo, wdMgr, reportRepo, sectionMgr, reportWriter)
        {
            _user = user;
        }

        protected abstract Guid GetCompletedWorkingDocumentId(T contextRow);

        protected abstract int GetPublishingGroupId(T contextRow);

        protected abstract void ConfigureWorkingDocument(T contextRow, WorkingDocument w);

        protected abstract ContextWorkingDocumentObjectGetter GetContextWorkingDocumentObjectGetter(int userTypeContextPublishedResourceId, string contextName,
            Guid userId, IReportingTableWriter reportWriter, IWorkingDocument workingDocument);

        protected virtual PublishingGroupResourceDTO GetPublishingGroupResource(int id)
        {
            PublishingGroupResourceDTO result = null;

            if (_resourceCache.TryGetValue(id, out result) == false)
            {
                result = _pubResourceRepo.GetById(id, PublishingGroupResourceLoadInstruction.None);

                _resourceCache.Add(id, result);
            }

            return result;
        }

        protected virtual ContextWorkingDocumentObjectGetter LoadContextDocument(T contextRow, IContextContainer ctx)
        {
            var contextCompletedId = GetCompletedWorkingDocumentId(contextRow);

            var contextDocumentId = GetPublishingGroupId(contextRow);

            var e = _pubResourceRepo.GetById(contextDocumentId, PublishingGroupResourceLoadInstruction.Data);
            var xmlblob = e.PublishingGroupResourceData.Data;

            var designDocument = PrepareContextDesignDocument(xmlblob);

            var answers = _reportRepo.GetContextAnswers(contextCompletedId);
            // merge the design document with the answers we have stored for the user
            InsertContextAnswers(designDocument, answers);

            //create working document
            //BUT: With the ID of a previous one if there is one
            var wd = new WorkingDocument(designDocument, contextCompletedId);
            //and context
            //possibly copying from an argument

            ConfigureWorkingDocument(contextRow, wd);

            //but override current users and some other linog related stuff
            Guid userId = Guid.Empty;
            if(_user != null)
            {
                userId = _user.UserId;
            }

            wd.Program.Set<CurrentContextFacilityCurrentUsers>(new CurrentContextFacilityCurrentUsers()
            {
                ReviewerId = userId,
                RevieweeId = userId
            });
            wd.Program.Set<IWorkingDocument>(wd);
            wd.Next(); // get some presenteds

            //GetAnswer on each presented to transfer default answer -> answer
            foreach (var pQuestion in wd.PresentedAsDepthFirstEnumerable().OfType<IPresentedQuestion>())
            {
                pQuestion.GetAnswer();
            }

            //Fill in the default answers to the working document

            //and now hand off to a documentobjectgwetter, who manages this guy from now on
            var getter = GetContextWorkingDocumentObjectGetter(e.Id, e.Name, userId, _reportWriter, wd);
            _docCache.Add(contextDocumentId, getter);
            return getter;
        }

        public abstract List<string> ContextNames();

        protected void InsertContextAnswers(IDesignDocument doc,
                                              IList<CompletedWorkingDocumentPresentedFactDTO> answers)
        {
            //iterate questions in doc and fill .DefaultAnswer on all of them

            foreach (var question in doc.Questions)
            {
                var question1 = question; //local variables in closures don't work as expected
                var answerEnt = answers.FirstOrDefault(a => a.InternalId == question1.Id);      // SingleOrDefault throws an exception if more than 1 (not that there should be more than 1)

                if (answerEnt == null)
                    continue;


                //custom handler?
                var smItem = _sectionMgr.ItemForObject(question);
                if (smItem.HasReportingDataHandler)
                {
                    smItem.CreateReportingDataHandler().FillQuestionDefault(answerEnt, question);
                }
                else
                {
                    //No, we need to do the hard work ourselves
                    var value = answerEnt.CompletedPresentedFactValues.First();
                    question.DefaultAnswer = value.Value;
                }
            }
        }

        protected IDesignDocument PrepareContextDesignDocument(string docxml)
        {
            var dd = _wdMgr.DesignDocumentFromString(docxml);
            _wdMgr.ResolveWorkingDocumentTemplates(dd.AsDepthFirstEnumerable());
            _wdMgr.GuaranteeProgramSourceCode(dd);
            return dd;
        }

        public abstract ContextWorkingDocumentObjectGetter LoadContextDocument(int publishedResourceid,
            IContextContainer ctx = null);

        public abstract ContextWorkingDocumentObjectGetter LoadContextDocument(string name, IContextContainer ctx = null);
    }
}
