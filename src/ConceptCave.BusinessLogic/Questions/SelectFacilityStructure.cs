﻿using ConceptCave.Checklist.Editor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.RunTime;
using ConceptCave.Core.Coding;

namespace ConceptCave.BusinessLogic.Questions
{
    public class SelectFacilityStructureQuestion : Question
    {
        public bool IncludeNextButton { get; set; }
        public int? StartFromParentId { get; set; }

        public override void Encode(IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode<bool>("IncludeNextButton", IncludeNextButton);
            encoder.Encode<int?>("StartFromParentId", StartFromParentId);
        }

        public override void Decode(IDecoder decoder)
        {
            base.Decode(decoder);

            IncludeNextButton = decoder.Decode<bool>("IncludeNextButton");
            StartFromParentId = decoder.Decode<int?>("StartFromParentId");
        }

        public override object DefaultAnswer
        {
            get
            {
                return _defaultAnswer;
            }
            set
            {
                if (value is int)
                {
                    _defaultAnswer = value;
                }
                else if (value is SelectFacilityStructureAnswer)
                {
                    _defaultAnswer = ((SelectFacilityStructureAnswer)value).AnswerValue;
                }
            }
        }

        public override IAnswer CreateBlankAnswer(IPresentedQuestion presentedQuestion)
        {
            var result = new SelectFacilityStructureAnswer() { PresentedQuestion = presentedQuestion };

            if (DefaultAnswer != null && DefaultAnswer is int)
            {
                result.AnswerValue = DefaultAnswer;
            }
            else if (DefaultAnswer != null && DefaultAnswer is SelectFacilityStructureAnswer)
            {
                result.AnswerValue = ((SelectFacilityStructureAnswer)DefaultAnswer).SelectedStructureId;
            }

            return result;
        }
    }

    public class SelectFacilityStructureAnswer : Answer
    {
        protected int? selectedStructureId;

        public override object AnswerValue {
            get
            {
                return selectedStructureId;
            }
            set
            {
                if (value != null)
                {
                    selectedStructureId = (int)value;
                }
                else
                {
                    selectedStructureId = null;
                }
            }
        }

        public int? SelectedStructureId
        {
            get
            {
                return selectedStructureId;
            }
            set
            {
                selectedStructureId = value;
            }
        }

        public override void Encode(IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("SelectedStructureId", selectedStructureId);
        }

        public override void Decode(IDecoder decoder)
        {
            base.Decode(decoder);

            selectedStructureId = decoder.Decode<int?>("SelectedStructureId");
        }

        public override string AnswerAsString
        {
            get
            {
                if(selectedStructureId.HasValue == false)
                {
                    return string.Empty;
                }

                return selectedStructureId.Value.ToString();
            }
        }
    }
}
