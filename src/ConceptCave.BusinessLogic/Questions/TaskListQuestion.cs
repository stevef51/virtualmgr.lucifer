﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using ConceptCave.BusinessLogic.Facilities;
using ConceptCave.BusinessLogic.Json;
using ConceptCave.BusinessLogic.LingoDataObjects;
using ConceptCave.BusinessLogic.Context;
using ConceptCave.Checklist.Editor;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ConceptCave.Checklist;
using ConceptCave.BusinessLogic.ScheduleManagement;
using VirtualMgr.Common;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System.Data.SqlClient;

namespace ConceptCave.BusinessLogic.Questions
{
    public enum TaskListSortType
    {
        /// <summary>
        /// Sort tasks by their distance (ascending) from the users current location gatherd via geocode
        /// </summary>
        Geocode,
        /// <summary>
        /// Sort tasks by their distances (ascending) from the locations associated with the last clockin. 
        /// If there isn't a clockin on the day or the last task didn't have a site, then the sort will fall back to geocode.
        /// </summary>
        LastClockin,
        /// <summary>
        /// Sort tasks based on their start times (ascending).
        /// </summary>
        StartTime,
        /// <summary>
        /// Sort tasks based on their sort order (ascending)
        /// </summary>
        SortOrder
    }

    public enum MeasurementType
    {
        Metric,
        Imperial
    }

    public enum TaskListQuestionTaskCreationTemplateType
    {
        Normal,
        Shortcut
    }
    public class TaskListQuestion : Question
    {
        public class TaskListQuestionTaskCreationTemplate : Node
        {
            /// <summary>
            /// Id of the task type to create
            /// </summary>
            public Guid TaskTypeId { get; set; }
            /// <summary>
            /// Text to be displayed in the UI used to create it
            /// </summary>
            public string Text { get; set; }
            /// <summary>
            /// The user has to enter the name and description of the task as a part of creating it
            /// </summary>
            public bool RequiresBasicDetails { get; set; }
            /// <summary>
            /// The user has to select a site (either by nearest or through some search UI)
            /// </summary>
            public bool RequiresSiteDetails { get; set; }
            /// <summary>
            /// Indicates how the UI should display the item for selection by the user
            /// </summary>
            public TaskListQuestionTaskCreationTemplateType Type { get; set; }
            /// <summary>
            /// If the user isn't in a hierarchy anywhere, which roster should be chosen for them
            /// </summary>
            public int? DefaultToRoster { get; set; }

            public override void Encode(Core.Coding.IEncoder encoder)
            {
                base.Encode(encoder);

                encoder.Encode<Guid>("TaskTypeId", TaskTypeId);
                encoder.Encode<string>("Text", Text);
                encoder.Encode<bool>("RequiresBasicDetails", RequiresBasicDetails);
                encoder.Encode<bool>("RequiresSiteDetails", RequiresSiteDetails);
                encoder.Encode<TaskListQuestionTaskCreationTemplateType>("Type", Type);
                encoder.Encode<int?>("DefaultToRoster", DefaultToRoster);
            }

            public override void Decode(Core.Coding.IDecoder decoder)
            {
                base.Decode(decoder);

                TaskTypeId = decoder.Decode<Guid>("TaskTypeId");
                Text = decoder.Decode<string>("Text");
                RequiresBasicDetails = decoder.Decode<bool>("RequiresBasicDetails");
                RequiresSiteDetails = decoder.Decode<bool>("RequiresSiteDetails");
                Type = decoder.Decode<TaskListQuestionTaskCreationTemplateType>("Type");
                DefaultToRoster = decoder.Decode<int?>("DefaultToRoster");
            }

            public void ToJson(JObject destination)
            {
                destination["tasktypeid"] = TaskTypeId;
                destination["text"] = Text;
                destination["requiresbasicdetails"] = RequiresBasicDetails;
                destination["requiressitedetails"] = RequiresSiteDetails;
                destination["type"] = Type.ToString();
                destination["defaulttoroster"] = null;
                if (DefaultToRoster.HasValue == true)
                {
                    destination["defaulttoroster"] = DefaultToRoster.Value;
                }
            }

            public void FromJson(JObject source)
            {
                TaskTypeId = Guid.Parse(source["tasktypeid"].ToString());
                Text = source["text"].ToString();
                RequiresBasicDetails = bool.Parse(source["requiresbasicdetails"].ToString());
                RequiresSiteDetails = bool.Parse(source["requiressitedetails"].ToString());
                Type = (TaskListQuestionTaskCreationTemplateType)Enum.Parse(typeof(TaskListQuestionTaskCreationTemplateType), source["type"].ToString(), true);
                if (source["defaulttoroster"] != null)
                {
                    string v = source["defaulttoroster"].ToString();
                    if (string.IsNullOrEmpty(v) == false)
                    {
                        DefaultToRoster = int.Parse(v);
                    }
                }
            }
        }

        public bool EnforceSingleClockin { get; set; }
        public TaskListSortType SortType { get; set; }

        // EVS-396 GeoSorting of the task list to be only once every 10sec
        public int SortThrottleSeconds { get; set; }
        public ISet<Guid> FilterByLabelIds { get; set; }

        public ISet<Guid> FilterByStatusIds { get; set; }

        public MeasurementType MeasurementType { get; set; }

        public bool AllowTaskGrouping { get; set; }
        public IList<TaskListQuestionTaskCreationTemplate> TaskCreationTemplates { get; set; }

        public bool ShowGeneralNotesForIncompleteTasks { get; set; }

        public int RefreshPeriodSeconds { get; set; }
        // EVS-412 Offline task list data to have a stale date
        public int StalePeriodSeconds { get; set; }
        public bool Listen { get; set; }

        public bool SupportOffline { get; set; }

        public string TasksAddedMessage { get; set; }

        public string TasksRemovedMessage { get; set; }

        public bool ShowTaskSites { get; set; }
        public bool ShowTaskDistances { get; set; }
        public bool ShowStartTimes { get; set; }

        public string MinimumTaskDurationMessage { get; set; }

        public bool AllowClaimTasksFromParent { get; set; }

        public bool AllowButtonStart { get; set; }
        public bool AllowQRCodeStart { get; set; }
        public bool AllowBeaconStart { get; set; }
        public bool AllowButtonFinish { get; set; }
        public bool AllowQRCodeFinish { get; set; }

        public bool PromptLateTasks { get; set; }

        public TaskListQuestion()
        {
            EnforceSingleClockin = false;
            FilterByLabelIds = new HashSet<Guid>();
            FilterByStatusIds = new HashSet<Guid>();
            TaskCreationTemplates = new List<TaskListQuestionTaskCreationTemplate>();
            AllowTaskGrouping = true;
            ShowTaskSites = true;
            ShowTaskDistances = true;
            ShowStartTimes = true;
            AllowButtonFinish = true;
            AllowButtonStart = true;

            MeasurementType = BusinessLogic.Questions.MeasurementType.Metric;

            PromptLateTasks = false;
        }

        public override object DefaultAnswer
        {
            get
            {
                return null;
            }
            set
            {

            }
        }

        public override bool IsAnswerable
        {
            get
            {
                return false;
            }
        }

        public override Checklist.Interfaces.IAnswer CreateBlankAnswer(Checklist.Interfaces.IPresentedQuestion presentedQuestion)
        {
            return null;
        }

        public override void Encode(Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode<bool>("EnforceSingleClockin", EnforceSingleClockin);
            encoder.Encode<TaskListSortType>("SortType", SortType);
            encoder.Encode<int>("SortThrottleSeconds", SortThrottleSeconds);
            encoder.Encode<ISet<Guid>>("FilterByLabelIds", FilterByLabelIds);
            encoder.Encode<ISet<Guid>>("FilterByStatusIds", FilterByStatusIds);
            encoder.Encode<MeasurementType>("MeasurementType", MeasurementType);
            encoder.Encode<bool>("AllowTaskGrouping", AllowTaskGrouping);
            encoder.Encode<IList<TaskListQuestionTaskCreationTemplate>>("CreationTemplates", TaskCreationTemplates);
            encoder.Encode<bool>("ShowGeneralNotesForIncompleteTasks", ShowGeneralNotesForIncompleteTasks);
            encoder.Encode("RefreshPeriodSeconds", RefreshPeriodSeconds);
            encoder.Encode("StalePeriodSeconds", StalePeriodSeconds);
            encoder.Encode("Listen", Listen);
            encoder.Encode("SupportOffline", SupportOffline);
            encoder.Encode("TasksAddedMessage", TasksAddedMessage);
            encoder.Encode("TasksRemovedMessage", TasksRemovedMessage);
            encoder.Encode<bool>("ShowTaskSites", ShowTaskSites);
            encoder.Encode<bool>("ShowTaskDistances", ShowTaskDistances);
            encoder.Encode<bool>("ShowStartTimes", ShowStartTimes);
            encoder.Encode<string>("MinimumTaskDurationMessage", MinimumTaskDurationMessage);
            encoder.Encode<bool>("AllowClaimTasksFromParent", AllowClaimTasksFromParent);
            encoder.Encode<bool>("AllowButtonStart", AllowButtonStart);
            encoder.Encode<bool>("AllowQRCodeStart", AllowQRCodeStart);
            encoder.Encode<bool>("AllowBeaconStart", AllowBeaconStart);
            encoder.Encode<bool>("AllowButtonFinish", AllowButtonFinish);
            encoder.Encode<bool>("AllowQRCodeFinish", AllowQRCodeFinish);
            encoder.Encode<bool>("PromptLateTasks", PromptLateTasks);
        }

        public override void Decode(Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            EnforceSingleClockin = decoder.Decode<bool>("EnforceSingleClockin", false);
            SortType = decoder.Decode<TaskListSortType>("SortType", TaskListSortType.Geocode);
            SortThrottleSeconds = decoder.Decode<int>("SortThrottleSeconds", 0);
            FilterByLabelIds = decoder.Decode<ISet<Guid>>("FilterByLabelIds", new HashSet<Guid>());
            FilterByStatusIds = decoder.Decode<ISet<Guid>>("FilterByStatusIds", new HashSet<Guid>());
            MeasurementType = decoder.Decode<MeasurementType>("MeasurementType", BusinessLogic.Questions.MeasurementType.Metric);
            AllowTaskGrouping = decoder.Decode<bool>("AllowTaskGrouping");
            TaskCreationTemplates = decoder.Decode<IList<TaskListQuestionTaskCreationTemplate>>("CreationTemplates", new List<TaskListQuestionTaskCreationTemplate>());
            ShowGeneralNotesForIncompleteTasks = decoder.Decode<bool>("ShowGeneralNotesForIncompleteTasks");
            RefreshPeriodSeconds = decoder.Decode<int>("RefreshPeriodSeconds");
            StalePeriodSeconds = decoder.Decode<int>("StalePeriodSeconds");
            Listen = decoder.Decode<bool>("Listen");
            SupportOffline = decoder.Decode<bool>("SupportOffline");
            TasksAddedMessage = decoder.Decode<string>("TasksAddedMessage");
            TasksRemovedMessage = decoder.Decode<string>("TasksRemovedMessage");
            ShowTaskSites = decoder.Decode<bool>("ShowTaskSites", true);
            ShowTaskDistances = decoder.Decode<bool>("ShowTaskDistances", true);
            ShowStartTimes = decoder.Decode<bool>("ShowStartTimes", true);
            MinimumTaskDurationMessage = decoder.Decode<string>("MinimumTaskDurationMessage", "");
            AllowClaimTasksFromParent = decoder.Decode<bool>("AllowClaimTasksFromParent", false);
            AllowButtonStart = decoder.Decode<bool>("AllowButtonStart", true);
            AllowQRCodeStart = decoder.Decode<bool>("AllowQRCodeStart", false);
            AllowBeaconStart = decoder.Decode<bool>("AllowBeaconStart", false);
            AllowButtonFinish = decoder.Decode<bool>("AllowButtonFinish", true);
            AllowQRCodeFinish = decoder.Decode<bool>("AllowQRCodeFinish", false);
            PromptLateTasks = decoder.Decode<bool>("PromptLateTasks", false);
        }

        public JArray TaskCreationTemplatesToJson()
        {
            JArray result = new JArray();

            TaskCreationTemplates.ForEach(t =>
            {
                JObject destination = new JObject();
                t.ToJson(destination);

                result.Add(destination);
            });

            return result;
        }

        public void TaskCreationTemplatesFromJson(string model)
        {
            TaskCreationTemplatesFromJson(JArray.Parse(model));
        }

        public void TaskCreationTemplatesFromJson(JArray model)
        {
            TaskCreationTemplates = new List<TaskListQuestionTaskCreationTemplate>();

            foreach (var f in model)
            {
                var template = new TaskListQuestionTaskCreationTemplate();
                template.FromJson((JObject)f);

                TaskCreationTemplates.Add(template);
            }
        }

        public override List<System.Collections.Specialized.NameValueCollection> ToNameValuePairs()
        {
            var r = base.ToNameValuePairs();

            var result = r[0];

            result.Add("enforcesingleclockin", EnforceSingleClockin.ToString());
            result.Add("sorttype", SortType.ToString());
            result.Add("sortthrottleseconds", SortThrottleSeconds.ToString());
            result.Add("filterbylabelids", string.Join(",", FilterByLabelIds));
            result.Add("filterbystatusids", string.Join(",", FilterByStatusIds));
            result.Add("measurementtype", MeasurementType.ToString());
            result.Add("allowtaskgrouping", AllowTaskGrouping.ToString());
            result.Add("showgeneralnotesforincompletetasks", ShowGeneralNotesForIncompleteTasks.ToString());
            result.Add("showtasksites", ShowTaskSites.ToString());
            result.Add("showtaskdistances", ShowTaskDistances.ToString());
            result.Add("taskcreationtemplates", TaskCreationTemplatesToJson().ToString());
            result.Add("showstarttimes", ShowStartTimes.ToString());
            result.Add("minimumtaskdurationmessage", MinimumTaskDurationMessage);
            result.Add("allowclaimtasksfromparent", AllowClaimTasksFromParent.ToString());
            result.Add("allowbuttonstart", AllowButtonStart.ToString());
            result.Add("allowqrcodestart", AllowQRCodeStart.ToString());
            result.Add("allowbeaconstart", AllowBeaconStart.ToString());
            result.Add("allowbuttonfinish", AllowButtonFinish.ToString());
            result.Add("allowqrcodefinish", AllowQRCodeFinish.ToString());
            result.Add("promptlatetasks", PromptLateTasks.ToString());
            return r;
        }

        public override void FromNameValuePairs(System.Collections.Specialized.NameValueCollection pairs)
        {
            base.FromNameValuePairs(pairs);

            foreach (var name in pairs.AllKeys)
            {
                string n = name.ToLower();
                bool val = false;

                switch (n)
                {
                    case "enforcesingleclockin":
                        if (bool.TryParse(pairs[name], out val) == true)
                        {
                            this.EnforceSingleClockin = val;
                        }
                        break;
                    case "sorttype":
                        SortType = (TaskListSortType)Enum.Parse(typeof(TaskListSortType), pairs[name]);
                        break;
                    case "sortthrottleseconds":
                        SortThrottleSeconds = int.Parse(pairs[name]);
                        break;
                    case "filterbylabelids":
                        if (string.IsNullOrEmpty(pairs[name]) == false)
                        {
                            FilterByLabelIds = new HashSet<Guid>(pairs[name].Split(',').Select(s => Guid.Parse(s)));
                        }
                        break;
                    case "filterbystatusids":
                        if (string.IsNullOrEmpty(pairs[name]) == false)
                        {
                            FilterByStatusIds = new HashSet<Guid>(pairs[name].Split(',').Select(s => Guid.Parse(s)));
                        }
                        break;
                    case "measurementtype":
                        MeasurementType = (MeasurementType)Enum.Parse(typeof(MeasurementType), pairs[name]);
                        break;
                    case "allowtaskgrouping":
                        if (bool.TryParse(pairs[name], out val) == true)
                        {
                            this.AllowTaskGrouping = val;
                        }
                        break;
                    case "showgeneralnotesforincompletetasks":
                        if (bool.TryParse(pairs[name], out val) == true)
                        {
                            this.ShowGeneralNotesForIncompleteTasks = val;
                        }
                        break;
                    case "taskcreationtemplates":
                        TaskCreationTemplatesFromJson(pairs[name]);
                        break;
                    case "showtasksites":
                        if (bool.TryParse(pairs[name], out val) == true)
                        {
                            this.ShowTaskSites = val;
                        }
                        break;
                    case "showtaskdistances":
                        if (bool.TryParse(pairs[name], out val) == true)
                        {
                            this.ShowTaskDistances = val;
                        }
                        break;
                    case "showstarttimes":
                        if (bool.TryParse(pairs[name], out val) == true)
                        {
                            this.ShowStartTimes = val;
                        }
                        break;
                    case "minimumtaskdurationmessage":
                        this.MinimumTaskDurationMessage = pairs[name];
                        break;
                    case "allowclaimtasksfromparent":
                        if (bool.TryParse(pairs[name], out val) == true)
                        {
                            this.AllowClaimTasksFromParent = val;
                        }
                        break;
                    case "allowbuttonstart":
                        if (bool.TryParse(pairs[name], out val) == true)
                        {
                            AllowButtonStart = val;
                        }
                        break;
                    case "allowqrcodestart":
                        if (bool.TryParse(pairs[name], out val) == true)
                        {
                            AllowQRCodeStart = val;
                        }
                        break;
                    case "allowbeaconstart":
                        if (bool.TryParse(pairs[name], out val) == true)
                        {
                            AllowBeaconStart = val;
                        }
                        break;
                    case "allowbuttonfinish":
                        if (bool.TryParse(pairs[name], out val) == true)
                        {
                            AllowButtonFinish = val;
                        }
                        break;
                    case "allowqrcodefinish":
                        if (bool.TryParse(pairs[name], out val) == true)
                        {
                            AllowQRCodeFinish = val;
                        }
                        break;
                    case "promptlatetasks":
                        if (bool.TryParse(pairs[name], out val) == true)
                        {
                            PromptLateTasks = val;
                        }
                        break;
                }
            }
        }
    }

    public class TaskListQuestionExecutionHandler : IExecutionMethodHandler, IServerSideQueuedExecutionHandler
    {
        protected ITaskRepository _taskRepo;
        protected IScheduledTaskRepository _scheduledTaskRepo;
        protected ITaskWorkLogRepository _logRepo;
        protected IProjectJobTaskWorkingDocumentRepository _taskWorkingDocumentRepo;
        protected IPublishingGroupResourceRepository _pgrRepo;
        protected IWorkingDocumentFactory _wdFactory;
        protected IMembershipRepository _memberRepo;
        protected IMediaRepository _mediaRepo;
        protected IWorkingDocumentRepository _wdRepo;
        protected IWorkingDocumentStateManager _wdStateManager;
        protected IHierarchyRepository _hierarchyRepo;
        protected IRosterRepository _rosterRepo;
        protected ITaskTypeRepository _taskTypeRepo;
        protected IProductCatalogRepository _catRepo;
        protected IOrderRepository _orderRepo;
        protected IFacilityStructureRepository _facRepo;
        private readonly IDateTimeProvider _dateTimeProvider;

        public TaskListQuestionExecutionHandler(ITaskRepository taskRepo,
            IScheduledTaskRepository scheduledTaskRepo,
            ITaskWorkLogRepository logRepo,
            IProjectJobTaskWorkingDocumentRepository taskWorkingDocumentRepo,
            IPublishingGroupResourceRepository pgrRepo,
            IMembershipRepository memberRepo,
            IWorkingDocumentFactory wdFactory,
            IMediaRepository mediaRepo,
            IWorkingDocumentRepository wdRepo,
            IWorkingDocumentStateManager wdStateManager,
            IHierarchyRepository hierarchyRepo,
            IRosterRepository rosterRepo,
            ITaskTypeRepository taskTypeRepo,
            IProductCatalogRepository catRepo,
            IOrderRepository orderRepo,
            IFacilityStructureRepository facRepo,
            IDateTimeProvider dateTimeProvider)
        {
            _taskRepo = taskRepo;
            _scheduledTaskRepo = scheduledTaskRepo;
            _logRepo = logRepo;
            _memberRepo = memberRepo;
            _taskWorkingDocumentRepo = taskWorkingDocumentRepo;
            _pgrRepo = pgrRepo;
            _mediaRepo = mediaRepo;
            _wdRepo = wdRepo;
            _wdStateManager = wdStateManager;
            _hierarchyRepo = hierarchyRepo;
            _rosterRepo = rosterRepo;
            _taskTypeRepo = taskTypeRepo;
            _catRepo = catRepo;
            _orderRepo = orderRepo;
            _facRepo = facRepo;
            _dateTimeProvider = dateTimeProvider;

            _wdFactory = wdFactory;
        }

        public string Name
        {
            get
            {
                return "TaskListManagement";
            }
        }

        public Newtonsoft.Json.Linq.JToken Execute(string method, Newtonsoft.Json.Linq.JToken parameters, IExecutionMethodHandlerContext context)
        {
            switch (method)
            {
                case "GetTasks":
                    return GetTasks(parameters);
                case "GetTask":
                    return GetTask(parameters);
                case "NewTask":
                    return NewTask(parameters);
                case "ToggleClockinClockout":
                    return ToggleClockinClockout(parameters);
                case "ClockinAndClockout":
                    return ToggleClockinClockout(parameters, true);
                case "Finish":
                    return Finish(parameters);
                case "GotoChecklist":
                    return GotoChecklist(parameters);
                case "GetTaskWorklog":
                    return GetTaskClockins(parameters);
                case "GetFolder":
                    return GetFolder(parameters);
                case "CreateGroup":
                    return CreateGroup(parameters);
                case "DeleteGroup":
                    return DeleteGroup(parameters);
                case "GetGroupFinishingDetails":
                    return GetGroupFinishingDetails(parameters);
                case "SaveChanges":
                    return SaveChanges(parameters);
                case "SavePhoto":
                    return SavePhoto(parameters);
                case "RemovePhoto":
                    return RemovePhoto(parameters);
                case "ChangeTaskType":
                    return ChangeTaskType(parameters);
                case "SaveOrder":
                    return SaveOrder(parameters);
                case "DeleteOrder":
                    return DeleteOrder(parameters);
            }

            throw new InvalidOperationException(string.Format("{0} does not exist as a method on the TaskListExecutionHandler", method));
        }

        protected IList<ProjectJobTaskDTO> FilterOutFutureTasks(IList<ProjectJobTaskDTO> tasks)
        {
            IList<ProjectJobTaskDTO> result = new List<ProjectJobTaskDTO>();

            var manager = new ScheduleManagementBase(_rosterRepo, _dateTimeProvider);

            Dictionary<int, DateTime> rosterEnds = new Dictionary<int, DateTime>();

            foreach (var t in tasks)
            {
                if (t.StartTimeUtc.HasValue == false)
                {
                    result.Add(t);
                    continue;
                }

                if (rosterEnds.ContainsKey(t.RosterId) == false)
                {
                    rosterEnds.Add(t.RosterId, manager.GetEndOfCurrentShiftInTimezone(t.RosterId, TimeZoneInfo.Utc));
                }

                // so if its forward of the end of the current roster, we'll filter it out by not adding
                // it to what we return
                if (t.StartTimeUtc.Value <= rosterEnds[t.RosterId])
                {
                    result.Add(t);
                }
            }

            return result;
        }

        protected void FillWithParentTasks(Guid userId, Guid[] labelFilters, Guid[] statusFilters, IList<ProjectJobTaskDTO> tasks)
        {
            var userBuckets = _hierarchyRepo.GetBucketsForUser(userId, null, HierarchyBucketLoadInstructions.None);

            foreach (var bucket in userBuckets)
            {
                if (bucket.ParentId.HasValue == false)
                {
                    continue;
                }

                var parentBucket = _hierarchyRepo.GetById(bucket.ParentId.Value, HierarchyBucketLoadInstructions.None);

                if (parentBucket.UserId.HasValue == false)
                {
                    continue;
                }

                var parentTasks = _taskRepo.GetForUser(parentBucket.UserId.Value, true, false,
                    TaskStatusFilter.Approved |
                    TaskStatusFilter.ApprovedContinued |
                    TaskStatusFilter.Paused |
                    TaskStatusFilter.Started,
                    FullTaskLoadInstructions(), null, labelFilters, statusFilters);

                parentTasks.ForEach(t => tasks.Add(t));
            }
        }

        protected JToken GetTasks(Guid userId, Guid[] labelFilters, Guid[] statusFilters, Guid[] taskId, bool includeParentTasks, bool idsOnly = false)
        {
            IList<ProjectJobTaskDTO> tasks = null;

            DateTime utcToday = DateTime.Now.Date.ToUniversalTime();

            if (taskId == null || taskId.Length == 0)
            {
                tasks = _taskRepo.GetForUser(userId, true, false,
                    TaskStatusFilter.Approved |
                    TaskStatusFilter.ApprovedContinued |
                    TaskStatusFilter.Paused |
                    TaskStatusFilter.Started,
                    FullTaskLoadInstructions(), null, labelFilters, statusFilters);

                if (includeParentTasks)
                {
                    FillWithParentTasks(userId, labelFilters, statusFilters, tasks);
                }
            }
            else
            {
                var task = _taskRepo.GetById(taskId,
                    FullTaskLoadInstructions());

                tasks = new List<ProjectJobTaskDTO>();
                task.ForEach(t => tasks.Add(t));
            }

            tasks = FilterOutFutureTasks(tasks);

            JObject result = new JObject();
            JArray jTasks = new JArray();
            result["opentasks"] = jTasks;

            if (idsOnly)
            {
                // the request is to only get the ids of the tasks
                foreach (var task in tasks)
                {
                    JObject jTask = new JObject();
                    jTask["id"] = task.Id;

                    jTasks.Add(jTask);
                }

                return result;
            }

            var member = _memberRepo.GetById(userId, MembershipLoadInstructions.None);

            foreach (var task in tasks)
            {
                JObject jTask = new JObject();
                jTask["id"] = task.Id;
                jTask["name"] = task.Name;
                jTask["description"] = task.Description;
                jTask["workinggroupid"] = task.WorkingGroupId;
                jTask["projectjobtaskgroupid"] = task.ProjectJobTaskGroupId;
                jTask["statusid"] = task.StatusId;
                jTask["level"] = task.Level;
                jTask["sortorder"] = task.SortOrder;
                if (task.Asset != null)
                {
                    var jAsset = new JObject();
                    jAsset["id"] = task.Asset.Id;
                    jAsset["name"] = task.Asset.Name;
                    jAsset["assettypeid"] = task.Asset.AssetTypeId;

                    jTask["asset"] = jAsset;
                }
                jTask["notifyuser"] = task.NotifyUser;
                jTask["requiresclaiming"] = false;
                if (includeParentTasks && task.OwnerId != userId)
                {
                    jTask["requiresclaiming"] = true;
                }

                // regarding the task types we have to do things a little differently
                // from the way things are stored in the db as there is allot of code referencing
                // the tasktypeid (documentation, images etc). So to keep things consistent from a UI
                // perspective between saves we make sure the tasktypeid stay's the same by having it
                // reference the original task type id. Then we have another property that can be used by
                // code that knows things can change.
                jTask["tasktypeid"] = task.OriginalTaskTypeId;
                jTask["subtasktypeid"] = task.TaskTypeId;

                jTask["starttime"] = null;
                if (task.StartTimeUtc.HasValue == true)
                {
                    jTask["starttime"] = task.StartTimeUtc.Value;
                }
                jTask["lateafter"] = null;
                if (task.LateAfterUtc.HasValue == true)
                {
                    jTask["lateafter"] = task.LateAfterUtc.Value;
                }

                jTask["minimumdurationleft"] = null;
                if (task.MinimumDuration.HasValue)
                {
                    jTask["minimumdurationleft"] = task.MinimumDuration.Value - (task.ActualDuration.HasValue ? task.ActualDuration.Value : 0);
                }

                AddCalculatedFields(task, jTask);

                jTasks.Add(jTask);
            }

            var types = _taskTypeRepo.GetAll(ProjectJobTaskTypeLoadInstructions.None);
            JObject jTaskTypes = new JObject();
            foreach (var tt in from type in tasks group type by type.ProjectJobTaskType_.Id into g select new { key = g.Key, items = g })
            {
                JObject jtt = new JObject();
                AddTaskType(tt.items.First().ProjectJobTaskType_, jtt, types);

                jTaskTypes[tt.key.ToString()] = jtt;
            }
            result["tasktypes"] = jTaskTypes;

            JArray jLabels = new JArray();
            foreach (var label in (from t in tasks from l in t.ProjectJobTaskLabels group l by l.LabelId into g select new { key = g.Key, items = g }))
            {
                JObject jl = new JObject();
                jl["id"] = label.key;
                jl["name"] = label.items.First().Label.Name;
                jl["forecolor"] = label.items.First().Label.Forecolor;
                jl["backcolor"] = label.items.First().Label.Backcolor;

                jLabels.Add(jl);
            }
            result["labels"] = jLabels;

            JArray jCatalogs = new JArray();
            Dictionary<int, JObject> catalogCache = new Dictionary<int, JObject>();
            foreach (var site in (from t in tasks where t.SiteId.HasValue == true select t.SiteId.Value).Distinct())
            {
                var cat = new List<ProductCatalogDTO>();
                var rules = new List<ProductCatalogFacilityStructureRuleDTO>();
                _catRepo.GetCatalogsForSite(site, cat, rules);

                if (cat == null || cat.Count == 0)
                {
                    continue;
                }

                foreach (var c in cat)
                {
                    if (c.Excluded == true)
                    {
                        continue;
                    }

                    if (c.ProductCatalogItems.Count == 0)
                    {
                        // no products in the catalog, can't order from it so no use sending it down
                        continue;
                    }

                    JObject jc = null;
                    if (catalogCache.TryGetValue(c.Id, out jc) == false)
                    {
                        jc = new JObject();
                        jCatalogs.Add(jc);

                        jc["id"] = c.Id;
                        jc["name"] = c.Name;
                        jc["stockcountwhenordering"] = c.StockCountWhenOrdering;
                        jc["showpricingwhenordering"] = c.ShowPricingWhenOrdering;
                        jc["sites"] = new JArray();

                        JArray jItems = new JArray();
                        foreach (var item in c.ProductCatalogItems)
                        {
                            JObject ji = new JObject();
                            ji["id"] = item.ProductId;
                            ji["name"] = item.Product.Name;
                            ji["mediaid"] = item.Product.MediaId;
                            ji["minstocklevel"] = item.MinStockLevel;
                            ji["price"] = item.Price;
                            if (item.Price.HasValue == false)
                            {
                                ji["price"] = item.Product.Price;
                            }

                            jItems.Add(ji);
                        }
                        jc["items"] = jItems;

                        catalogCache.Add(c.Id, jc);
                    }

                    // we map sites into the catalogs so that we don't repeat a catalogs
                    // data for each site. The UI can look up which catalogs map to a site
                    // through the below mapping
                    ((JArray)jc["sites"]).Add(site);
                }
            }
            result["catalogs"] = jCatalogs;

            var last = _logRepo.GetLast(userId, ProjectJobTaskWorkLogLoadInstructions.Task | ProjectJobTaskWorkLogLoadInstructions.TaskSite);
            result["lastworklog"] = null;
            if (last != null && last.ProjectJobTask.SiteId.HasValue == true &&
                last.DateCreated.Subtract(DateTime.UtcNow).TotalHours <= 8) // hard coding 8 hours as the time between shifts at the moment
            {
                if (last.ProjectJobTask.Site.Latitude.HasValue == true && last.ProjectJobTask.Site.Longitude.HasValue == true)
                {
                    JObject coords = new JObject();

                    JObject lastLog = new JObject();
                    lastLog["latitude"] = last.ProjectJobTask.Site.Latitude;
                    lastLog["longitude"] = last.ProjectJobTask.Site.Longitude;

                    coords["coords"] = lastLog;
                    result["lastworklog"] = coords;
                }
            }

            return result;
        }

        protected JToken GetTasks(JToken parameters)
        {
            string userIdString = parameters["userid"].ToString();

            Guid userId = Guid.Parse(userIdString);
            DateTime date = DateTime.Now.Date; // we get the time in local time, the repo can worry about conversion to UTC as the day is also important, so code below us can convert to UTC when it needs to

            var labels = parameters["filterbylabels"].Select(l => Guid.Parse(l.ToString())).ToArray();
            var statuses = parameters["filterbystatusses"].Select(s => Guid.Parse(s.ToString())).ToArray();

            var includeParentsTasks = bool.Parse(parameters["includeparentstasks"].ToString());

            bool idsOnly = false;

            if (parameters["onlyids"] != null)
            {
                idsOnly = true;
            }

            List<Guid> tasks = new List<Guid>();
            if (parameters["tasks"] != null)
            {
                JArray ts = (JArray)parameters["tasks"];
                foreach (var t in ts)
                {
                    tasks.Add(Guid.Parse(t.ToString()));
                }
            }

            var result = GetTasks(userId, labels, statuses, tasks.ToArray(), includeParentsTasks, idsOnly);

            return result;
        }

        protected void AddTaskType(ProjectJobTaskTypeDTO taskType, JToken result, IList<ProjectJobTaskTypeDTO> types)
        {
            result["id"] = taskType.Id;

            JObject resources = new JObject();
            foreach (var p in taskType.ProjectJobTaskTypePublishingGroupResources)
            {
                JObject r = new JObject();
                r["publishedresourceid"] = p.PublishingGroupResourceId;
                r["name"] = p.PublishingGroupResource.Name;
                r["mediaid"] = p.PublishingGroupResource.PictureId;
                r["ismandatory"] = p.Mandatory;
                r["allowmultiple"] = p.AllowMultiple;
                r["preventfinishing"] = p.PreventFinishing;

                resources[p.PublishingGroupResourceId.ToString()] = r;
            }
            result["publishedresources"] = resources;

            JArray documentation = new JArray();
            foreach (var d in taskType.ProjectJobTaskTypeMediaFolders)
            {
                JObject doc = new JObject();
                doc["folderid"] = d.MediaFolderId;
                doc["text"] = string.IsNullOrEmpty(d.Text) == true ? d.MediaFolder.Text : d.Text;

                documentation.Add(doc);
            }
            result["documentation"] = documentation;

            JArray finishStatuses = new JArray();
            foreach (var f in taskType.ProjectJobTaskTypeFinishedStatuses.Where(fs => fs.ProjectJobFinishedStatu.Stage == 0))
            {
                JObject stat = new JObject();
                stat["id"] = f.Id;
                stat["finishstatusid"] = f.ProjectJobFinishedStatusId;
                stat["text"] = f.ProjectJobFinishedStatu.Text;
                stat["requiresextranotes"] = f.ProjectJobFinishedStatu.RequiresExtraNotes;

                finishStatuses.Add(stat);
            }
            result["finishedstatuses"] = finishStatuses;

            JArray pausedStatuses = new JArray();
            foreach (var f in taskType.ProjectJobTaskTypeFinishedStatuses.Where(fs => fs.ProjectJobFinishedStatu.Stage == 1))
            {
                JObject stat = new JObject();
                stat["id"] = f.Id;
                stat["pausestatusid"] = f.ProjectJobFinishedStatusId;
                stat["text"] = f.ProjectJobFinishedStatu.Text;
                stat["requiresextranotes"] = f.ProjectJobFinishedStatu.RequiresExtraNotes;

                pausedStatuses.Add(stat);
            }
            result["pausedstatuses"] = pausedStatuses;

            JArray statuses = new JArray();
            foreach (var s in taskType.ProjectJobTaskTypeStatuses)
            {
                JObject stat = new JObject();
                stat["id"] = s.StatusId;
                stat["text"] = s.ProjectJobStatu.Text;
                stat["referencenumbermode"] = s.ReferenceNumberMode;

                statuses.Add(stat);
            }
            result["statuses"] = statuses;

            JArray relationships = new JArray();
            foreach (var r in taskType.ProjectJobTaskTypeSourceRelationships)
            {
                JObject rel = new JObject();
                rel["id"] = r.ProjectJobTaskTypeDestinationId;
                rel["name"] = (from t in types where t.Id.Equals(r.ProjectJobTaskTypeDestinationId) select t.Name).First();

                relationships.Add(rel);
            }
            result["relationships"] = relationships;

            JArray catalogs = new JArray();
            if (string.IsNullOrEmpty(taskType.ProductCatalogs) == false)
            {
                foreach (var c in taskType.ProductCatalogs.Split(','))
                {
                    catalogs.Add(c.Trim());
                }
            }
            result["catalogs"] = catalogs;


            result["autostart1stchecklist"] = taskType.AutoStart1stChecklist;
            result["autofinishtaskonlastchecklist"] = taskType.AutoFinishAfterLastChecklist;
            result["autofinishafterstart"] = taskType.AutoFinishAfterStart;
        }

        protected void AddCalculatedFields(ProjectJobTaskDTO task, JToken result)
        {
            // we are going to keep things simple for the client making the request, we'll work out if the task is active based on the clock in/out entries and will set an easy
            // to get at property on the task object we send back.
            var activeLogs = (from w in task.ProjectJobTaskWorkLogs where w.DateCompleted.HasValue == false select w);

            result["status"] = "new";

            result["userinterfacetype"] = task.ProjectJobTaskType_.UserInterfaceType;
            result["mediaid"] = task.ProjectJobTaskType_.MediaId;
            result["photosupport"] = task.ProjectJobTaskType_.PhotoSupport;

            if (activeLogs.Count() == 0)
            {
                if (task.ProjectJobTaskWorkLogs.Count > 0)
                {
                    result["status"] = "paused";
                }
            }
            else
            {
                result["status"] = "active";
            }

            if (task.SiteId.HasValue == true)
            {
                JObject site = new JObject();

                site["id"] = task.SiteId.Value.ToString();
                site["name"] = task.Site.Name;
                site["latitude"] = task.SiteLatitude;
                site["longitude"] = task.SiteLongitude;

                result["site"] = site;
            }
            else
            {
                result["site"] = null;
            }

            JArray labels = new JArray();
            foreach (var label in task.ProjectJobTaskLabels)
            {
                JObject l = new JObject();
                l["id"] = label.LabelId;

                labels.Add(l);
            }
            result["labels"] = labels;

            JArray activities = new JArray();
            foreach (var activity in task.ProjectJobTaskWorkloadingActivities)
            {
                JObject a = new JObject();
                a["workloadingstandardid"] = activity.WorkLoadingStandardId;
                a["sitefeatureid"] = activity.SiteFeatureId;
                a["sitefeature"] = activity.SiteFeature.Name;
                a["area"] = activity.SiteFeature.SiteArea.Name;
                a["text"] = activity.Text;
                a["completed"] = activity.Completed;

                activities.Add(a);
            }
            result["activities"] = activities;

            if (task.ProjectJobTaskType_ != null)
            {
                // we are also going to simplify the resources that dangle off the task type side of things
                // by creating an easier to reference collection and removing the task type altogether as it
                // contains a whole load of stuff that a client isn't interested in.
                JArray resources = new JArray();
                foreach (var p in task.ProjectJobTaskType_.ProjectJobTaskTypePublishingGroupResources)
                {
                    JObject r = new JObject();
                    r["publishedresourceid"] = p.PublishingGroupResourceId;
                    bool allowfinish;
                    IEnumerable<ProjectJobTaskWorkingDocumentDTO> docs;
                    IEnumerable<ProjectJobTaskWorkingDocumentDTO> completedDocs;
                    CalculateAllowFinish(task, p, out allowfinish, out docs, out completedDocs);

                    JArray wds = new JArray();
                    foreach (var wd in docs)
                    {
                        wds.Add(wd.WorkingDocumentId);
                    }

                    r["workingdocuments"] = wds;

                    r["allowfinish"] = allowfinish;
                    r["workingdocumentcount"] = docs.Count();
                    r["completedworkingdocumentcount"] = completedDocs.Count();

                    resources.Add(r);
                }

                result["publishedresources"] = resources;
            }

            JArray photos = new JArray();
            foreach (var p in task.ProjectJobTaskMedias)
            {
                JObject photo = new JObject();
                photo["mediaid"] = p.MediaId.ToString();

                photos.Add(photo);
            }
            result["photos"] = photos;

            JArray orders = new JArray();
            foreach (var o in task.Orders)
            {
                JObject order = new JObject();
                order["id"] = o.Id.ToString();
                order["__isnew"] = false;
                order["catalogid"] = o.ProductCatalogId.ToString();
                order["datecreated"] = o.DateCreated.ToString();
                order["deliverydate"] = null;
                order["totalprice"] = o.TotalPrice;
                if (o.ExpectedDeliveryDate.HasValue)
                {
                    order["deliverydate"] = o.ExpectedDeliveryDate.Value.ToString();
                }

                JArray items = new JArray();
                foreach (var i in o.OrderItems)
                {
                    JObject item = new JObject();
                    item["__isnew"] = false;
                    item["id"] = i.Id.ToString();
                    item["productid"] = i.ProductId.ToString();
                    item["quantity"] = i.Quantity;
                    item["stock"] = i.Stock;
                    item["minstocklevel"] = i.MinStockLevel;
                    item["price"] = i.Price;

                    items.Add(item);
                }
                order["items"] = items;

                orders.Add(order);
            }
            result["orders"] = orders;

            result["projectjobtasktype"] = null;
            result["projectjobtaskworkingdocuments"] = null;
        }

        private static void CalculateAllowFinish(ProjectJobTaskDTO task, ProjectJobTaskTypePublishingGroupResourceDTO p, out bool allowfinish, out IEnumerable<ProjectJobTaskWorkingDocumentDTO> docs, out IEnumerable<ProjectJobTaskWorkingDocumentDTO> completedDocs)
        {
            allowfinish = false;

            docs = (from w in task.ProjectJobTaskWorkingDocuments where w.PublishingGroupResourceId == p.PublishingGroupResourceId select w);
            completedDocs = (from w in docs where w.WorkingDocument.Status == (int)WorkingDocumentStatus.Completed select w);
            if (p.Mandatory == false)
            {
                // its not mandatory so it doesn't block finishing regardless of its state
                allowfinish = true;
            }
            else
            {
                if (p.PreventFinishing == true)
                {
                    // if we are preventing docs from finishing until the task is finished, then if the docs are started we
                    // need to allow things to be finished (as they are never going to get completed).
                    allowfinish = docs.Count() > 0;
                }
                else
                {
                    // we aren't preventing finishing, so finished docs denote wether or not the task can be finished.
                    allowfinish = completedDocs.Count() > 0;
                }
            }
        }

        protected JToken ToJson(object obj)
        {
            var settings = new JsonSerializerSettings();
            LowercaseContractResolver resolver = new LowercaseContractResolver();
            settings.ContractResolver = resolver;
            settings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;

            JToken result = JToken.Parse(JsonConvert.SerializeObject(obj, settings));

            return result;
        }

        protected ProjectJobTaskLoadInstructions FullTaskLoadInstructions()
        {
            return ProjectJobTaskLoadInstructions.WorkLog |
                ProjectJobTaskLoadInstructions.TaskType |
                ProjectJobTaskLoadInstructions.TaskTypeDocumentation |
                ProjectJobTaskLoadInstructions.TaskTypeDocumentationFolder |
                ProjectJobTaskLoadInstructions.TaskTypePublishedResources |
                ProjectJobTaskLoadInstructions.TaskTypeStatuses |
                ProjectJobTaskLoadInstructions.TaskTypeRelationships |
                ProjectJobTaskLoadInstructions.WorkingDocuments |
                ProjectJobTaskLoadInstructions.Users |
                ProjectJobTaskLoadInstructions.WorkloadingActivities |
                ProjectJobTaskLoadInstructions.TaskTypeFinishedStatuses |
                ProjectJobTaskLoadInstructions.AllTranslated |
                ProjectJobTaskLoadInstructions.Labels |
                ProjectJobTaskLoadInstructions.Media |
                ProjectJobTaskLoadInstructions.Order |
                ProjectJobTaskLoadInstructions.Asset;
        }

        protected JToken GetTask(JToken parameters)
        {
            string userIdString = parameters["userid"].ToString();
            string taskIdString = parameters["taskid"].ToString();

            Guid userId = Guid.Parse(userIdString);
            Guid taskId = Guid.Parse(taskIdString);

            var result = GetTasks(userId, null, null, new Guid[] { taskId }, false, false);

            return result;
        }

        protected JToken NewTask(JToken parameters)
        {
            Guid? taskId = null;
            bool taskAlreadyExists = false;
            if (parameters["taskid"] != null)
            {
                taskId = Guid.Parse(parameters["taskid"].ToString());
                taskAlreadyExists = _taskRepo.GetById(taskId.Value, ProjectJobTaskLoadInstructions.None) != null;
            }

            if (!taskAlreadyExists)
            {
                string userIdString = parameters["userid"].ToString();
                Guid userId = Guid.Parse(userIdString);
                Guid taskTypeId = Guid.Parse(parameters["template"]["tasktypeid"].ToString());
                string taskName = parameters["template"]["text"].ToString();
                string taskDescription = null;
                int? defaultToRosterId = null;
                Guid? siteId = null;
                if (parameters["template"]["siteid"] != null && parameters["template"]["siteid"].Type != JTokenType.Null)
                    siteId = Guid.Parse(parameters["template"]["siteid"].ToString());

                if (parameters["template"]["defaulttoroster"] != null)
                {
                    if (string.IsNullOrEmpty(parameters["template"]["defaulttoroster"].ToString()) == false)
                    {
                        defaultToRosterId = int.Parse(parameters["template"]["defaulttoroster"].ToString());
                    }
                }

                if (parameters["template"]["basicdetails"] != null)
                {
                    if (parameters["template"]["basicdetails"]["name"] != null)
                    {
                        taskName = parameters["template"]["basicdetails"]["name"].ToString();
                    }
                    if (parameters["template"]["basicdetails"]["description"] != null)
                    {
                        taskDescription = parameters["template"]["basicdetails"]["description"].ToString();
                    }
                }

                bool clockin = bool.Parse(parameters["clockin"].ToString());

                var buckets = _hierarchyRepo.GetBucketsForUser(userId, null, HierarchyBucketLoadInstructions.None);

                JObject result = new JObject();

                if (buckets.Count == 0 && defaultToRosterId.HasValue == false)
                {
                    result["Success"] = false;
                    result["Message"] = "User is not a part of an organisation hierarchy";

                    return result;
                }

                int roleId = -1;
                int? bucketId = null;
                int rosterId = -1;

                var primary = from b in buckets where b.IsPrimary == true select b;
                if (primary.Count() > 0)
                {
                    bucketId = primary.First().Id;
                    roleId = primary.First().RoleId;

                    var rosters = _rosterRepo.GetForHierarchies(new int[] { primary.First().HierarchyId });

                    if (rosters.Count() > 0)
                    {
                        rosterId = rosters[0].Id;
                    }
                }
                else if (buckets.Count() > 0)
                {
                    bucketId = buckets.First().Id;
                    roleId = buckets.First().RoleId;

                    var rosters = _rosterRepo.GetForHierarchies(new int[] { buckets.First().HierarchyId });

                    if (rosters.Count() > 0)
                    {
                        rosterId = rosters[0].Id;
                    }
                }

                if (rosterId == -1)
                {
                    if (defaultToRosterId.HasValue == false)
                    {
                        result["Success"] = false;
                        result["Message"] = "User is not a part of an organisation hierarchy and there isn't a default roster defined.";

                        return result;
                    }

                    // fall back to the default roster
                    rosterId = defaultToRosterId.Value;
                }

                using (TransactionScope scope = new TransactionScope())
                {
                    try
                    {
                        var task = _taskRepo.New(taskId, taskName, taskDescription, userId, rosterId, taskTypeId, bucketId, _memberRepo, null, siteId, ProjectJobTaskStatus.Approved);

                        parameters["taskid"] = task.Id;

                        if (parameters["enforcesingleclockin"] == null)
                        {
                            parameters["enforcesingleclockin"] = false;
                        }

                        if (clockin == true)
                        {
                            ToggleClockinClockout(parameters);
                        }

                        scope.Complete();
                    }
                    catch (ORMQueryExecutionException ex)
                    {
                        SqlException primaryKeyViolationEx = ex.InnerException as SqlException;
                        if (primaryKeyViolationEx?.Number == 2627)
                        {
                            // We have tried to insert the Task multiple times, likely the client is timing out and retrying, we can swallow this
                            // and let the GetTasks below finish the request
                        }
                        else
                        {
                            throw ex;
                        }
                    }
                }
            }
            return GetTask(parameters);
        }

        /// <summary>
        /// Gets the time the action occurred. If there is a client timestamp attached this is used and converted to UTC.
        /// If not, the DateTime.UtcNow is returned.
        /// </summary>
        /// <param name="parameters">JToken containing parameters from the client</param>
        /// <param name="userId">Id of user that should be used for context</param>
        /// <returns>Either UTC now or client timestamp in UTC</returns>
        protected DateTime GetActionTime(JToken parameters, Guid userId)
        {
            DateTime utcNow = DateTime.UtcNow;

            if (parameters["clientDateTimeStampUtc"] != null)
            {
                utcNow = DateTime.ParseExact(parameters["clientDateTimeStampUtc"].ToString(), "dd-MM-yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                utcNow = DateTime.SpecifyKind(utcNow, DateTimeKind.Utc);
            }

            return utcNow;
        }

        protected void SetWorkloadingActivitiesState(JToken parameters, ProjectJobTaskDTO task)
        {
            if (parameters["activities"] == null)
            {
                return;
            }

            JArray activities = (JArray)parameters["activities"];

            foreach (JObject activity in activities)
            {
                Guid workingStandardId = Guid.Parse(activity["workloadingstandardid"].ToString());
                Guid siteFeatureId = Guid.Parse(activity["sitefeatureid"].ToString());

                var act = from a in task.ProjectJobTaskWorkloadingActivities
                          where
a.WorkLoadingStandardId == workingStandardId &&
a.SiteFeatureId == siteFeatureId
                          select a;

                string complete = activity["completed"].ToString();

                if (string.IsNullOrEmpty(complete))
                {
                    act.First().Completed = null;
                }
                else
                {
                    act.First().Completed = bool.Parse(complete);
                }
            }
        }

        protected JToken ToggleClockinClockout(JToken parameters, bool forceClockout = false, bool doGroupClockins = true)
        {
            string taskIdString = parameters["taskid"].ToString();
            string userIdString = parameters["userid"].ToString();
            string enforceString = parameters["enforcesingleclockin"].ToString();
            string attemptClaimString = null;
            if (parameters["attemptclaim"] != null)
            {
                attemptClaimString = parameters["attemptclaim"].ToString();
            }

            string notes = null;
            if (parameters["notes"] != null)
            {
                notes = parameters["notes"].ToString();
            }

            var locationCandidate = parameters["location"]; // this could be 1 of 3 values, null, a location or a location error

            JToken location = null;
            JToken locationError = null;

            if (locationCandidate == null || locationCandidate.HasValues == false)
            {
                location = locationCandidate;
            }
            else if (locationCandidate["coords"] != null)
            {
                location = locationCandidate;
            }
            else
            {
                locationError = locationCandidate;
            }

            Guid taskId = Guid.Parse(taskIdString);
            Guid userId = Guid.Parse(userIdString);
            bool attemptClaim = false;

            if (string.IsNullOrEmpty(attemptClaimString) == false)
            {
                attemptClaim = bool.Parse(attemptClaimString);
            }

            bool enforceSingleClockin = bool.Parse(enforceString);

            var log = _logRepo.GetCurrentActive(taskId, userId, RepositoryInterfaces.Enums.ProjectJobTaskWorkLogLoadInstructions.None);
            var task = _taskRepo.GetById(taskId, ProjectJobTaskLoadInstructions.Users | ProjectJobTaskLoadInstructions.WorkloadingActivities);

            // variable to hold wether or not changes that take place here are going to require the client
            // to update its list of tasks.
            bool listRefreshRequired = false;

            ProjectJobTaskStatus status = ProjectJobTaskStatus.Paused;

            DateTime utcNow = GetActionTime(parameters, userId);

            Action<ProjectJobTaskWorkLogDTO, bool> setLocation = (dto, clockin) =>
            {
                if (location != null && location.HasValues == false)
                {
                    if (clockin == true)
                    {
                        dto.ClockinLatitude = null;
                        dto.ClockinLongitude = null;
                        dto.ClockinAccuracy = null;
                        dto.ClockinLocationStatus = (int)GeoLocationStatus.Unknown;
                    }
                    else
                    {
                        dto.ClockoutLatitude = null;
                        dto.ClockoutLongitude = null;
                        dto.ClockoutAccuracy = null;
                        dto.ClockoutLocationStatus = (int)GeoLocationStatus.Unknown;
                    }
                }
                else if (location != null)
                {
                    if (clockin == true)
                    {
                        dto.ClockinLatitude = decimal.Parse(location["coords"]["latitude"].ToString());
                        dto.ClockinLongitude = decimal.Parse(location["coords"]["longitude"].ToString());
                        dto.ClockinAccuracy = decimal.Parse(location["coords"]["accuracy"].ToString());
                        dto.ClockinLocationStatus = (int)GeoLocationStatus.Success;

                        if (task.SiteId.HasValue == true && task.Site.Latitude.HasValue == true && task.Site.Longitude.HasValue == true)
                        {
                            // we can work out the distance between the clockin and the site
                            LatLng l = new LatLng(task.Site.Latitude.Value, task.Site.Longitude.Value, 0);
                            dto.ClockinMetresFromSite = l.MetresDistanceFrom(dto.ClockinLatitude.Value, dto.ClockinLongitude.Value);
                        }
                    }
                    else
                    {
                        dto.ClockoutLatitude = decimal.Parse(location["coords"]["latitude"].ToString());
                        dto.ClockoutLongitude = decimal.Parse(location["coords"]["longitude"].ToString());
                        dto.ClockoutAccuracy = decimal.Parse(location["coords"]["accuracy"].ToString());
                        dto.ClockoutLocationStatus = (int)GeoLocationStatus.Success;

                        if (task.SiteId.HasValue == true && task.Site.Latitude.HasValue == true && task.Site.Longitude.HasValue == true)
                        {
                            // we can work out the distance between the clockin and the site
                            LatLng l = new LatLng(task.Site.Latitude.Value, task.Site.Longitude.Value, 0);
                            dto.ClockoutMetresFromSite = l.MetresDistanceFrom(dto.ClockoutLatitude.Value, dto.ClockoutLongitude.Value);
                        }
                    }
                }

                if (locationError != null)
                {
                    if (clockin == true)
                    {
                        dto.ClockinLatitude = null;
                        dto.ClockinLongitude = null;
                        dto.ClockinAccuracy = null;
                        dto.ClockinLocationStatus = int.Parse(locationError["code"].ToString());
                    }
                    else
                    {
                        dto.ClockoutLatitude = null;
                        dto.ClockoutLongitude = null;
                        dto.ClockoutAccuracy = null;
                        dto.ClockoutLocationStatus = int.Parse(locationError["code"].ToString());
                    }
                }
            };

            bool isClockin = log == null;

            if (isClockin == true)
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    if (task.WorkingGroupId.HasValue == true)
                    {
                        if (doGroupClockins == true)
                        {
                            var tasksInGroup = _taskRepo.GetByWorkingGroupId(task.WorkingGroupId.Value, ProjectJobTaskLoadInstructions.None);

                            foreach (var tg in tasksInGroup)
                            {
                                if (tg.Id != task.Id)
                                {
                                    var p = parameters.DeepClone();
                                    p["taskid"] = tg.Id;
                                    ToggleClockinClockout(p, false, false);
                                }
                            }
                        }
                    }
                    else if (enforceSingleClockin == true)
                    {
                        // we are only allowed to have 1 clockin open at any time for the user in any project, so close all others before we
                        // open a new one.
                        var openForUser = _logRepo.GetCurrentActive(userId, ProjectJobTaskWorkLogLoadInstructions.Task);

                        // we are only interested in work log items that are associated with a task in one of the live statusses
                        foreach (var l in (from o in openForUser
                                           where
                         ProjectJobTaskStatusGroups.LiveStates.Contains((ProjectJobTaskStatus)o.ProjectJobTask.Status) == true
                                           select o))
                        {
                            l.DateCompleted = utcNow;
                            setLocation(l, false);

                            l.ProjectJobTask.Status = (int)ProjectJobTaskStatus.Paused;
                            l.ProjectJobTask.StatusDate = DateTime.UtcNow;

                            _logRepo.Save(l, false, true);

                            listRefreshRequired = true;
                        }
                    }

                    // no open logs for this user, so we create a new one
                    log = _logRepo.New(taskId, userId, utcNow);

                    setLocation(log, true);

                    log.Notes = notes;

                    status = ProjectJobTaskStatus.Started;

                    scope.Complete();
                }

            }

            if (isClockin == false || (isClockin == true && forceClockout == true))
            {
                if (task.WorkingGroupId.HasValue == true)
                {
                    if (doGroupClockins == true)
                    {
                        var tasksInGroup = _taskRepo.GetByWorkingGroupId(task.WorkingGroupId.Value, ProjectJobTaskLoadInstructions.None);

                        foreach (var tg in tasksInGroup)
                        {
                            if (tg.Id != task.Id)
                            {
                                var p = parameters.DeepClone();
                                p["taskid"] = tg.Id;
                                ToggleClockinClockout(p, false, false);
                            }
                        }
                    }
                }

                log.DateCompleted = utcNow;

                setLocation(log, false);

                if (parameters["notes"] != null)
                {
                    log.Notes = parameters["notes"].ToString();
                }
            }

            // just in case the user has clocked in/out after things have been finished (possible
            // we are close to end of day and end of day process has been run slightly early say. 
            // in this scenario, we don't want to change the status of the task
            if (task.Status == (int)ProjectJobTaskStatus.FinishedByManagement)
            {
                status = ProjectJobTaskStatus.FinishedByManagement;
            }

            if (task.Status == (int)ProjectJobTaskStatus.FinishedBySystem)
            {
                status = ProjectJobTaskStatus.FinishedBySystem;
            }

            if (task.Status == (int)ProjectJobTaskStatus.FinishedComplete)
            {
                status = ProjectJobTaskStatus.FinishedComplete;
            }

            if (task.Status == (int)ProjectJobTaskStatus.FinishedIncomplete)
            {
                status = ProjectJobTaskStatus.FinishedIncomplete;
            }

            using (TransactionScope scope = new TransactionScope())
            {
                task.Status = (int)status;
                task.StatusDate = utcNow;

                if (attemptClaim && task.OwnerId != userId)
                {
                    var openForUser = _logRepo.GetCurrentActive(task.OwnerId, ProjectJobTaskWorkLogLoadInstructions.None);

                    // we are only interested in work log items that are associated with the current task
                    foreach (var l in (from o in openForUser
                                       where
                                        o.ProjectJobTaskId == task.Id
                                       select o))
                    {
                        l.DateCompleted = utcNow;
                        setLocation(l, false);

                        _logRepo.Save(l, false, false);
                    }

                    task.Owner = null;
                    task.OwnerId = userId;
                }

                SetWorkloadingActivitiesState(parameters, task);
                _taskRepo.Save(task.ProjectJobTaskWorkloadingActivities.ToArray(), false, false);

                _taskRepo.Save(task, false, false);
                _logRepo.Save(log, false, false);

                scope.Complete();
            }

            log = _logRepo.GetById(log.Id, RepositoryInterfaces.Enums.ProjectJobTaskWorkLogLoadInstructions.User);

            var result = ToJson(log);

            result["refreshtasklist"] = listRefreshRequired;

            return result;
        }

        public JToken Finish(JToken parameters, bool doGroupFinish = true)
        {
            bool clockout = false;

            if (parameters["clockout"] != null)
            {
                clockout = bool.Parse(parameters["clockout"].ToString());
            }

            string taskIdString = parameters["taskid"].ToString();
            Guid taskId = Guid.Parse(taskIdString);

            var task = _taskRepo.GetById(taskId,
                    ProjectJobTaskLoadInstructions.WorkingDocuments |
                    ProjectJobTaskLoadInstructions.TaskType |
                    ProjectJobTaskLoadInstructions.WorkloadingActivities |
                    ProjectJobTaskLoadInstructions.TaskTypePublishedResources);

            var taskStatus = (ProjectJobTaskStatus)task.Status;
            if (taskStatus == ProjectJobTaskStatus.FinishedByManagement ||
                taskStatus == ProjectJobTaskStatus.FinishedComplete ||
                taskStatus == ProjectJobTaskStatus.FinishedIncomplete ||
                taskStatus == ProjectJobTaskStatus.FinishedBySystem)
            {
                // its already finished nothing really to do here
                return ToJson(task);
            }

            using (TransactionScope scope = new TransactionScope())
            {
                if (doGroupFinish == true || clockout == true)
                {
                    var toggle = ToggleClockinClockout(parameters, true, true);

                    if (toggle["datecompleted"].ToString() == null)
                    {
                        throw new InvalidOperationException("A task cannot be finished while there are open clockins");
                    }
                }

                string userIdString = parameters["userid"].ToString();

                Guid userId = Guid.Parse(userIdString);

                // even though we loaded the task up above, we reload it here as it could have changed with auto
                // clockout stuff above
                task = _taskRepo.GetById(taskId, ProjectJobTaskLoadInstructions.WorkingDocuments |
                    ProjectJobTaskLoadInstructions.TaskType |
                    ProjectJobTaskLoadInstructions.WorkloadingActivities |
                    ProjectJobTaskLoadInstructions.TaskTypePublishedResources);

                var status = ProjectJobTaskStatus.FinishedIncomplete;
                if (parameters["complete"].Value<bool>() == true)
                {
                    status = ProjectJobTaskStatus.FinishedComplete;
                }

                var mandatories = (from p in task.ProjectJobTaskType.ProjectJobTaskTypePublishingGroupResources where p.Mandatory == true select p);

                if (mandatories.Count() > 0)
                {
                    // there are some resources that are marked as being mandatory, so extra rules to enforce here friends
                    foreach (var m in mandatories)
                    {
                        var wds = from w in task.ProjectJobTaskWorkingDocuments where w.PublishingGroupResourceId == m.PublishingGroupResourceId select w;

                        if (m.PreventFinishing == true)
                        {
                            // all must be started, so having none is not what we want
                            if (wds.Count() == 0)
                            {
                                throw new InvalidOperationException("The task has mandatory workflows that haven't been completed");
                            }
                        }
                        else
                        {
                            // must be at least 1 finished
                            var finished = from w in wds where w.WorkingDocument.Status == (int)WorkingDocumentStatus.Completed select w;
                            if (finished.Count() == 0)
                            {
                                throw new InvalidOperationException("The task has mandatory workflows that haven't been completed");
                            }
                        }
                    }
                }

                IList<ProjectJobTaskDTO> groupTasks = null;

                if (task.WorkingGroupId.HasValue == true)
                {
                    groupTasks = _taskRepo.GetByWorkingGroupId(task.WorkingGroupId.Value, ProjectJobTaskLoadInstructions.TaskType | ProjectJobTaskLoadInstructions.TaskTypePublishedResources | ProjectJobTaskLoadInstructions.WorkingDocuments);

                    foreach (var gt in groupTasks)
                    {
                        if (gt.Id == task.Id)
                        {
                            continue;
                        }

                        foreach (var p in gt.ProjectJobTaskType.ProjectJobTaskTypePublishingGroupResources)
                        {
                            bool allowfinish;
                            IEnumerable<ProjectJobTaskWorkingDocumentDTO> docs;
                            IEnumerable<ProjectJobTaskWorkingDocumentDTO> completedDocs;
                            CalculateAllowFinish(gt, p, out allowfinish, out docs, out completedDocs);

                            if (allowfinish == false)
                            {
                                throw new InvalidOperationException("The task is in a group that has unfinished mandatory checklists");
                            }
                        }
                    }
                }

                var toFinish = (from p in task.ProjectJobTaskType.ProjectJobTaskTypePublishingGroupResources where p.PreventFinishing == true select p);
                if (toFinish.Count() > 0)
                {
                    var wds = from w in task.ProjectJobTaskWorkingDocuments where (from f in toFinish select f.PublishingGroupResourceId).Contains(w.PublishingGroupResourceId) select w;

                    foreach (var w in wds)
                    {
                        // we need to try and finish each of these wds
                        if (w.WorkingDocument.Status == (int)WorkingDocumentStatus.Completed)
                        {
                            // it shouldn't happen, but lets not freak out about this
                            continue;
                        }

                        var wdDTO = _wdRepo.GetById(w.WorkingDocumentId, WorkingDocumentLoadInstructions.Data);
                        var doc = _wdFactory.CreateFrom(wdDTO);

                        if (doc.RunMode == RunMode.Presenting)
                        {
                            // we are only going to do 1 next, if it doesn't
                            // then go to finishing then we are just going to give in
                            var pm = _wdStateManager.AdvanceWorkingDocument(doc, wdDTO, null);

                            if (pm.State == RunMode.Finishing)
                            {
                                _wdStateManager.FinishWorkingDocument(doc, ref wdDTO);
                            }
                        }
                        else if (doc.RunMode == RunMode.Finishing)
                        {
                            _wdStateManager.FinishWorkingDocument(doc, ref wdDTO);
                        }
                    }
                }

                DateTime utcNow = GetActionTime(parameters, userId);

                task.DateCompleted = utcNow;
                if (task.__IsNew || task.DateCreated > task.DateCompleted.Value)
                {
                    task.DateCreated = task.DateCompleted.Value;
                }
                if (parameters["completionnotes"] != null)
                {
                    task.CompletionNotes = parameters["completionnotes"].ToString();
                }
                task.Status = (int)status;
                task.StatusDate = utcNow;

                // see if we need to move the task onto a particular user defined status as a part of finishing
                _taskRepo.SetTaskStatusWhenFinishing(task);

                List<ProjectJobTaskFinishedStatusDTO> statuses = new List<ProjectJobTaskFinishedStatusDTO>();

                if (status == ProjectJobTaskStatus.FinishedIncomplete && parameters["finishedstatuses"] != null)
                {
                    JArray fs = (JArray)parameters["finishedstatuses"];

                    foreach (JObject s in fs)
                    {
                        if (s["selected"] != null && bool.Parse(s["selected"].ToString()) == true)
                        {
                            string notes = null;

                            if (s["notes"] != null)
                            {
                                notes = s["notes"].ToString();
                            }

                            ProjectJobTaskFinishedStatusDTO ts = new ProjectJobTaskFinishedStatusDTO()
                            {
                                __IsNew = true,
                                ProjectJobFinishedStatusId = Guid.Parse(s["finishstatusid"].ToString()),
                                Notes = notes,
                                TaskId = task.Id
                            };

                            _taskRepo.Save(ts, false, false);
                        }
                    }
                }

                SetWorkloadingActivitiesState(parameters, task);
                _taskRepo.Save(task.ProjectJobTaskWorkloadingActivities.ToArray(), false, false);

                _taskRepo.Save(task, true, false);

                if (task.WorkingGroupId.HasValue == true && doGroupFinish == true)
                {
                    JArray groupData = (JArray)parameters["workinggroupdata"];

                    foreach (var gt in groupTasks)
                    {
                        if (gt.Id == taskId)
                        {
                            continue;
                        }

                        JObject jd = null;
                        foreach (JObject data in groupData)
                        {
                            if (data["id"].ToString() == gt.Id.ToString())
                            {
                                jd = data;
                                break;
                            }
                        }

                        var p = parameters.DeepClone();

                        // we manipulate the parameters object we have so that we can
                        // call against ourselves again for each of the tasks in the group.
                        // So let's copy across what's specific for the task
                        if (jd != null)
                        {
                            p["complete"] = jd["complete"];
                            p["completionnotes"] = jd["completionnotes"];
                            p["finishedstatuses"] = jd["finishedstatuses"];
                        }

                        p["taskid"] = gt.Id;

                        Finish(p, false);
                    }
                }

                task = _taskRepo.GetById(taskId, ProjectJobTaskLoadInstructions.WorkingDocuments |
                    ProjectJobTaskLoadInstructions.TaskType |
                    ProjectJobTaskLoadInstructions.TaskTypePublishedResources);

                scope.Complete();

                var result = ToJson(task);

                return result;
            }
        }

        public JToken GotoChecklist(JToken parameters)
        {
            string userIdString = parameters["userid"].ToString();
            Guid userId = Guid.Parse(userIdString);
            Guid siteId = userId;

            string taskIdString = parameters["taskid"].ToString();
            string pgResourceIdString = parameters["publishinggroupresourceid"].ToString();

            Guid taskId = Guid.Parse(taskIdString);
            int pgResourceId = int.Parse(pgResourceIdString);

            var checklists = _taskWorkingDocumentRepo.GetForTaskAndPublishingGroupResource(taskId, pgResourceId, RepositoryInterfaces.Enums.ProjectJobTaskWorkingDocumentLoadInstructions.WorkingDocument);
            var task = _taskRepo.GetById(taskId, ProjectJobTaskLoadInstructions.TaskType | ProjectJobTaskLoadInstructions.TaskTypePublishedResources);

            if (task.SiteId.HasValue == true)
            {
                siteId = task.SiteId.Value;
            }

            var actives = from c in checklists where c.WorkingDocument.Status == (int)WorkingDocumentStatus.Started select c;
            var completed = from c in checklists where c.WorkingDocument.Status == (int)WorkingDocumentStatus.Completed select c;

            var result = new JObject();
            result["allow"] = false;

            // there are completed checklists and we don't allow multiples, then lets not allow multiples
            if (completed.Count() > 0 && (from p in task.ProjectJobTaskType.ProjectJobTaskTypePublishingGroupResources where p.PublishingGroupResourceId == pgResourceId && p.AllowMultiple == true select p).Count() == 0)
            {
                return result;
            }

            Guid wdId = Guid.Empty;

            if (actives.Count() == 0)
            {
                // we need to create a new checklist
                var pgr = _pgrRepo.GetById(pgResourceId, PublishingGroupResourceLoadInstruction.None);

                using (TransactionScope scope = new TransactionScope())
                {
                    Dictionary<string, object> wdArgs = new Dictionary<string, object>();
                    wdArgs.Add("taskid", taskId);

                    var playerModel = _wdFactory.CreateStartAndSave(pgr.PublishingGroupId, pgr.ResourceId, userId, siteId, HrefFacility.EncodeArguments(wdArgs));

                    ProjectJobTaskWorkingDocumentDTO dto = new ProjectJobTaskWorkingDocumentDTO()
                    {
                        ProjectJobTaskId = taskId,
                        PublishingGroupResourceId = pgResourceId,
                        WorkingDocumentId = playerModel.WorkingDocumentId,
                        __IsNew = true
                    };

                    _taskWorkingDocumentRepo.Save(dto, false, false);

                    scope.Complete();

                    wdId = playerModel.WorkingDocumentId;
                }
            }
            else
            {
                // we are only going to allow 1 open of each checklist type at a time
                wdId = actives.First().WorkingDocumentId;
            }

            result["id"] = wdId;
            result["allow"] = true;
            return result;
        }

        public JToken GetTaskClockins(JToken parameters)
        {
            string userIdString = parameters["userid"].ToString();
            Guid userId = Guid.Parse(userIdString);

            string taskIdString = parameters["taskid"].ToString();
            Guid taskId = Guid.Parse(taskIdString);

            var task = _taskRepo.GetById(taskId, ProjectJobTaskLoadInstructions.WorkLog | ProjectJobTaskLoadInstructions.WorkLogUser);

            JObject result = new JObject();
            JArray log = new JArray();

            foreach (var l in task.ProjectJobTaskWorkLogs)
            {
                var entry = new JObject();
                entry["id"] = l.Id;
                entry["by"] = l.UserData.Name;
                entry["datecreated"] = TimeZoneInfo.ConvertTimeFromUtc(l.DateCreated, TimeZoneInfoResolver.ResolveWindows(l.UserData.TimeZone));
                if (l.DateCompleted.HasValue == true)
                {
                    entry["datecompleted"] = TimeZoneInfo.ConvertTimeFromUtc(l.DateCompleted.Value, TimeZoneInfoResolver.ResolveWindows(l.UserData.TimeZone));
                }
                else
                {
                    entry["datecompleted"] = null;
                }
                entry["notes"] = l.Notes;

                log.Add(entry);
            }

            result["log"] = log;

            return result;
        }

        protected JToken GetFolder(JToken parameters)
        {
            string userIdString = parameters["userid"].ToString();
            Guid userId = Guid.Parse(userIdString);

            string taskIdString = parameters["taskid"].ToString();
            Guid taskId = Guid.Parse(taskIdString);

            string folderIdString = parameters["mediafolderid"].ToString();
            Guid folderId = Guid.Parse(folderIdString);

            // this is relatively easy as we only ever hand back this level, we don't have to recurse down children etc
            var folder = _mediaRepo.GetFolderById(folderId, MediaFolderLoadInstruction.FolderFile | MediaFolderLoadInstruction.FolderFileMedia);

            JObject result = new JObject();

            JArray files = new JArray();
            foreach (var child in folder.MediaFolderFiles)
            {
                JObject c = new JObject();
                c["id"] = child.MediaId;
                c["name"] = child.Medium.Name;
                c["mimetype"] = child.Medium.Type;
                c["isimage"] = false;
                if (child.Medium.Type.ToLower().StartsWith("image") == true)
                {
                    c["isimage"] = true;
                }

                files.Add(c);
            }
            result["files"] = files;

            var childFolders = _mediaRepo.GetFolders(folder.Id, null, MediaFolderLoadInstruction.None);

            JArray folders = new JArray();
            foreach (var child in childFolders)
            {
                JObject c = new JObject();
                c["id"] = child.Id;
                c["text"] = child.Text;

                folders.Add(c);
            }
            result["folders"] = folders;

            return result;
        }

        protected JToken CreateGroup(JToken parameters)
        {
            string userIdString = parameters["userid"].ToString();
            Guid userId = Guid.Parse(userIdString);

            JArray tasks = (JArray)parameters["tasks"];

            Guid groupId = CombFactory.NewComb();

            List<Guid> ids = new List<Guid>();
            foreach (var t in tasks)
            {
                ids.Add(Guid.Parse(t.ToString()));
            }

            using (TransactionScope scope = new TransactionScope())
            {
                foreach (var id in ids)
                {
                    // we are going to guarantee the same starting point for each task in a group. If they are clocked in we are going to clock
                    // them out
                    var log = _logRepo.GetCurrentActive(id, userId, RepositoryInterfaces.Enums.ProjectJobTaskWorkLogLoadInstructions.None);

                    if (log != null)
                    {
                        // clock them out then
                        parameters["taskid"] = id;
                        ToggleClockinClockout(parameters);
                    }

                    var t = _taskRepo.GetById(id, ProjectJobTaskLoadInstructions.None);

                    t.WorkingGroupId = groupId;

                    _taskRepo.Save(t, false, false);
                }

                scope.Complete();
            }

            JObject result = new JObject();
            result["groupid"] = groupId;

            return result;
        }

        protected JToken DeleteGroup(JToken parameters)
        {
            string userIdString = parameters["userid"].ToString();
            Guid userId = Guid.Parse(userIdString);

            string groupIdString = parameters["workinggroupid"].ToString();
            Guid groupId = Guid.Parse(groupIdString);

            var tasks = _taskRepo.GetByWorkingGroupId(groupId, ProjectJobTaskLoadInstructions.None);

            using (TransactionScope scope = new TransactionScope())
            {
                foreach (var task in tasks)
                {
                    task.WorkingGroupId = null;

                    _taskRepo.Save(task, false, false);
                }

                scope.Complete();
            }

            JObject result = new JObject();
            result["success"] = true;

            return result;
        }

        protected JToken GetGroupFinishingDetails(JToken parameters)
        {
            string userIdString = parameters["userid"].ToString();
            string taskIdString = parameters["taskid"].ToString();

            Guid userId = Guid.Parse(userIdString);
            Guid taskId = Guid.Parse(taskIdString);

            var task = _taskRepo.GetById(taskId, ProjectJobTaskLoadInstructions.None);

            if (task.WorkingGroupId.HasValue == false)
            {
                JObject noResult = new JObject();

                return noResult;
            }

            var groupTasks = _taskRepo.GetByWorkingGroupId(task.WorkingGroupId.Value, ProjectJobTaskLoadInstructions.TaskType | ProjectJobTaskLoadInstructions.TaskTypeFinishedStatuses);

            JObject result = new JObject();
            JArray data = new JArray();

            foreach (var gt in groupTasks)
            {
                if (gt.Id == task.Id)
                {
                    // finishing statuses for the current task were previously sent up,
                    // we'll just return the other items in the group
                    continue;
                }

                JObject taskData = new JObject();
                JArray taskStatuses = new JArray();

                taskData["id"] = gt.Id;
                taskData["name"] = gt.Name;
                taskData["finishedstatuses"] = taskStatuses;

                foreach (var f in gt.ProjectJobTaskType.ProjectJobTaskTypeFinishedStatuses)
                {
                    JObject stat = new JObject();
                    stat["id"] = f.Id;
                    stat["finishstatusid"] = f.ProjectJobFinishedStatusId;
                    stat["text"] = f.ProjectJobFinishedStatu.Text;
                    stat["requiresextranotes"] = f.ProjectJobFinishedStatu.RequiresExtraNotes;

                    taskStatuses.Add(stat);
                }

                data.Add(taskData);
            }

            result["groupdata"] = data;

            return result;
        }

        protected JToken SaveChanges(JToken parameters)
        {
            JToken result = new JObject();
            if (parameters["changes"] == null)
            {
                result["success"] = false;

                return result;
            }

            string userIdString = parameters["userid"].ToString();
            string taskIdString = parameters["taskid"].ToString();

            Guid userId = Guid.Parse(userIdString);
            Guid taskId = Guid.Parse(taskIdString);

            var task = _taskRepo.GetById(taskId, ProjectJobTaskLoadInstructions.None);

            if (parameters["changes"]["statusid"] != null)
            {
                task.StatusId = Guid.Parse(parameters["changes"]["statusid"].ToString());
            }

            task.ReferenceNumber = "";
            if (parameters["changes"]["referencenumber"] != null)
            {
                task.ReferenceNumber = parameters["changes"]["referencenumber"].ToString();
            }

            _taskRepo.Save(task, false, false);

            // we return a JSON object representing the new task as though it would appear in the
            // task list
            result = GetTasks(userId, null, null, new Guid[] { taskId }, false);

            return ((JArray)result["opentasks"])[0];
        }

        protected JToken SavePhoto(JToken parameters)
        {
            string userIdString = parameters["userid"].ToString();
            string taskIdString = parameters["taskid"].ToString();
            string idString = parameters["id"].ToString();

            Guid userId = Guid.Parse(userIdString);
            Guid taskId = Guid.Parse(taskIdString);
            Guid id = Guid.Parse(idString);

            var task = _taskRepo.GetById(taskId, ProjectJobTaskLoadInstructions.None);

            string text = string.Empty;
            if (parameters["text"] != null)
            {
                text = parameters["text"].ToString();
            }

            var data = Convert.FromBase64String(parameters["data"].ToString());
            var mediaEnt = new MediaDTO()
            {
                __IsNew = true,
                DateCreated = DateTime.UtcNow,
                Description = "",
                Name = text,
                Filename = text,
                Type = "image/jpg",
                Id = id
            };
            var dataEnt = new MediaDataDTO()
            {
                __IsNew = true,
                Data = data,
                MediaId = mediaEnt.Id
            };
            mediaEnt.MediaData = dataEnt;
            dataEnt.Medium = mediaEnt;

            var taskPhoto = new ProjectJobTaskMediaDTO()
            {
                __IsNew = true,
                TaskId = taskId,
                MediaId = mediaEnt.Id
            };
            task.ProjectJobTaskMedias.Add(taskPhoto);

            using (TransactionScope scope = new TransactionScope())
            {
                _mediaRepo.Save(mediaEnt, false, true);

                _taskRepo.Save(task, false, true);

                scope.Complete();
            }

            JObject result = new JObject();
            result["success"] = true;
            result["mediaid"] = mediaEnt.Id.ToString();

            return result;
        }

        protected JToken RemovePhoto(JToken parameters)
        {
            string userIdString = parameters["userid"].ToString();
            string taskIdString = parameters["taskid"].ToString();
            string idString = parameters["mediaid"].ToString();

            Guid userId = Guid.Parse(userIdString);
            Guid taskId = Guid.Parse(taskIdString);
            Guid id = Guid.Parse(idString);

            _taskRepo.RemoveMedia(taskId, id);

            JObject result = new JObject();
            result["success"] = true;

            return result;
        }

        protected JToken ChangeTaskType(JToken parameters)
        {
            string userIdString = parameters["userid"].ToString();
            string taskIdString = parameters["taskid"].ToString();
            string idString = parameters["tasktypeid"].ToString();

            Guid userId = Guid.Parse(userIdString);
            Guid taskId = Guid.Parse(taskIdString);
            Guid taskTypeId = Guid.Parse(idString);

            var task = _taskRepo.GetById(taskId, ProjectJobTaskLoadInstructions.None);
            var tt = _taskTypeRepo.GetById(taskTypeId, ProjectJobTaskTypeLoadInstructions.None);

            task.TaskTypeId = taskTypeId;
            task.EstimatedDurationSeconds = tt.EstimatedDurationSeconds;

            _taskRepo.Save(task, false, false);

            JObject result = new JObject();
            result["success"] = true;

            return result;
        }

        protected bool IsZeroValue(JToken obj, string prop)
        {
            return obj[prop] == null || string.IsNullOrEmpty(obj[prop].ToString()) || decimal.Parse(obj[prop].ToString()) == 0;
        }

        protected JToken SaveOrder(JToken parameters)
        {
            string taskIdString = parameters["taskid"].ToString();
            string userIdString = parameters["userid"].ToString();

            JObject jOrder = (JObject)parameters["order"];
            JArray jItems = (JArray)jOrder["items"];
            string newString = jOrder["__isnew"].ToString();
            string catalogIdString = jOrder["catalogid"].ToString();

            bool isNew = bool.Parse(newString);
            Guid taskId = Guid.Parse(taskIdString);
            Guid userId = Guid.Parse(userIdString);
            int catalogId = int.Parse(catalogIdString);

            Guid orderId = Guid.Parse(jOrder["id"].ToString());

            var task = _taskRepo.GetById(taskId, ProjectJobTaskLoadInstructions.None);

            if (task.SiteId.HasValue == false)
            {
                throw new InvalidOperationException(string.Format("Task {0} must have a site for orders to be recorded against", task.Id));
            }

            var structure = _facRepo.FindBySiteId(task.SiteId.Value);
            if (structure == null)
            {
                throw new InvalidOperationException(string.Format("Site {0} is not mapped to a facility structure", task.SiteId.Value));
            }

            OrderDTO order = null;
            if (isNew == true)
            {
                order = new OrderDTO()
                {
                    __IsNew = true,
                    Id = orderId,
                    ProjectJobTaskId = taskId,
                    DateCreated = DateTime.UtcNow,
                    FacilityStructureId = structure.Id,
                    OrderedById = userId,
                    ProductCatalogId = catalogId
                };
            }
            else
            {
                order = _orderRepo.GetById(orderId, OrderLoadInstructions.Items);
            }

            var cats = new List<ProductCatalogDTO>();
            var rules = new List<ProductCatalogFacilityStructureRuleDTO>();
            _catRepo.GetCatalogsForFacilityStructure(structure.Id, cats, rules);
            var cat = (from c in cats where c.Id == catalogId select c).First();

            Dictionary<int, OrderItemDTO> toDelete = new Dictionary<int, OrderItemDTO>();
            order.OrderItems.ForEach(i => toDelete.Add(i.ProductId, i));
            order.OrderItems.Clear();

            if (order.TotalPrice.HasValue)
            {
                order.TotalPrice = 0; // get's added up as we go
            }

            foreach (var ji in jItems)
            {
                if (IsZeroValue(ji, "quantity") && IsZeroValue(ji, "stock"))
                {
                    // it needs to be deleted
                    continue;
                }

                bool newItem = bool.Parse(ji["__isnew"].ToString());
                int productId = int.Parse(ji["productid"].ToString());
                decimal quantity = 0;
                decimal? stock = 0;
                decimal? minStockLevel = 0;


                var p = (from c in cat.ProductCatalogItems where c.ProductId == productId select c).First();
                decimal? price = p.Price.HasValue ? p.Price : p.Product.Price;

                if (ji["quantity"] != null && string.IsNullOrEmpty(ji["quantity"].ToString()) == false)
                {
                    quantity = decimal.Parse(ji["quantity"].ToString());
                }
                decimal? totalPrice = price.HasValue ? price * quantity : null;

                if (ji["stock"] != null && string.IsNullOrEmpty(ji["stock"].ToString()) == false)
                {
                    stock = decimal.Parse(ji["stock"].ToString());
                }

                if (ji["minstocklevel"] != null && string.IsNullOrEmpty(ji["minstocklevel"].ToString()) == false)
                {
                    minStockLevel = decimal.Parse(ji["minstocklevel"].ToString());
                }

                Guid itemId = Guid.Parse(ji["id"].ToString());

                OrderItemDTO item = null;
                if (newItem == true)
                {
                    item = new OrderItemDTO()
                    {
                        __IsNew = true,
                        Id = itemId,
                        ProductId = productId
                    };
                }
                else
                {
                    item = toDelete[productId];
                    toDelete.Remove(productId); // if its in the order we aren't removing it
                }

                item.Quantity = quantity;
                item.Stock = stock;
                item.MinStockLevel = minStockLevel;
                item.Price = price;
                item.TotalPrice = totalPrice;

                if (price.HasValue)
                {
                    if (order.TotalPrice.HasValue == false)
                    {
                        order.TotalPrice = 0;
                    }

                    order.TotalPrice = order.TotalPrice + totalPrice;
                }
                order.OrderItems.Add(item);
            }

            using (TransactionScope scope = new TransactionScope())
            {
                _orderRepo.Save(order, true, true);
                if (toDelete.Count > 0)
                {
                    _orderRepo.RemoveItems((from i in toDelete select i.Value.Id).ToArray());
                }
                scope.Complete();
            }

            JObject result = new JObject();
            result["success"] = true;

            return result;
        }

        protected JToken DeleteOrder(JToken parameters)
        {
            string idString = parameters["id"].ToString();

            Guid orderId = Guid.Parse(idString);

            _orderRepo.Remove(orderId);

            JObject result = new JObject();
            result["success"] = true;

            return result;
        }

        private string ExtensionToContentType(string fname)
        {
            var extension = fname.Substring(fname.LastIndexOf('.'));
            switch (extension)
            {
                case ".png":
                    return "image/png";
                case ".jpg":
                case ".jpeg":
                    return "image/jpg";
                default:
                    return "";
            }
        }

        public bool MethodSupportsQueue(string method, JToken parameters)
        {
            if (parameters["clientDateTimeStamp"] == null)
            {
                return false;
            }

            switch (method)
            {
                case "ToggleClockinClockout":
                    return true;
                case "ClockinAndClockout":
                    return true;
                case "Finish":
                    return true;
                case "SaveChanges":
                    return true;
            }

            return false;
        }

        public string ExractPrimaryEntityId(string method, JToken parameters)
        {
            return parameters["taskid"].ToString();
        }
    }

}
