﻿using ConceptCave.Checklist.Editor;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.BusinessLogic.Questions
{
    public class DataSourceQuestion : Question
    {
        public List<IDataSourceQuestionFeed> Feeds { get; set; }

        public bool ShowUserInterface { get; set; }

        public DataSourceQuestion()
            : base()
        {
            Feeds = new List<IDataSourceQuestionFeed>();
        }

        public override object DefaultAnswer
        {
            get
            {
                return null;
            }
            set
            {

            }
        }

        public override Checklist.Interfaces.IAnswer CreateBlankAnswer(Checklist.Interfaces.IPresentedQuestion presentedQuestion)
        {
            return null;
        }

        public override bool IsAnswerable
        {
            get
            {
                return false;
            }
        }

        public JArray FeedsToJson(IWorkingDocument wd)
        {
            JArray result = new JArray();

            Feeds.ForEach(f =>
            {
                JObject m = new JObject();
                f.ToJson(m, wd);

                var fullname = this.Name + "." + f.Name;
                m["fullname"] = wd != null ? wd.Program.ExpandString(fullname) : fullname;

                result.Add(m);
            });

            return result;
        }

        public void FeedsFromJson(string model)
        {
            FeedsFromJson(JArray.Parse(model));
        }

        public void FeedsFromJson(JArray model)
        {
            Feeds = new List<IDataSourceQuestionFeed>();

            foreach(var f in model)
            {
                var feed = new DataSourceQuestionODataFeed();
                feed.FromJson((JObject)f);

                Feeds.Add(feed);
            }
        }

        public override void Encode(Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode<List<IDataSourceQuestionFeed>>("Feeds", Feeds);
            encoder.Encode<bool>("ShowUserInterface", ShowUserInterface);
        }

        public override void Decode(Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            Feeds = decoder.Decode<List<IDataSourceQuestionFeed>>("Feeds");
            ShowUserInterface = decoder.Decode<bool>("ShowUserInterface");
        }

        public override List<NameValueCollection> ToNameValuePairs()
        {
            var r = base.ToNameValuePairs();

            var result = r[0];

            result.Add("showuserinterface", ShowUserInterface.ToString());

            var feeds = FeedsToJson(null);

            result.Add("feeds", feeds.ToString());

            return r;
        }

        public override void FromNameValuePairs(NameValueCollection pairs)
        {
            base.FromNameValuePairs(pairs);

            foreach (var name in pairs.AllKeys)
            {
                string n = name.ToLower();

                switch (n)
                {
                    case "showuserinterface":
                        bool show = false;
                        if (bool.TryParse(pairs[name], out show) == true)
                        {
                            this.ShowUserInterface = show;
                        }
                        break;
                    case "feeds":
                        var feedString = pairs[name];

                        if(string.IsNullOrEmpty(feedString) == false)
                        {
                            JArray feeds = JArray.Parse(feedString);

                            FeedsFromJson(feeds);
                        }

                        break;
                }
            }
        }
    }

    public interface IDataSourceQuestionFeed
    {
        string Name { get; set; }

        void ToJson(JObject model, IWorkingDocument wd);

        void FromJson(JObject model);
    }

    public class DataSourceQuestionODataFeedFieldFormat : Node
    {
        public string FieldName { get; set; }
        public string Format { get;set; }

        public string Data {get;set;}

        public override void Encode(Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("FieldName", FieldName);
            encoder.Encode("Format", Format);
            encoder.Encode("Data", Data);
        }

        public override void Decode(Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            FieldName = decoder.Decode<string>("FieldName");
            Format = decoder.Decode<string>("Format");
            Data = decoder.Decode<string>("Data");
        }

        public void ToJson(JObject model, IWorkingDocument wd)
        {
            model["fieldname"] = wd == null ? FieldName : wd.Program.ExpandString(FieldName);
            model["format"] = wd == null ? Format : wd.Program.ExpandString(Format);
            model["data"] = wd == null ? Data : wd.Program.ExpandString(Data);
        }

        public void FromJson(JObject model)
        {
            FieldName = model["fieldname"].ToString();
            Format = model["format"].ToString();
            Data = model["data"].ToString();
        }
    }

    public class DataSourceQuestionODataFeedParameterDefinition : Node
    {
        /// <summary>
        /// The name of the variable when referenced in the data source. This is the string
        /// that should be used in queries when they are fed through to the odata source
        /// </summary>
        public string InternalName { get; set; }
        /// <summary>
        /// A comma seperated string of names the variable could be referenced as in the parameters
        /// object. This maps those names to the InternalName that is used.
        /// </summary>
        public string ExternalNames { get; set; }

        public string PreQueryInstruction { get; set; }

        public string DefaultValue { get; set; }

        public override void Decode(Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            InternalName = decoder.Decode<string>("InternalName");
            ExternalNames = decoder.Decode<string>("ExternalNames");
            PreQueryInstruction = decoder.Decode<string>("PreQueryInstruction");
            DefaultValue = decoder.Decode<string>("DefaultValue");
        }

        public override void Encode(Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("InternalName", InternalName);
            encoder.Encode("ExternalNames", ExternalNames);
            encoder.Encode("PreQueryInstruction", PreQueryInstruction);
            encoder.Encode("DefaultValue", DefaultValue);
        }

        public void ToJson(JObject model, IWorkingDocument wd)
        {
            model["internalname"] = InternalName;
            model["externalnames"] = ExternalNames;
            model["prequeryinstruction"] = PreQueryInstruction.ToString();

            string df = wd == null ? DefaultValue : wd.Program.ExpandString(DefaultValue);

            model["defaultvalue"] = df;
        }

        public void FromJson(JObject model)
        {
            InternalName = model["internalname"].ToString();
            ExternalNames = model["externalnames"].ToString();
            PreQueryInstruction = model["prequeryinstruction"].ToString();
            DefaultValue = model["defaultvalue"].ToString();
        }
    }

    public enum DataSourceQuestionODataFeedDataScope
    {
        Normal,
        IncludeCurrentUser,
        OnlyCurrentUser
    }

    public class DataSourceQuestionODataFeed : Node, IDataSourceQuestionFeed
    {
        /// <summary>
        /// The name that this feed will be referenced by so clients of the 
        /// feed can find it in a data source
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The URL to the feed.
        /// </summary>
        public string Feed { get; set; }

        /// <summary>
        /// If set to true the feed will use the local odata feed and the feed property will
        /// simply need to be the table off of that root feed (so Members etc)
        /// </summary>
        public bool UseLocalFeed { get; set; }

        /// <summary>
        /// The set of fields from the odata feed that should be shown by default
        /// </summary>
        public string VisibleFields { get; set; }

        /// <summary>
        /// The set of fields that should be used in the order by clause of the query
        /// </summary>
        public string OrderByFields { get; set; }
        /// <summary>
        /// The set of expressions that should be used in the filter clause of the query
        /// </summary>
        public string Filter { get; set; }

        public string ExtraFields { get; set; }

        /// <summary>
        /// OData $expand fields to be used
        /// </summary>
        public string ExpandFields { get; set; }

        /// <summary>
        /// OData $select fields to be used
        /// </summary>
        public string SelectFields { get; set; }

        public bool IncludeCount { get; set; }

        public string DataScope { get; set; }

        /// <summary>
        /// The set of fields that the feed should use as a unique key. The feed will then
        /// adjust the properties of the object rather than creating a new object each time
        /// the feed is refreshed. If the key doesn't exist a new one is created.
        /// </summary>
        public string KeyFields { get; set; }

        /// <summary>
        /// The set of fields that should be used as a key to transform rows into columns.
        /// </summary>
        public string TransformAroundFields { get; set; }

        /// <summary>
        /// The set of fields whose values should be transformed into columns (or field names)
        /// </summary>
        public string TransformToColumnTitlesFields { get; set; }
        /// <summary>
        /// The set of fields whose values should be used to populate the transformed into columns values
        /// </summary>
        public string TransformToColumnValuesFields { get; set; }

        public List<DataSourceQuestionODataFeedFieldFormat> FieldFormats { get; set; }

        public List<DataSourceQuestionODataFeedParameterDefinition> ParameterDefinitions { get; set; }

        /// <summary>
        /// Wether or not the datasource should use pages
        /// </summary>
        public bool UsePaging { get; set; }

        /// <summary>
        /// The number of items per page if paging is enabled
        /// </summary>
        public int ItemsPerPage { get; set; }

        public int RefreshPeriodSeconds { get; set; }

        public DataSourceQuestionODataFeed()
        {
            FieldFormats = new List<DataSourceQuestionODataFeedFieldFormat>();
        }

        protected void populateArrayFromString(JArray array, string toSplit)
        {
            if (string.IsNullOrEmpty(toSplit) == false)
            {
                foreach (var field in toSplit.Split(','))
                {
                    array.Add(field.Trim());
                }
            }
        }

        public void ToJson(JObject model, IWorkingDocument wd)
        {
            model["type"] = "odata";
            model["name"] = Name;
            model["feed"] = wd == null ? Feed : wd.Program.ExpandString(Feed);
            model["uselocalfeed"] = UseLocalFeed;

            string vf = wd == null ? VisibleFields : wd.Program.ExpandString(VisibleFields);
            string of = wd == null ? OrderByFields : wd.Program.ExpandString(OrderByFields);
            string tf = wd == null ? TransformAroundFields : wd.Program.ExpandString(TransformAroundFields);
            string tcf = wd == null ? TransformToColumnTitlesFields : wd.Program.ExpandString(TransformToColumnTitlesFields);
            string tcvf = wd == null ? TransformToColumnValuesFields : wd.Program.ExpandString(TransformToColumnValuesFields);

            string kf = wd == null ? KeyFields : wd.Program.ExpandString(KeyFields);

            model["visiblefields"] = vf;
            model["orderbyfields"] = of;
            model["transformaroundfields"] = tf;
            model["transformtocolumntitlesfields"] = tcf;
            model["transformtocolumnvaluesfields"] = tcvf;
            model["keyfields"] = kf;
            model["filter"] = wd == null ? Filter : wd.Program.ExpandString(Filter);
            model["extrafields"] = wd == null ? ExtraFields : wd.Program.ExpandString(ExtraFields);
            model["expandfields"] = wd == null ? ExpandFields : wd.Program.ExpandString(ExpandFields);
            model["selectfields"] = wd == null ? SelectFields : wd.Program.ExpandString(SelectFields);
            model["includecount"] = IncludeCount;
            model["usepaging"] = UsePaging;
            model["itemsperpage"] = ItemsPerPage;
            model["refreshperiodseconds"] = RefreshPeriodSeconds;

            model["datascope"] = DataScope;

            var displayFields = new JArray();
            populateArrayFromString(displayFields, vf);
            model["displayfields"] = displayFields;

            var keyFields = new JArray();
            populateArrayFromString(keyFields, kf);
            model["idfields"] = keyFields;

            var transformAroundFields = new JArray();
            populateArrayFromString(transformAroundFields, tf);
            model["transformkeyfields"] = transformAroundFields;

            var transformColumnFields = new JArray();
            populateArrayFromString(transformColumnFields, tcf);
            model["transformcolumnfields"] = transformColumnFields;

            var transformValueFields = new JArray();
            populateArrayFromString(transformValueFields, tcvf);
            model["transformvaluefields"] = transformValueFields;

            var formats = new JArray();
            foreach(var format in FieldFormats)
            {
                var fFormat = new JObject();
                format.ToJson(fFormat, wd);

                formats.Add(fFormat);
            }
            model["fieldformats"] = formats;

            var definitions = new JArray();
            foreach(var def in ParameterDefinitions)
            {
                var fDef = new JObject();
                def.ToJson(fDef, wd);

                definitions.Add(fDef);
            }
            model["parameterdefinitions"] = definitions;
        }

        public void FromJson(JObject model)
        {
            string val = model["name"].ToString();
            if(string.IsNullOrEmpty(val) == false)
            {
                Name = val;
            }

            val = model["feed"].ToString();
            if (string.IsNullOrEmpty(val) == false)
            {
                Feed = val;
            }

            UseLocalFeed = model["uselocalfeed"].Value<bool>();
            IncludeCount = model["includecount"].Value<bool>();

            val = model["visiblefields"].ToString();
            if (string.IsNullOrEmpty(val) == false)
            {
                VisibleFields = val;
            }

            val = model["keyfields"].ToString();
            if(string.IsNullOrEmpty(val) == false)
            {
                KeyFields = val;
            }

            val = model["orderbyfields"].ToString();
            if (string.IsNullOrEmpty(val) == false)
            {
                OrderByFields = val;
            }

            val = model["expandfields"].ToString();
            if (string.IsNullOrEmpty(val) == false)
            {
                ExpandFields = val;
            }

            val = model["selectfields"].ToString();
            if (string.IsNullOrEmpty(val) == false)
            {
                SelectFields = val;
            }

            val = model["transformaroundfields"].ToString();
            if (string.IsNullOrEmpty(val) == false)
            {
                TransformAroundFields = val;
            }

            val = model["transformtocolumntitlesfields"].ToString();
            if (string.IsNullOrEmpty(val) == false)
            {
                TransformToColumnTitlesFields = val;
            }

            val = model["transformtocolumnvaluesfields"].ToString();
            if (string.IsNullOrEmpty(val) == false)
            {
                TransformToColumnValuesFields = val;
            }

            val = model["filter"].ToString();
            if (string.IsNullOrEmpty(val) == false)
            {
                Filter = val;
            }

            //val = model["extrafields"].ToString();
            //if(string.IsNullOrEmpty(val) == false)
            //{
            //    ExtraFields = val;
            //}

            UsePaging = model["usepaging"].Value<bool>();
            ItemsPerPage = model["itemsperpage"].Value<int>();

            if(model["datascope"] != null)
            {
                DataScope = model["datascope"].ToString();
            }

            if(string.IsNullOrEmpty(model["refreshperiodseconds"].ToString()) == false)
            {
                RefreshPeriodSeconds = model["refreshperiodseconds"].Value<int>();
            }

            FieldFormats = new List<DataSourceQuestionODataFeedFieldFormat>();
            var formats = (JArray)model["fieldformats"];
            foreach(var jFormat in formats)
            {
                var format = new DataSourceQuestionODataFeedFieldFormat();
                format.FromJson((JObject)jFormat);

                FieldFormats.Add(format);
            }

            ParameterDefinitions = new List<DataSourceQuestionODataFeedParameterDefinition>();
            var definitions = (JArray)model["parameterdefinitions"];
            foreach(var jDef in definitions)
            {
                var def = new DataSourceQuestionODataFeedParameterDefinition();
                def.FromJson((JObject)jDef);

                ParameterDefinitions.Add(def);
            }
        }

        public override void Encode(Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("Name", Name);
            encoder.Encode("Feed", Feed);
            encoder.Encode("UseLocalFeed", UseLocalFeed);
            encoder.Encode("VisibleFields", VisibleFields);
            encoder.Encode("OrderByFields", OrderByFields);
            encoder.Encode("KeyFields", KeyFields);
            encoder.Encode("IncludeCount", IncludeCount);
            encoder.Encode("TransformAroundFields", TransformAroundFields);
            encoder.Encode("TransformToColumnTitlesFields", TransformToColumnTitlesFields);
            encoder.Encode("TransformToColumnValuesFields", TransformToColumnValuesFields);
            encoder.Encode("Filter", Filter);
            encoder.Encode("ExpandFields", ExpandFields);
            encoder.Encode("SelectFields", SelectFields);
            encoder.Encode("ExtraFields", ExtraFields);
            encoder.Encode("UsePaging", UsePaging);
            encoder.Encode("ItemsPerPage", ItemsPerPage);
            encoder.Encode("RefreshPeriodSeconds", RefreshPeriodSeconds);
            encoder.Encode("FieldFormats", FieldFormats);
            encoder.Encode("ParameterDefinitions", ParameterDefinitions);
            encoder.Encode("DataScope", DataScope);
        }

        public override void Decode(Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            Name = decoder.Decode<string>("Name");
            Feed = decoder.Decode<string>("Feed");
            UseLocalFeed = decoder.Decode<bool>("UseLocalFeed");
            VisibleFields = decoder.Decode<string>("VisibleFields");
            OrderByFields = decoder.Decode<string>("OrderByFields");
            KeyFields = decoder.Decode<string>("KeyFields");
            IncludeCount = decoder.Decode<bool>("IncludeCount");
            TransformAroundFields = decoder.Decode<string>("TransformAroundFields");
            TransformToColumnTitlesFields = decoder.Decode<string>("TransformToColumnTitlesFields");
            TransformToColumnValuesFields = decoder.Decode<string>("TransformToColumnValuesFields");
            Filter = decoder.Decode<string>("Filter");
            ExpandFields = decoder.Decode<string>("ExpandFields");
            SelectFields = decoder.Decode<string>("SelectFields");
            ExtraFields = decoder.Decode<string>("ExtraFields");
            UsePaging = decoder.Decode<bool>("UsePaging");
            ItemsPerPage = decoder.Decode<int>("ItemsPerPage");
            RefreshPeriodSeconds = decoder.Decode<int>("RefreshPeriodSeconds");
            FieldFormats = decoder.Decode<List<DataSourceQuestionODataFeedFieldFormat>>("FieldFormats");

            DataScope = decoder.Decode<string>("DataScope");

            if(FieldFormats == null)
            {
                FieldFormats = new List<DataSourceQuestionODataFeedFieldFormat>();
            }

            ParameterDefinitions = decoder.Decode<List<DataSourceQuestionODataFeedParameterDefinition>>("ParameterDefinitions");

            if(ParameterDefinitions == null)
            {
                ParameterDefinitions = new List<DataSourceQuestionODataFeedParameterDefinition>();
            }
        }
    }
}
