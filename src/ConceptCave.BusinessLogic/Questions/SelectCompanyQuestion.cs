﻿using ConceptCave.Checklist.Editor;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.RunTime;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.BusinessLogic.Questions
{
    public class SelectCompanyQuestion : Question
    {
        private ICompanyRepository _companyRepo;

        public SelectCompanyQuestion(ICompanyRepository companyRepo)
        {
            _companyRepo = companyRepo;
        }

        public override IAnswer CreateBlankAnswer(IPresentedQuestion presentedQuestion)
        {
            var result = new SelectCompanyAnswer(_companyRepo) { PresentedQuestion = presentedQuestion };

            if (DefaultAnswer != null && DefaultAnswer is Guid)
            {
                result.AnswerValue = DefaultAnswer;
            }
            else if (DefaultAnswer != null && DefaultAnswer is SelectCompanyAnswer)
            {
                result.AnswerValue = ((SelectCompanyAnswer)DefaultAnswer).SelectedCompanyId;
            }

            return result;
        }

        public override object DefaultAnswer
        {
            get
            {
                return _defaultAnswer;
            }
            set
            {
                if (value is Guid)
                {
                    _defaultAnswer = value;
                }
                else if (value is SelectUserAnswer)
                {
                    _defaultAnswer = ((SelectUserAnswer)value).AnswerValue;
                }
            }
        }
    }

    public class SelectCompanyAnswer : Answer
    {
        private ICompanyRepository _companyRepo;

        protected Guid? _selectedId;
        protected CompanyDTO _company;

        public SelectCompanyAnswer(ICompanyRepository companyRepo)
        {
            _companyRepo = companyRepo;
        }

        public CompanyDTO SelectedCompany
        {
            get
            {
                if(_selectedId.HasValue == false)
                {
                    return null;
                }

                if(_company == null)
                {
                    _company = _companyRepo.GetById(_selectedId.Value, RepositoryInterfaces.Enums.CompanyLoadInstructions.CountryState);
                }

                return _company;
            }
        }

        public Guid? SelectedCompanyId
        {
            get
            {
                return _selectedId;
            }
            set
            {
                UtcDateAnswered = DateTime.UtcNow;
                _selectedId = value;

                if (_selectedId == Guid.Empty)
                {
                    _selectedId = null;
                }

                _company = null;
            }
        }

        public override object AnswerValue
        {
            get
            {
                return SelectedCompanyId;
            }
            set
            {
                if (value != null)
                {
                    SelectedCompanyId = (Guid)value;
                }
                else
                {
                    SelectedCompanyId = null;
                }
            }
        }

        public override void Encode(Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("SelectedCompanyId", SelectedCompanyId);
        }

        public override void Decode(Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            SelectedCompanyId = decoder.Decode<Guid?>("SelectedCompanyId", null);
        }

        public override string AnswerAsString
        {
            get
            {
                if (SelectedCompanyId.HasValue == false)
                {
                    return string.Empty;
                }

                if (SelectedCompanyId == null)
                {
                    return "Unknown Company";
                }

                return SelectedCompany.Name;
            }
        }
    }
}
