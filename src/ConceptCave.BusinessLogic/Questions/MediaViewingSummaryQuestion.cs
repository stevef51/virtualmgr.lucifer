﻿using ConceptCave.Checklist.Editor;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.RepositoryInterfaces.ViewDefs;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.BusinessLogic.Questions
{
    public class MediaViewingSummaryQuestion : Question
    {
        /// <summary>
        /// Indicates wether or not the question should listen for event changes on the page. Used in dashboards to update the labels content
        /// </summary>
        public bool Listen { get; set; }

        /// <summary>
        /// The name of the question that this question should listen to
        /// </summary>
        public string ListenTo { get; set; }

        public override object DefaultAnswer
        {
            get
            {
                return null;
            }
            set
            {

            }
        }

        public override bool IsAnswerable
        {
            get
            {
                return false;
            }
        }

        public override Checklist.Interfaces.IAnswer CreateBlankAnswer(Checklist.Interfaces.IPresentedQuestion presentedQuestion)
        {
            return null;
        }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("Listen", Listen);
            encoder.Encode("ListenTo", ListenTo);
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            Listen = decoder.Decode<bool>("Listen");
            ListenTo = decoder.Decode<string>("ListenTo");
        }
    }

    public class MediaViewingSummaryQuestionExecutionHandler : IExecutionMethodHandler
    {
        protected IMediaRepository _mediaRepo;
        protected IMembershipRepository _memberRepo;

        public MediaViewingSummaryQuestionExecutionHandler(IMediaRepository mediaRepo, IMembershipRepository memberRepo)
        {
            _mediaRepo = mediaRepo;
            _memberRepo = memberRepo;
        }

        public string Name
        {
            get { return "MediaViewingSummary"; }
        }

        public Newtonsoft.Json.Linq.JToken Execute(string method, Newtonsoft.Json.Linq.JToken parameters, IExecutionMethodHandlerContext context)
        {
            switch (method)
            {
                case "GetFolderSummary":
                    return GetFolderSummary(parameters);

                case "GetFolderUserDetail":
                    return GetFolderUserDetail(parameters);
            }

            return null;
        }

        public JToken GetFolderSummary(JToken parameters)
        {
            Guid folderid = Guid.Parse(parameters["folderid"].Value<string>());

            var folder = _mediaRepo.GetFolderById(folderid, RepositoryInterfaces.Enums.MediaFolderLoadInstruction.FolderFile | RepositoryInterfaces.Enums.MediaFolderLoadInstruction.FolderFileMedia | RepositoryInterfaces.Enums.MediaFolderLoadInstruction.Roles);

            var viewings = _mediaRepo.GetLatestMediaViewingsForFolder(folderid, RepositoryInterfaces.Enums.MediaViewingLoadInstruction.Users | RepositoryInterfaces.Enums.MediaViewingLoadInstruction.Media);

            var roles = folder.MediaFolderRoles.Select(m => m.RoleId).ToList();
            
            IList<UserListResponse> users = null;
            
            if(roles.Count == 0)
            {
                users = _memberRepo.GetAllRealUsers().ToList();
            }
            else{
                users = _memberRepo.GetAllUsersByRoles(roles).ToList();
            }

            // viewings is a list of all viewings for each user of the latest, so if user a has viewed a piece of media twice (latest version), then there
            // will be 2 entries for it. We are only interested in counting it once
            Dictionary<string, JObject> viewingcache = new Dictionary<string, JObject>();
            Dictionary<Guid, JObject> cache = new Dictionary<Guid, JObject>();
            Dictionary<Guid, JObject> mediaCache = new Dictionary<Guid, JObject>();

            JArray summaries = new JArray();
            JArray mediaSummaries = new JArray();

            foreach(var user in users)
            {
                var summary = new JObject();
                summary["userid"] = user.UserId;
                summary["usersname"] = user.Name;
                summary["totalreads"] = 0;
                summary["totalaccepts"] = 0;
                summary["distinctreads"] = 0;

                summaries.Add(summary);

                cache.Add(user.UserId, summary);
            }

            foreach (var file in folder.MediaFolderFiles)
            {
                var mediaSummary = new JObject();
                mediaSummary["id"] = file.MediaId;
                mediaSummary["name"] = file.Medium.Name;
                mediaSummary["totalreads"] = 0;
                mediaSummary["totalaccepts"] = 0;
                mediaSummary["distinctreads"] = 0;

                mediaSummaries.Add(mediaSummary);

                mediaCache.Add(file.MediaId, mediaSummary);
            }

            int mediaCount = folder.MediaFolderFiles.Count;
            int userCount = users.Count;

            foreach (var viewing in viewings)
            {
                string key = string.Format("{0}:{1}", viewing.UserId, viewing.MediaId);

                bool viewedByUserBefore = false;

                JObject summary = null;
                if (viewingcache.TryGetValue(key, out summary) == true)
                {
                    viewedByUserBefore = true;
                }
                else
                {
                    viewingcache.Add(key, null);
                }

                JObject mediaSummary = null;
                if (mediaCache.TryGetValue(viewing.MediaId, out mediaSummary) == false)
                {
                    // this should only happen if media was in a folder and has then been moved,
                    // as the loop above should cover us for current media.
                    mediaSummary = new JObject();
                    mediaSummary["id"] = viewing.MediaId;
                    mediaSummary["name"] = viewing.Media.Name;
                    mediaSummary["totalreads"] = 0;
                    mediaSummary["totalaccepts"] = 0;
                    mediaSummary["distinctreads"] = 0;

                    mediaSummaries.Add(mediaSummary);

                    mediaCache.Add(viewing.MediaId, mediaSummary);

                    mediaCount++;
                }

                if (cache.TryGetValue(viewing.UserId, out summary) == false)
                {
                    summary = new JObject();
                    summary["id"] = viewing.UserId;
                    summary["usersname"] = viewing.UserData.Name;
                    summary["totalreads"] = 0;
                    summary["totalaccepts"] = 0;
                    summary["distinctreads"] = 0;

                    summaries.Add(summary);

                    cache.Add(viewing.UserId, summary);

                    userCount++;
                }

                mediaSummary["totalreads"] = mediaSummary["totalreads"].Value<int>() + 1;
                summary["totalreads"] = summary["totalreads"].Value<int>() + 1;

                if (viewedByUserBefore == false)
                {
                    if (viewing.Accepted == true)
                    {
                        summary["totalaccepts"] = summary["totalaccepts"].Value<int>() + 1;
                        mediaSummary["totalaccepts"] = mediaSummary["totalaccepts"].Value<int>() + 1;
                    }

                    summary["distinctreads"] = summary["distinctreads"].Value<int>() + 1;
                    mediaSummary["distinctreads"] = mediaSummary["distinctreads"].Value<int>() + 1;
                }
            }

            JObject result = new JObject();
            result["mediacount"] = mediaCount;
            result["usercount"] = userCount;
            result["usersummaries"] = summaries;
            result["mediasummaries"] = mediaSummaries;

            return result;
        }

        public JToken GetFolderUserDetail(JToken parameters)
        {
            Guid folderid = Guid.Parse(parameters["folderid"].Value<string>());
            Guid userid = Guid.Parse(parameters["userid"].Value<string>());


            // we want the list of what they've viewed with some stats
            // the list of what they haven't viewed

            // for the list of what they have viewed it would be nice to have
            // total views of each piece of media
            // total time spent reading the media

            var folder = _mediaRepo.GetFolderById(folderid, RepositoryInterfaces.Enums.MediaFolderLoadInstruction.FolderFile | RepositoryInterfaces.Enums.MediaFolderLoadInstruction.FolderFileMedia);
            var viewings = _mediaRepo.GetLatestMediaViewingsForFolder(folderid, userid, RepositoryInterfaces.Enums.MediaViewingLoadInstruction.None);

            var roles = folder.MediaFolderRoles.Select(m => m.RoleId).ToList();

            var viewed = from v in viewings select v.MediaId;

            var user = _memberRepo.GetById(userid, RepositoryInterfaces.Enums.MembershipLoadInstructions.None);

            JObject result = new JObject();
            JArray views = new JArray();
            result["views"] = views;

            foreach (var file in from f in folder.MediaFolderFiles where viewed.Contains(f.MediaId) select f)
            {
                var vs = from v in viewings where v.MediaId == file.MediaId select v;

                JObject summary = null;

                summary = new JObject();
                summary["id"] = file.MediaId;
                summary["name"] = file.Medium.Name;
                summary["totalreads"] = vs.Count();
                summary["totaltime"] = vs.Sum(v => v.TotalSeconds);

                var accepted = from v in vs orderby v.DateCompleted descending select v;

                if (accepted.First().Accepted == true)
                {
                    summary["accepted"] = true;
                    summary["acceptionnotes"] = "";
                }
                else
                {
                    summary["accepted"] = false;
                    summary["acceptionnotes"] = accepted.First().AcceptionNotes;
                }

                views.Add(summary);
            }

            JArray nViews = new JArray();
            result["notviewed"] = nViews;
            var notViewed = from f in folder.MediaFolderFiles where viewed.Contains(f.MediaId) == false select f;
            foreach (var file in notViewed)
            {
                var item = new JObject();
                item["id"] = file.MediaId;
                item["name"] = file.Medium.Name;

                nViews.Add(item);
            }

            result["name"] = user.Name;

            return result;
        }
    }
}
