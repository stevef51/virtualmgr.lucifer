﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Checklist.Editor;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.RunTime;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ConceptCave.BusinessLogic.Questions
{
    public class MediaDirectoryQuestion : Question
    {
        public bool ShowPageViewings { get; set; }

        protected IMediaRepository _mediaRepo;

        public MediaDirectoryQuestion(IMediaRepository mediaRepo)
        {
            _mediaRepo = mediaRepo;
        }

        public override object DefaultAnswer
        {
            get
            {
                return _defaultAnswer;
            }
            set
            {
                if (value is Guid)
                {
                    _defaultAnswer = value;
                }
                else if (value is MediaAnswer)
                {
                    _defaultAnswer = ((MediaAnswer)value).AnswerValue;
                }
            }
        }

        public override Checklist.Interfaces.IAnswer CreateBlankAnswer(Checklist.Interfaces.IPresentedQuestion presentedQuestion)
        {
            var result = new MediaAnswer(_mediaRepo) { PresentedQuestion = presentedQuestion };

            if (DefaultAnswer != null && DefaultAnswer is Guid)
            {
                result.AnswerValue = DefaultAnswer;
            }
            else if (DefaultAnswer != null && DefaultAnswer is IHasMediaId)
            {
                result.AnswerValue = ((IHasMediaId)DefaultAnswer).MediaId;
            }

            return result;
        }

        public override void Encode(Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode<bool>("ShowPageViewings", ShowPageViewings);

        }

        public override void Decode(Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            ShowPageViewings = decoder.Decode<bool>("ShowPageViewings", false);
        }
    }

    public class MediaAnswer : Answer, IHasMediaId
    {
        protected MediaDTO _selectedMedia;
        protected Guid? _selectedMediaId;

        protected IMediaRepository _mediaRepo;

        public MediaAnswer(IMediaRepository mediaRepo)
        {
            _mediaRepo = mediaRepo;
        }

        public MediaDTO SelectedMedia
        {
            get
            {
                if (SelectedMediaId.HasValue == false)
                {
                    return null;
                }

                if (_selectedMedia == null)
                {
                    _selectedMedia = _mediaRepo.GetById(_selectedMediaId.Value, MediaLoadInstruction.None);
                }

                return _selectedMedia;
            }
        }

        public void SetSelectedMediaFolderFile(Guid id)
        {
            var mediaFile = _mediaRepo.GetFolderFileById(id, MediaFolderFileLoadInstruction.None);

            if (mediaFile == null)
            {
                return;
            }

            SelectedMediaId = mediaFile.MediaId;
        }

        public Guid? SelectedMediaId
        {
            get
            {
                return _selectedMediaId;
            }
            set
            {
                UtcDateAnswered = DateTime.UtcNow;
                _selectedMediaId = value;

                if (_selectedMediaId == Guid.Empty)
                {
                    _selectedMediaId = null;
                }

                _selectedMedia = null;
            }
        }

        public override object AnswerValue
        {
            get
            {
                return SelectedMediaId;
            }
            set
            {
                if (value != null)
                {
                    SelectedMediaId = (Guid)value;
                }
                else
                {
                    SelectedMediaId = null;
                }
            }
        }

        public override string AnswerAsString
        {
            get
            {
                if (SelectedMediaId.HasValue == false)
                {
                    return string.Empty;
                }

                if (SelectedMedia == null)
                {
                    return "No Media";
                }

                return SelectedMedia.Name;
            }
        }

        public override void Encode(Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("SelectedMediaId", SelectedMediaId);
        }

        public override void Decode(Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            SelectedMediaId = decoder.Decode<Guid?>("SelectedMediaId", null);
        }

        #region IHasMediaId Members

        public Guid MediaId
        {
            get { return SelectedMediaId.HasValue ? SelectedMediaId.Value : Guid.Empty; }
        }

        #endregion
    }

    public class MediaDirectoryExecutionHandler : IExecutionMethodHandler
    {
        [JsonObject]
        public class MediaFolder
        {
            public MediaFolder()
            {
                Children = new List<MediaFolder>();

                Data = new MediaFolderData();
            }

            public MediaFolder(MediaFolderFileDTO source, IList<MediaViewingDTO> viewings)
                : this()
            {
                Label = source.Medium.Name;
                Data.Id = source.Id;
                Data.ParentId = null;
                Data.Decription = source.Medium.Description;
                Data.IsFile = true;
                Data.ShowViaChecklistId = source.ShowViaChecklistId;

                if (viewings == null)
                {
                    return;
                }

                var view = from v in viewings where v.MediaId == source.MediaId select v;

                if (view.Count() == 0)
                {
                    Data.ViewStatus = "unread";
                    return;
                }

                if (view.First().Version < source.Medium.Version)
                {
                    Data.ViewStatus = "read previous";
                }
                else
                {
                    Data.ViewStatus = "current";
                }
            }

            public MediaFolder(MediaFolderDTO source, IList<MediaViewingDTO> viewings)
                : this()
            {
                Label = source.Text;
                Data.Id = source.Id;
                Data.ParentId = source.ParentId;
                Data.ShowViaChecklistId = source.ShowViaChecklistId;

                foreach (var child in source.MediaFolders)
                {
                    var c = new MediaFolder(child, viewings);
                    Children.Add(c);
                }

                foreach (var file in source.MediaFolderFiles)
                {
                    var f = new MediaFolder(file, viewings);
                    Children.Add(f);
                }
            }

            [JsonProperty("label")]
            public string Label { get; set; }

            [JsonProperty("children")]
            public List<MediaFolder> Children { get; set; }

            [JsonProperty("data")]
            public MediaFolderData Data { get; set; }
        }

        [JsonObject]
        public class MediaFolderData
        {
            [JsonProperty("id")]
            public Guid Id { get; set; }

            [JsonProperty("parentid")]
            public Guid? ParentId { get; set; }

            [JsonProperty("isfile")]
            public bool IsFile { get; set; }

            [JsonProperty("description")]
            public string Decription { get; set; }

            [JsonProperty("viewstatus")]
            public string ViewStatus { get; set; }

            [JsonProperty("showviachecklistid")]
            public int? ShowViaChecklistId { get; set; }
        }

        protected IMediaRepository _mediaRepo;
        protected IMembershipRepository _membershipRepo;

        public MediaDirectoryExecutionHandler(IMediaRepository mediaRepo, IMembershipRepository memberRepo)
        {
            _mediaRepo = mediaRepo;
            _membershipRepo = memberRepo;
        }

        public string Name
        {
            get { return "MediaDirectory"; }
        }

        public Newtonsoft.Json.Linq.JToken Execute(string method, Newtonsoft.Json.Linq.JToken parameters, IExecutionMethodHandlerContext context)
        {
            switch (method)
            {
                case "GetFolders":
                    return GetFolders(parameters);

            }

            return null;
        }

        protected JToken GetFolders(JToken parameters)
        {
            string userIdString = parameters["userid"].ToString();

            Guid userId = Guid.Parse(userIdString);

            bool showPageViewings = false;
            if (parameters["showpageviewings"] != null)
            {
                showPageViewings = bool.Parse(parameters["showpageviewings"].ToString());
            }

            var roles = _membershipRepo.GetRolesForUser(userId).ToArray();

            var folders = _mediaRepo.GetFolders(false, roles, MediaFolderLoadInstruction.FolderFile | MediaFolderLoadInstruction.FolderFileMedia);

            IList<MediaViewingDTO> viewings = null;
            if (showPageViewings == true)
            {
                viewings = _mediaRepo.GetLatestMediaViewingsForUser(userId);
            }

            List<MediaFolder> result = new List<MediaFolder>();
            foreach (var folder in folders)
            {
                var mediaFolder = new MediaFolder(folder, viewings);
                result.Add(mediaFolder);
            }

            JObject r = new JObject();
            r["folders"] = JToken.FromObject(result);

            return r;
        }
    }
}
