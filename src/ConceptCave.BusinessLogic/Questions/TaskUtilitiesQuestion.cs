﻿using ConceptCave.Checklist.Editor;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.RepositoryInterfaces.ScheduleManagement;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace ConceptCave.BusinessLogic.Questions
{
    public class TaskUtilitiesQuestionExecutionHandler : IExecutionMethodHandler
    {
        public const string TASKUTILITIESROLE_ASSIGNTASKSTOOTHERS = "TaskUtilityOperationsAssignTasksToOthers";
        public const string TASKUTILITIESROLE_ACQUIRETASKSFROMOTHERS = "TaskUtilityOperationsAcquireTasksFromOthers";
        public const string TASKUTILITIESROLE_CANCEL = "TaskUtilityOperationsCancel";
        public const string TASKUTILITIESROLE_FINISHBYMANAGEMENT = "TaskUtilityOperationsFinishByManagement";
        public const string TASKUTILITIESROLE_DELETE = "TaskUtilityOperationsDelete";
        public const string TASKUTILITIESROLE_CLEARSTARTTIME = "TaskUtilityOperationsClearStartTime";

        protected IMembershipRepository _memberRepo;
        protected ITaskRepository _taskRepo;
        protected ITaskWorkLogRepository _logRepo;
        protected ITaskChangeRequestRepository _changeRepo;
        protected IHierarchyRepository _hierarchyRepo;
        protected IScheduleManagement _scheduleManagement;
        protected IRosterRepository _rosterRepo;

        public TaskUtilitiesQuestionExecutionHandler(IMembershipRepository memberRepo,
            ITaskRepository taskRepo,
            ITaskWorkLogRepository logRepo,
            ITaskChangeRequestRepository changeRepo,
            IHierarchyRepository hierarchyRepo,
            IScheduleManagement scheduleManagement,
            IRosterRepository rosterRepo)
        {
            _memberRepo = memberRepo;
            _taskRepo = taskRepo;
            _logRepo = logRepo;
            _changeRepo = changeRepo;
            _hierarchyRepo = hierarchyRepo;
            _scheduleManagement = scheduleManagement;
            _rosterRepo = rosterRepo;
        }

        public string Name
        {
            get { return "TaskUtilities"; }
        }

        public Newtonsoft.Json.Linq.JToken Execute(string method, Newtonsoft.Json.Linq.JToken parameters, IExecutionMethodHandlerContext context)
        {
            if (parameters["authentication"] != null)
            {
                if (Authenticated(parameters["authentication"]) == false)
                {
                    JObject result = new JObject();
                    result["authenticated"] = false;

                    return result;
                }
            }

            switch (method)
            {
                case "Reassign":
                    return Reassign(parameters, context);
                case "Cancel":
                    return Cancel(parameters, context);
                case "Finish":
                    return Finish(parameters, context);
                case "Delete":
                    return Delete(parameters, context);
                case "ClearStartTime":
                    return ClearStartTime(parameters, context);
                case "GetClaimableTasks":
                    return GetClaimableTasks(parameters, context);
            }

            throw new InvalidOperationException(string.Format("{0} does not exist as a method on the TaskUtilitiesExecutionHandler", method));
        }

        protected bool Authenticated(JToken authentication)
        {
            string username = null;
            string password = null;

            password = authentication["password"].ToString();

            if (bool.Parse(authentication["kioskmode"].ToString()) == false)
            {
                username = authentication["username"].ToString();
            }
            else
            {
                username = password;
            }

            return _memberRepo.ValidateUser(username, password);
        }

        protected Guid AuthenticatedUser(JToken authentication)
        {
            string username = null;

            if (authentication["username"] != null)
            {
                username = authentication["username"].ToString();
            }
            else
            {
                username = authentication["password"].ToString();
            }

            var result = _memberRepo.GetByUsername(username, MembershipLoadInstructions.None);

            return result.UserId;
        }

        protected bool canContinueWithTask(ProjectJobTaskDTO task)
        {
            if (ProjectJobTaskStatusGroups.LiveStates.Contains((ProjectJobTaskStatus)task.Status) == false)
            {
                return false;
            }

            var log = _logRepo.GetCurrentActive(task.Id, task.OwnerId, RepositoryInterfaces.Enums.ProjectJobTaskWorkLogLoadInstructions.None);

            if (log != null)
            {
                // the current owner of the task is clocked into it, they have to clock out
                // for it to be reassigned to someone else
                return false;
            }

            return true;
        }

        protected JToken Reassign(JToken parameters, IExecutionMethodHandlerContext context)
        {
            Guid assignToUserId = (Guid)context.CurrentUserId;
            int? rosterId = null;

            bool toOriginal = false;

            if (parameters["assignto"] != null && string.IsNullOrEmpty(parameters["assignto"].ToString()) == false)
            {
                string assignto = parameters["assignto"].ToString();

                if (assignto.ToLower() == "original")
                {
                    toOriginal = true;
                }
                else
                {
                    assignToUserId = Guid.Parse(parameters["assignto"].ToString());
                }
            }

            if (assignToUserId == (Guid)context.CurrentUserId)
            {
                // they're acquiring someone else's tasks and assigning to themselves, check their allowed to do this
                if (_memberRepo.UserIsInRole(assignToUserId, TASKUTILITIESROLE_ACQUIRETASKSFROMOTHERS) == false)
                {
                    throw new InvalidOperationException("User can not acquire tasks from other users");
                }
            }
            else
            {
                // they're assinging tasks to someone else, check they can do this
                if (_memberRepo.UserIsInRole((Guid)context.CurrentUserId, TASKUTILITIESROLE_ASSIGNTASKSTOOTHERS) == false)
                {
                    throw new InvalidOperationException("User can not assign tasks to other users");
                }
            }

            if (parameters["assigntorosterid"] != null && string.IsNullOrEmpty(parameters["assigntorosterid"].ToString()) == false)
            {
                // take direction from the caller on which roster this is meant to go across to, so we can work out the end time
                string assigntoroster = parameters["assigntorosterid"].ToString();

                rosterId = int.Parse(assigntoroster);
            }
            else
            {
                // nothing specified, so we'll take our best guess at the roster they are wanting to go to
                var buckets = _hierarchyRepo.GetBucketsForUser(assignToUserId, null, HierarchyBucketLoadInstructions.None);

                foreach (var b in (from p in buckets where p.IsPrimary == true select p))
                {
                    var roster = _rosterRepo.GetForHierarchies(new int[] { b.HierarchyId }, RosterLoadInstructions.None);

                    if (roster.Count() == 0)
                    {
                        continue;
                    }

                    rosterId = roster.First().Id;
                    break;
                }

                if (rosterId.HasValue == false)
                {
                    foreach (var b in (from p in buckets where p.IsPrimary == false select p))
                    {
                        var roster = _rosterRepo.GetForHierarchies(new int[] { b.HierarchyId }, RosterLoadInstructions.None);

                        if (roster.Count() == 0)
                        {
                            continue;
                        }

                        rosterId = roster.First().Id;
                        break;
                    }
                }
            }

            JArray tasks = (JArray)parameters["tasks"];

            DateTime utcNow = DateTime.UtcNow;

            using (TransactionScope scope = new TransactionScope())
            {
                foreach (JObject t in tasks)
                {
                    var task = _taskRepo.GetById(Guid.Parse(t["id"].ToString()), RepositoryInterfaces.Enums.ProjectJobTaskLoadInstructions.None);

                    if (canContinueWithTask(task) == false)
                    {
                        continue;
                    }

                    task.StatusDate = utcNow;
                    if (toOriginal == false)
                    {
                        task.OwnerId = assignToUserId;
                    }
                    else
                    {
                        task.OwnerId = task.OriginalOwnerId;
                    }

                    if (rosterId.HasValue == true)
                    {
                        // so we need to work out a new ends on time for the tasks
                        var endsOn = _scheduleManagement.GetEndOfCurrentShiftInTimezone(rosterId.Value, TimeZoneInfo.Utc);

                        // we don't allow an ends on to be moved back as sometimes a user maybe from
                        // an earlier roster and be working overtime, so only forwards updates to ends on
                        if (endsOn > task.EndsOn)
                        {
                            task.EndsOn = endsOn;
                        }
                    }

                    _taskRepo.Save(task, false, false);
                }

                scope.Complete();
            }

            JObject result = new JObject();
            result["success"] = true;

            return result;
        }

        protected JToken Cancel(JToken parameters, IExecutionMethodHandlerContext context)
        {
            Guid assignToUserId = (Guid)context.CurrentUserId;

            // they're acquiring someone else's tasks and assigning to themselves, check their allowed to do this
            if (_memberRepo.UserIsInRole(assignToUserId, TASKUTILITIESROLE_CANCEL) == false)
            {
                throw new InvalidOperationException("User can not cancel tasks");
            }

            JArray tasks = (JArray)parameters["tasks"];

            DateTime utcNow = DateTime.UtcNow;

            using (TransactionScope scope = new TransactionScope())
            {
                foreach (JObject t in tasks)
                {
                    var task = _taskRepo.GetById(Guid.Parse(t["id"].ToString()), RepositoryInterfaces.Enums.ProjectJobTaskLoadInstructions.None);

                    if (canContinueWithTask(task) == false)
                    {
                        continue;
                    }

                    task.Status = (int)ProjectJobTaskStatus.Cancelled;
                    task.DateCompleted = utcNow;
                    task.StatusDate = utcNow;

                    _taskRepo.Save(task, false, false);
                }

                scope.Complete();
            }

            JObject result = new JObject();
            result["success"] = true;

            return result;
        }

        protected JToken Finish(JToken parameters, IExecutionMethodHandlerContext context)
        {
            Guid assignToUserId = (Guid)context.CurrentUserId;

            if (parameters["authentication"] != null)
            {
                // if authentication is required, we are going to override the current user and 
                // use the authenticated user as who gets the tasks
                assignToUserId = AuthenticatedUser(parameters["authentication"]);
            }

            // they're acquiring someone else's tasks and assigning to themselves, check their allowed to do this
            if (_memberRepo.UserIsInRole(assignToUserId, TASKUTILITIESROLE_FINISHBYMANAGEMENT) == false)
            {
                throw new InvalidOperationException("User can not finish tasks");
            }

            JArray tasks = (JArray)parameters["tasks"];

            DateTime utcNow = DateTime.UtcNow;

            bool aquireTasks = false;
            if (bool.Parse(parameters["aquiretaskswhenfinishing"].ToString()) == true)
            {
                aquireTasks = true;
            }

            ProjectJobTaskStatus status = ProjectJobTaskStatus.FinishedByManagement;

            if (parameters["type"].ToString() == "finishcomplete")
            {
                status = ProjectJobTaskStatus.FinishedComplete;
            }

            using (TransactionScope scope = new TransactionScope())
            {
                foreach (JObject t in tasks)
                {
                    var task = _taskRepo.GetById(Guid.Parse(t["id"].ToString()), RepositoryInterfaces.Enums.ProjectJobTaskLoadInstructions.None);

                    if (canContinueWithTask(task) == false)
                    {
                        continue;
                    }

                    task.Status = (int)status;
                    task.DateCompleted = utcNow;
                    task.StatusDate = utcNow;

                    if (aquireTasks == true)
                    {
                        task.OwnerId = assignToUserId;
                    }

                    _changeRepo.DeleteOpenRequestsByTaskId(task.Id);
                    _taskRepo.SetTaskStatusWhenFinishing(task);

                    _taskRepo.Save(task, false, false);
                }

                scope.Complete();
            }

            JObject result = new JObject();
            result["success"] = true;

            return result;
        }

        protected JToken Delete(JToken parameters, IExecutionMethodHandlerContext context)
        {
            Guid assignToUserId = (Guid)context.CurrentUserId;

            // they're acquiring someone else's tasks and assigning to themselves, check their allowed to do this
            if (_memberRepo.UserIsInRole(assignToUserId, TASKUTILITIESROLE_DELETE) == false)
            {
                throw new InvalidOperationException("User can not delete tasks");
            }

            JArray tasks = (JArray)parameters["tasks"];

            DateTime utcNow = DateTime.UtcNow;

            List<Guid> ids = new List<Guid>();
            foreach (JObject t in tasks)
            {
                var task = _taskRepo.GetById(Guid.Parse(t["id"].ToString()), RepositoryInterfaces.Enums.ProjectJobTaskLoadInstructions.None);

                if (canContinueWithTask(task) == false && task.Status != (int)ProjectJobTaskStatus.Cancelled)
                {
                    continue;
                }

                ids.Add(task.Id);
            }

            _taskRepo.Remove(ids.ToArray());

            JObject result = new JObject();
            result["success"] = true;

            return result;
        }

        protected JToken ClearStartTime(JToken parameters, IExecutionMethodHandlerContext context)
        {
            Guid assignToUserId = (Guid)context.CurrentUserId;

            // they're acquiring someone else's tasks and assigning to themselves, check their allowed to do this
            if (_memberRepo.UserIsInRole(assignToUserId, TASKUTILITIESROLE_CLEARSTARTTIME) == false)
            {
                throw new InvalidOperationException("User can not clear start time on tasks");
            }

            JArray tasks = (JArray)parameters["tasks"];

            DateTime utcNow = DateTime.UtcNow;

            foreach (JObject t in tasks)
            {
                var task = _taskRepo.GetById(Guid.Parse(t["id"].ToString()), RepositoryInterfaces.Enums.ProjectJobTaskLoadInstructions.None);

                if (canContinueWithTask(task) == false)
                {
                    continue;
                }

                task.StartTimeUtc = null;
                task.LateAfterUtc = null;

                _taskRepo.Save(task, false, false);
            }

            JObject result = new JObject();
            result["success"] = true;

            return result;
        }

        protected JToken GetClaimableTasks(JToken parameters, IExecutionMethodHandlerContext context)
        {
            JObject result = new JObject();

            Guid assignToUserId = (Guid)context.CurrentUserId;

            var hierarchies = _hierarchyRepo.GetAllHierarchies(HierarchyLoadInstructions.Rosters);
            var userBuckets = _hierarchyRepo.GetBucketsForUser(assignToUserId, null, HierarchyBucketLoadInstructions.None);

            JArray bs = new JArray();
            result.Add("buckets", bs);

            foreach (var hierarchyId in (from b in userBuckets select b.HierarchyId).Distinct())
            {
                // The Hierarchy must have a Roster which must have ProvidesClaimableDutyList ticked
                var roster = (from h in hierarchies where h.Id == hierarchyId select h.Rosters.FirstOrDefault()).FirstOrDefault();
                if (!(roster?.ProvidesClaimableDutyList ?? false))
                {
                    continue;
                }
                var buckets = _hierarchyRepo.GetBucketsForHierarchy(hierarchyId, HierarchyBucketLoadInstructions.BucketRole | HierarchyBucketLoadInstructions.User);

                foreach (var bucket in (from b in buckets where b.TasksCanBeClaimed == true select b))
                {
                    var tasks = _taskRepo.GetForBucket(bucket.Id, true, false, TaskStatusFilter.All, ProjectJobTaskLoadInstructions.Users | ProjectJobTaskLoadInstructions.TaskType);

                    if (tasks.Count == 0)
                    {
                        continue;
                    }

                    if ((from t in tasks where t.OwnerId != assignToUserId select t).Count() == 0)
                    {
                        // the tasks are already owned by the user
                        continue;
                    }

                    if ((from t in tasks where t.OwnerId == t.OriginalOwnerId select t).Count() == 0)
                    {
                        // all of the tasks have already be reassigned
                        continue;
                    }

                    JObject b = new JObject();
                    b["id"] = bucket.Id;
                    b["name"] = bucket.Name;
                    b["roleid"] = bucket.RoleId;
                    b["rolename"] = bucket.HierarchyRole.Name;
                    b["hierarchyId"] = bucket.HierarchyId;
                    b["rosterId"] = roster.Id;
                    b["userId"] = bucket.UserId?.ToString() ?? "";
                    b["userName"] = bucket.UserData?.Name ?? "";

                    bs.Add(b);

                    JArray ts = new JArray();
                    b.Add("tasks", ts);

                    foreach (var task in tasks)
                    {
                        if (task.OwnerId != task.OriginalOwnerId)
                        {
                            // this task has already been reassigned
                            continue;
                        }

                        JObject t = new JObject();

                        t["id"] = task.Id.ToString();
                        t["name"] = task.Name;
                        t["description"] = task.Description;
                        if (task.SiteId.HasValue)
                        {
                            t["siteid"] = task.SiteId.ToString();
                            t["sitename"] = task.Site.Name;
                        }
                        t["tasktypeid"] = task.TaskTypeId.ToString();
                        t["tasktypename"] = task.ProjectJobTaskType.Name;

                        ts.Add(t);
                    }
                }
            }

            return result;
        }
    }
}
