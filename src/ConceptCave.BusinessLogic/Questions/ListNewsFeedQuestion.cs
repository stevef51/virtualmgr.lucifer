﻿using ConceptCave.Checklist.Editor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.BusinessLogic.Questions
{
    public enum NewsFeedDisplayAs
    {
        AlertSuccess,
        AlertError,
        AlertWarning,
        AlertInfo,
        NumberedList,
        BlockquoteList
    }

    public class ListNewsFeedQuestion : Question
    {
        public NewsFeedDisplayAs ViewAs { get; set; }
        public int ItemCount { get; set; }
        public string Channels { get; set; }

        public override object DefaultAnswer
        {
            get
            {
                return null;
            }
            set
            {

            }
        }

        public override bool IsAnswerable
        {
            get
            {
                return false;
            }
        }

        public override Checklist.Interfaces.IAnswer CreateBlankAnswer(Checklist.Interfaces.IPresentedQuestion presentedQuestion)
        {
            return null;
        }

        public override void Encode(Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode<NewsFeedDisplayAs>("ViewAs", ViewAs);
            encoder.Encode<int>("ItemCount", ItemCount);
            encoder.Encode<string>("Channels", Channels);
        }

        public override void Decode(Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            ViewAs = decoder.Decode<NewsFeedDisplayAs>("ViewAs");
            ItemCount = decoder.Decode<int>("ItemCount");
            Channels = decoder.Decode<string>("Channels");
        }

        public override List<System.Collections.Specialized.NameValueCollection> ToNameValuePairs()
        {
            var r = base.ToNameValuePairs();

            var result = r[0];

            result.Add("viewas", ViewAs.ToString());
            result.Add("itemcount", ItemCount.ToString());
            result.Add("channels", Channels);
 
            return r;
        }

        public override void FromNameValuePairs(System.Collections.Specialized.NameValueCollection pairs)
        {
            base.FromNameValuePairs(pairs);

            foreach (var name in pairs.AllKeys)
            {
                string n = name.ToLower();

                switch (n)
                {
                    case "viewas":
                        ViewAs = (NewsFeedDisplayAs)Enum.Parse(typeof(NewsFeedDisplayAs), pairs[name]);
                        break;
                    case "itemcount":
                        ItemCount = int.Parse(pairs[name]);
                        break;
                    case "channels":
                        Channels = pairs[name];
                        break;
                }
            }
        }
    }
}
