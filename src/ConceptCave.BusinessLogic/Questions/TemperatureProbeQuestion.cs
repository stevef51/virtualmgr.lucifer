﻿using ConceptCave.Checklist.Editor;
using ConceptCave.Checklist.RunTime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Core.Coding;
using ConceptCave.Checklist.Interfaces;

namespace ConceptCave.BusinessLogic.Questions
{
    public enum TemperatureReadingType
    {
        Realtime,
        MaximumReached
    }

    public class TemperatureProbeQuestion : Question
    {
        public bool DisplayAsFarenheit { get; set; }
        public int RefreshPeriod { get; set; }

        public decimal MinimumPassReading { get; set; }
        public decimal MaximumPassReading { get; set; }

        public int DisconnectProbeTimeout { get; set; }

        public string AllowedProbeProviders { get; set; }
        public string AllowedProbeSerialNumbers { get; set; }

        public int ProbeOutOfRangeTimeout { get; set; }

        public TemperatureReadingType TemperatureReading { get; set; } = TemperatureReadingType.Realtime;

        public override object DefaultAnswer
        {
            get
            {
                return _defaultAnswer;
            }
            set
            {
                _defaultAnswer = value;
            }
        }

        public override Checklist.Interfaces.IAnswer CreateBlankAnswer(Checklist.Interfaces.IPresentedQuestion presentedQuestion)
        {
            var result = new TemperatureProbeAnswer() { PresentedQuestion = presentedQuestion };

            if(DefaultAnswer != null && DefaultAnswer is string)
            {
                result.AnswerValue = decimal.Parse((string)DefaultAnswer);
            }
            else if(DefaultAnswer != null && DefaultAnswer is decimal)
            {
                result.AnswerValue = (decimal)DefaultAnswer;
            }
            else if(DefaultAnswer != null && DefaultAnswer is int)
            {
                result.AnswerValue = (decimal)DefaultAnswer;
            }
            else if(DefaultAnswer != null && DefaultAnswer is TemperatureProbeAnswer)
            {
                result.AnswerValue = ((TemperatureProbeAnswer)DefaultAnswer).AnswerValue;
            }

            return result;
        }

        public override void Encode(Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);
            encoder.Encode<bool>("DisplayAsFarenheit", DisplayAsFarenheit);
            encoder.Encode<int>("RefreshPeriod", RefreshPeriod);

            encoder.Encode<decimal>("MinimumPassReading", MinimumPassReading);
            encoder.Encode<decimal>("MaximumPassReading", MaximumPassReading);

            encoder.Encode<int>("DisconnectProbeTimeout", DisconnectProbeTimeout);
            if (AllowedProbeProviders != null && AllowedProbeProviders.Length > 0)
            {
                encoder.Encode<string>("AllowedProbeProviders", AllowedProbeProviders);
            }
            if (AllowedProbeSerialNumbers != null && AllowedProbeSerialNumbers.Length > 0)
            {
                encoder.Encode<string>("AllowedProbeSerialNumbers", AllowedProbeSerialNumbers);
            }
            encoder.Encode<int>("ProbeOutOfRangeTimeout", ProbeOutOfRangeTimeout);
            encoder.Encode<TemperatureReadingType>("TemperatureReading", TemperatureReading);
        }

        public override void Decode(Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            DisplayAsFarenheit = decoder.Decode<bool>("DisplayAsFarenheit");
            RefreshPeriod = decoder.Decode<int>("RefreshPeriod");

            MinimumPassReading = decoder.Decode<decimal>("MinimumPassReading");
            MaximumPassReading = decoder.Decode<decimal>("MaximumPassReading");

            DisconnectProbeTimeout = decoder.Decode<int>("DisconnectProbeTimeout");
            AllowedProbeProviders = decoder.Decode<string>("AllowedProbeProviders", null);
            AllowedProbeSerialNumbers = decoder.Decode<string>("AllowedProbeSerialNumbers", null);
            ProbeOutOfRangeTimeout = decoder.Decode<int>("ProbeOutOfRangeTimeout");
            TemperatureReading = decoder.Decode<TemperatureReadingType>("TemperatureReading");
        }

        public bool IsInRange(object value)
        {
            decimal v = -1000;
            if(value is decimal)
            {
                v = (decimal)value;
            }
            else if(value is decimal?)
            {
                if(((decimal?)value).HasValue == true)
                {
                    v = ((decimal?)value).Value;
                }
            }
            else if(value is TemperatureProbeAnswer)
            {
                return IsInRange(((TemperatureProbeAnswer)value).Celcius);
            }

            return v >= MinimumPassReading && v <= MaximumPassReading;
        }
    }

    public class TemperatureProbeAnswer : Answer, IAnswerExtraValues
    {
        protected decimal? _reading;
        protected decimal? _rawCelcius;
        protected string _probeSerialNumber;            // The probe that took the reading

        protected bool _decoding;

        public override void Decode(Core.Coding.IDecoder decoder)
        {
            _decoding = true;
            try
            {
                base.Decode(decoder);
                _rawCelcius = decoder.Decode<decimal?>("RawCelcius");
                _probeSerialNumber = decoder.Decode<string>("ProbeSerialNumber", null);
            }
            finally
            {
                _decoding = false;
            }
        }

        public override void Encode(IEncoder encoder)
        {
            base.Encode(encoder);
            if (_rawCelcius.HasValue)
                encoder.Encode("RawCelcius", _rawCelcius);
            if (_probeSerialNumber != null)
                encoder.Encode("ProbeSerialNumber", _probeSerialNumber);
        }

        public decimal? Celcius
        {
            get
            {
                return _reading;
            }
            set
            {
                UtcDateAnswered = DateTime.UtcNow;
                _reading = value;

                if (_decoding == true)
                {
                    return;
                }

                if (_reading.HasValue == false)
                {
                    this.PassFail = null;
                    return;
                }

                var q = (TemperatureProbeQuestion)this.PresentedQuestion.Question;

                if (_reading >= q.MinimumPassReading && _reading <= q.MaximumPassReading)
                {
                    this.PassFail = true;
                }
                else
                {
                    this.PassFail = false;
                }
            }
        }

        public string ProbeSerialNumber
        {
            get => _probeSerialNumber;
            set
            {
                if (_decoding == true)
                    return;

                _probeSerialNumber = value;
            }

        }
        public decimal? RawCelcius
        {
            get => _rawCelcius;
            set
            {
                if (_decoding == true)
                    return;

                _rawCelcius = value;
            }
        }

        public override object AnswerValue
        {
            get
            {
                return Celcius;
            }
            set
            {
                if (value != null)
                {
                    Celcius = (decimal)value;
                }
                else
                {
                    Celcius = null;
                }
            }
        }

        public override string AnswerAsString
        {
            get { return Celcius.ToString(); }
        }

        public Dictionary<string, object> ExtraValues
        {
            get
            {
                var dict = new Dictionary<string, object>();
                dict.Add("RawCelcius", _rawCelcius);
                dict.Add("ProbeSerialNumber", _probeSerialNumber);
                return dict;
            }
            set
            {
                object o = null;
                if (value.TryGetValue("RawCelcius", out o))
                {
                    try
                    {
                        _rawCelcius = (decimal?)o.ConvertToType(typeof(decimal));
                    }
                    catch
                    {
                        _rawCelcius = null;
                    }
                }
                if (value.TryGetValue("ProbeSerialNumber", out o))
                {
                    _probeSerialNumber = o.ToString();
                }
            }
        }

        public bool IsInRange()
        {
            return ((TemperatureProbeQuestion)this.PresentedQuestion.Question).IsInRange(this);
        }
    }
}
