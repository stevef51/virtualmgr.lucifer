﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using ConceptCave.Checklist.Editor;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using Newtonsoft.Json.Linq;
using VirtualMgr.Common;

namespace ConceptCave.BusinessLogic.Questions
{
    public class CompanySummaryQuestion : Question
    {
        public bool ShowUserList { get; set; }
        public bool CanCreateUsers { get; set; }
        public List<int> AllowedUserTypes { get; set; }

        public CompanySummaryQuestion()
        {
            AllowedUserTypes = new List<int>();
        }

        public override object DefaultAnswer
        {
            get
            {
                return null;
            }
            set
            {

            }
        }

        public override bool IsAnswerable
        {
            get
            {
                return false;
            }
        }

        public override Checklist.Interfaces.IAnswer CreateBlankAnswer(Checklist.Interfaces.IPresentedQuestion presentedQuestion)
        {
            return null;
        }

        public override void Encode(Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode<bool>("ShowUserList", ShowUserList);
            encoder.Encode<bool>("CanCreateUsers", CanCreateUsers);
            encoder.Encode<List<int>>("AllowedUserTypes", AllowedUserTypes);
        }

        public override void Decode(Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            ShowUserList = decoder.Decode<bool>("ShowUserList", true);
            CanCreateUsers = decoder.Decode<bool>("CanCreateUsers", false);
            AllowedUserTypes = decoder.Decode<List<int>>("AllowedUserTypes", new List<int>());

            if (AllowedUserTypes == null)
            {
                AllowedUserTypes = new List<int>();
            }
        }

        public override List<System.Collections.Specialized.NameValueCollection> ToNameValuePairs()
        {
            var r = base.ToNameValuePairs();

            var result = r[0];

            result.Add("showuserlist", ShowUserList.ToString());
            result.Add("cancreateusers", CanCreateUsers.ToString());
            result.Add("allowedusertypes", string.Join(",", AllowedUserTypes.ToArray()));

            return r;
        }

        public override void FromNameValuePairs(System.Collections.Specialized.NameValueCollection pairs)
        {
            base.FromNameValuePairs(pairs);

            foreach (var name in pairs.AllKeys)
            {
                string n = name.ToLower();

                switch (n)
                {
                    case "showuserlist":
                        bool show = false;
                        if (bool.TryParse(pairs[name], out show) == true)
                        {
                            this.ShowUserList = show;
                        }
                        break;
                    case "cancreateusers":
                        bool create = false;
                        if (bool.TryParse(pairs[name], out create) == true)
                        {
                            this.CanCreateUsers = create;
                        }
                        break;
                    case "allowedusertypes":
                        AllowedUserTypes = pairs[name].Split(',').Select(s => int.Parse(s)).ToList();
                        break;
                }
            }
        }
    }

    public class CompanySummaryQuestionExecutionHandler : IExecutionMethodHandler
    {
        protected ICompanyRepository _companyRepo;
        protected IMembershipRepository _memberRepo;
        protected IMembershipTypeRepository _memberTypeRepo;
        protected ICountryStateRepository _countryStateRepo;

        public CompanySummaryQuestionExecutionHandler(IMembershipRepository memberRepo, IMembershipTypeRepository memberTypeRepo, ICompanyRepository companyRepo, ICountryStateRepository countryStateRepo)
        {
            _memberRepo = memberRepo;
            _memberTypeRepo = memberTypeRepo;
            _companyRepo = companyRepo;
            _countryStateRepo = countryStateRepo;
        }

        public string Name
        {
            get { return "CompanySummary"; }
        }

        public Newtonsoft.Json.Linq.JToken Execute(string method, Newtonsoft.Json.Linq.JToken parameters, IExecutionMethodHandlerContext context)
        {
            switch (method)
            {
                case "GetSummary":
                    return GetSummary(parameters);
                case "GetUsers":
                    return GetUsers(parameters);
                case "NewUser":
                    return NewUser(parameters);
                case "UsernameExists":
                    return UsernameExists(parameters);

            }

            return null;
        }

        protected JToken GetSummary(JToken parameters)
        {
            string userIdString = parameters["userid"].ToString();

            Guid userId = Guid.Parse(userIdString);
            var user = _memberRepo.GetById(userId, RepositoryInterfaces.Enums.MembershipLoadInstructions.None);
            var tz = TimeZoneInfoResolver.ResolveWindows(user.TimeZone);

            if (user.CompanyId.HasValue == false)
            {
                return null;
            }

            bool includeUsers = bool.Parse(parameters["criteria"]["showuserlist"].ToString());
            bool cancreateusers = bool.Parse(parameters["criteria"]["cancreateusers"].ToString());
            JArray allowedUserTypes = (JArray)parameters["criteria"]["allowedusertypes"];

            var company = _companyRepo.GetById(user.CompanyId.Value, CompanyLoadInstructions.CountryState);

            JObject result = CompanyToJson(company);

            if (includeUsers == true)
            {
                if (cancreateusers == true)
                {
                    JArray usertypes = new JArray();
                    result["availableusertypes"] = usertypes;

                    var types = _memberTypeRepo.GetAll();

                    foreach (var token in allowedUserTypes)
                    {
                        int id = int.Parse(token.ToString());

                        var type = (from t in types where t.Id == id select t).First();

                        var ut = new JObject();
                        ut["id"] = type.Id;
                        ut["name"] = type.Name;

                        usertypes.Add(ut);
                    }
                }

                var users = GetUsers(parameters);

                result["users"] = users["users"];
            }

            return result;
        }

        public static JObject CompanyToJson(CompanyDTO company)
        {
            JObject result = new JObject();
            result["id"] = company.Id;
            result["name"] = company.Name;
            result["rating"] = company.Rating;

            JObject geo = new JObject();
            result["location"] = geo;
            geo["lat"] = company.Latitude;
            geo["lng"] = company.Longitude;

            JObject address = new JObject();
            result["address"] = address;

            address["street"] = company.Street;
            address["town"] = company.Town;
            address["stateid"] = company.CountryStateId;
            if (company.CountryStateId.HasValue == true)
            {
                address["state"] = company.CountryState.Name;
            }

            address["postcode"] = company.Postcode;

            return result;
        }

        protected JToken GetUsers(JToken parameters)
        {
            string userIdString = parameters["userid"].ToString();

            Guid userId = Guid.Parse(userIdString);
            var user = _memberRepo.GetById(userId, RepositoryInterfaces.Enums.MembershipLoadInstructions.None);
            var tz = TimeZoneInfoResolver.ResolveWindows(user.TimeZone);

            if (user.CompanyId.HasValue == false)
            {
                return null;
            }

            var users = _memberRepo.Search(null, 1, 50, MembershipLoadInstructions.AspNetMembership | MembershipLoadInstructions.MembershipType, user.CompanyId);

            JObject result = new JObject();

            JArray items = new JArray();
            result["users"] = items;

            foreach (var member in users)
            {
                JObject u = ConverToJson(member);

                items.Add(u);
            }

            return result;
        }

        protected JObject ConverToJson(DTO.DTOClasses.UserDataDTO member)
        {
            JObject u = new JObject();
            u["id"] = member.UserId;
            u["name"] = member.Name;
            u["type"] = member.UserType.Name;
            u["email"] = member.AspNetUser.Email;
            u["username"] = member.AspNetUser.UserName;
            return u;
        }

        protected JToken NewUser(JToken parameters)
        {
            string userIdString = parameters["userid"].ToString();

            Guid userId = Guid.Parse(userIdString);
            var user = _memberRepo.GetById(userId, RepositoryInterfaces.Enums.MembershipLoadInstructions.None);
            var tz = TimeZoneInfoResolver.ResolveWindows(user.TimeZone);

            if (user.CompanyId.HasValue == false)
            {
                return null;
            }

            string username = parameters["user"]["email"].ToString();
            string password = parameters["user"]["password"].ToString();

            string email = parameters["user"]["email"].ToString();
            int userType = int.Parse(parameters["user"]["type"].ToString());
            string name = parameters["user"]["name"].ToString();

            CompanyDTO company = null;

            if (user.CompanyId.HasValue == true)
            {
                company = _companyRepo.GetById(user.CompanyId.Value, CompanyLoadInstructions.CountryState);
            }

            UserDataDTO newUser = null;

            using (TransactionScope scope = new TransactionScope())
            {
                newUser = _memberRepo.Create(username, password, company == null ? null : company.CountryState.TimeZone, MembershipLoadInstructions.AspNetMembership);
                newUser.Name = name;
                newUser.AspNetUser.Email = email;
                newUser.UserTypeId = userType;
                newUser.CompanyId = user.CompanyId;

                _memberRepo.Save(newUser, false, true);

                scope.Complete();
            }

            newUser = _memberRepo.GetById(newUser.UserId, MembershipLoadInstructions.AspNetMembership | MembershipLoadInstructions.MembershipType);

            return ConverToJson(newUser);
        }

        protected JToken UsernameExists(JToken parameters)
        {
            string username = parameters["username"].ToString();

            var existing = _memberRepo.GetByUsername(username);

            JObject result = new JObject();
            result["exists"] = existing == null ? false : true;

            return result;
        }
    }
}
