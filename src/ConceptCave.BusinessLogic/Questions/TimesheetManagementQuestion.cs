﻿using ConceptCave.BusinessLogic.Json;
using ConceptCave.Checklist.Editor;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.BusinessLogic.Extensions;
using VirtualMgr.Common;

namespace ConceptCave.BusinessLogic.Questions
{
    public class TimesheetManagementQuestion : Question
    {
        public override bool IsAnswerable
        {
            get
            {
                return false;
            }
        }

        public override object DefaultAnswer
        {
            get
            {
                return null;
            }
            set
            {
            }
        }

        public override Checklist.Interfaces.IAnswer CreateBlankAnswer(Checklist.Interfaces.IPresentedQuestion presentedQuestion)
        {
            return null;
        }

        public override void Encode(Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);
        }

        public override void Decode(Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);
        }

        public override List<System.Collections.Specialized.NameValueCollection> ToNameValuePairs()
        {
            var r = base.ToNameValuePairs();

            var result = r[0];

            return r;
        }

        public override void FromNameValuePairs(System.Collections.Specialized.NameValueCollection pairs)
        {
            base.FromNameValuePairs(pairs);
        }
    }

    public class TimesheetManagementQuestionExecutionHandler : IExecutionMethodHandler
    {
        protected ITaskRepository _taskRepo;
        protected ITaskWorkLogRepository _taskWorklogRepo;
        protected IRosterRepository _rosterRepo;
        protected ITimesheetItemTypeRepository _timesheetTypeRepo;
        protected ITimesheetItemRepository _timesheetItemRepo;
        protected IHierarchyRepository _hierarchyRepo;
        protected IScheduledTaskRepository _scheduleRepo;

        public TimesheetManagementQuestionExecutionHandler(ITaskRepository taskRepo,
            ITaskWorkLogRepository taskWorklogRepo,
            IRosterRepository rosterRepo,
            ITimesheetItemTypeRepository timesheetTypeRepo,
            ITimesheetItemRepository timesheetItemRepo,
            IHierarchyRepository hierarchyRepo,
            IScheduledTaskRepository scheduleRepo)
        {
            _taskRepo = taskRepo;
            _taskWorklogRepo = taskWorklogRepo;
            _rosterRepo = rosterRepo;
            _timesheetTypeRepo = timesheetTypeRepo;
            _timesheetItemRepo = timesheetItemRepo;
            _hierarchyRepo = hierarchyRepo;
            _scheduleRepo = scheduleRepo;
        }

        public string Name
        {
            get { return "TimesheetManagement"; }
        }

        public Newtonsoft.Json.Linq.JToken Execute(string method, Newtonsoft.Json.Linq.JToken parameters, IExecutionMethodHandlerContext context)
        {
            switch (method)
            {
                case "CalculateTimesheet":
                    return CalculateTimesheet(parameters);
                case "GetTimesheetItemDetails":
                    return GetTimesheetItemDetails(parameters);
                case "Save":
                    return Save(parameters);
            }

            throw new InvalidOperationException(string.Format("{0} does not exist as a method on the TimesheetManagementExecutionHandler", method));

        }

        protected TimesheetItemTypeDTO CalculateType(DateTime localTime, HierarchyBucketDTO bucket, IList<TimesheetItemTypeDTO> types)
        {
            if (localTime.DayOfWeek == DayOfWeek.Saturday)
            {
                if (bucket != null && bucket.DefaultSaturdayTimesheetItemTypeId.HasValue)
                {
                    return (from t in types where t.Id == bucket.DefaultSaturdayTimesheetItemTypeId select t).First();
                }

                var satTypes = (from t in types where t.IsSaturdayPay == true select t);

                if (satTypes.Count() > 0)
                {
                    return satTypes.First();
                }
            }
            else if (localTime.DayOfWeek == DayOfWeek.Sunday)
            {
                if (bucket != null && bucket.DefaultSundayTimesheetItemTypeId.HasValue)
                {
                    return (from t in types where t.Id == bucket.DefaultSundayTimesheetItemTypeId select t).First();
                }

                var sunTypes = (from t in types where t.IsSundayPay == true select t);

                if (sunTypes.Count() > 0)
                {
                    return sunTypes.First();
                }
            }

            if (bucket != null && bucket.DefaultWeekDayTimesheetItemTypeId.HasValue)
            {
                return (from t in types where t.Id == bucket.DefaultWeekDayTimesheetItemTypeId select t).First();
            }

            var normalTypes = (from t in types where t.IsLeave == false && t.IsSaturdayPay == false && t.IsSundayPay == false select t);
            if (normalTypes.Count() > 0)
            {
                return normalTypes.First();
            }

            throw new InvalidOperationException("No timesheet item types were defined");
        }

        protected ProjectJobScheduleDayDTO CalculateShiftTime(RosterDTO roster, DateTime localDate, IList<ProjectJobTaskWorkLogDTO> clockins, IList<ProjectJobScheduleDayDTO> times)
        {
            int day = (int)localDate.DayOfWeek;

            var ts = from t in times where t.Day == day select t;

            ProjectJobScheduleDayDTO result = null;

            if (ts.Count() == 0)
            {
                // ok so either there is no 
                result = new ProjectJobScheduleDayDTO()
                {
                    StartTime = roster.StartTime,
                    EndTime = roster.EndTime,
                    LunchStart = DateTime.MinValue,
                    LunchEnd = DateTime.MinValue
                };

                if (clockins != null)
                {
                    // if tasks define a minimum duration for them, then we'll use that to
                    decimal duration = 0m;
                    foreach (var clock in (from c in clockins where c.DateCreated >= localDate.Date && c.DateCreated <= localDate.Date.AddDays(1) select c))
                    {
                        if (clock.ProjectJobTask.EstimatedDurationSeconds.HasValue == true)
                        {
                            duration += clock.ProjectJobTask.EstimatedDurationSeconds.Value;
                        }
                    }

                    if (duration > 0)
                    {
                        result.EndTime = roster.StartTime.AddSeconds((double)duration);
                    }
                }
            }
            else
            {
                result = ts.First();
            }

            return result;
        }

        protected void SetScheduledTimes(ProjectJobScheduleDayDTO day, TimesheetItemDTO item, DateTime currentDate, RosterDTO roster, TimeZoneInfo rosterTz)
        {
            item.ScheduledStartDate = TimeZoneInfo.ConvertTimeToUtc(currentDate.Date.Add(new TimeSpan(0, day.StartTime.Hour, day.StartTime.Minute, 0)), rosterTz);
            item.ScheduledEndDate = TimeZoneInfo.ConvertTimeToUtc(currentDate.Date.Add(new TimeSpan(0, day.EndTime.Hour, day.EndTime.Minute, 0)), rosterTz);

            if (item.ScheduledEndDate < item.ScheduledStartDate)
            {
                item.ScheduledEndDate = item.ScheduledEndDate.Value.AddDays(1);
            }

            item.ScheduledDuration = (decimal)item.ScheduledEndDate.Value.Subtract(item.ScheduledStartDate.Value).TotalSeconds;

            item.IsLunchPaid = roster.IncludeLunchInTimesheetDurations;
            item.LunchDuration = (decimal)day.LunchEnd.Subtract(day.LunchStart).TotalSeconds;
            item.ScheduledLunchStartDate = TimeZoneInfo.ConvertTimeToUtc(currentDate.Date.Add(new TimeSpan(0, day.LunchStart.Hour, day.LunchStart.Minute, 0)), rosterTz);
            item.ScheduledLunchEndDate = TimeZoneInfo.ConvertTimeToUtc(currentDate.Date.Add(new TimeSpan(0, day.LunchEnd.Hour, day.LunchEnd.Minute, 0)), rosterTz);

            if (item.ScheduledLunchEndDate < item.ScheduledLunchStartDate)
            {
                item.ScheduledLunchEndDate = item.ScheduledLunchEndDate.Value.AddDays(1);
            }

            if (roster.IncludeLunchInTimesheetDurations == false)
            {
                item.ScheduledDuration = item.ScheduledDuration - item.LunchDuration;
            }
        }

        protected List<TimesheetItemDTO> LoadOrCreateTimesheets(RosterDTO roster, int roleId, DateTime startDate, DateTime endDate, Guid ownerId, Guid createdByUserId)
        {
            var rosterTZ = TimeZoneInfoResolver.ResolveWindows(roster.Timezone);

            DateTime startDateUtc = TimeZoneInfo.ConvertTime(startDate, rosterTZ, TimeZoneInfo.Utc);
            DateTime endDateUtc = TimeZoneInfo.ConvertTime(endDate, rosterTZ, TimeZoneInfo.Utc);

            // so we need to get all of the tasks associated with that owner in that roster between those dates
            var taskClockins = _taskWorklogRepo.GetBetweenDates(roster.Id, ownerId, startDateUtc, endDateUtc,
                TaskStatusFilter.FinishedByManagement |
                TaskStatusFilter.FinishedBySystem |
                TaskStatusFilter.FinishedComplete |
                TaskStatusFilter.FinishedIncomplete,
                RepositoryInterfaces.Enums.ProjectJobTaskWorkLogLoadInstructions.Task, true);

            var buckets = _hierarchyRepo.GetBucketsForUser(ownerId, roster.HierarchyId, null, RepositoryInterfaces.Enums.HierarchyBucketLoadInstructions.None);
            buckets = (from b in buckets where b.RoleId == roleId select b).ToList();

            taskClockins = (from t in taskClockins where t.ProjectJobTask.HierarchyBucketId == buckets.First().Id select t).ToList();

            Dictionary<int, IList<ProjectJobScheduleDayDTO>> dayTimesCache = new Dictionary<int, IList<ProjectJobScheduleDayDTO>>();

            foreach (var bucket in buckets)
            {
                var times = _scheduleRepo.GetDaySchedule(roster.Id, bucket.RoleId, null);

                dayTimesCache.Add(bucket.RoleId, times);
            }

            Dictionary<string, TimesheetItemDTO> timesheetCache = new Dictionary<string, TimesheetItemDTO>();

            var savedSheets = _timesheetItemRepo.GetForRosterBetweenDates(roster.Id, ownerId, startDateUtc, endDateUtc, RepositoryInterfaces.Enums.TimesheetItemLoadInstructions.Worklogs);

            foreach (var s in savedSheets)
            {
                if (s.HierarchyRoleId.HasValue == true && s.HierarchyRoleId.Value != roleId)
                {
                    continue;
                }

                var d = TimeZoneInfo.ConvertTime(s.Date, TimeZoneInfo.Utc, rosterTZ);

                timesheetCache.Add(d.ToString() + "_" + s.TimesheetItemTypeId.ToString(), s);
            }

            var timesheetTypes = _timesheetTypeRepo.GetAll();

            var currentDate = startDate;
            while (currentDate <= endDate)
            {
                DateTime utcDate = TimeZoneInfo.ConvertTime(currentDate, rosterTZ, TimeZoneInfo.Utc);

                var nextDate = currentDate.AddDays(1);

                HierarchyBucketDTO bucket = null;
                if (buckets.Count > 0)
                {
                    bucket = buckets.First();
                }

                var timesheetItemTypeId = CalculateType(currentDate, bucket, timesheetTypes).Id;

                TimesheetItemDTO dto = null;
                if (timesheetCache.TryGetValue(currentDate.ToString() + "_" + timesheetItemTypeId.ToString(), out dto) == false)
                {
                    dto = new TimesheetItemDTO()
                    {
                        __IsNew = true,
                        DateCreated = DateTime.UtcNow,
                        Date = utcDate,
                        ActualStartDate = DateTime.MaxValue,
                        ActualEndDate = DateTime.MinValue,
                        ActualDuration = 0,
                        TimesheetItemTypeId = timesheetItemTypeId,
                        PayAsTimesheetItemTypeId = timesheetItemTypeId,
                        ApprovedStartDate = DateTime.MaxValue,
                        ApprovedEndDate = DateTime.MinValue,
                        ApprovedDuration = 0,
                        ApprovedByUserId = createdByUserId,
                        UserId = ownerId,
                        RosterId = roster.Id,
                        HierarchyRoleId = roleId
                    };

                    if (buckets.Count > 0)
                    {
                        IList<ProjectJobScheduleDayDTO> times = null;

                        if (dayTimesCache.TryGetValue(buckets.First().RoleId, out times) == true)
                        {
                            var time = CalculateShiftTime(roster, currentDate, taskClockins, times);
                            SetScheduledTimes(time, dto, currentDate, roster, rosterTZ);
                        }
                    }

                    timesheetCache.Add(currentDate.ToString() + "_" + timesheetItemTypeId.ToString(), dto);
                }

                // we are going to appropriate hours to the days that the user clocked in on
                foreach (var c in (from w in taskClockins where w.DateCreated >= utcDate && w.DateCreated < utcDate.AddDays(1) select w))
                {
                    if ((from d in dto.TimesheetedTaskWorkLogs select d.ProjectJobTaskWorkLogId).Contains(c.Id))
                    {
                        // already accounted for (this can occur if the dto has been saved)
                        continue;
                    }

                    if (c.ActualDuration.HasValue)
                    {
                        dto.ActualDuration += c.ActualDuration.Value;
                    }

                    if (c.DateCreated < dto.ActualStartDate)
                    {
                        dto.ActualStartDate = c.DateCreated;

                        DateTime scheduledStart = c.DateCreated;
                        if (dto.ScheduledStartDate.HasValue)
                        {
                            scheduledStart = dto.ScheduledStartDate.Value;
                        }

                        switch ((RosterTimesheetSelectionMethod)roster.TimesheetApprovedSourceMethod)
                        {
                            case RosterTimesheetSelectionMethod.ActualTimes:
                                dto.ApprovedStartDate = c.DateCreated;
                                break;
                            case RosterTimesheetSelectionMethod.ScheduledTimes:
                                dto.ApprovedStartDate = scheduledStart;
                                break;
                            case RosterTimesheetSelectionMethod.Minimised:
                                if (c.DateCreated > scheduledStart)
                                {
                                    dto.ApprovedStartDate = c.DateCreated;
                                }
                                else
                                {
                                    dto.ApprovedStartDate = scheduledStart;
                                }
                                break;
                            case RosterTimesheetSelectionMethod.Maximised:
                                if (c.DateCreated > scheduledStart)
                                {
                                    dto.ApprovedStartDate = scheduledStart;
                                }
                                else
                                {
                                    dto.ApprovedStartDate = c.DateCreated;
                                }
                                break;
                        }
                    }

                    if (c.DateCompleted.HasValue &&
                        c.DateCompleted > dto.ActualEndDate &&
                        c.ProjectJobTask.Status != (int)ProjectJobTaskStatus.FinishedBySystem)
                    {
                        dto.ActualEndDate = c.DateCompleted.Value;

                        DateTime scheduledEnd = c.DateCompleted.Value;
                        if (dto.ScheduledEndDate.HasValue)
                        {
                            scheduledEnd = dto.ScheduledEndDate.Value;
                        }

                        switch ((RosterTimesheetSelectionMethod)roster.TimesheetApprovedSourceMethod)
                        {
                            case RosterTimesheetSelectionMethod.ActualTimes:
                                dto.ApprovedEndDate = c.DateCompleted.Value;
                                break;
                            case RosterTimesheetSelectionMethod.ScheduledTimes:
                                dto.ApprovedEndDate = scheduledEnd;
                                break;
                            case RosterTimesheetSelectionMethod.Minimised:
                                if (c.DateCompleted.Value > scheduledEnd)
                                {
                                    dto.ApprovedEndDate = scheduledEnd;
                                }
                                else
                                {
                                    dto.ApprovedEndDate = c.DateCompleted.Value;
                                }
                                break;
                            case RosterTimesheetSelectionMethod.Maximised:
                                if (c.DateCompleted.Value > scheduledEnd)
                                {
                                    dto.ApprovedEndDate = c.DateCompleted.Value;
                                }
                                else
                                {
                                    dto.ApprovedEndDate = scheduledEnd;
                                }
                                break;
                        }
                    }
                    else if (c.ProjectJobTask.Status == (int)ProjectJobTaskStatus.FinishedBySystem &&
                        dto.ScheduledEndDate.HasValue == true &&
                        roster.TimesheetUseScheduledEndWhenFinishedBySystem == true)
                    {
                        dto.ActualEndDate = c.DateCompleted.Value;
                        dto.ApprovedEndDate = dto.ScheduledEndDate.Value;
                    }

                    TimesheetedTaskWorkLogDTO sheeted = new TimesheetedTaskWorkLogDTO() { __IsNew = true, ProjectJobTaskWorkLogId = c.Id };
                    dto.TimesheetedTaskWorkLogs.Add(sheeted);
                }

                currentDate = nextDate;
            }

            // merge in the leave side of things now
            var leaveType = (from l in timesheetTypes where l.IsLeave == true select l);
            if (leaveType.Count() > 0)
            {
                foreach (var bucket in buckets)
                {
                    currentDate = startDate;
                    while (currentDate <= endDate)
                    {
                        var nextDate = currentDate.AddDays(1);

                        var currentDateUTC = TimeZoneInfo.ConvertTimeToUtc(currentDate, rosterTZ);
                        var nextDateUTC = TimeZoneInfo.ConvertTimeToUtc(nextDate, rosterTZ);

                        var leave = _hierarchyRepo.GetBucketOverrides(currentDateUTC, ownerId, bucket.Id);

                        foreach (var l in leave)
                        {
                            TimesheetItemDTO dto = null;
                            if (timesheetCache.TryGetValue(currentDate.ToString() + "_" + leaveType.First().Id.ToString(), out dto) == false)
                            {
                                dto = new TimesheetItemDTO()
                                {
                                    __IsNew = true,
                                    DateCreated = DateTime.UtcNow,
                                    Date = currentDateUTC,
                                    ActualStartDate = DateTime.MaxValue,
                                    ActualEndDate = DateTime.MinValue,
                                    ActualDuration = 0,
                                    TimesheetItemTypeId = leaveType.First().Id,
                                    ApprovedStartDate = DateTime.MaxValue,
                                    ApprovedEndDate = DateTime.MinValue,
                                    ApprovedDuration = 0,
                                    ApprovedByUserId = createdByUserId,
                                    UserId = ownerId,
                                    RosterId = roster.Id,
                                    HierarchyBucketOverrideTypeId = l.HierarchyBucketOverrideTypeId
                                };

                                timesheetCache.Add(currentDate.ToString() + "_" + leaveType.First().Id.ToString(), dto);
                            }

                            IList<ProjectJobScheduleDayDTO> times = null;

                            if (dayTimesCache.TryGetValue(bucket.RoleId, out times) == true)
                            {
                                var time = CalculateShiftTime(roster, currentDate, null, times);
                                SetScheduledTimes(time, dto, currentDate, roster, rosterTZ);
                            }

                            if (dto.ScheduledStartDate.HasValue == false || dto.ScheduledEndDate.HasValue == false)
                            {
                                dto.ActualStartDate = TimeZoneInfo.ConvertTimeToUtc(currentDate.Date.AddSeconds(roster.StartTime.Subtract(roster.StartTime.Date).TotalSeconds), rosterTZ);
                                dto.ActualEndDate = TimeZoneInfo.ConvertTimeToUtc(currentDate.Date.AddSeconds(roster.EndTime.Subtract(roster.EndTime.Date).TotalSeconds), rosterTZ);
                                dto.ActualDuration = (decimal)dto.ActualEndDate.Subtract(dto.ActualStartDate).TotalSeconds;
                                dto.ApprovedStartDate = dto.ActualStartDate;
                                dto.ApprovedEndDate = dto.ActualEndDate;
                                dto.ApprovedDuration = dto.ActualDuration.Value;
                            }
                            else
                            {
                                dto.ActualStartDate = dto.ScheduledStartDate.Value;
                                dto.ActualEndDate = dto.ScheduledEndDate.Value;
                                dto.ActualDuration = dto.ScheduledDuration;
                                dto.ApprovedStartDate = dto.ActualStartDate;
                                dto.ApprovedEndDate = dto.ActualEndDate;
                                dto.ApprovedDuration = dto.ActualDuration.Value;
                            }
                        }

                        currentDate = nextDate;
                    }
                }
            }

            var result = new List<TimesheetItemDTO>();

            foreach (var t in timesheetCache.Values)
            {
                if (t.ActualDuration == 0)
                {
                    // no timesheet items if nothing's been done
                    continue;
                }

                RoundTimesheetItem(t, roster);

                t.ApprovedDuration = (decimal)t.ApprovedEndDate.Subtract(t.ApprovedStartDate).TotalSeconds;

                if (t.IsLunchPaid == false && t.LunchDuration.HasValue == true)
                {
                    t.ApprovedDuration = t.ApprovedDuration - t.LunchDuration.Value;
                }

                result.Add(t);
            }

            return result;
        }

        protected void RoundTimesheetItem(TimesheetItemDTO item, RosterDTO roster)
        {
            // regardless of rounding algorithm, we are going to remove seconds from things
            item.ApprovedStartDate = item.ApprovedStartDate.AddSeconds(-item.ApprovedStartDate.Second);
            item.ApprovedEndDate = item.ApprovedEndDate.AddSeconds(-item.ApprovedEndDate.Second);

            if ((RosterTimesheetRoundingMethod)roster.RoundTimesheetToNearestMethod == RosterTimesheetRoundingMethod.None)
            {
                return;
            }

            TimeSpan roundTo = new TimeSpan(0, 0, roster.RoundTimesheetToNearestMins, 0, 0);

            switch ((RosterTimesheetRoundingMethod)roster.RoundTimesheetToNearestMethod)
            {
                case RosterTimesheetRoundingMethod.Down:
                    item.ApprovedStartDate = item.ApprovedStartDate.RoundDown(roundTo);
                    item.ApprovedEndDate = item.ApprovedEndDate.RoundDown(roundTo);
                    break;
                case RosterTimesheetRoundingMethod.Up:
                    item.ApprovedStartDate = item.ApprovedStartDate.RoundUp(roundTo);
                    item.ApprovedEndDate = item.ApprovedEndDate.RoundUp(roundTo);
                    break;
                case RosterTimesheetRoundingMethod.Nearest:
                    item.ApprovedStartDate = item.ApprovedStartDate.RoundToNearest(roundTo);
                    item.ApprovedEndDate = item.ApprovedEndDate.RoundToNearest(roundTo);
                    break;
                case RosterTimesheetRoundingMethod.Shortest:
                    item.ApprovedStartDate = item.ApprovedStartDate.RoundUp(roundTo);
                    item.ApprovedEndDate = item.ApprovedEndDate.RoundDown(roundTo);
                    break;
                case RosterTimesheetRoundingMethod.Longest:
                    item.ApprovedStartDate = item.ApprovedStartDate.RoundDown(roundTo);
                    item.ApprovedEndDate = item.ApprovedEndDate.RoundUp(roundTo);
                    break;
            }
        }

        protected JToken CalculateTimesheet(JToken parameters)
        {
            string userIdString = parameters["userid"].ToString();
            string startDateString = parameters["startdate"].ToString();
            string endDateString = parameters["enddate"].ToString();
            string ownerIdString = parameters["ownerid"].ToString();
            string rosterIdString = parameters["rosterid"].ToString();
            string roleIdString = parameters["roleid"].ToString();

            Guid userId = Guid.Parse(userIdString);
            DateTime startDate = DateTime.ParseExact(startDateString, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            DateTime endDate = DateTime.ParseExact(endDateString, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            Guid ownerId = Guid.Parse(ownerIdString);
            int rosterId = int.Parse(rosterIdString);
            int roleId = int.Parse(roleIdString);

            var roster = _rosterRepo.GetById(rosterId, RepositoryInterfaces.Enums.RosterLoadInstructions.None);

            var rosterTZ = TimeZoneInfoResolver.ResolveWindows(roster.Timezone);

            var timesheets = LoadOrCreateTimesheets(roster, roleId, startDate, endDate, ownerId, userId);

            var result = new List<TimesheetItemDTO>();

            foreach (var ti in (from t in timesheets orderby t.Date ascending select t))
            {
                // our approved dates are in UTC, we need to convert them to local
                ti.Date = TimeZoneInfo.ConvertTime(ti.Date, TimeZoneInfo.Utc, rosterTZ);

                ti.ApprovedStartDate = TimeZoneInfo.ConvertTime(ti.ApprovedStartDate, TimeZoneInfo.Utc, rosterTZ);
                ti.ApprovedEndDate = TimeZoneInfo.ConvertTime(ti.ApprovedEndDate, TimeZoneInfo.Utc, rosterTZ);

                if (ti.ScheduledStartDate.HasValue)
                {
                    ti.ScheduledStartDate = TimeZoneInfo.ConvertTime(ti.ScheduledStartDate.Value, TimeZoneInfo.Utc, rosterTZ);
                }

                if (ti.ScheduledLunchStartDate.HasValue)
                {
                    ti.ScheduledLunchStartDate = TimeZoneInfo.ConvertTime(ti.ScheduledLunchStartDate.Value, TimeZoneInfo.Utc, rosterTZ);
                }

                if (ti.ScheduledEndDate.HasValue)
                {
                    ti.ScheduledEndDate = TimeZoneInfo.ConvertTime(ti.ScheduledEndDate.Value, TimeZoneInfo.Utc, rosterTZ);
                }

                if (ti.ScheduledLunchEndDate.HasValue)
                {
                    ti.ScheduledLunchEndDate = TimeZoneInfo.ConvertTime(ti.ScheduledLunchEndDate.Value, TimeZoneInfo.Utc, rosterTZ);
                }

                ti.ActualStartDate = TimeZoneInfo.ConvertTime(ti.ActualStartDate, TimeZoneInfo.Utc, rosterTZ);
                ti.ActualEndDate = TimeZoneInfo.ConvertTime(ti.ActualEndDate, TimeZoneInfo.Utc, rosterTZ);

                result.Add(ti);
            }

            var settings = new JsonSerializerSettings();

            LowercaseContractResolver resolver = new LowercaseContractResolver();

            var r = JArray.FromObject(result.ToArray(), new JsonSerializer() { ContractResolver = resolver, ReferenceLoopHandling = ReferenceLoopHandling.Ignore });

            var someObj = new JObject();
            someObj["timesheetitems"] = r;

            return someObj;
        }

        protected JToken GetTimesheetItemDetails(JToken parameters)
        {
            var sheeted = (JArray)parameters["sheeted"];
            var rosterId = int.Parse(parameters["rosterid"].ToString());

            List<Guid> ids = new List<Guid>();

            foreach (var item in sheeted)
            {
                ids.Add(Guid.Parse(item["projectjobtaskworklogid"].ToString()));
            }

            var roster = _rosterRepo.GetById(rosterId, RepositoryInterfaces.Enums.RosterLoadInstructions.None);

            var rosterTZ = TimeZoneInfoResolver.ResolveWindows(roster.Timezone);

            var clockins = _taskWorklogRepo.GetById(ids.ToArray(), RepositoryInterfaces.Enums.ProjectJobTaskWorkLogLoadInstructions.Task);

            JObject result = new JObject();
            JArray ts = new JArray();
            result["worklog"] = ts;

            foreach (var c in (from p in clockins orderby p.DateCompleted ascending select p))
            {
                JObject tjson = new JObject();
                tjson["id"] = c.Id;
                tjson["taskid"] = c.ProjectJobTaskId;
                tjson["name"] = c.ProjectJobTask.Name;
                tjson["description"] = c.ProjectJobTask.Description;
                tjson["taskstatus"] = ((ProjectJobTaskStatus)c.ProjectJobTask.Status).ToString();
                tjson["datestarted"] = TimeZoneInfo.ConvertTime(c.DateCreated, TimeZoneInfo.Utc, rosterTZ);
                tjson["datecompleted"] = null;
                tjson["durationseconds"] = null;
                if (c.DateCompleted.HasValue)
                {
                    tjson["datecompleted"] = TimeZoneInfo.ConvertTime(c.DateCompleted.Value, TimeZoneInfo.Utc, rosterTZ);
                    tjson["durationseconds"] = c.DateCompleted.Value.Subtract(c.DateCreated).TotalSeconds;
                }

                ts.Add(tjson);
            }

            return result;
        }

        protected JToken Save(JToken parameters)
        {
            try
            {
                string userIdString = parameters["userid"].ToString();
                string startDateString = parameters["startdate"].ToString();
                string endDateString = parameters["enddate"].ToString();
                string ownerIdString = parameters["ownerid"].ToString();
                string rosterIdString = parameters["rosterid"].ToString();
                string roleIdString = parameters["roleid"].ToString();


                Guid userId = Guid.Parse(userIdString);
                DateTime startDate = DateTime.ParseExact(startDateString, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                DateTime endDate = DateTime.ParseExact(endDateString, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                Guid ownerId = Guid.Parse(ownerIdString);
                int rosterId = int.Parse(rosterIdString);
                int roleId = int.Parse(roleIdString);

                var roster = _rosterRepo.GetById(rosterId, RepositoryInterfaces.Enums.RosterLoadInstructions.None);
                var rosterTZ = TimeZoneInfoResolver.ResolveWindows(roster.Timezone);

                List<TimesheetItemDTO> items = null;

                if (parameters["timesheetitems"] != null && string.IsNullOrEmpty(parameters["timesheetitems"].ToString()) == false)
                {
                    // we have some timesheet items from the client, deserialize these and
                    items = JsonConvert.DeserializeObject<List<TimesheetItemDTO>>(parameters["timesheetitems"].ToString());

                    // we now need convert the date/times back to UTC as they were converted to client local on the way up
                    foreach (var ti in items)
                    {
                        // our approved dates are in UTC, we need to convert them to local
                        ti.Date = TimeZoneInfo.ConvertTime(ti.Date, rosterTZ, TimeZoneInfo.Utc);

                        ti.ApprovedStartDate = TimeZoneInfo.ConvertTime(ti.ApprovedStartDate, rosterTZ, TimeZoneInfo.Utc);
                        ti.ApprovedEndDate = TimeZoneInfo.ConvertTime(ti.ApprovedEndDate, rosterTZ, TimeZoneInfo.Utc);

                        ti.ActualStartDate = TimeZoneInfo.ConvertTime(ti.ActualStartDate, rosterTZ, TimeZoneInfo.Utc);
                        ti.ActualEndDate = TimeZoneInfo.ConvertTime(ti.ActualEndDate, rosterTZ, TimeZoneInfo.Utc);
                    }
                }
                else
                {
                    // no timesheet items, so we'll go with what's on the server
                    items = LoadOrCreateTimesheets(roster, roleId, startDate, endDate, ownerId, userId);
                }

                foreach (var sheet in items)
                {
                    _timesheetItemRepo.Save(sheet, false, true);
                }

                JObject result = new JObject();
                result["success"] = true;

                return result;
            }
            catch (Exception e)
            {
                JObject result = new JObject();
                result["success"] = false;
                result["message"] = e.Message;

                return result;
            }
        }
    }
}
