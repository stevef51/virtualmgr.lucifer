﻿using System;
using System.Collections;
using System.Collections.Generic;
using ConceptCave.Checklist.Editor;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.RunTime;
using ConceptCave.Core.Coding;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Repositories;
using System.Linq;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using ConceptCave.BusinessLogic.Facilities;
using ConceptCave.Checklist.Lingo.ObjectProviders;

namespace ConceptCave.BusinessLogic.Questions
{
    public class SelectODataQuestion : Question
    {
        public SelectODataQuestion()
        {
        }

        public override IAnswer CreateBlankAnswer(IPresentedQuestion presentedQuestion)
        {
            var result = new SelectODataAnswer() { PresentedQuestion = presentedQuestion };

            if (DefaultAnswer != null)
            {
                result.AnswerValue = DefaultAnswer;
            }

            return result;
        }

        public override object DefaultAnswer
        {
            get
            {
                return _defaultAnswer;
            }
            set
            {
                _defaultAnswer = value;
            }
        }

        public string DataSource { get; set; }
        public string UpdateDataSources { get; set; }

        public string ClearDataSources { get; set; }

        public override void Decode(IDecoder decoder)
        {
            base.Decode(decoder);
            DataSource = decoder.Decode<string>("DataSource");
            UpdateDataSources = decoder.Decode<string>("UpdateDataSources");
            ClearDataSources = decoder.Decode<string>("ClearDataSources");
        }

        public override void Encode(IEncoder encoder)
        {
            base.Encode(encoder);
            encoder.Encode("DataSource", DataSource);
            encoder.Encode("UpdateDataSources", UpdateDataSources);
            encoder.Encode("ClearDataSources", ClearDataSources);
        }

        public override List<System.Collections.Specialized.NameValueCollection> ToNameValuePairs()
        {
            var r = base.ToNameValuePairs();

            var result = r[0];

            result.Add("datasource", DataSource);
            result.Add("updatedatasources", UpdateDataSources);
            result.Add("cleardatasources", ClearDataSources);

            return r;
        }

        public override void FromNameValuePairs(System.Collections.Specialized.NameValueCollection pairs)
        {
            base.FromNameValuePairs(pairs);

            foreach (var name in pairs.AllKeys)
            {
                string n = name.ToLower();

                switch (n)
                {
                    case "datasource":
                        DataSource = pairs[name];
                        break;
                    case "updatedatasources":
                        UpdateDataSources = pairs[name];
                        break;
                    case "cleardatasources":
                        ClearDataSources = pairs[name];
                        break;
                }
            }
        }
    }

    public class SelectODataAnswer : Answer
    {
        private DictionaryObjectProvider _selected;

        public SelectODataAnswer()
        {
        }

        public DictionaryObjectProvider Selected
        {
            get => _selected;
            set => _selected = value;
        }

        public JObject JObject
        {
            get
            {
                if (_selected == null)
                {
                    return null;
                }
                JsonFacility jf = new JsonFacility();
                return jf.DictionaryToJson(null, _selected);
            }
            set
            {
                JsonFacility jf = new JsonFacility();
                _selected = jf.JsonToDictionary(null, value);
            }
        }

        public override object AnswerValue
        {
            get => _selected;
            set
            {
                if (value is string)
                {
                    value = JObject.Parse(value.ToString());
                }
                if (value is JObject)
                {
                    JsonFacility jf = new JsonFacility();
                    _selected = jf.JsonToDictionary(null, (JObject)value);
                }
                else if (value is IDictionary)
                {
                    _selected = new DictionaryObjectProvider() { Dictionary = (IDictionary<string, object>)value };
                }
                else if (value is DictionaryObjectProvider)
                {
                    _selected = (DictionaryObjectProvider)value;
                }
                else
                {
                    _selected = null;
                }
            }
        }

        public override string AnswerAsString => JObject?.ToString();
    }
}
