﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using ConceptCave.Checklist.Editor;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using Newtonsoft.Json.Linq;
using VirtualMgr.Common;

namespace ConceptCave.BusinessLogic.Questions
{
    public class CompanyConstructorQuestion : Question
    {
        protected const string DefaultGeocodingUrl = "http://nominatim.openstreetmap.org/search?q=:street+:town+:postcode+:state+:country&format=json&polygon=0&addressdetails=0";
        protected const string DefaultTileUrl = ""; // leaflet defaults to open street map, so need to supply one

        public string TileUrl { get; set; }
        public string GeocodingUrl { get; set; }

        public bool CreateDefaultUser { get; set; }
        public List<int> AllowedUserTypes { get; set; }

        public CompanyConstructorQuestion()
        {
            AllowedUserTypes = new List<int>();
            TileUrl = DefaultTileUrl;
            GeocodingUrl = DefaultGeocodingUrl;
        }

        public override object DefaultAnswer
        {
            get
            {
                return null;
            }
            set
            {

            }
        }

        public override bool IsAnswerable
        {
            get
            {
                return false;
            }
        }

        public override Checklist.Interfaces.IAnswer CreateBlankAnswer(Checklist.Interfaces.IPresentedQuestion presentedQuestion)
        {
            return null;
        }

        public override void Encode(Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode<string>("TileUrl", TileUrl);
            encoder.Encode<string>("GeocodingUrl", GeocodingUrl);
            encoder.Encode<bool>("CreateDefaultUser", CreateDefaultUser);
            encoder.Encode<List<int>>("AllowedUserTypes", AllowedUserTypes);
        }

        public override void Decode(Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            TileUrl = decoder.Decode<string>("TileUrl", DefaultTileUrl);
            GeocodingUrl = decoder.Decode<string>("GeocodingUrl", DefaultGeocodingUrl);
            CreateDefaultUser = decoder.Decode<bool>("CreateDefaultUser", false);
            AllowedUserTypes = decoder.Decode<List<int>>("AllowedUserTypes", new List<int>());

            if (AllowedUserTypes == null)
            {
                AllowedUserTypes = new List<int>();
            }
        }

        public override List<System.Collections.Specialized.NameValueCollection> ToNameValuePairs()
        {
            var r = base.ToNameValuePairs();

            var result = r[0];

            result.Add("tileurl", TileUrl);
            result.Add("geocodingurl", GeocodingUrl);
            result.Add("createdefaultuser", CreateDefaultUser.ToString());
            result.Add("allowedusertypes", string.Join(",", AllowedUserTypes.ToArray()));

            return r;
        }

        public override void FromNameValuePairs(System.Collections.Specialized.NameValueCollection pairs)
        {
            base.FromNameValuePairs(pairs);

            foreach (var name in pairs.AllKeys)
            {
                string n = name.ToLower();

                switch (n)
                {
                    case "tileurl":
                        TileUrl = pairs[name];
                        break;
                    case "geocodingurl":
                        GeocodingUrl = pairs[name];
                        break;
                    case "createdefaultuser":
                        bool create = false;
                        if (bool.TryParse(pairs[name], out create) == true)
                        {
                            this.CreateDefaultUser = create;
                        }
                        break;
                    case "allowedusertypes":
                        AllowedUserTypes = pairs[name].Split(',').Select(s => int.Parse(s)).ToList();
                        break;
                }
            }
        }
    }

    public class CompanyConstructorQuestionExecutionHandler : IExecutionMethodHandler
    {
        protected ICompanyRepository _companyRepo;
        protected ICountryStateRepository _countryStateRepo;
        protected IMembershipRepository _memberRepo;
        protected IMembershipTypeRepository _memberTypeRepo;

        public CompanyConstructorQuestionExecutionHandler(ICompanyRepository companyRepo, IMembershipRepository memberRepo, ICountryStateRepository countryStateRepo, IMembershipTypeRepository memberTypeRepo)
        {
            _companyRepo = companyRepo;
            _memberRepo = memberRepo;
            _countryStateRepo = countryStateRepo;
            _memberTypeRepo = memberTypeRepo;
        }

        public string Name
        {
            get { return "CompanyConstructor"; }
        }

        public Newtonsoft.Json.Linq.JToken Execute(string method, Newtonsoft.Json.Linq.JToken parameters, IExecutionMethodHandlerContext context)
        {
            switch (method)
            {
                case "GetCriteria":
                    return GetCriteria(parameters);
                case "Create":
                    return Create(parameters);
                case "GetCompanies":
                    return GetCompanies(parameters);
            }

            return null;
        }

        protected JToken GetCriteria(JToken parameters)
        {
            string userIdString = parameters["userid"].ToString();

            Guid userId = Guid.Parse(userIdString);
            var user = _memberRepo.GetById(userId, RepositoryInterfaces.Enums.MembershipLoadInstructions.None);
            var tz = TimeZoneInfoResolver.ResolveWindows(user.TimeZone);

            bool createDefaultUser = bool.Parse(parameters["criteria"]["createdefaultuser"].ToString());
            JArray allowedUserTypes = (JArray)parameters["criteria"]["allowedusertypes"];

            var states = _countryStateRepo.GetAll();

            var result = new JObject();

            result["countrystates"] = CountryStatesExecutionHandler.ToJson(states);

            if (createDefaultUser == true)
            {
                var types = _memberTypeRepo.GetAll();
                JArray usertypes = new JArray();
                result["availableusertypes"] = usertypes;

                foreach (var token in allowedUserTypes)
                {
                    int id = int.Parse(token.ToString());

                    var type = (from t in types where t.Id == id select t).First();

                    var ut = new JObject();
                    ut["id"] = type.Id;
                    ut["name"] = type.Name;

                    usertypes.Add(ut);
                }
            }

            return result;
        }

        protected JToken Create(JToken parameters)
        {
            string userIdString = parameters["userid"].ToString();

            Guid userId = Guid.Parse(userIdString);
            var user = _memberRepo.GetById(userId, RepositoryInterfaces.Enums.MembershipLoadInstructions.None);
            var tz = TimeZoneInfoResolver.ResolveWindows(user.TimeZone);

            bool createDefaultUser = bool.Parse(parameters["criteria"]["createdefaultuser"].ToString());

            string name = parameters["company"]["name"].ToString();
            string street = parameters["company"]["address"]["street"].ToString();
            string town = parameters["company"]["address"]["town"].ToString();
            string postcode = parameters["company"]["address"]["postcode"].ToString();
            int state = int.Parse(parameters["company"]["address"]["state"].ToString());

            string defaultUserName = null;
            string defaultUserEmail = null;
            string defaultUserPassword = null;
            int defaultUserType = -1;

            if (createDefaultUser == true)
            {
                defaultUserName = parameters["company"]["user"]["name"].ToString();
                defaultUserPassword = parameters["company"]["user"]["password"].ToString();
                defaultUserEmail = parameters["company"]["user"]["email"].ToString();
                defaultUserType = int.Parse(parameters["company"]["user"]["type"].ToString());
            }

            decimal? lat = null;
            decimal? lng = null;

            if (parameters["company"]["location"] != null)
            {
                lat = decimal.Parse(parameters["company"]["location"]["lat"].ToString());
                lng = decimal.Parse(parameters["company"]["location"]["lng"].ToString());
            }

            CompanyDTO company = null;

            var countryState = _countryStateRepo.GetById(state);

            using (TransactionScope scope = new TransactionScope())
            {
                company = _companyRepo.New(name);
                company.Street = street;
                company.Town = town;
                company.Postcode = postcode;
                company.CountryStateId = state;
                company.Latitude = lat;
                company.Longitude = lng;

                _companyRepo.Save(company, false, false);

                UserDataDTO newUser = null;

                if (createDefaultUser == true)
                {
                    newUser = _memberRepo.Create(defaultUserEmail, defaultUserPassword, countryState.TimeZone, MembershipLoadInstructions.AspNetMembership);
                    newUser.Name = defaultUserName;
                    newUser.AspNetUser.Email = defaultUserEmail;
                    newUser.UserTypeId = defaultUserType;
                    newUser.CompanyId = company.Id;

                    _memberRepo.Save(newUser, false, true);
                }

                scope.Complete();
            }

            var result = new JObject();
            result["id"] = company.Id;

            return result;
        }

        public JToken GetCompanies(JToken parameters)
        {
            string userIdString = parameters["userid"].ToString();

            Guid userId = Guid.Parse(userIdString);
            var user = _memberRepo.GetById(userId, RepositoryInterfaces.Enums.MembershipLoadInstructions.None);
            var tz = TimeZoneInfoResolver.ResolveWindows(user.TimeZone);

            // for the moment, get all, if speed becomes an issue we would do a search around geo coords that come in the parameters
            var companies = _companyRepo.GetAll(CompanyLoadInstructions.CountryState);

            var result = new JObject();
            var comps = new JArray();
            result["companies"] = comps;

            foreach (var company in companies)
            {
                var c = CompanySummaryQuestionExecutionHandler.CompanyToJson(company);

                comps.Add(c);
            }

            return result;
        }
    }
}
