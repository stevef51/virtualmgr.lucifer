﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.BusinessLogic.Models;
using ConceptCave.Checklist.Editor;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.RepositoryInterfaces.Repositories;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ConceptCave.BusinessLogic.Questions
{
    public class TaskScheduleQuestion : Question
    {
        public Guid? CompanyId { get; set; }
        public Guid? UserId { get; set; }
        public bool ShowJobs { get; set; }


        /// <summary>
        /// Helper method for Lingo, which has issues with nullable Guids
        /// </summary>
        /// <param name="companyId"></param>
        public void SetCompany(object companyId)
        {
            CompanyId = (Guid?)companyId;
        }

        public void SetUser(object userId)
        {
            UserId = (Guid?)userId;
        }

        public override object DefaultAnswer
        {
            get
            {
                return null;
            }
            set
            {

            }
        }

        public override bool IsAnswerable
        {
            get
            {
                return false;
            }
        }

        public override Checklist.Interfaces.IAnswer CreateBlankAnswer(Checklist.Interfaces.IPresentedQuestion presentedQuestion)
        {
            return null;
        }

        public override void Encode(Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode<Guid?>("CompanyId", CompanyId);
            encoder.Encode<Guid?>("UserId", UserId);
            encoder.Encode<bool>("ShowJobs", ShowJobs);
        }

        public override void Decode(Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            CompanyId = decoder.Decode<Guid?>("CompanyId", null);
            UserId = decoder.Decode<Guid?>("UserId", null);
            ShowJobs = decoder.Decode<bool>("ShowJobs", true);
        }
    }

    //public class TaskScheduleQuestionExecutionHandler : IExecutionMethodHandler
    //{
    //    protected ITaskWorkLogRepository _worklogRepo;
    //    protected IMembershipRepository _memberRepo;

    //    public TaskScheduleQuestionExecutionHandler(ITaskWorkLogRepository worklogRepo, IMembershipRepository memberRepo)
    //    {
    //        _worklogRepo = worklogRepo;
    //        _memberRepo = memberRepo;
    //    }

    //    public string Name
    //    {
    //        get { return "TaskScheduleView"; }
    //    }

    //    public Newtonsoft.Json.Linq.JToken Execute(string method, Newtonsoft.Json.Linq.JToken parameters)
    //    {
    //        switch (method)
    //        {
    //            case "GetSchedule":
    //                return GetSchedule(parameters);

    //        }

    //        return null;
    //    }

    //    public JToken GetSchedule(JToken parameters)
    //    {
    //        // at present there is 1 of 3 views supported with this.
    //        // for a company (the companyid parameter is set)
    //        // for a user (the userid parameter is set)
    //        // everything (neither of the above is set)

    //        string userIdString = parameters["userid"].ToString();

    //        Guid userId = Guid.Parse(userIdString);
    //        var user = _memberRepo.GetById(userId, RepositoryInterfaces.Enums.MembershipLoadInstructions.None);
    //        var tz = TimeZoneHelpers.FindSystemTimeZoneById(user.TimeZone);

    //        string companyIdString = parameters["criteria"]["companyid"].ToString();
    //        string empIdString = parameters["criteria"]["userid"].ToString();
    //        string showJobsString = parameters["criteria"]["showjobs"].ToString();
    //        string startdateString = null;

    //        if (parameters["criteria"]["startdate"] != null)
    //        {
    //            startdateString = parameters["criteria"]["startdate"].ToString();
    //        }

    //        Guid? companyId = null;
    //        if (string.IsNullOrEmpty(companyIdString) == false)
    //        {
    //            companyId = Guid.Parse(companyIdString);
    //        }
            
    //        Guid? empId = null;
    //        if (string.IsNullOrEmpty(empIdString) == false)
    //        {
    //            empId = Guid.Parse(empIdString);
    //        }

    //        bool showJobs = bool.Parse(showJobsString);

    //        DateTime startDate = DateTime.Now.Date.ToUniversalTime();
    //        if (string.IsNullOrEmpty(startdateString) == false)
    //        {
    //            startDate = DateTime.ParseExact(startdateString, "dd-MM-yyyy", CultureInfo.InvariantCulture);
    //            startDate = startDate.ToUniversalTime();
    //        }

    //        var worklog = _worklogRepo.GetBetweenDates(null, companyId, empId, startDate, startDate.AddDays(1), RepositoryInterfaces.Enums.ProjectJobTaskWorkLogLoadInstructions.Task | RepositoryInterfaces.Enums.ProjectJobTaskWorkLogLoadInstructions.User | RepositoryInterfaces.Enums.ProjectJobTaskWorkLogLoadInstructions.Job | RepositoryInterfaces.Enums.ProjectJobTaskWorkLogLoadInstructions.Project);

    //        var groupByUser = from w in worklog group w by w.UserId into g select new { UserId = g.Key, Logs = g.ToList(), User = g.First().UserData };

    //        var result = new ScheduleGraphModel() { Scale = ScheduleGraphModelScale.Hourly, RowTitle = "User" };
    //        List<ScheduleGraphRowModel> items = new List<ScheduleGraphRowModel>();

    //        DateTime now = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, tz);

    //        foreach (var log in groupByUser)
    //        {
    //            ScheduleGraphRowModel item = new ScheduleGraphRowModel() { Text = log.User.Name };

    //            List<ScheduleGraphCellModel> cells = new List<ScheduleGraphCellModel>();

    //            foreach (var l in log.Logs)
    //            {
    //                ScheduleGraphCellModel cell = new ScheduleGraphCellModel();

    //                cell.Start = TimeZoneInfo.ConvertTimeFromUtc(l.DateCreated, tz);
    //                cell.End = now;
    //                if (l.DateCompleted.HasValue == true)
    //                {
    //                    cell.End = TimeZoneInfo.ConvertTimeFromUtc(l.DateCompleted.Value, tz);
    //                }
    //                cell.Duration = (int)cell.End.Subtract(cell.Start).TotalSeconds;

    //                cell.Text = showJobs == true ? l.ProjectJobTask.ProjectJob.Name : l.ProjectJobTask.Name;

    //                cells.Add(cell);
    //            }

    //            item.Items = cells.ToArray();

    //            items.Add(item);
    //        }

    //        result.Rows = items.ToArray();

    //        return JToken.FromObject(result);
    //    }
    //}
}
