﻿using ConceptCave.Checklist.Editor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.BusinessLogic.Questions
{
    public class TaskBatchOperationsQuestion : Question
    {
        public string AssignToDatasource {get;set;}
        public string AssignToParameterName { get; set; }
        public string UpdateDatasource { get; set; }

        public bool AllowReassign { get; set; }

        public bool AllowCancellation { get; set; }

        public bool AllowFinishByManagement { get; set; }

        public bool AllowFinishComplete { get; set; }

        public bool AquireTasksWhenFinishing { get; set; }

        public bool AllowDelete { get; set; }

        public bool AllowClearStartTime { get; set; }
        /// <summary>
        /// If true actions taken require the user to authenticate (enter username/password)
        /// before they can be taken
        /// </summary>
        public bool RequireAuthentication { get; set; }

        /// <summary>
        /// If true authentication uses kiosk mode authentication where the user
        /// simply enters a single numeric identifier.
        /// </summary>
        public bool KioskMode { get; set; }

        public bool UseButtons { get; set; }

        public override bool IsAnswerable
        {
            get
            {
                return false;
            }
        }

        public override object DefaultAnswer
        {
            get
            {
                return null;
            }
            set
            {
            }
        }

        public override Checklist.Interfaces.IAnswer CreateBlankAnswer(Checklist.Interfaces.IPresentedQuestion presentedQuestion)
        {
            return null;
        }

        public override void Encode(Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode<string>("AssignToDatasource", AssignToDatasource);
            encoder.Encode<string>("AssignToParameterName", AssignToParameterName);
            encoder.Encode<string>("UpdateDatasource", UpdateDatasource);

            encoder.Encode<bool>("AllowReassign", AllowReassign);
            encoder.Encode<bool>("AllowCancellation", AllowCancellation);
            encoder.Encode<bool>("AllowFinishByManagement", AllowFinishByManagement);
            encoder.Encode<bool>("AllowFinishComplete", AllowFinishComplete);
            encoder.Encode<bool>("AllowDelete", AllowDelete);
            encoder.Encode<bool>("AllowClearStartTime", AllowClearStartTime);

            encoder.Encode<bool>("AquireTasksWhenFinishing", AquireTasksWhenFinishing);

            encoder.Encode<bool>("RequireAuthentication", RequireAuthentication);
            encoder.Encode<bool>("KioskMode", KioskMode);

            encoder.Encode<bool>("UseButtons", UseButtons);
        }

        public override void Decode(Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            AssignToDatasource = decoder.Decode<string>("AssignToDatasource");
            AssignToParameterName = decoder.Decode<string>("AssignToParameterName");
            UpdateDatasource = decoder.Decode<string>("UpdateDatasource");

            AllowReassign = decoder.Decode<bool>("AllowReassign", true);
            AllowCancellation = decoder.Decode<bool>("AllowCancellation");
            AllowFinishByManagement = decoder.Decode<bool>("AllowFinishByManagement");
            AllowFinishComplete = decoder.Decode<bool>("AllowFinishComplete");
            AllowDelete = decoder.Decode<bool>("AllowDelete");
            AllowClearStartTime = decoder.Decode<bool>("AllowClearStartTime");

            AquireTasksWhenFinishing = decoder.Decode<bool>("AquireTasksWhenFinishing");

            RequireAuthentication = decoder.Decode<bool>("RequireAuthentication");
            KioskMode = decoder.Decode<bool>("KioskMode");

            UseButtons = decoder.Decode<bool>("UseButtons");
        }

        public override List<System.Collections.Specialized.NameValueCollection> ToNameValuePairs()
        {
            var r = base.ToNameValuePairs();

            var result = r[0];

            result.Add("assigntodatasource", AssignToDatasource.ToString());
            result.Add("assigntoparametername", AssignToParameterName.ToString());
            result.Add("updatedatasoource", UpdateDatasource.ToString());

            result.Add("allowreasssign", AllowReassign.ToString());
            result.Add("allowcancellation", AllowCancellation.ToString());
            result.Add("allowfinishbymanagement", AllowFinishByManagement.ToString());
            result.Add("allowfinishcomplete", AllowFinishComplete.ToString());
            result.Add("allowdelete", AllowDelete.ToString());
            result.Add("allowclearstarttime", AllowClearStartTime.ToString());

            result.Add("aquiretaskswhenfinishing", AquireTasksWhenFinishing.ToString());

            result.Add("requireauthentication", RequireAuthentication.ToString());
            result.Add("kioskmode", KioskMode.ToString());

            result.Add("usebuttons", UseButtons.ToString());
            return r;
        }

        public override void FromNameValuePairs(System.Collections.Specialized.NameValueCollection pairs)
        {
            base.FromNameValuePairs(pairs);

            foreach (var name in pairs.AllKeys)
            {
                string n = name.ToLower();

                switch (n)
                {
                    case "assigntodatasource":
                        AssignToDatasource = pairs[n];
                        break;
                    case "assigntoparametername":
                        AssignToParameterName = pairs[n];
                        break;
                    case "updatedatasoource":
                        UpdateDatasource = pairs[n];
                        break;
                    case "allowreasssign":
                        AllowReassign = bool.Parse(pairs[n]);
                        break;
                    case "allowcancellation":
                        AllowCancellation = bool.Parse(pairs[n]);
                        break;
                    case "allowfinishbymanagement":
                        AllowFinishByManagement = bool.Parse(pairs[n]);
                        break;
                    case "allowfinishcomplete":
                        AllowFinishComplete = bool.Parse(pairs[n]);
                        break;
                    case "allowdelete":
                        AllowDelete = bool.Parse(pairs[n]);
                        break;
                    case "allowclearstarttime":
                        AllowClearStartTime = bool.Parse(pairs[n]);
                        break;
                    case "aquiretaskswhenfinishing":
                        AquireTasksWhenFinishing = bool.Parse(pairs[n]);
                        break;
                    case "requireauthentication":
                        RequireAuthentication = bool.Parse(pairs[n]);
                        break;
                    case "kioskmode":
                        KioskMode = bool.Parse(pairs[n]);
                        break;
                    case "usebuttons":
                        UseButtons = bool.Parse(pairs[n]);
                        break;
                }
            }
        }
    }
}
