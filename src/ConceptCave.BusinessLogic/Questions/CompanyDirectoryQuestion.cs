﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Checklist.Editor;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Repositories;
using Newtonsoft.Json.Linq;
using VirtualMgr.Common;

namespace ConceptCave.BusinessLogic.Questions
{
    public class CompanyDirectoryQuestion : Question
    {
        protected const string DefaultTileUrl = ""; // leaflet defaults to open street map, so need to supply one

        public string TileUrl { get; set; }
        public bool AllowRating { get; set; }
        public bool AllowEditing { get; set; }

        public CompanyDirectoryQuestion()
        {
            TileUrl = DefaultTileUrl;
        }

        public override object DefaultAnswer
        {
            get
            {
                return null;
            }
            set
            {

            }
        }

        public override bool IsAnswerable
        {
            get
            {
                return false;
            }
        }

        public override Checklist.Interfaces.IAnswer CreateBlankAnswer(Checklist.Interfaces.IPresentedQuestion presentedQuestion)
        {
            return null;
        }

        public override void Encode(Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode<string>("TileUrl", TileUrl);
            encoder.Encode<bool>("AllowRating", AllowRating);
            encoder.Encode<bool>("AllowEditing", AllowEditing);
        }

        public override void Decode(Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            TileUrl = decoder.Decode<string>("TileUrl", DefaultTileUrl);
            AllowRating = decoder.Decode<bool>("AllowRating", AllowRating);
            AllowEditing = decoder.Decode<bool>("AllowEditing", AllowEditing);
        }

        public override List<System.Collections.Specialized.NameValueCollection> ToNameValuePairs()
        {
            var r = base.ToNameValuePairs();

            var result = r[0];

            result.Add("tileurl", TileUrl);
            result.Add("allowrating", AllowRating.ToString());
            result.Add("allowediting", AllowEditing.ToString());

            return r;
        }

        public override void FromNameValuePairs(System.Collections.Specialized.NameValueCollection pairs)
        {
            base.FromNameValuePairs(pairs);

            foreach (var name in pairs.AllKeys)
            {
                string n = name.ToLower();

                switch (n)
                {
                    case "tileurl":
                        TileUrl = pairs[name];
                        break;
                    case "allowrating":
                        bool allow = false;
                        if (bool.TryParse(pairs[name], out allow) == true)
                        {
                            this.AllowRating = allow;
                        }
                        break;
                    case "allowediting":
                        bool allowEdit = false;
                        if (bool.TryParse(pairs[name], out allowEdit) == true)
                        {
                            this.AllowEditing = allowEdit;
                        }
                        break;
                }
            }
        }
    }

    public class CompanyDirectoryQuestionExecutionHandler : IExecutionMethodHandler
    {
        protected ICompanyRepository _companyRepo;
        protected ICountryStateRepository _countryStateRepo;
        protected IMembershipRepository _memberRepo;

        public CompanyDirectoryQuestionExecutionHandler(ICompanyRepository companyRepo, IMembershipRepository memberRepo, ICountryStateRepository countryStateRepo)
        {
            _companyRepo = companyRepo;
            _memberRepo = memberRepo;
            _countryStateRepo = countryStateRepo;
        }

        public string Name
        {
            get { return "CompanyDirectory"; }
        }

        public Newtonsoft.Json.Linq.JToken Execute(string method, Newtonsoft.Json.Linq.JToken parameters, IExecutionMethodHandlerContext context)
        {
            switch (method)
            {
                case "Search":
                    return Search(parameters);
                case "Save":
                    return Save(parameters);
                case "SaveRating":
                    return SaveRating(parameters);
            }

            return null;
        }

        public JToken Search(JToken parameters)
        {
            string userIdString = parameters["userid"].ToString();

            Guid userId = Guid.Parse(userIdString);
            var user = _memberRepo.GetById(userId, RepositoryInterfaces.Enums.MembershipLoadInstructions.None);
            var tz = TimeZoneInfoResolver.ResolveWindows(user.TimeZone);

            string text = parameters["text"].ToString();
            int page = int.Parse(parameters["page"].ToString());
            int count = int.Parse(parameters["count"].ToString());

            var companies = _companyRepo.Search("%" + text + "%", page, count, RepositoryInterfaces.Enums.CompanyLoadInstructions.CountryState);

            var result = new JObject();
            var comps = new JArray();
            result["companies"] = comps;

            foreach (var company in companies)
            {
                var c = CompanySummaryQuestionExecutionHandler.CompanyToJson(company);

                comps.Add(c);
            }

            return result;
        }

        public JToken Save(JToken parameters)
        {
            string userIdString = parameters["userid"].ToString();

            Guid userId = Guid.Parse(userIdString);
            var user = _memberRepo.GetById(userId, RepositoryInterfaces.Enums.MembershipLoadInstructions.None);
            var tz = TimeZoneInfoResolver.ResolveWindows(user.TimeZone);

            Guid companyId = Guid.Parse(parameters["company"]["id"].ToString());
            string name = parameters["company"]["name"].ToString();
            string street = parameters["company"]["address"]["street"].ToString();
            string town = parameters["company"]["address"]["town"].ToString();
            string postcode = parameters["company"]["address"]["postcode"].ToString();
            int state = int.Parse(parameters["company"]["address"]["stateid"].ToString());

            decimal? lat = null;
            decimal? lng = null;

            if (parameters["company"]["location"] != null)
            {
                lat = decimal.Parse(parameters["company"]["location"]["lat"].ToString());
                lng = decimal.Parse(parameters["company"]["location"]["lng"].ToString());
            }

            CompanyDTO company = _companyRepo.GetById(companyId, RepositoryInterfaces.Enums.CompanyLoadInstructions.None);

            company.Name = name;
            company.Street = street;
            company.Town = town;
            company.Postcode = postcode;
            company.CountryStateId = state;
            company.Latitude = lat;
            company.Longitude = lng;

            _companyRepo.Save(company, false, false);

            company = _companyRepo.GetById(company.Id, RepositoryInterfaces.Enums.CompanyLoadInstructions.CountryState);

            var result = new JObject();
            result["company"] = CompanySummaryQuestionExecutionHandler.CompanyToJson(company);

            return result;
        }

        public JToken SaveRating(JToken parameters)
        {
            string userIdString = parameters["userid"].ToString();

            Guid userId = Guid.Parse(userIdString);
            var user = _memberRepo.GetById(userId, RepositoryInterfaces.Enums.MembershipLoadInstructions.None);
            var tz = TimeZoneInfoResolver.ResolveWindows(user.TimeZone);

            Guid companyId = Guid.Parse(parameters["companyid"].ToString());
            int rating = int.Parse(parameters["rating"].ToString());

            var company = _companyRepo.GetById(companyId, RepositoryInterfaces.Enums.CompanyLoadInstructions.None);
            company.Rating = rating;

            _companyRepo.Save(company, false, false);

            JObject result = new JObject();
            result["success"] = true;

            return result;
        }
    }
}
