﻿using ConceptCave.BusinessLogic.Facilities;
using ConceptCave.Checklist.Editor;
using ConceptCave.Checklist.Facilities;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.Lingo.ObjectProviders;
using ConceptCave.Checklist.RunTime;
using ConceptCave.Core;
using ConceptCave.Core.Coding;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.BusinessLogic.Questions
{
    public class Address
    {
        public string Street1 { get; set; }
        public string Street2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }
    }

    public class Customer
    {
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public Address Address { get; set; }
        public string CompanyName { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Title { get; set; }
        public string Phone { get; set; }
    }

    public enum PendingPaymentQuestionMode
    {
        Payment = 0,
        ViewOnly = 1,
        Edit = 2
    }

    public class PendingPaymentQuestion : Question
    {
        private Customer _customer;
        public Customer Customer
        {
            get => _customer;
            set
            {
                Customer c = value as Customer;
                if (c == null && value != null)
                {
                    c = JsonConvert.DeserializeObject<Customer>(JsonConvert.SerializeObject(value));
                }
                _customer = c;
            }
        }

        public PendingPaymentQuestionMode Mode { get; set; }

        private Guid? _orderId;
        public object OrderId
        {
            get => _orderId;
            set
            {
                if (value == null)
                    _orderId = null;
                else if (value is Guid)
                    _orderId = (Guid)value;
                else if (value is string)
                    _orderId = Guid.Parse(value.ToString());
                else if (value is OrderFacilityOrder)
                    _orderId = ((OrderFacilityOrder)value).Id;
            }
        }
        public PendingPaymentFacility.PendingPayments PendingPayments { get; set; }

        public override IAnswer CreateBlankAnswer(IPresentedQuestion presentedQuestion)
        {
            return new PendingPaymentAnswer() { PresentedQuestion = presentedQuestion };
        }

        public override object DefaultAnswer
        {
            get
            {
                return _defaultAnswer;
            }
            set
            {
                if (value is PendingPaymentAnswer)
                {
                    _defaultAnswer = ((PendingPaymentAnswer)value).AnswerValue;
                }
            }
        }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            if (Customer != null)
            {
                encoder.Encode("Customer", JsonConvert.SerializeObject(Customer));
            }
            encoder.Encode("PendingPayments", PendingPayments);
            encoder.Encode("OrderId", OrderId);
            encoder.Encode("Mode", Mode);
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            var customer = decoder.Decode<string>("Customer");
            if (customer != null)
            {
                Customer = JsonConvert.DeserializeObject<Customer>(customer);
            }
            PendingPayments = decoder.Decode<PendingPaymentFacility.PendingPayments>("PendingPayments");
            OrderId = decoder.Decode<Guid?>("OrderId");
            Mode = decoder.Decode<PendingPaymentQuestionMode>("Mode");
        }
    }

    public class PendingPaymentAnswer : Answer
    {
        public string[] CouponCodes { get; set; } = new string[0];

        public override void Decode(IDecoder decoder)
        {
            base.Decode(decoder);
            CouponCodes = decoder.Decode<string[]>("CouponCodes");
        }

        public override void Encode(IEncoder encoder)
        {
            base.Encode(encoder);
            encoder.Encode("CouponCodes", CouponCodes);
        }

        public override object AnswerValue {
            get => CouponCodes;
            set
            {
                if (value is string[])
                {
                    CouponCodes = (string[])CouponCodes;
                }
                else if (value is string)
                {
                    CouponCodes = value.ToString().Split(',');
                }
            }
        }

        public override string AnswerAsString => string.Join(",", CouponCodes);
    }
}
