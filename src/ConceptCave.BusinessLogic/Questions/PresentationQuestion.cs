﻿using ConceptCave.Checklist.Editor;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.RunTime;
using ConceptCave.Core.Coding;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Repositories;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.BusinessLogic.Questions
{
    public class PresentationQuestion : Question
    {
        /// <summary>
        /// JSON that describes what needs to be presented
        /// </summary>
        public string ManifestJson { get; set; }
        /// <summary>
        /// The manifest can define multiple modules, this selects which module
        /// should be presented. If null or empty then the first module is presented
        /// </summary>
        public string Module { get; set; }

        public bool TrackViewing { get; set; }

        public string NextButtonText { get; set; }
        public string BackButtonText { get; set; }

        public override object DefaultAnswer
        {
            get
            {
                return _defaultAnswer;
            }
            set
            {
                if (value is string)
                {
                    _defaultAnswer = value;
                }
                else if (value is PresentationQuestionAnswer)
                {
                    _defaultAnswer = ((PresentationQuestionAnswer)value).Data;
                }
            }
        }

        public override IAnswer CreateBlankAnswer(IPresentedQuestion presentedQuestion)
        {
            var result = new PresentationQuestionAnswer() { PresentedQuestion = presentedQuestion };

            if (DefaultAnswer != null && DefaultAnswer is string)
            {
                result.AnswerValue = DefaultAnswer;
            }
            else if (DefaultAnswer != null && DefaultAnswer is PresentationQuestionAnswer)
            {
                result.AnswerValue = ((PresentationQuestionAnswer)DefaultAnswer).Data;
            }

            return result;
        }

        public override bool IsAnswerable
        {
            get
            {
                return true;
            }
        }

        public override void Encode(IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode<string>("ManifestJson", ManifestJson);
            encoder.Encode<string>("Module", Module);
            encoder.Encode<bool>("TrackViewing", TrackViewing);

            encoder.Encode<string>("NextButtonText", NextButtonText);
            encoder.Encode<string>("BackButtonText", BackButtonText);
        }

        public override void Decode(IDecoder decoder)
        {
            base.Decode(decoder);

            ManifestJson = decoder.Decode<string>("ManifestJson");
            Module = decoder.Decode<string>("Module");
            TrackViewing = decoder.Decode<bool>("TrackViewing");

            NextButtonText = decoder.Decode<string>("NextButtonText");
            BackButtonText = decoder.Decode<string>("BackButtonText");
        }
    }

    public class PresentationQuestionAnswer : Answer
    {
        protected string _data;

        public string Data
        {
            get
            {
                return _data;
            }
            set
            {
                UtcDateAnswered = DateTime.UtcNow;
                _data = value;
            }
        }

        public override object AnswerValue
        {
            get
            {
                return Data;
            }
            set
            {
                if(value == null)
                {
                    Data = string.Empty;
                    return;
                }

                Data = value.ToString();
            }
        }

        public override string AnswerAsString => string.IsNullOrEmpty(_data) ? string.Empty : _data;

        public override void Encode(IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("Data", _data == null ? string.Empty : _data);
        }

        public override void Decode(IDecoder decoder)
        {
            base.Decode(decoder);

            _data = decoder.Decode<string>("Data");
        }

    }

    public class PresentationQuestionExecutionHandler : IExecutionMethodHandler
    {
        protected IMediaRepository _mediaRepo;

        public PresentationQuestionExecutionHandler(IMediaRepository mediaRepo)
        {
            _mediaRepo = mediaRepo;
        }

        public string Name
        {
            get { return "Presentation"; }
        }

        public JToken Execute(string method, JToken parameters, IExecutionMethodHandlerContext context)
        {
            switch (method)
            {
                case "GetPageCounts":
                    return GetPageCounts(parameters);
            }

            return null;
        }

        public JToken GetPageCounts(JToken parameters)
        {
            JArray result = (JArray)parameters["media"];

            foreach(JObject m in result)
            {
                Guid mediaId = Guid.Empty;
                MediaDTO media = null;

                if (m["id"] != null && string.IsNullOrEmpty(m["id"].ToString()) == false)
                {
                    mediaId = Guid.Parse(m["id"].ToString());
                    media = _mediaRepo.GetById(mediaId, RepositoryInterfaces.Enums.MediaLoadInstruction.None);
                }
                else if(m["name"] != null && string.IsNullOrEmpty(m["name"].ToString()) == false)
                {
                    media = _mediaRepo.GetByNameAndType(m["name"].ToString(), null, RepositoryInterfaces.Enums.MediaLoadInstruction.None);
                }
                else if (m["filename"] != null && string.IsNullOrEmpty(m["filename"].ToString()) == false)
                {
                    media = _mediaRepo.GetByFileNameAndType(m["filename"].ToString(), null, RepositoryInterfaces.Enums.MediaLoadInstruction.None);
                }

                if(media == null)
                {
                    m["error"] = "Media could not be located";
                    continue;
                }

                m["pages"] = media.PreviewCount;
                m["id"] = media.Id.ToString();
                m["name"] = media.Name;
                m["filename"] = media.Filename;
            }

            var r = new JObject();
            r["media"] = result;

            return r;
        }
    }
}
