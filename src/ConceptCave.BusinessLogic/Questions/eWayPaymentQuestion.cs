﻿using ConceptCave.BusinessLogic.Facilities;
using ConceptCave.Checklist.Editor;
using ConceptCave.Checklist.Facilities;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.Lingo.ObjectProviders;
using ConceptCave.Checklist.RunTime;
using ConceptCave.Core;
using ConceptCave.Core.Coding;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.BusinessLogic.Questions
{
    public class eWayPaymentQuestion : Question, IObjectSetter
    {
        public eWAY.Rapid.Models.Transaction _transaction { get; set; }

        public override IAnswer CreateBlankAnswer(IPresentedQuestion presentedQuestion)
        {
            return new eWayPaymentAnswer() { PresentedQuestion = presentedQuestion };
        }

        public override object DefaultAnswer
        {
            get
            {
                return _defaultAnswer;
            }
            set
            {
                if (value is eWayPaymentAnswer)
                {
                    _defaultAnswer = ((eWayPaymentAnswer)value).AnswerValue;
                }
            }
        }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            if (_transaction != null)
            {
                encoder.Encode("Transaction", JsonConvert.SerializeObject(_transaction));
            }
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            var s = decoder.Decode<string>("Transaction");
            if (s != null)
            {
                _transaction = JsonConvert.DeserializeObject<eWAY.Rapid.Models.Transaction>(s);
            }
        }

        public bool SetObject(IContextContainer context, string name, object value)
        {
            switch(name.ToLower())
            {
                case "transaction":
                    if (value is eWAY.Rapid.Models.Transaction)
                    {
                        _transaction = (eWAY.Rapid.Models.Transaction)value;
                    }
                    else if (value is DictionaryObjectProvider)
                    {
                        var jf = new JsonFacility();
                        value = jf.ToJson(context, value);
                    }
                    if (value is string)
                    {
                        _transaction = JsonConvert.DeserializeObject<eWAY.Rapid.Models.Transaction>(value.ToString());
                    }
                    return true;
            }
            return false;
        }

        public bool GetObject(IContextContainer context, string name, out object result)
        {
            result = null;
            switch (name.ToLower())
            {
                case "transaction":
                    if (_transaction != null)
                    {
                        var jf = new JsonFacility();
                        result = jf.JsonToDictionary(context, JObject.FromObject(_transaction));
                    }
                    return true;
            }
            return false;
        }
    }

    public class eWayPaymentAnswer : Answer
    {
        private string _transactionID;
        private bool _decoding;

        public override void Decode(IDecoder decoder)
        {
            _decoding = true;
            try
            {
                base.Decode(decoder);
                TransactionID = decoder.Decode<string>("TransactionID");
            }
            finally
            {
                _decoding = false;
            }
        }

        public override void Encode(IEncoder encoder)
        {
            base.Encode(encoder);
            encoder.Encode("TransactionID", TransactionID);
        }

        public string TransactionID {
            get => _transactionID;
            set
            {
                _transactionID = value;

                if (_decoding == true)
                {
                    return;
                }

                if (_transactionID == null)
                {
                    this.PassFail = null;
                    return;
                }
                else
                {
                    UtcDateAnswered = DateTime.UtcNow;
                }
            }
        }

        public override object AnswerValue { get => _transactionID; set => _transactionID = value as string; }

        public override string AnswerAsString => _transactionID;
    }
}
