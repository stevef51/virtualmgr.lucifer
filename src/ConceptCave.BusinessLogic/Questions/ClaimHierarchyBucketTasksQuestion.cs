﻿using ConceptCave.Checklist.Editor;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core.Coding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.BusinessLogic.Questions
{
    public class ClaimHierarchyBucketTasksQuestion : Question
    {
        /// <summary>
        /// If there are tasks in the task list, should the UI be shown
        /// </summary>
        public bool HideWhenTasksPresent { get; set; }

        /// <summary>
        /// Should the question hide and reveal the task list its associated with
        /// </summary>
        public bool HideShowTaskList { get; set; }

        /// <summary>
        /// Should the question use a menu item to first reveal the buttons
        /// </summary>
        public bool UseCompactUI { get; set; }

        public bool AllowAssignmentToOtherUsers { get; set; }

        public string ListenTo { get; set; }

        public const int DefaultMaximumUsersPerLetterBucket = 20;
        public int MaximumUsersPerLetterBucket { get; set; } = DefaultMaximumUsersPerLetterBucket;

        public override bool IsAnswerable => false;

        public override object DefaultAnswer
        {
            get
            {
                return null;
            }
            set
            {

            }
        }

        public override IAnswer CreateBlankAnswer(IPresentedQuestion presentedQuestion)
        {
            return null;
        }

        public override void Encode(IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode<bool>("HideWhenTasksPresent", HideWhenTasksPresent);
            encoder.Encode<bool>("HideShowTaskList", HideShowTaskList);
            encoder.Encode<bool>("UseCompactUI", UseCompactUI);
            encoder.Encode<bool>("AllowAssignmentToOtherUsers", AllowAssignmentToOtherUsers);
            encoder.Encode<string>("ListenTo", ListenTo);
            encoder.Encode(nameof(MaximumUsersPerLetterBucket), MaximumUsersPerLetterBucket);
        }

        public override void Decode(IDecoder decoder)
        {
            base.Decode(decoder);

            HideWhenTasksPresent = decoder.Decode<bool>("HideWhenTasksPresent");
            HideShowTaskList = decoder.Decode<bool>("HideShowTaskList");
            UseCompactUI = decoder.Decode<bool>("UseCompactUI");
            AllowAssignmentToOtherUsers = decoder.Decode<bool>("AllowAssignmentToOtherUsers");
            ListenTo = decoder.Decode<string>("ListenTo");
            MaximumUsersPerLetterBucket = decoder.Decode<int>(nameof(MaximumUsersPerLetterBucket), DefaultMaximumUsersPerLetterBucket);
        }

        public override List<System.Collections.Specialized.NameValueCollection> ToNameValuePairs()
        {
            var r = base.ToNameValuePairs();

            var result = r[0];

            result.Add("hidewhentaskspresent", HideWhenTasksPresent.ToString());
            result.Add("listento", ListenTo);

            return r;
        }

        public override void FromNameValuePairs(System.Collections.Specialized.NameValueCollection pairs)
        {
            base.FromNameValuePairs(pairs);

            foreach (var name in pairs.AllKeys)
            {
                string n = name.ToLower();
                bool val = false;

                switch (n)
                {
                    case "hidewhentaskspresent":
                        if (bool.TryParse(pairs[name], out val) == true)
                        {
                            this.HideWhenTasksPresent = val;
                        }
                        break;
                    case "listento":
                        this.ListenTo = pairs[name];
                        break;
                }
            }
        }
    }


}
