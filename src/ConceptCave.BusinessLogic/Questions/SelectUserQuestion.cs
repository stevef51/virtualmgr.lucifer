﻿using System;
using System.Collections;
using System.Collections.Generic;
using ConceptCave.Checklist.Editor;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.RunTime;
using ConceptCave.Core.Coding;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Repositories;
using System.Linq;

namespace ConceptCave.BusinessLogic.Questions
{
    public class SelectUserQuestion : Question
    {
        private IMembershipRepository _membershipRepo;

        public SelectUserQuestion(IMembershipRepository membershipRepo)
        {
            _membershipRepo = membershipRepo;
            AllowedLabelIds = new List<Guid>();
            AllowedUserTypeIds = new List<int>();
        }

        public override IAnswer CreateBlankAnswer(IPresentedQuestion presentedQuestion)
        {
            var result = new SelectUserAnswer(_membershipRepo) { PresentedQuestion = presentedQuestion };

            if (DefaultAnswer != null && DefaultAnswer is Guid)
            {
                result.AnswerValue = DefaultAnswer;
            }
            else if (DefaultAnswer != null && DefaultAnswer is SelectUserAnswer)
            {
                result.AnswerValue = ((SelectUserAnswer)DefaultAnswer).SelectedUserId;
            }
            else if (DefaultAnswer != null && DefaultAnswer is IHasUserId)
            {
                result.AnswerValue = ((IHasUserId)DefaultAnswer).UserId;
            }

            return result;
        }

        public override object DefaultAnswer
        {
            get
            {
                return _defaultAnswer;
            }
            set
            {
                if (value is Guid)
                {
                    _defaultAnswer = value;
                }
                else if (value is IHasUserId)
                {
                    _defaultAnswer = ((IHasUserId)value).UserId;
                }
                else if (value is SelectUserAnswer)
                {
                    _defaultAnswer = ((SelectUserAnswer)value).AnswerValue;
                }
            }
        }

        public IList<int> AllowedUserTypeIds { get; protected set; }
        public IList<Guid> AllowedLabelIds { get; protected set; }

        public string DataSource { get; set; }
        public string UpdateDataSources { get; set; }

        public string ClearDataSources { get; set; }

        public override void Decode(IDecoder decoder)
        {
            base.Decode(decoder);
            AllowedUserTypeIds = decoder.Decode<IList<int>>("AllowedUserTypeIds", new List<int>());
            AllowedLabelIds = decoder.Decode<IList<Guid>>("AllowedLabelIds", new List<Guid>());

            DataSource = decoder.Decode<string>("DataSource");
            UpdateDataSources = decoder.Decode<string>("UpdateDataSources");
            ClearDataSources = decoder.Decode<string>("ClearDataSources");
        }

        public override void Encode(IEncoder encoder)
        {
            base.Encode(encoder);
            encoder.Encode("AllowedUserTypeIds", AllowedUserTypeIds);
            encoder.Encode("AllowedLabelIds", AllowedLabelIds);

            encoder.Encode("DataSource", DataSource);
            encoder.Encode("UpdateDataSources", UpdateDataSources);
            encoder.Encode("ClearDataSources", ClearDataSources);
        }

        public override List<System.Collections.Specialized.NameValueCollection> ToNameValuePairs()
        {
            var r = base.ToNameValuePairs();

            var result = r[0];

            result.Add("allowedusertypes", string.Join(",", AllowedUserTypeIds));
            result.Add("allowedlabels", string.Join(",", AllowedLabelIds));

            result.Add("datasource", DataSource);
            result.Add("updatedatasources", UpdateDataSources);
            result.Add("cleardatasources", ClearDataSources);

            return r;
        }

        public override void FromNameValuePairs(System.Collections.Specialized.NameValueCollection pairs)
        {
            base.FromNameValuePairs(pairs);

            foreach (var name in pairs.AllKeys)
            {
                string n = name.ToLower();

                switch (n)
                {
                    case "allowedusertypes":
                        if(string.IsNullOrEmpty(pairs[name]) == false)
                        {
                            AllowedUserTypeIds = pairs[name].Split(',').Select(s => int.Parse(s)).ToList();
                        }
                        break;
                    case "allowedlabels":
                        if(string.IsNullOrEmpty(pairs[name]) == false)
                        {
                            AllowedLabelIds = pairs[name].Split(',').Select(s => Guid.Parse(s)).ToList();
                        }
                        break;
                    case "datasource":
                        DataSource = pairs[name];
                        break;
                    case "updatedatasources":
                        UpdateDataSources = pairs[name];
                        break;
                    case "cleardatasources":
                        ClearDataSources = pairs[name];
                        break;
                }
            }
        }
    }

    public class SelectUserAnswer : Answer
    {
        public SelectUserAnswer(IMembershipRepository membershipRepo)
        {
            _membershipRepo = membershipRepo;
        }

        protected UserDataDTO _selectedUser;
        protected Guid? _selectedUserId;
        private IMembershipRepository _membershipRepo;

        public UserDataDTO SelectedUser
        {
            get
            {
                if (SelectedUserId.HasValue == false)
                {
                    return null;
                }

                if (_selectedUser == null)
                {
                    _selectedUser = _membershipRepo.GetById(_selectedUserId.Value);
                }

                return _selectedUser;
            }
        }

        public Guid? SelectedUserId
        {
            get
            {
                return _selectedUserId;
            }
            set
            {
                UtcDateAnswered = DateTime.UtcNow;
                _selectedUserId = value;

                if (_selectedUserId == Guid.Empty)
                {
                    _selectedUserId = null;
                }

                _selectedUser = null;
            }
        }

        public override object AnswerValue
        {
            get
            {
                return SelectedUserId;
            }
            set
            {
                if (value != null)
                {
                    SelectedUserId = (Guid)value;
                }
                else
                {
                    SelectedUserId = null;
                }
            }
        }

        public override void Encode(Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("SelectedUserId", SelectedUserId);
        }

        public override void Decode(Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            SelectedUserId = decoder.Decode<Guid?>("SelectedUserId", null);
        }

        public override string AnswerAsString
        {
            get 
            {
                if (SelectedUserId.HasValue == false)
                {
                    return string.Empty;
                }

                if (SelectedUser == null)
                {
                    return "Unknown User";
                }

                return SelectedUser.Name;
            }
        }

        public bool HasSelectedUserId
        {
            get
            {
                return _selectedUserId.HasValue;
            }
        }
    }
}
