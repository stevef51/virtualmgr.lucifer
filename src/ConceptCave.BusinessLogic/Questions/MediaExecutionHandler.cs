﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Checklist;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Repositories;
using Newtonsoft.Json.Linq;

namespace ConceptCave.BusinessLogic.Questions
{
    public class MediaViewingExecutionHandler : IExecutionMethodHandler
    {
        protected IMediaRepository _mediaRepo;

        public MediaViewingExecutionHandler(IMediaRepository mediaRepo)
        {
            _mediaRepo = mediaRepo;
        }

        string IExecutionMethodHandler.Name
        {
            get { return "MediaViewing"; }
        }

        Newtonsoft.Json.Linq.JToken IExecutionMethodHandler.Execute(string method, Newtonsoft.Json.Linq.JToken parameters, IExecutionMethodHandlerContext context)
        {
            switch (method)
            {
                case "Notify":
                    return Notify(parameters);
            }

            return null;
        }

        protected JToken Notify(JToken parameters)
        {
            string userIdString = parameters["userid"].ToString();
            string workingIdString = parameters["workingdocumentid"].ToString();
            string mediaIdString = parameters["mediaid"].ToString();
            string previousMediaIdString = null;
            string currentString = null;

            if (parameters["previousmediaid"] != null)
            {
                previousMediaIdString = parameters["previousmediaid"].ToString();
            }

            if (parameters["currentpageview"] != null)
            {
                currentString = parameters["currentpageview"].ToString();
            }

            string nextPageString = parameters["nextpage"].ToString();

            Guid userId = Guid.Parse(userIdString);
            Guid workingId = Guid.Parse(workingIdString);
            Guid mediaId = Guid.Parse(mediaIdString);
            Guid? currentViewId = null;
            if (string.IsNullOrEmpty(currentString) == false)
            {
                currentViewId = Guid.Parse(currentString);
            }

            int? nextPage = null;
            if (string.IsNullOrEmpty(nextPageString) == false)
            {
                nextPage = int.Parse(nextPageString);
            }

            DateTime utcNow = DateTime.UtcNow;

            if(string.IsNullOrEmpty(previousMediaIdString) == false)
            {
                Guid previousMediaId = Guid.Parse(previousMediaIdString);

                if(previousMediaId != mediaId)
                {
                    // who ever is calling us supports moving between different pieces of media, we need to close out the
                    // previous page viewing for this piece of media
                    var previousViewing = _mediaRepo.GetMediaViewingByWorkingId(workingId, previousMediaId, RepositoryInterfaces.Enums.MediaViewingLoadInstruction.PageViews);

                    previousViewing.DateCompleted = utcNow;
                    previousViewing.TotalSeconds = (int)Math.Round(previousViewing.DateCompleted.Subtract(previousViewing.DateStarted).TotalSeconds);

                    var previousViewings = from p in previousViewing.MediaPageViewings orderby p.StartDate descending select p;

                    if (previousViewings.Count() > 0)
                    {
                        previousViewings.First().EndDate = utcNow;
                        previousViewings.First().TotalSeconds = (int)Math.Round(previousViewings.First().EndDate.Subtract(previousViewings.First().StartDate).TotalSeconds);
                    }

                    _mediaRepo.SaveMediaViewing(previousViewing, false, true);
                }
            }

            // get the header record and do stuff with that
            var viewing = _mediaRepo.GetMediaViewingByWorkingId(workingId, mediaId, RepositoryInterfaces.Enums.MediaViewingLoadInstruction.PageViews);

            if (viewing == null)
            {
                var media = _mediaRepo.GetById(mediaId, RepositoryInterfaces.Enums.MediaLoadInstruction.None);

                viewing = new DTO.DTOClasses.MediaViewingDTO()
                {
                    __IsNew = true,
                    Id = CombFactory.NewComb(),
                    MediaId = mediaId,
                    UserId = userId,
                    WorkingDocumentId = workingId,
                    Version = media.Version,
                    DateStarted = utcNow,
                    DateCompleted = utcNow,
                    TotalSeconds = 0
                };
            }

            viewing.DateCompleted = utcNow;
            viewing.TotalSeconds = (int)Math.Round(viewing.DateCompleted.Subtract(viewing.DateStarted).TotalSeconds);

            // now sort out the current page view
            MediaPageViewingDTO pageView = null;

            if (currentViewId.HasValue == true)
            {
                var pageViewings = from p in viewing.MediaPageViewings where p.Id == currentViewId select p;

                if (pageViewings.Count() > 0)
                {
                    pageView = pageViewings.First();
                }
            }

            if (pageView == null)
            {
                pageView = new MediaPageViewingDTO()
                {
                    __IsNew = true,
                    Id = CombFactory.NewComb(),
                    MediaViewingId = viewing.Id,
                    Page = 1,
                    StartDate = utcNow,
                    EndDate = utcNow,
                    TotalSeconds = 0
                };
            }

            pageView.EndDate = utcNow;
            pageView.TotalSeconds = (int)Math.Round(pageView.EndDate.Subtract(pageView.StartDate).TotalSeconds);

            viewing.MediaPageViewings.Add(pageView);

            // now if this isn't the first page we need to create a new page view for the page they are moving to, if the nextPage is null
            // the user isn't moving to another page, they're moving off, so nothing to do again
            if (currentViewId.HasValue == true && nextPage.HasValue)
            {
                pageView = new MediaPageViewingDTO()
                {
                    __IsNew = true,
                    Id = CombFactory.NewComb(),
                    MediaViewingId = viewing.Id,
                    Page = nextPage.Value,
                    StartDate = utcNow,
                    EndDate = utcNow,
                    TotalSeconds = 0
                };

                viewing.MediaPageViewings.Add(pageView);
            }

            _mediaRepo.SaveMediaViewing(viewing, false, true);

            JObject result = new JObject();
            result["currentpageview"] = pageView.Id;

            return result;
        }
    }
}
