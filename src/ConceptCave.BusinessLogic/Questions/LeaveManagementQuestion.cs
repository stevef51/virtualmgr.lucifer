﻿using ConceptCave.Checklist.Editor;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Repositories;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using VirtualMgr.Central;
using VirtualMgr.Common;
using VirtualMgr.MultiTenant;

namespace ConceptCave.BusinessLogic.Questions
{
    public class LeaveManagementQuestion : Question
    {
        public bool AllowReplacedByUser { get; set; }
        public override bool IsAnswerable
        {
            get
            {
                return false;
            }
        }

        public override object DefaultAnswer
        {
            get
            {
                return null;
            }
            set
            {
            }
        }

        public override Checklist.Interfaces.IAnswer CreateBlankAnswer(Checklist.Interfaces.IPresentedQuestion presentedQuestion)
        {
            return null;
        }

        public override void Encode(Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode<bool>("AllowReplacedByUser", AllowReplacedByUser);
        }

        public override void Decode(Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            var def = true;
            AllowReplacedByUser = decoder.Decode<bool>("AllowReplacedByUser", def);
        }

        public override List<System.Collections.Specialized.NameValueCollection> ToNameValuePairs()
        {
            var r = base.ToNameValuePairs();

            var result = r[0];

            return r;
        }

        public override void FromNameValuePairs(System.Collections.Specialized.NameValueCollection pairs)
        {
            base.FromNameValuePairs(pairs);
        }
    }

    public class LeaveManagementQuestionExecutionHandler : IExecutionMethodHandler
    {
        protected IHierarchyRepository _hierarchyRepo;
        protected ITaskRepository _taskRepo;
        protected IRosterRepository _rosterRepo;
        private readonly AppTenant _appTenant;

        public LeaveManagementQuestionExecutionHandler(
            IHierarchyRepository hierarchyRepo,
            ITaskRepository taskRepo,
            IRosterRepository rosterRepo,
            AppTenant appTenant)
        {
            _hierarchyRepo = hierarchyRepo;
            _taskRepo = taskRepo;
            _rosterRepo = rosterRepo;
            _appTenant = appTenant;
        }

        public string Name
        {
            get { return "LeaveManagement"; }
        }

        public Newtonsoft.Json.Linq.JToken Execute(string method, Newtonsoft.Json.Linq.JToken parameters, IExecutionMethodHandlerContext context)
        {
            switch (method)
            {
                case "AddLeave":
                    return AddLeave(parameters);
                case "DeleteLeave":
                    return DeleteLeave(parameters);
            }

            throw new InvalidOperationException(string.Format("{0} does not exist as a method on the TaskListExecutionHandler", method));
        }

        protected bool LeaveWrappedByExistingLeave(DateTime startDate, DateTime endDate, Guid userId, int hierarchyBucketId)
        {
            var sLeave = _hierarchyRepo.GetBucketOverrides(startDate, userId, hierarchyBucketId);
            if (sLeave.Count > 0)
            {
                return true;
            }

            var eLeave = _hierarchyRepo.GetBucketOverrides(endDate, userId, hierarchyBucketId);
            if (eLeave.Count > 0)
            {
                return true;
            }

            return false;
        }

        protected bool LeaveWrapsExistingLeave(DateTime startDate, DateTime endDate, Guid userId, int hierarchyBucketId)
        {
            var leave = _hierarchyRepo.GetBucketOverrides(startDate, endDate, userId, hierarchyBucketId);

            return leave.Count != 0;
        }

        protected Newtonsoft.Json.Linq.JToken AddLeave(JToken parameters)
        {
            JArray leave = (JArray)parameters["leave"];

            JArray leaveResults = new JArray();
            List<HierarchyBucketOverrideDTO> overrides = new List<HierarchyBucketOverrideDTO>();
            foreach (var l in leave)
            {
                // we bounce back the leave objects with a little extra info (success or otherwise) as a reply

                Guid userId = Guid.Parse(l["originalUserId"].ToString());
                int hierarchyBucketId = int.Parse(l["hierarchyBucketId"].ToString());
                DateTime startDate = DateTime.ParseExact(l["startDate"].ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                DateTime endDate = DateTime.ParseExact(l["endDate"].ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);

                var hb = _hierarchyRepo.GetById(hierarchyBucketId, RepositoryInterfaces.Enums.HierarchyBucketLoadInstructions.None);
                var roster = _rosterRepo.GetForHierarchies(new int[] { hb.HierarchyId });

                var rosterTZ = TimeZoneInfoResolver.ResolveWindows(roster.First().Timezone);

                // get those dates in UTC
                startDate = TimeZoneInfo.ConvertTime(startDate, rosterTZ, TimeZoneInfo.Utc);
                endDate = TimeZoneInfo.ConvertTime(endDate, rosterTZ, TimeZoneInfo.Utc);

                JObject r = new JObject();
                r["hierarchyBucketId"] = hierarchyBucketId;
                leaveResults.Add(r);

                // we can't have multiple instances of leave for any window, so check and make sure there isn't already leave defined
                // firstly that there isn't leave that wraps the new leave
                if (LeaveWrappedByExistingLeave(startDate, endDate, userId, hierarchyBucketId) || LeaveWrapsExistingLeave(startDate, endDate, userId, hierarchyBucketId))
                {
                    r["success"] = false;
                    r["message"] = "Leave is already defined during the period specified";
                    continue;
                }

                Guid? replaceUserId = null;
                if (l["replacingUserId"] != null)
                {
                    replaceUserId = Guid.Parse(l["replacingUserId"].ToString());
                }

                HierarchyBucketOverrideDTO entry = new HierarchyBucketOverrideDTO()
                {
                    __IsNew = true,
                    HierarchyBucketOverrideTypeId = int.Parse(l["leaveType"].ToString()),
                    DateCreated = DateTime.UtcNow,
                    StartDate = startDate,
                    EndDate = endDate,
                    OriginalUserId = userId,
                    UserId = replaceUserId,
                    Notes = l["notes"].ToString(),
                    RoleId = int.Parse(l["roleId"].ToString()),
                    HierarchyBucketId = hierarchyBucketId
                };

                overrides.Add(entry);
                r["success"] = true;
            }

            using (TransactionScope scope = new TransactionScope())
            {
                foreach (var o in overrides)
                {
                    _hierarchyRepo.Save(o, false, false);
                    _taskRepo.ManageTasksForNewLeave(o, _appTenant.TimeZoneInfo());
                }

                scope.Complete();
            }

            JObject result = new JObject();
            result["leave"] = leaveResults;

            return result;
        }

        protected Newtonsoft.Json.Linq.JToken DeleteLeave(JToken parameters)
        {
            int id = int.Parse(parameters["id"].ToString());

            var leave = _hierarchyRepo.GetBucketOverride(id);

            using (TransactionScope scope = new TransactionScope())
            {
                _taskRepo.ManageTasksForLeaveRemoval(leave, _appTenant.TimeZoneInfo());
                _hierarchyRepo.DeleteHierarchyBucketOverride(id);
                scope.Complete();
            }

            JObject result = new JObject();
            result["success"] = true;

            return result;
        }
    }
}
