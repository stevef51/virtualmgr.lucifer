﻿using ConceptCave.Checklist.Editor;
using ConceptCave.Checklist.RunTime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.BusinessLogic.Questions
{
    public class pHProbeQuestion : Question
    {
        public int RefreshPeriod { get; set; }

        public decimal MinimumPassReading { get; set; }
        public decimal MaximumPassReading { get; set; }

        public override object DefaultAnswer
        {
            get
            {
                return _defaultAnswer;
            }
            set
            {
                _defaultAnswer = value;
            }
        }

        public override Checklist.Interfaces.IAnswer CreateBlankAnswer(Checklist.Interfaces.IPresentedQuestion presentedQuestion)
        {
            var result = new pHProbeAnswer() { PresentedQuestion = presentedQuestion };

            if(DefaultAnswer != null && DefaultAnswer is string)
            {
                result.AnswerValue = decimal.Parse((string)DefaultAnswer);
            }
            else if(DefaultAnswer != null && DefaultAnswer is decimal)
            {
                result.AnswerValue = (decimal)DefaultAnswer;
            }
            else if(DefaultAnswer != null && DefaultAnswer is int)
            {
                result.AnswerValue = (decimal)DefaultAnswer;
            }
            else if(DefaultAnswer != null && DefaultAnswer is pHProbeAnswer)
            {
                result.AnswerValue = ((pHProbeAnswer)DefaultAnswer).AnswerValue;
            }

            return result;
        }

        public override void Encode(Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);
            encoder.Encode<int>("RefreshPeriod", RefreshPeriod);

            encoder.Encode<decimal>("MinimumPassReading", MinimumPassReading);
            encoder.Encode<decimal>("MaximumPassReading", MaximumPassReading);
        }

        public override void Decode(Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            RefreshPeriod = decoder.Decode<int>("RefreshPeriod");

            MinimumPassReading = decoder.Decode<decimal>("MinimumPassReading");
            MaximumPassReading = decoder.Decode<decimal>("MaximumPassReading");
        }

        public bool IsInRange(object value)
        {
            decimal v = -1000;
            if(value is decimal)
            {
                v = (decimal)value;
            }
            else if(value is decimal?)
            {
                if(((decimal?)value).HasValue == true)
                {
                    v = ((decimal?)value).Value;
                }
            }
            else if(value is pHProbeAnswer)
            {
                return IsInRange(((pHProbeAnswer)value).pH);
            }

            return v >= MinimumPassReading && v <= MaximumPassReading;
        }
    }

    public class pHProbeAnswer : Answer
    {
        protected decimal? _reading;

        protected bool _decoding;

        public override void Decode(Core.Coding.IDecoder decoder)
        {
            _decoding = true;
            base.Decode(decoder);
            _decoding = false;
        }

        public decimal? pH
        {
            get
            {
                return _reading;
            }
            set
            {
                UtcDateAnswered = DateTime.UtcNow;
                _reading = value;

                if(_decoding == true)
                {
                    return;
                }

                if (_reading.HasValue == false)
                {
                    this.PassFail = null;
                    return;
                }

                var q = (pHProbeQuestion)this.PresentedQuestion.Question;

                if(_reading >= q.MinimumPassReading && _reading <= q.MaximumPassReading)
                {
                    this.PassFail = true;
                }
                else
                {
                    this.PassFail = false;
                }
            }
        }

        public override object AnswerValue
        {
            get
            {
                return pH;
            }
            set
            {
                if(value != null)
                {
                    pH = (decimal)value;
                }
                else
                {
                    pH = null;
                }
            }
        }

        public override string AnswerAsString
        {
            get { return pH.ToString(); }
        }

        public bool IsInRange()
        {
            return ((pHProbeQuestion)this.PresentedQuestion.Question).IsInRange(this);
        }
    }
}
