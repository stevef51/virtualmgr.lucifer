﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Repositories;
using Newtonsoft.Json.Linq;

namespace ConceptCave.BusinessLogic.Questions
{
    /// <summary>
    /// A general execution handler to be called by multiple question types to get country state related data
    /// </summary>
    public class CountryStatesExecutionHandler : IExecutionMethodHandler
    {
        protected ICountryStateRepository _countryStateRepo;
        protected IMembershipRepository _memberRepo;

        public CountryStatesExecutionHandler(IMembershipRepository memberRepo, ICountryStateRepository countryStateRepo)
        {
            _memberRepo = memberRepo;
            _countryStateRepo = countryStateRepo;
        }

        public JToken Execute(string method, JToken parameters, IExecutionMethodHandlerContext context)
        {
            switch (method)
            {
                case "GetAll":
                    return GetAll(parameters);

            }

            return null;
        }

        public JToken GetAll(JToken parameters)
        {
            var states = _countryStateRepo.GetAll();

            var result = new JObject();
            result["countrystates"] = ToJson(states);

            return result;
        }


        public static JArray ToJson(IList<CountryStateDTO> states)
        {
            var countryStates = new JArray();

            foreach (var state in states)
            {
                var s = new JObject();
                s["id"] = state.Id;
                s["name"] = state.Name;
                s["abbreviation"] = state.Abbreviation;

                countryStates.Add(s);
            }

            return countryStates;
        }

        public string Name
        {
            get { throw new NotImplementedException(); }
        }

    }
}
