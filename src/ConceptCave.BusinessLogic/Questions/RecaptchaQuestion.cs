﻿using ConceptCave.Checklist.Editor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.RunTime;
using Newtonsoft.Json.Linq;
using ConceptCave.Core.Coding;
using ConceptCave.Checklist.Validators;
using ConceptCave.RepositoryInterfaces.Repositories;
using System.Net;
using System.IO;

namespace ConceptCave.BusinessLogic.Questions
{
    public class RecaptchaQuestion : Question
    {
        public const string RecaptchaSiteKeyName = "recaptcha.sitekey";
        public const string RecaptchaPrivateKeyName = "recaptcha.privatekey";

        private IGlobalSettingsRepository _globalRepo;

        protected string InvalidCaptchaMessage { get; set; }

        public RecaptchaQuestion(IGlobalSettingsRepository globalRepo)
        {
            _globalRepo = globalRepo;

            InvalidCaptchaMessage = "Recaptcha failed";
        }

        // we aren't supporting a default answer as this comes from Google
        public override object DefaultAnswer
        {
            get
            {
                return null;
            }
            set
            {

            }
        }

        public override IAnswer CreateBlankAnswer(IPresentedQuestion presentedQuestion)
        {
            return new RecaptchaAnswer() { PresentedQuestion = presentedQuestion };
        }

        public override void Encode(IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode<string>("InvalidCaptchaMessage", InvalidCaptchaMessage);
        }

        public override void Decode(IDecoder decoder)
        {
            base.Decode(decoder);

            InvalidCaptchaMessage = decoder.Decode<string>("InvalidCaptchaMessage");

            if (Validators.Count == 0)
            {
                Validators.Add(new RecaptchaValidator(_globalRepo));
            }

            (from v in Validators where v is RecaptchaValidator select v).Cast<RecaptchaValidator>().First().ErrorMessage = InvalidCaptchaMessage;
        }
    }

    public class RecaptchaValidator : ValidatorWithMessage
    {
        private IGlobalSettingsRepository _globalRepo;

        public RecaptchaValidator(IGlobalSettingsRepository globalSettings)
        {
            _globalRepo = globalSettings;
        }
        public override void doProcess(IProcessingRuleContext context)
        {
            string url = _globalRepo.GetSetting<string>("recaptcha.siteverifyurl");
            if(string.IsNullOrEmpty(url))
            {
                url = "https://www.google.com/recaptcha/api/siteverify";
            }

            string privateKey = _globalRepo.GetSetting<string>(RecaptchaQuestion.RecaptchaPrivateKeyName);
            if(string.IsNullOrEmpty(privateKey))
            {
                throw new InvalidOperationException("For Recaptcha question to be used a sitekey and a privatekey must be supplied in global settings");
            }

            // so we need to POST to the url handing in the private key and the response from the answer
            var postData = string.Format("&secret={0}&response={1}",
                             privateKey,
                             ((RecaptchaAnswer)context.Answer).RecaptchaResponse);

            var postDataAsBytes = Encoding.UTF8.GetBytes(postData);

            // Create web request
            var request = WebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = postDataAsBytes.Length;

            var dataStream = request.GetRequestStream();
            dataStream.Write(postDataAsBytes, 0, postDataAsBytes.Length);
            dataStream.Close();

            // Get the response.
            var response = request.GetResponse();

            string responseFromServer = null;

            using (dataStream = response.GetResponseStream())
            {
                using (var reader = new StreamReader(dataStream))
                {
                    responseFromServer = reader.ReadToEnd();
                }
            }

            try
            {
                JObject result = JObject.Parse(responseFromServer);

                if (bool.Parse(result["success"].ToString()) == false)
                {
                    context.ValidatorResult.AddInvalidReason(ErrorMessage);
                }
            }
            catch
            {
                context.ValidatorResult.AddInvalidReason(ErrorMessage);
            }
        }
    }

    public class RecaptchaAnswer : Answer
    {
        protected string _recaptchaResponse;

        public string RecaptchaResponse
        {
            get
            {
                return _recaptchaResponse;
            }
            set
            {
                UtcDateAnswered = DateTime.UtcNow;
                _recaptchaResponse = value;
            }
        }

        public override object AnswerValue
        {
            get
            {
                return RecaptchaResponse;
            }
            set
            {
                if(value is string)
                {
                    RecaptchaResponse = (string)value;
                }
            }
        }

        public override string AnswerAsString
        {
            get
            {
                return _recaptchaResponse;
            }
        }
    }
}
