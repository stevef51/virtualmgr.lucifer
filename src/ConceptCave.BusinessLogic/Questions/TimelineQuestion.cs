﻿using ConceptCave.Checklist.Editor;
using ConceptCave.Checklist.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.BusinessLogic.Questions
{
    public class TimelineQuestion : Question
    {
        public string DataSource { get; set; }

        public string UpdateDataSources { get; set; }

        public string ClearDataSources { get; set; }

        public bool FillHeight { get; set; }

        public int? BottomPaddingWhenFillingHeight { get; set; }

        public string DateFormat { get; set; }

        public override object DefaultAnswer
        {
            get
            {
                return null;
            }
            set
            {

            }
        }

        public override IAnswer CreateBlankAnswer(IPresentedQuestion presentedQuestion)
        {
            return null;
        }

        public override bool IsAnswerable
        {
            get
            {
                return false;
            }
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);
            DataSource = decoder.Decode<string>("DataSource");
            UpdateDataSources = decoder.Decode<string>("UpdateDataSources");
            ClearDataSources = decoder.Decode<string>("ClearDataSources");
            FillHeight = decoder.Decode<bool>("FillHeight");
            BottomPaddingWhenFillingHeight = decoder.Decode<int?>("BottomPaddingWhenFillingHeight");
            DateFormat = decoder.Decode<string>("DateFormat", "medium");
        }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);
            encoder.Encode<string>("DataSource", DataSource);
            encoder.Encode<string>("UpdateDataSources", UpdateDataSources);
            encoder.Encode<string>("ClearDataSources", ClearDataSources);
            encoder.Encode<bool>("FillHeight", FillHeight);
            encoder.Encode<int?>("BottomPaddingWhenFillingHeight", BottomPaddingWhenFillingHeight);
            encoder.Encode<string>("DateFormat", DateFormat);
        }

        public override List<System.Collections.Specialized.NameValueCollection> ToNameValuePairs()
        {
            var r = base.ToNameValuePairs();

            var result = r[0];

            result.Add("datasource", DataSource);
            result.Add("updatedatasources", UpdateDataSources);
            result.Add("cleardatasources", ClearDataSources);
            result.Add("fillheight", FillHeight.ToString());
            result.Add("dateformat", DateFormat);
            if(BottomPaddingWhenFillingHeight.HasValue)
            {
                result.Add("bottompaddingwhenfillingheight", BottomPaddingWhenFillingHeight.Value.ToString());
            }

            return r;
        }

        public override void FromNameValuePairs(System.Collections.Specialized.NameValueCollection pairs)
        {
            base.FromNameValuePairs(pairs);

            foreach (var name in pairs.AllKeys)
            {
                string n = name.ToLower();

                switch (n)
                {
                    case "datasource":
                        DataSource = pairs[name];
                        break;
                    case "updatedatasources":
                        UpdateDataSources = pairs[name];
                        break;
                    case "cleardatasources":
                        ClearDataSources = pairs[name];
                        break;
                    case "fillheight":
                        bool fill = false;
                        if (bool.TryParse(pairs[name], out fill) == true)
                        {
                            this.FillHeight = fill;
                        }
                        break;
                    case "bottompaddingwhenfillingheight":
                        this.BottomPaddingWhenFillingHeight = int.Parse(pairs[name]);
                        break;
                    case "dateformat":
                        this.DateFormat = pairs[name];
                        break;
                }
            }
        }
    }
}
