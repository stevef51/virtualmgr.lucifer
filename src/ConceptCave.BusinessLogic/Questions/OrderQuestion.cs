﻿using ConceptCave.Checklist.Editor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.Core;
using ConceptCave.Core.Coding;
using Newtonsoft.Json.Linq;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Checklist.RunTime;
using System.Transactions;
using ConceptCave.Checklist;
using ConceptCave.BusinessLogic.LingoDataObjects;

namespace ConceptCave.BusinessLogic.Questions
{
    public enum OrderingUI
    {
        TabularListing = 0,
        AddRemove = 1,
        AddRemoveManualEntry = 2
    }
    public class OrderQuestion : Question, IDoNotStoreInAnswerSource
    {
        protected IProductCatalogRepository _catRepo;
        protected IFacilityStructureRepository _facRepo;

        public bool AutoCreateOrderOnFinish { get; set; }

        /// <summary>
        /// If none null is used to filter the set of catalogs available at the facility structure
        /// down to only those listed in here (comma seperated string)
        /// </summary>
        public string Catalogs { get; set; }

        public bool ShowCatalogDescriptions { get; set; }

        public bool ShowGrandTotal { get; set; }
        public bool ShowSubTotals { get; set; }

        public string ProductTitleText { get; set; }
        public string PriceTitleText { get; set; }
        public string MinStockTitleText { get; set; }
        public string StockTitleText { get; set; }
        public string QuantityTitleText { get; set; }
        public string LineItemTotalText { get; set; }

        public string LineItemNotesTitleText { get; set; }

        public string CatalogTotalText { get; set; }
        public string GrandTotalText { get; set; }

        public string LengthByWidthLengthText { get; set; }
        public string LengthByWidthWidthText { get; set; }
        public string LengthByWidthAreaText { get; set; }

        public string AddRemoveManualEntryPrompt { get; set; }
        public string AddRemoveManualEntryNotes { get; set; }

        public OrderingUI UI { get; set; }

        public override object DefaultAnswer
        {
            get
            {
                return _defaultAnswer;
            }
            set
            {
                _defaultAnswer = value;
            }
        }

        public override bool IsAnswerable => true;

        public OrderQuestion(IProductCatalogRepository catRepo, IFacilityStructureRepository facRepo)
        {
            _catRepo = catRepo;
            _facRepo = facRepo;

            ShowGrandTotal = true;
            ShowSubTotals = true;
            ShowCatalogDescriptions = true;

            ProductTitleText = "Product";
            PriceTitleText = "Price";
            MinStockTitleText = "Min Stock";
            StockTitleText = "Stock";
            QuantityTitleText = "Order";
            LineItemTotalText = "Total";
            CatalogTotalText = "Total";
            GrandTotalText = "Grand Total";
            LineItemNotesTitleText = "Notes";

            LengthByWidthLengthText = "Length";
            LengthByWidthWidthText = "Width";
            LengthByWidthAreaText = "Area";
        }

        public override void Encode(Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode<string>("Catalogs", Catalogs);
            encoder.Encode<bool>("ShowGrandTotal", ShowGrandTotal);
            encoder.Encode<bool>("ShowSubTotals", ShowSubTotals);
            encoder.Encode<bool>("ShowCatalogDescriptions", ShowCatalogDescriptions);

            encoder.Encode<string>("ProductTitleText", ProductTitleText);
            encoder.Encode<string>("PriceTitleText", PriceTitleText);
            encoder.Encode<string>("MinStockTitleText", MinStockTitleText);
            encoder.Encode<string>("StockTitleText", StockTitleText);
            encoder.Encode<string>("QuantityTitleText", QuantityTitleText);
            encoder.Encode<string>("LineItemTotalText", LineItemTotalText);
            encoder.Encode<string>("CatalogTotalText", CatalogTotalText);
            encoder.Encode<string>("GrandTotalText", GrandTotalText);
            encoder.Encode<string>("LineItemNotesTitleText", LineItemNotesTitleText);

            encoder.Encode<string>("LengthByWidthLengthText", LengthByWidthLengthText);
            encoder.Encode<string>("LengthByWidthWidthText", LengthByWidthWidthText);
            encoder.Encode<string>("LengthByWidthAreaText", LengthByWidthAreaText);

            encoder.Encode<bool>("AutoCreateOrderOnFinish", AutoCreateOrderOnFinish);

            encoder.Encode<string>("AddRemoveManualEntryPrompt", AddRemoveManualEntryPrompt);
            encoder.Encode<string>("AddRemoveManualEntryNotes", AddRemoveManualEntryNotes);

            encoder.Encode<OrderingUI>("UI", UI);
        }

        public override void Decode(Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            Catalogs = decoder.Decode<string>("Catalogs");
            ShowGrandTotal = decoder.Decode<bool>("ShowGrandTotal");
            ShowSubTotals = decoder.Decode<bool>("ShowSubTotals");
            ShowCatalogDescriptions = decoder.Decode<bool>("ShowCatalogDescriptions");

            ProductTitleText = decoder.Decode<string>("ProductTitleText");
            PriceTitleText = decoder.Decode<string>("PriceTitleText");
            MinStockTitleText = decoder.Decode<string>("MinStockTitleText");
            StockTitleText = decoder.Decode<string>("StockTitleText");
            QuantityTitleText = decoder.Decode<string>("QuantityTitleText");
            LineItemTotalText = decoder.Decode<string>("LineItemTotalText");
            CatalogTotalText = decoder.Decode<string>("CatalogTotalText");
            GrandTotalText = decoder.Decode<string>("GrandTotalText");
            LineItemNotesTitleText = decoder.Decode<string>("LineItemNotesTitleText");

            LengthByWidthLengthText = decoder.Decode<string>("LengthByWidthLengthText");
            LengthByWidthWidthText = decoder.Decode<string>("LengthByWidthWidthText");
            LengthByWidthAreaText = decoder.Decode<string>("LengthByWidthAreaText");

            AutoCreateOrderOnFinish = decoder.Decode<bool>("AutoCreateOrderOnFinish");

            AddRemoveManualEntryPrompt = decoder.Decode<string>("AddRemoveManualEntryPrompt");
            AddRemoveManualEntryNotes = decoder.Decode<string>("AddRemoveManualEntryNotes");

            UI = decoder.Decode<OrderingUI>("UI");
        }

        public override IAnswer CreateBlankAnswer(IPresentedQuestion presentedQuestion)
        {
            var result = new OrderAnswer(null) { PresentedQuestion = presentedQuestion };

            if(DefaultAnswer != null)
            {
                if(DefaultAnswer is OrderAnswer)
                {
                    result.FacilityStructureId = ((OrderAnswer)DefaultAnswer).FacilityStructureId;
                    result.AnswerValue = ((OrderAnswer)DefaultAnswer).Orders;
                }

                if(DefaultAnswer is int)
                {
                    result.FacilityStructureId = (int)DefaultAnswer;
                }

                if(DefaultAnswer is string)
                {
                    result.FacilityStructureId = int.Parse((string)DefaultAnswer);
                }
            }

            return result;
        }

        public int? FacilityStructureId
        {
            get
            {
                if(DefaultAnswer is OrderAnswer)
                {
                    return ((OrderAnswer)DefaultAnswer).FacilityStructureId;
                }
                else if(DefaultAnswer is int)
                {
                    return (int)DefaultAnswer;
                }

                return null;
            }
        }

        public IList<ProductCatalogDTO> GetCatalogs()
        {
            if(FacilityStructureId.HasValue == false)
            {
                return new List<ProductCatalogDTO>();
            }

            var result = new List<ProductCatalogDTO>();
            var rules = new List<ProductCatalogFacilityStructureRuleDTO>();
            _catRepo.GetCatalogsForFacilityStructure(FacilityStructureId.Value, result, rules);

            if(result == null)
            {
                return new List<ProductCatalogDTO>();
            }

            return (from r in result where r.Excluded == false select r).ToList();
        }
    }

    /// <summary>
    /// The OrderTemplate class models an order for exchange between server and client and allows for order questions
    /// to be used across checklist pages with Lingo handing the state from one instsance of an order question to another
    /// </summary>
    public class OrderAnswer : Answer, IMandatoryValidatorOverride
    {
        protected IOrderRepository _orderRepo;
        public int FacilityStructureId { get; set; }

        public bool OrderCreated { get; set; }

        public Guid? OrderedById { get; set; }

        public Guid? TaskId { get; set; }

        /// <summary>
        /// We are going to store the state of the order (and order items) in a JSON object as
        /// this can be simply sent down to the client and manipulated there by client side javascript
        /// and are persistance code is nice and simple.
        /// </summary>
        /// 
        protected string _ordersAsString;

        public JArray OrdersToJson()
        {
            if(string.IsNullOrEmpty(_ordersAsString))
            {
                return new JArray();
            }

            return JArray.Parse(_ordersAsString);
        }

        /// <summary>
        /// Helper method for Lingo to do some extra stuff with orders
        /// </summary>
        /// <param name="name"></param>
        /// <param name="orders"></param>
        /// <returns></returns>
        public JObject FindOrder(string name, JArray orders)
        {
            JObject result = null;

            foreach(var order in orders)
            {
                if(order["name"].ToString() == name)
                {
                    result = (JObject)order;
                    break;
                }
            }

            return result;
        }

        public JArray FilterOutOrder(string name, JArray orders)
        {
            JArray result = new JArray();

            foreach (var order in orders)
            {
                if (order["name"].ToString() != name)
                {
                    result.Add(order);
                }
            }

            return result;
        }

        public JArray FindOrderedItems(JObject order)
        {
            JArray result = new JArray();

            foreach (var i in (JArray)order["items"])
            {
                if (IsZeroValue(i, "quantity") && IsZeroValue(i, "stock"))
                {
                    continue;
                }

                result.Add(i);
            }

            return result;
        }

        public string Orders
        {
            get
            {
                return _ordersAsString;
            }
            set
            {
                UtcDateAnswered = DateTime.UtcNow;
                _ordersAsString = value;
            }
        }

        public override object AnswerValue
        {
            get
            {
                return Orders;
            }
            set
            {
                if(value is string)
                {
                    Orders = (string)value;
                }
                else if(value is JArray)
                {
                    Orders = value.ToString();
                }
            }
        }

        public override string AnswerAsString => string.Empty;

        public OrderAnswer(IOrderRepository orderRepo)
        {
            _orderRepo = orderRepo;
        }

        public override void Encode(IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode<int>("FacilityStructureId", FacilityStructureId);
            encoder.Encode<bool>("OrderCreated", OrderCreated);
            encoder.Encode<Guid?>("OrderedById", OrderedById);
            encoder.Encode<Guid?>("TaskId", TaskId);
        }

        public override void Decode(IDecoder decoder)
        {
            base.Decode(decoder);

            FacilityStructureId = decoder.Decode<int>("FacilityStructureId");
            OrderCreated = decoder.Decode<bool>("OrderCreated");
            OrderedById = decoder.Decode<Guid?>("OrderedById");
            TaskId = decoder.Decode<Guid?>("TaskId");
        }

        protected bool IsZeroValue(JToken obj, string prop)
        {
            return obj[prop] == null || string.IsNullOrEmpty(obj[prop].ToString()) || decimal.Parse(obj[prop].ToString()) == 0;
        }

        protected OrderDTO CommitOrder(JObject order, Guid orderedByUserId, Guid? taskId)
        {
            var facilityStructureId = order["facilityStructureId"] != null ? int.Parse(order["facilityStructureId"].ToString()) : FacilityStructureId;
            OrderDTO result = new OrderDTO()
            {
                __IsNew = true,
                Id = CombFactory.NewComb(),
                DateCreated = DateTime.UtcNow,
                ProductCatalogId = int.Parse(order["id"].ToString()),
                OrderedById = orderedByUserId,
                FacilityStructureId = facilityStructureId,
                ProjectJobTaskId = taskId
            };

            decimal totalPrice = 0;

            foreach(var i in (JArray)order["items"])
            {
                if(IsZeroValue(i, "quantity") && IsZeroValue(i, "stock"))
                {
                    continue;
                }

                decimal quantity = 0;
                decimal? stock = null;
                decimal? minStockLevel = null;
                decimal? price = null;
                decimal? linePrice = null;
                string notes = null;
                string priceNotes = "";

                if (i["minstocklevel"] != null && string.IsNullOrEmpty(i["minstocklevel"].ToString()) == false)
                {
                    minStockLevel = decimal.Parse(i["minstocklevel"].ToString());
                }

                if (i["stock"] != null && string.IsNullOrEmpty(i["stock"].ToString()) == false)
                {
                    stock = decimal.Parse(i["stock"].ToString());
                }

                if (i["quantity"] != null && string.IsNullOrEmpty(i["quantity"].ToString()) == false)
                {
                    quantity = decimal.Parse(i["quantity"].ToString());
                }

                if (i["price"] != null && string.IsNullOrEmpty(i["price"].ToString()) == false)
                {
                    price = decimal.Parse(i["price"].ToString());
                }

                if (i["priceNotes"] != null && string.IsNullOrEmpty(i["priceNotes"].ToString()) == false)
                {
                    priceNotes = i["priceNotes"].ToString() + "\n";
                }

                if (i["notes"] != null && string.IsNullOrEmpty(i["notes"].ToString()) == false)
                {
                    notes = i["notes"].ToString();
                }

                if (i["totalPrice"] != null && string.IsNullOrEmpty(i["totalPrice"].ToString()) == false)
                {
                    linePrice = decimal.Parse(i["totalPrice"].ToString());
                }
                else if (price.HasValue)
                {
                    linePrice = price * quantity;
                }
                totalPrice += linePrice.Value;

                var item = new OrderItemDTO()
                {
                    __IsNew = true,
                    Id = CombFactory.NewComb(),
                    Quantity = quantity,
                    Stock = stock,
                    MinStockLevel = minStockLevel,
                    Price = price,
                    TotalPrice = linePrice,
                    ProductId = int.Parse(i["productid"].ToString()),
                    Notes = priceNotes + notes
                };

                result.OrderItems.Add(item);
            }

            result.TotalPrice = totalPrice;

            return result;
        }

        protected Guid? GetReviewerId(IContextContainer context, object reviewerId)
        {
            if(reviewerId == null && context == null)
            {
                return null;
            }

            if(reviewerId != null)
            {
                if(reviewerId is SelectUserAnswer)
                {
                    return ((SelectUserAnswer)reviewerId).SelectedUserId;
                }
                if(reviewerId is Guid)
                {
                    return (Guid)reviewerId;
                }
            }

            var users = context.Get<CurrentContextFacilityCurrentUsers>();
            return users.ReviewerId;
        }

        protected Guid? GetTaskId(IContextContainer context, object taskId)
        {
            if(taskId == null)
            {
                return null;
            }

            return (Guid)taskId;
        }

        public void CommitOrders(IContextContainer context, object reviewerId, object taskId)
        {
            if(OrderCreated)
            {
                return;
            }

            JArray orders = OrdersToJson();

            Guid? reviewer = GetReviewerId(context, reviewerId);
            Guid? task = GetTaskId(context, taskId);

            if(reviewer.HasValue == false)
            {
                throw new InvalidOperationException("There must be a reviewer supplied");
            }

            using (TransactionScope scope = new TransactionScope())
            {
                foreach (var order in orders)
                {
                    var o = CommitOrder((JObject)order, reviewer.Value, task);

                    if(o.OrderItems.Count == 0)
                    {
                        // no use putting an order in if there is nothing in it
                        continue;
                    }

                    _orderRepo.Save(o, false, true);
                }

                scope.Complete();
            }


            OrderCreated = true;
        }

        public bool IsValid()
        {
            JArray orders = OrdersToJson();

            int c = 0;

            foreach (var order in orders)
            {
                foreach (var i in (JArray)order["items"])
                {
                    if (IsZeroValue(i, "quantity") && IsZeroValue(i, "stock"))
                    {
                        continue;
                    }

                    c++;
                    break;
                }

                if(c > 0)
                {
                    break;
                }
            }

            return c != 0;
        }
    }
}
