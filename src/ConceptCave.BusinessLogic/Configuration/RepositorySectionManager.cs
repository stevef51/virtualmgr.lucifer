﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using ConceptCave.Checklist;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.BusinessLogic.Configuration
{
    public class RepositorySectionManager : IRepositorySectionManager
    {
        public List<IRepositorySectionManagerItem> Items { get; protected set; }

        private readonly Dictionary<string, ActionUrlItem> _actionUrls = new Dictionary<string, ActionUrlItem>();

        private readonly Dictionary<string, ExecutionHandlerItem> _executionHandlers = new Dictionary<string, ExecutionHandlerItem>();

        private readonly List<RosterConditionActionItem> _rosterConditionActions = new List<RosterConditionActionItem>();
        private readonly Dictionary<string, RosterConditionActionItem> _rosterConditionActionsById = new Dictionary<string, RosterConditionActionItem>();

        public IReadOnlyDictionary<string, ActionUrlItem> ActionUrls
        {
            get { return new ReadOnlyDictionary<string, ActionUrlItem>(_actionUrls);}
        }

        public IList<RosterConditionActionItem> RosterConditionActions
        {
            get
            {
                return _rosterConditionActions;
            }
        }

        public ITypeOverrideResolver OverrideResolver
        {
            get { return _overrideResolver; }
        }

        public IReflectionFactory ReflectionFactory
        {
            get { return _reflectionFac; }
        }

        private readonly RepositorySectionOverrideResolver _overrideResolver;
        private readonly IReflectionFactory _reflectionFac;

        public RepositorySectionManager()
        {
            Items = new List<IRepositorySectionManagerItem>();
        }

        public RepositorySectionManager(XmlNode section, IReflectionFactory reflectionFac) : this()
        {
            //first work out overrides
            var overrideEl = section.ChildNodes.OfType<XmlElement>().Single(e => e.Name.ToLower() == "overrides");
            _overrideResolver = new RepositorySectionOverrideResolver(overrideEl);
            _reflectionFac = reflectionFac;
            
            //Now work out ActionUrls
            var actionUrlEl = section.ChildNodes.OfType<XmlElement>().Single(e => e.Name.ToLower() == "actionurls");
            foreach (var node in actionUrlEl.ChildNodes.OfType<XmlElement>().Where(n => n.Name == "add"))
            {
                _actionUrls.Add(node.Attributes["key"].Value, new ActionUrlItem()
                {
                    Pattern = node.Attributes["value"].Value,
					ServerRooted = node.Attributes["serverRooted"] != null 
                    && bool.Parse(node.Attributes["serverRooted"].Value) //server rooted unless otherwise stated
                });
            }

            // now get execution hanlders
            var executionHandlers = section.ChildNodes.OfType<XmlElement>().Single(e => e.Name.ToLower() == "executionhandlers");
            foreach (var node in executionHandlers.ChildNodes.OfType<XmlElement>().Where(n => n.Name == "add"))
            {
                _executionHandlers.Add(node.Attributes["key"].Value, new ExecutionHandlerItem()
                {
                    Name = node.Attributes["key"].Value,
                    Type = node.Attributes["value"].Value
                });
            }

            // now get roster condition actions
            var rosterConditionActions = section.ChildNodes.OfType<XmlElement>().Single(e => e.Name.ToLower() == "rosterconditionactions");
            foreach (var node in rosterConditionActions.ChildNodes.OfType<XmlElement>().Where(n => n.Name == "add"))
            {
                RosterConditionActionItem item = new RosterConditionActionItem()
                                {
                    Id = node.Attributes["key"].Value,
                    Name = node.Attributes["name"].Value,
                    Handler = node.Attributes["handler"].Value,
                    Directive = node.Attributes["directive"].Value
                };

                _rosterConditionActionsById.Add(node.Attributes["key"].Value, item);
                _rosterConditionActions.Add(item);
            }

            var nodes = section.SelectNodes("//add");
            foreach (XmlElement element in nodes)
            {
                if (element.ParentNode.Name == "overrides") continue; //we already handled these
                if (element.ParentNode.Name == "actionUrls") continue;

                var item = new RepositorySectionManagerItem(element, element.ParentNode.Name, _overrideResolver, _reflectionFac);
                Items.Add(item);
            }
        }

        public string ExecutionHandlerTypeForName(string name)
        {
            return _executionHandlers[name].Type;
        }

        public string RosterConditionActionHandlerForId(string id)
        {
            return _rosterConditionActionsById[id].Handler;
        }

        public IEnumerable<IRepositorySectionManagerItem> ItemsForCategory(string category)
        {
            return (from i in Items where i.Category == category.ToLower() select i);
        }

        public IEnumerable<IRepositorySectionManagerItem> ItemsWithUserContextAnswerPopulater()
        {
            return (from i in Items where i.HasReportingDataHandler select i);
        }

        public IRepositorySectionManagerItem ItemForAssemblyQualifiedName(string name)
        {
            var rname = _overrideResolver.ResolveTargetToOverridden(name);
            return (from i in Items where i.ObjectType == rname select i).First();
        }

        public IRepositorySectionManagerItem ItemForObjectType(Type objectType)
        {
            return ItemForAssemblyQualifiedName(objectType.AssemblyQualifiedName);
        }

        public IRepositorySectionManagerItem ItemForFriendlyType(string type)
        {
            return (from i in Items where i.FriendlyName == type select i).DefaultIfEmpty(null).First();
        }

        public IRepositorySectionManagerItem ItemForObject(object o)
        {
            return ItemForObjectType(o.GetType());
        }

        public IRepositorySectionManagerItem ItemForResourceEntity(ResourceDTO resource)
        {
            return ItemForFriendlyType(resource.Type);
        }
    }
}
