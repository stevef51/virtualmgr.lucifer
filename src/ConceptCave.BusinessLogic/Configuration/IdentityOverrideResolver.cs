//
//  Author:
//    Steve Fillingham steve.fillingham@bworkflow.com
//
//  Copyright (c) 2013, SquareIT Software PTY LTD
//
//
//
using System;
using ConceptCave.Checklist.Interfaces;

namespace ConceptCave.BusinessLogic
{
	public class IdentityOverrideResolver : ITypeOverrideResolver
	{
		public IdentityOverrideResolver ()
		{
		}

		#region ITypeOverrideResolver implementation

		public Type ResolveType(string name)
		{
			return Type.GetType (name);
		}

		public string TypeName(Type type)
		{
			return type.AssemblyQualifiedName;
		}

		public string ResolveOverriddenToTarget(string name)
		{
			return name;
		}

		public string ResolveTargetToOverridden(string name)
		{
			return name;
		}

		#endregion
	}
}

