﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ConceptCave.BusinessLogic.Configuration
{
    public class RepositorySectionManagerOverride
    {
        public string Overrides { get; set; }
        public string Target { get; set; }

        public RepositorySectionManagerOverride(XmlElement element)
        {
            Overrides = element.GetAttribute("overrides");
            Target = element.GetAttribute("target");
        }
    }
}
