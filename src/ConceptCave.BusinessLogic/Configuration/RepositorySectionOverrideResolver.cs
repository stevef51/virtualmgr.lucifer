﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using ConceptCave.Checklist.Interfaces;


namespace ConceptCave.BusinessLogic.Configuration
{
    public class RepositorySectionOverrideResolver : ITypeOverrideResolver
    {
        private readonly IList<RepositorySectionManagerOverride> _overrides = new List<RepositorySectionManagerOverride>();

        public RepositorySectionOverrideResolver(XmlElement root)
        {
            foreach (var childE in root.ChildNodes.OfType<XmlElement>()
                                        .Where(childE => childE.Name == "add"))
            {
                _overrides.Add(new RepositorySectionManagerOverride(childE));
            }
        }

        public Type ResolveType(string name)
        {
            return Type.GetType(ResolveOverriddenToTarget(name));
        }

        public string TypeName(Type type)
        {
            return ResolveTargetToOverridden(type.AssemblyQualifiedName);
        }

        public string ResolveOverriddenToTarget(string name)
        {
            var oride = _overrides.FirstOrDefault(o => o.Overrides == name);
            return oride != null ? oride.Target : name;
        }

        public string ResolveTargetToOverridden(string name)
        {
            var oride = _overrides.FirstOrDefault(o => o.Target == name);
            return oride == null ? name : oride.Overrides;
        }
    }
}
