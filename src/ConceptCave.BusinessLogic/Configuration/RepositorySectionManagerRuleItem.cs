﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Checklist;
using ConceptCave.Checklist.Interfaces;

namespace ConceptCave.BusinessLogic.Configuration
{
    public class RepositorySectionManagerRuleItem : IRepositorySectionManagerRuleItem
    {
        public string Type { get; set; }
        public string FriendlyName { get; set; }
        public string EditorModelType { get; set; }

        private readonly RepositorySectionOverrideResolver _overrideResolver;
        private readonly IReflectionFactory _reflectionFac;

        public RepositorySectionManagerRuleItem(RepositorySectionOverrideResolver overrideResolver,
                                                IReflectionFactory reflectionFac)
        {
            _overrideResolver = overrideResolver;
            _reflectionFac = reflectionFac;
        }

        public IProcessingRule CreateRule()
        {
            return (IProcessingRule) _reflectionFac.InstantiateObject(_overrideResolver.ResolveType(this.Type));
        }

        public object CreateEditorModel()
        {
            return _reflectionFac.InstantiateObject(_overrideResolver.ResolveType(this.EditorModelType));
        }
    }
}
