﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using ConceptCave.BusinessLogic.ReportData;
using ConceptCave.Checklist;
using ConceptCave.Core;

namespace ConceptCave.BusinessLogic.Configuration
{
    public class RepositorySectionManagerItem : IRepositorySectionManagerItem
    {
        public string Category { get; set; }
        public string FriendlyName { get; set; }
        public string ObjectType { get; set; }
        public string EditorModelType { get; set; }
        public string EditorPartialView { get; set; }
        public string EditorNewPromptText { get; set; }
        public string PlayerModelType { get; set; }
        public string PlayerPartialView { get; set; }
        public string PlayerDataProvider { get; set; }
        public string ReaderPartialView { get; set; }
        public string DirectiveName { get; set; }
        public string ReportingDataHandlerName { get; set; }

        public List<IRepositorySectionManagerRuleItem> Validators { get; protected set; }

        private readonly RepositorySectionOverrideResolver _overrideResolver;
        private readonly IReflectionFactory _reflectionFac;

        public RepositorySectionManagerItem(XmlElement element, string category,
                                            RepositorySectionOverrideResolver overrideResolver,
                                            IReflectionFactory reflectionFac)
        {
            _overrideResolver = overrideResolver;
            _reflectionFac = reflectionFac;

            Category = category;

            FriendlyName = element.GetAttribute("friendlyName");
            ObjectType = element.GetAttribute("type");

            XmlElement editorElement = (XmlElement) element.SelectSingleNode("editor");

            if (editorElement != null)
            {
                EditorModelType = editorElement.GetAttribute("modeltype");
                EditorPartialView = editorElement.GetAttribute("partialview");
                EditorNewPromptText = editorElement.GetAttribute("newprompt");
            }

            XmlElement playerElement = (XmlElement) element.SelectSingleNode("player");

            if (playerElement != null)
            {
                PlayerModelType = playerElement.GetAttribute("modeltype");
                PlayerPartialView = playerElement.GetAttribute("partialview");
                PlayerDataProvider = playerElement.GetAttribute("dataprovider");
            }

            var reportingElement = (XmlElement) element.SelectSingleNode("reportingdatahandler");
            if (reportingElement != null)
            {
                ReportingDataHandlerName = reportingElement.GetAttribute("name");
            }

            XmlElement readerElement = (XmlElement) element.SelectSingleNode("reader");
            if (readerElement != null)
            {
                ReaderPartialView = readerElement.GetAttribute("partialview");
            }


            var directiveElement = (XmlElement) element.SelectSingleNode("directive");
            if (directiveElement != null)
                DirectiveName = directiveElement.GetAttribute("name");

            Validators = new List<IRepositorySectionManagerRuleItem>();

            XmlElement ValidatorElement = (XmlElement) element.SelectSingleNode("rules");

            if (ValidatorElement == null)
            {
                return;
            }

            foreach (XmlElement valElement in ValidatorElement.ChildNodes)
            {
                Validators.Add(new RepositorySectionManagerRuleItem(_overrideResolver, _reflectionFac)
                    {
                        FriendlyName = valElement.GetAttribute("friendlyName"),
                        Type = valElement.GetAttribute("type"),
                        EditorModelType = valElement.GetAttribute("modeltype")
                    });
            }
        }

        public bool HasReportingDataHandler
        {
            get { return !string.IsNullOrEmpty(ReportingDataHandlerName); }
        }

        public object CreateEditorModel()
        {
            return _reflectionFac.InstantiateObject(_overrideResolver.ResolveType(this.EditorModelType));
        }

        public object CreatePlayerModel()
        {
            return _reflectionFac.InstantiateObject(_overrideResolver.ResolveType(this.PlayerModelType));
        }

        public object CreatePlayerDataProvider()
        {
            return _reflectionFac.InstantiateObject(_overrideResolver.ResolveType(this.PlayerDataProvider));
        }

        public IReportingDataHandler CreateReportingDataHandler()
        {
            return
                (IReportingDataHandler)
                _reflectionFac.InstantiateObject(_overrideResolver.ResolveType(this.ReportingDataHandlerName));
        }


        public IRepositorySectionManagerRuleItem ValidatorItemForType(string type)
        {
            return (from v in Validators where v.Type == type select v).First();
        }

        public IRepositorySectionManagerRuleItem ValidatorItemForAssemblyQualifiedName(string name)
        {
            var rname = _overrideResolver.ResolveTargetToOverridden(name);
            return (from i in Validators where i.Type == rname select i).First();
        }

        public IRepositorySectionManagerRuleItem ValidatorItemForObjectType(Type objectType)
        {
            return ValidatorItemForAssemblyQualifiedName(objectType.AssemblyQualifiedName);
        }

        public IRepositorySectionManagerRuleItem ValidatorItemForObject(object o)
        {
            return ValidatorItemForObjectType(o.GetType());
        }

        public bool HasPlayerModel
        {
            get
            {
                return string.IsNullOrEmpty(this.PlayerModelType) == false;
            }
        }

        public bool HasPlayerDataProvider
        {
            get
            {
                return string.IsNullOrEmpty(this.PlayerDataProvider) == false;
            }
        }

        public bool HasReaderPartialView
        {
            get
            {
                return string.IsNullOrEmpty(this.ReaderPartialView) == false;
            }
        }

        public IResource CreateResource()
        {
            return (IResource)_reflectionFac.InstantiateObject(_overrideResolver.ResolveType(this.ObjectType));
        }
    }
}
