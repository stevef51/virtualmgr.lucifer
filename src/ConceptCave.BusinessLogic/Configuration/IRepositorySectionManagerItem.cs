﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.BusinessLogic.ReportData;
using ConceptCave.Core;

namespace ConceptCave.BusinessLogic.Configuration
{
    public interface IRepositorySectionManagerItem
    {
        string Category { get; set; }
        string FriendlyName { get; set; }
        string ObjectType { get; set; }
        string EditorModelType { get; set; }
        string EditorPartialView { get; set; }
        string EditorNewPromptText { get; set; }
        string PlayerModelType { get; set; }
        string PlayerPartialView { get; set; }
        string PlayerDataProvider { get; set; }
        string ReaderPartialView { get; set; }
        string DirectiveName { get; set; }
        string ReportingDataHandlerName { get; set; }
        List<IRepositorySectionManagerRuleItem> Validators { get; }
        bool HasPlayerModel { get; }
        bool HasPlayerDataProvider { get; }
        bool HasReaderPartialView { get; }
        bool HasReportingDataHandler { get; }
        object CreateEditorModel();
        object CreatePlayerModel();
        object CreatePlayerDataProvider();
        IReportingDataHandler CreateReportingDataHandler();
        IRepositorySectionManagerRuleItem ValidatorItemForType(string type);
        IRepositorySectionManagerRuleItem ValidatorItemForAssemblyQualifiedName(string name);
        IRepositorySectionManagerRuleItem ValidatorItemForObjectType(Type objectType);
        IRepositorySectionManagerRuleItem ValidatorItemForObject(object o);
        IResource CreateResource();
    }
}
