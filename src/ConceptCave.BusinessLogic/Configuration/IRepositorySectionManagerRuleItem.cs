﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Checklist.Interfaces;

namespace ConceptCave.BusinessLogic.Configuration
{
    public interface IRepositorySectionManagerRuleItem
    {
        string Type { get; set; }
        string FriendlyName { get; set; }
        string EditorModelType { get; set; }
        IProcessingRule CreateRule();
        object CreateEditorModel();
    }
}
