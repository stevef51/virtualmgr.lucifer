﻿using System;
using System.Collections.Generic;
using ConceptCave.Checklist;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.BusinessLogic.Configuration
{
    public interface IRepositorySectionManager
    {
        List<IRepositorySectionManagerItem> Items { get; }
        IEnumerable<IRepositorySectionManagerItem> ItemsForCategory(string category);
        IEnumerable<IRepositorySectionManagerItem> ItemsWithUserContextAnswerPopulater();
        IRepositorySectionManagerItem ItemForAssemblyQualifiedName(string name);
        IRepositorySectionManagerItem ItemForObjectType(Type objectType);
        IRepositorySectionManagerItem ItemForFriendlyType(string type);
        IRepositorySectionManagerItem ItemForObject(object o);
        IRepositorySectionManagerItem ItemForResourceEntity(ResourceDTO resource);

        ITypeOverrideResolver OverrideResolver { get; }
        IReflectionFactory ReflectionFactory { get; }

        IReadOnlyDictionary<string, ActionUrlItem> ActionUrls { get; }

        string ExecutionHandlerTypeForName(string name);

        string RosterConditionActionHandlerForId(string id);

        IList<RosterConditionActionItem> RosterConditionActions { get; }
    }
}