﻿namespace ConceptCave.BusinessLogic.Configuration
{
    public class ActionUrlItem
    {
        public string Pattern;
        public bool ServerRooted;
    }
}