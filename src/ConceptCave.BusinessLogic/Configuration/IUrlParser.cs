﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.BusinessLogic.Configuration
{
    public interface IUrlParser
    {
        string ResolveVirtualPath(string path);
    }
}
