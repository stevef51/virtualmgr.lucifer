﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Checklist;

namespace ConceptCave.BusinessLogic.Configuration
{
    public class IdentityReflectionFactory : IReflectionFactory
    {
        public T InstantiateObject<T>()
        {
            return (T) Activator.CreateInstance(typeof (T));
        }

        public object InstantiateObject(Type type)
        {
            return Activator.CreateInstance(type);
        }
    }
}
