﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.BusinessLogic.Configuration
{
    public class RosterConditionActionItem
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Handler { get; set; }
        public string Directive { get; set; }
    }
}
