﻿using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.BusinessLogic.Invoices
{
    public class InvoiceDate
    {
        public int Year { get; set; }
        public int Month { get; set; }
        public int Day { get; set; }

        public InvoiceDate()
        {

        }

        public InvoiceDate(DateTime dt)
        {
            Year = dt.Year;
            Month = dt.Month;
            Day = dt.Day;
        }

        public InvoiceDate(DateTimeOffset dt)
        {
            Year = dt.Year;
            Month = dt.Month;
            Day = dt.Day;
        }

        public DateTimeOffset Offset(TimeSpan offset)
        {
            return new DateTimeOffset(Year, Month, Day, 0, 0, 0, offset);
        }
    }

    public class InvoiceManager 
    {
        private readonly IInvoiceRepository _invoiceRepo;
        private readonly IUserBillingTypeRepository _userBillingTypeRepo;
        private readonly TimeZoneInfo _timeZoneInfo;

        public InvoiceManager(System.TimeZoneInfo timeZoneInfo, IInvoiceRepository invoiceRepo, IUserBillingTypeRepository userBillingTypeRepo)
        {
            _timeZoneInfo = timeZoneInfo;
            _invoiceRepo = invoiceRepo;
            _userBillingTypeRepo = userBillingTypeRepo;
        }

        private static string _baseN = "23456789ABCDEFGHJKLMNPQRSTUVWXYZ";      // Note omitted I,1 and O,0 to avoid confusion with each other
        private static DateTime _startOf2016 = new DateTime(2016, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        private string MakeInvoiceId(long number)
        {
            string id = "";
            do
            {
                id = _baseN[(int)(number % _baseN.Length)] + id;
                number = number / _baseN.Length;
            } while (number > 0);
            return id;
        }

        public class GenerateRequest
        {
            public string IdPrefix { get; set; }
            public string Currency { get; set; }
            public string Description { get; set; }
            public Guid? UserId { get; set; }
            public InvoiceDate FromDate { get; set; }
            public InvoiceDate ToDate { get; set; }
            public string BillingAddress { get; set; }
            public string BillingCompanyName { get; set; }
        }
        public InvoiceDTO Generate(GenerateRequest request)
        {
            // Get the last invoice date so we know date range of the invoice
            InvoiceDTO lastInvoice = _invoiceRepo.GetLastInvoice();
            InvoiceDTO invoice = new InvoiceDTO();
            invoice.__IsNew = true;
            invoice.Id = string.Format("{0}{1}", request.IdPrefix ?? "", MakeInvoiceId((DateTime.UtcNow.Ticks - _startOf2016.Ticks) / 10000000));        // Seconds since 2016 is unique enough
            invoice.Description = request.Description;
            invoice.GeneratedByUserId = request.UserId;
            invoice.GeneratedDateUtc = DateTime.UtcNow;
            invoice.ToDateUtc = request.ToDate.Offset(_timeZoneInfo.BaseUtcOffset);
            invoice.TotalAmount = 0M;
            invoice.FromDateUtc = request.FromDate.Offset(_timeZoneInfo.BaseUtcOffset);
            invoice.Currency = request.Currency;
            invoice.BillingAddress = request.BillingAddress;
            invoice.BillingCompanyName = request.BillingCompanyName;
            int group = 1;
            // Loop through all User Billing Types and each of their Users adding them to the Invoice 
            foreach (var billingType in _userBillingTypeRepo.GetAll(UserBillingTypeLoadInstructions.MembershipType | UserBillingTypeLoadInstructions.AndMembership))
            {
                var groupItem = new InvoiceLineItemDTO()
                {
                    __IsNew = true,
                    Line = group.ToString(),
                    ParentLine = null,
                    DetailLevel = 0,
                    Description = billingType.Name,
                    UnitPrice = billingType.AmountPerMonth
                };
                invoice.InvoiceLineItems.Add(groupItem);

                int line = 1;
                foreach (var userType in billingType.UserTypes)
                {
                    foreach(var userData in userType.UserDatas)
                    {
                        DateTimeOffset userFrom = request.FromDate.Offset(_timeZoneInfo.BaseUtcOffset);
                        DateTimeOffset userTo = request.ToDate.Offset(_timeZoneInfo.BaseUtcOffset);
                        string userDescription = string.Format("{0} - {1}", userType.Name, userData.Name);

                        if (userData.CommencementDate.HasValue)
                        {
                            var commencementDateUtc = userData.CommencementDate.Value.ToUniversalTime();
                            if (commencementDateUtc > userFrom.ToUniversalTime() && commencementDateUtc < userTo.ToUniversalTime())
                            {
                                userFrom = new DateTimeOffset(userData.CommencementDate.Value, _timeZoneInfo.BaseUtcOffset);
                                userDescription += string.Format(" (Start {0:d MMM yyyy})", userData.CommencementDate.Value);
                            }
                        }
                        if (userData.ExpiryDate.HasValue)
                        {
                            var expiryDateUtc = userData.ExpiryDate.Value.ToUniversalTime();
                            if (expiryDateUtc > userFrom.ToUniversalTime() && expiryDateUtc < userTo.ToUniversalTime())
                            {
                                userTo = new DateTimeOffset(userData.ExpiryDate.Value, _timeZoneInfo.BaseUtcOffset);
                                userDescription += string.Format(" (End {0:d MMM yyyy})", userData.ExpiryDate.Value);
                            }
                        }
                        
                        var userMonths = CalculateMonths(userFrom, userTo);
                        if (userMonths > 0)
                        {
                            var lineItem = new InvoiceLineItemDTO()
                            {
                                __IsNew = true,
                                ParentLine = groupItem.Line,
                                DetailLevel = 1,
                                Description = userDescription,
                                UserId = userData.UserId,
                                Quantity = userMonths,
                                UnitPrice = billingType.AmountPerMonth
                            };
                            lineItem.Total = Decimal.Round(lineItem.UnitPrice * lineItem.Quantity, 2);
                            if (lineItem.UnitPrice > 0)
                            {
                                // Recalculate Quantity such that Group Quantity total tallies correctly when * with UnitPrice
                                lineItem.Quantity = lineItem.Total / lineItem.UnitPrice;
                            }
                            groupItem.Quantity += lineItem.Quantity;
                            groupItem.Total += lineItem.Total;

                            if (lineItem.Total > 0M)
                            {
                                lineItem.Line = string.Format("{0}.{1}", groupItem.Line, line++);
                                invoice.InvoiceLineItems.Add(lineItem);
                            }
                        }
                    }
                }

                group++;
                invoice.TotalAmount += groupItem.Total;
            }

            return _invoiceRepo.SaveInvoice(invoice, true);
        }

        public decimal CalculateMonths(DateTimeOffset fromDate, DateTimeOffset toDate)
        {
            // Work in Tenant time at Midnight
            fromDate = fromDate.ToOffset(_timeZoneInfo.BaseUtcOffset).Date;
            toDate = toDate.ToOffset(_timeZoneInfo.BaseUtcOffset).Date;

            if (toDate < fromDate)
            {
                return 0M;
            }

            var months = 0M;
            DateTimeOffset dt = fromDate;        // Midnight Tenant time
            if (dt.Day == toDate.Day)
            {
                while (dt < toDate.Date)
                {
                    months += 1M;
                    dt = dt.AddMonths(1);
                }
            }
            else
            {
                while (dt < toDate.Date)
                {
                    var daysInMonth = (decimal)DateTime.DaysInMonth(dt.Year, dt.Month);
                    if (dt.Year == toDate.Year && dt.Month == toDate.Month)
                    {
                        // Same month, calculate day difference
                        var dayDiff = toDate.Day - dt.Day;
                        months = months + (dayDiff / daysInMonth);
                        dt = toDate.Date;
                    }
                    else if (dt.Day != 1)
                    {
                        // Partial month
                        months = months + ((daysInMonth - dt.Day) / daysInMonth);
                        var nextMonth = dt.AddMonths(1);
                        dt = new DateTimeOffset(nextMonth.Year, nextMonth.Month, 1, 0, 0, 0, _timeZoneInfo.BaseUtcOffset);
                    }
                    else
                    {
                        months += 1M;
                        dt = dt.AddMonths(1);
                    }
                }
            }
            return Decimal.Round(months, 6);
        }

        public bool DeleteInvoice(string id)
        {
            InvoiceDTO invoice = _invoiceRepo.GetInvoiceById(id);
            if (invoice == null)
                return false;

            // Cannot delete a Paid or Issued Invoice
            if (invoice.PaymentDateUtc.HasValue)
                return false;
            if (invoice.IssuedDateUtc.HasValue)
                return false;

            return _invoiceRepo.DeleteInvoice(id);
        }

        public class CheckInvoiceDatesResult
        {
            public InvoiceDTO FromClash { get; set; }
            public InvoiceDTO ToClash { get; set; }
        }

        public CheckInvoiceDatesResult CheckInvoiceDates(InvoiceDate fromDate, InvoiceDate toDate)
        {
            var result = new CheckInvoiceDatesResult();
            result.FromClash    = _invoiceRepo.FindInvoiceCovering(fromDate.Offset(_timeZoneInfo.BaseUtcOffset));
            result.ToClash      = _invoiceRepo.FindInvoiceCovering(toDate.Offset(_timeZoneInfo.BaseUtcOffset));
            return result;
        }

        public InvoiceDTO MarkAsPaid(string id, string paymentReference, DateTime paymentDate)
        {
            var invoice = _invoiceRepo.GetInvoiceById(id);
            if (invoice == null)
                return null;
            invoice.PaymentReference = paymentReference;
            invoice.PaymentDateUtc = paymentDate.ToUniversalTime();
            return _invoiceRepo.SaveInvoice(invoice, true);
        }

        public InvoiceDTO MarkAsUnpaid(string id)
        {
            return _invoiceRepo.MarkAsUnpaid(id);
        }

        public class NextInvoiceParametersResult
        {
            public bool FirstInvoice { get; set; }
            public bool FullInvoice { get; set; }
            public InvoiceDate FromDate { get; set; }
            public InvoiceDate ToDate { get; set; }
        }

        public NextInvoiceParametersResult GetNextInvoiceParameters()
        {
            var lastInvoice = _invoiceRepo.GetLastInvoice();
            var today = new DateTimeOffset(DateTime.Now.Date.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0, _timeZoneInfo.BaseUtcOffset);    // Midnight in the TimeZone

            if (lastInvoice == null)
            {
                return new NextInvoiceParametersResult()
                {
                    FirstInvoice = true,
                    FullInvoice = true,
                    FromDate = new InvoiceDate(today.AddMonths(-1)),              // 1 month back from today
                    ToDate = new InvoiceDate(today)
                };
            }

            // Not 1st invoice, calculate To date in full months
            var toDate = lastInvoice.ToDateUtc.ToOffset(_timeZoneInfo.BaseUtcOffset);
            while(toDate.AddMonths(1) < today)
            {
                toDate = toDate.AddMonths(1);
            }

            // Partial invoice possible ?
            if (toDate == lastInvoice.ToDateUtc)
            {
                return new NextInvoiceParametersResult()
                {
                    FirstInvoice = false,
                    FullInvoice = false,
                    FromDate = new InvoiceDate(lastInvoice.ToDateUtc),
                    ToDate = new InvoiceDate(today)
                };
            }
            else
            {
                return new NextInvoiceParametersResult()
                {
                    FirstInvoice = false,
                    FullInvoice = true,
                    FromDate = new InvoiceDate(lastInvoice.ToDateUtc),
                    ToDate = new InvoiceDate(toDate)
                };
            }
        }
    }
}
