﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using ConceptCave.BusinessLogic.Questions;
using ConceptCave.Checklist;
using ConceptCave.Checklist.Editor;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.RunTime;
using ConceptCave.Core;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using Newtonsoft.Json.Linq;
using System.Transactions;

namespace ConceptCave.BusinessLogic
{
    public class AnswerConverter : TypedCachedMethodDispatcher<Action<JObject, IPresentedQuestion, IWorkingDocument>>
    {
        private readonly IMediaRepository _mediaRepo;
        private readonly IMediaManager _mediaManager;
        private readonly IMembershipRepository _memberRepo;
        private readonly IGlobalSettingsRepository _settingsRepo;

		public AnswerConverter(IMediaRepository mediaRepo, 
            IMembershipRepository memberRepo,
            IMediaManager mediaManager, 
            IGlobalSettingsRepository settingsRepo) : this(mediaRepo, memberRepo, mediaManager, settingsRepo, true)
		{
		}

		public AnswerConverter(IMediaRepository mediaRepo, 
            IMembershipRepository memberRepo,
            IMediaManager mediaManager, 
            IGlobalSettingsRepository settingsRepo, bool shouldCache) : base(shouldCache)
		{
		    _mediaRepo = mediaRepo;
            _memberRepo = memberRepo;
		    _mediaManager = mediaManager;
            _settingsRepo = settingsRepo;
		}

        protected override Action<JObject, IPresentedQuestion, IWorkingDocument> ConstructDelegate(MethodInfo method)
        {
            return
                (Action<JObject, IPresentedQuestion, IWorkingDocument>)
                Delegate.CreateDelegate(typeof(Action<JObject, IPresentedQuestion, IWorkingDocument>), this, method);
        }

        public void FillInAnswers(JObject answers, IWorkingDocument document, Context.ContextType contextType = Context.ContextType.None)
        {
            if (answers == null)
            {
                return; // there ain't much we can do without answers
            }

            //The reason we do AsDepthFirstEnumerable() instead of Presented.Last()
            //Because we could be going backwards, and so the last presented won't actually be
            //what we have answers for
            //Therefore, we must search for any question which we have an answer for
            foreach (var presented in document.PresentedAsDepthFirstEnumerable())
            {
                var pQuestion = presented as IPresentedQuestion;
                if (pQuestion == null) //This is a section, do not care about it
                    continue;
                if (!pQuestion.Question.IsAnswerable) //This is a label or something, still do not care about it
                    continue;

                var delg = GetMethod(pQuestion.Question.GetType());
                try
                {
                    JToken answer = null;
                    if(contextType == Context.ContextType.None)
                    {
                        answer = answers[pQuestion.Id.ToString()];
                    }
                    else
                    {
                        // when using context info we have to search for the question id as it doesn't change between
                        // server loads (the working document isn't held in the db)
                        foreach(var a in answers)
                        {
                            if(a.Value["QuestionId"].ToString() == pQuestion.Question.Id.ToString())
                            {
                                answer = a.Value;
                                break;
                            }
                        }
                    }

                    // Default to question was not hidden
                    pQuestion.WasHidden = false;
                    if (answer != null) //we have an answer for this one
                    {
                        // If clientside decided to hide the question it will set "WasHidden" - we can then ignore server side validation on it
                        if (answer["WasHidden"] != null)
                        {
                            pQuestion.WasHidden = bool.Parse(answer["WasHidden"].ToString());
                        }

                        delg((JObject)answer, pQuestion, document);

                        // Handle extra values generically
                        var extraValues = pQuestion.GetAnswer() as IAnswerExtraValues;
                        if (extraValues != null)
                        {
                            var dict = new Dictionary<string, object>();
                            var jExtraValues = answer["ExtraValues"] as JObject;
                            if (jExtraValues != null)
                            {
                                foreach(var jev in jExtraValues.Properties())
                                {
                                    dict[jev.Name] = jev.Value?.ToString();
                                }
                            }
                            extraValues.ExtraValues = dict;
                        }
                    }
                }
                catch (KeyNotFoundException)
                {
                    //TODO: Should probably log this, but just do nothing for now
                    //This means that there was no answer object provided for this question
                }
            }
        }

        [HandlesType(typeof(LabelQuestion))]
        protected void LabelQuestionAnswerer(JObject answer, IPresentedQuestion pQuestion, IWorkingDocument document)
        {
            // we don't do anything except stop other code falling over
        }

        [HandlesType(typeof (FreeTextQuestion))]
        protected void FreeTextQuestionAnswerer(JObject answer, IPresentedQuestion pQuestion, IWorkingDocument document)
        {
            //answer it
            var wdAnswer = (FreeTextAnswer) pQuestion.GetAnswer();
            wdAnswer.AnswerText = (string) answer["Text"];
        }

        [HandlesType(typeof(NumberQuestion))]
        protected void NumberQuestionAnswerer(JObject answer, IPresentedQuestion pQuestion, IWorkingDocument document)
        {
            //answer it
            var wdAnswer = (NumberAnswer)pQuestion.GetAnswer();
            var number = answer["Number"];
            if (number != null)
            {
                var n = number.ToString();

                decimal d;
                if (decimal.TryParse(n, out d))
                {
                    wdAnswer.AnswerNumber = d;
                }
                else
                {
                    wdAnswer.AnswerNumber = null;
                }
            }
            else
            {
                wdAnswer.AnswerNumber = null;
            }
        }

        [HandlesType(typeof(TemperatureProbeQuestion))]
        protected void TemperatureProbeQuestionAnswerer(JObject answer, IPresentedQuestion pQuestion, IWorkingDocument document)
        {
            //answer it
            var wdAnswer = (TemperatureProbeAnswer)pQuestion.GetAnswer();

            var n = answer["Celcius"].ToString();

            wdAnswer.Celcius = string.IsNullOrEmpty(n) ? (decimal?)null : decimal.Parse(n);
        }

        [HandlesType(typeof(pHProbeQuestion))]
        protected void pHProbeQuestionAnswerer(JObject answer, IPresentedQuestion pQuestion, IWorkingDocument document)
        {
            //answer it
            var wdAnswer = (pHProbeAnswer)pQuestion.GetAnswer();

            var n = answer["pH"].ToString();

            wdAnswer.pH = string.IsNullOrEmpty(n) ? (decimal?)null : decimal.Parse(n);
        }

        [HandlesType(typeof(DateTimeQuestion))]
        protected void DateTimeQuestionAnswerer(JObject answer, IPresentedQuestion pQuestion, IWorkingDocument document)
        {
            var wdAnswer = (DateTimeAnswer)pQuestion.GetAnswer();
            var q = (DateTimeQuestion)pQuestion.Question;

            var answerValue = string.Format("{0:o}", answer["DateTime"]);
            if (answerValue == string.Empty)
            {
                wdAnswer.AnswerDateTime = null;
            }
            else
            {
                try
                {
                    wdAnswer.AnswerDateTime = DateTime.ParseExact(answerValue, "o", System.Globalization.CultureInfo.InvariantCulture);
                }
                catch
                {
                    wdAnswer.AnswerDateTime = DateTime.ParseExact(answerValue, "yyyy-dd-mm", System.Globalization.CultureInfo.InvariantCulture);
                }

            }
        }

        [HandlesType(typeof (MultiChoiceQuestion))]
        protected void MultiChoiceQuestionAnswerer(JObject answer, IPresentedQuestion pQuestion, IWorkingDocument document)
        {
            var wdAnswer = (MultiChoiceAnswer) pQuestion.GetAnswer();
            var q = (MultiChoiceQuestion) pQuestion.Question;

            //Why do we do this backwards?
            //Because of BWORK-158
            var chosenItems = new ListOf<IMultiChoiceItem>();

            var selectedToken = answer["Selected"];
            //There are three forms our data might come in
            //1- selectedToken might just be a plain string; in which case it is a GUID for the
            //only selected choice.
            //2- Or, selectedToken might be an object, in which case keys represent choice GUIDs
            //and values are bools which represent whether or not it was selected
            //3- Or, selectedToken might be an array, in which case entries represent GUIDs
            //for selected choices.

            switch (selectedToken.Type)
            {
                case JTokenType.String:
                case JTokenType.Guid:
                    //Case 1 above
                    var pGuidVal = Guid.Parse((string)selectedToken); //This cast works either way thankfully.
                    chosenItems.Add(q.List.Single(it => it.Id == pGuidVal));
                    break;
                case JTokenType.Object:
                    //Case 2 above
                    var pObjVal = (JObject) selectedToken;
                    foreach (var item in pObjVal)
                        if ((bool) item.Value)
                            chosenItems.Add(q.List.Single(it => it.Id == Guid.Parse(item.Key)));
                    break;
                case JTokenType.Array:
                    //Case 3 above
                    var pArrVal = (JArray) selectedToken;
                    foreach (var item in pArrVal)
                        chosenItems.Add(q.List.Single(it => it.Id == Guid.Parse((string)item)));
                    break;
                case JTokenType.None:
                case JTokenType.Null:
                case JTokenType.Undefined:
                    //Elected not to give any answer
                    break;
                default:
                    throw new InvalidOperationException("Invalid format for selected answer- must be a Guid, Object or Array");
            }

            wdAnswer.ChosenItems = chosenItems;
        }

        [HandlesType(typeof(CheckboxQuestion))]
        protected void CheckboxQuestionAnswerer(JObject answer, IPresentedQuestion pQuestion, IWorkingDocument document)
        {
            var wdAnswer = (CheckboxAnswer)pQuestion.GetAnswer();
            var q = (CheckboxQuestion)pQuestion.Question;

            var answerValue = answer["Value"].ToString();

            wdAnswer.AnswerBool = answerValue == string.Empty ? (bool?)null : bool.Parse(answerValue);
        }

        [HandlesType(typeof(SelectUserQuestion))]
        protected void SelectUserQuestionAnswerer(JObject answer, IPresentedQuestion pQuestion, IWorkingDocument document)
        {
            var wdAnswer = pQuestion.GetAnswer() as SelectUserAnswer;
            var idToken = answer["SelectedId"];
            if (idToken.Type == JTokenType.String)
                wdAnswer.SelectedUserId = Guid.Parse((string)idToken);
        }

        [HandlesType(typeof(SelectODataQuestion))]
        protected void SelectODataQuestionAnswerer(JObject answer, IPresentedQuestion pQuestion, IWorkingDocument document)
        {
            var wdAnswer = pQuestion.GetAnswer() as SelectODataAnswer;
            var selected = answer["Selected"];
            if (selected.Type == JTokenType.Object)
                wdAnswer.JObject = (JObject)selected;
        }

        [HandlesType(typeof(SelectFacilityStructureQuestion))]
        protected void SelectFacilityStructureQuestionAnswerer(JObject answer, IPresentedQuestion pQuestion, IWorkingDocument document)
        {
            var wdAnswer = pQuestion.GetAnswer() as SelectFacilityStructureAnswer;
            var idToken = answer["SelectedFacilityStructureId"];

            wdAnswer.SelectedStructureId = int.Parse(idToken.ToString());
        }

        [HandlesType(typeof(MediaDirectoryQuestion))]
        protected void MediaDirectoryQuestionAnswerer(JObject answer, IPresentedQuestion pQuestion, IWorkingDocument document)
        {
            var wdAnswer = pQuestion.GetAnswer() as MediaAnswer;
            var idToken = answer["SelectedId"];
            if (idToken != null && idToken.Type == JTokenType.String)
                wdAnswer.SetSelectedMediaFolderFile(Guid.Parse((string)idToken));
        }

        private string ExtensionToContentType(string fname)
        {
            var extension = fname.Substring(fname.LastIndexOf('.'));
            switch (extension)
            {
                case ".png":
                    return "image/png";
                case ".jpg":
                case ".jpeg":
                    return "image/jpg";
                default:
                    return "";
            }
        }

		[HandlesType(typeof(UploadMediaQuestion))]
		protected void UploadMediaQuestionAnswerer(JObject answer, IPresentedQuestion pQuestion, IWorkingDocument document)
		{
		    var wdAnswer = pQuestion.GetAnswer() as UploadMediaAnswer;
		    ((ICollection<IUploadMediaAnswerItem>)wdAnswer.Items).Clear();

            var q = (IUploadMediaQuestion)pQuestion.Question;

            //our answer is actually an array
		    foreach (var item in ((JArray)answer["Items"]).Cast<JObject>())
		    {
		        //either it has mediaId set or it does not
		        MediaDTO mediaEnt;
		        if (item["mediaId"] != null)
		        {
		            //need to update the media entity
                    var id = Guid.Parse((string)item["mediaId"]);
		            mediaEnt = _mediaRepo.GetById(id, MediaLoadInstruction.None);
		            mediaEnt.Description = (string) (item["description"] ?? "");
                    mediaEnt.Name = (string)(item["name"] ?? "");
                    //filename not changed
		            _mediaRepo.Save(mediaEnt, false, false);
		        }
		        else
		        {
                    //we've got  b64 data
		            var data = Convert.FromBase64String((string) item["data"]);
		            mediaEnt = new MediaDTO()
		            {
		                __IsNew = true,
		                DateCreated = DateTime.UtcNow,
		                Description = (string) (item["description"] ?? ""),
		                Name = (string) (item["name"] ?? ""),
		                Filename = (string) (item["fname"] ?? ""),
		                Type = ExtensionToContentType((string) (item["fname"] ?? "")),
                        Id = CombFactory.NewComb()
		            };
		            var dataEnt = new MediaDataDTO()
		            {
		                __IsNew = true,
		                Data = data,
		                MediaId = mediaEnt.Id
		            };
		            mediaEnt.MediaData = dataEnt;
		            dataEnt.Medium = mediaEnt;

                    using(TransactionScope scope = new TransactionScope())
                    {
                        _mediaRepo.Save(mediaEnt, true, true);

                        if(q.Destination != UploadMediaQuestionDestination.NotFiled)
                        {
                            // no matter what we are going to need a file object that points to our media, we can then
                            // file it under the relevant folder once we've worked out where that is
                            MediaFolderFileDTO file = new MediaFolderFileDTO()
                            {
                                __IsNew = true,
                                AutoCreated = true,
                                Id = CombFactory.NewComb(),
                                MediaId = mediaEnt.Id
                            };

                            var rootFolderId = _settingsRepo.GetSetting<Guid>(GlobalSettingsConstants.SystemSetting_UploadMediaQuestionUploadFolderId);
                            if(rootFolderId == Guid.Empty)
                            {
                                // we need to create the root folder
                                MediaFolderDTO rootFolder = new MediaFolderDTO()
                                {
                                    __IsNew = true,
                                    AutoCreated = true,
                                    Text = "Uploads",
                                    Id = CombFactory.NewComb()
                                };

                                _mediaRepo.SaveFolder(rootFolder, false, false);

                                _settingsRepo.SetSetting(GlobalSettingsConstants.SystemSetting_UploadMediaQuestionUploadFolderId, rootFolder.Id);

                                rootFolderId = rootFolder.Id;
                            }

                            UploadMediaQuestionDestination d = q.Destination;

                            var users = document.Program.Get<ConceptCave.BusinessLogic.LingoDataObjects.CurrentContextFacilityCurrentUsers>();

                            if(users.RevieweeId.HasValue == false && d != UploadMediaQuestionDestination.Reviewer)
                            {
                                // if there is no reviewee, then we fall back to using the reviewer folder structure
                                d = UploadMediaQuestionDestination.Reviewer;
                            }

                            MediaFolderDTO folder = null;

                            if(d == UploadMediaQuestionDestination.Reviewee)
                            {
                                var reviewee = _memberRepo.GetById(users.RevieweeId.Value);

                                folder = GetOrCreateFolder(rootFolderId, reviewee.Name);
                            }
                            else if(d == UploadMediaQuestionDestination.Reviewer)
                            {
                                var reviewer = _memberRepo.GetById(users.ReviewerId);

                                folder = GetOrCreateFolder(rootFolderId, reviewer.Name);
                            }
                            else if(d == UploadMediaQuestionDestination.RevieweeReviewer)
                            {
                                var reviewee = _memberRepo.GetById(users.RevieweeId.Value);

                                folder = GetOrCreateFolder(rootFolderId, reviewee.Name);

                                var reviewer = _memberRepo.GetById(users.ReviewerId);

                                folder = GetOrCreateFolder(folder.Id, reviewer.Name);
                            }
                            else if(d == UploadMediaQuestionDestination.ReviewerReviewee)
                            {
                                var reviewer = _memberRepo.GetById(users.ReviewerId);

                                folder = GetOrCreateFolder(rootFolderId, reviewer.Name);

                                var reviewee = _memberRepo.GetById(users.RevieweeId.Value);

                                folder = GetOrCreateFolder(folder.Id, reviewee.Name);
                            }

                            file.MediaFolderId = folder.Id;
                            _mediaRepo.SaveFolderFile(file, false, false);
                        }

                        scope.Complete();
                    }

		        }

                wdAnswer.Items.Add(new UploadMediaAnswerItem(_mediaManager)
                {
                    ContentType = mediaEnt.Type,
                    Filename = mediaEnt.Filename,
                    Name = mediaEnt.Name,
                    Description = mediaEnt.Description,
                    UploadDate = mediaEnt.DateCreated,
                    MediaId = mediaEnt.Id
                });
		    }
		}

        protected MediaFolderDTO GetOrCreateFolder(Guid parentId, string folderName)
        {
            var folder = _mediaRepo.GetFolder(parentId, folderName, MediaFolderLoadInstruction.None);

            if(folder == null)
            {
                folder = new MediaFolderDTO()
                {
                    __IsNew = true,
                    AutoCreated = true,
                    Text = folderName,
                    ParentId = parentId,
                    Id = CombFactory.NewComb()
                };

                _mediaRepo.SaveFolder(folder, true, false);
            }

            return folder;
        }

        [HandlesType(typeof(PresentWordDocumentQuestion))]
        protected void PresentWordDocumentQuestionAnswerer(JObject answer, IPresentedQuestion pQuestion, IWorkingDocument document)
        {
            var wdAnswer = pQuestion.GetAnswer() as PresentWordDocumentAnswer;

            wdAnswer.LastPageRead = int.Parse(answer["lastpageread"].Value<string>());
            wdAnswer.AllPagesRead = bool.Parse(answer["allpagesread"].Value<string>());
        }

        [HandlesType(typeof (GeoLocationQuestion))]
        protected void GeolocationQuestionAnswerer(JObject answer, IPresentedQuestion pQuestion, IWorkingDocument document)
        {
            var wdAnswer = pQuestion.GetAnswer() as GeoLocationAnswer;
            if (answer["Error"] != null && !string.IsNullOrEmpty(answer["Error"].Value<string>()))
            {
                wdAnswer.Coordinates = new LatLng((string) answer["Error"]);
            }
            else
            {
                wdAnswer.Coordinates = new LatLng((decimal) answer["Latitude"], (decimal) answer["Longitude"],
                    (decimal) answer["Accuracy"]);
            }
        }

        [HandlesType(typeof(SignatureQuestion))]
        protected void SignatureQuestionAnswerer(JObject answer, IPresentedQuestion pQuestion, IWorkingDocument document)
        {
            var wdAnswer = (SignatureAnswer)pQuestion.GetAnswer();
            var q = (SignatureQuestion)pQuestion.Question;

            var idToken = answer["Signature"];
            if (idToken.Type == JTokenType.String)
                wdAnswer.AnswerAsJsonString = (string)idToken;
        }

        [HandlesType(typeof(SelectCompanyQuestion))]
        protected void SelectCompanyQuestionAnswerer(JObject answer, IPresentedQuestion pQuestion, IWorkingDocument document)
        {
            var wdAnswer = (SelectCompanyAnswer)pQuestion.GetAnswer();
            var q = (SelectCompanyQuestion)pQuestion.Question;

            var idToken = answer["SelectedId"];
            if (idToken.Type == JTokenType.String)
                wdAnswer.SelectedCompanyId = Guid.Parse((string)idToken);
            else if (string.IsNullOrEmpty(idToken.Value<string>()) == true)
            {
                wdAnswer.SelectedCompanyId = null;
            }
        }

        [HandlesType(typeof(OrderQuestion))]
        protected void OrderQuestionAnswerer(JObject answer, IPresentedQuestion pQuestion, IWorkingDocument document)
        {
            //answer it
            var wdAnswer = (OrderAnswer)pQuestion.GetAnswer();
            wdAnswer.AnswerValue = answer["orders"];
        }

        [HandlesType(typeof(eWayPaymentQuestion))]
        protected void eWayPaymentQuestionAnswerer(JObject answer, IPresentedQuestion pQuestion, IWorkingDocument document)
        {
            //answer it
            var wdAnswer = (eWayPaymentAnswer)pQuestion.GetAnswer();

            wdAnswer.TransactionID = answer["TransactionID"]?.ToString();
        }

        [HandlesType(typeof(PendingPaymentQuestion))]
        protected void PendingPaymentQuestionAnswerer(JObject answer, IPresentedQuestion pQuestion, IWorkingDocument document)
        {
            // not much to do with PendingPayments
            var wdAnswer = (PendingPaymentAnswer)pQuestion.GetAnswer();
        }

        [HandlesType(typeof(RecaptchaQuestion))]
        protected void RecaptchaQuestionAnswerer(JObject answer, IPresentedQuestion pQuestion, IWorkingDocument document)
        {
            //answer it
            var wdAnswer = (RecaptchaAnswer)pQuestion.GetAnswer();
            wdAnswer.RecaptchaResponse = (string)answer["recaptchaResponse"];
        }

        [HandlesType(typeof(PresentationQuestion))]
        protected void PresentationQuestionAnswerer(JObject answer, IPresentedQuestion pQuestion, IWorkingDocument document)
        {
            //answer it
            var wdAnswer = (PresentationQuestionAnswer)pQuestion.GetAnswer();
            wdAnswer.Data = answer["Data"].ToString();
        }
    }
}