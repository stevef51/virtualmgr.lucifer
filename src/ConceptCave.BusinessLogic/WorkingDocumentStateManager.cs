﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using ConceptCave.BusinessLogic.Facilities;
using ConceptCave.BusinessLogic.LingoDataObjects;
using ConceptCave.BusinessLogic.ReportData;
using ConceptCave.BusinessLogic.Context;
using ConceptCave.Checklist;
using ConceptCave.Checklist.Editor;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.Lingo.Functions;
using ConceptCave.Checklist.RunTime;
using ConceptCave.Core;
using ConceptCave.Core.XmlCodable;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.BusinessLogic.Models;
using Newtonsoft.Json.Linq;
using ConceptCave.Checklist.Core;
using ConceptCave.Core.Coding;
using ConceptCave.Checklist.Facilities;
using ConceptCave.Checklist.Lingo.ObjectProviders;

namespace ConceptCave.BusinessLogic
{
    public class WorkingDocumentStateManager : IWorkingDocumentStateManager
    {
        public static string DesignDocumentRootElementName = "Document";
        public class WorkingDocumentContainer
        {
            public IWorkingDocument DocumentObject { get; set; }
            public WorkingDocumentDTO DocumentEntity { get; set; }

            public WorkingDocumentContainer()
            {
            }

            public WorkingDocumentContainer(IWorkingDocument documentObject, WorkingDocumentDTO documentEntity)
            {
                DocumentEntity = documentEntity;
                DocumentObject = documentObject;
            }
        }
   

        private readonly RenderConverter _rConverter;
        private readonly AnswerConverter _aConverter;
        private readonly IWorkingDocumentRepository _wdRepo;
        private readonly IBlockedRepository _blockedRepo;
        private readonly IPublishingGroupResourceRepository _pubResourceRepo;
        private readonly IPublishingGroupRepository _pubGroupRepo;
        private readonly IReportingTableWriter _reportWriter;
        private readonly IReflectionFactory _reflectionFactory;
        private readonly IMembershipRepository _memberRepo;
		private readonly IGlobalSettingsRepository _globalSettingRepo;

        public bool AllowSave { get; set; }

        public WorkingDocumentStateManager(RenderConverter rConverter, AnswerConverter aConverter,
            IWorkingDocumentRepository wdRepo, IBlockedRepository blockedRepo,
            IPublishingGroupResourceRepository pubResourceRepo, IReportingTableWriter reportWriter,
			IReflectionFactory reflectionFactory, IMembershipRepository memberRepo, IPublishingGroupRepository pubGroupRepo,
			IGlobalSettingsRepository globalSettingsRepo)
        {
            _rConverter = rConverter;
            _aConverter = aConverter;
            _wdRepo = wdRepo;
            _blockedRepo = blockedRepo;
            _pubResourceRepo = pubResourceRepo;
            _reportWriter = reportWriter;
            _reflectionFactory = reflectionFactory;
            _memberRepo = memberRepo;
            _pubGroupRepo = pubGroupRepo;
			_globalSettingRepo = globalSettingsRepo;

            AllowSave = true;
        }

        public PlayerModel ConstructPlayerModel(IWorkingDocument wdObject, WorkingDocumentDTO documentEntity,
                                             IValidatorResult validatorResult = null)
        {
            var returnModel = new PlayerModel()
            {
                WorkingDocumentId = documentEntity.Id,
                ChecklistName = documentEntity.Name,
                IsValid = validatorResult == null || !validatorResult.Invalid,
                State = wdObject.RunMode,
                CanPrevious = wdObject.CanPrevious(),
                RootStepCount = wdObject.Sections.Count
            };

            return returnModel;
        }

        public PlayerModel ContinueWorkingDocument(IWorkingDocument wdObj, WorkingDocumentDTO wdEntity)
        {
            var playerModel = ConstructPlayerModel(wdObj, wdEntity);
            var presented = wdObj.GetPresented();

            var currentUsersHelper = new CurrentContextFacilityCurrentUsers()
            {
                RevieweeId = wdEntity.RevieweeId,
                ReviewerId = wdEntity.ReviewerId
            };
            wdObj.Program.Set<CurrentContextFacilityCurrentUsers>(currentUsersHelper);

            //dirty temp hack: inject a href call target into context
            var href = _reflectionFactory.InstantiateObject<HrefFacility>();
            if (!wdObj.Facilities.OfType<HrefFacility>().Any())
            {
                wdObj.Program.Set<ICallTarget>("href", new DelegateCallTarget()
                {
                    Callback = (ctx, args) => href.Call(ctx, args)
                });
            }

            // EVS-699 "Mandatory" flag in checklist stops AutoFinish 
            // This was discovered as a part of tracking EVS-699, documents that have been finished but are marked as "Started" 
            // (as a result of EVS-699 bug) will throw InvalidOperation "Only presented ..." errors below, we can test and avert this
            if (presented != null)
            {
                playerModel.Presented = _rConverter.MakePresentedRenderable(presented, null, wdObj);
            }

            return playerModel;
        }

        public IList<PlayerModel> PlayDashboards(Guid uid, InjectContextObjects injector = null)
        {
            var dashboardResources = _memberRepo.GetDashboardData(uid);
            var compoundModel = new List<PlayerModel>();
            foreach (var res in dashboardResources)
            {
                //begin each working document
                IDictionary<string, object> args = null;
                if (!string.IsNullOrWhiteSpace(res.Config))
                {
                    var jsonFacility = new JsonFacility();
                    var json = jsonFacility.TryFromJson(null, res.Config);
                    if (json.Valid && json.Object is DictionaryObjectProvider)
                    {
                        args = ((DictionaryObjectProvider)json.Object).Dictionary;
                    }
                }
                var container = BeginWorkingDocument(res.PublishingGroupResourceId, uid, uid, null, args); //dashboards reviewee is self
                container.DocumentEntity.RevieweeId = uid;
                container.DocumentEntity.DateCreated = DateTime.UtcNow;

                var currentUsersHelper = new CurrentContextFacilityCurrentUsers()
                {
                    RevieweeId = uid,
                    ReviewerId = uid
                };
                container.DocumentObject.Program.Set<CurrentContextFacilityCurrentUsers>(currentUsersHelper);
                if (injector != null)
                    injector(container.DocumentObject);

                var pModel = AdvanceWorkingDocument(container.DocumentObject, container.DocumentEntity);

                if(string.IsNullOrEmpty(res.Title) == false)
                {
                    pModel.ChecklistName = res.Title;
                }

                if(string.IsNullOrEmpty(res.Tooltip) == false)
                {
                    pModel.ChecklistDescription = res.Tooltip;
                }

                pModel.AreDashboarding = true;
                compoundModel.Add(pModel);
            }

            return compoundModel;
        }

        public PlayerModel ReverseWorkingDocument(IWorkingDocument wdObject, WorkingDocumentDTO wdEntity)
        {
            if (!wdObject.CanPrevious())
            {
                throw new InvalidOperationException("Cannot rewind this working document");
            }

            var currentUsersHelper = new CurrentContextFacilityCurrentUsers()
            {
                RevieweeId = wdEntity.RevieweeId,
                ReviewerId = wdEntity.ReviewerId
            };
            wdObject.Program.Set<CurrentContextFacilityCurrentUsers>(currentUsersHelper);

            //we can previous, so let's previous
            wdObject.ProcessRules(ProcessDirection.Backward);
            wdObject.Previous();


            //And no matter what, there is a presentable at the moment here
            var presented = wdObject.GetPresented();
            var playerModel = ConstructPlayerModel(wdObject, wdEntity);
            playerModel.Presented = _rConverter.MakePresentedRenderable(presented, null, wdObject);
            return playerModel;
        }

        public PlayerModel SaveState(IWorkingDocument wdObject, WorkingDocumentDTO wdEntity, JObject answers)
        {
            var preState = wdObject.RunMode;
            var postState = preState;
            IValidatorResult validatorResult = null;

            //in all cases..
            wdObject.Program.Set<IWorkingDocument>(wdObject);
            wdObject.Program.Set<WorkingDocumentDTO>(wdEntity);

            //dirty temp hack: inject a href call target into context
            var href = _reflectionFactory.InstantiateObject<HrefFacility>();
            if (!wdObject.Facilities.OfType<HrefFacility>().Any())
            {
                wdObject.Program.Set<ICallTarget>("href", new DelegateCallTarget()
                {
                    Callback = (ctx, args) => href.Call(ctx, args)
                });
            }

            var currentUsersHelper = new CurrentContextFacilityCurrentUsers()
            {
                RevieweeId = wdEntity.RevieweeId,
                ReviewerId = wdEntity.ReviewerId
            };
            wdObject.Program.Set<CurrentContextFacilityCurrentUsers>(currentUsersHelper);

            if (preState == RunMode.Presenting)
            {
                _aConverter.FillInAnswers(answers, wdObject);
            }

            PlayerModel playerModel;

            //Shoot back a presentable
            var presented = wdObject.GetPresented(); //if we're Finished and there was no finish with, will be null
            //Assemble the presentable response
            playerModel = ConstructPlayerModel(wdObject, wdEntity, validatorResult);
            if (presented != null)
                playerModel.Presented = _rConverter.MakePresentedRenderable(presented, validatorResult, wdObject);
            //undefined otherwise
            //If we're here cos we finished and we're on iOS, rewind the document afterwards
            //This ensures that finish can be run again on the server
            return playerModel;
        }

        public PlayerModel AdvanceWorkingDocument(IWorkingDocument wdObject, WorkingDocumentDTO wdEntity, JObject answers = null)
        {
            return AdvanceWorkingDocument(wdObject, ref wdEntity, answers);
        }
        public PlayerModel AdvanceWorkingDocument(IWorkingDocument wdObject, ref WorkingDocumentDTO wdEntity, JObject answers = null)
        {
            var preState = wdObject.RunMode;
            var postState = preState;
            IValidatorResult validatorResult = null;

            //in all cases..
            wdObject.Program.Set<IWorkingDocument>(wdObject);
            wdObject.Program.Set<WorkingDocumentDTO>(wdEntity);

            //dirty temp hack: inject a href call target into context
            var href = _reflectionFactory.InstantiateObject<HrefFacility>();
            if (!wdObject.Facilities.OfType<HrefFacility>().Any())
            {
               wdObject.Program.Set<ICallTarget>("href", new DelegateCallTarget()
               {
                   Callback = (ctx, args) => href.Call(ctx, args)
               });
            }

            var currentUsersHelper = new CurrentContextFacilityCurrentUsers()
            {
                RevieweeId = wdEntity.RevieweeId,
                ReviewerId = wdEntity.ReviewerId
            };
            wdObject.Program.Set<CurrentContextFacilityCurrentUsers>(currentUsersHelper);

            if (preState == RunMode.Presenting)
            {
                //They just presented something, hence they will be expecting the answers
                _aConverter.FillInAnswers(answers, wdObject);
                validatorResult = wdObject.ProcessRules(ProcessDirection.Forward); //Do the non-lingo validation
                if (!validatorResult.Invalid)
                {
                    postState = wdObject.Next();
                    validatorResult = wdObject.Program.Get<IValidatorResult>(); //get lingo validation result
                }
            }
            else if (preState != RunMode.Finishing && preState != RunMode.Finished)
            {
                //If we haven't been given back a finishing checklist, do an advance
                postState = wdObject.Next();
            }
            else if (preState == RunMode.Finishing)
            {
				//Need to do the finishing on the server
				#if !iOS
                FinishWorkingDocument(wdObject, ref wdEntity);
				#endif
                //see if there's an extra presentable
                postState = wdObject.Next(); //should now be finished
            }

            PlayerModel playerModel;

            switch (postState)
            {
                case RunMode.Blocked:
                    //Record the blocking
                    foreach (var blocker in wdObject.Blockers)
                    {
                        //Add a record to blockers table
                        var blockEntity = _blockedRepo.Load(wdObject.Id, blocker.BlockedById);
                        _blockedRepo.Save(blockEntity);
                    }
                    //Assemble a blocked response
                    playerModel = ConstructPlayerModel(wdObject, wdEntity);
                    return playerModel;
                case RunMode.Finished:
                    //If it's finished, neeed to set the working document status
					#if iOS
						wdEntity.Status = (int)WorkingDocumentStatus.PendingCompletion;
					#else
						wdEntity.Status = (int)WorkingDocumentStatus.Completed;
					#endif
                    goto case RunMode.Presenting;
				case RunMode.Presenting:
                    //Shoot back a presentable
					var presented = wdObject.GetPresented (); //if we're Finished and there was no finish with, will be null
                    //Assemble the presentable response
					playerModel = ConstructPlayerModel (wdObject, wdEntity, validatorResult);
					if (presented != null)
						playerModel.Presented = _rConverter.MakePresentedRenderable (presented, validatorResult, wdObject);
                    //undefined otherwise
					//If we're here cos we finished and we're on iOS, rewind the document afterwards
					//This ensures that finish can be run again on the server
                    return playerModel;
                case RunMode.Finishing:
                    //Assemble a "call next to finish" response
                    //the ConstructPlayerModel call sets playerModel.State to Finishing, which is enough
                    //to tell the client what to do
                    playerModel = ConstructPlayerModel(wdObject, wdEntity);

                    if (wdObject.FinishMode == FinishMode.Auto)
                    {
                        // back round the loop one more time should finish this sucker off for us
                        playerModel = AdvanceWorkingDocument(wdObject, ref wdEntity, answers);
                    }

                    return playerModel;
                case RunMode.Running:
                    //Don't think this should happen?
                    throw new InvalidOperationException("Next() returned a Running RunMode");
                default:
                    throw new InvalidOperationException("Unknonw Run Mode");
            }
        }

        public IDesignDocument DesignDocumentFromString(string xml)
        {
			// Don't need to check the GlobalSettings.DocumentCoderType since SBONDecoder.GetCoder will parse the "xml" and use the correct Decoder 
            var decoder = CoderFactory.GetDecoder(xml, _reflectionFactory);
            DesignDocument designdoc;
            decoder.Decode(DesignDocumentRootElementName, out designdoc);
            return designdoc;
        }

        public string DesignDocumentToString(IDesignDocument doc)
        {
			IEncoder encoder = CoderFactory.CreateEncoder();
            encoder.Encode(DesignDocumentRootElementName, doc);
			return encoder.ToString();
        }

        public IWorkingDocument WorkingDocumentFromString(string xml)
        {
            return WorkingDocument.CreateFromString(xml, _reflectionFactory);
        }

        public string WorkingDocumentToString(IWorkingDocument doc)
        {
            IEncoder encoder = CoderFactory.CreateEncoder();
            encoder.Encode(WorkingDocument.WorkingDocumentRootElementName, doc);
			return encoder.ToString ();
        }

        public IWorkingDocument WorkingDocumentFromDesignDocument(IDesignDocument designDocument, IDictionary<string, object> args)
        {
            GuaranteeProgramSourceCode(designDocument);
            var wdoc = new WorkingDocument(designDocument);
            if (args != null)
                wdoc.Variables.Variable<object>("Arguments", new DictionaryObjectProvider() { Dictionary = args });
            return wdoc;
        }

        public WorkingDocumentContainer BeginWorkingDocument(PublishingGroupResourceDTO resourceent, Guid reviewerId,
            Guid? revieweeId, Guid? parentId = null, IDictionary<string, object> args = null)
        {
            if (resourceent == null)
                throw new ArgumentException("No publishing resource was found");

            //begin constructing our new entity
            var wdent = new WorkingDocumentDTO()
            {
                __IsNew = true,
                DateCreated = DateTime.UtcNow,
                PublishingGroupResourceId = resourceent.Id,
                Name = resourceent.Name,
                ReviewerId = reviewerId,
                RevieweeId = revieweeId,
                ParentId = parentId,
                Status = (int)WorkingDocumentStatus.Started
            };

            //cleanupafter code
            if (resourceent.IfNotFinishedCleanUpAfter.HasValue)
                wdent.CleanUpIfNotFinishedAfter =
                    DateTime.UtcNow.AddSeconds(resourceent.IfNotFinishedCleanUpAfter.Value);
            if (resourceent.IfFinishedCleanUpAfter.HasValue)
                wdent.CleanUpIfFinishedAfter =
                    DateTime.UtcNow.AddSeconds(resourceent.IfFinishedCleanUpAfter.Value);

            var designdoc = DesignDocumentFromString(resourceent.PublishingGroupResourceData.Data);
            var workingdoc = WorkingDocumentFromDesignDocument(designdoc, args);
            ResolveWorkingDocumentTemplates(workingdoc);

            //set up finishing
            if (resourceent.PopulateReportingData)
            {
                workingdoc.Facilities.Add(new FinishWorkingDocumentFacility(_reportWriter));
            }

            wdent.Id = workingdoc.Id;

            //set up data entity
            var wddataent = new WorkingDocumentDataDTO { __IsNew = true, WorkingDocumentId = wdent.Id };
            wdent.WorkingDocumentData = wddataent;
            wddataent.WorkingDocument = wdent;

            return new WorkingDocumentContainer(workingdoc, wdent);
        }

        public WorkingDocumentContainer BeginWorkingDocument(int groupId, int resourceId, Guid reviewerId, Guid? revieweeId, Guid? parentId = null, IDictionary<string, object> args = null)
        {
            //first grab the resource
            var resourceent = _pubResourceRepo.GetByCompoundId(groupId, resourceId,
                                                               PublishingGroupResourceLoadInstruction.Data);
            return BeginWorkingDocument(resourceent, reviewerId, revieweeId, parentId, args);
        }

        public WorkingDocumentContainer BeginWorkingDocument(int pubResourceid, Guid reviewerId, Guid? revieweeId, Guid? parentId = null, IDictionary<string, object> args = null)
        {
            var resourceent = _pubResourceRepo.GetById(pubResourceid, PublishingGroupResourceLoadInstruction.Data);
            return BeginWorkingDocument(resourceent, reviewerId, revieweeId, parentId, args);
        }

        public WorkingDocumentContainer BeginWorkingDocument(string pubGroupName, string resourceName,
            string reviewerName, string revieweeName = null, Guid? parentId = null, IDictionary<string, object> args = null)
        {
            var reviewerId = _memberRepo.GetByName(reviewerName).UserId;
            var revieweeId = revieweeName != null ? _memberRepo.GetByName(revieweeName).UserId : (Guid?)null;
            var pubGroupId = _pubGroupRepo.GetByName(pubGroupName).Id;
            var resourceEnt = _pubResourceRepo.GetByGroupAndName(pubGroupId, resourceName,
                PublishingGroupResourceLoadInstruction.Data);

            return BeginWorkingDocument(resourceEnt, reviewerId, revieweeId, parentId, args);
        }

        public void FinishWorkingDocument(IWorkingDocument wdDocument, ref WorkingDocumentDTO wdEntity)
        {
            if (wdDocument.RunMode != RunMode.Finishing)
                throw new InvalidOperationException("Cannot finish a document that is not ready for it");

            // get the finishing trigger from the program and add its final action
            var finishTrigger =
                (from t in wdDocument.Facilities where t is FinishWorkingDocumentFacility select t)
                .DefaultIfEmpty(null).First() as FinishWorkingDocumentFacility;

            // we pre-emptively save things before we do a finish as Lingo may have made changes to the
            // working document (through the Context.RevieweeId for example) that needs to get picked up
            // in the finishing side of things
            wdEntity = SaveWorkingDocument(wdDocument, wdEntity);

            if (finishTrigger != null && AllowSave == true)
            {
                finishTrigger.AddAction(wdDocument.Program);
            }
            // Execute all server actions in order ..
            var serverActions = wdDocument.Actions.OfType<IServerBasedAction>();
            serverActions.ToList().ForEach(a => a.ServerExecute(wdDocument.Program));

            wdEntity.Status = (int)WorkingDocumentStatus.Completed;
            wdEntity = SaveWorkingDocument(wdDocument, wdEntity);

            //unblock blockers
            var blockedEntities = _blockedRepo.GetByWaitingForId(wdEntity.Id);
            foreach (var blocked in blockedEntities)
            {
                var blockedWDEntity = _wdRepo.GetById(blocked.BlockedId, WorkingDocumentLoadInstructions.Data);
                var blockedWD = WorkingDocumentFromString(blockedWDEntity.WorkingDocumentData.Data);
                var wdeCopy = wdEntity;
                var blocker = blockedWD.Blockers.First(b => b.BlockedById == wdeCopy.Id);
                if (blocker != null)
                {
                    blocker.Unblock();
                    AdvanceWorkingDocument(blockedWD, blockedWDEntity);
                    _wdRepo.Save(blockedWDEntity, false, true);
                }
            }
        }

        public void ResolveWorkingDocumentTemplates(IWorkingDocument doc)
        {
            ResolveWorkingDocumentTemplates(doc.AsDepthFirstEnumerable());
        }

        public void ResolveWorkingDocumentTemplates(IEnumerable<IPresentable> enumerable)
        {
            //find everything that needs to be templated
            var tasklist = from q in enumerable
                           where q is ITemplatedPresentable &&
                            (q as ITemplatedPresentable).TemplatePublishedResourceId.HasValue &&
                            (q as ITemplatedPresentable).TemplateQuestionId.HasValue
                           select q as ITemplatedPresentable;

            var templateCache = new Dictionary<int, IDesignDocument>();

            foreach (var question in tasklist)
            {
                IDesignDocument templateDoc;

                if (templateCache.TryGetValue(question.TemplatePublishedResourceId.Value, out templateDoc) == false)
                {
                    //get the template
                    var templateDocEntity = _pubResourceRepo.GetById(question.TemplatePublishedResourceId.Value,
                        PublishingGroupResourceLoadInstruction.Data);
                    if (templateDocEntity == null)
                        continue;

                    templateDoc = DesignDocumentFromString(templateDocEntity.PublishingGroupResourceData.Data);

                    templateCache.Add(question.TemplatePublishedResourceId.Value, templateDoc);
                }

                var templateQ = (from q in templateDoc.AsDepthFirstEnumerable()
                                 where q is ITemplatedPresentable &&
                                       q is IUniqueNode &&
                                       (q as IUniqueNode).Id == question.TemplateQuestionId
                                 select (q as ITemplatedPresentable)).FirstOrDefault();

                if (templateQ == null)
                    continue;

                //found template source and target. copy relevant properties over
                question.MutateFromTemplate(templateQ);
            }
        }

        public WorkingDocumentDTO SaveWorkingDocument(IWorkingDocument wd, WorkingDocumentDTO wdEntity)
        {
			wdEntity.UtcLastModified = wdEntity.WorkingDocumentData.UtcLastModified = DateTime.UtcNow;

            if(AllowSave == true)
            {
                wdEntity.WorkingDocumentData.Data = WorkingDocumentToString(wd);
                return _wdRepo.Save(wdEntity, true, true);
            }
            else
            {
                return wdEntity;
            }
        }

        public void GuaranteeProgramSourceCode(IDesignDocument designDocument)
        {
            if (string.IsNullOrEmpty(designDocument.ProgramDefinition.SourceCode))
            {
                designDocument.ProgramDefinition.SourceCode = "present all sections";
            }
        }

        public bool CancelWorkingDocument(Guid workingDocumentId)
        {
            var wd = _wdRepo.GetById(workingDocumentId, WorkingDocumentLoadInstructions.None);
            if (wd != null)
            {
                wd.Status = (int)WorkingDocumentStatus.Cancelled;
                _wdRepo.Save(wd, false, false);
            }
            return wd != null;
        }
    }
}