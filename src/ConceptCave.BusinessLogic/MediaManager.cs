﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Checklist;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using System.Transactions;

namespace ConceptCave.BusinessLogic
{
    public class MediaManager : IMediaManager
    {
        private readonly IMediaRepository _mediaRepo;

        public MediaManager(IMediaRepository mediaRepo)
        {
            _mediaRepo = mediaRepo;
        }

        public Stream GetStream(string filename, string mimeType)
        {
            var ent = _mediaRepo.GetByFileNameAndType(filename, mimeType, MediaLoadInstruction.Data);
            return ent == null ? null : new MemoryStream(ent.MediaData.Data);
        }

        public Stream GetStream(Guid id)
        {
            var ent = _mediaRepo.GetById(id, MediaLoadInstruction.Data);
            return ent == null ? null : new MemoryStream(ent.MediaData.Data);
        }

        public Guid SaveStream(Stream stream, string filename, string mimeType, Guid? folderId, Guid? mediaId, IWorkingDocument document)
        {
            if ((mediaId.HasValue && mediaId.Value == Guid.Empty))
            {
                throw new ArgumentException("mediaId needs to be a valid GUID or empty");
            }

            if(folderId.HasValue && folderId.Value == Guid.Empty)
            {
                throw new ArgumentException("folder id needs to be a valid GUID or empty");
            }

            MediaFolderDTO folder = null;
            if(folderId.HasValue)
            {
                folder = _mediaRepo.GetFolderById(folderId.Value, MediaFolderLoadInstruction.None);
            }

            MediaDTO mediaEntity;
            if (mediaId.HasValue)
                mediaEntity = _mediaRepo.GetById(mediaId.Value, MediaLoadInstruction.Data);
            else
            {
                var mid = CombFactory.NewComb();
                mediaEntity = new MediaDTO()
                {
                    __IsNew = true,
                    Id = mid,
                    Filename = filename,
                    Type = mimeType,
                    Name = filename,
                    DateCreated = DateTime.UtcNow,
                    DateVersionCreated = DateTime.UtcNow,
                    MediaData = new MediaDataDTO()
                    {
                        __IsNew = true,
                        MediaId = mid
                    }
                };
            }

            mediaEntity.Name = filename ?? mediaEntity.Name;    // Allow a resave to change the Name 
            mediaEntity.WorkingDocumentId = document.Id;
            mediaEntity.MediaData.Data = new byte[stream.Length];
            stream.Read(mediaEntity.MediaData.Data, 0, (int)stream.Length);
            
            using(TransactionScope scope = new TransactionScope())
            {
                _mediaRepo.Save(mediaEntity, false, true);

                if (folder != null)
                {
                    var folderFile = new MediaFolderFileDTO()
                    {
                        Id = CombFactory.NewComb(),
                        __IsNew = true,
                        MediaId = mediaEntity.Id,
                        MediaFolderId = folder.Id
                    };

                    _mediaRepo.SaveFolderFile(folderFile, false, false);
                }

                scope.Complete();
            }

            return mediaEntity.Id;
        }

        public Guid SaveStream(Stream stream, string filename, string mimeType, Guid? mediaId, IWorkingDocument document)
        {
            return SaveStream(stream, filename, mimeType, null, mediaId, document);
        }

        public void MapMediaToUser(Guid mediaId, Guid userId)
        {
            _mediaRepo.MapMediaToUser(userId, mediaId);
        }

        public void MapMediaToOrder(Guid mediaId, Guid orderId, string tag = "")
        {
            _mediaRepo.MapMediaToOrder(orderId, mediaId, tag);
        }

        public void Delete(Guid mediaId)
		{
			_mediaRepo.Delete (mediaId);
		}

        public IMediaItem GetById(Guid id)
        {
            if (id == Guid.Empty)
            {
                return null;
            }

            return new Facilities.MediaFacility.LibraryMediaItem(this) { MediaId = id };
        }

        public IMediaItem GetByMediaFileId(Guid id)
        {
            if (id == Guid.Empty)
            {
                return null;
            }

            var m = _mediaRepo.GetFolderFileById(id, MediaFolderFileLoadInstruction.None);

            if (m == null)
            {
                return null;
            }

            return new Facilities.MediaFacility.LibraryMediaItem(this) { MediaId = m.MediaId };
        }

        public IMediaItem GetByName(string name, string type)
        {
            if(string.IsNullOrEmpty(name))
            {
                return null;
            }

            var m = _mediaRepo.GetByNameAndType(name, type, MediaLoadInstruction.None);

            if(m == null)
            {
                return null;
            }

            return new Facilities.MediaFacility.LibraryMediaItem(this) { MediaId = m.Id };
        }

        public void SetMediaViewingAccepted(Guid workingDocumentId, bool accepted, string acceptionNotes)
        {
            var view = _mediaRepo.GetMediaViewingByWorkingId(workingDocumentId, MediaViewingLoadInstruction.None);
            view.Accepted = accepted;
            view.AcceptionNotes = acceptionNotes;

            _mediaRepo.SaveMediaViewing(view, false, false);
        }
        public string MediaName(Guid id)
        {
            var ent = _mediaRepo.GetById(id, MediaLoadInstruction.None);
            return ent != null ? ent.Name : null;
        }
        public string MediaType(Guid id)
        {
            var ent = _mediaRepo.GetById(id, MediaLoadInstruction.None);
            return ent != null ? ent.Type : null;
        }

        public IList<string> MediaLabels(Guid id)
        {
            var ent = _mediaRepo.GetById(id, MediaLoadInstruction.MediaLabel | MediaLoadInstruction.Label);

            if(ent.LabelMedias.Count == 0)
            {
                return new List<string>();
            }

            return (from m in ent.LabelMedias select m.Label.Name).ToList();
        }

        public void CopyMediaViewing(Guid workingDocumentId, Guid mediaId, Guid[] userIds)
        {
            var viewing = _mediaRepo.GetMediaViewingByWorkingId(workingDocumentId, MediaViewingLoadInstruction.PageViews);

            using (TransactionScope scope = new TransactionScope())
            {
                foreach(var id in userIds)
                {
                    var newViewing = new DTO.DTOClasses.MediaViewingDTO()
                    {
                        __IsNew = true,
                        Id = CombFactory.NewComb(),
                        MediaId = viewing.MediaId,
                        UserId = id,
                        WorkingDocumentId = viewing.WorkingDocumentId,
                        Version = viewing.Version,
                        DateStarted = viewing.DateStarted,
                        DateCompleted = viewing.DateCompleted,
                        TotalSeconds = viewing.TotalSeconds
                    };

                    foreach(var pView in viewing.MediaPageViewings)
                    {
                        var pageView = new MediaPageViewingDTO()
                        {
                            __IsNew = true,
                            Id = CombFactory.NewComb(),
                            MediaViewingId = newViewing.Id,
                            Page = pView.Page,
                            StartDate = pView.StartDate,
                            EndDate = pView.EndDate,
                            TotalSeconds = pView.TotalSeconds
                        };

                        newViewing.MediaPageViewings.Add(pageView);
                    }

                    _mediaRepo.SaveMediaViewing(newViewing, false, true);
                }

                scope.Complete();
            }
        }
    }
}
