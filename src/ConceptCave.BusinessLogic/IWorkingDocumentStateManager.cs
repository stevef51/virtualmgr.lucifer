using System;
using System.Collections.Generic;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.RunTime;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.BusinessLogic.Models;
using Newtonsoft.Json.Linq;

namespace ConceptCave.BusinessLogic
{
    public delegate void InjectContextObjects(IWorkingDocument doc);

    public interface IWorkingDocumentStateManager
    {
        PlayerModel SaveState(IWorkingDocument wdObject, WorkingDocumentDTO wdEntity, JObject answers);
   
        PlayerModel ReverseWorkingDocument(IWorkingDocument wdObject, WorkingDocumentDTO wdEntity);
        PlayerModel AdvanceWorkingDocument(IWorkingDocument wdObject, WorkingDocumentDTO wdEntity, JObject answers = null);
        PlayerModel AdvanceWorkingDocument(IWorkingDocument wdObject, ref WorkingDocumentDTO wdEntity, JObject answers = null);
        PlayerModel ContinueWorkingDocument(IWorkingDocument wdObject, WorkingDocumentDTO wdEntity);

        IList<PlayerModel> PlayDashboards(Guid uid, InjectContextObjects injector = null);

        IDesignDocument DesignDocumentFromString(string xml);
        string DesignDocumentToString(IDesignDocument doc);
        IWorkingDocument WorkingDocumentFromString(string xml);
        string WorkingDocumentToString(IWorkingDocument doc);
        IWorkingDocument WorkingDocumentFromDesignDocument(IDesignDocument designDocument, IDictionary<string, object> args);

        bool AllowSave { get; set; }

        WorkingDocumentStateManager.WorkingDocumentContainer BeginWorkingDocument(int groupId, int resourceId, Guid reviewerId, Guid? revieweeId, Guid? parentId = null, IDictionary<string,object> args = null);
        WorkingDocumentStateManager.WorkingDocumentContainer BeginWorkingDocument(int pubResourceid, Guid reviewerId, Guid? revieweeId, Guid? parentId = null, IDictionary<string, object> args = null);
        WorkingDocumentStateManager.WorkingDocumentContainer BeginWorkingDocument(PublishingGroupResourceDTO resourceent, Guid reviewerId, Guid? revieweeId, Guid? parentId = null, IDictionary<string, object> args = null);

        WorkingDocumentStateManager.WorkingDocumentContainer BeginWorkingDocument(string pubGroupName,
            string resourceName,
            string reviewerName, string revieweeName = null, Guid? parentId = null, IDictionary<string,object> args = null);

        void FinishWorkingDocument(IWorkingDocument wdDocument, ref WorkingDocumentDTO wdEntity);
        void ResolveWorkingDocumentTemplates(IWorkingDocument doc);
        void ResolveWorkingDocumentTemplates(IEnumerable<IPresentable> enumerable);
        WorkingDocumentDTO SaveWorkingDocument(IWorkingDocument wd, WorkingDocumentDTO wdEntity);
        void GuaranteeProgramSourceCode(IDesignDocument program);
		PlayerModel ConstructPlayerModel(IWorkingDocument wdObject, WorkingDocumentDTO documentEntity,
		                               IValidatorResult validatorResult = null);

        bool CancelWorkingDocument(Guid workingDocumentId);
    }
}