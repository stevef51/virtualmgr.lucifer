﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.BusinessLogic.ReportData
{
    public interface IReportingDataHandler
    {
        void FillQuestionDefault(CompletedWorkingDocumentPresentedFactDTO ent, IQuestion question);
        void FillReportingEntity(CompletedWorkingDocumentPresentedFactDTO ent, IPresentedQuestion pQuestion, IWorkingDocument document);
    }
}
