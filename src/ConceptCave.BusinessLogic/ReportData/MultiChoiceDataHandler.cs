﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Checklist.Editor;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.RunTime;
using ConceptCave.Core;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.BusinessLogic.ReportData
{
    public class MultiChoiceDataHandler : IReportingDataHandler
    {

        public void FillQuestionDefault(CompletedWorkingDocumentPresentedFactDTO ent, IQuestion question)
        {
            //The MC question is happy to accept an enumerable of strings as a default answer
            var answerString = ent.CompletedPresentedFactValues.First().Value;
            var split = answerString.Split(',').Select(s => s.Replace("&#44;", ","));
            question.DefaultAnswer = split;
        }

        public void FillReportingEntity(CompletedWorkingDocumentPresentedFactDTO ent, IPresentedQuestion pQuestion, IWorkingDocument document)
        {
            //don't actually have anything to do here.
        }
    }
}
