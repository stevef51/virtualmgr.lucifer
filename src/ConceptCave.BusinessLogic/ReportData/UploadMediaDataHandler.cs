﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.RunTime;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;

namespace ConceptCave.BusinessLogic.ReportData
{
    public class UploadMediaDataHandler : IReportingDataHandler
    {
        private readonly IMediaRepository _mediaRepo;
        private readonly IMediaManager _mediaMgr;

        public UploadMediaDataHandler(IMediaRepository mediaRepo, IMediaManager mediaMgr)
        {
            _mediaRepo = mediaRepo;
            _mediaMgr = mediaMgr;
        }

        private void FillAnswer(CompletedWorkingDocumentPresentedFactDTO ent, IAnswer answer)
        {
            var umAnswer = (IUploadMediaAnswer) answer;
            var mediaAnswerEnts = ent.CompletedPresentedUploadMediaFactValues;

            foreach (var answerEnt in mediaAnswerEnts)
            {
                var media = _mediaRepo.GetById(answerEnt.MediaId, MediaLoadInstruction.None);
                var item = new UploadMediaAnswerItem(_mediaMgr)
                {
                    ContentType = media.Type,
                    Description = media.Description,
                    Filename = media.Filename,
                    MediaId = media.Id,
                    Name = media.Name,
                    UploadDate = media.DateCreated
                };
                umAnswer.Items.Add(item);
            }
        }

        public void FillQuestionDefault(CompletedWorkingDocumentPresentedFactDTO ent, IQuestion question)
        {
            var answer = new UploadMediaAnswer(_mediaMgr);
            FillAnswer(ent, answer);
            question.DefaultAnswer = answer;
        }

        public void FillReportingEntity(CompletedWorkingDocumentPresentedFactDTO ent, IPresentedQuestion pQuestion, IWorkingDocument document)
        {
            //Add media answers to the object graph
            var answer = (IUploadMediaAnswer) pQuestion.GetAnswer();
            foreach (var item in answer.Items)
            {
                ent.CompletedPresentedUploadMediaFactValues.Add(new CompletedPresentedUploadMediaFactValueDTO()
                {
                    __IsNew = true,
                    CompletedWorkingDocumentPresentedFact = ent,
                    CompletedWorkingDocumentPresentedFactId = ent.Id,
                    MediaId = item.MediaId,
                });
            }
        }
    }
}
