﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.BusinessLogic.Questions;
using Newtonsoft.Json.Linq;

namespace ConceptCave.BusinessLogic.ReportData
{
    public class OrderDataHandler : IReportingDataHandler
    {
        public void FillQuestionDefault(CompletedWorkingDocumentPresentedFactDTO ent, IQuestion question)
        {
            // we don't support orders through user context
        }

        public void FillReportingEntity(CompletedWorkingDocumentPresentedFactDTO ent, IPresentedQuestion pQuestion, IWorkingDocument document)
        {
            if(((OrderQuestion)pQuestion.Question).AutoCreateOrderOnFinish == false)
            {
                return;
            }

            OrderAnswer answer = (OrderAnswer)pQuestion.GetAnswer();

            answer.CommitOrders(document.Program, null, null);
        }
    }
}
