﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Checklist;
using ConceptCave.Checklist.Editor;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.RunTime;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.BusinessLogic.ReportData
{
    public class GeoLocationDataHandler : IReportingDataHandler
    {
        private void FillAnswer(CompletedWorkingDocumentPresentedFactDTO ent, IAnswer answer)
        {
            var gAnswer = (GeoLocationAnswer) answer;
            var geoEnt = ent.CompletedPresentedGeoLocationFactValues.SingleOrDefault();
            gAnswer.Coordinates = geoEnt != null ?
                new LatLng(geoEnt.Latitude, geoEnt.Longitude, geoEnt.Accuracy) :
                new LatLng(ent.CompletedPresentedFactValues.Single().Value);
        }


        //Fill question from entity
        public void FillQuestionDefault(CompletedWorkingDocumentPresentedFactDTO ent, IQuestion question)
        {
            var answer = new GeoLocationAnswer();
            FillAnswer(ent, answer);
            question.DefaultAnswer = answer;
        }

        //Break the question out into reporting tables
        public void FillReportingEntity(CompletedWorkingDocumentPresentedFactDTO ent, IPresentedQuestion pQuestion, IWorkingDocument document)
        {
            var answer = (IGeoLocationAnswer)pQuestion.GetAnswer();
            var question = (IGeoLocationQuestion)pQuestion.Question;

            //If there's an error, don't need to capture special geolocation answer
            if (!string.IsNullOrEmpty(answer.Coordinates.Error))
                return;

            var geoEntity = new CompletedPresentedGeoLocationFactValueDTO()
            {
                __IsNew = true,
                Id = CombFactory.NewComb(),
                CompletedWorkingDocumentPresentedFactId = ent.Id,
                CompletedWorkingDocumentPresentedFact = ent,
                Latitude = answer.Coordinates.Latitude,
                Longitude = answer.Coordinates.Longitude,
                Accuracy = answer.Coordinates.Accuracy
            };
            
            //attach it to the graph
            ent.CompletedPresentedGeoLocationFactValues.Add(geoEntity);
        }
    }
}
