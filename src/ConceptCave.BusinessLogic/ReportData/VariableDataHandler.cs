﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Checklist.RunTime;
using ConceptCave.Checklist.Interfaces;

namespace ConceptCave.BusinessLogic.ReportData
{
    public class VariableDataHandler : IReportingDataHandler
    {
        public void FillQuestionDefault(DTO.DTOClasses.CompletedWorkingDocumentPresentedFactDTO ent, Checklist.Interfaces.IQuestion question)
        {
            //The MC question is happy to accept an enumerable of strings as a default answer
            var answerString = ent.CompletedPresentedFactValues.First().Value;
            question.DefaultAnswer = VariableDataAnswer.StringToObject(answerString);
        }

        public void FillReportingEntity(DTO.DTOClasses.CompletedWorkingDocumentPresentedFactDTO ent, Checklist.Interfaces.IPresentedQuestion pQuestion, IWorkingDocument document)
        {
            //don't actually have anything to do here.
        }
    }
}
