﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using ConceptCave.BusinessLogic.Configuration;
using ConceptCave.Checklist;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.Validators;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.BusinessLogic.Context;

namespace ConceptCave.BusinessLogic.ReportData
{
    public class ReportingTableWriter : IReportingTableWriter
    {
        private readonly IRepositorySectionManager _sectionManager;
        private readonly IReportingRepository _reportRepo;
        private readonly IWorkingDocumentRepository _wdRepo;
        private readonly ILabelRepository _labelRepo;
        private readonly IPublishingGroupResourceRepository _pubResourceRepo;
        private readonly IMembershipRepository _memberRepo;
        private readonly IAssetRepository _assetRepo;

        public ReportingTableWriter(IRepositorySectionManager sectionManager, IReportingRepository reportRepo,
            IWorkingDocumentRepository wdRepo, ILabelRepository labelRepo,
            IPublishingGroupResourceRepository pubResoruceRepo, IMembershipRepository memberRepo, IAssetRepository assetRepo)
        {
            _sectionManager = sectionManager;
            _reportRepo = reportRepo;
            _wdRepo = wdRepo;
            _labelRepo = labelRepo;
            _pubResourceRepo = pubResoruceRepo;
            _memberRepo = memberRepo;
            _assetRepo = assetRepo;
        }

        //This guy does a lot of stuff that used to be done in IWorkflowManager
        //basically fills up the reporting DTO we're sitting on with answers based on the
        //provided document
        //He saves his own object graph
        //He does the orchestration, but the IReportingRepository does a lot of the manual copying
        //of fields and such
        //If you pass in a working document entity, doesn't matter if it doesn't exist in the DB
        //it's just used to read out a bunch of fields; make sure it's got them set
        public CompletedWorkingDocumentFactDTO BreakOutDocumentIntoReportingTables(IWorkingDocument document,
            WorkingDocumentDTO wdEntity = null, ContextType contextType = ContextType.None, bool refetch = false, int assetId = -1)
        {
            CompletedWorkingDocumentFactDTO result;

            using (var scope = new TransactionScope())
            {
                IList<LabelDTO> lblEnts;
                //step 1: We need a working document entity
                if (wdEntity == null)
                {
                    wdEntity = _wdRepo.GetById(document.Id,
                        WorkingDocumentLoadInstructions.Users |
                        WorkingDocumentLoadInstructions.Publishing |
                        WorkingDocumentLoadInstructions.Resource |
                        WorkingDocumentLoadInstructions.ResourceLabels);
                    //Also since we can be certain we have the right prefetch (we loaded it), grab the labels
                    //(see step 2b below)
                    lblEnts = wdEntity.PublishingGroupResource.Resource.LabelResources.Select(r => r.Label).ToList();
                }
                else
                {
                    //see if we can get lbl ents from the prefetch, otherwise we need to load a resource
                    if (wdEntity.PublishingGroupResource != null && wdEntity.PublishingGroupResource.Resource != null &&
                        wdEntity.PublishingGroupResource.LabelPublishingGroupResources != null)
                    {
                        lblEnts = wdEntity.PublishingGroupResource.Resource.LabelResources.Select(r => r.Label).ToList();
                    }
                    //nope, load up the resources ourselves
                    else
                    {
                        var resourceId = wdEntity.PublishingGroupResourceId;
                        var resource = _pubResourceRepo.GetById(resourceId,
                            PublishingGroupResourceLoadInstruction.Resource |
                            PublishingGroupResourceLoadInstruction.ResourceLabels);
                        lblEnts = resource.Resource.LabelResources.Select(r => r.Label).ToList();
                    }
                }

                //step 2: go over the document-level dimensions that have no deps
                //step 2a: User dimensions
                if (!wdEntity.RevieweeId.HasValue)
                    wdEntity.RevieweeId = wdEntity.ReviewerId;

                var completedUsers = _reportRepo.AddToUsersDimension(new[] { wdEntity.ReviewerId, wdEntity.RevieweeId.Value });
                var completedReviewer = completedUsers[0];
                var completedReviewee = completedUsers[1];

                //step 2b: labels on working documents
                var completedLabels = _reportRepo.AddToLabelsDimension(lblEnts);

                //step 2c: working document dimension
                var completedWD = _reportRepo.AddToWorkingDocumentDimension(wdEntity);

                //step 3: Make a reporting entity and add these things to it
                var reportingEntity = _reportRepo.WriteNewWorkingDocumentFact(wdEntity, completedReviewer,
                    completedReviewee, completedWD);

                // Fix CompletedWorkingDocumentFact.IsUserContext, ContextType
                reportingEntity.IsUserContext = contextType == ContextType.Membership;
                reportingEntity.ContextType = (int)contextType;

                //We wouldn't want there to be any questions still attached to this fact
                _reportRepo.DeletePresentedFactsForDocument(reportingEntity.WorkingDocumentId);

                reportingEntity.PassedQuestionCount = 0;
                reportingEntity.FailedQuestionCount = 0;
                reportingEntity.NaquestionCount = 0;
                reportingEntity.TotalQuestionCount = 0;

                //step 4: We can resolve M-M relationship dimensions now
                //e.g. labels
                _reportRepo.WriteLabelRelations(reportingEntity, completedLabels);

                var qtypeDimsToAdd = new HashSet<string>();
                var labelDimsToAdd = new HashSet<Guid>();
                var questionEntityMapping = new Dictionary<IPresentedQuestion, CompletedWorkingDocumentPresentedFactDTO>();
                var typeNameMapping = new Dictionary<Type, string>();

                //step 5: enumerate the questions to do the following things....
                var answerableQuestions =
                    document.PresentedAsDepthFirstEnumerable()
                        .OfType<IPresentedQuestion>()
                        .Where(p => p.Question.IsAnswerable);

                int presentedOrder = 0;
                foreach (var pQuestion in answerableQuestions)
                {
                    var question = pQuestion.Question;
                    var answer = pQuestion.GetAnswer();

                    //step 5a: update the pass/fail count
                    reportingEntity.TotalQuestionCount++;
                    if (answer.PassFail.HasValue && answer.PassFail.Value)
                        reportingEntity.PassedQuestionCount++;
                    else if (answer.PassFail.HasValue && !answer.PassFail.Value)
                        reportingEntity.FailedQuestionCount++;
                    else
                        reportingEntity.NaquestionCount++;

                    //5b: update score
                    //NA questions don't count though
                    if (answer.PassFail.HasValue)
                    {
                        reportingEntity.TotalPossibleScore += question.PossibleScore;
                        reportingEntity.TotalScore += answer.Score;
                    }

                    Func<DateTime, DateTime> sanitiseDate = (d) => d == default(DateTime) ? DateTime.UtcNow : d;

                    //5c: make a presented fact and attach it
                    var pfactEntity = new CompletedWorkingDocumentPresentedFactDTO()
                    {
                        __IsNew = true,
                        WorkingDocumentId = reportingEntity.WorkingDocumentId,
                        Id = CombFactory.NewComb(),
                        PresentedId = pQuestion.Id,
                        InternalId = question.Id,
                        ReportingWorkingDocumentId = reportingEntity.ReportingWorkingDocumentId,
                        ReportingUserRevieweeId = reportingEntity.ReportingUserRevieweeId,
                        ReportingUserReviewerId = reportingEntity.ReportingUserReviewerId,
                        Prompt = pQuestion.Prompt,
                        DateStarted = sanitiseDate(pQuestion.UtcDatePresented),
                        DateCompleted = sanitiseDate(answer.UtcDateAnswered),
                        PossibleScore = question.PossibleScore,
                        Score = answer.Score,
                        PassFail = answer.PassFail,
                        NotesText =
                            (answer.Notes != null && string.IsNullOrEmpty(answer.Notes.Text)) ? answer.Notes.Text : null,
                        ParentWorkingDocumentId = reportingEntity.ParentWorkingDocumentId,
                        IsUserContext = contextType == ContextType.Membership,
                        ContextType = (int)contextType,

                        CompletedWorkingDocumentFact = reportingEntity,
                        PresentedOrder = presentedOrder++
                    };
                    reportingEntity.CompletedWorkingDocumentPresentedFacts.Add(pfactEntity);
                    questionEntityMapping.Add(pQuestion, pfactEntity);

                    //5d: prepare for question and label dimensions
                    var qSectionItem = _sectionManager.ItemForObject(question);
                    var qtype = qSectionItem.FriendlyName;
                    qtypeDimsToAdd.Add(qtype);
                    labelDimsToAdd.UnionWith(question.Labels);

                    typeNameMapping[question.GetType()] = qtype;

                    //5e: give them a normal answer value ent
                    var factValueEntity = new CompletedPresentedFactValueDTO()
                    {
                        __IsNew = true,
                        CompletedWorkingDocumentPresentedFact = pfactEntity,
                        CompletedWorkingDocumentPresentedFactId = pfactEntity.Id,
                        Id = CombFactory.NewComb(),
                        Value = answer.AnswerAsString
                    };
                    pfactEntity.CompletedPresentedFactValues.Add(factValueEntity);

                    //5f: maybe the question type wants to do something special
                    if (qSectionItem.HasReportingDataHandler)
                    {
                        var handler = qSectionItem.CreateReportingDataHandler();
                        handler.FillReportingEntity(pfactEntity, pQuestion, document);
                    }
                }

                //step 6: We need to prepare the dimensions that presentedfacts depend on
                var labelsToAdd = _labelRepo.GetByIds(labelDimsToAdd.ToList());
                var completedPresentedLabels = _reportRepo.AddToLabelsDimension(labelsToAdd);
                var questionTypeEnts = _reportRepo.AddToTypeDimension(qtypeDimsToAdd.ToList());
                var questionTypeEntDict = questionTypeEnts.ToDictionary(k => k.Type, e => e);

                //step 7: We need to do another pass over presented questions to link these
                //possibly newly created dimensions up to presented fact enitties
                foreach (var kvp in questionEntityMapping)
                {
                    var question = kvp.Key.Question;
                    var entity = kvp.Value;

                    //7a: q type
                    var qtypename = typeNameMapping[question.GetType()];
                    var qtypedim = questionTypeEntDict[qtypename];
                    entity.ReportingPresentedTypeId = qtypedim.Id;
                    entity.CompletedPresentedTypeDimension = qtypedim;
                }

                //step 8: Now just save the unsaved parts of our object graph
                result = _reportRepo.Save(reportingEntity, refetch, true);

                //Step 8a: join labels to presented facts
                foreach (var kvp in questionEntityMapping)
                {
                    var question = kvp.Key.Question;
                    var entity = result.CompletedWorkingDocumentPresentedFacts.Single(e => e.Id == kvp.Value.Id); //refresh from refetched one

                    _reportRepo.WriteLabelRelations(entity,
                        completedPresentedLabels.Where(l => question.Labels.Contains(l.Id)).ToList());
                }


                //step 9: We might need to write back the status of the working document
                document.Program.Set<Guid>("ChecklistReportingId", reportingEntity.WorkingDocumentId);

                //step 10: Update user context if nescessary
                if (contextType == ContextType.Membership)
                    _memberRepo.CreateOrUpdateUserCtxEntry(completedReviewer.Id, wdEntity.PublishingGroupResourceId,
                        result.WorkingDocumentId);
                else if (contextType == ContextType.Asset)
                {
                    if (assetId == -1)
                    {
                        throw new ArgumentException("AssetId must map to an asset in the system");
                    }

                    _assetRepo.CreateOrUpdateAssetCtxEntry(assetId, wdEntity.PublishingGroupResourceId,
                        result.WorkingDocumentId);
                }

                scope.Complete();
            }

            return result;
        }

        //If the document entity is passed into BreakOutDocument itself by caller,
        //they might want to fill in holes in the document from the database
        //used in the user context getter on a non-persisted WD entity
        public void PrepareWorkingDocument(WorkingDocumentDTO wdEntity)
        {
            if (wdEntity.PublishingGroupResource == null)
            {
                wdEntity.PublishingGroupResource = _pubResourceRepo.GetById(wdEntity.PublishingGroupResourceId, PublishingGroupResourceLoadInstruction.Resource);
            }
        }
    }
}
