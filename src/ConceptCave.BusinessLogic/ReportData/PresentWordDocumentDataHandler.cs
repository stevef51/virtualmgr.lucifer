﻿using ConceptCave.Checklist.Editor;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.RunTime;
using ConceptCave.RepositoryInterfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.BusinessLogic.ReportData
{
    public class PresentWordDocumentDataHandler : IReportingDataHandler
    {
        IMediaRepository _mediaRepo;

        public PresentWordDocumentDataHandler(IMediaRepository mediaRepo)
        {
            _mediaRepo = mediaRepo;
        }

        public void FillQuestionDefault(DTO.DTOClasses.CompletedWorkingDocumentPresentedFactDTO ent, Checklist.Interfaces.IQuestion question)
        {
            // this is called when used in user context. We aren't going to support that at the moment (if ever)
        }

        public void FillReportingEntity(DTO.DTOClasses.CompletedWorkingDocumentPresentedFactDTO ent, Checklist.Interfaces.IPresentedQuestion pQuestion, IWorkingDocument document)
        {
            var answer = (PresentWordDocumentAnswer)pQuestion.GetAnswer();
            var question = (PresentWordDocumentQuestion)pQuestion.Question;

            var view = _mediaRepo.GetMediaViewingByWorkingId(ent.WorkingDocumentId, RepositoryInterfaces.Enums.MediaViewingLoadInstruction.None);
            
            if(view == null)
            {
                // if there is no view get out, see EVS-1249
                return;
            }

            if(view.Accepted == false)
            {
                view.Accepted = answer.Accepted;
            }

            if(view.AcceptionNotes == null)
            {
                view.AcceptionNotes = answer.AcceptionNotes;
            }

            _mediaRepo.SaveMediaViewing(view, false, false);
        }
    }
}
