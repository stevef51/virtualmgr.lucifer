﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Checklist;
using ConceptCave.Checklist.Editor;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.RunTime;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.BusinessLogic.ReportData
{
    public abstract class ExtraValueDataHandler<TAnswer> : IReportingDataHandler where TAnswer : IAnswerExtraValues, new()
    {
        //Fill question from entity
        public void FillQuestionDefault(CompletedWorkingDocumentPresentedFactDTO ent, IQuestion question)
        {
        }

        //Break the question out into reporting tables
        public void FillReportingEntity(CompletedWorkingDocumentPresentedFactDTO ent, IPresentedQuestion pQuestion, IWorkingDocument document)
        {
            var answer = (IAnswerExtraValues)pQuestion.GetAnswer();

            //If there's an error, don't need to capture special geolocation answer
            if (!answer.ExtraValues.Any())
                return;

            foreach (var ev in answer.ExtraValues)
            {
                var extraEntity = new CompletedPresentedFactExtraValueDTO()
                {
                    __IsNew = true,
                    Id = CombFactory.NewComb(),
                    CompletedWorkingDocumentPresentedFactId = ent.Id,
                    CompletedWorkingDocumentPresentedFact = ent,
                    Key = ev.Key,
                    Value = ev.Value?.ToString()
                };
                ent.CompletedPresentedFactExtraValues.Add(extraEntity);
            }
            //attach it to the graph
        }
    }
}
