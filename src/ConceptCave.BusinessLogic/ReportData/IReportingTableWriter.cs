﻿using ConceptCave.BusinessLogic.Context;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.BusinessLogic.ReportData
{
    public interface IReportingTableWriter {
        CompletedWorkingDocumentFactDTO BreakOutDocumentIntoReportingTables(IWorkingDocument document,
            WorkingDocumentDTO wdEntity = null, ContextType contextType = ContextType.None, bool refetch = false, int assetId = -1);

        void PrepareWorkingDocument(WorkingDocumentDTO wdEntity);
    }
}