﻿using ConceptCave.BusinessLogic.Models;
using ConceptCave.Data.EntityClasses;
using ConceptCave.Data.HelperClasses;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.SimpleExtensions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.BusinessLogic.Asset
{
    public class AssetTrackerExecutionHandler : IExecutionMethodHandler
    {
        private readonly IAssetTrackerRepository _assetTrackerRepo;
        private readonly ITabletRepository _tabletRepo;
        private readonly IGpsLocationRepository _gpsLocationRepo;
        private readonly IFacilityStructureRepository _facilityStructureRepo;
        private readonly IGlobalSettingsRepository _globalSettingsRepo;
        private readonly IMembershipRepository _memberRepo;

        public class FMARecordsParameters
        {
            [JsonProperty("rid")]
            public Guid RequestId { get; set; }
            [JsonProperty("tuuid")]
            public string TabletUUID { get; set; }
            [JsonProperty("fma")]
            public List<FMARecord> FMARecords { get; set; }
            [JsonProperty("tim")]
            public DateTime Timestamp { get; set; }
            public class FMARecord
            {
                [JsonProperty("br")]
                public BeaconRecord BeaconRecord { get; set; }
                [JsonProperty("bsb")]
                public BestStaticBeaconRecord BestStaticBeacon { get; set; }
            }

            public class BestStaticBeaconRecord
            {
                [JsonProperty("bid")]
                public string BeaconId { get; set; }
                [JsonProperty("rng")]
                public int Range { get; set; }
                [JsonProperty("rutc")]
                public DateTime RangeUtc { get; set; }
            }

            public class BeaconRecord
            {
                [JsonProperty("bid")]
                public string BeaconId { get; set; }
                [JsonProperty("rng")]
                public int Range { get; set; }
                [JsonProperty("rutc")]
                public DateTime RangeUtc { get; set; }
                [JsonProperty("frutc")]
                public DateTime FirstRangeUtc { get; set; }
                [JsonProperty("bt")]
                public BeaconType? BeaconType { get; set; }
                [JsonProperty("bp")]
                public int? BatteryPercent { get; set; }
            }
        }

        public class FMARecordsResponse
        {
            public class BeaconRecord
            {
                [JsonProperty("bid")]
                public string BeaconId { get; set; }
                [JsonProperty("bt")]
                public BeaconType BeaconType { get; set; }
            }
            [JsonProperty("b")]
            public List<BeaconRecord> Beacons { get; set; }
        }

        public AssetTrackerExecutionHandler(
            IAssetTrackerRepository assetTrackerRepo,
            ITabletRepository tabletRepo,
            IGpsLocationRepository gpsLocationRepo,
            IFacilityStructureRepository facilityStructureRepo,
            IGlobalSettingsRepository globalSettingsRepo,
            IMembershipRepository memberRepo)
        {
            _assetTrackerRepo = assetTrackerRepo;
            _tabletRepo = tabletRepo;
            _gpsLocationRepo = gpsLocationRepo;
            _facilityStructureRepo = facilityStructureRepo;
            _globalSettingsRepo = globalSettingsRepo;
            _memberRepo = memberRepo;
        }

        public string Name
        {
            get { return "AssetTracker"; }
        }

        public Newtonsoft.Json.Linq.JToken Execute(string method, Newtonsoft.Json.Linq.JToken parameters, IExecutionMethodHandlerContext context)
        {
            switch (method)
            {
                case "GetServiceOptions":
                    return ExecuteGetServiceOptions(parameters, context);

                case "FMARecords":
                    return JObject.FromObject(FMARecords(parameters.ToObject<FMARecordsParameters>()));
            }
            throw new InvalidOperationException(string.Format("{0} does not exist as a method on the AssetTrackerExecutionHandler", method));
        }

        protected FMARecordsResponse FMARecords(FMARecordsParameters parameters)
        {
            if (parameters.FMARecords != null)
            {
                List<FMARecord> fmaRecords = new List<FMARecord>();
                foreach (var fmaRecord in parameters.FMARecords)
                {
                    if (fmaRecord.BeaconRecord != null)
                    {
                        fmaRecords.Add(new FMARecord()
                        {
                            RecordType = FMARecordType.Beacon,
                            TabletUUID = parameters.TabletUUID,
                            TabletUtc = parameters.Timestamp,
                            BeaconId = fmaRecord.BeaconRecord.BeaconId,
                            Range = fmaRecord.BeaconRecord.Range,
                            RangeUtc = fmaRecord.BeaconRecord.RangeUtc,
                            FirstRangeUtc = fmaRecord.BeaconRecord.FirstRangeUtc,
                            BatteryPercent = fmaRecord.BeaconRecord.BatteryPercent
                        });
                    }
                    else if (fmaRecord.BestStaticBeacon != null)
                    {
                        fmaRecords.Add(new FMARecord()
                        {
                            RecordType = FMARecordType.BestStaticBeacon,
                            TabletUUID = parameters.TabletUUID,
                            TabletUtc = parameters.Timestamp,
                            BeaconId = fmaRecord.BestStaticBeacon.BeaconId,
                            Range = fmaRecord.BestStaticBeacon.Range,
                            RangeUtc = fmaRecord.BestStaticBeacon.RangeUtc
                        });
                    }
                }
                if (fmaRecords.Any())
                {
                    var beaconTypes = new Dictionary<string, BeaconType>();
                    _assetTrackerRepo.WriteFMARecords(fmaRecords, beaconTypes);

                    // Return list of Beacons that the client does not know the correct BeaconType (Static or Asset) of
                    // Client may send "null" or BeaconType.Unknown
                    return new FMARecordsResponse()
                    {
                        Beacons = (from b in parameters.FMARecords
                                   where b.BeaconRecord != null
                                   from bt in beaconTypes
                                   where b.BeaconRecord.BeaconId == bt.Key && bt.Value != b.BeaconRecord.BeaconType
                                   select new FMARecordsResponse.BeaconRecord()
                                   {
                                       BeaconId = bt.Key,
                                       BeaconType = bt.Value
                                   }).ToList()
                    };
                }
            }
            return new FMARecordsResponse();
        }

        public class GetServiceOptionsResult
        {
            public class GeneralOptions
            {
                public int averageCount { get; set; }
                public int beaconExpireSeconds { get; set; }
            }

            public class iOSOptions
            {
                public List<string> serviceUUIDs { get; set; }
            }

            public GeneralOptions general { get; set; }
            public iOSOptions iOS { get; set; }

            public bool enableBLEScanning { get; set; }
        }

        private JObject ExecuteGetServiceOptions(JToken jParameters, IExecutionMethodHandlerContext context)
        {
            //            var VirtualManager2016_09_UUID = "56697274-7561-6c4d-6772-323031363039";
            //            var VM_UUID = "766D";
            var Eddystone_UUID = "FEAA";

            var result = new GetServiceOptionsResult()
            {
                enableBLEScanning = true,
                general = new GetServiceOptionsResult.GeneralOptions()
                {
                    averageCount = 10,
                    beaconExpireSeconds = 20
                },
                iOS = new GetServiceOptionsResult.iOSOptions()
                {
                    serviceUUIDs = new List<string>(new[] { Eddystone_UUID })
                }
            };
            return JObject.FromObject(result);
        }
    }
}
