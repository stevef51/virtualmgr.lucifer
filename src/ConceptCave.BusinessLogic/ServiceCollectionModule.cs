﻿using System;
using ConceptCave.BusinessLogic.Context;
using ConceptCave.BusinessLogic.Invoices;
using ConceptCave.BusinessLogic.ReportData;
using ConceptCave.BusinessLogic.ScheduleManagement;
using ConceptCave.BusinessLogic.Workloading;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.RepositoryInterfaces.ScheduleManagement;
using ConceptCave.RepositoryInterfaces.Workloading;
using Microsoft.Extensions.DependencyInjection;

namespace ConceptCave.BusinessLogic
{
    public static class ServiceCollectionModule
    {
        public static IServiceCollection AddConceptCaveBusinessLogic(this IServiceCollection services)
        {
            services.AddSingleton<IDateTimeProvider, RealtimeDateTimeProvider>();
            services.AddTransient<IWorkingDocumentStateManager, WorkingDocumentStateManager>();
            services.AddTransient<IScheduleManagement, ScheduleManagementBase>();
            services.AddTransient<EndOfDayManagement>();
            services.AddTransient<ScheduleApprovalManagement>();
            services.AddTransient<IMediaManager, MediaManager>();

            services.AddScoped<IContextManagerFactory, ContextManagerFactory>();
            services.AddScoped<IReportingTableWriter, ReportingTableWriter>();

            services.AddTransient<IWorkloadingDurationCalculator, WorkloadingDurationCalculator>();
            services.AddTransient<IWorkloadingActivityFactory, WorkloadingActivityFactory>();
            services.AddScoped<IWorkingDocumentStateManager, WorkingDocumentStateManager>();
            services.AddScoped<RenderConverter>();
            services.AddScoped<AnswerConverter>();

            services.AddTransient<InvoiceManager>();

            return services;
        }
    }
}
