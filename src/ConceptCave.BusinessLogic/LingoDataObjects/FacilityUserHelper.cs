﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using ConceptCave.BusinessLogic.Context;
using ConceptCave.Checklist.Editor;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.Lingo.ObjectProviders;
using ConceptCave.Core;
using ConceptCave.Core.Coding;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using VirtualMgr.Common;

namespace ConceptCave.BusinessLogic.LingoDataObjects
{
    public class FacilityUserHelper : Node, IObjectGetter
    {
        public Guid UserId { get; set; }
        protected UserDataDTO _user;
        protected FacilityUserTypeHelper _type;
        protected Dictionary<string, ContextWorkingDocumentObjectGetter> contextGettersCache;

        protected List<string> _userContextNames;
        private readonly IMembershipRepository _membershipRepo;
        private readonly ILabelRepository _lblRepo;
        private readonly IContextManagerFactory _ctxMgrFac;

        //This constructor is what we want people to actually use
        public FacilityUserHelper(IMembershipRepository membershipRepo, ILabelRepository lblRepo,
            IContextManagerFactory ctxMgrFac, Guid uid) : this(membershipRepo, lblRepo, ctxMgrFac)
        {
            UserId = uid;
        }

        public FacilityUserHelper(IMembershipRepository membershipRepo, ILabelRepository lblRepo,
            IContextManagerFactory ctxMgrFac, UserDataDTO u)
            : this(membershipRepo, lblRepo, ctxMgrFac)
        {
            UserId = u.UserId;
            _user = u;
        }

        //But this is needed by the xmlcoder to actually be able to instantiate the object
        //(there's no binding for Guid, obviously)
        public FacilityUserHelper(IMembershipRepository membershipRepo, ILabelRepository lblRepo,
            IContextManagerFactory ctxMgrFac)
        {
            ContextData = new Dictionary<string, object>();
            contextGettersCache = new Dictionary<string, ContextWorkingDocumentObjectGetter>();
            _userContextNames = new List<string>();
            _membershipRepo = membershipRepo;
            _lblRepo = lblRepo;
            _ctxMgrFac = ctxMgrFac;
        }

        public UserDataDTO User
        {
            get
            {
                if (_user == null)
                {
                    _user = _membershipRepo.GetById(UserId, MembershipLoadInstructions.MembershipType |
                                                            MembershipLoadInstructions.ContextData |
                                                            MembershipLoadInstructions.ContextResources |
                                                            MembershipLoadInstructions.MembershipLabel |
                                                            MembershipLoadInstructions.Label |
                                                            MembershipLoadInstructions.AspNetMembership
                                                            );
                }

                return _user;
            }
            set
            {
                _user = value;
                UserId = _user.UserId;
            }
        }

        public bool IsUserValid
        {
            get
            {
                return User != null;
            }
        }

        public FacilityUserTypeHelper UserType
        {
            get
            {
                if (_type == null)
                {
                    var userType = _membershipRepo.GetUserTypeById(User.UserTypeId, MembershipTypeLoadInstructions.None);
                    _type = new FacilityUserTypeHelper()
                    {
                        UserTypeId = userType.Id,
                        Name = userType.Name
                    };
                }

                return _type;
            }
        }

        public object CompanyId
        {
            get
            {
                return User.CompanyId;
            }
            set
            {
                User.CompanyId = (Guid?)value;
            }
        }

        public string CompanyName
        {
            get
            {
                if (User.Company == null)
                {
                    return "";
                }

                return User.Company.Name;
            }
        }

        public string Email
        {
            get
            {
                return User != null ? User.AspNetUser.Email : null;
            }
            set
            {
                User.AspNetUser.Email = value;
            }
        }

        public string Username
        {
            get
            {
                return User != null ? User.AspNetUser.UserName : null;
            }
        }

        public DateTime CurrentTime
        {
            get
            {
                return TimeZoneInfo.ConvertTimeFromUtc(DateTime.SpecifyKind(DateTime.UtcNow, DateTimeKind.Utc),
                    TimeZoneInfoResolver.ResolveWindows(User.TimeZone));
            }
        }

        public DateTime ConvertToLocalTime(IContextContainer context, DateTime sourceTime)
        {
            return TimeZoneInfo.ConvertTimeFromUtc(sourceTime, TimeZoneInfoResolver.ResolveWindows(User.TimeZone));
        }

        public DateTime ConvertToUtcTime(IContextContainer context, DateTime sourceTime)
        {
            return TimeZoneInfo.ConvertTimeToUtc(sourceTime, TimeZoneInfoResolver.ResolveWindows(User.TimeZone));
        }

        public DateTime ConvertToLocalTime(DateTime sourceTime)
        {
            return TimeZoneInfo.ConvertTimeFromUtc(sourceTime, TimeZoneInfoResolver.ResolveWindows(User.TimeZone));
        }

        public DateTime ConvertToUtcTime(DateTime sourceTime)
        {
            return TimeZoneInfo.ConvertTimeToUtc(sourceTime, TimeZoneInfoResolver.ResolveWindows(User.TimeZone));
        }

        public DateTime? ConvertToLocalTime(DateTime? sourceTime)
        {
            if (!sourceTime.HasValue)
                return null;
            return TimeZoneInfo.ConvertTimeFromUtc(sourceTime.Value, TimeZoneInfoResolver.ResolveWindows(User.TimeZone));
        }

        public DateTime? ConvertToUtcTime(DateTime? sourceTime)
        {
            if (!sourceTime.HasValue)
                return null;
            return TimeZoneInfo.ConvertTimeToUtc(sourceTime.Value, TimeZoneInfoResolver.ResolveWindows(User.TimeZone));
        }

        /// <summary>
        /// Function was originally called this, ConvertToUtcTime added to be consistent with other methods. This method
        /// has been kept to support scripts using this method name (so we don't have to run through legacy scripts changing them)
        /// </summary>
        public DateTime ConvertToUtc(IContextContainer context, DateTime sourceTime)
        {
            return ConvertToUtcTime(context, sourceTime);
        }

        public bool HasExpiryDate
        {
            get
            {
                return User.ExpiryDate.HasValue;
            }
        }

        public bool HasCommencementDate
        {
            get
            {
                return User.CommencementDate.HasValue;
            }
        }

        public DateTime ExpiryDate
        {
            get
            {
                if (User.ExpiryDate.HasValue == false)
                {
                    return DateTime.MinValue;
                }

                return User.ExpiryDate.Value;
            }
            set
            {
                User.ExpiryDate = value;
            }
        }

        public DateTime CommencementDate
        {
            get
            {
                if (User.CommencementDate.HasValue == false)
                {
                    return DateTime.MinValue;
                }

                return User.CommencementDate.Value;
            }
            set
            {
                User.CommencementDate = value;
            }
        }

        public string Mobile
        {
            get
            {
                return User.Mobile;
            }
            set
            {
                User.Mobile = value;
            }
        }

        public object Coordinates
        {
            get
            {
                if (User.Latitude.HasValue == false || User.Longitude.HasValue == false)
                {
                    return null;
                }

                return new LatLng(User.Latitude.Value, User.Longitude.Value, 0);
            }
            set
            {
                if (value is IGeoLocationAnswer)
                {
                    User.Longitude = ((IGeoLocationAnswer)value).Coordinates.Longitude;
                    User.Latitude = ((IGeoLocationAnswer)value).Coordinates.Latitude;
                }
                else if (value is MembershipGeoCodeResult)
                {
                    User.Longitude = ((MembershipGeoCodeResult)value).Longitude;
                    User.Latitude = ((MembershipGeoCodeResult)value).Latitude;
                }
                else if (value is ILatLng)
                {
                    User.Longitude = ((ILatLng)value).Longitude;
                    User.Latitude = ((ILatLng)value).Latitude;
                }
            }
        }

        public void AddRole(IContextContainer context, string roleName)
        {
            //No harm comes of executing this even if the user is already in the role
            _membershipRepo.AddUserInRole(User.UserId, roleName);
        }

        /* TODO : This needs adding ?
        public void RemoveRole(IContextContainer context, string roleName)
        {
        }
         */

        public void AddLabel(IContextContainer context, object label)
        {
            if (label is string)
            {
                var l = (string)label;

                var result = _lblRepo.GetByName(l);

                if (result == null)
                {
                    return;
                }

                _membershipRepo.AddLabels(User.UserId, new[] { result.Id });
            }
        }

        public void RemoveLabel(IContextContainer context, object label)
        {
            if (label is string)
            {
                var l = (string)label;

                var result = _lblRepo.GetByName(l);

                if (result == null)
                {
                    return;
                }

                _membershipRepo.RemoveFromLabels(new Guid[] { result.Id }, User.UserId);
            }
        }

        private Func<LabelUserDTO, bool> CreateLabelFilter(object filterObj)
        {
            IEnumerable<object> filter = filterObj as IEnumerable<object>;
            if (filter == null && filterObj != null)
                filter = new object[] { filterObj };

            return (lu) =>
            {
                if (filter == null)
                    return true;
                return filter.FirstOrDefault(l =>
                {
                    Guid g = Guid.Empty;
                    if (l is string)
                    {
                        if (!Guid.TryParse((string)l, out g))
                        {
                            return string.Compare(lu.Label.Name, (string)l, true) == 0;
                        }
                    }
                    if (l is Guid)
                    {
                        g = (Guid)l;
                    }
                    if (g != Guid.Empty)
                    {
                        return lu.LabelId == g;
                    }
                    return false;
                }) != null;
            };
        }

        public ListObjectProvider GetLabelIds() => GetLabelIds(null);            

        public ListObjectProvider GetLabelIds(object filter = null)
        {
            var labelFilter = CreateLabelFilter(filter);
            return ListObjectProvider.From((from lu in User.LabelUsers where labelFilter(lu) select lu.LabelId).Cast<object>());
        }

        public ListObjectProvider GetLabelNames() => GetLabelNames(null);

        public ListObjectProvider GetLabelNames(object filter = null)
        {
            var labelFilter = CreateLabelFilter(filter);
            return ListObjectProvider.From(from lu in User.LabelUsers where labelFilter(lu) select lu.Label.Name);
        }

        public ListObjectProvider GetLabels() => GetLabels(null);

        public ListObjectProvider GetLabels(object filter = null)
        {
            var labelFilter = CreateLabelFilter(filter);
            var result = new List<DictionaryObjectProvider>();
            foreach(var lu in User.LabelUsers)
            {
                if (!labelFilter(lu))
                {
                    continue;
                }
                var l = new Dictionary<string, object>();
                l["name"] = lu.Label.Name;
                l["id"] = lu.LabelId;
                result.Add(new DictionaryObjectProvider(l));
            }
            return ListObjectProvider.From(result);
        }


        public bool HasLabel(IContextContainer context, object label)
        {
            if (label is string)
            {
                return
                    User.LabelUsers.Any(
                        lu => string.Equals(lu.Label.Name, (string)label, StringComparison.InvariantCultureIgnoreCase));
            }
            else if (label is Guid)
            {
                return User.LabelUsers.Any(lu => lu.LabelId == (Guid)label);
            }
            else return false;
        }

        protected Dictionary<string, object> ContextData { get; set; }

        public new Guid Id
        {
            get
            {
                return User.UserId;
            }
        }

        public string Name
        {
            get
            {
                return User.Name;
            }
        }

        public void Save()
        {
            if (_user == null)
            {
                // can't be any changes if the user hasn't been loaded
                return;
            }

            _membershipRepo.Save(_user, true, true);
        }

        public List<string> UserContextNames
        {
            get
            {
                if (_userContextNames == null)
                {
                    _userContextNames = new List<string>();

                    var result = _membershipRepo.GetUserContextResourcesForType(_user.UserTypeId,
                        PublishingGroupResourceLoadInstruction.None);

                    _userContextNames = (from r in result select r.Name).ToList();
                }

                return _userContextNames;
            }
        }

        /// <summary>
        /// Allows Lingo to get at the context data for a user through a somevar.GetData("checklist name") syntax.
        /// We provide both the GetData and the GetObject of the IObjectGetter so as to allow retrieval of conext
        /// data where the checklist name has spaces in it.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public object GetData(IContextContainer context, string name)
        {
            object value = null;

            if (GetObject(context, name, out value) == false)
            {
                return null;
            }

            return value;
        }

        #region IObjectGetter Members
        // allows Lingo to get at the context stuff using dot notation
        public bool GetObject(IContextContainer context, string name, out object result)
        {
            ContextWorkingDocumentObjectGetter getter;

            // force into a known case as name comes direct from what is written in Lingo, so site and Site are different
            // from our cache point of view
            string upperName = name.ToUpper();

            if (!contextGettersCache.TryGetValue(upperName, out getter))
            {
                getter = _ctxMgrFac.GetInstance(User, ContextType.Membership, User.UserId).LoadContextDocument(name, context);
                contextGettersCache.Add(upperName, getter);
            }
            result = getter;
            return result != null;
        }

        #endregion

        public override void Encode(IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("UserId", UserId);
        }

        public override void Decode(IDecoder decoder)
        {
            base.Decode(decoder);

            UserId = decoder.Decode<Guid>("UserId");
        }
    }
}
