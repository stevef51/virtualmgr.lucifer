﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Core;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Repositories;

namespace ConceptCave.BusinessLogic.LingoDataObjects
{
    public class FacilityLabelHelper : Node
    {
        protected Guid LabelId { get; set; }
        protected LabelDTO _label;
        private readonly ILabelRepository _lblRepo;

        public FacilityLabelHelper(ILabelRepository lblRepo)
        {
            _lblRepo = lblRepo;
        }

        protected internal LabelDTO Label
        {
            get
            {
                if (_label == null)
                {
                    _label = _lblRepo.GetById(LabelId);
                }
                return _label;
            }
            set
            {
                _label = value;
                LabelId = _label.Id;
            }
        }

        public void Save()
        {
            if (_label == null) return;
            _lblRepo.Save(_label, true);
        }

        public new Guid Id
        {
            get
            {
                return _label.Id;
            }
        }

        public string Name
        {
            get
            {
                return _label.Name;
            }
            set
            {
                _label.Name = value;
            }
        }
    }
}
