﻿using ConceptCave.BusinessLogic.Context;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;
using ConceptCave.Core.Coding;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Repositories;
using System;
using System.Collections.Generic;

namespace ConceptCave.BusinessLogic.LingoDataObjects
{
    public class FacilityAssetHelper: Node, IObjectGetter
    {
        protected int AssetId { get; set; }
        protected AssetDTO _asset;
        public IDictionary<string, object> Dictionary { get; set; }
        private readonly IAssetRepository _assetRepo;
        protected Dictionary<string, ContextWorkingDocumentObjectGetter> contextGettersCache;
        private readonly IContextManagerFactory _ctxMgrFac;

        public FacilityAssetHelper(IAssetRepository assetRepo,
           IContextManagerFactory ctxMgrFac, int assetid) : this(assetRepo, ctxMgrFac)
        {
            AssetId = assetid;
        }

        public FacilityAssetHelper(IAssetRepository assetRepo,
           IContextManagerFactory ctxMgrFac, AssetDTO a) : this(assetRepo, ctxMgrFac)
        {
            AssetId = a.Id;
            _asset   = a;
        }

        public FacilityAssetHelper(IAssetRepository assetRepo, 
           IContextManagerFactory ctxMgrFac)
        {
            contextGettersCache = new Dictionary<string, ContextWorkingDocumentObjectGetter>();

            _assetRepo = assetRepo;
            
            _ctxMgrFac = ctxMgrFac;
        }

        public AssetDTO Asset
        {
            get
            {
                if (_asset == null)
                {
                    _asset = _assetRepo.GetById(AssetId);
                }

                return _asset;
            }
            set
            {
                _asset = value;
                AssetId = _asset.Id;
            }
        }

        public void Save(IContextContainer context)
        {
            if (_asset == null)
            {
                // can't be any changes if the asset hasn't been loaded
                return;
            }

            _assetRepo.SaveAsset(_asset, true, true);
        }

        /// <summary>
        /// Allows Lingo to get at the context data for a user through a somevar.GetData("checklist name") syntax.
        /// We provide both the GetData and the GetObject of the IObjectGetter so as to allow retrieval of conext
        /// data where the checklist name has spaces in it.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public object GetData(IContextContainer context, string name)
        {
            object value = null;

            if (GetObject(context, name, out value) == false)
            {
                return null;
            }

            return value;
        }

        #region IObjectGetter Members
        // allows Lingo to get at the context stuff using dot notation
        public bool GetObject(IContextContainer context, string name, out object result)
        {
            ContextWorkingDocumentObjectGetter getter;

            // force into a known case as name comes direct from what is written in Lingo, so site and Site are different
            // from our cache point of view
            string upperName = name.ToUpper();

            if (!contextGettersCache.TryGetValue(upperName, out getter))
            {
                var users = context.Get<CurrentContextFacilityCurrentUsers>();

                getter = _ctxMgrFac.GetInstance(Asset.Id, ContextType.Asset, users.ReviewerId).LoadContextDocument(name, context);
                contextGettersCache.Add(upperName, getter);
            }

            result = getter;
            return result != null;
        }

        #endregion
        
        public override void Encode(IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("AssetId", AssetId);
        }

        public override void Decode(IDecoder decoder)
        {
            base.Decode(decoder);

            AssetId = decoder.Decode<int>("AssetId");
        }


    }
}
