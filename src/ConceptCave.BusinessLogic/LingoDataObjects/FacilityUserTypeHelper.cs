﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Core;
using ConceptCave.Core.Coding;

namespace ConceptCave.BusinessLogic.LingoDataObjects
{
    public class FacilityUserTypeHelper : Node
    {
        public int UserTypeId { get; set; }
        public string Name { get; set; }

        public override void Encode(IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("UserTypeId", UserTypeId);
            encoder.Encode("Name", Name);
        }

        public override void Decode(IDecoder decoder)
        {
            base.Decode(decoder);

            UserTypeId = decoder.Decode<int>("UserTypeId");
            Name = decoder.Decode<string>("Name");
        }
    }
}
