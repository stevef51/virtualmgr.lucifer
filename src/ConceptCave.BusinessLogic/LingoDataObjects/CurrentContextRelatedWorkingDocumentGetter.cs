﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.BusinessLogic.Context;
using ConceptCave.Checklist.Facilities;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;
using ConceptCave.Core.Coding;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using System.Diagnostics;

namespace ConceptCave.BusinessLogic.LingoDataObjects
{
    public class CurrentContextRelatedWorkingDocumentGetter : WorkingDocumentObjectGetter
    {
        protected Guid WorkingDocumentEntityId { get; set; }
        protected WorkingDocumentDTO Entity { get; set; }
        //protected IWorkingDocument WorkingDocument { get; set; }

        protected FacilityUserHelper _reviewer;
        protected FacilityUserHelper _reviewee;
        private IWorkingDocumentRepository _wdRepo;
        private IMembershipRepository _membershipRepo;
        private ILabelRepository _lblRepo;
        private IContextManagerFactory _ctxFac;

        public CurrentContextRelatedWorkingDocumentGetter(IWorkingDocumentRepository wdRepo,
            IMembershipRepository membershipRepo, ILabelRepository lblRepo, IContextManagerFactory ctxFac) : base()
        {
            _wdRepo = wdRepo;
            _membershipRepo = membershipRepo;
            _lblRepo = lblRepo;
            _ctxFac = ctxFac;
        }

        public CurrentContextRelatedWorkingDocumentGetter(Guid workingDocumentEntityId,
            IWorkingDocumentRepository wdRepo, IMembershipRepository membershipRepo, ILabelRepository lblRepo,
            IContextManagerFactory ctxFac)
            : this(wdRepo, membershipRepo, lblRepo, ctxFac)
        {
            WorkingDocumentEntityId = workingDocumentEntityId;
        }

        public override void Encode(IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("WorkingDocumentEntityId", WorkingDocumentEntityId);
        }

        public override void Decode(IDecoder decoder)
        {
            base.Decode(decoder);

            WorkingDocumentEntityId = decoder.Decode<Guid>("WorkingDocumentEntityId");
        }

        public override IWorkingDocument GetWorkingDocument(IContextContainer container)
        {
            if (WorkingDocument != null)
            {
                return WorkingDocument;
            }

            Entity = _wdRepo.GetById(WorkingDocumentEntityId, WorkingDocumentLoadInstructions.Data);

            WorkingDocument =
                ConceptCave.Checklist.RunTime.WorkingDocument.CreateFromString(Entity.WorkingDocumentData.Data);

            return WorkingDocument;
        }

        public FacilityUserHelper Reviewer
        {
            get
            {
                if (_reviewer == null)
                {
                    GetWorkingDocument(null);
                    _reviewer = new FacilityUserHelper(_membershipRepo, _lblRepo, _ctxFac, Entity.ReviewerId);
                }

                return _reviewer;
            }
        }

        public FacilityUserHelper Reviewee
        {
            get
            {
                if (_reviewee == null)
                {
                    GetWorkingDocument(null);

                    if (Entity.RevieweeId.HasValue == false)
                    {
                        return null;
                    }
                    _reviewee = new FacilityUserHelper(_membershipRepo, _lblRepo, _ctxFac, Entity.RevieweeId.Value);
                }

                return _reviewee;
            }
        }
    }
}
