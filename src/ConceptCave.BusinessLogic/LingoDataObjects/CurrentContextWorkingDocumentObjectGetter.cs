﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Checklist.Facilities;
using ConceptCave.Checklist.Interfaces;

namespace ConceptCave.BusinessLogic.LingoDataObjects
{
    public class CurrentContextWorkingDocumentObjectGetter : WorkingDocumentObjectGetter
    {
        public int UserTypeContextPublishedResourceId { get; protected set; }

        public CurrentContextWorkingDocumentObjectGetter() : base() { }

        public CurrentContextWorkingDocumentObjectGetter(int userTypeContextPublishedResourceId, IWorkingDocument workingDocument)
            : base(workingDocument)
        {
            UserTypeContextPublishedResourceId = userTypeContextPublishedResourceId;
        }

        public override void Encode(Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("UserTypeContextPublishedResourceId", UserTypeContextPublishedResourceId);
        }

        public override void Decode(Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            UserTypeContextPublishedResourceId = decoder.Decode<int>("UserTypeContextPublishedResourceId");
        }
    }
}
