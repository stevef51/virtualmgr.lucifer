﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Checklist.Editor;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;

namespace ConceptCave.BusinessLogic.LingoDataObjects
{
    public class MembershipGeoCodeResult : Node, IHasUserId
    {
        public Guid UserId { get; set; }
        public ILatLng Coordinates { get; set; }
        public double Distance { get; set; }

        public decimal Latitude
        {
            get
            {
                return Coordinates.Latitude;
            }
        }

        public decimal Longitude
        {
            get
            {
                return Coordinates.Longitude;
            }
        }

        public MembershipGeoCodeResult()
        {
            Coordinates = new LatLng();
        }

        public override void Encode(Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("UserId", UserId);
            encoder.Encode("Coordinates", Coordinates);
            encoder.Encode("Distance", Distance);
        }

        public override void Decode(Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            UserId = decoder.Decode<Guid>("UserId");
            Coordinates = decoder.Decode<ILatLng>("Coordinates");
            Distance = decoder.Decode<double>("Distance");
        }
    }
}
