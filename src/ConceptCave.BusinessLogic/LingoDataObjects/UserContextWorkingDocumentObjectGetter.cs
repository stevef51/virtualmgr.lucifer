﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.BusinessLogic.ReportData;
using ConceptCave.Checklist.Facilities;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core.Coding;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;

namespace ConceptCave.BusinessLogic.LingoDataObjects
{
    public class ContextWorkingDocumentObjectGetter : WorkingDocumentObjectGetter
    {
        public ContextWorkingDocumentObjectGetter() : base()
        {

        }

        public ContextWorkingDocumentObjectGetter(IWorkingDocument workingDocument) : base(workingDocument)
        {

        }

        public virtual void Save()
        {
            throw new NotImplementedException("This method should be overriden in a derived class");
        }
    }

    //This guy wraps up a working document which is a user context checklist
    //The base class provides the mechanism for interacting with it
    //This class just manages saving it
    public class UserContextWorkingDocumentObjectGetter : ContextWorkingDocumentObjectGetter
    {
        private string _contextName;
        private Guid _userId;
        private readonly IReportingTableWriter _reportWriter;
        public int UserTypeContextPublishedResourceId { get; protected set; }

        public UserContextWorkingDocumentObjectGetter(IReportingTableWriter reportWriter) : base()
        {
            _reportWriter = reportWriter;
        }

        public UserContextWorkingDocumentObjectGetter(int userTypeContextPublishedResourceId, string contextName,
            Guid userId, IReportingTableWriter reportWriter, IWorkingDocument workingDocument)
            : base(workingDocument)
        {
            UserTypeContextPublishedResourceId = userTypeContextPublishedResourceId;
            _contextName = contextName;
            _reportWriter = reportWriter;
            _userId = userId;
        }

        public override void Save()
        {
            //we need an entity to pretend we were in the database
            //see commentds in ReportingTabelWriter

            var wdEntity = new WorkingDocumentDTO()
            {
                __IsNew = true,
                ClientIp = "system",
                DateCreated = DateTime.UtcNow,
                UtcLastModified = DateTime.UtcNow,
                Id = WorkingDocument.Id,
                Name = _contextName,
                UserAgentId = null,
                RevieweeId = _userId,
                ReviewerId = _userId,
                Status = (int)WorkingDocumentStatus.Started,
                PublishingGroupResourceId = UserTypeContextPublishedResourceId,
            };

            //Actually saves the context
            _reportWriter.PrepareWorkingDocument(wdEntity);
            _reportWriter.BreakOutDocumentIntoReportingTables(WorkingDocument, wdEntity, Context.ContextType.Membership);
        }

        public override void Encode(IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("UserTypeContextPublishedResourceId", UserTypeContextPublishedResourceId);
            encoder.Encode("contextName", _contextName);
            encoder.Encode("userId", _userId);
        }

        public override void Decode(IDecoder decoder)
        {
            base.Decode(decoder);

            UserTypeContextPublishedResourceId = decoder.Decode<int>("UserTypeContextPublishedResourceId");
            _contextName = decoder.Decode<string>("contextName");
            _userId = decoder.Decode<Guid>("userId");
        }
    }

    public class AssetContextWorkingDocumentObjectGetter : ContextWorkingDocumentObjectGetter
    {
        private string _contextName;
        private Guid _userId;
        private readonly IReportingTableWriter _reportWriter;
        public int AssetTypeContextPublishedResourceId { get; protected set; }
        public int AssetId { get; protected set; }

        public AssetContextWorkingDocumentObjectGetter(IReportingTableWriter reportWriter) : base()
        {
            _reportWriter = reportWriter;
        }

        public AssetContextWorkingDocumentObjectGetter(int userTypeContextPublishedResourceId, int assetId, string contextName,
            Guid userId, IReportingTableWriter reportWriter, IWorkingDocument workingDocument)
            : base(workingDocument)
        {
            AssetTypeContextPublishedResourceId = userTypeContextPublishedResourceId;
            _contextName = contextName;
            _reportWriter = reportWriter;
            _userId = userId;
            AssetId = assetId;
        }

        public override void Save()
        {
            //we need an entity to pretend we were in the database
            //see comments in ReportingTabelWriter

            var wdEntity = new WorkingDocumentDTO()
            {
                __IsNew = true,
                ClientIp = "system",
                DateCreated = DateTime.UtcNow,
                UtcLastModified = DateTime.UtcNow,
                Id = WorkingDocument.Id,
                Name = _contextName,
                UserAgentId = null,
                RevieweeId = _userId,
                ReviewerId = _userId,
                Status = (int)WorkingDocumentStatus.Started,
                PublishingGroupResourceId = AssetTypeContextPublishedResourceId,
            };

            //Actually saves the context
            _reportWriter.PrepareWorkingDocument(wdEntity);
            _reportWriter.BreakOutDocumentIntoReportingTables(WorkingDocument, wdEntity, Context.ContextType.Asset, false, AssetId);
        }

        public override void Encode(IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("UserTypeContextPublishedResourceId", AssetTypeContextPublishedResourceId);
            encoder.Encode("contextName", _contextName);
            encoder.Encode("userId", _userId);
        }

        public override void Decode(IDecoder decoder)
        {
            base.Decode(decoder);

            AssetTypeContextPublishedResourceId = decoder.Decode<int>("UserTypeContextPublishedResourceId");
            _contextName = decoder.Decode<string>("contextName");
            _userId = decoder.Decode<Guid>("userId");
        }
    }

}
