﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.BusinessLogic.LingoDataObjects
{
    public class CurrentContextFacilityCurrentUsers
    {
        public Guid ReviewerId { get; set; }
        public Guid? RevieweeId { get; set; }

        public FacilityUserHelper Reviewer { get; set; }
        public FacilityUserHelper Reviewee { get; set; }

        //Seriously why is this here?
        public Dictionary<string, object> Context { get; set; }

        public CurrentContextFacilityCurrentUsers()
        {
            Context = new Dictionary<string, object>();
        }
    }
}
