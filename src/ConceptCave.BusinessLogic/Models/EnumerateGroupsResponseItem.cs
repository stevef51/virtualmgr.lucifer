﻿using ConceptCave.DTO.DTOClasses;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ConceptCave.BusinessLogic.Models
{
    public class EnumerateGroupsResponseItem
    {
        public EnumerateGroupsResponseItem()
        {
        }

        public EnumerateGroupsResponseItem(PublishingGroupDTO e)
        {
            Name = e.Name;
            Id = e.Id;
        }

        public string Name { get; set; }
        public int Id { get; set; }
      
    }
}