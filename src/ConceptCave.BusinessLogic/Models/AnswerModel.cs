﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;

namespace ConceptCave.BusinessLogic.Models
{
    public class AnswerModel
    {
        public Guid WorkingDocumentId { get; set; }
        public JObject Answers { get; set; }
    }
}