﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.BusinessLogic.Models
{
    public class EnumerateUsersResponseItem
    {
        public EnumerateUsersResponseItem()
        {
            
        }

        public EnumerateUsersResponseItem(UserDataDTO e)
        {
            Name = e.Name;
            UserName = e.AspNetUser.UserName;
            Id = e.UserId;
        }

        public string Name { get; set; }
        public string UserName { get; set; }
        public Guid Id { get; set; }
    }
}