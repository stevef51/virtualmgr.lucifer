﻿using ConceptCave.DTO.DTOClasses;
using System;
using System.Collections.Generic;
using System.Linq;
namespace ConceptCave.BusinessLogic.Models
{
    public class EnumerateChecklistsResponseItem
    {
        public EnumerateChecklistsResponseItem()
        {
        }

        public EnumerateChecklistsResponseItem(PublishingGroupResourceDTO e)
        {
            GroupResourceId = e.Id;
            GroupId = e.PublishingGroupId;
            ResourceId = e.ResourceId;
            Name = e.Name;
        }

        public string Name { get; set; }
        public int GroupResourceId { get; set; }
        public int ResourceId { get; set; }
        public int GroupId { get; set; }
    }
}