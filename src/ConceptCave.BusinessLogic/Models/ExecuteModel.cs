﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace ConceptCave.BusinessLogic.Models
{
    public class ExecuteModel
    {
        public string Handler { get; set; }
        public string Method { get; set; }
        public JToken Parameters { get; set; }
    }
}
