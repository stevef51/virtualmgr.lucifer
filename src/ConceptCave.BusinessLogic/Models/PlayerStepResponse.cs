﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConceptCave.Checklist.Interfaces;

namespace ConceptCave.BusinessLogic.Models
{
    public class PlayerStepResponse
    {
        public RunMode RunMode { get; set; }
        public IPresented Presented { get; set; }
    }
}