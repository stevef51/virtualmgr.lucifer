﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.BusinessLogic.Models
{
    //Model for JSON response for list of messages
    public class MessageModel
    {
        public enum MessageKind
        {
            Sent, Recieved
        }

        public Guid MessageId { get; set; }
        public string Text { get; set; }
        public MessageKind Kind { get; set; }
        public string SenderName { get; set; }
        public IList<string> RecipientUserNames { get; set; }
        public IList<string> RecipientLabelNames { get; set; }
        public DateTime SentAt { get; set; }
        public bool IsUnread { get; set; }

        public MessageModel()
        {
            RecipientLabelNames = new List<string>();
            RecipientLabelNames = new List<string>();
        }
    }
}
