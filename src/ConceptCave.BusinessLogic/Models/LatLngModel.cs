﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConceptCave.BusinessLogic.Models
{
    // This class models a Latitude, Longitude, Altitude position and is aimed at being compatible with Leaflet LatLng class
    public class LatLngModel
    {
        public decimal lat { get; set; }
        public decimal lng { get; set; }
        public decimal? alt { get; set; }       // Altitude
        public LatLngModel()
        {

        }

        public LatLngModel(decimal lat, decimal lng)
        {
            this.lat = lat;
            this.lng = lng;
        }
        public LatLngModel(decimal lat, decimal lng, decimal? alt)
        {
            this.lat = lat;
            this.lng = lng;
            this.alt = alt;
        }
    }
}