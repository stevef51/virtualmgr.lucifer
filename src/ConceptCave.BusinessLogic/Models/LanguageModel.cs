﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.BusinessLogic.Models
{
    public class LanguageModel
    {
        public string culturename { get; set; }
        public string languagename { get; set; }
        public string nativename { get; set; }
    }
}
