﻿using System;
using System.Collections.Generic;
using System.Linq;

using ConceptCave.Checklist.Interfaces;
using Newtonsoft.Json.Linq;

namespace ConceptCave.BusinessLogic.Models
{
    public class PlayerModel
    {
        public Guid WorkingDocumentId { get; set; }
        public string ChecklistName { get; set; }
        public string ChecklistDescription { get; set; }
        public bool IsValid { get; set; }
        public RunMode State { get; set; }
		public bool ClientFrozen { get; set; }
        public bool CanPrevious { get; set; }
        public bool AreDashboarding { get; set; }
        //Each presented holds some info about the question,
        //an Answer property with the answer
        //and a ValidationError array with any validation errors for it
        public JObject Presented { get; set; }

        /// <summary>
        /// Kind of a hint to the UI on how may steps could be available if all root steps
        /// are gone through
        /// </summary>
        public int RootStepCount { get; set; }
    }
}