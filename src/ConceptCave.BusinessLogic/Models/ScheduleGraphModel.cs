﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.BusinessLogic.Models
{
    public enum ScheduleGraphModelScale
    {
        Hourly
    }

    public class ScheduleGraphModel
    {
        public string RowTitle { get; set; }

        public ScheduleGraphModelScale Scale { get; set; }

        public ScheduleGraphRowModel[] Rows { get; set; }
    }

    public class ScheduleGraphRowModel
    {
        public string Text { get; set; }

        public ScheduleGraphCellModel[] Items { get; set; }
    }

    public class ScheduleGraphCellModel
    {
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public int Duration { get; set; }
        public string Text { get; set; }
    }

}
