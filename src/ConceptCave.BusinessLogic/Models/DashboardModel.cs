﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.BusinessLogic.Models
{
    public class DashboardModel
    {
        public DateTime GeneratedAtUtc { get; set; }
        public bool IsStale { get; set; }
        public IEnumerable<PlayerModel> Dashboards { get; set; } 
    }
}
