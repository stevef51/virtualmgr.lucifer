﻿using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConceptCave.BusinessLogic.Models
{
    public class GpsLocationModel
    {
        public bool __isnew { get; set; }
        public long id { get; set; }
        public int locationtype { get; set; }       // This is a GpsLocationType enum, but want it to serialize as int

        public LatLngModel pos { get; set; }
        public decimal accuracy { get; set; }
        public decimal? altitudeaccuracy { get; set; }

        public int? floor { get; set; }
        public int? facilitystructureid { get; set; }

        public static GpsLocationModel FromDTO(GpsLocationDTO dto)
        {
            return new GpsLocationModel()
            {
                __isnew = dto.__IsNew,
                id = dto.Id,
                locationtype = (int)(GpsLocationType)dto.LocationType,
                pos = new LatLngModel(dto.Latitude, dto.Longitude, dto.Altitude),
                accuracy = dto.AccuracyMetres,
                altitudeaccuracy = dto.AltitudeAccuracyMetres,
                floor = dto.Floor,
                facilitystructureid = dto.FacilityStructureFloorId
            };
        }
    }
}