﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.BusinessLogic.Models
{
    public class DetailChecklistResponseItem : EnumerateChecklistsResponseItem
    {
        public DetailChecklistResponseItem() : base()
        {
            AllowedReviewees = new List<EnumerateUsersResponseItem>();
            AllowedReviewers = new List<EnumerateUsersResponseItem>();
        }

        public DetailChecklistResponseItem(PublishingGroupResourceDTO e, IEnumerable<UserDataDTO> allowedReviewers,
                                           IEnumerable<UserDataDTO> allowedReviewees) : base(e)
        {
            AllowedReviewers = new List<EnumerateUsersResponseItem>(
                allowedReviewers.Select(r => new EnumerateUsersResponseItem(r)));
            AllowedReviewees = new List<EnumerateUsersResponseItem>(
                allowedReviewees.Select(r => new EnumerateUsersResponseItem(r)));
        }

        public IList<EnumerateUsersResponseItem> AllowedReviewers { get; set; }
        public IList<EnumerateUsersResponseItem> AllowedReviewees { get; set; }
    }
}