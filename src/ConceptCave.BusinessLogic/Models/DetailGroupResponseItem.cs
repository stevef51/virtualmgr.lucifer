﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.BusinessLogic.Models
{
    public class DetailGroupResponseItem : EnumerateGroupsResponseItem
    {
        public DetailGroupResponseItem() : base()
        {
            Checklists = new List<EnumerateChecklistsResponseItem>();
            Reviewees = new List<EnumerateUsersResponseItem>();
        }

        public DetailGroupResponseItem(PublishingGroupDTO e, IEnumerable<PublishingGroupResourceDTO> checklists,
                                       IEnumerable<UserDataDTO> allowedReviewees) : base(e)
        {
            Checklists = new List<EnumerateChecklistsResponseItem>(
                checklists.Select(r => new EnumerateChecklistsResponseItem(r)));
            Reviewees = new List<EnumerateUsersResponseItem>(
                allowedReviewees.Select(r => new EnumerateUsersResponseItem(r)));
        }

        public IList<EnumerateChecklistsResponseItem> Checklists { get; set; }
        public IList<EnumerateUsersResponseItem> Reviewees { get; set; } 
    }
}