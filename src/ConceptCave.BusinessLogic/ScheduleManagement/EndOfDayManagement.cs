﻿using ConceptCave.Checklist;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.RepositoryInterfaces.ScheduleManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace ConceptCave.BusinessLogic.ScheduleManagement
{
    public class EndOfDayManagement : ScheduleManagementBase, IEndOfDayManagement
    {
        private IHierarchyRepository _hierarchyRepo;
        private IScheduledTaskRepository _scheduleRepo;
        private ITaskRepository _taskRepo;
        private IMembershipRepository _memberRepo;
        private ITaskChangeRequestRepository _changeRequestRepo;
        private ITaskWorkLogRepository _logRepo;

        public bool MarkAsFinishedBySystem { get; set; }

        public EndOfDayManagement(IRosterRepository rosterRepo,
            IHierarchyRepository hierarchyRepo,
            IScheduledTaskRepository scheduleRepo,
            ITaskRepository taskRepo,
            IMembershipRepository memberRepo,
            ITaskChangeRequestRepository changeRequestRepo,
            ITaskWorkLogRepository logRepo,
            IDateTimeProvider dateTimeProvider) : base(rosterRepo, dateTimeProvider)
        {
            _hierarchyRepo = hierarchyRepo;
            _scheduleRepo = scheduleRepo;
            _taskRepo = taskRepo;
            _memberRepo = memberRepo;
            _changeRequestRepo = changeRequestRepo;
            _logRepo = logRepo;

            MarkAsFinishedBySystem = false;
        }

        public List<EndOfDayBucketJson> Buckets(int rosterId, DateTime rosterStartDate, TimeZoneInfo tz)
        {
            var roster = _rosterRepo.GetById(rosterId);

            var buckets = _hierarchyRepo.GetBucketsForHierarchy(roster.HierarchyId, RepositoryInterfaces.Enums.HierarchyBucketLoadInstructions.BucketRole);

            IList<HierarchyBucketDTO> rootBuckets = new List<HierarchyBucketDTO>();
            if (buckets.Count > 0)
            {
                rootBuckets.Add(buckets.First(b => !b.ParentId.HasValue));
            }

            return ConstructEndOfDayJson(rosterId, rosterStartDate, tz, roster, buckets);
        }

        public List<EndOfDayBucketJson> Buckets(int rosterId, Guid userId, DateTime rosterStartDate, TimeZoneInfo tz)
        {
            var roster = _rosterRepo.GetById(rosterId);

            var buckets = _hierarchyRepo.GetBucketsForUser(userId, roster.HierarchyId, null, RepositoryInterfaces.Enums.HierarchyBucketLoadInstructions.BucketRole);

            return ConstructEndOfDayJson(rosterId, rosterStartDate, tz, roster, buckets);
        }

        /// <summary>
        /// Simplified approval method that simply uses the ends on date of tasks + their status to work out
        /// if they should be finished or not.
        /// </summary>
        /// <param name="rosterId">ID of roster for which end of day is being run</param>
        /// <param name="utcDate">Date, in UTC, that tasks ends on will be selected on (less than)</param>
        public IList<ProjectJobTaskDTO> Approve(RosterDTO roster, DateTime utcDate)
        {
            var tasks = _taskRepo.GetForRoster(roster.Id, utcDate, TaskStatusFilter.Approved |
                TaskStatusFilter.ApprovedContinued |
                TaskStatusFilter.ChangeRosterRejected |
                TaskStatusFilter.ChangeRosterRequested |
                TaskStatusFilter.Paused |
                TaskStatusFilter.Started |
                TaskStatusFilter.Unapproved, ProjectJobTaskLoadInstructions.None);

            ProjectJobTaskStatus status = ProjectJobTaskStatus.FinishedByManagement;

            if (this.MarkAsFinishedBySystem == true)
            {
                status = ProjectJobTaskStatus.FinishedBySystem;
            }

            DateTime utcNow = DateTime.UtcNow;

            // so everything in tasks has an ends on date less than utcDate and isn't currently completed.
            // so we need to process these suckers and finish or approve continue them

            foreach (var dto in tasks)
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    if (roster.EndOfDayDefaultTaskStatus == (int)RosterEndOfDayDefaultStatus.ContinueNextShiftForUser)
                    {
                        dto.Status = (int)ProjectJobTaskStatus.ApprovedContinued;
                        dto.EndsOn = dto.EndsOn.AddDays(1);
                        dto.DateCompleted = null;

                        _taskRepo.Save(dto, true, false);
                        scope.Complete();
                        continue;
                    }

                    // so it's one of the finish status that it's getting
                    dto.Status = (int)status;
                    dto.StatusDate = utcNow;
                    dto.DateCompleted = utcNow;

                    _changeRequestRepo.DeleteOpenRequestsByTaskId(dto.Id);
                    _taskRepo.SetTaskStatusWhenFinishing(dto);
                    ClockOutOfTask(dto.Id, dto.OwnerId);
                    _taskRepo.Save(dto, true, false);

                    scope.Complete();
                }
            }

            return tasks;
        }

        public void Approve(List<EndOfDayBucketJson> buckets, Guid userId)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                DateTime utcNow = DateTime.UtcNow;

                ProjectJobTaskStatus status = ProjectJobTaskStatus.FinishedByManagement;

                if (this.MarkAsFinishedBySystem == true)
                {
                    status = ProjectJobTaskStatus.FinishedBySystem;
                }

                foreach (var bucket in buckets)
                {
                    // primary stuff first
                    foreach (var slot in bucket.PrimaryBucket.Slots)
                    {
                        if (slot.FinishedByManagementTasks != null)
                        {
                            foreach (var task in slot.FinishedByManagementTasks)
                            {
                                var dto = _taskRepo.GetById(task.Id, ProjectJobTaskLoadInstructions.None);
                                dto.Status = (int)status;
                                dto.StatusDate = utcNow;
                                dto.DateCompleted = utcNow;

                                _changeRequestRepo.DeleteOpenRequestsByTaskId(dto.Id);
                                _taskRepo.SetTaskStatusWhenFinishing(dto);
                                ClockOutOfTask(task.Id, dto.OwnerId);
                                _taskRepo.Save(dto, false, false);
                            }
                        }

                        if (slot.AssignedTasks != null)
                        {
                            foreach (var task in slot.AssignedTasks)
                            {
                                var dto = _taskRepo.GetById(task.Id, ProjectJobTaskLoadInstructions.None);
                                dto.Status = (int)ProjectJobTaskStatus.ApprovedContinued;
                                dto.StatusDate = utcNow;
                                dto.DateCompleted = null;

                                _changeRequestRepo.DeleteOpenRequestsByTaskId(dto.Id);
                                ClockOutOfTask(task.Id, dto.OwnerId);
                                _taskRepo.Save(dto, false, false);
                            }
                        }
                    }

                    foreach (var slot in bucket.SecondaryBucket.Slots)
                    {
                        if (slot.AssignedTasks == null || slot.AssignedTasks.Count == 0)
                        {
                            continue;
                        }

                        // we handle this by putting a change request in for the task. This
                        // doesn't move the task, just places it in a state where it can be moved
                        // by a supervisor in the other roster. Once they accept the change (in schedule approval)
                        // then things become permanent.
                        foreach (var task in slot.AssignedTasks)
                        {
                            var dto = _taskRepo.GetById(task.Id, ProjectJobTaskLoadInstructions.None);
                            dto.Status = (int)ProjectJobTaskStatus.ChangeRosterRequested;
                            dto.StatusDate = utcNow;

                            var openRequests = _changeRequestRepo.GetOpenRequestsByTaskId(task.Id);

                            ProjectJobTaskChangeRequestDTO change = null;

                            if (openRequests.Count > 0)
                            {
                                change = openRequests[0];
                            }
                            else
                            {
                                change = new ProjectJobTaskChangeRequestDTO()
                                {
                                    __IsNew = true,
                                    Id = CombFactory.NewComb(),
                                    ProjectJobTaskId = task.Id,
                                    FromRosterId = bucket.PrimaryBucket.RosterId,
                                    ChangeType = 1
                                };
                            }

                            change.RequestedByUserId = userId;
                            change.ToRosterId = slot.Id;

                            _changeRequestRepo.Save(change, false, false);
                            ClockOutOfTask(task.Id, dto.OwnerId);
                            _taskRepo.Save(dto, false, false);
                        }
                    }
                }

                scope.Complete();
            }
        }

        protected List<EndOfDayBucketJson> ConstructEndOfDayJson(int rosterId, DateTime rosterStartDate, TimeZoneInfo tz, RosterDTO roster, IList<HierarchyBucketDTO> buckets)
        {
            var rosters = _rosterRepo.GetAll();

            var rs = from r in rosters where r.Id != rosterId select r;

            BucketJson secondaryRoster = new BucketJson();
            secondaryRoster.Date = rosterStartDate;
            secondaryRoster.RosterId = -1;
            secondaryRoster.RosterName = "";
            secondaryRoster.Slots = new List<SlotJson>();

            // get any items that have been requested to be moved and are still open (if they've been accepted, then allowing them to be recalled isn't supported at present so lets not get them)
            var movedItems = _changeRequestRepo.GetOpenRequestsByFromRosterId(rosterId, ProjectJobTaskChangeRequestLoadInstructions.Task |
                ProjectJobTaskChangeRequestLoadInstructions.TaskType |
                ProjectJobTaskChangeRequestLoadInstructions.TaskUsers);

            foreach (var rster in rs)
            {
                // we just create a dummy slot in here so that the user can move tasks to it
                // to move the tasks between rosters
                SlotJson slot = new SlotJson();
                slot.Id = rster.Id;
                slot.Name = rster.Name;
                slot.AssignedTasks = new List<ScheduleJson>();

                var movedToThis = (from c in movedItems where c.ToRosterId == rster.Id select c);

                foreach (var t in movedToThis)
                {
                    ScheduleJson schedule = new ScheduleJson();
                    schedule.FromDto(t.ProjectJobTask, rosterStartDate, false);

                    slot.AssignedTasks.Add(schedule);
                }

                secondaryRoster.Slots.Add(slot);
            }

            List<EndOfDayBucketJson> result = new List<EndOfDayBucketJson>();

            var rosterStartDateUTC = TimeZoneInfo.ConvertTimeToUtc(rosterStartDate, tz);

            foreach (var bucket in buckets)
            {
                EndOfDayBucketJson endOfDayResult = new EndOfDayBucketJson();

                List<SlotJson> slots = new List<SlotJson>();

                var subtree = _hierarchyRepo.GetSubTreeBuckets(roster.HierarchyId, bucket.LeftIndex, bucket.RightIndex,
                    RepositoryInterfaces.Enums.HierarchyBucketLoadInstructions.BucketRole |
                    RepositoryInterfaces.Enums.HierarchyBucketLoadInstructions.User |
                    RepositoryInterfaces.Enums.HierarchyBucketLoadInstructions.Overrides |
                    RepositoryInterfaces.Enums.HierarchyBucketLoadInstructions.OverridesUsers,
                    rosterStartDateUTC);

                foreach (var node in subtree)
                {
                    if (node.UserId.HasValue == false)
                    {
                        // there can't be any tasks to this node as there is no one to get them so we go merrily on our way
                        continue;
                    }

                    // there is a user associated with the bucket, in this scenario we have to use a merge of the schedule and their active tasks.
                    CreateAssignedSchedule(node, roster, slots, rosterStartDate, tz);
                }

                BucketJson r = new BucketJson();
                r.Date = rosterStartDate;
                r.RosterId = rosterId;
                r.RosterName = roster.Name;
                r.Role = bucket.HierarchyRole.Name;
                r.Slots = slots;

                endOfDayResult.PrimaryBucket = r;
                endOfDayResult.SecondaryBucket = secondaryRoster;

                result.Add(endOfDayResult);
            }

            return result;
        }

        protected void CreateAssignedSchedule(HierarchyBucketDTO node, RosterDTO roster, IList<SlotJson> slots, DateTime rosterStartDate, TimeZoneInfo tz)
        {
            // we'll call live tasks anything that they were approved to do and didn't start, anything they started, or anything we've finished for them
            // everything that's been finished complete or incomplete we aren't interested in
            // we don't worry about the ones in the ChangeRosterRequested state as these are sort of 1/2 way between rosters
            // they'll get picked up in the secondary bucket loading as that's where they are sitting
            var liveAllTasks = _taskRepo.GetForBucket(node.Id, true, false,
                TaskStatusFilter.Approved |
                TaskStatusFilter.ApprovedContinued |
                TaskStatusFilter.ChangeRosterRejected |
                TaskStatusFilter.FinishedByManagement |
                TaskStatusFilter.FinishedBySystem |
                TaskStatusFilter.Paused |
                TaskStatusFilter.Started,
                ProjectJobTaskLoadInstructions.AllTranslated |
                ProjectJobTaskLoadInstructions.TaskType |
                ProjectJobTaskLoadInstructions.Users);
            // we grab these by status date as otherwise we'd end up with all finished by management tasks for the user
            // going back to the start of the universe (well the system at least :))
            var fbymAllTasks = _taskRepo.GetForBucket(node.Id, rosterStartDate, tz, ProjectJobTaskStatus.FinishedByManagement | ProjectJobTaskStatus.FinishedBySystem,
                ProjectJobTaskLoadInstructions.AllTranslated |
                ProjectJobTaskLoadInstructions.TaskType |
                ProjectJobTaskLoadInstructions.Users);

            // now just in case people have been moved around in the hierarchy 
            // (so for example start of day user1 in bucket 1, during the day they get promoted/moved so by end of day they're in bucket 2), 
            // we are going to need to group what we have from the bucket select by ownerid
            Dictionary<Guid, Guid> userHash = new Dictionary<Guid, Guid>();

            foreach (var uid in (from l in liveAllTasks select l.OwnerId).Distinct())
            {
                userHash.Add(uid, uid);
            }

            foreach (var uid in (from l in fbymAllTasks select l.OwnerId).Distinct())
            {
                if (userHash.ContainsKey(uid) == false)
                {
                    userHash.Add(uid, uid);
                }
            }

            // typicall we'll have 1 user in this hash, but if there has been a movement in the
            // hierarchy there maybe more, so a slot for each.
            foreach (var uid in userHash.Keys)
            {
                SlotJson slot = null;

                var existingSlot = from s in slots where s.UserId == uid && s.Id == node.Id select s;

                if (existingSlot.Count() > 0)
                {
                    slot = existingSlot.First();
                }
                else
                {
                    slot = new SlotJson();
                }

                var user = _memberRepo.GetById(uid);

                var liveTasks = from l in liveAllTasks where l.OwnerId == uid select l;
                var fbymTasks = from l in fbymAllTasks where l.OwnerId == uid select l;

                slot.Id = node.Id;
                slot.Role = node.HierarchyRole.Name;
                slot.RoleId = node.RoleId;

                slot.UserId = uid;
                slot.Name = user.Name;

                slots.Add(slot);

                // assigned tasks will be used to hold anything that the user is approved to do, or is started or paused by them
                List<ScheduleJson> assignedTasks = new List<ScheduleJson>();
                // this will hold anything that is marked as finished by management for them
                List<ScheduleJson> finishedByManagementTasks = new List<ScheduleJson>();

                slot.AssignedTasks = assignedTasks;
                slot.FinishedByManagementTasks = finishedByManagementTasks;

                var ud = TimeZoneInfo.ConvertTimeToUtc(rosterStartDate, tz);
                var ud1 = ud.AddDays(1);
                var nowUtc = DateTime.UtcNow;

                // anything that is in the finished by management status is easy, it just goes into that collection
                finishedByManagementTasks.AddRange(fbymTasks.Select(t => TaskToJson(t, rosterStartDate)));

                // anything that is considered live and who's ends on day hasn't yet passed gets put into 
                // the assigned tasks (it can be continued tomorrow)
                assignedTasks.AddRange((from l in liveTasks
                                        where
                                            l.Status != (int)ProjectJobTaskStatus.FinishedByManagement &&
                                            l.Status != (int)ProjectJobTaskStatus.FinishedBySystem &&
                                            l.EndsOn > nowUtc
                                        select l).Select(t => TaskToJson(t, rosterStartDate)));

                // anything else depends on what the setting is in the roster for the default end of day starting point, provided
                // its ends on date has passed by
                var others = from l in liveTasks where 
                                 l.Status != (int)ProjectJobTaskStatus.FinishedByManagement && 
                                 l.Status != (int)ProjectJobTaskStatus.FinishedBySystem &&
                                 l.EndsOn <= nowUtc
                             select l;

                if (roster.EndOfDayDefaultTaskStatus == (int)RosterEndOfDayDefaultStatus.FinishedByManagenent)
                {
                    finishedByManagementTasks.AddRange(others.Select(t => TaskToJson(t, rosterStartDate)));
                }
                else
                {
                    assignedTasks.AddRange(others.Select(t => TaskToJson(t, rosterStartDate)));
                }
            }
        }

        protected ScheduleJson TaskToJson(ProjectJobTaskDTO task, DateTime compareDate)
        {
            ScheduleJson result = new ScheduleJson();

            result.FromDto(task, compareDate, false);

            return result;
        }

        protected void ClockOutOfTask(Guid taskId, Guid userId)
        {
            var log = _logRepo.GetCurrentActive(taskId, userId, RepositoryInterfaces.Enums.ProjectJobTaskWorkLogLoadInstructions.None);

            if (log != null)
            {
                // there still clocked into something
                log.ClockoutAccuracy = null;
                log.ClockoutLatitude = null;
                log.ClockoutLongitude = null;
                log.ClockoutLocationStatus = (int)GeoLocationStatus.Unknown;

                log.DateCompleted = DateTime.UtcNow;

                _logRepo.Save(log, false, false);
            }
        }
    }
}
