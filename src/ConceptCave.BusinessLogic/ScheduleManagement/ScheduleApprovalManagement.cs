﻿using ConceptCave.Checklist;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.RepositoryInterfaces.ScheduleManagement;
using ConceptCave.RepositoryInterfaces.Workloading;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;
using VirtualMgr.Common;

namespace ConceptCave.BusinessLogic.ScheduleManagement
{
    public class ScheduleApprovalManagement : ScheduleManagementBase, IScheduleApprovalManagement
    {
        private IHierarchyRepository _hierarchyRepo;
        private IScheduledTaskRepository _scheduleRepo;
        private ITaskRepository _taskRepo;
        private IMembershipRepository _memberRepo;
        private ITaskChangeRequestRepository _changeRequestRepo;
        private IWorkloadingActivityFactory _activityFactory;
        private IWorkloadingDurationCalculator _durationCalculator;

        protected Dictionary<string, HierarchyBucketDTO> _bucketCache;

        public ScheduleApprovalManagement(IRosterRepository rosterRepo,
            IHierarchyRepository hierarchyRepo,
            IScheduledTaskRepository scheduleRepo,
            ITaskRepository taskRepo,
            IMembershipRepository memberRepo,
            ITaskChangeRequestRepository changeRequestRepo,
            IWorkloadingActivityFactory activityFactory,
            IWorkloadingDurationCalculator durationCalculator,
            IDateTimeProvider dateTimeProvider) : base(rosterRepo, dateTimeProvider)
        {
            _hierarchyRepo = hierarchyRepo;
            _scheduleRepo = scheduleRepo;
            _taskRepo = taskRepo;
            _memberRepo = memberRepo;
            _changeRequestRepo = changeRequestRepo;
            _activityFactory = activityFactory;
            _durationCalculator = durationCalculator;

            _bucketCache = new Dictionary<string, HierarchyBucketDTO>();
        }

        /// <summary>
        /// Retrieves the root level bucket for the roster
        /// </summary>
        /// <param name="rosterId"></param>
        /// <param name="rosterStartDateUTC"></param>
        /// <returns></returns>
        public ScheduleApprovalJson Buckets(int rosterId, DateTime rosterStartDate, TimeZoneInfo tz, bool includeWorkloading, bool includeRootNode = false)
        {
            var roster = _rosterRepo.GetById(rosterId, RosterLoadInstructions.ScheduleDays);

            var buckets = _hierarchyRepo.GetBucketsForHierarchy(roster.HierarchyId, RepositoryInterfaces.Enums.HierarchyBucketLoadInstructions.BucketRole);

            IList<HierarchyBucketDTO> rootBuckets = new List<HierarchyBucketDTO>();
            if (buckets.Count > 0)
            {
                rootBuckets.Add(buckets.First(b => !b.ParentId.HasValue));
            }

            return ConstructScheduleApprovalJson(rosterId, rosterStartDate, tz, roster, rootBuckets, includeWorkloading, includeRootNode);
        }

        public ScheduleApprovalJson Buckets(int rosterId, DateTime rosterStartDate, TimeZoneInfo tz, Guid userId, bool includeWorkloading, bool includeRootNode = false)
        {
            var roster = _rosterRepo.GetById(rosterId, RosterLoadInstructions.ScheduleDays);

            var buckets = _hierarchyRepo.GetBucketsForUser(userId, roster.HierarchyId, null, RepositoryInterfaces.Enums.HierarchyBucketLoadInstructions.BucketRole);

            return ConstructScheduleApprovalJson(rosterId, rosterStartDate, tz, roster, buckets, includeWorkloading, includeRootNode);
        }

        public BucketJson ApproveUnchanged(DateTime date, TimeZoneInfo tz, int roster, int[] slots, Guid processedByUserId, bool includeWorkloading)
        {
            BucketJson bucket = new BucketJson();
            bucket.RosterId = roster;
            bucket.Date = date;

            foreach (var id in slots)
            {
                var slot = LoadSlot(id, date, tz, includeWorkloading);
                bucket.Slots.Add(slot);
            }

            using (TransactionScope scope = new TransactionScope())
            {
                ApprovePrimaryBuckets(new BucketJson[] { bucket }, tz, processedByUserId);

                scope.Complete();
            }

            return bucket;
        }

        /// <summary>
        /// Replicates any tasks created against the primary user (held in userid in the bucket) for any
        /// users referenced by labels on a bucket that is flagged as assigning out based on labels
        /// </summary>
        /// <param name="bucket"></param>
        /// <returns></returns>
        public BucketJson ReplicateTasksForLabels(BucketJson bucket)
        {
            var candidates = (from s in bucket.Slots from
                              a in s.AssignedTasks where a.Bucket != null && a.Bucket.AssignTasksThroughLabels == true && a.Task != null select a);

            if (candidates.Count() == 0)
            {
                return bucket;
            }

            List<ProjectJobTaskDTO> tasks = new List<ProjectJobTaskDTO>();
            DateTime utcNow = DateTime.UtcNow;
            foreach (var c in candidates)
            {
                var mergedBuckets = _hierarchyRepo.GetMergedHierarchyBuckets(c.Bucket.Id, false);

                foreach (var mb in mergedBuckets)
                {
                    if (mb.UserId.HasValue == false)
                    {
                        continue;
                    }

                    if(mb.ExpiryDate.HasValue == true)
                    {
                        var tzInfo = TimeZoneInfo.FindSystemTimeZoneById(mb.TimeZone);
                        DateTime timeInZone = TimeZoneInfo.ConvertTimeFromUtc(utcNow, tzInfo);

                        if (timeInZone > mb.ExpiryDate)
                        {
                            // the user has expired, no use giving them a task
                            continue;
                        }
                    }

                    var task = _taskRepo.CopyTask(c.Task);
                    task.__IsNew = true;
                    task.Id = CombFactory.NewComb();
                    task.Owner = null;
                    task.OwnerId = mb.UserId.Value;
                    task.ScheduledOwnerId = mb.UserId.Value;
                    task.OriginalOwnerId = mb.UserId.Value;

                    tasks.Add(task);
                    c.ReplicatedTasks.Add(task);
                }
            }

            _taskRepo.Save(tasks, false, false);

            return bucket;
        }

        /// <summary>
        /// This method runs through the cancelled tasks lists and marks the change requests as cancelled
        /// and marks the tasks as Rejected.
        /// </summary>
        /// <param name="slots"></param>
        public void RejectCancelledSecondaryTasks(IEnumerable<SlotJson> slots, Guid processedByUserId)
        {
            if (slots == null)
            {
                return;
            }

            using (TransactionScope scope = new TransactionScope())
            {
                foreach (var slot in slots)
                {
                    if (slot.CancelledTasks.Count == 0)
                    {
                        continue;
                    }

                    var taskIds = from t in slot.CancelledTasks select t.Id;

                    var tasks = _taskRepo.GetById(taskIds.ToArray(), ProjectJobTaskLoadInstructions.None);

                    foreach (var task in tasks)
                    {
                        var openRequests = _changeRequestRepo.GetOpenRequestsByTaskId(task.Id, ProjectJobTaskChangeRequestLoadInstructions.None);
                        if (openRequests.Count == 0)
                        {
                            // this shouldn't happen
                            continue;
                        }

                        task.Status = (int)ProjectJobTaskStatus.ChangeRosterRejected;
                        task.StatusDate = DateTime.UtcNow;

                        openRequests.First().Status = (int)ProjectJobTaskChangeRequestStatus.Rejected;
                        openRequests.First().ProcessedByUserId = processedByUserId;

                        _taskRepo.Save(task, false, false);
                        _changeRequestRepo.Save(openRequests.First(), false, false);
                    }
                }

                scope.Complete();
            }
        }

        public void ApprovePrimaryBuckets(IEnumerable<BucketJson> buckets, TimeZoneInfo tz, Guid processedByUserId)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                foreach (var bucket in buckets)
                {
                    var date = bucket.Date;
                    var rosterId = bucket.RosterId;

                    IList<SlotJson> slots = bucket.Slots;

                    foreach (var slot in slots)
                    {
                        if (slot.UserId.HasValue == false)
                        {
                            // the slot doesn't have a user assigned, we can't do anything with this so move on
                            continue;
                        }

                        // this id is always correct as if the user has been swapped out for another user we assume that
                        // its modelled this in the incoming data.
                        Guid userId = slot.UserId.Value;

                        // see if any overrides have been, we do this by comparing userId with either the buckets userid or if there are overrides in place
                        // with the last overrides userid. If different then things have changed.
                        var node = _hierarchyRepo.GetById(slot.Id, HierarchyBucketLoadInstructions.Overrides | HierarchyBucketLoadInstructions.OverridesUsers, date);
                        Guid? overriddenUserId = node.UserId;

                        if (node.HierarchyBucketOverrides.Count > 0)
                        {
                            overriddenUserId = node.HierarchyBucketOverrides[node.HierarchyBucketOverrides.Count - 1].UserId;
                        }

                        if (overriddenUserId.HasValue == false || overriddenUserId.Value != userId)
                        {
                            var d = date.ToUniversalTime();
                            // the user has been changed from what we have in the db, we need to add an override
                            HierarchyBucketOverrideDTO o = new HierarchyBucketOverrideDTO()
                            {
                                __IsNew = true,
                                StartDate = d,
                                EndDate = d.AddDays(1),
                                DateCreated = DateTime.UtcNow,
                                RoleId = node.RoleId,
                                UserId = userId,
                                OriginalUserId = slot.OverriddenUserId
                            };
                            node.HierarchyBucketOverrides.Add(o);

                            _hierarchyRepo.Save(node, false, true);
                        }

                        // assigned stuff first
                        ManageApprovals(date, tz, rosterId, slot.Id, slot.AssignedTasks, userId, slot.OverriddenUserId.HasValue ? slot.OverriddenUserId.Value : userId, processedByUserId, ProjectJobTaskStatus.Approved);
                        ManageApprovals(date, tz, rosterId, slot.Id, slot.UnassignedTasks, userId, slot.OverriddenUserId.HasValue ? slot.OverriddenUserId.Value : userId, processedByUserId, ProjectJobTaskStatus.Unapproved);
                        ManageApprovals(date, tz, rosterId, slot.Id, slot.CancelledTasks, userId, slot.OverriddenUserId.HasValue ? slot.OverriddenUserId.Value : userId, processedByUserId, ProjectJobTaskStatus.Cancelled);
                    }
                }

                scope.Complete();
            }
        }

        public SlotJson LoadSlot(int id, DateTime date, TimeZoneInfo tz, bool includeWorkloading)
        {
            var node = _hierarchyRepo.GetById(id, RepositoryInterfaces.Enums.HierarchyBucketLoadInstructions.BucketRole |
            RepositoryInterfaces.Enums.HierarchyBucketLoadInstructions.User |
            RepositoryInterfaces.Enums.HierarchyBucketLoadInstructions.Overrides |
            RepositoryInterfaces.Enums.HierarchyBucketLoadInstructions.OverridesUsers,
            date);

            if (node == null)
            {
                throw new ArgumentException(string.Format("There is no hierarchy bucket with id {0}", id));
            }

            var roster = _rosterRepo.GetForHierarchies(new int[] { node.HierarchyId }, RosterLoadInstructions.ScheduleDays);
            IList<SlotJson> slots = new List<SlotJson>();

            if (node.UserId.HasValue == false)
            {
                // no user, pretty easy in that case as there can't be any task instances as there isn't anyone to associate those with
                CreateUnassignedSchedule(node, roster.First(), slots, date.DayOfWeek, date, tz, includeWorkloading);
            }
            else
            {
                // there is a user associated with the bucket, in this scenario we have to use a merge of the schedule and their active tasks.
                CreateAssignedSchedule(node, roster.First(), slots, date, tz, includeWorkloading);
            }

            slots.First().PrepareAsDetailed();

            return slots.First();
        }

        protected DateTime DateToUtc(DateTime date, string timezone)
        {
            var tz = TimeZoneInfoResolver.ResolveWindows(timezone);
            return TimeZoneInfo.ConvertTimeToUtc(date, tz);
        }

        private ScheduleApprovalJson ConstructScheduleApprovalJson(int rosterId,
            DateTime rosterStartDate,
            TimeZoneInfo tz,
            RosterDTO roster,
            IList<HierarchyBucketDTO> buckets,
            bool includeWorkloading,
            bool includeRootNode)
        {
            ScheduleApprovalJson scheduleApprovalResult = new ScheduleApprovalJson();
            scheduleApprovalResult.SecondaryBucket = new BucketJson()
            {
                Slots = new List<SlotJson>()
            };

            var fromOthers = _changeRequestRepo.GetOpenRequestsByToRosterId(rosterId,
                ProjectJobTaskChangeRequestLoadInstructions.Task |
                ProjectJobTaskChangeRequestLoadInstructions.TaskType |
                ProjectJobTaskChangeRequestLoadInstructions.TaskUsers |
                ProjectJobTaskChangeRequestLoadInstructions.Rosters |
                ProjectJobTaskChangeRequestLoadInstructions.TaskRole |
                ProjectJobTaskChangeRequestLoadInstructions.Users);

            var othersGrouped = from o in fromOthers where o.FromRosterId.HasValue == true group o by o.FromRosterId into g select new { rosterid = g.Key, tasks = g.ToList() };

            foreach (var other in othersGrouped)
            {
                SlotJson slot = new SlotJson();
                slot.UnassignedTasks = new List<ScheduleJson>();
                slot.Id = other.rosterid.Value;
                slot.Name = other.tasks.First().FromRoster.Name;
                slot.IsNormal = false;

                foreach (var task in other.tasks)
                {
                    ScheduleJson schedule = new ScheduleJson();
                    schedule.FromDto(task.ProjectJobTask, rosterStartDate, includeWorkloading, ScheduleJsonType.OtherRoster);
                    schedule.FromUserName = task.RequestedByUserData.Name;

                    slot.UnassignedTasks.Add(schedule);
                }

                scheduleApprovalResult.SecondaryBucket.Slots.Add(slot);
            }

            var rosterStartDateUTC = TimeZoneInfo.ConvertTimeToUtc(rosterStartDate, tz);

            foreach (var bucket in buckets)
            {
                List<SlotJson> slots = new List<SlotJson>();

                var subtree = _hierarchyRepo.GetSubTreeBuckets(roster.HierarchyId, bucket.LeftIndex, bucket.RightIndex,
                    RepositoryInterfaces.Enums.HierarchyBucketLoadInstructions.BucketRole |
                    RepositoryInterfaces.Enums.HierarchyBucketLoadInstructions.User |
                    RepositoryInterfaces.Enums.HierarchyBucketLoadInstructions.Overrides |
                    RepositoryInterfaces.Enums.HierarchyBucketLoadInstructions.OverridesUsers,
                    rosterStartDateUTC,
                    includeRootNode);

                foreach (var node in subtree)
                {
                    if (node.UserId.HasValue == false)
                    {
                        // no user, pretty easy in that case as there can't be any task instances as there isn't anyone to associate those with
                        // we use the local date/time for the day of the week
                        CreateUnassignedSchedule(node, roster, slots, rosterStartDate.DayOfWeek, rosterStartDate, tz, includeWorkloading);
                        continue;
                    }

                    // there is a user associated with the bucket, in this scenario we have to use a merge of the schedule and their active tasks.
                    CreateAssignedSchedule(node, roster, slots, rosterStartDate, tz, includeWorkloading);
                }

                BucketJson r = new BucketJson();
                r.Date = rosterStartDate;
                r.RosterId = rosterId;
                r.RosterName = roster.Name;
                r.Role = bucket.HierarchyRole.Name;
                r.Slots = slots;

                r.Slots.ForEach(s => s.PrepareAsHeader());

                scheduleApprovalResult.PrimaryBuckets.Add(r);
            }

            return scheduleApprovalResult;
        }

        protected void ManageApprovals(DateTime date, TimeZoneInfo tz, int rosterId, int bucketId, IList<ScheduleJson> tasks, Guid userId, Guid scheduledUserId, Guid processedByUserId, ProjectJobTaskStatus status)
        {
            if (tasks == null)
            {
                return;
            }

            foreach (ScheduleJson task in tasks)
            {
                string taskType = task.Type;
                Guid taskId = task.Id;

                if (taskType == ScheduleJsonType.Scheduled.ToString())
                {
                    var existingTask = _taskRepo.GetForUser(userId, taskId, rosterId, task.RoleId.Value, date, tz, RepositoryInterfaces.Enums.ProjectJobTaskLoadInstructions.None);

                    if (existingTask != null)
                    {
                        // this shouldn't happen really as the schedules and tasks are merged together with tasks taking precendence, so there shouldn't be a case where
                        // we have a schedule and a task. If the item coming in is a schedule it means there isn't a task for it, so we need to create one.
                        // If somehow we got here, we'll just move on with our lives for the moment.
                        continue;
                    }

                    var schedule = _scheduleRepo.GetById(taskId,
                        ProjectJobScheduledTaskLoadInstructions.ScheduleLabels |
                        ProjectJobScheduledTaskLoadInstructions.WorkloadingActivities |
                        ProjectJobScheduledTaskLoadInstructions.AllTranslated |
                        ProjectJobScheduledTaskLoadInstructions.CalendarRule);

                    // on the client side the scheduled task could have been moved to another user, in which case it will be in a slot that is for a different user
                    // but the original user of the task will be untouched
                    Guid? originalId = null;
                    if (task.OriginalUserId.HasValue == true && task.OriginalUserId.Value != userId)
                    {
                        originalId = task.OriginalUserId.Value;
                    }

                    var t = _taskRepo.New(schedule, tz, userId, bucketId, _memberRepo, originalId, originalId.HasValue ? originalId : scheduledUserId, status);

                    if (status == ProjectJobTaskStatus.Cancelled)
                    {
                        t.DateCompleted = DateTime.UtcNow;
                        _taskRepo.Save(t, false, false);
                    }

                    task.Task = t;
                }
                else
                {
                    if (task.__IsNew == false)
                    {
                        if (task.Type == ScheduleJsonType.Task.ToString())
                        {
                            task.Task = UpdateNormalTask(date, tz, userId, status, task, bucketId);
                        }
                        else
                        {
                            // ok its a task from another roster
                            task.Task = AcceptTaskFromAnotherRoster(task, rosterId, bucketId, userId, processedByUserId, status);
                        }
                    }
                    else
                    {
                        // its a new task created on the client
                        var newTask = _taskRepo.New(task.Name, userId, rosterId, task.TaskTypeId, bucketId, _memberRepo, null, task.SiteId, ProjectJobTaskStatus.Approved);

                        newTask.Description = task.Description;
                        newTask.Level = task.Level;
                        newTask.RoleId = task.RoleId;
                        if (string.IsNullOrEmpty(task.StartTime) == false)
                        {
                            var localStartTime = date.Date.Add(TimeSpan.Parse(task.StartTime));
                            var utcStartTime = TimeZoneInfo.ConvertTimeToUtc(localStartTime, tz);

                            newTask.StartTimeUtc = new DateTimeOffset(localStartTime, tz.GetUtcOffset(utcStartTime));
                        }

                        foreach (var trans in task.Translated)
                        {
                            var t = (from s in newTask.Translated where s.CultureName == trans.CultureName select s);
                            ProjectJobTaskTranslatedDTO tran = null;
                            if (t.Any() == true)
                            {
                                tran = t.First();
                                tran.Name = trans.Name;
                                tran.Description = trans.Description;
                            }
                            else
                            {
                                tran = new ProjectJobTaskTranslatedDTO()
                                {
                                    __IsNew = true,
                                    Id = newTask.Id,
                                    CultureName = trans.CultureName,
                                    Name = trans.Name,
                                    Description = trans.Description
                                };

                                newTask.Translated.Add(tran);
                            }
                        }

                        _taskRepo.Save(newTask, true, true);

                        task.Task = newTask;
                    }
                }
            }
        }

        protected void CreateUnassignedSchedule(HierarchyBucketDTO node, RosterDTO roster, IList<SlotJson> slots, DayOfWeek day, DateTime localDate, TimeZoneInfo timezone, bool includeWorkloading)
        {
            SlotJson slot = new SlotJson();
            slot.Name = "Unassigned";
            slot.Id = node.Id;
            slot.Role = node.HierarchyRole.Name;
            slot.RoleId = node.RoleId;
            slot.UserId = null;
            slot.AssignsTasksThroughLabels = node.AssignTasksThroughLabels;

            slots.Add(slot);

            List<ScheduleJson> tasks = new List<ScheduleJson>();

            if (roster.ScheduleApprovalDefaultTaskStatus == (int)ScheduleApprovalDefaultStatus.Unapproved)
            {
                slot.UnassignedTasks = tasks;
                slot.AssignedTasks = new List<ScheduleJson>();
            }
            else
            {
                slot.UnassignedTasks = new List<ScheduleJson>();
                slot.AssignedTasks = tasks;
            }

            var schedules = _scheduleRepo.GetForRoster(roster.Id, new int[] { node.RoleId },
                RepositoryInterfaces.Enums.ProjectJobScheduledTaskLoadInstructions.CalendarRule |
                ProjectJobScheduledTaskLoadInstructions.AllTranslated |
                ProjectJobScheduledTaskLoadInstructions.TaskType |
                ProjectJobScheduledTaskLoadInstructions.Users);

            switch (day)
            {
                case DayOfWeek.Sunday:
                    tasks.AddRange((from t in schedules where t.CalendarRule.ExecuteSunday == true select t).ToList().Select(s => ScheduleToJson(s, node, null, localDate, timezone, includeWorkloading == false ? null : _activityFactory)));
                    break;
                case DayOfWeek.Monday:
                    tasks.AddRange((from t in schedules where t.CalendarRule.ExecuteMonday == true select t).ToList().Select(s => ScheduleToJson(s, node, null, localDate, timezone, includeWorkloading == false ? null : _activityFactory)));
                    break;
                case DayOfWeek.Tuesday:
                    tasks.AddRange((from t in schedules where t.CalendarRule.ExecuteTuesday == true select t).ToList().Select(s => ScheduleToJson(s, node, null, localDate, timezone, includeWorkloading == false ? null : _activityFactory)));
                    break;
                case DayOfWeek.Wednesday:
                    tasks.AddRange((from t in schedules where t.CalendarRule.ExecuteWednesday == true select t).ToList().Select(s => ScheduleToJson(s, node, null, localDate, timezone, includeWorkloading == false ? null : _activityFactory)));
                    break;
                case DayOfWeek.Thursday:
                    tasks.AddRange((from t in schedules where t.CalendarRule.ExecuteThursday == true select t).ToList().Select(s => ScheduleToJson(s, node, null, localDate, timezone, includeWorkloading == false ? null : _activityFactory)));
                    break;
                case DayOfWeek.Friday:
                    tasks.AddRange((from t in schedules where t.CalendarRule.ExecuteFriday == true select t).ToList().Select(s => ScheduleToJson(s, node, null, localDate, timezone, includeWorkloading == false ? null : _activityFactory)));
                    break;
                case DayOfWeek.Saturday:
                    tasks.AddRange((from t in schedules where t.CalendarRule.ExecuteSaturday == true select t).ToList().Select(s => ScheduleToJson(s, node, null, localDate, timezone, includeWorkloading == false ? null : _activityFactory)));
                    break;
            }
        }

        protected void CreateAssignedSchedule(HierarchyBucketDTO node, RosterDTO roster, IList<SlotJson> slots, DateTime date, TimeZoneInfo tz, bool includeWorkloading)
        {
            SlotJson slot = new SlotJson();

            slot.Id = node.Id;
            slot.Role = node.HierarchyRole.Name;
            slot.RoleId = node.RoleId;
            slot.AssignsTasksThroughLabels = node.AssignTasksThroughLabels;

            // ok, so there is an opportunity on each day to override the bucket that does the work on that day. This caters to RDO's
            // where 2 or more people actually occupy what the facility classes as a single role.
            var day = (from d in roster.ProjectJobScheduleDays where d.RoleId == node.RoleId && d.Day == (int)date.DayOfWeek select d);

            if (day.Count() == 0 || day.First().OverrideRoleId.HasValue == false)
            {
                // no override for the role, so the node we have is all we need at this point
                SetSlotUsers(node, slot);
            }
            else
            {
                // so there is another role that is defined to override the node on the current day, we are going to have to load that bucket up
                HierarchyBucketDTO over = null;

                string overKey = roster.Id.ToString() + "_" + day.First().OverrideRoleId.ToString();

                if (_bucketCache.TryGetValue(overKey, out over) == false)
                {
                    var overs = _hierarchyRepo.GetBucketsForHierarchy(roster.HierarchyId, day.First().OverrideRoleId, HierarchyBucketLoadInstructions.User | HierarchyBucketLoadInstructions.Overrides | HierarchyBucketLoadInstructions.OverridesUsers);

                    if (overs.Count == 0)
                    {
                        over = node; // we'll just go with no override
                    }
                    else
                    {
                        over = overs.First();
                    }
                }

                SetSlotUsers(over, slot);
            }

            if (slot.UserId.HasValue == false)
            {
                CreateUnassignedSchedule(node, roster, slots, date.DayOfWeek, date, tz, includeWorkloading);
                return;
            }

            slots.Add(slot);

            List<ScheduleJson> unassignedTasks = new List<ScheduleJson>();
            List<ScheduleJson> assignedTasks = new List<ScheduleJson>();
            List<ScheduleJson> cancelledTasks = new List<ScheduleJson>();
            slot.UnassignedTasks = unassignedTasks;
            slot.AssignedTasks = assignedTasks;
            slot.CancelledTasks = cancelledTasks;

            var user = _memberRepo.GetById(slot.UserId.Value);

            // if its finished or marked to go to another roster, we aren't interested in it.
            var liveTasks = _taskRepo.GetForUser(slot.UserId.Value, true, false,
                TaskStatusFilter.Approved |
                TaskStatusFilter.ApprovedContinued |
                TaskStatusFilter.ChangeRosterRejected |
                TaskStatusFilter.Paused |
                TaskStatusFilter.Started |
                TaskStatusFilter.Unapproved,
                ProjectJobTaskLoadInstructions.AllTranslated |
                ProjectJobTaskLoadInstructions.TaskType |
                ProjectJobTaskLoadInstructions.Users, node.Id);
            // we have to do cancelled tasks seperately as they're not live, so the above get doesn't pick these out
            var cancelTasks = _taskRepo.GetForUser(slot.UserId.Value, date, tz, ProjectJobTaskStatus.Cancelled,
                ProjectJobTaskLoadInstructions.AllTranslated |
                ProjectJobTaskLoadInstructions.TaskType |
                ProjectJobTaskLoadInstructions.Users, node.Id);
            var scheduled = _scheduleRepo.GetForUser(slot.UserId.Value, node.RoleId, roster.Id, date, tz, false, true,
                ProjectJobScheduledTaskLoadInstructions.AllTranslated |
                ProjectJobScheduledTaskLoadInstructions.TaskType |
                ProjectJobScheduledTaskLoadInstructions.Users |
                ProjectJobScheduledTaskLoadInstructions.WorkloadingActivities);

            var ud = TimeZoneInfo.ConvertTimeToUtc(date, tz);
            var ud1 = ud.AddDays(1);

            // for a task to make it into the assigned list it needs to be approved on the date, otherwise it goes in the unapproved list
            assignedTasks.AddRange((from l in liveTasks where (l.StatusDate >= ud && l.StatusDate <= ud1) && l.Status != (int)ProjectJobTaskStatus.Unapproved && l.Status != (int)ProjectJobTaskStatus.Cancelled select l).Select(t => TaskToJson(t, date, includeWorkloading)));
            cancelledTasks.AddRange(cancelTasks.Select(c => TaskToJson(c, date, includeWorkloading)));
            // unapproved is similar to approved, just the status has to be unapproved
            unassignedTasks.AddRange((from l in liveTasks where l.Status == (int)ProjectJobTaskStatus.Unapproved && (l.StatusDate >= ud && l.StatusDate < ud1) select l).Select(c => TaskToJson(c, date, includeWorkloading)));

            // if its not in the assigned tasks or unassigned tasks list by now, then we want it, we just
            // aren't quite sure where to put it, so create a query for it
            var unapprovedQuery = (from l in liveTasks
                                   where
                                       (from a in assignedTasks select a.Id).Contains(l.Id) == false &&
                                       (from u in unassignedTasks select u.Id).Contains(l.Id) == false
                                   select l);

            if (roster.ScheduleApprovalDefaultTaskStatus == (int)ScheduleApprovalDefaultStatus.Unapproved)
            {
                // by default unapproved
                unassignedTasks.AddRange(unapprovedQuery.Select(t => TaskToJson(t, date, includeWorkloading)));
                unassignedTasks.AddRange(scheduled.Select(s => ScheduleToJson(s, node, slot.UserId, date, tz, includeWorkloading == false ? null : _activityFactory)));
            }
            else
            {
                // by default these guys are approved
                assignedTasks.AddRange(unapprovedQuery.Select(t => TaskToJson(t, date, includeWorkloading)));
                assignedTasks.AddRange(scheduled.Select(s => ScheduleToJson(s, node, slot.UserId, date, tz, includeWorkloading == false ? null : _activityFactory)));
            }
        }

        private static void SetSlotUsers(HierarchyBucketDTO node, SlotJson slot)
        {
            if (node.HierarchyBucketOverrides.Count == 0)
            {
                slot.UserId = node.UserId;
                slot.Name = node.UserData.Name;
            }
            else
            {
                // the bucket has overrides that are valid for it, so we use the latest one of those (which is the last one)
                var uId = node.HierarchyBucketOverrides[node.HierarchyBucketOverrides.Count - 1].UserId;
                slot.UserId = uId;

                if (uId.HasValue)
                {
                    slot.Name = node.HierarchyBucketOverrides[node.HierarchyBucketOverrides.Count - 1].UserData.Name;
                }

                slot.OverriddenUserId = node.UserId;
                slot.OverriddenUserName = node.UserData.Name;
            }
        }

        protected ScheduleJson ScheduleToJson(ProjectJobScheduledTaskDTO schedule, HierarchyBucketDTO bucket, Guid? userId, DateTime localDate, TimeZoneInfo timezone, IWorkloadingActivityFactory activityFactory)
        {
            ScheduleJson result = new ScheduleJson();

            result.FromDto(schedule, userId, localDate, timezone, activityFactory, _durationCalculator);
            result.Bucket = bucket;

            return result;
        }

        protected ScheduleJson TaskToJson(ProjectJobTaskDTO task, DateTime compareDate, bool includeWorkloading)
        {
            ScheduleJson result = new ScheduleJson();

            result.FromDto(task, compareDate, includeWorkloading);

            result.CanEdit = !task.ProjectJobScheduledTaskId.HasValue;

            return result;
        }

        protected ProjectJobTaskDTO UpdateNormalTask(DateTime date, TimeZoneInfo tz, Guid userId, ProjectJobTaskStatus status, ScheduleJson task, int bucketId)
        {
            // its already a task, so the only thing we need to do here is to make sure its status on today in 
            // the db is correct                        
            var existingTask = _taskRepo.GetById(task.Id, ProjectJobTaskLoadInstructions.AllTranslated);

            if (existingTask.Status == (int)ProjectJobTaskStatus.FinishedComplete ||
                existingTask.Status == (int)ProjectJobTaskStatus.FinishedIncomplete ||
                existingTask.Status == (int)ProjectJobTaskStatus.FinishedByManagement ||
                existingTask.Status == (int)ProjectJobTaskStatus.FinishedBySystem)
            {
                // if the person out in the field has finished it then we can't touch it
                return existingTask;
            }

            existingTask.Name = task.Name;
            existingTask.Description = task.Description;
            existingTask.TaskTypeId = task.TaskTypeId;
            existingTask.SiteId = task.SiteId;
            existingTask.Level = task.Level;
            existingTask.Status = (int)status;
            existingTask.StatusDate = DateTime.UtcNow;
            existingTask.DateCompleted = null;
            existingTask.HierarchyBucketId = bucketId;
            if (string.IsNullOrEmpty(task.StartTime) == false)
            {
                var localStartTime = date.Date.Add(TimeSpan.Parse(task.StartTime));
                var utcStartTime = TimeZoneInfo.ConvertTimeToUtc(localStartTime, tz);

                existingTask.StartTimeUtc = new DateTimeOffset(localStartTime, tz.GetUtcOffset(utcStartTime));
            }

            if (userId != existingTask.OwnerId)
            {
                existingTask.OwnerId = userId;
            }

            // translation stuff
            foreach (var trans in task.Translated)
            {
                var t = (from s in existingTask.Translated where s.CultureName == trans.CultureName select s);
                ProjectJobTaskTranslatedDTO tran = null;
                if (t.Any() == true)
                {
                    tran = t.First();
                    tran.Name = trans.Name;
                    tran.Description = trans.Description;
                }
                else
                {
                    tran = new ProjectJobTaskTranslatedDTO()
                    {
                        __IsNew = true,
                        Id = existingTask.Id,
                        CultureName = trans.CultureName,
                        Name = trans.Name,
                        Description = trans.Description
                    };

                    existingTask.Translated.Add(tran);
                }
            }

            if (status == ProjectJobTaskStatus.Cancelled)
            {
                existingTask.DateCompleted = DateTime.UtcNow;
            }

            _taskRepo.Save(existingTask, true, true);

            return existingTask;
        }

        protected ProjectJobTaskDTO AcceptTaskFromAnotherRoster(ScheduleJson task, int rosterId, int bucketId, Guid userId, Guid processedByUserId, ProjectJobTaskStatus status)
        {
            // we are going to do a few things here. Firstly we are going to mark the original task as being ChangeRosterApproved
            // so that it can act as a marker for the end of life of the task for the person who previously had it. 
            // We are then going to mark the change request off as having been handled.
            // We are then going to create a copy of that task and assign that as a spot task (so it won't be tied to a schedule) to who ever its been assigned to.

            // so first things first, lets get the existing task and mark it off has having been accepted
            var changes = _changeRequestRepo.GetOpenRequestsByTaskId(task.Id, ProjectJobTaskChangeRequestLoadInstructions.None);

            if (changes.Count == 0)
            {
                // this shouldn't happen
                throw new InvalidOperationException("The change request does not exist");
            }

            var existingTask = _taskRepo.GetById(task.Id, ProjectJobTaskLoadInstructions.None);

            Guid groupId = Guid.Empty;
            if (existingTask.ProjectJobTaskGroupId.HasValue)
            {
                groupId = existingTask.ProjectJobTaskGroupId.Value;
            }
            else
            {
                groupId = CombFactory.NewComb();

                existingTask.ProjectJobTaskGroupId = groupId;
            }

            existingTask.Status = (int)ProjectJobTaskStatus.ChangeRosterAccepted;
            existingTask.StatusDate = DateTime.UtcNow;

            changes.First().Status = (int)ProjectJobTaskChangeRequestStatus.Accepted;
            changes.First().ProcessedByUserId = processedByUserId;

            _taskRepo.Save(existingTask, false, false);
            _changeRequestRepo.Save(changes.First(), false, false);

            // now lets create a new task, which is essentially a copy of the one we've just marked as being accepted
            // only in the new roster and against the new person
            var newTask = _taskRepo.New(existingTask.Name, userId, rosterId, existingTask.TaskTypeId, bucketId, _memberRepo, null, task.SiteId, status, groupId);

            newTask.Description = existingTask.Description;
            newTask.Level = existingTask.Level;

            _taskRepo.Save(newTask, true, false);

            return newTask;
        }
    }
}
