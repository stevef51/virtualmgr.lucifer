﻿using ConceptCave.Checklist.Interfaces;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.RepositoryInterfaces.ScheduleManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualMgr.Common;

namespace ConceptCave.BusinessLogic.ScheduleManagement
{
    public class ScheduleManagementBase : IScheduleManagement
    {
        protected IRosterRepository _rosterRepo;
        private readonly IDateTimeProvider _dateTimeProvider;

        public ScheduleManagementBase(IRosterRepository rosterRepo, IDateTimeProvider dateTimeProvider)
        {
            _rosterRepo = rosterRepo;
            _dateTimeProvider = dateTimeProvider;
        }

        protected virtual RosterDTO LoadAndCacheRoster(int rosterId, IDictionary<int, RosterDTO> rosterCache)
        {
            RosterDTO roster = null;

            if (rosterCache != null && rosterCache.ContainsKey(rosterId))
            {
                roster = rosterCache[rosterId];
            }
            else
            {
                roster = _rosterRepo.GetById(rosterId);

                if (rosterCache != null)
                {
                    rosterCache.Add(rosterId, roster);
                }
            }

            return roster;
        }

        public virtual DateTime GetStartOfCurrentShiftInTimezone(int rosterId, TimeZoneInfo tz)
        {
            return GetStartOfCurrentShiftInTimezone(rosterId, tz, null);
        }

        public virtual DateTime GetStartOfCurrentShiftInTimezone(int rosterId, TimeZoneInfo tz, IDictionary<int, RosterDTO> rosterCache)
        {
            var roster = LoadAndCacheRoster(rosterId, rosterCache);

            return GetStartOfCurrentShiftInTimezone(roster, tz);
        }

        public virtual DateTime GetStartOfCurrentShiftInTimezone(RosterDTO roster, TimeZoneInfo tz)
        {
            // The Roster is ALWAYS scheduled in its own TimeZone, we are going to calculate that datetime in the Rosters timezone and then convert it to the requested timezone (which is probably UTC)
            var rosterTZ = TimeZoneInfoResolver.ResolveWindows(roster.Timezone);

            // the current time in the timezone requested
            var startTimeRoster = TimeZoneInfo.ConvertTimeFromUtc(_dateTimeProvider.UtcNow, rosterTZ);

            return GetStartOfShiftInTimezone(roster, startTimeRoster, rosterTZ, tz);
        }

        public virtual DateTime GetDSTSafeStartTime(RosterDTO roster, DateTime dateofShiftInRosterTimezone, TimeZoneInfo rosterTZ)
        {
            var compareTime = new DateTime(dateofShiftInRosterTimezone.Year, dateofShiftInRosterTimezone.Month, dateofShiftInRosterTimezone.Day, roster.StartTime.Hour, roster.StartTime.Minute, roster.StartTime.Second, DateTimeKind.Unspecified);

            // Adjust for DST Invalid time
            if (rosterTZ.IsInvalidTime(compareTime))
            {
                compareTime = compareTime.AddHours(1);
            }

            return compareTime;
        }

        public virtual DateTime GetStartOfShiftInTimezone(RosterDTO roster, DateTime dateOfShiftInRosterTimezone, TimeZoneInfo rosterTZ, TimeZoneInfo outputTZ)
        {
            var startTimeRoster = dateOfShiftInRosterTimezone;
            // so let's lay the timeline out as follows
            // 8AM Mon ----------------- Midnight ---------------- 8AM Tue
            // Where 8AM Mon is the start and 8AM Tue is the start of the roster the next day
            // So if localTime is > start then its on that day
            // if localTime is < start then its on the previous day
            // Note, we aren't taking the roster end time into account here intentionally as
            // processes may occur outside the operational times for the roster, for example
            // the automated end of day will want to run after the days work has been completed.
            // So I'm assuming a day as the timeframe rather than the roster's time span.

            // lets find out where things fall, to do this we get the roster start time on the day of local time,
            // we can then just compare the dates simply.
            var compareTime = GetDSTSafeStartTime(roster, startTimeRoster, rosterTZ);

            if (startTimeRoster < compareTime)
            {
                // so its before the roster starts, therefore we need to go back a day
                startTimeRoster = startTimeRoster.AddHours(-24);
            }

            // so the start of the roster is
            startTimeRoster = new DateTime(startTimeRoster.Year, startTimeRoster.Month, startTimeRoster.Day, roster.StartTime.Hour, roster.StartTime.Minute, roster.StartTime.Second, DateTimeKind.Unspecified);

            // Adjust for DST Invalid time
            if (rosterTZ.IsInvalidTime(startTimeRoster))
            {
                startTimeRoster = startTimeRoster.AddHours(1);
            }

            // Convert to UTC and then to requested timezone
            var startTimeUtc = TimeZoneInfo.ConvertTimeToUtc(startTimeRoster, rosterTZ);
            var tzTime = TimeZoneInfo.ConvertTimeFromUtc(startTimeUtc, outputTZ);

            return tzTime;
        }

        public virtual DateTime GetEndOfCurrentShiftInTimezone(int rosterId, TimeZoneInfo tz)
        {
            return GetEndOfCurrentShiftInTimezone(rosterId, tz, null);
        }

        public virtual DateTime GetEndOfCurrentShiftInTimezone(int rosterId, TimeZoneInfo tz, IDictionary<int, RosterDTO> rosterCache)
        {
            var roster = LoadAndCacheRoster(rosterId, rosterCache);

            return GetEndOfCurrentShiftInTimezone(roster, tz);
        }

        public virtual DateTime GetEndOfCurrentShiftInTimezone(RosterDTO roster, TimeZoneInfo tz)
        {
            // The Roster is ALWAYS scheduled in its own TimeZone, we are going to calculate that datetime in the Rosters timezone and then convert it to the requested timezone (which is probably UTC)
            var rosterTZ = TimeZoneInfoResolver.ResolveWindows(roster.Timezone);

            DateTime startTimeRoster = GetStartOfCurrentShiftInTimezone(roster, rosterTZ);

            return GetEndOfShiftInTimezone(roster, startTimeRoster, rosterTZ, tz);
        }

        public virtual DateTime GetEndOfShiftInTimezone(RosterDTO roster, DateTime startOfShiftInRosterTimezone, TimeZoneInfo rosterTZ, TimeZoneInfo outputTZ)
        {
            // so we now have the start
            DateTime endTimeRoster = new DateTime(startOfShiftInRosterTimezone.Year, startOfShiftInRosterTimezone.Month, startOfShiftInRosterTimezone.Day, roster.EndTime.Hour, roster.EndTime.Minute, roster.EndTime.Second, startOfShiftInRosterTimezone.Kind);

            // Adjust for DST Invalid time
            if (rosterTZ.IsInvalidTime(endTimeRoster))
            {
                endTimeRoster = endTimeRoster.AddHours(1);
            }

            // if the shift spans midnight, then our end will be before our start, lets get that sorted
            if (endTimeRoster < startOfShiftInRosterTimezone)
            {
                endTimeRoster = endTimeRoster.AddDays(1);
            }

            // Adjust for DST Invalid time
            if (rosterTZ.IsInvalidTime(endTimeRoster))
            {
                endTimeRoster = endTimeRoster.AddHours(1);
            }

            // Convert to UTC and then to requested timezone
            var endTimeUtc = TimeZoneInfo.ConvertTimeToUtc(endTimeRoster, rosterTZ);
            var tzTime = TimeZoneInfo.ConvertTimeFromUtc(endTimeUtc, outputTZ);

            return tzTime;
        }
    }
}
