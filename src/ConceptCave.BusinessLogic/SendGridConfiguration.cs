﻿using ConceptCave.Checklist.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using ConceptCave.RepositoryInterfaces.Repositories;

namespace ConceptCave.BusinessLogic
{
    public class SendGridConfiguration : ISendGridConfiguration
    {
        protected IGlobalSettingsRepository _settingRepo;

        public SendGridConfiguration(IGlobalSettingsRepository settingRepo)
        {
            _settingRepo = settingRepo;
        }

        public void AddXsmtpApiHeader(MailMessage message)
        {
            var templateId = _settingRepo.GetSetting<Guid>("SendGrid.DefaultTemplateId");

            if(templateId == Guid.Empty)
            {
                if(System.Configuration.ConfigurationManager.AppSettings["SendGrid.DefaultTemplateId"] != null)
                {
                    if(Guid.TryParse(System.Configuration.ConfigurationManager.AppSettings["SendGrid.DefaultTemplateId"], out templateId) == false)
                    {
                        return;
                    }
                }
                else
                {
                    return;
                }
            }

            string json = "{\"filters\":{\"templates\":{\"settings\":{\"enable\":1,\"template_id\":\"" + templateId.ToString() + "\"}}}}";
            message.Headers.Add("X-SMTPAPI", json);
        }
    }
}
