﻿using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Workloading;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.BusinessLogic.Workloading
{
    public class WorkloadingStandard : IWorkloadingStandard
    {
        public WorkloadingStandard() { }

        public WorkloadingStandard(WorkLoadingStandardDTO dto)
        {
            Seconds = dto.Seconds;

            Measurement = new WorkloadingMeasurement()
            {
                Value = dto.Measure,
                Unit = new WorkloadingMeasurementUnit(dto.MeasurementUnit)
            };

            Data = dto;
        }

        public decimal Seconds { get; set; }

        public decimal SecondsPerSingleMetricUnit()
        {
            if(Measurement == null)
            {
                throw new InvalidOperationException("A workloading measurement must be supplied");
            }

            var metricValue = Measurement.AsMetric();

            if(metricValue == 0)
            {
                return 0; // not sure this is the correct thing to return here?
            }

            return Seconds / metricValue;
        }

        public IWorkloadingMeasurement Measurement { get; set; }

        public object Data { get; set; }
    }
}
