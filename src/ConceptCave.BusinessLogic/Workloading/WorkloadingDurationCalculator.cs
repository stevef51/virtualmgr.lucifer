﻿using ConceptCave.RepositoryInterfaces.Workloading;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.BusinessLogic.Workloading
{
    public class WorkloadingDurationCalculator : IWorkloadingDurationCalculator
    {
        public void EstimateDuration(IWorkloadingActivity activity)
        {
            if(activity.Standard == null || activity.FeatureMeasurement == null)
            {
                activity.EstimatedDuration = 0;
                return;
            }

            // so we have a standard and we have a site measurement, let's
            // get both the site measurment and the standard in metric units so
            // we know we can work with their values together
            var siteMetricSiteMeasurmentValue = activity.FeatureMeasurement.AsMetric();
            var metricSeconds = activity.Standard.SecondsPerSingleMetricUnit();

            // so the estimated duration is the size of the task * how long it takes
            // to do each unit measurement of the task.
            activity.EstimatedDuration = metricSeconds * siteMetricSiteMeasurmentValue;
        }

        public void EstimateDuration(IList<IWorkloadingActivity> activities)
        {
            foreach(var activity in activities)
            {
                EstimateDuration(activity);
            }
        }

        public void EstimateDuration(IWorkloadingTask task)
        {
            if(task.Activities == null)
            {
                return;
            }

            foreach(var activity in task.Activities)
            {
                EstimateDuration(activity);
            }

            task.EstimatedDuration = (from a in task.Activities select a.EstimatedDuration).Sum();
        }
    }
}
