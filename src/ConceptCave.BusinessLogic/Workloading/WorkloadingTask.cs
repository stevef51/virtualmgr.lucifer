﻿using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Workloading;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.BusinessLogic.Workloading
{
    public class WorkloadingTask : IWorkloadingTask
    {
        public WorkloadingTask() 
        {
            Activities = new List<IWorkloadingActivity>();
        }

        public object Data { get; set; }

        public IList<IWorkloadingActivity> Activities { get; set; }

        public decimal EstimatedDuration { get; set; }
    }
}
