﻿using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.RepositoryInterfaces.Workloading;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.BusinessLogic.Workloading
{
    public class WorkloadingActivityFactory : IWorkloadingActivityFactory
    {
        protected IWorkLoadingBookRepository _bookRepo;
        protected IMembershipRepository _memberRepo;
        protected ISiteFeatureRepository _siteFeatureRepo;

        protected Dictionary<Guid, WorkLoadingStandardDTO> _standardsCache;
        protected Dictionary<Guid, UserDataDTO> _siteCache;

        public WorkloadingActivityFactory(IWorkLoadingBookRepository bookRepo,
            IMembershipRepository memberRepo,
            ISiteFeatureRepository siteFeatureRepo)
        {
            _bookRepo = bookRepo;
            _memberRepo = memberRepo;
            _siteFeatureRepo = siteFeatureRepo;

            _standardsCache = new Dictionary<Guid, WorkLoadingStandardDTO>();
            _siteCache = new Dictionary<Guid, UserDataDTO>();
        }

        public IWorkloadingActivity NewForFeature(Guid standardId, Guid siteFeatureId)
        {
            WorkLoadingStandardDTO standard = loadStandard(standardId);

            SiteFeatureDTO siteFeature = _siteFeatureRepo.GetFeatureById(siteFeatureId, RepositoryInterfaces.Enums.SiteFeatureLoadInstructions.Units);

            return new WorkloadingActivity(standard, siteFeature);
        }

        protected WorkLoadingStandardDTO loadStandard(Guid standardId)
        {
            WorkLoadingStandardDTO standard = null;

            if (_standardsCache.TryGetValue(standardId, out standard) == false)
            {
                standard = _bookRepo.GetStandardById(standardId, RepositoryInterfaces.Enums.WorkLoadingStandardLoadInstructions.Units);
                _standardsCache.Add(standardId, standard);
            }

            return standard;
        }

        public IList<IWorkloadingActivity> NewForSite(Guid standardId, Guid siteId)
        {
            WorkLoadingStandardDTO standard = loadStandard(standardId);

            UserDataDTO site = null;

            if(_siteCache.TryGetValue(siteId, out site) == false)
            {
                site = _memberRepo.GetById(siteId, RepositoryInterfaces.Enums.MembershipLoadInstructions.Features);
                _siteCache.Add(siteId, site);
            }

            List<IWorkloadingActivity> result = new List<IWorkloadingActivity>();

            var features = (from f in site.SiteFeatures where f.Id == standard.FeatureId select f);

            features.ToList().ForEach(f =>
            {
                result.Add(new WorkloadingActivity(standard, f));
            });

            return result;
        }

        public IList<IWorkloadingActivity> NewForSite(WorkLoadingStandardDTO standard, UserDataDTO site)
        {
            if(standard.MeasurementUnit == null)
            {
                throw new ArgumentException("Standard must have units loaded with it");
            }

            List<IWorkloadingActivity> result = new List<IWorkloadingActivity>();

            var features = (from f in site.SiteFeatures where f.WorkLoadingFeatureId == standard.FeatureId select f);

            features.ToList().ForEach(f =>
            {
                result.Add(new WorkloadingActivity(standard, f));
            });

            return result;
        }

        public bool ShouldCreate(ProjectJobScheduledTaskWorkloadingActivityDTO activity, DateTime localDate, TimeZoneInfo timezone)
        {
            var utcDate = TimeZoneInfo.ConvertTimeToUtc(localDate, timezone);
            var day = localDate.DayOfWeek;

            var dto = activity.ProjectJobScheduledTask;

            // to work out if this task should be added this time around we need to do a
            // bit of work. If the task repeats every 2 weeks and the activity within
            // it repeats every 2 instances of the task, that means the activity repeats
            // every 2 * 2 = 4 weeks.
            int totalFrequency = activity.Frequency * dto.CalendarRule.Period;

            if(totalFrequency != 1)
            {
                // we have to work out the date of the first occurence of the
                // dayOfWeek after calendarStartDate
                var daysUntilNextDayOfWeek = ((int)day - (int)dto.CalendarRule.StartDate.DayOfWeek + 7) % 7;
                var compareDate = dto.CalendarRule.StartDate.AddDays(daysUntilNextDayOfWeek);

                if ((utcDate.Date.Subtract(compareDate.ToUniversalTime().Date).TotalDays / 7) % totalFrequency != 0)
                {
                    return false;
                }
            }

            switch (day)
            {
                case DayOfWeek.Sunday:
                    if (activity.ExecuteSunday.HasValue == true && activity.ExecuteSunday.Value == false)
                    {
                        return false;
                    }
                    break;
                case DayOfWeek.Monday:
                    if (activity.ExecuteMonday.HasValue == true && activity.ExecuteMonday.Value == false)
                    {
                        return false;
                    }
                    break;
                case DayOfWeek.Tuesday:
                    if (activity.ExecuteTuesday.HasValue == true && activity.ExecuteTuesday.Value == false)
                    {
                        return false;
                    }
                    break;
                case DayOfWeek.Wednesday:
                    if (activity.ExecuteWednesday.HasValue == true && activity.ExecuteWednesday.Value == false)
                    {
                        return false;
                    }
                    break;
                case DayOfWeek.Thursday:
                    if (activity.ExecuteThursday.HasValue == true && activity.ExecuteThursday.Value == false)
                    {
                        return false;
                    }
                    break;
                case DayOfWeek.Friday:
                    if (activity.ExecuteFriday.HasValue == true && activity.ExecuteFriday.Value == false)
                    {
                        return false;
                    }
                    break;
                case DayOfWeek.Saturday:
                    if (activity.ExecuteSaturday.HasValue == true && activity.ExecuteSaturday.Value == false)
                    {
                        return false;
                    }
                    break;
            }

            return true;
        }
    }
}
