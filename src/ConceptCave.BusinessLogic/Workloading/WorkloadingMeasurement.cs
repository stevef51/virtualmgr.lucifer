﻿using ConceptCave.RepositoryInterfaces.Workloading;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.BusinessLogic.Workloading
{
    class WorkloadingMeasurement : IWorkloadingMeasurement
    {
        public decimal Value { get; set; }

        public decimal AsMetric()
        {
            if(Unit == null)
            {
                throw new InvalidOperationException("Metric conversion can not be done unless units are specified");
            }

            return Value * Unit.PerMetric;
        }

        public IWorkloadingMeasurementUnit Unit { get; set; }

        public object Data { get; set; }
    }
}
