﻿using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Workloading;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.BusinessLogic.Workloading
{
    public class WorkloadingActivity : IWorkloadingActivity
    {
        public WorkloadingActivity() { }

        public WorkloadingActivity(WorkLoadingStandardDTO standard, SiteFeatureDTO feature)
        {
            Standard = new WorkloadingStandard(standard);

            FeatureMeasurement = new WorkloadingMeasurement()
            {
                Value = feature.Measure,
                Unit = new WorkloadingMeasurementUnit(feature.MeasurementUnit),
                Data = feature
            };

            Data = standard;
        }

        public object Data { get; set; }

        public IWorkloadingMeasurement FeatureMeasurement { get; set; }

        public IWorkloadingStandard Standard { get; set; }

        public decimal EstimatedDuration { get; set; }
    }
}
