﻿using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Workloading;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.BusinessLogic.Workloading
{
    public class WorkloadingMeasurementUnit : IWorkloadingMeasurementUnit
    {
        public WorkloadingMeasurementUnit() { }

        public WorkloadingMeasurementUnit(MeasurementUnitDTO dto)
        {
            Name = dto.ShortName;
            PerMetric = dto.PerMetric;
            Dimensions = dto.Dimensions;
        }

        public string Name { get; set; }

        public decimal PerMetric { get; set; }

        public int Dimensions { get; set; }
    }
}
