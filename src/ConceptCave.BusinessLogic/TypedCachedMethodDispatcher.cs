﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using ConceptCave.Checklist.Interfaces;

namespace ConceptCave.BusinessLogic
{
    internal class HandlesTypeAttribute : Attribute
    {
        public Type SourceType { get; set; }

        public HandlesTypeAttribute(Type sourceType)
        {
            SourceType = sourceType;
        }
    }

    public abstract class TypedCachedMethodDispatcher<TMethod> //where TMethod : Delegate, but we can't actually write that
    {
        protected readonly IDictionary<Type, TMethod> MethodMappings = new ConcurrentDictionary<Type, TMethod>();
        protected readonly bool ShouldCache;

        //see http://stackoverflow.com/questions/191940/c-sharp-generics-wont-allow-delegate-type-constraints
        static TypedCachedMethodDispatcher() //static constructor called once per TMethod
        {
            if (!typeof(TMethod).IsSubclassOf(typeof(Delegate)))
            {
                throw new InvalidOperationException(string.Format("Type {0} is not a delegate type",
                                                                  typeof(TMethod).Name));
            }
        }

        protected TypedCachedMethodDispatcher(bool shouldCache = true)
        {
            ShouldCache = shouldCache;
        }

        protected TMethod GetMethod(Type handlesType)
        {
            TMethod retDelegate;
            if (!ShouldCache || !MethodMappings.TryGetValue(handlesType, out retDelegate))
            {
                retDelegate = GetMethodByReflection(handlesType);
                if (ShouldCache)
                    MethodMappings[handlesType] = retDelegate;
            }
            return retDelegate;
        }

        private TMethod GetMethodByReflection(Type handlesType)
        {
            //reflect our class looking for methods with the correct attribute
            var thisType = this.GetType();
            foreach (var method in thisType.GetMethods(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance))
            {
                var attrs = method.GetCustomAttributes(typeof(HandlesTypeAttribute), true);
                foreach (var attr in attrs.Cast<HandlesTypeAttribute>())
                {
                    if (attr.SourceType.Equals(handlesType))
                    {
                        //make a delegate out of the method info
                        return ConstructDelegate(method);
                    }
                }
            }
            throw new NotImplementedException(string.Format("No handler for type {0}", handlesType.Name));
        }

        //Subclasses need to override this and call a strongly cast version of Delegate.CreateDelegate
        protected abstract TMethod ConstructDelegate(MethodInfo method);
    }
}