﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.BusinessLogic.LingoDataObjects;
using ConceptCave.Checklist.Facilities;
using ConceptCave.Core;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;

namespace ConceptCave.BusinessLogic.Facilities
{
    public class HierarchyFacility : Facility
    {
        private IMembershipRepository membershipRepo;
        private IHierarchyRepository hierachyRepo;

        public class HierachyFacilityBucketItem : Node
        {
            private IMembershipRepository membershipRepo;
            private IHierarchyRepository hierarchyRepo;

            /// <summary>
            /// Parent of this node, null if its the root node
            /// </summary>
            public HierachyFacilityBucketItem Parent { get; set; }

            /// <summary>
            /// List of children for the node
            /// </summary>
            public List<HierachyFacilityBucketItem> Children { get; protected set; }

            /// <summary>
            /// The role that the bucket item accepts. This could be a role id or a string containing the role name
            /// </summary>
            public object Role { get; set; }

            /// <summary>
            /// The user that is mapped to this bucket item. This could be a user id, user facility helper object or a string representing the username
            /// </summary>
            public object User { get; set; }

            public bool IsPrimary { get; set; }

            public new int? Id { get; set; }

            public int HierarchyId { get; set; }

            public string Name { get; set; }
            public HierachyFacilityBucketItem(IMembershipRepository memberRepo, IHierarchyRepository hierRepo)
                : base()
            {
                membershipRepo = memberRepo;
                hierarchyRepo = hierRepo;
                Children = new List<HierachyFacilityBucketItem>();
            }

            public HierachyFacilityBucketItem NewChild()
            {
                HierachyFacilityBucketItem result = new HierachyFacilityBucketItem(membershipRepo, hierarchyRepo) { Parent = this };
                this.Children.Add(result);

                return result;
            }

            public override void Encode(Core.Coding.IEncoder encoder)
            {
                base.Encode(encoder);

                encoder.Encode<HierachyFacilityBucketItem>("Parent", Parent);
                encoder.Encode<List<HierachyFacilityBucketItem>>("Children", Children);
                encoder.Encode<object>("Role", Role);
                encoder.Encode<object>("User", User);
                encoder.Encode<bool>("IsPrimary", IsPrimary);
                encoder.Encode<int?>("BucketId", Id);
                encoder.Encode<int>("HierarchyId", HierarchyId);
                encoder.Encode<string>("Name", Name);
            }

            public override void Decode(Core.Coding.IDecoder decoder)
            {
                base.Decode(decoder);

                Parent = decoder.Decode<HierachyFacilityBucketItem>("Parent", null);
                Children = decoder.Decode<List<HierachyFacilityBucketItem>>("Children", new List<HierachyFacilityBucketItem>());
                Role = decoder.Decode<object>("Role", null);
                User = decoder.Decode<object>("User", null);
                IsPrimary = decoder.Decode<bool>("IsPrimary", false);
                Id = decoder.Decode<int?>("BucketId");
                HierarchyId = decoder.Decode<int>("HierarchyId");
                Name = decoder.Decode<string>("Name");
            }

            public void LoadFrom(HierarchyBucketDTO item, HierachyFacilityBucketItem parent)
            {
                this.Parent = parent;
                this.Role = item.RoleId;
                this.User = item.UserId;
                this.IsPrimary = item.IsPrimary.HasValue ? item.IsPrimary.Value : false;
                this.Id = item.Id;
                this.HierarchyId = item.HierarchyId;
                this.Name = item.Name;

                foreach (var child in item.Children)
                {
                    var c = new HierachyFacilityBucketItem(membershipRepo, hierarchyRepo);
                    this.Children.Add(c);
                    c.LoadFrom(child, this);
                }
            }

            public HierarchyBucketDTO BuildHierachy(HierarchyBucketDTO parent, Dictionary<string, int> roleCache, Dictionary<string, Guid> membershipCache)
            {
                HierarchyBucketDTO result = new HierarchyBucketDTO()
                {
                    __IsNew = this.Id.HasValue == false,
                    Name = this.Name ?? "",
                    IsPrimary = IsPrimary,
                    HierarchyId = this.HierarchyId
                };

                if (this.Id.HasValue == true)
                {
                    result.Id = this.Id.Value;
                }

                if (parent != null)
                {
                    parent.Children.Add(result);
                }

                if (roleCache == null)
                {
                    roleCache = new Dictionary<string, int>();
                }

                if (membershipCache == null)
                {
                    membershipCache = new Dictionary<string, Guid>();
                }

                int roleId = -1;
                if (Role is int)
                {
                    roleId = (int)Role;
                }
                else if (Role is string)
                {
                    int r = -1;
                    if (roleCache.TryGetValue((string)Role, out r) == false)
                    {
                        var loadedRole = hierarchyRepo.GetRolesByName((string)Role);

                        if (loadedRole.Count > 0)
                        {
                            r = loadedRole[0].Id;

                            roleCache.Add(loadedRole[0].Name, loadedRole[0].Id);
                        }
                    }

                    if (r != -1)
                    {
                        roleId = r;
                    }
                }

                if (roleId != -1)
                {
                    result.RoleId = roleId;
                }

                Guid userId = Guid.Empty;
                if (User is Guid)
                {
                    userId = (Guid)User;
                }
                else if (User is string)
                {
                    Guid u = Guid.Empty;
                    if (membershipCache.TryGetValue((string)User, out u) == false)
                    {
                        var loadedUser = membershipRepo.GetByUsername((string)User, MembershipLoadInstructions.None);

                        if (loadedUser != null)
                        {
                            userId = loadedUser.UserId;

                            membershipCache.Add((string)User, userId);
                        }
                    }
                }
                else if (User is FacilityUserHelper)
                {
                    userId = ((FacilityUserHelper)User).Id;
                }

                if (userId != Guid.Empty)
                {
                    result.UserId = userId;
                }

                foreach (var child in Children)
                {
                    child.BuildHierachy(result, roleCache, membershipCache);
                }

                return result;
            }

            public HierachyFacilityBucketItem GetParentDeep(int depth)
            {
                var parent = this.Parent;
                depth--;
                while (parent != null && depth > 0)
                {
                    parent = parent.Parent;
                    depth--;
                }
                if (depth > 0)
                {
                    return null;
                }
                return parent;
            }

            public HierachyFacilityBucketItem GetParentWithRole(object parentRole)
            {
                HierarchyRoleDTO parentRoleDto = null;
                if (parentRole is string)
                {
                    parentRoleDto = this.hierarchyRepo.GetRoleByName((string)parentRole);
                }
                else if (parentRole is int)
                {
                    parentRoleDto = this.hierarchyRepo.GetRoleById((int)parentRole);
                }
                if (parentRoleDto == null)
                {
                    return null;
                }

                var parent = this.Parent;
                while (parent != null)
                {
                    if ((int)parent.Role == parentRoleDto.Id)
                    {
                        return parent;
                    }
                    parent = parent.Parent;
                }
                return null;
            }

            public List<HierachyFacilityBucketItem> FindChildrenForUser(object inuser)
            {
                Guid userId = Guid.Empty;

                if (inuser is Guid)
                {
                    userId = (Guid)inuser;
                }
                else if (inuser is string)
                {
                    Guid u = Guid.Empty;

                    var loadedUser = membershipRepo.GetByUsername((string)inuser, MembershipLoadInstructions.None);

                    if (loadedUser != null)
                    {
                        userId = loadedUser.UserId;

                    }
                }
                else if (inuser is FacilityUserHelper)
                {
                    userId = ((FacilityUserHelper)inuser).Id;
                }

                List<HierachyFacilityBucketItem> result = new List<HierachyFacilityBucketItem>();

                RecursiveFindUser(this, userId, result);

                return result;
            }

            protected void RecursiveFindUser(HierachyFacilityBucketItem item, Guid userId, List<HierachyFacilityBucketItem> result)
            {
                if (item.User is Guid && (Guid)item.User == userId)
                {
                    result.Add(item);
                }

                foreach (var i in item.Children)
                {
                    RecursiveFindUser(i, userId, result);
                }
            }

            public IEnumerable<HierachyFacilityBucketItem> Flattened()
            {
                yield return this;
                foreach (var child in Children)
                {
                    foreach (var flat in child.Flattened())
                    {
                        yield return flat;
                    }
                }
            }
        }

        public HierarchyFacility(IMembershipRepository memberRepo, IHierarchyRepository hierRepo)
        {
            membershipRepo = memberRepo;
            hierachyRepo = hierRepo;
            Name = "Hierarchy";
        }

        /// <summary>
        /// Method to get all of the direct descendants of the parent handed in. The method returns the membership id of the
        /// users in each of the roles. If a child role is empty, then that role is skipped.
        /// </summary>
        /// <param name="parentId">If a GUID then the method will assume its a membership ID. If an int then it assumes its the id of the bucket</param>
        /// <returns>List of membership ids</returns>
        public List<object> GetAllDirectDescendantMembershipIds(object parentId)
        {
            if (parentId == null)
            {
                return null;
            }

            int sourceId = -1;

            // we need to look up the bucket id, we do this assuming that its the primary role we are looking for
            if (parentId is Guid)
            {
                var parent = hierachyRepo.GetPrimaryEntity((Guid)parentId);

                if (parent == null)
                {
                    return null;
                }

                sourceId = parent.Id;
            }

            var result = (from r in hierachyRepo.GetAllDirectDescendantEntities(sourceId) where r.UserId.HasValue select r.UserId.Value);

            // we eval it here so as to force execution of the Lingo query here rather than somewhere in Lingo
            return result.ToList().Cast<object>().ToList();
        }

        //Gets the direct *user* parent for this ID
        //return object because it can be just used like a guid from lingo
        //a Guid? would need .Value, .HasValue, etc
        public object GetPrimarySuperiorUser(Guid minionId)
        {
            var result = hierachyRepo.GetPrimaryParentEntityPunchThrough(minionId);
            //return null unless both result and result.Userid are not null
            return result == null ? null : (result.UserId.HasValue ? result.UserId : null);
        }

        public object GetParentDeep(object userId, int depth)
        {
            return GetParentDeep(userId, depth, null);
        }

        public object GetParentDeep(object userId, int depth, object hierarchy)
        {
            UserDataDTO memberDto = null;
            if (userId is Guid)
            {
                memberDto = membershipRepo.GetById((Guid)userId);
            }
            else if (userId is string)
            {
                memberDto = membershipRepo.GetByUsername((string)userId);
            }
            if (memberDto == null)
            {
                return null;
            }
            HierarchyDTO hierarchyDto = null;
            if (hierarchy != null)
            {
                if (hierarchy is string)
                {
                    hierarchyDto = hierachyRepo.GetHierarchy(hierarchy.ToString());
                }
                else if (hierarchy is int)
                {
                    hierarchyDto = hierachyRepo.GetHierarchy((int)hierarchy);
                }
            }
            else
            {
                var primary = hierachyRepo.GetPrimaryEntity(memberDto.UserId);
                if (primary == null)
                {
                    return null;
                }
                hierarchyDto = hierachyRepo.GetHierarchy(primary.HierarchyId);
            }
            if (hierarchyDto == null)
            {
                return null;
            }

            var result = hierachyRepo.GetParentDeep(memberDto.UserId, depth, hierarchyDto.Id);
            //return null unless both result and result.Userid are not null
            return result == null ? null : (result.UserId.HasValue ? result.UserId : null);
        }

        /// <summary>
        /// Allows Lingo to create an entirely new hierachy in memory
        /// </summary>
        /// <returns>New HieracyFacilityBucketItem</returns>
        public HierachyFacilityBucketItem New()
        {
            return new HierachyFacilityBucketItem(membershipRepo, hierachyRepo);
        }

        /// <summary>
        /// Loads the current hierachy stored in the db in an object graph callable by Lingo
        /// </summary>
        /// <returns></returns>
        public HierachyFacilityBucketItem Load(int id)
        {
            var dbRoot = hierachyRepo.LoadInMemory(id);

            HierachyFacilityBucketItem root = new HierachyFacilityBucketItem(membershipRepo, hierachyRepo);
            root.LoadFrom(dbRoot, null);

            return root;
        }

        public HierachyFacilityBucketItem Load(string hierarchyName)
        {
            var h = hierachyRepo.GetHierarchy(hierarchyName);

            if (h == null)
            {
                return null;
            }

            var hb = hierachyRepo.LoadInMemory(h.Id);

            HierachyFacilityBucketItem root = new HierachyFacilityBucketItem(membershipRepo, hierachyRepo);
            root.LoadFrom(hb, null);

            return root;
        }

        public HierachyFacilityBucketItem LoadUsersPrimary(object user)
        {
            UserDataDTO memberDto = null;
            if (user is Guid)
            {
                memberDto = membershipRepo.GetById((Guid)user);
            }
            else if (user is string)
            {
                memberDto = membershipRepo.GetByUsername((string)user);
            }
            if (memberDto == null)
            {
                return null;
            }

            var primaryHierarchy = hierachyRepo.GetPrimaryEntity(memberDto.UserId);
            if (primaryHierarchy == null)
            {
                return null;
            }

            var h = hierachyRepo.GetHierarchy(primaryHierarchy.HierarchyId);

            if (h == null)
            {
                return null;
            }

            var hb = hierachyRepo.LoadInMemory(h.Id);

            HierachyFacilityBucketItem root = new HierachyFacilityBucketItem(membershipRepo, hierachyRepo);
            root.LoadFrom(hb, null);

            // Now return the bucket for the user
            var userBucket = (from b in root.Flattened() where b.User is Guid && (Guid)b.User == memberDto.UserId select b).FirstOrDefault();

            return userBucket;
        }

        public List<HierachyFacilityBucketItem> LoadBucketsForUser(object user)
        {
            UserDataDTO memberDto = null;
            if (user is Guid)
            {
                memberDto = membershipRepo.GetById((Guid)user);
            }
            else if (user is string)
            {
                memberDto = membershipRepo.GetByUsername((string)user);
            }
            if (memberDto == null)
            {
                return null;
            }


            var hierarchies = hierachyRepo.GetHierarchiesForUser(memberDto.UserId);
            var buckets = hierarchies.SelectMany(h =>
            {
                var hb = hierachyRepo.LoadInMemory(h.Id);

                HierachyFacilityBucketItem root = new HierachyFacilityBucketItem(membershipRepo, hierachyRepo);
                root.LoadFrom(hb, null);

                // Now return the bucket for the user
                var userBuckets = (from b in root.Flattened() where b.User is Guid && (Guid)b.User == memberDto.UserId select b);

                return userBuckets;
            }).ToList();

            return buckets;
        }

        /// <summary>
        /// Clears out the current hiearchy from the db and saves the graph handed in
        /// </summary>
        /// <param name="root"></param>
        public void Save(HierachyFacilityBucketItem root)
        {
            var r = root.BuildHierachy(null, null, null);

            hierachyRepo.SetTreeNumbering(r);
            hierachyRepo.DumpAndSave(r);
        }
    }
}
