﻿using ConceptCave.BusinessLogic.Context;
using ConceptCave.BusinessLogic.LingoDataObjects;
using ConceptCave.Checklist.Facilities;
using ConceptCave.Core;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Repositories;
using System;

namespace ConceptCave.BusinessLogic.Facilities
{
    class AssetFacility:Facility
    {
        private IAssetRepository _assetRepo;
        private IContextManagerFactory _ctxFactory;
        public AssetFacility(IAssetRepository assetRepo, 
           IContextManagerFactory ctxFactory)
        {
            this.Name = "Asset";
            _assetRepo = assetRepo;
            _ctxFactory = ctxFactory;
        }

        public FacilityAssetHelper GetById(IContextContainer context, int AssetID)
        {
            var assetData = new FacilityAssetHelper(_assetRepo, _ctxFactory, AssetID);
            return assetData;
        }
    }
}
