﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Facilities;
using ConceptCave.Checklist.Lingo.ObjectProviders;
using ConceptCave.Core;
using ConceptCave.Core.Coding;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ConceptCave.BusinessLogic.Facilities
{
    public class JsonFacility : Facility
    {
        public JsonFacility()
        {
            Name = "Json";
        }

        protected JToken SerializeItem(IContextContainer context, object item)
        {
            if (item is DictionaryObjectProvider)
            {
                return DictionaryToJson(context, (DictionaryObjectProvider)item);
            }
            else if (item is ListObjectProvider)
            {
                return ListToJson(context, (ListObjectProvider)item);
            }
            else
            {
                return new JValue(item);
            }
        }

        public JObject DictionaryToJson(IContextContainer context, DictionaryObjectProvider source)
        {
            JObject result = new JObject();

            foreach (var prop in source.Properties)
            {
                var propValue = source.GetValue(context, prop.ToString());

                JToken pv = SerializeItem(context, propValue);

                result.Add(prop.ToString(), pv);
            }

            return result;
        }

        public JArray ListToJson(IContextContainer context, ListObjectProvider source)
        {
            JArray result = new JArray();

            foreach (var item in source)
            {
                result.Add(SerializeItem(context, item));
            }

            return result;
        }

        protected object DeserializeItem(IContextContainer context, JToken item)
        {
            if (item.Type == JTokenType.Object)
            {
                return JsonToDictionary(context, (JObject)item);
            }
            else if (item.Type == JTokenType.Array)
            {
                return JsonToList(context, (JArray)item);
            }
            else
            {
                return ((JValue)item).Value;
            }
        }

        public DictionaryObjectProvider JsonToDictionary(IContextContainer context, JObject o)
        {
            DictionaryObjectProvider result = new DictionaryObjectProvider();

            foreach (var prop in o.Properties())
            {
                result.SetValue(context, prop.Name, DeserializeItem(context, prop.Value));
            }

            return result;
        }

        public ListObjectProvider JsonToList(IContextContainer context, JArray a)
        {
            ListObjectProvider result = new ListObjectProvider();

            foreach (var item in a)
            {
                result.Add(DeserializeItem(context, item));
            }

            return result;
        }

        public string ToJson(IContextContainer context, object source)
        {
            return SerializeItem(context, source).ToString();
        }

        public object FromJson(IContextContainer context, string json)
        {
            JToken t = JToken.Parse(json);

            return DeserializeItem(context, t);
        }

        public object FromJson(IContextContainer context, byte[] data)
        {
            string json = Encoding.UTF8.GetString(data);
            return FromJson(context, json);
        }

        public class TryJsonResult : Node
        {
            public bool Valid { get; private set; }
            public string ErrorMessage { get; private set; }
            public object Object { get; private set; }

            public TryJsonResult(bool valid, string error, object o)
            {
                Valid = valid;
                ErrorMessage = error;
                Object = o;
            }

            public TryJsonResult()
            {
            }

            public override void Encode(IEncoder encoder)
            {
                base.Encode(encoder);
                if (Valid)
                    encoder.Encode<bool>("Valid", Valid);
                if (ErrorMessage != null)
                    encoder.Encode<string>("ErrorMessage", ErrorMessage);
                if (Object != null)
                    encoder.Encode<object>("Object", Object);
            }

            public override void Decode(IDecoder decoder)
            {
                base.Decode(decoder);

                Valid = decoder.Decode<bool>("Valid", false);
                ErrorMessage = decoder.Decode<string>("ErrorMessage", null);
                Object = decoder.Decode<object>("Object", null);
            }
        }

        public TryJsonResult TryFromJson(IContextContainer context, string json)
        {
            if (string.IsNullOrEmpty(json))
            {
                return new TryJsonResult(false, "Empty or null string", null);
            }
            try
            {
                var obj = FromJson(context, json);
                return new TryJsonResult(true, null, obj);
            }
            catch(Exception ex)
            {
                return new TryJsonResult(false, ex.Message, null);
            }
        }
    }
}
