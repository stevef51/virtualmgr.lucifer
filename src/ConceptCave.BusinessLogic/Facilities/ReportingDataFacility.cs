﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Checklist.Facilities;
using ConceptCave.Checklist.Lingo.ObjectProviders;
using ConceptCave.Core;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.BusinessLogic.Extensions;
using ConceptCave.RepositoryInterfaces.Repositories;

namespace ConceptCave.BusinessLogic.Facilities
{
    public class ReportingDataFacility : Facility
    {
        protected IWorkingDocumentRepository _wdRepo;

        public ReportingDataFacility(IWorkingDocumentRepository wdRepo)
        {
            Name = "ReportingData";
            _wdRepo = wdRepo;
        }

        public List<DictionaryObjectProvider> CurrentWorkingDocuments(IContextContainer container, Guid userId, int pageNumber, int pageSize)
        {
            if (userId == Guid.Empty)
            {
                return null;
            }

            if (pageNumber < 1)
            {
                return null;
            }

            if (pageSize < 1)
            {
                return null;
            }

            var items = _wdRepo.GetCurrentWorkingDocuments(userId, WorkingDocumentLoadInstructions.Users);

            return ConvertWorkingDocumentsForLingo(items);
        }

        public List<DictionaryObjectProvider> Anscestors(IContextContainer container, Guid id, int pageNumber, int pageSize)
        {
            if (id == Guid.Empty)
            {
                return null;
            }

            if (pageNumber < 1)
            {
                return null;
            }

            if (pageSize < 1)
            {
                return null;
            }

            List<WorkingDocumentDTO> currents = new List<WorkingDocumentDTO>();

            var current = _wdRepo.GetById(id, WorkingDocumentLoadInstructions.None);

            Guid? parentId = current.ParentId;

            while (parentId.HasValue == true)
            {
                var anscestor = _wdRepo.GetById(parentId.Value, WorkingDocumentLoadInstructions.Users);
                if (anscestor == null)
                    break;

                currents.Add(anscestor);

                parentId = anscestor.ParentId;
            }

            return ConvertWorkingDocumentsForLingo(currents);
        }

        /// <summary>
        /// Returns the list of working documents that have the same parent id as the working document handed in
        /// </summary>
        /// <param name="container"></param>
        /// <param name="userId"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public List<DictionaryObjectProvider> CurrentSiblingWorkingDocuments(IContextContainer container, Guid parentId, int pageNumber, int pageSize)
        {
            if (parentId == Guid.Empty)
            {
                return null;
            }

            if (pageNumber < 1)
            {
                return null;
            }

            if (pageSize < 1)
            {
                return null;
            }

            var items = _wdRepo.GetByParentId(parentId, WorkingDocumentLoadInstructions.Users);

            List<WorkingDocumentDTO> currents = new List<WorkingDocumentDTO>();

            // work out which are current (not completed)
            foreach (var i in items)
            {
                if (i.Status == (int)WorkingDocumentStatus.Started)
                {
                    currents.Add(i);
                }
            }

            return ConvertWorkingDocumentsForLingo(currents);
        }

        protected List<DictionaryObjectProvider> ConvertWorkingDocumentsForLingo(IEnumerable<WorkingDocumentDTO> entities)
        {
            List<DictionaryObjectProvider> result = new List<DictionaryObjectProvider>();

            foreach (var entity in entities)
            {
                DictionaryObjectProvider prov = new DictionaryObjectProvider();
                prov.Dictionary = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);
                prov.Dictionary["WorkingDocumentId"] = entity.Id;
                prov.Dictionary["DateCreated"] = entity.DateCreated;
                prov.Dictionary["DateCreatedForReviewer"] = entity.DateCreatedInReviewerTimeZone();
                prov.Dictionary["DateCreatedForReviewee"] = entity.DateCreatedInRevieweeTimeZone();
                prov.Dictionary["RevieweeId"] = entity.RevieweeId;
                if (entity.RevieweeId.HasValue)
                {
                    prov.Dictionary["RevieweeName"] = entity.Reviewer.Name;
                }
                else
                {
                    prov.Dictionary["RevieweeName"] = null;
                }
                prov.Dictionary["ReviewerId"] = entity.ReviewerId;
                prov.Dictionary["ReviewerName"] = entity.Reviewer.Name;
                prov.Dictionary["ParentId"] = entity.ParentId;
                prov.Dictionary["Name"] = entity.Name;

                result.Add(prov);
            }

            return result;
        }
    }
}
