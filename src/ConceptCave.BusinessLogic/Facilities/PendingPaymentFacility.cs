﻿using ConceptCave.BusinessLogic.LingoDataObjects;
using ConceptCave.BusinessLogic.Questions;
using ConceptCave.Checklist;
using ConceptCave.Checklist.Facilities;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.Lingo.ObjectProviders;
using ConceptCave.Core;
using ConceptCave.Core.Coding;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.SimpleExtensions;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using VirtualMgr.Common;
using VirtualMgr.MultiTenant;

namespace ConceptCave.BusinessLogic.Facilities
{
    public class PendingPaymentFacility : Facility
    {
        private readonly IPendingPaymentRepository _pendingPaymentRepo;
        private readonly IPaymentResponseRepository _paymentResponseRepo;
        private readonly IProductRepository _productRepo;
        private readonly IOrderRepository _orderRepo;
        private readonly IDateTimeProvider _dateTimeProvider;
        private readonly IJsFunctionService _jsFunctionService;
        private readonly Func<string, IPaymentGatewayFacility> _fnPaymentGatewayFacility;

        private readonly Func<Guid, FacilityUserHelper> _fnFacilityUserHelper;
        private readonly AppTenant _appTenant;

        public class PendingPayment : Node
        {
            public int OrderBy { get; set; }
            public int Status { get; set; }
            [JsonConverter(typeof(JsonUTCDateTimeConverter))]
            public DateTime StatusDateUtc { get; set; }
            [JsonConverter(typeof(JsonUTCDateTimeConverter))]
            public DateTime? DueDateUtc { get; set; }
            [JsonConverter(typeof(JsonUTCDateTimeConverter))]
            public DateTime? ActivatedDateUtc { get; set; }
            [JsonConverter(typeof(JsonUTCDateTimeConverter))]
            public DateTime? PaidInFullDateUtc { get; set; }
            [JsonConverter(typeof(JsonUTCDateTimeConverter))]
            public DateTime? CancelledDateUtc { get; set; }
            [JsonConverter(typeof(JsonUTCDateTimeConverter))]
            public DateTime? LastFailedDateUtc { get; set; }
            [JsonConverter(typeof(JsonUTCDateTimeConverter))]
            public DateTime? FromUtc { get; set; }
            [JsonConverter(typeof(JsonUTCDateTimeConverter))]
            public DateTime? ToUtc { get; set; }
            public string Description { get; set; }
            public decimal DueAmount { get; set; }
            public decimal FullAmount { get; set; }
            public decimal DiscountAmount { get; set; }
            public string Currency { get; set; }
            public string GatewayName { get; set; }
            public string GatewayContext { get; set; }
            public Guid OrderId { get; set; }
            public bool Sandbox { get; set; }
            public Guid UserId { get; set; }
            public decimal? Quantity { get; set; }
            public int? InvoiceNumber { get; set; }

            public PendingPayment()
            {

            }

            public PendingPayment(PendingPaymentDTO dto)
            {
                ((IUniqueNode)this).UseDecodedId(dto.Id);
                this.OrderBy = dto.OrderBy;
                this.Status = dto.Status;
                this.StatusDateUtc = dto.StatusDateUtc;
                this.Currency = dto.Currency;
                this.FullAmount = dto.FullAmount;
                this.DiscountAmount = dto.DiscountAmount;
                this.DueAmount = dto.DueAmount;
                this.DueDateUtc = dto.DueDateUtc;
                this.ActivatedDateUtc = dto.ActivatedDateUtc;
                this.CancelledDateUtc = dto.CancelledDateUtc;
                this.PaidInFullDateUtc = dto.PaidInFullDateUtc;
                this.LastFailedDateUtc = dto.LastFailedDateUtc;
                this.GatewayName = dto.GatewayName;
                this.GatewayContext = dto.GatewayContext;
                this.OrderId = dto.OrderId;
                this.FromUtc = dto.FromUtc;
                this.ToUtc = dto.ToUtc;
                this.Description = dto.Description;
                this.Sandbox = dto.Sandbox;
                this.UserId = dto.UserId;
                this.Quantity = dto.Quantity;
                this.InvoiceNumber = dto.InvoiceNumber;
            }

            public override void Encode(IEncoder encoder)
            {
                base.Encode(encoder);

                encoder.Encode("OrderBy", OrderBy);
                encoder.Encode("Status", Status);
                encoder.Encode("StatusDateUtc", StatusDateUtc);
                encoder.Encode("DueDateUtc", DueDateUtc);
                encoder.Encode("ActivatedDateUtc", ActivatedDateUtc);
                encoder.Encode("CancelledDateUtc", CancelledDateUtc);
                encoder.Encode("PaidInFullDateUtc", PaidInFullDateUtc);
                encoder.Encode("LastFailedDateUtc", LastFailedDateUtc);
                encoder.Encode("FullAmount", FullAmount);
                encoder.Encode("DiscountAmount", DiscountAmount);
                encoder.Encode("DueAmount", DueAmount);
                encoder.Encode("Currency", Currency);
                encoder.Encode("GatewayContext", GatewayContext);
                encoder.Encode("OrderId", OrderId);
                encoder.Encode("FromUtc", FromUtc);
                encoder.Encode("ToUtc", ToUtc);
                encoder.Encode("Description", Description);
                encoder.Encode("Sandbox", Sandbox);
                encoder.Encode("UserId", UserId);
                encoder.Encode("GatewayName", GatewayName);
                encoder.Encode("Quantity", Quantity);
                encoder.Encode("InvoiceNumber", InvoiceNumber);                
            }

            public override void Decode(IDecoder decoder)
            {
                base.Decode(decoder);

                OrderBy = decoder.Decode<int>("OrderBy");
                Status = decoder.Decode<int>("Status");
                StatusDateUtc = decoder.Decode<DateTime>("StatusDateUtc");
                DueDateUtc = decoder.Decode<DateTime>("DueDateUtc");
                ActivatedDateUtc = decoder.Decode<DateTime?>("ActivatedDateUtc");
                CancelledDateUtc = decoder.Decode<DateTime?>("CancelledDateUtc");
                PaidInFullDateUtc = decoder.Decode<DateTime?>("PaidInFullDateUtc");
                LastFailedDateUtc = decoder.Decode<DateTime?>("LastFailedDateUtc");
                FullAmount = decoder.Decode<decimal>("FullAmount");
                DiscountAmount = decoder.Decode<decimal>("DiscountAmount");
                DueAmount = decoder.Decode<decimal>("DueAmount");
                Currency = decoder.Decode<string>("Currency");
                GatewayContext = decoder.Decode<string>("GatewayContext");
                OrderId = decoder.Decode<Guid>("OrderId");
                FromUtc = decoder.Decode<DateTime?>("FromUtc");
                ToUtc = decoder.Decode<DateTime?>("ToUtc");
                Description = decoder.Decode<string>("Description");
                Sandbox = decoder.Decode<bool>("Sandbox");
                UserId = decoder.Decode<Guid>("UserId");
                GatewayName = decoder.Decode<string>("GatewayName");
                Quantity = decoder.Decode<decimal?>("Quantity");
                InvoiceNumber = decoder.Decode<int?>("InvoiceNumber");                             
            }
        }

        public ApplyCouponCodeResult CancelOrder(Guid orderId)
        {
            var order = _orderRepo.GetById(orderId, RepositoryInterfaces.Enums.OrderLoadInstructions.None);
            if (order != null && !order.CancelledDateUtc.HasValue)
            {
                var utcNow = _dateTimeProvider.UtcNow;

                order.CancelledDateUtc = utcNow;
                _orderRepo.Save(order, false, false);

                var pendingPayments = _pendingPaymentRepo.GetByOrderId(orderId, RepositoryInterfaces.Enums.PendingPaymentLoadInstructions.None);
                foreach (var pp in pendingPayments)
                {
                    if (pp.Status == (int)PendingPaymentStatus.Scheduled)
                    {
                        pp.Status = (int)PendingPaymentStatus.Cancelled;
                        pp.CancelledDateUtc = utcNow;
                        _pendingPaymentRepo.Save(pp, false, false);
                    }
                }
            }
            return CalculateCoupons(order, true);
        }

        public enum CouponCodeResult
        {
            Applied,
            InvalidCoupon,
            OutOfDate,
            AlreadyApplied,
            CouponLimitReached,
            NotApplied
        }

        public class ApplyCouponCodeResult
        {
            public OrderFacilityOrder order { get; set; }
            public CouponCodeResult result { get; set; }
            public string message { get; set; }
            public PendingPayments payments { get; set; }
            public object[] appliedCoupons { get; set; }
        }

        public ApplyCouponCodeResult GetOrderCoupons(Guid orderId)
        {
            var order = _orderRepo.GetById(orderId, RepositoryInterfaces.Enums.OrderLoadInstructions.Items | RepositoryInterfaces.Enums.OrderLoadInstructions.ItemProducts);

            return CalculateCoupons(order, true);
        }

        public ApplyCouponCodeResult ApplyCouponCode(string couponCode, Guid orderId, bool userAdded)
        {
            var result = new ApplyCouponCodeResult();

            var coupon = _productRepo.GetByCode(couponCode, true);
            if (coupon == null)
            {
                result.result = CouponCodeResult.InvalidCoupon;
                return result;
            }

            if (userAdded)
            {
                var extra = JObject.Parse(coupon.Extra ?? "{}");
                if (extra != null && (extra.ContainsKey("userCanAdd") && !extra.Value<bool>("userCanAdd")))
                {
                    result.result = CouponCodeResult.InvalidCoupon;
                    return result;
                }
            }

            var utc = _dateTimeProvider.UtcNow;
            if ((utc < (coupon.ValidFrom ?? utc)) || (utc > (coupon.ValidTo ?? utc)))
            {
                result.result = CouponCodeResult.OutOfDate;
                return result;
            }

            var order = _orderRepo.GetById(orderId, RepositoryInterfaces.Enums.OrderLoadInstructions.Items | RepositoryInterfaces.Enums.OrderLoadInstructions.ItemProducts);
            if ((from oi in order.OrderItems where oi.Product.Id == coupon.Id select oi).Any())
            {
                result.result = CouponCodeResult.AlreadyApplied;
                return result;
            }

            var couponItem = new OrderItemDTO()
            {
                __IsNew = true,
                Id = CombFactory.NewComb(),
                Product = coupon,
                Quantity = 1,
                Order = order,
            };
            order.OrderItems.Add(couponItem);

            var couponCount = (from oi in order.OrderItems
                               where oi.Product.IsCoupon
                               let extra = JObject.Parse(oi.Product.Extra ?? "{}")
                               where !extra.ContainsKey("excludeFromCount") || !extra.Value<bool>("excludeFromCount")
                               select oi).Count();
            if (couponCount > order.MaxCoupons)
            {
                result.result = CouponCodeResult.CouponLimitReached;
                return result;
            }

            return CalculateCoupons(order, false);
        }

        public ApplyCouponCodeResult RemoveCouponCode(Guid couponId, Guid orderId, bool userRemoved)
        {
            var order = _orderRepo.GetById(orderId, RepositoryInterfaces.Enums.OrderLoadInstructions.Items | RepositoryInterfaces.Enums.OrderLoadInstructions.ItemProducts);
            var couponItem = (from oi in order.OrderItems where oi.Id == couponId select oi).FirstOrDefault();
            if (couponItem != null)
            {
                var extra = JObject.Parse(couponItem.Product.Extra ?? "{}");
                if (!userRemoved || extra == null || !extra.ContainsKey("userCanRemove") || extra.Value<bool>("userCanRemove"))
                {
                    order.OrderItems.Remove(couponItem);
                    _orderRepo.RemoveItems(new[] { couponItem.Id });
                }
            }

            return CalculateCoupons(order, false);
        }

        private ApplyCouponCodeResult CalculateCoupons(OrderDTO order, bool readOnly)
        {
            var payments = _pendingPaymentRepo.GetByOrderId(order.Id, RepositoryInterfaces.Enums.PendingPaymentLoadInstructions.None);
            if (!readOnly)
            {
                foreach (var pp in payments)
                {
                    if (pp.Status <= (int)PendingPaymentStatus.Scheduled)
                    {
                        pp.DueAmount = pp.FullAmount;
                        pp.DiscountAmount = 0;
                    }
                }

                // Going to recalculate the Order Total on the fly, so we recalculate all coupons the Js Coupon logic
                // has to check the "isNew" flag on the coupon to determine if its a new coupon trying to be added
                var orderTotal = 0M;
                foreach (var orderItem in order.OrderItems)
                {
                    if (orderItem.Product.IsCoupon)
                    {
                        var c = orderItem.Product;
                        if (c.PriceJsFunctionId.HasValue)
                        {
                            var apply = _jsFunctionService.Execute<dynamic>(c.PriceJsFunctionId.Value, new
                            {
                                payments = from p in payments
                                           orderby p.OrderBy
                                           select new
                                           {
                                               p.Id,
                                               p.CreatedDateUtc,
                                               p.Description,
                                               p.FullAmount,
                                               p.DiscountAmount,
                                               p.DueAmount,
                                               p.DueDateUtc,
                                               p.FromUtc,
                                               p.Quantity,
                                               p.Sandbox,
                                               p.PaidInFullDateUtc,
                                               p.Status,
                                               p.UserId
                                           },
                                coupon = new
                                {
                                    isNew = orderItem.__IsNew,          // If false then the coupon is already added and limit checks are not done
                                    c.Id,
                                    c.Code,
                                    c.Description,
                                    Extra = string.IsNullOrEmpty(c.Extra) ? null : JsonConvert.DeserializeObject(c.Extra),
                                    c.Name,
                                    c.Price,
                                    c.ProductTypeId,
                                    c.ValidFrom,
                                    c.ValidTo
                                },
                                order = new
                                {
                                    order.Id,
                                    order.DateCreated,
                                    order.FacilityStructureId,
                                    order.TotalPrice,
                                    order.ProjectJobTaskId,
                                    order.ProductCatalogId,
                                    order.OrderedById,
                                    OrderItems = from oi in order.OrderItems
                                                 select new
                                                 {
                                                     oi.Id,
                                                     oi.MinStockLevel,
                                                     oi.Price,
                                                     oi.Quantity,
                                                     oi.TotalPrice,
                                                     oi.StartDate,
                                                     oi.EndDate,
                                                     Product = new
                                                     {
                                                         oi.Product.Id,
                                                         oi.Product.Description,
                                                         Extra = string.IsNullOrEmpty(oi.Product.Extra) ? null : JsonConvert.DeserializeObject(oi.Product.Extra),
                                                         oi.Product.IsCoupon,
                                                         oi.Product.Code,
                                                         oi.Product.Name
                                                     }
                                                 }
                                }
                            });

                            if (apply.result != null)
                            {
                                if (apply.result.notAppliedMessage != null)
                                {
                                    // Abort the recaculate now, this does not save any changes to the payments or the order
                                    return new ApplyCouponCodeResult()
                                    {
                                        result = CouponCodeResult.NotApplied,
                                        message = apply.result.notAppliedMessage.ToString()
                                    };
                                }

                                if (apply.result.payments != null)
                                {
                                    var orderItemDiscount = 0M;
                                    foreach (var returnedPP in apply.result.payments)
                                    {
                                        var id = Guid.Parse(returnedPP.id.ToString());
                                        var discount = decimal.Parse(returnedPP.discountAmount?.ToString() ?? "0");
                                        orderItemDiscount += discount;
                                        var pp = (from p in payments where p.Id == id select p).FirstOrDefault();

                                        if (pp.Status <= (int)PendingPaymentStatus.Scheduled)
                                        {
                                            pp.DiscountAmount = discount;
                                            pp.DueAmount = pp.FullAmount - discount;
                                        }
                                    }
                                    orderItem.Price = -orderItemDiscount;
                                    orderItem.TotalPrice = orderItem.Price;
                                }
                            }
                            else if (orderItem.__IsNew)
                            {
                                // This is a new coupon trying to be added and we failed to apply it, bail ..
                                return new ApplyCouponCodeResult()
                                {
                                    result = CouponCodeResult.NotApplied,
                                    message = "Error applying coupon"
                                };
                            }
                        }
                    }
                    orderTotal += orderItem.TotalPrice ?? 0M;
                }
                order.TotalPrice = orderTotal;
                // Save any changes to the Coupon Discount OrderItems
                _orderRepo.Save(order, true, true);
            }

            var result = new ApplyCouponCodeResult()
            {
                result = CouponCodeResult.Applied,
                payments = new PendingPayments()
                {
                    MaxCoupons = order.MaxCoupons,
                    Payments = new ListObjectProvider((from p in payments select new PendingPayment(p)).ToArray()),
                },
                appliedCoupons = (from c in order.OrderItems
                                  where c.Product.IsCoupon
                                  let extra = JObject.Parse(c.Product.Extra ?? "{}")
                                  select new
                                  {
                                      id = c.Id,
                                      code = c.Product.Code,
                                      description = c.Product.Description,
                                      excludeFromCount = extra.ContainsKey("excludeFromCount") && extra.Value<bool>("excludeFromCount"),
                                      userCanRemove = !extra.ContainsKey("userCanRemove") || extra.Value<bool>("userCanRemove")
                                  }).ToArray(),
                order = new OrderFacilityOrder(order)
            };

            if (!readOnly)
            {
                // Let the Payment Gateway modify its payment context
                var gatewayName = payments.First().GatewayName;
                var gateway = _fnPaymentGatewayFacility(gatewayName);
                if (gateway != null)
                {
                    gateway.Modify(result.payments);
                }

                Save(result.payments);
            }

            return result;
        }

        public PendingPayments GetByOrderId(Guid orderId)
        {
            var pp = _pendingPaymentRepo.GetByOrderId(orderId, RepositoryInterfaces.Enums.PendingPaymentLoadInstructions.PaymentResponses);
            return new PendingPayments()
            {
                Payments = new ListObjectProvider((from p in pp select new PendingPayment(p)).ToArray()),
            };
        }

        public class PaymentResponse : Node
        {
            public DateTime DateUtc { get; set; }
            public string Errors { get; set; }
            public string AuthorisationCode { get; set; }
            public string TransactionId { get; set; }
            public Guid PendingPaymentId { get; set; }
            public string Response { get; set; }
            public string ResponseCode { get; set; }
            public string ResponseMessage { get; set; }
            public decimal RequestedAmount { get; set; }
            public decimal TransactedAmount { get; set; }
            public bool Status { get; set; }
            public PaymentResponse()
            {

            }

            public PaymentResponse(PaymentResponseDTO dto)
            {
                ((IUniqueNode)this).UseDecodedId(dto.Id);
                DateUtc = dto.DateUtc;
                Errors = dto.Errors;
                AuthorisationCode = dto.GatewayAuthorisationCode;
                TransactionId = dto.GatewayTransactionId;
                PendingPaymentId = dto.PendingPaymentId;
                Response = dto.Response;
                ResponseCode = dto.ResponseCode;
                ResponseMessage = dto.ResponseMessage;
                TransactedAmount = dto.TransactedAmount ?? 0;
                RequestedAmount = dto.RequestedAmount;
                Status = dto.TransactionStatus;
            }

            public override void Encode(IEncoder encoder)
            {
                base.Encode(encoder);
                encoder.Encode("DateUtc", DateUtc);
                encoder.Encode("Errors", Errors);
                encoder.Encode("AuthorisationCode", AuthorisationCode);
                encoder.Encode("TransactionId", TransactionId);
                encoder.Encode("PendingPaymentId", PendingPaymentId);
                encoder.Encode("Response", Response);
                encoder.Encode("ResponseCode", ResponseCode);
                encoder.Encode("ResponseMessage", ResponseMessage);
                encoder.Encode("TransactedAmount", TransactedAmount);
                encoder.Encode("RequestedAmount", RequestedAmount);
                encoder.Encode("Status", Status);
            }

            public override void Decode(IDecoder decoder)
            {
                base.Decode(decoder);

                DateUtc = decoder.Decode<DateTime>("DateUtc");
                Errors = decoder.Decode<string>("Errors");
                AuthorisationCode = decoder.Decode<string>("AuthorisationCode");
                TransactionId = decoder.Decode<string>("TransactionId");
                PendingPaymentId = decoder.Decode<Guid>("PendingPaymentId");
                Response = decoder.Decode<string>("Response");
                ResponseCode = decoder.Decode<string>("ResponseCode");
                ResponseMessage = decoder.Decode<string>("ResponseMessage");
                TransactedAmount = decoder.Decode<decimal>("TransactedAmount");
                RequestedAmount = decoder.Decode<decimal>("RequestedAmount");
                Status = decoder.Decode<bool>("Status");
            }
        }

        public class PendingPayments : Node
        {
            public PaymentPlanParameters Plan { get; set; }

            public ListObjectProvider Payments { get; set; }

            public decimal PeriodQuantity { get; set; }
            public decimal TotalCost { get; set; }

            public int MaxCoupons { get; set; }

            public PendingPayments()
            {
                Payments = new ListObjectProvider();
            }

            public override void Encode(IEncoder encoder)
            {
                base.Encode(encoder);

                encoder.Encode("Plan", Plan);
                encoder.Encode("Payments", Payments);
                encoder.Encode("PeriodQuantity", PeriodQuantity);
                encoder.Encode("TotalCost", TotalCost);
                encoder.Encode("MaxCoupons", MaxCoupons);
            }

            public override void Decode(IDecoder decoder)
            {
                base.Decode(decoder);

                Plan = decoder.Decode<PaymentPlanParameters>("Plan");
                Payments = decoder.Decode<ListObjectProvider>("Payments");
                PeriodQuantity = decoder.Decode<decimal>("PeriodQuantity");
                TotalCost = decoder.Decode<decimal>("TotalCost");
                MaxCoupons = decoder.Decode<int>("MaxCoupons");
            }

        }

        public enum PaymentPlanPeriod
        {
            None,
            Daily,
            Weekly,
            Fortnightly,
            Monthly,
            Quarterly,
            Yearly
        }

        public enum TotalCostRounding
        {
            None,                       // No payments are adjusted, the Order should be updated to use the resulting PendingPayments.TotalCost
            FirstPayment,               // The 1st payment is adjusted to make the payments equal the Order.Total
            LastPayment                 // The last payment is adjusted to make the payments equal the Order.Total
        }

        public class PaymentPlanParameters : Node
        {
            [JsonIgnore]
            internal PaymentPlanPeriod _period { get; set; }
            public string Period
            {
                get => _period.ToString();
                set => _period = (PaymentPlanPeriod)Enum.Parse(typeof(PaymentPlanPeriod), value, true);
            }
            [JsonIgnore]
            public FacilityUserHelper User { get; set; }
            public string TimeZone { get; set; }
            public DateTime Start { get; set; }
            public DateTime End { get; set; }
            public bool FirstPaymentIsFullPeriod { get; set; }
            internal TotalCostRounding _totalCostRounding { get; set; }
            public string TotalCostRounding
            {
                get => _totalCostRounding.ToString();
                set => _totalCostRounding = (TotalCostRounding)Enum.Parse(typeof(TotalCostRounding), value, true);
            }
            public int? BillingDayOfPeriod { get; set; }
            public DateTime FirstBillingDateUtc { get; set; }
            public DateTime LastBillingDateUtc { get; set; }
            [JsonIgnore]
            public OrderFacilityOrder Order { get; set; }
            public decimal? CostPerDay { get; set; }
            public decimal? CostPerMonth { get; set; }
            public string Currency { get; set; }
            public string DateFormat { get; set; } = "d MMM";
            public override void Encode(IEncoder encoder)
            {
                base.Encode(encoder);
                encoder.Encode("Period", _period);
                encoder.Encode("User", User);
                encoder.Encode("Start", Start);
                encoder.Encode("End", End);
                encoder.Encode("FirstPaymentIsFullPeriod", FirstPaymentIsFullPeriod);
                encoder.Encode("TotalCostRounding", _totalCostRounding);
                encoder.Encode("BillingDayOfPeriod", BillingDayOfPeriod);
                encoder.Encode("FirstBillingDateUtc", FirstBillingDateUtc);
                encoder.Encode("LastBillingDateUtc", LastBillingDateUtc);
                encoder.Encode("Order", Order);
                encoder.Encode("CostPerDay", CostPerDay);
                encoder.Encode("CostPerMonth", CostPerMonth);
                encoder.Encode("Currency", Currency);
                encoder.Encode("DateFormat", DateFormat);
                encoder.Encode("TimeZone", TimeZone);
            }

            public override void Decode(IDecoder decoder)
            {
                base.Decode(decoder);

                _period = decoder.Decode<PaymentPlanPeriod>("Period");
                User = decoder.Decode<FacilityUserHelper>("User");
                Start = decoder.Decode<DateTime>("Start");
                End = decoder.Decode<DateTime>("End");
                FirstPaymentIsFullPeriod = decoder.Decode<bool>("FirstPaymentIsFullPeriod");
                _totalCostRounding = decoder.Decode<TotalCostRounding>("TotalCostRounding");
                BillingDayOfPeriod = decoder.Decode<int?>("BillingDayOfPeriod");
                FirstBillingDateUtc = decoder.Decode<DateTime>("FirstBillingDateUtc");
                LastBillingDateUtc = decoder.Decode<DateTime>("LastBillingDateUtc");
                Order = decoder.Decode<OrderFacilityOrder>("Order");
                CostPerDay = decoder.Decode<decimal?>("CostPerDay");
                CostPerMonth = decoder.Decode<decimal?>("CostPerMonth");
                Currency = decoder.Decode<string>("Currency");
                DateFormat = decoder.Decode<string>("DateFormat");
                TimeZone = decoder.Decode<string>("TimeZone");
            }
        }

        public PendingPaymentFacility(
            IPendingPaymentRepository pendingPaymentRepo,
            IPaymentResponseRepository paymentResponseRepo,
            IProductRepository productRepo,
            Func<Guid, FacilityUserHelper> fnFacilityUserHelper,
            IOrderRepository orderRepo,
            IDateTimeProvider dateTimeProvider,
            IJsFunctionService jsFunctionService,
            Func<string, IPaymentGatewayFacility> fnPaymentGatewayFacility,
            AppTenant appTenant)
        {
            _pendingPaymentRepo = pendingPaymentRepo;
            _paymentResponseRepo = paymentResponseRepo;
            _productRepo = productRepo;
            _fnFacilityUserHelper = fnFacilityUserHelper;
            _orderRepo = orderRepo;
            _dateTimeProvider = dateTimeProvider;
            _jsFunctionService = jsFunctionService;
            _fnPaymentGatewayFacility = fnPaymentGatewayFacility;
            _appTenant = appTenant;
            Name = "PendingPayment";
        }

        public PendingPayment NewPendingPayment(IContextContainer context, OrderFacilityOrder order)
        {
            return new PendingPayment()
            {
                Currency = "AUD",               // Default to AUD
                OrderId = order.Id
            };
        }

        public PaymentPlanParameters NewPaymentPlan()
        {
            return new PaymentPlanParameters();
        }

        public PendingPayments NewPendingPayments(IContextContainer context)
        {
            return new PendingPayments();
        }

        /// <summary>
        /// CostPerDay calculation - note, if cost = 1M then this function is returning the total number of days between the
        /// from and to dates.  From is inclusive, To is Exclusive
        /// Eg, 
        /// if cost = 1M, from = Jan 1 2019, to = Jan 1 2019 then that is 0 days = 0
        /// if cost = 1M, from = Jan 1 2019, to = Jan 2 2019 then that is 1 day = 1
        /// if cost = 1M, from = Jan 1 2019, to = Jan 31 2019 then that is 30 day = 30
        /// if cost = 1M, from = Jan 1 2019, to = Feb 1 2019 then that is 31 days = 31
        /// if cost = 1M, from = Jan 1 2019, to = Mar 5 2019 then that is 63 days = 63
        /// 
        /// </summary>
        /// <param name="from">Date to start from, inclusive</param>
        /// <param name="to">Date to end, exclusive</param>
        /// <param name="cost">The multiplier</param>
        /// <returns>The cost * the total number of days between from and to</returns>
        public decimal CostPerDay(DateTime from, DateTime to, int billingDay, decimal cost)
        {
            return ((int)to.Date.Subtract(from.Date).TotalDays - 1) * cost;
        }

        /// <summary>
        /// CostPerMonth calculation - note, if cost = 1M then this function is returning the fractional number of months between the
        /// from and to dates.  From is inclusive, To is Exclusive
        /// Eg, 
        /// if cost = 1M, from = Jan 1 2019, to = Jan 1 2019 then that is 0 day out of 31 = 0M/31M = 0
        /// if cost = 1M, from = Jan 1 2019, to = Jan 2 2019 then that is 1 day out of 31 = 1M/31M = 0.03225806452
        /// if cost = 1M, from = Jan 1 2019, to = Jan 31 2019 then that is 30 day out of 31 = 30M/31M = 0.9677419355
        /// if cost = 1M, from = Jan 1 2019, to = Feb 1 2019 then that is 1 month + 0 day out of 28 = 1 + 0M/28M = 1
        /// if cost = 1M, from = Jan 1 2019, to = Mar 5 2019 then that is 2 month + 4 day out of 31 = 2 + 4M/31M = 2.1290322581
        /// 
        /// </summary>
        /// <param name="from">Date to start from, inclusive</param>
        /// <param name="to">Date to end, exclusive</param>
        /// <param name="cost">The multiplier</param>
        /// <returns>The cost * the fractional number of months between from and to</returns>
        public decimal CostPerMonth(DateTime from, DateTime to, int billingDay, decimal cost)
        {
            var total = 0M;
            from = from.Date;
            to = to.Date;

            // We have to cost upto the next billing date in fractions of month first
            var billingDate = new DateTime(from.Year, from.Month, billingDay);

            // Account for short from..to 
            if (billingDate > to)
                billingDate = to;

            if (billingDate < from)
            {
                billingDate = billingDate.AddMonths(1);

                // eg, billingDay = 15 and from = 20th Jan => (31 - 20) + 15 => 26 days from 20th Jan .. 15th Feb
                total += cost * (((decimal)(1 + (DateTime.DaysInMonth(from.Year, from.Month) - from.Day))) / DateTime.DaysInMonth(from.Year, from.Month));
                total += cost * ((decimal)(billingDate.Day - 1) / DateTime.DaysInMonth(billingDate.Year, billingDate.Month));
            }
            else
            {
                // eg, billingDay = 15 and from = 3rd Jan => 15 - 3 = 12 days
                total += cost * (((decimal)(billingDate.Day - from.Day)) / DateTime.DaysInMonth(from.Year, from.Month));
            }

            // Ok, we now walk 1 full month at a time until we can't walk a full month
            var dt = billingDate;
            var next = dt.AddMonths(1);
            while (next <= to)
            {
                total += cost;
                dt = next;
                next = next.AddMonths(1);
            }

            // Calculate remaining fractional month
            if (to.Year == dt.Year && to.Month == dt.Month)
            {
                // They are in the same month
                total += cost * (((decimal)(to.Day - dt.Day)) / DateTime.DaysInMonth(to.Year, to.Month));
            }
            else
            {
                // They are 1 month apart
                total += cost * (((decimal)(DateTime.DaysInMonth(dt.Year, dt.Month) - dt.Day)) / DateTime.DaysInMonth(dt.Year, dt.Month));
                total += cost * ((decimal)(to.Day - 1) / DateTime.DaysInMonth(to.Year, to.Month));
            }
            return total;
        }

        public decimal? GetQuantity(IContextContainer context, PaymentPlanParameters plan)
        {
            // One of these scenarios, if billingDayOfMonth is 15
            //
            //   2018          2019                                                     2020
            //   Dec           Jan           Feb           Mar    ..      Dec           Jan
            //   1      15     1      15     1      15     1      15      1      15     1
            //
            // 1.                 ---
            // 2.                 ----+----
            // 3.                 ----+-----------
            // 4.         ------------
            // 5.         ------------+-------------+-------------+--------------+---------
            // 
            if (plan.End < plan.Start)
                return null;

            TimeZoneInfo planTimezone = string.IsNullOrEmpty(plan.TimeZone) ? _appTenant.TimeZoneInfo() : TimeZoneInfoResolver.ResolveWindows(plan.TimeZone);
            Func<DateTime, DateTime> fnPlanLocal = dt => TimeZoneInfo.ConvertTime(dt, TimeZoneInfo.Utc, planTimezone);

            // Perform these calculations in plan timezone, assume day starts at 00:00
            var planStartLocal = fnPlanLocal(DateTime.SpecifyKind(plan.Start, DateTimeKind.Utc)).Date;
            var planEndLocal = fnPlanLocal(DateTime.SpecifyKind(plan.End, DateTimeKind.Utc)).Date.AddDays(1);   // +1 since plan.End is inclusive

            var billingDay = Math.Max(plan.BillingDayOfPeriod ?? 1, 1);
            switch (plan._period)
            {
                default:
                    return 0;

                case PaymentPlanPeriod.Monthly:
                    return CostPerMonth(planStartLocal, planEndLocal, billingDay, 1M);

                case PaymentPlanPeriod.Weekly:
                    return CostPerDay(planStartLocal, planEndLocal, billingDay, 1M) / 7M;

                case PaymentPlanPeriod.Fortnightly:
                    return CostPerDay(planStartLocal, planEndLocal, billingDay, 1M) / 14M;

                case PaymentPlanPeriod.Yearly:
                    return CostPerDay(planStartLocal, planEndLocal, billingDay, 1M) / 365M;

                case PaymentPlanPeriod.Quarterly:
                    return CostPerMonth(planStartLocal, planEndLocal, billingDay, 1M) / 3M;

                case PaymentPlanPeriod.Daily:
                    return CostPerDay(planStartLocal, planEndLocal, billingDay, 1M);
            }
        }

        public PendingPayments Create(IContextContainer context, PaymentPlanParameters plan, IPaymentGatewayFacility gateway, object gatewayContext)
        {
            // One of these scenarios, if billingDayOfMonth is 15
            //
            //   2018          2019                                                     2020
            //   Dec           Jan           Feb           Mar    ..      Dec           Jan
            //   1      15     1      15     1      15     1      15      1      15     1
            //
            // 1.                 ---
            // 2.                 ----+----
            // 3.                 ----+-----------
            // 4.         ------------
            // 5.         ------------+-------------+-------------+--------------+---------
            // 
            var list = new List<PendingPayment>();

            if (plan.End < plan.Start)
                return null;

            var utcNow = _dateTimeProvider.UtcNow;
            string description = "";
            Dictionary<int, ProductDTO> products = new Dictionary<int, ProductDTO>();
            foreach (var orderItem in plan.Order.Items)
            {
                ProductDTO product = null;
                if (!products.TryGetValue(orderItem.ProductId, out product))
                {
                    product = _productRepo.GetById(orderItem.ProductId);
                    products.Add(product.Id, product);
                }

                description += product.Name + (!string.IsNullOrWhiteSpace(product.Description) ? " - " + product.Description : "");
                if (!string.IsNullOrEmpty(orderItem.Notes))
                {
                    description += " - " + orderItem.Notes;
                }
                description += "\n";
            }
            description += "{0:" + plan.DateFormat + "} - {1:" + plan.DateFormat + "}";

            Guid userId = plan.User.Id;

            // DST changes but more importantly it is likely the 1st is the Billing Day with 00:00 the billing time, a UTC +ve offset will mean
            // the UTC datetime for 1st Mar 00:00 Melbourne time is 28th Feb 13:00 UTC, when we add 1 month to this (trying to get to the 1st April) we 
            // instead get 28th Mar 13:00 UTC which is not 1st April Melbourne time.
            TimeZoneInfo planTimezone = string.IsNullOrEmpty(plan.TimeZone) ? _appTenant.TimeZoneInfo() : TimeZoneInfoResolver.ResolveWindows(plan.TimeZone);
            Func<DateTime, DateTime> fnUtc = dt => TimeZoneInfo.ConvertTime(dt, planTimezone, TimeZoneInfo.Utc);
            Func<DateTime, DateTime> fnPlanLocal = dt => TimeZoneInfo.ConvertTime(dt, TimeZoneInfo.Utc, planTimezone);

            var planStartLocal = fnPlanLocal(DateTime.SpecifyKind(plan.Start, DateTimeKind.Utc)).Date;
            var planEndLocal = fnPlanLocal(DateTime.SpecifyKind(plan.End, DateTimeKind.Utc)).Date.AddDays(1);

            Func<DateTime, DateTime> fnNextBilling = null;
            Func<DateTime, DateTime, int, decimal, decimal> fnCostPerPeriod = CostPerDay;
            var costPerPeriod = plan.CostPerDay ?? 0M;
            DateTime billingDateLocal = planStartLocal;

            switch (plan._period)
            {
                case PaymentPlanPeriod.None:
                    break;

                case PaymentPlanPeriod.Monthly:
                    if (plan.CostPerMonth.HasValue)
                    {
                        fnCostPerPeriod = CostPerMonth;
                        costPerPeriod = plan.CostPerMonth ?? 0M;
                    }
                    fnNextBilling = dt => dt.AddMonths(1);
                    if (plan.BillingDayOfPeriod.HasValue)
                    {
                        plan.BillingDayOfPeriod = Math.Max(plan.BillingDayOfPeriod ?? 1, 1);

                        // We have to do our calculations in User Local Time since it is likely BillingDayOfPeriod is 1st
                        // and we could end with UTC billingDate as last day of previous month
                        billingDateLocal = new DateTime(planStartLocal.Year, planStartLocal.Month, plan.BillingDayOfPeriod.Value, 0, 0, 0, DateTimeKind.Unspecified);
                        if (billingDateLocal < planStartLocal)
                            billingDateLocal = billingDateLocal.AddMonths(1);
                    }
                    else
                    {
                        plan.BillingDayOfPeriod = billingDateLocal.Day;
                    }
                    break;

                case PaymentPlanPeriod.Weekly:
                    fnNextBilling = dt => dt.AddDays(7);
                    if (plan.BillingDayOfPeriod.HasValue)
                    {
                        billingDateLocal = planStartLocal.AddDays(plan.BillingDayOfPeriod.Value - (int)planStartLocal.DayOfWeek);
                    }
                    else
                    {
                        plan.BillingDayOfPeriod = (int)billingDateLocal.DayOfWeek;
                    }
                    break;

                case PaymentPlanPeriod.Fortnightly:
                    fnNextBilling = dt => dt.AddDays(14);
                    if (plan.BillingDayOfPeriod.HasValue)
                    {
                        billingDateLocal = planStartLocal.AddDays(plan.BillingDayOfPeriod.Value - (int)planStartLocal.DayOfWeek);
                    }
                    else
                    {
                        plan.BillingDayOfPeriod = (int)billingDateLocal.DayOfWeek;
                    }
                    break;

                case PaymentPlanPeriod.Yearly:
                    fnNextBilling = dt => dt.AddYears(1);
                    if (plan.BillingDayOfPeriod.HasValue)
                    {
                        billingDateLocal = planStartLocal.AddDays(plan.BillingDayOfPeriod.Value - planStartLocal.DayOfYear);
                    }
                    else
                    {
                        plan.BillingDayOfPeriod = billingDateLocal.DayOfYear;
                    }
                    break;

                case PaymentPlanPeriod.Quarterly:
                    if (plan.CostPerMonth.HasValue)
                    {
                        fnCostPerPeriod = CostPerMonth;
                        costPerPeriod = plan.CostPerMonth ?? 0M;
                    }
                    fnNextBilling = dt => dt.AddMonths(3);
                    if (plan.BillingDayOfPeriod.HasValue)
                    {
                        plan.BillingDayOfPeriod = Math.Max(plan.BillingDayOfPeriod ?? 1, 1);
                        // We have to do our calculations in User Local Time since it is likely BillingDayOfPeriod is 1st
                        // and we could end with UTC billingDate as last day of previous month
                        billingDateLocal = new DateTime(planStartLocal.Year, 1, plan.BillingDayOfPeriod.Value, 0, 0, 0, DateTimeKind.Unspecified);
                        while (billingDateLocal < planStartLocal)
                            billingDateLocal = billingDateLocal.AddMonths(3);
                    }
                    else
                    {
                        plan.BillingDayOfPeriod = billingDateLocal.Day;
                    }
                    break;

                case PaymentPlanPeriod.Daily:
                    fnNextBilling = dt => dt.AddDays(1);
                    plan.BillingDayOfPeriod = 0;
                    break;
            }

            var billingDay = Math.Max(plan.BillingDayOfPeriod ?? 1, 1);
            while (billingDateLocal < planStartLocal)
                billingDateLocal = fnNextBilling(billingDateLocal);

            // Should the 1st payment be at least 1 full period
            if (plan.FirstPaymentIsFullPeriod)
            {
                billingDateLocal = fnNextBilling(billingDateLocal);
            }

            // Pay in Advance logic ..
            plan.FirstBillingDateUtc = fnUtc(billingDateLocal);

            // Handle first payment 
            if (plan._period == PaymentPlanPeriod.None || planStartLocal < billingDateLocal)
            {
                if (plan._period == PaymentPlanPeriod.None || planEndLocal < billingDateLocal)
                {
                    // 1. This is 1 payment
                    list.Add(new PendingPayment()
                    {
                        DueDateUtc = utcNow,          // Due now
                        FullAmount = decimal.Round(fnCostPerPeriod(planStartLocal, planEndLocal, billingDay, costPerPeriod), 2),
                        FromUtc = fnUtc(planStartLocal),
                        ToUtc = fnUtc(planEndLocal),
                        Description = string.Format(description, planStartLocal, planEndLocal.AddDays(-1)),
                        Quantity = fnCostPerPeriod(planStartLocal, planEndLocal, billingDay, 1M)
                    });
                }
                else
                {
                    // 2, 3.  Pay upto first billing period (ie pay 1st month in advance)
                    list.Add(new PendingPayment()
                    {
                        DueDateUtc = utcNow,          // Due now
                        FullAmount = decimal.Round(fnCostPerPeriod(planStartLocal, billingDateLocal, billingDay, costPerPeriod), 2),
                        FromUtc = fnUtc(planStartLocal),
                        ToUtc = fnUtc(billingDateLocal),
                        Description = string.Format(description, planStartLocal, billingDateLocal.AddDays(-1)),
                        Quantity = fnCostPerPeriod(planStartLocal, billingDateLocal, billingDay, 1M)
                    });
                }
            }
            if (plan._period != PaymentPlanPeriod.None)
            {
                // Handle full period payments
                var nextBillingDateLocal = fnNextBilling(billingDateLocal);
                while (planEndLocal >= nextBillingDateLocal)
                {
                    plan.LastBillingDateUtc = fnUtc(billingDateLocal);
                    list.Add(new PendingPayment()
                    {
                        DueDateUtc = fnUtc(billingDateLocal),
                        FullAmount = decimal.Round(fnCostPerPeriod(billingDateLocal, nextBillingDateLocal, billingDay, costPerPeriod), 2),
                        FromUtc = fnUtc(billingDateLocal),
                        ToUtc = fnUtc(nextBillingDateLocal),
                        Description = string.Format(description, billingDateLocal, nextBillingDateLocal.AddDays(-1)),
                        Quantity = 1M
                    });
                    billingDateLocal = nextBillingDateLocal;
                    nextBillingDateLocal = fnNextBilling(nextBillingDateLocal);
                }

                // Handle trailing amount
                if (planEndLocal > billingDateLocal)
                {
                    plan.LastBillingDateUtc = fnUtc(billingDateLocal);

                    // Note the .AddDays(1) to planEnd below since plan.End is inclusive but fnCostPerPeriod expects To is Exclusive
                    list.Add(new PendingPayment()
                    {
                        DueDateUtc = fnUtc(billingDateLocal),
                        FullAmount = decimal.Round(fnCostPerPeriod(billingDateLocal, planEndLocal, billingDay, costPerPeriod), 2),
                        FromUtc = fnUtc(billingDateLocal),
                        ToUtc = fnUtc(planEndLocal),
                        Description = string.Format(description, billingDateLocal, planEndLocal.AddDays(-1)),
                        Quantity = fnCostPerPeriod(billingDateLocal, planEndLocal, billingDay, 1M)
                    });
                }

                var orderBy = 0;
                foreach (var pp in list)
                {
                    pp.Status = (int)(orderBy == 0 ? PendingPaymentStatus.DueNow : PendingPaymentStatus.Inactive);
                    pp.StatusDateUtc = utcNow;
                    pp.DueDateUtc = orderBy == 0 ? utcNow : pp.DueDateUtc;
                    pp.OrderBy = orderBy++;
                    pp.OrderId = plan.Order.Id;
                    pp.UserId = userId;
                    pp.Currency = plan.Currency;
                    pp.DueAmount = pp.FullAmount;
                    pp.DiscountAmount = 0;
                }
                var totalPlanned = list.Sum(i => i.DueAmount);
                if (plan._totalCostRounding != TotalCostRounding.None && totalPlanned != plan.Order.TotalPrice)
                {
                    var roundingPlan = plan._totalCostRounding == TotalCostRounding.FirstPayment ? list.First() : list.Last();
                    roundingPlan.FullAmount += plan.Order.TotalPrice - totalPlanned;
                    roundingPlan.DueAmount = roundingPlan.FullAmount;
                }
            }

            var result = new PendingPayments()
            {
                Plan = plan,
                PeriodQuantity = list.Sum(i => i.Quantity.Value),
                TotalCost = list.Sum(i => i.DueAmount),
                MaxCoupons = plan.Order.MaxCoupons
            };
            result.Payments.AddRange(list.ToArray());

            gateway.Prepare(context, result, gatewayContext);

            return result;
        }

        public void Save(PendingPayments pendingPayments)
        {
            foreach (PendingPayment pp in pendingPayments.Payments)
            {
                var dto = _pendingPaymentRepo.GetPendingPaymentById(pp.Id, RepositoryInterfaces.Enums.PendingPaymentLoadInstructions.None);
                if (dto == null)
                {
                    dto = new PendingPaymentDTO()
                    {
                        __IsNew = true,
                        Id = pp.Id,
                        OrderBy = pp.OrderBy,
                        CreatedDateUtc = _dateTimeProvider.UtcNow,
                        GatewayName = pp.GatewayName,
                        Sandbox = pp.Sandbox,
                        OrderId = pp.OrderId,
                        UserId = pp.UserId
                    };
                }
                dto.Status = (int)pp.Status;
                dto.StatusDateUtc = pp.StatusDateUtc;
                dto.PaidInFullDateUtc = pp.PaidInFullDateUtc;
                dto.CancelledDateUtc = pp.CancelledDateUtc;
                dto.LastFailedDateUtc = pp.LastFailedDateUtc;
                dto.Currency = pp.Currency ?? "AUD";
                dto.DueAmount = pp.DueAmount;
                dto.FullAmount = pp.FullAmount;
                dto.DiscountAmount = pp.DiscountAmount;
                dto.DueDateUtc = pp.DueDateUtc;
                dto.Description = pp.Description;
                dto.FromUtc = pp.FromUtc;
                dto.ToUtc = pp.ToUtc;
                dto.Quantity = pp.Quantity;
                dto.GatewayContext = pp.GatewayContext;
                dto.InvoiceNumber = pp.InvoiceNumber;

                _pendingPaymentRepo.Save(dto, true, false);
            }
        }

        public void Save(PendingPayment pp)
        {
            Save(pp, false);
        }

        public void Save(PendingPayment pp, bool createIfNotFound)
        {
            var dto = _pendingPaymentRepo.GetPendingPaymentById(pp.Id, RepositoryInterfaces.Enums.PendingPaymentLoadInstructions.None);
            if (dto == null)
            {
                if (!createIfNotFound)
                {
                    return;
                }

                dto = new PendingPaymentDTO()
                {
                    __IsNew = true,
                    Id = pp.Id,
                    OrderBy = pp.OrderBy,
                    CreatedDateUtc = _dateTimeProvider.UtcNow,
                    GatewayName = pp.GatewayName,
                    Sandbox = pp.Sandbox,
                    OrderId = pp.OrderId,
                    UserId = pp.UserId
                };
            }
            dto.Status = (int)pp.Status;
            dto.StatusDateUtc = pp.StatusDateUtc;
            dto.PaidInFullDateUtc = pp.PaidInFullDateUtc;
            dto.CancelledDateUtc = pp.CancelledDateUtc;
            dto.LastFailedDateUtc = pp.LastFailedDateUtc;
            dto.Currency = pp.Currency ?? "AUD";
            dto.DueAmount = pp.DueAmount;
            dto.FullAmount = pp.FullAmount;
            dto.DiscountAmount = pp.DiscountAmount;
            dto.DueDateUtc = pp.DueDateUtc;
            dto.Description = pp.Description;
            dto.FromUtc = pp.FromUtc;
            dto.ToUtc = pp.ToUtc;
            dto.Quantity = pp.Quantity;
            dto.GatewayContext = pp.GatewayContext;
            dto.InvoiceNumber = pp.InvoiceNumber;

            _pendingPaymentRepo.Save(dto, true, false);
        }


        public void Refresh(PendingPayments pendingPayments)
        {
            if (pendingPayments.Plan?.Order?.Id == null)
                return;

            var fresh = _pendingPaymentRepo.GetByOrderId(pendingPayments.Plan.Order.Id, RepositoryInterfaces.Enums.PendingPaymentLoadInstructions.PaymentResponses);
            var list = new ListObjectProvider();
            foreach (var dto in fresh)
            {
                list.Add(new PendingPayment(dto));
            }
            pendingPayments.Payments = list;
        }

        public PendingPayment GetPendingPayment(IContextContainer context, Guid pendingPaymentId)
        {
            var dto = _pendingPaymentRepo.GetPendingPaymentById(pendingPaymentId, RepositoryInterfaces.Enums.PendingPaymentLoadInstructions.None);
            return dto == null ? null : new PendingPayment(dto);
        }

        public PaymentResponse GetPaymentResponse(IContextContainer context, Guid paymentResponseId)
        {
            var dto = _paymentResponseRepo.GetPaymentResponseById(paymentResponseId, RepositoryInterfaces.Enums.PaymentResponseLoadInstructions.None);
            return dto == null ? null : new PaymentResponse(dto);
        }

        public PaymentResponse GetPaymentResponseForTransactionId(IContextContainer context, string gatewayName, bool sandbox, string transactionId)
        {
            var dto = _paymentResponseRepo.GetPaymentResponseForTransactionId(gatewayName, sandbox, transactionId, RepositoryInterfaces.Enums.PaymentResponseLoadInstructions.None);
            return dto == null ? null : new PaymentResponse(dto);
        }

        public PaymentResponse GetPaymentResponseForTransactionId(IContextContainer context, IPaymentGatewayFacility facility, string transactionId)
        {
            var dto = _paymentResponseRepo.GetPaymentResponseForTransactionId(facility.GatewayName, facility.UseSandbox, transactionId, RepositoryInterfaces.Enums.PaymentResponseLoadInstructions.None);
            return dto == null ? null : new PaymentResponse(dto);
        }

        public ListObjectProvider GetPaymentResponses(IContextContainer context, Guid pendingPaymentId)
        {
            var dtos = _paymentResponseRepo.GetPaymentResponsesForPendingPayment(pendingPaymentId, RepositoryInterfaces.Enums.PaymentResponseLoadInstructions.None);
            return dtos == null ? null : new ListObjectProvider(from dto in dtos select new PaymentResponse(dto));
        }
    }
}
