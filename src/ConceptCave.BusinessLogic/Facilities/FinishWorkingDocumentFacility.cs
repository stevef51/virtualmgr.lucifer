﻿using ConceptCave.BusinessLogic.ReportData;
using ConceptCave.Checklist.Facilities;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;

namespace ConceptCave.BusinessLogic.Facilities
{

    public class FinishWorkingDocumentFacility : Facility
    {
        private readonly IReportingTableWriter _tWriter;

        public FinishWorkingDocumentFacility(IReportingTableWriter tWriter)
        {
            _tWriter = tWriter;
        }

        public class Action : Node, IFacilityAction, IServerBasedAction
        {
            private readonly FinishWorkingDocumentFacility _outer;

            public Action(FinishWorkingDocumentFacility outer)
            {
                _outer = outer;
            }

            #region IServerBasedAction Members

            public void ServerExecute(ConceptCave.Core.IContextContainer context)
            {
                var program = context.Get<ILingoProgram>();
                _outer._tWriter.BreakOutDocumentIntoReportingTables(program.WorkingDocument);
            }

            #endregion
        }

        public void AddAction(ConceptCave.Core.IContextContainer context)
        {
            AddServerBasedAction(context, new Action(this));
        }
    }
}
