﻿using ConceptCave.Checklist.Facilities;
using ConceptCave.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Core.Coding;
using ConceptCave.Checklist;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.Checklist.Lingo.ObjectProviders;
using ConceptCave.BusinessLogic.Questions;
using ConceptCave.Checklist.Interfaces;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json;

namespace ConceptCave.BusinessLogic.Facilities
{
    public class OrderFacilityOrder : Node
    {
        public int FacilityStructureId { get; set; }
        public int ProductCatalogId { get; set; }
        public Guid OrderedByUserId { get; set; }
        public object TaskId { get; set; }
        [JsonConverter(typeof(JsonUTCDateTimeConverter))]
        public DateTime? PendingExpiryDate { get; set; }
        public int MaxCoupons { get; set; }
        public Guid? ExtensionOfOrderId { get; set; }
        [JsonConverter(typeof(JsonUTCDateTimeConverter))]
        public DateTime? CancelledDateUtc { get; set; }

        public IList<OrderFacilityOrderItem> Items { get; protected set; }

        public OrderFacilityOrder() : base()
        {
            Items = new List<OrderFacilityOrderItem>();
        }

        public OrderFacilityOrder(OrderDTO dto)
        {
            FacilityStructureId = dto.FacilityStructureId;
            ProductCatalogId = dto.ProductCatalogId;
            OrderedByUserId = dto.OrderedById;
            TaskId = dto.ProjectJobTaskId;
            PendingExpiryDate = dto.PendingExpiryDate;
            MaxCoupons = dto.MaxCoupons;
            ExtensionOfOrderId = dto.ExtensionOfOrderId;
            CancelledDateUtc = dto.CancelledDateUtc;
            Items = new List<OrderFacilityOrderItem>();
            foreach(var oi in dto.OrderItems)
            {
                Items.Add(new OrderFacilityOrderItem()
                {
                    EndDate = oi.EndDate,
                    MinStockLevel = oi.MinStockLevel,
                    Notes = oi.Notes,
                    Price = oi.Price,
                    ProductId = oi.ProductId,
                    Quantity = oi.Quantity,
                    StartDate = oi.StartDate,
                    Stock = oi.Stock,
                    TotalPrice = oi.TotalPrice
                });
            }
        }

        public OrderFacilityOrderItem NewItem(OrderFacilityProductCatalogItem item)
        {
            OrderFacilityOrderItem result = new OrderFacilityOrderItem(item);

            return result;
        }
        public OrderFacilityOrderItem NewItem()
        {
            var i = new OrderFacilityOrderItem();
            Items.Add(i);
            return i;
        }

        public override void Encode(IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode<int>("FacilityStructureId", FacilityStructureId);
            encoder.Encode<int>("ProductCatalogId", ProductCatalogId);
            encoder.Encode<Guid>("OrderedByUserId", OrderedByUserId);
            encoder.Encode<object>("TaskId", TaskId);
            encoder.Encode("PendingExpiryDate", PendingExpiryDate);
            encoder.Encode<IList<OrderFacilityOrderItem>>("Items", Items);
            encoder.Encode("MaxCoupons", MaxCoupons);
            encoder.Encode("ExtensionOfOrderId", ExtensionOfOrderId);
        }

        public override void Decode(IDecoder decoder)
        {
            base.Decode(decoder);

            FacilityStructureId = decoder.Decode<int>("FacilityStructureId");
            ProductCatalogId = decoder.Decode<int>("ProductCatalogId");
            OrderedByUserId = decoder.Decode<Guid>("OrderedByUserId");
            TaskId = decoder.Decode<Guid?>("TaskId");
            PendingExpiryDate = decoder.Decode<DateTime?>("PendingExpiryDate");
            Items = decoder.Decode<IList<OrderFacilityOrderItem>>("Items");
            MaxCoupons = decoder.Decode<int>("MaxCoupons");
            ExtensionOfOrderId = decoder.Decode<Guid?>("ExtensionOfOrderId");
        }

        public decimal TotalPrice
        {
            get
            {
                decimal tp = 0;

                foreach(var item in Items)
                {
                    if(item.Price.HasValue == false)
                    {
                        continue;
                    }

                    tp += item.TotalPrice.Value;
                }

                return Math.Round(tp, 2);
            }
        }

        internal OrderDTO ToDTO()
        {
            OrderDTO result = new OrderDTO()
            {
                __IsNew = true,
                Id = this.Id,
                DateCreated = DateTime.UtcNow,
                ProductCatalogId = ProductCatalogId,
                OrderedById = OrderedByUserId,
                FacilityStructureId = FacilityStructureId,
                ProjectJobTaskId = (Guid?)TaskId,
                PendingExpiryDate = PendingExpiryDate,
                MaxCoupons = MaxCoupons,
                ExtensionOfOrderId = ExtensionOfOrderId
            };

            foreach (var i in Items)
            {
                var item = new OrderItemDTO()
                {
                    __IsNew = true,
                    Id = i.Id,
                    Quantity = i.Quantity,
                    Stock = i.Stock,
                    MinStockLevel = i.MinStockLevel,
                    Price = i.Price,
                    TotalPrice = i.TotalPrice,
                    ProductId = i.ProductId,
                    Notes = i.Notes,
                    StartDate = (DateTime?)i.StartDate,
                    EndDate = (DateTime?)i.EndDate
                };

                result.OrderItems.Add(item);
            }

            decimal? tp = TotalPrice;

            result.TotalPrice = tp == 0 ? null : tp;

            return result;
        }
    }

    public class OrderFacilityOrderItem : Node
    {
        public decimal Quantity { get; set; }
        public decimal? Stock { get; set; }
        public decimal? MinStockLevel { get; set; }
        public decimal? Price { get; set; }
        private decimal? _totalPrice;
        public decimal? TotalPrice
        {
            get
            {
                if (_totalPrice.HasValue)
                {
                    return _totalPrice.Value;
                }
                return Price.HasValue == false ? null : Quantity * Price;
            }
            set
            {
                // Allow setting TotalPrice directly which avoids rounding issues when Quantity is fractional
                _totalPrice = value;
            }
        }
        public int ProductId { get; set; }
        public string Notes { get; set; }

        protected DateTime? _startDate;

        public object StartDate
        {
            get
            {
                return _startDate;
            }
            set
            {
                DateTime? v = null;

                if(value is DateTime)
                {
                    v = (DateTime)value;
                }
                else if(value is DateTime?)
                {
                    v = (DateTime?)value;
                }
                else if(value is IDateTimeAnswer)
                {
                    v = ((IDateTimeAnswer)value).AnswerDateTime;
                }

                _startDate = v;
            }
        }

        protected DateTime? _endDate;
        public object EndDate
        {
            get
            {
                return _endDate;
            }
            set
            {
                DateTime? v = null;

                if (value is DateTime)
                {
                    v = (DateTime)value;
                }
                else if (value is DateTime?)
                {
                    v = (DateTime?)value;
                }
                else if (value is IDateTimeAnswer)
                {
                    v = ((IDateTimeAnswer)value).AnswerDateTime;
                }

                _endDate = v;
            }
        }

        public OrderFacilityOrderItem(OrderFacilityProductCatalogItem item) : base()
        {
            MinStockLevel = item.MinStockLevel;
            Price = item.Price;
            ProductId = item.ProductId;
        }

        public OrderFacilityOrderItem()
        {

        }

        public override void Encode(IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode<decimal>("Quantity", Quantity);
            encoder.Encode<decimal?>("Stock", Stock);
            encoder.Encode<decimal?>("MinStockLevel", MinStockLevel);
            encoder.Encode<decimal?>("Price", Price);
            encoder.Encode<int>("ProductId", ProductId);
            encoder.Encode<string>("Notes", Notes);
            encoder.Encode<object>("StartDate", StartDate);
            encoder.Encode<object>("EndDate", EndDate);
            encoder.Encode<decimal?>("TotalPrice", _totalPrice);
        }

        public override void Decode(IDecoder decoder)
        {
            base.Decode(decoder);

            Quantity = decoder.Decode<decimal>("Quantity");
            Stock = decoder.Decode<decimal?>("Stock");
            MinStockLevel = decoder.Decode<decimal?>("MinStockLevel");
            Price = decoder.Decode<decimal>("Price");
            ProductId = decoder.Decode<int>("ProductId");
            Notes = decoder.Decode<string>("Notes");
            StartDate = decoder.Decode<DateTime?>("StartDate");
            EndDate = decoder.Decode<DateTime?>("EndDate");
            _totalPrice = decoder.Decode<decimal?>("TotalPrice");
        }
    }

    public class OrderFaciltyProductCatalogList : ListObjectProvider
    {
        public OrderFacilityProductCatalog FindCatalog(object key)
        {
            OrderFacilityProductCatalog result = null;
            if (key is string)
            {
                var ci = (from i in this.Cast<OrderFacilityProductCatalog>() where i.Name.ToLower() == ((string)key).ToLower() select i);

                if (ci.Count() > 0)
                {
                    result = ci.First();
                }
            }
            else if (key is int)
            {
                var ci = (from i in this.Cast<OrderFacilityProductCatalog>() where i.CatalogId == (int)key select i);

                if (ci.Count() > 0)
                {
                    result = ci.First();
                }
            }

            return result;
        }
    }

    public class OrderFacilityProductCatalog : Node
    {
        public int FacilityStructureId { get; set; }
        public int CatalogId { get; set; }
        public string Name { get; set; }

        public OrderFaciltyProductCatalogItemList Items { get; protected set; }

        public OrderFacilityProductCatalog() : base()
        {
            Items = new OrderFaciltyProductCatalogItemList();
        }

        public OrderFacilityProductCatalog(ProductCatalogDTO catalog): this()
        {
            FromDto(catalog);
        }

        public OrderFacilityProductCatalogItem FindCatalogItem(object key)
        {
            return Items.FindCatalogItem(key);
        }

        public override void Encode(IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode<int>("FacilityStructureId", FacilityStructureId);
            encoder.Encode<int>("CatalogId", CatalogId);
            encoder.Encode<string>("Name", Name);
            encoder.Encode<OrderFaciltyProductCatalogItemList>("Items", Items);
        }

        public override void Decode(IDecoder decoder)
        {
            base.Decode(decoder);

            FacilityStructureId = decoder.Decode<int>("FacilityStructureId");
            CatalogId = decoder.Decode<int>("CatalogId");
            Name = decoder.Decode<string>("Name");
            Items = decoder.Decode<OrderFaciltyProductCatalogItemList>("Items");
        }

        internal void FromDto(ProductCatalogDTO catalog)
        {
            FacilityStructureId = catalog.FacilityStructureId;
            CatalogId = catalog.Id;
            Name = catalog.Name;

            foreach(var i in catalog.ProductCatalogItems)
            {
                OrderFacilityProductCatalogItem item = new OrderFacilityProductCatalogItem(i);

                Items.Add(item);
            }
        }
    }

    public class OrderFaciltyProductCatalogItemList : ListObjectProvider
    {
        public OrderFacilityProductCatalogItem FindCatalogItem(object key)
        {
            OrderFacilityProductCatalogItem result = null;
            if (key is string)
            {
                var ci = (from i in this.Cast<OrderFacilityProductCatalogItem>() where i.Name.ToLower() == ((string)key).ToLower() select i);

                if (ci.Count() > 0)
                {
                    result = ci.First();
                }
            }
            else if (key is int)
            {
                var ci = (from i in this.Cast<OrderFacilityProductCatalogItem>() where i.ProductId == (int)key select i);

                if (ci.Count() > 0)
                {
                    result = ci.First();
                }
            }

            return result;
        }
    }

    public class OrderFacilityProductCatalogItem : Node
    {
        public int ProductId { get; set; }
        public string Name { get; set; }
        public decimal? Price { get; set; }
        public decimal? MinStockLevel { get; set; }

        public OrderFacilityProductCatalogItem() : base() { }

        public OrderFacilityProductCatalogItem(ProductCatalogItemDTO item) : this()
        {
            FromDto(item);
        }

        internal void FromDto(ProductCatalogItemDTO item)
        {
            ProductId = item.ProductId;
            Name = item.Product.Name;
            Price = item.Price.HasValue ? item.Price : item.Product.Price;
            MinStockLevel = item.MinStockLevel;
        }
    }

    public class OrderFacility : TaskAwareFacility
    {
        protected IOrderRepository _orderRepo;
        protected IProductCatalogRepository _productCatalogRepo;

        public OrderFacility(IOrderRepository orderRepo, IProductCatalogRepository productCatalogRepo)
        {
            Name = "Order";
            _orderRepo = orderRepo;
            _productCatalogRepo = productCatalogRepo;
        }

        protected int? GetFacilityStructureId(object facilityStructureId)
        {
            int id = 0;

            if (facilityStructureId is int)
            {
                id = (int)facilityStructureId;
            }
            else if (facilityStructureId is SelectFacilityStructureAnswer)
            {
                var answ = (SelectFacilityStructureAnswer)facilityStructureId;
                if (answ.SelectedStructureId.HasValue)
                {
                    id = answ.SelectedStructureId.Value;
                }
                else
                {
                    return null;
                }
            }

            return id;
        }

        public OrderFaciltyProductCatalogList CatalogsForFacilityStructure(object facilityStructureId)
        {
            int? id = GetFacilityStructureId(facilityStructureId);

            if(id.HasValue == false)
            {
                return null;
            }

            var r = new List<ProductCatalogDTO>();
            var rules = new List<ProductCatalogFacilityStructureRuleDTO>();
            _productCatalogRepo.GetCatalogsForFacilityStructure(id.Value, r, rules);

            OrderFaciltyProductCatalogList result = new OrderFaciltyProductCatalogList();

            foreach(var c in (from p in r where p.Excluded == false select p))
            {
                OrderFacilityProductCatalog catalog = new OrderFacilityProductCatalog(c);
                result.Add(catalog);
            }

            return result;
        }

        public OrderFacilityOrder ExtendOrder(object orderObj)
        {
            Guid orderId = Guid.Empty;
            if (orderObj is string)
                orderId = Guid.Parse(orderObj.ToString());
            else if (orderObj is OrderFacilityOrder)
                orderId = ((OrderFacilityOrder)orderObj).Id;
            else if (orderObj is Guid)
                orderId = (Guid)orderObj;
            else
                return null;

            var dto = _orderRepo.GetById(orderId, RepositoryInterfaces.Enums.OrderLoadInstructions.None);
            if (dto == null)
            {
                return null;
            }
            var result = new OrderFacilityOrder()
            {
                FacilityStructureId = dto.FacilityStructureId,
                OrderedByUserId = dto.OrderedById,
                MaxCoupons = dto.MaxCoupons,
                ProductCatalogId = dto.ProductCatalogId,
                ExtensionOfOrderId = dto.Id
            };
            if (HasCurrentTask)
            {
                result.TaskId = CurrentTaskId;
            }
            return result;
        }

        public OrderFacilityOrder GetById(Guid id)
        {
            var dto = _orderRepo.GetById(id, RepositoryInterfaces.Enums.OrderLoadInstructions.Items);
            if (dto == null)
            {
                return null;
            }
            return new OrderFacilityOrder(dto);
        }

        public OrderFacilityOrder NewOrder(object facilityStructureId, object productCatalogId, Guid orderedBy)
        {
            int? id = GetFacilityStructureId(facilityStructureId);

            if(id.HasValue == false)
            {
                return null;
            }

            int pcId = 0;

            if(productCatalogId is int)
            {
                // easy
                pcId = (int)productCatalogId;
            }
            else if(productCatalogId is string)
            {
                // more difficult
                var catalogs = CatalogsForFacilityStructure(id.Value);
                var catalog = catalogs.FindCatalog((string)productCatalogId);

                if(catalog == null)
                {
                    return null;
                }

                pcId = catalog.CatalogId;
            }
            else if(productCatalogId is OrderFacilityProductCatalog)
            {
                pcId = ((OrderFacilityProductCatalog)productCatalogId).CatalogId;
            }

            var result = new OrderFacilityOrder()
            {
                FacilityStructureId = id.Value,
                ProductCatalogId = pcId,
                OrderedByUserId = orderedBy
            };

            if(HasCurrentTask)
            {
                result.TaskId = CurrentTaskId;
            }

            return result;
        }

        public void Save(OrderFacilityOrder order)
        {
            var dto = order.ToDTO();

            _orderRepo.Save(dto, false, true);
        }

        private Guid GetOrderId(object order)
        {
            if (order is Guid)
            {
                return (Guid)order;
            }
            else if (order is OrderFacilityOrder)
            {
                return ((OrderFacilityOrder)order).Id;
            }
            else if (order is string)
            {
                return Guid.Parse(order.ToString());
            }
            return Guid.Empty;
        }
        public void ClearPendingExpiry(object order)
        {
            var id = GetOrderId(order);
            var dto = _orderRepo.GetById(id, RepositoryInterfaces.Enums.OrderLoadInstructions.None);
            if (dto != null && dto.PendingExpiryDate.HasValue)
            {
                dto.PendingExpiryDate = null;
                _orderRepo.Save(dto, false, false);
            }
        }

        public void SetWorkflowState(object order, string state)
        {
            var id = GetOrderId(order);
            var dto = _orderRepo.GetById(id, RepositoryInterfaces.Enums.OrderLoadInstructions.None);
            if (dto != null)
            {
                dto.WorkflowState = state;
                _orderRepo.Save(dto, false, false);
            }
        }
    }
}
