﻿using ConceptCave.Checklist.Facilities;
using ConceptCave.Checklist.Lingo.ObjectProviders;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.BusinessLogic.Facilities
{
    public class FacilityStructureFacility : Facility
    {
        protected IFacilityStructureRepository _facRepo;
        protected IHierarchyRepository _hierarchyRepo;

        public FacilityStructureFacility(IFacilityStructureRepository facRepo, IHierarchyRepository hierarchyRepo)
        {
            Name = "FacilityStructure";

            _facRepo = facRepo;
            _hierarchyRepo = hierarchyRepo;
        }

        public DictionaryObjectProvider GetStructure(object id)
        {
            int sid = -1;

            if (id is int)
            {
                sid = (int)id;
            }
            else
            {
                sid = int.Parse(id.ToString());
            }

            var s = _facRepo.GetById(sid);

            return BuildFacilityStructure(s);
        }

        public DictionaryObjectProvider GetParent(object item, string parentType)
        {
            FacilityStructureType pt = FacilityStructureType.Site;

            if (Enum.TryParse(parentType, out pt) == false)
            {
                return null;
            }

            var parent = _facRepo.GetDeepParent(int.Parse(item.ToString()), pt);

            return BuildFacilityStructure(parent);
        }

        protected DictionaryObjectProvider BuildFacilityStructure(FacilityStructureDTO item)
        {
            if (item == null)
            {
                return null;
            }

            DictionaryObjectProvider result = new DictionaryObjectProvider();
            result.SetValue(null, "Id", item.Id);
            result.SetValue(null, "ParentId", item.ParentId);
            result.SetValue(null, "Name", item.Name);
            result.SetValue(null, "Description", item.Description);

            return result;
        }

        public bool IsChildOf(object child, object parent)
        {
            int? childId = null;
            int? parentId = null;

            if (child is int)
            {
                childId = (int)child;
            }
            else if (child is string)
            {
                var s = _facRepo.FindByName(null, (string)child);

                if (s != null)
                {
                    childId = s.Id;
                }
            }

            if (parent is int)
            {
                parentId = (int)parent;
            }
            else if (parent is string)
            {
                var s = _facRepo.FindByName(null, (string)parent);

                if (s != null)
                {
                    parentId = s.Id;
                }
            }

            if (childId.HasValue == false || parentId.HasValue == false)
            {
                return false;
            }

            return ChildOf(childId.Value, parentId.Value);
        }

        protected bool ChildOf(int child, int parent)
        {
            var p = _facRepo.GetById(parent);

            if (p == null)
            {
                return false;
            }

            var s = _facRepo.GetDeepParent(child, (FacilityStructureType)p.StructureType);

            return s.Id == p.Id;
        }

        public object UserIdWithRole(object facilityStructure, object role)
        {
            var users = UserIdsWithRole(facilityStructure, role);
            if (users == null)
            {
                return null;
            }
            return ((Guid[])users).FirstOrDefault();
        }

        public object UserIdsWithRole(object facilityStructure, object role)
        {
            FacilityStructureDTO fsDto = null;
            if (facilityStructure is int)
            {
                fsDto = _facRepo.GetById((int)facilityStructure);
            }
            else if (facilityStructure is string)
            {
                fsDto = _facRepo.FindByName(null, (string)facilityStructure);
            }

            if (fsDto == null)
            {
                return null;
            }

            HierarchyRoleDTO roleDto = null;
            if (role is int)
            {
                roleDto = _hierarchyRepo.GetRoleById((int)role);
            }
            else if (role is string)
            {
                roleDto = _hierarchyRepo.GetRoleByName((string)role);
            }

            if (roleDto == null)
            {
                return null;
            }

            while (!fsDto.HierarchyId.HasValue && fsDto.ParentId.HasValue)
            {
                fsDto = _facRepo.GetById(fsDto.ParentId.Value, RepositoryInterfaces.Enums.FacilityStructureLoadInstructions.None);
                if (fsDto == null)
                {
                    return null;
                }
            }

            if (!fsDto.HierarchyId.HasValue)
            {
                return null;
            }
            var buckets = _hierarchyRepo.GetBucketsForHierarchy(fsDto.HierarchyId.Value, roleDto.Id, RepositoryInterfaces.Enums.HierarchyBucketLoadInstructions.None);

            return buckets?.Where(b => b.UserId.HasValue).Select(b => b.UserId.Value)?.ToArray();
        }
    }
}
