﻿using System;
using System.IO;
using ConceptCave.Checklist.Facilities;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;
using System.Collections;
using System.Collections.Generic;
using ConceptCave.Checklist.Lingo.ObjectProviders;

namespace ConceptCave.BusinessLogic.Facilities
{
    public class MediaFacility : Facility
    {
        private readonly IMediaManager _mediaMgr;

        public abstract class MediaItem : Node, ISizeableMediaItem
        {
            public virtual string Name { get; set; }

            public virtual string MediaType { get; set; }

            public abstract byte[] GetData(IContextContainer context);

            public virtual int? Width { get; set; }

            public virtual int? Height { get; set; }

            public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
            {
                base.Encode(encoder);

                encoder.Encode("Name", Name);
                encoder.Encode("MediaType", MediaType);
                encoder.Encode("Width", Width);
                encoder.Encode("Height", Height);
            }

            public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
            {
                base.Decode(decoder);

                Name = decoder.Decode<string>("Name");
                MediaType = decoder.Decode<string>("MediaType");
                Width = decoder.Decode<int?>("Width");
                Height = decoder.Decode<int?>("Height");
            }
        }

        /// <summary>
        /// Models media that is held in the working document
        /// </summary>
        public class EmbeddedMediaItem : MediaItem
        {
            protected byte[] Data { get; set; }

            public EmbeddedMediaItem() : base() { }

            public EmbeddedMediaItem(string data)
                : base()
            {
                Data = System.Text.UTF8Encoding.UTF8.GetBytes(data);
            }

            public EmbeddedMediaItem(byte[] data) : base()
            {
                Data = data;
            }

            public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
            {
                base.Encode(encoder);

                encoder.Encode("Data", Data);
            }

            public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
            {
                base.Decode(decoder);

                Data = decoder.Decode<byte[]>("Data");
            }

            public override byte[] GetData(IContextContainer context)
            {
                return Data;
            }

            public void SetData(byte[] data)
            {
                Data = data;
            }
        }

        /// <summary>
        /// Models media that is stored in the media library
        /// </summary>
        public class LibraryMediaItem : MediaItem
        {
            private readonly IMediaManager _mediaMgr;

            private IList<string> _labels;

            public LibraryMediaItem(IMediaManager mediaMgr)
            {
                _mediaMgr = mediaMgr;
            }

            public Guid MediaId { get; set; }

            public override string Name
            {
                get
                {
                    if (base.Name == null)
                    {
                        base.Name = _mediaMgr.MediaName(MediaId);                      
                    }
                    return base.Name;
                }
                set
                {
                    base.Name = value;
                }
            }

            public override string MediaType
            {
                get
                {
                    if (base.MediaType == null)
                    {
                        base.MediaType = _mediaMgr.MediaType(MediaId);
                    }
                    return base.MediaType;
                }
                set
                {
                    base.MediaType = value;
                }
            }

            /// <summary>
            /// Helper method for lingo to see if the media has a particular label on it
            /// </summary>
            /// <param name="label">Text of label to test for</param>
            /// <returns>True if label is present, otherwise false</returns>
            public bool HasLabel(string label)
            {
                if(_labels == null)
                {
                    _labels = _mediaMgr.MediaLabels(MediaId);
                }

                return _labels.Contains(label);
            }

            public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
            {
                base.Encode(encoder);

                encoder.Encode("MediaId", MediaId);
            }

            public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
            {
                base.Decode(decoder);

                MediaId = decoder.Decode<Guid>("MediaId");
            }

            public override byte[] GetData(IContextContainer context)
            {
                var mediaStream = _mediaMgr.GetStream(MediaId);
                var memoryStream = new MemoryStream();
                mediaStream.CopyTo(memoryStream);
                return memoryStream.ToArray();
            }
        }

        public MediaFacility(IMediaManager mediaMgr)
        {
            this.Name = "Media";
            _mediaMgr = mediaMgr;
        }

        public MediaItem GetMedia(string data)
        {
            return new EmbeddedMediaItem(data);
        }

        public MediaItem GetMediaById(object id)
        {
            return GetMediaById(id, null, null);
        }

        public MediaItem GetMediaById(object id, int? width)
        {
            return GetMediaById(id, width, null);
        }

        public MediaItem GetMediaById(object id, int? width, int? height)
        {
            Guid mediaId = Guid.Empty;

            if (id is Guid)
            {
                mediaId = (Guid)id;
            }
            else if (id is string)
            {
                mediaId = Guid.Parse((string)id);
            }
            else if (id is IHasMediaId)
            {
                mediaId = ((IHasMediaId)id).MediaId;
            }
            else
            {
                return null;
            }

            var result = new LibraryMediaItem(_mediaMgr) { MediaId = mediaId, Width = width, Height = height };

            return result;
        }

        public object GetMediaByMediaFileId(object id)
        {
            Guid mediaId = Guid.Empty;

            if (id is Guid)
            {
                mediaId = (Guid)id;
            }
            else if (id is string)
            {
                mediaId = Guid.Parse((string)id);
            }
            else if (id is IHasMediaId)
            {
                mediaId = ((IHasMediaId)id).MediaId;
            }
            else
            {
                return null;
            }

            return _mediaMgr.GetByMediaFileId(mediaId);
        }

        public object GetMediaByName(string name, string type)
        {
            if(string.IsNullOrEmpty(name))
            {
                return null;
            }

            return _mediaMgr.GetByName(name, type);
        }

        public object Save(IContextContainer context, object media)
        {
            if ((media is IMediaItem) == false)
            {
                return Guid.Empty;
            }

            IMediaItem item = (IMediaItem)media;

            Guid? id = null;

            if (media is LibraryMediaItem)
            {
                id = ((LibraryMediaItem)media).MediaId;
            }

            var memoryStream = new MemoryStream(item.GetData(context));

            return _mediaMgr.SaveStream(memoryStream, item.Name, item.MediaType, id, context.Get<IWorkingDocument>());
        }

        public void MapMediaToUser(IContextContainer context, Guid mediaId, Guid userId)
        {
            _mediaMgr.MapMediaToUser(mediaId, userId);
        }

        public void MapMediaToOrder(IContextContainer context, Guid mediaId, Guid orderId, string category = "")
        {
            _mediaMgr.MapMediaToOrder(mediaId, orderId, category);
        }

        public void WriteMediaViewingAccepted(IContextContainer context, bool accepted, string acceptionNotes)
        {
            var wd = context.Get<IWorkingDocument>();

            _mediaMgr.SetMediaViewingAccepted(wd.Id, accepted, acceptionNotes);
        }

        public void ResizeImage(ISizeableMediaItem smi, int width, int height)
        {
            if (smi == null)
                return;
            smi.Width = width;
            smi.Height = height;
        }

        public void ResizeImageWidth(IContextContainer context, ISizeableMediaItem smi, int width)
        {
            if (smi == null)
                return;

            var ms = new MemoryStream(smi.GetData(context));
            using (var img = System.Drawing.Image.FromStream(ms))
            {
                double ratio = (double)img.Height / (double)img.Width;
                smi.Width = width;
                smi.Height = (int)(width * ratio);
            }
        }
        public void ResizeImageHeight(IContextContainer context, ISizeableMediaItem smi, int height)
        {
            if (smi == null)
                return;

            var ms = new MemoryStream(smi.GetData(context));
            using (var img = System.Drawing.Image.FromStream(ms))
            {
                double ratio = (double)img.Width / (double)img.Height;
                smi.Height = height;
                smi.Width = (int)(height * ratio);
            }
        }

        protected void ExtractUserIds(IContextContainer context, object user, ref List<Guid> userIds)
        {
            if (user is Guid)
            {
                userIds.Add((Guid)user);
            }
            else if (user is string)
            {
                userIds.Add(Guid.Parse((string)user));
            }
            else if (user is DictionaryObjectProvider)
            {
                var d = (DictionaryObjectProvider)user;

                if(d.HasValueFor("userid"))
                {
                    ExtractUserIds(context, d.GetValue(context, "userid"), ref userIds);
                }
                else if(d.HasValueFor("id"))
                {
                    ExtractUserIds(context, d.GetValue(context, "id"), ref userIds);
                }
            }
            else if (user is ListObjectProvider)
            {
                foreach (var obj in ((ListObjectProvider)user))
                {
                    ExtractUserIds(context, obj, ref userIds);
                }
            }
        }

        public void CopyMediaViewing(IContextContainer context, object media, object user)
        {
            List<Guid> userIds = new List<Guid>();
            Guid mediaId = Guid.Empty;

            if(media is Guid)
            {
                mediaId = (Guid)media;
            }
            else if(media is IMediaItem)
            {
                mediaId = ((IMediaItem)media).Id;
            }
            else if(media is string)
            {
                mediaId = Guid.Parse((string)media);
            }

            ExtractUserIds(context, user, ref userIds);

            var wd = context.Get<IWorkingDocument>();

            _mediaMgr.CopyMediaViewing(wd.Id, mediaId, userIds.ToArray());
        }
    }
}
