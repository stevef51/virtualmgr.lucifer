﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Checklist.Facilities;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.Checklist.Lingo;

namespace ConceptCave.BusinessLogic.Facilities
{
    public class SequenceFacility : Facility
    {
        private readonly ISequenceRepository _seqRepo;

        public SequenceFacility(ISequenceRepository seqRepo)
        {
            Name = "Sequence";
            _seqRepo = seqRepo;
        }

        public int NextValue(string sequence)
        {
#if iOS
            throw new LingoException("Sequence facility won't work on iOS");
#else
            return _seqRepo.NextValue(sequence);
#endif
        }
    }
}
