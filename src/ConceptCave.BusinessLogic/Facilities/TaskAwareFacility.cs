﻿using ConceptCave.Checklist.Facilities;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.Lingo.ObjectProviders;
using ConceptCave.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.BusinessLogic.Facilities
{
    public class TaskAwareFacility : Facility, IRequiresContextContainer
    {
        protected IContextContainer _currentContext;

        public event RequiresContextContainerHandler ContextRequired = delegate { };

        internal virtual IContextContainer ContextContainer
        {
            get
            {
                if (_currentContext == null)
                {
                    RequiresContextContainerArgs args = new RequiresContextContainerArgs();
                    ContextRequired(this, args);

                    _currentContext = args.Context;
                }

                return _currentContext;
            }
        }

        public virtual bool HasCurrentTask
        {
            get
            {
                var doc = ContextContainer.Get<IWorkingDocument>();

                var args = doc.Variables.Variable<object>("Arguments");

                object obj = null;

                if (args != null)
                {
                    obj = ((DictionaryObjectProvider)args.Value).GetValue(null, "taskid");
                }

                return obj != null;
            }
        }

        public virtual Guid CurrentTaskId
        {
            get
            {
                var doc = ContextContainer.Get<IWorkingDocument>();

                return (Guid)((DictionaryObjectProvider)doc.Variables.Variable<object>("Arguments").Value).GetValue(null, "taskid");
            }
            set
            {
                var doc = ContextContainer.Get<IWorkingDocument>();

                if (doc.Variables.Variable<object>("Arguments") == null)
                {
                    // there are no query string arguments, so lets fake things
                    doc.Variables.Variable<object>("Arguments", new DictionaryObjectProvider());
                }

                ((DictionaryObjectProvider)doc.Variables.Variable<object>("Arguments").Value).SetValue(null, "taskid", value);
            }
        }

    }
}
