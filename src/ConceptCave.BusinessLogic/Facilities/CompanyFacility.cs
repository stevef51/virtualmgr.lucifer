﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Checklist.Facilities;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.Checklist.Lingo;
using ConceptCave.Core;
using ConceptCave.Core.Coding;
using ConceptCave.DTO.DTOClasses;

namespace ConceptCave.BusinessLogic.Facilities
{
    public class CompanyFacility : Facility
    {
        private readonly ICompanyRepository _companyRepo;

        public CompanyFacility(ICompanyRepository companyRepo)
        {
            Name = "Company";
            _companyRepo = companyRepo;
        }

        public Company GetById(Guid id)
        {
            var dto = _companyRepo.GetById(id, RepositoryInterfaces.Enums.CompanyLoadInstructions.None);
            if (dto == null)
            {
                return null;
            }
            return new Company(this, dto);
        }

        public Company FindByName(string name)
        {
            var dto = _companyRepo.Search(name, 0, 1, RepositoryInterfaces.Enums.CompanyLoadInstructions.None).FirstOrDefault();
            if (dto == null)
            {
                return null;
            }
            return new Company(this, dto);
        }

        public void Save(Company company)
        {
            company._dto = _companyRepo.Save(company.DTO, true, false);
        }

        public Company New()
        {
            return new Company(this, new CompanyDTO()
            {
                __IsNew = true,
                Id = Guid.NewGuid()
            });
        }

        public class Company : Node
        {
            private CompanyFacility _facility;
            internal CompanyDTO _dto;

            public Company()
            {

            }

            public Company(CompanyFacility facility, CompanyDTO dto)
            {
                _facility = facility;
                _dto = dto;
                this._id = dto.Id;
            }

            public override void Encode(IEncoder encoder)
            {
                base.Encode(encoder);

                encoder.Encode("Facility", _facility);
            }

            public override void Decode(IDecoder decoder)
            {
                base.Decode(decoder);
                _facility = decoder.Decode<CompanyFacility>("Facility");
            }

            public CompanyDTO DTO
            {
                get
                {
                    if (_dto == null)
                    {
                        _dto = _facility._companyRepo.GetById(this.Id, RepositoryInterfaces.Enums.CompanyLoadInstructions.None);
                    }
                    return _dto;
                }
            }
        }
    }
}
