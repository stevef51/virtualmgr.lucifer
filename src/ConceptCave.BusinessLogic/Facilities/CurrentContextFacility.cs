﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using ConceptCave.BusinessLogic.LingoDataObjects;
using ConceptCave.BusinessLogic.Context;
using ConceptCave.Checklist.Editor;
using ConceptCave.Checklist.Facilities;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.RunTime;
using ConceptCave.Core;
using ConceptCave.Core.Coding;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;

namespace ConceptCave.BusinessLogic.Facilities
{
    public class CurrentContextFacility : Facility, IDecoderPostProcessing, IRequiresContextContainer, IObjectSetter
    {
        /* 
                private Guid _reviewerId;
                private Guid? _revieweeId;

                private bool? _hasParent;
                private Guid _parentId;

                private LatLng _loginLocation;
        */
        private WorkingDocumentGetter _parent;
        private FacilityUserHelper _reviewer;
        private FacilityUserHelper _reviewee;
        private IContextContainer _currentContext;
        private CurrentContextFacilityCurrentUsers _currentUsers;

        private readonly ILabelRepository _lblRepo;
        private readonly IWorkingDocumentRepository _wdRepo;
        private readonly IMembershipRepository _membershipRepo;
        private readonly IContextManagerFactory _ctxFac;

        public event RequiresContextContainerHandler ContextRequired = delegate { };

        public CurrentContextFacility(IWorkingDocumentRepository wdRepo, IMembershipRepository membershipRepo,
            ILabelRepository lblRepo, IContextManagerFactory ctxFac)
        {
            Name = "Context";
            _wdRepo = wdRepo;
            _membershipRepo = membershipRepo;
            _lblRepo = lblRepo;
            _ctxFac = ctxFac;
        }

        internal IContextContainer ContextContainer
        {
            get
            {
                if (_currentContext == null)
                {
                    RequiresContextContainerArgs args = new RequiresContextContainerArgs();
                    ContextRequired(this, args);

                    _currentContext = args.Context;
                }

                return _currentContext;
            }
        }

        internal CurrentContextFacilityCurrentUsers CurrentUsers
        {
            get
            {
                if (_currentUsers == null)
                {
                    if (ContextContainer.Has<CurrentContextFacilityCurrentUsers>())
                    {
                        _currentUsers = ContextContainer.Get<CurrentContextFacilityCurrentUsers>();
                    }
                }

                return _currentUsers;
            }
        }

        public object ReviewerId
        {
            get
            {
                var docEntity = ContextContainer.Get<WorkingDocumentDTO>();
                return docEntity.ReviewerId;
            }
            set
            {

                var docEntity = ContextContainer.Get<WorkingDocumentDTO>();


                Guid valueAsGuid;
                if (value is Guid)
                    valueAsGuid = (Guid)value;
                else if (value is string)
                    valueAsGuid = Guid.Parse(value as string);
                else
                    return;

                // check that the user exists before we do anything so radical as set the reviewee
                var user = _membershipRepo.GetById(valueAsGuid, MembershipLoadInstructions.None);

                if (user == null)
                {
                    return;
                }

                docEntity.ReviewerId = valueAsGuid;
                _reviewer = null;
            }
        }

        public object RevieweeId
        {
            get
            {
                var docEntity = ContextContainer.Get<WorkingDocumentDTO>();
                return docEntity.RevieweeId.HasValue ? (object)docEntity.RevieweeId.Value : null;
            }
            set
            {

                var docEntity = ContextContainer.Get<WorkingDocumentDTO>();


                Guid valueAsGuid;
                if (value is Guid)
                    valueAsGuid = (Guid)value;
                else if (value is string)
                    valueAsGuid = Guid.Parse(value as string);
                else
                    return;

                // check that the user exists before we do anything so radical as set the reviewee
                var user = _membershipRepo.GetById(valueAsGuid, MembershipLoadInstructions.None);

                if (user == null)
                {
                    return;
                }

                docEntity.RevieweeId = valueAsGuid;
                _reviewee = null;
            }
        }

        public Guid CurrentId
        {
            get
            {
                var docEntity = ContextContainer.Get<WorkingDocumentDTO>();
                return docEntity.Id;
            }
        }

        public string ChecklistName
        {
            get
            {
                var docEntity = ContextContainer.Get<WorkingDocumentDTO>();
                return docEntity.Name;
            }
            set
            {
                if (string.IsNullOrEmpty(value) == true)
                {
                    return;
                }

                var docEntity = ContextContainer.Get<WorkingDocumentDTO>();
                docEntity.Name = value;
            }
        }

        public Guid? ParentId
        {
            get
            {
                var docEntity = ContextContainer.Get<WorkingDocumentDTO>();
                return docEntity.ParentId;
            }
        }

        public WorkingDocumentGetter Parent
        {
            get
            {
                if (ParentId == Guid.Empty)
                {
                    return null;
                }

                if (_parent != null)
                {
                    return _parent;
                }

                _parent = new CurrentContextRelatedWorkingDocumentGetter(ParentId.Value, _wdRepo, _membershipRepo, _lblRepo, _ctxFac);

                return _parent;
            }
        }

        public FacilityUserHelper Reviewer
        {
            get
            {
                if (_reviewer == null)
                {
                    _reviewer = new FacilityUserHelper(_membershipRepo, _lblRepo, _ctxFac, (Guid)ReviewerId);
                }

                return _reviewer;
            }
        }

        public FacilityUserHelper Reviewee
        {
            get
            {
                if (_reviewee == null)
                {
                    if (RevieweeId == null)
                        return null;

                    _reviewee = new FacilityUserHelper(_membershipRepo, _lblRepo, _ctxFac, (Guid)RevieweeId);
                }

                return _reviewee;
            }
        }

        #region IObjectSetter Members

        public bool SetObject(IContextContainer context, string name, object value)
        {
            if (CurrentUsers == null)
            {
                return false;
            }

            CurrentUsers.Context[name] = value;

            return true;
        }

        #endregion

        #region IObjectGetter Members

        public bool GetObject(IContextContainer context, string name, out object result)
        {
            if (CurrentUsers == null)
            {
                result = null;
                return false;
            }

            if (CurrentUsers.Context.ContainsKey(name) == false)
            {
                result = null;
                return false;
            }

            result = CurrentUsers.Context[name];

            return true;
        }

        #endregion
    }
}
