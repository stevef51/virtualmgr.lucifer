﻿using ConceptCave.Checklist;
using ConceptCave.Checklist.Facilities;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.BusinessLogic.Facilities
{
    public class NewsFeedFacility : Facility
    {
        protected INewsFeedRepository _newsfeedRepo;

        public NewsFeedFacility(INewsFeedRepository newsfeedRepo)
        {
            Name = "NewsFeed";

            _newsfeedRepo = newsfeedRepo;
        }

        public void Add(string channel, string program, string title, string headline, DateTime expires, object userid)
        {
            Guid id = Guid.Empty;
            if(userid is string)
            {
                id = Guid.Parse((string)userid);
            }
            else if(userid is Guid)
            {
                id = (Guid)userid;
            }
            else{
                return;
            }

            var feed = new NewsFeedDTO() {
                __IsNew = true,
                Id = CombFactory.NewComb(),
                Channel = channel,
                Program = program,
                Title = title,
                Headline = headline,
                ValidTo = expires,
                DateCreated = DateTime.UtcNow,
                CreatedBy = id
            };

            _newsfeedRepo.Save(feed, true, false);
        }
    }
}
