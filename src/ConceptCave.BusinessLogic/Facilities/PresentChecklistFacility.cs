﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using ConceptCave.Checklist.Facilities;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;

namespace ConceptCave.BusinessLogic.Facilities
{
    public class PresentChecklistFacility : Facility
    {
        public class PresentedChecklist : Node, IBlocker, IObjectGetter
        {
            public Guid WorkingDocumentId { get; set; }

            private readonly IWorkingDocumentRepository _wdRepo;
            private readonly IWorkingDocumentStateManager _wdStateManager;

            public PresentedChecklist(IWorkingDocumentRepository wdRepo, IWorkingDocumentStateManager wdStateManager)
            {
                _wdRepo = wdRepo;
                _wdStateManager = wdStateManager;
            }

            #region IBlocker Members

            public Guid BlockedById { get { return WorkingDocumentId; } }

            private List<IBlockable> _blockables = new List<IBlockable>();
            public IEnumerable<IBlockable> Blockables
            {
                get { return _blockables; }
            }

            public void Block(IBlockable blockable)
            {
                _blockables.Add(blockable);
                blockable.BlockOn(this);
            }

            public void Unblock()
            {
                IsBlocked = false;
                _blockables.ForEach(b => b.UnblockFrom(this));
                _blockables.Clear();
            }

            public bool IsBlocked { get; set; }

            #endregion

            public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
            {
                base.Encode(encoder);

                encoder.Encode("WorkingDocumentId", WorkingDocumentId);
                encoder.Encode("Blockables", _blockables);
                encoder.Encode("IsBlocked", IsBlocked);
            }

            public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
            {
                base.Decode(decoder);

                WorkingDocumentId = decoder.Decode<Guid>("WorkingDocumentId");
                _blockables = decoder.Decode<List<IBlockable>>("Blockables");
                IsBlocked = decoder.Decode<bool>("IsBlocked");
            }

            public bool GetObject(IContextContainer context, string name, out object result)
            {
                var wdEnt = _wdRepo.GetById(WorkingDocumentId, WorkingDocumentLoadInstructions.Data);
                if (wdEnt == null)
                {
                    result = null;
                    return false;
                }

                var wdObj = _wdStateManager.WorkingDocumentFromString(wdEnt.WorkingDocumentData.Data);
                result = new WorkingDocumentObjectGetter(wdObj);
                return true;
            }
        }

        public class RePresentResult : Node
        {
            public bool Success { get; set; }
        }

        /// <summary>
        /// Class for storing the DefaultAnswerSources for a hierachy of checklists. These can then be used to hydrate the child checklists with previous answers
        /// </summary>
        public class DefaultAnswerSourceHieachyItem : Node
        {
            public int PublisingGroupResourceId { get; set; }
            public Guid ReviewerId { get; set; }
            public Guid? RevieweeId { get; set; }

            public IDefaultAnswerSource Source { get; set; }

            public List<DefaultAnswerSourceHieachyItem> Children { get; set; }

            public DefaultAnswerSourceHieachyItem() : base()
            {
                Source = new ConceptCave.Checklist.RunTime.InlineDefaultAnswerSource();
                Children = new List<DefaultAnswerSourceHieachyItem>();
            }

            public DefaultAnswerSourceHieachyItem(WorkingDocumentDTOHierachyItem item, IWorkingDocumentStateManager wds) : this()
            {
                this.PublisingGroupResourceId = item.Dto.PublishingGroupResourceId;
                this.ReviewerId = item.Dto.ReviewerId;
                this.RevieweeId = item.Dto.RevieweeId;

                var wd = wds.WorkingDocumentFromString(item.Dto.WorkingDocumentData.Data);

                Source.PopulateFrom(wd);
            }

            protected void LoadChildren(WorkingDocumentDTOHierachyItem item, IWorkingDocumentStateManager wds)
            {
                foreach (var c in item.Children)
                {
                    Children.Add(new DefaultAnswerSourceHieachyItem(c, wds));
                }
            }

            public override void Encode(Core.Coding.IEncoder encoder)
            {
                base.Encode(encoder);

                encoder.Encode("PublisingGroupResourceId", PublisingGroupResourceId);
                encoder.Encode("ReviewerId", ReviewerId);
                encoder.Encode("RevieweeId", RevieweeId);
                encoder.Encode("Source", Source);
                encoder.Encode("Children", Children);
            }

            public override void Decode(Core.Coding.IDecoder decoder)
            {
                base.Decode(decoder);

                PublisingGroupResourceId = decoder.Decode<int>("PublisingGroupResourceId");
                ReviewerId = decoder.Decode<Guid>("ReviewerId");
                RevieweeId = decoder.Decode<Guid?>("RevieweeId");
                Source = decoder.Decode<IDefaultAnswerSource>("Source");
                Children = decoder.Decode<List<DefaultAnswerSourceHieachyItem>>("Children");
            }
        }

        public string PublishingGroupName { get; set; }
        public string ChecklistName { get; set; }
        public string ReviewerName { get; set; }
        public bool PresentToParentReviewee { get; set; }
        public bool PublishUnderParent { get; set; }
        public bool ReferenceParentWorkingDocument { get; set; }

        public List<DefaultAnswerSourceHieachyItem> DefaultAnswerSourceHierachies { get; set; }

        protected IWorkingDocumentStateManager _wdStateManager;
        protected IWorkingDocumentRepository _wdRepo;
        protected IPublishingGroupResourceRepository _pgrRepo;

        public PresentChecklistFacility(IWorkingDocumentStateManager workingDocumentStateManager, IWorkingDocumentRepository workingDocumentRepo,
            IPublishingGroupResourceRepository publishingGroupResourceRepo)
        {
            _wdStateManager = workingDocumentStateManager;
            _wdRepo = workingDocumentRepo;
            _pgrRepo = publishingGroupResourceRepo;
            PresentToParentReviewee = true;
            PublishUnderParent = true;
            Name = "PresentChecklist";
        }

        public PresentedChecklist Present(ConceptCave.Core.IContextContainer context)
        {
            IWorkingDocument referenced = context.Get<IWorkingDocument>();
            WorkingDocumentDTO dto = context.Get<WorkingDocumentDTO>();
            ILingoProgram program = context.Get<ILingoProgram>();

            Guid? parentId = null;
            if (PublishUnderParent)
            {
                parentId = dto.Id;
            }

            var pgName = program.ExpandString(PublishingGroupName);
            var checklistName = program.ExpandString(ChecklistName);
            var reviewerName = program.ExpandString(ReviewerName);

            var newWorkingDocument = _wdStateManager.BeginWorkingDocument(pgName, checklistName, reviewerName, null, parentId);

            if (PresentToParentReviewee == true)
            {
                newWorkingDocument.DocumentEntity.RevieweeId = dto.RevieweeId;
            }

            if (DefaultAnswerSourceHierachies != null && DefaultAnswerSourceHierachies.Count > 0)
            {
                // hmm, ok we may need to inject some previous state into this sucker
                var pgr = _pgrRepo.GetByCompoundName(pgName, checklistName);

                if (pgr != null)
                {
                    var item = (from d in DefaultAnswerSourceHierachies
                                where d.PublisingGroupResourceId == pgr.Id &&
d.ReviewerId == newWorkingDocument.DocumentEntity.ReviewerId &&
d.RevieweeId == newWorkingDocument.DocumentEntity.RevieweeId
                                select d)
                                    .DefaultIfEmpty(null).First();

                    if (item != null)
                    {
                        // we've found one, set the default answers
                        newWorkingDocument.DocumentObject.DefaultAnswerSource = item.Source;

                        // and if required, inject the children into its present checklist facility
                        var subPresent = (from f in newWorkingDocument.DocumentObject.Facilities where f is PresentChecklistFacility select f).DefaultIfEmpty(null).First();

                        if (subPresent != null)
                        {
                            // inject children into
                            ((PresentChecklistFacility)subPresent).DefaultAnswerSourceHierachies = item.Children;
                        }
                    }
                }
            }

            _wdStateManager.AdvanceWorkingDocument(newWorkingDocument.DocumentObject, newWorkingDocument.DocumentEntity);

            _wdStateManager.SaveWorkingDocument(newWorkingDocument.DocumentObject, newWorkingDocument.DocumentEntity);

            return new PresentedChecklist(_wdRepo, _wdStateManager) { WorkingDocumentId = newWorkingDocument.DocumentEntity.Id, IsBlocked = true };
        }

        /// <summary>
        /// Represents the workign document. This method will result in all of the document being represented being destroyed, any reporting data with the
        /// checklist its self or its children being destroyed (it'll all get recreated when the checklist is finished).
        /// </summary>
        /// <param name="context"></param>
        /// <param name="workingDocumentIdToRePresent">Working Document to be re presented</param>
        /// <param name="hydrateWorkingDocument">Should the working document be hydrated with the answers from it (have answers populate the default answer source) </param>
        /// <param name="hydrateChildren">Should any children be re hydrated when their created</param>
        /// <returns></returns>
        public RePresentResult RePresent(ConceptCave.Core.IContextContainer context, Guid workingDocumentIdToRePresent, bool hydrateWorkingDocument, bool hydrateChildren)
        {
            var item = _wdRepo.GetByIdWithDescendants(workingDocumentIdToRePresent, RepositoryInterfaces.Enums.WorkingDocumentLoadInstructions.Data);

            if (item == null)
            {
                return new RePresentResult() { Success = false };
            }

            using (TransactionScope scope = new TransactionScope())
            {
                // blow them all away first (yes aye karumba!)
                foreach (var w in item.AsDepthFirstEnumerable())
                {
                    _wdRepo.DeleteWorkingDocument(w.Dto.Id, true);
                }

                // now we need to re-create the top one (its the one that's been requested to be re presented after all), first up lets get what we had
                IWorkingDocument wd = _wdStateManager.WorkingDocumentFromString(item.Dto.WorkingDocumentData.Data);
                var dto = RecreatePublishingGroupResource(item, wd);

                var wdc = _wdStateManager.BeginWorkingDocument(dto, item.Dto.ReviewerId, item.Dto.RevieweeId, item.Dto.ParentId);

                if (hydrateWorkingDocument == true)
                {
                    // and now lets give it a kick start
                    wdc.DocumentObject.DefaultAnswerSource.PopulateFrom(wd);

                    wdc.DocumentEntity.WorkingDocumentData.Data = _wdStateManager.WorkingDocumentToString(wdc.DocumentObject);
                }

                if (hydrateChildren == true)
                {
                    var subPresent = (from f in wdc.DocumentObject.Facilities where f is PresentChecklistFacility select f).DefaultIfEmpty(null).First();

                    if (subPresent != null)
                    {
                        // ok the checklist could present children, so need to get something to store what they could be hydrated with
                        List<DefaultAnswerSourceHieachyItem> childHydrations = new List<DefaultAnswerSourceHieachyItem>();
                        item.Children.ForEach(c => childHydrations.Add(new DefaultAnswerSourceHieachyItem(c, _wdStateManager)));

                        // inject into the present facility in the working document
                        ((PresentChecklistFacility)subPresent).DefaultAnswerSourceHierachies = childHydrations;
                    }
                }

                // finally save and commit the transaction, wipe foreheads, grab a cigar and your choice of wine and kick back for the evening
                _wdStateManager.AdvanceWorkingDocument(wdc.DocumentObject, wdc.DocumentEntity);
                _wdStateManager.SaveWorkingDocument(wdc.DocumentObject, wdc.DocumentEntity);

                scope.Complete();
            }

            return new RePresentResult() { Success = true };
        }

        protected PublishingGroupResourceDTO RecreatePublishingGroupResource(WorkingDocumentDTOHierachyItem item, IWorkingDocument wd)
        {
            PublishingGroupResourceDTO dto = new PublishingGroupResourceDTO()
            {
                Id = item.Dto.PublishingGroupResourceId,
                Name = item.Dto.Name
            };

            if (item.Dto.CleanUpIfNotFinishedAfter.HasValue)
            {
                // hmm, we need to calculate what this value was at the time of publishing, the difference between date created of the wd and its clean up date is what we want
                dto.IfNotFinishedCleanUpAfter = (int?)item.Dto.CleanUpIfNotFinishedAfter.Value.Subtract(item.Dto.DateCreated).TotalSeconds;
            }

            if (item.Dto.CleanUpIfFinishedAfter.HasValue)
            {
                dto.IfFinishedCleanUpAfter = (int?)item.Dto.CleanUpIfFinishedAfter.Value.Subtract(item.Dto.DateCreated).TotalSeconds;
            }

            // need to work out if this one populates reporting tables

            if ((from f in wd.Facilities where f is FinishWorkingDocumentFacility select f).Count() > 0)
            {
                dto.PopulateReportingData = true;
            }

            PublishingGroupResourceDataDTO data = new PublishingGroupResourceDataDTO()
            {
                Data = _wdStateManager.DesignDocumentToString(wd.DesignDocument)
            };

            dto.PublishingGroupResourceData = data;

            return dto;
        }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("PublishingGroupName", PublishingGroupName);
            encoder.Encode("ChecklistName", ChecklistName);
            encoder.Encode("ReviewerName", ReviewerName);
            encoder.Encode("PresentToParentReviewee", PresentToParentReviewee);
            encoder.Encode("PublishUnderParent", PublishUnderParent);
            encoder.Encode("ReferenceParentWorkingDocument", ReferenceParentWorkingDocument);
            encoder.Encode("DefaultAnswerSourceHierachies", DefaultAnswerSourceHierachies);
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            PublishingGroupName = decoder.Decode<string>("PublishingGroupName");
            ChecklistName = decoder.Decode<string>("ChecklistName");
            ReviewerName = decoder.Decode<string>("ReviewerName");
            PresentToParentReviewee = decoder.Decode<bool>("PresentToParentReviewee");
            PublishUnderParent = decoder.Decode<bool>("PublishUnderParent", true);
            ReferenceParentWorkingDocument = decoder.Decode<bool>("ReferenceParentWorkingDocument");
            DefaultAnswerSourceHierachies = decoder.Decode<List<DefaultAnswerSourceHieachyItem>>("DefaultAnswerSourceHierachies");
        }
    }
}
