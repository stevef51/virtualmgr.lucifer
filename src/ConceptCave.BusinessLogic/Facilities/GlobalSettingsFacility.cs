﻿using ConceptCave.Checklist.Facilities;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.RepositoryInterfaces.Repositories;

namespace ConceptCave.BusinessLogic.Facilities
{
    public class GlobalSettingsFacility : Facility, IObjectSetter
    {
        //This class is used to set and retrieve settings from tblGlobalSetting

        private readonly IGlobalSettingsRepository _gsRepo;

        public GlobalSettingsFacility(IGlobalSettingsRepository gsRepo)
        {
            Name = "GlobalSettings";
            _gsRepo = gsRepo;
        }
       
        //The Read/WriteSetting methods are available for settings with a space in their name
        public string ReadSetting(string settingName)
        {
            return _gsRepo.GetSetting<string>(settingName);
        }

        public void WriteSetting(string settingName, object settingValue)
        {
            _gsRepo.SetSetting(settingName, settingValue);
        }


        //These methods handle setting settings by GlobalSettings.SomeSettingName := "a value"
        public bool SetObject(Core.IContextContainer context, string name, object value)
        {
            WriteSetting(name, value);
            return true;
        }

        public bool GetObject(Core.IContextContainer context, string name, out object result)
        {
            result = ReadSetting(name);
            return true;
        }
    }
}
