﻿using ConceptCave.Checklist.Facilities;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.Lingo.ObjectProviders;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using System.Xml;
using VirtualMgr.Common;

namespace ConceptCave.BusinessLogic.Facilities
{
    public class WageEasyFacility : Facility
    {
        protected IRosterRepository _rosterRepo;
        protected ITimesheetItemRepository _timesheetItemRepo;
        protected ITimesheetItemTypeRepository _timesheetItemTypeRepo;
        protected IMembershipRepository _memberRepo;
        protected IHierarchyBucketOverrideTypeRepository _bucketTypeRepo;

        protected IList<RosterDTO> _rosters;

        protected const string DateFormat = "yyyy-MM-ddTHH:mm:00";

        public WageEasyFacility(IRosterRepository rosterRepo,
            ITimesheetItemRepository timesheetItemRepo,
            ITimesheetItemTypeRepository timesheetItemTypeRepo,
            IMembershipRepository memberRepo,
            IHierarchyBucketOverrideTypeRepository bucketTypeRepo)
        {
            Name = "WageEasy";

            _rosterRepo = rosterRepo;
            _timesheetItemRepo = timesheetItemRepo;
            _timesheetItemTypeRepo = timesheetItemTypeRepo;
            _memberRepo = memberRepo;
            _bucketTypeRepo = bucketTypeRepo;
        }

        protected IList<RosterDTO> Rosters
        {
            get
            {
                if (_rosters == null)
                {
                    _rosters = _rosterRepo.GetAll();
                }

                return _rosters;
            }
        }

        protected IList<int> FindNoneLeaveTypes(IList<TimesheetItemTypeDTO> types)
        {
            return (from t in types where t.IsLeave == false select t.Id).ToList();
        }

        protected IList<int> FindLeaveTypes(IList<TimesheetItemTypeDTO> types)
        {
            return (from t in types where t.IsLeave == true select t.Id).ToList();
        }

        protected void AppendTimes(DateTime startTime, DateTime endTime, TimeZoneInfo rosterTz, XmlElement parent)
        {
            XmlElement st = parent.OwnerDocument.CreateElement("StartTime");
            st.InnerText = TimeZoneInfo.ConvertTimeFromUtc(startTime, rosterTz).ToString(DateFormat);
            parent.AppendChild(st);

            XmlElement et = parent.OwnerDocument.CreateElement("FinishTime");
            et.InnerText = TimeZoneInfo.ConvertTimeFromUtc(endTime, rosterTz).ToString(DateFormat);
            parent.AppendChild(et);
        }

        public IMediaItem ProcessTimesheetItems(object rosters, bool markAsExported)
        {
            IList<RosterDTO> rs = ExtractRosterIds(rosters);

            if (rs == null)
            {
                return null;
            }

            var timesheetTypes = _timesheetItemTypeRepo.GetAll();

            var noneLeaveTypes = FindNoneLeaveTypes(timesheetTypes);
            var leaveTypes = FindLeaveTypes(timesheetTypes);

            // grab all the timesheet items
            List<TimesheetItemDTO> items = new List<TimesheetItemDTO>();
            foreach (var roster in rs)
            {
                var timesheets = _timesheetItemRepo.GetForRosterForExport(roster.Id, RepositoryInterfaces.Enums.TimesheetItemLoadInstructions.Users);

                items.AddRange(timesheets);
            }

            XmlDocument doc = new XmlDocument();
            XmlElement root = doc.CreateElement("TimeCards");
            var version = doc.CreateAttribute("Version");
            version.Value = "2.3";
            root.Attributes.Append(version);

            doc.AppendChild(root);

            // now break them out by user
            foreach (var employeeSheets in (from i in items group i by i.UserId into g select new { UserId = g.Key, Items = g.ToList() }))
            {
                XmlElement timecard = doc.CreateElement("TimeCard");
                root.AppendChild(timecard);

                XmlElement timecardNum = doc.CreateElement("TimeCardNo");
                var member = _memberRepo.GetById(employeeSheets.UserId, RepositoryInterfaces.Enums.MembershipLoadInstructions.AspNetMembership);
                timecardNum.InnerText = member.AspNetUser.UserName;
                timecard.AppendChild(timecardNum);

                XmlElement employeeName = doc.CreateElement("EmployeeName");
                employeeName.InnerText = member.Name;
                timecard.AppendChild(employeeName);

                decimal totalHours = 0;

                // Grab the none leave stuff first
                foreach (var sheet in (from i in employeeSheets.Items where noneLeaveTypes.Contains(i.TimesheetItemTypeId) select i))
                {
                    var roster = (from r in rs where r.Id == sheet.RosterId select r);
                    var rosterTz = TimeZoneInfoResolver.ResolveWindows(roster.First().Timezone);

                    if (sheet.IsLunchPaid == true || sheet.ScheduledLunchStartDate.HasValue == false || sheet.ScheduledLunchEndDate.HasValue == false)
                    {
                        XmlElement shift = doc.CreateElement("Shift");
                        timecard.AppendChild(shift);

                        totalHours = AddShiftHours(doc, totalHours, sheet.ApprovedStartDate, sheet.ApprovedEndDate, sheet.ApprovedDuration, rosterTz, shift);
                    }
                    else
                    {
                        XmlElement shift = doc.CreateElement("Shift");
                        timecard.AppendChild(shift);

                        totalHours = AddShiftHours(doc, totalHours, sheet.ApprovedStartDate, sheet.ScheduledLunchStartDate.Value, sheet.ApprovedDuration, rosterTz, shift);
                        totalHours = AddShiftHours(doc, totalHours, sheet.ScheduledLunchEndDate.Value, sheet.ApprovedEndDate, sheet.ApprovedDuration, rosterTz, shift);
                    }
                }

                var hierarchyLeaveTypes = _bucketTypeRepo.GetAll();

                // now the leave entries
                foreach (var sheet in (from i in employeeSheets.Items where leaveTypes.Contains(i.TimesheetItemTypeId) select i))
                {
                    XmlElement leave = doc.CreateElement("Leave");
                    timecard.AppendChild(leave);

                    var lt = (from l in hierarchyLeaveTypes where l.Id == sheet.HierarchyBucketOverrideTypeId select l);
                    var leaveType = doc.CreateAttribute("Type");
                    leaveType.Value = lt.First().Name;
                    leave.Attributes.Append(leaveType);

                    var leaveHours = doc.CreateAttribute("Hours");
                    leaveHours.Value = DateTime.Now.Date.AddSeconds((double)sheet.ApprovedDuration).ToString("HH:mm");
                    leave.Attributes.Append(leaveHours);

                    XmlElement fromDate = doc.CreateElement("FromDate");
                    fromDate.InnerText = sheet.ApprovedStartDate.ToString("yyyy-MM-dd");
                    leave.AppendChild(fromDate);

                    XmlElement toDate = doc.CreateElement("ToDate");
                    toDate.InnerText = sheet.ApprovedEndDate.ToString("yyyy-MM-dd");
                    leave.AppendChild(toDate);
                }

                XmlElement tHours = doc.CreateElement("TotalHours");
                timecard.AppendChild(tHours);
                tHours.InnerText = totalHours.ToString("##.#####");
            }

            if (markAsExported)
            {
                DateTime exportDate = DateTime.UtcNow;
                using (TransactionScope scope = new TransactionScope())
                {
                    foreach (var sheet in items)
                    {
                        sheet.ExportedDate = exportDate;
                        _timesheetItemRepo.Save(sheet, false, false);
                    }

                    scope.Complete();
                }
            }

            var result = new MediaFacility.EmbeddedMediaItem(doc.OuterXml);

            return result;
        }

        private decimal AddShiftHours(XmlDocument doc, decimal totalHours, DateTime startDate, DateTime endDate, decimal approvedDuration, TimeZoneInfo rosterTz, XmlElement shift)
        {
            XmlElement shiftHours = doc.CreateElement("ShiftHours");
            shift.AppendChild(shiftHours);

            totalHours = totalHours + approvedDuration / 3600;

            AppendTimes(startDate, endDate, rosterTz, shiftHours);

            XmlElement department = doc.CreateElement("Department");
            shiftHours.AppendChild(department);
            XmlElement job = doc.CreateElement("Job");
            shiftHours.AppendChild(job);
            XmlElement payRateCode = doc.CreateElement("PayRateCode");
            shiftHours.AppendChild(payRateCode);
            return totalHours;
        }

        protected IList<RosterDTO> ExtractRosterIds(object rosters)
        {
            if (rosters == null)
            {
                return Rosters;
            }

            if (rosters is int[])
            {
                var rs = (int[])rosters;
                return (from r in Rosters where rs.Contains(r.Id) select r).ToList();
            }
            else if (rosters is ListObjectProvider)
            {
                var rs = (ListObjectProvider)rosters;

                List<RosterDTO> result = new List<RosterDTO>();
                foreach (var r in rs)
                {
                    var find = (from a in Rosters where a.Id == (int)r select a);
                    if (find.Count() > 0)
                    {
                        result.Add(find.First());
                    }
                }

                return result;
            }
            else if (rosters is string)
            {
                var rs = (string)rosters;
                List<RosterDTO> result = new List<RosterDTO>();
                foreach (var r in rs.Split(','))
                {
                    var id = int.Parse(r);
                    var find = (from a in Rosters where a.Id == id select a);
                    if (find.Count() > 0)
                    {
                        result.Add(find.First());
                    }
                }

                return result;
            }

            return null;
        }
    }
}
