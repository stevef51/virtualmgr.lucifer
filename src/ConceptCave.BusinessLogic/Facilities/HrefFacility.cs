﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.BusinessLogic.Configuration;
using ConceptCave.Checklist.Facilities;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.Lingo.ObjectProviders;
using ConceptCave.Core;

namespace ConceptCave.BusinessLogic.Facilities
{
    public class HrefFacility : Facility, ICallTarget
    {
        private readonly IUrlParser _urlParser;
        private readonly IRepositorySectionManager _sectionManager;

        public HrefFacility(IUrlParser urlParser, IRepositorySectionManager sectionManager)
        {
            _urlParser = urlParser;
            _sectionManager = sectionManager;
            Name = "href";
        }

        public static string EncodeArguments(IDictionary<string, object> dict)
        {
            using (var ms = new MemoryStream())
            {
                SBON.SBONConvert.Serialize(ms, dict);
                return Uri.EscapeDataString(Convert.ToBase64String(ms.ToArray()));
            }
        }

        public static Dictionary<string, object> DecodeArguments(string b64)
        {
            try
            {
                byte[] b64bytes = Convert.FromBase64String(Uri.UnescapeDataString(b64));

                using (var ms = new MemoryStream(b64bytes))
                {
                    return SBON.SBONConvert.Deserialize(ms, new Dictionary<string, object>());
                }
            }
            catch (System.Exception)
            {
                // ok its not base 64 or SBON encoded.
                var result = new Dictionary<string, object>();
                var parts = b64.Split('&');
                foreach (var part in parts)
                {
                    var p = Uri.UnescapeDataString(part);

                    if (p.IndexOf('=') == -1)
                    {
                        result.Add(p, null);
                    }
                    else
                    {
                        var pieces = p.Split('=');
                        result.Add(pieces[0], pieces[1]);
                    }
                }

                return result;
            }

        }

        public object Call(IContextContainer context, object[] args)
        {
            if (args.Length == 0)
                return null;


            var action = args[0].ToString();

            var argl = args.Skip(1).ToList();

            //Default second argument: working document id
            if (!argl.Any())
                argl.Add(context.Get<IWorkingDocument>().Id.ToString());

            // Any Dictionary arguments make no sense in href context, automatically encode them ..
            for (int i = 0; i < argl.Count; i++)
            {
                var dop = argl[i] as DictionaryObjectProvider;
                if (dop != null)
                {
                    argl[i] = EncodeArguments(dop.Dictionary);
                }
            }

            try
            {
                var actionItem = _sectionManager.ActionUrls[action];
                var actionFormat = actionItem.Pattern;
                var virtualPath = string.Format(actionFormat, argl.ToArray());

                return string.Format(actionItem.ServerRooted ? //return either serverUrl filter or clientUrl filter
                    "{{{{ '{0}' | serverUrl}}}}" :
                    "{{{{ '{0}' | clientUrl}}}}", virtualPath);
            }
            catch (KeyNotFoundException)
            {
                return null;
            }
            catch (FormatException)
            {
                return null;
            }
        }
    }
}
