﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConceptCave.BusinessLogic.LingoDataObjects;
using ConceptCave.BusinessLogic.Context;
using ConceptCave.Checklist.Editor;
using ConceptCave.Checklist.Facilities;
using ConceptCave.Core;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using VirtualMgr.MultiTenant;

namespace ConceptCave.BusinessLogic.Facilities
{
    public class MembershipFacility : Facility
    {
        private IMembershipRepository _memberRepo;
        private ILabelRepository _lblRepo;
        private IContextManagerFactory _ctxFactory;
        private readonly AppTenant _appTenant;

        public class MembershipTypeResult : Node
        {
            public new int Id { get; set; }
            public string Name { get; set; }

            public override void Encode(Core.Coding.IEncoder encoder)
            {
                base.Encode(encoder);

                encoder.Encode("Id", Id);
                encoder.Encode("Name", Name);
            }

            public override void Decode(Core.Coding.IDecoder decoder)
            {
                base.Decode(decoder);

                Id = decoder.Decode<int>("Id");
                Name = decoder.Decode<string>("Name");
            }
        }

        public MembershipFacility(
            IMembershipRepository memberRepo,
            ILabelRepository lblRepo,
            IContextManagerFactory ctxFactory,
            AppTenant appTenant)
        {
            this.Name = "Membership";
            _memberRepo = memberRepo;
            _lblRepo = lblRepo;
            _ctxFactory = ctxFactory;
            _appTenant = appTenant;
        }

        public MembershipTypeResult GetMembershipType(IContextContainer context, string name)
        {
            var type = _memberRepo.GetUserTypeByName(name);

            if (type == null)
            {
                return null;
            }

            var result = new MembershipTypeResult()
            {
                Id = type.Id,
                Name = type.Name
            };

            return result;
        }

        public bool Exists(IContextContainer context, string username)
        {
            var user = _memberRepo.GetByUsername(username);

            return user != null;
        }

        public FacilityUserHelper New(IContextContainer context, string username, string password, string name, string email, object membershipType)
        {
            var user = _memberRepo.Create(username, password, instructions: MembershipLoadInstructions.AspNetMembership);

            user.Name = name;
            user.AspNetUser.Email = email;
            user.TimeZone = _appTenant.TimeZoneInfo().Id;


            int membershipTypeId = 0;
            if (membershipType is int)
            {
                membershipTypeId = (int)membershipType;
            }
            else if (membershipType is MembershipTypeResult)
            {
                membershipTypeId = ((MembershipTypeResult)membershipType).Id;
            }

            user.UserTypeId = membershipTypeId;

            user = _memberRepo.Save(user, true, true);

            return new FacilityUserHelper(_memberRepo, _lblRepo, _ctxFactory, user.UserId);
        }

        public FacilityUserHelper GetById(IContextContainer context, Guid userId)
        {
            return new FacilityUserHelper(_memberRepo, _lblRepo, _ctxFactory, userId);
        }

        public FacilityUserHelper GetByUsername(IContextContainer context, string userName)
        {
            var member = _memberRepo.GetByUsername(userName);
            return new FacilityUserHelper(_memberRepo, _lblRepo, _ctxFactory, member.UserId);
        }

        public void AddUserToRole(IContextContainer context, FacilityUserHelper user, string roleName)
        {
            if (_memberRepo.UserIsInRole(user.Id, roleName) == false)
            {
                _memberRepo.AddUserInRole(user.Id, roleName);
            }
        }

        public List<MembershipGeoCodeResult> GetClosestUsers(IContextContainer context, decimal latitude, decimal longitude, int count)
        {
            var resultRows = _memberRepo.GetClosestUsers(latitude, longitude, count, false);
            return resultRows.Select(r => new MembershipGeoCodeResult()
            {
                Distance = (double)r.Distance,
                UserId = r.UserId,
                Coordinates = new LatLng(r.Latitude, r.Longitude, 0)
            }).ToList();
        }

        public MembershipGeoCodeResult GetClosestUser(IContextContainer context, decimal latitude, decimal longitude)
        {
            var result = GetClosestUsers(context, latitude, longitude, 1);

            return result.Count > 0 ? result[0] : null;
        }

        public double? GetDistanceFromUser(IContextContainer context, decimal latitude, decimal longitude, object site)
        {
            if (site == null)
            {
                return null;
            }

            UserDataDTO user = null;

            if (site is Guid)
            {
                user = _memberRepo.GetById((Guid)site, MembershipLoadInstructions.None);
            }
            else if (site is FacilityUserHelper)
            {
                user = ((FacilityUserHelper)(site)).User;
            }

            if (user == null || user.Latitude.HasValue == false || user.Longitude.HasValue == false)
            {
                return null;
            }

            var distance = GetDistanceBetweenTwoPoints(user.Latitude.Value, user.Longitude.Value, latitude, longitude);

            return distance;
        }

        protected double GetDistanceBetweenTwoPoints(decimal lat1, decimal long1, decimal lat2, decimal long2)
        {
            var distance = Math.Sqrt(
                Math.Pow(69.1 * Convert.ToDouble((lat1 - lat2)), 2) +
                Math.Pow(69.1 * Convert.ToDouble((long2 - long1)) *
                Math.Cos(Convert.ToDouble(lat1) / 57.3), 2)) * 1.609344;

            return distance;
        }

        public string GeneratePassword(int length, int numberOfNoneNumericChars)
        {
            return _memberRepo.GeneratePassword(length, numberOfNoneNumericChars); //throws on iOS
        }

        public object ChangePassword(object user, string newPassword)
        {
            ConceptCave.Checklist.Lingo.ObjectProviders.DictionaryObjectProvider result = new ConceptCave.Checklist.Lingo.ObjectProviders.DictionaryObjectProvider();

            if (string.IsNullOrEmpty(newPassword) == true)
            {
                result.SetValue(null, "success", false);
                result.SetValue(null, "message", "Password must have a value");

                return result;
            }

            Guid userId = Guid.Empty;

            if (user is Guid)
            {
                userId = (Guid)user;
            }
            else if (user is string)
            {
                var u = _memberRepo.GetByUsername((string)user, MembershipLoadInstructions.None);

                if (u != null)
                {
                    userId = u.UserId;
                }
            }
            else if (user is FacilityUserHelper)
            {
                userId = ((FacilityUserHelper)user).Id;
            }

            if (userId == Guid.Empty)
            {
                result.SetValue(null, "success", false);
                result.SetValue(null, "message", "Object can not be mapped to a user");

                return result;
            }

            var r = _memberRepo.ChangePassword(userId, newPassword);

            result.SetValue(null, "success", r.Success);
            result.SetValue(null, "message", r.Message);

            return result;
        }
    }
}
