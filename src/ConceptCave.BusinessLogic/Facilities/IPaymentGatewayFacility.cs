﻿using ConceptCave.Checklist.Interfaces;
using ConceptCave.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.BusinessLogic.Facilities
{
    public interface IPaymentGatewayFacility : IFacility
    {
        string GatewayName { get; }
        
        bool UseSandbox { get; }

        void Prepare(IContextContainer context, PendingPaymentFacility.PendingPayments result, object gatewayContext);
        void Modify(PendingPaymentFacility.PendingPayments pendingPayments);
    }
}
