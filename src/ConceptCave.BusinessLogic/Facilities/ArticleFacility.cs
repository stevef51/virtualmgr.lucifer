﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Checklist.Facilities;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.Lingo;
using ConceptCave.Checklist.Lingo.ObjectProviders;
using ConceptCave.Core;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Repositories;

namespace ConceptCave.BusinessLogic.Facilities
{
    public static class ArticleExtension
    {
        public static DictionaryObjectProvider GroupBy(this IList<ArticleFacility.Article> that, IContextContainer context, string name)
        {
            ContextContainer ctx = new ContextContainer(context);
            ctx.Set<bool>("IsLeafMember", true);            // This requests the .Value

            Func<object, string> fnStr = o =>
                {
                    if (o is ArticleFacility.Article)
                        return ((ArticleFacility.Article)o).Id.ToString();
                    else
                        return o.ToString();
                };
            var dict = (from a in that let v = a.GetValue(ctx, name) where v != null group a by fnStr(v) into g select g).ToDictionary(g => g.Key, g => (object)g.ToList());

            return new DictionaryObjectProvider() { Dictionary = dict };
        }
    }

    public class ArticleFacility : Facility
    {
        public class Article : Node, IObjectSetter
        {
            public override string ToString()
            {
                List<string> list = new List<string>();
                list.Add(string.Format("Id={{{0}}}", Id));

                var dto = _articleRepo.LoadArticle(Id, RepositoryInterfaces.Enums.ArticleLoadInstructions.PrimaryFields);
                if (dto != null)
                {
                    foreach (var v in dto.ArticleBools)
                        list.Add(string.Format("{0}={1}", v.Name, v.Value));
                    foreach (var v in dto.ArticleBigInts)
                        list.Add(string.Format("{0}={1}", v.Name, v.Value));
                    foreach (var v in dto.ArticleDateTimes)
                        list.Add(string.Format("{0}={1:yyyy-MM-ddTHH:mm:ss}", v.Name, v.Value));
                    foreach (var v in dto.ArticleGuids)
                        list.Add(string.Format("{0}={1}", v.Name, v.Value));
                    foreach (var v in dto.ArticleNumbers)
                        list.Add(string.Format("{0}={1}", v.Name, v.Value));
                    foreach (var v in dto.ArticleStrings)
                        list.Add(string.Format("{0}='{1}'", v.Name, v.Value));
                    foreach (var v in dto.ArticleSubArticles)
                        list.Add(string.Format("{0}=(Sub){{{1}}}", v.Name, v.SubArticleId));
                }
                return "[" + string.Join(", ", list) + "]";
            }

            protected static List<object> GetArticleValues(IArticleRepository repo, ArticleDTO dto)
            {
                return
                    dto.ArticleBools.Select(i => (object)i.Value).Concat(
                    dto.ArticleBigInts.Select(i => (object)i.Value)).Concat(
                    dto.ArticleDateTimes.Select(i => (object)i.Value)).Concat(
                    dto.ArticleGuids.Select(i => (object)i.Value)).Concat(
                    dto.ArticleNumbers.Select(i => (object)i.Value)).Concat(
                    dto.ArticleStrings.Select(i => (object)i.Value)).Concat(
                    dto.ArticleSubArticles.Select(i => new Article(repo, i.SubArticleId)))
                    .ToList();
            }

            public class ArticleValue : IEvaluatable
            {
                public Article Article { get; private set; }
                public string Name { get; private set; }

                public ArticleValue(Article article, string name)
                {
                    Article = article;
                    Name = name;
                }

                public object Value
                {
                    get
                    {
                        var repo = Article._articleRepo;
                        var dto = repo.LoadArticleWithPropertyNames(Article.Id, Name);
                        var list = GetArticleValues(repo, dto);

                        if (list.Count > 1)
                            return list;

                        if (list.Count == 1)
                            return list[0];

                        return null;


                    }

                    set
                    {
                        var repo = Article._articleRepo;
                        if (value == null)
                        {
                            repo.DeleteArticlePropertyName(Article.Id, Name);
                        }
                        else
                        {
                            value = value.ConvertToPrimitive();
                            Type type = value.GetType();
                            FnSaveObject setObject = null;
                            if (_saveObject.TryGetValue(type, out setObject))
                            {
                                setObject(Article, Name, value, false, true);
                            }
                        }
                    }
                }

                public object Evaluate(Core.IContextContainer context)
                {
                    return this.Value;
                }

                public void Clear()
                {
                    Article._articleRepo.DeleteArticlePropertyName(Article.Id, Name);
                }

                public void Add(object value)
                {
                    if (value == null)
                        return;

                    value = value.ConvertToPrimitive();
                    FnSaveObject saveObject = null;
                    if (_saveObject.TryGetValue(value.GetType(), out saveObject))
                        saveObject(Article, Name, value, true, false);
                }

                public void AddUnique(object value)
                {
                    if (value == null)
                        return;

                    value = value.ConvertToPrimitive();
                    FnSaveObject saveObject = null;
                    if (_saveObject.TryGetValue(value.GetType(), out saveObject))
                        saveObject(Article, Name, value, true, true);
                }

                public bool Contains(object value)
                {
                    if (value == null)
                        return false;

                    value = value.ConvertToPrimitive();
                    FnLoadObject loadObject = null;
                    if (!_loadObject.TryGetValue(value.GetType(), out loadObject))
                        return false;

                    return loadObject(Article, Name, value, null).Any();
                }
            }

            private delegate void FnSaveObject(Article article, string name, object value, bool addValue, bool ignoreDuplicate);
            private delegate IEnumerable<object> FnLoadObject(Article article, string name, object value, int? index);

            private static Dictionary<Type, FnSaveObject> _saveObject = new Dictionary<Type, FnSaveObject>();
            private static Dictionary<Type, FnLoadObject> _loadObject = new Dictionary<Type, FnLoadObject>();
            protected IArticleRepository _articleRepo;

            static Article()
            {
                _saveObject[typeof(bool)] = SaveBool;                    
                _saveObject[typeof(byte)] = SaveBigInt;
                _saveObject[typeof(UInt16)] = SaveBigInt;
                _saveObject[typeof(Int16)] = SaveBigInt;
                _saveObject[typeof(UInt32)] = SaveBigInt;
                _saveObject[typeof(Int32)] = SaveBigInt;
                _saveObject[typeof(UInt64)] = SaveBigInt;
                _saveObject[typeof(Int64)] = SaveBigInt;
                _saveObject[typeof(float)] = SaveNumber;
                _saveObject[typeof(double)] = SaveNumber;
                _saveObject[typeof(decimal)] = SaveNumber;
                _saveObject[typeof(DateTime)] = SaveDateTime;
                _saveObject[typeof(Guid)] = SaveGuid;
                _saveObject[typeof(String)] = SaveString;
                _saveObject[typeof(Article)] = SaveSubArticle;

                _loadObject[typeof(bool)] = LoadBool;
                _loadObject[typeof(byte)] = LoadBigInt;
                _loadObject[typeof(UInt16)] = LoadBigInt;
                _loadObject[typeof(Int16)] = LoadBigInt;
                _loadObject[typeof(UInt32)] = LoadBigInt;
                _loadObject[typeof(Int32)] = LoadBigInt;
                _loadObject[typeof(UInt64)] = LoadBigInt;
                _loadObject[typeof(Int64)] = LoadBigInt;
                _loadObject[typeof(float)] = LoadNumber;
                _loadObject[typeof(double)] = LoadNumber;
                _loadObject[typeof(decimal)] = LoadNumber;
                _loadObject[typeof(DateTime)] = LoadDateTime;
                _loadObject[typeof(Guid)] = LoadGuid;
                _loadObject[typeof(String)] = LoadString;
                _loadObject[typeof(Article)] = LoadSubArticle;
            }

            protected static int GetNextIndex(Article article, string name, bool addValue)
            {
                var repo = article._articleRepo;
                var articleId = article.Id;
                var articleDto = repo.LoadArticleWithPropertyNames(articleId, name);
                int index = 0;

                if (articleDto != null)
                {
                    var list = GetArticleValues(repo, articleDto);

                    if (!addValue && list.Count > 0)
                        repo.DeleteArticlePropertyName(articleId, name);
                    else
                        index = list.Count;
                }

                return index;
            }

            protected static void Save(Article article)
            {
                var repo = article._articleRepo;
                var dto = repo.LoadArticle(article.Id, RepositoryInterfaces.Enums.ArticleLoadInstructions.None);
                if (dto == null)
                    repo.SaveArticle(new ArticleDTO() { __IsNew = true, Id = article.Id });
            }

            #region SaveXXX
            protected static void SaveBool(Article article, string name, object objValue, bool addValue, bool ignoreDuplicate)
            {
                Save(article);
                var repo = article._articleRepo;

                var value = Convert.ToBoolean(objValue);
                if (ignoreDuplicate && repo.LoadArticleBool(article.Id, name, value, null).Any())
                    return;

                var dto = new ArticleBoolDTO();
                dto.__IsNew = true;
                dto.ArticleId = article.Id;
                dto.Name = name;
                dto.Index = GetNextIndex(article, name, addValue);
                dto.Value = value;
                repo.SaveArticleBool(dto);
            }

            protected static void SaveBigInt(Article article, string name, object objValue, bool addValue, bool ignoreDuplicate)
            {
                Save(article);
                var repo = article._articleRepo;

                var value = Convert.ToInt64(objValue);
                if (ignoreDuplicate && repo.LoadArticleBigInt(article.Id, name, value, null).Any())
                    return;

                var dto = new ArticleBigIntDTO();
                dto.__IsNew = true;
                dto.ArticleId = article.Id;
                dto.Name = name;
                dto.Index = GetNextIndex(article, name, addValue);
                dto.Value = value;
                repo.SaveArticleBigInt(dto);
            }

            protected static void SaveString(Article article, string name, object objValue, bool addValue, bool ignoreDuplicate)
            {
                Save(article);
                var repo = article._articleRepo;

                var value = objValue.ToString();
                if (ignoreDuplicate && repo.LoadArticleString(article.Id, name, value, null).Any())
                    return;
                
                var dto = new ArticleStringDTO();
                dto.__IsNew = true;
                dto.ArticleId = article.Id;
                dto.Name = name;
                dto.Index = GetNextIndex(article, name, addValue);
                dto.Value = value;
                repo.SaveArticleString(dto);
            }

            protected static void SaveNumber(Article article, string name, object objValue, bool addValue, bool ignoreDuplicate)
            {
                Save(article);
                var repo = article._articleRepo;

                var value = Convert.ToDecimal(objValue);
                if (ignoreDuplicate && repo.LoadArticleNumber(article.Id, name, value, null).Any())
                    return;

                var dto = new ArticleNumberDTO();
                dto.__IsNew = true;
                dto.ArticleId = article.Id;
                dto.Name = name;
                dto.Index = GetNextIndex(article, name, addValue);
                dto.Value = value;
                repo.SaveArticleNumber(dto);
            }

            protected static void SaveDateTime(Article article, string name, object objValue, bool addValue, bool ignoreDuplicate)
            {
                Save(article);
                var repo = article._articleRepo;

                var value = Convert.ToDateTime(objValue);
                if (ignoreDuplicate && repo.LoadArticleDateTime(article.Id, name, value, null).Any())
                    return;

                var dto = new ArticleDateTimeDTO();
                dto.__IsNew = true;
                dto.ArticleId = article.Id;
                dto.Name = name;
                dto.Index = GetNextIndex(article, name, addValue);
                dto.Value = value;
                repo.SaveArticleDateTime(dto);
            }

            protected static void SaveGuid(Article article, string name, object objValue, bool addValue, bool ignoreDuplicate)
            {
                Save(article);
                var repo = article._articleRepo;

                var value = (Guid)objValue;
                if (ignoreDuplicate && repo.LoadArticleGuid(article.Id, name, value, null).Any())
                    return;

                var dto = new ArticleGuidDTO();
                dto.__IsNew = true;
                dto.ArticleId = article.Id;
                dto.Name = name;
                dto.Index = GetNextIndex(article, name, addValue);
                dto.Value = value;
                repo.SaveArticleGuid(dto);
            }

            protected static void SaveSubArticle(Article article, string name, object objValue, bool addValue, bool ignoreDuplicate)
            {
                var repo = article._articleRepo;

                Save(article);
                var subArticle = (Article)objValue;
                Save(subArticle);

                if (ignoreDuplicate && repo.LoadArticleSubArticle(article.Id, name, subArticle.Id, null).Any())
                    return;

                var dto = new ArticleSubArticleDTO();
                dto.__IsNew = true;
                dto.ArticleId = article.Id;
                dto.Name = name;
                dto.Index = GetNextIndex(article, name, addValue);
                dto.SubArticleId = subArticle.Id;
                repo.SaveArticleSubArticle(dto);
            }

            #endregion

            #region LoadXXX
            protected static IEnumerable<object> LoadBool(Article article, string name, object objValue, int? index)
            {
                bool? value = null;
                if (objValue != null)
                    value = Convert.ToBoolean(objValue);
                return article._articleRepo.LoadArticleBool(article._id, name, value, index).Select(dto => (object)dto.Value);
            }

            protected static IEnumerable<object> LoadBigInt(Article article, string name, object objValue, int? index)
            {
                long? value = null;
                if (objValue != null)
                    value = Convert.ToInt64(objValue);
                return article._articleRepo.LoadArticleBigInt(article._id, name, value, index).Select(dto => (object)dto.Value);
            }

            protected static IEnumerable<object> LoadString(Article article, string name, object objValue, int? index)
            {
                string value = null;
                if (objValue != null)
                    value = objValue.ToString();
                return article._articleRepo.LoadArticleString(article._id, name, value, index).Select(dto => dto.Value);
            }

            protected static IEnumerable<object> LoadDateTime(Article article, string name, object objValue, int? index)
            {
                DateTime? value = null;
                if (objValue != null)
                    value = Convert.ToDateTime(objValue);
                return article._articleRepo.LoadArticleDateTime(article._id, name, value, index).Select(dto => (object)dto.Value);
            }

            protected static IEnumerable<object> LoadNumber(Article article, string name, object objValue, int? index)
            {
                decimal? value = null;
                if (objValue != null)
                    value = Convert.ToDecimal(objValue);
                return article._articleRepo.LoadArticleNumber(article._id, name, value, index).Select(dto => (object)dto.Value);
            }

            protected static IEnumerable<object> LoadGuid(Article article, string name, object objValue, int? index)
            {
                Guid? value = null;
                if (objValue != null)
                    value = (Guid)objValue;
                return article._articleRepo.LoadArticleGuid(article._id, name, value, index).Select(dto => (object)dto.Value);
            }

            protected static IEnumerable<object> LoadSubArticle(Article article, string name, object objValue, int? index)
            {
                Guid? subArticleId = null;
                if (objValue is Article)
                    subArticleId = ((Article)objValue)._id;
                else if (objValue is Guid)
                    subArticleId = (Guid)objValue;
                return article._articleRepo.LoadArticleSubArticle(article._id, name, subArticleId, index).Select(dto => new Article(article._articleRepo, dto.SubArticleId));
            }


            #endregion

            public Article(IArticleRepository articleRepo)
            {
                _articleRepo = articleRepo;
            }

            public Article(IArticleRepository articleRepo, Guid articleId)
            {
                _articleRepo = articleRepo;
                _id = articleId;
            }


            // 
            // Called of the form "article.name = value"
            public bool SetObject(Core.IContextContainer context, string name, object value)
            {
                new ArticleValue(this, name).Value = value;
                return true;
            }

            public bool GetObject(Core.IContextContainer context, string name, out object result)
            {
                var value = new ArticleValue(this, name);
                var valueValue = value.Value;

                if (valueValue is Article)
                    result = valueValue;
                
                else if (context.Get<bool>("IsFunctionCall"))
                    result = value;

                else if (context.Get<bool>("IsLeafMember"))
                    result = valueValue;

                else
                    result = value;
                return true;
            }

            public object GetValue(Core.IContextContainer context, string name)
            {
                object o = null;
                if (!GetObject(context, name, out o))
                    return null;
                return o;
            }
        }

        private IArticleRepository _articleRepo;

        public ArticleFacility(IArticleRepository articleRepo)
        {
            _articleRepo = articleRepo;
            Name = "Article";
        }

        public Article New()
        {
            return new Article(_articleRepo);
        }

        public Article Load(Guid articleId)
        {
            return new Article(_articleRepo, articleId);
        }

        public Article Load(string articleId)
        {
            return Load(Guid.Parse(articleId));
        }

        public Article FindFirst(string f1, object o1)
        {
            return FindFirst(new object[] { f1, o1 });
        }

        public Article FindFirst(string f1, object o1, string f2, object o2)
        {
            return FindFirst(new object[] { f1, o1, f2, o2 });
        }

        public Article FindFirst(string f1, object o1, string f2, object o2, string f3, object o3)
        {
            return FindFirst(new object[] { f1, o1, f2, o2, f3, o3 });
        }

        public Article FindFirst(string f1, object o1, string f2, object o2, string f3, object o3, string f4, object o4)
        {
            return FindFirst(new object[] { f1, o1, f2, o2, f3, o3, f4, o4 });
        }

        public IList<Article> FindAll(string f1, object o1)
        {
            return FindAll(new object[] { f1, o1 }).ToList();
        }

        public IList<Article> FindAll(string f1, object o1, string f2, object o2)
        {
            return FindAll(new object[] { f1, o1, f2, o2 }).ToList();
        }

        public IList<Article> FindAll(string f1, object o1, string f2, object o2, string f3, object o3)
        {
            return FindAll(new object[] { f1, o1, f2, o2, f3, o3 }).ToList();
        }

        public IList<Article> FindAll(string f1, object o1, string f2, object o2, string f3, object o3, string f4, object o4)
        {
            return FindAll(new object[] { f1, o1, f2, o2, f3, o3, f4, o4 }).ToList();
        }

        /// <summary>
        /// Find the 1st matching Article using a WHERE clause that is formed by pairing objects as name value pairs joined by AND
        /// </summary>
        /// <param name="whereNameEqualValue">Must be even number, assumed name value pairs with AND join</param>
        /// <returns></returns>
        public Article FindFirst(IEnumerable<object> whereNameEqualValue)
        {
            return FindAll(whereNameEqualValue).FirstOrDefault();
        }

        /// <summary>
        /// Find the 1st matching Article using a WHERE clause that is formed by pairing objects as name value pairs joined by AND
        /// </summary>
        /// <param name="fieldNames">List of field names</param>
        /// <param name="equalsValues">List of values (must be same size as fieldNames)</param>
        /// <returns>The first Article which has any field matching any value</returns>
        public Article FindFirst(IEnumerable<string> fieldNames, IEnumerable<object> equalsValues)
        {
            return FindAll(fieldNames, equalsValues).FirstOrDefault();
        }

        /// <summary>
        /// Find the 1st matching Article using a WHERE clause that is formed by pairing objects as name value pairs joined by AND
        /// </summary>
        /// <param name="whereNameEqualValue">Must be even number, assumed name value pairs with AND join</param>
        /// <returns></returns>
        public IList<Article> FindAll(IEnumerable<object> whereNameEqualValue)
        {
            List<string> fieldNames = new List<string>();
            List<object> equalsValues = new List<object>();

            int i = 0;
            foreach (var o in whereNameEqualValue)
            {
                if ((i++ & 1) == 0)
                    fieldNames.Add(o.ToString());
                else
                    equalsValues.Add(o);
            }

            return FindAll(fieldNames, equalsValues);
        }

        /// <summary>
        /// Find the 1st matching Article using a WHERE clause that is formed by pairing objects as name value pairs joined by AND
        /// </summary>
        /// <param name="fieldNames">List of field names</param>
        /// <param name="equalsValues">List of values (must be same size as fieldNames)</param>
        /// <returns>The first Article which has any field matching any value</returns>
        public IList<Article> FindAll(IEnumerable<string> fieldNames, IEnumerable<object> equalsValues)
        {
            var fn = fieldNames.ToArray();
            var ev = equalsValues.ToArray();

            if (fn.Length != ev.Length)
                throw new LingoException(string.Format("FindFirst expects even number of arguments (name value pairs) but received {0} names and {1} values", fn.Length, ev.Length));

            var list = _articleRepo.Find(fn, ev);

            return (from a in list select new Article(_articleRepo, a.Id)).ToList();
        }
    }
}
