﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConceptCave.Checklist.Editor;
using ConceptCave.Checklist.Facilities;
using ConceptCave.Checklist.Interfaces;
using ConceptCave.Checklist.Lingo.ObjectProviders;
using ConceptCave.Core;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces.Repositories;
using System.Transactions;
using ConceptCave.Checklist;
using System.Dynamic;
using ConceptCave.Core.Coding;
using VirtualMgr.MultiTenant;
using ConceptCave.SimpleExtensions;

namespace ConceptCave.BusinessLogic.Facilities
{
    public enum TaskFacilityUserBatchType
    {
        /// <summary>
        /// Either usernames or userids will be supplied
        /// </summary>
        Users,
        /// <summary>
        /// Either label names or label ids will be supplied
        /// </summary>
        Labels,
        /// <summary>
        /// Either user type names or user type ids will be supplied
        /// </summary>
        UserTypes
    }

    /// <summary>
    /// Object to model the different ways in which we are going to support the batch creation of tasks
    /// to users.
    /// </summary>
    public class TaskFacilityUserBatch : Node
    {
        public TaskFacilityUserBatchType BatchType { get; set; }
        public List<object> Items { get; protected set; }

        public TaskFacilityUserBatch() : base()
        {
            Items = new List<object>();
        }

        public override void Decode(IDecoder decoder)
        {
            base.Decode(decoder);

            BatchType = decoder.Decode<TaskFacilityUserBatchType>("BatchType");
            Items = decoder.Decode<List<object>>("Items");
        }

        public override void Encode(IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("BatchType", BatchType);
            encoder.Encode("Items", Items);
        }

        public void Add(object item)
        {
            Items.Add(item);
        }
    }
    public class TaskFacility : TaskAwareFacility
    {
        public class UserInfo
        {
            public Guid UserId { get; set; }
            public int RosterId { get; set; }
            public int? BucketId { get; set; }
            public int? RoleId { get; set; }
            public bool HasError { get; set; }
            public string Error { get; set; }
        }

        private readonly ITaskRepository _taskRepo;
        private readonly ITaskTypeRepository _typeRepo;
        private readonly IMembershipRepository _memberRepo;
        private readonly IHierarchyRepository _hierarchyRepo;
        private readonly IRosterRepository _rosterRepo;
        private readonly IProjectJobTaskWorkingDocumentRepository _taskWdRepo;
        private readonly IWorkingDocumentRepository _wdRepo;
        private readonly ILabelRepository _labelRepo;
        private readonly IMembershipTypeRepository _memberTypeRepo;
        private readonly AppTenant _appTenant;

        private DictionaryObjectProvider _currentTask;
        private ProjectJobTaskDTO _currentTaskDTO;

        public TaskFacility(ITaskRepository taskRepo,
            ITaskTypeRepository typeRepo,
            IMembershipRepository memberRepo,
            IHierarchyRepository hierarchyRepo,
            IRosterRepository rosterRepo,
            IProjectJobTaskWorkingDocumentRepository taskWdRepo,
            IWorkingDocumentRepository wdRepo,
            ILabelRepository labelRepo,
            IMembershipTypeRepository memberTypeRepo,
            AppTenant appTenant)
        {
            Name = "Task";
            _taskRepo = taskRepo;
            _typeRepo = typeRepo;
            _memberRepo = memberRepo;
            _hierarchyRepo = hierarchyRepo;
            _rosterRepo = rosterRepo;
            _taskWdRepo = taskWdRepo;
            _wdRepo = wdRepo;
            _labelRepo = labelRepo;
            _memberTypeRepo = memberTypeRepo;
            _appTenant = appTenant;
        }


        protected Guid ReviewerId
        {
            get
            {
                var docEntity = ContextContainer.Get<WorkingDocumentDTO>();
                return docEntity.ReviewerId;
            }
        }

        protected Guid? RevieweeId
        {
            get
            {
                var docEntity = ContextContainer.Get<WorkingDocumentDTO>();

                if (docEntity.RevieweeId.HasValue)
                {
                    return docEntity.RevieweeId.Value;
                }

                return null;
            }
        }

        public DictionaryObjectProvider Current
        {
            get
            {
                if (_currentTask == null)
                {
                    _currentTask = BuildTask(CurrentTaskId);
                }

                return _currentTask;
            }
        }

        protected ProjectJobTaskDTO CurrentTask
        {
            get
            {
                if (_currentTaskDTO == null)
                {
                    _currentTask = BuildTask(CurrentTaskId);
                }

                return _currentTaskDTO;
            }
        }

        protected DictionaryObjectProvider BuildTask(Guid taskId)
        {
            _currentTaskDTO = _taskRepo.GetById(taskId, RepositoryInterfaces.Enums.ProjectJobTaskLoadInstructions.WorkLog |
                RepositoryInterfaces.Enums.ProjectJobTaskLoadInstructions.TaskType |
                RepositoryInterfaces.Enums.ProjectJobTaskLoadInstructions.Roster |
                RepositoryInterfaces.Enums.ProjectJobTaskLoadInstructions.AllTranslated);

            DictionaryObjectProvider result = new DictionaryObjectProvider();

            result.SetValue(null, "Id", _currentTaskDTO.Id);
            result.SetValue(null, "Name", _currentTaskDTO.Name);
            result.SetValue(null, "Description", _currentTaskDTO.Description);
            result.SetValue(null, "DateCreated", _currentTaskDTO.DateCreated);
            result.SetValue(null, "DateCompleted", _currentTaskDTO.DateCompleted);
            result.SetValue(null, "TaskTypeId", _currentTaskDTO.TaskTypeId);
            result.SetValue(null, "TaskType", _currentTaskDTO.ProjectJobTaskType.Name);
            result.SetValue(null, "RosterId", _currentTaskDTO.RosterId);
            result.SetValue(null, "Roster", _currentTaskDTO.Roster.Name);
            result.SetValue(null, "HierarchyId", _currentTaskDTO.Roster.HierarchyId);
            result.SetValue(null, "BucketId", _currentTaskDTO.HierarchyBucketId);
            result.SetValue(null, "SiteId", _currentTaskDTO.SiteId);
            result.SetValue(null, "Context", _currentTaskDTO.Context);

            if (_currentTaskDTO.ProjectJobTaskWorkLogs.Count > 0)
            {
                var log = _currentTaskDTO.ProjectJobTaskWorkLogs.Last();

                result.SetValue(null, "ClockinStatus", RepositoryInterfaces.Enums.GeoLocationStatus.Unknown.ToString());
                if (log.ClockinLocationStatus.HasValue == true)
                {
                    result.SetValue(null, "ClockinStatus", ((RepositoryInterfaces.Enums.GeoLocationStatus)log.ClockinLocationStatus.Value).ToString());
                }

                if (log.ClockinLocationStatus == (int)RepositoryInterfaces.Enums.GeoLocationStatus.Success)
                {
                    var location = new LatLng(log.ClockinLatitude.Value, log.ClockinLongitude.Value, log.ClockinAccuracy.Value);

                    result.SetValue(null, "ClockinLocation", location);
                }
            }

            return result;
        }

        protected int RetrieveRoster(object roster)
        {
            if (roster is int)
            {
                return (int)roster;
            }
            else if (roster is string)
            {
                var rosters = _rosterRepo.GetAll();
                var rs = (from r in rosters where r.Name == (string)roster select r.Id);

                if (rs.Count() == 0)
                {
                    return -1;
                }

                return rs.First();
            }

            return -1;
        }

        protected Guid RetrieveTaskType(object type)
        {
            if (type is Guid)
            {
                return (Guid)type;
            }
            else if (type is string)
            {
                var taskType = _typeRepo.GetByName((string)type, RepositoryInterfaces.Enums.ProjectJobTaskTypeLoadInstructions.None);

                if (taskType == null)
                {
                    return Guid.Empty;
                }

                return taskType.Id;
            }

            return Guid.Empty;
        }

        protected UserInfo RetreiveUserId(object user, bool throwOnError = false)
        {
            if (user is UserInfo)
            {
                return (UserInfo)user;
            }
            if(user is IDynamicMetaObjectProvider)
            {
                var duser = user as dynamic;
                return new UserInfo()
                {
                    UserId = duser.UserId,
                    RosterId = duser.RosterId,
                    BucketId = duser.BucketId,
                    HasError = duser.HasError,
                    Error = duser.Error,
                    RoleId = duser.RoleId
                };
            }

            var result = HasCurrentTask ? RetrieveUserIdInTask(user) : RetrieveUserIdNotInTask(user);
            if (throwOnError && result.HasError)
            {
                throw new InvalidOperationException(result.Error);
            }
            return result;
        }

        protected UserInfo RetrieveUserIdInTask(object user)
        {
            var result = new UserInfo();
            result.UserId = Guid.Empty;
            result.BucketId = null;
            result.RosterId = CurrentTask.RosterId;
            result.RoleId = null;
            result.HasError = false;
            result.Error = "";

            if (user is Guid)
            {
                result.UserId = (Guid)user;
            }
            else if (user is string)
            {
                string userString = ((string)user).ToLower();

                if (userString == "current")
                {
                    result.UserId = CurrentTask.OwnerId;
                }
                else if(userString == "original")
                {
                    result.UserId = CurrentTask.OriginalOwnerId;
                }
                else if(userString != "parent")
                {
                    // we'll assume its the username of the user
                    var mem = _memberRepo.GetByUsername(userString, RepositoryInterfaces.Enums.MembershipLoadInstructions.None);
                    if(mem != null)
                    {
                        result.UserId = mem.UserId;
                    }
                    else
                    {
                        result.HasError = true;
                        result.Error = string.Format("No user matching {0} could be found", userString);

                        return result;
                    }
                }
            }
            else
            {
                result.HasError = true;
                result.Error = "Unknown parameter type";

                return result;
            }

            // if we reach here we either have a userid, or we have the text 'parent' as being what's handed in
            // lets deal with the easy one first, which is having an id
            if(result.UserId != Guid.Empty)
            {
                var buckets = _hierarchyRepo.GetBucketsForUser(result.UserId, null, RepositoryInterfaces.Enums.HierarchyBucketLoadInstructions.None);

                // lets assign the new task to the hierarchy that the current task is attached to if we can
                var bs = from b in buckets where b.HierarchyId == CurrentTask.Roster.HierarchyId select b;
                if (bs.Count() > 0)
                {
                    buckets = bs.ToList();
                }

                if (buckets.Count == 0)
                {
                    result.HasError = true;
                    result.Error = "User is not in an organisation hierachy";

                    return result;
                }

                result.BucketId = buckets.First().Id;
                result.RoleId = buckets.First().RoleId;
            }
            else
            {
                // at this point the parameter must be parent, which means there is a little work to do
                var buckets = _hierarchyRepo.GetBucketsForUser(CurrentTask.OwnerId, null, RepositoryInterfaces.Enums.HierarchyBucketLoadInstructions.None);

                // lets assign the new task to the hierarchy that the current task is attached to if we can
                var bs = from b in buckets where b.HierarchyId == CurrentTask.Roster.HierarchyId select b;
                if (bs.Count() > 0)
                {
                    buckets = bs.ToList();
                }

                if (buckets.Count == 0 || buckets.First().ParentId.HasValue == false)
                {
                    result.HasError = true;
                    result.Error = "User is not in an organisation hierarchy";

                    return result;
                }

                var parent = _hierarchyRepo.GetById(buckets.First().ParentId.Value, RepositoryInterfaces.Enums.HierarchyBucketLoadInstructions.None);

                if (parent.UserId.HasValue == false)
                {
                    result.HasError = true;
                    result.Error = "Parent bucket in organisation hierarchy for user does not have a user assigned to it";

                    return result;
                }

                result.UserId = parent.UserId.Value;
                result.BucketId = parent.Id;
                result.RoleId = parent.RoleId;
            }

            return result;
        }

        protected UserInfo RetrieveUserIdNotInTask(object user)
        {
            var result = new UserInfo();
            result.UserId = Guid.Empty;
            result.BucketId = null;
            result.RosterId = -1;
            result.RoleId = null;
            result.HasError = false;
            result.Error = "";

            if (user is Guid)
            {
                result.UserId = (Guid)user;
            }
            else if (user is string)
            {
                string userString = ((string)user).ToLower();

                if (userString == "current" || userString == "original")
                {
                    result.UserId = ReviewerId;
                }
                else if (userString != "parent")
                {
                    // we'll assume its the username of the user
                    var mem = _memberRepo.GetByUsername(userString, RepositoryInterfaces.Enums.MembershipLoadInstructions.None);
                    if (mem != null)
                    {
                        result.UserId = mem.UserId;
                    }
                    else
                    {
                        result.HasError = true;
                        result.Error = string.Format("No user matching {0} could be found", userString);

                        return result;
                    }
                }
            }
            else
            {
                result.HasError = true;
                result.Error = "Unknown parameter type";

                return result;
            }

            var currentUserBuckets = _hierarchyRepo.GetBucketsForUser(ReviewerId, null, RepositoryInterfaces.Enums.HierarchyBucketLoadInstructions.None);

            if (currentUserBuckets.Count() == 0)
            {
                result.HasError = true;
                result.Error = "User completing checklist is not in an organisation hierarchy";

                return result;
            }

            // get the primary one if there is one
            var currentBucket = (from b in currentUserBuckets where b.IsPrimary == true select b).DefaultIfEmpty(null).First();
            if (currentBucket == null)
            {
                // fall back to the first one we come across
                currentBucket = currentUserBuckets.First();
            }

            // if we reach here we either have a userid, or we have the text 'parent' as being what's handed in
            // lets deal with the easy one first, which is having an id
            if (result.UserId != Guid.Empty)
            {
                var buckets = _hierarchyRepo.GetBucketsForUser(result.UserId, null, RepositoryInterfaces.Enums.HierarchyBucketLoadInstructions.None);

                if (buckets.Count == 0)
                {
                    result.HasError = true;
                    result.Error = "User is not in an organisation hierarchy";

                    return result;
                }

                // lets assign the new task to the hierarchy that the current task is attached to if we can
                var bs = from b in buckets where b.HierarchyId == currentBucket.HierarchyId select b;
                if (bs.Count() > 0)
                {
                    buckets = bs.ToList();
                }

                var rosters = _rosterRepo.GetForHierarchies(new int[] { buckets.First().HierarchyId });

                if (rosters.Count == 0)
                {
                    result.HasError = true;
                    result.Error = "The hierarchy is not attached to a roster";

                    return result;
                }

                result.BucketId = buckets.First().Id;
                result.RoleId = buckets.First().RoleId;
                result.RosterId = rosters.First().Id;
            }
            else
            {
                // at this point the parameter must be parent, which means there is a little work to do
                var buckets = _hierarchyRepo.GetBucketsForUser(ReviewerId, null, RepositoryInterfaces.Enums.HierarchyBucketLoadInstructions.None);

                if (buckets.Count == 0)
                {
                    result.HasError = true;
                    result.Error = "User is not in an organisation hierarchy";

                    return result;
                }

                // lets assign the new task to the hierarchy that the current task is attached to if we can
                var bs = from b in buckets where b.HierarchyId == currentBucket.HierarchyId select b;
                if (bs.Count() > 0)
                {
                    buckets = bs.ToList();
                }

                if (buckets.First().ParentId.HasValue == false)
                {
                    result.HasError = true;
                    result.Error = "User is not in an organisation hierarchy";

                    return result;
                }

                var parent = _hierarchyRepo.GetById(buckets.First().ParentId.Value, RepositoryInterfaces.Enums.HierarchyBucketLoadInstructions.None);

                if (parent.UserId.HasValue == false)
                {
                    result.HasError = true;
                    result.Error = "Parent bucket in organisation hierarchy for user does not have a user assigned to it";

                    return result;
                }

                var rosters = _rosterRepo.GetForHierarchies(new int[] { parent.HierarchyId });

                if (rosters.Count == 0)
                {
                    result.HasError = true;
                    result.Error = "The hierarchy is not attached to a roster";

                    return result;
                }

                result.UserId = parent.UserId.Value;
                result.BucketId = parent.Id;
                result.RoleId = parent.RoleId;
                result.RosterId = rosters.First().Id;
            }

            return result;
        }

        protected Guid? RetrieveSiteId(object site)
        {
            if (site == null)
            {
                return null;
            }
            if (site is Guid)
            {
                return (Guid)site;
            }
            else if (site is string)
            {
                string siteString = (string)site;
                if (siteString.ToLower() == "current")
                {
                    if (HasCurrentTask == true && CurrentTask.SiteId.HasValue)
                    {
                        return CurrentTask.SiteId.Value;
                    }
                    else
                    {
                        return RevieweeId;
                    }
                }
                else
                {
                    var mem = _memberRepo.GetByUsername(siteString, RepositoryInterfaces.Enums.MembershipLoadInstructions.None);
                    return mem.UserId;
                }
            }

            return null;
        }

        public object InterpretUser(object user)
        {
            var userInfo = RetreiveUserId(user);

            DictionaryObjectProvider result = new DictionaryObjectProvider();
            result.SetValue(null, "HasError", userInfo.HasError);
            result.SetValue(null, "Error", userInfo.Error);
            result.SetValue(null, "UserId", userInfo.UserId);
            result.SetValue(null, "BucketId", userInfo.BucketId);
            result.SetValue(null, "RosterId", userInfo.RosterId);
            result.SetValue(null, "RoleId", userInfo.RoleId);

            return result;
        }

        public TaskFacilityUserBatch CreateUsersBatch()
        {
            TaskFacilityUserBatch result = new TaskFacilityUserBatch() { BatchType = TaskFacilityUserBatchType.Users };

            return result;
        }

        public TaskFacilityUserBatch CreateLabelsBatch()
        {
            TaskFacilityUserBatch result = new TaskFacilityUserBatch() { BatchType = TaskFacilityUserBatchType.Labels };

            return result;
        }

        public TaskFacilityUserBatch CreateUserTypesBatch()
        {
            TaskFacilityUserBatch result = new TaskFacilityUserBatch() { BatchType = TaskFacilityUserBatchType.UserTypes };

            return result;
        }

        public object New(string name, string description, object type, object user, object site, int level, string status)
        {
            return NewTasks(name, description, type, user, site, level, status, (DateTime?)null, null, null, null);
        }

        public object New(string name, string description, object type, object user, object site, int level, string status, object startTime)
        {
            if (startTime == null || startTime is TimeSpan?)
            {
                return New(name, description, type, user, site, level, status, startTime, null);
            }

            throw new ArgumentException("StartTime must be one of null, DateTime or Timespan");
        }

        public object New(string name, string description, object type, object user, object site, int level, string status, object startTime, object lateAfter)
        {
            if (startTime == null || startTime is DateTime?)
            {
                return NewUsingDateTime(name, description, type, user, site, level, status, (DateTime?)startTime, (TimeSpan?)lateAfter, null, null);
            }

            if (startTime is TimeSpan?)
            {
                return NewUsingTimeSpan(name, description, type, user, site, level, status, (TimeSpan?)startTime, (TimeSpan?)lateAfter, null, null);
            }

            throw new ArgumentException("StartTime must be one of null, DateTime or Timespan");
        }

        public object New(string name, string description, object type, object user, object site, int level, string status, object startTime, object lateAfter, object assetId)
        {
            if (startTime == null || startTime is DateTime?)
            {
                return NewUsingDateTime(name, description, type, user, site, level, status, (DateTime?)startTime, (TimeSpan?)lateAfter, (int?)assetId, null);
            }

            if (startTime is TimeSpan?)
            {
                return NewUsingTimeSpan(name, description, type, user, site, level, status, (TimeSpan?)startTime, (TimeSpan?)lateAfter, (int?)assetId, null);
            }

            throw new ArgumentException("StartTime must be one of null, DateTime or Timespan");
        }

        public object New(string name, string description, object type, object user, object site, int level, string status, object startTime, object lateAfter, object assetId, object notifyUser)
        {
            if (startTime == null || startTime is DateTime?)
            {
                return NewUsingDateTime(name, description, type, user, site, level, status, (DateTime?)startTime, (TimeSpan?)lateAfter, (int?)assetId, (bool?)notifyUser);
            }

            if (startTime is TimeSpan?)
            {
                return NewUsingTimeSpan(name, description, type, user, site, level, status, (TimeSpan?)startTime, (TimeSpan?)lateAfter, (int?)assetId, (bool?)notifyUser);
            }

            throw new ArgumentException("StartTime must be one of null, DateTime or Timespan");
        }

        protected object NewUsingTimeSpan(string name, string description, object type, object user, object site, int level, string status, TimeSpan? startTime, TimeSpan? lateAfter, int? assetId, bool? notifyUser)
        {
            DateTime? utcStartTime = null;
            if (startTime.HasValue)
            {
                var tz = _appTenant.TimeZoneInfo();
                var localStartTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, tz).Date.Add(startTime.Value);
                utcStartTime = TimeZoneInfo.ConvertTimeToUtc(localStartTime, tz);
            }
            return NewTasks(name, description, type, user, site, level, status, utcStartTime, lateAfter, assetId, notifyUser);
        }

        protected object NewUsingDateTime(string name, string description, object type, object user, object site, int level, string status, DateTime? startTimeUtc, TimeSpan? lateAfter, int? assetId, bool? notifyUser)
        {
            return NewTasks(name, description, type, user, site, level, status, startTimeUtc, lateAfter, assetId, notifyUser);
        }

        protected object NewTasks(string name, string description, object type, object user, object site, int level, string status, DateTime? startTimeUtc, TimeSpan? lateAfter, int? assetId, bool? notifyUser)
        {
            if ((user is TaskFacilityUserBatch) == false)
            {
                return NewTask(name, description, type, user, site, level, status, startTimeUtc, lateAfter, assetId, notifyUser);
            }

            ListObjectProvider result = new ListObjectProvider();

            // we can shortcut a few things so these don't get repeatedly looked up
            Guid taskTypeId = RetrieveTaskType(type);
            Guid? siteId = RetrieveSiteId(site);

            TaskFacilityUserBatch batch = (TaskFacilityUserBatch)user;
            List<UserInfo> userIds = new List<UserInfo>();

            switch (batch.BatchType)
            {
                case TaskFacilityUserBatchType.Users:
                    userIds.AddRange(batch.Items.Select(u => RetreiveUserId(u, true)));
                    break;

                case TaskFacilityUserBatchType.Labels:
                    // Labels are use to 'target' both HierarchyBuckets AND users, so for each Label we find HierarchyBuckets and Users with each Label
                    var labelledBuckets = new List<List<HierarchyBucketDTO>>();
                    var labelledUsers = new List<List<Guid>>();
                    foreach (var item in batch.Items)
                    {
                        Guid labelId = Guid.Empty;
                        if (item is Guid)
                        {
                            labelId = (Guid)item;
                        }
                        else if (item is string)
                        {
                            var label = _labelRepo.GetByName((string)item);
                            if (label == null)
                            {
                                continue;
                            }

                            labelId = label.Id;
                        }
                        else
                        {
                            continue;
                        }
                        
                        labelledBuckets.Add(_hierarchyRepo.GetBucketsWithLabel(labelId, RepositoryInterfaces.Enums.HierarchyBucketLoadInstructions.Hierarchy | RepositoryInterfaces.Enums.HierarchyBucketLoadInstructions.HierarchyRosters).ToList());
                        labelledUsers.Add(_memberRepo.GetAllUsersByLabel(new[] { labelId }).Select(u => u.UserId).ToList());
                    }

                    // Then we intersect all the lists of Buckets (looking for most targetted Bucket - ie the Bucket which has all labels specified) 
                    var compareBuckets = new LambdaComparer<HierarchyBucketDTO>(b => b.Id.GetHashCode(), (a, b) => a.Id.CompareTo(b.Id));
                    var targetedBuckets = labelledBuckets.Aggregate<IEnumerable<HierarchyBucketDTO>>((p, n) => p.Intersect(n, compareBuckets)).ToList();

                    // Then we intersect all the lists of Users (looking for most targetted Users - ie the Users which has all labels specified) 
                    var targetedUserIds = labelledUsers.Aggregate<IEnumerable<Guid>>((p, n) => p.Intersect(n)).ToList();

                    var hb = targetedBuckets.FirstOrDefault(b => b.Hierarchy.Rosters.Any());
                    if (hb != null)
                    {
                        userIds.AddRange(targetedUserIds.Select(userId => new UserInfo()
                        {
                            UserId = userId,
                            RosterId = hb.Hierarchy.Rosters.First().Id,
                            RoleId = hb.RoleId,
                            BucketId = hb.Id
                        }));
                    }
                    break;

                case TaskFacilityUserBatchType.UserTypes:
                    var types = _memberTypeRepo.GetAll();

                    foreach (var item in batch.Items)
                    {
                        int typeId = -1;
                        if (item is int)
                        {
                            typeId = (int)item;
                        }
                        else if (item is string)
                        {
                            string i = item.ToString().ToLower();
                            var t = (from m in types where m.Name.ToLower() == i select m);

                            if (t.Count() == 0)
                            {
                                continue;
                            }

                            typeId = t.First().Id;
                        }
                        else
                        {
                            continue;
                        }

                        var typeUsers = _memberRepo.GetUsersByType(typeId);
                        userIds.AddRange(typeUsers.Select(u => RetreiveUserId(u.UserId, true)));
                    }
                    break;
            }

            // any users that have expired we are going to remove
            DateTime utcNow = DateTime.UtcNow;
            List<UserInfo> toRemove = new List<UserInfo>();
            foreach(var u in userIds)
            {
                var m = _memberRepo.GetById(u.UserId);

                if(m.ExpiryDate.HasValue == false)
                {
                    continue;
                }

                var tzInfo = TimeZoneInfo.FindSystemTimeZoneById(m.TimeZone);
                DateTime timeInZone = TimeZoneInfo.ConvertTimeFromUtc(utcNow, tzInfo);

                if (timeInZone > m.ExpiryDate)
                {
                    toRemove.Add(u);
                }
            }
            if(toRemove.Count > 0)
            {
                toRemove.ForEach(m => userIds.Remove(m));
            }

            using (TransactionScope scope = new TransactionScope())
            {
                foreach (var u in userIds)
                {
                    var r = NewTask(name, description, type, u, site, level, status, startTimeUtc, lateAfter, assetId, notifyUser);
                    result.Add(r);
                }

                scope.Complete();
            }

            return result;
        }

        protected Guid NewTask(string name, string description, object type, object user, object site, int level, string status, DateTime? startTimeUtc, TimeSpan? lateAfter, int? assetId, bool? notifyUser)
        {
            Guid taskTypeId = RetrieveTaskType(type);

            var userInfo = user as UserInfo ?? RetreiveUserId(user, true);

            Guid? siteId = RetrieveSiteId(site);

            RepositoryInterfaces.Enums.ProjectJobTaskStatus taskStatus = (RepositoryInterfaces.Enums.ProjectJobTaskStatus)Enum.Parse(typeof(RepositoryInterfaces.Enums.ProjectJobTaskStatus), status);

            using (TransactionScope scope = new TransactionScope())
            {
                Guid? groupId = null;

                if (HasCurrentTask == true)
                {
                    groupId = CurrentTask.ProjectJobTaskGroupId;

                    if (groupId.HasValue == false)
                    {
                        groupId = CombFactory.NewComb();

                        CurrentTask.ProjectJobTaskGroupId = groupId;

                        _taskRepo.Save(CurrentTask, true, false);
                    }
                }

                Guid userId = userInfo.UserId;
                int rosterId = userInfo.RosterId;
                int? bucketId = userInfo.BucketId;

                var task = _taskRepo.New(name, description, userId, rosterId, taskTypeId, bucketId, _memberRepo, null, siteId, taskStatus, groupId, assetId);
                task.NotifyUser = notifyUser ?? false;
                task.StartTimeUtc = startTimeUtc;
                if (startTimeUtc.HasValue && lateAfter.HasValue)
                {
                    task.LateAfterUtc = startTimeUtc.Value.Add(lateAfter.Value);
                }
                if (taskStatus == RepositoryInterfaces.Enums.ProjectJobTaskStatus.FinishedComplete ||
                    taskStatus == RepositoryInterfaces.Enums.ProjectJobTaskStatus.FinishedIncomplete ||
                    taskStatus == RepositoryInterfaces.Enums.ProjectJobTaskStatus.FinishedByManagement ||
                    taskStatus == RepositoryInterfaces.Enums.ProjectJobTaskStatus.FinishedBySystem)
                {
                    task.DateCompleted = DateTime.UtcNow;
                }

                task.Level = level;
                task.RoleId = userInfo.RoleId;
                _taskRepo.Save(task, true, false);

                scope.Complete();

                return task.Id;
            }
        }

        public void AddLabelsToTask(object taskId, string labels)
        {
            Guid tId = Guid.Empty;

            if (taskId is Guid)
            {
                tId = (Guid)taskId;
            }
            else if (taskId is string)
            {
                tId = Guid.Parse((string)taskId);
            }

            var split = labels.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);

            var task = _taskRepo.GetById(tId, RepositoryInterfaces.Enums.ProjectJobTaskLoadInstructions.Labels);

            var toAdd = new List<LabelDTO>();

            foreach (string s in split)
            {
                if ((from l in task.ProjectJobTaskLabels where l.Label.Name.ToLower() == s.ToLower() select l).Count() > 0)
                {
                    continue;
                }

                var lbl = _labelRepo.GetByName(s);
                if (lbl != null)
                    toAdd.Add(lbl);
            }

            foreach (var lbl in toAdd)
            {
                task.ProjectJobTaskLabels.Add(new ProjectJobTaskLabelDTO()
                {
                    __IsNew = true,
                    LabelId = lbl.Id,
                    ProjectJobTaskId = task.Id
                });
            }

            _taskRepo.Save(task, false, true);
        }

        public Guid InsertIntoNewTask(IContextContainer container, string taskName, string taskDescription, object ownerId, object siteId, object taskType, object roster, string taskStatus)
        {
            Guid taskTypeId = RetrieveTaskType(taskType);
            int rId = RetrieveRoster(roster);

            RepositoryInterfaces.Enums.ProjectJobTaskStatus stat = (RepositoryInterfaces.Enums.ProjectJobTaskStatus)Enum.Parse(typeof(RepositoryInterfaces.Enums.ProjectJobTaskStatus), taskStatus);

            var task = _taskRepo.New(taskName, taskDescription, (Guid)ownerId, rId, taskTypeId, null, _memberRepo, null, (Guid?)siteId, stat);

            InsertIntoTask(container, task.Id, true);

            return task.Id;
        }

        public bool InsertIntoTask(IContextContainer container, object taskId, bool moveIfRequired)
        {
            var wd = container.Get<IWorkingDocument>();

            return InsertIntoTask(container, wd.Id, taskId, moveIfRequired);
        }

        /// <summary>
        /// Maps a working document to a task. Can be used to map a checklist that is fired 
        /// outside of a task into a task so that things all hang together.
        /// </summary>
        /// <param name="checklistId">Checklist to put into a task</param>
        /// <param name="taskId">Task to put it in to</param>
        /// <param name="moveIfRequired">True if the task is to be moved from one task to another.</param>
        /// <returns></returns>
        public bool InsertIntoTask(IContextContainer container, object checklistId, object taskId, bool moveIfRequired)
        {
            Guid cId = Guid.Empty;
            Guid tId = Guid.Empty;

            if (checklistId is Guid)
            {
                cId = (Guid)checklistId;
            }
            else if (checklistId is string)
            {
                cId = Guid.Parse((string)checklistId);
            }

            if (taskId is Guid)
            {
                tId = (Guid)taskId;
            }
            else if (taskId is string)
            {
                tId = Guid.Parse((string)taskId);
            }

            if (cId == Guid.Empty || tId == Guid.Empty)
            {
                return false;
            }

            var current = _taskWdRepo.GetForWorkingDocument(cId, RepositoryInterfaces.Enums.ProjectJobTaskWorkingDocumentLoadInstructions.None);

            if (current != null && moveIfRequired == false)
            {
                // its already under a task and we've not been instructed to move it
                return false;
            }

            if (current == null)
            {

                // we use the working document to work out the publishing group to map to
                var wdDto = _wdRepo.GetById(cId, RepositoryInterfaces.Enums.WorkingDocumentLoadInstructions.None);

                current = new ProjectJobTaskWorkingDocumentDTO()
                {
                    __IsNew = true,
                    PublishingGroupResourceId = wdDto.PublishingGroupResourceId,
                    WorkingDocumentId = wdDto.Id
                };
            }

            current.ProjectJobTaskId = tId;

            _taskWdRepo.Save(current, false, false);

            return true;
        }

        public DictionaryObjectProvider GetTask(IContextContainer context, object taskId)
        {
            Guid id = Guid.Empty;

            if (taskId is string)
            {
                id = Guid.Parse((string)taskId);
            }
            else if (taskId is Guid)
            {
                id = (Guid)taskId;
            }

            return BuildTask(id);
        }

        /// <summary>
        /// Tasks can be related to each other using the following sort of form
        /// source task relates to destination task via an audit relationship.
        /// This method gets the task related to TaskId via the relationship. The IsSource parameter
        /// denotes which side of the relationship TaskId is on.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="taskId">Id of task that that is a part of the relationship</param>
        /// <param name="relationship">The type of relationship</param>
        /// <param name="isSource">Wether or not TaskId is on the source or destination side</param>
        /// <returns></returns>
        public DictionaryObjectProvider GetTaskViaRelationship(IContextContainer context, object taskId, string relationship, bool isSource)
        {
            Guid id = Guid.Empty;

            if (taskId is string)
            {
                id = Guid.Parse((string)taskId);
            }
            else if (taskId is Guid)
            {
                id = (Guid)taskId;
            }

            var type = _taskRepo.GetRelationshipTypeByName(relationship);

            if (type == null)
            {
                return null;
            }

            ProjectJobTaskRelationshipDTO r = null;

            if (isSource)
            {
                r = _taskRepo.GetRelationshipBySourceId(id, type.Id);

                // we've been given the source, so we want the destination
                return BuildTask(r.DestinationProjectJobTaskId);
            }
            else
            {
                r = _taskRepo.GetRelationshipByDestinationId(id, type.Id);

                // we've been given the destination, so we want the source
                return BuildTask(r.SourceProjectJobTaskId);
            }
        }

        public bool SetTaskOwner(object owner)
        {
            return SetTaskOwner(owner, null);
        }

        public bool SetTaskOwner(object owner, object task)
        {
            var taskId = GetTaskId(task);
            var ownerId = GetUserId(owner);

            if (!ownerId.HasValue)
                return false;

            var taskDto = _taskRepo.GetById(taskId, RepositoryInterfaces.Enums.ProjectJobTaskLoadInstructions.None);
            if (taskDto == null)
                return false;
 
            taskDto.OwnerId = ownerId.Value;

            _taskRepo.Save(taskDto, false, false);

            return true;
        }

        public bool SetTaskContext(object task, string context)
        {
            var taskId = GetTaskId(task);

            var taskDto = _taskRepo.GetById(taskId, RepositoryInterfaces.Enums.ProjectJobTaskLoadInstructions.None);
            if (taskDto == null)
                return false;

            taskDto.Context = context;

            _taskRepo.Save(taskDto, false, false);

            return true;
        }

        public bool SetTaskStatus(object task, string status)
        {
            var taskId = GetTaskId(task);

            var taskDto = _taskRepo.GetById(taskId, RepositoryInterfaces.Enums.ProjectJobTaskLoadInstructions.None);
            if (taskDto == null)
                return false;

            taskDto.Status = (int)(RepositoryInterfaces.Enums.ProjectJobTaskStatus)Enum.Parse(typeof(RepositoryInterfaces.Enums.ProjectJobTaskStatus), status);
            _taskRepo.Save(taskDto, false, false);

            return true;
        }

        public ListObjectProvider GetRosterTasksWithStatus(IContextContainer context, object rosterObj, object statusObj)
        {
            TaskStatusFilter status = TaskStatusFilter.All;
            if (statusObj is int)
            {
                status = (TaskStatusFilter)statusObj;
            }
            else if (statusObj is string)
            {
                status = (TaskStatusFilter)Enum.Parse(typeof(TaskStatusFilter), statusObj.ToString());
            }
            int rosterId = 0;
            if (rosterObj is int)
            {
                rosterId = (int)rosterObj;
            }
            else if (rosterObj is string)
            {
                var rosterDto = _rosterRepo.FindByName(rosterObj.ToString(), RepositoryInterfaces.Enums.RosterLoadInstructions.None);
                if (rosterDto != null)
                {
                    rosterId = rosterDto.Id;
                }
            }
            return new ListObjectProvider(_taskRepo.GetForRoster(rosterId, status, RepositoryInterfaces.Enums.ProjectJobTaskLoadInstructions.None).Select(r => r.Id.ToString()).ToArray());
        }

        protected Guid GetTaskId(object task)
        {
            Guid taskId = Guid.Empty;
            if (task == null)
            {
                taskId = this.CurrentTaskId;
            }
            else if (task is string)
            {
                taskId = Guid.Parse(task.ToString());
            }
            else if (task is Guid)
            {
                taskId = (Guid)task;
            }
            else if (task is DictionaryObjectProvider)
            {
                taskId = (Guid)((DictionaryObjectProvider)task).GetValue(null, "Id");
            }
            return taskId;
        }

        protected Guid? GetUserId(object user)
        {
            Guid? userId = null;
            if (user is string)
            {
                userId = Guid.Parse(user.ToString());
            }
            else if (user is Guid)
            {
                userId = (Guid)user;
            }
            return userId;
        }

        public bool SetTaskSite(object task, object site)
        {
            var taskId = GetTaskId(task);
            var siteId = GetUserId(site);

            var taskDto = _taskRepo.GetById(taskId, RepositoryInterfaces.Enums.ProjectJobTaskLoadInstructions.None);
            if (taskDto == null)
                return false;

            taskDto.SiteId = siteId;

            _taskRepo.Save(taskDto, false, false);

            return true;
        }

        public bool SetTaskLengthInDays(object task, object lengthInDays)
        {
            var taskId = GetTaskId(task);
            var taskDto = _taskRepo.GetById(taskId, RepositoryInterfaces.Enums.ProjectJobTaskLoadInstructions.None);
            if (taskDto == null)
                return false;

            taskDto.EndsOn = _taskRepo.CalculateTaskLengthInDays(taskDto.RosterId, lengthInDays?.ToString(), null);

            _taskRepo.Save(taskDto, false, false);

            return true;
        }
    }
}
