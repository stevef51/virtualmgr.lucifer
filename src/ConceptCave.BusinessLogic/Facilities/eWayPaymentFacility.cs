﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConceptCave.Checklist.Interfaces;
using System.Net.Mail;
//using ConceptCave.Checklist.Properties;
using ConceptCave.Checklist.Commands.Runtime;
using ConceptCave.Core;
using ConceptCave.Checklist.Core;
using System.IO;
using System.Collections.Specialized;
using eWAY.Rapid.Models;
using ConceptCave.BusinessLogic.Facilities;
using ConceptCave.Checklist.Lingo.ObjectProviders;
using ConceptCave.BusinessLogic.Questions;
using eWAY.Rapid;
using ConceptCave.RepositoryInterfaces.Repositories;
using Newtonsoft.Json;
using ConceptCave.Core.Coding;
using ConceptCave.DTO.DTOClasses;
using NLog;

namespace ConceptCave.Checklist.Facilities
{
    public class eWayPaymentMerchantConfig
    {
        public string APIKey { get; set; }
        public string Password { get; set; }
    }

    public class eWayPaymentFacility : Facility, IPaymentGatewayFacility
    {
        private static readonly Logger log = LogManager.GetCurrentClassLogger();
        public const string GATEWAY_NAME = "eway";

        public string GatewayName => GATEWAY_NAME;

        public class TransactionTemplate : Node, IHasName, IHasIndex
        {
            private readonly IProductRepository _productRepo;

            public string JSON { get; set; }
            public string Name { get; set; }
            public Func<int> Index { get; set; }

            public TransactionTemplate() : base() 
            {
            }

            public TransactionTemplate(IProductRepository productRepo, TransactionTemplate template)
            {
                _productRepo = productRepo;
                JSON = template.JSON;
            }

            private string JSONEscape(string s) => s.Replace("'", "\'").Replace("\"", "\\\"");

            public object Expand(ConceptCave.Core.IContextContainer context)
            {
                ILingoProgram program = context.Get<ILingoProgram>();

                string json = program.ExpandString(JSON ?? "", JSONEscape);

                JsonFacility jf = new JsonFacility();
                return jf.FromJson(context, json);
            }

            public object Expand(ConceptCave.Core.IContextContainer context, OrderFacilityOrder order)
            {
                ILingoProgram program = context.Get<ILingoProgram>();

                string json = program.ExpandString(JSON ?? "", JSONEscape);

                Transaction transaction = JsonConvert.DeserializeObject<Transaction>(json);
                transaction.LineItems = new List<LineItem>();

                Dictionary<int, ProductDTO> products = new Dictionary<int, ProductDTO>();
                foreach(var orderItem in order.Items)
                {
                    ProductDTO product = null;
                    if (!products.TryGetValue(orderItem.ProductId, out product))
                    {
                        product = _productRepo.GetById(orderItem.ProductId);
                        products.Add(product.Id, product);
                    }

                    var description = product.Name + "\n" + (!string.IsNullOrWhiteSpace(product.Description) ? product.Description + "\n" : "");
                    if (orderItem.StartDate != null)
                    {
                        description += ((DateTime?)orderItem.StartDate).Value.ToString("d MMM yyyy");
                    }
                    if (orderItem.EndDate != null)
                    {
                        description += " to " + ((DateTime?)orderItem.EndDate).Value.ToString("d MMM yyyy");
                    }
                    transaction.LineItems.Add(new LineItem()
                    {
                        SKU = product.Code,
                        Quantity = (int)orderItem.Quantity,
                        UnitCost = orderItem.Price.HasValue ? (int)(orderItem.Price * 100M) : 0,
                        Description = description
                    });
                }

                transaction.PaymentDetails = transaction.PaymentDetails ?? new PaymentDetails();
                transaction.PaymentDetails.TotalAmount = (int)(order.TotalPrice * 100M);

                JsonFacility jf = new JsonFacility();
                return jf.FromJson(context, JsonConvert.SerializeObject(transaction));
            }

            public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
            {
                base.Encode(encoder);

                encoder.Encode("Name", Name);
                encoder.Encode("JSON", JSON);
            }

            public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
            {
                base.Decode(decoder);

                Name = decoder.Decode<string>("Name");
                JSON = decoder.Decode<string>("JSON");
            }

            public override string ToString()
            {
                return JSON;
            }

        }

        private INamedList<TransactionTemplate> _templates = new NamedList<TransactionTemplate>();
        private Func<bool, IRapidClientSync> _fnRapidClient;
        private readonly IProductRepository _productRepo;
        private readonly IPendingPaymentRepository _pendingPaymentRepo;
        private readonly IGatewayCustomerRepository _gatewayCustomerRepo;
        private readonly IOrderRepository _orderRepo;

        public bool UseSandbox { get; set; } = true;

        public eWayPaymentFacility(Func<bool, IRapidClientSync> fnRapidClient, IProductRepository productRepo, IPendingPaymentRepository pendingPaymentRepo, IGatewayCustomerRepository gatewayCustomerRepo, IOrderRepository orderRepo)
        {
            Name = "eWayPayment";
            _fnRapidClient = fnRapidClient;
            _productRepo = productRepo;
            _pendingPaymentRepo = pendingPaymentRepo;
            _gatewayCustomerRepo = gatewayCustomerRepo;
            _orderRepo = orderRepo;
        }

        public override void Encode(ConceptCave.Core.Coding.IEncoder encoder)
        {
            base.Encode(encoder);

            encoder.Encode("Templates", _templates);
            encoder.Encode("UseSandbox", UseSandbox);
        }

        public override void Decode(ConceptCave.Core.Coding.IDecoder decoder)
        {
            base.Decode(decoder);

            _templates = decoder.Decode<INamedList<TransactionTemplate>>("Templates");
            UseSandbox = decoder.Decode<bool>("UseSandbox");
        }

        public TransactionTemplate TemplateByName(IContextContainer context, string name)
        {
            var template = _templates.Lookup(name);
            if (template == null)
                return null;
            return new TransactionTemplate(_productRepo, template);
        }

        public TransactionTemplate TemplateByIndex(IContextContainer context, int index)
        {
            var template = _templates.Lookup(index);
            if (template == null)
                return null;
            return new TransactionTemplate(_productRepo, template);
        }

        public INamedList<TransactionTemplate> Templates
        {
            get
            {
                return _templates;
            }
        }

        public Transaction PrepareTransaction(Transaction transaction, PendingPaymentFacility.PendingPayment pendingPayment)
        {
            transaction.LineItems = new List<LineItem>();
            var li = new LineItem()
            {
                Description = pendingPayment.Description,
                Total = (int)(pendingPayment.DueAmount * 100)
            };
            transaction.LineItems.Add(li);
            transaction.PaymentDetails = transaction.PaymentDetails ?? new PaymentDetails();
            transaction.PaymentDetails.TotalAmount = (int)(pendingPayment.DueAmount * 100);
            return transaction;
        }

        public DictionaryObjectProvider PrepareTransaction(IContextContainer context, DictionaryObjectProvider transactionObj, PendingPaymentFacility.PendingPayment pendingPayment)
        {
            JsonFacility jf = new JsonFacility();
            var json = jf.ToJson(context, transactionObj);

            var transaction = Newtonsoft.Json.JsonConvert.DeserializeObject<Transaction>(json);

            transaction.LineItems = new List<LineItem>();
            var li = new LineItem()
            {
                Description = pendingPayment.Description,
                Total = (int)(pendingPayment.DueAmount * 100)
            };
            transaction.LineItems.Add(li);
            transaction.PaymentDetails = transaction.PaymentDetails ?? new PaymentDetails();
            transaction.PaymentDetails.TotalAmount = (int)(pendingPayment.DueAmount * 100);

            return (DictionaryObjectProvider)jf.FromJson(context, JsonConvert.SerializeObject(transaction));
        }

        public DictionaryObjectProvider CalculateTotal(IContextContainer context, DictionaryObjectProvider transactionObj)
        {
            JsonFacility jf = new JsonFacility();
            var json = jf.ToJson(context, transactionObj);

            var transaction = Newtonsoft.Json.JsonConvert.DeserializeObject<Transaction>(json);

            // Needs something here ..
            transaction.RedirectURL = "https://virtualmgr.com";

            if (transaction.PaymentDetails == null)
            {
                int totalAmount = 0;
                foreach (var li in transaction.LineItems)
                {
                    totalAmount += li.UnitCost * li.Quantity;
                }

                transaction.PaymentDetails = new PaymentDetails();
                transaction.PaymentDetails.TotalAmount = totalAmount;
            }

            return (DictionaryObjectProvider)jf.FromJson(context, JsonConvert.SerializeObject(transaction));
        }

        public CreateResponse Create(IContextContainer context, DictionaryObjectProvider transactionObj)
        {
            transactionObj = CalculateTotal(context, transactionObj);

            JsonFacility jf = new JsonFacility();
            var json = jf.ToJson(context, transactionObj);

            var transaction = Newtonsoft.Json.JsonConvert.DeserializeObject<Transaction>(json);
            transaction.SaveCustomer = string.IsNullOrEmpty(transaction.Customer.TokenCustomerID);

            var client = _fnRapidClient(UseSandbox);
            var response = client.Create(eWAY.Rapid.Enums.PaymentMethod.ResponsiveShared, transaction);

            // For some reason the LineItems are removed in the "echo"
            if (response.Transaction != null)
            {
                response.Transaction.LineItems = transaction.LineItems;
            }

            return new CreateResponse()
            {
                UseSandbox = UseSandbox,
                AccessCode = response.AccessCode,
                SharedPaymentURL = response.SharedPaymentUrl,
                Transaction = JsonConvert.SerializeObject(response.Transaction),
                Errors = response.Errors?.Count > 0 ? (from errorCode in response.Errors select RapidClientFactory.UserDisplayMessage(errorCode, "EN")).ToArray() : null
            };
        }

        public (bool success, Guid? paymentResponseId, string[] errors) DirectTransaction(PendingPaymentDTO pendingPayment, decimal payAmount)
        {
            var client = _fnRapidClient(pendingPayment.Sandbox);
            var transaction = JsonConvert.DeserializeObject<Transaction>(pendingPayment.GatewayContext);

            var gatewayCustomer = _gatewayCustomerRepo.GetById(pendingPayment.UserId, this.GatewayName);
            if (gatewayCustomer == null)
            {
                return (false, null, new[] { $"User {pendingPayment.UserId} missing TokenCustomerId for {this.GatewayName}" });
            }


            transaction.Customer = new eWAY.Rapid.Models.Customer()
            {
                TokenCustomerID = gatewayCustomer.TokenCustomerId
            };
            transaction.PaymentDetails = transaction.PaymentDetails ?? new PaymentDetails();
            transaction.PaymentDetails.TotalAmount = (int)(payAmount * 100);
            transaction.TransactionType = eWAY.Rapid.Enums.TransactionTypes.Recurring;

            var r = client.Create(eWAY.Rapid.Enums.PaymentMethod.Direct, transaction);

            var errors = r.Errors != null && r.Errors.Any() ? (from code in r.Errors select $"{code} - {eWAY.Rapid.RapidClientFactory.UserDisplayMessage(code, "EN")}").ToArray() : null;

            var transactionSuccess = r.TransactionStatus?.Status ?? false;
            var utcNow = DateTime.UtcNow;
            var pr = new PaymentResponseDTO()
            {
                __IsNew = true,
                Id = CombFactory.NewComb(),
                PendingPayment = pendingPayment,
                DateUtc = utcNow,
                GatewayAuthorisationCode = r.TransactionStatus?.ProcessingDetails?.AuthorisationCode ?? "",
                GatewayTransactionId = r.TransactionStatus?.TransactionID.ToString() ?? "",
                ResponseCode = r.TransactionStatus?.ProcessingDetails?.ResponseCode ?? "",
                ResponseMessage = r.TransactionStatus?.ProcessingDetails?.ResponseMessage ?? "",
                RequestedAmount = payAmount,
                TransactedAmount = transactionSuccess ? r.TransactionStatus?.Total / 100M : 0M,
                TransactionStatus = transactionSuccess,
                Response = JsonConvert.SerializeObject(r),
                Errors = errors != null ? string.Join(", ", errors) : null
            };
            pendingPayment.PaymentResponses.Add(pr);

            // If the Tx failed then mark its LastFailedDate ..
            if (!transactionSuccess)
            {
                // Leave the PendingPayment as Scheduled so we will retry this next time round
                pendingPayment.StatusDateUtc = utcNow;
                pendingPayment.LastFailedDateUtc = utcNow;
            }
            _pendingPaymentRepo.Save(pendingPayment, true, true);

            if (errors != null)
            {
                return (false, pr.Id, errors);
            }

            // Can Mark as PaidInFull ??
            var totalPaid = pendingPayment.PaymentResponses.Where(_ => _.TransactionStatus && _.TransactedAmount.HasValue).Sum(_ => _.TransactedAmount.Value);
            if (totalPaid >= pendingPayment.DueAmount)
            {
                pendingPayment.LastFailedDateUtc = null;
                pendingPayment.Status = (int)PendingPaymentStatus.Paid;
                pendingPayment.StatusDateUtc = utcNow;
                pendingPayment.PaidInFullDateUtc = utcNow;
                _pendingPaymentRepo.Save(pendingPayment, false, false);
            }

            return (transactionSuccess, pr.Id, null);
        }

        public class eWayPaymentTransactionContext
        {
            public eWAY.Rapid.Models.Transaction Transaction { get; set; }
            public eWAY.Rapid.Models.CreateTransactionResponse Response { get; set; }
        }


        public void Prepare(IContextContainer context, PendingPaymentFacility.PendingPayments pendingPayments, object gatewayContext)
        {
            DictionaryObjectProvider transactionObj = (DictionaryObjectProvider)gatewayContext;

            JsonFacility jf = new JsonFacility();
            var json = jf.ToJson(context, transactionObj);

            eWayPaymentTransactionContext ewayContext = new eWayPaymentTransactionContext();
            ewayContext.Transaction = JsonConvert.DeserializeObject<Transaction>(json);
            ewayContext.Transaction.RedirectURL = "https://virtualmgr.com";                     // This in theory should never redirect to according to doc, but is required

            foreach(PendingPaymentFacility.PendingPayment pp in pendingPayments.Payments)
            {
                pp.Sandbox = this.UseSandbox;
                pp.GatewayName = this.GatewayName;

                ewayContext.Transaction.LineItems = new List<LineItem>();
                ewayContext.Transaction.LineItems.Add(new LineItem()
                {
                    Description = pp.Description,
                    Quantity = 1,
                    UnitCost = (int)(pp.DueAmount * 100)
                });
                ewayContext.Transaction.PaymentDetails = ewayContext.Transaction.PaymentDetails ?? new PaymentDetails();
                ewayContext.Transaction.PaymentDetails.TotalAmount = (int)(pp.DueAmount * 100);

                pp.GatewayContext = JsonConvert.SerializeObject(ewayContext);
            }
        }
        public void Modify(PendingPaymentFacility.PendingPayments pendingPayments)
        {
            foreach (PendingPaymentFacility.PendingPayment pp in pendingPayments.Payments)
            {
                var ewayContext = JsonConvert.DeserializeObject<eWayPaymentTransactionContext>(pp.GatewayContext);
                ewayContext.Transaction.LineItems = new List<LineItem>();
                ewayContext.Transaction.LineItems.Add(new LineItem()
                {
                    Description = pp.Description,
                    Quantity = 1,
                    UnitCost = (int)(pp.DueAmount * 100)
                });
                ewayContext.Transaction.PaymentDetails = ewayContext.Transaction.PaymentDetails ?? new PaymentDetails();
                ewayContext.Transaction.PaymentDetails.TotalAmount = (int)(pp.DueAmount * 100);

                pp.GatewayContext = JsonConvert.SerializeObject(ewayContext);
            }
        }

        public class QueryTransactionResponse
        {
            public eWAY.Rapid.Models.QueryTransactionResponse TransactionResponse { get; set; }
            public List<PendingPaymentFacility.PendingPayment> PendingPayments { get; set; }
        }

        public QueryTransactionResponse QueryTransaction(Guid pendingPaymentId, bool ignoreIfProcessing)
        {
            var pendingPayment = _pendingPaymentRepo.GetPendingPaymentById(pendingPaymentId, RepositoryInterfaces.Enums.PendingPaymentLoadInstructions.PaymentResponses);
            if (pendingPayment == null || (ignoreIfProcessing && pendingPayment.Status == (int)PendingPaymentStatus.Processing))
                return null;

            var context = JsonConvert.DeserializeObject<eWayPaymentTransactionContext>(pendingPayment.GatewayContext);

            var client = _fnRapidClient(pendingPayment.Sandbox);
            var r = client.QueryTransaction(context.Response.AccessCode);

            if (r.TransactionStatus == null || r.TransactionStatus.TransactionID == 0)
                return null;

            var utcNow = DateTime.UtcNow;
            if (!string.IsNullOrEmpty(r.Transaction.Customer.TokenCustomerID))
            {
                var gatewayCustomer = _gatewayCustomerRepo.GetById(pendingPayment.UserId, this.GatewayName);
                if (gatewayCustomer == null)
                {
                    log.Info($"New gateway TokenCustomerId {r.Transaction.Customer.TokenCustomerID} for {pendingPayment.UserId}");

                    gatewayCustomer = new GatewayCustomerDTO()
                    {
                        __IsNew = true,
                        UserId = pendingPayment.UserId,
                        GatewayName = this.GatewayName,
                        TokenCustomerId = r.Transaction.Customer.TokenCustomerID
                    };
                    _gatewayCustomerRepo.Save(gatewayCustomer, false, false);
                }
            }

            var pr = pendingPayment.PaymentResponses.FirstOrDefault(_ => _.GatewayTransactionId == r.TransactionStatus.TransactionID.ToString());
            if (pr == null)
            {
                var errors = r.Errors != null && r.Errors.Any() ? (from code in r.Errors select $"{code} - {eWAY.Rapid.RapidClientFactory.UserDisplayMessage(code, "EN")}").ToArray() : null;
                pr = new PaymentResponseDTO()
                {
                    __IsNew = true,
                    Id = CombFactory.NewComb(),
                    PendingPayment = pendingPayment,
                    DateUtc = utcNow,
                    GatewayAuthorisationCode = r.TransactionStatus?.ProcessingDetails?.AuthorisationCode ?? "",
                    GatewayTransactionId = r.TransactionStatus?.TransactionID.ToString() ?? "",
                    ResponseCode = r.TransactionStatus?.ProcessingDetails?.ResponseCode ?? "",
                    ResponseMessage = r.TransactionStatus?.ProcessingDetails?.ResponseMessage ?? "",
                    RequestedAmount = pendingPayment.DueAmount,
                    TransactedAmount = r.TransactionStatus?.Total / 100M,
                    TransactionStatus = r.TransactionStatus?.Status ?? false,
                    Response = JsonConvert.SerializeObject(r),
                    Errors = errors != null ? string.Join(", ", errors) : null
                };
                pendingPayment.PaymentResponses.Add(pr);
                _pendingPaymentRepo.Save(pendingPayment, true, true);
                log.Info($"Payment response {pr.Id} authcode {pr.GatewayAuthorisationCode} txid {pr.GatewayTransactionId} for ${pr.TransactedAmount} code {pr.ResponseCode}");
            }

            // Can Mark as PaidInFull ??
            var totalPaid = pendingPayment.PaymentResponses.Where(_ => _.TransactionStatus).Sum(_ => _.TransactedAmount);
            if (totalPaid >= pendingPayment.DueAmount)
            {
                pendingPayment.Status = (int)PendingPaymentStatus.Paid;
                pendingPayment.StatusDateUtc = utcNow;
                pendingPayment.PaidInFullDateUtc = utcNow;
            }
            else
            {
                pendingPayment.Status = (int)PendingPaymentStatus.DueNow;
            }
            _pendingPaymentRepo.Save(pendingPayment, false, false);
           

            // Activate any following PendingPayments
            if (r.TransactionStatus.Status.HasValue && r.TransactionStatus.Status.Value)
            {
                if (!pendingPayment.ActivatedDateUtc.HasValue)
                {
                    // Activate all other pending payments against this order ..
                    var pendings = _pendingPaymentRepo.GetByOrderId(pendingPayment.OrderId);
                    foreach (var p in pendings)
                    {
                        if (p.Status == (int)PendingPaymentStatus.Inactive)
                        {
                            p.Status = (int)PendingPaymentStatus.Scheduled;
                            p.StatusDateUtc = utcNow;
                        }
                        p.ActivatedDateUtc = utcNow;
                    }
                    _pendingPaymentRepo.Save(pendings, false, false);

                    // Mark the order as not pending anymore, it is now a live booking
                    var order = _orderRepo.GetById(pendingPayment.OrderId, RepositoryInterfaces.Enums.OrderLoadInstructions.None);
                    if (order != null && order.PendingExpiryDate.HasValue)
                    {
                        order.PendingExpiryDate = null;
                        order.WorkflowState = "paid";
                        _orderRepo.Save(order, false, false);
                    }
                }
            }

            return new QueryTransactionResponse()
            {
                TransactionResponse = r,
                PendingPayments = (from pp in _pendingPaymentRepo.GetByOrderId(pendingPayment.OrderId) select new PendingPaymentFacility.PendingPayment(pp)).ToList()
            };
        }

        public eWAY.Rapid.Models.CreateTransactionResponse CreateTransaction(Guid pendingPaymentId)
        {
            var pendingPayment = _pendingPaymentRepo.GetPendingPaymentById(pendingPaymentId);
            if (pendingPayment == null)
                return null;

            var context = JsonConvert.DeserializeObject<eWayPaymentTransactionContext>(pendingPayment.GatewayContext);
            var gatewayCustomer = _gatewayCustomerRepo.GetById(pendingPayment.UserId, this.GatewayName);
            if (gatewayCustomer != null)
            {
                context.Transaction.Customer.TokenCustomerID = gatewayCustomer.TokenCustomerId;
            }
            else
            {
                context.Transaction.SaveCustomer = true;
            }
            var client = _fnRapidClient(pendingPayment.Sandbox);

            context.Response = client.Create(eWAY.Rapid.Enums.PaymentMethod.ResponsiveShared, context.Transaction);
            pendingPayment.Status = (int)PendingPaymentStatus.Processing;
            pendingPayment.GatewayContext = JsonConvert.SerializeObject(context);
            _pendingPaymentRepo.Save(pendingPayment, false, false);

            return context.Response;
        }


        public object CheckProcessingPayments()
        {
            var processingPayments = _pendingPaymentRepo.FindProcessing(this.GatewayName);
            if (processingPayments.Any())
            {
                foreach (var pp in processingPayments)
                {
                    log.Info($"Checking processing payment {pp.Id}");
                    QueryTransaction(pp.Id, false);
                }
            }
            return new
            {
                Count = processingPayments.Count()
            };
        }


        public class CreateResponse : Node
        {
            public bool UseSandbox { get; set; }
            public string[] Errors { get; set; }
            public string SharedPaymentURL { get; set; }
            public string Transaction { get; set; }
            public string AccessCode { get; set; }

            public override void Decode(IDecoder decoder)
            {
                base.Decode(decoder);

                UseSandbox = decoder.Decode<bool>("UseSandbox");
                Errors = decoder.Decode<string[]>("Errors");
                SharedPaymentURL = decoder.Decode<string>("SharedPaymentUrl");
                Transaction = decoder.Decode<string>("Transaction");
                AccessCode = decoder.Decode<string>("AccessCode");
            }

            public override void Encode(IEncoder encoder)
            {
                base.Encode(encoder);

                encoder.Encode("UseSandbox", UseSandbox);
                encoder.Encode("Errors", Errors);
                encoder.Encode("SharedPaymentUrl", SharedPaymentURL);
                encoder.Encode("Transaction", Transaction);
                encoder.Encode("AccessCode", AccessCode);
            }
        }

    }
}
