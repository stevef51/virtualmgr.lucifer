﻿using ConceptCave.Checklist.Facilities;
using ConceptCave.Repository.PeriodicExecution;
using ConceptCave.RepositoryInterfaces;
using ConceptCave.RepositoryInterfaces.Repositories;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.BusinessLogic.PeriodicExecution
{
    public class eWayPaymentPeriodicExecutionHandler : IPeriodicExecutionHandler
    {
        private IPendingPaymentRepository _pendingPaymentRepo;
        private eWayPaymentFacility _ewayFacility;
        private readonly Func<IPeriodicExecutionHandler> _fnChecklistHandler;

        public eWayPaymentPeriodicExecutionHandler(IPendingPaymentRepository pendingPaymentRepo, eWayPaymentFacility ewayFacility, Func<string, IPeriodicExecutionHandler> fnCustomHandler)
        {
            _pendingPaymentRepo = pendingPaymentRepo;
            _ewayFacility = ewayFacility;
            _fnChecklistHandler = () => fnCustomHandler("ExecutePublishedChecklist");
        }

        public string Name => "eWayPayment";

        public IPeriodicExecutionHandlerResult Execute(JObject parameters, IContinuousProgressCategory progress)
        {
            var result = new PeriodicExecutionHandlerResult();
            result.Success = false;
            var jData = new JObject();
            var jIssues = new JArray();

            var overdue = _pendingPaymentRepo.FindScheduled(_ewayFacility.GatewayName, DateTime.UtcNow, RepositoryInterfaces.Enums.PendingPaymentLoadInstructions.PaymentResponses);
            foreach(var pp in overdue)
            {
                try
                {
                    var alreadyPaid = pp.PaymentResponses.Where(_ => _.TransactionStatus && _.TransactedAmount.HasValue).Sum(_ => _.TransactedAmount.Value);
                    var totalDue = pp.DueAmount - alreadyPaid;
                    if (totalDue > 0)
                    {
                        (var success, var paymentResponseId, var errors) = _ewayFacility.DirectTransaction(pp, totalDue);

                        JObject extra = (JObject)parameters["extra"];
                        if (extra == null)
                        {
                            extra = new JObject();
                            parameters["extra"] = extra;
                        }

                        // ExecutePublishedChecklist expects ReviewerId to be a String
                        parameters["ReviewerId"] = pp.UserId.ToString();
                        extra["_success"] = success;
                        extra["_pendingpaymentid"] = pp.Id;
                        extra["_paymentresponseid"] = paymentResponseId;
                        extra["_errors"] = errors != null ? string.Join(", ", errors) : null;

                        var handler = _fnChecklistHandler();
                        handler.Execute(parameters, progress);

                        if (errors == null)
                        {
                            jData[pp.Id.ToString()] = paymentResponseId;
                            if (success)
                                progress.Complete($"{pp.Id} processed {paymentResponseId}");
                            else
                                progress.Warning($"{pp.Id} failed {paymentResponseId}");
                        }
                        else
                        {
                            jIssues.Add(JObject.FromObject(new
                            {
                                PendingPaymentId = pp.Id,
                                Errors = errors
                            }));
                            progress.Error($"{pp.Id} errors : {string.Join(", ", errors)}");
                        }
                    }
                }
                catch(Exception ex)
                {
                    var errors = new[] { ex.Message };
                    jIssues.Add(JObject.FromObject(new
                    {
                        PendingPaymentId = pp.Id,
                        Errors = errors
                    }));
                    progress.Error($"{pp.Id} errors : {string.Join(", ", errors)}");
                }
            }

            if (jIssues.Any())
            {
                result.Issues = jIssues;
                result.Success = false;
            }
            else
                result.Success = true;
            result.Data = jData;
            return result;
        }
    }
}
