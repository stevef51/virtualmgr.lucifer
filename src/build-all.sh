#!/bin/bash
DIR=$(dirname $0);
$DIR/build-api.sh
$DIR/build-auth.sh
$DIR/build-media.sh
$DIR/build-odata.sh
$DIR/build-tenant.sh
$DIR/build-spa.sh
$DIR/build-grandmaster-api.sh
$DIR/build-grandmaster-spa.sh
