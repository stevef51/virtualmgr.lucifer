﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.Checklist.Reporting.Data.Repositories
{
    public class TaskScheduleRepository : RepositoryBase
    {
        public TaskScheduleRepository() : base() { }

        public TaskScheduleRepository(string nameOrConnectionString) : base(nameOrConnectionString) { }

        public IQueryable<Schedule> Schedules(Guid? ownerId)
        {
            DataModel.TidyHtml = true;
            var result = DataModel.SchedulesEnforceSecurity;

            if (ownerId.HasValue)
            {
                result = result.Where(r => r.OwnerId == ownerId.Value);
            }
            else
            {
                result = result.Where(r => r.OwnerId.HasValue);
            }

            result = result.Where(r => r.IsArchived == false);

            return result;
        }
    }
}
