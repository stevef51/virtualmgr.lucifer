﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.Checklist.Reporting.Data.Repositories
{
    public class CRONJobRepository : RepositoryBase
    {
        public CRONJobRepository() : base() { }

        public CRONJobRepository(string nameOrConnectionString) : base(nameOrConnectionString) { }

        public IQueryable<tblQRTZHistory> BetweenDates(DateTime? startDate, DateTime? endDate, string jobName)
        {
            var result = (IQueryable<tblQRTZHistory>)DataModel.tblQRTZHistories;

            if(startDate.HasValue == true)
            {
                startDate = DataModel.ConvertToUTC(startDate.Value);
                result = result.Where(t => t.RunTime >= startDate.Value);
            }

            if(endDate.HasValue == true)
            {
                endDate = DataModel.ConvertToUTC(endDate.Value);
                result = result.Where(t => t.RunTime <= endDate.Value);
            }

            if(string.IsNullOrEmpty(jobName) == false)
            {
                result = result.Where(t => t.Job == jobName);
            }

            return result;
        }
    }
}
