﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.Checklist.Reporting.Data.Repositories
{
    public class ChecklistRepository : RepositoryBase
    {
        public ChecklistRepository() : base() { }

        public ChecklistRepository(string nameOrConnectionString) : base(nameOrConnectionString) { }

        public IQueryable<Checklist> StartedBetween(DateTime? startDate, DateTime? endDate, bool? ascending)
        {
            var result = (IQueryable<Checklist>)DataModel.ChecklistsEnforceSecurity;

            if (startDate.HasValue == true)
            {
                startDate = DataModel.ConvertToUTC(startDate.Value);
                result = result.Where(c => c.DateStarted >= startDate.Value);
            }

            if (endDate.HasValue == true)
            {
                endDate = DataModel.ConvertToUTC(endDate.Value);
                result = result.Where(c => c.DateStarted <= endDate.Value);
            }

            if (ascending.HasValue == true)
            {
                if (ascending == false)
                {
                    result = result.OrderByDescending(c => c.DateStarted);
                }
                else
                {
                    result = result.OrderBy(c => c.DateStarted);
                }
            }

            return result;
        }

        public IQueryable<Checklist> StartedBetween(DateTime? startDate, DateTime? endDate, string checklist, bool? ascending)
        {
            var result = StartedBetween(startDate, endDate, ascending);

            if (string.IsNullOrEmpty(checklist) == false)
            {
                result = result.Where(c => c.Name == checklist);
            }

            return result;
        }

        public IQueryable<Checklist> StartedBetween(DateTime? startDate, DateTime? endDate, string checklist, Guid? reviewer, bool? ascending)
        {
            var result = StartedBetween(startDate, endDate, checklist, ascending);

            if (reviewer.HasValue == true)
            {
                result = result.Where(c => c.ReviewerId == reviewer);
            }

            return result;
        }

        public IQueryable<Checklist> StartedBetween(DateTime? startDate, DateTime? endDate, string checklist, Guid? reviewer, Guid? reviewee, bool? ascending)
        {
            var result = StartedBetween(startDate, endDate, checklist, reviewer, ascending);

            if (reviewee.HasValue == true)
            {
                result = result.Where(c => c.RevieweeId == reviewee);
            }

            return result;
        }

        public IQueryable<Checklist> CompletedBetween(DateTime? startDate, DateTime? endDate, bool? ascending)
        {
            var result = (IQueryable<Checklist>)DataModel.ChecklistsEnforceSecurity;

            if (startDate.HasValue == true)
            {
                result = result.Where(c => c.DateCompleted >= DataModel.ConvertToUTC(startDate.Value));
            }

            if (endDate.HasValue == true)
            {
                result = result.Where(c => c.DateCompleted <= DataModel.ConvertToUTC(endDate.Value));
            }

            if (ascending.HasValue == true)
            {
                if (ascending == false)
                {
                    result = result.OrderByDescending(c => c.DateCompleted);
                }
                else
                {
                    result = result.OrderBy(c => c.DateCompleted);
                }
            }

            return result;
        }

        public IQueryable<Checklist> CompletedBetween(DateTime? startDate, DateTime? endDate, string checklist, bool? ascending)
        {
            var result = CompletedBetween(startDate, endDate, ascending);

            if (string.IsNullOrEmpty(checklist) == false)
            {
                result = result.Where(c => c.Name == checklist);
            }

            return result;
        }

        public IQueryable<Checklist> CompletedBetween(DateTime? startDate, DateTime? endDate, string checklist, Guid? reviewer, bool? ascending)
        {
            var result = CompletedBetween(startDate, endDate, checklist, ascending);

            if (reviewer.HasValue == true)
            {
                result = result.Where(c => c.ReviewerId == reviewer);
            }

            return result;
        }

        public IQueryable<Checklist> CompletedBetween(DateTime? startDate, DateTime? endDate, string checklist, Guid? reviewer, Guid? reviewee, bool? ascending)
        {
            var result = CompletedBetween(startDate, endDate, checklist, reviewer, ascending);

            if (reviewee.HasValue == true)
            {
                result = result.Where(c => c.RevieweeId == reviewee);
            }

            return result;
        }
    }
}
