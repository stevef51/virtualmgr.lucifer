﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.Checklist.Reporting.Data.Repositories
{
    public class HealthRepository : RepositoryBase
    {
        public bool CheckDatabaseConnection()
        {
            try
            {
                this.DataModel.Database.Connection.Open();
                this.DataModel.Database.Connection.Close();
                return true;
            }
            catch(Exception e)
            {
                return false;
            }
        }
    }
}
