﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualMgr.Central;

namespace ConceptCave.Checklist.Reporting.Data.Repositories
{
    public class ReportTicketRepository 
    {
        public class TicketModel
        {
            public tblReportTicket Ticket { get; set; }
            public Membership RunAsUser { get; set; }
        }

        private TenantDataModel DataModel;

        public ReportTicketRepository()
        {
            DataModel = new TenantDataModel();
        }

        public TicketModel GetById(Guid id)
        {
            var ticket = DataModel.tblReportTickets.Where(r => r.Id == id).FirstOrDefault();
            if (ticket == null)
                return null;

            return new TicketModel() { Ticket = ticket, RunAsUser = DataModel.Memberships.Where(m => m.UserId == ticket.RunAsUserId).FirstOrDefault() };
        }

        public IEnumerable<tblReportTicket> Tickets(int? reportId, Guid? runAsUserId, DateTime? createdFrom, DateTime? createdTo, DateTime? lastRunFrom, DateTime? lastRunTo)
        {
            var result = DataModel.tblReportTickets.AsQueryable();

            if (reportId.HasValue)
            {
                result = result.Where(r => r.ReportId == reportId.Value);
            }

            if (runAsUserId.HasValue)
            {
                result = result.Where(r => r.RunAsUserId == runAsUserId.Value);
            }

            if (createdFrom.HasValue)
            {
                result = result.Where(r => r.DateCreatedUtc >= createdFrom.Value);
            }

            if (createdTo.HasValue)
            {
                result = result.Where(r => r.DateCreatedUtc <= createdTo.Value);
            }

            if (lastRunFrom.HasValue)
            {
                result = result.Where(r => r.LastRunOnUtc >= lastRunFrom.Value);
            }

            if (lastRunTo.HasValue)
            {
                result = result.Where(r => r.LastRunOnUtc <= lastRunTo.Value);
            }

            return result;
        }

        public void Save(TicketModel ticket)
        {
            DataModel.SaveChanges();
        }
    }
}
