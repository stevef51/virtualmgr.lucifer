﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.Checklist.Reporting.Data.Repositories
{
    public class OrderRepository : RepositoryBase
    {
        public OrderRepository() : base() { }

        public OrderRepository(string nameOrConnectionString) : base(nameOrConnectionString) { }

        public IQueryable<OrderItem> StartedBetween(DateTime? startDate, DateTime? endDate, int? facilityId, int? buildingId, int? floorId, int? zoneId, int? siteId)
        {
            IQueryable<OrderItem> result;
            result = (IQueryable<OrderItem>)DataModel.OrderItemsEnforceSecurityAndScope(QueryScope.IncludeCurrentUser);

            if (startDate.HasValue == true)
            {
                startDate = DataModel.ConvertToUTC(startDate.Value);
                result = result.Where(c => c.DateCreated >= startDate.Value);
            }

            if (endDate.HasValue == true)
            {
                endDate = DataModel.ConvertToUTC(endDate.Value);
                result = result.Where(c => c.DateCreated <= endDate.Value);
            }

            if (siteId.HasValue == true && siteId != 0)
            {
                result = result.Where(c => c.FacilityStructureId == siteId.Value);
            }
            else if (zoneId.HasValue == true && zoneId != 0)
            {
                var siteIds = DataModel.FacilityStructures.Where(x => x.ParentId == zoneId).Select(x => x.Id);
                result = result.Where(c => siteIds.Contains(c.FacilityStructureId));
            }
            else if (floorId.HasValue == true && floorId != 0)
            {
                var zoneIds = DataModel.FacilityStructures.Where(x => x.ParentId == floorId).Select(x => x.Id);
                var siteIds = DataModel.FacilityStructures.Where(x => zoneIds.Contains(x.ParentId ?? 0)).Select(x => x.Id);
                result = result.Where(c => siteIds.Contains(c.FacilityStructureId));
            }
            else if (buildingId.HasValue == true && buildingId != 0)
            {
                var floorIds = DataModel.FacilityStructures.Where(x => x.ParentId == buildingId).Select(x => x.Id);
                var zoneIds = DataModel.FacilityStructures.Where(x => floorIds.Contains(x.ParentId ?? 0)).Select(x => x.Id);
                var siteIds = DataModel.FacilityStructures.Where(x => zoneIds.Contains(x.ParentId ?? 0)).Select(x => x.Id);
                result = result.Where(c => siteIds.Contains(c.FacilityStructureId));
            }
            else if (facilityId.HasValue == true && facilityId != 0)
            {
                var buildIds = DataModel.FacilityStructures.Where(x => x.ParentId == facilityId).Select(x => x.Id);
                var floorIds = DataModel.FacilityStructures.Where(x => buildIds.Contains(x.ParentId ?? 0)).Select(x => x.Id);
                var zoneIds = DataModel.FacilityStructures.Where(x => floorIds.Contains(x.ParentId ?? 0)).Select(x => x.Id);
                var siteIds = DataModel.FacilityStructures.Where(x => zoneIds.Contains(x.ParentId ?? 0)).Select(x => x.Id);
                result = result.Where(c => siteIds.Contains(c.FacilityStructureId));
            }

            return result;
        }

        public List<FacilityStructure> GetFacility()
        {
            IQueryable<FacilityStructure> facility;
            facility = (IQueryable<FacilityStructure>)DataModel.FacilityStructures;
            var result = facility.Where(c => c.ParentId == null).ToList();
            result.Insert(0, new FacilityStructure() { Name = "Select", Id = 0 });
            return result;
        }

        public List<FacilityStructure> GetFacility(int? parentId)
        {
            IQueryable<FacilityStructure> facility;
            facility = (IQueryable<FacilityStructure>)DataModel.FacilityStructures;
            List<FacilityStructure> result = new List<FacilityStructure>();
            if (parentId.HasValue == true && parentId != 0)
            {
                result = facility.Where(c => c.ParentId == parentId).ToList();
            }
            result.Insert(0, new FacilityStructure() { Name = "Select", Id = 0 });
            return result;
        }

    }
}
