﻿using ConceptCave.Checklist.Reporting.Data.DTO;
using ConceptCave.Scheduling;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;

namespace ConceptCave.Checklist.Reporting.Data.Repositories
{
    public enum TimesheetGrouping
    {
        None = 0,
        Site = 1,
        Cleaner = 2
    }

    public class TaskRepository : RepositoryBase
    {
        public TaskRepository() : base() 
        {
            DataModel.TaskResultSettings.ExpandStatusText = true;
        }

        public TaskRepository(string nameOrConnectionString) : base(nameOrConnectionString) 
        {
            DataModel.TaskResultSettings.ExpandStatusText = true;
        }

        public IQueryable<Task> CreatedBetweenDates(DateTime? startDate, DateTime? endDate, bool? ascending)
        {
            var result = (IQueryable<Task>)DataModel.TasksEnforceSecurity;

            if (startDate.HasValue == true)
            {
                startDate = DataModel.ConvertToUTC(startDate.Value);
                result = result.Where(t => t.DateCreated >= startDate.Value);
            }

            if (endDate.HasValue == true)
            {
                endDate = DataModel.ConvertToUTC(endDate.Value);
                result = result.Where(t => t.DateCreated <= endDate.Value);
            }

            if(ascending.HasValue == true)
            {
                if(ascending == false)
                {
                    result = result.OrderByDescending(t => t.DateCreated);
                }
                else
                {
                    result = result.OrderBy(t => t.DateCreated);
                }
            }

            return result;
        }

        public IQueryable<Task> CreatedBetweenDates(DateTime? startDate, DateTime? endDate, object owner, object site, bool? ascending)
        {
            var result = CreatedBetweenDates(startDate, endDate, ascending);

            var o = AsGuid(owner);

            if(o.HasValue == true)
            {
                result = result.Where(t => t.OwnerId == o.Value);
            }

            var s = AsGuid(site);

            if(s.HasValue == true)
            {
                result = result.Where(t => t.SiteId == s.Value);
            }

            return result;
        }

        public IQueryable<Task> CreatedBetweenDates(DateTime? startDate, DateTime? endDate, object owner, object site, bool? hasChecklists, bool? ascending)
        {
            var result = CreatedBetweenDates(startDate, endDate, owner, site, ascending);

            if(hasChecklists.HasValue)
            {
                if(hasChecklists.Value == true)
                {
                    result.Where(t => t.WorkingDocumentCount > 0);
                }
                else
                {
                    result.Where(t => t.WorkingDocumentCount == 0);
                }

            }

            return result;
        }

        public IQueryable<Task> CreatedBetweenDates(DateTime? startDate, DateTime? endDate, object owner, object site, string status, object taskType, bool? ascending)
        {
            var result = CreatedBetweenDates(startDate, endDate, owner, site, ascending);

            if(status != null)
            {
                var statuses = status.Split(',');
                result = result.Where(t => statuses.Contains(t.StatusText));
            }

            var tt = AsGuid(taskType);

            if(tt.HasValue == true)
            {
                result = result.Where(t => t.TaskTypeId == tt.Value);
            }

            return result;
        }

        public IQueryable<Task> CompletedBetweenDates(DateTime? startDate, DateTime? endDate, bool? ascending)
        {
            var result = (IQueryable<Task>)DataModel.TasksEnforceSecurity;

            if (startDate.HasValue == true)
            {
                startDate = DataModel.ConvertToUTC(startDate.Value);
                result = result.Where(t => t.DateCompleted >= startDate.Value);
            }

            if (endDate.HasValue == true)
            {
                endDate = DataModel.ConvertToUTC(endDate.Value);
                result = result.Where(t => t.DateCompleted <= endDate.Value);
            }

            if (ascending.HasValue == true)
            {
                if (ascending == false)
                {
                    result = result.OrderByDescending(t => t.DateCompleted);
                }
                else
                {
                    result = result.OrderBy(t => t.DateCompleted);
                }
            }

            return result;
        }

        public IQueryable<Task> CompletedBetweenDates(DateTime? startDate, DateTime? endDate, bool? ascending, bool? excludeNullDateStarted)
        {
            var result = (IQueryable<Task>)DataModel.TasksEnforceSecurity;

            if (startDate.HasValue == true)
            {
                startDate = DataModel.ConvertToUTC(startDate.Value);
                result = result.Where(t => t.DateCompleted >= startDate.Value);
            }

            if (endDate.HasValue == true)
            {
                endDate = DataModel.ConvertToUTC(endDate.Value);
                result = result.Where(t => t.DateCompleted <= endDate.Value);
            }

            if (excludeNullDateStarted.HasValue && excludeNullDateStarted.Value)
            {
                result = result.Where(t => t.DateStarted.HasValue);
            }

            if (ascending.HasValue == true)
            {
                if (ascending == false)
                {
                    result = result.OrderByDescending(t => t.DateCompleted);
                }
                else
                {
                    result = result.OrderBy(t => t.DateCompleted);
                }
            }

            return result;
        }


        public IQueryable<Task> CompletedBetweenDates(DateTime? startDate, DateTime? endDate, object owner, object site, bool? ascending)
        {
            var result = CompletedBetweenDates(startDate, endDate, ascending);

            var o = AsGuid(owner);

            if (o.HasValue == true)
            {
                result = result.Where(t => t.OwnerId == o.Value);
            }

            var s = AsGuid(site);

            if (s.HasValue == true)
            {
                result = result.Where(t => t.SiteId == s.Value);
            }

            return result;
        }

        public IQueryable<Task> CompletedBetweenDates(DateTime? startDate, DateTime? endDate, object owner, object site, bool? hasChecklists, bool? ascending)
        {
            var result = CompletedBetweenDates(startDate, endDate, owner, site, ascending);

            if (hasChecklists.HasValue)
            {
                if (hasChecklists.Value == true)
                {
                    result = result.Where(t => t.WorkingDocumentCount > 0);
                }
                else
                {
                    result = result.Where(t => t.WorkingDocumentCount == 0);
                }

            }

            return result;
        }

        public IQueryable<Task> CompletedBetweenDates(DateTime? startDate, DateTime? endDate, object owner, object site, string status, object taskType, bool? ascending)
        {
            var result = CompletedBetweenDates(startDate, endDate, owner, site, ascending);

            if (string.IsNullOrEmpty(status) == false)
            {
                result = result.Where(t => t.StatusText == status);
            }

            var tt = AsGuid(taskType);

            if (tt.HasValue == true)
            {
                result = result.Where(t => t.TaskTypeId == tt.Value);
            }

            return result;
        }

        public IQueryable<Task> LeaveTasksCreatedBetweeenDates(DateTime? startDate, DateTime? endDate, bool? ascending)
        {
            var result = CreatedBetweenDates(startDate, endDate, ascending);

            result = result.Where(t => t.ScheduledOwnerId != t.OriginalOwnerId);

            return result;
        }

        public IQueryable<TimeBetweenTask> TimeBetweenTasks(DateTime? startDate, DateTime? endDate, Guid? owner)
        {
            var result = DataModel.TimeBetweenTasksEnforceSecurity;

            if (startDate.HasValue)
            {
                startDate = DataModel.ConvertToUTC(startDate.Value);
                result = result.Where(r => r.Date >= startDate.Value);
            }

            if (endDate.HasValue)
            {
                endDate = DataModel.ConvertToUTC(endDate.Value);
                result = result.Where(r => r.Date <= endDate.Value);
            }

            if (owner.HasValue)
            {
                result = result.Where(r => r.UserId == owner.Value);
            }

            result = result.OrderBy(r => r.Name).OrderBy(r => r.UserId).OrderBy(r => r.Date);

            return result;
        }

        public IQueryable<OutstandingTask> OutstandingTasksBySiteOwner(Guid? siteId, DateTime? fromDate, int? minCount)
        {
            if (fromDate.HasValue)
                fromDate = DataModel.ConvertToUTC(fromDate.Value);

            var result = DataModel.OutstandingTasksEnforceSecurity;

            // Note From & To have a slightly different meaning here since each Row has a Min & Max date (the last date range of the outstanding task)
            // in fact FromDate is the only one that makes any real sense
            if (fromDate.HasValue)
                result = result.Where(m => m.MaxStatusDate >= fromDate.Value);

            if (siteId.HasValue)
                result = result.Where(m => m.SiteId == siteId.Value);

            if (minCount.HasValue)
                result = result.Where(m => m.Count >= minCount.Value);

            return result;
        }

        public IQueryable<TimesheetItem> Timesheet(DateTime startDate, DateTime endDate, Guid? ownerId, Guid? siteId, string notesPrompt, string grouping, bool includeLeave)
        {
            return Timesheet(startDate, endDate, ownerId, siteId, notesPrompt, (TimesheetGrouping)Enum.Parse(typeof(TimesheetGrouping), grouping, true), includeLeave, null);
        }

        public IQueryable<TimesheetItem> Timesheet(DateTime startDate, DateTime endDate, Guid? ownerId, Guid? siteId, string notesPrompt, TimesheetGrouping grouping, bool includeLeave)
        {
            return Timesheet(startDate, endDate, ownerId, siteId, notesPrompt, grouping, includeLeave, null);
        }

        protected IList<DateTime> BuildLeaveDates(DateTime startDate, DateTime endDate, Leave leave)
        {
            List<DateTime> result = new List<DateTime>();

            DateTime sd = startDate;
            if(leave.StartDate > sd)
            {
                sd = leave.StartDate;
            }

            DateTime ed = endDate;
            if(leave.EndDate < ed)
            {
                ed = leave.EndDate;
            }

            var date = sd;
            while(date <= ed)
            {
                result.Add(date);

                date = date.AddDays(1);
            }

            return result;
        }

        public IQueryable<TimesheetItem> LeaveTimesheet(DateTime startDate, DateTime endDate, Guid? ownerId, Guid? siteId, TimesheetGrouping grouping, IList<TimesheetItem> existing)
        {
            var overrides = DataModel.LeaveEnforeSecurity;

            /* there are a few options
             *                       startDate ----------------------- endDate
             *           sd ---- ed                                                           no overlap
             *                           sd ---------- ed                                     partial overlap (ed >= startDate && ed <= endDate)
             *                                    sd ---------- ed                            complete overlap (modelled by above and below anyway)
             *                                                   sd --------------- ed        partial overlap (sd >= startDate && sd <= endDate)
             *                                                                  sd ------ ed  no overlap
            */
            overrides = overrides.Where(o => (o.EndDate >= startDate && o.EndDate <= endDate) ||
            (o.StartDate <= startDate && o.EndDate >= endDate) ||
            (o.StartDate >= startDate && o.StartDate <= endDate));

            IList<TimesheetItem> result = new List<TimesheetItem>();

            if(existing != null)
            {
                result = existing;
            }

            var tz = TimeZoneInfo.FindSystemTimeZoneById(DataModel.CurrentUser.TimeZone);

            var enforcer = new ConceptCave.Scheduling.ExclusionRuleEnforcer();

            enforcer.Rules.Add(new ConceptCave.Scheduling.ExclusionRules.ExpiredOrNotStartedExclusionRule());
            enforcer.Rules.Add(new ConceptCave.Scheduling.ExclusionRules.DailyScheduleExclusionRule());
            enforcer.Rules.Add(new ConceptCave.Scheduling.ExclusionRules.CronScheduleExclusionRule());

            List<Task> tasks = new List<Task>();
            // each override models leave for someone. The person could have been replcaced, but may also
            // not have been replaced, in which case no task data is available for them. So we need to work
            // out which schedules were in place for each override for each day of the schedules leave
            foreach (var o in overrides)
            {
                var dates = BuildLeaveDates(startDate, endDate, o);

                var candidates = (from s in DataModel.Schedules where 
                                     s.RosterId == o.RosterId && 
                                     s.RoleId == o.RoleId && 
                                     s.IsArchived == false select s);

                if (ownerId.HasValue)
                    candidates = candidates.Where(t => t.OwnerId == ownerId);

                if (siteId.HasValue)
                    candidates = candidates.Where(t => t.SiteId == siteId);

                var scheduleDays = from s in DataModel.ScheduleDays
                                   where
                                       s.RosterId == o.RosterId &&
                                       s.RoleId == o.RoleId
                                   select s;

                List<IScheduledTask> schedules = new List<IScheduledTask>();
                foreach(var s in candidates)
                {
                    schedules.Add(new ScheduleExclusionRuleHelper(s));
                }

                foreach(var date in dates)
                {
                    // since the enforcer changes the collection handed into it, we take a copy
                    // of the original and use that as it'll get used on each day
                    var copy = schedules.ConvertAll<IScheduledTask>(i => i);

                    enforcer.Enforce(copy, date, tz, false);

                    foreach(var schedule in copy)
                    {
                        var s = (ScheduleExclusionRuleHelper)schedule;

                        if(s.OwnerId.HasValue == false && o.OriginalUserId.HasValue == false)
                        {
                            continue;
                        }

                        Guid oId = o.OriginalUserId.HasValue ? o.OriginalUserId.Value : s.OwnerId.Value;
                        string oName = o.OriginalUserId.HasValue ? o.OriginalUser : s.Entity.Owner;

                        // we are going to create task objects and kinda fake things out so that
                        // we can leverage the initleave code we already have
                        Task t = new Task()
                        {
                            OwnerId = oId,
                            Owner = oName,
                            SiteId = s.SiteId,
                            SiteName = s.Entity.SiteName,
                            DateCreated = date,
                        };

                        var sd = from d in scheduleDays where d.Day == (int)date.DayOfWeek select d;
                        if(sd.Count() > 0)
                        {
                            t.DateCreated = new DateTime(t.DateCreated.Year, t.DateCreated.Month, t.DateCreated.Day, sd.First().StartTime.Hour, sd.First().StartTime.Minute, 0);
                            t.DateCompleted = t.DateCreated.Add(sd.First().EndTime.Subtract(sd.First().StartTime));
                            t.EstimatedDurationSeconds = (decimal)t.DateCompleted.Value.Subtract(t.DateCreated).TotalSeconds;
                        }
                        else
                        {
                            if (t.EstimatedDurationSeconds.HasValue)
                            {
                                t.DateCompleted = t.DateCreated.AddSeconds((double)t.EstimatedDurationSeconds.Value);
                                t.EstimatedDurationSeconds = (decimal)t.DateCompleted.Value.Subtract(t.DateCreated).TotalSeconds;
                            }  
                        }

                        tasks.Add(t);
                    }
                }
            }

            Dictionary<Guid, Company> companyCache = new Dictionary<Guid, Company>();
            (from t in tasks where t.OwnerCompanyId.HasValue == true select t.OwnerCompanyId.Value).Distinct().ToList().ForEach(t => companyCache.Add(t, (from c in DataModel.Companies where c.Id == t select c).First()));

            if (grouping == TimesheetGrouping.Site)
            {
                foreach (var bySite in (from t in tasks group t by t.SiteId into g select new { SiteId = g.Key, tasks = g.ToList() }))
                {
                    // by cleaner/owner
                    foreach (var byOwner in (from t in bySite.tasks group t by t.OwnerId into g select new { OwnerId = g.Key, tasks = g.ToList() }))
                    {
                        // now finally by date
                        foreach (var byDate in (from t in byOwner.tasks group t by t.DateCreated.Date into g select new { Date = g.Key, tasks = g.ToList() }))
                        {
                            TimesheetItem item = new TimesheetItem();

                            if (byDate.tasks.Count() > 0)
                            {
                                item.InitForLeave(byDate.tasks.AsQueryable<Task>(), companyCache);

                                result.Add(item);
                            }
                        }
                    }
                }

                return result.OrderBy(r => r.SiteName).ThenBy(r => r.Date).ThenBy(r => r.OwnerName).AsQueryable<TimesheetItem>();
            }
            else if(grouping == TimesheetGrouping.Cleaner)
            {
                foreach (var byOwner in (from t in tasks group t by t.OwnerId into g select new { OwnerId = g.Key, tasks = g.ToList() }))
                {
                    // now finally by date
                    foreach (var byDate in (from t in byOwner.tasks group t by t.DateCreated.Date into g select new { DateCreated = g.Key, tasks = g.ToList() }))
                    {
                        TimesheetItem item = new TimesheetItem();

                        if (byDate.tasks.Count() > 0)
                        {
                            item.InitForLeave(byDate.tasks.AsQueryable<Task>(), companyCache);

                            result.Add(item);
                        }
                    }
                }

                return result.OrderBy(r => r.OwnerName).ThenBy(r => r.Date).AsQueryable<TimesheetItem>();
            }
            else
            {
                foreach(var task in tasks)
                {
                    TimesheetItem item = new TimesheetItem();

                    List<Task> ts = new List<Task>();
                    ts.Add(task);

                    item.InitForLeave(ts.AsQueryable<Task>(), companyCache);

                    result.Add(item);
                }

                return result.OrderBy(r => r.Date).AsQueryable<TimesheetItem>();
            }
        }

        public IQueryable<TimesheetItem> Timesheet(DateTime startDate, DateTime endDate, Guid? ownerId, Guid? siteId, string notesPrompt, string grouping, bool includeLeave, IQueryable<TaskWorklog> tasks = null)
        {
            return Timesheet(startDate, endDate, ownerId, siteId, notesPrompt, (TimesheetGrouping)Enum.Parse(typeof(TimesheetGrouping), grouping, true), includeLeave, tasks);
        }

        public IQueryable<TimesheetItem> Timesheet(DateTime startDate, DateTime endDate, Guid? ownerId, Guid? siteId, string notesPrompt, TimesheetGrouping grouping, bool includeLeave, IQueryable<TaskWorklog> tasks = null)
        {
            IQueryable<TaskWorklog> worklogs = tasks;

            if(worklogs == null)
            {
                worklogs = DataModel.TaskWorklogsEnforceSecurity;
            }

            worklogs.Include(t => t.Task);

            worklogs = worklogs.Where(t => t.DateCompleted != null && 
                t.DateCompleted.Value >= startDate && 
                t.DateCompleted.Value < endDate);

            if (ownerId.HasValue)
                worklogs = worklogs.Where(t => t.UserId == ownerId);

            if (siteId.HasValue)
                worklogs = worklogs.Where(t => t.SiteId == siteId);

            IQueryable<ChecklistResult> checklists = null;

            if(string.IsNullOrEmpty(notesPrompt) == false)
            {
                checklists = DataModel.ChecklistResultsEnforceSecurity;

                var tids = from w in worklogs select w.TaskId;

                checklists = checklists.Where(c => c.TaskId.HasValue && tids.Contains(c.TaskId.Value));

                checklists = checklists.Where(c => c.Prompt == notesPrompt);
            }

            IList<TimesheetItem> result = ConvertToTimesheetItems(worklogs, checklists, grouping, false);

            if(includeLeave == true)
            {
                result = LeaveTimesheet(startDate, endDate, ownerId, siteId, grouping, result).ToList<TimesheetItem>();
            }

            if(grouping == TimesheetGrouping.Site)
            {
                return result.OrderBy(r => r.SiteName).ThenBy(r => r.Date).ThenBy(r => r.OwnerName).AsQueryable<TimesheetItem>();
            }
            else if(grouping == TimesheetGrouping.Cleaner)
            {
                return result.OrderBy(r => r.OwnerName).ThenBy(r => r.Date).AsQueryable<TimesheetItem>();
            }
            else
            {
                return result.OrderBy(r => r.Date).AsQueryable<TimesheetItem>();
            }
        }

        protected IList<TimesheetItem> ConvertToTimesheetItems(IQueryable<TaskWorklog> worklogs, IQueryable<ChecklistResult> checklists, TimesheetGrouping grouping, bool isLeave)
        {
            List<TimesheetItem> result = new List<TimesheetItem>();

            if(worklogs.Count() == 0)
            {
                return result;
            }

            Dictionary<Guid, Company> companyCache = new Dictionary<Guid, Company>();
            (from t in worklogs where t.OwnerCompanyId.HasValue == true select t.OwnerCompanyId.Value).Distinct().ToList().ForEach(t => companyCache.Add(t, (from c in DataModel.Companies where c.Id == t select c).First()));

            if (grouping == TimesheetGrouping.Site)
            {
                // by site first
                foreach (var bySite in (from w in worklogs group w by w.Task.SiteId into g select new { SiteId = g.Key, logs = g.ToList() }))
                {
                    // by cleaner/owner
                    foreach (var byOwner in (from w in bySite.logs group w by w.Task.OwnerId into g select new { OwnerId = g.Key, logs = g.ToList() }))
                    {
                        // now finally by date
                        foreach (var byDate in (from w in byOwner.logs group w by w.Task.DateCreated.Date into g select new { Date = g.Key, logs = g.ToList() }))
                        {
                            IQueryable<ChecklistResult> filteredChecklists = null;
                            if (checklists != null)
                            {
                                filteredChecklists = (from c in checklists
                                                      where c.ReviewerId == byOwner.OwnerId &&
                                                          DbFunctions.TruncateTime(c.ChecklistDateStarted) == byDate.Date
                                                      select c);
                            }

                            TimesheetItem item = new TimesheetItem() { IsLeave = isLeave };

                            if (byDate.logs.Count() > 0)
                            {
                                item.Init(byDate.logs.AsQueryable<TaskWorklog>(), filteredChecklists, companyCache);

                                result.Add(item);
                            }
                        }
                    }
                }
            }
            else if(grouping == TimesheetGrouping.Cleaner)
            {
                foreach (var byOwner in (from w in worklogs group w by w.UserId into g select new { OwnerId = g.Key, logs = g.ToList() }))
                {
                    // now finally by date
                    foreach (var byDate in (from w in byOwner.logs group w by w.Task.DateCreated.Date into g select new { DateCreated = g.Key, logs = g.ToList() }))
                    {
                        IQueryable<ChecklistResult> filteredChecklists = null;

                        if (checklists != null)
                        {
                            filteredChecklists = (from c in checklists
                                                  where c.ReviewerId == byOwner.OwnerId &&
                                                      DbFunctions.TruncateTime(c.ChecklistDateStarted) == byDate.DateCreated
                                                  select c);
                        }

                        TimesheetItem item = new TimesheetItem() { IsLeave = isLeave };

                        if (byDate.logs.Count() > 0)
                        {
                            item.Init(byDate.logs.AsQueryable<TaskWorklog>(), filteredChecklists, companyCache);

                            result.Add(item);
                        }
                    }
                }
            }
            else
            {
                foreach (var log in (from w in worklogs select w))
                {
                    IQueryable<ChecklistResult> filteredChecklists = null;

                    if (checklists != null)
                    {
                        filteredChecklists = (from c in checklists
                                              where c.TaskId == log.TaskId
                                              select c);
                    }

                    TimesheetItem item = new TimesheetItem() { IsLeave = isLeave };

                    List<TaskWorklog> logs = new List<TaskWorklog>();
                    logs.Add(log);

                    item.Init(logs.AsQueryable<TaskWorklog>(), filteredChecklists, companyCache);

                    result.Add(item);
                }
            }

            return result;
        }
    }
}
