﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.Checklist.Reporting.Data.Repositories
{
    public class ChecklistResultRepository : RepositoryBase
    {
        public ChecklistResultRepository() : base() { }

        public ChecklistResultRepository(string nameOrConnectionString) : base(nameOrConnectionString) { }

        public IQueryable<ChecklistResult> StartedBetween(DateTime? startDate, DateTime? endDate, bool? ascending)
        {
            var result = (IQueryable<ChecklistResult>)DataModel.ChecklistResultsEnforceSecurity;

            if(startDate.HasValue == true)
            {
                startDate = DataModel.ConvertToUTC(startDate.Value);
                result = result.Where(c => c.ChecklistDateStarted >= startDate.Value);
            }

            if(endDate.HasValue == true)
            {
                endDate = DataModel.ConvertToUTC(endDate.Value);
                result = result.Where(c => c.ChecklistDateStarted <= endDate.Value);
            }

            if(ascending.HasValue == true)
            {
                if(ascending == false)
                {
                    result = result.OrderByDescending(c => c.ChecklistDateStarted);
                }
                else
                {
                    result = result.OrderBy(c => c.ChecklistDateStarted);
                }
            }

            return result;
        }

        public IQueryable<ChecklistResult> StartedBetween(DateTime? startDate, DateTime? endDate, string checklist, bool? ascending)
        {
            var result = StartedBetween(startDate, endDate, ascending);

            if(string.IsNullOrEmpty(checklist) == false)
            {
                result = result.Where(c => c.Checklist == checklist);
            }

            return result;
        }

        public IQueryable<ChecklistResult> StartedBetween(DateTime? startDate, DateTime? endDate, string checklist, string prompt, bool? ascending)
        {
            var result = StartedBetween(startDate, endDate, checklist, ascending);

            if (string.IsNullOrEmpty(prompt) == false)
            {
                result = result.Where(c => c.Prompt == prompt);
            }

            return result;
        }

        public IQueryable<ChecklistResult> StartedBetween(DateTime? startDate, DateTime? endDate, string checklist, string prompt, string questionType, bool? ascending)
        {
            var result = StartedBetween(startDate, endDate, checklist, prompt, ascending);

            if (string.IsNullOrEmpty(questionType) == false)
            {
                result = result.Where(c => c.QuestionType == questionType);
            }

            return result;
        }

        public IQueryable<ChecklistResult> StartedBetween(DateTime? startDate, DateTime? endDate, string checklist, string prompt, string questionType, Guid? reviewer, bool? ascending)
        {
            var result = StartedBetween(startDate, endDate, checklist, prompt, questionType, ascending);

            if (reviewer.HasValue == true)
            {
                result = result.Where(c => c.ReviewerId == reviewer);
            }

            return result;
        }

        public IQueryable<ChecklistResult> StartedBetween(DateTime? startDate, DateTime? endDate, string checklist, string prompt, string questionType, Guid? reviewer, Guid? reviewee, bool? ascending)
        {
            var result = StartedBetween(startDate, endDate, checklist, prompt, questionType, reviewer, ascending);

            if (reviewee.HasValue == true)
            {
                result = result.Where(c => c.RevieweeId == reviewee);
            }

            return result;
        }

        public IQueryable<ChecklistResult> StartedBetween(DateTime? startDate, DateTime? endDate, string checklist, string prompt, string questionType, Guid? reviewer, Guid? reviewee, Guid? task, bool? ascending)
        {
            var result = StartedBetween(startDate, endDate, checklist, prompt, questionType, reviewer, reviewee, ascending);

            if (task.HasValue == true)
            {
                result = result.Where(c => c.TaskId == task);
            }

            return result;
        }

        public IQueryable<ChecklistResult> CompletedBetween(DateTime? startDate, DateTime? endDate, bool? ascending, bool? includeCurrentUser)
        {
            IQueryable<ChecklistResult> result;

            if (includeCurrentUser.HasValue && includeCurrentUser.Value == true)
            {
                result = (IQueryable<ChecklistResult>)DataModel.ChecklistResultsEnforceSecurityAndScope(QueryScope.IncludeCurrentUser);
            }
            else
            {
                result = (IQueryable<ChecklistResult>)DataModel.ChecklistResultsEnforceSecurity;
            }

            if (startDate.HasValue == true)
            {
                startDate = DataModel.ConvertToUTC(startDate.Value);
                result = result.Where(c => c.ChecklistDateCompleted >= startDate.Value);
            }

            if (endDate.HasValue == true)
            {
                endDate = DataModel.ConvertToUTC(endDate.Value);
                result = result.Where(c => c.ChecklistDateCompleted <= endDate.Value);
            }

            if (ascending.HasValue == true)
            {
                if (ascending == false)
                {
                    result = result.OrderByDescending(c => c.ChecklistDateCompleted);
                }
                else
                {
                    result = result.OrderBy(c => c.ChecklistDateCompleted);
                }
            }

            return result;
        }

        public IQueryable<ChecklistResult> CompletedBetween(DateTime? startDate, DateTime? endDate, bool? ascending)
        {
            return CompletedBetween(startDate, endDate, ascending, true);
        }

        public IQueryable<ChecklistResult> CompletedBetween(DateTime? startDate, DateTime? endDate, string checklist, bool? ascending)
        {
            var result = CompletedBetween(startDate, endDate, ascending);

            if (string.IsNullOrEmpty(checklist) == false)
            {
                result = result.Where(c => c.Checklist == checklist);
            }

            return result;
        }

        public IQueryable<ChecklistResult> CompletedBetween(DateTime? startDate, DateTime? endDate, string checklist, string prompt, bool? ascending)
        {
            var result = CompletedBetween(startDate, endDate, checklist, ascending);

            if (string.IsNullOrEmpty(prompt) == false)
            {
                result = result.Where(c => c.Prompt == prompt);
            }

            return result;
        }

        public IQueryable<ChecklistResult> CompletedBetween(DateTime? startDate, DateTime? endDate, string checklist, string prompt, string questionType, bool? ascending)
        {
            var result = CompletedBetween(startDate, endDate, checklist, prompt, ascending);

            if (string.IsNullOrEmpty(questionType) == false)
            {
                result = result.Where(c => c.QuestionType == questionType);
            }

            return result;
        }

        public IQueryable<ChecklistResult> CompletedBetween(DateTime? startDate, DateTime? endDate, string checklist, string prompt, string questionType, Guid? reviewer, bool? ascending)
        {
            var result = CompletedBetween(startDate, endDate, checklist, prompt, questionType, ascending);

            if (reviewer.HasValue == true)
            {
                result = result.Where(c => c.ReviewerId == reviewer);
            }

            return result;
        }

        public IQueryable<ChecklistResult> CompletedBetween(DateTime? startDate, DateTime? endDate, string checklist, string prompt, string questionType, Guid? reviewer, Guid? reviewee, bool? ascending)
        {
            var result = CompletedBetween(startDate, endDate, checklist, prompt, questionType, reviewer, ascending);

            if (reviewee.HasValue == true)
            {
                result = result.Where(c => c.RevieweeId == reviewee);
            }

            return result;
        }

        public IQueryable<ChecklistResult> CompletedBetween(DateTime? startDate, DateTime? endDate, string checklist, string prompt, string questionType, Guid? reviewer, Guid? reviewee, Guid? task, bool? ascending)
        {
            var result = CompletedBetween(startDate, endDate, checklist, prompt, questionType, reviewer, reviewee, ascending);

            if (task.HasValue == true)
            {
                result = result.Where(c => c.TaskId == task);
            }

            return result;
        }

        public IQueryable<ChecklistResult> CompletedBetween(DateTime? startDate, DateTime? endDate, string checklist, string prompt, string questionType, Guid? reviewer, Guid? reviewee, Guid? task, bool? ascending, string[] includeQuestionLabels, string[] excludeQuestionLabels)
        {
            var result = CompletedBetween(startDate, endDate, checklist, prompt, questionType, reviewer, reviewee, task, ascending);

            if (includeQuestionLabels != null)
            {
                result = result.Where(c => c.ChecklistResultLabels.Any(crl => includeQuestionLabels.Contains(crl.Name)));
            }
            if (excludeQuestionLabels != null)
            {
                result = result.Where(c => c.ChecklistResultLabels.All(crl => !excludeQuestionLabels.Contains(crl.Name)));
            }

            return result;
        }

        public IQueryable<string> GetQuestionsByKitchen(string siteName)
        {
            if (!string.IsNullOrEmpty(siteName))
            {
                IQueryable<ChecklistResult> result = (IQueryable<ChecklistResult>)DataModel.ChecklistResultsEnforceSecurityAndScope(QueryScope.IncludeCurrentUser); 
                result = result.Where(x => x.ReviewerName == siteName);
                return result.Select(x => x.Prompt).Distinct();
            }
            else
            {
                return new List<string>().AsQueryable();
            }
        }

    }
}
