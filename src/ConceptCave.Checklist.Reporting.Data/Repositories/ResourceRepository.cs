﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.Checklist.Reporting.Data.Repositories
{
    public class ResourceRepository : RepositoryBase
    {
        public ResourceRepository()
        {
        }

        public ResourceRepository(string connectionString) : base(connectionString)
        {

        }

        public IQueryable<Resource> Resources(string likeName, string type, object labels)
        {
            var result = DataModel.Resources.AsQueryable<Resource>();

            if (likeName != null)
            {
                result = result.Where(r => r.Name.Contains(likeName));
            }

            if (type != null)
            {
                result = result.Where(r => r.Type == type);
            }

            if (labels != null)
            {
                string[] labelNames = null;
                if (labels is string)
                    labelNames = new string[] { (string)labels };
                else if (labels is string[])
                    labelNames = (string[])labels;

                result = from r in result from rl in r.ResourceLabels where labelNames.Contains(rl.Name) select r;
            }

            return result;
        }
    }
}
