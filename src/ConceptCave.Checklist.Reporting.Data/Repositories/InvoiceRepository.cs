﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.Checklist.Reporting.Data.Repositories
{
    public class InvoiceRepository : RepositoryBase
    {
        public InvoiceRepository() : base() { }

        public InvoiceRepository(string nameOrConnectionString) : base(nameOrConnectionString) { }

        public IQueryable<tblInvoice> Invoices(DateTime? from, DateTime? to)
        {
            IQueryable<tblInvoice> query = DataModel.tblInvoices;
            if (from.HasValue)
            {
                from = DataModel.ConvertToUTC(from.Value);
                query = query.Where(i => i.FromDateUtc >= from.Value);
            }
            if (to.HasValue)
            {
                to = DataModel.ConvertToUTC(to.Value);
                query = query.Where(i => i.ToDateUtc <= to.Value);
            }

            return query.OrderBy(i => i.FromDateUtc);
        }

        public IQueryable<tblInvoice> Invoice(string id)
        {
            IQueryable<tblInvoice> query = DataModel.tblInvoices;
            query = query.Where(i => i.Id == id);
            return query;
        }

        public IQueryable<tblInvoiceLineItem> InvoiceLineItems(string id)
        {
            IQueryable<tblInvoiceLineItem> query = DataModel.tblInvoiceLineItems;
            query = query.Where(li => li.InvoiceId == id);
            return query.OrderBy(li => li.Id);
        }
    }
}
