﻿using ConceptCave.Checklist.Reporting.Data.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.Checklist.Reporting.Data.Repositories
{
    public class BagRepository : RepositoryBase
    {
        protected TaskRepository _taskRepo;
        protected ChecklistResultRepository _checklistResultRepo;

        public BagRepository() : base() 
        {
            _taskRepo = new TaskRepository();
            _checklistResultRepo = new ChecklistResultRepository();
        }

        public BagRepository(string nameOrConnectionString)
            : base(nameOrConnectionString) 
        {
            _taskRepo = new TaskRepository(nameOrConnectionString);
            _checklistResultRepo = new ChecklistResultRepository(nameOrConnectionString);
        }

        protected IQueryable<TaskBag> ToBag(IQueryable<Task> tasks)
        {
            List<TaskBag> bags = new List<TaskBag>();

            foreach (var t in tasks)
            {
                bags.Add(new TaskBag(this, t));
            }

            return bags.AsQueryable<TaskBag>();
        }
        protected IQueryable<ChecklistResultBag> ToBag(IQueryable<ChecklistResult> list)
        {
            List<ChecklistResultBag> bags = new List<ChecklistResultBag>();
            foreach(var l in list)
            {
                bags.Add(new ChecklistResultBag(this, l));
            }
            return bags.AsQueryable<ChecklistResultBag>();
        }

        public IQueryable<TaskBag> CreatedBetweenDates(DateTime? startDate, DateTime? endDate, bool? ascending)
        {
            var tasks = _taskRepo.CreatedBetweenDates(startDate, endDate, ascending);

            return ToBag(tasks);
        }

        public IQueryable<TaskBag> CreatedBetweenDates(DateTime? startDate, DateTime? endDate, object owner, object site, bool? ascending)
        {
            var tasks = _taskRepo.CreatedBetweenDates(startDate, endDate, owner, site, ascending);

            return ToBag(tasks);
        }

        public IQueryable<TaskBag> CreatedBetweenDates(DateTime? startDate, DateTime? endDate, object owner, object site, bool? hasChecklists, bool? ascending)
        {
            var tasks = _taskRepo.CreatedBetweenDates(startDate, endDate, owner, site, hasChecklists, ascending);

            return ToBag(tasks);
        }


        public IQueryable<TaskBag> CreatedBetweenDates(DateTime? startDate, DateTime? endDate, object owner, object site, string status, object taskType, bool? ascending)
        {
            var tasks = _taskRepo.CreatedBetweenDates(startDate, endDate, owner, site, status, taskType, ascending);

            return ToBag(tasks);
        }

        public IQueryable<TaskBag> CompletedBetweenDates(DateTime? startDate, DateTime? endDate, bool? ascending)
        {
            var tasks = _taskRepo.CompletedBetweenDates(startDate, endDate, ascending);

            return ToBag(tasks);
        }

        public IQueryable<TaskBag> CompletedBetweenDates(DateTime? startDate, DateTime? endDate, object owner, object site, bool? ascending)
        {
            var tasks = _taskRepo.CompletedBetweenDates(startDate, endDate, owner, site, ascending);

            return ToBag(tasks);
        }

        public IQueryable<TaskBag> CompletedBetweenDates(DateTime? startDate, DateTime? endDate, object owner, object site, bool? hasChecklists, bool? ascending)
        {
            var tasks = _taskRepo.CompletedBetweenDates(startDate, endDate, owner, site, hasChecklists, ascending);

            return ToBag(tasks);
        }

        public IQueryable<TaskBag> CompletedBetweenDates(DateTime? startDate, DateTime? endDate, object owner, object site, string status, object taskType, bool? ascending)
        {
            var tasks = _taskRepo.CompletedBetweenDates(startDate, endDate, owner, site, status, taskType, ascending);

            return ToBag(tasks);
        }

        public IQueryable<ChecklistResult> ChecklistResultsFortask(Guid taskId)
        {
            var result = from c in DataModel.ChecklistResults where c.TaskId == taskId select c;

            return result;
        }

        public IQueryable<tblMediaData> MediaForTask(Guid taskId)
        {
            var result = from m in DataModel.tblMediaDatas where 
                             (from c in DataModel.ChecklistMediaStreams where c.TaskId == taskId select c.MediaId).Contains(m.MediaId) 
                         select m;

            return result;
        }

        public IQueryable<tblMediaData> MediaForUploadMediaValue(string value)
        {
            var ids = value.Split(',').Select(v => Guid.Parse(v)).ToList();

            var result = from m in DataModel.tblMediaDatas where ids.Contains(m.MediaId) select m;

            return result;
        }

        public IQueryable<ChecklistResultBag> ChecklistResultCompletedBetween(DateTime? startDate, DateTime? endDate, string checklist, string prompt, string questionType, Guid? reviewer, Guid? reviewee, Guid? task, bool? ascending)
        {
            var result = _checklistResultRepo.CompletedBetween(startDate, endDate, checklist, prompt, questionType, reviewer, reviewee, ascending);

            return ToBag(result);
        }

        public IQueryable<ChecklistResultBag> ChecklistResultCompletedBetween(DateTime? startDate, DateTime? endDate, string checklist, string prompt, string questionType, Guid? reviewer, Guid? reviewee, Guid? task, bool? ascending, string[] includeQuestionLabels, string[] excludeQuestionLabels)
        {
            var result = _checklistResultRepo.CompletedBetween(startDate, endDate, checklist, prompt, questionType, reviewer, reviewee, null, ascending, includeQuestionLabels, excludeQuestionLabels);

            return ToBag(result);
        }

        public IQueryable<TaskFinishStatus> TaskFinishStatusForTask(Guid taskId)
        {
            var result = from s in DataModel.TaskFinishStatus1 where s.Id == taskId select s;

            return result;
        }
    }
}
