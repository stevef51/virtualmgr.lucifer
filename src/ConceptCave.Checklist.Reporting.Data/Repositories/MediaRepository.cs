﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.Checklist.Reporting.Data.Repositories
{
    public class MediaRepository : RepositoryBase
    {
        protected Guid? MembershipNoMediaImageId { get; set; }
        protected bool AttemptedNoMediaImageLoad { get; set; }

        protected byte[] MembershipNoMediaImage { get; set; }

        public MediaRepository() : base() 
        {
        }

        public MediaRepository(string nameOrConnectionString)
            : base(nameOrConnectionString) 
        {
        }

        public Guid? NoMembershipMediaId
        {
            get
            {
                if(AttemptedNoMediaImageLoad == true)
                {
                    return MembershipNoMediaImageId;
                }

                var setting = from s in DataModel.tblGlobalSettings where s.Name == "MembershipNoMediaImageId                          " select s.Value;
                AttemptedNoMediaImageLoad = true;

                if (setting.Count() != 0)
                {
                    MembershipNoMediaImageId = Guid.Parse(setting.First());
                }

                return MembershipNoMediaImageId;
            }
        }

        public byte[] NoMembershipMediaImage
        {
            get
            {
                if(NoMembershipMediaId.HasValue == false)
                {
                    return null;
                }

                if(MembershipNoMediaImage != null)
                {
                    return MembershipNoMediaImage;
                }

                var result = from m in DataModel.tblMediaDatas where m.MediaId == NoMembershipMediaId.Value select m.Data;

                if(result.Count() == 0)
                {
                    MembershipNoMediaImage = new byte[] {};
                }
                else
                {
                    MembershipNoMediaImage = result.First();
                }

                return MembershipNoMediaImage;
            }
        }

        public byte[] ForUser(object user)
        {
            if(user == null)
            {
                return null;
            }

            var u = AsGuid(user);

            var result = from m in DataModel.tblMediaDatas where (from p in DataModel.MembershipsEnforceSecurity where p.UserId == u select p.MediaId).Contains(m.MediaId) select m.Data;

            if(result.Count() == 0)
            {
                if(NoMembershipMediaId.HasValue == true)
                {
                    return NoMembershipMediaImage;
                }
                else
                {
                    return null;
                }
            }

            return result.First();
        }
    }
}
