﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.Checklist.Reporting.Data.Repositories
{
    public class RepositoryBase : DbContext
    {
        protected SecurityAndTimezoneAwareDataModel DataModel { get; set; }

        public RepositoryBase()
        {            
            DataModel = new SecurityAndTimezoneAwareDataModel();
        }

        public RepositoryBase(string nameOrConnectionString)
        {
            DataModel = new SecurityAndTimezoneAwareDataModel(nameOrConnectionString);
        }

        protected virtual Guid? AsGuid(object id)
        {
            if (id is Guid)
            {
                return (Guid)id;
            }
            else if (id is string)
            {
                return Guid.Parse((string)id);
            }

            return null;
        }
    }
}
