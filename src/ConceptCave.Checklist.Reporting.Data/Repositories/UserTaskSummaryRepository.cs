﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.Checklist.Reporting.Data.Repositories
{
    public class UserTaskSummaryRepository : RepositoryBase
    {
        public UserTaskSummaryRepository() : base() { }

        public UserTaskSummaryRepository(string nameOrConnectionString) : base(nameOrConnectionString) { }

        public IQueryable<UserTaskSummary_Result> BetweenDates(DateTime startDate, DateTime endDate)
        {
            startDate = DataModel.ConvertToUTC(startDate);
            endDate = DataModel.ConvertToUTC(endDate);

            IQueryable<UserTaskSummary_Result> result = DataModel.UserTaskSummaryEnforceSecurity(startDate, endDate);

            return result;
        }

        public IQueryable<SupervisorSummary_Result> SupervisorBetweenDates(DateTime startDate, DateTime endDate)
        {
            startDate = DataModel.ConvertToUTC(startDate);
            endDate = DataModel.ConvertToUTC(endDate);

            IQueryable<SupervisorSummary_Result> result = DataModel.SupervisorSummary(startDate, endDate).ToList().AsQueryable<SupervisorSummary_Result>();
            return result;
        }
    }
}
