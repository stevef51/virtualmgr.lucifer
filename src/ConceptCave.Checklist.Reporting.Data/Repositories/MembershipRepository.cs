﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace ConceptCave.Checklist.Reporting.Data.Repositories
{
    public class MembershipRepository : RepositoryBase
    {
        public MembershipRepository() : base() { }

        public MembershipRepository(string nameOrConnectionString) : base(nameOrConnectionString) { }

        public IQueryable<Membership> ById(Guid? id)
        {
            var result = (IQueryable<Membership>)DataModel.MembershipsEnforceSecurity;

            if (id.HasValue == true)
            {
                result = result.Where(m => m.UserId == id);
            }

            return result;
        }

        public IQueryable<Membership> ByUsername(string username, bool? ascending)
        {
            var result = (IQueryable<Membership>)DataModel.MembershipsEnforceSecurity;

            if(string.IsNullOrEmpty(username) == false)
            {
                result = result.Where(m => m.UserName == username);
            }

            if(ascending.HasValue == true)
            {
                if(ascending == false)
                {
                    result = result.OrderByDescending(m => m.UserName);
                }
            }

            return result;
        }

        public IQueryable<Membership> LikeUsername(string username, bool? ascending)
        {
            var result = (IQueryable<Membership>)DataModel.MembershipsEnforceSecurity;

            if(string.IsNullOrEmpty(username) == false)
            {
                result = result.Where(m => m.UserName.Contains(username));
            }

            if (ascending.HasValue == true)
            {
                if (ascending == false)
                {
                    result = result.OrderByDescending(m => m.UserName);
                }
            }

            return result;
        }

        public IQueryable<Membership> ByName(string name, bool? ascending)
        {
            var result = (IQueryable<Membership>)DataModel.MembershipsEnforceSecurity;

            if (string.IsNullOrEmpty(name) == false)
            {
                result = result.Where(m => m.Name == name);
            }

            if (ascending.HasValue == true)
            {
                if (ascending == false)
                {
                    result = result.OrderByDescending(m => m.Name);
                }
            }

            return result;
        }

        public IQueryable<Membership> LikeName(string name, bool? ascending)
        {
            var result = (IQueryable<Membership>)DataModel.MembershipsEnforceSecurity;

            if (string.IsNullOrEmpty(name) == false)
            {
                result = result.Where(m => m.Name.Contains(name));
            }

            if (ascending.HasValue == true)
            {
                if (ascending == false)
                {
                    result = result.OrderByDescending(m => m.Name);
                }
            }

            return result;
        }

        public IQueryable<Membership> Sites(bool? ascendingName)
        {
            var result = (IQueryable<Membership>)DataModel.MembershipsEnforceSecurity;

            result = result.Where(m => m.IsASite == true);

            if (ascendingName.HasValue == true)
            {
                if (ascendingName == false)
                {
                    result = result.OrderByDescending(m => m.Name);
                }
                else
                {
                    result = result.OrderBy(m => m.Name);
                }
            }

            return result;
        }

        public IQueryable<Membership> Members(bool? isASite, string likeName, string userType)
        {
            var result = (IQueryable<Membership>)DataModel.MembershipsEnforceSecurity;

            result = result.Include(m => m.MembershipLabels);

            if (isASite.HasValue)
            {
                result = result.Where(m => m.IsASite == isASite.Value);
            }

            if (!string.IsNullOrEmpty(likeName))
            {
                result = result.Where(m => m.Name.Contains(likeName));
            }

            if (!string.IsNullOrEmpty(userType))
            {
                result = result.Where(m => m.UserType == userType);
            }

            return result;
        }


        public IQueryable<Membership> NotSites(bool? ascendingName)
        {
            var result = (IQueryable<Membership>)DataModel.MembershipsEnforceSecurity;

            result = result.Where(m => m.IsASite == false);

            if (ascendingName.HasValue == true)
            {
                if (ascendingName == false)
                {
                    result = result.OrderByDescending(m => m.Name);
                }
                else
                {
                    result = result.OrderBy(m => m.Name);
                }
            }

            return result;
        }


        public IQueryable<Membership> ByUserType(string userType, bool? ascending, bool? ascendingUsername, bool? ascendingName)
        {
            return ByUserType(string.IsNullOrEmpty(userType) ? null : new string[] { userType }, ascending, ascendingUsername, ascendingName);
        }

        public IQueryable<Membership> ByUserType(string[] userTypes, bool? ascending, bool? ascendingUsername, bool? ascendingName)
        {
            var result = (IQueryable<Membership>)DataModel.MembershipsEnforceSecurity;

            if (userTypes != null)
            {
                result = result.Where(m => userTypes.Contains(m.UserType));
            }

            if (ascending.HasValue == true)
            {
                if (ascending == false)
                {
                    result = result.OrderByDescending(m => m.UserType);
                }
                else
                {
                    result = result.OrderBy(m => m.UserType);
                }
            }

            if (ascendingUsername.HasValue == true)
            {
                if (ascendingUsername == false)
                {
                    result = result.OrderByDescending(m => m.UserName);
                }
                else
                {
                    result = result.OrderBy(m => m.UserName);
                }
            }

            if (ascendingName.HasValue == true)
            {
                if (ascendingName == false)
                {
                    result = result.OrderByDescending(m => m.Name);
                }
                else
                {
                    result = result.OrderBy(m => m.Name);
                }
            }

            return result;
        }

        public IQueryable<Membership> ByLabel(string label, bool? ascending, bool? ascendingUsername, bool? ascendingName)
        {
            var result = (IQueryable<Membership>)DataModel.MembershipsEnforceSecurity;
            result.Include(m => m.MembershipLabels);

            result = result.Where(m => (from l in m.MembershipLabels select l.Name).Contains(label));

            if (ascending.HasValue == true)
            {
                if (ascending == false)
                {
                    result = result.OrderByDescending(m => m.UserType);
                }
                else
                {
                    result = result.OrderBy(m => m.UserType);
                }
            }

            if (ascendingUsername.HasValue == true)
            {
                if (ascendingUsername == false)
                {
                    result = result.OrderByDescending(m => m.UserName);
                }
                else
                {
                    result = result.OrderBy(m => m.UserName);
                }
            }

            if (ascendingName.HasValue == true)
            {
                if (ascendingName == false)
                {
                    result = result.OrderByDescending(m => m.Name);
                }
                else
                {
                    result = result.OrderBy(m => m.Name);
                }
            }

            return result;
        }

    }
}
