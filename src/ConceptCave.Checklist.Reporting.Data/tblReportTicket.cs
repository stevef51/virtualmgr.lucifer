
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace ConceptCave.Checklist.Reporting.Data
{

using System;
    using System.Collections.Generic;
    
public partial class tblReportTicket
{

    public System.Guid Id { get; set; }

    public int ReportId { get; set; }

    public string JsonParameters { get; set; }

    public System.Guid RunAsUserId { get; set; }

    public string DefaultFormat { get; set; }

    public System.DateTime DateCreatedUtc { get; set; }

    public Nullable<System.DateTime> LastRunOnUtc { get; set; }



    public virtual tblReport tblReport { get; set; }

}

}
