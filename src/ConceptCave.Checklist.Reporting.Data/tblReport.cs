
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace ConceptCave.Checklist.Reporting.Data
{

using System;
    using System.Collections.Generic;
    
public partial class tblReport
{

    public tblReport()
    {

        this.tblReportTickets = new HashSet<tblReportTicket>();

        this.AspNetRoles = new HashSet<AspNetRole>();

    }


    public int Id { get; set; }

    public string Name { get; set; }

    public string Description { get; set; }

    public int Type { get; set; }

    public string Configuration { get; set; }



    public virtual tblReportData tblReportData { get; set; }

    public virtual ICollection<tblReportTicket> tblReportTickets { get; set; }

    public virtual ICollection<AspNetRole> AspNetRoles { get; set; }

}

}
