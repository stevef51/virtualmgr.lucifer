﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.Checklist.Reporting.Data.DTO
{
    /// <summary>
    /// Class to model a timesheet row containing the summary of work completed
    /// across tasks at sites for a task owner on a particular day
    /// </summary>
    public class TimesheetItem
    {
        public Guid Id { get; set; }
        public DateTime Date { get; set; }
        public string SiteName { get; set; }
        public Guid? SiteId { get; set; }
        public string OwnerName { get; set; }
        public Guid OwnerId { get; set; }

        public string CompanyName { get; set; }

        public string Role { get; set; }
        public DateTime FirstInteraction { get; set; }
        public DateTime LastInteraction { get; set; }

        public decimal ActiveDuration { get; set; }
        public decimal InactiveDuration { get; set; }

        public decimal InteractionDifference { get; set; }

        public decimal ExpectedDuration { get; set; }

        public int TaskCount { get; set; }

        public string ChecklistNotes { get; set; }

        public bool IsLeave { get; set; }

        public void Init(IQueryable<TaskWorklog> tasks, IQueryable<ChecklistResult> results, Dictionary<Guid, Company> companyCache)
        {
            if(tasks == null || tasks.Count() == 0)
            {
                throw new ArgumentException("Tasks must contain entries for TimesheetItem to be built from");
            }

            // we are going to assume that the first task in the list we have gives us the template (who, where etc)
            var t = tasks.First();

            Id = Guid.NewGuid();
            Date = t.Task.DateCreated.Date;
            SiteName = t.SiteName;
            SiteId = t.SiteId;
            OwnerName = t.Owner;
            OwnerId = t.UserId;
            if(t.Task.OwnerCompanyId.HasValue)
            {
                Company c = null;
                if(companyCache.TryGetValue(t.Task.OwnerCompanyId.Value, out c))
                {
                    CompanyName = c.Name;
                }
            }

            var ts = (from tt in tasks select tt.Task).GroupBy(tt => tt.Id).Select(tt => tt.First()).ToList();

            FirstInteraction = (from p in tasks select p.DateCreated).Min();
            LastInteraction = (from p in tasks select p.DateCompleted.Value).Max();
            InteractionDifference = (Decimal)LastInteraction.Subtract(FirstInteraction).TotalSeconds;
            ActiveDuration = (from p in tasks select p.ActualDuration.Value).Sum();
            InactiveDuration = (from p in tasks select p.InactiveDuration).Sum();
            ExpectedDuration = (from p in ts where p.EstimatedDurationSeconds.HasValue select p.EstimatedDurationSeconds.Value).Sum();
            TaskCount = ts.Count();

            if(results != null && results.Count() > 0)
            {
                ChecklistNotes = string.Join(", ", (from r in results select r.Value).ToArray());
            }
        }

        public void InitForLeave(IQueryable<Task> tasks, Dictionary<Guid, Company> companyCache)
        {
            if (tasks == null || tasks.Count() == 0)
            {
                throw new ArgumentException("Tasks must contain entries for TimesheetItem to be built from");
            }

            // we are going to assume that the first task in the list we have gives us the template (who, where etc)
            var t = tasks.First();

            Id = Guid.NewGuid();
            Date = t.DateCreated.Date;
            SiteName = t.SiteName;
            SiteId = t.SiteId;
            OwnerName = t.Owner;
            OwnerId = t.OwnerId;

            if (t.OwnerCompanyId.HasValue)
            {
                Company c = null;
                if (companyCache.TryGetValue(t.OwnerCompanyId.Value, out c))
                {
                    CompanyName = c.Name;
                }
            }

            var ts = tasks.GroupBy(tt => tt.Id).Select(tt => tt.First()).ToList();

            FirstInteraction = (from p in tasks select p.DateCreated).Min();
            var lastInteractions = (from p in tasks where p.DateCompleted.HasValue select p.DateCompleted.Value);
            if(lastInteractions.Count() > 0)
            {
                LastInteraction = lastInteractions.Max();
            }

            InteractionDifference = (from p in ts where p.EstimatedDurationSeconds.HasValue select p.EstimatedDurationSeconds.Value).Sum();
            ActiveDuration = 0;
            InactiveDuration = 0;
            ExpectedDuration = (from p in ts where p.EstimatedDurationSeconds.HasValue select p.EstimatedDurationSeconds.Value).Sum();
            TaskCount = ts.Count();

            IsLeave = true;
        }
    }
}
