﻿using ConceptCave.Checklist.Reporting.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConceptCave.Checklist.Reporting.Data.DTO
{
    public class CalendarEvent
    {
        public const string ScheduleStatusText = "Scheduled";
        public Guid Id { get; set; }

        public int RosterId { get; set; }
        public string Roster { get; set; }
        public Guid TaskTypeId { get; set; }
        public string TaskType { get; set; }
        public DateTime DateCreated { get; set; }

        public String DayOfWeek { get; set; }

        public DateTime? DateStarted { get; set; }
        public DateTime? DateCompleted { get; set; }
        public int? DaysSinceCompletion { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Guid? OwnerId { get; set; }
        public string Owner { get; set; }
        public Guid? OwnerCompanyId { get; set; }
        public Guid? OriginalOwnerId { get; set; }
        public string OriginalOwner { get; set; }
        public Guid? SiteId { get; set; }
        public string SiteName { get; set; }
        public int? RoleId { get; set; }
        public string Role { get; set; }
        public int Level { get; set; }
        public int Status { get; set; }
        public DateTime? StatusDate { get; set; }
        public string StatusText { get; set; }
        public Guid? CustomStatusId { get; set; }
        public string CustomStatusText { get; set; }

        public CalendarEvent(Schedule schedule, DateTime instanceDate)
        {
            Id = schedule.Id;
            RosterId = schedule.RosterId.Value;
            Roster = schedule.Roster;
            TaskTypeId = schedule.TaskTypeId.Value;
            TaskType = schedule.TaskType;
            DateCreated = instanceDate;
            DayOfWeek = instanceDate.DayOfWeek.ToString();
            Name = schedule.Name;
            Description = schedule.Description;
            OwnerId = schedule.OwnerId;
            OwnerCompanyId = schedule.OwnerCompanyId;
            Owner = schedule.Owner;
            OriginalOwnerId = schedule.OwnerId;
            OriginalOwner = schedule.Owner;
            SiteId = schedule.SiteId;
            SiteName = schedule.SiteName;
            RoleId = schedule.RoleId;
            Role = schedule.Role;
            Level = schedule.Level.Value;
            Status = -1;
            StatusDate = instanceDate;
            StatusText = CalendarEvent.ScheduleStatusText;
        }

        public CalendarEvent() { }
    }
}