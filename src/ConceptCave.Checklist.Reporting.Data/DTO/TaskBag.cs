﻿using ConceptCave.Checklist.Reporting.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.Checklist.Reporting.Data.DTO
{
    /// <summary>
    /// Class the wraps up the retrieval of things related to a task
    /// that might want to be itereated over while reporting.
    /// </summary>
    public class TaskBag
    {
        protected BagRepository _bagRepo;

        protected List<ImageBag> _images;

        protected List<ChecklistResultBag> _checklistResults;

        protected List<TaskFinishStatus> _statuses;

        protected string _incompleteStatusText;

        public Task Task { get; protected set; }

        /// <summary>
        /// This is a list of any images held against workflows against the task
        /// </summary>
        public List<ImageBag> Images
        {
            get
            {
                if(_images == null)
                {
                    _images = new List<ImageBag>();
                    var media = _bagRepo.MediaForTask(Task.Id);

                    foreach(var m in media)
                    {
                        Images.Add(new ImageBag(m.Data, Images.Count));
                    }
                }

                return _images;
            }
        }

        public List<ChecklistResultBag> ChecklistResults
        {
            get
            {
                if(_checklistResults == null)
                {
                    _checklistResults = new List<ChecklistResultBag>();

                    foreach(var c in _bagRepo.ChecklistResultsFortask(Task.Id))
                    {
                        _checklistResults.Add(new ChecklistResultBag(_bagRepo, c));
                    }
                }

                return _checklistResults;
            }
        }

        public string IncompleteStatusText
        {
            get
            {
                if(string.IsNullOrEmpty(_incompleteStatusText) == true)
                {
                    if(_statuses == null)
                    {
                        _statuses = _bagRepo.TaskFinishStatusForTask(Task.Id).ToList();
                    }

                    _incompleteStatusText = "";
                    foreach(var s in _statuses)
                    {
                        _incompleteStatusText += s.Text;
                        if(string.IsNullOrEmpty(s.Notes) == false)
                        {
                            _incompleteStatusText += ": " + s.Notes;
                        }

                        _incompleteStatusText += ", ";
                    }
                    _incompleteStatusText = _incompleteStatusText.TrimEnd(' ', ',');
                }

                return _incompleteStatusText;
            }
        }

        public TaskBag(BagRepository bagRepo, Task task)
        {
            this._bagRepo = bagRepo;
            this.Task = task;
        }
    }
}
