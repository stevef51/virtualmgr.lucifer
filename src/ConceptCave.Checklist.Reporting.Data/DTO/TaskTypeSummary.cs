﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.Checklist.Reporting.Data.DTO
{
    public class TaskTypeSummary
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid? OwnerId { get; set; }
        public int? OwnerTypeId { get; set; }
        public string OwnerTypeName { get; set; }
        public string OwnerName { get; set; }
        public Guid? SiteId { get; set; }
        public int? SiteTypeId { get; set; }
        public string SiteTypeName { get; set; }
        public string SiteName { get; set; }

        public int TotalTasks { get; set; }
        public int TotalTasksWithNoWorkflows { get; set; }
        public int TotalTasksWithWorkflows { get; set; }
        public decimal TotalClockedInTimeSeconds { get; set; }
    }


}
