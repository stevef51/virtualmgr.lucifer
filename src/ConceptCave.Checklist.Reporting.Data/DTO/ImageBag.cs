﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.Checklist.Reporting.Data.DTO
{
    public class ImageBag
    {
            public byte[] Data { get; protected set; }

            public int Index { get; protected set; }

            public ImageBag(byte[] data, int index)
            {
                Data = data;
                Index = index;
            }
    }
}
