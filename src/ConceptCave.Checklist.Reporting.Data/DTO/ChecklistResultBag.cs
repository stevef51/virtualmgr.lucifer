﻿using ConceptCave.Checklist.Reporting.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.Checklist.Reporting.Data.DTO
{
    public class ChecklistResultBag
    {
        public ChecklistResult ChecklistResult { get; protected set; }

        protected List<ImageBag> _images;

        protected BagRepository _bagRepo;

        public ChecklistResultBag(BagRepository bagRepo, ChecklistResult checklistResult)
        {
            _bagRepo = bagRepo;
            ChecklistResult = checklistResult;
        }

        public List<ImageBag> Images
        {
            get
            {
                if(_images == null && ChecklistResult.QuestionType == "UploadMedia")
                {
                    _images = new List<ImageBag>();

                    if(string.IsNullOrEmpty(ChecklistResult.Value) == false)
                    {
                        foreach (var img in _bagRepo.MediaForUploadMediaValue(ChecklistResult.Value))
                        {
                            _images.Add(new ImageBag(img.Data, _images.Count));
                        }
                    }
                }

                return _images;
            }
        }
        
    }
}
