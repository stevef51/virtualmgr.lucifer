﻿using ConceptCave.Checklist.Reporting.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConceptCave.Checklist.Reporting.Data.DTO
{
    public class ScheduleExclusionRuleHelper : ConceptCave.Scheduling.IScheduledTask
    {
        public Schedule Entity { get; set; }
        public Guid Id
        {
            get { return Entity.Id; }
        }

        public Guid? OwnerId
        {
            get { return Entity.OwnerId; }
        }

        public Guid? SiteId
        {
            get { return Entity.SiteId; }
        }

        public Guid TaskTypeId
        {
            get { return Entity.TaskTypeId.Value; }
        }

        public int CalendarRuleId
        {
            get { return Entity.CalendarRuleId.Value; }
        }

        public ConceptCave.Scheduling.ICalendarRule CalendarRule { get; protected set; }

        public int RosterId
        {
            get { return Entity.RosterId.Value; }
        }

        public int Level
        {
            get { return Entity.Level.Value; }
        }

        public int? RoleId
        {
            get { return Entity.RoleId; }
        }

        public bool IsArchived
        {
            get { return Entity.IsArchived.Value; }
        }

        public string LengthInDays
        {
            get { return Entity.LengthInDays; }
        }

        public IList<ConceptCave.Scheduling.ITask> Tasks { get; protected set; }

        public ScheduleExclusionRuleHelper(Schedule entity)
        {
            Entity = entity;
            CalendarRule = new CalendarRuleExclusionHelper(entity);

            Tasks = new List<ConceptCave.Scheduling.ITask>();
        }
    }

    public class CalendarRuleExclusionHelper : ConceptCave.Scheduling.ICalendarRule
    {
        public CalendarRuleExclusionHelper(Schedule entity)
        {
            Entity = entity;
        }

        public Schedule Entity { get; protected set; }

        public int Id
        {
            get { return Entity.CalendarRuleId.Value; }
        }

        public DateTime DateCreated
        {
            get { return Entity.DateCreated.Value; }
        }

        public DateTime StartDate
        {
            get { return Entity.StartDate.Value; }
        }

        public DateTime? EndDate
        {
            get { return Entity.EndDate; }
        }

        public string Frequency
        {
            get { return Entity.Frequency; }
        }

        public bool ExecuteSunday
        {
            get { return Entity.ExecuteSunday.Value; }
        }

        public bool ExecuteMonday
        {
            get { return Entity.ExecuteMonday.Value; }
        }

        public bool ExecuteTuesday
        {
            get { return Entity.ExecuteTuesday.Value; }
        }

        public bool ExecuteWednesday
        {
            get { return Entity.ExecuteWednesday.Value; }
        }

        public bool ExecuteThursday
        {
            get { return Entity.ExecuteThursday.Value; }
        }

        public bool ExecuteFriday
        {
            get { return Entity.ExecuteFriday.Value; }
        }

        public bool ExecuteSaturday
        {
            get { return Entity.ExecuteSaturday.Value; }
        }

        public string CRONExpression
        {
            get { return Entity.CRONExpression; }
        }

        public int Period
        {
            get { return Entity.Period.Value; }
        }
    }

}