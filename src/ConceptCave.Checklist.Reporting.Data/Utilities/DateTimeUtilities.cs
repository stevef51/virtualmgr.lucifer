﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VirtualMgr.Central;

namespace ConceptCave.Checklist.Reporting.Data.Utilities
{
    public class DateTimeUtilities
    {
        public static string DurationAsMinutesAndSeconds(DateTime? startDate, DateTime? endDate)
        {
            return DurationAsMinutesAndSeconds(startDate, endDate, false);
        }

        public static string DurationAsMinutesAndSeconds(DateTime? startDate, DateTime? endDate, bool includeZero)
        {
            if (startDate.HasValue == false || endDate.HasValue == false)
            {
                return "";
            }

            return DurationAsMinutesAndSeconds(endDate.Value.Subtract(startDate.Value), includeZero);
        }

        public static string DurationAsMinutesAndSeconds(TimeSpan duration)
        {
            return DurationAsMinutesAndSeconds(duration, false);
        }

        public static string DurationAsMinutesAndSeconds(TimeSpan duration, bool includeZero)
        {
            if (duration.TotalSeconds < 1.0 && includeZero == false)
            {
                return "";
            }

            if (duration.TotalHours > 24)
                return string.Format("{0} days {1} hours {2} mins {3} secs", duration.Days, duration.Hours, duration.Minutes, duration.Seconds);

            else if (duration.TotalMinutes > 60)
                return string.Format("{0} hours {1} mins {2} secs", duration.Hours, duration.Minutes, duration.Seconds);

            else if (duration.TotalSeconds > 60)
                return string.Format("{0} mins {1} secs", duration.Minutes, duration.Seconds);

            else
                return string.Format("{0} secs", duration.Seconds);
        }

        public static string DurationAsMinutesAndSeconds(int duration)
        {
            return DurationAsMinutesAndSeconds(duration, false);
        }

        public static string DurationAsMinutesAndSeconds(int duration, bool includeZero)
        {
            return DurationAsMinutesAndSeconds(new TimeSpan(0, 0, 0, duration), includeZero);
        }

        public static string DurationAsMinutesAndSeconds(object duration)
        {
            return DurationAsMinutesAndSeconds(duration, false);
        }

        public static string DurationAsMinutesAndSeconds(object duration, bool includeZero)
        {
            if(duration == null)
            {
                if (includeZero)
                    return "0 secs";
                else
                    return "";
            }

            if(duration is int)
            {
                return DurationAsMinutesAndSeconds((int)duration, includeZero);
            }
            else if(duration is decimal)
            {
                return DurationAsMinutesAndSeconds((int)((decimal)duration), includeZero);
            }
            else if (duration is double)
            {
                return DurationAsMinutesAndSeconds((int)((double)duration), includeZero);
            }

            return "";
        }

        /// <summary>
        /// Gets an object representing the currently logged in user to the system
        /// </summary>
        /// <returns></returns>
        public static object CurrentUser()
        {
            var dataModel = new TenantDataModel();
            
            var data = (from m in dataModel.Memberships where m.UserName == Thread.CurrentPrincipal.Identity.Name select m);

            if (data.Count() > 0)
            {
                return data.First();
            }

            return null;
        }

        /// <summary>
        /// Converts the date object handed in and returns a date in the timezone denoted by relativeTo.
        /// </summary>
        /// <param name="date">Date in UTC to convert</param>
        /// <param name="relativeTo">Denotes what timezone to convert to. Typically this is the object returned by the CurrentUser method.</param>
        /// <returns></returns>
        public static DateTime ToUserLocal(object date, object relativeTo)
        {
            if((date is DateTime) == false)
            {
                return DateTime.MinValue;
            }

            // if we can't get anything else, we'll default to Tenant timezone
            TimeZoneInfo timeZoneInfo = MultiTenantManager.CurrentTenant != null ? MultiTenantManager.CurrentTenant.TimeZoneInfo : TimeZoneInfo.Local;

            if(relativeTo != null)
            {
                string timeZone = "UTC";
                if (relativeTo is string)
                {
                    timeZone = (string)relativeTo;
                }
                else if (relativeTo is Membership)
                {
                    timeZone = ((Membership)relativeTo).TimeZone;
                }
                timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(timeZone);
            }

            DateTime d = (DateTime)date;

            return TimeZoneInfo.ConvertTimeFromUtc(DateTime.SpecifyKind(d, DateTimeKind.Utc), timeZoneInfo);
        }
    }
}
