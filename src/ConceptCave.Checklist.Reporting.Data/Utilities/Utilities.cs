﻿using ConceptCave.Checklist.Reporting.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.Checklist.Reporting.Data.Utilities
{
    static class Utilities
    {
        public static Guid ConvertToGuid(object value)
        {
            if (value == null)
                return Guid.Empty;

            if (value is Guid)
                return (Guid)value;

            return Guid.Parse(value.ToString());
        }

        public static Guid? ConvertToNullableGuid(object value)
        {
            if (value == null)
                return null;

            return ConvertToGuid(value);
        }

        public static string NewLine()
        {
            return Environment.NewLine;
        }

        public static Color ConvertToColor(object value)
        {
            if (value == null)
                return Color.Transparent;

            if (value is Color)
                return (Color)value;

            if (value is Int32 || value is Int64)
                return Color.FromArgb(Convert.ToInt32(value));

            return Color.FromName(value.ToString());
        }

        public static string[] Split(string value)
        {
            return value.Split(',');
        }

        public static List<object> Select(IEnumerable<object> list, string property)
        {
            PropertyInfo pi = null;
            List<dynamic> r = new List<dynamic>();
            foreach(var o in list)
            {
                if (o == null)
                    continue;

                if (pi == null)
                {
                    pi = o.GetType().GetProperty(property);
                }

                var v = pi.GetValue(o);
                r.Add(v); 
            }
            return r;
        }

        public static Bitmap JsonToImage(string json)
        {
            try
            {
                SignatureRepository sig = new SignatureRepository();

                return sig.JsonToImage(json);
            }
            catch
            {
                return null;
            }
        }
    }
}
