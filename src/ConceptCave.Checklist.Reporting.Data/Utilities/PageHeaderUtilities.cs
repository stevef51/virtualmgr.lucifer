﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.Checklist.Reporting.Data.Utilities
{
    public class PageHeaderUtilities
    {
        public static byte[] Logo()
        {
            var data = new TenantDataModel();

            var s = from g in data.tblGlobalSettings where g.Name == "Reporting.Logo" select g; 

            if(s.Count() == 0)
            {
                return null;
            }

            Guid id = Guid.Parse(s.First().Value);

            var d = from m in data.tblMediaDatas where m.MediaId == id select m;

            return d.First().Data;
        }
    }
}
