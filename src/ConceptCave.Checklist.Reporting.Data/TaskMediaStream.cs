
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace ConceptCave.Checklist.Reporting.Data
{

using System;
    using System.Collections.Generic;
    
public partial class TaskMediaStream
{

    public System.Guid MediaId { get; set; }

    public System.Guid TaskId { get; set; }

    public string TaskName { get; set; }

    public string TaskDescription { get; set; }

    public System.DateTime TaskDateCreated { get; set; }

    public Nullable<System.DateTime> TaskDateCompleted { get; set; }

    public int TaskLevel { get; set; }

    public System.DateTime TaskStatusDate { get; set; }

    public string TaskCompletionNotes { get; set; }

    public string TaskStatusText { get; set; }

    public Nullable<int> HierarchyBucketId { get; set; }

    public System.Guid TaskTypeId { get; set; }

    public string TaskType { get; set; }

    public System.Guid OwnerId { get; set; }

    public string Owner { get; set; }

    public Nullable<System.Guid> OwnerCompanyId { get; set; }

    public System.Guid SiteId { get; set; }

    public string SiteName { get; set; }

}

}
