
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace ConceptCave.Checklist.Reporting.Data
{

using System;
    using System.Collections.Generic;
    
public partial class Product
{

    public int Id { get; set; }

    public string Name { get; set; }

    public string Description { get; set; }

    public string Code { get; set; }

    public Nullable<System.Guid> MediaId { get; set; }

    public Nullable<decimal> Price { get; set; }

    public int ProductTypeId { get; set; }

    public Nullable<int> PriceJsFunctionId { get; set; }

    public bool IsCoupon { get; set; }

    public string Extra { get; set; }

}

}
