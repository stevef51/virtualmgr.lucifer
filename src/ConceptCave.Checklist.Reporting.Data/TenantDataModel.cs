﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualMgr.Central;

namespace ConceptCave.Checklist.Reporting.Data
{
    public class TenantDataModel : DataModel
    {
        public TenantDataModel() : base(GetConnectionString(null))
        {
            Configuration.ProxyCreationEnabled = false;
        }

        public TenantDataModel(string suggestedEntityConnectionString) : base(GetConnectionString(suggestedEntityConnectionString))
        {
        }

        public TenantDataModel(string suggestedEntityConnectionString, string connectionString) : base(GetConnectionString(suggestedEntityConnectionString, connectionString))
        {
        }

        private static string GetConnectionString(string suggestedEntityConnectionString, string tenantConnectionString = null)
        {
            EntityConnectionStringBuilder connectionStringBuilder = null;
            try
            {
                if (suggestedEntityConnectionString != null)
                    connectionStringBuilder = new EntityConnectionStringBuilder(suggestedEntityConnectionString);
            }
            catch
            {
                // It was not an Entity Model Connection String
            }

            if (connectionStringBuilder == null)
                connectionStringBuilder = new EntityConnectionStringBuilder(System.Configuration.ConfigurationManager.ConnectionStrings["DataModel"].ConnectionString);

            // In a production Multi-tenant environment we will use the MultiTenantManager to retrieve correct connection string
            // otherwise we are probably running in Telerik designer and will use passed in connection
            if (MultiTenantManager.CurrentTenant != null)
            {
                var tenantConnectionBuilder = new SqlConnectionStringBuilder(tenantConnectionString ?? MultiTenantManager.CurrentTenant.ConnectionString);
                
                // We have to enable MultipleActiveResultSets due to the possibility of recursive SqlDataReaders on the DataModel connection
                tenantConnectionBuilder.MultipleActiveResultSets = true;
                connectionStringBuilder.ProviderConnectionString = tenantConnectionBuilder.ConnectionString;
            }

            return connectionStringBuilder.ConnectionString;
        }
    }
}
