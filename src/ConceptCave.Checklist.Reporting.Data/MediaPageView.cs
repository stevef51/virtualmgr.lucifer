
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace ConceptCave.Checklist.Reporting.Data
{

using System;
    using System.Collections.Generic;
    
public partial class MediaPageView
{

    public System.Guid Id { get; set; }

    public System.Guid MediaViewingId { get; set; }

    public int Page { get; set; }

    public System.DateTime StartDate { get; set; }

    public System.DateTime EndDate { get; set; }

    public int TotalSeconds { get; set; }

    public System.Guid MediaId { get; set; }

    public string Name { get; set; }

    public string UserName { get; set; }

    public int UserTypeId { get; set; }

    public string UserType { get; set; }

    public string Document { get; set; }

}

}
