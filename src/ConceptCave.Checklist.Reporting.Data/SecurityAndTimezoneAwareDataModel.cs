﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using VirtualMgr.Central;
using VirtualMgr.Central.Interfaces;

namespace ConceptCave.Checklist.Reporting.Data
{
    public class TaskResultSettings
    {
        public bool ExpandStatusText { get; set; }
    }

    public enum QueryScope
    {
        Normal,
        IncludeCurrentUser,
        OnlyCurrentUser
    }

    /// <summary>
    /// This is a class derived from DataModel that manages the filtering of the various data
    /// based on the business rules (company and hierarchy etc). It also convers any date/times
    /// from UTC to the current users local timezone. The class models this logic in a central
    /// location so that odata and reporting queries get the same logic applied to them.
    /// </summary>
    public class SecurityAndTimezoneAwareDataModel : TenantDataModel
    {

        private bool _hasLoadedUser;
        private bool _hasLoadedDefaults;

        // Metric distances are in Metres, Imperial in feet
        public const string DefaultUnitsForDistancesSetting = "DefaultUnitsForDistances";

        public TaskResultSettings TaskResultSettings { get; protected set; }

        private tblGlobalSetting _defaultUnitsForDistances;
        private Membership _currentUser;
        private TimeZoneInfo _currentTimezone;

        private ITenantContext _tenant;

        public bool TidyHtml { get; set; }

        public SecurityAndTimezoneAwareDataModel()
        {
            // we hook into the object materialized event so that we can do some work on that
            ((IObjectContextAdapter)this).ObjectContext.ObjectMaterialized += ObjectContext_ObjectMaterialized;

            this.TaskResultSettings = new TaskResultSettings();
        }

        public SecurityAndTimezoneAwareDataModel(string entityConnectionString) : base(entityConnectionString)
        {
            ((IObjectContextAdapter)this).ObjectContext.ObjectMaterialized += ObjectContext_ObjectMaterialized;

            this.TaskResultSettings = new TaskResultSettings();
        }

        public SecurityAndTimezoneAwareDataModel(ITenantContext tenant) : base(null, tenant.ConnectionString)
        {
            _tenant = tenant;
            ((IObjectContextAdapter)this).ObjectContext.ObjectMaterialized += ObjectContext_ObjectMaterialized;

            this.TaskResultSettings = new TaskResultSettings();
        }

        public virtual IQueryable<Schedule> SchedulesEnforceSecurity
        {
            get
            {
                var result = (IQueryable<Schedule>)this.Schedules;

                // we do this as the queries seem to get executed on a different thread, at least in web api,
                // so force the current user in the calling thread to be cached
                LoadCurrentUser();

                if (IsCurrentUserInRole("NotLimitedToCompany") == false && CurrentUser != null)
                {
                    if (CurrentUser.CompanyId.HasValue == false)
                    {
                        // we can't be limited to a company and not have one, get out of here
                        throw new System.Security.SecurityException("User is limited to a company but doesn't have a company defined");
                    }

                    result = result.Where(m => m.OwnerCompanyId == CurrentUser.CompanyId);
                }

                if (IsCurrentUserInRole("NotLimitedToHierarchy") == false && CurrentUser != null)
                {
                    var parents = this.MergedTeamHierarchyWithLabels.Where(b => b.UserId == CurrentUser.UserId &&
                                (b.OpenSecurityThroughLabels == true || b.OpenSecurityThroughLabels == false && b.PrimaryRecord == 1));

                    // we want all buckets that are in the hierarchies this user is in and who are bounded by the buckets this user is in
                    // since we are going to query the schedule using the bucket id, we can go direct to tblHierarchyBuckets for the id
                    // as the mergedhierarchylabel view will just contain duplicates of the bucket id, any schedules will be the same
                    // for anyone being assigned tasks through the bucket
                    var children = this.tblHierarchyBuckets.Where(b => (from p in parents where b.LeftIndex > p.LeftIndex && b.RightIndex < p.RightIndex select p.HierarchyId).Contains(b.HierarchyId));

                    var bucketIds = (from b in children select b.Id);

                    result = result.Where(m => bucketIds.Contains(m.HierarchyBucketId));
                }

                return result;
            }
        }

        public virtual IQueryable<PendingPayment> PendingPaymentsEnforceSecurity
        {
            get
            {
                var result = (IQueryable<PendingPayment>)this.PendingPayments;

                // we do this as the queries seem to get executed on a different thread, at least in web api,
                // so force the current user in the calling thread to be cached
                LoadCurrentUser();

                if (IsCurrentUserInRole("InvoiceAdmin") == false && CurrentUser != null)
                {
                    result = result.Where(m => m.UserId == CurrentUser.UserId);
                }

                return result;
            }
        }

        public virtual IQueryable<PaymentResponse> PaymentResponsesEnforceSecurity
        {
            get
            {
                var result = (IQueryable<PaymentResponse>)this.PaymentResponses;
                result.Include(p => p.PendingPayment);

                // we do this as the queries seem to get executed on a different thread, at least in web api,
                // so force the current user in the calling thread to be cached
                LoadCurrentUser();

                if (IsCurrentUserInRole("InvoiceAdmin") == false && CurrentUser != null)
                {
                    result = result.Where(m => m.PendingPayment.UserId == CurrentUser.UserId);
                }

                return result;
            }
        }

        public virtual IQueryable<Roster> RostersEnforceSecurity
        {
            get
            {
                var result = (IQueryable<Roster>)this.Rosters;

                // we do this as the queries seem to get executed on a different thread, at least in web api,
                // so force the current user in the calling thread to be cached
                LoadCurrentUser();

                if (IsCurrentUserInRole("NotLimitedToHierarchy") == false && CurrentUser != null)
                {
                    var rosters = (from h in this.TeamHierarchies where h.UserId == CurrentUser.UserId select h.RosterId).Distinct();

                    result = result.Where(r => rosters.Contains(r.Id));
                }

                return result;
            }
        }

        public virtual IQueryable<RosterRole> RosterRolesEnforceSecurity
        {
            get
            {
                return RosterRolesEnforceSecurityAndScope(null);
            }
        }

        public virtual IQueryable<RosterRole> RosterRolesEnforceSecurityAndScope(QueryScope? scope)
        {
            QueryScope sc = QueryScope.Normal;

            if (scope.HasValue == true)
            {
                sc = scope.Value;
            }

            var result = (IQueryable<RosterRole>)this.RosterRoles;

            // we do this as the queries seem to get executed on a different thread, at least in web api,
            // so force the current user in the calling thread to be cached
            LoadCurrentUser();

            if (IsCurrentUserInRole("NotLimitedToCompany") == false && CurrentUser != null)
            {
                if (CurrentUser.CompanyId.HasValue == false)
                {
                    // we can't be limited to a company and not have one, get out of here
                    throw new System.Security.SecurityException("User is limited to a company but doesn't have a company defined");
                }

                result = result.Where(m => m.CompanyId == CurrentUser.CompanyId);
            }

            if (IsCurrentUserInRole("NotLimitedToHierarchy") == false && CurrentUser != null)
            {
                if(sc == QueryScope.OnlyCurrentUser)
                {
                    result = result.Where(b => b.UserId == CurrentUser.UserId);
                }
                else
                {
                    var parents = this.MergedTeamHierarchyWithLabels.Where(b => b.UserId == CurrentUser.UserId &&
                                (b.OpenSecurityThroughLabels == true || b.OpenSecurityThroughLabels == false && b.PrimaryRecord == 1));

                    IQueryable<tblHierarchyBucket> children = null;

                    if(sc == QueryScope.Normal)
                    {
                        // we want all buckets that are in the hierarchies this user is in and who are bounded by the buckets this user is in
                        children = this.tblHierarchyBuckets.Where(b => (from p in parents where b.LeftIndex > p.LeftIndex && b.RightIndex < p.RightIndex select p.HierarchyId).Contains(b.HierarchyId));
                    }
                    else
                    {
                        // we want all buckets that are in the hierarchies this user is in and who are bounded by the buckets this user is in
                        children = this.tblHierarchyBuckets.Where(b => (from p in parents where b.LeftIndex >= p.LeftIndex && b.RightIndex <= p.RightIndex select p.HierarchyId).Contains(b.HierarchyId));
                    }

                    var bucketIds = (from b in children select b.Id);

                    result = result.Where(m => bucketIds.Contains(m.HierarchyBucketId));
                }
            }

            return result;
        }

        public virtual IQueryable<TeamHierarchy> TeamHierarchiesEnforceSecurityAndScope(QueryScope? scope)
        {
            QueryScope sc = QueryScope.Normal;

            if (scope.HasValue == true)
            {
                sc = scope.Value;
            }

            var result = (IQueryable<TeamHierarchy>)this.TeamHierarchies;

            // we do this as the queries seem to get executed on a different thread, at least in web api,
            // so force the current user in the calling thread to be cached
            LoadCurrentUser();

            if (IsCurrentUserInRole("NotLimitedToCompany") == false && CurrentUser != null)
            {
                if (CurrentUser.CompanyId.HasValue == false)
                {
                    // we can't be limited to a company and not have one, get out of here
                    throw new System.Security.SecurityException("User is limited to a company but doesn't have a company defined");
                }

                result = result.Where(r => r.CompanyId == CurrentUser.CompanyId);
            }

            if (IsCurrentUserInRole("NotLimitedToHierarchy") == false && CurrentUser != null)
            {
                if(sc == QueryScope.OnlyCurrentUser)
                {
                    result = result.Where(r => r.UserId == CurrentUser.UserId);
                }
                else
                {
                    var parents = this.TeamHierarchies.Where(b => b.UserId == CurrentUser.UserId);

                    if (sc == QueryScope.Normal)
                    {
                        result = result.Where(b => (from p in parents where b.HierarchyId == p.HierarchyId && b.LeftIndex > p.LeftIndex && b.RightIndex < p.RightIndex select p).Count() > 0);
                    }
                    else
                    {
                        result = result.Where(b => (from p in parents where b.HierarchyId == p.HierarchyId && b.LeftIndex >= p.LeftIndex && b.RightIndex <= p.RightIndex select p).Count() > 0);
                    }
                }
            }

            return result;
        }

        public virtual IQueryable<Task> TasksEnforceSecurityAndScope(QueryScope? scope)
        {
            QueryScope sc = QueryScope.Normal;

            if(scope.HasValue == true)
            {
                sc = scope.Value;
            }

            var result = (IQueryable<Task>)this.Tasks;

            // we do this as the queries seem to get executed on a different thread, at least in web api,
            // so force the current user in the calling thread to be cached
            LoadCurrentUser();

            if (IsCurrentUserInRole("NotLimitedToCompany") == false && CurrentUser != null)
            {
                if (CurrentUser.CompanyId.HasValue == false)
                {
                    // we can't be limited to a company and not have one, get out of here
                    throw new System.Security.SecurityException("User is limited to a company but doesn't have a company defined");
                }

                result = result.Where(m => m.OwnerCompanyId == CurrentUser.CompanyId);
            }

            if (IsCurrentUserInRole("NotLimitedToHierarchy") == false && CurrentUser != null)
            {
                var parents = this.MergedTeamHierarchyWithLabels.Where(b => b.UserId == CurrentUser.UserId &&
                                    (b.OpenSecurityThroughLabels == true || b.OpenSecurityThroughLabels == false && b.PrimaryRecord == 1));

                // we want all buckets that are in the hierarchies this user is in and who are bounded by the buckets this user is in
                IQueryable<tblHierarchyBucket> children = this.tblHierarchyBuckets.Where(b => (from p in parents where b.LeftIndex > p.LeftIndex && b.RightIndex < p.RightIndex select p.HierarchyId).Contains(b.HierarchyId));
                
                if(sc == QueryScope.IncludeCurrentUser)
                {
                    children = this.tblHierarchyBuckets.Where(b => (from p in parents where b.LeftIndex >= p.LeftIndex && b.RightIndex <= p.RightIndex select p.HierarchyId).Contains(b.HierarchyId));
                }
                else if(sc == QueryScope.OnlyCurrentUser)
                {
                    children = this.tblHierarchyBuckets.Where(b => b.UserId == CurrentUser.UserId);
                }

                var bucketIds = (from b in children select b.Id);

                result = result.Where(m => m.HierarchyBucketId.HasValue == true && bucketIds.Contains(m.HierarchyBucketId.Value));
            }
            else if(CurrentUser != null)
            {
                if(sc == QueryScope.OnlyCurrentUser)
                {
                    result = result.Where(t => t.OwnerId == CurrentUser.UserId);
                }
                else if(sc == QueryScope.Normal)
                {
                    result = result.Where(t => t.OwnerId != CurrentUser.UserId);
                }
            }

            return result;
        }

        public virtual IQueryable<Task> TasksEnforceSecurity
        {
            get
            {
                return this.TasksEnforceSecurityAndScope(null);
            }
        }

        public virtual IQueryable<TaskWorklog> TaskWorklogsEnforceSecurityAndScope(QueryScope? scope)
        {
            QueryScope sc = QueryScope.Normal;

            if (scope.HasValue == true)
            {
                sc = scope.Value;
            }

            var result = (IQueryable<TaskWorklog>)this.TaskWorklogs;

            // we do this as the queries seem to get executed on a different thread, at least in web api,
            // so force the current user in the calling thread to be cached
            LoadCurrentUser();

            if (IsCurrentUserInRole("NotLimitedToCompany") == false && CurrentUser != null)
            {
                if (CurrentUser.CompanyId.HasValue == false)
                {
                    // we can't be limited to a company and not have one, get out of here
                    throw new System.Security.SecurityException("User is limited to a company but doesn't have a company defined");
                }

                result = result.Where(m => m.OwnerCompanyId == CurrentUser.CompanyId);
            }

            if (IsCurrentUserInRole("NotLimitedToHierarchy") == false && CurrentUser != null)
            {
                var parents = this.MergedTeamHierarchyWithLabels.Where(b => b.UserId == CurrentUser.UserId &&
                                    (b.OpenSecurityThroughLabels == true || b.OpenSecurityThroughLabels == false && b.PrimaryRecord == 1));

                // we want all buckets that are in the hierarchies this user is in and who are bounded by the buckets this user is in
                IQueryable<tblHierarchyBucket> children = this.tblHierarchyBuckets.Where(b => (from p in parents where b.LeftIndex > p.LeftIndex && b.RightIndex < p.RightIndex select p.HierarchyId).Contains(b.HierarchyId));

                if (sc == QueryScope.IncludeCurrentUser)
                {
                    children = this.tblHierarchyBuckets.Where(b => (from p in parents where b.LeftIndex >= p.LeftIndex && b.RightIndex <= p.RightIndex select p.HierarchyId).Contains(b.HierarchyId));
                }
                else if (sc == QueryScope.OnlyCurrentUser)
                {
                    children = this.tblHierarchyBuckets.Where(b => b.UserId == CurrentUser.UserId);
                }

                var bucketIds = (from b in children select b.Id);

                result = result.Where(m => m.HierarchyBucketId.HasValue == true && bucketIds.Contains(m.HierarchyBucketId.Value));
            }
            else if (CurrentUser != null)
            {
                if (sc == QueryScope.OnlyCurrentUser)
                {
                    result = result.Where(t => t.UserId == CurrentUser.UserId);
                }
                else if (sc == QueryScope.Normal)
                {
                    result = result.Where(t => t.UserId != CurrentUser.UserId);
                }
            }

            return result;

        }

        public virtual IQueryable<TaskWorklog> TaskWorklogsEnforceSecurity
        {
            get
            {
                return this.TaskWorklogsEnforceSecurityAndScope(null);
            }
        }

        public virtual IQueryable<Order> OrdersEnforceSecurityAndScope(QueryScope? scope)
        {
            QueryScope sc = QueryScope.Normal;

            if (scope.HasValue == true)
            {
                sc = scope.Value;
            }

            var result = (IQueryable<Order>)this.Orders;

            // we do this as the queries seem to get executed on a different thread, at least in web api,
            // so force the current user in the calling thread to be cached
            LoadCurrentUser();

            if (IsCurrentUserInRole("NotLimitedToCompany") == false && CurrentUser != null)
            {
                if (CurrentUser.CompanyId.HasValue == false)
                {
                    // we can't be limited to a company and not have one, get out of here
                    throw new System.Security.SecurityException("User is limited to a company but doesn't have a company defined");
                }

                result = result.Where(m => m.OrderedByCompanyId == CurrentUser.CompanyId);
            }

            if (IsCurrentUserInRole("NotLimitedToHierarchy") == false && CurrentUser != null)
            {
                var parents = this.MergedTeamHierarchyWithLabels.Where(b => b.UserId == CurrentUser.UserId &&
                                    (b.OpenSecurityThroughLabels == true || b.OpenSecurityThroughLabels == false && b.PrimaryRecord == 1));

                // we want all buckets that are in the hierarchies this user is in and who are bounded by the buckets this user is in
                IQueryable<tblHierarchyBucket> children = this.tblHierarchyBuckets.Where(b => (from p in parents where b.LeftIndex > p.LeftIndex && b.RightIndex < p.RightIndex select p.HierarchyId).Contains(b.HierarchyId));

                if (sc == QueryScope.IncludeCurrentUser)
                {
                    children = this.tblHierarchyBuckets.Where(b => (from p in parents where b.LeftIndex >= p.LeftIndex && b.RightIndex <= p.RightIndex select p.HierarchyId).Contains(b.HierarchyId));
                }
                else if (sc == QueryScope.OnlyCurrentUser)
                {
                    children = this.tblHierarchyBuckets.Where(b => b.UserId == CurrentUser.UserId);
                }

                var bucketIds = (from b in children select b.Id);

                result = result.Where(m => m.HierarchyBucketId.HasValue == true && bucketIds.Contains(m.HierarchyBucketId.Value));
            }
            else if (CurrentUser != null)
            {
                if (sc == QueryScope.OnlyCurrentUser)
                {
                    result = result.Where(t => t.OrderedById == CurrentUser.UserId);
                }
                else if (sc == QueryScope.Normal)
                {
                    result = result.Where(t => t.OrderedById != CurrentUser.UserId);
                }
            }

            return result;
        }

        public virtual IQueryable<OrderItem> OrderItemsEnforceSecurityAndScope(QueryScope? scope)
        {
            QueryScope sc = QueryScope.Normal;

            if (scope.HasValue == true)
            {
                sc = scope.Value;
            }

            var result = (IQueryable<OrderItem>)this.OrderItems;

            // we do this as the queries seem to get executed on a different thread, at least in web api,
            // so force the current user in the calling thread to be cached
            LoadCurrentUser();

            if (IsCurrentUserInRole("NotLimitedToCompany") == false && CurrentUser != null)
            {
                if (CurrentUser.CompanyId.HasValue == false)
                {
                    // we can't be limited to a company and not have one, get out of here
                    throw new System.Security.SecurityException("User is limited to a company but doesn't have a company defined");
                }

                result = result.Where(m => m.OrderedByCompanyId == CurrentUser.CompanyId);
            }

            if (IsCurrentUserInRole("NotLimitedToHierarchy") == false && CurrentUser != null)
            {
                var parents = this.MergedTeamHierarchyWithLabels.Where(b => b.UserId == CurrentUser.UserId &&
                                    (b.OpenSecurityThroughLabels == true || b.OpenSecurityThroughLabels == false && b.PrimaryRecord == 1));

                // we want all buckets that are in the hierarchies this user is in and who are bounded by the buckets this user is in
                IQueryable<tblHierarchyBucket> children = this.tblHierarchyBuckets.Where(b => (from p in parents where b.LeftIndex > p.LeftIndex && b.RightIndex < p.RightIndex select p.HierarchyId).Contains(b.HierarchyId));

                if (sc == QueryScope.IncludeCurrentUser)
                {
                    children = this.tblHierarchyBuckets.Where(b => (from p in parents where b.LeftIndex >= p.LeftIndex && b.RightIndex <= p.RightIndex select p.HierarchyId).Contains(b.HierarchyId));
                }
                else if (sc == QueryScope.OnlyCurrentUser)
                {
                    children = this.tblHierarchyBuckets.Where(b => b.UserId == CurrentUser.UserId);
                }

                var bucketIds = (from b in children select b.Id);

                result = result.Where(m => m.HierarchyBucketId.HasValue == true && bucketIds.Contains(m.HierarchyBucketId.Value));
            }
            else if (CurrentUser != null)
            {
                if (sc == QueryScope.OnlyCurrentUser)
                {
                    result = result.Where(t => t.OrderedById == CurrentUser.UserId);
                }
                else if (sc == QueryScope.Normal)
                {
                    result = result.Where(t => t.OrderedById != CurrentUser.UserId);
                }
            }

            return result;
        }


        public virtual IQueryable<TimeBetweenTask> TimeBetweenTasksEnforceSecurity
        {
            get
            {
                var result = (IQueryable<TimeBetweenTask>)this.TimeBetweenTasks;

                // we do this as the queries seem to get executed on a different thread, at least in web api,
                // so force the current user in the calling thread to be cached
                LoadCurrentUser();

                if (IsCurrentUserInRole("NotLimitedToCompany") == false && CurrentUser != null)
                {
                    if (CurrentUser.CompanyId.HasValue == false)
                    {
                        // we can't be limited to a company and not have one, get out of here
                        throw new System.Security.SecurityException("User is limited to a company but doesn't have a company defined");
                    }

                    result = result.Where(m => m.CompanyId == CurrentUser.CompanyId);
                }

                if (IsCurrentUserInRole("NotLimitedToHierarchy") == false && CurrentUser != null)
                {
                    var parents = this.MergedTeamHierarchyWithLabels.Where(b => b.UserId == CurrentUser.UserId &&
                                        (b.OpenSecurityThroughLabels == true || b.OpenSecurityThroughLabels == false && b.PrimaryRecord == 1));

                    // we want all buckets that are in the hierarchies this user is in and who are bounded by the buckets this user is in
                    var children = this.tblHierarchyBuckets.Where(b => (from p in parents where b.LeftIndex > p.LeftIndex && b.RightIndex < p.RightIndex select p.HierarchyId).Contains(b.HierarchyId));

                    var bucketIds = (from b in children select b.Id);

                    result = result.Where(m => m.HierarchyBucketId.HasValue == true && bucketIds.Contains(m.HierarchyBucketId.Value));
                }

                return result;
            }
        }

        public virtual IQueryable<TaskWorkLoadingActivity> TaskWorkLoadingActivitiesEnforceSecurity
        {
            get
            {
                var result = (IQueryable<TaskWorkLoadingActivity>)this.TaskWorkLoadingActivities;

                // we do this as the queries seem to get executed on a different thread, at least in web api,
                // so force the current user in the calling thread to be cached
                LoadCurrentUser();

                if (IsCurrentUserInRole("NotLimitedToCompany") == false && CurrentUser != null)
                {
                    if (CurrentUser.CompanyId.HasValue == false)
                    {
                        // we can't be limited to a company and not have one, get out of here
                        throw new System.Security.SecurityException("User is limited to a company but doesn't have a company defined");
                    }

                    result = result.Where(m => m.OwnerCompanyId == CurrentUser.CompanyId);
                }

                if (IsCurrentUserInRole("NotLimitedToHierarchy") == false && CurrentUser != null)
                {
                    var parents = this.MergedTeamHierarchyWithLabels.Where(b => b.UserId == CurrentUser.UserId &&
                                        (b.OpenSecurityThroughLabels == true || b.OpenSecurityThroughLabels == false && b.PrimaryRecord == 1));

                    // we want all buckets that are in the hierarchies this user is in and who are bounded by the buckets this user is in
                    var children = this.tblHierarchyBuckets.Where(b => (from p in parents where b.LeftIndex > p.LeftIndex && b.RightIndex < p.RightIndex select p.HierarchyId).Contains(b.HierarchyId));

                    var bucketIds = (from b in children select b.Id);

                    result = result.Where(m => m.HierarchyBucketId.HasValue == true && bucketIds.Contains(m.HierarchyBucketId.Value));
                }

                return result;
            }
        }

        public virtual IQueryable<IncompleteTask> IncompleteTasksEnforceSecurityAndScope(QueryScope? scope)
        {
            QueryScope sc = QueryScope.Normal;

            if (scope.HasValue == true)
            {
                sc = scope.Value;
            }

            var result = (IQueryable<IncompleteTask>)this.IncompleteTasks;

            // we do this as the queries seem to get executed on a different thread, at least in web api,
            // so force the current user in the calling thread to be cached
            LoadCurrentUser();

            if (IsCurrentUserInRole("NotLimitedToCompany") == false && CurrentUser != null)
            {
                if (CurrentUser.CompanyId.HasValue == false)
                {
                    // we can't be limited to a company and not have one, get out of here
                    throw new System.Security.SecurityException("User is limited to a company but doesn't have a company defined");
                }

                result = result.Where(m => m.OwnerCompanyId == CurrentUser.CompanyId);
            }

            if (IsCurrentUserInRole("NotLimitedToHierarchy") == false && CurrentUser != null)
            {
                var parents = this.MergedTeamHierarchyWithLabels.Where(b => b.UserId == CurrentUser.UserId &&
                                    (b.OpenSecurityThroughLabels == true || b.OpenSecurityThroughLabels == false && b.PrimaryRecord == 1));

                // we want all buckets that are in the hierarchies this user is in and who are bounded by the buckets this user is in
                IQueryable<tblHierarchyBucket> children = this.tblHierarchyBuckets.Where(b => (from p in parents where b.LeftIndex > p.LeftIndex && b.RightIndex < p.RightIndex select p.HierarchyId).Contains(b.HierarchyId));

                if (sc == QueryScope.IncludeCurrentUser)
                {
                    children = this.tblHierarchyBuckets.Where(b => (from p in parents where b.LeftIndex >= p.LeftIndex && b.RightIndex <= p.RightIndex select p.HierarchyId).Contains(b.HierarchyId));
                }
                else if (sc == QueryScope.OnlyCurrentUser)
                {
                    children = this.tblHierarchyBuckets.Where(b => b.UserId == CurrentUser.UserId);
                }

                var bucketIds = (from b in children select b.Id);

                result = result.Where(m => m.HierarchyBucketId.HasValue == true && bucketIds.Contains(m.HierarchyBucketId.Value));
            }
            else if (CurrentUser != null)
            {
                if (sc == QueryScope.OnlyCurrentUser)
                {
                    result = result.Where(t => t.OwnerId == CurrentUser.UserId);
                }
                else if (sc == QueryScope.Normal)
                {
                    result = result.Where(t => t.OwnerId != CurrentUser.UserId);
                }
            }

            return result;
        }

        public virtual IQueryable<IncompleteTask> IncompleteTasksEnforceSecurity
        {
            get
            {
                return this.IncompleteTasksEnforceSecurityAndScope(null);
            }
        }

        public virtual IQueryable<Membership> MembershipsEnforceSecurity
        {
            get
            {
                return MembershipsEnforceSecurityWithParams(false);
            }
        }

        public virtual IQueryable<Membership> MembershipsEnforceSecurityWithParams(bool excludeOutsideHierarchies, bool includeCurrentUser = false, bool attemptHierarchySideStep = false)
        {
            var result = (IQueryable<Membership>)this.Memberships;

            // we do this as the queries seem to get executed on a different thread, at least in web api,
            // so force the current user in the calling thread to be cached
            LoadCurrentUser();

            if (IsCurrentUserInRole("NotLimitedToCompany") == false && CurrentUser != null)
            {
                if (CurrentUser.CompanyId.HasValue == false)
                {
                    // we can't be limited to a company and not have one, get out of here
                    throw new System.Security.SecurityException("User is limited to a company but doesn't have a company defined");
                }

                result = result.Where(m => m.CompanyId == CurrentUser.CompanyId);
            }

            bool excludeHierarchy = IsCurrentUserInRole("NotLimitedToHierarchy") == true;
            excludeHierarchy = excludeHierarchy || IsCurrentUserInRole("CanSideStepHierarchy") && attemptHierarchySideStep == true;


            if (excludeHierarchy == false && CurrentUser != null)
            {
                // so any records where either security is open through labels, or if not then only the primary record is used (ie the userid in tblHierarchyBucket)
                var parents = this.MergedTeamHierarchyWithLabels.Where(b => b.UserId == CurrentUser.UserId && 
                                    (b.OpenSecurityThroughLabels == true || b.OpenSecurityThroughLabels == false && b.PrimaryRecord == 1));

                // we want all buckets that are in the hierarchies this user is in and who are bounded by the buckets this user is in
                var children = this.MergedTeamHierarchyWithLabels.Where(b => (from p in parents where b.LeftIndex > p.LeftIndex && b.RightIndex < p.RightIndex select p.HierarchyId).Contains(b.HierarchyId));
                if (includeCurrentUser)
                {
                    children = this.MergedTeamHierarchyWithLabels.Where(b => (from p in parents where b.LeftIndex >= p.LeftIndex && b.RightIndex <= p.RightIndex select p.HierarchyId).Contains(b.HierarchyId));
                }

                var userIds = (from b in children where b.UserId.HasValue select b.UserId.Value);

                var allUsersInHierachies = from b in this.MergedTeamHierarchyWithLabels where b.UserId.HasValue select b.UserId.Value;

                if(excludeOutsideHierarchies == false)
                {
                    // we return either users under the current user or any users that aren't in any hierarchy (this means sites don't need adding into hierarchies)
                    result = result.Where(m => userIds.Contains(m.UserId) || allUsersInHierachies.Contains(m.UserId) == false);
                }
                else
                {
                    // only users under the current user
                    result = result.Where(m => userIds.Contains(m.UserId));
                }
            }
            else
            {
                if(excludeOutsideHierarchies == true)
                {
                    var allUsersInHierachies = from b in this.MergedTeamHierarchyWithLabels
                                               where 
                                               b.UserId.HasValue &&
                                               (b.OpenSecurityThroughLabels == true || b.OpenSecurityThroughLabels == false && b.PrimaryRecord == 1)
                                               select b.UserId.Value;
                    result = result.Where(m => allUsersInHierachies.Contains(m.UserId) == true);
                }
            }

            return result;
        }

        public virtual IQueryable<MembershipContext> MembershipContextsEnforceSecurityWithParams(bool excludeOutsideHierarchies, bool includeCurrentUser = false)
        {
            var result = (IQueryable<MembershipContext>)this.MembershipContexts;

            // we do this as the queries seem to get executed on a different thread, at least in web api,
            // so force the current user in the calling thread to be cached
            LoadCurrentUser();

            if (IsCurrentUserInRole("NotLimitedToCompany") == false && CurrentUser != null)
            {
                if (CurrentUser.CompanyId.HasValue == false)
                {
                    // we can't be limited to a company and not have one, get out of here
                    throw new System.Security.SecurityException("User is limited to a company but doesn't have a company defined");
                }

                result = result.Where(m => m.CompanyId == CurrentUser.CompanyId);
            }

            if (IsCurrentUserInRole("NotLimitedToHierarchy") == false && CurrentUser != null)
            {
                var parents = this.MergedTeamHierarchyWithLabels.Where(b => b.UserId == CurrentUser.UserId &&
                                    (b.OpenSecurityThroughLabels == true || b.OpenSecurityThroughLabels == false && b.PrimaryRecord == 1));

                // we want all buckets that are in the hierarchies this user is in and who are bounded by the buckets this user is in
                var children = this.MergedTeamHierarchyWithLabels.Where(b => (from p in parents where b.LeftIndex > p.LeftIndex && b.RightIndex < p.RightIndex select p.HierarchyId).Contains(b.HierarchyId));
                if (includeCurrentUser)
                {
                    children = this.MergedTeamHierarchyWithLabels.Where(b => (from p in parents where b.LeftIndex >= p.LeftIndex && b.RightIndex <= p.RightIndex select p.HierarchyId).Contains(b.HierarchyId));
                }

                var userIds = (from b in children where b.UserId.HasValue select b.UserId.Value);

                var allUsersInHierachies = from b in this.MergedTeamHierarchyWithLabels where b.UserId.HasValue select b.UserId.Value;

                if (excludeOutsideHierarchies == false)
                {
                    // we return either users under the current user or any users that aren't in any hierarchy (this means sites don't need adding into hierarchies)
                    result = result.Where(m => userIds.Contains(m.UserId) || allUsersInHierachies.Contains(m.UserId) == false);
                }
                else
                {
                    // only users under the current user
                    result = result.Where(m => userIds.Contains(m.UserId));
                }
            }
            else
            {
                if (excludeOutsideHierarchies == true)
                {
                    var allUsersInHierachies = from b in this.MergedTeamHierarchyWithLabels where 
                                               b.UserId.HasValue &&
                                               (b.OpenSecurityThroughLabels == true || b.OpenSecurityThroughLabels == false && b.PrimaryRecord == 1)
                                               select b.UserId.Value;
                    result = result.Where(m => allUsersInHierachies.Contains(m.UserId) == true);
                }
            }

            return result;
        }

        public virtual IQueryable<MembershipMedia> MembershipMediasEnforceSecurity
        {
            get
            {
                var result = (IQueryable<MembershipMedia>)this.MembershipMedias;

                // we do this as the queries seem to get executed on a different thread, at least in web api,
                // so force the current user in the calling thread to be cached
                LoadCurrentUser();

                if (IsCurrentUserInRole("NotLimitedToCompany") == false && CurrentUser != null)
                {
                    if (CurrentUser.CompanyId.HasValue == false)
                    {
                        // we can't be limited to a company and not have one, get out of here
                        throw new System.Security.SecurityException("User is limited to a company but doesn't have a company defined");
                    }

                    result = result.Where(m => m.CompanyId == CurrentUser.CompanyId);
                }

                if (IsCurrentUserInRole("NotLimitedToHierarchy") == false && CurrentUser != null)
                {
                    var parents = this.MergedTeamHierarchyWithLabels.Where(b => b.UserId == CurrentUser.UserId &&
                                        (b.OpenSecurityThroughLabels == true || b.OpenSecurityThroughLabels == false && b.PrimaryRecord == 1));

                    // we want all buckets that are in the hierarchies this user is in and who are bounded by the buckets this user is in
                    var children = this.MergedTeamHierarchyWithLabels.Where(b => (from p in parents where b.LeftIndex > p.LeftIndex && b.RightIndex < p.RightIndex select p.HierarchyId).Contains(b.HierarchyId));

                    var userIds = (from b in children where b.UserId.HasValue select b.UserId.Value);

                    var allUsersInHierachies = from b in this.MergedTeamHierarchyWithLabels where 
                                               b.UserId.HasValue &&
                                               (b.OpenSecurityThroughLabels == true || b.OpenSecurityThroughLabels == false && b.PrimaryRecord == 1)
                                               select b.UserId.Value;

                    // we return either users under the current user or any users that aren't in any hierarchy (this means sites don't need adding into hierarchies)
                    result = result.Where(m => userIds.Contains(m.UserId) || allUsersInHierachies.Contains(m.UserId) == false);
                }

                return result;
            }
        }

        public virtual IQueryable<Checklist> ChecklistsEnforceSecurityAndScope(QueryScope? scope)
        {
            QueryScope sc = QueryScope.Normal;

            if (scope.HasValue == true)
            {
                sc = scope.Value;
            }

            var result = (IQueryable<Checklist>)this.Checklists;

            if (IsCurrentUserInRole("NotLimitedToCompany") == false && CurrentUser != null)
            {
                if (CurrentUser.CompanyId.HasValue == false)
                {
                    // we can't be limited to a company and not have one, get out of here
                    throw new System.Security.SecurityException("User is limited to a company but doesn't have a company defined");
                }

                result = result.Where(m => m.ReviewerCompanyId == CurrentUser.CompanyId);
            }

            if (IsCurrentUserInRole("NotLimitedToHierarchy") == false && CurrentUser != null)
            {
                var parents = this.MergedTeamHierarchyWithLabels.Where(b => b.UserId == CurrentUser.UserId &&
                                    (b.OpenSecurityThroughLabels == true || b.OpenSecurityThroughLabels == false && b.PrimaryRecord == 1));

                // we want all buckets that are in the hierarchies this user is in and who are bounded by the buckets this user is in
                IQueryable<tblHierarchyBucket> children = this.tblHierarchyBuckets.Where(b => (from p in parents where b.LeftIndex > p.LeftIndex && b.RightIndex < p.RightIndex select p.HierarchyId).Contains(b.HierarchyId));

                if (sc == QueryScope.IncludeCurrentUser)
                {
                    children = this.tblHierarchyBuckets.Where(b => (from p in parents where b.LeftIndex >= p.LeftIndex && b.RightIndex <= p.RightIndex select p.HierarchyId).Contains(b.HierarchyId));
                }
                else if (sc == QueryScope.OnlyCurrentUser)
                {
                    children = this.tblHierarchyBuckets.Where(b => b.UserId == CurrentUser.UserId);
                }

                var bucketIds = (from b in children select b.Id);

                result = result.Where(m => m.HierarchyBucketId.HasValue == true && bucketIds.Contains(m.HierarchyBucketId.Value));
            }
            else if (CurrentUser != null)
            {
                if (sc == QueryScope.OnlyCurrentUser)
                {
                    result = result.Where(t => t.ReviewerId == CurrentUser.UserId);
                }
                else if (sc == QueryScope.Normal)
                {
                    result = result.Where(t => t.ReviewerId != CurrentUser.UserId);
                }
            }

            return result;
        }

        public virtual IQueryable<Checklist> ChecklistsEnforceSecurity
        {
            get
            {
                return this.ChecklistsEnforceSecurityAndScope(null);
            }
        }

        public virtual IQueryable<WorkingDocument> WorkingDocumentsEnforceSecurityAndScope(QueryScope? scope)
        {
            QueryScope sc = QueryScope.Normal;

            if (scope.HasValue == true)
            {
                sc = scope.Value;
            }

            var result = (IQueryable<WorkingDocument>)this.WorkingDocuments;

            if (IsCurrentUserInRole("NotLimitedToCompany") == false && CurrentUser != null)
            {
                if (CurrentUser.CompanyId.HasValue == false)
                {
                    // we can't be limited to a company and not have one, get out of here
                    throw new System.Security.SecurityException("User is limited to a company but doesn't have a company defined");
                }

                result = result.Where(m => m.ReviewerCompanyId == CurrentUser.CompanyId);
            }

            if (IsCurrentUserInRole("NotLimitedToHierarchy") == false && CurrentUser != null)
            {
                var parents = this.MergedTeamHierarchyWithLabels.Where(b => b.UserId == CurrentUser.UserId &&
                                    (b.OpenSecurityThroughLabels == true || b.OpenSecurityThroughLabels == false && b.PrimaryRecord == 1));

                // we want all buckets that are in the hierarchies this user is in and who are bounded by the buckets this user is in
                IQueryable<tblHierarchyBucket> children = this.tblHierarchyBuckets.Where(b => (from p in parents where b.LeftIndex > p.LeftIndex && b.RightIndex < p.RightIndex select p.HierarchyId).Contains(b.HierarchyId));

                if (sc == QueryScope.IncludeCurrentUser)
                {
                    children = this.tblHierarchyBuckets.Where(b => (from p in parents where b.LeftIndex >= p.LeftIndex && b.RightIndex <= p.RightIndex select p.HierarchyId).Contains(b.HierarchyId));
                }
                else if (sc == QueryScope.OnlyCurrentUser)
                {
                    children = this.tblHierarchyBuckets.Where(b => b.UserId == CurrentUser.UserId);
                }

                var bucketIds = (from b in children select b.Id);

                result = result.Where(m => m.HierarchyBucketId.HasValue == true && bucketIds.Contains(m.HierarchyBucketId.Value));
            }
            else if (CurrentUser != null)
            {
                if (sc == QueryScope.OnlyCurrentUser)
                {
                    result = result.Where(t => t.ReviewerId == CurrentUser.UserId);
                }
                else if (sc == QueryScope.Normal)
                {
                    result = result.Where(t => t.ReviewerId != CurrentUser.UserId);
                }
            }

            return result;
        }

        public virtual IQueryable<WorkingDocument> WorkingDocumentsEnforceSecurity
        {
            get
            {
                return this.WorkingDocumentsEnforceSecurityAndScope(null);
            }
        }

        public IQueryable<ChecklistResult> ChecklistResultsEnforceSecurityAndScope(QueryScope? scope)
        {
            QueryScope sc = QueryScope.Normal;

            if (scope.HasValue == true)
            {
                sc = scope.Value;
            }

            var result = (IQueryable<ChecklistResult>)this.ChecklistResults;

            if (IsCurrentUserInRole("NotLimitedToCompany") == false && CurrentUser != null)
            {
                if (CurrentUser.CompanyId.HasValue == false)
                {
                    // we can't be limited to a company and not have one, get out of here
                    throw new System.Security.SecurityException("User is limited to a company but doesn't have a company defined");
                }

                result = result.Where(m => m.ReviewerCompanyId == CurrentUser.CompanyId);
            }

            if (IsCurrentUserInRole("NotLimitedToHierarchy") == false && CurrentUser != null)
            {
                var parents = this.MergedTeamHierarchyWithLabels.Where(b => b.UserId == CurrentUser.UserId &&
                                    (b.OpenSecurityThroughLabels == true || b.OpenSecurityThroughLabels == false && b.PrimaryRecord == 1));

                // we want all buckets that are in the hierarchies this user is in and who are bounded by the buckets this user is in
                IQueryable<tblHierarchyBucket> children = this.tblHierarchyBuckets.Where(b => (from p in parents where b.LeftIndex > p.LeftIndex && b.RightIndex < p.RightIndex select p.HierarchyId).Contains(b.HierarchyId));

                if (sc == QueryScope.IncludeCurrentUser)
                {
                    children = this.tblHierarchyBuckets.Where(b => (from p in parents where b.LeftIndex >= p.LeftIndex && b.RightIndex <= p.RightIndex select p.HierarchyId).Contains(b.HierarchyId));
                }
                else if (sc == QueryScope.OnlyCurrentUser)
                {
                    children = this.tblHierarchyBuckets.Where(b => b.UserId == CurrentUser.UserId);
                }

                var bucketIds = (from b in children select b.Id);

                result = result.Where(m => m.HierarchyBucketId.HasValue == true && bucketIds.Contains(m.HierarchyBucketId.Value));
            }
            else if (CurrentUser != null)
            {
                if (sc == QueryScope.OnlyCurrentUser)
                {
                    result = result.Where(t => t.ReviewerId == CurrentUser.UserId);
                }
                else if (sc == QueryScope.Normal)
                {
                    result = result.Where(t => t.ReviewerId != CurrentUser.UserId);
                }
            }

            return result;
        }

        public IQueryable<ChecklistResult> ChecklistResultsEnforceSecurity
        {
            get
            {
                return this.ChecklistResultsEnforceSecurityAndScope(null);
            }
        }

        public IQueryable<ChecklistMediaStream> ChecklistMediaStreamsEnforceSecurityAndScope(QueryScope? scope)
        {
            QueryScope sc = QueryScope.Normal;

            if (scope.HasValue == true)
            {
                sc = scope.Value;
            }

            var result = (IQueryable<ChecklistMediaStream>)this.ChecklistMediaStreams;

            if (IsCurrentUserInRole("NotLimitedToCompany") == false && CurrentUser != null)
            {
                if (CurrentUser.CompanyId.HasValue == false)
                {
                    // we can't be limited to a company and not have one, get out of here
                    throw new System.Security.SecurityException("User is limited to a company but doesn't have a company defined");
                }

                result = result.Where(m => m.ReviewerCompanyId == CurrentUser.CompanyId);
            }

            if (IsCurrentUserInRole("NotLimitedToHierarchy") == false && CurrentUser != null)
            {
                var parents = this.MergedTeamHierarchyWithLabels.Where(b => b.UserId == CurrentUser.UserId &&
                                    (b.OpenSecurityThroughLabels == true || b.OpenSecurityThroughLabels == false && b.PrimaryRecord == 1));

                // we want all buckets that are in the hierarchies this user is in and who are bounded by the buckets this user is in
                IQueryable<tblHierarchyBucket> children = this.tblHierarchyBuckets.Where(b => (from p in parents where b.LeftIndex > p.LeftIndex && b.RightIndex < p.RightIndex select p.HierarchyId).Contains(b.HierarchyId));

                if (sc == QueryScope.IncludeCurrentUser)
                {
                    children = this.tblHierarchyBuckets.Where(b => (from p in parents where b.LeftIndex >= p.LeftIndex && b.RightIndex <= p.RightIndex select p.HierarchyId).Contains(b.HierarchyId));
                }
                else if (sc == QueryScope.OnlyCurrentUser)
                {
                    children = this.tblHierarchyBuckets.Where(b => b.UserId == CurrentUser.UserId);
                }

                var bucketIds = (from b in children select b.Id);

                result = result.Where(m => m.HierarchyBucketId.HasValue == true && bucketIds.Contains(m.HierarchyBucketId.Value));
            }
            else if (CurrentUser != null)
            {
                if (sc == QueryScope.OnlyCurrentUser)
                {
                    result = result.Where(t => t.ReviewerId == CurrentUser.UserId);
                }
                else if (sc == QueryScope.Normal)
                {
                    result = result.Where(t => t.ReviewerId != CurrentUser.UserId);
                }
            }

            return result;
        }

        public IQueryable<TaskMediaStream> TaskMediaStreamsEnforceSecurityAndScope(QueryScope? scope)
        {
            QueryScope sc = QueryScope.Normal;

            if (scope.HasValue == true)
            {
                sc = scope.Value;
            }

            var result = (IQueryable<TaskMediaStream>)this.TaskMediaStreams;

            if (IsCurrentUserInRole("NotLimitedToCompany") == false && CurrentUser != null)
            {
                if (CurrentUser.CompanyId.HasValue == false)
                {
                    // we can't be limited to a company and not have one, get out of here
                    throw new System.Security.SecurityException("User is limited to a company but doesn't have a company defined");
                }

                result = result.Where(m => m.OwnerCompanyId == CurrentUser.CompanyId);
            }

            if (IsCurrentUserInRole("NotLimitedToHierarchy") == false && CurrentUser != null)
            {
                var parents = this.MergedTeamHierarchyWithLabels.Where(b => b.UserId == CurrentUser.UserId &&
                                    (b.OpenSecurityThroughLabels == true || b.OpenSecurityThroughLabels == false && b.PrimaryRecord == 1));

                // we want all buckets that are in the hierarchies this user is in and who are bounded by the buckets this user is in
                IQueryable<tblHierarchyBucket> children = this.tblHierarchyBuckets.Where(b => (from p in parents where b.LeftIndex > p.LeftIndex && b.RightIndex < p.RightIndex select p.HierarchyId).Contains(b.HierarchyId));

                if (sc == QueryScope.IncludeCurrentUser)
                {
                    children = this.tblHierarchyBuckets.Where(b => (from p in parents where b.LeftIndex >= p.LeftIndex && b.RightIndex <= p.RightIndex select p.HierarchyId).Contains(b.HierarchyId));
                }
                else if (sc == QueryScope.OnlyCurrentUser)
                {
                    children = this.tblHierarchyBuckets.Where(b => b.UserId == CurrentUser.UserId);
                }

                var bucketIds = (from b in children select b.Id);

                result = result.Where(m => m.HierarchyBucketId.HasValue == true && bucketIds.Contains(m.HierarchyBucketId.Value));
            }
            else if (CurrentUser != null)
            {
                if (sc == QueryScope.OnlyCurrentUser)
                {
                    result = result.Where(t => t.OwnerId == CurrentUser.UserId);
                }
                else if (sc == QueryScope.Normal)
                {
                    result = result.Where(t => t.OwnerId != CurrentUser.UserId);
                }
            }

            return result;
        }

        public IQueryable<ChecklistMediaStream> ChecklistMediaStreamsEnforceSecurity
        {
            get
            {
                return this.ChecklistMediaStreamsEnforceSecurityAndScope(null);
            }
        }

        public IQueryable<TaskMediaStream> TaskMediaStreamsEnforceSecurity
        {
            get
            {
                return this.TaskMediaStreamsEnforceSecurityAndScope(null);
            }
        }

        public IQueryable<AssetType> AssetTypesEnforeSecurity
        {
            get
            {
                return this.AssetTypes;
            }
        }

        public IQueryable<TimesheetItemType> TimesheetItemTypesEnforeSecurity
        {
            get
            {
                return this.TimesheetItemTypes;
            }
        }

        public IQueryable<LeaveType> LeaveTypesEnforeSecurity
        {
            get
            {
                return this.LeaveTypes;
            }
        }

        public IQueryable<Leave> LeaveEnforeSecurity
        {
            get
            {
                return this.LeaveEnforceSecurityAndScope(null);
            }
        }

        public IQueryable<Leave> LeaveEnforceSecurityAndScope(QueryScope? scope)
        {
            QueryScope sc = QueryScope.Normal;

            if (scope.HasValue == true)
            {
                sc = scope.Value;
            }

            var result = (IQueryable<Leave>)this.Leaves;

            if (IsCurrentUserInRole("NotLimitedToCompany") == false && CurrentUser != null)
            {
                if (CurrentUser.CompanyId.HasValue == false)
                {
                    // we can't be limited to a company and not have one, get out of here
                    throw new System.Security.SecurityException("User is limited to a company but doesn't have a company defined");
                }

                result = result.Where(m => m.OriginalUserCompanyId == CurrentUser.CompanyId);
            }

            if (IsCurrentUserInRole("NotLimitedToHierarchy") == false && CurrentUser != null)
            {
                var parents = this.MergedTeamHierarchyWithLabels.Where(b => b.UserId == CurrentUser.UserId &&
                                    (b.OpenSecurityThroughLabels == true || b.OpenSecurityThroughLabels == false && b.PrimaryRecord == 1));

                // we want all buckets that are in the hierarchies this user is in and who are bounded by the buckets this user is in
                IQueryable<tblHierarchyBucket> children = this.tblHierarchyBuckets.Where(b => (from p in parents where b.LeftIndex > p.LeftIndex && b.RightIndex < p.RightIndex select p.HierarchyId).Contains(b.HierarchyId));

                if (sc == QueryScope.IncludeCurrentUser)
                {
                    children = this.tblHierarchyBuckets.Where(b => (from p in parents where b.LeftIndex >= p.LeftIndex && b.RightIndex <= p.RightIndex select p.HierarchyId).Contains(b.HierarchyId));
                }
                else if (sc == QueryScope.OnlyCurrentUser)
                {
                    children = this.tblHierarchyBuckets.Where(b => b.UserId == CurrentUser.UserId);
                }

                var bucketIds = (from b in children select b.Id);

                result = result.Where(m => bucketIds.Contains(m.HierarchyBucketId));
            }
            else if (CurrentUser != null)
            {
                if (sc == QueryScope.OnlyCurrentUser)
                {
                    result = result.Where(t => t.OriginalUserId == CurrentUser.UserId);
                }
                else if (sc == QueryScope.Normal)
                {
                    result = result.Where(t => t.OriginalUserId != CurrentUser.UserId);
                }
            }

            return result;
        }

        public IQueryable<tblHierarchy> CurrentUserHierarchies
        {
            get
            {
                if (IsCurrentUserInRole("NotLimitedToHierarchy") == false && CurrentUser != null)
                {
                    return from hb in tblHierarchyBuckets where hb.UserId == CurrentUser.UserId select hb.tblHierarchy;
                }
                else
                {
                    return from h in tblHierarchies select h;
                }
            }
        }

        public IQueryable<tblHierarchyBucket> CurrentUserHierarchyBuckets
        {
            get
            {
                if (IsCurrentUserInRole("NotLimitedToHierarchy") == false && CurrentUser != null)
                {
                    return from hb in tblHierarchyBuckets where hb.UserId == CurrentUser.UserId select hb;
                }
                else
                {
                    return from hb in tblHierarchyBuckets select hb;
                }
            }
        }

        public IQueryable<Asset> AssetsEnforceSecurity
        {
            get
            {
                return AssetsEnforceSecurityWithParams();
            }
        }

        public virtual IQueryable<Asset> AssetsEnforceSecurityWithParams()
        {
            var result = (IQueryable<Asset>)this.Assets;

            // we do this as the queries seem to get executed on a different thread, at least in web api,
            // so force the current user in the calling thread to be cached
            LoadCurrentUser();

            if (IsCurrentUserInRole("NotLimitedToCompany") == false && CurrentUser != null)
            {
                if (CurrentUser.CompanyId.HasValue == false)
                {
                    // we can't be limited to a company and not have one, get out of here
                    throw new System.Security.SecurityException("User is limited to a company but doesn't have a company defined");
                }

                result = result.Where(m => m.CompanyId == CurrentUser.CompanyId);
            }

            return result;
        }

        public IQueryable<AssetLastKnownLocation> AssetLastKnownLocationEnforceSecurity
        {
            get
            {
                return this.AssetLastKnownLocations;
            }
        }

        public IQueryable<UserTaskSummary_Result> UserTaskSummaryEnforceSecurity(DateTime? startDate, DateTime? endDate)
        {
            IQueryable<UserTaskSummary_Result> result = null;

            if (IsCurrentUserInRole("NotLimitedToHierarchy") == false && CurrentUser != null)
            {
                var parents = this.MergedTeamHierarchyWithLabels.Where(b => b.UserId == CurrentUser.UserId &&
                                    (b.OpenSecurityThroughLabels == true || b.OpenSecurityThroughLabels == false && b.PrimaryRecord == 1));

                List<UserTaskSummary_Result> building = new List<UserTaskSummary_Result>();

                foreach (var parent in parents)
                {
                    var hResult = this.UserTaskSummary(startDate, endDate, parent.HierarchyId, parent.LeftIndex, parent.RightIndex, true).ToList();
                    building.AddRange(hResult);
                }

                result = building.AsQueryable<UserTaskSummary_Result>();
            }
            else
            {
                result = this.UserTaskSummary(startDate, endDate, null, null, null, false).ToList().AsQueryable<UserTaskSummary_Result>();
            }

            if (IsCurrentUserInRole("NotLimitedToCompany") == false && CurrentUser != null)
            {
                if (CurrentUser.CompanyId.HasValue == false)
                {
                    // we can't be limited to a company and not have one, get out of here
                    throw new System.Security.SecurityException("User is limited to a company but doesn't have a company defined");
                }

                result = result.Where(m => m.CompanyId == CurrentUser.CompanyId);
            }

            // we do this here as the object materialize event doesn't seem to be fired
            // with the stored proc calls :(
            foreach(var r in result)
            {
                ConvertToUserLocal(r);
            }

            return result;
        }

        public IQueryable<UserTaskRosterSummary_Result> UserTaskRosterSummaryEnforceSecurity(Nullable<System.DateTime> startDate, Nullable<System.DateTime> endDate, Nullable<int> hierarchyId, Nullable<int> leftIndex, Nullable<int> rightIndex, QueryScope? scope)
        {
            QueryScope sc = QueryScope.Normal;

            if (scope.HasValue == true)
            {
                sc = scope.Value;
            }

            if(sc == QueryScope.IncludeCurrentUser && leftIndex.HasValue && rightIndex.HasValue)
            {
                leftIndex--;
                rightIndex++;
            }

            var result = this.UserTaskRosterSummary(startDate, endDate, hierarchyId, leftIndex, rightIndex);

            var ret = result.ToList();

            // we do this here as the object materialize event doesn't seem to be fired
            // with the stored proc calls :(
            foreach (var r in ret)
            {
                ConvertToUserLocal(r);
            }

            return ret.AsQueryable<UserTaskRosterSummary_Result>();
        }

        public virtual IQueryable<ErrorLogDetail> ErrorLogDetailsEnforceSecurity
        {
            get
            {
                var result = (IQueryable<ErrorLogDetail>)this.ErrorLogDetails;

                // we do this as the queries seem to get executed on a different thread, at least in web api,
                // so force the current user in the calling thread to be cached
                LoadCurrentUser();

                if (IsCurrentUserInRole("Developer") == false)
                {
                    // we can't be limited to a company and not have one, get out of here
                    throw new System.Security.SecurityException("User does not have rights to see this data");
                }

                return result;
            }
        }

        public virtual IQueryable<ErrorLogSummary> ErrorLogSummariesEnforceSecurity
        {
            get
            {
                var result = (IQueryable<ErrorLogSummary>)this.ErrorLogSummaries;

                // we do this as the queries seem to get executed on a different thread, at least in web api,
                // so force the current user in the calling thread to be cached
                LoadCurrentUser();

                if (IsCurrentUserInRole("Developer") == false)
                {
                    // we can't be limited to a company and not have one, get out of here
                    throw new System.Security.SecurityException("User does not have rights to see this data");
                }

                return result;
            }
        }

        public virtual IQueryable<OutstandingTask> OutstandingTasksEnforceSecurity
        {
            get
            {
                var result = (IQueryable<OutstandingTask>)this.OutstandingTasks;

                // we do this as the queries seem to get executed on a different thread, at least in web api,
                // so force the current user in the calling thread to be cached
                LoadCurrentUser();

                if (IsCurrentUserInRole("NotLimitedToHierarchy") == false && CurrentUser != null)
                {
                    var parents = this.MergedTeamHierarchyWithLabels.Where(b => b.UserId == CurrentUser.UserId &&
                                        (b.OpenSecurityThroughLabels == true || b.OpenSecurityThroughLabels == false && b.PrimaryRecord == 1));

                    // we want all buckets that are in the hierarchies this user is in and who are bounded by the buckets this user is in
                    var children = this.tblHierarchyBuckets.Where(b => (from p in parents where b.LeftIndex > p.LeftIndex && b.RightIndex < p.RightIndex select p.HierarchyId).Contains(b.HierarchyId));

                    var bucketIds = (from b in children select b.Id);

                    result = result.Where(m => bucketIds.Contains(m.HierarchyBucketId));
                }

                return result;
            }
        }

        public virtual IQueryable<ESProbe> ESProbesEnforceSecurityAndScope(QueryScope? scope)
        {
            var result = (IQueryable<ESProbe>)this.ESProbes;
            // we do this as the queries seem to get executed on a different thread, at least in web api,
            // so force the current user in the calling thread to be cached
            LoadCurrentUser();

            if (IsCurrentUserInRole("NotLimitedToCompany") == false && CurrentUser != null)
            {
                if (CurrentUser.CompanyId.HasValue == false)
                {
                    // we can't be limited to a company and not have one, get out of here
                    throw new System.Security.SecurityException("User is limited to a company but doesn't have a company defined");
                }

                result = result.Where(m => m.CompanyId == CurrentUser.CompanyId);
            }

            return result;
        }

        public virtual IQueryable<ESSensor> ESSensorsEnforceSecurityAndScope(QueryScope? scope)
        {
            LoadCurrentUser();
            return this.ESSensors;
        }

        public virtual IQueryable<ESSensorReading> ESSensorReadingsEnforceSecurityAndScope(QueryScope? scope)
        {
            LoadCurrentUser();
            return this.ESSensorReadings;
        }



        public void LoadCurrentUser()
        {
            if (_hasLoadedUser == false)
            {
                string userName = Thread.CurrentPrincipal.Identity.Name;
                if (userName == "")
                {
                    userName = System.Configuration.ConfigurationManager.AppSettings["ConceptCave.Checklist.Reporting.DefaultUserName"] ?? "";
                }

                var data = (from m in this.Memberships where m.UserName == userName select m);

                if (data.Count() > 0)
                {
                    _currentUser = data.First();
                }

                _hasLoadedUser = true;
            }

            if(_currentUser == null && System.Configuration.ConfigurationManager.AppSettings["ConceptCave.Checklist.Reporting.Data.NoUser"] == null)
            {
                throw new InvalidOperationException("A current user is required");
            }
        }

        public bool IsCurrentUserInRole(string role)
        {
            string testingRole = System.Configuration.ConfigurationManager.AppSettings["ConceptCave.Checklist.Reporting.Role." + role];
            if (testingRole != null)
                return bool.Parse(testingRole);

            return Thread.CurrentPrincipal.IsInRole(role);
        }

        protected virtual void LoadCurrentDefaults()
        {
            if(_hasLoadedDefaults == false)
            {
                var settings = (from g in this.tblGlobalSettings where g.Name == DefaultUnitsForDistancesSetting select g);

                if(settings.Count() > 0)
                {
                    _defaultUnitsForDistances = settings.First();
                }

                _hasLoadedDefaults = true;
            }
        }

        public virtual Membership CurrentUser
        {
            get
            {
                LoadCurrentUser();

                return _currentUser;
            }
        }

        public virtual tblGlobalSetting DefaultUnitsForDistances
        {
            get
            {
                LoadCurrentDefaults();

                return _defaultUnitsForDistances;
            }
        }

        public virtual bool ConvertDistancesToFeet
        {
            get
            {
                var d = DefaultUnitsForDistances;

                if(d == null)
                {
                    return false; // default to M if no setting
                }

                if(d.Value.ToLower() == "imperial")
                {
                    return true;
                }

                return false;
            }
        }

        protected TimeZoneInfo CurrentTimeZone
        {
            get
            {
                if (_currentTimezone == null && CurrentUser != null)
                {
                    _currentTimezone = TimeZoneInfo.FindSystemTimeZoneById(CurrentUser.TimeZone);
                }

                return _currentTimezone;
            }
        }

        protected virtual DateTime ConvertToUserLocal(DateTime date)
        {
            return TimeZoneInfo.ConvertTimeFromUtc(DateTime.SpecifyKind(date, DateTimeKind.Utc),
                    CurrentTimeZone);
        }

        public virtual DateTime ConvertToUTC(DateTime date)
        {
            return TimeZoneInfo.ConvertTimeToUtc(DateTime.SpecifyKind(date, DateTimeKind.Unspecified), CurrentTimeZone);
        }

        /// <summary>
        /// We hook into this event as it gives us a chance to change things after the entity has been populated with data from the database.
        /// We take this opportunity to change the date/time properties from UTC to the local user's timezone
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ObjectContext_ObjectMaterialized(object sender, System.Data.Entity.Core.Objects.ObjectMaterializedEventArgs e)
        {
            // we do things in 2 parts. First any none date stuff that doesn't depend on the user/current timezone
            if(e.Entity is Task)
            {
                ProcessFields((Task)e.Entity);
            }

            if(CurrentTimeZone == null)
            {
                // nothing we can do, so just go with default UTC times
                return;
            }

            // then the stuff that does depend on the timezone

            if(e.Entity is Task)
            {
                ConvertToUserLocal((Task)e.Entity);
            }
            else if(e.Entity is TimeBetweenTask)
            {
                ConvertToUserLocal((TimeBetweenTask)e.Entity);
            }
            else if(e.Entity is TaskWorklog)
            {
                ConvertToUserLocal((TaskWorklog)e.Entity);
            }
            else if(e.Entity is TaskMediaStream)
            {
                ConvertToUserLocal((TaskMediaStream)e.Entity);
            }
            else if (e.Entity is IncompleteTask)
            {
                ConvertToUserLocal((IncompleteTask)e.Entity);
            }
            else if (e.Entity is Schedule)
            {
                ReshapeRecord((Schedule)e.Entity);
            }
            else if(e.Entity is Leave)
            {
                ConvertToUserLocal((Leave)e.Entity);
            }
            else if(e.Entity is Checklist)
            {
                ConvertToUserLocal((Checklist)e.Entity);
            }
            else if(e.Entity is ChecklistResult)
            {
                ConvertToUserLocal((ChecklistResult)e.Entity);
            }
            else if(e.Entity is ChecklistMediaStream)
            {
                ConvertToUserLocal((ChecklistMediaStream)e.Entity);
            }
            else if (e.Entity is WorkingDocument)
            {
                ConvertToUserLocal((WorkingDocument)e.Entity);
            }
            else if(e.Entity is Media)
            {
                ConvertToUserLocal((Media)e.Entity);
            }
            else if(e.Entity is MediaPageView)
            {
                ConvertToUserLocal((MediaPageView)e.Entity);
            }
            else if(e.Entity is MediaView)
            {
                ConvertToUserLocal((MediaView)e.Entity);
            }
            else if(e.Entity is MembershipMedia)
            {
                ConvertToUserLocal((MembershipMedia)e.Entity);
            }
            else if(e.Entity is tblQRTZHistory)
            {
                ConvertToUserLocal((tblQRTZHistory)e.Entity);
            }
            else if (e.Entity is ErrorLogDetail)
            {
                ConvertToUserLocal((ErrorLogDetail)e.Entity);
            }
            else if (e.Entity is ErrorLogSummary)
            {
                ConvertToUserLocal((ErrorLogSummary)e.Entity);
            }
            else if(e.Entity is Asset)
            {
                ConvertToUserLocal((Asset)e.Entity);
            }
            else if (e.Entity is GpsLocation)
            {
                ConvertToUserLocal((GpsLocation)e.Entity);
            }
            else if (e.Entity is AssetLocationHistory)
            {
                ConvertToUserLocal((AssetLocationHistory)e.Entity);
            }
            else if(e.Entity is Order)
            {
                ConvertToUserLocal((Order)e.Entity);
            }
            else if (e.Entity is OrderItem)
            {
                ConvertToUserLocal((OrderItem)e.Entity);
            }
            else if (e.Entity is Tablet)
            {
                ConvertToUserLocal((Tablet)e.Entity);
            }
            else if (e.Entity is PendingPayment)
            {
                ConvertToUserLocal((PendingPayment)e.Entity);
            }
            else if(e.Entity is PaymentResponse)
            {
                ConvertToUserLocal((PaymentResponse)e.Entity);
            }
            else if (e.Entity is NewsFeed)
            {
                ConvertToUserLocal((NewsFeed)e.Entity);
            }
        }

        private void ConvertToUserLocal(Tablet tablet)
        {
            var tenant = _tenant ?? MultiTenantManager.CurrentTenant;
            if (tenant != null)
            {
                tablet.TenantId = tenant.Id;
                tablet.TenantName = tenant.Name;
            }
        }

        private void ConvertToUserLocal(PendingPayment pending)
        {
            if(pending.ActivatedDateUtc.HasValue)
            {
                pending.ActivatedDateLocal = ConvertToUserLocal(pending.ActivatedDateUtc.Value);
            }
            if (pending.CancelledDateUtc.HasValue)
            {
                pending.CancelledDateLocal = ConvertToUserLocal(pending.CancelledDateUtc.Value);
            }
            if(pending.PaidInFullDateLocal.HasValue)
            {
                pending.PaidInFullDateLocal = ConvertToUserLocal(pending.PaidInFullDateLocal.Value);
            }

            pending.CreatedDateLocal = ConvertToUserLocal(pending.CreatedDateUtc);
            pending.StatusDateLocal = ConvertToUserLocal(pending.StatusDateUtc);

            if (pending.DueDateUtc.HasValue)
            {
                pending.DueDateLocal = ConvertToUserLocal(pending.DueDateUtc.Value);
            }

            if (pending.LastFailedDateUtc.HasValue)
            {
                pending.LastFailedDateLocal = ConvertToUserLocal(pending.LastFailedDateUtc.Value);
            }
        }

        private void ConvertToUserLocal(PaymentResponse resp)
        {
            resp.DateLocal = ConvertToUserLocal(resp.DateUtc);
        }

        private void ConvertToUserLocal(NewsFeed resp)
        {
            resp.DateCreated = ConvertToUserLocal(resp.DateCreated);
            resp.ValidTo = ConvertToUserLocal(resp.ValidTo);
        }

        private void ConvertToUserLocal(AssetLocationHistory assetLocationHistory)
        {
            assetLocationHistory.BestRangeUtc = ConvertToUserLocal(assetLocationHistory.BestRangeUtc);
            assetLocationHistory.FirstRangeUtc = ConvertToUserLocal(assetLocationHistory.FirstRangeUtc);
            if (assetLocationHistory.GoneUtc.HasValue)
            {
                assetLocationHistory.GoneUtc = ConvertToUserLocal(assetLocationHistory.GoneUtc.Value);
            }
            if (assetLocationHistory.NextStaticBeaconRangeUtc.HasValue)
            {
                assetLocationHistory.NextStaticBeaconRangeUtc = ConvertToUserLocal(assetLocationHistory.NextStaticBeaconRangeUtc.Value);
            }
            if (assetLocationHistory.PrevStaticBeaconRangeUtc.HasValue)
            {
                assetLocationHistory.PrevStaticBeaconRangeUtc = ConvertToUserLocal(assetLocationHistory.PrevStaticBeaconRangeUtc.Value);
            }
        }

        private void ConvertToUserLocal(Order order)
        {
            order.DateCreated = ConvertToUserLocal(order.DateCreated);
            if (order.ExpectedDeliveryDate.HasValue)
            {
                order.ExpectedDeliveryDate = ConvertToUserLocal(order.ExpectedDeliveryDate.Value);
            }
            if (order.PendingExpiryDate.HasValue)
            {
                order.PendingExpiryDate = ConvertToUserLocal(order.PendingExpiryDate.Value);
            }
            if (order.CancelledDateLocal.HasValue)
            {
                order.CancelledDateLocal = ConvertToUserLocal(order.CancelledDateLocal.Value);
            }
        }
        private void ConvertToUserLocal(OrderItem orderItem)
        {
            orderItem.DateCreated = ConvertToUserLocal(orderItem.DateCreated);
            if (orderItem.EndDate.HasValue)
            {
                orderItem.EndDate = ConvertToUserLocal(orderItem.EndDate.Value);
            }
            if (orderItem.StartDate.HasValue)
            {
                orderItem.StartDate = ConvertToUserLocal(orderItem.StartDate.Value);
            }
            if (orderItem.PendingExpiryDate.HasValue)
            {
                orderItem.PendingExpiryDate = ConvertToUserLocal(orderItem.PendingExpiryDate.Value);
            }
            if (orderItem.CancelledDateLocal.HasValue)
            {
                orderItem.CancelledDateLocal = ConvertToUserLocal(orderItem.CancelledDateLocal.Value);
            }
        }

        private void ConvertToUserLocal(GpsLocation gpsLocation)
        {
            if (gpsLocation.TimestampUtc.HasValue)
            {
                gpsLocation.TimestampUtc = ConvertToUserLocal(gpsLocation.TimestampUtc.Value);
            }
            if (gpsLocation.LastTimestampUtc.HasValue)
            {
                gpsLocation.LastTimestampUtc = ConvertToUserLocal(gpsLocation.LastTimestampUtc.Value);
            }
        }


        public void ProcessFields(Task task)
        {
            if (task == null)
            {
                return;
            }

            if (this.TaskResultSettings.ExpandStatusText == true && string.IsNullOrEmpty(task.StatusText) == false)
            {
                task.StatusText = System.Text.RegularExpressions.Regex.Replace(task.StatusText, "(\\B[A-Z])", " $1");
            }
        }

        public static string MakeTidyHtml(string html)
        {
            try
            {
                var tidy = new Tidy.Core.Tidy();
                var tmc = new Tidy.Core.TidyMessageCollection();
                tidy.Options.DocType = Tidy.Core.DocType.Omit;
                tidy.Options.DropFontTags = true;
                tidy.Options.LogicalEmphasis = true;
                tidy.Options.Xhtml = true;
                tidy.Options.XmlOut = true;
                tidy.Options.MakeClean = true;
                tidy.Options.TidyMark = false;

                MemoryStream input = new MemoryStream();
                MemoryStream output = new MemoryStream();

                byte[] byteArray = Encoding.UTF8.GetBytes(html);
                input.Write(byteArray, 0, byteArray.Length);
                input.Position = 0;
                tidy.Parse(input, output, tmc);

                string result = Encoding.UTF8.GetString(output.ToArray());

                // Rip out contents of <body> we dont want the full document, just the snippet
                var bstart = result.IndexOf("<body>");
                if (bstart >= 0)
                {
                    var bend = result.LastIndexOf("</body>");
                    if (bend >= 0)
                        result = result.Substring(bstart + 6, bend - bstart - 6);
                }

                return result;
            }
            catch
            {
                return null;
            }
        }

        public void ReshapeRecord(Schedule schedule)
        {
            if (schedule == null)
            {
                return;
            }

            if (schedule.DateCreated.HasValue)
            {
                schedule.DateCreated = ConvertToUserLocal(schedule.DateCreated.Value);
            }
            if (schedule.StartDate.HasValue)
            {
                schedule.StartDate = ConvertToUserLocal(schedule.StartDate.Value);
            }
            if (schedule.EndDate.HasValue)
            {
                schedule.EndDate = ConvertToUserLocal(schedule.EndDate.Value);
            }

            if (TidyHtml)
            {
                schedule.Description = MakeTidyHtml(schedule.Description);
            }
        }

        public void ConvertToUserLocal(Task task)
        {
            if(task == null)
            {
                return;
            }

            task.DateCreated = ConvertToUserLocal(task.DateCreated);
            task.EndsOn = ConvertToUserLocal(task.EndsOn);
            if (task.DateStarted.HasValue == true)
            {
                task.DateStarted = ConvertToUserLocal(task.DateStarted.Value);
            }

            if (task.DateCompleted.HasValue == true)
            {
                task.DateCompleted = ConvertToUserLocal(task.DateCompleted.Value);
            }
            task.StatusDate = ConvertToUserLocal(task.StatusDate);

            if (task.StartTime.HasValue == true)
            {
                task.StartTime = ConvertToUserLocal(task.StartTime.Value);
            }

            if (task.LateAfter.HasValue == true)
            {
                task.LateAfter = ConvertToUserLocal(task.LateAfter.Value);
            }

        }

        public void ConvertToUserLocal(TimeBetweenTask task)
        {
            if(task == null)
            {
                return;
            }

            if(task.Date.HasValue == true)
            {
                task.Date = ConvertToUserLocal(task.Date.Value);
            }
            if (task.StartTime.HasValue == true)
            {
                task.StartTime = ConvertToUserLocal(task.StartTime.Value);
            }
            if (task.EndTime.HasValue == true)
            {
                task.EndTime = ConvertToUserLocal(task.EndTime.Value);
            }
        }

        public void ConvertToUserLocal(TaskWorklog task)
        {
            if (task == null)
            {
                return;
            }

            task.DateCreated = ConvertToUserLocal(task.DateCreated);

            if (task.DateCompleted.HasValue == true)
            {
                task.DateCompleted = ConvertToUserLocal(task.DateCompleted.Value);
            }

            if (ConvertDistancesToFeet == true && task.ClockinDistanceFromSite.HasValue == true)
            {
                task.ClockinDistanceFromSite = task.ClockinDistanceFromSite.Value * 3.2808399m;
            }

            if(ConvertDistancesToFeet == true && task.ClockinAccuracy.HasValue == true)
            {
                task.ClockinAccuracy = task.ClockinAccuracy.Value * 3.2808399m;
            }

            if (ConvertDistancesToFeet == true && task.ClockoutDistanceFromSite.HasValue == true)
            {
                task.ClockoutDistanceFromSite = task.ClockoutDistanceFromSite.Value * 3.2808399m;
            }

            if (ConvertDistancesToFeet == true && task.ClockoutAccuracy.HasValue == true)
            {
                task.ClockoutAccuracy = task.ClockoutAccuracy.Value * 3.2808399m;
            }
        }

        public void ConvertToUserLocal(TaskMediaStream task)
        {
            if(task == null)
            {
                return;
            }

            task.TaskDateCreated = ConvertToUserLocal(task.TaskDateCreated);
            task.TaskStatusDate = ConvertToUserLocal(task.TaskStatusDate);

            if(task.TaskDateCompleted.HasValue == true)
            {
                task.TaskDateCompleted = ConvertToUserLocal(task.TaskDateCompleted.Value);
            }
        }

        public void ConvertToUserLocal(IncompleteTask task)
        {
            if (task == null)
            {
                return;
            }

            task.DateCreated = ConvertToUserLocal(task.DateCreated.Value);
            if (task.DateStarted.HasValue == true)
            {
                task.DateStarted = ConvertToUserLocal(task.DateStarted.Value);
            }

            if (task.DateCompleted.HasValue == true)
            {
                task.DateCompleted = ConvertToUserLocal(task.DateCompleted.Value);
            }
        }

        public void ConvertToUserLocal(Leave leave)
        {
            leave.DateCreated = ConvertToUserLocal(leave.DateCreated);
            leave.StartDate = ConvertToUserLocal(leave.StartDate);
            leave.EndDate = ConvertToUserLocal(leave.EndDate);
        }

        public void ConvertToUserLocal(Checklist checklist)
        {
            checklist.DateStarted = ConvertToUserLocal(checklist.DateStarted);
            checklist.DateCompleted = ConvertToUserLocal(checklist.DateCompleted);
            if (checklist.TaskDateCreated.HasValue == true)
            {
                checklist.TaskDateCreated = ConvertToUserLocal(checklist.TaskDateCreated.Value);
            }
            if (checklist.TaskDateCompleted.HasValue == true)
            {
                checklist.TaskDateCompleted = ConvertToUserLocal(checklist.TaskDateCompleted.Value);
            }
            if (checklist.TaskStatusDate.HasValue == true)
            {
                checklist.TaskStatusDate = ConvertToUserLocal(checklist.TaskStatusDate.Value);
            }
        }

        public void ConvertToUserLocal(ChecklistResult checklistresult)
        {
            checklistresult.ChecklistDateStarted = ConvertToUserLocal(checklistresult.ChecklistDateStarted);
            checklistresult.ChecklistDateCompleted = ConvertToUserLocal(checklistresult.ChecklistDateCompleted);
            checklistresult.QuestionDateStarted = ConvertToUserLocal(checklistresult.QuestionDateStarted);
            checklistresult.QuestionDateCompleted = ConvertToUserLocal(checklistresult.QuestionDateCompleted);

            if (checklistresult.TaskDateCreated.HasValue == true)
            {
                checklistresult.TaskDateCreated = ConvertToUserLocal(checklistresult.TaskDateCreated.Value);
            }
            if (checklistresult.TaskDateCompleted.HasValue == true)
            {
                checklistresult.TaskDateCompleted = ConvertToUserLocal(checklistresult.TaskDateCompleted.Value);
            }
            if (checklistresult.TaskStatusDate.HasValue == true)
            {
                checklistresult.TaskStatusDate = ConvertToUserLocal(checklistresult.TaskStatusDate.Value);
            }
        }

        public void ConvertToUserLocal(WorkingDocument wd)
        {
            wd.DateCreated = ConvertToUserLocal(wd.DateCreated);
        }

        public void ConvertToUserLocal(ChecklistMediaStream checklistresult)
        {
            checklistresult.ChecklistDateStarted = ConvertToUserLocal(checklistresult.ChecklistDateStarted);
            checklistresult.ChecklistDateCompleted = ConvertToUserLocal(checklistresult.ChecklistDateCompleted);

            if (checklistresult.TaskDateCreated.HasValue == true)
            {
                checklistresult.TaskDateCreated = ConvertToUserLocal(checklistresult.TaskDateCreated.Value);
            }
            if (checklistresult.TaskDateCompleted.HasValue == true)
            {
                checklistresult.TaskDateCompleted = ConvertToUserLocal(checklistresult.TaskDateCompleted.Value);
            }
            if (checklistresult.TaskStatusDate.HasValue == true)
            {
                checklistresult.TaskStatusDate = ConvertToUserLocal(checklistresult.TaskStatusDate.Value);
            }
        }

        public void ConvertToUserLocal(UserTaskSummary_Result summary)
        {
            if (summary.LastActivityDate.HasValue == true)
            {
                summary.LastActivityDate = ConvertToUserLocal(summary.LastActivityDate.Value);
            }

            if(summary.FirstActivityDate.HasValue == true)
            {
                summary.FirstActivityDate = ConvertToUserLocal(summary.FirstActivityDate.Value);
            }
        }

        public void ConvertToUserLocal(UserTaskRosterSummary_Result summary)
        {
            if (summary.LastActivityDate.HasValue == true)
            {
                summary.LastActivityDate = ConvertToUserLocal(summary.LastActivityDate.Value);
            }

            if (summary.FirstActivityDate.HasValue == true)
            {
                summary.FirstActivityDate = ConvertToUserLocal(summary.FirstActivityDate.Value);
            }
        }

        public void ConvertToUserLocal(Media m)
        {
            m.DateCreated = ConvertToUserLocal(m.DateCreated);
            m.DateVersionCreated = ConvertToUserLocal(m.DateVersionCreated);
        }

        public void ConvertToUserLocal(MediaPageView mv)
        {
            mv.EndDate = ConvertToUserLocal(mv.EndDate);
            mv.StartDate = ConvertToUserLocal(mv.StartDate);
        }

        public void ConvertToUserLocal(MediaView m)
        {
            m.DateStarted = ConvertToUserLocal(m.DateStarted);
            m.DateCompleted = ConvertToUserLocal(m.DateCompleted);
        }

        public void ConvertToUserLocal(MembershipMedia m)
        {
            m.DateCreated = ConvertToUserLocal(m.DateCreated);
        }

        public void ConvertToUserLocal(tblQRTZHistory t)
        {
            t.RunTime = ConvertToUserLocal(t.RunTime);
        }

        public void ConvertToUserLocal(ErrorLogDetail e)
        {
            e.DateCreated = ConvertToUserLocal(e.DateCreated);
        }

        public void ConvertToUserLocal(ErrorLogSummary e)
        {
            if (e.LastDate.HasValue)
            {
                e.LastDate = ConvertToUserLocal(e.LastDate.Value);
            }
            if (e.FirstDate.HasValue)
            {
                e.FirstDate = ConvertToUserLocal(e.FirstDate.Value);
            }
        }

        public void ConvertToUserLocal(Asset e)
        {
            e.DateCreated = ConvertToUserLocal(e.DateCreated);

            if(e.DateDecomissioned.HasValue)
            {
                e.DateDecomissioned = ConvertToUserLocal(e.DateDecomissioned.Value);
            }
        }
    }
}
