
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace ConceptCave.Checklist.Reporting.Data
{

using System;
    using System.Collections.Generic;
    
public partial class MediaView
{

    public System.Guid Id { get; set; }

    public System.Guid MediaId { get; set; }

    public System.Guid UserId { get; set; }

    public string Name { get; set; }

    public string UserName { get; set; }

    public int UserTypeId { get; set; }

    public string UserType { get; set; }

    public int Version { get; set; }

    public System.DateTime DateStarted { get; set; }

    public System.DateTime DateCompleted { get; set; }

    public int TotalSeconds { get; set; }

    public bool Accepted { get; set; }

    public string AcceptionNotes { get; set; }

    public System.Guid WorkingDocumentId { get; set; }

    public string Document { get; set; }

}

}
