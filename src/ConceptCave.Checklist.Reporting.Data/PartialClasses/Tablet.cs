﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.Checklist.Reporting.Data
{
    public partial class Tablet 
    {
        public int? TenantId { get; set; }
        public string TenantName { get; set; }

        public Tablet()
        {

        }

        public Tablet(Tablet tablet) 
        {
            this.Archived = tablet.Archived;
            this.Description = tablet.Description;
            this.LastLoginUtc = tablet.LastLoginUtc;
            this.Manufacturer = tablet.Manufacturer;
            this.Model = tablet.Model;
            this.Name = tablet.Name;
            this.Platform = tablet.Platform;
            this.Serial = tablet.Serial;
            this.SiteId = tablet.SiteId;
            this.SiteName = tablet.SiteName;
            this.SSID = tablet.SSID;
            this.TabletProfileId = tablet.TabletProfileId;
            this.TabletProfileName = tablet.TabletProfileName;
            this.UUID = tablet.UUID;
            this.VersionNumber = tablet.VersionNumber;            
        }
    }
}
