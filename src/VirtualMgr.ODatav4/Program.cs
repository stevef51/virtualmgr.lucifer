﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
//using Lamar.Microsoft.DependencyInjection;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using Serilog;

namespace VirtualMgr.ODatav4
{
    public class Program
    {
        public static JObject NpmPackage()
        {
            return JObject.Parse(System.IO.File.ReadAllText("version.json"));
        }

        public static void Main(string[] args)
        {
            WebHost.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration((hostContext, config) =>
                {
                    config.Sources.Clear();
                    var env = hostContext.HostingEnvironment;
                    config.SetBasePath(env.ContentRootPath);
                    config.AddJsonFile("appsettings.json", optional: false, reloadOnChange: false);
                    config.AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true, reloadOnChange: false);
                    config.AddEnvironmentVariables("VM_");
                })
                //.UseLamar()
                .UseSerilog()
                .ConfigureLogging((hostContext, builder) =>
                {
                    var package = NpmPackage();
                    var version = package["version"].Value<string>();
                    var applicationName = package["name"].Value<string>();

                    Log.Logger = new LoggerConfiguration()
                        .ReadFrom.Configuration(hostContext.Configuration)
                        .Enrich.WithProperty("App Name", $"{applicationName} V{version}")
                        .Enrich.WithProperty("AppSessionId", Guid.NewGuid())
                        .CreateLogger();
                    builder.AddConfiguration(hostContext.Configuration.GetSection("Logging"));
                    builder.AddSerilog(dispose: true);

                    Log.Logger.Information($"Starting {applicationName} V{version} ...");
                })
                .UseStartup<Startup>()
                .Build()
                .Run();
        }
    }
}
