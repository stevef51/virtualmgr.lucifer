﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.SpaServices.Webpack;
using Newtonsoft.Json;
using System.IO;
using Microsoft.Extensions.Caching.Distributed;
using System.Text;
using VirtualMgr.MultiTenant;
using Serilog;
using Microsoft.Extensions.FileProviders;
using Microsoft.AspNet.OData.Extensions;
using VirtualMgr.EFData;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using VirtualMgr.AspNetCore;
using VirtualMgr.Common;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Rewrite;
using ServiceStack.Redis;
using Microsoft.Extensions.Logging;

namespace VirtualMgr.ODatav4
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<ServiceOptions>(Configuration.GetSection("Service"));
            services.Configure<ServiceOptions>(options => options.ServiceName = options.Services.OData);

            var tempProvider = services.BuildServiceProvider();
            var serviceOptions = tempProvider.GetRequiredService<IOptions<ServiceOptions>>().Value;

            services.AddLogging();

            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();


            services.Configure<ForwardedHeadersOptions>(options =>
            {
                options.ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto;
            });
            services.AddModule(new VirtualMgr.Redis.InjectionModule(serviceOptions.Services.Redis));

            services.AddOData();
            services.AddMvcCore(action => action.EnableEndpointRouting = false);


            services.AddModule<VirtualMgr.Membership.DirectCore.InjectionModule>();
            services.AddModule(new CommonAuthenticationInjectionModule(serviceOptions));
            services.AddVirtualMgrAppTenant<RequestHostTenantDomainResolver>(Configuration);
            services.AddModule(new VirtualMgr.MassTransit.InjectionModule(Configuration, serviceOptions.ServiceName));

            services.AddLLBLGenRuntimeConfiguration(System.Diagnostics.TraceLevel.Error);
            services.AddVirtualMgrMultiTenantLLBLGen(Configuration);
            services.AddVirtualMgrEFData(Configuration);

            services.AddSingleton<ITimeZoneInfoResolver>(TimeZoneInfoResolver.Use(new LinuxTimeZoneInfoResolver()));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IApplicationLifetime lifetime, IDistributedCache cache, IOptions<ServiceOptions> serviceOptions)
        {
            foreach (var nvp in Configuration.AsEnumerable())
            {
                Log.Logger.Information($"{nvp.Key}={nvp.Value}");
            }

            app.UsePathBase($"/{serviceOptions.Value.ServiceName}");

            app.UseMiddleware<VirtualMgr.ODatav4.ODatav3AdapterMiddleware>();
            app.UseVirtualMgrAppTenant();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            //            app.UseHttpsRedirection();
            app.UseMiddleware<VirtualMgr.AspNetCore.Cors.CorsMiddleware>();

            app.UseAuthentication();
            app.UseCookiePolicy();

            app.UseMvc(routes =>
            {
                routes.SetTimeZoneInfo(TimeZoneInfo.Utc);
                routes.EnableDependencyInjection();
                routes.Expand().Select().OrderBy().Filter().Count().MaxTop(null);
                routes.MapODataServiceRoute("odata-v4", "odata-v4", EdmModelBuilder.Build());
            });
        }
    }
}
