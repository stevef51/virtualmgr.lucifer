﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Query;
using VirtualMgr.ODatav4.Database;
using VirtualMgr.EFData.DataScopes;

namespace VirtualMgr.ODatav4.Controllers
{
    public class WorkingDocumentsController : ODataControllerBase
    {
        public WorkingDocumentsController(SecurityAndTimezoneAwareDataModel db) : base(db)
        {
        }

        protected override object GetDataScope(Dictionary<string, DataScopeField> metaData)
        {
            return new QueryUserDataScope().PopulateMetaData(metaData);
        }

        [EnableQuery]
        public IQueryable<WorkingDocument> GetWorkingDocuments(string datascope)
        {
            var scope = QueryUserDataScope.FromDataScopeString(datascope);

            return db.WorkingDocuments; // TODO EnforceSecurityAndScope(scope.QueryScope);
        }
    }
}
