﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Query;
using VirtualMgr.ODatav4.Database;
using VirtualMgr.EFData.DataScopes;

namespace VirtualMgr.ODatav4.Controllers
{
    public class TabletLocationHistoryController : ODataControllerBase
    {
        public TabletLocationHistoryController(SecurityAndTimezoneAwareDataModel db) : base(db)
        {
        }

        [EnableQuery(MaxExpansionDepth = 5)]
        public IQueryable<TabletLocationHistory> GetTabletLocationHistory()
        {
            return db.TabletLocationHistories;
        }
    }
}
