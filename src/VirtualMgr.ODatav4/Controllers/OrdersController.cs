﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Query;
using VirtualMgr.ODatav4.Database;
using VirtualMgr.EFData.DataScopes;

namespace VirtualMgr.ODatav4.Controllers
{
    public class OrdersController : ODataControllerBase
    {
        public OrdersController(SecurityAndTimezoneAwareDataModel db) : base(db)
        {
        }

        protected override object GetDataScope(Dictionary<string, DataScopeField> metaData)
        {
            return new QueryUserDataScope().PopulateMetaData(metaData);
        }

        [EnableQuery]
        public IQueryable<Order> GetOrders(string datascope = null)
        {
            var scope = QueryUserDataScope.FromDataScopeString(datascope);
            IQueryable<Order> result = db.Orders; // TODO EnforceSecurityAndScope(scope.QueryScope);

            return result;
        }
    }
}
