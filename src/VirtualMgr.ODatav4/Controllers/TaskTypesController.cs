﻿using System.Linq;
using Microsoft.AspNet.OData;
using VirtualMgr.ODatav4.Database;

namespace VirtualMgr.ODatav4.Controllers
{
    public class TaskTypesController : ODataControllerBase
    {
        public TaskTypesController(SecurityAndTimezoneAwareDataModel db) : base(db)
        {
        }

        [EnableQuery]
        public IQueryable<TaskType> GetTaskTypes()
        {
            return db.TaskTypes;
        }
    }
}
