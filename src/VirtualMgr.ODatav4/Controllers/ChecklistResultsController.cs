﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Query;
using VirtualMgr.ODatav4.Database;
using VirtualMgr.EFData.DataScopes;

namespace VirtualMgr.ODatav4.Controllers
{

    public class ChecklistResultsController : ODataControllerBase
    {
        public ChecklistResultsController(SecurityAndTimezoneAwareDataModel db) : base(db)
        {
        }

        protected override object GetDataScope(Dictionary<string, DataScopeField> metaData)
        {
            return new QueryUserDataScope().PopulateMetaData(metaData);
        }


        [EnableQuery]
        public IQueryable<ChecklistResult> GetChecklistResults(ODataQueryOptions<ChecklistResult> options)
        {
            return GetData(null);
        }

        [EnableQuery]
        public IQueryable<ChecklistResult> GetChecklistResults(string datascope)
        {
            return GetData(datascope);
        }

        protected IQueryable<ChecklistResult> GetData(string datascope)
        {
            var scope = QueryUserDataScope.FromDataScopeString(datascope);

            return db.ChecklistResults; // TODO EnforceSecurityAndScope(scope.QueryScope);
        }
    }
}
