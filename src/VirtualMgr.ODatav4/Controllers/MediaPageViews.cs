﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Query;
using VirtualMgr.ODatav4.Database;
using VirtualMgr.EFData.DataScopes;

namespace VirtualMgr.ODatav4.Controllers
{

    public class MediaPageViewsController : ODataControllerBase
    {
        public MediaPageViewsController(SecurityAndTimezoneAwareDataModel db) : base(db)
        {
        }

        public IQueryable<MediaPageView> GetMediaPageViews()
        {
            return db.MediaPageViews;
        }
    }
}
