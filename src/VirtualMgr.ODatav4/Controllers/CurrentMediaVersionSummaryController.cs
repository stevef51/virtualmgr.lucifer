﻿using System.Linq;
using Microsoft.AspNet.OData;
using VirtualMgr.ODatav4.Database;

namespace VirtualMgr.ODatav4.Controllers
{
    public class CurrentMediaVersionSummaryController : ODataControllerBase
    {
        public CurrentMediaVersionSummaryController(SecurityAndTimezoneAwareDataModel db) : base(db)
        {
        }

        [EnableQuery]
        public IQueryable<CurrentMediaVersionSummary> GetCurrentMediaVersionSummary()
        {
            return db.CurrentMediaVersionSummaries;
        }
    }
}
