﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Query;
using VirtualMgr.ODatav4.Database;
using VirtualMgr.EFData.DataScopes;
using VirtualMgr.MultiTenant;

namespace VirtualMgr.ODatav4.Controllers
{

    public class DBSize
    {
        public long DatabaseBytes { get; set; }
        public long DataFileBytes { get; set; }
        public long LogFileBytes { get; set; }
    }

    public class DBSizesController : ODataControllerBase
    {
        private readonly AppTenant _appTenant;

        public DBSizesController(SecurityAndTimezoneAwareDataModel db, AppTenant appTenant) : base(db)
        {
            _appTenant = appTenant;
        }

        [EnableQuery]
        public IQueryable<DBSize> GetDBSizes()
        {
            var result = new List<DBSize>();

            using (var cn = new SqlConnection(_appTenant.ConnectionString))
            {
                cn.Open();
                var cmd = new SqlCommand(@"
SELECT 
    SUM(reserved_page_count) * CAST(8 * 1024 AS BIGINT) AS DatabaseBytes,
    (SELECT SUM(df.size) * CAST(8 * 1024 AS BIGINT) FROM sys.database_files df WHERE df.type_desc = 'ROWS') AS DataFileBytes,
    (SELECT SUM(df.size) * CAST(8 * 1024 AS BIGINT) FROM sys.database_files df WHERE df.type_desc = 'LOG') AS LogFileBytes
FROM sys.dm_db_partition_stats
", cn);
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    result.Add(new DBSize()
                    {
                        DatabaseBytes = reader.GetInt64(reader.GetOrdinal("DatabaseBytes")),
                        DataFileBytes = reader.GetInt64(reader.GetOrdinal("DataFileBytes")),
                        LogFileBytes = reader.GetInt64(reader.GetOrdinal("LogFileBytes")),
                    });
                }
            }

            return result.AsQueryable();
        }
    }
}
