﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Query;
using VirtualMgr.ODatav4.Database;
using VirtualMgr.EFData.DataScopes;

namespace VirtualMgr.ODatav4.Controllers
{

    public class ESSensorReadingsController : ODataControllerBase
    {
        public ESSensorReadingsController(SecurityAndTimezoneAwareDataModel db) : base(db)
        {
        }

        // GET: odata/UserData
        [EnableQuery]
        public IQueryable<ESSensorReading> GetESSensorReadings()
        {
            return db.ESSensorReadings; // TODO EnforceSecurityAndScope(null);
        }
    }
}
