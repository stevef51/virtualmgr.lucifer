﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Query;
using VirtualMgr.ODatav4.Database;
using VirtualMgr.EFData.DataScopes;

namespace VirtualMgr.ODatav4.Controllers
{
    public class PaymentResponsesController : ODataControllerBase
    {
        public PaymentResponsesController(SecurityAndTimezoneAwareDataModel db) : base(db)
        {
        }

        [EnableQuery]
        public IQueryable<PaymentResponse> GetPaymentResponses()
        {
            return db.PaymentResponses.SelectForUser(db.CurrentUser); // TODO EnforceSecurity;
        }
    }
}
