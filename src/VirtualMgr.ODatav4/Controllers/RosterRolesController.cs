﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Query;
using VirtualMgr.ODatav4.Database;
using VirtualMgr.EFData.DataScopes;

namespace VirtualMgr.ODatav4.Controllers
{
    public class RosterRolesController : ODataControllerBase
    {
        public RosterRolesController(SecurityAndTimezoneAwareDataModel db) : base(db)
        {
        }

        protected override object GetDataScope(Dictionary<string, DataScopeField> metaData)
        {
            return new QueryUserDataScope().PopulateMetaData(metaData);
        }

        [EnableQuery]
        public IQueryable<RosterRole> GetRosterRoles(string datascope)
        {
            var scope = QueryUserDataScope.FromDataScopeString(datascope);

            return db.RosterRoles; // TODO EnforceSecurityAndScope(scope.QueryScope);
        }
    }
}
