﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Authorization;
using VirtualMgr.AspNetCore.Authorization;
using VirtualMgr.EFData.DataScopes;
using VirtualMgr.ODatav4.Database;

namespace VirtualMgr.ODatav4.Controllers
{
    [AuthorizeAny()]
    public class MembersController : ODataControllerBase
    {
        public MembersController(SecurityAndTimezoneAwareDataModel db) : base(db)
        {
        }

        protected override object GetDataScope(Dictionary<string, DataScopeField> metaData)
        {
            return new MembershipDataScope().PopulateMetaData(metaData);
        }


        // GET: odata/UserData
        [EnableQuery]
        public IQueryable<VirtualMgr.ODatav4.Database.Membership> GetMembers(string datascope = null)
        {
            MembershipDataScope scope = MembershipDataScope.FromDataScopeString(datascope);

            IQueryable<VirtualMgr.ODatav4.Database.Membership> result = db.Memberships;// .MembershipsEnforceSecurityWithParams(scope.ExcludeOutsideHierarchies, scope.IncludeCurrentUser, scope.AttemptHierarchySideStep);

            if (scope.ExcludeExpired)
            {
                result = result.Where(r => !r.ExpiryDate.HasValue || r.ExpiryDate.Value > DateTime.UtcNow);
            }

            return result;
        }
    }
}
