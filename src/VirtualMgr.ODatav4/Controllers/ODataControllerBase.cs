﻿using System.Collections.Generic;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using System;
using VirtualMgr.ODatav4.Database;
using VirtualMgr.EFData.DataScopes;
using Microsoft.AspNet.OData.Query;
using System.Linq;
using VirtualMgr.Common;

namespace VirtualMgr.ODatav4.Controllers
{
    public class ODataControllerBase : ODataController, IDisposable
    {
        protected readonly SecurityAndTimezoneAwareDataModel db;

        public ODataControllerBase(SecurityAndTimezoneAwareDataModel db)
        {
            this.db = db;
        }


        [HttpPost]
        public virtual object DataScopeInfo()
        {
            Dictionary<string, DataScopeField> metaData = new Dictionary<string, DataScopeField>();
            var dataScope = GetDataScope(metaData);
            return new { DataScope = dataScope, MetaData = metaData };
        }
        public void Dispose()
        {
            db.Dispose();
        }

        protected virtual object GetDataScope(Dictionary<string, DataScopeField> metaData)
        {
            return null;
        }

        protected object SelectForUser<TDto, TConverter>(ODataQueryOptions options, IQueryable query) where TConverter : IConvertUtcToLocal<TDto>, new()
        {
            var odataQuery = options.ApplyTo(query);
            var dtoQuery = odataQuery as IQueryable<TDto>;

            return dtoQuery != null ? db.SelectForUser<TDto, TConverter>(dtoQuery) : odataQuery;
        }
    }
}