﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Query;
using Microsoft.Extensions.Logging;
using VirtualMgr.Common;
using VirtualMgr.ODatav4.Database;
using VirtualMgr.EFData.DataScopes;
using VirtualMgr.ODatav4.Converters;
using VirtualMgr.AspNetCore.Authorization;

namespace VirtualMgr.ODatav4.Controllers
{
    [AuthorizeBearer(Roles = "User")]
    public class TasksController : ODataControllerBase
    {
        public TasksController(SecurityAndTimezoneAwareDataModel db) : base(db)
        {
        }

        protected override object GetDataScope(Dictionary<string, DataScopeField> metaData)
        {
            return new QueryUserDataScope().PopulateMetaData(metaData);
        }

        [EnableQuery]
        public object GetTasks(ODataQueryOptions<Task> options, string datascope = null)
        {
            var scope = QueryUserDataScope.FromDataScopeString(datascope);
            var query = db.TasksEnforceSecurityAndScope(scope.QueryScope);
            return SelectForUser<Task, TaskConverter>(options, query);
        }
    }
}
