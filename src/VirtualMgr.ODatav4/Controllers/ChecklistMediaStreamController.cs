﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Query;
using VirtualMgr.ODatav4.Database;
using VirtualMgr.EFData.DataScopes;

namespace VirtualMgr.ODatav4.Controllers
{
    public class ChecklistMediaStreamController : ODataControllerBase
    {
        public ChecklistMediaStreamController(SecurityAndTimezoneAwareDataModel db) : base(db)
        {
        }

        protected override object GetDataScope(Dictionary<string, DataScopeField> metaData)
        {
            return new QueryUserDataScope().PopulateMetaData(metaData);
        }

        public IQueryable<ChecklistMediaStream> GetChecklistMediaStream(ODataQueryOptions<ChecklistMediaStream> options)
        {
            return GetChecklistMediaStream(options, null);
        }

        // GET: odata/UserData
        public IQueryable<ChecklistMediaStream> GetChecklistMediaStream(ODataQueryOptions<ChecklistMediaStream> options, string datascope)
        {
            var scope = QueryUserDataScope.FromDataScopeString(datascope);

            return db.ChecklistMediaStreams; // TODO EnforceSecurityAndScope(scope.QueryScope);
        }
    }
}
