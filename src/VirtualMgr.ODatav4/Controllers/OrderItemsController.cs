﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Query;
using VirtualMgr.ODatav4.Database;
using VirtualMgr.EFData.DataScopes;

namespace VirtualMgr.ODatav4.Controllers
{
    public class OrderItemsController : ODataControllerBase
    {
        public OrderItemsController(SecurityAndTimezoneAwareDataModel db) : base(db)
        {
        }

        protected override object GetDataScope(Dictionary<string, DataScopeField> metaData)
        {
            return new QueryUserDataScope().PopulateMetaData(metaData);
        }

        [EnableQuery]
        public IQueryable<OrderItem> GetOrderItems(string datascope = null)
        {
            var scope = QueryUserDataScope.FromDataScopeString(datascope);
            IQueryable<OrderItem> result = db.OrderItems; // TODO EnforceSecurityAndScope(scope.QueryScope);

            result = result.OrderBy(i => i.SortOrder);

            return result;
        }
    }
}
