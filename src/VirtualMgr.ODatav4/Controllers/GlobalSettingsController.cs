﻿using System.Linq;
using Microsoft.AspNet.OData;
using VirtualMgr.ODatav4.Database;

namespace VirtualMgr.ODatav4.Controllers
{
    public class GlobalSettingsController : ODataControllerBase
    {
        public GlobalSettingsController(SecurityAndTimezoneAwareDataModel db) : base(db)
        {
        }

        [EnableQuery]
        public IQueryable<GlobalSetting> GetGlobalSettings()
        {
            return db.GlobalSettings;
        }
    }
}
