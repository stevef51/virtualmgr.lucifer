﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Query;
using VirtualMgr.ODatav4.Database;
using VirtualMgr.EFData.DataScopes;

namespace VirtualMgr.ODatav4.Controllers
{
    public class ESProbesController : ODataControllerBase
    {
        public ESProbesController(SecurityAndTimezoneAwareDataModel db) : base(db)
        {
        }

        [EnableQuery]
        public IQueryable<ESProbe> GetESProbes()
        {
            return db.ESProbes; // TODO EnforceSecurityAndScope(null);
        }
    }
}
