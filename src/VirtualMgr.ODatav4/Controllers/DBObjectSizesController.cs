﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Query;
using VirtualMgr.ODatav4.Database;
using VirtualMgr.EFData.DataScopes;
using VirtualMgr.MultiTenant;

namespace VirtualMgr.ODatav4.Controllers
{

    public class DBObjectSize
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public long Bytes { get; set; }
    }

    public class DBObjectSizesController : ODataControllerBase
    {
        private readonly SqlTenantMaster _appTenantMaster;

        public DBObjectSizesController(SecurityAndTimezoneAwareDataModel db, SqlTenantMaster appTenantMaster) : base(db)
        {
            _appTenantMaster = appTenantMaster;
        }

        [EnableQuery]
        public IQueryable<DBObjectSize> GetDBObjectSizes()
        {
            var result = new List<DBObjectSize>();

            using (var cn = new SqlConnection(_appTenantMaster.ConnectionString))
            {
                cn.Open();
                var cmd = new SqlCommand(@"
SELECT
      sys.objects.name, sys.objects.type_desc, SUM(reserved_page_count) * CAST(8 * 1024 AS BIGINT) AS Bytes
FROM    
      sys.dm_db_partition_stats, sys.objects
WHERE    
      sys.dm_db_partition_stats.object_id = sys.objects.object_id

GROUP BY sys.objects.name, sys.objects.type_desc
", cn);
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    result.Add(new DBObjectSize()
                    {
                        Name = reader.GetString(reader.GetOrdinal("name")),
                        Type = reader.GetString(reader.GetOrdinal("type_desc")),
                        Bytes = reader.GetInt64(reader.GetOrdinal("Bytes")),
                    });
                }
            }

            return result.AsQueryable();
        }
    }
}
