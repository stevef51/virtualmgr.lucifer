﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Query;
using VirtualMgr.ODatav4.Database;
using VirtualMgr.EFData.DataScopes;

namespace VirtualMgr.ODatav4.Controllers
{
    public class ChecklistsController : ODataControllerBase
    {
        public ChecklistsController(SecurityAndTimezoneAwareDataModel db) : base(db)
        {
        }

        protected override object GetDataScope(Dictionary<string, DataScopeField> metaData)
        {
            return new QueryUserDataScope().PopulateMetaData(metaData);
        }

        [EnableQuery]
        public IQueryable<Checklist> GetChecklists(ODataQueryOptions<Checklist> options)
        {
            return GetChecklists(options, null);
        }

        [EnableQuery]
        public IQueryable<Checklist> GetChecklists(ODataQueryOptions<Checklist> options, string datascope)
        {
            var scope = QueryUserDataScope.FromDataScopeString(datascope);

            IQueryable<Checklist> result = db.Checklists; // TODO .CheckistsEnforceSecurityAndScope(scope.QueryScope);

            return result;
        }
    }
}
