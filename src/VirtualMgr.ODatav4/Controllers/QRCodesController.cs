﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Query;
using VirtualMgr.ODatav4.Database;
using VirtualMgr.EFData.DataScopes;
using VirtualMgr.AspNetCore.Authorization;

namespace VirtualMgr.ODatav4.Controllers
{
    [AuthorizeBearer(Roles = "User")]
    public class QRCodesController : ODataControllerBase
    {
        public QRCodesController(SecurityAndTimezoneAwareDataModel db) : base(db)
        {
        }

        [EnableQuery]
        public IQueryable<QRCode> GetQRCodes()
        {
            return db.QRCodes;
        }
    }
}
