﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Query;
using VirtualMgr.ODatav4.Database;
using VirtualMgr.EFData.DataScopes;

namespace VirtualMgr.ODatav4.Controllers
{
    public class MediaViewsController : ODataControllerBase
    {
        public MediaViewsController(SecurityAndTimezoneAwareDataModel db) : base(db)
        {
        }

        public IQueryable<MediaView> GetMediaViews()
        {
            return db.MediaViews;
        }
    }
}
