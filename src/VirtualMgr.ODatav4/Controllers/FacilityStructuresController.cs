﻿using System.Linq;
using Microsoft.AspNet.OData;
using VirtualMgr.ODatav4.Database;

namespace VirtualMgr.ODatav4.Controllers
{
    public class FacilityStructuresController : ODataControllerBase
    {
        public FacilityStructuresController(SecurityAndTimezoneAwareDataModel db) : base(db)
        {

        }

        [EnableQuery]
        public IQueryable<FacilityStructure> GetFacilityStructures()
        {
            return db.FacilityStructures;
        }
    }
}
