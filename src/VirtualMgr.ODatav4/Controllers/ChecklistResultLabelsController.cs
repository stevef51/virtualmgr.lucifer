﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Query;
using VirtualMgr.ODatav4.Database;
using VirtualMgr.EFData.DataScopes;

namespace VirtualMgr.ODatav4.Controllers
{

    public class ChecklistResultLabelsController : ODataControllerBase
    {
        public ChecklistResultLabelsController(SecurityAndTimezoneAwareDataModel db) : base(db)
        {
        }

        [EnableQuery]
        public IQueryable<ChecklistResultLabel> GetChecklistResultLabels(string datascope)
        {
            var scope = QueryUserDataScope.FromDataScopeString(datascope);

            return db.ChecklistResultLabels; // TODO EnforceSecurityAndScope(scope.QueryScope);
        }
    }
}
