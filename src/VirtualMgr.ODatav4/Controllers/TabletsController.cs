﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Query;
using VirtualMgr.Common;
using VirtualMgr.ODatav4.Database;
using VirtualMgr.EFData.DataScopes;
using VirtualMgr.MultiTenant;

namespace VirtualMgr.ODatav4.Controllers
{
    public class TabletsController : ODataControllerBase
    {
        private readonly AppTenant _appTenant;
        private readonly IFetchAppTenants _fetchAppTenants;
        private readonly ITimeZoneInfoResolver _timeZoneInfoResolver;

        public TabletsController(SecurityAndTimezoneAwareDataModel db, AppTenant appTenant, IFetchAppTenants fetchAppTenants, ITimeZoneInfoResolver timeZoneInfoResolver) : base(db)
        {
            _appTenant = appTenant;
            _fetchAppTenants = fetchAppTenants;
            _timeZoneInfoResolver = timeZoneInfoResolver;
        }

        [EnableQuery]
        public async Task<IQueryable<Tablet>> GetTablets()
        {
            Func<AppTenant, Func<Tablet, Tablet>> fnTabletTenant = tenant => tablet =>
            {
                tablet.TenantId = tenant.Id;
                tablet.TenantName = tenant.Name;
                return tablet;
            };

            if (!_appTenant.ActiveProfile.IsTenantMaster)
            {
                return db.Tablets.Select(fnTabletTenant(_appTenant)).AsQueryable();
            }
            else
            {
                // Merge all tenants
                IEnumerable<Tablet> result = new Tablet[0];
                var tenants = await _fetchAppTenants.FetchAppTenantsAsync();
                foreach (var tenant in tenants)
                {
                    var tenantDb = new SecurityAndTimezoneAwareDataModel(tenant, _timeZoneInfoResolver, this.db._user, this.db._roles);
                    result = result.Concat(tenantDb.Tablets.Select(fnTabletTenant(tenant)));
                }
                return result.AsQueryable();
            }
        }
    }
}
