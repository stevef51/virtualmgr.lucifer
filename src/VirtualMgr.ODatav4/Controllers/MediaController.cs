﻿using System.Linq;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Query;
using VirtualMgr.ODatav4.Database;

namespace VirtualMgr.ODatav4.Controllers
{
    public class MediaController : ODataControllerBase
    {
        public MediaController(SecurityAndTimezoneAwareDataModel db) : base(db)
        {
        }

        [EnableQuery]
        public IQueryable<Media> GetMedia()
        {
            return db.Media;
        }
    }
}
