﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Query;
using VirtualMgr.ODatav4.Database;
using VirtualMgr.EFData.DataScopes;

namespace VirtualMgr.ODatav4.Controllers
{

    public class MemberContextsController : ODataControllerBase
    {
        public MemberContextsController(SecurityAndTimezoneAwareDataModel db) : base(db)
        {
        }

        protected override object GetDataScope(Dictionary<string, DataScopeField> metaData)
        {
            return new MembershipDataScope().PopulateMetaData(metaData);
        }

        [EnableQuery]
        public IQueryable<MembershipContext> GetMemberContexts(string datascope = null)
        {
            var scope = MembershipDataScope.FromDataScopeString(datascope);

            IQueryable<MembershipContext> result = db.MembershipContexts; // TODO EnforceSecurityWithParams(scope.ExcludeOutsideHierarchies, scope.IncludeCurrentUser);

            if (scope.ExcludeExpired)
            {
                result = result.Where(r => !r.ExpiryDate.HasValue || r.ExpiryDate.Value > DateTime.UtcNow);
            }

            return result;
        }
    }
}
