﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Query;
using VirtualMgr.ODatav4.Database;
using VirtualMgr.EFData.DataScopes;

namespace VirtualMgr.ODatav4.Controllers
{
    public class TaskMediaStreamController : ODataControllerBase
    {
        public TaskMediaStreamController(SecurityAndTimezoneAwareDataModel db) : base(db)
        {
        }

        protected override object GetDataScope(Dictionary<string, DataScopeField> metaData)
        {
            return new QueryUserDataScope().PopulateMetaData(metaData);
        }


        public IQueryable<TaskMediaStream> GetTaskMediaStream(string datascope = null)
        {
            var scope = QueryUserDataScope.FromDataScopeString(datascope);

            return db.TaskMediaStreams; // TODO EnforceSecurityAndScope(scope.QueryScope);
        }
    }
}
