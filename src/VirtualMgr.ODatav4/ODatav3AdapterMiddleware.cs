using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Primitives;

namespace VirtualMgr.ODatav4
{
    public class ODatav3AdapterMiddleware
    {
        private readonly ILogger _logger;
        private readonly RequestDelegate _next;

        private abstract class TransformQuery
        {
            public abstract void Transform(IDictionary<string, StringValues> query);
        }

        private class TransformRegexReplace : TransformQuery
        {
            private readonly string _clause;
            private readonly Regex _regex;
            private readonly string _replace;
            public TransformRegexReplace(string clause, string regex, string replace)
            {
                _clause = clause;
                _regex = new Regex(regex);
                _replace = replace;
            }
            public override void Transform(IDictionary<string, StringValues> query)
            {
                if (query.ContainsKey(_clause))
                {
                    var values = query[_clause];
                    var newValues = new List<string>();
                    foreach (var value in values)
                    {
                        newValues.Add(_regex.Replace(value, _replace));
                    }
                    query[_clause] = new StringValues(newValues.ToArray());
                }
            }
        }

        private class TransformInlineCountClause : TransformQuery
        {
            public override void Transform(IDictionary<string, StringValues> query)
            {
                if (query.ContainsKey("$inlinecount"))
                {
                    if (query["$inlinecount"] == "allpages")
                    {
                        query["$count"] = "true";
                    }
                    query.Remove("$inlinecount");
                }
            }
        }

        private class TransformSelectExpandClause : TransformQuery
        {
            public override void Transform(IDictionary<string, StringValues> query)
            {
                var newSelect = new List<string>();
                var newExpand = new Dictionary<string, List<string>>();
                if (query.ContainsKey("$expand"))
                {
                    var expandClause = query["$expand"].ToString();
                    foreach (var expand in expandClause.Split(','))
                    {
                        newExpand[expand] = new List<string>();
                    }
                }
                if (query.ContainsKey("$select"))
                {
                    var selectClause = query["$select"].ToString();
                    if (!string.IsNullOrEmpty(selectClause))
                    {
                        var selects = selectClause.Split(',');
                        foreach (var select in selects)
                        {
                            var selectChilds = select.Split('/');
                            newSelect.Add(selectChilds[0]);
                            for (int i = 1; i < selectChilds.Length; i++)
                            {
                                List<string> existingExpand;
                                if (!newExpand.TryGetValue(selectChilds[0], out existingExpand))
                                {
                                    existingExpand = new List<string>();
                                    newExpand[selectChilds[0]] = existingExpand;
                                }
                                existingExpand.Add(selectChilds[i]);
                            }
                        }
                    }
                }
                if (newSelect.Any())
                {
                    query["$select"] = new StringValues(string.Join(",", newSelect.Distinct()));
                }
                if (newExpand.Any())
                {
                    var expandSelect = from nvp in newExpand
                                       let s = nvp.Value.Any() ? $"($select={string.Join(',', nvp.Value.Distinct())})" : ""
                                       select $"{nvp.Key}{s}";
                    query["$expand"] = new StringValues(string.Join(",", expandSelect));
                }
            }
        }

        private static readonly TransformQuery[] V3toV4 =
        {
            new TransformRegexReplace("$filter", @"guid'(?<guid>.{36})'", @"${guid}"),
            new TransformRegexReplace("$filter", @"datetime'(?<date>.*?)T(?<time>.{12})(?<offset>.*?)'", @"cast(${date}T${time}${offset},Edm.DateTimeOffset)"),
            new TransformRegexReplace("$filter", @"substringof\(\s*(?<param1>.*?),\s*(?<param2>.*?)\)", @"contains(${param2},${param1})"),
            new TransformInlineCountClause(),
            new TransformSelectExpandClause()
        };

        public ODatav3AdapterMiddleware(RequestDelegate next, ILogger<ODatav3AdapterMiddleware> logger)
        {
            _next = next ?? throw new ArgumentNullException(nameof(next));
            _logger = logger;
        }

        public async Task Invoke(HttpContext context)
        {
            PathString matched;
            PathString remaining;
            if (context.Request.Path.StartsWithSegments(new PathString("/odata-v3"), out matched, out remaining))
            {
                // Pick out /odata-v3 requests and remap them to /odata-v4
                var v4query = QueryHelpers.ParseQuery(context.Request.QueryString.Value);
                // Map v3 Edm types to v4
                foreach (var v3tov4 in V3toV4)
                {
                    v3tov4.Transform(v4query);
                }

                var newQuery = QueryHelpers.AddQueryString("", v4query.ToDictionary(nvp => nvp.Key, nvp => nvp.Value.ToString()));
                context.Request.QueryString = new QueryString(newQuery);

                // Rewrite /odata-v3 to /odata-v4
                context.Request.Path = new PathString("/odata-v4").Add(remaining);

                _logger.LogInformation("ODataV3 to V4 rewrite {rewrite}", context.Request.GetDisplayUrl());
            }
            await _next(context);
        }
    }
}
