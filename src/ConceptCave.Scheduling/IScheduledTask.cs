﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.Scheduling
{
    public interface IScheduledTask
    {
        Guid Id { get; }
        Guid? OwnerId { get; }
        Guid? SiteId { get; }
        Guid TaskTypeId { get; }
        int CalendarRuleId { get; }
        ICalendarRule CalendarRule { get; }

        int RosterId { get; }
        int Level { get; }

        int? RoleId { get; }
        bool IsArchived { get; }
        string LengthInDays { get; }

        IList<ITask> Tasks { get; }
    }
}
