﻿using ConceptCave.Scheduling.ExclusionRules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.Scheduling
{
    public class ExclusionRuleEnforcer
    {
        public IList<IScheduledTaskCalendarExclusionRule> Rules { get; protected set; }

        public ExclusionRuleEnforcer()
        {
            Rules = new List<IScheduledTaskCalendarExclusionRule>();
        }

        /// <summary>
        /// Enforces the exclusion rules on the set of scheduled tasks handed in. Returns the set of
        /// scheduled tasks that have been removed from the set handed in.
        /// </summary>
        /// <param name="scheduledTasks">Scheduled tasks to remove based on exclusion rules</param>
        /// <param name="date">Date that is to be considered for removal of tasks</param>
        /// <param name="removeSchedulesWithLiveTasks">Flag to indicate wether or not rules should also eliminate based on live tasks being present</param>
        /// <returns>The set of scheduled tasks removed from scheduledTasks</returns>
        public IList<IScheduledTask> Enforce(IList<IScheduledTask> scheduledTasks, DateTime date, TimeZoneInfo timeZoneInfo, bool removeSchedulesWithLiveTasks)
        {
            var sortedRules = (from r in Rules orderby r.Priority ascending select r);

            List<IScheduledTask> toRemove = new List<IScheduledTask>();

            foreach(var r in sortedRules)
            {
                var ruleExcluded = r.GetExcluded(scheduledTasks, date, timeZoneInfo, removeSchedulesWithLiveTasks);

                foreach(var s in ruleExcluded)
                {
                    toRemove.Add(s);
                }
            }

            foreach(var s in toRemove)
            {
                scheduledTasks.Remove(s);
            }

            return toRemove;
        }
    }
}
