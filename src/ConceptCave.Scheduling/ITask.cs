﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.Scheduling
{
    public interface ITask
    {
        Guid Id { get; }
        Guid? ScheduledTaskId { get; }

        int RosterId { get; }
        Guid TaskTypeId { get; }

        DateTime DateCreated { get; }
        DateTime? DateCompleted { get; }
        Guid OwnerId { get; }
        Guid OriginalOwnerId { get; }
        Guid? SiteId { get; }

        int Status { get; }
    }
}
