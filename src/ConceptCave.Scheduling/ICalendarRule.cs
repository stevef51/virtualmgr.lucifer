﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.Scheduling
{
    public interface ICalendarRule
    {
        int Id { get; }
        DateTime DateCreated { get; }
        DateTime StartDate { get; }
        DateTime? EndDate { get; }
        string Frequency { get; }
        bool ExecuteSunday { get; }
        bool ExecuteMonday { get; }
        bool ExecuteTuesday { get; }
        bool ExecuteWednesday { get; }
        bool ExecuteThursday { get; }
        bool ExecuteFriday { get; }
        bool ExecuteSaturday { get; }
        string CRONExpression { get; }
        int Period { get; }
    }
}
