﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.Scheduling
{
    public enum CalendarRuleFrequency
    {
        Single,
        Daily,
        Cron
    }
}
