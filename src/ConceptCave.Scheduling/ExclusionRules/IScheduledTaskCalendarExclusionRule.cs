﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.Scheduling.ExclusionRules
{
    public interface IScheduledTaskCalendarExclusionRule
    {
        int Priority { get; }
        IList<IScheduledTask> GetExcluded(IEnumerable<IScheduledTask> candidates, DateTime date, TimeZoneInfo timeZoneInfo, bool removeWithLiveTasks);
    }
}
