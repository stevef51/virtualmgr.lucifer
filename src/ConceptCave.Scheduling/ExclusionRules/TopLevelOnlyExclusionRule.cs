﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.Scheduling.ExclusionRules
{
    public class TopLevelOnlyExclusionRule : IScheduledTaskCalendarExclusionRule
    {
        public int Priority
        {
            get 
            {                 
                return int.MaxValue; // needs to get executed late
            }
        }

        public IList<IScheduledTask> GetExcluded(IEnumerable<IScheduledTask> candidates, DateTime date, TimeZoneInfo timeZoneInfo, bool removeWithLiveTasks)
        {
            var topLevel = (from c in candidates orderby c.Level descending select c.Level).First();

            return (from c in candidates where c.Level != topLevel select c).ToList();
        }
    }
}
