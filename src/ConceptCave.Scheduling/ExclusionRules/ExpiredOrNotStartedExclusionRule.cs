﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.Scheduling.ExclusionRules
{
    public class ExpiredOrNotStartedExclusionRule : IScheduledTaskCalendarExclusionRule
    {
        public int Priority
        {
            get { return 0; }
        }

        public IList<IScheduledTask> GetExcluded(IEnumerable<IScheduledTask> candidates, DateTime date, TimeZoneInfo timeZoneInfo, bool removeWithLiveTasks)
        {
            return (from c in candidates
                    where c.CalendarRule.StartDate > date ||
                        (c.CalendarRule.EndDate.HasValue == true &&
                        c.CalendarRule.EndDate.Value < date)
                    select c).ToList();
        }
    }
}
