﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.Scheduling.ExclusionRules
{
    public class CronScheduleExclusionRule : IScheduledTaskCalendarExclusionRule
    {       
        public int Priority
        {
            get { return 100; }
        }

        public IList<IScheduledTask> GetExcluded(IEnumerable<IScheduledTask> candidates, DateTime date, TimeZoneInfo timeZoneInfo, bool removeWithLiveTasks)
        {
            var query = from c in candidates where c.CalendarRule.Frequency == CalendarRuleFrequency.Cron.ToString() select c;

            if(query.Count() == 0)
            {
                // nothing to do here
                return new List<IScheduledTask>();
            }

            // The <date> is a local date in the <timeZoneInfo> TimeZone, Quartz.CronExpression.GetNextValidTimeAfter expects a UTC date and will internally convert this 
            // to the configured <timeZoneInfo> since we set its timezone below, so convert it to UTC
            var dateUtc = TimeZoneInfo.ConvertTimeToUtc(date, timeZoneInfo);
            DateTimeOffset offset = new DateTimeOffset(dateUtc);

            List<IScheduledTask> result = new List<IScheduledTask>();
            foreach(var c in query)
            {
                var expression = new Quartz.CronExpression(c.CalendarRule.CRONExpression);
                expression.TimeZone = timeZoneInfo;

                // we don't support less than 24 hours scheduling
                var lastRun = expression.GetNextValidTimeAfter(offset.AddHours(-24));
                var nextRun = expression.GetNextValidTimeAfter(offset);

                if(lastRun == nextRun)
                {
                    // ok so we haven't passed over the boundary between the last run and the next one, so we can leave here
                    result.Add(c);
                    break;
                }

                // so we are on the boundary of cross over when the CRON should be generating something
                if (removeWithLiveTasks == true && c.Tasks.Count > 0)
                {
                    // if there are any tasks associated with the schedule, then we exclude this one
                    result.Add(c);
                }
            }

            return result;
        }
    }
}
