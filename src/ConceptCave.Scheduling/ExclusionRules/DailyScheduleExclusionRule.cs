﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConceptCave.Scheduling.ExclusionRules
{
    public class DailyScheduleExclusionRule : IScheduledTaskCalendarExclusionRule
    {
        public int Priority
        {
            get { return 100; } // executed in the middle somewhere
        }

        public IList<IScheduledTask> GetExcluded(IEnumerable<IScheduledTask> candidates, DateTime date, TimeZoneInfo timeZoneInfo, bool removeWithLiveTasks)
        {
            var query = from c in candidates
                        where
                            c.CalendarRule.Frequency == CalendarRuleFrequency.Daily.ToString()
                        select c;

            IEnumerable<IScheduledTask> liveExclusions = new List<IScheduledTask>();

            var utcDate = date.ToUniversalTime();

            if (removeWithLiveTasks == true)
            {
                // we want to exclude anything that has task instances that have been created on the date handed in
                liveExclusions = (from c in query
                                  where
                                      c.Tasks.Count > 0 &&
                                      (from t in c.Tasks
                                       where
                                           t.DateCreated >= utcDate &&
                                           t.DateCreated < utcDate.AddDays(1)
                                       select t).Count() > 0
                                  select c);
            }

            DayOfWeek day = date.DayOfWeek;

            Func<DateTime, DayOfWeek, int, bool> MatchesPeriod = (calendarStartDate, dayOfWeek, period) =>
            {
                // we have to work out the date of the first occurence of the
                // dayOfWeek after calendarStartDate
                var daysUntilNextDayOfWeek = ((int)dayOfWeek - (int)calendarStartDate.DayOfWeek + 7) % 7;
                var compareDate = calendarStartDate.AddDays(daysUntilNextDayOfWeek);

                // we'll remove the time from the equation as its days we are interested in
                // compare date is in UTC, so we use the UTC version of the date that was
                // handed into the GetExcluded function
                return (utcDate.Date.Subtract(compareDate.Date).TotalDays / 7) % period == 0;
            };

            switch (day)
            {
                case DayOfWeek.Sunday:
                    return (from c in query
                            where c.CalendarRule.ExecuteSunday == false ||
                            (c.CalendarRule.Period > 1 &&
                            MatchesPeriod(c.CalendarRule.StartDate, utcDate.DayOfWeek, c.CalendarRule.Period) == false)
                            select c).Union(liveExclusions).ToList();
                case DayOfWeek.Monday:
                    return (from c in query
                            where c.CalendarRule.ExecuteMonday == false ||
                            (c.CalendarRule.Period > 1 &&
                            MatchesPeriod(c.CalendarRule.StartDate, utcDate.DayOfWeek, c.CalendarRule.Period) == false)
                            select c).Union(liveExclusions).ToList();
                case DayOfWeek.Tuesday:
                    return (from c in query
                            where c.CalendarRule.ExecuteTuesday == false ||
                            (c.CalendarRule.Period > 1 &&
                            MatchesPeriod(c.CalendarRule.StartDate, utcDate.DayOfWeek, c.CalendarRule.Period) == false)
                            select c).Union(liveExclusions).ToList();
                case DayOfWeek.Wednesday:
                    return (from c in query
                            where c.CalendarRule.ExecuteWednesday == false ||
                            (c.CalendarRule.Period > 1 &&
                            MatchesPeriod(c.CalendarRule.StartDate, utcDate.DayOfWeek, c.CalendarRule.Period) == false)
                            select c).Union(liveExclusions).ToList();
                case DayOfWeek.Thursday:
                    return (from c in query
                            where c.CalendarRule.ExecuteThursday == false ||
                            (c.CalendarRule.Period > 1 &&
                            MatchesPeriod(c.CalendarRule.StartDate, utcDate.DayOfWeek, c.CalendarRule.Period) == false)
                            select c).Union(liveExclusions).ToList();
                case DayOfWeek.Friday:
                    return (from c in query
                            where c.CalendarRule.ExecuteFriday == false ||
                            (c.CalendarRule.Period > 1 &&
                            MatchesPeriod(c.CalendarRule.StartDate, utcDate.DayOfWeek, c.CalendarRule.Period) == false)
                            select c).Union(liveExclusions).ToList();
                default:
                    return (from c in query
                            where c.CalendarRule.ExecuteSaturday == false ||
                            (c.CalendarRule.Period > 1 &&
                            MatchesPeriod(c.CalendarRule.StartDate, utcDate.DayOfWeek, c.CalendarRule.Period) == false)
                            select c).Union(liveExclusions).ToList();
            }
        }
    }
}
