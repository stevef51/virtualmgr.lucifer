﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualMgr.Membership.Direct.IdentityOverrides;
using Security = System.Web.Security;
using Microsoft.AspNet.Identity;
using VirtualMgr.Membership.Interfaces;

namespace VirtualMgr.Membership
{
    public class MembershipWrapper : IMembership
    {
        public int MinRequiredPasswordLength => Security.Membership.MinRequiredPasswordLength;

        protected readonly ApplicationUserManager _manager;
        protected readonly ICurrentContextProvider _currentUserProvider;

        public MembershipWrapper(ApplicationUserManager manager, ICurrentContextProvider currentUserProvider)
        {
            _manager = manager;
            _currentUserProvider = currentUserProvider;
        }

        public IMembershipUser CreateUser(string username, string password)
        {
            var user = new ApplicationUser();
            user.UserName = username;
            user.LockoutEnabled = true;

            var result = _manager.Create(user, password);

            if(result == IdentityResult.Success)
            {
                user = _manager.Find(username, password);

                return new MembershipUserWrapper(user, _manager);
            }

            return null;
        }

        public bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            var user = _manager.FindByName(username);

            return _manager.Delete(user) == IdentityResult.Success;
        }

        public string GeneratePassword(int length, int numberOfNonAlphanumericCharacters)
        {
            throw new NotImplementedException("this method is not supported under the identity framework");
        }

        public IMembershipUser GetUser(object providerUserKey)
        {
            return new MembershipUserWrapper(_manager.FindById(providerUserKey.ToString()), _manager);
        }

        public IMembershipUser GetUser(string username)
        {
            return new MembershipUserWrapper(_manager.FindByName(username), _manager);
        }

        public IMembershipUser GetUser()
        {
            return GetUser(_currentUserProvider.ProviderUserKey);
        }

        public IMembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            return GetUser(providerUserKey);
        }

        public bool ValidateUser(string username, string password)
        {
            var user = _manager.FindByName(username);

            if (user == null || _manager.IsLockedOut(user.Id))
            {
                return false;
            }

            // Valid user, verify password
            var result = _manager.PasswordHasher.VerifyHashedPassword(user.PasswordHash, password);
            if (result == PasswordVerificationResult.Success)
            {
                return true;
            }
            else if (result == PasswordVerificationResult.SuccessRehashNeeded)
            {
                // Logged in using old Membership credentials - update hashed password in database
                // Since we update the user on login anyway, we'll just set the new hash
                // Optionally could set password via the ApplicationUserManager by using
                // RemovePassword() and AddPassword()
                user.PasswordHash = _manager.PasswordHasher.HashPassword(password);
                _manager.Update(user);

                return true;
            }
            else
            {
                // Failed login, increment failed login counter
                // Lockout for 15 minutes if more than 10 failed attempts
                user.AccessFailedCount++;

                //if (user.AccessFailedCount >= 10)
                //{
                //    user.LockoutEndDateUtc = DateTime.UtcNow.AddMinutes(15);
                //}

                _manager.Update(user);
                return false;
            }
        }
    }
}
