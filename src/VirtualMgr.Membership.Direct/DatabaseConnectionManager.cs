﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualMgr.Membership.Direct
{
    public class DatabaseConnectionManager
    {
        public DatabaseConnectionManager(Func<ConnectionStringSettings> connectionSettings)
        {
            FnGetConnectionStringSettings = connectionSettings;
        }
        protected Func<ConnectionStringSettings> FnGetConnectionStringSettings { get; set; }

        public ConnectionStringSettings ConnectionString
        {
            get
            {
                return FnGetConnectionStringSettings();
            }
        }
    }
}
