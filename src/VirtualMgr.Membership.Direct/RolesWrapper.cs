﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualMgr.Membership.Direct.IdentityOverrides;
using Security = System.Web.Security;
using Microsoft.AspNet.Identity;

namespace VirtualMgr.Membership
{
    public class RolesWrapper : IRoles, IDisposable
    {
        protected ApplicationRoleManager _roleManager;
        protected ApplicationRoleManager RoleManager {
            get
            {
                return _roleManager;
            }
        }

        protected ApplicationUserManager _userManager;
        protected ApplicationUserManager UserManager
        {
            get
            {
                return _userManager;
            }
        }
    
        public RolesWrapper(ApplicationRoleManager roleManager, ApplicationUserManager userManager)
        {
            _roleManager = roleManager;
            _userManager = userManager;
        }

        public string ApplicationName
        {
            get;set;
        }

        /// <summary>
        /// This should no longer be used
        /// </summary>
        public int CookieTimeout
        {
            get
            {
                return -1;
            }
        }

        public bool CacheRolesInCookie
        {
            get
            {
                return false;
            }
        }

        public string CookieName
        {
            get
            {
                return string.Empty;
            }
        }

        public bool Enabled { get; set; }

        public bool CookieSlidingExpiration => false;

        public string CookiePath => "";

        public bool CreatePersistentCookie => false;

        public string Domain => "";

        public int MaxCachedResults => 0;

        public bool CookieRequireSSL => true;

        public void AddUsersToRole(string[] usernames, string roleName)
        {
            usernames.ToList().ForEach(u =>
            {
                var user = UserManager.FindByName(u);

                if(user != null)
                {
                    UserManager.AddToRole(user.Id, roleName);
                }
            }
            );
        }

        public void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            usernames.ToList().ForEach(u =>
            {
                var user = UserManager.FindByName(u);

                if (user != null)
                {
                    UserManager.AddToRoles(user.Id, roleNames);
                }
            });
        }

        public void AddUserToRole(string username, string roleName)
        {
            var user = UserManager.FindByName(username);

            if (user != null)
            {
                UserManager.AddToRole(user.Id, roleName);
            }
        }

        public void AddUserToRoles(string username, string[] roleNames)
        {
            var user = UserManager.FindByName(username);

            if (user != null)
            {
                UserManager.AddToRoles(user.Id, roleNames);
            }
        }

        public void CreateRole(string roleName)
        {
            RoleManager.Create(new ApplicationRole() { Name = roleName, Id = RT.Comb.Provider.Sql.Create().ToString() });
        }

        public void DeleteCookie()
        {

        }

        public bool DeleteRole(string roleName)
        {
            var role = RoleManager.FindByName(roleName);

            if(role == null)
            {
                return false;
            }

            try
            {
                RoleManager.Delete(role);
            }
            catch
            {
                return false;
            }


            return true;
        }

        public bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            if(throwOnPopulatedRole == false)
            {
                return DeleteRole(roleName);
            }

            var role = RoleManager.FindByName(roleName);

            if (role == null)
            {
                return false;
            }

            RoleManager.Delete(role);

            return true;
        }

        public string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException("This method is currently not implemented");
        }

        public string[] GetAllRoles()
        {
            return (from r in RoleManager.Roles orderby r.Name select r.Name).ToArray();
        }

        public string[] GetRolesForUser()
        {
            throw new NotImplementedException("This method is not implemented");
        }

        public string[] GetRolesForUser(string username)
        {
            var user = UserManager.FindByName(username);

            return UserManager.GetRoles(user.Id).ToArray();
        }

        public string[] GetUsersInRole(string roleName)
        {
            var role = RoleManager.FindByName(roleName);

            if(role == null)
            {
                throw new ArgumentException(string.Format("{0} does not exist", roleName));
            }

            return (from u in UserManager.Users from r in u.Roles where r.RoleId == role.Id select u.UserName).ToArray();
        }

        public bool IsUserInRole(string username, string roleName)
        {
            var user = UserManager.FindByName(username);

            return UserManager.IsInRole(user.Id, roleName);
        }

        public bool IsUserInRole(string roleName)
        {
            throw new NotImplementedException("This method is not implemented");
        }

        public void RemoveUserFromRole(string username, string roleName)
        {
            var user = UserManager.FindByName(username);
            UserManager.RemoveFromRole(user.Id, roleName);
        }
        public void RemoveUserFromRoles(string username, string[] roleNames)
        {
            var user = UserManager.FindByName(username);
            UserManager.RemoveFromRoles(user.Id, roleNames);
        }

        public void RemoveUsersFromRole(string[] usernames, string roleName)
        {
            usernames.ToList().ForEach(u =>
            {
                var user = UserManager.FindByName(u);
                UserManager.RemoveFromRole(user.Id, roleName);
            });
        }

        public void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            usernames.ToList().ForEach(u =>
            {
                var user = UserManager.FindByName(u);
                UserManager.RemoveFromRoles(user.Id, roleNames);
            });
        }

        public bool RoleExists(string roleName)
        {
            return RoleManager.RoleExists(roleName);
        }

        public void Dispose()
        {
            if (_roleManager != null)
                _roleManager.Dispose();

            if (_userManager != null)
                _userManager.Dispose();
        }
    }
}
