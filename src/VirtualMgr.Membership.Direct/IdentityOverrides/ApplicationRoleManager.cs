﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualMgr.Membership.Direct.IdentityOverrides
{
    public class ApplicationRoleManager : RoleManager<ApplicationRole, string>
    { 
        public ApplicationRoleManager(RoleStore<ApplicationRole> roleStore)
            : base(roleStore)
        {
        }
    }
}
