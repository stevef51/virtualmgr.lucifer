﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualMgr.Membership.Direct.IdentityOverrides
{
    public class ApplicationUser : IdentityUser
    {
        public ApplicationUser() { }
    }
}
