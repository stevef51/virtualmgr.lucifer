﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualMgr.Membership.Direct.IdentityOverrides
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, string, IdentityUserLogin, IdentityUserRole, IdentityUserClaim>
    {
        public ApplicationDbContext(DatabaseConnectionManager connectionManager)
            : base(GetConnectionString(null, connectionManager)) {
        }

        private static string GetConnectionString(string suggestedEntityConnectionString, DatabaseConnectionManager connectionManager, string tenantConnectionString = null)
        {
            var tenantConnectionBuilder = new SqlConnectionStringBuilder(tenantConnectionString ?? connectionManager.ConnectionString.ConnectionString);

            // We have to enable MultipleActiveResultSets due to the possibility of recursive SqlDataReaders on the DataModel connection
            tenantConnectionBuilder.MultipleActiveResultSets = true;

            return tenantConnectionBuilder.ConnectionString;
        }
    }
}
