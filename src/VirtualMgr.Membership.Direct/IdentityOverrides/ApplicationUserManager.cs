﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace VirtualMgr.Membership.Direct.IdentityOverrides
{
    public class ApplicationUserManager : UserManager<ApplicationUser, string>
    {
        public ApplicationUserManager(UserStore<ApplicationUser, ApplicationRole, string, IdentityUserLogin, IdentityUserRole, IdentityUserClaim> store)
          : base(store)
        {
            this.PasswordHasher = new SqlPasswordHasher();
        }

        public IdentityResult ChangePassword(string key, string newPassword)
        {
            var store = this.Store as IUserPasswordStore<ApplicationUser, string>;
            if (store == null)
            {
                var errors = new string[]
                {
                    "Current UserStore doesn't implement IUserPasswordStore"
                };

                return new IdentityResult(errors);
            }

            if (PasswordValidator != null)
            {
                var passwordResult = PasswordValidator.ValidateAsync(key);

                if (!passwordResult.Result.Succeeded)
                    return passwordResult.Result;
            }

            var newPasswordHash = this.PasswordHasher.HashPassword(newPassword);

            var user = this.FindById(key);

            store.SetPasswordHashAsync(user, newPasswordHash);
            this.Update(user);

            return IdentityResult.Success;
        }
    }
}
