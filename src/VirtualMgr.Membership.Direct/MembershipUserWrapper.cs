﻿using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualMgr.Membership.Direct.IdentityOverrides;
using Security = System.Web.Security;

namespace VirtualMgr.Membership
{
    class MembershipUserWrapper : IMembershipUser
    {
        private readonly ApplicationUser _membershipUser;
        private readonly ApplicationUserManager _manager;

        public MembershipUserWrapper(ApplicationUser membershipUser, ApplicationUserManager manager)
        {
            _membershipUser = membershipUser;
            _manager = manager;
        }

        public object ProviderUserKey => _membershipUser.Id;

        public DateTime LastPasswordChangedDate => DateTime.MinValue;

        public DateTime LastActivityDate
        {
            get
            {
                return DateTime.MinValue;
            }
            set
            {

            }
        }
        public DateTime LastLoginDate
        {
            get
            {
                return DateTime.MinValue;
            }
            set
            {
                
            }
        }

        public DateTime CreationDate => DateTime.MinValue;

        public DateTime LastLockoutDate => DateTime.MinValue;

        public bool IsLockedOut => _membershipUser.LockoutEndDateUtc > DateTime.UtcNow;

        public bool IsApproved
        {
            get
            {
                return true;
            }
            set
            {

            }
        }
        public string Comment
        {
            get
            {
                return string.Empty;
            }
            set
            {

            }
        }

        public string PasswordQuestion => string.Empty;

        public string ProviderName => string.Empty;

        public bool IsOnline => true;

        public string UserName => _membershipUser.UserName;

        public string Email { get => _membershipUser.Email; set => _membershipUser.Email = value; }

        public bool ChangePassword(string oldPassword, string newPassword)
        {
            var result = _manager.ChangePassword(_membershipUser.Id, newPassword);

            return result.Succeeded;
        }

        public bool ChangePasswordQuestionAndAnswer(string password, string newPasswordQuestion, string newPasswordAnswer)
        {
            throw new NotImplementedException("This method is no longer supported under the identity framework");
        }

        public string GetPassword(string passwordAnswer)
        {
            throw new NotImplementedException("This method is no longer supported under the identity framework");
        }

        public string GetPassword()
        {
            throw new NotImplementedException("This method is no longer supported under the identity framework");
        }

        public string ResetPassword()
        {
            throw new NotImplementedException("This method is no longer supported under the identity framework");
        }

        public string ResetPassword(string passwordAnswer)
        {
            throw new NotImplementedException("This method is no longer supported under the identity framework");
        }

        public bool UnlockUser()
        {
            var result = _manager.SetLockoutEndDate(_membershipUser.Id, DateTimeOffset.UtcNow);

            return result.Succeeded;
        }
    }
}
