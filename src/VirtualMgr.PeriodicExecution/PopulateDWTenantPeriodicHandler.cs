﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using NLog;
using ConceptCave.RepositoryInterfaces.Repositories;

namespace VirtualMgr.PeriodicExecution
{
    public class TenantPopulateBuildingFloorsDimensionPeriodicHandler : DWPopulatePeriodicHandler
    {
        public TenantPopulateBuildingFloorsDimensionPeriodicHandler(Func<string, IDWPopulate> fn) 
            : base("TenantPopulateBuildingFloorsDimension", fn("Tenant_Dim_BuildingFloors"))
        {
        }
    }
    public class TenantPopulateCompaniesDimensionPeriodicHandler : DWPopulatePeriodicHandler
    {
        public TenantPopulateCompaniesDimensionPeriodicHandler(Func<string, IDWPopulate> fn) 
            : base("TenantPopulateCompaniesDimension", fn("Tenant_Dim_Companies"))
        {
        }
    }
    public class TenantPopulateDatesDimensionPeriodicHandler : DWPopulatePeriodicHandler
    {
        public TenantPopulateDatesDimensionPeriodicHandler(Func<string, IDWPopulate> fn) 
            : base("TenantPopulateDatesDimension", fn("Tenant_Dim_Dates"))
        {
        }
    }
    public class TenantPopulateDateTimesDimensionPeriodicHandler : DWPopulatePeriodicHandler
    {
        public TenantPopulateDateTimesDimensionPeriodicHandler(Func<string, IDWPopulate> fn) 
            : base("TenantPopulateDateTimesDimension", fn("Tenant_Dim_DateTimes"))
        {
        }
    }
    public class TenantPopulateFacilitiesDimensionPeriodicHandler : DWPopulatePeriodicHandler
    {
        public TenantPopulateFacilitiesDimensionPeriodicHandler(Func<string, IDWPopulate> fn) 
            : base("TenantPopulateFacilitiesDimension", fn("Tenant_Dim_Facilities"))
        {
        }
    }
    public class TenantPopulateFacilityBuildingsDimensionPeriodicHandler : DWPopulatePeriodicHandler
    {
        public TenantPopulateFacilityBuildingsDimensionPeriodicHandler(Func<string, IDWPopulate> fn) 
            : base("TenantPopulateFacilityBuildingsDimension", fn("Tenant_Dim_FacilityBuildings"))
        {
        }
    }
    public class TenantPopulateFloorZonesDimensionPeriodicHandler : DWPopulatePeriodicHandler
    {
        public TenantPopulateFloorZonesDimensionPeriodicHandler(Func<string, IDWPopulate> fn) 
            : base("TenantPopulateFloorZonesDimension", fn("Tenant_Dim_FloorZones"))
        {
        }
    }
    public class TenantPopulateRostersDimensionPeriodicHandler : DWPopulatePeriodicHandler
    {
        public TenantPopulateRostersDimensionPeriodicHandler(Func<string, IDWPopulate> fn) 
            : base("TenantPopulateRostersDimension", fn("Tenant_Dim_Rosters"))
        {
        }
    }
    public class TenantPopulateSitesDimensionPeriodicHandler : DWPopulatePeriodicHandler
    {
        public TenantPopulateSitesDimensionPeriodicHandler(Func<string, IDWPopulate> fn) 
            : base("TenantPopulateSitesDimension", fn("Tenant_Dim_Sites"))
        {
        }
    }

    public class TenantPopulateTaskCustomStatusDimensionPeriodicHandler : DWPopulatePeriodicHandler
    {
        public TenantPopulateTaskCustomStatusDimensionPeriodicHandler(Func<string, IDWPopulate> fn) 
            : base("TenantPopulateTaskCustomStatusDimension", fn("Tenant_Dim_TaskCustomStatus"))
        {
        }
    }
    public class TenantPopulateTaskStatusDimensionPeriodicHandler : DWPopulatePeriodicHandler
    {
        public TenantPopulateTaskStatusDimensionPeriodicHandler(Func<string, IDWPopulate> fn) 
            : base("TenantPopulateTaskStatusDimension", fn("Tenant_Dim_TaskStatus"))
        {
        }
    }
    public class TenantPopulateTaskTypesDimensionPeriodicHandler : DWPopulatePeriodicHandler
    {
        public TenantPopulateTaskTypesDimensionPeriodicHandler(Func<string, IDWPopulate> fn) 
            : base("TenantPopulateTaskTypesDimension", fn("Tenant_Dim_TaskTypes"))
        {
        }
    }
    public class TenantPopulateUsersDimensionPeriodicHandler : DWPopulatePeriodicHandler
    {
        public TenantPopulateUsersDimensionPeriodicHandler(Func<string, IDWPopulate> fn) 
            : base("TenantPopulateUsersDimension", fn("Tenant_Dim_Users"))
        {
        }
    }
    public class TenantPopulateZoneSitesDimensionPeriodicHandler : DWPopulatePeriodicHandler
    {
        public TenantPopulateZoneSitesDimensionPeriodicHandler(Func<string, IDWPopulate> fn) 
            : base("TenantPopulateZoneSitesDimension", fn("Tenant_Dim_ZoneSites"))
        {
        }
    }
    public class TenantPopulateTaskFactsPeriodicHandler : DWPopulatePeriodicHandler
    {
        public TenantPopulateTaskFactsPeriodicHandler(Func<string, IDWPopulate> fn) 
            : base("TenantPopulateTaskFacts", fn("Tenant_Fact_Tasks"))
        {
        }
    }
}
