﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using NLog;
using ConceptCave.RepositoryInterfaces.Repositories;

namespace VirtualMgr.PeriodicExecution
{
    public class TenantMasterPopulateBuildingFloorsDimensionPeriodicHandler : DWPopulatePeriodicHandler
    {
        public TenantMasterPopulateBuildingFloorsDimensionPeriodicHandler(Func<string, IDWPopulate> fn) 
            : base("TenantMasterPopulateBuildingFloorsDimension", fn("TenantMaster_Dim_BuildingFloors"))
        {
        }
    }
    public class TenantMasterPopulateCompaniesDimensionPeriodicHandler : DWPopulatePeriodicHandler
    {
        public TenantMasterPopulateCompaniesDimensionPeriodicHandler(Func<string, IDWPopulate> fn) 
            : base("TenantMasterPopulateCompaniesDimension", fn("TenantMaster_Dim_Companies"))
        {
        }
    }
    public class TenantMasterPopulateDatesDimensionPeriodicHandler : DWPopulatePeriodicHandler
    {
        public TenantMasterPopulateDatesDimensionPeriodicHandler(Func<string, IDWPopulate> fn) 
            : base("TenantMasterPopulateDatesDimension", fn("TenantMaster_Dim_Dates"))
        {
        }
    }
    public class TenantMasterPopulateDateTimesDimensionPeriodicHandler : DWPopulatePeriodicHandler
    {
        public TenantMasterPopulateDateTimesDimensionPeriodicHandler(Func<string, IDWPopulate> fn) 
            : base("TenantMasterPopulateDateTimesDimension", fn("TenantMaster_Dim_DateTimes"))
        {
        }
    }
    public class TenantMasterPopulateFacilitiesDimensionPeriodicHandler : DWPopulatePeriodicHandler
    {
        public TenantMasterPopulateFacilitiesDimensionPeriodicHandler(Func<string, IDWPopulate> fn) 
            : base("TenantMasterPopulateFacilitiesDimension", fn("TenantMaster_Dim_Facilities"))
        {
        }
    }
    public class TenantMasterPopulateFacilityBuildingsDimensionPeriodicHandler : DWPopulatePeriodicHandler
    {
        public TenantMasterPopulateFacilityBuildingsDimensionPeriodicHandler(Func<string, IDWPopulate> fn) 
            : base("TenantMasterPopulateFacilityBuildingsDimension", fn("TenantMaster_Dim_FacilityBuildings"))
        {
        }
    }
    public class TenantMasterPopulateFloorZonesDimensionPeriodicHandler : DWPopulatePeriodicHandler
    {
        public TenantMasterPopulateFloorZonesDimensionPeriodicHandler(Func<string, IDWPopulate> fn) 
            : base("TenantMasterPopulateFloorZonesDimension", fn("TenantMaster_Dim_FloorZones"))
        {
        }
    }
    public class TenantMasterPopulateRostersDimensionPeriodicHandler : DWPopulatePeriodicHandler
    {
        public TenantMasterPopulateRostersDimensionPeriodicHandler(Func<string, IDWPopulate> fn) 
            : base("TenantMasterPopulateRostersDimension", fn("TenantMaster_Dim_Rosters"))
        {
        }
    }
    public class TenantMasterPopulateSitesDimensionPeriodicHandler : DWPopulatePeriodicHandler
    {
        public TenantMasterPopulateSitesDimensionPeriodicHandler(Func<string, IDWPopulate> fn) 
            : base("TenantMasterPopulateSitesDimension", fn("TenantMaster_Dim_Sites"))
        {
        }
    }

    public class TenantMasterPopulateTaskCustomStatusDimensionPeriodicHandler : DWPopulatePeriodicHandler
    {
        public TenantMasterPopulateTaskCustomStatusDimensionPeriodicHandler(Func<string, IDWPopulate> fn) 
            : base("TenantMasterPopulateTaskCustomStatusDimension", fn("TenantMaster_Dim_TaskCustomStatus"))
        {
        }
    }
    public class TenantMasterPopulateTaskStatusDimensionPeriodicHandler : DWPopulatePeriodicHandler
    {
        public TenantMasterPopulateTaskStatusDimensionPeriodicHandler(Func<string, IDWPopulate> fn) 
            : base("TenantMasterPopulateTaskStatusDimension", fn("TenantMaster_Dim_TaskStatus"))
        {
        }
    }
    public class TenantMasterPopulateTaskTypesDimensionPeriodicHandler : DWPopulatePeriodicHandler
    {
        public TenantMasterPopulateTaskTypesDimensionPeriodicHandler(Func<string, IDWPopulate> fn) 
            : base("TenantMasterPopulateTaskTypesDimension", fn("TenantMaster_Dim_TaskTypes"))
        {
        }
    }
    public class TenantMasterPopulateUsersDimensionPeriodicHandler : DWPopulatePeriodicHandler
    {
        public TenantMasterPopulateUsersDimensionPeriodicHandler(Func<string, IDWPopulate> fn) 
            : base("TenantMasterPopulateUsersDimension", fn("TenantMaster_Dim_Users"))
        {
        }
    }
    public class TenantMasterPopulateZoneSitesDimensionPeriodicHandler : DWPopulatePeriodicHandler
    {
        public TenantMasterPopulateZoneSitesDimensionPeriodicHandler(Func<string, IDWPopulate> fn) 
            : base("TenantMasterPopulateZoneSitesDimension", fn("TenantMaster_Dim_ZoneSites"))
        {
        }
    }
    public class TenantMasterPopulateTaskFactsPeriodicHandler : DWPopulatePeriodicHandler
    {
        public TenantMasterPopulateTaskFactsPeriodicHandler(Func<string, IDWPopulate> fn) 
            : base("TenantMasterPopulateTaskFacts", fn("TenantMaster_Fact_Tasks"))
        {
        }
    }

    public class TenantMasterPopulateTenantsDimensionPeriodicHandler : DWPopulatePeriodicHandler
    {
        public TenantMasterPopulateTenantsDimensionPeriodicHandler(Func<string, IDWPopulate> fn) 
            : base("TenantMasterPopulateTenantsDimension", fn("TenantMaster_Dim_Tenants"))
        {
        }
    }

}
