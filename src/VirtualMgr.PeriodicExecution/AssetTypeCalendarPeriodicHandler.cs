﻿using ConceptCave.BusinessLogic.ScheduleManagement;
using ConceptCave.BusinessLogic.Workloading;
using ConceptCave.Repository.Injected;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.RepositoryInterfaces.TaskEvents;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Converters;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using VirtualMgr.Central;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces;
using ConceptCave.Repository.PeriodicExecution;

namespace VirtualMgr.PeriodicExecution
{
    public class AssetTypeCalendarPeriodicHandler : IPeriodicExecutionHandler
    {
        public interface ICalculateNextDate
        {
            DateTime? CalculateNextDate(DateTime fromDate);
        }

        public class ScheduleExpression : ICalculateNextDate
        {
            public enum Interval
            {
                days = 0,
                weeks = 1,
                months = 2
            }
            public class Simple : ICalculateNextDate
            {
                public int every { get; set; }
                public Interval interval { get; set; }

                public DateTime? CalculateNextDate(DateTime fromDate)
                {
                    switch(interval)
                    {
                        case Interval.days:
                            return fromDate.AddDays(every);
                        case Interval.weeks:
                            return fromDate.AddDays(every * 7);
                        case Interval.months:
                            return fromDate.AddMonths(every);
                    }
                    return null;
                }
            }
            public string type { get; set; }
            public Simple simple { get; set; }
            public bool[] onlydays { get; set; }

            public DateTime? CalculateNextDate(DateTime fromDate)
            {
                DateTime? next = null;
                switch(type)
                {
                    case "simple":
                        next = simple?.CalculateNextDate(fromDate);
                        break;
                }
                
                // Note, if onlydays is all 'false' then ignore, we will use 'next' as it currently stands
                if (next.HasValue && onlydays != null && onlydays.Any(b => b == true))
                {
                    // We know at least 1 entry is true from above, so while is safe ..
                    while(!onlydays[(int)next.Value.DayOfWeek])
                    {
                        next = next.Value.AddDays(1);
                    }
                }
                return next;
            }

        }

        private static readonly Logger log = LogManager.GetCurrentClassLogger();

        private readonly IAssetTypeCalendarRepository _assetTypeCalendarRepo;
        private readonly IAssetRepository _assetRepo;
        private readonly IAssetScheduleRepository _assetScheduleRepo;
        private readonly ITaskRepository _taskRepo;
        private readonly IFacilityStructureRepository _facilityStructureRepo;
        private readonly IHierarchyRepository _hierarchyRepo;
        private readonly IRosterRepository _rosterRepo;
        private readonly IMembershipRepository _membershipRepo;

        private Dictionary<int, FacilityStructureDTO> _facilityStructures = new Dictionary<int, FacilityStructureDTO>();
        private Dictionary<int, HierarchyDTO> _hierarchies = new Dictionary<int, HierarchyDTO>();

        public AssetTypeCalendarPeriodicHandler(
            IAssetTypeCalendarRepository assetTypeCalendarRepo, 
            IAssetRepository assetRepo,
            IAssetScheduleRepository assetScheduleRepo,
            ITaskRepository taskRepo,
            IFacilityStructureRepository facilityStructureRepo,
            IHierarchyRepository hierarchyRepo,
            IRosterRepository rosterRepo,
            IMembershipRepository membershipRepo)
        {
            _assetTypeCalendarRepo = assetTypeCalendarRepo;
            _assetRepo = assetRepo;
            _assetScheduleRepo = assetScheduleRepo;
            _taskRepo = taskRepo;
            _facilityStructureRepo = facilityStructureRepo;
            _hierarchyRepo = hierarchyRepo;
            _rosterRepo = rosterRepo;
            _membershipRepo = membershipRepo;
        }

        public string Name
        {
            get { return "AssetTypeCalendar"; }
        }

        public IPeriodicExecutionHandlerResult Execute(Newtonsoft.Json.Linq.JObject parameters, IContinuousProgressCategory progress)
        {
            log.Info("Start scheduled execution of asset type calendars");
            var result = new PeriodicExecutionHandlerResult();

            try
            {
                var utcNow = DateTime.UtcNow;
                var jlog = new JObject();
                var jDispatch = new JArray();
                jlog.Add("dispatch", jDispatch);

                var jMissing = new JArray();
                jlog.Add("missing", jMissing);

                var jIssues = new JArray();

                var calendars = _assetTypeCalendarRepo.GetAll(AssetTypeCalendarLoadInstructions.CalendarTask);

                // First, find all asset schedules which are due to run now
                var expiredSchedules = _assetScheduleRepo.FindExpired(utcNow, AssetScheduleLoadInstructions.Asset);
                foreach(var schedule in expiredSchedules)
                {
                    var jSchedule = new JObject();
                    jSchedule["asset"] = JObject.FromObject(new { id = schedule.AssetId, name = schedule.Asset?.Name });

                    var calendar = calendars.FirstOrDefault(c => c.Id == schedule.AssetTypeCalendarId);
                    if (calendar != null)
                    {
                        jSchedule["calendar"] = JObject.FromObject(new { id = calendar.Id, name = calendar.Name });
                        DispatchTask(jIssues, schedule, calendar);
                    }
                    
                    // Reschedule for next run
                    schedule.LastRunUtc = schedule.NextRunUtc;  // This has to be none-null to get here
                    schedule.NextRunUtc = CalculateNextRunDate(schedule.LastRunUtc.Value, calendar);
                    _assetScheduleRepo.Save(schedule, false, false);
                    jSchedule["nextRunUtc"] = schedule.NextRunUtc;
                    jDispatch.Add(jSchedule);
                }

                // Ok, now we need to look for Assets which are missing their schedules
                // This can happen if a new Asset is added, or a Calendar for an Asset Type is added/changed
                // Select all Assets of the Calendars Asset Type
                var missingAssetCalendars = _assetRepo.GetAssetsWithMissingCalendarSchedules();
                foreach(var missing in missingAssetCalendars)
                {
                    var assetSchedule = new AssetScheduleDTO()
                    {
                        __IsNew = true,
                        AssetId = missing.Item1.Id,
                        AssetTypeCalendarId = missing.Item2.Id,
                    };
                    assetSchedule.NextRunUtc = CalculateNextRunDate(missing.Item1.DateCreated, missing.Item2);

                    _assetScheduleRepo.Save(assetSchedule, false, false);

                    var jSchedule = new JObject();
                    jSchedule["asset"] = JObject.FromObject(new { id = missing.Item1.Id, name = missing.Item1.Name });
                    jSchedule["calender"] = JObject.FromObject(new { id = missing.Item2.Id, name = missing.Item2.Name });
                    jSchedule["nextRunUtc"] = assetSchedule.NextRunUtc;
                    jMissing.Add(jSchedule);
                }

                log.Info("Completed scheduled execution asset type calendars");

                result.Success = true;
                result.Data = jlog;
                if (jIssues.Count > 0)
                {
                    result.Issues = jIssues;
                }

                progress.Complete();

                return result;
            }
            catch (Exception e)
            {
                LogEventInfo logEvent = new LogEventInfo(LogLevel.Error, "Schedule", "An exception occurred during automated asset type calendars");
                logEvent.Exception = e;
                log.Log(logEvent);

                JObject obj = new JObject();
                obj["Message"] = e.Message;
                obj["StackTrace"] = e.StackTrace;

                result.Success = false;
                result.Data = obj;

                progress.Error(e.Message);

                return result;
            }

        }

        private void DispatchTask(JArray jIssues, AssetScheduleDTO schedule, AssetTypeCalendarDTO calendar)
        {
            foreach (AssetTypeCalendarTaskDTO calendarTask in calendar.AssetTypeCalendarTasks)
            {
                var roleId = calendarTask.HierarchyRoleId;
                var facilityId = schedule.Asset.FacilityStructureId;
                if (facilityId == null)
                {
                    // Asset has no facility, cannot lookup the user for the role and dispatch the task
                    var jIssue = new JObject();
                    jIssue["assetId"] = schedule.AssetId;
                    jIssue["calendarTaskId"] = calendarTask.Id;
                    jIssue["hierarchyRoleId"] = calendarTask.HierarchyRoleId;
                    jIssue["type"] = "AssetMissingFacility";
                    jIssue["message"] = string.Format("Cannot dispatch Task '{0}' with Role '{1}' for Asset '{2}' since the Asset has no Facility defined",
                        calendarTask.ProjectJobTaskType.Name, calendarTask.HierarchyRole.Name, schedule.Asset.Name);
                    jIssues.Add(jIssue);
                    continue;
                }

                // Get the Assets Facility Structure
                FacilityStructureDTO facilityDto = null;
                if (!_facilityStructures.TryGetValue(facilityId.Value, out facilityDto))
                {
                    var dto = _facilityStructureRepo.GetById(facilityId.Value, FacilityStructureLoadInstructions.ParentDeep);
                    while (dto != null)
                    {
                        _facilityStructures[dto.Id] = dto;
                        dto = dto.Parent;
                    }

                    _facilityStructures.TryGetValue(facilityId.Value, out facilityDto);
                }

                // Walk up the Facility Structure until we find a valid Hierarchy
                var facilityWithHierarchy = facilityDto;
                while (facilityWithHierarchy != null && !facilityWithHierarchy.HierarchyId.HasValue)
                {
                    facilityWithHierarchy = facilityWithHierarchy.Parent;
                }

                if (facilityWithHierarchy == null)
                {
                    var jIssue = new JObject();
                    jIssue["assetId"] = schedule.AssetId;
                    jIssue["calendarTaskId"] = calendarTask.Id;
                    jIssue["hierarchyRoleId"] = calendarTask.HierarchyRoleId;
                    jIssue["facilityStructureId"] = facilityDto.Id;
                    jIssue["type"] = "MissingFacilityHierarchy";
                    jIssue["message"] = string.Format("Cannot dispatch Task '{0}' with Role '{1}' for Asset '{2}' since the Asset's Facility '{3}' has no Hierarchy defined",
                        calendarTask.ProjectJobTaskType.Name, calendarTask.HierarchyRole.Name, schedule.Asset.Name, facilityDto.Name);
                    jIssues.Add(jIssue);
                    continue;
                }
                HierarchyDTO hierarchyDto = null;
                if (!_hierarchies.TryGetValue(facilityWithHierarchy.HierarchyId.Value, out hierarchyDto))
                {
                    hierarchyDto = _hierarchyRepo.GetHierarchy(facilityWithHierarchy.HierarchyId.Value);
                    _hierarchies[hierarchyDto.Id] = hierarchyDto;
                }
                // Lookup the Role within the Hierarchy ..
                var buckets = _hierarchyRepo.GetBucketsForHierarchy(hierarchyDto.Id, calendarTask.HierarchyRoleId, HierarchyBucketLoadInstructions.User);
                var primary = buckets.FirstOrDefault(b => b.UserId.HasValue);
                if (primary == null)
                {
                    var jIssue = new JObject();
                    jIssue["assetId"] = schedule.AssetId;
                    jIssue["calendarTaskId"] = calendarTask.Id;
                    jIssue["hierarchyRoleId"] = calendarTask.HierarchyRoleId;
                    jIssue["facilityStructureId"] = facilityWithHierarchy.Id;
                    jIssue["hierarchyId"] = hierarchyDto.Id;
                    jIssue["type"] = "MissingUserForHierarchyRole";
                    jIssue["message"] = string.Format("Cannot dispatch Task '{0}' with Role '{1}' for Asset '{2}' since the Asset's Hierarchy '{3}' does not have a User for the Role",
                        calendarTask.ProjectJobTaskType.Name, calendarTask.HierarchyRole.Name, schedule.Asset.Name, hierarchyDto.Name);
                    jIssues.Add(jIssue);
                    continue;
                }
                var userId = primary.UserId;

                // See if there is an override for this user
                var overrides = _hierarchyRepo.GetBucketOverrides(DateTime.UtcNow, primary.UserId.Value, primary.Id);
                var overrideUserId = overrides.FirstOrDefault(o => o.UserId.HasValue)?.UserId;
                if (overrideUserId != null)
                {
                    userId = overrideUserId;
                }

                var rosters = _rosterRepo.GetForHierarchies(new int[] { hierarchyDto.Id });
                var rosterDto = rosters.FirstOrDefault();
                if (rosterDto == null)
                {
                    var jIssue = new JObject();
                    jIssue["assetId"] = schedule.AssetId;
                    jIssue["calendarTaskId"] = calendarTask.Id;
                    jIssue["hierarchyRoleId"] = calendarTask.HierarchyRoleId;
                    jIssue["facilityStructureId"] = facilityWithHierarchy.Id;
                    jIssue["hierarchyId"] = hierarchyDto.Id;
                    jIssue["type"] = "MissingRosterForHierarchy";
                    jIssue["message"] = string.Format("Cannot dispatch Task '{0}' with Role '{1}' for Asset '{2}' since the Asset's Hierarchy '{3}' does not have a Roster",
                        calendarTask.ProjectJobTaskType.Name, calendarTask.HierarchyRole.Name, schedule.Asset.Name, hierarchyDto.Name);
                    jIssues.Add(jIssue);
                    continue;
                }

                var taskDto = _taskRepo.New(
                    string.IsNullOrEmpty(calendarTask.TaskName) ? calendarTask.ProjectJobTaskType.Name : calendarTask.TaskName,
                    calendarTask.TaskDescription, 
                    userId.Value, 
                    rosterDto.Id, 
                    calendarTask.TaskTypeId, 
                    primary.Id, 
                    _membershipRepo, 
                    userId.Value, 
                    calendarTask.TaskSiteId, 
                    ProjectJobTaskStatus.Approved,
                    null, 
                    schedule.AssetId, 
                    calendarTask.TaskLevel, 
                    calendarTask.TaskLengthInDays.HasValue ? calendarTask.TaskLengthInDays.ToString() : null);

                var startTimeLocal = schedule.OverrideStartTimeLocal ?? calendarTask.StartTimeLocal;
                if (startTimeLocal.HasValue)
                {
                    var tzi = calendar.TimeZone != null ? TimeZoneInfo.FindSystemTimeZoneById(calendar.TimeZone) : MultiTenantManager.CurrentTenant.TimeZoneInfo;
                    var taskRunLocal = TimeZoneInfo.ConvertTimeToUtc(schedule.NextRunUtc.Value, tzi);
                    var localStartTime = taskRunLocal.Date.Add(startTimeLocal.Value);
                    localStartTime = DateTime.SpecifyKind(localStartTime, DateTimeKind.Unspecified);
                    var utcStartTime = TimeZoneInfo.ConvertTime(localStartTime, tzi, TimeZoneInfo.Utc);
                    taskDto.StartTimeUtc = new DateTimeOffset(localStartTime, tzi.GetUtcOffset(utcStartTime));
                }

                foreach (var label in calendarTask.AssetTypeCalendarTaskLabels)
                {
                    taskDto.ProjectJobTaskLabels.Add(new ProjectJobTaskLabelDTO()
                    {
                        __IsNew = true,
                        LabelId = label.LabelId,
                        ProjectJobTask = taskDto
                    });
                }
                _taskRepo.Save(taskDto, false, true);
            }
        }

        private DateTime? CalculateNextRunDate(DateTime fromDateUtc, AssetTypeCalendarDTO calendar)
        {
            fromDateUtc = DateTime.SpecifyKind(fromDateUtc, DateTimeKind.Utc);
            var expression = calendar.ScheduleExpression.JsonDeserialize<ScheduleExpression>();

            var tzi = calendar.TimeZone != null ? TimeZoneInfo.FindSystemTimeZoneById(calendar.TimeZone) : MultiTenantManager.CurrentTenant.TimeZoneInfo;
            var fromDateLocal = TimeZoneInfo.ConvertTimeFromUtc(fromDateUtc, tzi);
            var nextDate = expression?.CalculateNextDate(fromDateLocal);
            if (!nextDate.HasValue)
            {
                return null;
            }
            var nextDateUtc = TimeZoneInfo.ConvertTimeToUtc(nextDate.Value, tzi);
            return nextDateUtc;
        }
    }
}
