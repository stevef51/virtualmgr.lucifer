﻿using ConceptCave.BusinessLogic.Json;
using ConceptCave.BusinessLogic.ScheduleManagement;
using ConceptCave.BusinessLogic.Workloading;
using ConceptCave.Repository.Injected;
using ConceptCave.Repository.PeriodicExecution;
using ConceptCave.RepositoryInterfaces;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.RepositoryInterfaces.ScheduleManagement;
using ConceptCave.RepositoryInterfaces.TaskEvents;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using VirtualMgr.Central;

namespace VirtualMgr.PeriodicExecution
{
    public class EndOfDayPeriodicHandler : IPeriodicExecutionHandler
    {
        private readonly IRosterRepository _rosterRepo;
        private readonly IRosterTaskEventsManager _rosterTaskEventsManager;
        private readonly EndOfDayManagement _endOfDayManagement;

        private static readonly Logger log = LogManager.GetCurrentClassLogger();

        public EndOfDayPeriodicHandler(IRosterRepository rosterRepo, IRosterTaskEventsManager rosterTaskEventsManager, EndOfDayManagement endOfDayManagement)
        {
            _rosterRepo = rosterRepo;
            _rosterTaskEventsManager = rosterTaskEventsManager;
            _endOfDayManagement = endOfDayManagement;
            _endOfDayManagement.MarkAsFinishedBySystem = true;
        }

        public string Name
        {
            get { return "EndOfDay"; }
        }

        public IPeriodicExecutionHandlerResult Execute(Newtonsoft.Json.Linq.JObject parameters, IContinuousProgressCategory progress)
        {
            log.Info("Start scheduled execution of roster end of day");
            var result = new PeriodicExecutionHandlerResult();

            try
            {

                var utcNow = DateTime.UtcNow;
                JObject quartzLog = new JObject();
                result.Data = quartzLog;

                JArray rostersLog = new JArray();
                quartzLog["Rosters"] = rostersLog;

                quartzLog["UTCTime"] = utcNow;

                var localTime = TimeZoneInfo.ConvertTimeFromUtc(utcNow, MultiTenantManager.CurrentTenant.TimeZoneInfo);
                quartzLog["LocalTime"] = localTime;
                log.Debug("current time is {0} in {1}", localTime, MultiTenantManager.CurrentTenant.TimeZoneInfo.DisplayName);

                var rosters = _rosterRepo.GetAll(ConceptCave.RepositoryInterfaces.Enums.RosterLoadInstructions.Conditions | ConceptCave.RepositoryInterfaces.Enums.RosterLoadInstructions.ConditionActions);

                log.Info("Found {0} rosters to be processed for end of day", rosters.Count);
                quartzLog["RosterCount"] = rosters.Count;
                if (rosters.Count == 0)
                {
                    result.Success = true;

                    return result;
                }

                foreach (var roster in rosters)
                {
                    using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(0, 0, 10, 0, 0)))
                    {
                        JObject rosterLog = new JObject();
                        rostersLog.Add(rosterLog);

                        rosterLog["id"] = roster.Id;
                        log.Debug("Starting processing of roster {0}", roster.Id);

                        var tasks = _endOfDayManagement.Approve(roster, utcNow);

                        log.Debug("{0} tasks were found and processed for roster {1} at UTC time {2}", tasks.Count, roster.Name, utcNow);
                        rosterLog["TasksProcessed"] = tasks.Count;

                        rosterLog["RosterEventTasksCandidates"] = tasks.Count;

                        _rosterTaskEventsManager.Run(TaskEventStage.EndOfDayAutomatic, tasks, (from c in roster.RosterEventConditions where c.NextRunTime.HasValue == false select c).ToList());

                        log.Debug("Completed processing of roster {0}", roster.Id);

                        scope.Complete();
                    }
                }

                log.Info("Completed scheduled execution roster end of day");

                result.Success = true;

                progress.Complete();

                return result;
            }
            catch (Exception e)
            {
                LogEventInfo logEvent = new LogEventInfo(LogLevel.Error, "Schedule", "An exception occurred during automated end of day processing");
                logEvent.Exception = e;
                log.Log(logEvent);

                JObject obj = new JObject();
                obj["Message"] = e.Message;
                obj["StackTrace"] = e.StackTrace;

                result.Success = false;
                result.Data = obj;

                progress.Error(e.Message);

                return result;
            }
        }
    }
}
