﻿using ConceptCave.BusinessLogic.ScheduleManagement;
using ConceptCave.BusinessLogic.Workloading;
using ConceptCave.Repository.Injected;
using ConceptCave.Repository.PeriodicExecution;
using ConceptCave.RepositoryInterfaces;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.RepositoryInterfaces.TaskEvents;
using Newtonsoft.Json.Linq;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using VirtualMgr.Central;

namespace VirtualMgr.PeriodicExecution
{
    public class RosterTimedEventsPeriodicHandler : IPeriodicExecutionHandler
    {
        private readonly IRosterRepository _rosterRepo;
        private readonly ITaskRepository _taskRepo;
        private readonly ScheduleApprovalManagement _scheduleApprovalManagement;
        private readonly IRosterTaskEventsManager _rosterTaskEventsManager;

        private static readonly Logger log = LogManager.GetCurrentClassLogger();

        public RosterTimedEventsPeriodicHandler(IRosterRepository rosterRepo, ITaskRepository taskRepo, ScheduleApprovalManagement scheduleApprovalManagement, IRosterTaskEventsManager rosterTaskEventsManager)
        {
            _rosterRepo = rosterRepo;
            _taskRepo = taskRepo;
            _scheduleApprovalManagement = scheduleApprovalManagement;
            _rosterTaskEventsManager = rosterTaskEventsManager;
        }

        public string Name
        {
            get { return "RosterTimedEvents"; }
        }

        public IPeriodicExecutionHandlerResult Execute(Newtonsoft.Json.Linq.JObject parameters, IContinuousProgressCategory progress)
        {
            log.Info("Start scheduled execution of roster timed events");
            var result = new PeriodicExecutionHandlerResult();
            try
            {
                var utcNow = DateTime.UtcNow;
                JObject quartzLog = new JObject();
                result.Data = quartzLog;

                JArray rostersLog = new JArray();
                quartzLog["Rosters"] = rostersLog;

                quartzLog["UTCTime"] = utcNow;

                var localTime = TimeZoneInfo.ConvertTimeFromUtc(utcNow, MultiTenantManager.CurrentTenant.TimeZoneInfo);
                quartzLog["LocalTime"] = localTime;

                log.Debug("current time is {0} in {1}", localTime, MultiTenantManager.CurrentTenant.TimeZoneInfo.DisplayName);

                // we get all rosters and their actions and will work out which timed events should be run
                var rosters = _rosterRepo.GetAll(RosterLoadInstructions.Conditions | RosterLoadInstructions.ConditionActions);

                log.Info("Found {0} rosters to be approved", rosters.Count);
                quartzLog["RosterCount"] = rosters.Count;

                if (rosters.Count == 0)
                {
                    result.Success = true;

                    return result;
                }

                foreach (var roster in rosters)
                {
                    JObject rosterLog = new JObject();
                    rostersLog.Add(rosterLog);

                    rosterLog["id"] = roster.Id;

                    var conditions = (from c in roster.RosterEventConditions where c.NextRunTime.HasValue == true && c.NextRunTime.Value <= utcNow select c);

                    log.Info("There are {0} timed event conditions to be run for roster {1}", conditions.Count(), roster.Name);
                    rosterLog["TimedConditionsToRun"] = conditions.Count();
                    if (conditions.Count() == 0)
                    {
                        continue;
                    }

                    var rosterTimezone = TimeZoneInfo.FindSystemTimeZoneById(roster.Timezone);

                    var rosterStart = _scheduleApprovalManagement.GetStartOfCurrentShiftInTimezone(roster, rosterTimezone);
                    var rosterEnd = _scheduleApprovalManagement.GetEndOfCurrentShiftInTimezone(roster, rosterTimezone);

                    var rosterStartUtc = TimeZoneInfo.ConvertTimeToUtc(rosterStart, rosterTimezone);
                    var rosterEndUtc = TimeZoneInfo.ConvertTimeToUtc(rosterEnd, rosterTimezone);

                    DateTime localNow = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, rosterTimezone);

                    rosterLog["StartRosterProcessingUTC"] = DateTime.UtcNow;
                    rosterLog["RosterStart"] = rosterStart;
                    rosterLog["RosterEnd"] = rosterEnd;
                    rosterLog["RosterTimezone"] = roster.Timezone;

                    // get all of the tasks created between the roster start and end
                    var tasks = _taskRepo.GetForRoster(roster.Id, rosterStartUtc, rosterEndUtc, ConceptCave.RepositoryInterfaces.Repositories.TaskStatusFilter.All, ProjectJobTaskLoadInstructions.None);

                    rosterLog["TaskCount"] = tasks.Count;

                    using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(0, 0, 10, 0)))
                    {
                        var cs = conditions.ToList();

                        _rosterTaskEventsManager.Run(TaskEventStage.Time, tasks, cs);

                        _rosterTaskEventsManager.SetNextUTCRunTime(localNow, rosterTimezone, cs);

                        _rosterRepo.Save(roster, false, true);

                        scope.Complete();
                    }
                }

                log.Info("Completed scheduled execution roster timed");

                result.Success = true;

                progress.Complete();

                return result;
            }
            catch (Exception e)
            {
                LogEventInfo logEvent = new LogEventInfo(LogLevel.Error, "Schedule", "An exception occurred during the execution of roster timed events");
                logEvent.Exception = e;
                log.Log(logEvent);

                JObject obj = new JObject();
                obj["Message"] = e.Message;
                obj["StackTrace"] = e.StackTrace;

                result.Success = false;
                result.Data = obj;

                progress.Error(e.Message);

                return result;
            }
        }
    }
}
