﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.BusinessLogic;
using ConceptCave.BusinessLogic.Json;
using ConceptCave.BusinessLogic.LingoDataObjects;
using ConceptCave.Checklist.Lingo;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.Repository.PeriodicExecution;
using ConceptCave.RepositoryInterfaces;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace VirtualMgr.PeriodicExecution
{
    public class ExecutePublishedChecklistPEJob : IPeriodicExecutionHandler
    {
        public static string PublishingGroupResourceIdName = "PublishingGroupResourceId";
        public static string ReviewerIdName = "ReviewerId";
        public static string RevieweeIdName = "RevieweeId";
        private readonly IWorkingDocumentStateManager _stateMgr;

        public ExecutePublishedChecklistPEJob(IWorkingDocumentStateManager stateMgr)
        {
            _stateMgr = stateMgr;
        }

        public string Name => "Execute Published Checklist";

        public IPeriodicExecutionHandlerResult Execute(JObject parameters, IContinuousProgressCategory progress)
        {
            var result = new PeriodicExecutionHandlerResult();
            result.Success = false;
            var jIssues = new JArray();

            var jPgrId = parameters[PublishingGroupResourceIdName];
            var jReviewerId = parameters[ReviewerIdName];

            if (jPgrId == null)
            {
                jIssues.Add("Missing publishing resource group id");
            }
            if (jReviewerId == null)
            {
                jIssues.Add("Missing reviewer id");
            }
            if (jIssues.Count == 0)
            {
                try
                {
                    _stateMgr.AllowSave = false;

                    int publishingGroupResourceId = jPgrId.Value<int>();
                    Guid reviewerId = Guid.Parse(jReviewerId.Value<string>());
                    Guid? revieweeId = parameters[RevieweeIdName] != null ? parameters[RevieweeIdName].Type != JTokenType.Null ? (Guid?)Guid.Parse(parameters[RevieweeIdName].Value<string>()) : null : null;

                    // feed through any context stuff we have so that its available to the working document when its run
                    Dictionary<string, object> args = null;
                    var jExtra = parameters["extra"] as JObject;
                    if (jExtra != null)
                    {
                        args = new Dictionary<string, object>();
                        foreach (var obj in jExtra)
                        {
                            switch(obj.Value.Type)
                            {
                                case JTokenType.Boolean:
                                    args.Add(obj.Key, obj.Value.Value<bool>());
                                    break;
                                case JTokenType.Date:
                                    args.Add(obj.Key, obj.Value.Value<DateTime>());
                                    break;
                                case JTokenType.Float:
                                    args.Add(obj.Key, obj.Value.Value<double>());
                                    break;
                                case JTokenType.Guid:
                                    args.Add(obj.Key, obj.Value.Value<Guid>());
                                    break;
                                case JTokenType.Integer:
                                    args.Add(obj.Key, obj.Value.Value<Int64>());
                                    break;
                                case JTokenType.Null:
                                    args.Add(obj.Key, null);
                                    break;
                                case JTokenType.String:
                                    args.Add(obj.Key, obj.Value.Value<string>());
                                    break;
                                case JTokenType.TimeSpan:
                                    args.Add(obj.Key, obj.Value.Value<TimeSpan>());
                                    break;
                            }
                        }
                    }

                    var workingDocContainer = _stateMgr.BeginWorkingDocument(publishingGroupResourceId, reviewerId, null, null, args);
                    var workingEntity = workingDocContainer.DocumentEntity;
                    var wd = workingDocContainer.DocumentObject;

                    CurrentContextFacilityCurrentUsers currentUsers = new CurrentContextFacilityCurrentUsers() { ReviewerId = reviewerId, RevieweeId = revieweeId.HasValue ? revieweeId.Value : reviewerId };
                    wd.Program.Set<CurrentContextFacilityCurrentUsers>(currentUsers);
                    wd.Program.Set<WorkingDocumentDTO>(workingEntity);

                    wd.Next();

                    _stateMgr.FinishWorkingDocument(wd, ref workingEntity);

                    result.Success = true;
                }
                catch (LingoException le)
                {
                    JObject obj = new JObject();
                    obj["Message"] = le.Message;
                    obj["StackTrace"] = le.StackTrace;
                    obj["Position"] = le.Ast.Location.Position;
                    obj["Line"] = le.Ast.Location.Line;
                    obj["SourceCode"] = le.ProgramDefinition.SourceCode;

                    JArray vars = new JArray();
                    foreach (var v in le.Program.WorkingDocument.Variables)
                    {
                        JObject var = new JObject();
                        string name = "";
                        string value = "";

                        if (string.IsNullOrEmpty(v.Name) == false)
                        {
                            name = v.Name;
                        }

                        if (v.ObjectValue != null)
                        {
                            value = v.ObjectValue.ToString();
                        }

                        var["Name"] = name;
                        var["Value"] = value;

                        vars.Add(var);
                    }
                    obj["Variables"] = vars;

                    var settings = new JsonSerializerSettings();
                    LowercaseContractResolver resolver = new LowercaseContractResolver();
                    settings.ContractResolver = resolver;
                    settings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;

                    throw le;
                }
                catch (Exception e)
                {
                    JObject obj = new JObject();
                    obj["Message"] = e.Message;
                    obj["StackTrace"] = e.StackTrace;

                    var settings = new JsonSerializerSettings();
                    LowercaseContractResolver resolver = new LowercaseContractResolver();
                    settings.ContractResolver = resolver;
                    settings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;

                    throw e;
                }
            }

            if (jIssues.Count > 0)
            {
                result.Issues = jIssues;
            }
            return result;
        }
    }
}
