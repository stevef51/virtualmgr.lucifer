﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConceptCave.Repository;
using ConceptCave.Repository.PeriodicExecution;
using ConceptCave.RepositoryInterfaces;
using Newtonsoft.Json.Linq;
using NLog;

namespace VirtualMgr.PeriodicExecution
{
    public class RolesCheckerPeriodicHandler : IPeriodicExecutionHandler
    {
        private static readonly Logger log = LogManager.GetCurrentClassLogger();
        public string Name => "RolesCheck";

        public IPeriodicExecutionHandlerResult Execute(JObject parameters, IContinuousProgressCategory progress)
        {
            log.Info("Start scheduled execution of roles check");
            var result = new PeriodicExecutionHandlerResult();
            try
            {
                var missing = RoleRepository.CheckCoreRolesExist();

                if(missing.Length > 0)
                {
                    JObject obj = new JObject();
                    obj["Message"] = "The set of roles defined in the database does not match those defined in the RolesRepository CoreRoles dictionary. This could indicate an update script has not run or the release could possibly be missing a script.";
                    obj["MissingRoles"] = string.Join(",", missing);
                    result.Success = false;
                    result.Data = obj;

                    return result;
                }

                log.Info("Completed scheduled execution of media policy management");
                result.Success = true;

                progress.Complete();

                return result;
            }
            catch (Exception e)
            {
                LogEventInfo logEvent = new LogEventInfo(LogLevel.Error, "Schedule", "An exception occurred during media policy management");
                logEvent.Exception = e;
                log.Log(logEvent);

                JObject obj = new JObject();
                obj["Message"] = e.Message;
                obj["StackTrace"] = e.StackTrace;

                result.Success = false;
                result.Data = obj;

                progress.Error(e.Message);

                return result;
            }
        }
    }
}
