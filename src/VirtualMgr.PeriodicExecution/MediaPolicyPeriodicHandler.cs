﻿using ConceptCave.Repository.Injected;
using ConceptCave.Repository.PeriodicExecution;
using ConceptCave.Repository.PolicyManagement;
using ConceptCave.RepositoryInterfaces;
using ConceptCave.RepositoryInterfaces.PolicyManagement;
using Newtonsoft.Json.Linq;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualMgr.PeriodicExecution
{
    public class MediaPolicyPeriodicHandler : IPeriodicExecutionHandler
    {
        private static readonly Logger log = LogManager.GetCurrentClassLogger();

        private readonly IMediaPolicyManager _mediaPolicyManager;

        public MediaPolicyPeriodicHandler(IMediaPolicyManager mediaPolicyManager)
        {
            _mediaPolicyManager = mediaPolicyManager;
        }

        public string Name
        {
            get { return "MediaPolicy"; }
        }

        public IPeriodicExecutionHandlerResult Execute(Newtonsoft.Json.Linq.JObject parameters, IContinuousProgressCategory progress)
        {
            log.Info("Start scheduled execution of media policy management");
            var result = new PeriodicExecutionHandlerResult();
            try
            {
                _mediaPolicyManager.Run();

                log.Info("Completed scheduled execution of media policy management");
                result.Success = true;

                progress.Complete();

                return result;
            }
            catch (Exception e)
            {
                LogEventInfo logEvent = new LogEventInfo(LogLevel.Error, "Schedule", "An exception occurred during media policy management");
                logEvent.Exception = e;
                log.Log(logEvent);

                JObject obj = new JObject();
                obj["Message"] = e.Message;
                obj["StackTrace"] = e.StackTrace;

                result.Success = false;
                result.Data = obj;

                progress.Error(e.Message);

                return result;
            }
        }
    }
}
