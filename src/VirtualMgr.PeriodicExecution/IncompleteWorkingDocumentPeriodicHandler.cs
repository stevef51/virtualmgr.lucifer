﻿using ConceptCave.Repository;
using ConceptCave.Repository.PeriodicExecution;
using ConceptCave.RepositoryInterfaces;
using Newtonsoft.Json.Linq;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualMgr.PeriodicExecution
{
    public class IncompleteWorkingDocumentPeriodicHandler : IPeriodicExecutionHandler
    {
        private static readonly Logger log = LogManager.GetCurrentClassLogger();
        public string Name
        {
            get { return "IncompleteWorkingDocuments"; }
        }

        public IPeriodicExecutionHandlerResult Execute(Newtonsoft.Json.Linq.JObject parameters, IContinuousProgressCategory progress)
        {
            log.Info("Start scheduled execution of deletion of incomplete workng documents");

            DateTime maxDate = DateTime.UtcNow;

            log.Debug("using {0} as max date for deletion of incomplete working documents", maxDate);

            var result = new PeriodicExecutionHandlerResult();

            try
            {
                WorkingDocumentRepository.DeleteIncompleteWorkingDocuments(maxDate);

                log.Info("Completed scheduled execution of deletion of incomplete workng documents");

                result.Success = true;

                progress.Complete();

                return result;
            }
            catch (Exception e)
            {
                LogEventInfo logEvent = new LogEventInfo(LogLevel.Error, "Schedule", "An exception occurred during the deletion of incomplete working documents");
                logEvent.Exception = e;
                log.Log(logEvent);

                JObject obj = new JObject();
                obj["Message"] = e.Message;
                obj["StackTrace"] = e.StackTrace;

                result.Success = false;
                result.Data = obj;

                progress.Error(e.Message);

                return result;
            }
        }
    }
}
