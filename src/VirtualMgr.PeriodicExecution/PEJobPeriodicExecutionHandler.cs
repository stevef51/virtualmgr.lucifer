﻿using ConceptCave.BusinessLogic.ScheduleManagement;
using ConceptCave.BusinessLogic.Workloading;
using ConceptCave.Repository.Injected;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.RepositoryInterfaces.TaskEvents;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Converters;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using VirtualMgr.Central;
using ConceptCave.DTO.DTOClasses;
using ConceptCave.RepositoryInterfaces;
using Quartz;
using System.Diagnostics;
using ConceptCave.Repository.PeriodicExecution;

namespace VirtualMgr.PeriodicExecution
{
    public enum CustomJobPeriodicExecutionType
    {
        ExecutePublishedChecklist,
        eWayProcessPendingPayment,
    }

    public class CustomJobPeriodicHandler : IPeriodicExecutionHandler
    {
        private static readonly Logger log = LogManager.GetCurrentClassLogger();

        private readonly IPEJobRepository _peJobRepo;

        private readonly Func<string, IPeriodicExecutionHandler> _fnHandler;

        public CustomJobPeriodicHandler(
            Func<string, IPeriodicExecutionHandler> handlerTypes,
            IPEJobRepository peJobRepo)
        {
            _peJobRepo = peJobRepo;
            _fnHandler = handlerTypes;
        }

        public string Name
        {
            get { return "Custom PE Jobs"; }
        }

        public IPeriodicExecutionHandlerResult Execute(Newtonsoft.Json.Linq.JObject parameters, IContinuousProgressCategory progress)
        {
            log.Info("Start scheduled execution of custom PE Jobs");
            var result = new PeriodicExecutionHandlerResult();
            result.Success = true;

            try
            {
                var jlog = new JObject();
                var jJobs = new JArray();
                jlog.Add("Jobs", jJobs);
                result.Data = jlog;

                var jobs = _peJobRepo.FindOverdue(DateTime.UtcNow);

                foreach(var job in jobs)
                {
                    var thisRunUtc = DateTime.UtcNow;

                    var jJob = new JObject();
                    jJob["Job"] = JObject.FromObject(new { id = job.Id, name = job.Name });
                    jJobs.Add(jJob);

                    var jobProgress = progress.NewCategory(job.Name);
                    try
                    {
                        Stopwatch sw = new Stopwatch();

                        sw.Start();
                        var jobresult = RunJob(job, jJob, progress, thisRunUtc);
                        sw.Stop();
                        jJob["ExecutionTime"] = sw.Elapsed;
                        jJob["Success"] = jobresult.Success;
                        if (jobresult.Issues != null)
                            jJob["Issues"] = jobresult.Issues;
                        if (jobresult.Data != null)
                            jJob["Data"] = jobresult.Data;
                    }
                    catch (Exception e)
                    {
                        LogEventInfo logEvent = new LogEventInfo(LogLevel.Error, "Schedule", string.Format("An exception occurred during Custom PE Job {0} ({1})", job.Name, job.Id));
                        logEvent.Exception = e;
                        log.Log(logEvent);

                        jJob["Exception"] = JObject.FromObject(new { Message = e.Message, StackTrace = e.StackTrace });
                        result.Success = false;

                        jobProgress.Error(e.Message);
                    }

                    var expression = new CronExpression(job.Cronexpression);
                    var currentTz = MultiTenantManager.CurrentTenant.TimeZoneInfo;

                    expression.TimeZone = currentTz;

                    // GetTimeAfter expects a UTC time and returns a UTC time, it uses the above TimeZone only when
                    // the Cron expression requires a fixed Hour like "Every day at 4pm" (0 0 16 1/1 * ? *)
                    // for Cron expressions like "Every 10 mins" (0 0/10 * 1/1 * ? *) the TimeZone is not used
                    var nextRunTime = expression.GetTimeAfter(new DateTimeOffset(thisRunUtc));

                    job.LastRunUtc = thisRunUtc;
                    job.NextRunUtc = nextRunTime.Value.DateTime;            // GetTimeAfter returns UTC so no conversion back necessary

                    _peJobRepo.Save(job, false);

                    var history = new PEJobHistoryDTO()
                    {
                        __IsNew = true,
                        JobId = job.Id,
                        RanAtUtc = thisRunUtc,
                        Success = true
                    };
                    _peJobRepo.Save(history, false);
                }

                log.Info("Completed scheduled execution custom PE Jobs");

                result.Data = jlog;

                progress.Complete();

                return result;
            }
            catch (Exception e)
            {
                LogEventInfo logEvent = new LogEventInfo(LogLevel.Error, "Schedule", "An exception occurred during automated custom PE Jobs");
                logEvent.Exception = e;
                log.Log(logEvent);

                JObject obj = new JObject();
                obj["Message"] = e.Message;
                obj["StackTrace"] = e.StackTrace;

                result.Success = false;
                result.Data = obj;

                progress.Error(e.Message);

                return result;
            }

        }

        private IPeriodicExecutionHandlerResult RunJob(PEJobDTO job, JObject jJob, IContinuousProgressCategory progress, DateTime thisRunUtc)
        {
            var handler = _fnHandler(job.JobType);
            if (handler == null)
            {
                throw new ApplicationException("JobType not found " + job.JobType);
            }

            JObject parameters = string.IsNullOrEmpty(job.JobData) ? new JObject() : JObject.Parse(job.JobData);
            JObject extra = (JObject)parameters["extra"];
            if (extra == null)
            {
                extra = new JObject();
                parameters["extra"] = extra;
            }

            extra["_lastrunutc"] = job.LastRunUtc;
            extra["_thisrunutc"] = thisRunUtc;          

            return handler.Execute(parameters, progress);
        }
    }
}
