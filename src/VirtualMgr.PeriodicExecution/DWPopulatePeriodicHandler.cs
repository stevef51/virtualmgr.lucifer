﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using NLog;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.RepositoryInterfaces;
using ConceptCave.Repository.PeriodicExecution;

namespace VirtualMgr.PeriodicExecution
{
    public abstract class DWPopulatePeriodicHandler: IPeriodicExecutionHandler  
    {
        private static readonly Logger log = LogManager.GetCurrentClassLogger();

        private IDWPopulate _dwRepo;
        private string _name;
        public string Name => _name;

        public DWPopulatePeriodicHandler(string name, IDWPopulate dwRepo)
        {
            _name = name;
            _dwRepo = dwRepo;
        }

        public IPeriodicExecutionHandlerResult Execute(JObject parameters, IContinuousProgressCategory progress)
        {
            // ok we are pretty simple, we just call the stored proc who does all our work for us
            log.Info("Start scheduled execution of " + _name + " population");
            var result = new PeriodicExecutionHandlerResult();
            try
            {
                int tenantId = VirtualMgr.Central.MultiTenantManager.CurrentTenant.Id;

                var affectedCount = _dwRepo.Populate(tenantId, progress);
                result.Data = new JObject();
                result.Data["Affected"] = affectedCount;

                log.Info("Completed scheduled execution of " + _name + " population");

                result.Success = true;

                progress.Complete(string.Format("{0} Affected", affectedCount));

                return result;
            }
            catch (Exception e)
            {
                LogEventInfo logEvent = new LogEventInfo(LogLevel.Error, "Schedule", "An exception occurred during the execution of populating the datawarehouse " + _name);
                logEvent.Exception = e;
                log.Log(logEvent);

                JObject obj = new JObject();
                obj["Message"] = e.Message;
                obj["StackTrace"] = e.StackTrace;

                result.Success = false;
                result.Data = obj;

                progress.Error(e.Message);

                return result;
            }
        }
    }
}
