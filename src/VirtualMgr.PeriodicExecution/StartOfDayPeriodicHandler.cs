﻿using ConceptCave.BusinessLogic.ScheduleManagement;
using ConceptCave.BusinessLogic.Workloading;
using ConceptCave.Repository.Injected;
using ConceptCave.Repository.PeriodicExecution;
using ConceptCave.RepositoryInterfaces;
using ConceptCave.RepositoryInterfaces.Enums;
using ConceptCave.RepositoryInterfaces.Repositories;
using ConceptCave.RepositoryInterfaces.TaskEvents;
using Newtonsoft.Json.Linq;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using VirtualMgr.Central;

namespace VirtualMgr.PeriodicExecution
{
    public class StartOfDayPeriodicHandler : IPeriodicExecutionHandler
    {
        private readonly IRosterRepository _rosterRepo;
        private readonly IRosterTaskEventsManager _rosterTaskEventsManager;
        private readonly ScheduleApprovalManagement _scheduleApprovalManagement;

        private static readonly Logger log = LogManager.GetCurrentClassLogger();

        public StartOfDayPeriodicHandler(IRosterRepository rosterRepo, IRosterTaskEventsManager rosterTaskEventsManager, ScheduleApprovalManagement scheduleApprovalManagement)
        {
            _rosterRepo = rosterRepo;
            _rosterTaskEventsManager = rosterTaskEventsManager;
            _scheduleApprovalManagement = scheduleApprovalManagement;
        }

        public string Name
        {
            get { return "StartOfDay"; }
        }

        public IPeriodicExecutionHandlerResult Execute(Newtonsoft.Json.Linq.JObject parameters, IContinuousProgressCategory progress)
        {
            log.Info("Start scheduled execution of roster approval");
            var result = new PeriodicExecutionHandlerResult();

            try
            {
                var utcNow = DateTime.UtcNow;
                JObject quartzLog = new JObject();
                result.Data = quartzLog;

                JArray rostersLog = new JArray();
                quartzLog["Rosters"] = rostersLog;

                quartzLog["UTCTime"] = utcNow;

                var tenantTime = TimeZoneInfo.ConvertTimeFromUtc(utcNow, MultiTenantManager.CurrentTenant.TimeZoneInfo);
                quartzLog["LocalTime"] = tenantTime;

                log.Debug("Tenant time is {0} in {1}", tenantTime, MultiTenantManager.CurrentTenant.TimeZoneInfo.DisplayName);

                // get all rosters with an automatic schedule approval time that hasn't been run today and 
                // whose start time has passed
                var rosters = _rosterRepo.GetPendingAutomaticScheduleApprovalRosters(utcNow, RosterLoadInstructions.Conditions | RosterLoadInstructions.ConditionActions);

                log.Info("Found {0} rosters to be approved", rosters.Count);
                quartzLog["RosterCount"] = rosters.Count;

                if (rosters.Count == 0)
                {
                    result.Success = true;

                    return result;
                }

                foreach (var roster in rosters)
                {
                    using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(0, 0, 10, 0, 0)))
                    {
                        JObject rosterLog = new JObject();
                        rostersLog.Add(rosterLog);

                        rosterLog["id"] = roster.Id;

                        log.Debug("Starting processing of roster {0}", roster.Id);

                        var rosterTimezone = TimeZoneInfo.FindSystemTimeZoneById(roster.Timezone);

                        var rosterStart = _scheduleApprovalManagement.GetStartOfCurrentShiftInTimezone(roster, rosterTimezone);
                        var rosterEnd = _scheduleApprovalManagement.GetEndOfCurrentShiftInTimezone(roster, rosterTimezone);

                        rosterLog["StartRosterProcessingUTC"] = DateTime.UtcNow;
                        rosterLog["RosterStart"] = rosterStart;
                        rosterLog["RosterEnd"] = rosterEnd;
                        rosterLog["RosterTimezone"] = roster.Timezone;

                        log.Debug("Start of roster for {0} is calculated to be {1}", tenantTime, rosterStart);

                        var buckets = _scheduleApprovalManagement.Buckets(roster.Id, rosterStart, rosterTimezone, false, true);

                        rosterLog["PrimaryBucketCount"] = buckets.PrimaryBuckets.Count;

                        if (buckets.PrimaryBuckets.Count == 0)
                        {
                            // no buckets to process
                            continue;
                        }

                        log.Debug("{0} buckets retrieved for roster {1}", buckets.PrimaryBuckets.Count, roster.Id);

                        foreach (var bucket in buckets.PrimaryBuckets)
                        {
                            rosterLog["SlotCount"] = bucket.Slots.Count;
                            if (bucket.Slots.Count == 0)
                            {
                                continue;
                            }

                            if (bucket.Slots.First().UserId.HasValue == false)
                            {
                                // no one in the root slot for the bucket, we ignore these hierarchies
                                continue;
                            }

                            var processedByUserId = bucket.Slots.First().UserId.Value;

                            rosterLog["PrimaryBucketUser"] = processedByUserId;

                            log.Debug("{0} slots to be processed as user {1}", bucket.Slots.Count, processedByUserId);

                            var slotIds = from s in bucket.Slots select s.Id;

                            // we just approve things as unchanged, the default schedule approval status will sort
                            // out what this actually means.
                            var b = _scheduleApprovalManagement.ApproveUnchanged(rosterStart, rosterTimezone, roster.Id, slotIds.ToArray(), processedByUserId, false);

                            // grab the set of tasks that were involved in the above
                            var tasks = (from s in b.Slots from a in s.AssignedTasks select a.Task).ToList();
                            tasks.AddRange((from s in b.Slots from a in s.CancelledTasks select a.Task));
                            tasks.AddRange((from s in b.Slots from a in s.FinishedByManagementTasks select a.Task));
                            tasks.AddRange((from s in b.Slots from a in s.UnassignedTasks select a.Task));

                            rosterLog["RosterEventTasksCandidates"] = tasks.Count;

                            _rosterTaskEventsManager.Run(TaskEventStage.ScheduleApprovalAutomatic, tasks, (from c in roster.RosterEventConditions where c.NextRunTime.HasValue == false select c).ToList());

                            log.Debug("Completed processing of buckets for roster {0}", roster.Id);
                        }

                        roster.AutomaticScheduleApprovalTimeLastRun = TimeZoneInfo.ConvertTimeToUtc(rosterStart, rosterTimezone);
                        rosterLog["UTCProcessedAt"] = roster.AutomaticScheduleApprovalTimeLastRun;
                        rosterLog["EndRosterProcessingUTC"] = DateTime.UtcNow;
                        _rosterRepo.Save(roster, false, false);

                        log.Debug("Completed processing of roster {0}", roster.Id);

                        scope.Complete();
                    }
                }

                log.Info("Completed scheduled execution roster approval");

                result.Success = true;

                progress.Complete();

                return result;
            }
            catch (Exception e)
            {
                LogEventInfo logEvent = new LogEventInfo(LogLevel.Error, "Schedule", "An exception occurred during automated schedule approval");
                logEvent.Exception = e;
                log.Log(logEvent);

                JObject obj = new JObject();
                obj["Message"] = e.Message;
                obj["StackTrace"] = e.StackTrace;

                result.Success = false;
                result.Data = obj;

                progress.Error(e.Message);

                return result;
            }

        }
    }
}
