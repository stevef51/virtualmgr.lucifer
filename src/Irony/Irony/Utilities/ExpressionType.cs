//
//  Author:
//    Steve Fillingham steve.fillingham@bworkflow.com
//
//  Copyright (c) 2013, SquareIT Software PTY LTD
//
//
//
using System;

namespace Irony
{
	// Summary:
	//     All the ExpressionType enums since Mono decided to miss 39 of them ?!??
	public class ExpressionType4
	{
		// Summary:
		//     A node that represents arithmetic addition without overflow checking.
		public const System.Linq.Expressions.ExpressionType Add = (System.Linq.Expressions.ExpressionType)0;
		//
		// Summary:
		//     A node that represents arithmetic addition with overflow checking.
		public const System.Linq.Expressions.ExpressionType AddChecked = (System.Linq.Expressions.ExpressionType)1;
		//
		// Summary:
		//     A node that represents a bitwise AND operation.
		public const System.Linq.Expressions.ExpressionType And = (System.Linq.Expressions.ExpressionType)2;
		//
		// Summary:
		//     A node that represents a short-circuiting conditional AND operation.
		public const System.Linq.Expressions.ExpressionType AndAlso = (System.Linq.Expressions.ExpressionType)3;
		//
		// Summary:
		//     A node that represents getting the length of a one-dimensional array.
		public const System.Linq.Expressions.ExpressionType ArrayLength = (System.Linq.Expressions.ExpressionType)4;
		//
		// Summary:
		//     A node that represents indexing into a one-dimensional array.
		public const System.Linq.Expressions.ExpressionType ArrayIndex = (System.Linq.Expressions.ExpressionType)5;
		//
		// Summary:
		//     A node that represents a method call.
		public const System.Linq.Expressions.ExpressionType Call = (System.Linq.Expressions.ExpressionType)6;
		//
		// Summary:
		//     A node that represents a null coalescing operation.
		public const System.Linq.Expressions.ExpressionType Coalesce = (System.Linq.Expressions.ExpressionType)7;
		//
		// Summary:
		//     A node that represents a conditional operation.
		public const System.Linq.Expressions.ExpressionType Conditional = (System.Linq.Expressions.ExpressionType)8;
		//
		// Summary:
		//     A node that represents an expression that has a constant value.
		public const System.Linq.Expressions.ExpressionType Constant = (System.Linq.Expressions.ExpressionType)9;
		//
		// Summary:
		//     A node that represents a cast or conversion operation. If the operation is
		//     a numeric conversion, it overflows silently if the converted value does not
		//     fit the target type.
		public const System.Linq.Expressions.ExpressionType Convert = (System.Linq.Expressions.ExpressionType)10;
		//
		// Summary:
		//     A node that represents a cast or conversion operation. If the operation is
		//     a numeric conversion, an exception is thrown if the converted value does
		//     not fit the target type.
		public const System.Linq.Expressions.ExpressionType ConvertChecked = (System.Linq.Expressions.ExpressionType)11;
		//
		// Summary:
		//     A node that represents arithmetic division.
		public const System.Linq.Expressions.ExpressionType Divide = (System.Linq.Expressions.ExpressionType)12;
		//
		// Summary:
		//     A node that represents an equality comparison.
		public const System.Linq.Expressions.ExpressionType Equal = (System.Linq.Expressions.ExpressionType)13;
		//
		// Summary:
		//     A node that represents a bitwise XOR operation.
		public const System.Linq.Expressions.ExpressionType ExclusiveOr = (System.Linq.Expressions.ExpressionType)14;
		//
		// Summary:
		//     A node that represents a "greater than" numeric comparison.
		public const System.Linq.Expressions.ExpressionType GreaterThan = (System.Linq.Expressions.ExpressionType)15;
		//
		// Summary:
		//     A node that represents a "greater than or equal" numeric comparison.
		public const System.Linq.Expressions.ExpressionType GreaterThanOrEqual = (System.Linq.Expressions.ExpressionType)16;
		//
		// Summary:
		//     A node that represents applying a delegate or lambda expression to a list
		//     of argument expressions.
		public const System.Linq.Expressions.ExpressionType Invoke = (System.Linq.Expressions.ExpressionType)17;
		//
		// Summary:
		//     A node that represents a lambda expression.
		public const System.Linq.Expressions.ExpressionType Lambda = (System.Linq.Expressions.ExpressionType)18;
		//
		// Summary:
		//     A node that represents a bitwise left-shift operation.
		public const System.Linq.Expressions.ExpressionType LeftShift = (System.Linq.Expressions.ExpressionType)19;
		//
		// Summary:
		//     A node that represents a "less than" numeric comparison.
		public const System.Linq.Expressions.ExpressionType LessThan = (System.Linq.Expressions.ExpressionType)20;
		//
		// Summary:
		//     A node that represents a "less than or equal" numeric comparison.
		public const System.Linq.Expressions.ExpressionType LessThanOrEqual = (System.Linq.Expressions.ExpressionType)21;
		//
		// Summary:
		//     A node that represents creating a new System.Collections.IEnumerable object
		//     and initializing it from a list of elements.
		public const System.Linq.Expressions.ExpressionType ListInit = (System.Linq.Expressions.ExpressionType)22;
		//
		// Summary:
		//     A node that represents reading from a field or property.
		public const System.Linq.Expressions.ExpressionType MemberAccess = (System.Linq.Expressions.ExpressionType)23;
		//
		// Summary:
		//     A node that represents creating a new object and initializing one or more
		//     of its members.
		public const System.Linq.Expressions.ExpressionType MemberInit = (System.Linq.Expressions.ExpressionType)24;
		//
		// Summary:
		//     A node that represents an arithmetic remainder operation.
		public const System.Linq.Expressions.ExpressionType Modulo = (System.Linq.Expressions.ExpressionType)25;
		//
		// Summary:
		//     A node that represents arithmetic multiplication without overflow checking.
		public const System.Linq.Expressions.ExpressionType Multiply = (System.Linq.Expressions.ExpressionType)26;
		//
		// Summary:
		//     A node that represents arithmetic multiplication with overflow checking.
		public const System.Linq.Expressions.ExpressionType MultiplyChecked = (System.Linq.Expressions.ExpressionType)27;
		//
		// Summary:
		//     A node that represents an arithmetic negation operation.
		public const System.Linq.Expressions.ExpressionType Negate = (System.Linq.Expressions.ExpressionType)28;
		//
		// Summary:
		//     A node that represents a unary plus operation. The result of a predefined
		//     unary plus operation is simply the value of the operand; but user-defined
		//     implementations may have non-trivial results.
		public const System.Linq.Expressions.ExpressionType UnaryPlus = (System.Linq.Expressions.ExpressionType)29;
		//
		// Summary:
		//     A node that represents an arithmetic negation operation that has overflow
		//     checking.
		public const System.Linq.Expressions.ExpressionType NegateChecked = (System.Linq.Expressions.ExpressionType)30;
		//
		// Summary:
		//     A node that represents calling a constructor to create a new object.
		public const System.Linq.Expressions.ExpressionType New = (System.Linq.Expressions.ExpressionType)31;
		//
		// Summary:
		//     A node that represents creating a new one-dimensional array and initializing
		//     it from a list of elements.
		public const System.Linq.Expressions.ExpressionType NewArrayInit = (System.Linq.Expressions.ExpressionType)32;
		//
		// Summary:
		//     A node that represents creating a new array where the bounds for each dimension
		//     are specified.
		public const System.Linq.Expressions.ExpressionType NewArrayBounds = (System.Linq.Expressions.ExpressionType)33;
		//
		// Summary:
		//     A node that represents a bitwise complement operation.
		public const System.Linq.Expressions.ExpressionType Not = (System.Linq.Expressions.ExpressionType)34;
		//
		// Summary:
		//     A node that represents an inequality comparison.
		public const System.Linq.Expressions.ExpressionType NotEqual = (System.Linq.Expressions.ExpressionType)35;
		//
		// Summary:
		//     A node that represents a bitwise OR operation.
		public const System.Linq.Expressions.ExpressionType Or = (System.Linq.Expressions.ExpressionType)36;
		//
		// Summary:
		//     A node that represents a short-circuiting conditional OR operation.
		public const System.Linq.Expressions.ExpressionType OrElse = (System.Linq.Expressions.ExpressionType)37;
		//
		// Summary:
		//     A node that represents a reference to a parameter defined in the context
		//     of the expression.
		public const System.Linq.Expressions.ExpressionType Parameter = (System.Linq.Expressions.ExpressionType)38;
		//
		// Summary:
		//     A node that represents raising a number to a power.
		public const System.Linq.Expressions.ExpressionType Power = (System.Linq.Expressions.ExpressionType)39;
		//
		// Summary:
		//     A node that represents an expression that has a constant value of type System.Linq.Expressions.Expression.
		//     A System.Linq.Expressions.ExpressionType.Quote node can contain references
		//     to parameters defined in the context of the expression it represents.
		public const System.Linq.Expressions.ExpressionType Quote = (System.Linq.Expressions.ExpressionType)40;
		//
		// Summary:
		//     A node that represents a bitwise right-shift operation.
		public const System.Linq.Expressions.ExpressionType RightShift = (System.Linq.Expressions.ExpressionType)41;
		//
		// Summary:
		//     A node that represents arithmetic subtraction without overflow checking.
		public const System.Linq.Expressions.ExpressionType Subtract = (System.Linq.Expressions.ExpressionType)42;
		//
		// Summary:
		//     A node that represents arithmetic subtraction with overflow checking.
		public const System.Linq.Expressions.ExpressionType SubtractChecked = (System.Linq.Expressions.ExpressionType)43;
		//
		// Summary:
		//     A node that represents an explicit reference or boxing conversion where null
		//     is supplied if the conversion fails.
		public const System.Linq.Expressions.ExpressionType TypeAs = (System.Linq.Expressions.ExpressionType)44;
		//
		// Summary:
		//     A node that represents a type test.
		public const System.Linq.Expressions.ExpressionType TypeIs = (System.Linq.Expressions.ExpressionType)45;
	
		//
		// Summary:
		//     An assignment operation; such as (a = b).
		public const System.Linq.Expressions.ExpressionType Assign = (System.Linq.Expressions.ExpressionType)46;
		//
		// Summary:
		//     A block of expressions.
		public const System.Linq.Expressions.ExpressionType Block = (System.Linq.Expressions.ExpressionType)47;
		//
		// Summary:
		//     Debugging information.
		public const System.Linq.Expressions.ExpressionType DebugInfo = (System.Linq.Expressions.ExpressionType)48;
		//
		// Summary:
		//     A unary decrement operation; such as (a - 1) in C# and Visual Basic. The
		//     object a should not be modified in place.
		public const System.Linq.Expressions.ExpressionType Decrement = (System.Linq.Expressions.ExpressionType)49;
		//
		// Summary:
		//     A dynamic operation.
		public const System.Linq.Expressions.ExpressionType Dynamic = (System.Linq.Expressions.ExpressionType)50;
		//
		// Summary:
		//     A default value.
		public const System.Linq.Expressions.ExpressionType Default = (System.Linq.Expressions.ExpressionType)51;
		//
		// Summary:
		//     An extension expression.
		public const System.Linq.Expressions.ExpressionType Extension = (System.Linq.Expressions.ExpressionType)52;
		//
		// Summary:
		//     A "go to" expression; such as goto Label in C# or GoTo Label in Visual Basic.
		public const System.Linq.Expressions.ExpressionType Goto = (System.Linq.Expressions.ExpressionType)53;
		//
		// Summary:
		//     A unary increment operation; such as (a + 1) in C# and Visual Basic. The
		//     object a should not be modified in place.
		public const System.Linq.Expressions.ExpressionType Increment = (System.Linq.Expressions.ExpressionType)54;
		//
		// Summary:
		//     An index operation or an operation that accesses a property that takes arguments.
		public const System.Linq.Expressions.ExpressionType Index = (System.Linq.Expressions.ExpressionType)55;
		//
		// Summary:
		//     A label.
		public const System.Linq.Expressions.ExpressionType Label = (System.Linq.Expressions.ExpressionType)56;
		//
		// Summary:
		//     A list of run-time variables. For more information; see System.Linq.Expressions.RuntimeVariablesExpression.
		public const System.Linq.Expressions.ExpressionType RuntimeVariables = (System.Linq.Expressions.ExpressionType)57;
		//
		// Summary:
		//     A loop; such as for or while.
		public const System.Linq.Expressions.ExpressionType Loop = (System.Linq.Expressions.ExpressionType)58;
		//
		// Summary:
		//     A switch operation; such as switch in C# or Select Case in Visual Basic.
		public const System.Linq.Expressions.ExpressionType Switch = (System.Linq.Expressions.ExpressionType)59;
		//
		// Summary:
		//     An operation that throws an exception; such as throw new Exception().
		public const System.Linq.Expressions.ExpressionType Throw = (System.Linq.Expressions.ExpressionType)60;
		//
		// Summary:
		//     A try-catch expression.
		public const System.Linq.Expressions.ExpressionType Try = (System.Linq.Expressions.ExpressionType)61;
		//
		// Summary:
		//     An unbox value type operation; such as unbox and unbox.any instructions in
		//     MSIL.
		public const System.Linq.Expressions.ExpressionType Unbox = (System.Linq.Expressions.ExpressionType)62;
		//
		// Summary:
		//     An addition compound assignment operation; such as (a += b); without overflow
		//     checking; for numeric operands.
		public const System.Linq.Expressions.ExpressionType AddAssign = (System.Linq.Expressions.ExpressionType)63;
		//
		// Summary:
		//     A bitwise or logical AND compound assignment operation; such as (a &= b)
		//     in C#.
		public const System.Linq.Expressions.ExpressionType AndAssign = (System.Linq.Expressions.ExpressionType)64;
		//
		// Summary:
		//     An division compound assignment operation; such as (a /= b); for numeric
		//     operands.
		public const System.Linq.Expressions.ExpressionType DivideAssign = (System.Linq.Expressions.ExpressionType)65;
		//
		// Summary:
		//     A bitwise or logical XOR compound assignment operation; such as (a ^= b)
		//     in C#.
		public const System.Linq.Expressions.ExpressionType ExclusiveOrAssign = (System.Linq.Expressions.ExpressionType)66;
		//
		// Summary:
		//     A bitwise left-shift compound assignment; such as (a <<= b).
		public const System.Linq.Expressions.ExpressionType LeftShiftAssign = (System.Linq.Expressions.ExpressionType)67;
		//
		// Summary:
		//     An arithmetic remainder compound assignment operation; such as (a %= b) in
		//     C#.
		public const System.Linq.Expressions.ExpressionType ModuloAssign = (System.Linq.Expressions.ExpressionType)68;
		//
		// Summary:
		//     A multiplication compound assignment operation; such as (a *= b); without
		//     overflow checking; for numeric operands.
		public const System.Linq.Expressions.ExpressionType MultiplyAssign = (System.Linq.Expressions.ExpressionType)69;
		//
		// Summary:
		//     A bitwise or logical OR compound assignment; such as (a |= b) in C#.
		public const System.Linq.Expressions.ExpressionType OrAssign = (System.Linq.Expressions.ExpressionType)70;
		//
		// Summary:
		//     A compound assignment operation that raises a number to a power; such as
		//     (a ^= b) in Visual Basic.
		public const System.Linq.Expressions.ExpressionType PowerAssign = (System.Linq.Expressions.ExpressionType)71;
		//
		// Summary:
		//     A bitwise right-shift compound assignment operation; such as (a >>= b).
		public const System.Linq.Expressions.ExpressionType RightShiftAssign = (System.Linq.Expressions.ExpressionType)72;
		//
		// Summary:
		//     A subtraction compound assignment operation; such as (a -= b); without overflow
		//     checking; for numeric operands.
		public const System.Linq.Expressions.ExpressionType SubtractAssign = (System.Linq.Expressions.ExpressionType)73;
		//
		// Summary:
		//     An addition compound assignment operation; such as (a += b); with overflow
		//     checking; for numeric operands.
		public const System.Linq.Expressions.ExpressionType AddAssignChecked = (System.Linq.Expressions.ExpressionType)74;
		//
		// Summary:
		//     A multiplication compound assignment operation; such as (a *= b); that has
		//     overflow checking; for numeric operands.
		public const System.Linq.Expressions.ExpressionType MultiplyAssignChecked = (System.Linq.Expressions.ExpressionType)75;
		//
		// Summary:
		//     A subtraction compound assignment operation; such as (a -= b); that has overflow
		//     checking; for numeric operands.
		public const System.Linq.Expressions.ExpressionType SubtractAssignChecked = (System.Linq.Expressions.ExpressionType)76;
		//
		// Summary:
		//     A unary prefix increment; such as (++a). The object a should be modified
		//     in place.
		public const System.Linq.Expressions.ExpressionType PreIncrementAssign = (System.Linq.Expressions.ExpressionType)77;
		//
		// Summary:
		//     A unary prefix decrement; such as (--a). The object a should be modified
		//     in place.
		public const System.Linq.Expressions.ExpressionType PreDecrementAssign = (System.Linq.Expressions.ExpressionType)78;
		//
		// Summary:
		//     A unary postfix increment; such as (a++). The object a should be modified
		//     in place.
		public const System.Linq.Expressions.ExpressionType PostIncrementAssign = (System.Linq.Expressions.ExpressionType)79;
		//
		// Summary:
		//     A unary postfix decrement; such as (a--). The object a should be modified
		//     in place.
		public const System.Linq.Expressions.ExpressionType PostDecrementAssign = (System.Linq.Expressions.ExpressionType)80;
		//
		// Summary:
		//     An exact type test.
		public const System.Linq.Expressions.ExpressionType TypeEqual = (System.Linq.Expressions.ExpressionType)81;
		//
		// Summary:
		//     A ones complement operation; such as (~a) in C#.
		public const System.Linq.Expressions.ExpressionType OnesComplement = (System.Linq.Expressions.ExpressionType)82;
		//
		// Summary:
		//     A true condition value.
		public const System.Linq.Expressions.ExpressionType IsTrue = (System.Linq.Expressions.ExpressionType)83;
		//
		// Summary:
		//     A false condition value.
		public const System.Linq.Expressions.ExpressionType IsFalse = (System.Linq.Expressions.ExpressionType)84;
	}
}

