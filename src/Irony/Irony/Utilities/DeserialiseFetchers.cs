﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Irony.Parsing;

namespace Irony.Utilities
{
    public static class DeserialiseFetchers
    {
        public static Production ReadProduction(LanguageData ldata, IDictionary<string, BnfTerm> termidx, BinaryReader reader)
        {
            var ntname = reader.ReadString();
            var lv = (NonTerminal) termidx[ntname];
            //var lv = ldata.GrammarData.NonTerminals.Single(t => t.Name == ntname);
            //read the rvalues for the production
            var rvct = reader.ReadInt32();
            var rvnamelist = new List<string>();
            for (int j = 0; j < rvct; j++)
            {
                rvnamelist.Add(reader.ReadString());
            }
            var prod = lv.Productions.Single(p => p.RValues.Select(r => r.Name).SequenceEqual(rvnamelist));
            return prod;
        }

        public static void WriteProduction(Production prod, BinaryWriter writer)
        {
            writer.Write(prod.LValue.Name);
            writer.Write(prod.RValues.Count);
            foreach (var rvalue in prod.RValues)
                writer.Write(rvalue.Name);
        }

        public static ParserAction ReadAction(LanguageData ldata, IList<ParserState> states, IDictionary<string, ParserState> stateidx, 
            IDictionary<string, BnfTerm> termidx, BinaryReader reader)
        {
            var actionType = (FrozenParserAction)reader.ReadInt32();
            ParserAction action;
            switch (actionType)
            {
                case FrozenParserAction.AcceptParserAction:
                    action = new AcceptParserAction();
                    break;
                case FrozenParserAction.ReduceParserAction:
                    var prod = DeserialiseFetchers.ReadProduction(ldata, termidx, reader);
                    action = new ReduceParserAction(prod);
                    break;
                case FrozenParserAction.ShiftParserAction:
                    var tname = reader.ReadString();
                    var term = termidx[tname];
                    //var term = ldata.GrammarData.AllTerms.Single(t => t.Name == tname);
                    var sname = reader.ReadString();
                    var state = stateidx[sname];
                    //var state = states.Single(s => s.Name == sname);
                    action = new ShiftParserAction(term, state);
                    break;
                case FrozenParserAction.ErrorRecoveryParserAction:
                    action = new ErrorRecoveryParserAction();
                    break;
                case FrozenParserAction.PrecedenceBasedParserAction:
                    var ptname = reader.ReadString();
                    var pterm = termidx[ptname];
                    //var pterm = ldata.GrammarData.AllTerms.Single(t => t.Name == ptname);
                    var psname = reader.ReadString();
                    var pstate = stateidx[psname];
                    //var pstate = states.Single(s => s.Name == psname);
                    var pprod = DeserialiseFetchers.ReadProduction(ldata, termidx, reader);
                    action = new PrecedenceBasedParserAction(pterm, pstate, pprod);
                    break;
                case FrozenParserAction.ReduceListBuilderParserAction:
                    var lprod = DeserialiseFetchers.ReadProduction(ldata, termidx, reader);
                    action = new ReduceListBuilderParserAction(lprod);
                    break;
                case FrozenParserAction.ReduceTransientParserAction:
                    var tprod = DeserialiseFetchers.ReadProduction(ldata, termidx, reader);
                    action = new ReduceTransientParserAction(tprod);
                    break;
                case FrozenParserAction.ReduceListContainerParserAction:
                    var rlprod = DeserialiseFetchers.ReadProduction(ldata, termidx, reader);
                    action = new ReduceListContainerParserAction(rlprod);
                    break;
                case FrozenParserAction.Null:
                    action = null;
                    break;
                default:
                    throw new NotImplementedException("I only implemented enough of this to work for lingo");
            }

            return action;
        }

        public static void WriteAction(ParserAction action, BinaryWriter writer)
        {
            if (action == null)
            {
                writer.Write((int)FrozenParserAction.Null);
            }
            else if (action is ReduceListBuilderParserAction)
            {
                var caction = action as ReduceListBuilderParserAction;
                writer.Write((int)FrozenParserAction.ReduceListBuilderParserAction);
                WriteProduction(caction.Production, writer);
            }
            else if (action is ReduceTransientParserAction)
            {
                var caction = action as ReduceTransientParserAction;
                writer.Write((int)FrozenParserAction.ReduceTransientParserAction);
                WriteProduction(caction.Production, writer);
            }
            else if (action is ReduceListContainerParserAction)
            {
                var caction = action as ReduceListContainerParserAction;
                writer.Write((int)FrozenParserAction.ReduceListContainerParserAction);
                WriteProduction(caction.Production, writer);
            }
            else if (action is PrecedenceBasedParserAction)
            {
                var caction = action as PrecedenceBasedParserAction;
                writer.Write((int)FrozenParserAction.PrecedenceBasedParserAction);
                writer.Write(caction._shiftAction.Term.Name);
                writer.Write(caction._shiftAction.NewState.Name);
                WriteProduction(caction._reduceAction.Production, writer);
            }
            else if (action is ShiftParserAction)
            {
                var caction = action as ShiftParserAction;
                writer.Write((int)FrozenParserAction.ShiftParserAction);
                writer.Write(caction.Term.Name);
                writer.Write(caction.NewState.Name);
            }
            else if (action is ReduceParserAction)
            {
                var caction = action as ReduceParserAction;
                writer.Write((int)FrozenParserAction.ReduceParserAction);
                WriteProduction(caction.Production, writer);
            }
            else if (action is AcceptParserAction)
            {
                writer.Write((int)FrozenParserAction.AcceptParserAction);
            }
            else if (action is ErrorRecoveryParserAction)
            {
                writer.Write((int)FrozenParserAction.ErrorRecoveryParserAction);
            }
            else
            {
                throw new NotImplementedException("Only implemented enough of these for lingo");
            }
        }
    }
}
