﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

using Irony.Parsing; 

namespace Irony.Interpreter.Ast {


  public class OperatorInfo {
    public string Symbol;
    public ExpressionType ExpressionType;
    public int Precedence;
    public Associativity Associativity;
  }

  public class OperatorInfoDictionary : Dictionary<string, OperatorInfo> {
    public OperatorInfoDictionary(bool caseSensitive) : base(caseSensitive ? StringComparer.Ordinal : StringComparer.OrdinalIgnoreCase) { }

    public void Add(string symbol, ExpressionType expressionType, int precedence, Associativity associativity = Associativity.Left) {
      var info = new OperatorInfo() {
        Symbol = symbol, ExpressionType = expressionType,
        Precedence = precedence, Associativity = associativity
      };
      this[symbol] = info;
    }
  }//class


  public class OperatorHandler {
    private OperatorInfoDictionary _registeredOperators;


    public OperatorHandler(bool languageCaseSensitive) {
      _registeredOperators = new OperatorInfoDictionary(languageCaseSensitive);
      BuildDefaultOperatorMappings(); 
    }

    public ExpressionType GetOperatorExpressionType(string symbol) {
      OperatorInfo opInfo;
      if (_registeredOperators.TryGetValue(symbol, out opInfo))
        return opInfo.ExpressionType;
      return CustomExpressionTypes.NotAnExpression;
    }

    public virtual ExpressionType GetUnaryOperatorExpressionType(string symbol) {
      switch (symbol.ToLowerInvariant()) {
        case "+": return ExpressionType.UnaryPlus;
        case "-": return ExpressionType.Negate;
        case "!":
        case "not":
        case "~":
          return ExpressionType.Not;
        default:
          return CustomExpressionTypes.NotAnExpression;
      }
    }


    public virtual ExpressionType GetBinaryOperatorForAugmented(ExpressionType augmented) {
     switch(augmented) {
        case ExpressionType4.AddAssign:
        case ExpressionType4.AddAssignChecked:
          return ExpressionType4.AddChecked;
        case ExpressionType4.AndAssign:
          return ExpressionType4.And;
        case ExpressionType4.Decrement:
          return ExpressionType4.SubtractChecked; 
        case ExpressionType4.DivideAssign:
          return ExpressionType4.Divide;
        case ExpressionType4.ExclusiveOrAssign:
          return ExpressionType4.ExclusiveOr;
        case ExpressionType4.LeftShiftAssign:
          return ExpressionType4.LeftShift;
        case ExpressionType4.ModuloAssign:
          return ExpressionType4.Modulo;
        case ExpressionType4.MultiplyAssign:
        case ExpressionType4.MultiplyAssignChecked:
          return ExpressionType4.MultiplyChecked;
        case ExpressionType4.OrAssign:
          return ExpressionType4.Or;
        case ExpressionType4.RightShiftAssign:
          return ExpressionType4.RightShift;
        case ExpressionType4.SubtractAssign:
        case ExpressionType4.SubtractAssignChecked:
          return ExpressionType4.SubtractChecked;
        default: 
		  return CustomExpressionTypes.NotAnExpression;
      }
    }
    
    public virtual OperatorInfoDictionary BuildDefaultOperatorMappings() {
      var dict = _registeredOperators;
      dict.Clear(); 
      int p = 0; //precedence

      p += 10;
      dict.Add("=", ExpressionType4.Assign, p);
      dict.Add("+=", ExpressionType4.AddAssignChecked, p);
      dict.Add("-=", ExpressionType4.SubtractAssignChecked, p);
      dict.Add("*=", ExpressionType4.MultiplyAssignChecked, p);
      dict.Add("/=", ExpressionType4.DivideAssign, p);
      dict.Add("%=", ExpressionType4.ModuloAssign, p);
      dict.Add("|=", ExpressionType4.OrAssign, p);
      dict.Add("&=", ExpressionType4.AndAssign, p);
      dict.Add("^=", ExpressionType4.ExclusiveOrAssign, p);

      p += 10;
      dict.Add("==", ExpressionType.Equal, p);
      dict.Add("!=", ExpressionType.NotEqual, p);
      dict.Add("<>", ExpressionType.NotEqual, p);

      p += 10;
      dict.Add("<", ExpressionType.LessThan, p);
      dict.Add("<=", ExpressionType.LessThanOrEqual, p);
      dict.Add(">", ExpressionType.GreaterThan, p);
      dict.Add(">=", ExpressionType.GreaterThanOrEqual, p);

      p += 10;
      dict.Add("|", ExpressionType.Or, p);
      dict.Add("or", ExpressionType.Or, p);
      dict.Add("||", ExpressionType.OrElse, p);
      dict.Add("orelse", ExpressionType.OrElse, p);
      dict.Add("^", ExpressionType.ExclusiveOr, p);
      dict.Add("xor", ExpressionType.ExclusiveOr, p);

      p += 10;
      dict.Add("&", ExpressionType.And, p);
      dict.Add("and", ExpressionType.And, p);
      dict.Add("&&", ExpressionType.AndAlso, p);
      dict.Add("andalso", ExpressionType.AndAlso, p);

      p += 10;
      dict.Add("!", ExpressionType.Not, p);
      dict.Add("not", ExpressionType.Not, p);

      p += 10;
      dict.Add("<<", ExpressionType.LeftShift, p);
      dict.Add(">>", ExpressionType.RightShift, p);

      p += 10;
      dict.Add("+", ExpressionType.AddChecked, p); 
      dict.Add("-", ExpressionType.SubtractChecked, p);

      p += 10;
      dict.Add("*", ExpressionType.MultiplyChecked, p);
      dict.Add("/", ExpressionType.Divide, p);
      dict.Add("%", ExpressionType.Modulo, p);
      dict.Add("**", ExpressionType.Power, p);

      p += 10;
      dict.Add("??", ExpressionType.Coalesce, p);
      dict.Add("?", ExpressionType.Conditional, p);

      return dict; 
    }//method

  }


}
