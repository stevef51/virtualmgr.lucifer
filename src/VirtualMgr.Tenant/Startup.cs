﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.SpaServices.Webpack;
using Newtonsoft.Json;
using System.IO;
using Microsoft.Extensions.Caching.Distributed;
using System.Text;
using Lamar;
using VirtualMgr.MultiTenant;
using Serilog;
using Microsoft.Extensions.FileProviders;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.AspNetCore.HttpOverrides;
using System.Net.Http;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.Extensions.Options;
using System.Collections.Concurrent;
using VirtualMgr.AspNetCore;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.DataProtection;
using ServiceStack.Redis;

namespace VirtualMgr.Tenant
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureContainer(ServiceRegistry services)
        {
            services.Configure<ServiceOptions>(Configuration.GetSection("Service"));
            services.Configure<ServiceOptions>(options => options.ServiceName = options.Services.Tenant);

            var tempProvider = services.BuildServiceProvider();
            var serviceOptions = tempProvider.GetRequiredService<IOptions<ServiceOptions>>().Value;


            services.AddLogging();

            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

            services.Configure<ForwardedHeadersOptions>(options =>
            {
                options.ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto;
            });

            services.AddModule(new VirtualMgr.Redis.InjectionModule(serviceOptions.Services.Redis));
            services.AddModule(new CommonAuthenticationInjectionModule(serviceOptions));

            services.AddVirtualMgrAppTenant<RequestHostTenantDomainResolver>(Configuration);
            services.AddModule(new VirtualMgr.MassTransit.InjectionModule(Configuration, serviceOptions.ServiceName));

            services.AddLLBLGenRuntimeConfiguration(System.Diagnostics.TraceLevel.Error);
            services.AddVirtualMgrMultiTenantLLBLGen(Configuration);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IApplicationLifetime lifetime, IDistributedCache cache, IOptions<ServiceOptions> serviceOptions)
        {
            foreach (var nvp in Configuration.AsEnumerable())
            {
                Log.Logger.Information($"{nvp.Key}={nvp.Value}");
            }

            var webRootPath = env.WebRootPath;
            app.UsePathBase($"/{serviceOptions.Value.ServiceName}");

            app.UseVirtualMgrAppTenant();

            if (env.IsDevelopment())
            {
                // Only turn this on if you want to see all Headers logged
                //                app.LogRequestHeaders(app.ApplicationServices.GetService<ILoggerFactory>());
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            // Ok, we want anything below "/tenant" to map to the "wwwroot/tenants/{tenant.Folder}" ..
            var wwwTenants = Path.Combine(webRootPath, "tenants");
            if (!System.IO.Directory.Exists(wwwTenants))
            {
                System.IO.Directory.CreateDirectory(wwwTenants);
            }

            // note, in here the context.Request.PathBase == "/tenant"
            // the following will remap a Request.Path of "" to "{tenant.Folder}"
            app.UsePerTenantStaticFiles<AppTenant>("", async tenant =>
            {
                var themeInstaller = app.ApplicationServices.GetService<AppTenantThemeInstaller>();
                await themeInstaller.InstallThemeIfRequiredAsync(tenant, Path.Combine(wwwTenants, tenant.Folder));
                return $"/{tenant.Folder}";
            });


            // and then setup static files on "" to map to "wwwroot/tenants"
            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(wwwTenants),
                RequestPath = ""
            });
        }
    }
}
