workflow SendEmail
{
  Param
  (
    # Input parameters
    [Parameter (Mandatory = $true)]
    [string] $MessageBody,

    [Parameter (Mandatory = $true)]
    [string] $Subject,

    [Parameter (Mandatory = $true)]
    [string] $EmailTo,

    [parameter(Mandatory=$true)]
    [PSCredential] $Credential
  )

    InlineScript
    {
        # Create new MailMessage
        $Message = New-Object System.Net.Mail.MailMessage
         
        # Set address-properties
        $Message.From = "no-reply@virtualmgr.com"
        $Message.replyTo = "no-reply@virtualmgr.com"
        $Message.To.Add($Using:EmailTo)
   
        # Set email subject
        $Message.SubjectEncoding = ([System.Text.Encoding]::UTF8)
        $Message.Subject = $Using:Subject
         
        # Set email body
        $Message.Body = $Using:MessageBody
        $Message.BodyEncoding = ([System.Text.Encoding]::UTF8)
        $Message.IsBodyHtml = $true
         
        # Create and set SMTP
        $SmtpClient = New-Object System.Net.Mail.SmtpClient 'smtp.sendgrid.net', 587
        $SmtpClient.Credentials = $Using:Credential
        $SmtpClient.EnableSsl   = $true
   
        # Send email message
        $SmtpClient.Send($Message)
    }
}