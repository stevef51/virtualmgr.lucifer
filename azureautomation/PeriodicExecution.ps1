workflow PeriodicExecution
{
	param (
		# Fully-qualified name of the Azure DB server
		[parameter(Mandatory=$true)]
		[string] $SqlServerName,

		[parameter(Mandatory=$true)]
		[string] $TenantMasterName,

		[parameter(Mandatory=$true)]
		[string] $EmailTo,

		# Credentials for $SqlServerName stored as an Azure Automation credential asset
		# When using in the Azure Automation UI, please enter the name of the credential asset for the "Credential" parameter
		[parameter(Mandatory=$true)]
		[PSCredential] $Credential
	)

    $VerbosePreference = 'continue'

    $IsRunning = Get-AutomationVariable -Name $TenantMasterName
    if($IsRunning -eq $true)
    {
        Write-Verbose "PERunning variable indicates that Periodic Execution is already running, this instance will quit"
        exit
    }

	$Tenants = inlinescript {
        Write-Verbose "Setting PERunning to true to prevent multiple instances"
		# Set up credentials
		$ServerName = $Using:SqlServerName
		$MasterName = $Using:TenantMasterName
		$UserId = $Using:Credential.UserName
		$Password = ($Using:Credential).GetNetworkCredential().Password
		$databases = @()

        Set-AutomationVariable -Name $MasterName -Value $true

		# Create connection to Master DB
		Try {
			$MasterConnectionString = "Server = $ServerName; Database = $MasterName; User ID = $UserId; Password = $Password;"

			$MasterDatabaseConnection = New-Object System.Data.SqlClient.SqlConnection
			$MasterDatabaseConnection.ConnectionString = $MasterConnectionString
			$MasterDatabaseConnection.Open();

			# Create command to query the name of active databases in $ServerName
			$MasterDatabaseCommand = New-Object System.Data.SqlClient.SqlCommand
			$MasterDatabaseCommand.Connection = $MasterDatabaseConnection
			$MasterDatabaseCommand.CommandText =
			"
			select HostName from mtc.tblTenants where [Enabled] = 1
			"

			$MasterDbResult = $MasterDatabaseCommand.ExecuteReader()

			while($MasterDbResult.Read()) {
                $HostName = @($MasterDbResult[0].ToString())
                
                Write-Verbose "Adding: $HostName to processing queue"
				
                $databases += $HostName
			}
		}
		# Catch errors connecting to master database.
		Catch {
			Write-Error $_
		}
		Finally {
			if ($null -ne $MasterDatabaseConnection) {
				$MasterDatabaseConnection.Close()
				$MasterDatabaseConnection.Dispose()
			}
		}

        return $databases
	}

    $WwwCredential = Get-AutomationPSCredential -Name "evs-www"

    $PEResult = $true
    $PEResultDetails = ""

	foreach ($Tenant in $Tenants) {
		Try {
            Write-Verbose "$Tenant"

			$TenantResult = TenantPeriodicExecute `
				-Credential $WwwCredential `
				-EmailTo $EmailTo `
				-TenantUrl $Tenant

            if($TenantResult.success -eq $false)
            {
                $PEResult = $false
                $PEResultDetails = $PEResultDetails + $TenantResult.tenant + " "
            }
		}
		Catch {
			Write-Error $_
		}
	}

    Write-Verbose "Setting PERunning to false to allow subsequent instances"
    Set-AutomationVariable -Name $TenantMasterName -Value $false

    if($PEresult -eq $false)
    {
        Write-Error "Periodic Execution failed for tenants $PEResultDetails"
        throw "Periodic Execution failed for tenants $PEResultDetails"
    }
}