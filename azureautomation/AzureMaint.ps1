<# .SYNOPSIS Perform index maintenance .DESCRIPTION This runbook uses Azure Automation to perform reindexing and statistics maintenance of all databases on a target server. As prerequisite, please create an Azure Automation credential asset that contains the username and password for the target Azure SQL DB logical server ($SqlServerName). Make sure that you have installed the scripts IndexOptimize.sql, CommandLog.sql and CommandExecute.sql of Ola Hallengren (https://ola.hallengren.com/downloads.html) Make sure to get Ola's modified scripts which work on Azure here: https://ola.hallengren.com/downloads.html .EXAMPLE SQLServerIndexMaintenance .NOTES AUTHOR: Original author Pieter Vanhove: http://pietervanhove.azurewebsites.net/?p=14137 Heavily modified by Larry Silverman, CTO, TrackAbout Very slightly modified by Alexander Arvidsson: http://www.arcticdba.se #>

workflow AzureMaint
{
	param 
	(
		# Fully-qualified name of the Azure DB server
		[parameter(Mandatory=$true)]
		[string] $SqlServerName,

		[parameter(Mandatory=$true)]
		[string] $TenantMasterName,

		# Credentials for $SqlServerName stored as an Azure Automation credential asset
		# When using in the Azure Automation UI, please enter the name of the credential asset for the "Credential" parameter
		[parameter(Mandatory=$true)]
		[PSCredential] $Credential
	)

	inlinescript 
	{
		# Set up credentials
		$ServerName = $Using:SqlServerName
		$MasterName = $Using:TenantMasterName
		$UserId = $Using:Credential.UserName
		$Password = ($Using:Credential).GetNetworkCredential().Password
		$databases = @()

		# Create connection to Master DB
		Try 
		{
			$MasterConnectionString = "Server = $ServerName; Database = $MasterName; User ID = $UserId; Password = $Password;"

			$MasterDatabaseConnection = New-Object System.Data.SqlClient.SqlConnection
			$MasterDatabaseConnection.ConnectionString = $MasterConnectionString
			$MasterDatabaseConnection.Open();

			# Create command to query the name of active databases in $ServerName
			$MasterDatabaseCommand = New-Object System.Data.SqlClient.SqlCommand
			$MasterDatabaseCommand.Connection = $MasterDatabaseConnection
			$MasterDatabaseCommand.CommandText =
			"
			select distinct ConnectionString from mtc.tblTenants where [Enabled] = 1
			"

			$MasterDbResult = $MasterDatabaseCommand.ExecuteReader()

			while($MasterDbResult.Read()) {
			$databases += @($MasterDbResult[0].ToString())
			}
		}
		# Catch errors connecting to master database.
		Catch 
		{
			Write-Error $_
		}
		Finally 
		{
			if ($null -ne $MasterDatabaseConnection) {
				$MasterDatabaseConnection.Close()
				$MasterDatabaseConnection.Dispose()
			}
		}

		# Create connection for each individual database
		# Iterate through each database under $ServerName
		foreach ($DbName in $databases) 
		{
			Try 
			{
				# Setup connection string for $DbName
				$ChildDatabaseConnection = New-Object System.Data.SqlClient.SqlConnection
				$ChildDatabaseConnection.ConnectionString = $DbName
				$ChildDatabaseConnection.Open();

				# Create command for a specific database $DBName
				$DatabaseCommand = New-Object System.Data.SqlClient.SqlCommand
				$DatabaseCommand.Connection = $ChildDatabaseConnection

				$CurrentDatabaseName = $ChildDatabaseConnection.Database

				Write-Output "Performing index and statistics maintenance on $CurrentDatabaseName"

				$DatabaseCommand.CommandText ="
				EXECUTE dba.IndexOptimize
				@Databases = '" + $CurrentDatabaseName + "',
				@FragmentationLow = NULL,
				@FragmentationMedium = 'INDEX_REORGANIZE,INDEX_REBUILD_ONLINE,INDEX_REBUILD_OFFLINE',
				@FragmentationHigh = 'INDEX_REBUILD_ONLINE,INDEX_REBUILD_OFFLINE',
				@FragmentationLevel1 = 5,
				@FragmentationLevel2 = 30,
				@UpdateStatistics = 'ALL',
				@OnlyModifiedStatistics = 'Y',
				@LogToTable = 'Y'
				"
				$DatabaseCommand.CommandTimeout = 0
				# Write-Output $DatabaseCommand.CommandText
				$NonQueryResult = $DatabaseCommand.ExecuteNonQuery()
			}

			# Inner catch for individual database failures.
			# We want to keep processing the next database.
			Catch 
			{
				Write-Error $_
			}
			Finally 
			{
				if ($null -ne $ChildDatabaseConnection)
				{
					$ChildDatabaseConnection.Close()
					$ChildDatabaseConnection.Dispose()
				}
			}
		}
	}
}