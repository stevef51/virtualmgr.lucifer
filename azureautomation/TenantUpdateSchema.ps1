workflow TenantUpdateSchema
{
  Param
  (
    # Input parameters
    [Parameter (Mandatory = $true)]
    [string] $TenantUrl,
	
	[Parameter (Mandatory = $true)]
    [string] $TenantName,
 
    [parameter(Mandatory=$true)]
    [PSCredential] $Credential
  )
    $VerbosePreference = 'continue'

    $IsOk = InlineScript
    {
        $Url = $Using:TenantUrl
		$tenantName = $Using:TenantName
		
        $EndPoint = "https://$Url/portal/multitenant/updatedatabaseschemas?format=text&tenantName=$tenantName"
        
        Write-Verbose "Hitting database schema endpoint at $EndPoint"

		try
		{
			$result = Invoke-RestMethod -Method 'Get' -Uri $EndPoint -Credential $Using:Credential -TimeoutSec 1800

            Write-Verbose "Completed schema update of $EndPoint with result of $result"

			return @{success=$true;message=$result}
		}
		catch
		{
			return @{success=$false;message=$_.ErrorDetails.Message}
		}
    }
}