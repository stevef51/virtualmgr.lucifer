workflow UpdateSchemas
{
	param (
		# Fully-qualified name of the Azure DB server
		[parameter(Mandatory=$true)]
		[string] $SqlServerName,

		[parameter(Mandatory=$true)]
		[string] $TenantMasterName,

		[parameter(Mandatory=$true)]
		[string] $EmailTo,

		# Credentials for $SqlServerName stored as an Azure Automation credential asset
		# When using in the Azure Automation UI, please enter the name of the credential asset for the "Credential" parameter
		[parameter(Mandatory=$true)]
		[PSCredential] $Credential
	)

    $VerbosePreference = 'continue'

	$Tenants = inlinescript {
		# Set up credentials
		$ServerName = $Using:SqlServerName
		$MasterName = $Using:TenantMasterName
		$UserId = $Using:Credential.UserName
		$Password = ($Using:Credential).GetNetworkCredential().Password
		$databases = @()

		# Create connection to Master DB
		Try {
			$MasterConnectionString = "Server = $ServerName; Database = $MasterName; User ID = $UserId; Password = $Password;"

			$MasterDatabaseConnection = New-Object System.Data.SqlClient.SqlConnection
			$MasterDatabaseConnection.ConnectionString = $MasterConnectionString
			$MasterDatabaseConnection.Open();

			# Create command to query the name of active databases in $ServerName
			$MasterDatabaseCommand = New-Object System.Data.SqlClient.SqlCommand
			$MasterDatabaseCommand.Connection = $MasterDatabaseConnection
			$MasterDatabaseCommand.CommandText =
			"
			select HostName from mtc.tblTenants where [Enabled] = 1
			"

			$MasterDbResult = $MasterDatabaseCommand.ExecuteReader()

			while($MasterDbResult.Read()) {
                $HostName = @($MasterDbResult[0].ToString())
                
                Write-Verbose "Adding: $HostName to processing queue"
				
                $databases += $HostName
			}
		}
		# Catch errors connecting to master database.
		Catch {
			Write-Error $_
		}
		Finally {
			if ($null -ne $MasterDatabaseConnection) {
				$MasterDatabaseConnection.Close()
				$MasterDatabaseConnection.Dispose()
			}
		}

        return $databases
	}

    $WwwCredential = Get-AutomationPSCredential -Name "evs-www"

	$UpdateOk = $true
	$UpdateResult = ""

	foreach ($Tenant in $Tenants) {
		Try {
            Write-Verbose "$Tenant"

			$TenantResult = TenantUpdateSchema `
				-Credential $WwwCredential `
				-EmailTo $EmailTo `
				-TenantUrl $Tenant
			
            if($TenantResult.success -eq $false)
            {
                $UpdateOk = $false
            }
			
			$UpdateResult = $UpdateResult + $TenantResult.message + "<br/>"
		}
		Catch {
			Write-Error $_
		}
	}
	
	$SmtpCredential = Get-AutomationPSCredential -Name "evs-smtp"

    if($UpdateOk -ne $true)
    {
        $errMessage = $IsOk.message
        # ok, so it's a last try and we've failed, so let someone know
        $Message = "Tenant: $TenantUrl <br /><br /> $errMessage"
        $Recipient = $EmailTo

        Write-Warning "Database Schema Update Failed, Email will be sent to $EmailTo with details $UpdateResult"

        EmailError `
            -EmailTo $Recipient `
            -RunbookName "UpdateSchemas" `
            -MessageBody $Message
    }
    else
    {
        Write-Verbose "Completed updating schema, no errors reported"
        $emailMessage = $UpdateResult

        SendEmail `
		-Subject "Database Schema Update Completed" `
		-Credential $SmtpCredential `
		-EmailTo $EmailTo `
		-MessageBody "The following was the log from the update. <br/><br/>$emailMessage"
    }
}
