workflow TenantPeriodicExecute
{
  Param
  (
    # Input parameters
    [Parameter (Mandatory = $true)]
    [string] $TenantUrl,

    [Parameter (Mandatory = $true)]
    [string] $EmailTo,
 
    [parameter(Mandatory=$true)]
    [PSCredential] $Credential
  )
    $VerbosePreference = 'continue'

    $IsOk = InlineScript
    {
        $Url = $Using:TenantUrl
        $EndPoint = "https://$Url/portal/periodicexecution/execute?format=text"
        
        Write-Verbose "Hitting periodic excecution at $EndPoint"

        $LoopCount = 1

        Do
        {
            try
            {
                $PE = Invoke-RestMethod -Method 'Get' -Uri $EndPoint -Credential $Using:Credential

                Write-Verbose "Completed periodic execution of $EndPoint with result of $PE"

                # get out of the loop, everything is good
                $LoopCount = 4
                return $true
            }
            catch
            {
                $LoopCount++

                If($LoopCount -eq 4)
                {
                     return $_.ErrorDetails.Message
                }
                else
                {
                    Write-Warning "There have been $LoopCount failed periodic execution(s) of $EndPoint"
                    Write-Warning $_.Exception|format-list -force
                    # ok, so still some loops to go, let's pause awhile to see if things sort themselves out
                    Start-Sleep -s 5
                }
            }
        } While ($LoopCount -le 3)
    }

    if($IsOk -ne $true)
    {
        # ok, so it's a last try and we've failed, so let someone know
        $Message = "Tenant: $TenantUrl <br /><br /> $IsOk"
        $Recipient = $EmailTo

        Write-Warning "Periodic Execution Failed for $TenantUrl, Email will be sent to $EmailTo with details $IsOk"

        EmailError `
            -EmailTo $Recipient `
            -RunbookName "TenantPeriodicExecute" `
            -MessageBody $Message

        # Now return something up to the caller to the let them know how things went
        $Result = @{success=$false;tenant=$TenantUrl}
        $Result
    }
    else
    {
        # Now return something up to the caller to the let them know how things went
        $Result = @{success=$true;tenant=$TenantUrl}
        $Result
    }
}