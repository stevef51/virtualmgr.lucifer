workflow EmailError
{
  Param
  (
    # Input parameters
    [Parameter (Mandatory = $true)]
    [string] $RunbookName,
 
    [Parameter (Mandatory = $true)]
    [string] $MessageBody,

    [Parameter (Mandatory = $true)]
    [string] $EmailTo
  )
    $SmtpCredential = Get-AutomationPSCredential -Name "evs-smtp" 
    $Subj = "Failed job: $RunbookName"
    $Message = "Error message: <br /><br /><br /><br /><br /> $MessageBody"
    $Recipient = $EmailTo

    # We just wrap up things for the sendemail runbook and tailor things for an error email
    SendEmail `
        -Credential $SmtpCredential `
        -EmailTo $Recipient `
        -Subject $Subj `
        -MessageBody $Message

}