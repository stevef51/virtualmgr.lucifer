FROM node

# Install git, process tools, unzip, node
RUN apt update && apt install -y procps unzip \
    curl nodejs

# Azure AKS CLI
RUN curl -sL https://aka.ms/InstallAzureCLIDeb | bash

# Install kubectl
RUN curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl && \
    chmod +x ./kubectl && \
    mv ./kubectl /usr/local/bin/kubectl

# Install helm
WORKDIR /home
RUN curl https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 > get_helm.sh && \
    chmod +x get_helm.sh && \
    ./get_helm.sh && \
    az login && \
    az account set --subscription a25f9a86-51e8-42b3-801a-fe9e450c2964 && \
    az aks get-credentials --resource-group lucifer-dev --name lucifer-dev-centralus

RUN npm install -g typescript

# Install MongoDb CLI
RUN wget -qO - https://www.mongodb.org/static/pgp/server-4.2.asc | apt-key add - && \
    echo "deb http://repo.mongodb.org/apt/debian stretch/mongodb-org/4.2 main" | tee /etc/apt/sources.list.d/mongodb-org-4.2.list && \
    apt-get update && \
    apt-get install -y mongodb-org-shell

