
FROM mcr.microsoft.com/dotnet/core/sdk:2.2

# Install git, process tools, unzip
RUN apt-get update && apt-get -y install git procps unzip \
    curl apt-transport-https debconf-utils apt-utils make

# Install NodeJs
RUN curl -sL https://deb.nodesource.com/setup_11.x | bash - 
RUN apt-get -y install nodejs 

# Install sqlpackage
RUN \
    wget -O /tmp/sqlpackage.zip https://go.microsoft.com/fwlink/?linkid=2087431 && \
    mkdir /usr/sbin/sqlpackage && \
    unzip /tmp/sqlpackage.zip -d /usr/sbin/sqlpackage && \
    chmod a+x /usr/sbin/sqlpackage/sqlpackage

ENV PATH="${PATH}:/usr/sbin/sqlpackage"

# adding custom MS repository
RUN curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add -
RUN curl https://packages.microsoft.com/config/ubuntu/16.04/prod.list > /etc/apt/sources.list.d/mssql-release.list

# install SQL Server drivers and tools
RUN apt-get update && ACCEPT_EULA=Y apt-get install -y msodbcsql mssql-tools
RUN echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> ~/.bashrc
RUN /bin/bash -c "source ~/.bashrc"


# Locale stuff so sqlcmd works .. (from https://daten-und-bass.io/blog/fixing-missing-locale-setting-in-ubuntu-docker-image/)
RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive apt-get install -y locales \
    && sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen \
    && dpkg-reconfigure --frontend=noninteractive locales \
    && update-locale LANG=en_US.UTF-8
ENV LANG en_US.UTF-8 
ENV LC_ALL en_US.UTF-8

# Clean up
RUN apt-get autoremove -y \
    && apt-get clean -y \
    && rm -rf /var/lib/apt/lists/* \
    && rm -rf /tmp/sqlpackage.zip

# Set the default shell to bash instead of sh
ENV SHELL /bin/bash

# Install typescript globally
RUN npm install -g typescript