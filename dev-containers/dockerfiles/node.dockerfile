FROM node

# Install git, process tools, unzip
RUN apt-get update && apt-get -y install git procps unzip \
    curl apt-transport-https debconf-utils apt-utils make

# Clean up
RUN apt-get autoremove -y \
    && apt-get clean -y \
    && rm -rf /var/lib/apt/lists/* 

# Set the default shell to bash instead of sh
ENV SHELL /bin/bash

# Install typescript globally
RUN npm install -g typescript