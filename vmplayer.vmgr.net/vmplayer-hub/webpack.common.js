﻿const path = require('path');
const webpack = require('webpack');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const ManifestPlugin = require('webpack-manifest-plugin');
const ExtractTextWebpackPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

const { InjectManifest } = require('workbox-webpack-plugin');

module.exports = {
    entry: {
        app: './wwwroot/src/app.js'
    },
    output: {
        path: path.resolve(__dirname, 'wwwroot/dist'),
        filename: '[name].bundle.[hash].js',
        publicPath: '/'
    },
    /*    watchOptions: {
            aggregateTimeout: 1000,
            poll: 5000
        }, */
    plugins: [
        new ManifestPlugin(),                   // Produces the Webpack manifest.json in dist folder
        new CleanWebpackPlugin(),               // This cleans out the ./wwwroot/dist folder on each build, only for dev really as prod uses Docker which is always clean anyway
        new ExtractTextWebpackPlugin('[name].bundle.css'),
        new HtmlWebpackPlugin({
            filename: 'index.html',
            inject: false,                          // We will insert the bundles manually in the template
            template: path.resolve(__dirname, 'wwwroot/src/index.ejs'),
            environment: process.env.NODE_ENV
        }),
        new webpack.EnvironmentPlugin({
            NODE_ENV: 'Production'              // Use 'Production' unless process.env.NODE_ENV is defined
        }),
        new BundleAnalyzerPlugin({
            analyzerHost: '0.0.0.0'
        }),
        new InjectManifest({
            swSrc: 'wwwroot/service-worker.js',
        })
    ],
    module: {
        rules: [{
            test: /\.js?$/,
            exclude: /node_modules/,
            use: {
                loader: 'babel-loader',
                options: {
                    compact: true,
                    presets: [
                        '@babel/preset-env'
                    ],
                    plugins: ['angularjs-annotate']     // This will fix any Angular functions not using explicit DI 
                    // (eg function($scope) instead of ['$scope', function($scope)])
                }
            }
        },
        {
            test: /template\.html$/,
            use: ['raw-loader']
        },
        {
            test: /\.(css|scss)$/,
            use: ExtractTextWebpackPlugin.extract({
                use: ['css-loader', 'sass-loader']
            })
        },
        {
            test: /\.(png|svg|jpg|jpeg|gif)$/,
            use: [
                'file-loader'
            ]
        },
        {
            test: /\.(woff|woff2|eot|ttf|otf)$/,
            use: [
                {
                    loader: 'file-loader',
                    options: {
                        name: '[name].[ext]',
                        outputPath: 'fonts/'
                    }
                }
            ]
        },
        /*
         * Necessary to be able to use angular 1 with webpack as explained in https://github.com/webpack/webpack/issues/2049
         */
        {
            test: require.resolve('angular'),
            loader: 'exports-loader?window.angular'
        }
        ]
    }
};