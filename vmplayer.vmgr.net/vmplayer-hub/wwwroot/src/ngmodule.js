'use strict';

import angular from 'angular';
import 'angular-ui-router';
import 'angular-animate';
import 'angular-material';
import 'angular-material/angular-material.css';

export default angular.module('vmplayer-hub', [
    'ui.router',
    'ui.router.util',
    'ui.router.router',
    'ui.router.state',
    'ngMaterial'
]);