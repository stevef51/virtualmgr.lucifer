'use strict';

import app from './ngmodule';

app.config(
    function ($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state('root', {
                abstract: true,
                url: '',
                views: {
                    'body@': {
                        component: 'bodyCtrl'
                    },
                    'header-control@root': {
                        component: 'rootHeaderCtrl'
                    },
                    'content-control@root': {
                        component: 'rootContentCtrl'
                    },
                    'header-right@root': {
                        component: 'rootHeaderRightCtrl'
                    }
                }
            })

            .state('settings', {
                url: '/settings',
                views: {
                    'body@': {
                        component: 'settingsCtrl'
                    }
                }
            })

        $urlRouterProvider.otherwise('/settings');

    }
);


