﻿'use strict';

import angular from 'angular';

import './body.scss';
import 'typeface-roboto';
import './views';
import './config';
import './routes';

const environment = process.env.NODE_ENV;
console.log(`Environment is ${environment}`);


angular.bootstrap(document.documentElement, ['vmplayer-hub'], {
    strictDi: true
});
