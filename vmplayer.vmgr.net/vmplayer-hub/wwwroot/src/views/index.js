'use strict';

import './body';
import './root-header';
import './root-header-right';
import './root-content';
import './settings-ctrl';