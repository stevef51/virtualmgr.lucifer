if (workbox) {
    console.log(`Yay! Workbox is loaded 🎉 running on ${self.location}`);

    workbox.precaching.precacheAndRoute(self.__precacheManifest)

} else {
    console.log(`Boo! Workbox didn't load 😬`);
}

