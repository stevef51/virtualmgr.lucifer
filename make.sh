#!/bin/bash
cd src
cd VirtualMgr.SPA && ./make.sh && cd ..
cd VirtualMgr.ODatav4 && ./make.sh && cd ..
cd VirtualMgr.Media && ./make.sh && cd ..
cd VirtualMgr.Identity.Server && ./make.sh && cd ..
cd VirtualMgr.Api.Lingo && ./make.sh && cd ..
cd VirtualMgr.Tenant && ./make.sh && cd ..
